--[Blank Grids]
--This is an autogenerated file.

--[Constants]
local ciIsNotWall = 0
local ciIsWall = 1
local ciNeedWallHi = -1
local ciNeedBlackout = -1

--High Wall
local ciWallHigh_EW = 0
local ciWallHigh_NS = 1
local ciWallHigh_Corner_ULN = 2
local ciWallHigh_Corner_ULS = 3
local ciWallHigh_Corner_URN = 4
local ciWallHigh_Corner_URS = 5
local ciWallHigh_Junction_LN = 6
local ciWallHigh_Junction_LS = 7
local ciWallHigh_Junction_RN = 8
local ciWallHigh_Junction_RS = 9
local ciWallHigh_Junction_MN = 10
local ciWallHigh_Junction_MS = 11
local ciWallHigh_Corner_LL = 12
local ciWallHigh_Corner_LR = 13

--Blackout
local ciBlackout_None = -1
local ciBlackout_Full = 0
local ciBlackout_CornerLL = 1
local ciBlackout_LowMiddle = 2
local ciBlackout_CornerLR = 3
local ciBlackout_CornerIL = 4
local ciBlackout_CornerIR = 5
local ciBlackout_Left = 6
local ciBlackout_Right = 7

--[Grid Builder]
--[ ========================================= Low Bits ========================================== ]
local iaGrids = {}
iaGrids[  1] = {0, 0, 0,
                0, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}

iaGrids[  2] = {1, 0, 0,
                0, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
			
iaGrids[  3] = {0, 1, 0,
                0, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[  4] = {1, 1, 0,
                0, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
			
--[ =========================================== 4 Bit =========================================== ]
iaGrids[  5] = {0, 0, 1,
                0, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[  6] = {1, 0, 1,
                0, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[  7] = {0, 1, 1,
                0, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[  8] = {1, 1, 1,
                0, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
			
--[ =========================================== 8 Bit =========================================== ]
iaGrids[  9] = {0, 0, 0,
                1, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 10] = {1, 0, 0,
                1, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 11] = {0, 1, 0,
                1, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[ 12] = {1, 1, 0,
                1, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[ 13] = {0, 0, 1,
                1, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 14] = {1, 0, 1,
                1, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 15] = {0, 1, 1,
                1, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[ 16] = {1, 1, 1,
                1, 1, 0,
                0, 0, 0,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
			
			
			
--[ =========================================== 16 Bit ========================================== ]
iaGrids[ 17] = {0, 0, 0,
                0, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 18] = {1, 0, 0,
                0, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 19] = {0, 1, 0,
                0, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[ 20] = {1, 1, 0,
                0, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[ 21] = {0, 0, 1,
                0, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 22] = {1, 0, 1,
                0, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 23] = {0, 1, 1,
                0, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[ 24] = {1, 1, 1,
                0, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[ 25] = {0, 0, 0,
                1, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 26] = {1, 0, 0,
                1, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 27] = {0, 1, 0,
                1, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 28] = {1, 1, 0,
                1, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 29] = {0, 0, 1,
                1, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 30] = {1, 0, 1,
                1, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 31] = {0, 1, 1,
                1, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 32] = {1, 1, 1,
                1, 1, 1,
                0, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
			
			
--[ =========================================== 32 Bit ========================================== ]
iaGrids[ 33] = {0, 0, 0,
                0, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 34] = {1, 0, 0,
                0, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 35] = {0, 1, 0,
                0, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 36] = {1, 1, 0,
                0, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 37] = {0, 0, 1,
                0, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 38] = {1, 0, 1,
                0, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 39] = {0, 1, 1,
                0, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 40] = {1, 1, 1,
                0, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 41] = {0, 0, 0,
                1, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 42] = {1, 0, 0,
                1, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 43] = {0, 1, 0,
                1, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[ 44] = {1, 1, 0,
                1, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[ 45] = {0, 0, 1,
                1, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 46] = {1, 0, 1,
                1, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 47] = {0, 1, 1,
                1, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[ 48] = {1, 1, 1,
                1, 1, 0,
                1, 0, 0,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[ 49] = {0, 0, 0,
                0, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 50] = {1, 0, 0,
                0, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 51] = {0, 1, 0,
                0, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[ 52] = {1, 1, 0,
                0, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[ 53] = {0, 0, 1,
                0, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 54] = {1, 0, 1,
                0, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 55] = {0, 1, 1,
                0, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[ 56] = {1, 1, 1,
                0, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[ 57] = {0, 0, 0,
                1, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 58] = {1, 0, 0,
                1, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 59] = {0, 1, 0,
                1, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 60] = {1, 1, 0,
                1, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 61] = {0, 0, 1,
                1, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 62] = {1, 0, 1,
                1, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 63] = {0, 1, 1,
                1, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 64] = {1, 1, 1,
                1, 1, 1,
                1, 0, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
			
			
			
			
--[ =========================================== 64 Bit ========================================== ]
iaGrids[ 65] = {0, 0, 0,
                0, 1, 0,
                0, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 66] = {1, 0, 0,
                0, 1, 0,
                0, 1, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 67] = {0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_None}
iaGrids[ 68] = {1, 1, 0,
                0, 1, 0,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_None}
iaGrids[ 69] = {0, 0, 1,
                0, 1, 0,
                0, 1, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 70] = {1, 0, 1,
                0, 1, 0,
                0, 1, 0,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[ 71] = {0, 1, 1,
                0, 1, 0,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Corner_ULS, ciBlackout_None}
iaGrids[ 72] = {1, 1, 1,
                0, 1, 0,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_MS, ciBlackout_None}
iaGrids[ 73] = {0, 0, 0,
                1, 1, 0,
                0, 1, 0,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_None}
iaGrids[ 74] = {1, 0, 0,
                1, 1, 0,
                0, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[ 75] = {0, 1, 0,
                1, 1, 0,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_None}
iaGrids[ 76] = {1, 1, 0,
                1, 1, 0,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_None}
iaGrids[ 77] = {0, 0, 1,
                1, 1, 0,
                0, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[ 78] = {1, 0, 1,
                1, 1, 0,
                0, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[ 79] = {0, 1, 1,
                1, 1, 0,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_None}
iaGrids[ 80] = {1, 1, 1,
                1, 1, 0,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_None}
iaGrids[ 81] = {0, 0, 0,
                0, 1, 1,
                0, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[ 82] = {1, 0, 0,
                0, 1, 1,
                0, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[ 83] = {0, 1, 0,
                0, 1, 1,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_None}
iaGrids[ 84] = {1, 1, 0,
                0, 1, 1,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_None}
iaGrids[ 85] = {0, 0, 1,
                0, 1, 1,
                0, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[ 86] = {1, 0, 1,
                0, 1, 1,
                0, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[ 87] = {0, 1, 1,
                0, 1, 1,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_None}
iaGrids[ 88] = {1, 1, 1,
                0, 1, 1,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_None}
iaGrids[ 89] = {0, 0, 0,
                1, 1, 1,
                0, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 90] = {1, 0, 0,
                1, 1, 1,
                0, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 91] = {0, 1, 0,
                1, 1, 1,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 92] = {1, 1, 0,
                1, 1, 1,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 93] = {0, 0, 1,
                1, 1, 1,
                0, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 94] = {1, 0, 1,
                1, 1, 1,
                0, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 95] = {0, 1, 1,
                1, 1, 1,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 96] = {1, 1, 1,
                1, 1, 1,
                0, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 97] = {0, 0, 0,
                0, 1, 0,
                1, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 98] = {1, 0, 0,
                0, 1, 0,
                1, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[ 99] = {0, 1, 0,
                0, 1, 0,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_None}
iaGrids[100] = {1, 1, 0,
                0, 1, 0,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_None}
iaGrids[101] = {0, 0, 1,
                0, 1, 0,
                1, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[102] = {1, 0, 1,
                0, 1, 0,
                1, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[103] = {0, 1, 1,
                0, 1, 0,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LS, ciBlackout_None}
iaGrids[104] = {1, 1, 1,
                0, 1, 0,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_MS, ciBlackout_None}
iaGrids[105] = {0, 0, 0,
                1, 1, 0,
                1, 1, 0,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_CornerIR}
iaGrids[106] = {1, 0, 0,
                1, 1, 0,
                1, 1, 0,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_CornerIR}
iaGrids[107] = {0, 1, 0,
                1, 1, 0,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_CornerIR}
iaGrids[108] = {1, 1, 0,
                1, 1, 0,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_Left}
iaGrids[109] = {0, 0, 1,
                1, 1, 0,
                1, 1, 0,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_CornerIR}
iaGrids[110] = {1, 0, 1,
                1, 1, 0,
                1, 1, 0,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_CornerIR}
iaGrids[111] = {0, 1, 1,
                1, 1, 0,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_CornerIR}
iaGrids[112] = {1, 1, 1,
                1, 1, 0,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Corner_ULS, ciBlackout_Left}
iaGrids[113] = {0, 0, 0,
                0, 1, 1,
                1, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[114] = {1, 0, 0,
                0, 1, 1,
                1, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[115] = {0, 1, 0,
                0, 1, 1,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_None}
iaGrids[116] = {1, 1, 0,
                0, 1, 1,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_None}
iaGrids[117] = {0, 0, 1,
                0, 1, 1,
                1, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[118] = {1, 0, 1,
                0, 1, 1,
                1, 1, 0,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_None}
iaGrids[119] = {0, 1, 1,
                0, 1, 1,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_None}
iaGrids[120] = {1, 1, 1,
                0, 1, 1,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_None}
iaGrids[121] = {0, 0, 0,
                1, 1, 1,
                1, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_CornerIR}
iaGrids[122] = {1, 0, 0,
                1, 1, 1,
                1, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_CornerIR}
iaGrids[123] = {0, 1, 0,
                1, 1, 1,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_MN, ciBlackout_CornerIR}
iaGrids[124] = {1, 1, 0,
                1, 1, 1,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_Left}
iaGrids[125] = {0, 0, 1,
                1, 1, 1,
                1, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_CornerIR}
iaGrids[126] = {1, 0, 1,
                1, 1, 1,
                1, 1, 0,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_CornerIR}
iaGrids[127] = {0, 1, 1,
                1, 1, 1,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Junction_MN, ciBlackout_CornerIR}
iaGrids[128] = {1, 1, 1,
                1, 1, 1,
                1, 1, 0,
                ciIsNotWall, ciWallHigh_Corner_ULN, ciBlackout_Left}
			
			
			
			
			
		
--[ ========================================== 128 Bit ========================================== ]
iaGrids[129] = {0, 0, 0,
                0, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[130] = {1, 0, 0,
                0, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[131] = {0, 1, 0,
                0, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[132] = {1, 1, 0,
                0, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[133] = {0, 0, 1,
                0, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[134] = {1, 0, 1,
                0, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[135] = {0, 1, 1,
                0, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[136] = {1, 1, 1,
                0, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[137] = {0, 0, 0,
                1, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[138] = {1, 0, 0,
                1, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[139] = {0, 1, 0,
                1, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[140] = {1, 1, 0,
                1, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[141] = {0, 0, 1,
                1, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[142] = {1, 0, 1,
                1, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[143] = {0, 1, 1,
                1, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[144] = {1, 1, 1,
                1, 1, 0,
                0, 0, 1,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[145] = {0, 0, 0,
                0, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[146] = {1, 0, 0,
                0, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[147] = {0, 1, 0,
                0, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[148] = {1, 1, 0,
                0, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[149] = {0, 0, 1,
                0, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[150] = {1, 0, 1,
                0, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[151] = {0, 1, 1,
                0, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[152] = {1, 1, 1,
                0, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[153] = {0, 0, 0,
                1, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[154] = {1, 0, 0,
                1, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[155] = {0, 1, 0,
                1, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[156] = {1, 1, 0,
                1, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[157] = {0, 0, 1,
                1, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[158] = {1, 0, 1,
                1, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[159] = {0, 1, 1,
                1, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[160] = {1, 1, 1,
                1, 1, 1,
                0, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[161] = {0, 0, 0,
                0, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[162] = {1, 0, 0,
                0, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[163] = {0, 1, 0,
                0, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[164] = {1, 1, 0,
                0, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[165] = {0, 0, 1,
                0, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[166] = {1, 0, 1,
                0, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[167] = {0, 1, 1,
                0, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[168] = {1, 1, 1,
                0, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[169] = {0, 0, 0,
                1, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[170] = {1, 0, 0,
                1, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[171] = {0, 1, 0,
                1, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[172] = {1, 1, 0,
                1, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[173] = {0, 0, 1,
                1, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[174] = {1, 0, 1,
                1, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[175] = {0, 1, 1,
                1, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[176] = {1, 1, 1,
                1, 1, 0,
                1, 0, 1,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_None}
iaGrids[177] = {0, 0, 0,
                0, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[178] = {1, 0, 0,
                0, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[179] = {0, 1, 0,
                0, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[180] = {1, 1, 0,
                0, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[181] = {0, 0, 1,
                0, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[182] = {1, 0, 1,
                0, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[183] = {0, 1, 1,
                0, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[184] = {1, 1, 1,
                0, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_None}
iaGrids[185] = {0, 0, 0,
                1, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[186] = {1, 0, 0,
                1, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[187] = {0, 1, 0,
                1, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[188] = {1, 1, 0,
                1, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[189] = {0, 0, 1,
                1, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[190] = {1, 0, 1,
                1, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[191] = {0, 1, 1,
                1, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[192] = {1, 1, 1,
                1, 1, 1,
                1, 0, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[193] = {0, 0, 0,
                0, 1, 0,
                0, 1, 1,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[194] = {1, 0, 0,
                0, 1, 0,
                0, 1, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[195] = {0, 1, 0,
                0, 1, 0,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_None}
iaGrids[196] = {1, 1, 0,
                0, 1, 0,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Corner_URS, ciBlackout_None}
iaGrids[197] = {0, 0, 1,
                0, 1, 0,
                0, 1, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[198] = {1, 0, 1,
                0, 1, 0,
                0, 1, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_None}
iaGrids[199] = {0, 1, 1,
                0, 1, 0,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Corner_ULS, ciBlackout_None}
iaGrids[200] = {1, 1, 1,
                0, 1, 0,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_MS, ciBlackout_None}
iaGrids[201] = {0, 0, 0,
                1, 1, 0,
                0, 1, 1,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_None}
iaGrids[202] = {1, 0, 0,
                1, 1, 0,
                0, 1, 1,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_None}
iaGrids[203] = {0, 1, 0,
                1, 1, 0,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_None}
iaGrids[204] = {1, 1, 0,
                1, 1, 0,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_None}
iaGrids[205] = {0, 0, 1,
                1, 1, 0,
                0, 1, 1,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_None}
iaGrids[206] = {1, 0, 1,
                1, 1, 0,
                0, 1, 1,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_None}
iaGrids[207] = {0, 1, 1,
                1, 1, 0,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_RS, ciBlackout_None}
iaGrids[208] = {1, 1, 1,
                1, 1, 0,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_None}
iaGrids[209] = {0, 0, 0,
                0, 1, 1,
                0, 1, 1,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_CornerIL}
iaGrids[210] = {1, 0, 0,
                0, 1, 1,
                0, 1, 1,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_CornerIL}
iaGrids[211] = {0, 1, 0,
                0, 1, 1,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_CornerIL}
iaGrids[212] = {1, 1, 0,
                0, 1, 1,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_CornerIL}
iaGrids[213] = {0, 0, 1,
                0, 1, 1,
                0, 1, 1,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_CornerIL}
iaGrids[214] = {1, 0, 1,
                0, 1, 1,
                0, 1, 1,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_CornerIR}
iaGrids[215] = {0, 1, 1,
                0, 1, 1,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_Right}
iaGrids[216] = {1, 1, 1,
                0, 1, 1,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Corner_URS, ciBlackout_Right}
iaGrids[217] = {0, 0, 0,
                1, 1, 1,
                0, 1, 1,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_CornerIL}
iaGrids[218] = {1, 0, 0,
                1, 1, 1,
                0, 1, 1,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_CornerIL}
iaGrids[219] = {0, 1, 0,
                1, 1, 1,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_MN, ciBlackout_CornerIL}
iaGrids[220] = {1, 1, 0,
                1, 1, 1,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_MN, ciBlackout_CornerIL}
iaGrids[221] = {0, 0, 1,
                1, 1, 1,
                0, 1, 1,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_CornerIL}
iaGrids[222] = {1, 0, 1,
                1, 1, 1,
                0, 1, 1,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_CornerIL}
iaGrids[223] = {0, 1, 1,
                1, 1, 1,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_Right}
iaGrids[224] = {1, 1, 1,
                1, 1, 1,
                0, 1, 1,
                ciIsNotWall, ciWallHigh_Corner_URN, ciBlackout_Right}
iaGrids[225] = {0, 0, 0,
                0, 1, 0,
                1, 1, 1,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[226] = {1, 0, 0,
                0, 1, 0,
                1, 1, 1,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[227] = {0, 1, 0,
                0, 1, 0,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_None}
iaGrids[228] = {1, 1, 0,
                0, 1, 0,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_Corner_URS, ciBlackout_None}
iaGrids[229] = {0, 0, 1,
                0, 1, 0,
                1, 1, 1,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[230] = {1, 0, 1,
                0, 1, 0,
                1, 1, 1,
                ciIsWall, ciWallHigh_Junction_MN, ciBlackout_None}
iaGrids[231] = {0, 1, 1,
                0, 1, 0,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_None}
iaGrids[232] = {1, 1, 1,
                0, 1, 0,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_MS, ciBlackout_None}
iaGrids[233] = {0, 0, 0,
                1, 1, 0,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_CornerIR}
iaGrids[234] = {1, 0, 0,
                1, 1, 0,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_CornerIR}
iaGrids[235] = {0, 1, 0,
                1, 1, 0,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_CornerIR}
iaGrids[236] = {1, 1, 0,
                1, 1, 0,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_Left}
iaGrids[237] = {0, 0, 1,
                1, 1, 0,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_CornerIR}
iaGrids[238] = {1, 0, 1,
                1, 1, 0,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_URN, ciBlackout_CornerIR}
iaGrids[239] = {0, 1, 1,
                1, 1, 0,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_RN, ciBlackout_CornerIR}
iaGrids[240] = {1, 1, 1,
                1, 1, 0,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_Left}
iaGrids[241] = {0, 0, 0,
                0, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_CornerIL}
iaGrids[242] = {1, 0, 0,
                0, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_CornerIL}
iaGrids[243] = {0, 1, 0,
                0, 1, 1,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_CornerIL}
iaGrids[244] = {1, 1, 0,
                0, 1, 1,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_CornerIL}
iaGrids[245] = {0, 0, 1,
                0, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_CornerIL}
iaGrids[246] = {1, 0, 1,
                0, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_ULN, ciBlackout_CornerIL}
iaGrids[247] = {0, 1, 1,
                0, 1, 1,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_NS, ciBlackout_Right}
iaGrids[248] = {1, 1, 1,
                0, 1, 1,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_LN, ciBlackout_Right}
iaGrids[249] = {0, 0, 0,
                1, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_LowMiddle}
iaGrids[250] = {1, 0, 0,
                1, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_LowMiddle}
iaGrids[251] = {0, 1, 0,
                1, 1, 1,
                1, 1, 1,
                ciIsNotWall, ciWallHigh_Junction_MN, ciBlackout_LowMiddle}
iaGrids[252] = {1, 1, 0,
                1, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_LL, ciBlackout_CornerLL}
iaGrids[253] = {0, 0, 1,
                1, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_LowMiddle}
iaGrids[254] = {1, 0, 1,
                1, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_EW, ciBlackout_LowMiddle}
iaGrids[255] = {0, 1, 1,
                1, 1, 1,
                1, 1, 1,
                ciIsWall, ciWallHigh_Corner_LR, ciBlackout_CornerLR}
iaGrids[256] = {1, 1, 1,
                1, 1, 1,
                1, 1, 1,
                ciIsNotWall, ciNeedWallHi, ciBlackout_Full}

--[Upload]
--Send the information to the static listing.
for i = 1, 256, 1 do
	AdlevGenerator_SetProperty("Wall Grid", iaGrids[i][1], iaGrids[i][2], iaGrids[i][3], iaGrids[i][4], iaGrids[i][5], iaGrids[i][6], iaGrids[i][7], iaGrids[i][8], iaGrids[i][9], iaGrids[i][10], iaGrids[i][11], iaGrids[i][12])
	
end
