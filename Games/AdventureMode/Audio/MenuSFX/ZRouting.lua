--[Menu Audio]
--Adventure Mode's menus have a few more sound effects than the baseline menu.
local sGUISFXPath = fnResolvePath()
AudioManager_Register("Menu|Levelup",         "AsSound", "AsSample", sGUISFXPath .. "Levelup.wav")
AudioManager_Register("Menu|MetalPing",       "AsSound", "AsSample", sGUISFXPath .. "MetalPing.ogg")
AudioManager_Register("Menu|BuyOrSell",       "AsSound", "AsSample", sGUISFXPath .. "ShopBuyOrSell.wav")
AudioManager_Register("Menu|TextTick",        "AsSound", "AsSample", sGUISFXPath .. "TextTick.ogg")
AudioManager_Register("Menu|TrueLevelUp",     "AsSound", "AsSample", sGUISFXPath .. "TrueLevelUp.wav")
AudioManager_Register("Menu|SpecialItem",     "AsSound", "AsSample", sGUISFXPath .. "GotImportantItem.ogg")