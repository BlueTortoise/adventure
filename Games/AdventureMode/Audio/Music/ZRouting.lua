--[ ===================================== Music Routing File ==================================== ]
--[Variables]
local sBasePath = fnResolvePath()

--[Registration Function]
--Loads music with set loops.
local function fnRegisterMusic(psMusicName, psPath, pfLoopStart, pfLoopEnd)
	
	--Arg check.
	if(psMusicName == nil) then return end
	if(psPath == nil) then return end
	
	--If the pfLoopStart and/or pfLoopEnd are nil, then this doesn't loop.
	if(pfLoopStart == nil or pfLoopEnd == nil) then
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath)
		
	--Otherwise, register with looping.
	else
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath, pfLoopStart, pfLoopEnd)
	end

end

--[Ambient Tracks]
local sAmbientPath = sBasePath .. "Ambient Tracks/"
fnRegisterMusic("DatacoreAmbient", sAmbientPath .. "DatacoreAmbient.ogg", 0.000,        6.281)
fnRegisterMusic("RegulusCave",     sAmbientPath .. "RegulusCave.ogg",     0.000,        8.000)
fnRegisterMusic("NigissuSegQuppu", sAmbientPath .. "NigissuSegQuppu.ogg", 0.000, 300 + 45.080)
fnRegisterMusic("PowerCore",       sAmbientPath .. "PowerCore.ogg",       2.000,       14.146 + 2.00)
fnRegisterMusic("FactoryFloor",    sAmbientPath .. "FactoryFloor.ogg",    3.000,       18.000)

--[Battle Themes]
local sBattlePath = sBasePath .. "Battle Themes/"
fnRegisterMusic("BattleThemeMei",       sBattlePath .. "Battle Theme Mei.ogg",        5.658, 60 + 42.088)
fnRegisterMusic("BattleThemeSanya",     sBattlePath .. "Battle Theme Sanya.ogg",     11.781, 60 + 23.822)
fnRegisterMusic("BattleThemeChristine", sBattlePath .. "Battle Theme Christine.ogg",  6.371, 60 + 36.367)
fnRegisterMusic("MotherTheme",          sBattlePath .. "ThemeOfTheMother.ogg",        0.000, 60 +  3.985)
fnRegisterMusic("RottenTheme",          sBattlePath .. "ThemeOfTheRotten.ogg",        2.000, 60 + 33.195)
fnRegisterMusic("CombatVictory",        sBattlePath .. "CombatVictory.ogg")
fnRegisterMusic("DesolateShort",        sBattlePath .. "Desolate Shortened.ogg")

--[Character Themes]
local sCharThemePath = sBasePath .. "Character Themes/"
fnRegisterMusic("2855sTheme",       sCharThemePath .. "2855s Theme.ogg",            36.514, 120 + 11.656)
fnRegisterMusic("AdinasTheme",      sCharThemePath .. "AdinasTheme.ogg",             1.423,  60 + 30.750)
fnRegisterMusic("BreannesTheme",    sCharThemePath .. "Breannes Theme.ogg",         13.735,  60 + 36.459)
fnRegisterMusic("FlorentinasTheme", sCharThemePath .. "Florentinas Theme.ogg",      15.557,       79.941)
fnRegisterMusic("NadiasTheme",      sCharThemePath .. "Nadias Theme.ogg",            0.000,       39.746)
fnRegisterMusic("SophiesTheme",     sCharThemePath .. "Sophies Theme.ogg",           8.990,  60 + 31.416)
fnRegisterMusic("SophiesThemeSlow", sCharThemePath .. "Sophies Theme Slow.ogg",      0.000,       45.806)
fnRegisterMusic("SophiesIntro",     sCharThemePath .. "Sophies Theme Slow Open.ogg")
fnRegisterMusic("Vivify",           sCharThemePath .. "Vivify.ogg",                 11.529,       39.620)

--I had way too much fun with this.
fnRegisterMusic("SophieToTheRescue", sCharThemePath .. "Sophie to the Rescue.ogg", 16.191, 53.015)

--[Layered Tracks]
local sLayeredPath = sBasePath .. "Layered Tracks/"
fnRegisterMusic("GalaTenseLayer",        sLayeredPath .. "GalaTension.ogg",      0.000, 60 +  7.450)
fnRegisterMusic("GalaPianoDistantLayer", sLayeredPath .. "GalaPianoDistant.ogg", 0.000, 60 +  7.450)
fnRegisterMusic("GalaPianoLayer",        sLayeredPath .. "GalaPiano.ogg",        0.000, 60 +  7.450)

--Cultist Combat
fnRegisterMusic("CultistPassive", sLayeredPath .. "CultistPassive.ogg", 0.000, 60 + 53.430)
fnRegisterMusic("CultistTense",   sLayeredPath .. "CultistTense.ogg",   0.000, 60 + 53.430)
fnRegisterMusic("CultistCombat",  sLayeredPath .. "CultistCombat.ogg",  0.000, 60 + 53.430)

--LRT Music
fnRegisterMusic("TelecommLayer0", sLayeredPath .. "Telecomm0.ogg", 20.000, 20.000 + 21.328)
fnRegisterMusic("TelecommLayer1", sLayeredPath .. "Telecomm1.ogg", 20.000, 20.000 + 21.328)
fnRegisterMusic("TelecommLayer2", sLayeredPath .. "Telecomm2.ogg", 20.000, 20.000 + 21.328)

--The Administrator
fnRegisterMusic("Admin0", sLayeredPath .. "Admin0.ogg", 0.000, 19.197)
fnRegisterMusic("Admin1", sLayeredPath .. "Admin1.ogg", 0.000, 19.197)
fnRegisterMusic("Admin2", sLayeredPath .. "Admin2.ogg", 0.000, 19.197)
fnRegisterMusic("Admin3", sLayeredPath .. "Admin3.ogg", 0.000, 19.197)

--[Misc Tracks]
local sMiscPath = sBasePath .. "Misc Tracks/"
fnRegisterMusic("ABillionEyes",   sMiscPath .. "A Billion Eyes.ogg",      0.000,       30.067)
fnRegisterMusic("Briefing",       sMiscPath .. "Briefing.ogg",            2.432,       33.057)
fnRegisterMusic("ThemeOfCourage", sMiscPath .. "Theme of Courage.ogg",    3.734,       25.615)
fnRegisterMusic("Waltz",          sMiscPath .. "WaltzNo2.ogg")
fnRegisterMusic("TimeSensitive",  sMiscPath .. "TimeSensitive.ogg", 120 + 7.561, 120 + 34.408)
fnRegisterMusic("BattleAmbience", sMiscPath .. "BattleAmbience.ogg",      0.000,  60 + 30.000)
fnRegisterMusic("Special|War",    sMiscPath .. "Battle.ogg")

--Dummy Theme. Plays no sound, used for special effects.
fnRegisterMusic("DummyTrack", sMiscPath .. "DummyTrack.ogg", 0.000, 4.000)

--[World Themes]
local sWorldPath = sBasePath .. "World Themes/"
fnRegisterMusic("Apprehension",         sWorldPath .. "Apprehension.ogg",           60.064, 120 +  6.6661)
fnRegisterMusic("Biolabs",              sWorldPath .. "Biolabs.ogg",                46.553, 180 +  4.799)
fnRegisterMusic("ChristineDream",       sWorldPath .. "ChristineDream.ogg",          1.961,  60 + 36.000)
fnRegisterMusic("EquinoxTheme",         sWorldPath .. "EquinoxTheme.ogg",            9.101, 180 + 52.556)
fnRegisterMusic("ForestTheme",          sWorldPath .. "Forest Theme.ogg",            4.813,  60 + 57.546)
fnRegisterMusic("HighlandsTheme",       sWorldPath .. "Highlands Theme.ogg",         3.009,  60 +  1.009)
fnRegisterMusic("NixNedar",             sWorldPath .. "NixNedar.ogg",                4.702,  60 + 37.694)
fnRegisterMusic("QuantirManseTheme",    sWorldPath .. "QuantirMansionTheme.ogg",    42.270,  60 + 51.892)
fnRegisterMusic("QuantirManseThemeLow", sWorldPath .. "QuantirMansionThemeLow.ogg",  3.061,  60 + 13.285)
fnRegisterMusic("RegulusCity",          sWorldPath .. "Regulus City.ogg",            4.673,  60 +  7.384)
fnRegisterMusic("RegulusContainment",   sWorldPath .. "RegulusContainment.ogg",      2.187, 120 + 33.144)
fnRegisterMusic("RegulusTense",         sWorldPath .. "RegulusTense.ogg",           31.503, 120 + 23.504)
fnRegisterMusic("SerenityShort",        sWorldPath .. "SerenityShort.ogg",           1.500,        6.497)
fnRegisterMusic("SerenityObservatory",  sWorldPath .. "SerenityObservatory.ogg",    14.983,  60 +  0.001)
fnRegisterMusic("SerenityCrater",       sWorldPath .. "SerenityCrater.ogg",          0.000, 120 + 17.485)
fnRegisterMusic("SerenityTense",        sWorldPath .. "SerenityTense.ogg",          14.534,       49.836)
fnRegisterMusic("SprocketCity",         sWorldPath .. "Sprocket City.ogg",           2.423, 120 + 23.021)
fnRegisterMusic("TheyKnowWeAreHere",    sWorldPath .. "They Know We Are Here.ogg",  12.983,  60 + 14.159)
fnRegisterMusic("TownTheme",            sWorldPath .. "Town Theme.ogg",              6.404,       99.603)

--[Some Moths Do]
--The exciting ACTION MOVIE from Chapter 5.
local sMothPath = sBasePath .. "Some Moths Do/"
fnRegisterMusic("Moth Catchup",   sMothPath .. "MothCatchUp.ogg") --Does not loop.
fnRegisterMusic("Moth Passive",   sMothPath .. "MothPassive.ogg",   3.005,      36.501)
fnRegisterMusic("Moth Intense",   sMothPath .. "MothIntense.ogg",   4.000, 60 + 23.009)
fnRegisterMusic("Moth Speedboat", sMothPath .. "MothSpeedboat.ogg", 2.000, 8.009)
