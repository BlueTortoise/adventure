--[Voice Audio]
--These are the voice ticks that play when a character speaks.
local fLocalVolume = 0.45
local sVoicePath = fnResolvePath()

--[Main Characters]
--Function.
local fnRegisterVoice = function(sName, sFilename, fUseVolume)
    
    --Arg check.
    if(sName == nil) then return end
    
    --If no filename is provided, it's the same as the character name.
    if(sFilename == nil) then sFilename = sName end
    
    --Volume. If no volume is passed, used the local volume.
    local fApplyVolume = fLocalVolume
    if(fUseVolume ~= nil) then fApplyVolume = fUseVolume end
    
    --Register.
    AudioManager_Register("Voice|" .. sName,  "AsSound", "AsSample", sVoicePath .. sFilename .. ".ogg")
        AudioPackage_SetLocalVolume(fApplyVolume)
    
end

--Main character listing.
fnRegisterVoice("55")
fnRegisterVoice("Administrator", "Administrator", 0.25)
fnRegisterVoice("Aquillia")
fnRegisterVoice("Alraune")
fnRegisterVoice("Bee")
fnRegisterVoice("Blythe")
fnRegisterVoice("Breanne")
fnRegisterVoice("Chris")
fnRegisterVoice("Christine")
fnRegisterVoice("Darkmatter")
fnRegisterVoice("Isabel")
fnRegisterVoice("Florentina")
fnRegisterVoice("Ghost")
fnRegisterVoice("JX101")
fnRegisterVoice("Maram")
fnRegisterVoice("Maisie")
fnRegisterVoice("Mei")
fnRegisterVoice("Nadia")
fnRegisterVoice("Sophie")
fnRegisterVoice("Septima")
fnRegisterVoice("SX399")
fnRegisterVoice("Werecat")

--[Generic NPCs]
--These are often used by multiple minor characters.
for i = 0, 17, 1 do
    AudioManager_Register(string.format("Voice|GenericF%02i", i), "AsSound", "AsSample", sVoicePath .. string.format("Generic/Fem%02i.ogg", i))
    AudioPackage_SetLocalVolume(fLocalVolume)
end
for i = 0, 19, 1 do
    AudioManager_Register(string.format("Voice|GenericM%02i", i), "AsSound", "AsSample", sVoicePath .. string.format("Generic/Mal%02i.ogg", i))
    AudioPackage_SetLocalVolume(fLocalVolume)
end
for i = 0, 3, 1 do
    AudioManager_Register(string.format("Voice|GenericN%02i", i), "AsSound", "AsSample", sVoicePath .. string.format("Generic/Neu%02i.ogg", i))
    AudioPackage_SetLocalVolume(fLocalVolume)
end

--[Baaaa]
--Baaaa
AudioManager_Register("Sheep00", "AsSound", "AsSample", sVoicePath .. "Sheep/Sheep00.ogg")
AudioManager_Register("Sheep01", "AsSound", "AsSample", sVoicePath .. "Sheep/Sheep01.ogg")
AudioManager_Register("Sheep02", "AsSound", "AsSample", sVoicePath .. "Sheep/Sheep02.ogg")
AudioManager_Register("Sheep03", "AsSound", "AsSample", sVoicePath .. "Sheep/Sheep03.ogg")
AudioManager_Register("Sheep04", "AsSound", "AsSample", sVoicePath .. "Sheep/Sheep04.ogg")
AudioManager_Register("Sheep05", "AsSound", "AsSample", sVoicePath .. "Sheep/Sheep05.ogg")
AudioManager_Register("Sheep06", "AsSound", "AsSample", sVoicePath .. "Sheep/Sheep06.ogg")
AudioManager_Register("Sheep07", "AsSound", "AsSample", sVoicePath .. "Sheep/Sheep07.ogg")
