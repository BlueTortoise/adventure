--[ ===================================== World Interaction ===================================== ]
--Sound effects that involve things that happen in the world, in general. Basically, doesn't fit into another category.
-- To make them slightly easier (or harder) to find, they are divided into subfolders.

--[Futuristic]
--Sound effects IN SPAAAAAACE. Mostly used in chapter 5 or other chapters where golems show up with their
-- lazers and pew pew.
local sFuturisticPath = fnResolvePath() .. "Futuristic/"
AudioManager_Register("World|AdminLightsOn",  "AsSound", "AsSample", sFuturisticPath .. "AdminLightsOn.ogg")
AudioManager_Register("World|AdminPowerUp",   "AsSound", "AsSample", sFuturisticPath .. "AdminPowerUp.ogg")
AudioManager_Register("World|AutoDoorFail",   "AsSound", "AsSample", sFuturisticPath .. "AutoDoorFail.ogg")
AudioManager_Register("World|AutoDoorOpen",   "AsSound", "AsSample", sFuturisticPath .. "AutoDoorOpen.ogg")
AudioManager_Register("World|BlastDoorClose", "AsSound", "AsSample", sFuturisticPath .. "BlastDoorClose.ogg")
AudioManager_Register("World|Keycard",        "AsSound", "AsSample", sFuturisticPath .. "Keycard.wav")
AudioManager_Register("World|RemoteDoor",     "AsSound", "AsSample", sFuturisticPath .. "RemoteDoor.wav")
AudioManager_Register("World|SparksA",        "AsSound", "AsSample", sFuturisticPath .. "SparksA.ogg")
AudioManager_Register("World|SparksB",        "AsSound", "AsSample", sFuturisticPath .. "SparksB.ogg")
AudioManager_Register("World|SparksC",        "AsSound", "AsSample", sFuturisticPath .. "SparksC.ogg")
AudioManager_Register("World|SparksD",        "AsSound", "AsSample", sFuturisticPath .. "SparksD.ogg")
AudioManager_Register("World|TerminalOn",     "AsSound", "AsSample", sFuturisticPath .. "TerminalOn.ogg")
AudioManager_Register("World|TerminalOff",    "AsSound", "AsSample", sFuturisticPath .. "TerminalOff.ogg")
AudioManager_Register("World|TramArrive",     "AsSound", "AsSample", sFuturisticPath .. "TramArrive.ogg")
AudioManager_Register("World|TramRunning",    "AsSound", "AsSample", sFuturisticPath .. "TramRunning.ogg")
AudioManager_Register("World|TramStart",      "AsSound", "AsSample", sFuturisticPath .. "TramStart.ogg")
AudioManager_Register("World|TramStop",       "AsSound", "AsSample", sFuturisticPath .. "TramStop.ogg")
AudioManager_Register("World|ValveSqueak",    "AsSound", "AsSample", sFuturisticPath .. "ValveSqueak.ogg")

--[Horror]
--Maximum fear.
local sHorrorPath = fnResolvePath() .. "Horror/"
AudioManager_Register("World|Chaos",         "AsSound", "AsSample", sHorrorPath .. "Chaos.ogg")
AudioManager_Register("World|Heartbeat",     "AsSound", "AsSample", sHorrorPath .. "HeartBeat.ogg")
AudioManager_Register("World|ThingApproach", "AsSound", "AsSample", sHorrorPath .. "ThingApproach.ogg")
AudioManager_Register("World|ThingDistant",  "AsSound", "AsSample", sHorrorPath .. "ThingDistant.ogg")
AudioManager_Register("World|ThingRecede",   "AsSound", "AsSample", sHorrorPath .. "ThingRecede.ogg")
AudioManager_Register("World|ThingSmash",    "AsSound", "AsSample", sHorrorPath .. "ThingSmash.ogg")

--[Impacts]
--Includes explosions, bumping into things, and gunshots.
local sImpactsPath = fnResolvePath() .. "Impacts/"
AudioManager_Register("World|BigExplosion",          "AsSound", "AsSample", sImpactsPath .. "BigExplosion.ogg")
AudioManager_Register("World|BulletImpact0",         "AsSound", "AsSample", sImpactsPath .. "BulletImpact0.ogg")
AudioManager_Register("World|BulletImpact1",         "AsSound", "AsSample", sImpactsPath .. "BulletImpact1.ogg")
AudioManager_Register("World|BulletImpact2",         "AsSound", "AsSample", sImpactsPath .. "BulletImpact2.ogg")
AudioManager_Register("World|BulletImpact3",         "AsSound", "AsSample", sImpactsPath .. "BulletImpact3.ogg")
AudioManager_Register("World|BulletImpactImportant", "AsSound", "AsSample", sImpactsPath .. "BulletImpactImportant.ogg")
AudioManager_Register("World|Explosion",             "AsSound", "AsSample", sImpactsPath .. "FarExplosion.ogg")
AudioManager_Register("World|GlassFall",             "AsSound", "AsSample", sImpactsPath .. "GlassFall.ogg")
AudioManager_Register("World|HardHit",               "AsSound", "AsSample", sImpactsPath .. "HardHit.ogg")
AudioManager_Register("World|LaserDistant",          "AsSound", "AsSample", sImpactsPath .. "Impact_Laser_Distant.ogg")
AudioManager_Register("World|Knock",                 "AsSound", "AsSample", sImpactsPath .. "Knock.ogg")
AudioManager_Register("World|MachineGun",            "AsSound", "AsSample", sImpactsPath .. "MachineGun.ogg")
AudioManager_Register("World|Slap",                  "AsSound", "AsSample", sImpactsPath .. "Slap.ogg")
AudioManager_Register("World|Thump",                 "AsSound", "AsSample", sImpactsPath .. "Thump.wav")
AudioManager_Register("World|TranqShot",             "AsSound", "AsSample", sImpactsPath .. "TranqShot.ogg")
AudioManager_Register("World|VeryBigExplosion",      "AsSound", "AsSample", sImpactsPath .. "VeryBigExplosion.ogg")
AudioManager_Register("World|WhipCrack",             "AsSound", "AsSample", sImpactsPath .. "WhipCrack.ogg")

--Mugging.
AudioManager_Register("World|MugHit",     "AsSound", "AsSample", sImpactsPath .. "Thump.wav")
AudioManager_Register("World|MugSuccess", "AsSound", "AsSample", sImpactsPath .. "Slap.ogg")
AudioManager_Register("World|MugMiss",    "AsSound", "AsSample", sImpactsPath .. "MugMiss.ogg")

--[Movement]
--All footstep sounds, climbing, as well as opening chests and doors.
local sMovementPath = fnResolvePath() .. "Movement/"
AudioManager_Register("World|ClimbLadder",  "AsSound", "AsSample", sMovementPath .. "ClimbLadder.ogg")
AudioManager_Register("World|LimbCladder",  "AsSound", "AsSample", sMovementPath .. "LimbCladder.ogg")
AudioManager_Register("World|OpenChest",    "AsSound", "AsSample", sMovementPath .. "OpenChest.ogg")
AudioManager_Register("World|OpenDoor",     "AsSound", "AsSample", sMovementPath .. "OpenDoor.wav")
AudioManager_Register("World|FootstepL_00", "AsSound", "AsSample", sMovementPath .. "Running_StepL_00.ogg")
AudioManager_Register("World|FootstepR_00", "AsSound", "AsSample", sMovementPath .. "Running_StepR_00.ogg")
AudioManager_Register("World|TakeArmor",    "AsSound", "AsSample", sMovementPath .. "TakeArmor.wav")
AudioManager_Register("World|TakeItem",     "AsSound", "AsSample", sMovementPath .. "TakeItem.wav")
AudioManager_Register("World|TakeWeapon",   "AsSound", "AsSample", sMovementPath .. "TakeWeapon.wav")
AudioManager_Register("World|Jump",         "AsSound", "AsSample", sMovementPath .. "Jump.ogg")
AudioManager_Register("World|Land",         "AsSound", "AsSample", sMovementPath .. "Land.ogg")

--[Nature]
--Pertaining to the natural world. Includes animal noises, water, trees, rocks falling, everyone dying.
local sNaturePath = fnResolvePath() .. "Nature/"
AudioManager_Register("World|BigSplash",   "AsSound", "AsSample", sNaturePath .. "BigSplash.ogg")
AudioManager_Register("World|Chirp",       "AsSound", "AsSample", sNaturePath .. "Chirp.ogg")
AudioManager_Register("World|FreezeOver",  "AsSound", "AsSample", sNaturePath .. "FreezeOver.ogg")
AudioManager_Register("World|Goat0",       "AsSound", "AsSample", sNaturePath .. "Goat0.ogg")
AudioManager_Register("World|Goat1",       "AsSound", "AsSample", sNaturePath .. "Goat1.ogg")
AudioManager_Register("World|Goat2",       "AsSound", "AsSample", sNaturePath .. "Goat2.ogg")
AudioManager_Register("World|Goat3",       "AsSound", "AsSample", sNaturePath .. "Goat3.ogg")
AudioManager_Register("World|RockRumbleA", "AsSound", "AsSample", sNaturePath .. "RockRumbleA.ogg")
AudioManager_Register("World|RockRumbleB", "AsSound", "AsSample", sNaturePath .. "RockRumbleB.ogg")
AudioManager_Register("World|RockRumbleC", "AsSound", "AsSample", sNaturePath .. "RockRumbleC.ogg")

--[Switches]
--Switches, levers, and all that.
local sSwitchesPath = fnResolvePath() .. "Switches/"
AudioManager_Register("World|ButtonClick", "AsSound", "AsSample", sSwitchesPath .. "ButtonClick.ogg")
AudioManager_Register("World|FlipSwitch",  "AsSound", "AsSample", sSwitchesPath .. "FlipSwitch.wav")

--[System]
--Used by the UI or other world events that often are chapter-independent.
local sSystemPath = fnResolvePath() .. "System/"
AudioManager_Register("World|Catalyst",    "AsSound", "AsSample", sSystemPath .. "Catalyst Signal.ogg")
AudioManager_Register("World|Fall",        "AsSound", "AsSample", sSystemPath .. "Fall.wav")
AudioManager_Register("World|Firework0",   "AsSound", "AsSample", sSystemPath .. "Firework0.ogg")
AudioManager_Register("World|Firework1",   "AsSound", "AsSample", sSystemPath .. "Firework1.ogg")
AudioManager_Register("World|Firework2",   "AsSound", "AsSample", sSystemPath .. "Firework1.ogg")
AudioManager_Register("World|Flash",       "AsSound", "AsSample", sSystemPath .. "Flash.ogg")
AudioManager_Register("World|GetCatalyst", "AsSound", "AsSample", sSystemPath .. "GetCatalyst.ogg")
AudioManager_Register("World|Noisemaker",  "AsSound", "AsSample", sSystemPath .. "Noisemaker.ogg")
AudioManager_Register("World|Stairs",      "AsSound", "AsSample", sSystemPath .. "Stairs.ogg")
AudioManager_Register("World|Teleport",    "AsSound", "AsSample", sSystemPath .. "Teleport.wav")
