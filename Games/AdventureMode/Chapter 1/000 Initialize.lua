--[ ================================== Chapter Initialization =================================== ]
--Called when the chapter is initialized. This can be either because it was loaded in the chapter select
-- or because the game was loaded with the chapter already active.
--If loading the game, the initial values created her are overwritten afterwards. These thus serve
-- as initial values. If a variable exists here but does not exist in the save file, the default
-- value is used.
local sBasePath = fnResolvePath()
gbInitializeDebug = false
Debug_PushPrint(gbInitializeDebug, "Beginning Chapter 1 Initialization.\n")

--[ ========================================== Loading ========================================== ]
if(gbLoadedChapter1Assets ~= true) then
    
    --Flag.
    Debug_Print(" Loading assets.\n")
    gbLoadedChapter1Assets = true
    
    --Load Sequence
    fnIssueLoadReset("AdventureModeCH1")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/100 Chapter 1 Sprites.lua")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/101 Chapter 1 Portraits.lua")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/102 Chapter 1 Actors.lua")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/103 Chapter 1 Scenes.lua")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/104 Chapter 1 Overlays.lua")
    SLF_Close()
    fnCompleteLoadSequence()  
    Debug_Print(" Loading assets done.\n")  

end

--[ ==================================== Party Initialization =================================== ]
--Create Mei. Her default job is "Fencer".
Debug_Print(" Creating Mei.\n")
LM_ExecuteScript(gsRoot .. "Combat/Party/Mei/000 Initialize.lua")

--Create a Silver Runestone and equip it to Mei.
Debug_Print(" Creating Mei's equipment.\n")
LM_ExecuteScript(gsItemListing, "Silver Runestone")
LM_ExecuteScript(gsItemListing, "Mei's Work Uniform")
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Equip Item To Slot", "Item A", "Silver Runestone")
    AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Mei's Work Uniform")
DL_PopActiveObject()

--Create Florentina. Her default job is "Merchant".
Debug_Print(" Creating Florentina.\n")
LM_ExecuteScript(gsRoot .. "Combat/Party/Florentina/000 Initialize.lua")

--Mark Mei as the party leader.
gsPartyLeaderName = "Mei"

--Set the party positions. Mei is the leader, all others are empty.
Debug_Print(" Positioning party slots.\n")
AdvCombat_SetProperty("Clear Party")
AdvCombat_SetProperty("Party Slot", 0, "Mei")

--Setup the XP tables.
Debug_Print(" Booting XP table.\n")
LM_ExecuteScript(gsRoot .. "Combat/XP Table/Chapter 1.lua")

--[ ================================== Variable Initialization ================================== ]
--Mark this as the current chapter.
Debug_Print(" Initializing chapter 1 variables.\n")
VM_SetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N", 1.0)

--Create script variables. This is done in its own file.
LM_ExecuteScript(sBasePath .. "001 Variables.lua")

--The farm variables are used in Adina's farm sequence.
LM_ExecuteScript(sBasePath .. "002 Farm Variables.lua")

--Create paths referring to enemies.
LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/999 Alias List.lua")

--[ =================================== Character Appearance ==================================== ]
--Call the costume handlers to build default appearances for the characters.
Debug_Print(" Running costume resolvers.\n")
LM_ExecuteScript(gsCostumeAutoresolve, "Mei_Human")
LM_ExecuteScript(gsCostumeAutoresolve, "Florentina_Merchant")

--[ ====================================== Doctor Bag Zero ====================================== ]
--Mei does not start the chapter with the Doctor Bag. Zero it off here.
Debug_Print(" Setting doctor bag.\n")
VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", 0)
VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", 0)
AdInv_SetProperty("Doctor Bag Charges", 0)
AdInv_SetProperty("Doctor Bag Charges Max", 0)

--[ ======================================= Party Restore ======================================= ]
--Make sure everyone is at full HP. This affects the whole party roster, not just the active party.
Debug_Print(" Restoring party.\n")
AdvCombat_SetProperty("Restore Roster")

--[ ======================================= Path Building ======================================= ]
--Run this script to build cutscenes that can be accessed from the debug menu.
Debug_Print(" Building paths.\n")
LM_ExecuteScript(sBasePath .. "Scenes/Build Scene List.lua")

--Build a set of aliases. These must be used in place of hard paths for in-engine script calls.
LM_ExecuteScript(sBasePath .. "Scenes/Build Alias List.lua")

--Build a list of locations the player can warp to from the debug menu.
LM_ExecuteScript(gsRoot .. "Maps/Build Warp List.lua", "Chapter 1")

--Standard combat paths.
AdvCombat_SetProperty("Standard Retreat Script", gsRoot .. "Chapter 1/Combat Scenes/000 Standard Retreat.lua")
AdvCombat_SetProperty("Standard Defeat Script",  gsRoot .. "Chapter 1/Combat Scenes/001 Standard Defeat.lua")
AdvCombat_SetProperty("Standard Victory Script", gsRoot .. "Chapter 1/Combat Scenes/002 Standard Victory.lua")

--[ ====================================== Music and Sound ====================================== ]
--Default combat music.
Debug_Print(" Setting combat music.\n")
AdvCombat_SetProperty("Default Combat Music", "BattleThemeMei", 93.816)

--[ ====================================== Field Abilities ====================================== ]
if(DL_Exists("Root/Special/Combat/FieldAbilities/WraithForm") == true) then
    DL_Purge("Root/Special/Combat/FieldAbilities/", false)
end
Debug_Print(" Setting field abilities.\n")
DL_AddPath("Root/Special/Combat/FieldAbilities/")
Debug_Print(" Creating deadly jump.\n")
LM_ExecuteScript(gsFieldAbilityListing .. "Deadly Jump.lua", gciFieldAbility_Create)
Debug_Print(" Creating wraithform.\n")
LM_ExecuteScript(gsFieldAbilityListing .. "Wraithform.lua", gciFieldAbility_Create)
Debug_Print(" Creating scout sight.\n")
LM_ExecuteScript(gsFieldAbilityListing .. "Scout Sight.lua", gciFieldAbility_Create)
Debug_Print(" Creating pick lock.\n")
LM_ExecuteScript(gsFieldAbilityListing .. "Pick Lock.lua", gciFieldAbility_Create)
Debug_Print(" Putting field abilities in slots.\n")
AdvCombat_SetProperty("Set Field Ability", 0, "Root/Special/Combat/FieldAbilities/DeadlyJump")
AdvCombat_SetProperty("Set Field Ability", 1, "Root/Special/Combat/FieldAbilities/WraithForm")
AdvCombat_SetProperty("Set Field Ability", 2, "Root/Special/Combat/FieldAbilities/ScoutSight")
AdvCombat_SetProperty("Set Field Ability", 3, "Root/Special/Combat/FieldAbilities/PickLock")
Debug_Print(" Done with field abilities.\n")

--[ ======================================== Intro Bypass ======================================= ]
--The player can optionally bypass the intro. If that happens, this code executes.
if(gbBypassIntro == true) then
    
    --Debug.
    Debug_Print(" Setting intro bypass values.\n")

    --Doctor Bag starts at its normal charges. Set flags to upload these to the inventory.
    gbAutoSetDoctorBagProperties = true
    gbAutoSetDoctorBagCurrentValues = true
    VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", 100)
    VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", 100)
    AdInv_SetProperty("Doctor Bag Charges", 100)
    AdInv_SetProperty("Doctor Bag Charges Max", 100)
    LM_ExecuteScript(gsComputeDoctorBagTotalPath)
    
    --Give Mei the Rusty Katana.
    LM_ExecuteScript(gsItemListing, "Rusty Katana")
    LM_ExecuteScript(gsItemListing, "Rusty Jian")
    LM_ExecuteScript(gsItemListing, "Steel Katana")
    AdvCombat_SetProperty("Push Party Member", "Mei")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Rusty Katana")
    DL_PopActiveObject()
    
    --Debug: Put Florentina in the party
    --AdvCombat_SetProperty("Party Slot", 1, "Florentina")
    
    --Debug: Give Florentina her weaponry.
    --[=[LM_ExecuteScript(gsItemListing, "Hunting Knife")
    LM_ExecuteScript(gsItemListing, "Flowery Tunic")
    LM_ExecuteScript(gsItemListing, "Florentina's Pipe")
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Hunting Knife")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Flowery Tunic")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Accessory A", "Florentina's Pipe")
    DL_PopActiveObject()]=]
    
    --Change Mei's form.
    --LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Rubber.lua")
    
end
Debug_PopPrint("Finished chapter 1 initialization.\n")
