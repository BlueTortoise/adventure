--[ ==================================== Build Cutscene List ==================================== ]
--Creates a list of cutscenes that will appear on the debug menu. These can be both existing cutscenes
-- and debug cutscenes that change character properties and add/remove equipment.
local saSceneList = {}
local fnAddScene = function(sSceneName)
    local iSlot = #saSceneList + 1
    saSceneList[iSlot] = sSceneName
end

--[ ====================================== Quickset Scenes ====================================== ]
--These are used to quickly add characters or provide equipment.

--Adds Florentina to the party, or removes her.
fnAddScene("A Quickadd Florentina")
fnAddScene("A Quickremove Florentina")

--Sets Adina's Mind Control plot flags.
fnAddScene("A Quickset AdinaControl")

--Gives the party equipment to fight bosses.
fnAddScene("A QuicklevelCh1BossSet")

--Gives the party the best equipment in the chapter.
fnAddScene("A QuicklevelCh1Max")

--Flags the party as having rescued Claudia.
fnAddScene("A Quickset SavedClaudia")

--Flags the party as having rescued Cassandra.
fnAddScene("A Quickset SavedCassandra")

--[ ==================================== Transform Cutscenes ==================================== ]
--Used to quickly TF Mei to a form, setting all the requisite flags.
fnAddScene("Transform_MeiToAlraune")
fnAddScene("Transform_MeiToBee")
fnAddScene("Transform_MeiToGhost")
fnAddScene("Transform_MeiToHuman")
fnAddScene("Transform_MeiToSlime")
fnAddScene("Transform_MeiToWerecat")

--[ ====================================== Defeat Cutscenes ===================================== ]
--Same as being immediately defeated by the enemy in question.
fnAddScene("Defeat_BackToSave")
fnAddScene("Defeat_Alraune")
fnAddScene("Defeat_Bee")
fnAddScene("Defeat_BeeAsAlraune")
fnAddScene("Defeat_Cultists")
fnAddScene("Defeat_Ghost")
fnAddScene("Defeat_Slime")
fnAddScene("Defeat_Werecat")
fnAddScene("Defeat_Zombee")

--[ ===================================== Normal Cutscenes ====================================== ]
fnAddScene("CutsceneSample")
fnAddScene("Basement_CultistMeeting")


--[ ======================================== Upload List ======================================== ]
--Upload the list to the debug menu.
local i = 1
ADebug_SetProperty("Cutscenes Total", #saSceneList)
while(saSceneList[i] ~= nil) do
	ADebug_SetProperty("Cutscene Path", i-1, saSceneList[i])
	i = i + 1
end