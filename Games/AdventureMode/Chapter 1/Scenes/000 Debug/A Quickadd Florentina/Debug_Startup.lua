--[Special]
--This instantly adds Florentina to the party, along with attendant variables. Does nothing if she's already in the party.
for i = 1, gsFollowersTotal, 1 do
	if(gsaFollowerNames[i] == "Florentina") then
		return
	end
end
	
--Create if she doesn't exist.
if(EM_Exists("Florentina") == false) then
	fnSpecialCharacter("Florentina", "Alraune", -100, -100, gci_Face_South, false, nil)
end

--Lua globals.
gsFollowersTotal = 1
gsaFollowerNames = {"Florentina"}
giaFollowerIDs = {0}

--Get Florentina's uniqueID. 
EM_PushEntity("Florentina")
	local iFlorentinaID = RE_GetID()
DL_PopActiveObject()

--Store it and tell her to follow.
giaFollowerIDs = {iFlorentinaID}
AL_SetProperty("Follow Actor ID", iFlorentinaID)

--Place Florentina in the combat lineup.
AdvCombat_SetProperty("Party Slot", 1, "Florentina")

--Florentina's equipment
if(gbHasFlorentinasEquipment == false) then
	gbHasFlorentinasEquipment = true
	LM_ExecuteScript(gsItemListing, "Hunting Knife")
	LM_ExecuteScript(gsItemListing, "Flowery Tunic")
	LM_ExecuteScript(gsItemListing, "Florentina's Pipe")
	AdvCombat_SetProperty("Push Party Member", "Florentina")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Hunting Knife")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Flowery Tunic")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Accessory A", "Florentina's Pipe")
	DL_PopActiveObject()
end

--Party Folding
AL_SetProperty("Fold Party")

--Unlock dialogue topics.
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Claudia", 1)
WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
WD_SetProperty("Unlock Topic", "Pandemonium", 1)

--Script variables that normally must be set to meet Florentina. It is assumed Florentina met Mei as a human.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S", "Human")