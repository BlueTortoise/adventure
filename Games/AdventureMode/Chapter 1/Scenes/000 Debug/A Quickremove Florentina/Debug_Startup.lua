--[Special]
--This instantly removes Florentina from the party. Used to test cutscenes, it never triggers normally.
-- This should be called before a map relocation.
fnRemovePartyMember("Florentina", true)