--[Special]
--Opens a debug vendor that will sell you all the gems in the game for free.

--For now, just give one of each gem.
LM_ExecuteScript(gsItemListing, "Glintsteel Gem")
LM_ExecuteScript(gsItemListing, "Yemite Gem")
LM_ExecuteScript(gsItemListing, "Ardrite Gem")
LM_ExecuteScript(gsItemListing, "Rubose Gem")
LM_ExecuteScript(gsItemListing, "Blurleen Gem")
LM_ExecuteScript(gsItemListing, "Qederite Gem")