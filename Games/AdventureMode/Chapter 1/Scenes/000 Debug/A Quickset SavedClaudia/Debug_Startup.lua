--[Special]
--Quickly sets it such that Mei has rescued Claudia.
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetClaudiaWithoutHacksaw", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N", 1.0)