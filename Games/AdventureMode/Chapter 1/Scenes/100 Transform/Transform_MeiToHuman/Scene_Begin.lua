--[Transform Mei to Human]
--Used at save points when Mei transforms from something else back to a human. May trigger cutscenes.

--Special: Block transformations.
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

--[Variables]
--Store which form Mei started the scene in.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--[Execute Transformation]
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Cutscene Execution]
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Well, isn't that a neat trick.[SOFTBLOCK] How'd you do that?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I just focus on the runestone, and myself.[SOFTBLOCK] Then, poof.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Pretty impressive.[SOFTBLOCK] If we could find a way to sell that...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Don't get all defensive now, it was just an idea.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I mean...[SOFTBLOCK] it won't work for anyone but me.[SOFTBLOCK] Don't ask me how I know that, I just do.[SOFTBLOCK] It's like instinct.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Until I see proof otherwise, I'll keep it as an open option.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] This runestone...[SOFTBLOCK][E|Neutral] I get the feeling it's more important than a get-rich-quick scheme.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] More important, sure.[SOFTBLOCK] Exclusive with, not as much.") ]])
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--If Mei started as an Alraune...
if(sStartingForm == "Alraune") then
	
	--Scene Variables.
	local iLittleOnes = VM_GetVar("Root/Variables/Global/Mei/Cutscene|Human|iLittleOnes", "N")
	local iSoLonely  = VM_GetVar("Root/Variables/Global/Mei/Cutscene|Human|iSoLonely",  "N")
	
	--Mei talks about the plants who can't hear her anymore.
	if(iLittleOnes == 0.0) then
		
		--Flip the variable.
		VM_SetVar("Root/Variables/Global/Mei/Cutscene|Human|iLittleOnes", "N", 1.0)
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		if(bIsFlorentinaPresent == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		end
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: L-little ones? Can - [SOFTBLOCK]oh. I can't hear you any more...[BLOCK][CLEAR]") ]])
		
		--Florentina is not present.
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I wonder if you can hear me...") ]])
		
		--If Florentina is present, she comments.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I wonder if you can hear me...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] They can.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] How lovely![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused]...[SOFTBLOCK] Amatuer...") ]])
		end
		fnCutsceneBlocker()
	
	--Mei feels lonely since she can't hear the plants.
	elseif(iSoLonely == 0.0) then
		
		--Flip the variable.
		VM_SetVar("Root/Variables/Global/Mei/Cutscene|Human|iSoLonely", "N", 1.0)
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Blush") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] It's so quiet all of a sudden.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I guess you never realize how lonely you are until you're in a crowd who won't talk to you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's okay little ones.[SOFTBLOCK] I'll come back as soon as I can.") ]])
		fnCutsceneBlocker()
	end
	
--If Mei started as a Bee...
elseif(sStartingForm == "Bee") then
	
	--Scene Variables.
	local iQuietTime = VM_GetVar("Root/Variables/Global/Mei/Cutscene|Human|iQuietTime", "N")
	
	--Mei talks about the plants who can't hear her anymore.
	if(iQuietTime == 0.0) then
		
		--Flip the variable.
		VM_SetVar("Root/Variables/Global/Mei/Cutscene|Human|iQuietTime", "N", 1.0)
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		if(bIsFlorentinaPresent == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		end
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Sisters?[SOFTBLOCK][E|Sad] Oh, right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
		
		--Florentina is not present.
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I miss my stinger...") ]])
		
		--If Florentina is present, she comments.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I miss my stinger...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Weirdo.") ]])
		end
		fnCutsceneBlocker()
	end

--If Mei started as an Slime...
elseif(sStartingForm == "Slime") then
	
	--Scene Variables.
	local iNotSoSquish = VM_GetVar("Root/Variables/Global/Mei/Cutscene|Human|iNotSoSquish", "N")
	
	--Mei talks about the plants who can't hear her anymore.
	if(iNotSoSquish == 0.0) then
		
		--Flip the variable.
		VM_SetVar("Root/Variables/Global/Mei/Cutscene|Human|iNotSoSquish", "N", 1.0)
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		if(bIsFlorentinaPresent == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		end
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I suppose it was unavoidable.[SOFTBLOCK] But...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Not so squishy anymore...[BLOCK][CLEAR]") ]])
		
		--Florentina is not present.
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I guess I can get used to this again.") ]])
		
		--If Florentina is present, she comments.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Could you please not fantasize about masturbating right in front of me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Oh hush.[SOFTBLOCK] What would you know about humility?[SOFTBLOCK] You don't even wear a bra![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Now that's more like it.[SOFTBLOCK] Let those fangs out.") ]])
		end
		fnCutsceneBlocker()
	end
end