--[Transform Mei to Werecat]
--Used at save points when Mei transforms from something else to a Werecat.

--Special: Block transformations.
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

--[Variables]
--Store which form Mei started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--[Execute Transformation]
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Cutscene Execution]
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Well, well, well.[SOFTBLOCK] I saw your rune glowing.[SOFTBLOCK] I didn't know it could do this![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Purr...[SOFTBLOCK] it's quite a rush.[SOFTBLOCK] I'm sorry, but I don't know how it works.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] How did you know what I was going to ask?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's written all over your face.[SOFTBLOCK] You want to find some way to turn this process into profit.[SOFTBLOCK] Purrr...[SOFTBLOCK] I like the look...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Heh.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Like me, you are a hunter.[SOFTBLOCK] A predator.[SOFTBLOCK] Except, you hunt for advantage.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I happen to think I'm good at the other kind of hunting, too.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] You suit my pride just fine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] So about the runestone...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Mmm...[SOFTBLOCK] I'm sorry, but I don't think it will work on anyone but me.[SOFTBLOCK] I know this to be a fact, but I cannot say how I know it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Just as I cannot say how I came by the runestone, come to think of it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] We can experiment later, assuming there's no way back to Earth.[SOFTBLOCK] Maybe we can tease some secrets out of that thing.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] The hunt for the truth...[SOFTBLOCK] I suppose that, too, is a hunt.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Then let us resume our hunt for a way back to Earth.") ]])

	--Common.
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Scenes that are independent of form.
if(true) then
	
end