--[Defeat By Bee as Alraune]
--Cutscene proper. Uses the dialogue's SceneHandler to get the TF sequence.

--[Repeat Check]
--If we've seen this scene, it doesn't play again.
local iHasSeenAlrauneDefeatByBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenAlrauneDefeatByBeeScene", "N")
if(iHasSeenAlrauneDefeatByBeeScene == 1) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

--[Remove Florentina]
--If Florentina is in the party, remove her. She rejoins shortly.
fnRemovePartyMember("Florentina", true)

--[Map Check]
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "AlrauneAndBeeScene") then
	AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
	AL_BeginTransitionTo("AlrauneAndBeeScene", LM_GetCallStack(0))
	return
end

--Set the flag.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenAlrauneDefeatByBeeScene", "N", 1.0)

--If the Florentina entity happens to be on the field, move her off.
if(EM_Exists("Florentina") == true) then
	EM_PushEntity("Florentina")
		TA_SetProperty("Position", -10, -10)
	DL_PopActiveObject()
end

--[Combat]
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

--[Music]
AL_SetProperty("Music", "Null")

--[Cutscene Execution]
--Fade from black to nothing over 120 ticks. This fade is under the UI.
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Reposition Mei, make her use the wounded image.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 16, 14)
	TA_SetProperty("Set Special Frame", "Wounded")
DL_PopActiveObject()
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1.0)
DL_PopActiveObject()

--Spawn a bee.
TA_Create("Scene_Bee")
	TA_SetProperty("Facing", gci_Face_East)
	TA_SetProperty("Position", 16, 18)
	fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
	TA_SetProperty("Auto Animates", true)
    TA_SetProperty("Y Oscillates", true)
DL_PopActiveObject()

--Scene setup.
fnPartyStopMovement()

--Wait a bit for the fade.
fnCutsceneWait(90)
fnCutsceneBlocker()

--[Dialogue]
--Mei gets up. Wait a bit before speaking.
fnCutsceneWait(60)
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()
fnCutsceneBlocker()

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Oof.[SOFTBLOCK] Bee stingers sure pack a punch...") ]])

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--[Movement]
--Bee moves up to Mei.
fnCutsceneWait(10)
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Bee North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Scene_Bee")
	ActorEvent_SetProperty("Face", 0, -1.0)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
Cutscene_CreateEvent("Move Bee North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Scene_Bee")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--[Dialogue]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Oh jeez.[SOFTBLOCK] Leave me alone![SOFTBLOCK] Not again...") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()

--[Scene]
--Wait a bit for dialogue to finish fading.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Scene part.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The bee sidled up to Mei and placed a hand on her shoulder.[SOFTBLOCK] Mei recoiled, expecting another attack, but was too weak to pull away.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Wordlessly, the bee drew closer and took Mei into her arms.[SOFTBLOCK] The gentle buzzing of her wings ceased as she pulled the two into an embrace.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei began to protest,[SOFTBLOCK] to call out,[SOFTBLOCK] but the bee placed a single finger against her lips to hush her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Surprised at the gesture, Mei looked into the bee girl's hungry eyes, and then understood that she did not desire to fight.") ]])
fnCutsceneBlocker()

--[Dialogue]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] You want...[SOFTBLOCK] my nectar?") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()

--[Scene]
--Wait a bit for dialogue to finish fading.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Scene part.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The bee nodded and licked her lips expectantly.") ]])
fnCutsceneBlocker()

--[Dialogue]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ...[SOFTBLOCK] Continue...") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()

--[Scene]
--Wait a bit for dialogue to finish fading.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade under the dialogue to black.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--Scene part.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "With a light push, Mei found herself sprawled on her back.[SOFTBLOCK] The bee quickly straddled her, glowing with wordless excitement.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She massaged Mei's chest with practiced delicacy, cupping her breasts beneath the leafy foliage that covered her and drawing it back to expose the stiffening nipples beneath.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She teased the Alraune's sensitive nipples with her fingers, glancing about them with a delicate touch that caused Mei to become envious of the flowers she remembered on Earth.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The thought was fleeting, and soon passed as a soft moan fell from her lips and her back arched in reply.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The bee girl's sun-kissed hair fell about her face as she flicked her tongue about Mei's breast and took her flower's waiting nipple into her mouth.[SOFTBLOCK] Her lips followed close behind and closed upon the teat.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei reached out, groping blindly as her eyes fluttered shut, seeking something, anything, to fill her hands. Taking the bee's head into her hands, the touch of the silken strands of her hair between her fingers served only to heighten her arousal.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Even as Mei's fingers combed through the bee's nest of hair, the bee began to suck upon her nipple, her lips and breath pulling and releasing to the beat of an unheard song as her flickering tongue danced in time.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The fount of her breast soon ran dry under the expert care of the bee, and a quiet protest began to build on Mei's lips.[SOFTBLOCK] It fell silent as the dance of the bee's lips and tongue resumed upon her other breast.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the second breast drained, the bee began to pull back, and a mixture of anxiety and despair filled Mei's mind.[SOFTBLOCK] 'Is there no more?' asked one.[SOFTBLOCK] 'She's going to leave.' said the other.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Recognizing the look that passed upon Mei's face, the bee smiled at her and pressed her hands against Mei's thighs with a gentle firmness.[SOFTBLOCK] A welcoming gasp escaped Mei's chest as she eagerly complied.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The bee nuzzled herself up against the expetant lips of Mei's sex, already grown damp with the first rivulets of her sticky nectar.[SOFTBLOCK] Once more her tongue began to dance to the unheard rhythm upon the petals of her flower.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Each flick of her tongue sent a wave of carnal pleasure along Mei's spine.[SOFTBLOCK] Her body flushed as the energy coursed through her limbs, curling her toes and digging her fingers deep into the soft ground that had become their bed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A yearning moan surged from Mei's chest as the bee's tongue plunged into the depths of her flower, grasping for the final few drops of nectar that hid therein.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The surge of energy coursed through Mei's body once more, and every cell of her being called out to her partner.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Finished with her harvest, the bee girl lay herself beside her flower, tracing her fingers from thigh to stomach to breast to lips in a gentle caress as the pair lay in the warmth of the afternoon sun.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "At length Mei's mind cleared, and as her pulse slowed and her breath steadied, she looked at the bee and saw once more the kindly smile that had eased her fears before.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "But within the bee's eyes passed a darker countenance, and Mei was at once aware of its meaning.") ]])
fnCutsceneBlocker()

--[Dialogue]
--Wait a bit.
fnCutsceneWait(85)
fnCutsceneBlocker()

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Please...[SOFTBLOCK][SOFTBLOCK] Don't go.") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()

--[Scene]
--Wait a bit for dialogue to finish fading.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Scene part.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The bee traced her fingers along Mei's body once more before leaning in to her lips and placing a single sticky kiss upon them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She stood, even as her flower's pleading hands reached out for her, turning to answer the unheard song of her hive that called to her, calling her home to yield up the wealth of nectar that filled her swollen abdomen.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Testing her wings, pulling herself into a hover, she hastened away to her hive in the north.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Now alone, Mei stood and prepared to return to her journey.") ]])
fnCutsceneBlocker()

--[Movement]
--Remove special status.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneBlocker()

--[Transition to Save Point]
--Do that.
fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_BeeAsAlraune/Scene_PostTransition.lua") ]])
fnCutsceneBlocker()
