--[Catfight Scene]
--You are not fit to join my pride!

--[Flag]
--Set this.
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iSpawnNadiaEvermoonW", "N", 1.0)

--Variables.
local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

--[Movement]
--Move Mei and the werecats up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (39.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (50.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (52.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Werecats look back.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (The hunt...[SOFTBLOCK] Nadia...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: The kinfang hesitates.[SOFTBLOCK] It will be an easy kill.[SOFTBLOCK] Perhaps we should let the plant girl run first?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Does the new kinfang not want the kill?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] There will be no hunt here tonight![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Kinfang must hunt, must kill, or kinfang is weak![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Not weak, strong![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Hss![SOFTBLOCK] Weak![SOFTBLOCK] The new one is weak![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Wrong again!") ]])
fnCutsceneBlocker()

fnCutsceneWait(15)
fnCutsceneBlocker()

--[Movement]
--Left Werecat moves onto Mei. Get thrown into the wall.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (51.00 * gciSizePerTile), (39.25 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Mei rotates in place to emulate a throw.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  1, 1)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  1, 0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  1, -1)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Sound effect plays on this frame.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  -1, -1)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Werecat switches to wounded frame and flies into the wall.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (50.25 * gciSizePerTile), (36.00 * gciSizePerTile), 2.5)
DL_PopActiveObject()
fnCutsceneBlocker()

--Ricochet.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (37.50 * gciSizePerTile), 0.25)
DL_PopActiveObject()
fnCutsceneBlocker()

--Nadia gets up and walks outside.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Teleport To", (48.25 * gciSizePerTile), (30.00 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina is present, she wakes up too.
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (53.25 * gciSizePerTile), (26.00 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(35)
fnCutsceneBlocker()

--Nadia starts moving outside.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (48.25 * gciSizePerTile), (32.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
if(iHasFlorentina == 1.0) then
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorN") ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (53.25 * gciSizePerTile), (31.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (31.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Facing. The werecats look north as the door opens.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorW") ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Keep moving Nadia outside.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (48.25 * gciSizePerTile), (34.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (34.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (35.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (34.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
end

fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorS") ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Nadia walks outside.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (37.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (50.75 * gciSizePerTile), (37.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (37.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (51.75 * gciSizePerTile), (37.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face",  0, 1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face",  0, 1)
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(35)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Nadia", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
if(iHasFlorentina == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Surprise") ]])
end

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Hss![SOFTBLOCK] The prey awakens![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Fool kinfang![SOFTBLOCK] She is not prey![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: You would defy the moonlight!?") ]])
fnCutsceneBlocker()

fnCutsceneWait(15)
fnCutsceneBlocker()

--[Movement]
--Mei attacks the other werecat.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei moves onto the werecat.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (52.25 * gciSizePerTile), (38.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(10)
fnCutsceneBlocker()

--Sound effect, Werecat goes down.
fnCutsceneWait(15)
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(20)
fnCutsceneBlocker()

--Mei throws the werecat.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  1, 0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (56.25 * gciSizePerTile), (38.50 * gciSizePerTile), 2.5)
DL_PopActiveObject()
fnCutsceneBlocker()

--Ricochet.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (55.25 * gciSizePerTile), (39.00 * gciSizePerTile), 0.25)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Nadia", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
if(iHasFlorentina == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Happy") ]])
end

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] You grow fat from hunting weak prey![SOFTBLOCK] There are more deserving hunts to be had![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] The idle rich, the wicked, the cruel![SOFTBLOCK] We do not attack a sleeping one, and we do not hunt with more fangs than preys![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] You are not fit to join my pride![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Hssss!") ]])
fnCutsceneBlocker()

fnCutsceneWait(35)
fnCutsceneBlocker()

--[Movement]
--WerecatA stands up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneWait(35)
fnCutsceneBlocker()

--WerecatB stands up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneWait(35)
fnCutsceneBlocker()

--Werecats run off.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (52.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (53.25 * gciSizePerTile), (42.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (53.25 * gciSizePerTile), (52.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  -1, -1)
DL_PopActiveObject()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
if(iHasFlorentina == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
else
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Happy") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
end

--Dialogue if Florentina is not present.
if(iHasFlorentina == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Mei![SOFTBLOCK] What the hay is going on here?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Apologies, Nadia.[SOFTBLOCK] My kinfangs thought you were prey.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: You know, if you had told me you were cursed I could have tried to cure you.[SOFTBLOCK] Now it's too late.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And leave the moonlight's grace?[SOFTBLOCK] No thank you.[SOFTBLOCK] I prefer this.[BLOCK][CLEAR]") ]])

	--Variables. Determine if Nadia knows Mei is a shapeshifter.
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")

	--If Nadia has seen the third scene, she knows Mei is a shapeshifter.
	if(iHasSeenTrannadarThirdScene == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Besides, I can transform if I so choose.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Oh yeah![SOFTBLOCK] Cool![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: I almost forgot about that.[SOFTBLOCK] How does that work?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] My runestone, I think.[SOFTBLOCK] All I need is to concentrate...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: I'm not much of a magic expert unless you need help with your garden.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Oh, I know![SOFTBLOCK] I know where a grove of catnip is![SOFTBLOCK] Want some?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Don't patronize me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: It's a joke![SOFTBLOCK] A joke![SOFTBLOCK] Ack![BLOCK][CLEAR]") ]])

	--Nadia does not know Mei is a shapeshifter.
	else
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I would not have wanted to stop you by force.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Well if you're happy, that's cool.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] and...[SOFTBLOCK] my instinct is that I may return to human form at any time.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This runestone...[SOFTBLOCK] yes...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Huh?[SOFTBLOCK] Let me see that![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Well, I can feel the magic but it's not Alraune magic, so I don't know how it works.[SOFTBLOCK] You say it lets you transform?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Purr...[SOFTBLOCK] a demonstration is in order?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Purr-haps![SOFTBLOCK] Ha ha![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[SOFTBLOCK] Just for that I'm not showing you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Aww, c'mon![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nope.[SOFTBLOCK] Later.[SOFTBLOCK] Heh.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Aw, jeez![BLOCK][CLEAR]") ]])
	end

	--Resumption.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Anyway, I'm not tired anymore.[SOFTBLOCK] The sun should be up soon.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Want to move out?[SOFTBLOCK] I just need to grab my bag.[BLOCK][CLEAR]") ]])

	--If Mei had sex with Nadia:
	local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
	if(iMeiHasDoneNadia == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Perhaps we ought to linger -[SOFTBLOCK] in your bedroom.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ..![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Ooh, the look on your face...[SOFTBLOCK] you want - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Sh-[SOFTBLOCK]shush![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I see how it is.[SOFTBLOCK] Later, then.[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Then get your things and let's go...[SOFTBLOCK][SOFTBLOCK] lover...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ...[SOFTBLOCK][SOFTBLOCK] lover...") ]])

	--Normal case:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Let's get going then.[SOFTBLOCK] Probably best to scram before my kinfangs come back.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I hope they'll learn a lesson.[SOFTBLOCK] If not, I can teach them one again![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Okay![SOFTBLOCK] Let's roll!") ]])

	end
	fnCutsceneBlocker()

--Dialogue is Florentina is present:
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Mei![SOFTBLOCK] What the hay is going on here?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] The best beatdown I've seen in a while![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Why didn't you do that when they attacked us yesterday?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Maybe I wasn't motivated enough.[SOFTBLOCK][EMOTION|Mei|Smirk] But now?[SOFTBLOCK] I love this![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Well, if you had told me you were cursed, I could have tried to cure you.[SOFTBLOCK] It's too late now![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Too late?[SOFTBLOCK] Purr...[SOFTBLOCK] You'd have kept me from the moon's grace?[SOFTBLOCK] No thank you, keep your cures.[BLOCK][CLEAR]") ]])

	--Variables. Determine if Nadia knows Mei is a shapeshifter.
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local iFlorentinaKnowsAboutRune   = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")

	--If Nadia has seen the third scene, she knows Mei is a shapeshifter.
	if(iHasSeenTrannadarThirdScene == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Besides, I can transform if I so choose.[SOFTBLOCK] Remember?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Oh yeah![SOFTBLOCK] Cool![BLOCK][CLEAR]") ]])
		
		--Florentina doesn't know about Mei's runestone:
		if(iFlorentinaKnowsAboutRune == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Excuse me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Purr?[SOFTBLOCK] Oh, yes, I've not informed you, have I?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I don't understand it either, but I could show you.[SOFTBLOCK] Later, at a rest stop.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It might be dangerous here, if there are any kinfangs nearby.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You don't have the face of a liar.[SOFTBLOCK] Then again...[SOFTBLOCK] cats...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: I know you'll find a way to purr-[SOFTBLOCK]suede her to show you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] I WILL HIT YOU.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] All right, all right![SOFTBLOCK] Knock it off, you two![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] We should probably get going.[SOFTBLOCK] The sun will be up soon, and I'm not tired anymore.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Okay![SOFTBLOCK] Follow me!") ]])
		
		--Florentina knows about the runestone:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] I hope you elect to stay as a werecat for a while.[SOFTBLOCK] Maybe we can throw some more cats into trees?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I wish I didn't have to...[EMOTION|Mei|Neutral] but better to hurt them than suffer weak pridemates.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: You're really getting into it, aren't you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] This is a big improvement![SOFTBLOCK] More, please![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] You have a one-track mind![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] We may as well get going.[SOFTBLOCK] The sun should be up soon, and I'm not tired anymore.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Okay![SOFTBLOCK] Follow me!") ]])
		end

	--Nadia does not know Mei is a shapeshifter.
	else
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Besides, my runestone lets me transform when I so choose.[BLOCK][CLEAR]") ]])
		
		--Florentina doesn't know about Mei's runestone:
		if(iFlorentinaKnowsAboutRune == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] It does?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: It does?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Hey, that's my line![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] But, you don't look like you're lying.[SOFTBLOCK] What gives?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll show you when we're in a safe spot.[SOFTBLOCK] Not here, though, there may be kinfangs nearby.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nadia, please lead the way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Okay![SOFTBLOCK] Follow me!") ]])
		
		--Florentina knows about the runestone:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] I hope you elect to stay as a werecat for a while.[SOFTBLOCK] Maybe we can throw some more cats into trees?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Wait wait, what?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, I didn't believe it either.[SOFTBLOCK] But it's true.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'd show you, but it leaves me vulnerable and there may be more kinfangs nearby.[SOFTBLOCK] Perhaps later.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Wow, that's so cool, Mei![SOFTBLOCK] It's meow-nificent![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[SOFTBLOCK] Do you want to claw her eyes out or should I do it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Ugh, the cat puns are going to kill me.[SOFTBLOCK][EMOTION|Mei|Neutral] Could you just take us back to the trading post now?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Okay![SOFTBLOCK] Follow me!") ]])
		end
	end

end

--Remove the werecats as followers.
fnCutsceneInstruction([[ AL_SetProperty("Unfollow Actor Name", "WerecatA") ]])
fnCutsceneInstruction([[ AL_SetProperty("Unfollow Actor Name", "WerecatB") ]])

--Scene relive: Stop here.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
if(iIsRelivingScene == 1.0) then
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()

--Normal case:
else

	--Wait a bit.
	fnCutsceneWait(35)
	fnCutsceneBlocker()

	--Warp to Evermoon West.
fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Werecat/Scene_PostTransitionEvermoon.lua") ]])
	fnCutsceneBlocker()
end