--[Kinfang Scene]
--Mei joins the werecats.

--[System]
--Close the doors in the cabin. They're needed later.
AL_SetProperty("Close Door", "DoorS")
AL_SetProperty("Close Door", "DoorW")
AL_SetProperty("Close Door", "DoorN")

--[Dialogue]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Werecat] (Kiiiinfaaaaang...[SOFTBLOCK] coooome to uuuusssss)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (That sound...[SOFTBLOCK] it...[SOFTBLOCK] they've been making it for hours,[SOFTBLOCK] I could hear them,[SOFTBLOCK] but I couldn't understand it until now...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I need to go to them...)") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--[Movement]
--Walk.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (34.25 * gciSizePerTile), (57.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (56.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Dialogue]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Werecat] (Yesss, kiiinfaaanggg...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Kinfang coommesss...)") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Movement]
--Black the scene out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, 0, 0, 0, 1, true) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--Reposition Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (17.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Unblack the scene.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
fnCutsceneWait(90)
fnCutsceneBlocker()

--[Dialogue]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: The kinfang heeds the call of the moon.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I am incomplete.[SOFTBLOCK] Complete me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Take your place...") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Movement]
--Mei moves to the center of the grove.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (17.25 * gciSizePerTile), (14.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Crouch.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Scene]
--Black things out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, 0, 0, 0, 1, true) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--Scene.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her pulse throbbed under the surging adrenaline, and Mei fell to her knees in the center of the grove as the werecats looked on.[SOFTBLOCK] They watched in patient fascination, groping at their exposed sexes as the clouds above began to part.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The wounds she bore from battle began to tingle as the moonlight fell upon her, and the lingering pain faded.[SOFTBLOCK] Far above them, the clouds drifted further apart, and the first rays of the revealed moon glinted from the werecats' fangs.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The sounds of the night began to crescendo in Mei's ears as the light of the moon grew.[SOFTBLOCK] Sounds she had never heard before, even on the quietest of nights.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The night was a concert of creatures large and small.[SOFTBLOCK] Creatures whose only purpose was to serve as game for the werecats.[SOFTBLOCK] The excitement of the impending hunt compounded the anticipation of the dawning moon's light.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "At last the clouds broke and the light of the moon fell fully upon Mei's torn flesh.[SOFTBLOCK] A cry of desire escaped her lips as the light drew forth the curse that lingered within her blood.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The light fell upon her, illuminating her, and soon the gathered werecats descended upon her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Their claws dug into her flesh.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Their teeth pierced her muscle.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "But no pain was felt by the one cursed to receive them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei's head fell back and her eyes gazed upon the moon.[SOFTBLOCK] However terrible the attacks delivered upon her, the wounds only amplified her arousal as each claw and bite grew her curse.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Fur began to pierce her torn flesh as her limbs reformed, her body becoming bestial.[SOFTBLOCK] Each twist seemed to grow the touch of the moon's caressing light.[SOFTBLOCK] A caress that began to reach deep within her...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Fangs filled her mouth as the fresh scent of the night struck her senses.[SOFTBLOCK] Claws sprung forth, and darkness grew bright as her eyes transformed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The gathered werecats turned from her, falling upon each other to continue their orgy of cursing.[SOFTBLOCK] Mei could not resist the moonlight's call to join them in their carnal pleasure.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Now kinfang like those around her, she sampled their flesh and fur with her tongue.[SOFTBLOCK] The pheremones and oils of their groomed bodies pulled at her mind and heightened her own arousal.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Soon, several of the werecats turned back to her, pulling her to the ground and set upon the glistening dampness that began to form around her sex.[SOFTBLOCK] She arched her back and encouraged them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "There under the watchful moonlight, the bestial orgy raged on as the werecats set upon each other, clawing and biting and licking.[SOFTBLOCK] The presense of time itself was lost as Mei found herself drifting in the high of their pheremonal influence.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "But even the lustful desires of the werecats would reach their limits.[SOFTBLOCK] Their night was not yet over, and a feast must be had for their newest pridemate.[SOFTBLOCK] Mei would lead the hunt.[SOFTBLOCK] Instinct told her this.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Those werecats who had first amplified her curse followed her, and silently they returned to the cabin.") ]])
fnCutsceneBlocker()

--Switch Mei to werecat form.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
fnCutsceneBlocker()

--Remove special frame.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--Teleport Mei and two of the werecats over.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (31.25 * gciSizePerTile), (56.75 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Teleport To", (31.25 * gciSizePerTile), (56.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Teleport To", (31.25 * gciSizePerTile), (56.40 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneBlocker()

--[System]
--Add these werecats to Mei's following list temporarily.
EM_PushEntity("WerecatA")
	local iWerecatAID = RE_GetID()
DL_PopActiveObject()
EM_PushEntity("WerecatB")
	local iWerecatBID = RE_GetID()
DL_PopActiveObject()

--Store it and tell them to follow.
AL_SetProperty("Follow Actor ID", iWerecatAID)
AL_SetProperty("Follow Actor ID", iWerecatBID)

--[Party Folding]
--Fold the party positions up.
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

--Unblack the scene.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()
