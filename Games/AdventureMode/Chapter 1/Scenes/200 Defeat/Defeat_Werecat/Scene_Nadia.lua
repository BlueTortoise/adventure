--[Nadia Scene]
--Nadia shows up and offers to help Mei out!

--[Variables]
local iHasFlorentina              = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
local iHasAlrauneForm             = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")

--[Dialogue]
--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Nadia] Hey![SOFTBLOCK] Hold up a second!") ]])
fnCutsceneBlocker()

--[Movement]
--Move Nadia to position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Teleport To", (51.25 * gciSizePerTile), (55.50 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina exists, reposition her.
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (50.25 * gciSizePerTile), (55.50 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Mei moves out a bit and Nadia moves north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (37.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (50.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--No Florentina:
if(iHasFlorentina == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])

--Yes Florentina:
else
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Florentina", "Neutral") ]])

end

--If Florentina is present:
if(iHasFlorentina == 1.0) then
	
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Mei![SOFTBLOCK] The little ones told me you got in trouble![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Nadia![SOFTBLOCK] Florentina![SOFTBLOCK] Thank goodness![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're quick, aren't you? [SOFTBLOCK]The cats lost interest in me a few minutes after we got separated.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I've been running for a while...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I ran into Nadia over here.[SOFTBLOCK] She volunteered to help look for you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Good thing I was out here on patrol.[SOFTBLOCK] You all right?[BLOCK][CLEAR]") ]])
	
	--Variables.
	local bNadiaKnowsShapeshifter = false
	local iHasSeenTrannadarSecondScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N")
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local sTrannadarFirstSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S")
	local sTrannadarSecondSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S")
	
	--Third scene: Flip this flag.
	if(iHasSeenTrannadarThirdScene == 1.0) then bNadiaKnowsShapeshifter = true end
	
	--Has seen the second scene, which can only trigger if Scene 1 was "Human" and Scene 2 was "Non-Human".
	if(iHasSeenTrannadarSecondScene == 1.0) then
		
		--Set this flag. Nadia knows Mei is a shapeshifter.
		bNadiaKnowsShapeshifter = true
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		if(sTrannadarSecondSceneForm == "Alraune") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You're looking a lot less leafy than last time...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah![SOFTBLOCK] The little ones have already told me.[SOFTBLOCK] I thought they were joking, but here you are![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarSecondSceneForm == "Bee") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You lost your feelers huh?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh? Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me. You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarSecondSceneForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] I see the whole slime thing was temporary for you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh? Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me. You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarSecondSceneForm == "Ghost") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Did you get tired of being dead?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well, you see, I can - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah![SOFTBLOCK] The little ones have already told me.[SOFTBLOCK] You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		end
	
	--If the first scene was a non-human form:
	elseif(iHasSeenTrannadarSecondScene == 0.0 and sTrannadarFirstSceneForm ~= "Human") then
		
		--Set this flag. Nadia knows Mei is a shapeshifter.
		bNadiaKnowsShapeshifter = true
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		if(sTrannadarFirstSceneForm == "Alraune") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You're looking a lot less leafy than last time...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me.[SOFTBLOCK] I thought they were joking, but here you are![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarFirstSceneForm == "Bee") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You lost your feelers huh?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me.[SOFTBLOCK] You can shapeshift, huh?[SOFTBLOCK] Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarFirstSceneForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] I see the whole slime thing was temporary for you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me.[SOFTBLOCK] You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarFirstSceneForm == "Ghost") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Did you get tired of being dead?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well, you see, I can - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah![SOFTBLOCK] The little ones have already told me.[SOFTBLOCK] You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		end
	end
	
	--Resume. This is also what happens if Nadia has only seen Mei as a human.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm a little bruised, but in one piece.[SOFTBLOCK] I was just thinking I could patch myself up in this cabin.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Huh, yeah.[SOFTBLOCK] This one's abandoned, I think, but it's a pretty defensible position.[SOFTBLOCK] Or at least that's what the Cap'n would say![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Ohmygosh, Mei![SOFTBLOCK] Are you thinking what I'm thinking?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Oh, please no...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] SLUMBER PARTY!!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] That's a thing that us girls do, right?[SOFTBLOCK] Humans love that![SOFTBLOCK] I even remember seeing a book on slumber parties once![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I kinda wish the werecats would come back...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh come on, Florentina.[SOFTBLOCK] It might be fun![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] SQUEEEEEEEEEEE![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] I promise I'll take you back to the Trading Post first thing in the morning![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] We're gonna have so much fun![SOFTBLOCK] Pillow fights![SOFTBLOCK] Scary stories![SOFTBLOCK] I can't wait![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Actually, uh, I could really just go to bed.[SOFTBLOCK] Sprinting away from vicious cat creatures really took it out of me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] That's okay, too.[SOFTBLOCK] Here, you can have one of my trail rations for dinner![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] She's not going to want to eat Alraune food, you dummy.[BLOCK][CLEAR]") ]])
	
	--Determine if Nadia knows Mei can turn into an Alraune.
	local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
	
	--Nadia knows Mei is a shapeshifter:
	if(bNadiaKnowsShapeshifter) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You're a shapeshifter, right Mei?[SOFTBLOCK] Can you eat Alraune food?[BLOCK][CLEAR]") ]])
		
		--Mei has Alraune form:
		if(iHasAlrauneForm == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, is that rotted fruit with cinnamon?[SOFTBLOCK] Delicious![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *nom*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Okay, that was a joy to watch.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mmmm![SOFTBLOCK] I -[SOFTBLOCK] I apparently love the taste of mulch now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, thanks Nadia[SOFTBLOCK]. I'm bushed.[SOFTBLOCK] Can we go to bed?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Y-[SOFTBLOCK]you made a pun![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] N-[SOFTBLOCK]no![SOFTBLOCK] Not on purpose![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] It's okay, I'll borrow that one for later! Goodnight![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, see you in the morning.[SOFTBLOCK] *yawn*[BLOCK][CLEAR]") ]])
		
		--Mei does not have Alraune form:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Rotting mulch...?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That's...[SOFTBLOCK] the exact opposite of appetizing.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I'll take it if you don't want it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] So that's a no?[SOFTBLOCK] Okay, here ya go, Florry![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Call me that again and I'll cut you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Okay, Florry![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *yawn*[SOFTBLOCK] Can you two fight it out tomorrow?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Okay![SOFTBLOCK] See you in the morning![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] This isn't over...[BLOCK][CLEAR]") ]])
		end
	
	--Nadia does not know Mei is a shapeshifter:
	else
	
		--Human case, no Alraune TF:
		if(iHasAlrauneForm == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It smells...[SOFTBLOCK] kinda bad...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] It does?[SOFTBLOCK] Oh, oops.[SOFTBLOCK] We Alraunes eat compost.[SOFTBLOCK] I forgot you humans don't like that.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You know, I'm absolutely not hungry anymore.[SOFTBLOCK] I think I'll just go to bed...[BLOCK][CLEAR]") ]])
	
		--Alraune case:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOFTBLOCK]*sniff*[SOFTBLOCK] Rotting fruit, mulch, and...[SOFTBLOCK] a little cinnamon?[SOFTBLOCK] Smells delicious...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Oh, crud.[SOFTBLOCK] This is Alraune food, not human food.[SOFTBLOCK] Sorry![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I wasn't kidding.[SOFTBLOCK] Here, let me have some.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *nom*...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mmmm.[SOFTBLOCK] Squishy...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well that was a treat to watch.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm -[SOFTBLOCK] hmm.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I guess the little ones haven't told Nadia yet.[SOFTBLOCK] Maybe I'll tell her later.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, bed time.[SOFTBLOCK] See you in the morning...[BLOCK][CLEAR]") ]])
		end
	end
	
	--Common.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] There's only two beds.[SOFTBLOCK] Do you two want to share a room?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well I'd be okay with it if Nadia is...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Oh, you seem to think that was a choice.[SOFTBLOCK][EMOTION|Florentina|Happy] You two are sharing a room, I was just wondering if you [SOFTBLOCK]*wanted*[SOFTBLOCK] to.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Ooh![SOFTBLOCK] Fun![SOFTBLOCK] It won't be a problem, right?[SOFTBLOCK] We're all girls here![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm too tired to argue...") ]])

	--Common.
	fnCutsceneBlocker()
	
--If Mei has not met Nadia before:
elseif(iHasSeenTrannadarFirstScene == 0.0) then
	VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N", 1.0)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] Hey there, stranger![SOFTBLOCK] I heard a commotion, is everything all right?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh dear...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] Huh?[SOFTBLOCK] Oh, you can relax![SOFTBLOCK] I'm one of the good guys![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Name's Nadia.[SOFTBLOCK] I work for the Trading Post.[SOFTBLOCK] Are you in trouble?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I got into a scrap with some cat creatures, but I got away.[SOFTBLOCK] I'm fine, just a little tired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You ought to be careful if you're going to be out here alone.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not alone by choice...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You're also not alone![SOFTBLOCK] Come on, I'll lead you to the Trading Post.[SOFTBLOCK] You'll be safe there.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm...[SOFTBLOCK] really tired...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Oh crud, am I screwing this up?[SOFTBLOCK] I'm trying to be nice![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, you're doing great.[SOFTBLOCK] I just really am tired.[SOFTBLOCK] I was going to check out this cabin, see if it's a safe spot to sleep.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] This one?[SOFTBLOCK] It's abandoned, but it should be pretty secure.[SOFTBLOCK] I think the last owner headed off to Sturnheim.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Tell you what, I'll stay here with you and I can lead you to the Trading Post in the morning.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Well, she seems nice enough.[SOFTBLOCK] I don't think I have the strength to argue anyway...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Sure, okay.[SOFTBLOCK] I'm really bushed, and it's late.[SOFTBLOCK] I've had quite a day.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOFTBLOCK]*yawn*[SOFTBLOCK] Hey, you wouldn't know what this rune means, would you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Mm, no.[SOFTBLOCK] Never seen that before.[SOFTBLOCK] But, you know who would?[SOFTBLOCK] My old pal, Florentina.[SOFTBLOCK] She runs the general goods store at the Trading Post.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] She or Cap'n Blythe can probably help you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I guess I'll ask them, then.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] My name's Mei, by the way.[SOFTBLOCK] Pleased to meet you.[SOFTBLOCK] Thanks for your help.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] No problem![SOFTBLOCK] Here, you can have one of my trail rations for dinner![BLOCK][CLEAR]") ]])
	
	--Human case, no Alraune TF:
	if(iHasAlrauneForm == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It smells...[SOFTBLOCK] kinda bad...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] It does?[SOFTBLOCK] Oh, oops.[SOFTBLOCK] We Alraunes eat compost.[SOFTBLOCK] I forgot you humans don't like that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You know, I'm absolutely not hungry anymore.[SOFTBLOCK] I think I'll just go to bed...") ]])

	--Alraune case:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOFTBLOCK]*sniff*[SOFTBLOCK] rotting fruit, mulch, and...[SOFTBLOCK] a little cinnamon?[SOFTBLOCK] Smells delicious...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Oh, crud.[SOFTBLOCK] This is Alraune food, not human food.[SOFTBLOCK] Sorry![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I wasn't kidding.[SOFTBLOCK] Here, let me have some.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *nom*...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mmmm.[SOFTBLOCK] Squishy...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] I've never met a human who liked Alraune food![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm -[SOFTBLOCK] hmm.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Might be best not to reveal certain facts to random strangers, even if she is a leaf-sister...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, bed time.[SOFTBLOCK] See you in the morning...") ]])
	end
	fnCutsceneBlocker()

--If Mei has met Nadia before:
elseif(iHasSeenTrannadarFirstScene == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hey there, Mei![SOFTBLOCK] I heard a commotion, is everything all right?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nadia![SOFTBLOCK] Oh, thank goodness![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I got into a fight with some cat creatures and barely escaped.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Really?[SOFTBLOCK] Wow, you sure are a magnet for trouble.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Good thing I was out here on patrol.[SOFTBLOCK] You all right?[BLOCK][CLEAR]") ]])
	
	--Variables.
	local bNadiaKnowsShapeshifter = false
	local iHasSeenTrannadarSecondScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N")
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local sTrannadarFirstSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S")
	local sTrannadarSecondSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S")
	
	--Third scene: Flip this flag.
	if(iHasSeenTrannadarThirdScene == 1.0) then bNadiaKnowsShapeshifter = true end
	
	--Has seen the second scene, which can only trigger if Scene 1 was "Human" and Scene 2 was "Non-Human".
	if(iHasSeenTrannadarSecondScene == 1.0) then
		
		--Set this flag. Nadia knows Mei is a shapeshifter.
		bNadiaKnowsShapeshifter = true
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		if(sTrannadarSecondSceneForm == "Alraune") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You're looking a lot less leafy than last time...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah![SOFTBLOCK] The little ones have already told me.[SOFTBLOCK] I thought they were joking, but here you are![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarSecondSceneForm == "Bee") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You lost your feelers huh?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh? Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me. You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarSecondSceneForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] I see the whole slime thing was temporary for you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh? Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me. You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarSecondSceneForm == "Ghost") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Did you get tired of being dead?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well, you see, I can - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah![SOFTBLOCK] The little ones have already told me.[SOFTBLOCK] You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		end
	
	--If the first scene was a non-human form:
	elseif(iHasSeenTrannadarSecondScene == 0.0 and sTrannadarFirstSceneForm ~= "Human") then
		
		--Set this flag. Nadia knows Mei is a shapeshifter.
		bNadiaKnowsShapeshifter = true
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		if(sTrannadarFirstSceneForm == "Alraune") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You're looking a lot less leafy than last time...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me.[SOFTBLOCK] I thought they were joking, but here you are![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarFirstSceneForm == "Bee") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You lost your feelers huh?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me.[SOFTBLOCK] You can shapeshift, huh?[SOFTBLOCK] Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarFirstSceneForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] I see the whole slime thing was temporary for you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah! The little ones have already told me.[SOFTBLOCK] You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		elseif(sTrannadarFirstSceneForm == "Ghost") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Did you get tired of being dead?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[SOFTBLOCK] Oh, well, you see, I can - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Hah![SOFTBLOCK] The little ones have already told me.[SOFTBLOCK] You can shapeshift, huh? Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You've got some magic going on?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's it.[SOFTBLOCK] I think.[SOFTBLOCK] Not sure myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Well as long as you're okay on that front![SOFTBLOCK] What about the rest of you?[BLOCK][CLEAR]") ]])
		end
	end
	
	--Resume. This is also what happens if Nadia has only seen Mei as a human.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm a little bruised, but in one piece.[SOFTBLOCK] I was just thinking I could patch myself up in this cabin.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Huh, yeah.[SOFTBLOCK] This one's abandoned, I think, but it's a pretty defensible position.[SOFTBLOCK] Or at least that's what the Cap'n would say![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Ohmygosh, Mei![SOFTBLOCK] Are you thinking what I'm thinking?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That it's late and I want to go to bed?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] SLUMBER PARTY!!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] That's a thing that us girls do, right?[SOFTBLOCK] Humans love that![SOFTBLOCK] I even remember seeing a book on slumber parties once![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I guess I wouldn't mind the company.[SOFTBLOCK] If something happens, it'd be nice to have some backup.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] SQUEEEEEEEEEEE![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] I promise I'll take you back to the Trading Post first thing in the morning![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] We're gonna have so much fun![SOFTBLOCK] Pillow fights![SOFTBLOCK] Scary stories![SOFTBLOCK] I can't wait![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No really, Nadia.[SOFTBLOCK] I just want to eat something and go to bed.[SOFTBLOCK] I'm...[SOFTBLOCK] really tired.[SOFTBLOCK] I guess sprinting away from cat creatures takes it out of you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] That's okay, too.[SOFTBLOCK] Here, you can have one of my trail rations for dinner![BLOCK][CLEAR]") ]])
	
	--Determine if Nadia knows Mei can turn into an Alraune.
	local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
	
	--Nadia knows Mei is a shapeshifter:
	if(bNadiaKnowsShapeshifter) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] You're a shapeshifter, right Mei?[SOFTBLOCK] Can you eat Alraune food?[BLOCK][CLEAR]") ]])
		
		--Mei has Alraune form:
		if(iHasAlrauneForm == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, is that rotted fruit with cinnamon?[SOFTBLOCK] Delicious![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *nom*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mmmm![SOFTBLOCK] I -[SOFTBLOCK] I apparently love the taste of mulch now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] It's also really nutritious! [BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, thanks Nadia[SOFTBLOCK]. I'm bushed.[SOFTBLOCK] Can we go to bed?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Y-[SOFTBLOCK]you made a pun![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] N-[SOFTBLOCK]no![SOFTBLOCK] Not on purpose![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] It's okay, I'll borrow that one for later! Goodnight![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, see you in the morning. *yawn*") ]])
		
		--Mei does not have Alraune form:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Rotting mulch...?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That's...[SOFTBLOCK] the exact opposite of appetizing.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] So that's a no?[SOFTBLOCK] Okay, more for me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not hungry anymore.[SOFTBLOCK] I think I'll just go to bed.[SOFTBLOCK] Goodnight...[SOFTBLOCK] *yawn*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] See you in the morning!") ]])
		end
	
	--Nadia does not know Mei is a shapeshifter:
	else
	
		--Human case, no Alraune TF:
		if(iHasAlrauneForm == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It smells...[SOFTBLOCK] kinda bad...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] It does?[SOFTBLOCK] Oh, oops.[SOFTBLOCK] We Alraunes eat compost.[SOFTBLOCK] I forgot you humans don't like that.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You know, I'm absolutely not hungry anymore.[SOFTBLOCK] I think I'll just go to bed...") ]])
	
		--Alraune case:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOFTBLOCK]*sniff*[SOFTBLOCK] Rotting fruit, mulch, and...[SOFTBLOCK] a little cinnamon?[SOFTBLOCK] Smells delicious...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] Oh, crud.[SOFTBLOCK] This is Alraune food, not human food.[SOFTBLOCK] Sorry![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I wasn't kidding.[SOFTBLOCK] Here, let me have some.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *nom*...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mmmm.[SOFTBLOCK] Squishy...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[E|Neutral] I've never met a human who liked Alraune food![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm -[SOFTBLOCK] hmm.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I guess the little ones haven't told Nadia yet.[SOFTBLOCK] Maybe I'll tell her later.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, bed time.[SOFTBLOCK] See you in the morning...") ]])
		end
	end

	--Common.
	fnCutsceneBlocker()
end

--Fade to black.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, true, gcf_Evening_R, gcf_Evening_G, gcf_Evening_B, gcf_Evening_A, 0, 0, 0, 1, true) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Movement]
--Reposition the actors. Switch Nadia to sleeping. Where Mei and Florentina go depends on whether or not Florentina was present.
if(iHasFlorentina == 0.0) then

	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Teleport To", (53.25 * gciSizePerTile), (26.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Teleport To", (49.25 * gciSizePerTile), (30.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Special Frame", "Sleep")
	DL_PopActiveObject()
	fnCutsceneBlocker()

--Florentina is present:
else

	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Teleport To", (48.25 * gciSizePerTile), (31.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Teleport To", (49.25 * gciSizePerTile), (30.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Special Frame", "Sleep")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (54.25 * gciSizePerTile), (26.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Sleep")
	DL_PopActiveObject()
	fnCutsceneBlocker()


end

--Close the doors.
fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorS") ]])
fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorW") ]])
fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorN") ]])

--Advance to night time.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 135, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
fnCutsceneWait(180)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Grr, I'm restless and tired at the same time.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (I can't sleep when it's so bright out![SOFTBLOCK] Must be a full moon.)") ]])
fnCutsceneBlocker()

--[Movement]
--Walk to the window. Depends on where Mei is.
if(iHasFlorentina == 0.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (26.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face",  0, -1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

--If Mei is in Nadia's room.
else
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (48.25 * gciSizePerTile), (30.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face",  0, -1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

end

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Huh, the moon is behind the clouds...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (...[SOFTBLOCK] I need to...[SOFTBLOCK] go outside...[SOFTBLOCK] see the moon...)") ]])
fnCutsceneBlocker()

--If Florentina is present, set her activation script here. This is in case the player tries to talk to her.
if(iHasFlorentina == 1.0) then
	EM_PushEntity("Florentina")
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Florentina/WerecatScene.lua")
	DL_PopActiveObject()
end

