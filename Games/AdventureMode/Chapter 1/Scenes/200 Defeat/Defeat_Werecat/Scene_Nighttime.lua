--[Defeat By Werecat]
--Cutscene proper. Only does anything if Mei is a bee, otherwise it takes you to the last rest point.
local bSkipMostOfScene = false

--[Repeat Check]
--If Mei has already seen this scene, it's a normal KO.
local iHasWerecatForm = VM_SetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
if(iHasWerecatForm == 1.0) then
	
	LM_ExecuteScript(gsStandardGameOver)
	return
end

--[Knockout Scene]
--If we're not on the cutscene map, knock down both Mei and Florentina.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "WerecatScene") then
	
	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"WerecatScene\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutsceneInstruction(sString)
	return
end

--[Remove Florentina]
--Take her out of the party, both on the status screen and overworld.
fnRemovePartyMember("Florentina", true)

--[Combat]
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

--[Topics]
--Unlock these topics if they weren't already.
--None yet!

--[Music]
AL_SetProperty("Music", "Null")

--[Camera]
--Move Mei to this position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (100.25 * gciSizePerTile), (29.50 * gciSizePerTile))
DL_PopActiveObject()

--Focus on this position.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Focus Position", (100.25 * gciSizePerTile), (55.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Unfade.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(75)
fnCutsceneBlocker()

--[Movement]
--Teleport Mei and move her south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (100.25 * gciSizePerTile), (53.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(25)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (100.25 * gciSizePerTile), (55.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Unlock the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Focus Actor Name", "Mei")
DL_PopActiveObject()
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] [SOFTBLOCK]*pant*[SOFTBLOCK] *pant*[SOFTBLOCK] ...[SOFTBLOCK] phew![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (I think I lost those damn cat creatures![SOFTBLOCK] Man, they're fast!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (And now I'm totally lost.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (The sun's getting pretty low.[SOFTBLOCK] Better find a place to hole up for the night...)") ]])
fnCutsceneBlocker()
