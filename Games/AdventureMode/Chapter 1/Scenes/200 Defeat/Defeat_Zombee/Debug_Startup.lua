--[Debug_Startup]
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
Debug_PushPrint(true, "Debug Firing Cutscene: Defeat_Zombee\n")

--Flip the flag so this scene plays even if the player has already seen it.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenZombeeScene", "N", 0.0)

--Switch Mei to bee form.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua")

--Otherwise, run the file normally. It may or may not change the map itself.
LM_ExecuteScript(fnResolvePath() .. "Scene_Begin.lua")

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")