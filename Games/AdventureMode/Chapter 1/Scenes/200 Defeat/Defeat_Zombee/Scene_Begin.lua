--[Defeat By Zombee]
--Cutscene proper. Only does anything if Mei is a bee, otherwise it takes you to the last rest point.
local bSkipMostOfScene = false

--[Repeat Check]
--If Mei has already seen this scene, it's a normal KO.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasSeenZombeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenZombeeScene", "N")
if(iHasSeenZombeeScene == 1.0 or sMeiForm ~= "Bee") then
	
	LM_ExecuteScript(gsStandardGameOver)
	return
end

--[Knockout Scene]
--If we're not on the cutscene map, knock down both Mei and Florentina.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "BeehiveBasementScene") then
	
	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"BeehiveBasementScene\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutsceneInstruction(sString)
	return
end

--[Remove Florentina]
--Take her out of the party, both on the status screen and overworld.
fnRemovePartyMember("Florentina", true)

--[Combat]
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

--[Topics]
--Unlock these topics if they weren't already.
--None yet!

--[Music]
AL_SetProperty("Music", "Apprehension")

--[Actors]
--Spawn some additional Zombees. These are NPCs.
TA_Create("ZombeeL")
	TA_SetProperty("Position", 5, 5)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
	TA_SetProperty("Facing", gci_Face_South)
DL_PopActiveObject()
TA_Create("ZombeeR")
	TA_SetProperty("Position", 7, 5)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
	TA_SetProperty("Facing", gci_Face_South)
DL_PopActiveObject()

--Move Mei to this position.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 6, 5)
	TA_SetProperty("Set Special Frame", "Crouch")
DL_PopActiveObject()

--[Cutscene Execution]
--Set this variable so the scene doesn't play twice.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenZombeeScene", "N", 1.0)

--Fading.
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Reposition Mei, make her use the wounded image.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1.0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Scene setup.
fnPartyStopMovement()

--Wait a bit for the fade.
fnCutsceneWait(180)
fnCutsceneBlocker()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (My head...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (S-[SOFTBLOCK]sister drones?[SOFTBLOCK] Oh no, they're-[SOFTBLOCK] I have to get out of here!)") ]])
fnCutsceneBlocker()

--[Movement]
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--The bees drag Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeL")
	ActorEvent_SetProperty("Move To", (5.25 * gciSizePerTile), (6.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (6.25 * gciSizePerTile), (6.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeR")
	ActorEvent_SetProperty("Move To", (7.25 * gciSizePerTile), (6.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--The bees drag Mei again.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeL")
	ActorEvent_SetProperty("Move To", (5.25 * gciSizePerTile), (7.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (6.25 * gciSizePerTile), (7.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeR")
	ActorEvent_SetProperty("Move To", (7.25 * gciSizePerTile), (7.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (The honey here...[SOFTBLOCK] corrupted...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] (Pl-[SOFTBLOCK]please.[SOFTBLOCK] I don't want this...[SOFTBLOCK] Snap out of it, sisters!)") ]])
fnCutsceneBlocker()

--[Scene]
--Fade out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--Scene plays under darkness. It goes badly for Mei.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Heedless to her psychic pleas, the corrupted drones dragged Mei towards the violet honey.[SOFTBLOCK] Muscle, mind, and spirit all recoiled at the thought of what was to come, but their grip was strong.[SOFTBLOCK] There would be no escape.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei struggled despite her captors' grasp, struggling to buy herself another second,[SOFTBLOCK] another moment,[SOFTBLOCK] another hope that would bring rescue closer.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Surely this was not to be her fate.[SOFTBLOCK] Surely someone would come and save her.[SOFTBLOCK] Surely...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The frothing honey seemed to simmer in anticipation as the drones dragged her to the precipice, and...[SOFTBLOCK][SOFTBLOCK] let her go.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei raised her head.[SOFTBLOCK] Unsure if this was part of their plan or something unexpected, she gingerly tried to stand.[SOFTBLOCK] As she rose to her feet, her eyes became transfixed on what was before her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "From within the corrupted honey, something stirred.[SOFTBLOCK] It seemed as if a hand pressed upwards against the viscious surface, but the honey held naught but a few trapped bubbles.[SOFTBLOCK] The ghostly hand pressed, lingered, and then subsided.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A feeling of dread clutched at her mind as her feelers began to hear...[SOFTBLOCK] something.[SOFTBLOCK] Ideas that were neither hers nor those of her hive began to trickle in.[SOFTBLOCK] She could not comprehend them, they were beyond her, but she could feel them taking root in her mind...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Suddenly, the world went dark.[SOFTBLOCK] Panic rose within her as Mei looked around frantically, but saw nothing.[SOFTBLOCK] An empty void, stretching into the infinite, surrounded her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She tried to run, but her legs would not carry her.[SOFTBLOCK] Fear gripped her mind, enthralled her body, and held her fast.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Only when she looked at herself did she realize she was wet with sweat.[SOFTBLOCK] She wiped it from her brow, and looked at her hand.[SOFTBLOCK] Though under her full control, it seemed to belong to someone else.[SOFTBLOCK] It seemed alien.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Transfixed on her hand, she found herself wishing to be back at her home, or the outpost, or with her drone sisters.[SOFTBLOCK] Anywhere but this cursed place.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "When at last she looked up again, the empty void had changed.[SOFTBLOCK] Stars.[SOFTBLOCK] The stars of the sky looked back at her.[SOFTBLOCK] Had she been taken outside?[SOFTBLOCK] She could not feel the wind on her wings.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Was she underground?[SOFTBLOCK] No, she was not.[SOFTBLOCK] She could see the stars.[SOFTBLOCK] She could see them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "All around her, filling the void.[SOFTBLOCK] The lights that filled her vision...[SOFTBLOCK] The lights...[SOFTBLOCK] The stars...[SOFTBLOCK] were not stars...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "One by one, the stars began to rotate around her.[SOFTBLOCK] Then, one of them blinked.[SOFTBLOCK] Soon, more blinked.[SOFTBLOCK] Now, many stars blinked in unison.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Not stars, but eyes...[SOFTBLOCK] the eyes of some enormous creature that stretched across the sky and filled the void around her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei's mouth fell open.[SOFTBLOCK] She tried to call out.[SOFTBLOCK] She tried to scream.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", ".[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK]s[SOFTBLOCK]c[SOFTBLOCK]r[SOFTBLOCK]e[SOFTBLOCK]a[SOFTBLOCK]m[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK].[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "But the void[SOFTBLOCK] remained silent.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She felt at her mouth, but her hands would no longer follow her commands.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Helpless, terrified, wanting to scream.[SOFTBLOCK] Silent.[SOFTBLOCK] Mei could do nothing as the multitude of eyes continued to stare at her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Then, she felt something prickling at the tender flesh between her legs...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A hand had grabbed her sex, squeezing it.[SOFTBLOCK] Where the fingers went, a feeling of pure hatred emanated through her flesh.[SOFTBLOCK] Mei tried to stand,[SOFTBLOCK] to run ...[SOFTBLOCK] fight ...[SOFTBLOCK] but she could not.[SOFTBLOCK] She was helpless as the hand penetrated her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It thrust into her, and more thoughts flooded into her mind.[SOFTBLOCK] She could hear them.[SOFTBLOCK] She could not understand them.[SOFTBLOCK] She knew now that she was small, worthless, and...[SOFTBLOCK] a part of something bigger and grander.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the hand-[SOFTBLOCK] as the fingers-[SOFTBLOCK] As the hatred continued to massage her exposed clitoris, her enfeebled mind was overrun by these new feelings pouring into her.[SOFTBLOCK] She accepted them.[SOFTBLOCK] She must accept them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She was small, worthless, and deserved to be controlled.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The malice that groped her began to change,[SOFTBLOCK] to feel[SOFTBLOCK] pleasurable.[SOFTBLOCK] It was still the hand of hatred, but that was what Mei wanted.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Needed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She needed it to delve into the deepest reaches of her womanhood.[SOFTBLOCK] To fill her.[SOFTBLOCK] And,[SOFTBLOCK] it did.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Scene again. Stay strong, Mei!
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The very essence of time had slipped from her mind.[SOFTBLOCK] Had she been here for hours?[SOFTBLOCK] Days?[SOFTBLOCK] She could not guess.[SOFTBLOCK] She could not even summon the will to try.[SOFTBLOCK] The hand had taken her, completely and fully.[SOFTBLOCK] It left its handprint on her vagina as it receded from her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Panting, sweating, her thoughts lay barren.[SOFTBLOCK] The hand had raped her mind as well as her body.[SOFTBLOCK] There was no Mei.[SOFTBLOCK] There was only the hand.[SOFTBLOCK] The Hand.[SOFTBLOCK] She smiled.[SOFTBLOCK] This was what she wanted.[SOFTBLOCK] This was what she needed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The world returned to her as the ephemeral Hand slid silently back into the corrupted honey.[SOFTBLOCK] Now, the drones near her would not stop her.[SOFTBLOCK] She rose to her feet and gingerly touched her sex, glistening with fresh honey.[SOFTBLOCK] The Handprint was there, and the Handprint was her whole being.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She turned to leave the room and bring all other creatures of Pandemonium to heel before her unknown, unknowable master...") ]])
fnCutsceneBlocker()

--Activate a fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Switch Mei to the MC frames.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Zombee.lua") ]])
fnCutsceneBlocker()

--Remove Mei from the crouch pose.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Bee][E|MC] (Drone obeys.[SOFTBLOCK] Drone obeys.[SOFTBLOCK] Capture bees.[SOFTBLOCK] Convert bees.[SOFTBLOCK] Drone obeys.[SOFTBLOCK] Drone obeys.)") ]])
fnCutsceneBlocker()
