--[Scene Post-Transition]
--After transition, play a random dialogue line.

--[Variables]
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--[Overlay]
--Set the overlay to fullblack. Fade in slowly.
AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Wounded]
--Move Florentina one pixel up so she's behind Mei.
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", 16.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", 0.0, 16.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

--[Fade In]
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Change Mei to the "Downed" frames.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

--If Florentina is present, set her to downed as well.
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(240)
fnCutsceneBlocker()

--[Crouch]
--Mei gets up.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--Florentina, if present, gets up.
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Normal]
--Mei stands up.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--Florentina, if present, gets up.
if(bIsFlorentinaPresent) then
	
	--Facing down.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0.0, 1.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, -1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()

end

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

--[Talking]
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Mei talks to herself.
if(bIsFlorentinaPresent == false) then
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 8)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] That could have gone better...") ]])

	elseif(iDialogueRoll == 1) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Second verse, same as the first...") ]])

	elseif(iDialogueRoll == 2) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] I learned something useful::[SOFTBLOCK] Getting hit, hurts.") ]])

	elseif(iDialogueRoll == 3) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Let's try not to repeat that particular mistake...") ]])

	elseif(iDialogueRoll == 4) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Now I know how video game characters feel...") ]])

	elseif(iDialogueRoll == 5) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] *cough* [SOFTBLOCK] Still a better love story than Twilight...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Heh.") ]])

	elseif(iDialogueRoll == 6) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Good thing I'm still in one piece...") ]])

	elseif(iDialogueRoll == 7) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I think the key is to duck right before they hit me.[SOFTBLOCK] Yeah, that's it.") ]])

	elseif(iDialogueRoll == 8) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Ugh.[SOFTBLOCK] Better not make a habit of this.") ]])
	end

--Florentina and Mei.
else
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 12)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] That could have gone better...") ]])

	elseif(iDialogueRoll == 1) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Second verse, same as the first...") ]])

	elseif(iDialogueRoll == 2) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] I learned something useful::[SOFTBLOCK] Getting hit, hurts.") ]])

	elseif(iDialogueRoll == 3) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Let's try not to repeat that particular mistake...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Let's.[BLOCK][CLEAR]") ]])

	elseif(iDialogueRoll == 4) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Now I know how video game characters feel...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] What's a video game?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, nevermind.") ]])

	elseif(iDialogueRoll == 5) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] *cough* [SOFTBLOCK] Still a better love story than Twilight...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Heh.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I don't follow.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] You don't want to know.") ]])

	elseif(iDialogueRoll == 6) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Good thing I'm still in one piece...") ]])

	elseif(iDialogueRoll == 7) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I think the key is to duck right before they hit me.[SOFTBLOCK] Yeah, that's it.") ]])

	elseif(iDialogueRoll == 8) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Ugh.[SOFTBLOCK] Better not make a habit of this.") ]])
	
	elseif(iDialogueRoll == 9) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I'll be generous and assume that wasn't part of the plan.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Gotta be flexible, right?") ]])
	
	elseif(iDialogueRoll == 10) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] If I didn't know better, I'd think you [SOFTBLOCK]*liked*[SOFTBLOCK] getting beaten up.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hey, don't judge me.") ]])
	
	elseif(iDialogueRoll == 11) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Still in one piece, Florentina?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Not for long at this pace!") ]])
	
	elseif(iDialogueRoll == 12) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] Why must we fight?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Because violence is usually the answer.") ]])
	end

end

--Common.
if(bIsFlorentinaPresent == true) then
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", 0.0, -16.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", -16.0, 0.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--If Florentina is present, walk her to Mei and fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
