--[Retreating Case]
--Cutscene that fires whenever the player retreats. Takes them safely back to the last save point.
-- This is used 99% of the time. Some special cases preclude retreating when we don't want to allow the player to
-- bypass something and go to a save point.
AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
AL_BeginTransitionTo("LASTSAVE", fnResolvePath() .. "Scene_PostTransition.lua")

--[Combat]
--Restore party to full HP in case the player doesn't want to use the save point. This saves time, and is a quality-of-life feature.
-- This is disabled at the moment until I figure out if I want retreating to be a full enemy reset.
--AdvCombat_SetProperty("Restore Party")