--[Emergency Revert]
--Common, used when defeated by a monster that has a special scene afterwards which requires Mei to be in human form. This
-- reverts her back to human using the flashwhite sequence.

--Variables.
local sForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--Knock down Mei.
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--If Florentina is present, knock her down as well.
if(gsFollowersTotal > 0) then
	Cutscene_CreateEvent("Event", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end

--Wait a few ticks.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Mei is wounded.
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

--If Florentina is present, wound her as well.
if(gsFollowersTotal > 0) then
	Cutscene_CreateEvent("Event", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(70)
fnCutsceneBlocker()

--If Mei is not a human, start the transformation sequence.
if(sForm ~= "Human") then
	Cutscene_CreateEvent("Flash Mei White", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Flashwhite", "Wounded")
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua") ]])
	fnCutsceneWait(gci_Flashwhite_Ticks_Total)
	fnCutsceneBlocker()
end

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Fade to black.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 75, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(75)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()