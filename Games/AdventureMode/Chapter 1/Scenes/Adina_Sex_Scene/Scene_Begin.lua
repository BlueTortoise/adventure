--[Adina Sex Scene]
--Cutscene proper. This scene is triggered from a floor trigger. It will end up moving Mei to the SaltFlats map!

--[Spawning]
TA_Create("Adina")
	TA_SetProperty("Position", -100, 100)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/Adina/", false)
	TA_SetProperty("Facing", gci_Face_South)
DL_PopActiveObject() 

--[Movement]
--Move Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (12.25 * gciSizePerTile), (10.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Spawn Adina.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Adina")
	ActorEvent_SetProperty("Teleport To", (7.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a few ticks.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Adina moves south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Adina")
	ActorEvent_SetProperty("Move To", (7.25 * gciSizePerTile), (10.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Face Adina east, and Mei west.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Adina")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Fade out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Now, my thrall, we sleep.[BLOCK][CLEAR][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: There is only one bed.[SOFTBLOCK] Will you lay with me?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes mistress.[SOFTBLOCK] Your will is my will.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Superb.") ]])
fnCutsceneWait(60)
fnCutsceneBlocker()

--[Wait a Bit]
fnCutsceneWait(60)
fnCutsceneBlocker()

--[First Day]
--On the first day, this scene plays.
local iDaysPassed = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N")
if(iDaysPassed < 2) then

	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Thrall.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes, my mistress?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Kiss me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes miss-") ]])
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	--[Scene]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Completely under the spell of the Alraune, Mei embraced her mistress and kissed her deeply.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The two pressed their bodies against one another.[SOFTBLOCK] Mei, covered in the sweat of her day's toil, caressed the blue flesh of her leafy mistress.[SOFTBLOCK] Their fluids merged and mixed as their bodies embraced.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Unable to resist any command, Mei still felt the pleasure of touching and being touched.[SOFTBLOCK] Adina proved an able lover.[SOFTBLOCK] The two continued to kiss as their hands explored one another's features.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina soon found Mei was sensitive along the small of her back.[SOFTBLOCK] With each touch, Mei would push herself towards Adina.[SOFTBLOCK] Now, Adina pulled.[SOFTBLOCK] Mei made no effort to resist her mistress.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "One of the vines that wrapped around Adina's shoulder unwrapped itself and slid down as Adina continued to caress Mei's midriff.[SOFTBLOCK] Mei mindlessly awaited it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The vine entered her dress and found its way to her underwear.[SOFTBLOCK] It slid in, as did Adina's hand.[SOFTBLOCK] Mei's followed, and the three appendages began to massage her sex in tandem.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mistress and slave writhed together as their hands and vines worked in silent tandem.[SOFTBLOCK] Adina withdrew her hand as another vine took its place, instead sending her hand to her own sex.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina controlled Mei's masturbation to keep pace with her own.[SOFTBLOCK] When she slowed, her thrall did.[SOFTBLOCK] When she hastened, her thrall did.[SOFTBLOCK] Mei submitted in her entirety to the motions of Adina's vines.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei's crescendo came in perfect step with Adina's.[SOFTBLOCK] As Adina was about to climax, Mei held her own back.[SOFTBLOCK] Her mistress had not ordered her to cum, so she did not.[SOFTBLOCK] The strain began to build.[SOFTBLOCK] Mei brought all her efforts to stopping her own orgasm.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The sensation continued to build, yet Mei remained resolute.[SOFTBLOCK] She would not orgasm, she would not fail her mistress.[SOFTBLOCK] Adina smiled at the sight of her loyal thrall shaking with intense pleasure as she built higher and higher.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Now, with a kiss, Adina allowed her thrall to finish...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Racked by the powerful orgasm, Mei's body accepted her total subservience as her mind accepted its blankness.[SOFTBLOCK] She fell unconscious as Adina basked in the afterglow...") ]])
	fnCutsceneBlocker()

--[Subsequent Days]
--Every day after the first, this scene plays.
else

	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Thrall.[SOFTBLOCK] Undress.[SOFTBLOCK] Lie down.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes mistress.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Present yourself.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes mistress.[SOFTBLOCK] I obey.") ]])
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	--[Scene]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei waited patiently as she lay on the bed, presenting her rear to her mistress.[SOFTBLOCK] No thoughts passed her head except that she needed to maintain her pose until told otherwise.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She could hear her mistress pacing on the salted floor behind her, but she did not know what she was doing.[SOFTBLOCK] She did not care.[SOFTBLOCK] She had not been told to care.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "A few moments passed as Mei anticipated her next instruction.[SOFTBLOCK] She felt something cool and slimy touch her thigh and begin to trace along her leg, moving towards her exposed sex.[SOFTBLOCK] She did not move or react.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The cool feeling drew up her leg, wrapping around it as it travelled upwards.[SOFTBLOCK] Her mistress had moved closer, and now sat next to her on the bed.[SOFTBLOCK] She placed her soft hand on her thrall's back.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The feeling of her mistress touching her sent a wave of warmth through her.[SOFTBLOCK] The hand reassured her empty mind, brought it peace and security.[SOFTBLOCK] Whatever traces of independence she had felt melted from the thrall's mind.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The oozing vine of her mistress reached into her sex and began to prod.[SOFTBLOCK] Instinctively, Mei moved her hips to accomodate it, helping it as it did its duty.[SOFTBLOCK] The hand of her mistress remained firm on her back.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mistress had begun to touch herself, starting slowly as the vine prodded inside Mei in the same rhythm.[SOFTBLOCK] Each push inside her was matched by her mistress stroking herself.[SOFTBLOCK] Mei kept her position and waited for further instructions.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The hand of her mistress withdrew, replaced by her other.[SOFTBLOCK] It was cooler, wetter, covered in the nectar of her mistress.[SOFTBLOCK] The vine pressed deeper as Mei began to build.[SOFTBLOCK] Anticipation filled her body, but not her empty mind.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mistress stood and repositioned herself behind Mei.[SOFTBLOCK] She could feel her mistress leaning near her rear.[SOFTBLOCK] Her mistress pressed her tongue against Mei's lips, working with the slimy vine.[SOFTBLOCK] Mei built higher, and higher, but still she held her pose.[SOFTBLOCK] Her mistress had not told her otherwise.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The pleasure filled her body.[SOFTBLOCK] Every inch of her trembled with anticipation as she felt wave after wave surge and break inside her.[SOFTBLOCK] Her mistress was perfect with her tongue.[SOFTBLOCK] Mei, without being told, now had a thought of her own.[SOFTBLOCK] She wanted to be perfect for her mistress.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei was now shuddering with sheer delight.[SOFTBLOCK] Obedience combined with physical stimulation to produce a euphoric wave that threatened to overwhelm her pitiful mind.[SOFTBLOCK] One thing stood in the way of her orgasm.[SOFTBLOCK] She had not been ordered to.[SOFTBLOCK] Not yet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The tongue of her mistress withdrew from her, as did the vine.[SOFTBLOCK] Mei did not care, and could not care.[SOFTBLOCK] She had held her position and not moved without being told.[SOFTBLOCK] She had fulfilled her purpose as a thrall.[SOFTBLOCK] That would have been enough, if her mistress willed it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mistress, seeing her thrall prostrated before her, clearly shuddering from intense pleasure, said one word.[SOFTBLOCK][SOFTBLOCK] 'Cum'.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The wave overtook Mei, overwhelming her blank, obedient mind.[SOFTBLOCK] She blacked out in sheer pleasure as every nerve in her body lit in unison.[SOFTBLOCK] Her sleep was dreamless, thoughtless, and obedient.[SOFTBLOCK] She was her mistress' thrall, now, and forever.") ]])
	fnCutsceneBlocker()


end

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Level transition.
fnCutsceneInstruction([[ AL_BeginTransitionTo("SaltFlats", gsRoot .. "Chapter 1/Scenes/Adina_Sex_Scene/Scene_PostTransition.lua") ]])
fnCutsceneBlocker()