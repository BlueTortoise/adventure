--[Scene Post-Transition]
--Mei and Florentina have dragged the cultist to the upper part of the beehive.

--[Variables]
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")

--[Setup]
--Fade back in.
AL_SetProperty("Music", "ForestTheme")
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0)

--Reposition Florentina.
EM_PushEntity("Florentina")
	TA_SetProperty("Position", 14, 17)
	TA_SetProperty("Facing", gci_Face_East)
DL_PopActiveObject()

--Reposition Mei.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 16, 17)
	TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()

--Spawn the downed cultist here.
TA_Create("Cultist")
	TA_SetProperty("Position", 15, 17)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
		
	--Special frames.
	TA_SetProperty("Wipe Special Frames")
	TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
DL_PopActiveObject()

--Set the downed cultist to the wounded frame.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Happy") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Happy") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] There.[SOFTBLOCK] Have at her, bees![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Heh.[SOFTBLOCK] They'll turn her into a bee, and then use her knowledge to purify the hive.[SOFTBLOCK] Good thinking.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] It's only fair.[BLOCK][CLEAR]") ]])

--If Mei is a bee or had bee form, her dialogue changes:
if(sMeiForm == "Bee" or iHasBeeForm == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Besides, now she'll have a family to love her and care for her.[SOFTBLOCK] All will be forgiven.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Whatever.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I hope the other bees can cleanse the hive.[SOFTBLOCK] It'll be hard work...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] The immediate danger is over.[SOFTBLOCK] They can handle it.[SOFTBLOCK] Might be a while before they take down all the corrupted bees, though.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] In the meantime...[BLOCK][CLEAR]") ]])

--If Mei does not have bee form:
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I hope -[SOFTBLOCK] I hope the bees are nice to her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Probably.[SOFTBLOCK] If not, just desserts.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Better not go in the hive basement for a while.[SOFTBLOCK] Taking down the corrupted bees might be a while in coming.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] In the meantime...[BLOCK][CLEAR]") ]])
	 
end

--Mei asks Florentina an important question.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hold up a second.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] ?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You didn't have to help.[SOFTBLOCK] I know you made up some excuse, but you didn't have to help.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And you did.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Oh jeez.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well let's get this in the open, then.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I'm starting to like you, kid.[SOFTBLOCK] When I saw you get all worked up about those cultists, I couldn't help myself.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You liked me getting all shouty?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] You believed in something.[SOFTBLOCK] You put passion into it.[SOFTBLOCK] You let that passion guide you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] That's what I -[SOFTBLOCK] I look for in a f-[SOFTBLOCK]friend.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Friend![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] B-[SOFTBLOCK]business associate![SOFTBLOCK] You're a business associate.[SOFTBLOCK] Nothing more.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh, of course![SOFTBLOCK] You're a cherished business associate, too![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I'm never going to live this down, am I?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Depends on how long we live.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Now, business associate, shall we get back to it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Yeah. Back to the mission.") ]])
fnCutsceneBlocker()

--[Party Fold]
--Florentina walks onto Mei.
Cutscene_CreateEvent("Move Florentina to Mei", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Fold the party positions up.
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

--Trip this flag. Don't show this cutscene on the way out.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeesIgnoreScene", "N", 1.0)