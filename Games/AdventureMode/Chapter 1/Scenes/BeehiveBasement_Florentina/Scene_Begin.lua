--[Beehive Basement - Florentina]
--Mei tries to enter the beehive basement mini-dungeon with Florentina in tow. The bees don't want to let her in,
-- but Florentina can be *very* convincing.

--[Variables]
local sMeiForm                  = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasBeeForm               = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")

--[Special Flags]
--These Zombees will keep patrolling during the cutscene for dramatic effect.
EM_PushEntity("ZombeeA")
	TA_SetProperty("Set Ignore World Stop", true)
DL_PopActiveObject()
EM_PushEntity("ZombeeB")
	TA_SetProperty("Set Ignore World Stop", true)
DL_PopActiveObject()
EM_PushEntity("ZombeeC")
	TA_SetProperty("Set Ignore World Stop", true)
DL_PopActiveObject()
EM_PushEntity("ZombeeD")
	TA_SetProperty("Set Ignore World Stop", true)
DL_PopActiveObject()

--[Movement]
--Move Mei to the blockade.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", 18.25 * gciSizePerTile, 23.50 * gciSizePerTile)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", 18.25 * gciSizePerTile, 24.50 * gciSizePerTile)
DL_PopActiveObject()

--Bees move to intercept!
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 24.50 * gciSizePerTile)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 23.50 * gciSizePerTile)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 22.50 * gciSizePerTile)
DL_PopActiveObject()

--Turn to face Mei/Florentina.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Face", 0.0, 1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(35)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])

--Mei is not a bee, does not have bee form:
if(sMeiForm ~= "Bee" and iHasBeeForm == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] All right, let's rumble![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] What happened?[SOFTBLOCK] You bees forget how to fight?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Let's give them a refresher course.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, wait.[SOFTBLOCK] I think something is going on here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*The bee seems to be preoccupied.[SOFTBLOCK] It keeps looking over its shoulder as it tries to block you*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] This -[SOFTBLOCK] this might be bad, Mei.[SOFTBLOCK] I've never seen bees acting like this.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What's wrong, bee?") ]])
	fnCutsceneBlocker()

--Mei is not a bee, and has bee form:
elseif(sMeiForm ~= "Bee" and iHasBeeForm == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Zzzz! Zz! Bzz? (Drone sisters![SOFTBLOCK] It's me![SOFTBLOCK] Don't you recognize me?)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Bee: Zz. Bzz. ZzzZzz. (Drone unidentified.[SOFTBLOCK] Language credentials accepted.[SOFTBLOCK] Go back.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Zz? (Why?)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Bee: Zzzz. BzzZz. ZzzBzz. (Information dangerous.[SOFTBLOCK] Hive under threat.[SOFTBLOCK] Forget this encounter.)[BLOCK][CLEAR]") ]])
	
	--If Florentina knows about Mei's runestone:
	if(iFlorentinaKnowsAboutRune == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Excuse me, are you going to speak in a manner I can understand?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Huh?[SOFTBLOCK] Oh, sorry.[BLOCK][CLEAR]") ]])
	
	--Otherwise:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] You speak bee?[SOFTBLOCK] Is bee even a language?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Huh?[SOFTBLOCK] Oh, sorry.[SOFTBLOCK] Yeah, sort of.[SOFTBLOCK] It's complicated.[BLOCK][CLEAR]") ]])
	end
	
	--Resume.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] These bees are saying there's some sort of threat, but they won't tell me what.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Threat?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well they can handle it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No, something is different.[SOFTBLOCK] Drone, show me.[SOFTBLOCK] Zzzbzzz.") ]])
	fnCutsceneBlocker()

--Mei is a bee:
elseif(sMeiForm == "Bee") then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Sister drone, you are not speaking with the hive.[SOFTBLOCK] Why?[SOFTBLOCK] What has happened?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ...[SOFTBLOCK] Yes?[SOFTBLOCK] But -[SOFTBLOCK] oh my.[SOFTBLOCK] *Oh my!*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I can see your feelers going crazy over there.[SOFTBLOCK] What's up?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I don't know![SOFTBLOCK] They won't tell me.[SOFTBLOCK] They've put a quarantine in place.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Well I'm sure they've got things under control, then.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] That's what they're trying to say, but I can see all over them that they don't.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Drone, show me.[SOFTBLOCK] Show me the threat.") ]])
	fnCutsceneBlocker()

end

--[Movement]
--Wait a bit.
fnCutsceneWait(35)
fnCutsceneBlocker()

--Bees look to the north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
fnCutsceneWait(35)
fnCutsceneBlocker()

--Mei and Florentina look north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(35)
fnCutsceneBlocker()
	
--[Camera]
--Camera moves up north.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0)
	CameraEvent_SetProperty("Focus Position", 14.25 * gciSizePerTile, 11.50 * gciSizePerTile)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Hold position for a while.
fnCutsceneWait(180)
fnCutsceneBlocker()

--Camera reverts to following Mei.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0)
	CameraEvent_SetProperty("Focus Actor Name", "Mei")
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Movement]
--Everyone looks back at each other.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
fnCutsceneWait(35)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])

--Mei is not a bee, does not have bee form:
if(sMeiForm ~= "Bee" and iHasBeeForm == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] What happened to that bee?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*The bee tries to push you away*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] N-[SOFTBLOCK]no![SOFTBLOCK] That bee -[SOFTBLOCK] it had a handprint on it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] So?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] The cultists who kidnapped me...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Those rat bastards![SOFTBLOCK] They're preying on these bees, too![SOFTBLOCK] I won't stand for this![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Let us through, bee![SOFTBLOCK] We'll sort them out![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] I am so turned on right now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] We -[SOFTBLOCK][EMOTION|Mei|Neutral] Huh?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*The bees stand firm, and do not move*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I guess they're trying to protect us by keeping those things in here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Bee, you have to let us help![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] They're way too wired.[SOFTBLOCK] Luckily, I think I can solve this problem.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] How about a bit of -[SOFTBLOCK] this?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*A sweet smell begins to emanate from the flowers on Florentina's body...*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*...*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*The smell seems to be relaxing the bees...*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mmmm, that's nice.[SOFTBLOCK] What are you doing?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You may not have noticed, but I am about two-thirds poppy.[SOFTBLOCK] My natural perfume is a mild sedative.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] That's right, bees.[SOFTBLOCK] Breathe it nice and deep.[BLOCK][CLEAR]") ]])
	
	--Alraune Mei has something to say.
	if(sMeiForm == "Alraune") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I didn't know we could do that...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You probably can't.[SOFTBLOCK] You look more like some sort of nightshade to me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] What else can this body do...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Heh.[SOFTBLOCK] Focus on the task at hand, for now.[SOFTBLOCK] Maybe I can show you some tricks later.[BLOCK][CLEAR]") ]])
	
	--All other forms.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I didn't know you could do that...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] It does very little to humans, but bees are extra sensitive to plant odors.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] ...[SOFTBLOCK] Hey, don't go overboard there, Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Perish the thought![SOFTBLOCK] It just smells nice.[BLOCK][CLEAR]") ]])
	end
	
	--Resume.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Okay, let's go kick some cultist -[SOFTBLOCK] hey![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] What?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Why are you helping me?[SOFTBLOCK] You've got nothing against the cult.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] This is a beehive.[SOFTBLOCK] The bees don't wear clothes or valuables.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And so you think they put them somewhere - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] When they make a new bee.[SOFTBLOCK] Precisely.[SOFTBLOCK] If we find anything nice, the bees won't mind us borrowing it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Phew![SOFTBLOCK] For a second there I thought you were a good person.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, onward!") ]])
	fnCutsceneBlocker()

--Mei is not a bee, and has bee form:
elseif(sMeiForm ~= "Bee" and iHasBeeForm == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] That poor drone...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] We've got to do something to help her...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's just a bee, Mei.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Can you not see the pain on her face?[SOFTBLOCK] I can almost hear her crying out...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Calm down, Mei.[SOFTBLOCK] I didn't mean it like that.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] That handprint on her face...[SOFTBLOCK] I've seen that before...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] H-[SOFTBLOCK]hey![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Those rat-fink BASTARDS![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Woah![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] The cultists who kidnapped me![SOFTBLOCK] They had handprints on their robes![SOFTBLOCK] They're behind this![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Let me at them![SOFTBLOCK] I'll teach them to mess with the bees![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*The bees stand firm, and do not move*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Heh, I'm getting a little turned on.[SOFTBLOCK] Mei, tell you what.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] How about I get these bees to let us through...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*A sweet smell begins to emanate from the flowers on Florentina's body...*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What is that sweet, sweet smell?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*The smell seems to be relaxing the bees...*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] We Alraunes naturally secrete perfumes.[SOFTBLOCK] This is mine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I happen to be two-thirds poppy.[SOFTBLOCK] Do you know what opiates are?[BLOCK][CLEAR]") ]])
	
	--Alraune Mei has something to say.
	if(sMeiForm == "Alraune") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Can I do that too?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Not by the look of you.[SOFTBLOCK] You look to be more of a nightshade.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I'm sure I can tease some tricks out of your body, though.[SOFTBLOCK] But, later.[SOFTBLOCK] We've got a job to do.[BLOCK][CLEAR]") ]])
	
	--Otherwise:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're drugging them?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] It will calm them down a bit.[SOFTBLOCK] They should let us through now.[BLOCK][CLEAR]") ]])
	end
	
	--Resume.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thanks, Florentina.[SOFTBLOCK] I can't just let those cultists walk all over these poor, innocent bees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hey, wait a minute![SOFTBLOCK] Why are you suddenly defending the innocent?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Seeing you get all worked up really got my juices running...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] I mean...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] The bees dump the valuables of their converts somewhere.[SOFTBLOCK] There's got to be a practical treasure hoard down here somewhere.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh, sure.[SOFTBLOCK] That's it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Doesn't matter.[SOFTBLOCK] Let's go save the bees!") ]])
	fnCutsceneBlocker()

--Mei is a bee:
elseif(sMeiForm == "Bee") then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] That poor drone...[SOFTBLOCK] So this is what happened...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] She cries out to us, but we dare not answer.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Get it together, Mei![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] Such a terrible fate...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I said...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|Slap]*slap*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Keep.[SOFTBLOCK] It.[SOFTBLOCK] Together.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] You're right.[SOFTBLOCK] I've got to be strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I feel their despair as my own thoughts, but...[SOFTBLOCK] I won't be able to save them if I give in.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You give your own pep-talks?[SOFTBLOCK] Great, because I happen to be really bad at that.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I saw -[SOFTBLOCK] I saw - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] CULTISTS![SOFTBLOCK] The handprints, those robes![SOFTBLOCK] Those cultist bastards are behind this![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Woah![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] They're the ones who kidnapped me, and now they're preying on my hive![SOFTBLOCK] I will not allow it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Hot![SOFTBLOCK] Keep going![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Wait, what?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Nothing...[SOFTBLOCK] let's go ruin some cultist's day![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, my sister drones won't let us through.[SOFTBLOCK] They have to maintain the quarantine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh, is that all?[SOFTBLOCK] I think I can change their minds...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*A sweet smell begins to emanate from the flowers on Florentina's body...*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] That smell...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*The smell seems to be relaxing the bees...*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] F-[SOFTBLOCK]Florentina...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Worked like a charm.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] What are you doing?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Natural Alraune perfume.[SOFTBLOCK] I happen to be two-thirds poppy, you know.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Let me...[SOFTBLOCK] taste your nectar...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh, you'd like that wouldn't you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] But we've got a job to do here.[SOFTBLOCK] Clear your mind.[SOFTBLOCK] Focus on the task at hand.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Y-yeah.[SOFTBLOCK] Gotta...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Gotta save the hive!") ]])
	fnCutsceneBlocker()
end

--[Movement]
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Move the bees back to their start positions, but a bit slower.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Move To", 15.25 * gciSizePerTile, 24.50 * gciSizePerTile, 0.45)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 22.50 * gciSizePerTile, 0.45)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Move To", 16.25 * gciSizePerTile, 22.50 * gciSizePerTile, 0.45)
DL_PopActiveObject()

--Florentina moves onto Mei, fold the party.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", 18.25 * gciSizePerTile, 23.50 * gciSizePerTile)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
