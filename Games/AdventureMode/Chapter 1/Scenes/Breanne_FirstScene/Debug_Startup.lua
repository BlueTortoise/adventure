--[Debug_Startup]
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
Debug_PushPrint(true, "Debug Firing Cutscene: Breanne First Scene\n")

--In debug mode, the scene plays even if we've already seen it.
VM_SetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N", 0.0)

--If the map is not Trannadar Trading Post, go there. The constructer will autofire the scene when we arrive.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "BreannesPitStop") then
	AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
	AL_BeginTransitionTo("BreannesPitStop", LM_GetCallStack(0))
	return
end

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")