--[Debug_Startup]
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
Debug_PushPrint(true, "Debug Firing Cutscene: CutsceneSample\n")

--Make sure we're in the right room.
local sCurrentLevelName = AL_GetProperty("Name")
Debug_Print("Current level: " .. sCurrentLevelName .. "\n")

--This cutscene takes place in Breanne's Pit Stop.
if(sCurrentLevelName ~= "BreannesPitStop") then
	Debug_Print("Activating level transition to BreannesPitStop.\n")
	AL_BeginTransitionTo("BreannesPitStop", LM_GetCallStack(0))

--Already there.
else
	--Debug.
	Debug_Print("Currently in the correct location.\n")
	
	--Get Mei's starting position.
	EM_PushEntity("Mei")
		local iMeiX, iMeiY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Reposition Breanne.
	EM_PushEntity("Breanne")
		TA_SetProperty("Position", 12, 13)
	DL_PopActiveObject()
	
	--Scene setup.
	fnPartyStopMovement()
	
	--Breanne notices you and walks over.
	Cutscene_CreateEvent("Move Breanne Left", "Actor")
		ActorEvent_SetProperty("Subject Name", "Breanne")
		ActorEvent_SetProperty("Move To", iMeiX + 32, iMeiY)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Breanne Left", "Actor")
		ActorEvent_SetProperty("Subject Name", "Breanne")
		ActorEvent_SetProperty("Move Amount", -1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Stop Moving", "Actor")
		ActorEvent_SetProperty("Subject Name", "Breanne")
		ActorEvent_SetProperty("Stop Moving")
	DL_PopActiveObject()
	fnCutsceneBlocker()
			
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Target Dialogue Box", gcbDialogue_Source_NPC) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	
	--Actual talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: This is a sample cutscene.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Showing neutral emotion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Showing laugh emotion.")]])
	fnCutsceneBlocker()
	
end

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")