--[Combat Defeat]
--After the battle is over, and the party was defeated, play this scene. There are several scenes here based on the defeat variable.
local iCassandraDefeatHandler = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N")

--[Zero Case]
--This is the case when fighting the werecats at the campsite.
if(iCassandraDefeatHandler == 0.0) then

	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Party takes the crouch pose.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
	if(bIsFlorentinaPresent) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
	end
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Party takes the wounded pose.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
	if(bIsFlorentinaPresent) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
	end
	fnCutsceneWait(120)
	fnCutsceneBlocker()

	--Fade to black.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

	--Wait a bit.
	fnCutsceneWait(120)
	fnCutsceneBlocker()

	--Transition to a new map:
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonNE", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_PostTransition.lua") ]])
	fnCutsceneBlocker()

--[One Case]
--If Mei is solo, and tried to save Cassandra, and was a werecat, this scene plays:
elseif(iCassandraDefeatHandler == 1.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Your strength does not fail you, but you are outnumbered, kinfang.[SOFTBLOCK] Submit![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Perhaps you are not so weak...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Nor are you![SOFTBLOCK] The time draws near.[SOFTBLOCK] Join us in the ceremony![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Ahhh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I will make her feel the moon's grace...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei strode forth into the glade.[SOFTBLOCK] The mewling, helpless girl looked up at her.[SOFTBLOCK] She had been bound and gagged by the cats.[SOFTBLOCK] Her struggle ignited a fire in Mei's heart.[SOFTBLOCK] She was strong.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei knelt in front of the girl.[SOFTBLOCK] She looked like an adventurer of some sort.[SOFTBLOCK] Her clothes had scratches and claw marks on them.[SOFTBLOCK] The nearby cats likewise had signs of battle on them.[SOFTBLOCK] The sight of it made her wet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The time approached.[SOFTBLOCK] The cats continued to pace and touch themselves as night had fallen and the moon had come out.[SOFTBLOCK] With each moment, the girl struggled less and less.[SOFTBLOCK] She, too, had become wet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei took position behind her, smelling at her fluids.[SOFTBLOCK] The pheremones of the other cats hit a fevered pitch.[SOFTBLOCK] Soon they were clawing and licking at one another.[SOFTBLOCK] Mei focused her attention on the bound girl.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The curse took hold over her, sprouting forth fur and a tail from her behind.[SOFTBLOCK] Mei pulled down her pants and exposed her glistening sex to the moonlight.[SOFTBLOCK] Mei began licking.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Other cats took position around them, licking and clawing all over them.[SOFTBLOCK] Mei continued to tongue the girl's juices as she became bestial like Mei.[SOFTBLOCK] Her claws sprang from her hands, allowing her to rip open her bindings.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The cats welcomed her by tongueing every inch of her body.[SOFTBLOCK] She surprised Mei, suddenly, by pushing her over.[SOFTBLOCK] Mei tumbled over, surprised by the girl's strength.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She now descended upon Mei's sex as the orgy continued.[SOFTBLOCK] Mei lost herself in the pile of fur, tongue, and claw.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--[Two Case]
--If Mei is solo, and tried to save Cassandra, and was a human, this scene plays:
elseif(iCassandraDefeatHandler == 2.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Valiant, but futile.[SOFTBLOCK] Submit to us, human.[SOFTBLOCK] You shall join our pride.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I...[SOFTBLOCK] I...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Come...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I...[SOFTBLOCK] submit...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei stepped gingerly into the glade.[SOFTBLOCK] The girl, bound, gagged, and helpless, had watched the battle.[SOFTBLOCK] She continued to struggle against her bonds as Mei knelt beside her.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei's head was swimming as she began feeling at the girl's body.[SOFTBLOCK] The cats had been here, awaiting the moon's grace.[SOFTBLOCK] They were touching themselves and one another in anticipation.[SOFTBLOCK] Mei followed suit.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The other werecats scratched at her, causing her skin to tingle wherever their cursed claws went.[SOFTBLOCK] There was no pain, only excitement.[SOFTBLOCK] Mei soon began scratching at the bound girl, fresh curse running through her own fingernails.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "As the moon drew nearer, the girl stopped struggling.[SOFTBLOCK] At the sight of the girl, Mei had become wet, and could sense that the girl had as well.[SOFTBLOCK] Lost in a bestial surge of pheremones, Mei pulled at the girl's pants and began fingering her sex.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "At last the moonlight became strong enough.[SOFTBLOCK] Mei marvelled at her hands as the curse took over her, turning them to claws and growing black fur all across her body.[SOFTBLOCK] The scent of the night struck her new nose as the same changes followed on the girl.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Now kinfang, she used her claws to tear off the bindings.[SOFTBLOCK] The blonde fang had not changed as much as she had. She seemed horrified as her claws formed and her feet remolded to suit her new purpose.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fur grew out from her skin and her teeth sharpened into fangs. While the new instincts of her body began to change her mind, her horror turned to confusion. Uncertainty. Mei dealt with her by planting a kiss on her lips.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The thrill of the moonlight overcame the last of the blonde fang's resistance, and she revelled in the changes. She returned Mei's kiss and began groping at the cats who stood near her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "More cats had entered the clearing, smelling the pheremones.[SOFTBLOCK] The cats swarmed over the lip-locked lovers, kissing and licking.[SOFTBLOCK] A werecat began fingering Mei's sex while others licked her fur and skin with their rough tongues.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei and the blonde fang lost themselves in the orgy of cats as the moon gave them its perfect grace.[SOFTBLOCK] Minutes dragged into hours as cats continued to suck and lick one another.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Eventually, the moon's arc bent past the line of trees.[SOFTBLOCK] With the loss of their patron, the endless orgasms exhausted the night hunters.[SOFTBLOCK] Soon, Mei drifted to sleep with the blonde fang still in her arms.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()
	
--[Three Case]
--If Mei is not a human or werecat, is solo, and tried to rescue Cassandra but failed:
elseif(iCassandraDefeatHandler == 3.0) then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: You smell like a human -[SOFTBLOCK] your disguise does not fool us![SOFTBLOCK] Remove it![SOFTBLOCK] Submit![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I...[SOFTBLOCK] I...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Come...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I...[SOFTBLOCK] submit...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei stepped gingerly into the glade.[SOFTBLOCK] The girl, bound, gagged, and helpless, had watched the battle.[SOFTBLOCK] She continued to struggle against her bonds as Mei knelt beside her.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei's head was swimming as she began feeling at the girl's body.[SOFTBLOCK] The cats had been here, awaiting the moon's grace.[SOFTBLOCK] They were touching themselves and one another in anticipation.[SOFTBLOCK] Mei followed suit.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The other werecats scratched at her, causing her skin to tingle wherever their cursed claws went.[SOFTBLOCK] There was no pain, only excitement.[SOFTBLOCK] Mei soon began scratching at the bound girl, fresh curse running through her own fingernails.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "As the moon drew nearer, the girl stopped struggling.[SOFTBLOCK] At the sight of the girl, Mei had become wet, and could sense that the girl had as well.[SOFTBLOCK] Lost in a bestial surge of pheremones, Mei pulled at the girl's pants and began fingering her sex.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "At last the moonlight became strong enough.[SOFTBLOCK] Mei marvelled at her hands as the curse took over her, turning them to claws and growing black fur all across her body.[SOFTBLOCK] The scent of the night struck her new nose as the same changes followed on the girl.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Now kinfang, she used her claws to tear off the bindings.[SOFTBLOCK] The blonde fang had not changed as much as she had. She seemed horrified as her claws formed and her feet remolded to suit her new purpose.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fur grew out from her skin and her teeth sharpened into fangs. While the new instincts of her body began to change her mind, her horror turned to confusion. Uncertainty. Mei dealt with her by planting a kiss on her lips.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The thrill of the moonlight overcame the last of the blonde fang's resistance, and she revelled in the changes. She returned Mei's kiss and began groping at the cats who stood near her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "More cats had entered the clearing, smelling the pheremones.[SOFTBLOCK] The cats swarmed over the lip-locked lovers, kissing and licking.[SOFTBLOCK] A werecat began fingering Mei's sex while others licked her fur and skin with their rough tongues.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei and the blonde fang lost themselves in the orgy of cats as the moon gave them its perfect grace.[SOFTBLOCK] Minutes dragged into hours as cats continued to suck and lick one another.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Eventually, the moon's arc bent past the line of trees.[SOFTBLOCK] With the loss of their patron, the endless orgasms exhausted the night hunters.[SOFTBLOCK] Soon, Mei drifted to sleep with the blonde fang still in her arms.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--[Four Case]
--Mei tried to rescue Cassandra as a human with Florentina's help:
elseif(iCassandraDefeatHandler == 4.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Valiant, but futile.[SOFTBLOCK] Submit to us, human.[SOFTBLOCK] You shall join our pride.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Come on, Mei.[SOFTBLOCK] We can take them.[SOFTBLOCK] Just, stand up, grip your sword...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I...[SOFTBLOCK] I...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Come...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I...[SOFTBLOCK] submit...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] What the hay?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: She heeds the call of the night.[SOFTBLOCK] She will join us, plant.[SOFTBLOCK] We bear you no ill will.[SOFTBLOCK] You will be allowed to leave as testament to your strength.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] (Great.[SOFTBLOCK] I can't take them all on alone.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Can I at least watch?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrrrrr...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei stepped gingerly into the glade.[SOFTBLOCK] The girl, bound, gagged, and helpless, had watched the battle.[SOFTBLOCK] She continued to struggle against her bonds as Mei knelt beside her.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei's head was swimming as she began feeling at the girl's body.[SOFTBLOCK] The cats had been here, awaiting the moon's grace.[SOFTBLOCK] They were touching themselves and one another in anticipation.[SOFTBLOCK] Mei followed suit.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The other werecats scratched at her, causing her skin to tingle wherever their cursed claws went.[SOFTBLOCK] There was no pain, only excitement.[SOFTBLOCK] Mei soon began scratching at the bound girl, fresh curse running through her own fingernails.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "As the moon drew nearer, the girl stopped struggling.[SOFTBLOCK] At the sight of the girl, Mei had become wet, and could sense that the girl had as well.[SOFTBLOCK] Lost in a bestial surge of pheremones, Mei pulled at the girl's pants and began fingering her sex.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "At last the moonlight became strong enough.[SOFTBLOCK] Mei marvelled at her hands as the curse took over her, turning them to claws and growing black fur all across her body.[SOFTBLOCK] The scent of the night struck her new nose as the same changes followed on the girl.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Now kinfang, she used her claws to tear off the bindings.[SOFTBLOCK] The blonde fang had not changed as much as she had. She seemed horrified as her claws formed and her feet remolded to suit her new purpose.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fur grew out from her skin and her teeth sharpened into fangs. While the new instincts of her body began to change her mind, her horror turned to confusion. Uncertainty. Mei dealt with her by planting a kiss on her lips.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The thrill of the moonlight overcame the last of the blonde fang's resistance, and she revelled in the changes. She returned Mei's kiss and began groping at the cats who stood near her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "More cats had entered the clearing, smelling the pheremones.[SOFTBLOCK] The cats swarmed over the lip-locked lovers, kissing and licking.[SOFTBLOCK] A werecat began fingering Mei's sex while others licked her fur and skin with their rough tongues.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei and the blonde fang lost themselves in the orgy of cats as the moon gave them its perfect grace.[SOFTBLOCK] Minutes dragged into hours as cats continued to suck and lick one another.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Eventually, the moon's arc bent past the line of trees.[SOFTBLOCK] With the loss of their patron, the endless orgasms exhausted the night hunters.[SOFTBLOCK] Soon, Mei drifted to sleep with the blonde fang still in her arms.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--Mei tried to rescue Cassandra as a werecat with Florentina's help:
elseif(iCassandraDefeatHandler == 5.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Your strength does not fail you, but you are outnumbered, kinfang.[SOFTBLOCK] Submit![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Perhaps you are not so weak...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Come on Mei, we can take them.[SOFTBLOCK] Just, catch your breath...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ha ha, plant![SOFTBLOCK] Your resolve is admirable![SOFTBLOCK] Join us in the ceremony![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Unnhhh...[SOFTBLOCK] yes....[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I think the pheremones are getting to you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] I can't take them alone.[SOFTBLOCK] I guess I'll just watch.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Unnhh...[SOFTBLOCK] Ahhh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I will make her feel the moon's grace...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei strode forth into the glade.[SOFTBLOCK] The mewling, helpless girl looked up at her.[SOFTBLOCK] She had been bound and gagged by the cats.[SOFTBLOCK] Her struggle ignited a fire in Mei's heart.[SOFTBLOCK] She was strong.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei knelt in front of the girl.[SOFTBLOCK] She looked like an adventurer of some sort.[SOFTBLOCK] Her clothes had scratches and claw marks on them.[SOFTBLOCK] The nearby cats likewise had signs of battle on them.[SOFTBLOCK] The sight of it made her wet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The time approached.[SOFTBLOCK] The cats continued to pace and touch themselves as night had fallen and the moon had come out.[SOFTBLOCK] With each moment, the girl struggled less and less.[SOFTBLOCK] She, too, had become wet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei took position behind her, smelling at her fluids.[SOFTBLOCK] The pheremones of the other cats hit a fevered pitch.[SOFTBLOCK] Soon they were clawing and licking at one another.[SOFTBLOCK] Mei focused her attention on the bound girl.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The curse took hold over her, sprouting forth fur and a tail from her behind.[SOFTBLOCK] Mei pulled down her pants and exposed her glistening sex to the moonlight.[SOFTBLOCK] Mei began licking.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Other cats took position around them, licking and clawing all over them.[SOFTBLOCK] Mei continued to tongue the girl's juices as she became bestial like Mei.[SOFTBLOCK] Her claws sprang from her hands, allowing her to rip open her bindings.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The cats welcomed her by tongueing every inch of her body.[SOFTBLOCK] She surprised Mei, suddenly, by pushing her over.[SOFTBLOCK] Mei tumbled over, surprised by the girl's strength.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She now descended upon Mei's sex as the orgy continued.[SOFTBLOCK] Mei lost herself in the pile of fur, tongue, and claw.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--Mei tried to rescue Cassandra as a non-human non-werecat with Florentina's help:
elseif(iCassandraDefeatHandler == 6.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Valiant, but futile.[SOFTBLOCK] Your disguise cannot hide that you are a strong human.[SOFTBLOCK] Submit to us.[SOFTBLOCK] You shall join our pride.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Come on, Mei.[SOFTBLOCK] We can take them.[SOFTBLOCK] Just, stand up, grip your sword...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I...[SOFTBLOCK] I...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Come...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I...[SOFTBLOCK] submit...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] What the hay?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: She heeds the call of the night.[SOFTBLOCK] She will join us, plant.[SOFTBLOCK] We bear you no ill will.[SOFTBLOCK] You will be allowed to leave as testament to your strength.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] (Great.[SOFTBLOCK] I can't take them all on alone.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Can I at least watch?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrrrrr...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei stepped gingerly into the glade.[SOFTBLOCK] The girl, bound, gagged, and helpless, had watched the battle.[SOFTBLOCK] She continued to struggle against her bonds as Mei knelt beside her.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei's head was swimming as she began feeling at the girl's body.[SOFTBLOCK] The cats had been here, awaiting the moon's grace.[SOFTBLOCK] They were touching themselves and one another in anticipation.[SOFTBLOCK] Mei followed suit.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The other werecats scratched at her, causing her skin to tingle wherever their cursed claws went.[SOFTBLOCK] There was no pain, only excitement.[SOFTBLOCK] Mei soon began scratching at the bound girl, fresh curse running through her own fingernails.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "As the moon drew nearer, the girl stopped struggling.[SOFTBLOCK] At the sight of the girl, Mei had become wet, and could sense that the girl had as well.[SOFTBLOCK] Lost in a bestial surge of pheremones, Mei pulled at the girl's pants and began fingering her sex.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "At last the moonlight became strong enough.[SOFTBLOCK] Mei marvelled at her hands as the curse took over her, turning them to claws and growing black fur all across her body.[SOFTBLOCK] The scent of the night struck her new nose as the same changes followed on the girl.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Now kinfang, she used her claws to tear off the bindings.[SOFTBLOCK] The blonde fang had not changed as much as she had. She seemed horrified as her claws formed and her feet remolded to suit her new purpose.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fur grew out from her skin and her teeth sharpened into fangs. While the new instincts of her body began to change her mind, her horror turned to confusion. Uncertainty. Mei dealt with her by planting a kiss on her lips.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The thrill of the moonlight overcame the last of the blonde fang's resistance, and she revelled in the changes. She returned Mei's kiss and began groping at the cats who stood near her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "More cats had entered the clearing, smelling the pheremones.[SOFTBLOCK] The cats swarmed over the lip-locked lovers, kissing and licking.[SOFTBLOCK] A werecat began fingering Mei's sex while others licked her fur and skin with their rough tongues.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei and the blonde fang lost themselves in the orgy of cats as the moon gave them its perfect grace.[SOFTBLOCK] Minutes dragged into hours as cats continued to suck and lick one another.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Eventually, the moon's arc bent past the line of trees.[SOFTBLOCK] With the loss of their patron, the endless orgasms exhausted the night hunters.[SOFTBLOCK] Soon, Mei drifted to sleep with the blonde fang still in her arms.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--[Too Late: Failed]
--Mei is a human, solo, and failed to save Cassandra. She lost the ensuing battle and will now become a werecat.
elseif(iCassandraDefeatHandler == 7.0) then
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Oh, look at you.[SOFTBLOCK] Trying to save someone who doesn't want to be saved?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Come to me.[SOFTBLOCK] Join me...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Nnnghh...[SOFTBLOCK] so horny...[SOFTBLOCK] the moonlight is doing this to me...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Yes...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Purrrrr...[BLOCK][CLEAR]") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Wounded in the battle, Mei could hardly summon the strength to resist as the werecats swarmed around her.[SOFTBLOCK] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[SOFTBLOCK] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[SOFTBLOCK] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei nearly gasped as she realized her vision had been enhanced.[SOFTBLOCK] She could now see perfectly in the dark.[SOFTBLOCK] Every inch of the blonde cat's body was visible to her.[SOFTBLOCK] She could see each individual hair stand on end.[SOFTBLOCK] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[SOFTBLOCK] Her body shuddered with pleasure as the changes rippled forth.[SOFTBLOCK] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The girl kissed her as the changes finished.[SOFTBLOCK] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheremones and moonlight hit a fevered pitch.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[SOFTBLOCK] They worshipped it with their bodies, cleaning their new fangs with their tongues.[SOFTBLOCK] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--[Too Late Non-Human]
--Mei is a non-human, solo, and failed to save Cassandra. She lost the ensuing battle and will now become a werecat.
elseif(iCassandraDefeatHandler == 8.0) then

	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Oh, look at you.[SOFTBLOCK] Trying to save someone who doesn't want to be saved?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Your disguise is clever, but we can smell the human on you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Come to me.[SOFTBLOCK] Join me...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Nnnghh...[SOFTBLOCK] so horny...[SOFTBLOCK] the moonlight is doing this to me...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Yes...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Purrrrr...[BLOCK][CLEAR]") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Wounded in the battle, Mei could hardly summon the strength to resist as the werecats swarmed around her.[SOFTBLOCK] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[SOFTBLOCK] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[SOFTBLOCK] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei nearly gasped as she realized her vision had been enhanced.[SOFTBLOCK] She could now see perfectly in the dark.[SOFTBLOCK] Every inch of the blonde cat's body was visible to her.[SOFTBLOCK] She could see each individual hair stand on end.[SOFTBLOCK] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[SOFTBLOCK] Her body shuddered with pleasure as the changes rippled forth.[SOFTBLOCK] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The girl kissed her as the changes finished.[SOFTBLOCK] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheremones and moonlight hit a fevered pitch.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[SOFTBLOCK] They worshipped it with their bodies, cleaning their new fangs with their tongues.[SOFTBLOCK] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--[Too Late Human + Florentina]
--Mei is a human, with Florentina, and failed to save Cassandra. She lost the ensuing battle and will now become a werecat.
elseif(iCassandraDefeatHandler == 9.0) then

	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Oh, look at you.[SOFTBLOCK] Trying to save someone who doesn't want to be saved?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Come to me.[SOFTBLOCK] Join me...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Come -[SOFTBLOCK] come on, Mei.[SOFTBLOCK] We can take them, just -[SOFTBLOCK] stand up.[SOFTBLOCK] Come on...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Nnnghh...[SOFTBLOCK] so horny...[SOFTBLOCK] the moonlight is doing this to me...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Yes...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Mei? Hey![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I want this...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Oh, lovely.[SOFTBLOCK] You've got the curse, too.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Nothing to do but watch, I guess.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Purrrrr...[BLOCK][CLEAR]") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Wounded in the battle, Mei could hardly summon the strength to resist as the werecats swarmed around her.[SOFTBLOCK] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[SOFTBLOCK] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[SOFTBLOCK] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei nearly gasped as she realized her vision had been enhanced.[SOFTBLOCK] She could now see perfectly in the dark.[SOFTBLOCK] Every inch of the blonde cat's body was visible to her.[SOFTBLOCK] She could see each individual hair stand on end.[SOFTBLOCK] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[SOFTBLOCK] Her body shuddered with pleasure as the changes rippled forth.[SOFTBLOCK] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The girl kissed her as the changes finished.[SOFTBLOCK] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheremones and moonlight hit a fevered pitch.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[SOFTBLOCK] They worshipped it with their bodies, cleaning their new fangs with their tongues.[SOFTBLOCK] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--[Too Late Non-Human + Florentina]
--Mei is a non-human, with Florentina, and failed to save Cassandra. She lost the ensuing battle and will now become a werecat.
elseif(iCassandraDefeatHandler == 10.0) then

	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Oh, look at you.[SOFTBLOCK] Trying to save someone who doesn't want to be saved?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Your disguise is clever, but we can smell the human on you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Come to me.[SOFTBLOCK] Join me...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Come -[SOFTBLOCK] come on, Mei.[SOFTBLOCK] We can take them, just -[SOFTBLOCK] stand up.[SOFTBLOCK] Come on...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Nnnghh...[SOFTBLOCK] so horny...[SOFTBLOCK] the moonlight is doing this to me...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Yes...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Mei? Hey![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I want this...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Oh, lovely.[SOFTBLOCK] You've got the curse, too.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Nothing to do but watch, I guess.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Purrrrr...[BLOCK][CLEAR]") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Wounded in the battle, Mei could hardly summon the strength to resist as the werecats swarmed around her.[SOFTBLOCK] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[SOFTBLOCK] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[SOFTBLOCK] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei nearly gasped as she realized her vision had been enhanced.[SOFTBLOCK] She could now see perfectly in the dark.[SOFTBLOCK] Every inch of the blonde cat's body was visible to her.[SOFTBLOCK] She could see each individual hair stand on end.[SOFTBLOCK] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[SOFTBLOCK] Her body shuddered with pleasure as the changes rippled forth.[SOFTBLOCK] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The girl kissed her as the changes finished.[SOFTBLOCK] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheremones and moonlight hit a fevered pitch.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[SOFTBLOCK] They worshipped it with their bodies, cleaning their new fangs with their tongues.[SOFTBLOCK] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()
end