--[Scene Post-Transition]
--Variables.
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Fade to fullbright.
AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Spawn the cats.
TA_Create("WerecatScene")
	TA_SetProperty("Position", 32, 2)
	TA_SetProperty("Depth", 1)
	fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
DL_PopActiveObject()

--Reposition Mei and Florentina.
if(bIsFlorentinaPresent == false) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face",  0, 1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Teleport To", (32.25 * gciSizePerTile), (3.00 * gciSizePerTile), 1.0)
	DL_PopActiveObject()
else
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face",  0, 1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Teleport To", (31.75 * gciSizePerTile), (3.00 * gciSizePerTile), 1.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face",  0, 1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (32.75 * gciSizePerTile), (3.00 * gciSizePerTile), 1.0)
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Werecat throws Mei and Florentina.
if(bIsFlorentinaPresent == false) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (6.00 * gciSizePerTile), 3.5)
	DL_PopActiveObject()
else
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (31.75 * gciSizePerTile), (6.00 * gciSizePerTile), 3.5)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (32.75 * gciSizePerTile), (6.00 * gciSizePerTile), 3.5)
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Landing.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
if(bIsFlorentinaPresent == false) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (6.50 * gciSizePerTile), 0.5)
	DL_PopActiveObject()
else
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (31.75 * gciSizePerTile), (6.50 * gciSizePerTile), 0.5)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (32.75 * gciSizePerTile), (6.50 * gciSizePerTile), 0.5)
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Dialogue]
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat:[E|Neutral] Very fun, but weak![SOFTBLOCK] Come back when you have mastered your fury![SOFTBLOCK] Ha ha ha ha!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--[Movement]
--Werecat walks off the screen.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatScene")
	ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (0.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatScene")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(50)
fnCutsceneBlocker()

--Mei and Florentina crouch.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end
fnCutsceneWait(25)
fnCutsceneBlocker()

--Mei and Florentina get up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
end
fnCutsceneWait(50)
fnCutsceneBlocker()

--If Mei is alone:
if(bIsFlorentinaPresent == false) then
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (Ugh, that hurt...[SOFTBLOCK] I better go find a spot to rest up...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Stupid cat, laughing at me![SOFTBLOCK] What a jerk![SOFTBLOCK] Your luck won't last forever!)") ]])
	fnCutsceneBlocker()

--Florentina is here:
else
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face",  1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Those cats were tougher than they looked...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Strength in numbers.[SOFTBLOCK] We should go take another crack at them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Yeah -[SOFTBLOCK] after a rest, right?[SOFTBLOCK] I need a minute.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Werecats are quick but they're not durable.[SOFTBLOCK] We need a plan...") ]])
	fnCutsceneBlocker()

end
fnCutsceneWait(25)
fnCutsceneBlocker()

--If Florentina is present, walk her onto Mei and fold the party.
if(bIsFlorentinaPresent == true) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (31.75 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end

--Start the music back up.
fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneWait(1)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Music", "ForestTheme") ]])
fnCutsceneBlocker()
