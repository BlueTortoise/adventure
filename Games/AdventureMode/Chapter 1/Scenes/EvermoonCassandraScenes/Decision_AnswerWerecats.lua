--[Decision Response]
--Script that handles the dialogue case where the werecats offer Mei the chance to join them, or fight to save Cassandra.
local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local sCassandraLocation   = VM_GetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S")
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Get the decision choice.
local sDecision = LM_GetScriptArgument(0)

--[Post Decision]
--Mei tells the cats to let the girl go. Mei is alone and a werecat.
if(sDecision == "LetHerGoSoloCat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If you let the girl go, I'll let you walk away from here in one piece.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: You would threaten our pride?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hssss![SOFTBLOCK] You used so many fangs to hunt one human![SOFTBLOCK] Is she strong, or are you so weak?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Prove your strength, then, fang![SOFTBLOCK] Prove it and you may have her![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Gladly!") ]])
	fnCutsceneBlocker()
	
	--Battle!
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Reinitialize") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Activate") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsWerecatSolo.lua") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Unretreatable", true) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
	fnCutsceneBlocker()

--Mei tells the cats to let the girl go. Mei has Florentina and is a werecat.
elseif(sDecision == "LetHerGoTeamCat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 4.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If you let the girl go, I'll let you walk away from here in one piece.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: You would threaten our pride?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hssss![SOFTBLOCK] You used so many fangs to hunt one human![SOFTBLOCK] Is she strong, or are you so weak?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Prove your strength, then, fang![SOFTBLOCK] Prove it and you may have her![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Gladly!") ]])
	fnCutsceneBlocker()
	
	--Battle!
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Reinitialize") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Activate") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsWerecatTeam.lua") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Unretreatable", true) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
	fnCutsceneBlocker()

--Mei decides to join in. Mei is alone and a werecat.
elseif(sDecision == "JoinThemSoloCat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
	
	--Clear music.
	AL_SetProperty("Music", "Null")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] She will make an excellent addition.[SOFTBLOCK] Let us prepare ourselves for the ceremony.") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei strode forth into the glade.[SOFTBLOCK] The mewling, helpless girl looked up at her.[SOFTBLOCK] She had been bound and gagged by the cats.[SOFTBLOCK] Her struggle ignited a fire in Mei's heart.[SOFTBLOCK] She was strong.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei knelt in front of the girl.[SOFTBLOCK] She looked like an adventurer of some sort.[SOFTBLOCK] Her clothes had scratches and claw marks on them.[SOFTBLOCK] The nearby cats likewise had signs of battle on them.[SOFTBLOCK] The sight of it made her wet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The time approached.[SOFTBLOCK] The cats continued to pace and touch themselves as night had fallen and the moon had come out.[SOFTBLOCK] With each moment, the girl struggled less and less.[SOFTBLOCK] She, too, had become wet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei took position behind her, smelling at her fluids.[SOFTBLOCK] The pheremones of the other cats hit a fevered pitch.[SOFTBLOCK] Soon they were clawing and licking at one another.[SOFTBLOCK] Mei focused her attention on the bound girl.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The curse took hold over her, sprouting forth fur and a tail from her behind.[SOFTBLOCK] Mei pulled down her pants and exposed her glistening sex to the moonlight.[SOFTBLOCK] Mei began licking.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Other cats took position around them, licking and clawing all over them.[SOFTBLOCK] Mei continued to tongue the girl's juices as she became bestial like Mei.[SOFTBLOCK] Her claws sprang from her hands, allowing her to rip open her bindings.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The cats welcomed her by tongueing every inch of her body.[SOFTBLOCK] She surprised Mei, suddenly, by pushing her over.[SOFTBLOCK] Mei tumbled over, surprised by the girl's strength.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She now descended upon Mei's sex as the orgy continued.[SOFTBLOCK] Mei lost herself in the pile of fur, tongue, and claw.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--Mei decides to join in. Florentina is here.
elseif(sDecision == "JoinThemTeamCat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
	
	--Clear music.
	AL_SetProperty("Music", "Null")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] She will make an excellent addition.[SOFTBLOCK] Let us prepare ourselves for the ceremony.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Excuse me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Was I not clear?[SOFTBLOCK] The girl will become fang when the moon graces us.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] I seem to recall we were supposed to save her, Mei.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] She is strong.[SOFTBLOCK] She will be a strong fang.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I'll let you take part in the ceremony...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Eheheh -[SOFTBLOCK] eeehhh.[SOFTBLOCK] No thanks.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] I'll -[SOFTBLOCK] I'll watch...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Your loss.") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei strode forth into the glade.[SOFTBLOCK] The mewling, helpless girl looked up at her.[SOFTBLOCK] She had been bound and gagged by the cats.[SOFTBLOCK] Her struggle ignited a fire in Mei's heart.[SOFTBLOCK] She was strong.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei knelt in front of the girl.[SOFTBLOCK] She looked like an adventurer of some sort.[SOFTBLOCK] Her clothes had scratches and claw marks on them.[SOFTBLOCK] The nearby cats likewise had signs of battle on them.[SOFTBLOCK] The sight of it made her wet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The time approached.[SOFTBLOCK] The cats continued to pace and touch themselves as night had fallen and the moon had come out.[SOFTBLOCK] With each moment, the girl struggled less and less.[SOFTBLOCK] She, too, had become wet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei took position behind her, smelling at her fluids.[SOFTBLOCK] The pheremones of the other cats hit a fevered pitch.[SOFTBLOCK] Soon they were clawing and licking at one another.[SOFTBLOCK] Mei focused her attention on the bound girl.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The curse took hold over her, sprouting forth fur and a tail from her behind.[SOFTBLOCK] Mei pulled down her pants and exposed her glistening sex to the moonlight.[SOFTBLOCK] Mei began licking.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Other cats took position around them, licking and clawing all over them.[SOFTBLOCK] Mei continued to tongue the girl's juices as she became bestial like Mei.[SOFTBLOCK] Her claws sprang from her hands, allowing her to rip open her bindings.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The cats welcomed her by tongueing every inch of her body.[SOFTBLOCK] She surprised Mei, suddenly, by pushing her over.[SOFTBLOCK] Mei tumbled over, surprised by the girl's strength.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She now descended upon Mei's sex as the orgy continued.[SOFTBLOCK] Mei lost herself in the pile of fur, tongue, and claw.") ]])
	fnCutsceneBlocker()

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()
end