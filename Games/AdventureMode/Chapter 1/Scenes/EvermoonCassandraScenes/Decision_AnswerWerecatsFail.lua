--[Decision Response]
--In this case, Mei was human and failed to rescue Cassandra. The cats will give you the chance to join them without a fight.
local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local sCassandraLocation   = VM_GetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S")
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Get the decision choice.
local sDecision = LM_GetScriptArgument(0)

--[Post Decision]
--Fight it out!
if(sDecision == "Fight") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Back off, cat![SOFTBLOCK] You won't have me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ha ha ha![SOFTBLOCK] You are outnumbered, human![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Fangs![SOFTBLOCK] Add her to our pride!") ]])
	fnCutsceneBlocker()
	
	--Battle!
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Reinitialize") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Activate") ]])
	
	--Florentina is not here:
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_FailRescueCassandraAsHumanSolo.lua") ]])
	
	--Florentina is present:
	else
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_FailRescueCassandraAsHumanTeam.lua") ]])
	end
	
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
	fnCutsceneInstruction([[ AdvCombat_SetProperty("Unretreatable", true) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
	fnCutsceneBlocker()

--Join their pride.
elseif(sDecision == "Join") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 2.0) --Indicates Cassandra turned Mei.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
	
	--Clear music.
	AL_SetProperty("Music", "Null")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	
	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Make me...[SOFTBLOCK] one of you...[SOFTBLOCK] please...") ]])
	
	--Florentina is present:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Make me...[SOFTBLOCK] one of you...[SOFTBLOCK] please...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Mei?[SOFTBLOCK] Have you lost your mind?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Why fight them?[SOFTBLOCK] They're so beautiful and graceful...[SOFTBLOCK] I want to be like them...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Uh, okay.[SOFTBLOCK] Well, I guess you've made up your mind.[SOFTBLOCK] I suppose I'll meet you back at the campsite.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You could join in...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Cats aren't my thing, kid.[SOFTBLOCK] But, maybe I'll watch.") ]])
	end
	
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei strode forward, parting the pack of cats.[SOFTBLOCK] They allowed her to reach the center of their pride before they swarmed over her.[SOFTBLOCK] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[SOFTBLOCK] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[SOFTBLOCK] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei nearly gasped as she realized her vision had been enhanced.[SOFTBLOCK] She could now see perfectly in the dark.[SOFTBLOCK] Every inch of the blonde cat's body was visible to her.[SOFTBLOCK] She could see each individual hair stand on end.[SOFTBLOCK] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[SOFTBLOCK] Her body shuddered with pleasure as the changes rippled forth.[SOFTBLOCK] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The girl kissed her as the changes finished.[SOFTBLOCK] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheremones and moonlight hit a fevered pitch.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[SOFTBLOCK] They worshipped it with their bodies, cleaning their new fangs with their tongues.[SOFTBLOCK] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()
end