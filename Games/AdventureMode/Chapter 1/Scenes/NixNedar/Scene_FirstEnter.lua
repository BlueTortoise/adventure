--[Nix Nedar Entry]
--Cutscene proper. Plays when Mei first enters Nix Nedar.

--[Topics]
--Unlock these topics if they weren't already.
--None yet!

--[Music]
AL_SetProperty("Music", "Null")

--[Camera]
--Move Mei to this position.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0.7, 0.7, 0.7, 1, 0.7, 0.7, 0.7, 1) ]])
fnCutsceneWait(180)
fnCutsceneBlocker()

Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (8.25 * gciSizePerTile), (7.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()

--Unfade.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0.7, 0.7, 0.7, 1, 0.7, 0.7, 0.7, 0) ]])
fnCutsceneWait(180)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] That's one small step for Mei...") ]])
fnCutsceneBlocker()

--[Movement]
--Mei moves south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (8.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Look around.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, 1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Look around.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, 1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Look around.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, -1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Look around.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, -1)
DL_PopActiveObject()
fnCutsceneWait(60)
fnCutsceneBlocker()

--Look around.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(60)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] One giant leap for where the heck am I?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Okay, just look around.[SOFTBLOCK] I can figure this out...") ]])
fnCutsceneBlocker()

--Music.
fnCutsceneInstruction([[ AL_SetProperty("Music", "NixNedar") ]])

