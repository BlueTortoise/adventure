--[Debug_Startup]
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
Debug_PushPrint(true, "Debug Firing Cutscene: Outland Bee Scene\n")

--If the map is incorrect, go to the needed map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "PlainsNW") then

	--Remove Florentina from the party for this scene.
    fnRemovePartyMember("Florentina", true)
	
	--Execute.
	AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
	AL_BeginTransitionTo("PlainsNW", LM_GetCallStack(0))
	return
end

--In debug mode, the scene plays even if we've already seen it.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N", 0.0)

--If the map is PlainsNW, reposition Mei to the northewest edge and switch her to bee form.
EM_PushEntity("Adventure Party", 0)
	TA_SetProperty("Position", 9, 7)
	TA_SetProperty("Facing", gci_Face_South)
DL_PopActiveObject()
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua")

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")