--[Outland Farm Post-Bee Scene]
--After Mei gets turned into a bee, this scene plays on her way back.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N", 1.0)

--[Variables]
local iIsFlorentinaPresent = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
local iHasFoundOutlandAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")

--[Initial Movements]
--Mei walks towards the NPCs assembled outside the farm.
Cutscene_CreateEvent("Move Mei To Actors", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Face Mei to the south.
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(2)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneInstruction([[ fnPartyStopMovement() ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--[Florentina Not Present]
--If Florentina is not present, the cutscene is slightly different.
if(iIsFlorentinaPresent == 0.0) then

	--Set this variable to 2.0 so Florentina doesn't ask about bee stuff at the next campfire.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 2.0)

	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	
	--Claudia is not present:
	if(iSavedClaudia == 0.0) then
	
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "MercF", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])

		--If Mei has not previously met Karina:
		if(iHasFoundOutlandAcolyte == 0.0) then

			--Flag.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 2.0)

			--Talking.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Guard: Hey, back up, bee.[SOFTBLOCK] Go on, scram.[SOFTBLOCK] Stay on your side of the river.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Wait![SOFTBLOCK] I mean you no harm, I just need to pass through here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: It talked! I was right![SOFTBLOCK] I was so right![SOFTBLOCK] Eeeeee![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Why of course I can talk.[SOFTBLOCK] Please just let me through, I don't want a fight.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: L-[SOFTBLOCK]let her through![SOFTBLOCK] Do as she says![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Thank you very much.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Guard: Hrmpf.[SOFTBLOCK] I'll let her through, but you don't call the shots around here.[SOFTBLOCK] We've got our eyes on you, bee.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I don't want to cause any trouble.[SOFTBLOCK] I...[SOFTBLOCK] I'm sorry...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: No trouble at all![SOFTBLOCK] Oh this is so exciting![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: You...[SOFTBLOCK][E|Neutral] the hive speaks extensively of you.[SOFTBLOCK] You are interested in us?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Very much so![SOFTBLOCK] My name is Karina, and I'm researching your hive right now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: I'd love to give you an interview, if you're willing.[SOFTBLOCK] I honestly didn't know bees could talk.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] We haven't much to say to humans.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: But you do?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'm a non-conformist.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Splendid![SOFTBLOCK] About that interview...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It'll have to wait.[SOFTBLOCK] I'm on a very important mission for the hive.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Oh, oh![SOFTBLOCK] You've already given me so much to write about.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: If you change your mind, please, I'm sure I can find something to reward your hive with.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The hive will consider your offer.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Guard: Just don't get up to anything suspicious.") ]])
			fnCutsceneBlocker()
		
		--If Mei has previously met Karina:
		else

			--Talking.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Guard: Hey, back up, bee.[SOFTBLOCK] Go on, scram.[SOFTBLOCK] Stay on your side of the river.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Wait![SOFTBLOCK] I mean you no harm, I just need to pass through here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: H-[SOFTBLOCK]hey![SOFTBLOCK] I recognize you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Oh, I hope you didn't get turned into a bee on my account.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You were the one researching us, right?[SOFTBLOCK] I'm afraid it wasn't on your account.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I don't want a fight.[SOFTBLOCK] Please let me pass, I'll be good.[SOFTBLOCK] We promise.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Could you do that for me, Lieutenant?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Please?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Guard: Fine.[SOFTBLOCK] Don't try anything suspicious, talking bee, or we'll be all over you like a smoke cloud.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thank you very much, miss...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Sister Karina, at your service.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Mei, the talking bee, evidently.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Do you think you could spare me some time for an interview?[SOFTBLOCK] You're literally the only talking bee I've ever encountered.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Is it a residual after-effect of having met me earlier?[SOFTBLOCK] Was there an emotional bond that drew you from the hive mind?[SOFTBLOCK] I have so many questions![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'd love to, but the hive has charged me with a very important mission.[SOFTBLOCK] Your questions will have to wait.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Pleasepleaseplease?[SOFTBLOCK] I'm sure I could find something to reward your hive with![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The hive will consider your offer.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Guard: Just don't get up to anything suspicious.") ]])
			fnCutsceneBlocker()

		end
	
	--Claudia is present:
	else
	
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "MercF", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])

		--If Mei has not previously met Karina:
		if(iHasFoundOutlandAcolyte == 0.0) then

			--Flag.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 2.0)

			--Talking.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Guard: Hey, back up, bee.[SOFTBLOCK] Go on, scram.[SOFTBLOCK] Stay on your side of the river.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Wait![SOFTBLOCK] I mean you no harm, I just need to pass through here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Do my eyes betray me?[SOFTBLOCK] Is that you, my savior, Mei?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Mei?[SOFTBLOCK] The one who rescued you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: She has changed form again.[SOFTBLOCK] She walks between them as water flows in the river.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Lieutenant, this bee poses no threat to you.[SOFTBLOCK] Please, allow her to pass.[SOFTBLOCK] She is the one who rescued me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Guard: Oh, so this bee is responsible for inflicting you on the mercs here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Guard: Ugh.[SOFTBLOCK] Fine.[SOFTBLOCK] Don't try anything, bee.[SOFTBLOCK] Talking or not, I don't trust you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I apologize, Mei.[SOFTBLOCK] I did not recognize you at first.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: This is Sister Karina.[SOFTBLOCK] She is the Dove responsible for studying your hive.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Thank you so much for rescuing Sister Claudia![SOFTBLOCK] I don't know what I'd do if she didn't come back...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: You would pray for guidance and prostrate yourself before the divine, child.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: If no guidance came, you were to return to the town of Dry Well.[SOFTBLOCK] Do you remember?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Oh, right.[SOFTBLOCK] Sorry, Sister.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Good to see that you're okay after being cooped up in that prison.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: A test of faith, and no more.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Clearly your runestone draws you to a larger destiny.[SOFTBLOCK] The bees cannot swerve the path of the divine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uh, sure.[SOFTBLOCK] The hive tasked me with finding out more about my runestone, so I better get to it.[SOFTBLOCK] Don't want to leave the drones hanging...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Wait![SOFTBLOCK] Can I interview you?[SOFTBLOCK] It'd help the cause of science![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Karina, she has aided us enough.[SOFTBLOCK] Press no more on her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll let the hive know you asked.[SOFTBLOCK] I'm sure I'll be back once I've figured this thing out.[SOFTBLOCK] Until then!") ]])
			fnCutsceneBlocker()

		end
	end

--[Florentina Is Present, Has Not Seen TF]
--If Florentina was present before the transformation, the cutscene focuses slightly more on her. In this case she doesn't know about Mei's transformation ability.
elseif(iIsFlorentinaPresent == 1.0 and (iFlorentinaKnowsAboutRune == 0.0 or iSavedClaudia == 1.0)) then

	--This cutscene counts as meeting Karina.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 1.0)
	
	--This cutscene also reveals the power of Mei's runestone to Florentina.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)

	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	
	--Claudia is not present:
	if(iSavedClaudia == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])
	
	--Claudia is present:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])
	end

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Look, I'm just asking nicely.[SOFTBLOCK] She'll have black hair and a slim build.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: You mean like that one?") ]])
	fnCutsceneBlocker()
	
	--Florentina and Karina look at Mei.
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--They face each other again.
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	
	--Claudia is not present:
	if(iSavedClaudia == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])
	
	--Claudia is present:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])
	end

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Nope, that one's way too ugly.[SOFTBLOCK] Couldn't be her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Florentina?[SOFTBLOCK] You're okay?") ]])
	fnCutsceneBlocker()
	
	--Florentina and Karina look at Mei again.
	fnCutsceneBlocker()
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	
	--Claudia is not present:
	if(iSavedClaudia == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])
	
	--Claudia is present:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])
	end

	--If Mei has not previously met Karina:
	if(iHasFoundOutlandAcolyte == 0.0) then

		--Not saved Claudia:
		if(iSavedClaudia == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Well the attitude is hard to mistake.[SOFTBLOCK] Mei, are you still in there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Of course![SOFTBLOCK] I -[SOFTBLOCK] well I'm mostly all here.[SOFTBLOCK] There's lots of other drones too, but I'm here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Incredible![SOFTBLOCK] She retained her identity![SOFTBLOCK] You said her name was Mei?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yes, that's my name.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Stupendous![SOFTBLOCK] I don't believe it![SOFTBLOCK] Mei, Mei, could I get an interview from you?[SOFTBLOCK] Please?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] An interview?[SOFTBLOCK] Why?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: My name is Karina, and I've been researching your hive for some time now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: I wasn't aware that bees could speak.[SOFTBLOCK] I could -[SOFTBLOCK] I could get all the facts from an insider's perspective![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We were aware of your attentions, but not their nature.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Better yet, Karina here is one of Claudia's followers.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Is that so?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: That's correct.[SOFTBLOCK] It was she who charged me with studying you.[SOFTBLOCK] I haven't heard from the convent in a few weeks, though.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Can you tell us where she went?[SOFTBLOCK] Perhaps we can find her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: We heard some rumours about a very rare partirhuman species in Quantir, east of here.[SOFTBLOCK] Claudia took most of the convent that way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: They were supposed to have come back by now, but maybe they found something incredible.[SOFTBLOCK] I'm not sure they can top this, though![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Quantir is where we head next, then.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: But my interview...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm sorry, but the hive has given me a very important mission.[SOFTBLOCK] I must fulfill it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Hey, don't you get any ideas.[SOFTBLOCK] I still want to show Claudia that runestone.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That is the mission I have been charged with.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: If you change your mind, I'm sure I can find something to reward your hive with.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The hive will consider your offer.[SOFTBLOCK] Florentina, shall we be off?") ]])

			--Clear the flag on Next Move.
			WD_SetProperty("Clear Topic Read", "NextMove")
		
		--Saved Claudia:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Well the attitude is hard to mistake.[SOFTBLOCK] Mei, are you still in there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Of course![SOFTBLOCK] I -[SOFTBLOCK] well I'm mostly all here.[SOFTBLOCK] There's lots of other drones too, but I'm here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: It is as I predicted.[SOFTBLOCK] Her path is fixed by the divine.[SOFTBLOCK] The bees cannot alter it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, these two won't shut up about you, Mei.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Nice to see you again, Claudia![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: As it is to see you, Mei.[SOFTBLOCK] This is Sister Karina.[SOFTBLOCK] She had been aiding my studies until that unfortunate business in the manor.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Mei![SOFTBLOCK] I've been studying your hive![SOFTBLOCK] Could you maybe give me an interview?[SOFTBLOCK] I've never met a talking bee before![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: You could tell me all about what it's like being a bee![SOFTBLOCK] This will help the cause of science so much![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm sorry, but the hive has given me a crucial mission.[SOFTBLOCK] I must understand my runestone and why it allows me to remember myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Not by coincidence that the bees charge you with the task the divine has.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] They've been talking like this since I got here.[SOFTBLOCK] Cripes.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: All is known by the divine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Shut up?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If I have some spare time, I'll put it on my priorities list.[SOFTBLOCK] Florentina, shall we be off?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: But the interview...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: She has done much for the Doves already, Karina.[SOFTBLOCK] Trust her judgement.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *I am so glad to get out of here...*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] *She was like this the whole time when I rescued her, too!*") ]])
		end
	
	--If Mei has previously met Karina:
	else

		--Not saved Claudia:
		if(iSavedClaudia == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Well the attitude is hard to mistake.[SOFTBLOCK] Mei, are you still in there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Of course -[SOFTBLOCK][EMOTION|Mei|Offended] hey![SOFTBLOCK] Who are you calling ugly?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Mei, was it?[SOFTBLOCK] Do you remember me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Of course I do.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: This is astounding![SOFTBLOCK] I could barely believe it when you spoke.[SOFTBLOCK] Can all bees speak?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We still have the vocal chords for it, but mentally, it's -[SOFTBLOCK] not easy.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Oh, oh, I must get my notepad![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Forget the notepad.[SOFTBLOCK] Maybe you could tell me why you can talk, huh?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I wish I knew.[SOFTBLOCK] In fact, that's why I'm here.[SOFTBLOCK] The hive sent me to find out.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I can say it has something to do with my runestone, but that's all.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Saves me the trouble of busting in there and, er, \"recovering\" it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You were going to steal my runestone!?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well the bees sure as sh*rts weren't going to need it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: That runestone...[SOFTBLOCK] yes that symbol does seem familiar.[SOFTBLOCK] I have no doubt Claudia could assist you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Good, so the goal hasn't changed.[SOFTBLOCK] Let's get going.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: W-[SOFTBLOCK]wait![SOFTBLOCK] Mei![SOFTBLOCK] Please, I have so many questions![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Would you be willing to give me an interview?[SOFTBLOCK] You could advance the cause of science so much![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We'll consider it, but this mission is of crucial importance.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] The entire hive is counting on me.[SOFTBLOCK] I can't let them down.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ... you are such a goodie-goodie...") ]])
		
		--Saved Claudia:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Well the attitude is hard to mistake.[SOFTBLOCK] Mei, are you still in there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Of course![SOFTBLOCK] I -[SOFTBLOCK] hey![SOFTBLOCK] Who are you calling ugly?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: It is as I predicted.[SOFTBLOCK] Her path is fixed by the divine.[SOFTBLOCK] The bees cannot alter it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, these two won't shut up about you, Mei.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Nice to see you again, Claudia![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: As it is to see you, Mei.[SOFTBLOCK] This is Sister Karina.[SOFTBLOCK] She had been aiding my studies until that unfortunate business in the manor.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Mei![SOFTBLOCK] I've been studying your hive![SOFTBLOCK] Could you maybe give me an interview?[SOFTBLOCK] I've never met a talking bee before![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: You could tell me all about what it's like being a bee![SOFTBLOCK] This will help the cause of science so much![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm sorry, but the hive has given me a crucial mission.[SOFTBLOCK] I must understand my runestone and why it allows me to remember myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Not by coincidence that the bees charge you with the task the divine has.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] They've been talking like this since I got here.[SOFTBLOCK] Cripes.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: All is known by the divine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Shut up?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If I have some spare time, sure.[SOFTBLOCK] Florentina, shall we be off?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: But the interview...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: She has done much for the Doves already, Karina.[SOFTBLOCK] Trust her judgement.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *I am so glad to get out of here...*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] *She was like this the whole time when I rescued her, too!*") ]])
		end
	
	end
	fnCutsceneBlocker()

	--Florentina walks onto Mei.
	Cutscene_CreateEvent("Move Florentina To Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (17.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(5)
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--[System]
	--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
    fnAddPartyMember("Florentina")

--[Florentina Is Present, Has Seen TF]
--If Florentina was present before the transformation, the cutscene focuses slightly more on her. In this case she knows Mei can transform at will.
elseif(iIsFlorentinaPresent == 1.0 and iFlorentinaKnowsAboutRune == 1.0) then

	--This cutscene counts as meeting Karina.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 1.0)

	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] She'll be coming this way.[SOFTBLOCK] Look for one with black hair and a little runestone.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: You mean like that one?") ]])
	fnCutsceneBlocker()
	
	--Florentina and Karina look at Mei.
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--They face each other again.
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Nope, that one's way too ugly.[SOFTBLOCK] Couldn't be her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Glad to see you're all right, Florentina.") ]])
	fnCutsceneBlocker()
	
	--Florentina and Karina look at Mei again.
	fnCutsceneBlocker()
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])

	--If Mei has not previously met Karina:
	if(iHasFoundOutlandAcolyte == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I take it your runestone had something to do with this?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yes.[SOFTBLOCK] It flashed white, and I could remember myself.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Incredible![SOFTBLOCK] Florentina was telling me you'd retain your identity, but I didn't believe it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You...[SOFTBLOCK] you've been watching us.[SOFTBLOCK] I mean -[SOFTBLOCK] the hive.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Mei, meet Karina.[SOFTBLOCK] She's been assigned to research the bees by Sister Claudia.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Claudia?[SOFTBLOCK] You're one of her followers?[SOFTBLOCK] Is she here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: I'm afraid not, she left with most of the convent to research the partirhumans in Quantir, over the hills.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: She was supposed to have returned by now to collect my notes, but something must have delayed them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Say, would you be opposed to giving me an interview?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm on a very important mission...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Please?[SOFTBLOCK] You must know that you're the only bee that can speak with humans.[SOFTBLOCK] Your insights would be unprecedented...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] But I'm not a very special bee.[SOFTBLOCK] Really, I'm just like all the others.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I want to be like all the others...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Snap out of it, kid.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I must complete my mission.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: What mission?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Claudia will know why my runestone does this.[SOFTBLOCK] We must find her.[SOFTBLOCK] That is my mission.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: If you change your mind...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I'm sorry, but you humans can offer the hive nothing.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: There's got to be something![SOFTBLOCK] We can work something out![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm, the hive will consider it.[SOFTBLOCK] Florentina, shall we?") ]])

		--Clear the flag on Next Move.
		WD_SetProperty("Clear Topic Read", "NextMove")
	
	--If Mei has previously met Karina:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I take it your runestone had something to do with this?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yes.[SOFTBLOCK] It flashed white, and I could remember myself.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Incredible![SOFTBLOCK] Florentina was telling me you'd retain your identity, but I didn't believe it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Mei, Mei![SOFTBLOCK] Do you remember me?[SOFTBLOCK] Have you lost any other memories?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: How do you bees communicate?[SOFTBLOCK] Can the other bees speak?[SOFTBLOCK] Oh -[SOFTBLOCK] I have so many questions![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Er, in order?[SOFTBLOCK] Yes, no, with these antennae, and...[SOFTBLOCK] sort of, but it's not easy.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Hee hee![SOFTBLOCK] The other drones said you ask them the same questions![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: From a safe distance, of course.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The runestone has something to do with it.[SOFTBLOCK] I don't know what, but it does.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: So if you got rid of it, would you become like any other bee?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I -[SOFTBLOCK] I don't know how I know, but no.[SOFTBLOCK] I'm sure it wouldn't.[SOFTBLOCK] I can feel a connection to it...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So as I had been saying before, if Claudia - [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: We'd love to pay your fee for a chance to study the rune![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Fee!?[SOFTBLOCK] Florentina, we're trying to find me a way home![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] The way I see it, once we send you on your way I get to keep the rune.[SOFTBLOCK] Fair trade, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You won't be needing it back on Earth, will you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Who's to say I want to go back to Earth?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[SOFTBLOCK] you...[SOFTBLOCK] said it...[SOFTBLOCK] right?[SOFTBLOCK] You don't want to go home?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] That was before I became a bee![SOFTBLOCK] The hive is my home now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Fine, whatever.[SOFTBLOCK] The job is still to find Claudia, so let's do that okay?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Would you be willing to give me an interview?[SOFTBLOCK] You could advance the cause of science![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] My hive will consider it...") ]])
		
	end
	fnCutsceneBlocker()

	--Florentina walks onto Mei.
	Cutscene_CreateEvent("Move Florentina To Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (17.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(5)
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--[System]
	--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
	fnAddPartyMember("Florentina")
end
