--[Slimeville Entry]
--Played the first time Mei enters Slimeville.
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Disable the collision flag on the slime guide.
EM_PushEntity("Guide")
	TA_SetProperty("Clipping Flag", false)
DL_PopActiveObject()

--Black out the screen.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])

--Reposition everyone offscreen.
fnCutsceneTeleport("Mei",        -100.25, -100.50)
fnCutsceneTeleport("Guide",      -100.25, -100.50)
if(bIsFlorentinaPresent == true) then
	fnCutsceneTeleport("Florentina", -100.25, -100.50)
end

--Position the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
	CameraEvent_SetProperty("Focus Position", (41.25 * gciSizePerTile), (47.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Teleport the slime to the provided position. Move her north one tile.
fnCutsceneTeleport("Guide", 41.25, 48.00)
fnCutsceneBlocker()
fnCutsceneMove("Guide", 41.25, 47.50)
fnCutsceneBlocker()

--Teleport Mei in. Move her north as well, and focus the camera on her.
fnCutsceneTeleport("Mei", 41.25, 48.00)
fnCutsceneBlocker()
fnCutsceneMove("Guide", 41.25, 46.50)
fnCutsceneMove("Mei", 41.25, 47.50)
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
	CameraEvent_SetProperty("Focus Actor Name", "Mei")
DL_PopActiveObject()
fnCutsceneBlocker()

--If Florentina is in the party, add this sequence.
if(bIsFlorentinaPresent == true) then
	fnCutsceneTeleport("Florentina", 41.25, 48.00)
	fnCutsceneBlocker()
	fnCutsceneMove("Guide", 41.25, 45.50)
	fnCutsceneMove("Mei", 41.25, 46.50)
	fnCutsceneMove("Florentina", 41.25, 47.50)
	fnCutsceneBlocker()
end

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[VOICE|Slime] Almost there!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Move everyone north.
fnCutsceneMove("Guide", 41.25, 42.50)
fnCutsceneMove("Mei", 41.25, 43.50)
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Florentina", 41.25, 44.50)
end
fnCutsceneBlocker()

--Turn.
fnCutsceneMove("Guide", 40.25, 42.50)
fnCutsceneMove("Mei", 41.25, 42.50)
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Florentina", 41.25, 43.50)
end
fnCutsceneBlocker()

--Extra turn if Florentina is present:
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Guide", 39.25, 42.50)
	fnCutsceneMove("Mei", 40.25, 42.50)
	fnCutsceneMove("Florentina", 41.25, 42.50)
end
fnCutsceneBlocker()

--Move everyone west.
fnCutsceneMove("Guide", 31.25, 42.50)
fnCutsceneMove("Mei", 32.25, 42.50)
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Florentina", 33.25, 42.50)
end
fnCutsceneBlocker()

--Turn.
fnCutsceneMove("Guide", 31.25, 41.50)
fnCutsceneMove("Mei", 31.25, 42.50)
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Florentina", 32.25, 42.50)
end
fnCutsceneBlocker()

--Extra turn if Florentina is present:
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Guide", 31.25, 40.50)
	fnCutsceneMove("Mei", 31.25, 41.50)
	fnCutsceneMove("Florentina", 31.25, 42.50)
end
fnCutsceneBlocker()

--Move everyone north.
fnCutsceneMove("Guide", 31.25, 38.50)
fnCutsceneMove("Mei", 31.25, 39.50)
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Florentina", 31.25, 40.50)
end
fnCutsceneBlocker()

--Turn.
fnCutsceneMove("Guide", 30.25, 38.50)
fnCutsceneMove("Mei", 31.25, 38.50)
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Florentina", 31.25, 39.50)
end
fnCutsceneBlocker()

--Extra turn if Florentina is present:
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Guide", 29.25, 38.50)
	fnCutsceneMove("Mei", 30.25, 38.50)
	fnCutsceneMove("Florentina", 31.25, 38.50)
end
fnCutsceneBlocker()

--Final move west.
fnCutsceneMove("Guide", 21.25, 38.50)
fnCutsceneMove("Mei", 22.25, 38.50)
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Florentina", 23.25, 38.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneFace("Guide", 1, 0)
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Welcome to our secret village![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Wow, it's an actual slime village.[SOFTBLOCK] Cool![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Inorite![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Make sure to check out the art gallery and our library and all the cool stuff we have![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Absolutely![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: If you want to go back to the river, just let me know and I'll show you the way![SOFTBLOCK] Have fun!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--If Florentina is present, fold the party.
if(bIsFlorentinaPresent == true) then
	fnCutsceneMove("Florentina", 22.25, 38.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end

--Re-enable collisions on the slime guide.
fnCutsceneInstruction([[ TA_ChangeCollisionFlag("Guide", true) ]])