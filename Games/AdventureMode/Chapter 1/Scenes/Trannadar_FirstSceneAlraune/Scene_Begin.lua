--[Trannadar Intro Scene: Alraune]
--If Mei is an Alraune the first time she enters Trannadar, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
	Cutscene_CreateEvent("Move Mei North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Move Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

else
	Cutscene_CreateEvent("Move Mei West", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

--Wait a bit.
fnCutsceneInstruction([[ fnPartyStopMovement() ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
	Cutscene_CreateEvent("Face Nadia South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Face Nadia South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
    
--Face them east.
else
	Cutscene_CreateEvent("Face Nadia East", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe East", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()

end
fnCutsceneWait(5)
fnCutsceneBlocker()

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Blythe", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Nadia", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: ...[SOFTBLOCK] but that's before we get to all the backbiting.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Not again.[SOFTBLOCK] I said I was sorry![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Is something going on?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: We have a visitor.[SOFTBLOCK] Do it properly this time.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: All right, all right.[SOFTBLOCK] Hello, and welcome to Trannadar Trading Post.[SOFTBLOCK] I'm Nadia.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ... [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ... [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Line?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: You need to tell her our security policy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Oh yeah![SOFTBLOCK] No violence, that's our job.[SOFTBLOCK] We're allowed to beat people up but you aren't.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Was that right?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: Would it kill you to be subtle about it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Thanks, I think![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: I am Darius Blythe, captain of the guard here.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: Our policy on Alraunes is rather lenient, but we won't tolerate violence.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei.[SOFTBLOCK] And,[SOFTBLOCK] I wouldn't dream of it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Don't hesitate to ask if you need something, leaf-sister![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Actually, perhaps you could help me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Have you ever heard of Earth?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Like, soil?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Not a good sign.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: Why are you looking for an Earth?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's where I'm from, and I'd like to be able to get back there.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: You're taking this whole 'Plant Girl' thing even more literally than I do![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Har-dee-har.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: It's not something I've seen on any map.[SOFTBLOCK] Perhaps you should ask Florentina.[SOFTBLOCK] She runs the supply shop.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That I will.[SOFTBLOCK] Thank you.") ]])
fnCutsceneBlocker()

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
