--[Trannadar Intro Scene: Bee]
--If Mei is an Alraune the first time she enters Trannadar, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
	Cutscene_CreateEvent("Move Mei North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Move Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
    
else
	Cutscene_CreateEvent("Move Mei West", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

--Wait a bit.
fnCutsceneInstruction([[ fnPartyStopMovement() ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
	Cutscene_CreateEvent("Face Nadia South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Face Nadia South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()

--Face them east.
else
	Cutscene_CreateEvent("Face Nadia East", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe East", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()

end
fnCutsceneWait(5)
fnCutsceneBlocker()

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Blythe", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: ... the point being that you need to keep the uniform on when you're on duty.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: But it was hot out![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uh, hello?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Ack![SOFTBLOCK] A bee![SOFTBLOCK] Don't sting me![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I wasn't going to sting you, relax.[SOFTBLOCK][E|Surprise] Can I do that?[SOFTBLOCK] Can I sting people?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: A talking bee? [SOFTBLOCK]Maybe you were right, bossman.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Right about what?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: There have been a lot of strange happenings as of late.[SOFTBLOCK] Now we have a talking bee.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Can all the bees talk, or just you?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We can all talk, we just don't need to.[SOFTBLOCK] We can talk to each other using these antennae, you see.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Oh, cool![SOFTBLOCK] So are you, like a representative?[SOFTBLOCK] Are you wanting to buy something?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Er, no.[SOFTBLOCK] I'm looking for someone who can help me figure out what this runestone is.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Oh, that's way out my league.[SOFTBLOCK] You'll want to talk to Florentina, if anyone is going to know, it's her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: I don't believe I've introduced myself.[SOFTBLOCK] I am Darius Blythe, captain of the guard here.[SOFTBLOCK] This is Nadia, our newest recruit.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Howdy![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Call me Mei.[SOFTBLOCK] Pleased to meet you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Ooh, do all bees have names?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] no, just me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: Don't pester the poor thing, Nadia.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Sorry, boss.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: I must warn you that we will not tolerate any sort of violence during your stay.[SOFTBLOCK] If you have a dispute, bring it to me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Got it.") ]])
fnCutsceneBlocker()

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
