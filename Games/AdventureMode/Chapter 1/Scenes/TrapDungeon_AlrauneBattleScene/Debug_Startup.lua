--[Debug_Startup]
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
Debug_PushPrint(true, "Debug Firing Cutscene: TrapDungeon_AlrauneBattleScene\n")

--Set this variable so the scene plays even if we've already seen it.
VM_SetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N", 0.0)

--Change maps to the TrapDungeonA map. The player needs to walk south to trigger it.
AL_BeginTransitionTo("TrapDungeonA", "Null")

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")