--[Combat Victory]
--The party won! Or, did they?

--[Setup]
--Flag.
VM_SetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N", 2.0)
		
--Reset flag.
WD_SetProperty("Clear Topic Read", "Cultists")

--Darken the screen.
AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1)
AudioManager_PlayMusic("Null")

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] *pant*[SOFTBLOCK] *pant*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] That...[SOFTBLOCK] thing...[SOFTBLOCK] *pant*[SOFTBLOCK] wasn't even at full strength...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] What the hell was that!?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Whatever it was, it's dead.[SOFTBLOCK] Burn the body.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *sniff*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] No![SOFTBLOCK] These barrels are full of gunpowder, genius![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Why?[SOFTBLOCK] What would they need with so much gunpowder?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[SOFTBLOCK] I can think of one reason.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Insurance.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] They knew this would happen...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] H-[SOFTBLOCK]hey.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Did the body just move?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] B-b[SOFTBLOCK]-[SOFTBLOCK]but I cut it in two![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Well then cut it into four next time![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] F-[SOFTBLOCK]Florentina! Grab that barrel![SOFTBLOCK] The one that's leaking powder![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] And then what?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] RUN!") ]])
fnCutsceneBlocker()

--Wait a while.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Dialogue resumes.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Happy") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Happy") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended][SOFTBLOCK] I've got the door![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Come on, come on, light![SOFTBLOCK] Light damn it![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Stop mucking about, and light the damn powder![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Got it![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] NOW RUN AND DON'T LOOK BACK!") ]])
fnCutsceneBlocker()

--Wait a while.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Explosion sound effect.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(10)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(5)
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(5)
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Transition]
--Scene resumes on the top floor of the beehive.
fnCutsceneInstruction([[ AL_BeginTransitionTo("TrapDungeonA", gsRoot .. "Chapter 1/Scenes/TrapDungeon_BossBattle/Victory_PostTransition.lua") ]])
fnCutsceneBlocker()