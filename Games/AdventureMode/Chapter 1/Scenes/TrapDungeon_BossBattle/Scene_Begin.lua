--[Boss Battle]
--Battle against the pure ones.

--[Spawning]
--Spawn a cultist.
TA_Create("Cultist")
	TA_SetProperty("Position", 9, 18)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/CultistM/", false)
	TA_SetProperty("Facing", gci_Face_North)
	TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
DL_PopActiveObject()

--[Movement]
--Mei and Florentina move up a bit.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (19.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistM", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: We were wrong.[SOFTBLOCK] The hole wasn't a hole.[SOFTBLOCK] It was watching us.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] We're...[SOFTBLOCK] too late...[SOFTBLOCK] aren't we?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: It's all so obvious.[SOFTBLOCK] It was between the between.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Don't you get it?[SOFTBLOCK] No divisions.[SOFTBLOCK] The desire was right,[SOFTBLOCK] because we desired it to be right.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: They all add to one.[SOFTBLOCK] The line is a circle.[SOFTBLOCK] We're in between.[SOFTBLOCK] You can't leave if you never arrived.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Shut up, or I'll shut you up.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (This guy...[SOFTBLOCK] he's making total sense to me...[SOFTBLOCK] Why?)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: It's the part we don't see.[SOFTBLOCK] I'm a crack.[SOFTBLOCK] Crack[SOFTBLOCK] nook[SOFTBLOCK] space.[SOFTBLOCK] Everything with something filling it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Shut up,[SOFTBLOCK] or I'll give you a crack upside the head![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Hee hee hee![SOFTBLOCK] Look![SOFTBLOCK] I'm already empty!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(90)
fnCutsceneBlocker()

--[Movement]
--Cultist falls over.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(90)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Sad") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] He's...[SOFTBLOCK] not bleeding...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] He was empty inside.[SOFTBLOCK] He is empty...[SOFTBLOCK] I am empty.[SOFTBLOCK] I was always empty...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] [SOFTBLOCK]*Not*[SOFTBLOCK] the right time for a pity party![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Mei![SOFTBLOCK] MEI![SOFTBLOCK] Look at that thing![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] !!!!![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Weapons up![SOFTBLOCK] Right now!") ]])
fnCutsceneBlocker()

--[Battle]
fnCutsceneWait(30)
fnCutsceneBlocker()
fnCutsceneInstruction([[
	AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
	AdvCombat_SetProperty("Reinitialize") 
	AdvCombat_SetProperty("Activate") 
	AdvCombat_SetProperty("Unretreatable", true)
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/902 Boss Infirm.lua", 0)
	AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/TrapDungeon_BossBattle/Combat_Victory.lua")
	AdvCombat_SetProperty("Defeat Script",  gsStandardGameOver)
]])
fnCutsceneBlocker()
