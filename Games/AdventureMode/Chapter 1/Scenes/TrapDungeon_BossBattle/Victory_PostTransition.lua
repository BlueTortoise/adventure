--[Scene Post-Transition]
--Was this really a victory?

--[Spawning]
TA_Create("AlrauneA")
	TA_SetProperty("Position", 14, 23)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_East)
	TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootA.lua")
DL_PopActiveObject()
TA_Create("AlrauneB")
	TA_SetProperty("Position", 17, 23)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootB.lua")
DL_PopActiveObject()
TA_Create("Rochea")
	TA_SetProperty("Position", 16, 24)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
	TA_SetProperty("Facing", gci_Face_North)
	TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_Rochea/RootDungeonAfter.lua")
DL_PopActiveObject()

--[Setup]
--Reposition the party way offscreen.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()

--Camera focuses on Rochea, unfade.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 1000)
	CameraEvent_SetProperty("Focus Actor Name", "Rochea")
DL_PopActiveObject()
fnCutsceneWait(120)
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0)
fnCutsceneBlocker()

--[Movement]
--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Rochea moves north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Rochea")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (23.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(60)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Rochea", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Mei, Florentina...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Please do not have sacrificed yourselves...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: There was so much life left in you...") ]])
fnCutsceneBlocker()
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Dialogue Again]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Happy") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Getting all melodramatic?[SOFTBLOCK] Why am I not surprised!?") ]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Movement]
--Open the door.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutsceneInstruction([[ AL_SetProperty("Open Door", "ToDungeonB") ]])
fnCutsceneWait(30)
fnCutsceneBlocker()

--Reposition Mei and Florentina to the door.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (16.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Teleport To", (16.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Move our heroes out.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (15.75 * gciSizePerTile), (22.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (16.75 * gciSizePerTile), (22.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Close Door", "ToDungeonB") ]])
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: You live![SOFTBLOCK] We had feared - [SOFTBLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Sorry, leaf-sister, but I don't much feel like celebrating.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It got pretty rough, down there.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: We heard an explosion.[SOFTBLOCK] Did the cultists succeed?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] They did, but we killed...[SOFTBLOCK] it...[SOFTBLOCK] and sealed the place off.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Nothing could have survived that blast.[SOFTBLOCK] We barely did.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Yeah...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: I see.[SOFTBLOCK] The looks on your faces suggest this was not the victory we had hoped for.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] This wasn't even the start.[SOFTBLOCK] This was...[SOFTBLOCK] a taunt.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You were acting kinda weird down there, kid.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I think I understand something without knowing it.[SOFTBLOCK] I feel the answers to questions I didn't ask.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But, I can count on my friends to help.[SOFTBLOCK] I -[SOFTBLOCK] as long as we're there for each other, I know we can do it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Nature can take care of herself.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Indeed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Don't go thinking we're friends, Rochea.[SOFTBLOCK] This is it.[SOFTBLOCK] I'm washing my hands of this.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Can't you two make nice?[SOFTBLOCK] For my sake?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well, you and the girls are good in a scrap.[SOFTBLOCK] Maybe you're worth a damn after all.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: You are uncleansed, yet placed yourself at great risk for the sake of all.[SOFTBLOCK] Perhaps you, too, are worth a 'damn'.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah?[SOFTBLOCK] Great.[SOFTBLOCK] Mei, let's get out of here before Rochea starts thinking I don't despise her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: We shall tarry here a little longer to delay the cultists.[SOFTBLOCK] After that, we have...[SOFTBLOCK] many new sisters to join.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You would join them even after what they've done?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: They will find forgiveness among the daughters of the wild.[SOFTBLOCK] Further, this is likely not the last conflict.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Even with their help, we are outnumbered twenty to one.[SOFTBLOCK] We cannot afford to be choosy in a time of war.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Then you need to get the humans on your side, too![SOFTBLOCK] Talk to Blythe![SOFTBLOCK] He will help![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: We -[SOFTBLOCK] we cannot trust them.[SOFTBLOCK] You are exceptions, clearly, but - [SOFTBLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Some things just don't change.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] C'mon Mei.[SOFTBLOCK] Let's get out of here.") ]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Party Fold]
--Florentina walks onto Mei.
Cutscene_CreateEvent("Move Florentina to Mei", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (15.75 * gciSizePerTile), (22.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Fold the party positions up.
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
