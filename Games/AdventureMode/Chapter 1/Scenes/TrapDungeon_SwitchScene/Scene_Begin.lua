--[Switch Scene]
--Cutscene that plays when flipping the switch after talking to the cultist. Also destroys the switch mechanism.

--[Flag]
--Prevents scene from running again.
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSpecialAcolyte", "N", 2.0)

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So I think this is the one she was talking about.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] When I'm through, hit the switch.[SOFTBLOCK] Okay?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right!") ]])
fnCutsceneBlocker()

--[Movement]
--Florentina moves to a standardized position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (27.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Move into the lock.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Brief dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Florentina]Florentina:[VOICE|Florentina][E|Neutral] Hit it!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()
	
--Change switch state.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"FloodSwitch\", false) ")
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Open the door.
fnCutsceneInstruction("AL_SetProperty(\"Open Door\", \"Door04\")")
fnCutsceneInstruction("AL_SetProperty(\"Close Door\", \"Door03\")")
fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Florentina moves past it.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (27.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Florentina hits the other switch.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"FloodSwitchN\", true) ")
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Open the door.
fnCutsceneInstruction("AL_SetProperty(\"Open Door\", \"Door03\")")
fnCutsceneInstruction("AL_SetProperty(\"Close Door\", \"Door04\")")
fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Brief dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Florentina]Florentina:[VOICE|Florentina][E|Neutral] It worked![SOFTBLOCK] Come on!") ]])
fnCutsceneBlocker()

--Mei moves over.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Florentina hits the switch again.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"FloodSwitchN\", false) ")
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Open the door.
fnCutsceneInstruction("AL_SetProperty(\"Open Door\", \"Door04\")")
fnCutsceneInstruction("AL_SetProperty(\"Close Door\", \"Door03\")")
fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, 1)
DL_PopActiveObject()

--Mei moves over.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (5.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, -1)
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Great work![SOFTBLOCK] I was getting so sick of all these switches![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Couldn't have done it without you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But, what's this switch over here?") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Movement]
--Both move over.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (30.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It says 'Initiates, [SOFTBLOCK]*absolutely*[SOFTBLOCK] do not pull this lever.[SOFTBLOCK] It will break all the other levers and force all the gates open, permanently.[SOFTBLOCK] We will be defenseless!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] [SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK].[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] MINE![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] NO![SOFTBLOCK] MINE!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Random roll. Who will win?
local iRoll = LM_GetRandomNumber(0, 100)

--Low roll, Mei wins!
if(iRoll <= 50) then
	
	--Florentina moves onto Mei. Mei moves slightly ahead.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (4.50 * gciSizePerTile), 2.5)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (31.75 * gciSizePerTile), (4.50 * gciSizePerTile), 1.5)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Mei turns and smacks Florentina!
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Florentina moves south a bit. Mei moves to the switch.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (31.75 * gciSizePerTile), (5.00 * gciSizePerTile), 0.25)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (4.50 * gciSizePerTile), 2.5)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Mei hits the switch and breaks everything.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
	fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"BreakSwitch\", true) ")
	fnCutsceneBlocker()
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	--All the doors open.
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door00") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door01") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door02") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door03") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door04") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door05") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door06") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door07") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door08") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door09") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door10") ]])
	
	--Sounds.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()

	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--Florentina gets back up.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", -1, 1)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Florentina faces Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 1, -1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--[Dialogue]
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] I'm sorry![SOFTBLOCK] Are you hurt?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Just my ego.[SOFTBLOCK] But, I get the next one![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Okay, okay.[SOFTBLOCK] Let's get going!") ]])
	fnCutsceneBlocker()
	
	--[System]
	--Move Florentina onto Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (4.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Florentina wins.
else
	--Florentina runs over Mei. Mei is knocked southwards.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (4.50 * gciSizePerTile), 2.5)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (31.75 * gciSizePerTile), (5.50 * gciSizePerTile), 0.50)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Florentina turns north and flips the switch.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Florentina hits the switch and breaks everything.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
	fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"BreakSwitch\", true) ")
	fnCutsceneBlocker()
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	--All the doors open.
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door00") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door01") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door02") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door03") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door04") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door05") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door06") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door07") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door08") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door09") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door10") ]])
	
	--Sounds.
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()

	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--Mei gets back up.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1, -1)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Florentina faces Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1, 1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--[Dialogue]
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Yeah,[SOFTBLOCK] suck it,[SOFTBLOCK] cultists![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hey![SOFTBLOCK] I wanted to do it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Destiny waits not for the slow.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Aww.[SOFTBLOCK] Can I get the next one then?[SOFTBLOCK] I want to ruin someone's day![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well, since you asked so nicely,[SOFTBLOCK] okay.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right.[SOFTBLOCK] Back to work!") ]])
	fnCutsceneBlocker()
	
	--[System]
	--Move Florentina onto Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (4.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end