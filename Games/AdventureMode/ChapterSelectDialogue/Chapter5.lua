--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iChapter1 = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N")
        
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chapter 5.[SOFTBLOCK] Christine.[BLOCK][CLEAR]") ]])
    if(iChapter1 < 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "It is recommended to complete Chapter 1 before playing Chapter 5.[BLOCK][CLEAR]") ]])
    end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Do you want to play this chapter?[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	
	--Decision mode.
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

--"Yes" Launches Chapter 5.
elseif(sTopicName == "Yes") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Change the main character to Christine.
	gsPartyLeaderName = "Christine"
	
	--Push Christine on the activity stack and take control of her. This causes her to transition to the new level correctly.
	EM_PushEntity("Christine")
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()
	AL_SetProperty("Player Actor ID", giPartyLeaderID)
    
    --Loading. Load Chapter 1's graphics.
    fnIssueLoadReset("AdventureModeCH5")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/500 Chapter 5 Sprites.lua")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/501 Chapter 5 Portraits.lua")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/502 Chapter 5 Actors.lua")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/503 Chapter 5 Scenes.lua")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/504 Chapter 5 Overlays.lua")
    SLF_Close()
    fnCompleteLoadSequence()
	
	--Debug.
	LM_ExecuteScript(gsRoot .. "Chapter1Init/000 Party.lua")
    
    --Transform Christine into a human male. This will set his graphics. Also do this for all other created characters
    -- for the same reason.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Male.lua")
    LM_ExecuteScript(gsRoot .. "FormHandlers/55/Form_Doll.lua")
    LM_ExecuteScript(gsRoot .. "FormHandlers/SX399/Form_SteamDroid.lua")
    LM_ExecuteScript(gsRoot .. "FormHandlers/JX101/Form_SteamDroid.lua")

	gsPartyLeaderName = "Christine"
	VM_SetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N", 5.0)
	LM_ExecuteScript(gsRoot .. "Chapter5Init/000 Party.lua")
	LM_ExecuteScript(gsRoot .. "Chapter5Init/001 Variables.lua")
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Build Scene List.lua")
	LM_ExecuteScript(gsRoot .. "Maps/Build Warp List.lua", "Chapter 5")
	gsStandardGameOver = gsRoot .. "Chapter5Scenes/Defeat_BackToSave/Scene_Begin.lua"
	gsStandardRetreat = gsRoot .. "Chapter5Scenes/Retreat/Scene_Begin.lua"
    
    --Doctor Bag. Set to zero.
	VM_SetVar("Root/Variables/System/Special/iDoctorBagCharges", "N", giTotalDoctorBagCharges)
    VM_SetVar("Root/Variables/System/Special/iDoctorBagChargesMax", "N", 0)
	AdInv_SetProperty("Doctor Bag Charges", giTotalDoctorBagCharges)
	AdInv_SetProperty("Doctor Bag Charges Max", 0)

	--Load the map.
	AL_BeginTransitionTo("RegulusCryoA", "Null")

--"No" stops here.
elseif(sTopicName == "No") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")

end