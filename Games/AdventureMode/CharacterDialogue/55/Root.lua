--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.
-- Note that this is used for chat dialogues, not the part in Sector 198.

--[Arguments]
--Argument Listing:
-- 0: sTopicName  - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
-- 1: sIsFireside - Optional. If it exists, this was activated from the AdventureMenu at a save point. In those cases, the 
--                  Active Object is undefined.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName  = LM_GetScriptArgument(0)
local sIsFireside = LM_GetScriptArgument(1)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Change music to Florentina's theme.
	glLevelMusic = AL_GetProperty("Music")
	AL_SetProperty("Music", "2855sTheme")
	
	--Position party opposite one another.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	
	--Florentina's introduction.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55: Hold here, Christine.[SOFTBLOCK] Let's exchange information before we move on.[BLOCK][CLEAR]") ]])

	--[Topics]
	--Activate topics mode once the dialogue is complete.
    local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
    if(iReachedBiolabs == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "2855") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs") ]])
    end
end
