--[FarmHandler]
--If Mei is currently working as a thrall on the salt flats farm, this is executed.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Base]
--If this is the "Hello" then this is the first speech event.
if(sTopicName == "Hello") then

	--[Variable Checking]
	local bIsAllFarmworkDone = true
	local bNeedsPollen = false
	for i = 0, 7, 1 do
		
		--Get the state.
		local iVariable = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iPlotState" .. i, "N")
		
		--At least one plot needs tending, so the farmwork is not done.
		if(iVariable ~= gci_SFF_NoTending) then
			bIsAllFarmworkDone = false
		end
		
		--If at least one plot needs pollen, Adina changes her dialogue.
		if(iVariable == gci_SFF_SpecialPollen) then
			bNeedsPollen = true
		end
		
	end
		
	--[Dialogue Setup]
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

	--[Not All Farmwork Is Done]
	if(bIsAllFarmworkDone == false) then

		--[Variables]
		local iHasPollen  = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasPollen", "N")
		local iDaysPassed = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N")
		local iMeiLovesAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")

		--[Mei Needs Pollen]
		--Adina gives Mei the pollen for the farm job.
		if(bNeedsPollen == true and iHasPollen == 0.0) then
			
			--Pollen get!
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasPollen", "N", 1.0)
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: I require your pollen to complete my task, mistress.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Very well, take it. Breathe it in, let it soak into you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes mistress.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Now go, thrall.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: I serve.") ]])

		--[Mei Needs Pollen, and Has It]
		--Adina tells Mei to get back to work.
		elseif(bNeedsPollen == true and iHasPollen == 1.0) then

			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Thrall, your work is incomplete.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes, mistress.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Complete it.[BLOCK]") ]])
	
			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Acquiesce\", " .. sDecisionScript .. ", \"YesMistress\") ")
			
			--Bonus option if Mei has been enslaved for 2+ days:
			if(iMeiLovesAdina == 1.0) then
				fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Thrall Wishes To Kiss You\",  " .. sDecisionScript .. ", \"KissHer\") ")
			end
			
			--Resistance Option.
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Resist\",  " .. sDecisionScript .. ", \"Resist\") ")
			fnCutsceneBlocker()
		
		--[Mei Needs No Pollen]
		--Adina tells Mei to get back to work.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Thrall, your work is incomplete.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes, mistress.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Complete it.[BLOCK]") ]])
	
			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Acquiesce\", " .. sDecisionScript .. ", \"YesMistress\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Resist\",  " .. sDecisionScript .. ", \"Resist\") ")
			fnCutsceneBlocker()
		end

	--[Farmwork Is Done]
	else

		--Variables.
		local iTimeOfDay = VM_GetVar("Root/Variables/Global/Time/iTimeOfDay", "N")
		
		--If it's morning, time will have advanced to noon when the work is done:
		if(iTimeOfDay == 9.0) then
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: My tasks are complete, mistress.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Very good, thrall.[SOFTBLOCK] Your work resumes after lunch.") ]])
			fnCutsceneBlocker()
		
			--Activate a fade to black.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, gcf_Noon_R, gcf_Noon_G, gcf_Noon_B, gcf_Noon_A, 0, 0, 0, 1, true) ]])
			fnCutsceneWait(180)
			fnCutsceneBlocker()
			
			--Fade to noon, which is clear.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Noon_R, gcf_Noon_G, gcf_Noon_B, gcf_Noon_A, true) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnStandardMajorDialogue()
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Thrall.[SOFTBLOCK] Tend to the little ones.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: As you wish, mistress.") ]])
			fnCutsceneBlocker()
	
			--Generate some problems for the farm.
			fnCutsceneInstruction([[ fnGenerateFarmProblems() ]])
			fnCutsceneBlocker()
			
			--Advance what time it is.
			VM_SetVar("Root/Variables/Global/Time/iTimeOfDay", "N", 12.0)
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", 0)
		
		--If it's noon:
		elseif(iTimeOfDay == 12.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: My tasks are complete, mistress.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Very good, thrall.[SOFTBLOCK] There are more tasks for you.[SOFTBLOCK] Go do them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: I obey.") ]])
			fnCutsceneBlocker()
	
			--Generate some problems for the farm.
			fnCutsceneInstruction([[ fnGenerateFarmProblems() ]])
			fnCutsceneBlocker()
			
			--Advance what time it is.
			VM_SetVar("Root/Variables/Global/Time/iTimeOfDay", "N", 15.0)
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", 0)
		
		--If it's 3 o clock, time will have advanced to sunset:
		elseif(iTimeOfDay == 15.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: I have completed my tasks, mistress.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Perhaps some food is in order.[SOFTBLOCK] Please, dine with me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Your will is my will, mistress.") ]])
			fnCutsceneBlocker()
		
			--Activate a fade to black.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, gcf_Evening_R, gcf_Evening_G, gcf_Evening_B, gcf_Evening_A, 0, 0, 0, 1, true) ]])
			fnCutsceneWait(180)
			fnCutsceneBlocker()
			
			--Fade to night colors.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 135, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnStandardMajorDialogue()
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The sun has set. Thrall, put the tools away, it is time to rest.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes, mistress.") ]])
			fnCutsceneBlocker()
			
			--Advance what time it is.
			VM_SetVar("Root/Variables/Global/Time/iTimeOfDay", "N", 18.0)
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", 0)
			
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPutThingsAway", "N", 1)

		--If it's night, time to go to bed.
		elseif(iTimeOfDay == 18.0) then
		
			--Variables.
			local iHasWater = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasWater", "N")
			local iHasFertilizer = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasFertilizer", "N")
			
			--Needs to put the tools away.
			if(iHasWater > 0 or iHasFertilizer > 0) then
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Thrall, put the tools away.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes, mistress.") ]])
				fnCutsceneBlocker()
		
			--Ending case.
			else
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Let us retire to my chambers, then.[SOFTBLOCK] Thrall, please go ahead.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes, mistress.[SOFTBLOCK] I will await you.") ]])
				fnCutsceneBlocker()
				
				VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPutThingsAway", "N", 0)
				VM_SetVar("Root/Variables/Chapter1/SaltFlats/iAwaitAdina", "N", 1)
			end
		end
	end

--[YesMistress]
--Dialogue ender.
elseif(sTopicName == "YesMistress") then

	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Yes, mistress.[SOFTBLOCK] I obey.") ]])
	fnCutsceneBlocker()

--[Resist]
--This is how you break out of Adina's charms.
elseif(sTopicName == "Resist") then

	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: I -[SOFTBLOCK] I -[SOFTBLOCK] will -[SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: What is it, thrall?[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Acquiesce\", " .. sDecisionScript .. ", \"YesMistress\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Resist\",  " .. sDecisionScript .. ", \"ResistAgain\") ")
	fnCutsceneBlocker()

--[ResistAgain]
--Mei is too swole to control!
elseif(sTopicName == "ResistAgain") then

	--Variables.
	local iDaysPassed        = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N")
	local iTasksDoneTotal    = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iTasksDoneTotal", "N")
	local iMeiRecontrolCount = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iMeiRecontrolCount", "N")

	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Set variables so Mei can leave.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N", 0.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] I...[SOFTBLOCK] will...[SOFTBLOCK] ob...[SOFTBLOCK] ey...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] I...") ]])
	fnCutsceneBlocker()
	
	--Un-whiteout Mei.
	fnCutsceneInstruction([[ fnClearWhiteout() ]])
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	
	--If this is a re-hypnosis:
	if(iMeiRecontrolCount > 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|MC] I -[SOFTBLOCK] am -[SOFTBLOCK][E|Blush] sorry mistress.[SOFTBLOCK] The spell has faded.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Hm.[SOFTBLOCK] The road calls to you again.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yes.[SOFTBLOCK] I suppose I must return to my journey.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I will miss you.[SOFTBLOCK] I will miss...[SOFTBLOCK] this...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Return to me any time.[SOFTBLOCK] I will be here, waiting for you.[SOFTBLOCK] Perhaps next time will be the last.") ]])
	
	--If no days have passed and Mei has done no tasks:
	elseif(iDaysPassed == 0.0 and iTasksDoneTotal == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I -[SOFTBLOCK] will do no such thing![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] What did you do to me!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Ah, it seems you have overcome the effects of the spell.[SOFTBLOCK] I suppose this was inevitable.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: It seems you are stronger willed than most, as it lasted mere moments.[SOFTBLOCK] Impressive.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] You drug me and try to use me for manual labour?[SOFTBLOCK] I don't think so![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Prepare to -[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Please understand, Mei.[SOFTBLOCK] I meant no harm to you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] No harm!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Indeed, in these few moments you were under my control, I merely asked a bit of labour from you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I could well have asked you to slit your own throat, but I did not.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You used hypnotic magic to prove a point?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Yes, I did.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: My understanding of humans is indeed limited.[SOFTBLOCK] I apologize if there was any discomfort.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Well, there wasn't...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I didn't think I needed to point out that we humans don't appreciate being controlled![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Is there anything I can do to compensate you for it?[SOFTBLOCK] I truly am sorry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] N-[SOFTBLOCK]no, I suppose no real harm was done.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And it seems you made your point, if in the weirdest way possible.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Even at your total mercy, you didn't hurt me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Indeed.[SOFTBLOCK] At least I have learned much from you, even if I have failed to earn your trust.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No it's all right.[SOFTBLOCK] I think I understand you a bit better, too.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Just, ask next time you're going to do that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I have much to think about.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei?[SOFTBLOCK] To be honest, I had truly assumed you would rent me in two when you had the opportunity.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Perhaps not all humans are truly dangerous.[SOFTBLOCK] Perhaps some can be reasoned with.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Maybe not all Alraunes are dangerous, either.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Misguided, but not dangerous.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Indeed.[SOFTBLOCK] Thank you, Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: If there is anything I can do to aid you on your journey, do not hesistate to ask.") ]])
	
	--If Mei has done at least one task but no days have passed:
	elseif(iDaysPassed == 0.0 and iTasksDoneTotal > 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I -[SOFTBLOCK] will do no such thing![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] What did you do to me!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Ah, it seems you have overcome the effects of the spell.[SOFTBLOCK] I suppose this was inevitable.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] You drugged me![SOFTBLOCK] And had me do your dirty work![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Drugged?[SOFTBLOCK] Hardly.[SOFTBLOCK] That was a bit of magic, the pollen merely acted as a conduit.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Grrr...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I apologize, but I hope this means you can trust me now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Do you think I'm stupid!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, when you were under my influence, I could have done anything to you.[SOFTBLOCK] I could have ordered you to slash your own throat, and you would complied.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] And?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: And I did no such thing, just as you did not strike me when my back was turned.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] But you were making me do farm work![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: If you would have preferred, I could have had you stand in the field until the magic subsided.[SOFTBLOCK] That would likely have been boring.[SOFTBLOCK] Obviously, I could not have asked you your preference![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: It was not difficult work, was it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well...[SOFTBLOCK] no...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] In fact, I was kind of enjoying it.[SOFTBLOCK] It tingled whenever I obeyed...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Now we have both put ourselves at the mercy of the other and not been destroyed for it.[SOFTBLOCK] Clearly we are creatures of honor.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Honestly, Mei, I truly did expect you to strike me down when you had the chance.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I guess in a way, you really didn't violate my trust.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] And it did feel good, even if I wasn't in control of myself...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: There is no need for further deception, then.[SOFTBLOCK] The point is made.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Okay, Adina, you're on the level.[SOFTBLOCK] I guess.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] But you need to ask before you do something like that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I understand.[SOFTBLOCK] I meant nothing by it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You have given me much to think about, Mei.[SOFTBLOCK] Perhaps not all humans are as dangerous as I had thought.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I will...[SOFTBLOCK] medidate on this.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Could you maybe tell the other Alraunes?.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Already I have sent word through the little ones, but they are as obstinate as I was.[SOFTBLOCK] I doubt they will believe me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Still, you have a friend here if you are in need.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This has been a very bizarre friendship...") ]])
	
	--If less than two days have passed:
	elseif(iDaysPassed < 2.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I -[SOFTBLOCK] will do no such thing![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] What did you do to me!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Ah, it seems you have overcome the effects of the spell.[SOFTBLOCK] I suppose this was inevitable.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] You -[SOFTBLOCK] you -[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] You drugged me and violated me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: There were no drugs present in the pollen.[SOFTBLOCK] I merely used it as a vector for a spell I had woven.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Don't try to hide behind technicalities![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] You forced yourself on me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: When you were under my control, you could well have said no.[SOFTBLOCK] I would even have let you sleep on my bed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: We Alraunes are comfortable on a bed of grass, but none grows down there.[SOFTBLOCK] I would have slept outside.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] B-[SOFTBLOCK]but-[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, when you had the opportunity to lay with me, you did.[SOFTBLOCK] Your inhibitions had been removed, and you chose to.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I am sorry if it seemed any other way.[SOFTBLOCK] I thought I was making you happy.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You're right...[SOFTBLOCK] I'm not normally attracted to women, but you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I apologize.[SOFTBLOCK] We Alraunes do not think of sex the way you humans do.[SOFTBLOCK] I see that now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *sigh*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I am so confused right now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Still, the point I had been trying to make with my spell was that I would not bring harm to you, even were you helpless before me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And...[SOFTBLOCK] you didn't...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Okay, okay.[SOFTBLOCK] I get what you were going for.[SOFTBLOCK] But -[SOFTBLOCK] asking is very important to humans.[SOFTBLOCK] We don't like being manipulated.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] At least, not without our permission.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Perhaps by learning about each other, our races can coexist peacefully.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, you have given me much to consider.[SOFTBLOCK] Please, if there is anything I can do to aid you on your journey, do not hesitate to ask.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Y-[SOFTBLOCK]yeah, sure.[SOFTBLOCK] You've given me a lot to think about, too.") ]])
	
	--If two or more days have passed:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I -[SOFTBLOCK] mistress!?[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Why have you returned my will?[SOFTBLOCK] I only wanted to serve you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I have done nothing to you.[SOFTBLOCK] You have broken the spell of your own accord.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Huh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] You're right, but...[SOFTBLOCK] I want to obey, and yet still be of my own mind...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] I don't know any more![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Please, do not cry.[SOFTBLOCK] You are confused right now.[SOFTBLOCK] Perhaps the spell's effect still lingers?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] No, it's not that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] The spell is gone, but I am driven by something else.[SOFTBLOCK] I still have things I want to do with my life.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Your journey to find your home?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I think so.[SOFTBLOCK] I can't stay here just yet.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I am sorry, mistress, but I must leave.[SOFTBLOCK] Another path calls to me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, this is not at all what I had expected.[SOFTBLOCK] I had merely been trying to prove to you that I could be trusted not to harm you, if I had the chance.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: But it seems you found something you had been looking for in me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] You are someone who loves me and will take care of me.[SOFTBLOCK] Few humans are lucky enough to find someone like you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: And you, in turn, care deeply for the little ones here.[SOFTBLOCK] I can see that in how you tend to them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I merely put my love for you into care for them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Still, I had not expected this.[SOFTBLOCK] Perhaps not all humans are beyond redemption.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Perhaps by learning of one another, Alraune and Human can live in harmony...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I must meditate on this as I care for the little ones.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And I need to find out what it is that drives me...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Truly, Mei, I wish you luck on your journey.[SOFTBLOCK] You are always welcome here should you find your greater place.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah.[SOFTBLOCK] I might take you up on that later...") ]])
		
	end
	fnCutsceneBlocker()
	
	--Remove all farmwork indicators.
	fnCutsceneInstruction([[ fnPlaceProblemIndicators(true) ]])
	fnCutsceneBlocker()

--[Kiss Her]
--Mei wants to kiss her mistress.
elseif(sTopicName == "KissHer") then

	--Clean.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Mistress, your slave wishes to kiss you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mm, come closer.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: *kiss*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall: Mistress...[SOFTBLOCK] Lower...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Ah, well then...") ]])
	fnCutsceneBlocker()
	
	--Scene.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "There, on the salted earth, Mei knelt before her mistress.[SOFTBLOCK] A look of bemusement on her face, Adina drew towards her ensorcelled lover.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With no will of her own, Mei's lust grew.[SOFTBLOCK] Yet, she did not act on it, could not, until he mistress allowed her.[SOFTBLOCK] Each second built Mei's desire higher...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mistress lowered the green guards around her most delicate area, and Mei pressed her lips softly against her mistresses lips.[SOFTBLOCK] She kissed her in total submission.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her task complete, Mei's empty mind stared at the vagina of her mistress.[SOFTBLOCK] She wished to kiss her again, but had not been told to.[SOFTBLOCK] Adina chuckled to herself.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "'Again, Thrall'[SOFTBLOCK] she said.[SOFTBLOCK] Mei complied instantly and without question.[SOFTBLOCK] Each kiss brought her closer to pure mindless bliss.[SOFTBLOCK] Each kiss bonded her closer to her mistress.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She brought her hands up to steady her mistress' legs, and slipped her tongue into Adina's sex.[SOFTBLOCK] A small amount of nectar touched Mei's tongue, but she had not been told to taste it, so she did not.[SOFTBLOCK] She continued the deep kiss.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei kissed her mistress, scarcely aware of anything save pure ecstasy in her own, total, domination.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her task complete, she mindlessly stood in front of her mistress as if nothing had happened.[SOFTBLOCK] It was time to resume her work.") ]])
	fnCutsceneBlocker()

end