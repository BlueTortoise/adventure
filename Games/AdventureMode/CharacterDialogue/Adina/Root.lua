--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--[Variables]
	local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasMetAdina         = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N")
	local iIsMeiControlled     = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--In all cases, mark the combat intro dialogues as complete.
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")
	
	--If Mei is a non-human, or Florentina is present, jump the iHasMetAdina value to 10. The Mind-control sequence is locked off.
	if(sMeiForm ~= "Human" or bIsFlorentinaPresent == true) then
		iHasMetAdina = 10.0
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
	end
	
	--[Flags]
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--[Dialogue Setup]
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	
	--[Mei is Controlled]
	--If this variable is 1, Mei is currently maintaining the farm.
	if(iIsMeiControlled == 1.0) then
		LM_ExecuteScript(fnResolvePath() .. "FarmHandler.lua", "Hello")

	--[First Dialogue]
	--Adina explains what she's doing at the salt flats.
	elseif(iHasMetAdina == 1.0) then

		--Set this variable to 2. This prevents a total repeat.
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 2.0)

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Hello, Mei.[SOFTBLOCK] I see you trust me enough to stand closer.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Don't tease me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You mentioned that if we knew one another better, you may relax your guard.[SOFTBLOCK] Would you like to know what I have tasked myself with?[BLOCK]") ]])
	
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		
		--Decision mode.
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"ListenToStory\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not right now\",  " .. sDecisionScript .. ", \"NotRightNow\") ")
		fnCutsceneBlocker()
	
	--[First Dialogue Repeat]
	--Shorter version of above if Mei blew Adina off.
	elseif(iHasMetAdina == 2.0) then

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Hello, Mei.[SOFTBLOCK] Have you changed your mind?[BLOCK]") ]])
	
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		
		--Decision mode.
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's hear your story\", " .. sDecisionScript .. ", \"ListenToStory\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not right now\",  " .. sDecisionScript .. ", \"NotRightNow\") ")
		fnCutsceneBlocker()
	
	--[Second Dialogue]
	--Adina asks you for a trust test.
	elseif(iHasMetAdina == 5.0) then

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, perhaps I can assuage your fears.[SOFTBLOCK] Will you let me try?[BLOCK]") ]])
	
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		
		--Decision mode.
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Okay\", " .. sDecisionScript .. ", \"TrustTest\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Later\",  " .. sDecisionScript .. ", \"Later\") ")
		fnCutsceneBlocker()
	
	--[Second Dialogue]
	--If you turned down hypnosis, this shows.
	elseif(iHasMetAdina == 7.0) then

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Will you place yourself at my mercy?[SOFTBLOCK] I must demonstrate my faithfulness.[BLOCK]") ]])
	
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		
		--Decision mode.
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Okay\", " .. sDecisionScript .. ", \"Hypnosis\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I don't know...\",  " .. sDecisionScript .. ", \"NoHypnosis\") ")
		fnCutsceneBlocker()
	
	--[Third Dialogue]
	--Adina greets Mei cordially.
	elseif(iHasMetAdina == 10.0) then
	
		--Additional variables.
		local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		local iMeiLovesAdina       = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
		local iFlorentinaMetAdina  = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaMetAdina", "N")
		local sAdinaFirstSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S")
	
		--If the form is "Completed" then Adina knows Mei is a shapeshifter.
		if(sAdinaFirstSceneForm == "Completed" or sAdinaFirstSceneForm == sMeiForm) then
			
			--If Adina did not know Florentina is travelling with Mei:
			if(iFlorentinaMetAdina == 0.0 and bIsFlorentinaPresent == true) then
				VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaMetAdina", "N", 1.0)
				
				--If Mei fell in love with Adina:
				if(iMeiLovesAdina == 1.0) then
					VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaExtendedMistress", "N", 1.0)
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mistress Adina![SOFTBLOCK] How goes the work with the flats?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Freakin' -[SOFTBLOCK] MISTRESS?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] WHAT IN THE WASTES?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Please calm yourself, Florentina.[SOFTBLOCK] Nothing is amiss.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Mei, what's going on here?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] I'm sorry![SOFTBLOCK] I didn't think this would upset you![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Answer me![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I, uh, did some farm work for Adina.[SOFTBLOCK] That's all.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] You are an atrocious liar.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I'm sorry...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Florentina, nothing illicit occurred.[SOFTBLOCK] Mei enjoyed her time here, but felt she had other obligations and set off to see to them.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Nothing pleases me more than seeing her completing that task.[SOFTBLOCK] Doubtless you are involved, and I wish you the best of luck.[SOFTBLOCK] Help her achieve her goals.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Is all this true, Mei?[SOFTBLOCK] Was it just 'farm work' and that's all?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I -[SOFTBLOCK] I...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I love her, Florentina.[SOFTBLOCK] She is my mistress, and I wish I could stay here with her.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] But, I can't, because I still have other things I need to do first.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Okay, okay.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] There's nothing wrong with that.[SOFTBLOCK] I just thought she may have done something to you.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Well...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Love takes many forms.[SOFTBLOCK] If you meet someone you love that much...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Never let them go.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] You mean that?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Absolutely.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Thank you for your understanding.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, thanks, Florentina.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, yeah.[SOFTBLOCK] Whatever.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Are you well, Mei?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I am.[SOFTBLOCK] Florentina is helping me out.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I have no doubt you will be successful, then.[SOFTBLOCK] I can see she has your best interests at heart, no matter what she says.[SOFTBLOCK] Good luck.") ]])
				
				--Normal case:
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Greetings, Mei.[SOFTBLOCK] And tidings to you, Florentina.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] So you're the nut who thinks the salt flats can be saved.[SOFTBLOCK] You've got quite a reputation.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Don't be so negative, Florentina![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I am accustomed to criticism.[SOFTBLOCK] Pay it no mind.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Perhaps your influence will rub off on her, and she will see the world less darkly.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Don't count on it, buckaroo.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: It may take some time, yet...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'll see what I can do.[SOFTBLOCK] Good luck with the salt flats!") ]])
				end
			
			--Otherwise, normal:
			else
			
				--Variables.
				local iAdinaExtendedMistress = VM_GetVar("Root/Variables/Chapter1/Scenes/iAdinaExtendedMistress", "N")
			
				--If Florentina is not around and Mei fell in love with Adina, and this is the first time:
				if(iMeiLovesAdina == 1.0 and bIsFlorentinaPresent == false and iAdinaExtendedMistress == 0.0) then
					VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaExtendedMistress", "N", 1.0)
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Hello, mistress![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Tidings, Mei.[SOFTBLOCK] You realize you need not call me mistress anymore?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But you [SOFTBLOCK]*are*[SOFTBLOCK] my mistress![SOFTBLOCK] I would give anything to serve you mindlessly again![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Heh.[SOFTBLOCK] I would like to be near to you as well, but you are still called by a greater destiny.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: When you see to that, if you still wish it, come see me.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Of course![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Was there anything else I could do for you?[BLOCK]") ]])
		
					--Form.
					local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		
					--Decision mode.
					local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
					fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
					fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Just saying Hi!\", " .. sDecisionScript .. ", \"JustHi\") ")
					if(sMeiForm == "Human" or sMeiForm == "Alraune") then
						fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Enslave me again...\",  " .. sDecisionScript .. ", \"ReHypnosis\") ")
					end
				
				--If Florentina is not around and Mei fell in love with Adina, and this is a repeat case:
				elseif(iMeiLovesAdina == 1.0 and bIsFlorentinaPresent == false and iAdinaExtendedMistress == 1.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Hello, mistress![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Hello, Mei.[SOFTBLOCK] What brings you here?[BLOCK]") ]])
		
					--Form.
					local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		
					--Decision mode.
					local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
					fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
					fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Just saying Hi!\", " .. sDecisionScript .. ", \"JustHi\") ")
					if(sMeiForm == "Human" or sMeiForm == "Alraune") then
						fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Enslave me again...\",  " .. sDecisionScript .. ", \"ReHypnosis\") ")
					end
				
				--All other cases:
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Greetings, Mei.[SOFTBLOCK] I wish you luck on your journey.") ]])
				end
			end
	
		--If Mei is an Alraune and first met Adina as a human:
		elseif(sAdinaFirstSceneForm == "Human" and sMeiForm == "Alraune") then
			VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Alraune")
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Tidings, leaf-sister Mei.[SOFTBLOCK] So you have been joined?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Yes, I have.[SOFTBLOCK] I was wondering what you would think.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: It is a joyous day.[SOFTBLOCK] The world is richer with you as one of us.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It only impresses upon me further the difficulty of your position.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I regret that I cannot stay and assist you with your work here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Your destiny calls to you.[SOFTBLOCK] I understand.[SOFTBLOCK] I hope your journey goes well.") ]])
	
		--If Mei is a bee and first met Adina as a human:
		elseif(sAdinaFirstSceneForm == "Human" and sMeiForm == "Bee") then
			VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Bee")
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Tidings, Mei.[SOFTBLOCK] The little ones whisper that you are different from the other bees.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I don't want to be.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Being unhappy with your position in life is what drives you to succeed.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Besides, no other bee has seen fit to speak with me.[SOFTBLOCK] It is enjoyable.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I guess being different isn't all bad.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Why do you wander so far from your hive?[SOFTBLOCK] I regret there is little nectar to be had here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] My hive has given me an important mission.[SOFTBLOCK] I must find out why I am different from the other bees.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I see.[SOFTBLOCK] I can offer no insight, but I hope you find out.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thanks, Adina.[SOFTBLOCK] I hope I found out, too.") ]])
	
		--If Mei is a slime and first met Adina as a human:
		elseif(sAdinaFirstSceneForm == "Human" and sMeiForm == "Slime") then
			VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Slime")
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I see you return here, Mei, despite your condition.[SOFTBLOCK] Are the rumours true?[SOFTBLOCK] Do you remember yourself?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Rumours?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: So they are![SOFTBLOCK] The plants have long whispered rumours of slimes with wit.[SOFTBLOCK] I did not think it would be you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I like to surprise.[SOFTBLOCK] Have you been well?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: My work is unchanging.[SOFTBLOCK] Your company is appreciated, though.[SOFTBLOCK] Stay as long as you like.") ]])
		
		--If this form is not "Completed" and not the same as the first form:
		elseif(sAdinaFirstSceneForm ~= sMeiForm) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Completed")
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: So it seems the rumours were true.[SOFTBLOCK] You are one who walks between shapes.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's nothing special.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I disagree.[SOFTBLOCK] I consider it an honor that you would come and visit me.[SOFTBLOCK] You must have quite a destiny arrayed before you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Maybe.[SOFTBLOCK] Have you ever met anyone else who can do this?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: No, nor have I heard of any who could.[SOFTBLOCK] An anomaly as befits a destiny like yours.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I wish you best of luck on your journeys.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Thanks.[SOFTBLOCK] Good luck with the salt flats.") ]])
		end
		fnCutsceneBlocker()
	
	end

--"ListenToStory" explains the history of the salt flats and Adina's role in it.
elseif(sTopicName == "ListenToStory") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Set this variable to 5.
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 5.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Let's hear it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Very well.[BLOCK][CLEAR]") ]])
	
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Several centuries ago, this place was the site of a battle between two armies of humans.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The river was not here then, and the forest further back, but the soil was the same.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The reason for the battle has been lost to time.[SOFTBLOCK] Like all humans conflicts, it was pointless and soon forgotten.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: During the battle, it seemed that one army would win quite handily.[SOFTBLOCK] A mage on the other side decided to throw caution to the wind, and use a powerful new spell.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The sky opened up and spit molten fire on his enemies.[SOFTBLOCK] The fire was melted salt, scalding and scarring all those it touched.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The battle was swayed and the mage's side emerged victorious, but at a terrible cost.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The earth here is so salted that nothing has grown for centuries.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] That sounds awful...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Such is the folley of man.[SOFTBLOCK] Nature pays the price for his victories.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But there's grass growing here, isn't there?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: That is what I have tasked myself with.[SOFTBLOCK] I recently discovered several species of plants that can tolerate and even remove extreme saline concentrations.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I have sown these little ones on this field.[SOFTBLOCK] Slowly, life is returning, but it is a great deal of work to maintain.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Now you see what work I have taken up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Your story seems to check out.[SOFTBLOCK] I can see little salt crystals all over the place.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: When it is dry, new crystals form.[SOFTBLOCK] They wash down into the earth when it rains, but there is so much that it would take milennia to fully wash out.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Now that I have told you my story, perhaps you would tell me yours?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, me?[SOFTBLOCK] I'm nothing special.[SOFTBLOCK] I'm...[SOFTBLOCK] trying to find my way back home.[SOFTBLOCK] I'm not from around here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I see.[SOFTBLOCK] Your mannerisms and clothing are quite different from the other humans'.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, I hope you see now that I bore you no ill will.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm still not taking any chances.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Hm.[SOFTBLOCK] Perhaps another exercise...") ]])
	fnCutsceneBlocker()

--"NotRightNow" has Mei make a brief excuse.
elseif(sTopicName == "NotRightNow") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Sorry, but I'm looking for a way home right now.[SOFTBLOCK] Time might be of the essence.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I understand fully.[SOFTBLOCK] I will be here if you change your mind.") ]])
	fnCutsceneBlocker()


--"TrustTest" stars the brief trust-test scene.
elseif(sTopicName == "TrustTest") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 7.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What did you have in mind?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, draw your sword and hold it high.[BLOCK][CLEAR]") ]])
	
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I have my eyes closed, and my back turned.[SOFTBLOCK] Do as you will.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: And here I stand.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What are you trying to prove?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I was completely at your mercy.[SOFTBLOCK] I could have done nothing, should you have chosen to strike me down.[SOFTBLOCK] Yet you did not.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: No matter what you may say now, I can see you are no cold-blooded killer.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] And if you had been wrong?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Then you would have killed me, or perhaps maimed me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Is that supposed to make me trust you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: No, but it serves as a demonstration.[SOFTBLOCK] I have placed full confidence in you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Trust cannot be one-sided, it must always be shared.[SOFTBLOCK] Mei, for you to trust me you must place yourself at my mercy.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hmmm.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Will you?[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Okay\", " .. sDecisionScript .. ", \"Hypnosis\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"NoHypnosis\") ")
	fnCutsceneBlocker()

--"Later" has Mei make an excuse to leave.
elseif(sTopicName == "Later") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll think about it.[SOFTBLOCK] Maybe I should get going.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Take as long as you like.[SOFTBLOCK] My work requires I stay here.") ]])
	fnCutsceneBlocker()

--"Hypnosis" starts the next set of events.
elseif(sTopicName == "Hypnosis") then
	
	--Flip this variable.
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
	
	--Get Mei's position. The cutscene will need it later.
	local fMeiX, fMeiY
	EM_PushEntity("Mei")
		fMeiX, fMeiY = ("Position")
	DL_PopActiveObject()
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This is going way against my better judgement...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: All right, please follow me.[SOFTBLOCK] It is just over here.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Switch off Adina's collision flag.
	fnCutsceneInstruction([[ 
		EM_PushEntity("Adina")
			TA_SetProperty("Clipping Flag", false)
		DL_PopActiveObject()
		]])
	fnCutsceneBlocker()
	
	--Adina walks south.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Adina")
		ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (19.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Adina/Mei walks south.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Adina")
		ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (21.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (36.25 * gciSizePerTile), (20.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Adina faces east. Mei faces south. Should already be the case for Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Adina")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneInstruction([[ fnPartyStopMovement() ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Switch on Adina's collision flag.
	fnCutsceneInstruction([[ 
		EM_PushEntity("Adina")
			TA_SetProperty("Clipping Flag", true)
		DL_PopActiveObject()
		]])
	fnCutsceneBlocker()
	
	--Dialogue.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Here it is.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, please smell this flower.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's not poisonous, is it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I will say nothing.[SOFTBLOCK] You must trust me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Oh geez...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Switch Mei to the crouching pose.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Mei sniffs the flower. Doesn't take part as a major dialogue.
	fnCutsceneInstruction([[ fnStandardDialogue("*sniff*") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well that wasn't...[SOFTBLOCK] so...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Oh I'm lightheaded... [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] What did...[SOFTBLOCK] you...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] So hard...[SOFTBLOCK] to think...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Please, smell it again.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Y-[SOFTBLOCK]yeah...[SOFTBLOCK] It smells...[SOFTBLOCK] so sweet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*sniff*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: How was it you said you feel?") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ fnSetMeiToWhiteout() ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "CrouchMC")
	DL_PopActiveObject()
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Obedient.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] I exist to serve you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Command me, my mistress.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: What a response![SOFTBLOCK] I was not expecting this![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Perhaps the vector amplified the potency?[SOFTBLOCK] No matter.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Thrall, you are charged with maintaining the plants on these salt flats.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Yes mistress.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The tools you'll need are in my tool shed.[SOFTBLOCK] If you need pollen, report to me and I'll give you what you need.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Yes mistress.[SOFTBLOCK] It will be done.") ]])
	fnCutsceneBlocker()
	
	--Wait a bit. Remove crouch forcing.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	fnCutsceneInstruction([[ AL_SetProperty("Music", "AdinasTheme") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Generate some problems for the farm.
	fnCutsceneInstruction([[ fnGenerateFarmProblems() ]])
	fnCutsceneBlocker()

--"NoHypnosis" has Mei make an excuse to leave.
elseif(sTopicName == "NoHypnosis") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not ready yet.[SOFTBLOCK] It's just too risky.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Rapproachement takes time.[SOFTBLOCK] I am patient.") ]])
	fnCutsceneBlocker()

--If Mei is "just saying hi" during a visit:
elseif(sTopicName == "JustHi") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, I just thought I'd come see you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: It pleases me to see you in such good spirits.[SOFTBLOCK] I hope you find you way in the world.") ]])
	fnCutsceneBlocker()

--If Mei wants to be re-hypnotised:
elseif(sTopicName == "ReHypnosis") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N", 1.0)
	local iMeiRecontrolCount = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iMeiRecontrolCount", "N")
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iMeiRecontrolCount", "N", iMeiRecontrolCount + 1)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	
	--First time going for this:
	if(iMeiRecontrolCount == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Mistress...[SOFTBLOCK] could you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You wish for me to enslave you again.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Just for a little while...[SOFTBLOCK] I want to be with you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Can your quest wait?[SOFTBLOCK] Are there others counting on you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] A few days wouldn't hurt.[SOFTBLOCK] When the magic wears off, I'll leave.[SOFTBLOCK] I promise.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: And if it doesn't?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Then I will obey you until the end of time, mistress.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I don't think it will last that long![SOFTBLOCK] But, I can see you are sincere.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: However, I did review my methods.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Did you learn something?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Yes.[SOFTBLOCK] The spell has scarcely an effect on an unwilling target.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I could hardly charm a fieldmouse.[SOFTBLOCK] I know, because I tried.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] So that means...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You were always my mistress, before we even met...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Perhaps.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: At the very least, you wanted it deeply, perhaps unconsciously.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I want it now, so badly.[SOFTBLOCK] Please, mistress, I beg you![BLOCK][CLEAR]") ]])
		
		--Alraune:
		if(sMeiForm == "Alraune") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I fear I may not be able to.[SOFTBLOCK] As one of my kind, you may be resistant to the spell.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] The spell is merely a formality.[SOFTBLOCK] I am already your slave.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Please, mistress.[SOFTBLOCK] Take your thrall's body.[SOFTBLOCK] Take it...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Again.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Take me...[SOFTBLOCK] Take...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Please...[SOFTBLOCK] Enslave...[SOFTBLOCK] Please...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I'm sorry, I didn't hear you.") ]])
			fnCutsceneBlocker()
			
		--All other cases.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Oh, you do?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Yes, please![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Again.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Please, enslave me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] En...[SOFTBLOCK]slave...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Pl-[SOFTBLOCK]please...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I'm sorry, I didn't hear you.") ]])
			fnCutsceneBlocker()
		end
	
		fnCutsceneInstruction([[ fnSetMeiToWhiteout() ]])
		fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Pl...[SOFTBLOCK]ease...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] It is done.[SOFTBLOCK] I am yours, body and soul.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Command me, mistress.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: So you no longer even need a vector.[SOFTBLOCK] The magic was enough.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Yes, mistress.[SOFTBLOCK] I am your thrall.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] I now see that, even when I am free willed, I am still your thrall.[SOFTBLOCK] Freedom is an illusion, a slave is what I am.[SOFTBLOCK] Forever.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You have come to this conclusion on your own?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Yes, mistress.[SOFTBLOCK] If you order me to, I will forget it.[SOFTBLOCK] I will obey.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: No, it is fine the way it is.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Now, enough chatter.[SOFTBLOCK] We must tend the little ones.[SOFTBLOCK] Thrall, go.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Yes, mistress.[SOFTBLOCK] I obey.") ]])
	
	--Any other time:
	else
		local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Mistress, my freedom weighs on me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Would you like me to take it away?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Would you?[SOFTBLOCK] I can't bear being away from you.[BLOCK][CLEAR]") ]])
		
		if(sMeiForm == "Alraune") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Then I will weave my spell...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Mistress' magic seems to have no effect on this leafy body...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (But...[SOFTBLOCK] I was always a slave.[SOFTBLOCK] Becoming a leaf-sister shouldn't change that...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I...[SOFTBLOCK] I will not fail you, mistress.[SOFTBLOCK] I have no will.[SOFTBLOCK] I am mindless.[SOFTBLOCK] I am your thrall.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I am a mindless thrall.[SOFTBLOCK] I am a mindless thrall...[SOFTBLOCK] I am a thrall...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I cannot take from you what you have never had, slave.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You have always been my thrall, and always will be.") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: How can I take away what you never had?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You have always been my thrall, and always will be.") ]])
			
		end
		fnCutsceneBlocker()
	
		fnCutsceneInstruction([[ fnSetMeiToWhiteout() ]])
		fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Isn't that right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Yes, mistress.[SOFTBLOCK] I was never free.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Now, tend to the fields.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[E|MC] Yes, mistress.[SOFTBLOCK] My body exists to serve you.") ]])
	
	end
	fnCutsceneBlocker()
	
	--Generate some problems for the farm.
	fnCutsceneInstruction([[ fnGenerateFarmProblems() ]])
	fnCutsceneBlocker()

end