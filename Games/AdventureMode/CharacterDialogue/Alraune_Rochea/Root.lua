--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.
-- Haha, Alraune! Root! Plant comedy!

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Hello]
--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--[System]
	--Make the NPC face who is talking to them.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue. Rochea uses the stock Alraune portraits.
	WD_SetProperty("Show")
	WD_SetProperty("Major Sequence", true)
	WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral")
	WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral")
	
	--Say a simple greeting and go to topics mode.
	WD_SetProperty("Append", "Rochea: Greetings, leaf-sister Mei.[SOFTBLOCK] What may I help you with?[BLOCK][CLEAR]")

	--[Topics]
	--Activate topics mode once the dialogue is complete.
	WD_SetProperty("Activate Topics After Dialogue", "Rochea")

end