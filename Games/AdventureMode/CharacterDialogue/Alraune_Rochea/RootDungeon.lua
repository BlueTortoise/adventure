--[Root]
--Dialogue when Rochea is in the Trap Dungeon.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Hello]
--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--[System]
	--Make the NPC face who is talking to them.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--[Dialogue]
	--Setup.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: We have cleared the garrison here, but more remain further inside.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Worse, it seems they have erected some sort of mechanical blockade.[SOFTBLOCK] We cannot pass it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: I just wanted to tell you both, good luck.[SOFTBLOCK] We're all counting on you.") ]])

end