--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Make the NPC face who is talking to them.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	
	--If the player has not completed the dungeon:
	if(iCompletedTrapDungeon == 0.0) then
		fnStandardDialogue("Alraune:[VOICE|Alraune] We will prevent the cultists from pincering you.[SOFTBLOCK] Please, leaf-sister, hurry!")
	
	--Line changes after completing the dungeon.
	else
		fnStandardDialogue("Alraune:[VOICE|Alraune] The defilers have felt this blow, but they will rally, yet...")
	end
end