--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local iHasAnnoyedVendor = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasAnnoyedVendor", "N")
	
	--First case:
	if(iHasAnnoyedVendor == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasAnnoyedVendor", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[VOICE|MercF] Well, hello there.[SOFTBLOCK] You look like a discerning individual.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] What do you have for sale?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[VOICE|MercF] Me?[SOFTBLOCK] Oh, a bit of this, a bit of that.[SOFTBLOCK] But, just for you, I have something very special.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Oh joy.[SOFTBLOCK] A ridiculously obvious scam.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] What is it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[VOICE|MercF] This is an Amulet of Annihilation.[SOFTBLOCK] A very rare antique, normally worth millions of platina.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[VOICE|MercF] Since I like the look of you, it's yours for just ten thousand platina.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] Oh, only ten thousand?[SOFTBLOCK] If I paid less than a hundred thousand I'd assume it's defective.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[VOICE|MercF] D-[SOFTBLOCK]defective?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] Yeah.[SOFTBLOCK] If it's broken then I don't want it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[VOICE|MercF] You really want this piece of c[SOFTBLOCK]r[SOFTBLOCK]a[SOFTBLOCK][SOFTBLOCK]ss jewelry?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] Maybe.[SOFTBLOCK] I'd need to take it for a test run first, to make sure it's in working order.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[VOICE|MercF] Uh, I'm not sure I can do that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] Aren't you a reputable Amulet of Annihilation vendor?[SOFTBLOCK] Where I'm from, we have a registry for that.[SOFTBLOCK] What's your serial number?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[VOICE|MercF] Wait, sorry, I think I'm all sold out.[SOFTBLOCK] Yep, this one is reserved...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] Oh, shame.") ]])
	
	--Repeat case:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] Got any spare amulets yet?[SOFTBLOCK] They're a steal at just ten thousand.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[VOICE|MercF] I -[SOFTBLOCK] I don't know what you're talking about.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Hehe)") ]])
	
	end
	fnCutsceneBlocker()
end
