--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasMetAquillia = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasMetAquillia", "N")
	local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	
	--Aquillia has not revealed herself yet.
	if(iHasMetAquillia == 0.0) then
	
		--[Setup]
		--Constants.
		local ciRotationTicks = 5
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetAquillia", "N", 1.0)
		if(iHasSeenTrannadarFlorentinaScene == 1.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N", 1.0)
		end
	
		--[Dialogue]
		--Aquillia takes slot 5, dressed as the prisoner.
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Cultist") ]])
	
		--If Mei is an Alraune during this conversation:
		if(sMeiForm == "Alraune") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Hey, you made it![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: ...[SOFTBLOCK] and apparently had a bit of a run-in with the plant girls, too.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Er, do I know you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: You don't recognize me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Oh right...") ]])
		
		--If Mei is a Bee during the conversation:
		elseif(sMeiForm == "Bee") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Hey, you made it![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: ...[SOFTBLOCK] and apparently had a bit of a run-in with the bee girls, too.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I don't mind.[SOFTBLOCK] Best thing that ever happened to me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: And you can talk, too?[SOFTBLOCK] Oh, Hirundo is going to get a real kick out of this...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Excuse me for being rude, but do I know you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: You don't recognize me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Oh right...") ]])
		
		--If Mei is a Slime during the conversation:
		elseif(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Hey, you made it![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Or maybe you just look like her...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: You -[SOFTBLOCK] you just keep your distance, slime...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Don't be afraid, I'm not looking to eat you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: You can talk?[SOFTBLOCK] That's actually really rad to the max.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Rad to the max?[SOFTBLOCK] Who is this person?)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do I know you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Maybe, maybe not.[SOFTBLOCK] You look a lot like someone who helped me out of a bind.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Perhaps this will jog your memory.") ]])
		
		--Human/unhandled.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Hey, you made it![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] A cultist?[SOFTBLOCK] Out here?[SOFTBLOCK] I thought I was done with you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Hey, you don't recognize me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Oh right...") ]])
		end
		fnCutsceneBlocker()
	
		--[Movement]
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Aquillia spins around.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks * 2)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 0, -1)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks * 2)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 1, 0)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks * 2)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 0, 1)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks * 2)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 0, -1)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 1, 0)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 0, 1)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks)
		fnCutsceneBlocker()
		
		--Aquillia switches sprites.
		fnCutsceneInstruction([[ EM_PushEntity("Aquillia")
			fnSetCharacterGraphics("Root/Images/Sprites/Aquillia/", false)
		DL_PopActiveObject() ]])
		
		--KEEP SPINNING!
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 0, -1)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 1, 0)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 0, 1)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks * 2)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 0, -1)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks * 2)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 1, 0)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks * 2)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Aquillia")
			ActorEvent_SetProperty("Face", 0, 1)
		DL_PopActiveObject()
		fnCutsceneWait(ciRotationTicks * 2)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Face Mei again.
		fnCutsceneInstruction([[ EM_PushEntity("Aquillia")
			TA_SetProperty("Face Character", "PlayerEntity")
		DL_PopActiveObject() ]])
		fnCutsceneBlocker()
	
		--[Dialogue]
		--Start up.
		fnStandardMajorDialogue()
		
		--Aquillia takes slot 4, this time in her proper portrait.
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Ta-[SOFTBLOCK]freakin-[SOFTBLOCK]dah,[SOFTBLOCK] baby.[SOFTBLOCK] Do I look good or what?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Oh my gosh, I'm so sorry I didn't recognize you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're okay?[SOFTBLOCK] Have you seen a doctor about that arm?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Oh, this?[SOFTBLOCK] Yeah I got it patched up.[SOFTBLOCK] I'll be fine in no time.[BLOCK][CLEAR]") ]])
		
		--If Florentina is present:
		if(iHasSeenTrannadarFlorentinaScene == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Really?[SOFTBLOCK] Who patched it up?[SOFTBLOCK] I didn't see you at the trading post...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: A friend of mine.[SOFTBLOCK] She's good at the whole \"Helping the sick\" gig.[BLOCK][CLEAR]") ]])
		end
		
		--If Mei is a Bee during the conversation:
		if(sMeiForm == "Bee") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I could get you some royal jelly to help speed it up...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Hah![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Thanks for the offer, but I think I'll be fine.[BLOCK][CLEAR]") ]])
		
		--If Mei is a Slime during the conversation:
		elseif(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Is there anything I can do to help?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Er, I'd rather not get slime on any of my bruises.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Besides, you kind of already saved my life.[SOFTBLOCK] I should be asking what I can do for you![BLOCK][CLEAR]") ]])
		
		--Human/unhandled.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Well, I wish you a speedy recovery![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Well, thanks for that.[SOFTBLOCK] And the rescue, too.[BLOCK][CLEAR]") ]])
		end
		
		--Conversation continues.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh![SOFTBLOCK] Where are my manners?[SOFTBLOCK] My name is Mei.[SOFTBLOCK] I don't think I introduced myself back in that dingy basement.[BLOCK][CLEAR]") ]])
		if(iHasSeenTrannadarFlorentinaScene == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This is my friend, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Friend?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Associate?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Business partner.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] That works...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You two get along so well.[BLOCK][CLEAR]") ]])
		end
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Aquillia Whitebride, at your service.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Say, Aquillia, you didn't happen to see how I...[SOFTBLOCK] got into that prison, did you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: No, no I didn't.[SOFTBLOCK] Figured you got chucked in there like me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What did they want with you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I was snooping around their place for, shall we say, reasons of my own.[SOFTBLOCK] They didn't like that one bit.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I may also have gouged an eye out of one of their lackeys.[BLOCK][CLEAR]") ]])
		if(iHasSeenTrannadarFlorentinaScene == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] *growl*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: What?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] Oh, nothing.[SOFTBLOCK] Nothing at all.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Hey, I paid a price for it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I can tell by the bruises.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Mostly administered by a half-blind and very angry initiate.[BLOCK][CLEAR]") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I like you a lot already.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: It cost me.[SOFTBLOCK] They beat me up pretty bad, as you saw.[BLOCK][CLEAR]") ]])
		end
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Kept demanding I join up with them and acting all surprised when I said no.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] What did they expect?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Dunno, but I did see the same treatment get extended to others when I was spying on them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Difference is that it seemed to work on them.[SOFTBLOCK] Eerie, the way they acted.[BLOCK][CLEAR]") ]])
		if(iHasSeenTrannadarFlorentinaScene == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You should probably tell Captain Blythe what you know.[SOFTBLOCK] He'll want whatever info he can get.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Tch, tell the mercs?[SOFTBLOCK] They're probably bought off already.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: And if they're not, they could be.[SOFTBLOCK] I'm not putting my trust in them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Paranoid?[SOFTBLOCK] I like that.[BLOCK][CLEAR]") ]])
		end
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] So is this your cabin?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Me?[SOFTBLOCK] No.[SOFTBLOCK] This belongs to a friend of mine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I've gotta wait for my arm to heal up before I start on getting a bit of sweet, sweet revenge.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] After what happened last time?[SOFTBLOCK] Please, don't take any risks.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I know what they're up to.[SOFTBLOCK] I know how they work now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] But they'll kill you if they catch you again![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You're way too sweet for someone who's good with a sword.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You haven't even known me for a day and already you're acting like we're old chums.[SOFTBLOCK] Touching.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] I just -[SOFTBLOCK] promise you'll be extra careful.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Yeah okay, *mom*.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Heh, all right all right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: What about you, Mei?[SOFTBLOCK] Want to get some delicious vengeance?[BLOCK][CLEAR]") ]])
		if(iHasSeenTrannadarFlorentinaScene == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] We're currently trying to find her a way back home.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Forgive and forget, huh?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] When you set out for revenge, first, dig two graves.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's a saying where I'm from.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You're a far better person than I, Mei. I hope you find your way.") ]])
		else
			
			--Alraune form.
			if(sMeiForm == "Alraune") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Revenge is...[SOFTBLOCK] not the Alraune way.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Pah![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Even if it was, everyone back home is probably worried about me.[SOFTBLOCK] I have to let them know I'm okay.[SOFTBLOCK] That takes priority.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: That, I can't fault.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I hope you find your way back home.") ]])
			
			--If Mei is a Bee during the conversation:
			elseif(sMeiForm == "Bee") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The hive has given me a much more important mission.[SOFTBLOCK] Besides, I need to find a way back home.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I need to at least tell my parents that I'm okay.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You're thinking of them even at a time like this?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] They'll worry.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: ...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I have a lot of respect for that, Mei.[SOFTBLOCK] Good luck.") ]])
			
			--If Mei is a Slime during the conversation:
			elseif(sMeiForm == "Slime") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I need to go back home.[SOFTBLOCK] Let my parents know I'm okay.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I think you're being a slime will have an impact on that.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] A phone call will have to do, in that case.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: A what?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's -[SOFTBLOCK] oh nevermind.[SOFTBLOCK] The point is, I can't let them worry about me.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: ...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I have a lot of respect for that, Mei.[SOFTBLOCK] Good luck.") ]])
			
			--Human/unhandled.
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I need to get home.[SOFTBLOCK] My parents are probably worried sick.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You're thinking of them even at a time like this?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I have obligations and I intend to honor them.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You're a far better person than I, Mei.[SOFTBLOCK] I hope you find your way.") ]])
			end
		end
		
		--Common.
		fnCutsceneBlocker()
		
	--Normal dialogue.
	else
	
		--Common.
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Neutral") ]])
		
		--Variables.
		local iHasFlorentinaMetAquillia = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N")
	
		--Florentina is present, but has not met Aquillia.
		if(iHasSeenTrannadarFlorentinaScene == 1.0 and iHasFlorentinaMetAquillia == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Well what do we have here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] And just what kind of question is that?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You smell like money and perfume.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Get used to it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Is she a friend of yours, Mei?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This is Florentina.[SOFTBLOCK] Florentina, meet Aquillia.[SOFTBLOCK] We broke out of cultist prison together.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Cultist prison?[SOFTBLOCK] That explains the excitement earlier.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] All the plants started jabbering about a bunch of very angry people fanning out from the old mansion.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I guess they noticed we slipped out.[SOFTBLOCK] Took them long enough.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Wish I had time to leave them a fulminating caustic surprise, but I'm not much good with my arm like this.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Florentina promised to help me find a way back home.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Good luck with that.[SOFTBLOCK] Sorry I can't do much more than wish you well.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Knowing you're okay is enough.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Yeah, sure.[SOFTBLOCK] You sap.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I think I like you, Aquillia.") ]])
			
		--Florentina is present and has met Aquillia.
		elseif(iHasSeenTrannadarFlorentinaScene == 1.0 and iHasFlorentinaMetAquillia == 1.0) then
		
			--If Mei happens to need some Kokayanee...
			local iCokeCount = AdInv_GetProperty("Item Count", "Kokayanee")
			local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
			if(iTakenPieJob == 1.0 and iCokeCount < 1) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Aquillia, please don't take this the wrong way, but...[SOFTBLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Mei needs to get high.[SOFTBLOCK] You cool?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Word.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] No, I - [SOFTBLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: S'the least I can do for you.[SOFTBLOCK] What do you like?[SOFTBLOCK] Pixie-sticks?[SOFTBLOCK] LDS? Bike-Chain?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Kokayanee, actually.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Nerd.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I want to start her off slow.[SOFTBLOCK] She's a neophyte in the light.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You're still a nerd, but Florentina here is cool.[SOFTBLOCK] Here ya go.[SOFTBLOCK] First hit is free.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Got Kokayanee*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Well, thanks, Aquillia![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: I'm kidding, I'm not a dealer.[SOFTBLOCK] But I know some people who know some people.[SOFTBLOCK] If you want any of the good stuff, you talk to me.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] This will do for now.[SOFTBLOCK] I'll tell you how she takes it.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Natch.[SOFTBLOCK] Enjoy talking to the clouds.") ]])
		
				--Add the item.
				LM_ExecuteScript(gsItemListing, "Kokayanee")
		
				--Reset flag.
				WD_SetProperty("Clear Topic Read", "Pepper Pie")
		
			--Normal case:
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Waiting is the freakin' worst.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: It'll all gonna be worth it when I get my mitts on those taffing cultists...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Violence as a first resort?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Aquillia...[SOFTBLOCK] Will you be my new best friend?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: ...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: You know, I thought today was going pretty badly.[SOFTBLOCK] But now?[SOFTBLOCK] I got a new best friend.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (This is so ghastly...)") ]])
			end
		
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: If you change your mind, I'm not in a rush here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: My friend said it'll still be at least a week before my arm heals.[SOFTBLOCK] Arcane medicine ain't what it used to be.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Get better first.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Aquillia: Like I've got a choice.") ]])
		end
		
		--Common.
		fnCutsceneBlocker()
	end
end
