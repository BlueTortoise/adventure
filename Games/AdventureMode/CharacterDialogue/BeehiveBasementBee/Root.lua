--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

	--Variables.
	local sForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasSeenBeePanicScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeePanicScene", "N")

	--If Mei is not in bee mode:
	if(sForm ~= "Bee") then
		
		--Before Florentina has calmed them:
		if(iHasSeenBeePanicScene < 2.0) then
			fnStandardDialogue("(This bee looks absolutely terrified...)")
		
		--After:
		else
			fnStandardDialogue("(While the bee seems calm, fear is still evident on her face...)")
		end
	
	--In bee mode, the dialogue changes.
	else
		
		--Before Florentina has calmed them:
		if(iHasSeenBeePanicScene < 2.0) then
			fnStandardDialogue("(Your drone sister is deeply fearful, but is not speaking with you...)")
		
		--After:
		else
			fnStandardDialogue("(While calmer than before, your drone sister is still deeply fearful...)")
		end
	end

end