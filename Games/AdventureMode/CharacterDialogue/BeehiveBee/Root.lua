--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

	--Variables.
	local sForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
	local iIsDuringBeeTransform = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")

	--If Mei is not in bee mode, the bee is just ignoring her.
	if(sForm ~= "Bee") then
		
		--If Mei has the bee transformation.
		if(iHasBeeForm == 1.0) then
			fnStandardDialogue("Your bee sister is uninterested in you for the moment.")
		
		--If not during the bee transformation:
		elseif(iIsDuringBeeTransform == 0.0) then
			fnStandardDialogue("This bee seems to be ignoring you.[SOFTBLOCK] It seems to be too busy to view you as a threat.")
		
		--During the bee transformation.
		else
			fnStandardDialogue("This bee seems to be ignoring you.")
		end
	
	--In bee mode, the dialogue changes.
	else
		fnStandardDialogue("There is no need to speak to this bee, you can already hear her thoughts.")
	end

end