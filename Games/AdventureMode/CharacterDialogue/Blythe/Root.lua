--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Nadia takes slot 5.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Blythe", "Neutral") ]])
	
	--Standard dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: May I help you with something?[BLOCK][CLEAR]") ]])

	--[Topics]
	--Activate topics mode once the dialogue is complete.
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Blythe") ]])
end
