--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--[Common]
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--[Variables]
	local bIsFlorentinaPresent        = AL_GetProperty("Is Character Following", "Florentina")
	local sMeiForm                    = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local sMeiFirstForm               = VM_GetVar("Root/Variables/Chapter1/Breanne/sMeiFirstForm", "S")
	local iHasSeenMeiNewForm          = VM_GetVar("Root/Variables/Chapter1/Breanne/iHasSeenMeiNewForm", "N")
	local iMetMeiWithFlorentina       = VM_GetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N")
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local iGotWerecatCollar           = VM_GetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N")
	
	--Variables that might cause Breanne to start the Flower Quest.
	local iTalkedJob     = VM_GetVar("Root/Variables/Chapter1/Breanne/iTalkedJob", "N")
	local iTakenPieJob   = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
	local iTalkedParents = VM_GetVar("Root/Variables/Chapter1/Breanne/iTalkedParents", "N")
	AdvCombat_SetProperty("Push Party Member", "Mei")
		local sMeiLevel = AdvCombatEntity_GetProperty("Level")
	DL_PopActiveObject()
	
	--[Talking]
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Breanne takes slot 5.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Breanne", "Neutral") ]])
	
	--If Mei is not in the first form Breanne saw her in:
	if(sMeiFirstForm ~= sMeiForm and iHasSeenMeiNewForm == 0.0) then
	
		--Flags.
		VM_SetVar("Root/Variables/Chapter1/Breanne/iHasSeenMeiNewForm", "N", 1.0)
	
		--If Nadia knows about Mei's shapeshifting properties...
		if(iHasSeenTrannadarThirdScene == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well hello, Mei.[SOFTBLOCK] You're looking different than normal.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Wait, I can explain.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: No need for that, hun.[SOFTBLOCK] Nadia was here earlier, told me everything.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Crazy magic shapeshifter, eh?[SOFTBLOCK] Quite a story, but the evidence is right in front of me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Word travels fast around here, doesn't it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: With Nadia, sure.[SOFTBLOCK] She's ten gossips in a five stone bag.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Not that I mind![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: All right, then.[SOFTBLOCK] As long as you're okay with it.[BLOCK][CLEAR]") ]])
			
			if(sMeiForm == "Werecat" and iGotWerecatCollar == 0.0) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh![SOFTBLOCK] Oh![SOFTBLOCK] Here, this would look great on you![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] A collar?[SOFTBLOCK] I am not a simple housecat, Breanne![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Are you sure?[SOFTBLOCK] Look closer![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] [SOUND|World|TakeItem](Received Moonglow Collar)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] This...[SOFTBLOCK] It glows like the moon![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I think they're called...[SOFTBLOCK] Moonglow Stones?[SOFTBLOCK] They're supposed to capture moonlight at night and release it during the day.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I found it in the forest a while back.[SOFTBLOCK] I think it suits you![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] It's beautiful![SOFTBLOCK] Thank you, Breannepurrr![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ... [SOFTBLOCK]Sorry about getting upset earlier.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Aww, that's okay.[SOFTBLOCK] No harm done.[SOFTBLOCK] Take good care of that, now![BLOCK][CLEAR]") ]])
				VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
				LM_ExecuteScript(gsItemListing, "Moonglow Collar")
			end
		
		--If Florentina is present:
		elseif(bIsFlorentinaPresent == true) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N", 1.0)
			
			--If Mei was originally a human:
			if(sMeiFirstForm == "Human") then
				
				--If Mei is a slime:
				if(sMeiForm == "Slime") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, no.[SOFTBLOCK] Mei...[SOFTBLOCK] I'm so sorry...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Breanne, I can explain...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, you can speak?[SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Don't look at me.[SOFTBLOCK] I didn't think she had vocal chords.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well golly, what a relief![SOFTBLOCK] I had thought I lost a friend.[SOFTBLOCK] Don't spook me like that again![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: It's my runestone.[SOFTBLOCK] It lets me stay, well, me.[SOFTBLOCK] I can even turn back into a human if I want to.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Now that there is a special rock.[SOFTBLOCK] You hang on to that.[SOFTBLOCK] World needs more girls like you, and it certainly doesn't need more slimes.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: That said, you look pretty good for mass of goo.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: I think she likes you, Mei.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Don't ya go readin' too far into that![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Now then, what can I do for you?") ]])
				
				--If Mei is an Alraune:
				elseif(sMeiForm == "Alraune") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mei![SOFTBLOCK] You're -[SOFTBLOCK] Florentina, what have you gone and done?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Why do you always blame me first?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You no good, dirty - [SOFTBLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: It's not her fault, honest![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Besides, I don't mind this.[SOFTBLOCK] Not one bit.[SOFTBLOCK] It feels...[SOFTBLOCK] liberating...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You promise it wasn't her?[SOFTBLOCK] She's a tricky one.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Absolutely not![SOFTBLOCK] You ought to know better![SOFTBLOCK] If I've made one thing clear, it's that I don't associate with Rochea and her ilk.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: ...[SOFTBLOCK] All right, all right.[SOFTBLOCK] I'm sorry to have accused you.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Besides, it's not permanent.[SOFTBLOCK] This runestone lets me change form if I feel like it.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: It does?[SOFTBLOCK] That's awful handy.[SOFTBLOCK] Seems there's more to you than meets the eye.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Does that mean you feel like being a plant girl?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I love it.[SOFTBLOCK] Could I possibly convince you to be joined?[SOFTBLOCK] The sensation is incomparable...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'm afraid that'd be a one-way trip for me, dearie, and I'll not be making it any time soon.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Unless you think that stone of yours might work on me?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: It won't.[SOFTBLOCK] I'm not sure how I know that, but I do.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'm quite happy as I am, but thank you for the offer.[SOFTBLOCK] Now what can I do for you?[BLOCK][CLEAR]") ]])
	
				--If Mei is a bee:
				elseif(sMeiForm == "Bee") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Dearest me, Mei.[SOFTBLOCK] Did you pick a fight with the bees?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: What happened, Florentina?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: It's not what it looks like.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You can speak?[SOFTBLOCK] Are you still you?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: In some ways.[SOFTBLOCK] In others, less-so.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: It's quite a trick, that.[SOFTBLOCK] I didn't believe it myself at first.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: It has to do with my runestone, but I don't know why.[SOFTBLOCK] It lets me change form, and remember who I am.[SOFTBLOCK] For better or worse...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I've had some friends go that way.[SOFTBLOCK] I reckon they'd want that runestone to work a treat on them too.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: ...[SOFTBLOCK] Yes?[SOFTBLOCK] I see.[SOFTBLOCK] I'll tell her.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: She's talking to the hive.[SOFTBLOCK] She gets like that sometimes.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: They said they remember you, Breanne.[SOFTBLOCK] They said not to worry, and that they're very happy.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, Joanie...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Joanie?[SOFTBLOCK] You mean -[SOFTBLOCK] well, we don't normally have names.[SOFTBLOCK] Yes, she's here.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'm about to cry.[SOFTBLOCK] Look away, please.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: She's very happy as a bee.[SOFTBLOCK] You have nothing to worry about.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: *sniff*[SOFTBLOCK] You mean that?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: We have no reason to lie.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Isn't this a tearful reunion-by-proxy.[SOFTBLOCK] Mei, don't we have a job to do here?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Don't be so callous.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh Joanie, I'm so glad you're okay.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: *sniff*[SOFTBLOCK] No no, gotta be strong.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Let's talk about something else, okay?[SOFTBLOCK] But you tell Joanie that if she wants to come see me...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: She already knows.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Thanks so much, Mei.[SOFTBLOCK] Thank you.[SOFTBLOCK] Really...[BLOCK][CLEAR]") ]])
					
					--Flag.
					VM_SetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N", 1.0)
	
				--If Mei is a werecat:
				elseif(sMeiForm == "Werecat") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh my, Mei![SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] What does it look like?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh dear.[SOFTBLOCK] The cure is so easy to mix up, but I'm afraid it won't have any effect once the transformation is complete.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Cure?[SOFTBLOCK] Hss![SOFTBLOCK] No thanks![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I've never felt better![SOFTBLOCK] The moon is my eternal companion![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You should come hunt with me, Breanne.[SOFTBLOCK] You'd love the speed, the rush of the kill...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Hah![SOFTBLOCK] Thanks for the offer, but I'm all right the way I am.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I need strong pridemates.[SOFTBLOCK] If you change your mind...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I'm a pridemate, am I?[SOFTBLOCK] Yeah, Breanne, you should get your hammer and tag along![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Aww, you're really flattering me![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Worth a shot.[SOFTBLOCK] Hey, you wouldn't have any chicken in the pantry, would you?[SOFTBLOCK] I'm starved![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Didn't you just say you're a hunter?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We hunt mostly for sport, for food only when necessary.[SOFTBLOCK] But, I'm so busy looking for a way back to Earth...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'll give you some chicken if you're willing to curl up on my lap and purr.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Don't patronize me![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Hmm... how about this?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] [SOUND|World|TakeItem](Received Moonglow Collar)[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] This...[SOFTBLOCK] It glows like the moon![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I think they're called...[SOFTBLOCK] Moonglow Stones?[SOFTBLOCK] They're supposed to capture moonlight at night and release it during the day.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I found it in the forest a while back.[SOFTBLOCK] I think it suits you![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] It's beautiful![SOFTBLOCK] Thank you, Breannepurrr![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Anything for a friend![SOFTBLOCK] Take good care of it, now!") ]])
					VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
					LM_ExecuteScript(gsItemListing, "Moonglow Collar")
	
				--If Mei is a ghost:
				elseif(sMeiForm == "Ghost") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: ...[SOFTBLOCK] Mei?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Hello, Breanne.[SOFTBLOCK] What have you been up to lately?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Breathing![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Huh?[SOFTBLOCK] Oh...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's not what it looks like.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'd say it is, considering you're transparent![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Incredible how you can see right through her, right?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I'm fine, really.[SOFTBLOCK] In fact, this is quite nice.[SOFTBLOCK] I'll never get hungry, or sick.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I suppose so...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] There's always a good side, if you look for it.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's quite cold, though.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Could we perhaps talk about something else?[BLOCK][CLEAR]") ]])
				end
		
			--If Mei did not enter as a human.
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mei?[SOFTBLOCK] Well, aren't you full of surprises.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Oh, dear.[SOFTBLOCK] I can explain.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Y'see, my runestone does this thing...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Say no more.[SOFTBLOCK] There's magic afoot, is there?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I guess so.[SOFTBLOCK] It flashes white and then, poof.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Very useful to have around.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: You don't seem surprised.[SOFTBLOCK] Have you ever seen someone like Mei before?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Can't say that I have.[SOFTBLOCK] If there were any rules about Pandemonium, you just broke one of them.[BLOCK][CLEAR]") ]])
			
				if(sMeiForm == "Werecat" and iGotWerecatCollar == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh![SOFTBLOCK] Oh![SOFTBLOCK] Here, this would look great on you![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] A collar?[SOFTBLOCK] I am not a simple housecat, Breanne![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Are you sure?[SOFTBLOCK] Look closer![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] [SOUND|World|TakeItem](Received Moonglow Collar)[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] This...[SOFTBLOCK] It glows like the moon![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I think they're called...[SOFTBLOCK] Moonglow Stones?[SOFTBLOCK] They're supposed to capture moonlight at night and release it during the day.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I found it in the forest a while back.[SOFTBLOCK] I think it suits you![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] It's beautiful![SOFTBLOCK] Thank you, Breannepurrr![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ... [SOFTBLOCK]Sorry about getting upset earlier.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Aww, that's okay.[SOFTBLOCK] No harm done.[SOFTBLOCK] Take good care of that, now![BLOCK][CLEAR]") ]])
					VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
					LM_ExecuteScript(gsItemListing, "Moonglow Collar")
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'll be keeping an eye on you two, that's for sure.[SOFTBLOCK] If you've got any stories I'd love to hear them.[BLOCK][CLEAR]") ]])
				end
				
				--Flag.
				WD_SetProperty("Unlock Topic", "Pandemonium", 1)
	
			end
	
		--Otherwise, Breanne observes it herself.
		else
		
			--If Mei was originally a human:
			if(sMeiFirstForm == "Human") then
				
				--If Mei is a slime:
				if(sMeiForm == "Slime") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, no.[SOFTBLOCK] Mei...[SOFTBLOCK] I'm so sorry...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Breanne, I can explain...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, you can speak?[SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well golly, what a relief![SOFTBLOCK] I had thought I lost a friend.[SOFTBLOCK] Don't spook me like that again![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: It's my runestone.[SOFTBLOCK] It lets me stay, well, me.[SOFTBLOCK] I can even turn back into a human if I want to.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Now that there is a special rock.[SOFTBLOCK] You hang on to that.[SOFTBLOCK] World needs more girls like you, and it certainly doesn't need more slimes.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: That said, you look pretty good for mass of goo.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Oh, why thank you.[SOFTBLOCK] I can look however I want, but I like this the most...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: ...[SOFTBLOCK] Don't ya go readin' too far into that![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Now then, what can I do for you?[BLOCK][CLEAR]") ]])
				
				--If Mei is an Alraune:
				elseif(sMeiForm == "Alraune") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mei![SOFTBLOCK] You're -[SOFTBLOCK] What have you gone and done?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Is this a problem?[SOFTBLOCK] I rather like myself now...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Besides, it's not permanent.[SOFTBLOCK] This runestone lets me change form if I feel like it.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: It does?[SOFTBLOCK] That's awful handy.[SOFTBLOCK] Seems there's more to you than meets the eye.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Does that mean you feel like being a plant girl?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I love it.[SOFTBLOCK] Could I possibly convince you to be joined?[SOFTBLOCK] The sensation is incomparable...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'm afraid that'd be a one-way trip for me, dearie, and I'll not be making it any time soon.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Unless you think that stone of yours might work on me?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: It won't.[SOFTBLOCK] I'm not sure how I know that, but I do.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'm quite happy as I am, but thank you for the offer.[SOFTBLOCK] Now what can I do for you?[BLOCK][CLEAR]") ]])
	
				--If Mei is a bee:
				elseif(sMeiForm == "Bee") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Dearest me, Mei.[SOFTBLOCK] Did you pick a fight with the bees?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: It's not what it looks like.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You can speak?[SOFTBLOCK] Are you still you?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: In some ways.[SOFTBLOCK] In others, less-so.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: It has to do with my runestone, but I don't know why.[SOFTBLOCK] It lets me change form, and remember who I am.[SOFTBLOCK] For better or worse...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I've had some friends go that way.[SOFTBLOCK] I reckon they'd want that runestone to work a treat on them too.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: ...[SOFTBLOCK] Yes?[SOFTBLOCK] I see.[SOFTBLOCK] I'll tell her.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Excuse me?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Apologies, I was talking to the hive.[SOFTBLOCK] We can hear one another, you see.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: They said they remember you, Breanne.[SOFTBLOCK] They said not to worry, and that they're very happy.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, Joanie...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Joanie?[SOFTBLOCK] You mean - [SOFTBLOCK]well, we don't normally have names.[SOFTBLOCK] But, yes, she's here.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'm about to cry.[SOFTBLOCK] Look away, please.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: She's very happy as a bee.[SOFTBLOCK] You have nothing to worry about.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: *sniff*[SOFTBLOCK] You mean that?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: We have no reason to lie.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh Joanie, I'm so glad you're okay.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: *sniff*[SOFTBLOCK] No no, gotta be strong.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Let's talk about something else, okay?[SOFTBLOCK] But you tell Joanie that if she wants to come see me...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: She already knows.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Thanks so much, Mei.[SOFTBLOCK] Thank you.[SOFTBLOCK] Really...[BLOCK][CLEAR]") ]])
					
					--Flag.
					VM_SetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N", 1.0)
	
				--If Mei is a werecat:
				elseif(sMeiForm == "Werecat") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh my, Mei![SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] What does it look like?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh dear.[SOFTBLOCK] The cure is so easy to mix up, but I'm afraid it won't have any effect once the transformation is complete.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Cure?[SOFTBLOCK] Hss![SOFTBLOCK] No thanks![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I've never felt better![SOFTBLOCK] The moon is my eternal companion![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You should come hunt with me, Breanne.[SOFTBLOCK] You'd love the speed, the rush of the kill...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Hah![SOFTBLOCK] Thanks for the offer, but I'm all right the way I am.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I need strong pridemates.[SOFTBLOCK] If you change your mind...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Aww, Mei, you're really flattering me![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Worth a shot.[SOFTBLOCK] Hey, you wouldn't have any chicken in the pantry, would you?[SOFTBLOCK] I'm starved![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Didn't you just say you're a hunter?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We hunt mostly for sport, for food only when necessary.[SOFTBLOCK] But, I'm so busy looking for a way back to Earth...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'll give you some chicken if you're willing to curl up on my lap and purr.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Don't patronize me![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, I'm sorry![SOFTBLOCK] Here, how about a gift?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] [SOUND|World|TakeItem](Received Moonglow Collar)[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] This...[SOFTBLOCK] It glows like the moon![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I think they're called...[SOFTBLOCK] Moonglow Stones?[SOFTBLOCK] They're supposed to capture moonlight at night and release it during the day.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I found it in the forest a while back.[SOFTBLOCK] I think it suits you![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] It's beautiful![SOFTBLOCK] Thank you, Breannepurrr![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ... [SOFTBLOCK]Sorry about getting upset earlier.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Aww, that's okay.[SOFTBLOCK] No harm done.[SOFTBLOCK] Take good care of that, now![BLOCK][CLEAR]") ]])
					VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
					LM_ExecuteScript(gsItemListing, "Moonglow Collar")
	
				--If Mei is a ghost:
				elseif(sMeiForm == "Ghost") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: ...[SOFTBLOCK] Mei?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Hello, Breanne.[SOFTBLOCK] What have you been up to lately?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Breathing![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Huh?[SOFTBLOCK] Oh...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's not what it looks like.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'd say it is, considering you're transparent![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I'm fine, really.[SOFTBLOCK] In fact, this is quite nice.[SOFTBLOCK] I'll never get hungry, or sick.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I suppose so...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] There's always a good side, if you look for it.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's quite cold, though.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Could we perhaps talk about something else?[BLOCK][CLEAR]") ]])
	
				end
		
			--If Mei did not enter as a human.
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mei?[SOFTBLOCK] Well, aren't you full of surprises.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Oh, dear.[SOFTBLOCK] I can explain.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Y'see, my runestone does this thing...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Say no more.[SOFTBLOCK] There's magic afoot, is there?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I guess so.[SOFTBLOCK] It flashes white and then, poof.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Very useful to have around.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: You don't seem surprised.[SOFTBLOCK] Have you met anyone else who can do this?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Can't say that I have.[SOFTBLOCK] If there were any rules about Pandemonium, you just broke one of them.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'll be keeping an eye on you, that's for sure.[SOFTBLOCK] If you've got any stories I'd love to hear them.[BLOCK][CLEAR]") ]])
				
				--Flag.
				WD_SetProperty("Unlock Topic", "Pandemonium", 1)
			end
		end
	
	--If Mei arrived with Florentina when she was not in tow earlier.
	elseif(bIsFlorentinaPresent == true and iMetMeiWithFlorentina == 0.0) then
			
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Is that Florentina?[SOFTBLOCK] You don't visit nearly enough![SOFTBLOCK] How are you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Breanne...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Friendlier than the usual greeting.[SOFTBLOCK] You're improving.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Do you two have a history?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: She's a sometime business partner, and sometime rival.[SOFTBLOCK] You know, best friend material.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Don't believe her lies.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Aww, that's as close to a sign of affection as you'll get out of her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I'll keep that in mind.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Now what can I do for you two?[SOFTBLOCK] Did Florentina figure out your conundrum like I said?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: No, but I'd bet that Sister Claudia would.[SOFTBLOCK] Is she here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Claudia?[SOFTBLOCK] Claudia...[SOFTBLOCK] oh right, the monk.[SOFTBLOCK] No, she went northeast a few weeks ago with her convent.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Guess we're going northeast, then.[SOFTBLOCK] Thanks, Breanne.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: No trouble at all![SOFTBLOCK] Oh, I think one of her followers was at Outland Farm west of here.[SOFTBLOCK] You might want to ask them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Thanks![SOFTBLOCK] You're a huge help, you know that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Just doing my job.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: You two are so saccharine it makes me sick.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Aww, flattery will get you everywhere.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Can I help you with anything else?[BLOCK][CLEAR]") ]])
				
		--Flag.
		WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
	
	--If for any reason Mei is a werecat and has not received the moonglow collar:
	elseif(iGotWerecatCollar == 0.0 and sMeiForm == "Werecat") then
	
		--Flags.
		VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
		LM_ExecuteScript(gsItemListing, "Moonglow Collar")
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, Mei, I was just thinking about you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ...[SOFTBLOCK] What is that glow in your pocket?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Nothing gets past those eyes of yours, I see.[SOFTBLOCK] Here, take a look![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This glows just like the moon...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOUND|World|TakeItem](Received Moonglow Collar)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I found this in the forest a while back.[SOFTBLOCK] It glows during the day if you leave it outside at night.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I think it's called a Moonglow stone.[SOFTBLOCK] I think.[SOFTBLOCK] Not totally sure.[SOFTBLOCK] But I thought you'd like it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] It's beautiful, thank you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But I don't have a gift to give you in return...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Just seein' you in such spirits is gift enough, darlin'.[BLOCK][CLEAR]") ]])
	
	--If none of the above cases trigger, check if Breanne can offer Mei the Pie quest. Mei cannot be a ghost or a slime.
	elseif(bIsFlorentinaPresent == true and iTalkedJob == 1.0 and iTalkedParents == 1.0 and iTakenPieJob == 0.0 and sMeiLevel >= 3 and sMeiForm ~= "Ghost" and sMeiForm ~= "Slime") then
	
		--Set flag.
		VM_SetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N", 1.0)
		
		--Unlock the pie topic with Florentina.
		WD_SetProperty("Unlock Topic", "Pepper Pie", 1)
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: My stars, Mei![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh?[SOFTBLOCK] Did I do something wrong?[SOFTBLOCK] I'm sorry...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Just the opposite, actually. I was just admiring your arms, there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You've been getting quite a lot of exercise, haven't you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] ..?[SOFTBLOCK] Oh, I suppose I have.[SOFTBLOCK] I guess constantly fighting for your life does that to you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] This is Breanne's awful way of disguising admiration.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Hush![SOFTBLOCK] I actually wanted to venture an idea.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mei, have you ever heard of Rondheim Pepper Pie?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] !!![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, what's that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well, I was thinking of ways to help you out on your quest, and - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] We'll take it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well don't go jumping the gun, now.[SOFTBLOCK] I don't actually have all of the ingredients.[SOFTBLOCK] That's where you come in.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Is anyone going to actually explain what this pie is?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's a very powerful magic potion.[SOFTBLOCK] Very rare.[SOFTBLOCK] The ingredients are hard to track down, and you need to be an expert baker to get it right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Pshaw![SOFTBLOCK] It's not that hard![SOFTBLOCK] But, she's right about it being powerful magic.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But didn't you say it was a pie?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: If you like drinking magic potions, then more power to you.[SOFTBLOCK] Personally it tastes like sweaty sock drippings to me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's common practice to mix potions into baked goods to make them taste better.[SOFTBLOCK] Cakes, pies, donuts, you name it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh![SOFTBLOCK] Okay![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I've got most of the ingredients on me, but there's a few more I need and I can't really leave the Pit Stop to get them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Let me see your list...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Kokayanee, Gaardian Cave Moss, Decayed Nectar?[SOFTBLOCK] Yeah we can manage this stuff.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We can?[SOFTBLOCK] I don't even know what half of these are![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Ask me at a rest stop and I'll give you a list of what we need and what I know about them.[SOFTBLOCK] There's quite a few.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: If you can get me all of these then I'll make you a nice Pepper Pie![SOFTBLOCK] Really lay the hurt on those baddies out there![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You better believe we're on the job.[SOFTBLOCK] C'mon, Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Good luck![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *You really seem to want this Pepper Pie, Florentina.[SOFTBLOCK] Is it expensive?[SOFTBLOCK] Are you looking to sell it?*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] *It'd fetch a hefty sum, sure, but I really just want to eat it.[SOFTBLOCK] It's better than sex.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] *You'll agree when you taste it, trust me.*[BLOCK][CLEAR]") ]])
	
	--If on the pie job and you have what you need:
	elseif(iTakenPieJob == 1.0) then
	
		--Item Variables.
		local iMossCount      = AdInv_GetProperty("Item Count", "Gaardian Cave Moss")
		local iPaperCount     = AdInv_GetProperty("Item Count", "Booped Paper")
		local iNectarCount    = AdInv_GetProperty("Item Count", "Decayed Bee Nectar")
		local iFlowerCount    = AdInv_GetProperty("Item Count", "Hypnotic Flower Petals")
		local iSalamiCount    = AdInv_GetProperty("Item Count", "Translucent Quantirian Salami")
		local iKokayaneeCount = AdInv_GetProperty("Item Count", "Kokayanee")
	
		--Party has everything:
		if(iMossCount > 0 and iPaperCount > 0 and iNectarCount > 0 and iFlowerCount > 0 and iSalamiCount > 0 and iKokayaneeCount > 0) then
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Look what we got![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You guys are fast![SOFTBLOCK] Hey, if the whole Earth thing doesn't work out, we could go into business together![SOFTBLOCK] We'd make a small fortune selling these![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I might take you up on that.[SOFTBLOCK] Don't tempt me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] So what do we do now?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You patiently wait here while I do a bit of baking.[SOFTBLOCK] Make yourselves comfortable!") ]])
			fnCutsceneBlocker()
			
			--Wait a bit.
			fnCutsceneWait(45)
			fnCutsceneBlocker()
			
			--Fade out.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneBlocker()
			
			--Wait a while.
			fnCutsceneWait(240)
			fnCutsceneBlocker()
			
			--Reposition everyone.
			fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Kitchen Door") ]])
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Teleport To", (20.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Teleport To", (19.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Teleport To", (27.25 * gciSizePerTile), (17.50 * gciSizePerTile))
			DL_PopActiveObject()
			
			--Change facing.
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Face",  -1, 0)
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Face",  1, 0)
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Face",  1, 0)
			DL_PopActiveObject()
			fnCutsceneBlocker()
			
			--Fade back in.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
			fnCutsceneBlocker()
			
			--Wait a bit.
			fnCutsceneWait(30)
			fnCutsceneBlocker()
			
			--Breanne dialogue:
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne:[VOICE|Breanne] Finished![SOFTBLOCK] And it smells great!") ]])
			fnCutsceneBlocker()
			
			--Breanne walks out of the kitchen.
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Face",  1, 0)
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Move To", (24.25 * gciSizePerTile), (16.50 * gciSizePerTile))
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (16.50 * gciSizePerTile))
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Move To", (21.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneBlocker()
			
			--Wait a bit.
			fnCutsceneWait(30)
			fnCutsceneBlocker()
			
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
			fnCutsceneBlocker()
			
			--Dialogue.
			fnStandardMajorDialogue()
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Breanne", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Who wants the first taste?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Me, me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, you go ahead, kid.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *nom*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] (...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I think I'm going to explode out of sheer pleasure![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Have another bite![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *nom*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Oof, now I might explode, and then explode again![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Can you do that?[SOFTBLOCK] Can you explode twice?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Of course not![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well the pie was a huge success, and it's all thanks to you.[SOFTBLOCK] Nice work, everyone![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] This stuff is a powerful potion, Mei.[SOFTBLOCK] It'll make those desperate battles just a little less desperate.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] As if I needed more reason to eat it![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thanks a ton, Breanne![SOFTBLOCK] This is great![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Just hoping it helps you along.[SOFTBLOCK] Good luck out there![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Now was there anything else I could do for ya?[BLOCK][CLEAR]") ]])
			
			--Gain the pie.
			LM_ExecuteScript(gsItemListing, "Pepper Pie")
			
			--Remove the other items.
			AdInv_SetProperty("Remove Item", "Gaardian Cave Moss")
			AdInv_SetProperty("Remove Item", "Booped Paper")
			AdInv_SetProperty("Remove Item", "Decayed Bee Nectar")
			AdInv_SetProperty("Remove Item", "Hypnotic Flower Petals")
			AdInv_SetProperty("Remove Item", "Translucent Quantirian Salami")
			AdInv_SetProperty("Remove Item", "Kokayanee")
			
			--Set variable.
			VM_SetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N", 2.0)
			
		--Default case:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Howdy, Mei![SOFTBLOCK] What can I do for ya?[BLOCK][CLEAR]") ]])
		end
	
	--Default case, no special flags.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Howdy, Mei![SOFTBLOCK] What can I do for ya?[BLOCK][CLEAR]") ]])
	end

	--[Topics]
	--Activate topics mode once the dialogue is complete.
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Breanne") ]])
end
