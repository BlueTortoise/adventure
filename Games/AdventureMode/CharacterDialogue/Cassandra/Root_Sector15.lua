--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Flag.
	local iCassandraSpokenTo = VM_GetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenTo", "N")
	
	--First time speaking to Cassandra:
	if(iCassandraSpokenTo == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenTo", "N", 1.0)
	
		--Speak.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Golem") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Unit 771852![SOFTBLOCK] I am glad you are well![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Greetings Unit 771853.[SOFTBLOCK] Have you received a function yet?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I have.[SOFTBLOCK] I have been assigned oversight of cleanup here in Sector 15, as well as general efficiency improvements.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: It seems no Lord Unit was previously assigned.[SOFTBLOCK] I relish the challenge of the task.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Good for you![SOFTBLOCK] I'm sure you'll make things very productive around here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: What of you?[SOFTBLOCK] Why are you visiting this sector?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Just passing through.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Say, you haven't remembered anything about how you got to Regulus City, have you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: No.[SOFTBLOCK] What few organic memories I have were repurposed for my new function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: It is just as well.[SOFTBLOCK] As a human, I was purposeless.[SOFTBLOCK] Now, I will serve the Cause of Science.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: As we all do.[SOFTBLOCK] Good luck, Unit 771853!") ]])
	
	--Repeats.
	else
	
		--Speak.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Golem") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Hmm.[SOFTBLOCK] I seem to have a vague memory of a white-haired -[SOFTBLOCK] not human.[SOFTBLOCK] She was a woman, but I'm not sure she was a human.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Old memories resurfacing?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: They are not relevant.[SOFTBLOCK] I will ignore them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I must serve the Cause of Science.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: That is what we do.[SOFTBLOCK] See you later, Unit 771853.") ]])
	
	end
end
