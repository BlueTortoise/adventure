--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Flag.
	local iCassandraSpokenTo = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraSpokenTo", "N")
	
	--First time speaking to Cassandra:
	if(iCassandraSpokenTo == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraSpokenTo", "N", 1.0)
	
		--Speak.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Mei![SOFTBLOCK] Good to see you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Likewise![SOFTBLOCK] Has your memory recovered?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Ugh, not at all.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Maybe if you retraced your steps.[SOFTBLOCK] What can you remember?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I was at my home in Jeffespeir, reading a book on astronomy.[SOFTBLOCK] Then, I heard someone knock on my door.[SOFTBLOCK] Then, a blur -[SOFTBLOCK] and then I was in the forest.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: My clothes are completely different and I don't know how I got here, but I was looking for someone very important.[SOFTBLOCK] I think.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Who were you looking for?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: That, I don't know.[SOFTBLOCK] I do remember it was extremely important that I find them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I'm sure I'll figure it out in time.[SOFTBLOCK] This trading post is pretty busy.[SOFTBLOCK] Whoever I was looking for will probably recognize me when they come through here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Well, I wish you well.[SOFTBLOCK] Hopefully you find whoever you were looking for.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Thanks, Mei.[SOFTBLOCK] I hope you find what you were looking for as well.") ]])
	
	--Repeats.
	else
	
		--Speak.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Hmmm...[SOFTBLOCK] I'm pretty sure I was looking for a white-haired woman, but that doesn't describe anyone here...") ]])
	
	end
end
