--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMetClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
	local iHasHacksaw = AdInv_GetProperty("Item Count", "Hacksaw")
	local iGotHacksaw = VM_GetVar("Root/Variables/Chapter1/Scenes/iGotHacksaw", "N")
	local iHasMetFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	local iMeiReadClaudiasJournal = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiReadClaudiasJournal", "N")
	local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
	local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
	
	--Hasn't met Claudia yet.
	if(iMetClaudia == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N", 1.0)
	
		--If Mei has the hacksaw:
		if(iHasHacksaw == 1.0) then
			
			--If Mei hasn't met Florentina, she doesn't know who Claudia is or why she's looking for her:
			if(iHasMetFlorentina == 0.0) then
				
				--Standard.
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: And the clouds parted at her behest, for she had proven her penitence.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Her deliverer bore a tool.[SOFTBLOCK] The faithful were rewarded for their piety.[BLOCK][CLEAR]") ]])
				
				--Mei is a slime:
				if(sMeiForm == "Slime") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh....[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: ..![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: You can speak, slime?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You can call me Mei, if you like.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Apologies![SOFTBLOCK] I've been alone here for -[SOFTBLOCK] I'm not sure how long.[SOFTBLOCK] My only company has been these bones.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Surely you are no mean slime.[SOFTBLOCK] I've no doubt the Host has sent you to guide me back to my path.[BLOCK][CLEAR]") ]])
				
				--Mei is a ghost:
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh....[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Oh, most interesting.[SOFTBLOCK] Your comrades were not talkative.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Comrades?[SOFTBLOCK] Oh, the other ghosts.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not like them.[SOFTBLOCK] I remember who I am.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: And who is that?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Call me Mei.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Well, Mei, doubtless you have seen what happened to my convent.[SOFTBLOCK] They, unfortunately, are not so aware of themselves.[BLOCK][CLEAR]") ]])
				end
				
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Are you here to deliver me?[SOFTBLOCK] Within your form I see you have an instrument I could use.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Maybe you can answer a few questions first.[SOFTBLOCK] Namely, what are you doing in this moldy jail cell?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Jail cell?[SOFTBLOCK] My child, this is no jail cell.[SOFTBLOCK] It is a cruel stage where the actors suffer and die for the audience.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Do you see the indent on the roof there?[SOFTBLOCK] There is a mechanism there that opens.[SOFTBLOCK] People fall in here, but they cannot leave by any means.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Here they starve to death in obscurity.[SOFTBLOCK] Their means of deliverence cruelly just without their reach...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That'd explain the bag of tools.[SOFTBLOCK] Just who are you?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I am Claudia Romanus, leader of the Heavenly Doves.[SOFTBLOCK] Or former leader, I should say...[BLOCK][CLEAR]") ]])
				
				if(iMeiReadClaudiasJournal == 1.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I saw your journals upstairs.[SOFTBLOCK] How'd you wind up here?[BLOCK][CLEAR]") ]])
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Never heard of you, but I guess that's not surprising.[SOFTBLOCK] I haven't been here very long.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] How'd you wind up in here?[BLOCK][CLEAR]") ]])
				end
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: My convent and I were researching the unusual partirhuman specimens in the Quantir Estate, though we could not find a trace of them.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: We were about to give up and move on when they came vengefully from every direction at once.[SOFTBLOCK] We split up, and I must have wandered into a trap.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I fell into this room.[SOFTBLOCK] My convent has not come looking...[SOFTBLOCK] I fear they are now residents of the Estate.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Man, everyone keeps getting locked up around here.[SOFTBLOCK] This place sucks.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hey, you wouldn't happen to know how I can get back home, would you?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Well I -[SOFTBLOCK] that runestone...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: That symbol is of Rilmani origin![SOFTBLOCK] You are -[SOFTBLOCK] you are not of Pandemonium, are you?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: A joyous occasion![SOFTBLOCK] One of the promised has come and finds me![SOFTBLOCK] I am blessed![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Kind of an odd attitude for someone who was going to starve to death.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Starve?[SOFTBLOCK] Certainly not.[SOFTBLOCK] My faith -[SOFTBLOCK] I am unyielding.[SOFTBLOCK] I could endure a thousand years in this cage if it proved my loyalty.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, sure you could.[SOFTBLOCK] So -[SOFTBLOCK] the Rilmani can help me get home?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: They are arbiters of the dimensional pact.[SOFTBLOCK] If you are here, they must be involved, or at least aware of it.[SOFTBLOCK] Find one.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I regret that I cannot help you further.[SOFTBLOCK] My translation notes are still in the Quantir Estate.[SOFTBLOCK] Without them I cannot help you locate a Rilmani.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But if I can find those notes?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: There are no 'ifs' in the Divine's plans.[SOFTBLOCK] You will find them and you will be delivered to the Rilmani as surely as water rolls downhill.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Lovely.[SOFTBLOCK] Guess I'll go find those notes then.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Perhaps you would see fit to loan me your saw?[SOFTBLOCK] If the Host has found my atonement sufficient, I will return to my mission.[BLOCK]") ]])

				--Decision script is this script. It must be surrounded by quotes.
				local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
				fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
				fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Here you go\", " .. sDecisionScript .. ", \"Loan\") ")
				fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Nope\",  " .. sDecisionScript .. ", \"Nope\") ")
				fnCutsceneBlocker()
	
				--Topic.
				WD_SetProperty("Unlock Topic", "Rilmani", 1)
				
			--Otherwise, Mei knows this is her girl:
			else
				
				--Standard:
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: And the clouds parted at her behest, for she had proven her penitence.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Her deliverer bore a tool.[SOFTBLOCK] The faithful were rewarded for their piety.[BLOCK][CLEAR]") ]])
				
				--Mei is a slime:
				if(sMeiForm == "Slime") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh....[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: ..![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: You can speak, slime?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You can call me Mei, if you like.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Apologies![SOFTBLOCK] I've been alone here for -[SOFTBLOCK] I'm not sure how long.[SOFTBLOCK] My only company has been these bones.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Surely you are no mean slime.[SOFTBLOCK] I've no doubt the Host has sent you to guide me back to my path.[BLOCK][CLEAR]") ]])
				
				--Mei is a ghost:
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh....[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Oh, most interesting.[SOFTBLOCK] Your comrades were not talkative.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Comrades?[SOFTBLOCK] Oh, the other ghosts.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not like them.[SOFTBLOCK] I remember who I am.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: And who is that?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Call me Mei.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Well, Mei, doubtless you have seen what happened to my convent.[SOFTBLOCK] They, unfortunately, are not so aware of themselves.[BLOCK][CLEAR]") ]])
				end
				
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Are you here to deliver me?[SOFTBLOCK] Within your form I see you have an instrument I could use.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Maybe you can answer a few questions first.[SOFTBLOCK] Namely, what are you doing in this moldy jail cell?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Jail cell?[SOFTBLOCK] My child, this is no jail cell.[SOFTBLOCK] It is a cruel stage where the actors suffer and die for the audience.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Do you see the indent on the roof there?[SOFTBLOCK] There is a mechanism there that opens.[SOFTBLOCK] People fall in here, but they cannot leave by any means.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Here they starve to death in obscurity.[SOFTBLOCK] Their means of deliverence cruelly just without their reach...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That'd explain the bag of tools.[SOFTBLOCK] Just who are you?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I am Claudia Romanus, leader of the Heavenly Doves.[SOFTBLOCK] Or former leader, I should say...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Claudia![SOFTBLOCK] You're the one I've been looking for![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Florentina said you'd know how to get back to Earth![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: The name Earth is foreign to me, I am afraid.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] B-[SOFTBLOCK]but - [SOFTBLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Please, child, do not lose hope.[SOFTBLOCK] As surely as you have found me by the Divine's will, you have the means of your own deliverance.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: The runestone that hovers in your form...[SOFTBLOCK] It is a Rilmani artifact.[BLOCK][CLEAR]") ]])
				if(iMeiKnowsRilmani == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Really?[BLOCK][CLEAR]") ]])

					--Clear the flag on Next Move.
					WD_SetProperty("Clear Topic Read", "NextMove")
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Yeah![SOFTBLOCK] That's what your journal said![BLOCK][CLEAR]") ]])
				end
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Indeed.[SOFTBLOCK] Please ignore the doubt and rumours.[SOFTBLOCK] The Rilmani are very real, and theirs is the duty of upholding the Dimensional Pact.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: If you can find one, I have no doubt they will return you to your home.[BLOCK][CLEAR]") ]])
				if(iMeiKnowsRilmani == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Great![SOFTBLOCK] How do I find one?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: That, I cannot aid you with.[SOFTBLOCK] My research notes are still somewhere in the Quantir Estate.[SOFTBLOCK] If you could find them, they might help you.[BLOCK][CLEAR]") ]])
				end
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Okay![SOFTBLOCK] Now I know what to do![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: The Divine will guide you as it guides all things.[SOFTBLOCK] Perhaps you may see fit to loan me the hacksaw that is within your body?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: If I have atoned for my failures, I may continue my mission of holy purpose.[BLOCK]") ]])
				
				--Decision script is this script. It must be surrounded by quotes.
				local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
				fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
				fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Here you go\", " .. sDecisionScript .. ", \"Loan\") ")
				fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Nope\",  " .. sDecisionScript .. ", \"Nope\") ")
				fnCutsceneBlocker()
	
				--Topic.
				WD_SetProperty("Unlock Topic", "Rilmani", 1)
		
			end
		
		--Mei does not have the hacksaw:
		else
		
			--Flag.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iMetClaudiaWithoutHacksaw", "N", 1.0)
			
			--If Mei hasn't met Florentina, she doesn't know who Claudia is or why she's looking for her:
			if(iHasMetFlorentina == 0.0) then
				
				--Standard:
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: And the clouds did not part, the sun did not shine.[SOFTBLOCK] Her faith was insufficient, her piety pathetic.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: She would suffer until true purity had been attained, and no sooner.[BLOCK][CLEAR]") ]])
				
				--Mei is a slime:
				if(sMeiForm == "Slime") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh....[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: ..![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: You can speak, slime?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You can call me Mei, if you like.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Apologies![SOFTBLOCK] I've been alone here for -[SOFTBLOCK] I'm not sure how long.[SOFTBLOCK] My only company has been these bones.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Surely you are no mean slime.[SOFTBLOCK] I've no doubt the Host has sent you to guide me back to my path.[BLOCK][CLEAR]") ]])
				
				--Mei is a ghost:
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh....[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Oh, most interesting.[SOFTBLOCK] Your comrades were not talkative.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Comrades?[SOFTBLOCK] Oh, the other ghosts.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not like them.[SOFTBLOCK] I remember who I am.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: And who is that?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Call me Mei.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Well, Mei, doubtless you have seen what happened to my convent.[SOFTBLOCK] They, unfortunately, are not so aware of themselves.[BLOCK][CLEAR]") ]])
				end
				
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Are you here to deliver me, perchance?[SOFTBLOCK] I apologize that I did not recognize you for what you were sooner![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Maybe you can answer a few questions first.[SOFTBLOCK] Namely, what are you doing in this moldy jail cell?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Jail cell?[SOFTBLOCK] My child, this is no jail cell.[SOFTBLOCK] It is a cruel stage where the actors suffer and die for the audience.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Do you see the indent on the roof there?[SOFTBLOCK] There is a mechanism there that opens.[SOFTBLOCK] People fall in here, but they cannot leave by any means.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Here they starve to death in obscurity.[SOFTBLOCK] Their means of deliverence cruelly just without their reach...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That'd explain the bag of tools.[SOFTBLOCK] Just who are you?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I am Claudia Romanus, leader of the Heavenly Doves.[SOFTBLOCK] Or former leader, I should say...[BLOCK][CLEAR]") ]])
				
				if(iMeiReadClaudiasJournal == 1.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I saw your journals upstairs.[SOFTBLOCK] How'd you wind up here?[BLOCK][CLEAR]") ]])
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Never heard of you, but I guess that's not surprising.[SOFTBLOCK] I haven't been here very long.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] How'd you wind up in here?[BLOCK][CLEAR]") ]])
				end
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: My convent and I were researching the unusual partirhuman specimens in the Quantir Estate, though we could not find a trace of them.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: We were about to give up and move on when they came vengefully from every direction at once.[SOFTBLOCK] We split up, and I must have wandered into a trap.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I fell into this room.[SOFTBLOCK] My convent has not come looking...[SOFTBLOCK] I fear they are now residents of the Estate.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Man, everyone keeps getting locked up around here.[SOFTBLOCK] This place sucks.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hey, you wouldn't happen to know how I can get back home, would you?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Well I -[SOFTBLOCK] that runestone...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: That symbol is of Rilmani origin![SOFTBLOCK] You are -[SOFTBLOCK] you are not of Pandemonium, are you?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: A joyous occasion![SOFTBLOCK] One of the promised has come and finds me![SOFTBLOCK] I am blessed![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Kind of an odd attitude for someone who was going to starve to death.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Starve?[SOFTBLOCK] Certainly not.[SOFTBLOCK] My faith -[SOFTBLOCK] I am unyielding.[SOFTBLOCK] I could endure a thousand years in this cage if it proved my loyalty.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, sure you could.[SOFTBLOCK] So -[SOFTBLOCK] the Rilmani can help me get home?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: They are arbiters of the dimensional pact.[SOFTBLOCK] If you are here, they must be involved, or at least aware of it.[SOFTBLOCK] Find one.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I regret that I cannot help you further.[SOFTBLOCK] My translation notes are still in the Quantir Estate.[SOFTBLOCK] Without them I cannot help you locate a Rilmani.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But if I can find those notes?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: There are no [SOFTBLOCK]'ifs'[SOFTBLOCK] in the Divine's plans.[SOFTBLOCK] You will find them and you will be delivered to the Rilmani as surely as water rolls downhill.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Lovely.[SOFTBLOCK] Guess I'll go find those notes then.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Unfortunately, it seems that your nature is your advantage.[SOFTBLOCK] If you could find a way to help me out of this cage, I would be grateful.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll see what I can do...") ]])
				
				--Topic.
				WD_SetProperty("Unlock Topic", "Rilmani", 1)
			
			--Otherwise, Mei knows this is her girl:
			else
	
				--Topic.
				WD_SetProperty("Unlock Topic", "Rilmani", 1)
				
				--Standard:
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: And the clouds did not part, the sun did not shine.[SOFTBLOCK] Her faith was insufficient, her piety pathetic.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: She would suffer until true purity had been attained, and no sooner.[BLOCK][CLEAR]") ]])
				
				--Mei is a slime:
				if(sMeiForm == "Slime") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh....[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: ..![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: You can speak, slime?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You can call me Mei, if you like.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Apologies![SOFTBLOCK] I've been alone here for -[SOFTBLOCK] I'm not sure how long.[SOFTBLOCK] My only company has been these bones.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Surely you are no mean slime.[SOFTBLOCK] I've no doubt the Host has sent you to guide me back to my path.[BLOCK][CLEAR]") ]])
				
				--Mei is a ghost:
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh....[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Oh, most interesting.[SOFTBLOCK] Your comrades were not talkative.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Comrades?[SOFTBLOCK] Oh, the other ghosts.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not like them.[SOFTBLOCK] I remember who I am.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: And who is that?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Call me Mei.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Well, Mei, doubtless you have seen what happened to my convent.[SOFTBLOCK] They, unfortunately, are not so aware of themselves.[BLOCK][CLEAR]") ]])
				end
				
				--Continue:
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Are you here to deliver me, perchance?[SOFTBLOCK] I apologize that I did not recognize you for what you were sooner![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Maybe you can answer a few questions first.[SOFTBLOCK] Namely, what are you doing in this moldy jail cell?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Jail cell?[SOFTBLOCK] My child, this is no jail cell.[SOFTBLOCK] It is a cruel stage where the actors suffer and die for the audience.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Do you see the indent on the roof there?[SOFTBLOCK] There is a mechanism there that opens.[SOFTBLOCK] People fall in here, but they cannot leave by any means.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Here they starve to death in obscurity.[SOFTBLOCK] Their means of deliverence cruelly just without their reach...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That'd explain the bag of tools.[SOFTBLOCK] Just who are you?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I am Claudia Romanus, leader of the Heavenly Doves.[SOFTBLOCK] Or former leader, I should say...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Claudia![SOFTBLOCK] You're the one I've been looking for![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Florentina said you'd know how to get back to Earth![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: The name Earth is foreign to me, I am afraid.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] B-[SOFTBLOCK]but - [SOFTBLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Please, child, do not lose hope.[SOFTBLOCK] As surely as you have found me by the Divine's will, you have the means of your own deliverance.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: The runestone that hovers in your form...[SOFTBLOCK] It is a Rilmani artifact.[BLOCK][CLEAR]") ]])
				if(iMeiKnowsRilmani == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Really?[BLOCK][CLEAR]") ]])

					--Clear the flag on Next Move.
					WD_SetProperty("Clear Topic Read", "NextMove")
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah![SOFTBLOCK] That's what your journal said![BLOCK][CLEAR]") ]])
				end
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Indeed.[SOFTBLOCK] Please ignore the doubt and rumours.[SOFTBLOCK] The Rilmani are very real, and theirs is the duty of upholding the Dimensional Pact.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: If you can find one, I have no doubt they will return you to your home.[BLOCK][CLEAR]") ]])
				if(iMeiKnowsRilmani == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Great![SOFTBLOCK] How do I find one?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: That, I cannot aid you with.[SOFTBLOCK] My research notes are still somewhere in the Quantir Estate.[SOFTBLOCK] If you could find them, they might help you.[BLOCK][CLEAR]") ]])
				end
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Okay![SOFTBLOCK] Now I know what to do![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Unfortunately, it seems that your nature is your advantage.[SOFTBLOCK] If you could find a way to help me out of this cage, I would be grateful.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll see what I can do...") ]])
			end
		end
		
		--Common.
		fnCutsceneBlocker()
	
	--Mei has met with Claudia:
	else
	
		--If Mei has the hacksaw on her person/slime.
		if(iHasHacksaw == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Blessed child.[SOFTBLOCK] Has my moment of redemption come?[BLOCK]") ]])
				
			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Here you go\", " .. sDecisionScript .. ", \"Loan\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Nope\",  " .. sDecisionScript .. ", \"Nope\") ")
			fnCutsceneBlocker()
		
		--Mei doesn't have the hacksaw and didn't pick it up to begin with.
		elseif(iGotHacksaw == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Blessed child.[SOFTBLOCK] Is there anything else I may do to assist you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I might have an idea of how to get you out of here, but I need a minute.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: A minute or a decade, all are equal in the infinite sight of the Divine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (What a nut...)") ]])
		
		--Mei has freed Claudia.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Blessed child.[SOFTBLOCK] I must tarry and ease the suffering of those who died here.[SOFTBLOCK] Please, go on and find your way.") ]])
		end
	end
	
--Give Claudia the hacksaw.
elseif(sTopicName == "Loan") then
	
	--Remove the hacksaw.
	AdInv_SetProperty("Remove Item", "Hacksaw")
	VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N", 1.0)
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hold on...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Here you go, sorry it's a little - you know...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Truly you are a saint.[SOFTBLOCK] I must remain here a few more moments, though.[SOFTBLOCK] The dead here are restless, and I must perform their rites.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: If our paths cross again, I may yet have a reward for one as kind as you.[SOFTBLOCK] Please, be well.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (She's so nice it's kind of sickening...)") ]])
	fnCutsceneBlocker()
	
	--Clear Claudia's topic flag.
	WD_SetProperty("Clear Topic Read", "Claudia")
	
--Don't give Claudia the hacksaw.
elseif(sTopicName == "Nope") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: The look on your face is one of guilt.[SOFTBLOCK] Please do not feel that way.[SOFTBLOCK] Your choice is not yours, but of the Divine's.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I shall prove my worth by fasting here, with the dead.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: If I may aid you further, please return, child.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] You're not mad?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: I have much to atone for with my suffering.[SOFTBLOCK] When the Divine sees fit, I will be released, and no sooner.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Religious people sure are wacko...)") ]])
	fnCutsceneBlocker()
end
