--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMetCrowbarChan       = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetCrowbarChan", "N")
	local iGotSlimeDancersDress = VM_GetVar("Root/Variables/Chapter1/Scenes/iGotSlimeDancersDress", "N")
	
	--Dialogue.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChan", "Neutral") ]])
	
	--Haven't met her yet:
	if(iMetCrowbarChan == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Mei![SOFTBLOCK] Nyuuu![SOFTBLOCK] Good to see you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Well I -[SOFTBLOCK] I -[SOFTBLOCK] I don't remember taking any drugs...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] What, you don't recognize me?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I believe I'd remember a talking, floating crowbar.[SOFTBLOCK] So, no, I don't.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Oh, right.[SOFTBLOCK] This is adventure mode, isn't it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] I managed to score a cameo, nyuuu![SOFTBLOCK] I can break the fourth wall and nobody can do anything about it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] So maybe you should tell me how I'm supposed to know you, then?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Well, I came in second-to-last place during the popularity poll.[SOFTBLOCK] You came in first.[SOFTBLOCK] That's why Chapter One is about you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Chapter One?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] It's -[SOFTBLOCK] oh why bother.[SOFTBLOCK] We used to be best friends, but that was in Classic Mode.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Hopefully, once this is all over, we can all go out for drinks or something.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Sure.[SOFTBLOCK] Okay.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Nice seeing you again![SOFTBLOCK] Good luck on your adventure![SOFTBLOCK] Nyuuu![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Oh, and Salty said he'd like to apologize.[SOFTBLOCK] He said there was absolutely no excuse for this.[BLOCK][CLEAR]") ]])
		
		
		--Not in slime form:
		if(sMeiForm ~= "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Backing away slowly now...") ]])
		
		--In slime form:
		else
			VM_SetVar("Root/Variables/Chapter1/Scenes/iGotSlimeDancersDress", "N", 1.0)
			LM_ExecuteScript(gsItemListing, "Dress of the Slime Dancer")
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Backing away slowly now...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Now hold up a second, Mei![SOFTBLOCK] This is version 1.05![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (What is this magic crowbar talking about?)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] And that means I have a special present for nyuuu![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOUND|World|TakeItem](Received Dress of the Slime Dancer!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's a dress?[SOFTBLOCK] Uhm, I happen to like my body right now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] No, it's a slime dress![SOFTBLOCK] It's made specially for slime dancers![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Because when you do a twirl and splatter the audience, they tend to get upset for some reason.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, I see.[SOFTBLOCK] It merges with my body.[SOFTBLOCK] That's really cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] I knew you'd like it![SOFTBLOCK] Nyuu![SOFTBLOCK] Good luck!") ]])
		
		end
	
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMetCrowbarChan", "N", 1.0)
	
	--Have met her:
	else
	
		--Haven't received the slime dancer dress yet:
		if(iGotSlimeDancersDress == 0.0 and sMeiForm == "Slime") then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iGotSlimeDancersDress", "N", 1.0)
			LM_ExecuteScript(gsItemListing, "Dress of the Slime Dancer")
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Oh, Mei![SOFTBLOCK] There you are![SOFTBLOCK] Did you know it's Version 1.05?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (What is this magic crowbar talking about?)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] And that means I have a special present for nyuuu![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOUND|World|TakeItem](Received Dress of the Slime Dancer!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's a dress?[SOFTBLOCK] Uhm, I happen to like my body right now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] No, it's a slime dress![SOFTBLOCK] It's made specially for slime dancers![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Because when you do a twirl and splatter the audience, they tend to get upset for some reason.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, I see.[SOFTBLOCK] It merges with my body.[SOFTBLOCK] That's really cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] I knew you'd like it! Nyuu! Good luck!") ]])
	
		--Normal:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] I'm still kinda steamed about the popularity poll.[SOFTBLOCK] I heard Sanya got a big makeover, how come I didn't?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Because you're -[SOFTBLOCK] so cute already?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Nyuuu![SOFTBLOCK] You always know what to say!") ]])
		end
	end
end