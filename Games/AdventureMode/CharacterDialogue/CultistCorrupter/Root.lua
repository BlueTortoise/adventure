--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Get back to work, drone![BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Resist...\",  " .. sDecisionScript .. ", \"ResistFirst\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Resist...\",  " .. sDecisionScript .. ", \"ResistFirst\") ")
	fnCutsceneBlocker()

--Obey: Sooner or later, the player picks this:
elseif(sTopicName == "Obey") then

	--Clear any pending instructions.
	WD_SetProperty("Hide")
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Bee] Drone obeys.[SOFTBLOCK] Drone obeys.[SOFTBLOCK] Capture bees.[SOFTBLOCK] Convert bees.[SOFTBLOCK] Drone obeys.") ]])
	fnCutsceneBlocker()

--ResistFirst, part of the resistance sequence.
elseif(sTopicName == "ResistFirst") then

	--Clear any pending instructions.
	WD_SetProperty("Hide")
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Perhaps you needed some additional motivation?[SOFTBLOCK][CLEAR]") ]])
	fnCutsceneBlocker()
	
	--Screen overlay. Purple, fade down.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Heartbeat") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Over_GUI, false, 0.8, 0.2, 1.0, 1, 0, 0, 0, 0) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK]*Hatred and obedience surge from the handprint on your vagina...*[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Resist...\",   " .. sDecisionScript .. ", \"ResistSecond\") ")
	fnCutsceneBlocker()

--ResistSecond, part of the resistance sequence.
elseif(sTopicName == "ResistSecond") then

	--Clear any pending instructions.
	WD_SetProperty("Hide")
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Heh, again, then.[SOFTBLOCK][CLEAR]") ]])
	fnCutsceneBlocker()
	
	--Screen overlay. Purple, fade down.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Heartbeat") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Over_GUI, false, 0.8, 0.2, 1.0, 1, 0, 0, 0, 0) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK]*The print on your vagina surges.[SOFTBLOCK] Obedience rushes through you.[SOFTBLOCK] It fills you.[SOFTBLOCK] It is you.*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK]*You are a drone.[SOFTBLOCK] Drone obeys.*[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutsceneBlocker()

end
