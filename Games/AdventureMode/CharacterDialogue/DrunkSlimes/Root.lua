--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")

	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiTriedFruit = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiTriedFruit", "N")
	
	--If Mei has tried the fruit before:
	if(iMeiTriedFruit == 1.0) then
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Neutral") ]])
		
		--There is a 1/100 chance of this happening:
		local iRoll = LM_GetRandomNumber(1, 100)
		if(iRoll == 79 - 10) then --Der's a dinger for ya.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Hey there, good lookin'.[SOFTBLOCK] I got a bucket a' chicken.[SOFTBLOCK] Wanna do it?[BLOCK][CLEAR]") ]])
		
		--Normal.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Hey there, beautiful, want to bump membranes?[BLOCK][CLEAR]") ]])
		end
		
		--If Mei is a slime:
		if(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I don't even know if that makes sense![SOFTBLOCK] Just being around these fruits is making me tipsy...") ]])
		
		--If Mei is not a slime:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Are you hitting on that drunk slime?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You don't think her sheen is arousing?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] But you're not a slime![SOFTBLOCK] At least, not right now...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] All I have to do is give her a kiss and I will be...") ]])
		end
	
	--Mei hasn't tried the fruit:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](The slime gives you a dopey smile.[SOFTBLOCK] It looks...[SOFTBLOCK] unsteady...)") ]])
	
	end
end