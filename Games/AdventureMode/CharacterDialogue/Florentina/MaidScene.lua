--[Werecat Scene]
--If you check Florentina while she's sleeping during the werecat TF.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables:
	local iLydieLeftParty           = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
	local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Dialogue when Natalie is still a ghost:
	if(iLydieLeftParty == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Grrr![SOFTBLOCK] Mei![SOFTBLOCK] Unchain me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] The countess got some sort of wiggling houseplant.[SOFTBLOCK] How exotic![SOFTBLOCK] I wonder where she got that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I'm not a -[SOFTBLOCK] *glurg*[SOFTBLOCK] *ack*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Laugh] There you go, all watered up.[SOFTBLOCK] Weren't you a thirsty little one?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Stop -[SOFTBLOCK] *glarg*[SOFTBLOCK] I AM NO HOUSE PLANT![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] When I manage to get out of here...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Countess said that talking to plants makes them grow faster.[SOFTBLOCK] But, I have work to do.[SOFTBLOCK] Talk to you later, little lady.") ]])

	--Rescue Florentina:
	else
	
		--[Dialogue]
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Mei![SOFTBLOCK] Snap out of it and unchain me, damn it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Relax.[SOFTBLOCK] I can't get the lock when you're wiggling so much.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] You're in there again?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah.[SOFTBLOCK] I touched my runestone.[SOFTBLOCK] I'm fine now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Phew.[SOFTBLOCK] I hate being chained up![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So the runestone made you remember who you are?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Sort of.[SOFTBLOCK] I'm Natalie, too, but I think I just absorbed her memories.[SOFTBLOCK] Maybe her soul is in me now?[SOFTBLOCK] I don't know...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Natalie, huh?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well at least you're not treating me like a houseplant.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Sorry.[SOFTBLOCK] Whatever the curse is, it -[SOFTBLOCK] does things to the way you see the world...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So you're fine, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] I'm dead, Florentina...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But not fully?[SOFTBLOCK] The runestone...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're only dead if you believe you are.[SOFTBLOCK] Stay motivated.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I think the runestone is keeping me focused.[SOFTBLOCK] I can...[SOFTBLOCK] change back...[SOFTBLOCK] if I have to.[BLOCK][CLEAR]") ]])
		
		if(iFlorentinaKnowsAboutRune == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Oh really?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I can show you when we get to a safe spot.[SOFTBLOCK] I can feel it...[BLOCK][CLEAR]") ]])
		end
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] But I don't want to leave Natalie behind...[SOFTBLOCK] the poor girl didn't even get to see what life could really be...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] She's dead.[SOFTBLOCK] She's at peace.[SOFTBLOCK] Let it lie.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll just remember her, then.[SOFTBLOCK] And, we should see if we can help the other maids.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Ugh, ghostbusting...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I don't think I can talk you out of it, eh?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Not a chance!") ]])
		fnCutsceneBlocker()

		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--[Movement]
		--Florentina moves on to Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (6.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (6.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
		--[System]
		--Re-add Florentina to the lineup.
        fnAddPartyMember("Florentina")
		
		--Modify the flags. Ghost TF is off.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 0.0)

	end

end