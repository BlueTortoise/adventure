--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName  - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
-- 1: sIsFireside - Optional. If it exists, this was activated from the AdventureMenu at a save point. In those cases, the 
--                  Active Object is undefined.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName  = LM_GetScriptArgument(0)
local sIsFireside = LM_GetScriptArgument(1)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iSavedBeehive = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N")
	local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	local iCompletedQuantirMansion = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")
	local iHasSeenFlorentinaTwentyWin = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaTwentyWin", "N")
	
	--Special: If Florentina really likes Mei, unlock this topic:
	local iFlorentinaAdventure = 0.0
	if(iSavedBeehive == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
	if(iCompletedTrapDungeon == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
	if(iCompletedQuantirMansion == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
	if(iHasSeenFlorentinaTwentyWin == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
	if(iFlorentinaAdventure >= 3) then
		WD_SetProperty("Unlock Topic", "Past", 1)
	end
	
	--Change music to Florentina's theme.
	glLevelMusic = AL_GetProperty("Music")
	AL_SetProperty("Music", "FlorentinasTheme")
	
	--Position party opposite one another.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Florentina's introduction.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Yes, what is it?[BLOCK][CLEAR]") ]])

	--[Topics]
	--Activate topics mode once the dialogue is complete.
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Florentina") ]])
end
