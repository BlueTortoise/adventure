--[Werecat Scene]
--If you check Florentina while she's sleeping during the werecat TF.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Florentina takes slot 4.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[VOICE|Florentina] Zzzzzz...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (She's sleeping.[SOFTBLOCK] I never realized how beautiful Florentina is...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (The moonlight dances over her...[SOFTBLOCK] it's -[SOFTBLOCK] so exciting![SOFTBLOCK] maybe I should...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] (..![SOFTBLOCK] Florentina is sleeping with her knife in her hand!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (She must be really paranoid...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Better not try anything.[SOFTBLOCK] Wouldn't want her to stab me by mistake!)") ]])

end