--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Play a goat sound.
	local iRandomNumber = LM_GetRandomNumber(0, 3)
	AudioManager_PlaySound("World|Goat" .. iRandomNumber)
	
	--Increment the bother counter.
	local iGoatBotherCount = VM_GetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N")
	VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", iGoatBotherCount + 1.0)
	local iGoatBotherCountTotal = VM_GetVar("Root/Variables/Global/Goat/iGoatBotherCountTotal", "N")
	VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCountTotal", "N", iGoatBotherCountTotal + 1.0)
end
