--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Hypatia takes slot 5.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Hypatia", "Neutral") ]])
	
	--Variables.
	local iHasSeenHypatiaDiscountDialogue = VM_GetVar("Root/Variables/Chapter1/Hypatia/iHasSeenHypatiaDiscountDialogue", "N")
	if(iHasSeenHypatiaDiscountDialogue == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Hypatia/iHasSeenHypatiaDiscountDialogue", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: Hey boss, need some supplies?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do we get a discount?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] No.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] What?[SOFTBLOCK] But what if we need something important?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Then you'll have no problem stumping up the cash.[SOFTBLOCK] I'm not in this for the charity.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] *growl*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] How can I afford to pay Hypatia here a lavish salary if I'm giving goods away for free?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're taking food out of her mouth here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: Yeah![SOFTBLOCK] I may not get paid much but it's more than nothing![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] See?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Fine then.") ]])
	
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: What can I get for you?") ]])
	end
	fnCutsceneBlocker()

	--Run the shop.
	fnCutsceneInstruction([[ AM_SetShopProperty("Show", "Trannadar General Goods", gsRoot .. "CharacterDialogue/Hypatia/Shop Setup.lua", "Null") ]])
	fnCutsceneBlocker()
	
end
