--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Intercom state.
	local iIntercomState = VM_GetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N")
	
	--Basic setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])

	--If this is the first time talking to the intercom, unlock the airlock. Chris should be male in this.
	if(iIntercomState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Ah, you are operational.[SOFTBLOCK] Are you able to speak?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] *cou[SOFTBLOCK]gh*[SOFTBLOCK][SOFTBLOCK] who are you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] That's not relevant, but I am within normal cognitive parameters.[SOFTBLOCK] Your concern is noted.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I observed you in the airlock on the thermal sensors.[SOFTBLOCK] The optical camera feed has been cut for that area.[SOFTBLOCK] Are you able to proceed?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Could you please just give me a moment...[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I will need your assistance if we are to exit this facility in a timely manner.[SOFTBLOCK] I've sustained a great deal of material damage, but you seem to be in functional order.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I am in the control center at the moment.[SOFTBLOCK] I will unlock the airlock door from here, but I won't be able to get you much further unless you have a PDU.[SOFTBLOCK] One moment...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Ah, good.[SOFTBLOCK] It seems there is one in the EVA room.[SOFTBLOCK] Please obtain it and report to the nearest intercom for instructions.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Hey![SOFTBLOCK] What's going on, where am I, and who are you?[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] ...[SOFTBLOCK] I am unable to answer your questions at the moment, as I have the same ones.[SOFTBLOCK] I am unsure of how I got here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Perhaps if we are to meet up, we can pool our knowledge.[SOFTBLOCK] Please obtain the PDU.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] But [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Were the instructions not clear?[SOFTBLOCK] Please obtain the PDU in the next room, then report to an intercom.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (This lady's not letting up, but I can't get out of this room on my own.[SOFTBLOCK] Perhaps she was brought here like me?[SOFTBLOCK] Guess I better get that PDU, either way...)") ]])
		fnCutsceneBlocker()
	
	--2855 chides Chris and tells him to get to work.
	elseif(iIntercomState == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Please obtain the PDU in the next room, then report to an intercom.[SOFTBLOCK] I will have further instructions then.") ]])
		fnCutsceneBlocker()
	
	--Chris has the PDU.
	elseif(iIntercomState == 2.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 3.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] You have arrived.[SOFTBLOCK] The network is down and I am unable to reboot it, but the PDU should be able to provide access to the rest of the facility.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] This thing said I was on Regulus.[SOFTBLOCK] Where is that?[SOFTBLOCK] Where is Earth?[SOFTBLOCK] And who -[SOFTBLOCK] retired -[SOFTBLOCK] all those golems?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Please do not ask nonsense questions.[SOFTBLOCK] We will need to work together if we are to make it to Regulus City.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] The PDU said I should go there...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] That is where I intend to go.[SOFTBLOCK] I have numerous queries for the database.[SOFTBLOCK] We can therefore assist one another, but you need to stop asking questions and do as you are told.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Okay...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Good.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I can provide you access to the facility via your PDU now.[SOFTBLOCK] However, the doors are on lockdown.[SOFTBLOCK] You'll need special security cards to override them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] You will also need to locate several parts if we are to access Regulus City.[SOFTBLOCK] One moment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Diagnostics suggest we will need two motivators from a Doll unit, an ocular unit, and an authenticator chip.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] How do I - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Do not ask questions until directed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Your PDU should be able to extract the parts.[SOFTBLOCK] Make sure it scans the part before extracting it.[SOFTBLOCK] Damaged components will not work.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] You will need a security-red keycard to access the command deck...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I can't locate one.[SOFTBLOCK] Protocols suggest one should be on a command unit in that area.[SOFTBLOCK] Get searching.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Any questions?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] ...[SOFTBLOCK] No.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Good.[SOFTBLOCK] I will do what I can from here to assist you.[SOFTBLOCK] Get moving.") ]])
		fnCutsceneBlocker()
	
	--Chris has the PDU, repeat.
	elseif(iIntercomState == 3.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Do you need me to repeat any instructions?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Your current objective is to locate a security-red keycard.[SOFTBLOCK] One should be on a command unit in that area.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] You're also looking for two motivators, an ocular unit, and an authenticator chip.[SOFTBLOCK] Damaged parts will not do.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Now get moving.") ]])
	
	--Chris has the red keycard.
	elseif(iIntercomState == 4.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 5.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Do you have the red security card?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Yeah.[SOFTBLOCK] I found it on a dead...[SOFTBLOCK] doll...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Oh, then you should have scanned its parts.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] No good.[SOFTBLOCK] The PDU said they were useless.[SOFTBLOCK] That chip you wanted had been removed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] ...[SOFTBLOCK][SOFTBLOCK] Unfortunate.[SOFTBLOCK] The authenticator chip is the most important piece we need.[SOFTBLOCK] I am assuming neither you nor your PDU have one.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] ...[SOFTBLOCK] No?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] There should be more command units in the facility.[SOFTBLOCK] Plus, there is a blue security restriction on the command deck.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] The logs are incomplete, but they indicate that at least one blue-authorized unit was assigned to the containment area.[SOFTBLOCK] You'll need that security card.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] More importantly you need to find those parts.[SOFTBLOCK] Search thoroughly for them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Can't I just rest for a moment?[SOFTBLOCK] This place is a nightmare.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] You have higher concerns than that.[SOFTBLOCK] The longer you delay, the longer you're in this 'nightmare'.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Now, the containment area is on the west side past the red security door.[SOFTBLOCK] I'll unlock it for you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] When you're in there...[SOFTBLOCK] be quiet and don't waste time.[SOFTBLOCK] The power is out in that whole sector.[SOFTBLOCK] I can't guarantee the containment seals are still in place.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Just what does that mean?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Don't ask nonsense questions.[SOFTBLOCK] Just find that blue card and the parts we need.[SOFTBLOCK] Is your task clear?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] ...[SOFTBLOCK] Yes.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I obviously can't check the visible scanners, but I am assuming that area is dark due to the power outage.[SOFTBLOCK] There are a number of portable lights in that area.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] See if you can locate one, otherwise it will be difficult to navigate the containment area.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Now.[SOFTBLOCK] Get going.") ]])
	
	--Chris has the PDU, repeat.
	elseif(iIntercomState == 5.0) then
	
		--Variables.
		local iHasDollMotivatorA = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N")
		local iHasDollMotivatorB = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N")
		local iHasDollOcular     = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular",     "N")
	
		--Chris does not have the parts.
		if(iHasDollMotivatorA == 0 or iHasDollMotivatorB == 0 or iHasDollOcular == 0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I can repeat your instructions, if necessary.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] You need to locate a security-blue keycard.[SOFTBLOCK] One should be in the containment area in the northwest of the facility.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] You're also looking for two motivators, an ocular unit, and an authenticator chip.[SOFTBLOCK] Damaged parts will not do.[SOFTBLOCK] Scan any command units you find.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Now get moving.") ]])
		
		--Chris has the parts.
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 6.0)
			VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 2.0)
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] The thermal scope indicates an unusually high heat output.[SOFTBLOCK] Is something wrong?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Listen, some of these doll things got killed by something big.[SOFTBLOCK] Really big.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] So the thermal output is related to a fear response.[SOFTBLOCK] Suppress it.[SOFTBLOCK] Fear will make you make irrational choices and lead to failure.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] You have the keycard and the parts, correct?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Well, I got some of them.[SOFTBLOCK] But I couldn't find the chip you wanted.[SOFTBLOCK] All the dolls had theirs removed.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] ...[SOFTBLOCK] Unfortunate.[SOFTBLOCK] There are tactical advantages to be had removing the chips.[SOFTBLOCK] An intelligence is at work.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] We may yet be able to fabricate one.[SOFTBLOCK] The fabricators are past the green doors in the east end of the facility.[SOFTBLOCK] A blanked chip would do just as well as a recovered one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] So I'll - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Not yet.[SOFTBLOCK] It seems that area doesn't have enough power to open the doors.[SOFTBLOCK] I'll need to reroute power to the fabricators, so you'll have to find a way around.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I've given you access to the transit tunnels.[SOFTBLOCK] You should be able to find a way to the fabrication bays.[SOFTBLOCK] Go east from your position.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] If there are no further reasons to delay, get going.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (It's not worth the effort to argue with her...)") ]])
		end
		
	--Chris needs to go to the fabrication bay.
	elseif(iIntercomState == 6.0) then
	
		--Variables.
		local iSawRuinedFabricator = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N")
	
		--Hasn't seen the fabricator yet.
		if(iSawRuinedFabricator == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Your current assignment is to enter the fabrication bay.[SOFTBLOCK] You'll need to find a way in other than the main doorway.[SOFTBLOCK] Check the transit station on the east side of the facility.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Once there, fabricate an authenticator chip.[SOFTBLOCK] Then, report to the command center on the upper floor of the facility.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Get going.") ]])
		
		--Has seen it.
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 7.0)
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Umm...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Motion trackers in the fabrication bay indicate someone was in them.[SOFTBLOCK] Was it you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Yes...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] And do you have an authenticator chip?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] No.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] There appears to be a problem with the power supply to that area.[SOFTBLOCK] I will - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] The whole place was wrecked.[SOFTBLOCK] It looked like a bomb went off.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] You are certain?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Nothing was usable in there.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] *tap tap tap*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] It seems any record of the damage was removed from the central system.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Well, without a chip, we're stranded here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] We should meet.[SOFTBLOCK] Strategize.[SOFTBLOCK] We may yet be able to help one another.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I'll remove the lock on the command station's door.[SOFTBLOCK] It's next to the airlock on the command deck.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] I...[SOFTBLOCK] I will wait here for you.[SOFTBLOCK] Do not delay.") ]])
	
		end
	
	--Repeat.
	elseif(iIntercomState == 7.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|2855] Please proceed to the command station.[SOFTBLOCK] I removed the lock on the door.[SOFTBLOCK] It's next to the airlock on the command deck.") ]])
	
	--No answer.
    elseif(iIntercomState == 8.0) then
        local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
        if(iTalkedToSophie == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (No unit answering intercom.[SOFTBLOCK] Report error to Regulus City Administration, priority::[SOFTBLOCK] 8820355U-B2.[SOFTBLOCK] Resume current assignment.)") ]])
    
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (There is no answer on the intercom...)") ]])
        end
	end
end
