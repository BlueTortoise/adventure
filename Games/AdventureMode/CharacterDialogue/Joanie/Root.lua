--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Hypatia takes slot 5.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Bee", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Breanne", "Neutral") ]])
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a bee during this converation:
	if(sMeiForm == "Bee") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: (Sister Drone.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (Sister Drone.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: (We enjoy spending time with Breanne.[SOFTBLOCK] We remember Breanne.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (We enjoy Breanne's company.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: (We would like to convert Breanne.[SOFTBLOCK] We would enjoy her.[SOFTBLOCK] She would make a good drone.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (We agree.[SOFTBLOCK] She would be a good drone.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (We cannot convert Breanne.[SOFTBLOCK] We should not convert Breanne.[SOFTBLOCK] We must not convert Breanne.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: (We understand.[SOFTBLOCK] We will enjoy her company.[SOFTBLOCK] Thank you for reuniting us, Sister Drone.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (We are happy to help.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Are you guys talking or something?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Yes.[SOFTBLOCK] When our antennae are moving like that, it means we're talking.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Joanie won't talk to me verbally, but I guess I don't need her to.[SOFTBLOCK] It's just nice having her here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Joanie, you feel free to look for nectar in my garden.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I'll ask the hive to give her foraging preference for this area.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Thanks a lot, Mei.[SOFTBLOCK] This means a so much...") ]])
	
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: Bzzz.[SOFTBLOCK] (Sister Drone.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Zzz.[SOFTBLOCK] Bzzz.[SOFTBLOCK] (Hello Sister Drone. I lack antennae, but we may speak verbally.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Are you guys talking?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Yes.[SOFTBLOCK] I can...[SOFTBLOCK] I don't know how, but I understand her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: Zzz.[SOFTBLOCK] Zzzz.[SOFTBLOCK] (The hive has difficulty recognizing you. We apologize for any attempts to re-add you.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Bzzz.[SOFTBLOCK] Bzzz.[SOFTBLOCK] (I must remain like this for a while longer.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: How do you do that?[SOFTBLOCK] Can you teach me?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I guess a part of me is still a bee.[SOFTBLOCK] Maybe it'll be that way forever.[SOFTBLOCK] I don't think I could teach you, it's not like learning a language.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: We sort of understand each other's intentions.[SOFTBLOCK] I know what she's thinking because that's what I'd be thinking if I were her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I can tell you she's very happy to see you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Joanie...[SOFTBLOCK] I missed you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: She'll come and see you whenever she can.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Thanks a lot, Mei.[SOFTBLOCK] This means a so much...") ]])
	
	end
	fnCutsceneBlocker()
	
end
