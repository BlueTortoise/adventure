--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Karina takes slot 4.
	local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
	if(iSavedClaudia == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])
	
	--If Claudia was rescued, they move a bit.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Karina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Claudia", "Neutral") ]])
	end
		
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasFoundOutlandAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
	local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
	
	--If we've rescued Claudia but not met Karina before:
	if(iSavedClaudia == 1.0 and iHasFoundOutlandAcolyte == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 2.0)
		
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Claudia?[SOFTBLOCK] Is that you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: It is not by chance that we again meet, my child.[SOFTBLOCK] The divine lays our paths to cross this way.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: This is Sister Karina.[SOFTBLOCK] She is my assistant.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Pleasure to meet you![SOFTBLOCK] You must be the one who saved Sister Claudia![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: If there's anything I can do for you, let me know![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I was just surprised to see you out here.[SOFTBLOCK] Is everything going well?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: We begin the long path to redemption.[SOFTBLOCK] We must rebuild our convent and finish our holy task.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're pretty driven, I'm sure you'll be okay.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: The divine will watch over us, and you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Indeed.[SOFTBLOCK] I wish you luck on your path.") ]])
	
	--If we've rescued Claudia and have met Karina before, but don't have any bee stuff:
    elseif(iSavedClaudia == 1.0 and iHasFoundOutlandAcolyte == 2.0) then
    
        --If Mei is not in Bee form:
        if(sMeiForm ~= "Bee") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: Hello again, my child.[SOFTBLOCK] We will pray for your success.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Yeah![SOFTBLOCK] Blessings be upon you!") ]])
        
        --Mei is in bee form:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Mei![SOFTBLOCK] Could you answer a few questions about being a bee?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: It would do much to aid our research.[SOFTBLOCK] I only lament that the Doves call upon you again to aid us...[BLOCK][CLEAR]") ]])
            WD_SetProperty("Unlock Topic", "Karina_Claudia", 1)
            fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Karina") ]])
        end
        
	--If we've rescued Claudia and Karina knows about Mei being a bee:
	elseif(iSavedClaudia == 1.0 and iHasFoundOutlandAcolyte == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Mei![SOFTBLOCK] Could you answer a few questions about being a bee?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Claudia: It would do much to aid our research.[SOFTBLOCK] I only lament that the Doves call upon you again to aid us...[BLOCK][CLEAR]") ]])
		WD_SetProperty("Unlock Topic", "Karina_Claudia", 1)
		fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Karina") ]])
	
	--If we haven't met Karina yet, and Florentina is not present.
	elseif(iHasFoundOutlandAcolyte == 0.0 and bIsFlorentinaPresent == false) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 2.0)
	
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Excuse me, but what are you looking at?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Oh, hello![SOFTBLOCK] You must be new around here.[SOFTBLOCK] There's a bee hive across the river there, you can see the top of it from here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Are you worried about the bees?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Perish the thought![SOFTBLOCK] I'm actually researching them.[SOFTBLOCK] This spot is a pretty good vantage point.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Don't let me stop you, then.[SOFTBLOCK] Good luck.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Thanks!") ]])

	--If we haven't met Karina yet, but have already spoken to her once, the dialogue changes slightly. Doesn't unlock topics mode.
	elseif(iHasFoundOutlandAcolyte == 2.0 and bIsFlorentinaPresent == false and sMeiForm ~= "Bee") then
	
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: I've been watching those bees for a few weeks now.[SOFTBLOCK] They seem to be pretty agitated by something...") ]])

	--If Mei is a bee but we don't have Florentina present.
	elseif(iHasFoundOutlandAcolyte == 2.0 and bIsFlorentinaPresent == false and sMeiForm == "Bee") then
	
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Have you reconsidered my offer?[SOFTBLOCK] Want to give me an interview?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The hive is currently considering it.[SOFTBLOCK] I will let you know we reach consensus.") ]])

	--If we haven't met Karina yet, and Florentina is present.
	elseif(iHasFoundOutlandAcolyte == 0.0 and bIsFlorentinaPresent == true) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 1.0)
	
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Excuse me, but what are you looking at?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Oh, hello![SOFTBLOCK] You must be new around here.[SOFTBLOCK] There's a bee hive across the river there, you can see the top of it from here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Wait, Mei.[SOFTBLOCK] I think this is the one we're looking for.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Were you looking for me?[SOFTBLOCK] I assure you, my convent is completely peaceful.[SOFTBLOCK] We'll compensate you for any damages![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh yeah, this is her.[SOFTBLOCK] Are you with Sister Claudia's gang?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Yes.[SOFTBLOCK] My name is Sister Karina.[SOFTBLOCK] I apologize for any - [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Actually we're just looking for Claudia.[SOFTBLOCK] We heard she came this way.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Please, bounty hunters are unnecessary![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *Sheesh, these guys must get up to all sorts of things.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Can you just tell us where Claudia is?[SOFTBLOCK] We need her help with getting Mei here back to Earth.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Earth?[SOFTBLOCK] Never heard of it, but Sister Claudia probably has.[SOFTBLOCK] She has knowledge granted by the divine, you see.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Yeah, sure she does.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Unfortunately she is not here.[SOFTBLOCK] She took most of the convent to Quantir in search of a rare species of partirhuman, you see.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: She was to have returned by now, actually.[SOFTBLOCK] I hope she's all right.[SOFTBLOCK] She tends to get into trouble...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I suppose we're going out to Quantir, then.[SOFTBLOCK] Thanks for the help, Karina.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] That's east of here.[SOFTBLOCK] Lead the way.") ]])
		WD_SetProperty("Unlock Topic", "Partirhuman", 1)

		--Clear the flag on Next Move.
		WD_SetProperty("Clear Topic Read", "NextMove")

	--If we met Karina previously, and Florentina is now present:
	elseif(iHasFoundOutlandAcolyte == 2.0 and bIsFlorentinaPresent == true) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 1.0)
	
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I believe this is the one we're looking for.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Hello again![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I see you two are acquainted.[SOFTBLOCK] Good.[SOFTBLOCK] Then you'll have no problem telling us where Sister Claudia is.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Straight to the point, are you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] You'll have to excuse her.[SOFTBLOCK] My name is Mei, and this is my friend Florentina.[SOFTBLOCK] We're currently looking for someone who might know about Earth, or what this runestone is.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Well, then![SOFTBLOCK] I am Sister Karina, and you've come to the right place![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Unfortunately, Sister Claudia is not here.[SOFTBLOCK] She's gone over the hills to Quantir to look for a rare species of partirhuman.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: I'm afraid she's been away longer than expected.[SOFTBLOCK] I haven't heard from the convent at all, actually.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Great.[SOFTBLOCK] Let's go.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: In a hurry, I see![SOFTBLOCK] Do be careful.[SOFTBLOCK] I earnestly hope they found something interesting, because the alternative is much worse.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] We'll be on guard.[SOFTBLOCK] Thanks for your help.") ]])
		WD_SetProperty("Unlock Topic", "Partirhuman", 1)

		--Clear the flag on Next Move.
		WD_SetProperty("Clear Topic Read", "NextMove")

	--If we have met Karina and Florentina informed her of Claudia's goals, this plays:
	elseif(iHasFoundOutlandAcolyte == 1.0 and sMeiForm ~= "Bee") then
	
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Hello![SOFTBLOCK] Can I help you with something else?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] You've already helped quite a bit.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: If there's anything else the Heavenly Doves convent can do for you, please let us know.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Perhaps you would like a pamphlet...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] No![SOFTBLOCK] That's quite all right![SOFTBLOCK] Come on, Mei!") ]])

	--If we have met Karina and Mei is currently a Bee:
	else
	
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Karina: Mei![SOFTBLOCK] Could you perhaps answer a few questions about what it's like being a bee?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Karina") ]])

	end
end
