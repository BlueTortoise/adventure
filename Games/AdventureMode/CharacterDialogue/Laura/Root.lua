--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

	--Common.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])

	--Variables
	local iHasGhostForm = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
	
	--Mei does not have ghost form, so Laura does not even notice her.
	if(iHasGhostForm == 0.0) then
		
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (Seems this ghost hasn't even noticed me...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost: Yes yes, I'm busy. Go pester someone else!") ]])
	
	--Mei has ghost form.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hello...[SOFTBLOCK] Laura.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Laura: Oh![SOFTBLOCK] I'm sorry Natalie, I didn't see you there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Laura: Are you taking yet another half-hour break?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Uh, yeah.[SOFTBLOCK] That's it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Laura: Don't let anyone catch you.[SOFTBLOCK] And I'm not giving you another snack![SOFTBLOCK] The food is already running low![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (The food looks like it's ghostly, too...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I wasn't really all that hungry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Laura: You better get back to work.[SOFTBLOCK] Nice seeing you.") ]])
	
	end
end