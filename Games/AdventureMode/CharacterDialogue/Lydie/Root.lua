--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

	--Common.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])

	--Variables
	local iIsGhostTF        = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
	local iPutMaidOutfitOn  = VM_GetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N")
	local iToldNatalieTwice = VM_GetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N")
	local iCleaningProgress = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N")
	local iLydieLeftParty   = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
	
	--After the ghost TF is over:
	if(iIsGhostTF == 0.0 or (iIsGhostTF == 1.0 and iLydieLeftParty == 1.0)) then
	
		--Variables
		local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		local iCompletedQuantirMansion = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")
		
		--If Florentina is present, reorganize the dialogue.
		if(bIsFlorentinaPresent == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Null") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		end
		
		--If the mansion has been completed:
		if(iCompletedQuantirMansion == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Natalie.[SOFTBLOCK] No.[SOFTBLOCK] Not Natalie.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ..?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Am I -[SOFTBLOCK] dead?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I don't know how to say this...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: We're all dead.[SOFTBLOCK] You're not.[SOFTBLOCK] Right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Something is different.[SOFTBLOCK] You're not Natalie, but you look so much like her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: You freed us, didn't you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] The Warden...[SOFTBLOCK] The Countess...[SOFTBLOCK] They've moved on.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: I knew them, I think.[SOFTBLOCK] I -[SOFTBLOCK] this isn't my body, is it?[SOFTBLOCK] I'm sorry, whoever I was.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Natalie -[SOFTBLOCK] Mei.[SOFTBLOCK] You were Mei.[SOFTBLOCK] Please remember us.[SOFTBLOCK] Tell someone what happened here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I will.[SOFTBLOCK] You can rest easily.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Funny, isn't it?[SOFTBLOCK] Natalie was my best friend, and now, after all you've done, you are, too.[SOFTBLOCK] Thank you, Mei.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: I don't know how long until I fade away, but I can do it happily...") ]])
		
		--Normal:
		elseif(sMeiForm == "Ghost" and bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Oh hey, Natalie.[SOFTBLOCK] Everything all right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah.[SOFTBLOCK] Just wanted to see how you're doing.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Still some washing to do.[SOFTBLOCK] I'll catch up with you later!") ]])
		
		--Normal, with Florentina:
		elseif(sMeiForm == "Ghost" and bIsFlorentinaPresent == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Natalie, why are you lugging around a houseplant?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *Why is she calling me a houseplant?!*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *That's what you look like to them!*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I was just doing some...[SOFTBLOCK] redecorating.[SOFTBLOCK] Don't worry, the Countess said it was okay.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Oh, good.[SOFTBLOCK] This place could use some livening up.[SOFTBLOCK] Don't strain yourself, though.[SOFTBLOCK] That's a huge plant!") ]])
	
		--Non-ghost without Florentina:
		elseif(sMeiForm ~= "Ghost" and bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Jeez, Natalie, you look terrible![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, I'm fine.[SOFTBLOCK] My makeup is just a little off.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: If you say so.[SOFTBLOCK] Stay healthy, okay?[SOFTBLOCK] Last thing we need is you getting sick, too!") ]])
	
		--Non-ghost with Florentina:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Jeez, Natalie, you look terrible![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, I'm fine.[SOFTBLOCK] My makeup is just a little off.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Maybe you're allergic to that houseplant you're lugging around?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *Why is she calling me a houseplant?!*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *That's what you look like to them!*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, maybe.[SOFTBLOCK] I was just doing some redecorating.[SOFTBLOCK] The Countess said it was okay.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Well, all right then.[SOFTBLOCK] Just make sure you don't inhale too much pollen or you'll be sneezing all week!") ]])
	
		end
	
	--Natalie has not put the maid outfit on yet:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Quick, put the spare uniform on before someone sees![SOFTBLOCK] It's in the crate over there![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Right, right, okay!") ]])
	
	--Natalie has put the maid outfit on:
	elseif(iPutMaidOutfitOn == 1.0 and iToldNatalieTwice == 0.0 and iCleaningProgress < 9.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: What are you waiting for?[SOFTBLOCK] Get cleaning![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Don't gotta tell me twice!") ]])
	
	--Natalie has put the maid outfit on, and been told twice:
	elseif(iPutMaidOutfitOn == 1.0 and iToldNatalieTwice == 1.0 and iCleaningProgress < 9.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: What are you waiting for?[SOFTBLOCK] Get cleaning![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Don't gotta tell me twice![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Apparently I do, because I have told you twice![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Surprise] Ack![SOFTBLOCK] Don't scold me, Lydie!") ]])
	
	--Natalie is done cleaning:
	elseif(iCleaningProgress == 9.0) then
	
		--Flags.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCanLeaveRoomW", "N", 1.0)
		
		--Disable Lydie's collision and add her to Natalie's following list.
		gsFollowersTotal = 1
		gsaFollowerNames = {"Lydie"}
		giaFollowerIDs = {0}

		--Get Florentina's uniqueID. 
		EM_PushEntity("Lydie")
			local iLydieID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs = {iLydieID}
		AL_SetProperty("Follow Actor ID", iLydieID)
		
		--Disable collision.
		EM_PushEntity("Lydie")
			TA_SetProperty("Clipping Flag", false)
			TA_SetProperty("Activation Script", "Null")
		DL_PopActiveObject()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Wow, you really managed to pull it off![SOFTBLOCK] If you hadn't been slacking, I'd be tempted to tell the Countess how well you did![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Pah![SOFTBLOCK] She's never going to believe you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: And why not?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Happy] You're such a suck-up![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Am not![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Laugh] Are too![SOFTBLOCK] Ha ha![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: You're not normally such a slacker.[SOFTBLOCK] What happened?[SOFTBLOCK] Were you daydreaming again?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] I think so.[SOFTBLOCK] I had such a weird dream.[SOFTBLOCK] I dreamt I was some girl named Mei, from Hong Kong.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] I was such a great warrior![SOFTBLOCK] I had a sword and I escaped from some creepy cultists and - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Always the hero![SOFTBLOCK] You couldn't hurt a fly![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Blush] That's why we have dreams![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Have you ever had a dream like that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Oh, of course.[SOFTBLOCK] I once dreamt I was a researcher, looking into monster species.[SOFTBLOCK] I was writing about...[SOFTBLOCK] slimes, I think?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Well that's much more fitting.[SOFTBLOCK] You're always reading stuff![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: How come you don't?[SOFTBLOCK] I could teach you how to read, it's not hard.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] It's way too late for me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: It's never too late.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Sad] I don't think the Countess would approve.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Surprise] Oh, jeez.[SOFTBLOCK] I should really clean my clothes![SOFTBLOCK] They're covered in sweat![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Look at this...[SOFTBLOCK] grey and gold?[SOFTBLOCK] Where'd you get this outfit?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] (Hmm, was I wearing this in my dream?[SOFTBLOCK] And...[SOFTBLOCK][EMOTION|Mei|Surprise] a sword?[SOFTBLOCK] Why was I carrying that?)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Smirk] I don't know![SOFTBLOCK] I must have borrowed it from someone.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Come on, let's go clean it up before someone sees you.[SOFTBLOCK] The Countess said we've got to keep the manor spotless.") ]])
		fnCutsceneBlocker()
		
		--Move Natalie onto Lydie:
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei") --You spelled Natalie wrong, idiot!
			ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (8.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Fold Lydie into Natalie's party:
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
end