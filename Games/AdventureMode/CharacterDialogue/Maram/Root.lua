--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Nadia takes slot 5.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
	
	--Standard dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Maram: Greetings, Mei.[SOFTBLOCK] When you have spoken with Septima, I am to train you on scrying with the mirror.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Maram: I am also to serve as your sparring partner and companion.[SOFTBLOCK] I relish the opportunity.") ]])
end
