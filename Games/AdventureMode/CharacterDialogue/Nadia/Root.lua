--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Nadia takes slot 5.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
	
	--Standard dialogue.
	local iRoll = LM_GetRandomNumber(0, 100)
	if(iRoll < 25) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Mei![SOFTBLOCK] Good to see ya, what's up?[BLOCK][CLEAR]") ]])
	elseif(iRoll < 50) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Nice to see ya, Mei.[BLOCK][CLEAR]") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: What can I do for ya?[BLOCK][CLEAR]") ]])
	end

	--[Topics]
	--Activate topics mode once the dialogue is complete.
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Nadia")  ]])
end
