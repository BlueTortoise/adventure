--[Werecat Scene]
--If you check Nadia while she's sleeping during the werecat TF.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Nadia takes slot 4.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[VOICE|Nadia] Zzzzzz...[BLOCK][CLEAR]") ]])
	
	--Dialogue if Mei hasn't slept with Nadia yet:
	local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
	if(iMeiHasDoneNadia == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (She's sleeping.[SOFTBLOCK] She's...[SOFTBLOCK] so pretty...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Why am I so horny all of the sudden?[SOFTBLOCK] The moonlight[SOFTBLOCK] is making my skin tingle...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I could...[SOFTBLOCK] take her...[SOFTBLOCK] yes...[SOFTBLOCK] she would want it...)[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Take Her\", " .. sDecisionScript .. ", \"Take\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"Leave\") ")
		fnCutsceneBlocker()
	
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (How beautiful her form is...[SOFTBLOCK] but the night calls...)[BLOCK][CLEAR]") ]])
	end

--H-Scene with Nadia.
elseif(sTopicName == "Take") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Set flags.
	VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N", 1.0)
	
	--Don't set the topic if this is a relive case.
	local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
	if(iIsRelivingScene == 0.0) then
		WD_SetProperty("Unlock Topic", "Lover", 1)
	end
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ..![SOFTBLOCK] Mei?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: What are you doing?[SOFTBLOCK] S-[SOFTBLOCK]Stop![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Shhhhhhhh.") ]])
	fnCutsceneBlocker()
	
	--Scene.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei's fingers danced across Nadia's lithe, prone form.[SOFTBLOCK] Her armor, covered in the dirt of the day's travels, had been set aside, leaving the plant girl in her leafy underwear.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia attempted to protest, pushing against Mei, but her hand was limp, uncertain, and her arms grew weak.[SOFTBLOCK] Mei held firm as her fingers traced the contours of Nadia's form.[SOFTBLOCK] Nadia pressed back less each moment.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "'S-[SOFTBLOCK]stop...' Nadia pleaded.[SOFTBLOCK] Mei delicately traced her hands along Nadia's stiffening nipples, pausing only long enough to give each a delicate flick.[SOFTBLOCK] Nadia's protests soon ceased.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Now Mei lay down next to Nadia, her tongue following the paths her fingers had so recently danced along.[SOFTBLOCK] A final appeal began to rise from the plant girl's lips, but Mei silenced it with her tongue.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "As the two kissed and caressed one another, Mei pressed her body closer and felt Nadia's trembling fingers begin their own exploration of her body.[SOFTBLOCK] There was no hesitation, no withdrawl, just desire.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia's touch was passive and delicate, waiting for each move Mei made.[SOFTBLOCK] She would give only a token resistance before surrendering in the end.[SOFTBLOCK] A quiet moan was her only protest as Mei began to massage the Alraune's flowering sex.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei had masturbated many times before, and her fingers worked with an expert's touch.[SOFTBLOCK] Arousing her mate proved to be simple and enjoyable as she rubbed the firm bud of Nadia's clitoris with her fingers.[SOFTBLOCK] The first hint of nectar began to waft from the girl's body, its sweet scent encouraging Mei's fingers.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "'Aren't you going to stop me?' Mei asked with a sinister tone.[SOFTBLOCK] She smiled wickedly as Nadia meekly looked away.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei used her free hand to pull Nadia's head back, and kissed her again.[SOFTBLOCK] Her smooth, blue flesh tasted faintly of the perfume that all Alraunes emitted, and the scent of nectar caught in her breath.[SOFTBLOCK] Mei treasured the sensation.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Soon Nadia's hips began to move in time with Mei's fingers as she continued to massage the Alraune's sex, thrusting and drawing away in tempo with the gasping moans of her breath.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Reluctantly, Nadia's own fingers, still trembling along the edges of Mei's body, slid inward, resting for only a moment in the heat of Mei's engorged sex before making their first explorative plunge within.[SOFTBLOCK] Moonlight glinted in from a gap in the clouds, lighting up every nerve on Mei's skin as it danced across her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her awareness suddenly heightened, Mei increased her pace, and the Alraune gasped.[SOFTBLOCK] Nadia could not weather the sudden barrage of pleasure, and she grasped for her lover's hand.[SOFTBLOCK] Mei smiled as she allowed her to pleasure herself as she slid her own hands, slick with a hot nectar, to toy once more with Nadia's breasts.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Moaning, sweating, Nadia brought herself to orgasm, but Mei did not relent.[SOFTBLOCK] Taking pleasure in the vulnerable, exposed form of Nadia, her fingers continued to massage the girl's erect nipples.[SOFTBLOCK] She could see perfectly in the darkness.[SOFTBLOCK] She could see the interplay of light and shadow over Nadia's darkened, curvaceous form.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Only with great reluctance could Mei pry herself away.[SOFTBLOCK] Nadia had already fallen asleep after the intensity of her orgasm.[SOFTBLOCK] Mei stood and drew the blanket over the sleeping Alraune.[SOFTBLOCK] Something in the night was calling her, and she had to answer...") ]])
	
--Leave Nadia alone.
elseif(sTopicName == "Leave") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
end
