--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] Look there, Voidwalker.[SOFTBLOCK] That star that shines across the cleft of realities is one from your home.[SOFTBLOCK] Sirius, I believe you call it.[SOFTBLOCK] Does that comfort you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] It...[SOFTBLOCK] does, oddly.[SOFTBLOCK] I don't feel as lonely, now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] You are among friends here.[SOFTBLOCK] If we can help you feel at ease, we will do what we can.") ]])
end