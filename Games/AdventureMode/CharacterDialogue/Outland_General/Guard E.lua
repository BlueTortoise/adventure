--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	WD_SetProperty("Show")
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is in slime form:
	if(sMeiForm == "Slime") then
		WD_SetProperty("Append", "Merc:[VOICE|MercF] Be on your best behavior.")
	
	--Mei is in bee form:
	elseif(sMeiForm == "Bee") then
		WD_SetProperty("Append", "Merc:[VOICE|MercF] If you're a spy, you'll be dealt with.[SOFTBLOCK] Don't try anything.")
	
	--All other cases.
	else
		WD_SetProperty("Append", "Merc:[VOICE|MercF] I can't wait for this job to be over so I can go home and just relax.[SOFTBLOCK] I hate being paranoid.")
	
	end
end
