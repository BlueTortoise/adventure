--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	WD_SetProperty("Show")
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is in slime form:
	if(sMeiForm == "Slime") then
		WD_SetProperty("Append", "Merc:[VOICE|MercF] We're watching you.[SOFTBLOCK] Don't try to slime anyone.")
	
	--Mei is in bee form:
	elseif(sMeiForm == "Bee") then
		WD_SetProperty("Append", "Merc:[VOICE|MercF] Just because you got the go-ahead doesn't mean I trust you.[SOFTBLOCK] No sudden moves.")
	
	--All other cases.
	else
		WD_SetProperty("Append", "Merc:[VOICE|MercF] The bees have been agitated lately.[SOFTBLOCK] Be careful if you're going across the river.")
	
	end
end
