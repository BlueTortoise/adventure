--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	WD_SetProperty("Show")
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is in slime form:
	if(sMeiForm == "Slime") then
		WD_SetProperty("Append", "Merc:[VOICE|MercM] Talking slimes.[SOFTBLOCK] Weird, but I guess stranger things have happened.")
	
	--Mei is in bee form:
	elseif(sMeiForm == "Bee") then
		WD_SetProperty("Append", "Merc:[VOICE|MercM] Becoming a bee girl would be so great.[SOFTBLOCK] I wish I could just run away from it all...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[VOICE|Mei] You could.[SOFTBLOCK] We'd love to have you.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Merc:[VOICE|MercM] M-[SOFTBLOCK]maybe I should.[SOFTBLOCK] Maybe not.[SOFTBLOCK] I don't know...")
	
	--All other cases.
	else
		WD_SetProperty("Append", "Merc:[VOICE|MercM] Forest's a real dangerous place.[SOFTBLOCK] Be careful.")
	
	end
end
