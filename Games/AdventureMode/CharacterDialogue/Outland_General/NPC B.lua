--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Dialogue.
	WD_SetProperty("Show")
	WD_SetProperty("Append", "Farmer:[VOICE|MercM] What am I going to do with all this junk?[BLOCK][CLEAR]Farmer:[VOICE|MercM] Hey, want to buy some worthless crap?[BLOCK][CLEAR]Farmer:[VOICE|MercM] ...[BLOCK][CLEAR]Farmer:[VOICE|MercM] No harm in asking, right?")
end
