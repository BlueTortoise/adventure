--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Make the NPC face who is talking to them.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--If Mei is an Alraune:
	if(sMeiForm == "Alraune") then
	
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Greetings, leaf-sister.[SOFTBLOCK] I have been looking for those willing to be joined, but have found nothing here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You trust the humans who come through here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: I do not, nor need I.[SOFTBLOCK] There is a magical field which repulses those who would do harm.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Ask the innkeeper of it, she will tell you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Have you offered to join Breanne?[SOFTBLOCK] I think she would be a fine addition.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Mm.[SOFTBLOCK] She is not interested.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Perhaps when she takes a trip to the lakeside, we could ambush her.[BLOCK][CLEAR]") ]])
		
		--If Florentina is not present:
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: I will pass the idea to the others.[SOFTBLOCK] Do not underestimate her, she is a capable fighter.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] So much the better that we join her, then.[SOFTBLOCK] She might serve to protect the vulnerable.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Indeed.[SOFTBLOCK] It has been a pleasure, leaf-sister.") ]])
		
		--If Florentina is present:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Mei![SOFTBLOCK] What in the wastes is wrong with you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Florentina is not like us.[SOFTBLOCK] She may intervene.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You're damn right I will![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] S-[SOFTBLOCK]sorry Florentina.[SOFTBLOCK] I just -[SOFTBLOCK] I think she'd make a great leaf-sister.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Over my dead body![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: If her joining would bring such discord, we must forego it.[SOFTBLOCK] I will attempt to convince her further.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I hope you're successful.") ]])
		end
	
		--Topic unlock.
		WD_SetProperty("Unlock Topic", "PitStopMagicField", 1)
	
	--If Mei is a Bee:
	elseif(sMeiForm == "Bee") then
		fnStandardDialogue("Alraune:[VOICE|Alraune] Mm, little bee, I would love to nuzzle you, but I have tasks I must see to.[SOFTBLOCK] Perhaps later.")
	
	--If Mei is a ghost:
	elseif(sMeiForm == "Ghost") then
		fnStandardDialogue("Alraune:[VOICE|Alraune] An...[SOFTBLOCK] interesting one, you are.[SOFTBLOCK] Please, not too close...")
	
	--If Mei is a Slime:
	elseif(sMeiForm == "Slime") then
		fnStandardDialogue("Alraune:[VOICE|Alraune] Hello, little slime.[SOFTBLOCK] Aren't you cute?")
	
	--If Mei is a werecat:
	elseif(sMeiForm == "Werecat") then
		fnStandardDialogue("Alraune:[VOICE|Alraune] Ah, a hunter in the night![SOFTBLOCK] I hope your pursuits go well.")
	
	--Human.
	elseif(sMeiForm == "Human") then
	
		--Variables.
		local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
		local iMeiVolunteeredToAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N")
		local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
	
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Hello, human.[SOFTBLOCK] I have been seeking those who would walk with nature.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Perhaps you would be willing?[SOFTBLOCK] I can make arrangements, we would not need to go far.[BLOCK][CLEAR]") ]])
		
		--Mei has Alraune form:
		if(iHasAlrauneForm == 1.0) then
			
			--Set this flag.
			VM_SetVar("Root/Variables/Chapter1/Breanne/iMeiWillJoinSuitors", "N", 1.0)
			
			--Mei volunteered:
			if(iMeiVolunteeredToAlraune == 1.0) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] You don't recognize me, leaf-sister?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: L-[SOFTBLOCK]Leaf-sister Mei?[SOFTBLOCK] How is this possible!?[SOFTBLOCK] I was at your joining![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: ...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Ah, I see.[SOFTBLOCK] The little ones tell me you possess some sort of magic beyond my understanding.[BLOCK][CLEAR]") ]])
			
			--Involuntary:
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] My heart already beats in time with the forest.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Truly?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Ah, I see.[SOFTBLOCK] The little ones speak of you, leaf-sister Mei.[BLOCK][CLEAR]") ]])
			end
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Perhaps we can use this to the advantage of the wilds?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] How do you mean?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: The humans here implicitly distrust me, but they may put faith in you.[SOFTBLOCK] Tell them the benefits of our path.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I don't think they'll listen to me, but I could try.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] But, if someone elicits to walk with us...[SOFTBLOCK] May I be the one to join them?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: I would not deny you the honor.[SOFTBLOCK] You would certainly have earned it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I am merely a servant.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Good luck in your endeavours.") ]])
			fnCutsceneBlocker()
			
			--If Florentina is present, and she knows about the runestone.
			if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 1.0) then
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hey, Mei?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yes?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I normally wouldn't care about you tricking some fools into being joined.[SOFTBLOCK][EMOTION|Florentina|Happy] Hell, I might help if I thought it would be funny.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] But don't you think it's kinda sick?[SOFTBLOCK] It's not something they can undo.[SOFTBLOCK] They don't have the rune that you do.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I love what I am -[SOFTBLOCK] or, what I will be, once this is all over.[SOFTBLOCK] This human body is so...[SOFTBLOCK] imperfect...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I know that, once they feel it, they would never want to go back.[SOFTBLOCK] I'd feel no guilt.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] So then how do you explain me, huh?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do you want to become a human again?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] ...[SOFTBLOCK] No, but...[SOFTBLOCK] you wouldn't understand why.[SOFTBLOCK] It is...[SOFTBLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Painful?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Just, shut up, Mei.[SOFTBLOCK] Let's go.") ]])
			
			--If Florentina is present, but doesn't know about the runestone.
			elseif(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Mei, have you been sneaking hits off my pipe when I'm not looking?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The way you abuse that thing?[SOFTBLOCK] Just when is it not in your hands?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So if that's a no, what's this crap the little ones are saying?[SOFTBLOCK] Why are you acting all flowery?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Perhaps I'll show you when we reach a campfire...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You better.[SOFTBLOCK] I don't like being outside the loop.") ]])
			end
		
		--Mei does not have Alraune form:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: The joining process would be most pleasurable, I assure you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Joining?[SOFTBLOCK] Is that what it's called?[BLOCK][CLEAR]") ]])
		
			--If Florentina is not present:
			if(bIsFlorentinaPresent == false) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Yes.[SOFTBLOCK] Our most skilled leaf-sister will immerse you in a special mixture of pollen, water, and our magic.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: You will become one with the forest.[SOFTBLOCK] You will hear the whispers of the flowers.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Even now they call to you.[SOFTBLOCK] They wish to befriend you, for you to join their family.[BLOCK][CLEAR]") ]])
			
			--If Florentina is present:
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Yeah, no.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Don't let her sweet-talk you, Mei.[SOFTBLOCK] It's not all sunshine and rainbows.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: You, Florentina, have forsaken the gifts given to you.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Skepticism is healthy.[SOFTBLOCK] Mei, c'mon.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I don't know...[BLOCK]") ]])
			end
		
			--Dialogue option.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Be Joined\", " .. sDecisionScript .. ", \"BeJoined\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Nope\") ")
			fnCutsceneBlocker()
		end
	end

--Nope.
elseif(sTopicName == "Nope") then

	--Variables.
	local bIsFlorentinaPresent      = fnIsCharacterPresent("Florentina")

	--Clean.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	
	--Florentina is not present:
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I appreciate the offer, but I'll have to decline.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: It is the response I am accustomed to.[SOFTBLOCK] Very well.[SOFTBLOCK] If you change your mind, seek out a daughter of the wild.") ]])
		fnCutsceneBlocker()

	--Florentina is present, and knows about the runestone.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I appreciate the offer, but I'll have to decline.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Good call.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: It is the response I am accustomed to.[SOFTBLOCK] Very well.[SOFTBLOCK] If you change your mind, seek out a daughter of the wild.") ]])
		fnCutsceneBlocker()
	end

--Mei decides to become an Alraune.
elseif(sTopicName == "BeJoined") then

	--Variables.
	local bIsFlorentinaPresent      = fnIsCharacterPresent("Florentina")
	local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
	
	--Set flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N", 1.0)

	--Clean.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	
	--Florentina is not present:
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I...[SOFTBLOCK] want it...[SOFTBLOCK] I want to be happy...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I didn't have a lot of friends back home.[SOFTBLOCK] Not real friends.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Every blade of grass will be your friend when you are joined.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I'm just so nervous...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Whatever for?[SOFTBLOCK] You will be happy and secure.[SOFTBLOCK] You will not age or falter from disease.[SOFTBLOCK] You will be more than human.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Let's go.[SOFTBLOCK] Let's not waste any more time.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Stupendous![SOFTBLOCK] I will send word ahead.[SOFTBLOCK] Please, follow me.") ]])
		fnCutsceneBlocker()

	--Florentina is present, and knows about the runestone.
	elseif(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I...[SOFTBLOCK] want it...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Mei, have you lost your mind?[SOFTBLOCK] Think of what you're giving up![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It'll be all right.[SOFTBLOCK] Runestone, remember?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Yeah, I can't argue with that.[SOFTBLOCK] It's almost like you're running a con.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If I don't like it...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: You will love every moment of your life when you are joined.[SOFTBLOCK] I speak from experience.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] And you speak for yourself exclusively.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, let's go.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Wanderer Florentina...[SOFTBLOCK] you will not be present.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] ![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Why not?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: She is not like us.[SOFTBLOCK] She is uncleansed.[SOFTBLOCK] She may attempt to sabotage the process.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] You haughty scum![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] But...[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: It is the only way forward.[SOFTBLOCK] I am sorry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Florentina, would you...[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Yeah.[SOFTBLOCK] I'll wait outside when we get there.[SOFTBLOCK] As if I wanted to associate with your jerks anyway.") ]])
		fnCutsceneBlocker()

	--Florentina is present, and does not know about the runestone.
	elseif(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I...[SOFTBLOCK] want it...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Mei, have you lost your mind?[SOFTBLOCK] Think of what you're giving up![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ..?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not giving up anything.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Your humanity is at stake![SOFTBLOCK] You won't be you anymore![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, I will.[SOFTBLOCK] I can feel it.[SOFTBLOCK] I -[SOFTBLOCK] my runestone...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I can't explain it.[SOFTBLOCK] I'll show you.[SOFTBLOCK] Afterwards.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Please, come.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Lead the way.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Wanderer Florentina...[SOFTBLOCK] you will not be present.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] ![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Why not?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: She is not like us.[SOFTBLOCK] She is uncleansed.[SOFTBLOCK] She may attempt to sabotage the process.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] You haughty scum![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] But...[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: It is the only way forward.[SOFTBLOCK] I am sorry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Florentina, would you...[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Yeah.[SOFTBLOCK] I'll wait outside when we get there.[SOFTBLOCK] As if I wanted to associate with your jerks anyway.") ]])
		fnCutsceneBlocker()
	end
	
	--Fade to black.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(90)
	fnCutsceneBlocker()
	
	--Mini dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: New sister.[SOFTBLOCK] Please, breathe my pollen.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hmm?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Your body must be pliable.[SOFTBLOCK] This pollen will help.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Ooooh... I feel so...[SOFTBLOCK] sleepy...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(90)
	fnCutsceneBlocker()
	
	--Execute the Alraune cutscene.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Alraune/Scene_Begin.lua") ]])
end