--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiWillJoinSuitors   = VM_GetVar("Root/Variables/Chapter1/Breanne/iMeiWillJoinSuitors", "N")
	local bIsFlorentinaPresent  = fnIsCharacterPresent("Florentina")
	local iMeiKnowsAboutSuitors = VM_GetVar("Root/Variables/Chapter1/Breanne/iMeiKnowsAboutSuitors", "N")
	
	--Make the NPC face who is talking to them.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Normal case:
	if(iMeiWillJoinSuitors == 0.0 or iMeiKnowsAboutSuitors == 0.0) then
		fnStandardDialogue("Suitor:[VOICE|MercM] Hello there, beautiful...[SOFTBLOCK] *hic*")
	
	--Mei wants to join this guy:
	elseif(iMeiKnowsAboutSuitors == 1.0 and iMeiKnowsAboutSuitors == 1.0) then
	
		--Major dialogue:
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "MercM", "Neutral") ]])
	
		--If Mei is something other than human:
		if(sMeiForm ~= "Human") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Hello there, beautiful...[SOFTBLOCK] *hic*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well hello to you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Out of curiosity, you wouldn't be...[SOFTBLOCK] here for Breanne, would you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: 'Coursh.[SOFTBLOCK] Her folksh are loaded, and she's a mite pretty, too.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Well you know...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Shorry darlin *hic*, but I ain't into that.[SOFTBLOCK] I like my ladies to be, ladies, you know?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Dang it...)") ]])
			fnCutsceneBlocker()
	
		--If Mei is human:
		else
		
			--Flag.
			VM_SetVar("Root/Variables/Chapter1/Breanne/iMeiJoinedSuitor", "N", 1.0)
		
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Hello there, beautiful...[SOFTBLOCK] *hic*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well hello to you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Out of curiosity, you wouldn't be...[SOFTBLOCK] here for Breanne, would you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: 'Coursh.[SOFTBLOCK] Her folksh are loaded, and she's a mite pretty, too.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] (This guy's clearly drunk.[SOFTBLOCK] Probably wouldn't be that hard to lure him away and join him...)[BLOCK]") ]])
			
			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Join Him\", " .. sDecisionScript .. ", \"Join\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Leave Him\",  " .. sDecisionScript .. ", \"Leave\") ")
			fnCutsceneBlocker()
		end
	end

--Mei decides to join the idiot.
elseif(sTopicName == "Join") then

	--Variables.
	local bIsFlorentinaPresent  = fnIsCharacterPresent("Florentina")
	
	--Clean.
	WD_SetProperty("Hide")
	
	--Dialogue Setup.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	end
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "MercM", "Neutral") ]])
	
	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And you don't think I'm a 'mite pretty'?[SOFTBLOCK] I'm crushed![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Now I didn't shay that, missy.[SOFTBLOCK] In fact...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You like what you see?[SOFTBLOCK] Maybe if I lean over a bit?[BLOCK][CLEAR]") ]])
	
	--If Florentina is not present:
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] (What a sucker![SOFTBLOCK] Being drunk probably isn't hurting...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Oh yeah, just -[SOFTBLOCK] put your hand right there.[SOFTBLOCK] That's right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You don't need to think about any girl but me, right, handsome?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Ooh my...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] But not here.[SOFTBLOCK] Meet me at my cabin, it's just south of here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Y-[SOFTBLOCK]yeah![SOFTBLOCK] I'll be there![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Don't keep a girl waiting.[SOFTBLOCK] And...[SOFTBLOCK] bring a bit of wine for me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Sure thing, missy![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (What an idiot![SOFTBLOCK] This is too easy!)") ]])
		fnCutsceneBlocker()
		
	--If Florentina is present:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] *Mei, what are you doing?*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *What does it look like?*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *Something I'll be putting an end to!*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hey, wino.[SOFTBLOCK] She's - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Heh, no offense missy, but this lady over here has my attentions.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: When a man has options, he takes the best one.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] [SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK].[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] *You do your thing.[SOFTBLOCK] I'll be over here.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh yeah, just -[SOFTBLOCK] put your hand right there.[SOFTBLOCK] That's right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You don't need to think about any girl but me, right, handsome?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Ooh my...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] But not here.[SOFTBLOCK] Meet me at my cabin, it's just south of here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Y-[SOFTBLOCK]yeah![SOFTBLOCK] I'll be there![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Don't keep a girl waiting.[SOFTBLOCK] And...[SOFTBLOCK] bring a bit of wine for me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Shure thing, misshy!") ]])
		fnCutsceneBlocker()
		
		fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Sheesh.[SOFTBLOCK] The least they could do is make it a challenge.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'm a waitress.[SOFTBLOCK] I manipulate people for a living.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Manipulate?[SOFTBLOCK] All you did was turn to the side and he was practically drooling.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I happen to have a nice figure.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Now I better get to the cabin.[SOFTBLOCK] I've earned this joining.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, about that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You don't feel bad at all?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] He'll love being a daughter of the wilds.[SOFTBLOCK] I might even truly make love to my new leaf-sister...[SOFTBLOCK] she'll be so pretty...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I'll just sit this one out if it's all the same to you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Suit yourself.[SOFTBLOCK] I have a date!") ]])
		fnCutsceneBlocker()
	end
	
	--Fade to black.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
	fnCutsceneWait(180)
	fnCutsceneBlocker()
	
	--Remove the man actor.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "PliableMan")
		ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--Teleport Mei to the entrance of the Pit Stop.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Teleport To", (2.25 * gciSizePerTile), (14.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--If Florentina is present, teleport her as well.
	if(bIsFlorentinaPresent == true) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Teleport To", (16.25 * gciSizePerTile), (14.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneBlocker()
	end
	
	--Transform Mei into an Alraune.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Next dialogue sequence.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Alraune", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Alraune2", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "MercM", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Hey there, handsome.[SOFTBLOCK] Did you bring the wine?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Heh, you brought shome friendsh along?[SOFTBLOCK] I like that.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: He does not seem to realize the situation.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: Well why don't you jusht take a sheat over here, pretty thing?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This is a trap, stupid.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Suitor: What kind of trap?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[SOFTBLOCK] I shouldn't need to explain this.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] In fact, I don't have to.[SOFTBLOCK] Sisters?") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()

	--Scene.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The fool put up no resistance as the three wildflowers descended upon him.[SOFTBLOCK] As if by way of apology, Mei gave his crotch a brief stroke as their combined pollen put him to the edge of sleep.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "They dragged him into the concealed basement where the other Alraunes waited patiently.[SOFTBLOCK] Her sisters placed the barely conscious man near the edge of the pool, but left the real work to Mei.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei looked over her catch.[SOFTBLOCK] As a human, she may have found him attractive, but no longer.[SOFTBLOCK] Her libido cared only for the tranquility and beauty that came with subsumption by nature.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea, the most skilled leaf-sister at joining, waited now at the edge of the pool.[SOFTBLOCK] There was no need to speak.[SOFTBLOCK] Mei's instincts knew what to do.[SOFTBLOCK] She merely wished for Rochea to advise her and be near to her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She lifted his body and carried it effortlessly into the pool.[SOFTBLOCK] Somehow, here in the earth, surrounded by the spirits of the forest, her strength had grown far beyond what she or any other human could have achieved.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "With Rochea's hand steadying hers, she held the man in the pool, floating with him and keeping his head above the surface.[SOFTBLOCK] As she had before, she felt the warm, comforting embrace of the pool soak her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She now understood what was happening.[SOFTBLOCK] The pollen of millions of plants from all over the forest had been collected here, mixed with the magic of her sisters and water provided by the earth.[SOFTBLOCK] The collective dreams of millions of plants soaked her and her human protege.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "His skin had turned blue, and his body had become lithe, curvaceous, better than it was before.[SOFTBLOCK] She felt at his, [SOFTBLOCK]no,[SOFTBLOCK] her,[SOFTBLOCK] chest.[SOFTBLOCK] She was pleased with what her hands felt, as was her charge, who moaned softly.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea now put one hand on the new sister's head.[SOFTBLOCK] Mei put hers over Rochea's, and they both pressed the young one down into the fluid.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "They now had some time while the joining would complete without them.[SOFTBLOCK] Mei drew Rochea close to her and kissed her softly on the lips.[SOFTBLOCK] Rochea smiled at her and returned the favour.[SOFTBLOCK] Covered in Alraune magical fluid, the two joined their bodies as one.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "From near them in the fluid, a form stirred.[SOFTBLOCK] A young, beautiful face emerged from the waters.[SOFTBLOCK] She blinked as she adapted to her new eyes, drawing them around the room to take in the new sights.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When her eyes drew across Mei's, she thrust forward.[SOFTBLOCK] Rochea swam away to allow Mei's new sister to kiss her.[SOFTBLOCK] Her hands pushed through the fluid to find Mei's hips and caress her legs...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()

	--Scene.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rising from the pool together, Mei and her new leaf-sister continued to lock lips at each opportunity that presented itself.[SOFTBLOCK] She had never heard the man's name, and never cared to.[SOFTBLOCK] He was a distant memory, of no importance now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea emerged behind them and placed her hand on the new sister's back.[SOFTBLOCK] Her sister reached out a hand to grasp Mei's, and Mei held it gingerly.[SOFTBLOCK] Then, Rochea led the new sister away.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Next dialogue sequence.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Ahhh....[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: The cleansing has begun.[SOFTBLOCK] It will be three days before she is pure.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I'm so glad another walks the path with us.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: As are we all.[SOFTBLOCK] Mei, perhaps we were wrong about you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ..?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: When you refused to be cleansed, we had feared you were clinging to humanity.[SOFTBLOCK] It is clear that is not the case.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: While you are still not one of us, truly, we have no reason to doubt your motives.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I serve the forest with my whole being.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: I apologize for questioning you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Have you thought of a name for your new sister?[SOFTBLOCK] It is customary that you be allowed to suggest one, as you brought her here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Camellia.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: An interesting name.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's a species of tree from my home.[SOFTBLOCK] I played under one in the park after school.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: I am sorry.[SOFTBLOCK] It seems your home still calls to you.[SOFTBLOCK] I had hoped perhaps you would stay with us.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Whatever my destiny is, I hope it brings me back here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If not, then...[SOFTBLOCK] the least I could do is bring nature's blessing to Earth.[SOFTBLOCK] Perhaps join my own leaf-sisters there.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: If you cannot stay with us, we hope that you will find happiness.[SOFTBLOCK] Return to us any time.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Will leaf-sister Camellia remember me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: She will remember everything after she opened her true eyes for the first time.[SOFTBLOCK] You were the first thing she saw.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: I will tell her of your role in her joining, if you like.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thank you, Rochea.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Now, the road calls.[SOFTBLOCK] Until next time!") ]])
	fnCutsceneBlocker()
	
	fnCutsceneWait(180)
	fnCutsceneBlocker()
	
	--Overlay goes away.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	--If Florentina was not in the party:
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hmm, enough fun.[SOFTBLOCK] Back to work!") ]])
		fnCutsceneBlocker()
	
	--If Florentina was in the party.
	else
	
		--Florentina walks up.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (4.25 * gciSizePerTile), (14.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Hmmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Nice afterglow, kid.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] H-[SOFTBLOCK]huh?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] So you're all done?[SOFTBLOCK] Done tricking innocent people so you can get off on it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Hey...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Look, Mei.[SOFTBLOCK] I'm not really sure what I expected from you.[SOFTBLOCK] You seem all right most of the time.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I haven't known you very long, but I didn't think you were this way.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused][SOFTBLOCK] You're a predator.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I am -[SOFTBLOCK] I'm not![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You waited until you found a drunk, stupid man.[SOFTBLOCK] You took him someplace and had your way with him.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] It's not like that...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] She'll be happier this way...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You didn't even ask.[SOFTBLOCK] You just took it, from someone who wasn't even aware enough to say no.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] You're right...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Hmpf.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] This body...[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Don't you dare.[SOFTBLOCK] I've never seduced someone and joined them against their will.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So what's your excuse?[SOFTBLOCK] Or are you just a rotten person?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No.[SOFTBLOCK] No![SOFTBLOCK] What I did was right![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You're taking the side of humans because you still are one, at heart.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You live with them, you deal with them.[SOFTBLOCK] You trust them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] But you can trust them, because you are one![SOFTBLOCK] Well I'm not![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You can be any time you want to.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Out of necessity and nothing else![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I did what was right...[SOFTBLOCK][EMOTION|Mei|Sad] didn't I?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] From one perspective.[SOFTBLOCK] I'm telling you to look at the other ones.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] The worst part is that you're probably right.[SOFTBLOCK] She will be happier, probably.[SOFTBLOCK] But it's for the wrong reasons.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Why do the reasons matter?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Because if the ends justify the means, then there is no atrocity you won't do.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] There's a lot of people with body counts in the thousands for whom the end justified the means.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] We had them on Earth, too.[SOFTBLOCK] But they all had one common feature::[SOFTBLOCK] They were all stupid, violent, flawed...[SOFTBLOCK] human.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] There's just no getting through to you, is there?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I -[SOFTBLOCK] I won't do it again.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh?[SOFTBLOCK] Because I told you not to?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Because...[SOFTBLOCK] Because you might be right.[SOFTBLOCK] I need to think about it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I'm being pulled in so many different directions.[SOFTBLOCK] I would never have done this back home.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Is this world changing me, or was I always like this...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Buck up, kid.[SOFTBLOCK] You're under a lot of stress.[SOFTBLOCK] You'll figure it out.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] And then maybe you'll atone for what you did.[SOFTBLOCK] Lord knows I have...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ..?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Enough yakking.[SOFTBLOCK] Let's move out.") ]])
		fnCutsceneBlocker()
		
		--Florentina walks to Mei, fold the party.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (2.25 * gciSizePerTile), (14.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Start the music back up.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "BreannesTheme") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
	
--Mei decides to leave the guy alone.
elseif(sTopicName == "Leave") then
	WD_SetProperty("Hide")
end