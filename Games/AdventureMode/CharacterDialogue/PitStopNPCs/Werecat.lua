--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Make the NPC face who is talking to them.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--If Mei is not a werecat:
	if(sMeiForm ~= "Werecat") then
		fnStandardDialogue("Werecat:[VOICE|Werecat] Hss![SOFTBLOCK] Sod off!")
	
	--If Mei is a werecat:
	else
		fnStandardDialogue("Werecat:[VOICE|Werecat] Find your own fishing spot, kinfang.[SOFTBLOCK] This one is mine.")
	
	end

end