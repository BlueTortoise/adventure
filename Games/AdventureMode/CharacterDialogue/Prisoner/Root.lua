--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--[Setup]
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Prisoner takes slot 5.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Neutral") ]])
	
	--[Starting]
	--The first time Mei speaks to the prisoner, we get a longer scene that clears some flags.
	local iFirstTalkToPrisoner = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iFirstTalkToPrisoner", "N")
	local iFoughtCultist       = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iFoughtCultist", "N")
	if(iFirstTalkToPrisoner == 1.0) then
		
		--[Flags]
		VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iFirstTalkToPrisoner", "N", 0.0)
	
		--[Dialogue]
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: Help...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Hey hey, stay with me here.[SOFTBLOCK] What happened, where are we?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: They grabbed me, beat me up, tossed me in here...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Who's they?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: Don't know.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, sit tight.[SOFTBLOCK] I'll try to find a way out.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: Wait.[SOFTBLOCK] I found this.[SOFTBLOCK] Probably left by a previous occupant.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: My arm's broken, but maybe you can use it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Received Rusty Katana*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] A sword?[SOFTBLOCK] You want me to...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: They won't just let us go, you know.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (It always comes back to violence, doesn't it?)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Stay strong, you can do this!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: Aim for their eyes...[SOFTBLOCK] they really hate that...") ]])
		fnCutsceneBlocker()
		
		--[Item]
		--Give Mei a sword.
		LM_ExecuteScript(gsItemListing, "Rusty Katana")
	
	--[Second Time]
	--The prisoner changes her dialogue after you talk to her.
	elseif(iFoughtCultist == 0.0) then
	
		--[Dialogue]
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: I'll...[SOFTBLOCK] I'll be fine.[SOFTBLOCK] Go on without me...") ]])

	--[Beaten Cultist]
	--If you talk to the Prisoner after defeating the cultist...
	else
	
		--[Dialogue]
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: Go ahead, I'll catch up in a minute.") ]])

	end
end
