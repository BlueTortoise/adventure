--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Variables.
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    
    --Normal:
    if(iStartedShoppingSequence < 4.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You're reading the last page first?[SOFTBLOCK] Shame on you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Go over there and let the cutscene play out as it's supposed to.") ]])
	
    --Shopping completed.
    elseif(iStartedShoppingSequence == 4.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 5.0)
        VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWentToQuarters", "N", 0.0)
    
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] I assume you've completed your dalliances.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Nice to see you too, 55.[SOFTBLOCK] I should have gotten you a cup of oil.[SOFTBLOCK] Maybe that'd brighten your mood.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We really do need to talk about...[SOFTBLOCK] you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is nothing to talk about.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am a machine, and machines do not care about your feelings.[SOFTBLOCK] We will proceed regardless.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (She's backsliding...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Fine.[SOFTBLOCK] Then be a good robot and tell me why you called me here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have performed my initial probings of the situation surrounding the upcoming Gala.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] However, our preparatory work will take some time.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Just how many explosives do you need?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] My existing supplies are beyond adequate.[SOFTBLOCK][EMOTION|2855|Neutral] However, the civilian populace of Regulus City is in disarray.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have become a member of many of the small resistance cells that exist within the city.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] They...[SOFTBLOCK] already exist?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] There are hundreds, mostly existing of small groups of units.[SOFTBLOCK] Two or three per group, disgruntled and usually in direct verbal contact, often working in the same department.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Some of the larger groups attempt to coordinate the smaller groups via the network.[SOFTBLOCK] Arrests tend to follow when the groups are compromised.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In addition to poor encryption protocols, there are infiltrators and agents provacatuer from the security forces.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have done what I can to improve their communications.[SOFTBLOCK] Arrests are down by sixty percent.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And I assume you want to arm and train these groups?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] If the Sunrise Gala is to be our focal point, they must be ready.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So let's get to it, then![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your enthusiasm is most welcome, considering your frivolities.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Welcome sounds like there's an emotion behind it, perfect machine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Noted.[SOFTBLOCK] I will reconfigure my linguistic routines to be as trite as possible, since you seem to impart sentience upon them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Sigh*[BLOCK][CLEAR]") ]])
        
        --Variables.
        local iSawEquinoxMessage = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N")
        local iCompletedEquinox  = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
        local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
        local iSXUpgradeQuest    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        local iFinished198       = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
        
        --If any of these are true:
        if(iSXUpgradeQuest < 3.0 or (iSawEquinoxMessage == 1.0 and iCompletedEquinox == 0.0) or iCompletedSerenity == 0.0 or iFinished198 == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There are still other tasks we could undertake, as well.[BLOCK][CLEAR]") ]])
            
            --Handler:
            if(iSXUpgradeQuest < 3.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We may attempt to seek an alliance with the Steam Droids.[SOFTBLOCK] Their firepower and organization would serve our cause well.[BLOCK][CLEAR]") ]])
            end
            if(iSawEquinoxMessage == 1.0 and iCompletedEquinox == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Investigating the Equinox Laboratories may yield equipment or valuable information.[BLOCK][CLEAR]") ]])
            end
            if(iCompletedSerenity == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The Serenity Crater Observatory would serve as an excellent strong point.[SOFTBLOCK] An alliance with them is desirable.[BLOCK][CLEAR]") ]])
            end
            if(iFinished198 == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The unresolved situation in Sector 198 may be turned to our advantage if handled carefully.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Further, acquiring equipment for our own purposes will prove useful.[SOFTBLOCK] We should not discount that in favour of strategic objectives.[BLOCK][CLEAR]") ]])
        
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] While we have done an admirable job of obtaining allies, equipment, and information, there may yet be advantages to be had we do not know of.[BLOCK][CLEAR]") ]])
        
        end
    
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Therefore, I am leaving it to your discretion.[SOFTBLOCK] I will continue to assist you until you give the word.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Once you are satisfied with our preparations, I will be requiring all of your time in our task of arming the resistance groups.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (If I were playing a video game, I'd take that as a warning -[SOFTBLOCK] no going back once we start.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Unfortunately, this isn't a video game.[SOFTBLOCK][EMOTION|Christine|Laugh] Otherwise I'd be sure to make a new save!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I get you, 55.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] I will remain here.[SOFTBLOCK] If you exit the city, I will accompany you as usual.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Speak to me here whenever you would like to begin the next phase.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Will I even have time for dates with Sophie?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You will be maintaining your cover as a Lord Golem, however, I will be requiring your non-work time.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I get you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is one other thing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is about my sister.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Unit 2856...[SOFTBLOCK] I hope she's okay...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Do you maybe want to find her?[SOFTBLOCK] Reunite with her?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] I have already attempted to locate her, but was unable to.[SOFTBLOCK] She is 'off the grid', so to speak.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you come into contact with her, she is to be terminated.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] !!![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is the only witness who knows we were in the LRT facility.[SOFTBLOCK] Other than Project Vivify, that is.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The Drone Units stationed there lack the long-term memory capabilities to remember us, and I wiped their memory backup banks when I had core access.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] But she's your sister![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is also the unit most capable of destroying me, and you.[SOFTBLOCK] She knows who you are, what you are, where you work...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] To protect yourself, our goals, and your tandem unit, she must be eliminated.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Affirmative.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Now, move out.[SOFTBLOCK] Speak to me when you are ready to begin the next phase of our preparations.") ]])
        VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 0.0)
    
    --Asks about starting the next sequence.
    elseif(iStartedShoppingSequence == 5.0) then
    
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you prepared to begin the next phase of the plan?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is your final opportunity to acquire arms and allies before we begin.[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's Go\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
    end

--Start preparations.
elseif(sTopicName == "Yes") then
	WD_SetProperty("Hide")
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] Let us proceed.") ]])
    fnCutsceneBlocker()
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
    
    fnCutsceneWait(45)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    fnCutsceneWait(165)
    fnCutsceneBlocker()
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: And so, for the next two months, Christine and 2855 sought out new allies.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: They provided what they could to the aspiring rebels. Arms, training, software, whatever they could spare.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: The gala drew near, and Unit 2855 called a meeting to discuss their plans for the big night...") ]])
    fnCutsceneBlocker()

    --Next scene.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityX", "FORCEPOS:9.5x17.0x0") ]])
	fnCutsceneBlocker()

--Not quite yet.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Not just yet, 55.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If there was something else you needed to discuss, speak with me at a rest point in the field.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Affirmative.[SOFTBLOCK] Moving out.") ]])
end