--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Due to the Gala, our research projects are suspended for the evening.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] That makes sense from a security perspective, but they have the nerve to not even invite us to the Gala![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] What are we supposed to do for the evening?[SOFTBLOCK] Research the effects of frustration on a Lord Unit?") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My tandem unit is rather cross this evening.[SOFTBLOCK] We'll have to sneak onto the roof to watch the sunrise.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Greetings, Lord Units.[SOFTBLOCK] I'm plainclothes security.[SOFTBLOCK] I assure you my fashion sense is superb.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Most of our uniformed security units are Drone Units.[SOFTBLOCK] So of course, I got assigned to babysit them in the headquarters...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] They haven't set anything on fire yet, so that's a good thing.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] GREETINGS, LORD UNIT.[SOFTBLOCK] SECURITY STATUS:: SPIFFY.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaF") then
        
        --Variables.
        local iBumpedIntoLatexDrone = VM_GetVar("Root/Variables/Chapter5/Scenes/iBumpedIntoLatexDrone", "N")
        local iGotStationary        = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotStationary", "N")
        
        --Normal:
        if(iBumpedIntoLatexDrone == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] PATROLLING HALLWAYS.[SOFTBLOCK] PATROLLING HALLWAYS.[SOFTBLOCK] PATROLLING HALLWAYS...") ]])
            fnCutsceneBlocker()
        
        --Repeat:
        elseif(iBumpedIntoLatexDrone == 1.0 and iGotStationary == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You stay right here, drone. I'll go find something to write my complaing on.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] AFFIRMATIVE, LORD UNIT.") ]])
            fnCutsceneBlocker()
        
        --Got the stationary.
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSentComplaint", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ...[SOFTBLOCK] Unit bumped my tandem unit...[SOFTBLOCK] very displeased...[SOFTBLOCK] critical damages...[SOFTBLOCK] there.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Unit, deliver this complaint to your superior officer right now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] AFFIRMATIVE, LORD UNIT.[SOFTBLOCK] MOVING TO SUPERIOR OFFICER.[SOFTBLOCK] THANK YOU FOR YOUR COMPLAINT.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Christine", 20.75, 13.50)
            fnCutsceneMove("Sophie", 21.75, 13.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneFace("Sophie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneMove("GolemGalaF", 19.75, 12.50)
            fnCutsceneMove("GolemGalaF", 19.75,  4.50)
            fnCutsceneBlocker()
            fnCutsceneMove("Sophie", 20.75, 13.50)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
        
        end
        
    elseif(sActorName == "GolemGalaG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THE AREAS BEYOND HERE REQUIRE SECURITY PERMISSIONS TO ACCESS, LORD UNIT.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] PLEASE DO NOT WANDER OUTSIDE OF THE DESIGNATED ENJOYMENT AREA, LORD UNIT.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] FUN IS CURRENTLY WITHIN ACCEPTABLE LEVELS.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT IS ENJOYING THE MUSIC, AND PROTECTING THIS AREA.[SOFTBLOCK] IN ORDER OF PRIORITY.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] RECHARGING...[SOFTBLOCK] RECHARGING...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The drone has stuck its hand into the outlet powering the terminal...)") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaL") then
        
        --Standard.
        local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
        local iGotPrimeEarrings = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotPrimeEarrings", "N")
        if(iGalaMetPrimeA == 0.0 or iGotPrimeEarrings == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT BUMPED INTO A COMMAND UNIT.[SOFTBLOCK] PUNISHMENT IS TO REORGANIZE THE STATIONERY SUPPLIES HERE.") ]])
            fnCutsceneBlocker()
        
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGotPrimeEarrings", "N", 1.0)
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT BUMPED INTO A COMMAND UNIT.[SOFTBLOCK] PUNISHMENT IS TO REORGANIZE THE STATIONERY SUPPLIES HERE.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] SORTING SUPPLIES...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Wait a minute, this Drone is carrying some earrings...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Drone Unit, I am requisitioning those.[SOFTBLOCK] Give them to me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [SOUND|World|TakeItem]Got Earrings![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THANK YOU, LORD UNIT.[SOFTBLOCK] THIS UNIT COULD NOT FIGURE OUT WHERE TO SORT THOSE ITEMS.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Resume your sorting, Drone Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I guess I should return these to Prime Command Unit 1007...)") ]])
            fnCutsceneBlocker()
        
        end
        
    elseif(sActorName == "GolemGalaO") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] It's really hard to play cards when your processor is perfectly computing the statistics of the next draw...") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaP") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We have to stay in here until all the Lord Units leave and we can begin cleanup.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaQ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I guess this is as close to a sunrise party as I'm going to get.[SOFTBLOCK] This assignment bites.") ]])
        fnCutsceneBlocker()
        
    --GolemGalaM is the contact objective.
    elseif(sActorName == "GolemGalaM") then
    
        --Variables.
        TA_SetProperty("Face Character", "PlayerEntity")
        local iGalaMetContact      = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetContact", "N")
        local iGalaGotBug          = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGotBug", "N")
        local iGalaInstalledBug    = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaInstalledBug", "N")
        local iSawPostDance        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPostDance", "N")
        local iKnowToGetPermission = VM_GetVar("Root/Variables/Chapter5/Scenes/iKnowToGetPermission", "N")
        if(iGalaMetContact == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetContact", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Lord Golem?[SOFTBLOCK] May I help you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Possibly.[SOFTBLOCK] Have you noticed the odd electrical interference in this building?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] (Oh, the code phrase?)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Yes, but you can always enjoy the hum. Right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *We can't speak here, Pink Leader.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *I set up an audio looper in the break room, but the security units kicked us out to use it as their field headquarters.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *The looper is in the spare defragmentation tube.[SOFTBLOCK] Bring it to me.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ah, yes.[SOFTBLOCK] I'll recalibrate my PDU.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *We're on it.*") ]])
            fnCutsceneBlocker()
        
        --Met contact, don't have the bug yet.
        elseif(iGalaMetContact == 1.0 and iGalaGotBug == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *The bug is in the spare defragmentation tube behind the western break room.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Enjoy your night at the Gala, Lord Unit.") ]])
            fnCutsceneBlocker()
        
        --Has the bug. Need to install it.
        elseif(iGalaMetContact == 1.0 and iGalaGotBug == 1.0 and iGalaInstalledBug == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaInstalledBug", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal0State", "N", 1.0)
            AL_SetProperty("Flag Objective True", "0: Meet Contact")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *Click, click, click.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Sorry about that, Pink Leader.[SOFTBLOCK] The audio looper is in place.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Can we speak freely?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Yes.[SOFTBLOCK] The security units won't be able to use the microphones to listen to us, the looper will just play the audio from earlier today.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] They'll notice eventually, but probably sometime tomorrow.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Unfortunately, they've been pretty good at keeping me from doing any scouting for you.[SOFTBLOCK] Sorry.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We'll have to do the legwork, then.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] By the way, my lovely tandem unit here is Sophie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Pleased to meet you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh, what an enchanting dress...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Now, Lord Unit, I haven't been able to figure out who the acting security lead is for the gala.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] But I do have an idea::[SOFTBLOCK] They're using a lot of Drone Units.[SOFTBLOCK] And, Drone Units are pretty dumb.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Maybe you can trick one into revealing who the security lead is?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good idea.[SOFTBLOCK] I'll see what I can do.") ]])
            fnCutsceneBlocker()
        
        --Bug installed. Repeat conversation.
        elseif(iGalaInstalledBug == 1.0 and iSawPostDance == 0.0) then
        
            local iIdentifiedSecurityLead = VM_GetVar("Root/Variables/Chapter5/Scenes/iIdentifiedSecurityLead", "N")
            if(iIdentifiedSecurityLead == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Sorry, I can't do much else for you.[SOFTBLOCK] But you can ask me any questions you may have.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I would recommend trying to trick a Drone Unit into revealing who the security lead of the gala is.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Worth a shot.[SOFTBLOCK] I'll ask around.") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Sorry, I can't do much else for you.[SOFTBLOCK] But you can ask me any questions you may have.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't have any right now, but I'll keep that in mind.[SOFTBLOCK] Stay safe.") ]])
            end
        
        --Post dance.
        elseif(iSawPostDance == 1.0 and iKnowToGetPermission == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowToGetPermission", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're still clear to speak?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We are.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's about time we make our exit from the party.[SOFTBLOCK] It's almost two hours to sunrise.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I need to make my way to the physics research block.[SOFTBLOCK] Any advice?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] The research block is to the west of here.[SOFTBLOCK] You can take an underground passage, and I assume the security will be fairly light.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I don't know if you saw it, but I saw a lot of security units getting redeployed.[SOFTBLOCK] They ran through the middle of the hall there.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Maybe I shouldn't admit I was caught up dancing...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Didn't see it, but that might be a good thing.[SOFTBLOCK] Any idea where they got redeployed?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] No, I haven't a clue.[SOFTBLOCK] I'm still confined to this room.[SOFTBLOCK] I just peeked out into the hallway.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hopefully my path will be clear of security units...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But there's still a guard posted on the west side.[SOFTBLOCK] She'll definitely see if I try to leave.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh, uh well...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Y'see, Lord Unit,[SOFTBLOCK] there are rooms reserved for...[SOFTBLOCK] tandem units...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] It's not something we're supposed to talk about, but, y'know, cleanup duty...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I happen to have a tandem unit right here![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Shame we don't have time to, actually...[SOFTBLOCK] you know...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] You'd need to get special permission from someone.[SOFTBLOCK] Probably a security unit, though a Command Unit would have authorization.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] If you know someone high up who owes you a favour, now would be a good time to make use of it.[BLOCK][CLEAR]") ]])
            
            --Variables.
            local iReturnedPrimeEarrings = VM_GetVar("Root/Variables/Chapter5/Scenes/iReturnedPrimeEarrings", "N")
            if(iReturnedPrimeEarrings == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, Sophie![SOFTBLOCK] Wasn't Prime Command Unit 1007 looking for something near the shoe rack?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I think so![SOFTBLOCK] If we help her out, maybe she'll help us out![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Good thinking.[SOFTBLOCK] Good luck, Lord Unit.[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Actually, Unit 1007 owes me one.[SOFTBLOCK] Let's go talk to her.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] She's probably still out front.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Good luck, Lord Unit.[BLOCK][CLEAR]") ]])
            end
            
            --Return.
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But what about you, friend?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, yeah.[SOFTBLOCK] Do you know what's going to happen?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Just that I need to make sure my crew is out of the building at the moment of sunrise.[SOFTBLOCK] Don't worry, we have a plan.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I'm going to claim that my crew needs to get some cleaning supplies.[SOFTBLOCK] We'll leave on the tram.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Whatever is going to happen, well, we won't be here to see it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good.[SOFTBLOCK] I guess we won't see each other again.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Not until Regulus City is a free place.[SOFTBLOCK] Once again, good luck, free machine sister.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Same to you, free machine sister.") ]])
            fnCutsceneBlocker()
            
        --Repeat.
        elseif(iKnowToGetPermission == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] You should get a higher-up to give you permission to use the reserved rooms on the west side of the building.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We'll meet again when the sun shines over free Regulus, machine sisters.") ]])
            fnCutsceneBlocker()
        
        end
        
    elseif(sActorName == "DroneSpecial") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] AWAITING PUNISHMENT FROM SUPERIOR OFFICER.") ]])
        fnCutsceneBlocker()
        
    --CommandGalaA is one of the prime command units.
    elseif(sActorName == "CommandGalaA") then
    
        --Variables.
        TA_SetProperty("Face Character", "PlayerEntity")
        local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
        local iKnowToGetPermission = VM_GetVar("Root/Variables/Chapter5/Scenes/iKnowToGetPermission", "N")
        local iHasPermission = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPermission", "N")
        
        --First meeting.
        if(iGalaMetPrimeA == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N", 1.0)
            
            --If all three primes are identified, flag this.
            local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
            local iGalaMetPrimeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
            local iGalaMetPrimeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N")
            if(iGalaMetPrimeA == 1.0 and iGalaMetPrimeB == 1.0 and iGalaMetPrimeC == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N", 1.0)
                AL_SetProperty("Flag Objective True", "1: Identify Prime Command Units")
            end
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good evening, Command Unit.[SOFTBLOCK] Are you admiring the shoe rack?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Nothing of the sort, Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I misplaced my earrings and have been unable to locate them.[SOFTBLOCK] If I don't find them soon, I might have to divert these Drone Units to help me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unfortunate, but understandable.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Have we met?[SOFTBLOCK] I am searching my archives...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't believe so, Command Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] T-t-[SOFTBLOCK]this...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] What is it, Lord Unit?[SOFTBLOCK] Out with it![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] F-first...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is our first year at the gala.[SOFTBLOCK] Andrea here is a little nervous.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Apologies, Command Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] (I hope she doesn't look too closely!!!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] There is something unusual about you, isn't there?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ..?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Hah![SOFTBLOCK] I see it now![SOFTBLOCK] It's because I am a Command Unit![SOFTBLOCK] I assume I am the first you've seen?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] We're not ogres like many of the lower Lords make us out to be, I assure you.[SOFTBLOCK] Yes, we are intolerant of failure, but we also seek to reward good performance.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Even I was nervous my first year, Andrea.[SOFTBLOCK] Do your best and you will overcome it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Just remember, you are a Lord Unit.[SOFTBLOCK] You must set an example for others, even other Lord Units.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And I am Christine, of Sector 96.[SOFTBLOCK] Pleased to meet you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Hmmm, Sector 96?[SOFTBLOCK] It's at the top of my priority search lists.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] You are the Lord Unit that has made those impressive gains in productivity.[SOFTBLOCK] Suiting that you'd receive an invitation.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] I am Unit 1007.[SOFTBLOCK] I am the Prime Command Unit of Transportation.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] It![SOFTBLOCK] Is is...[SOFTBLOCK] It is an honor, Prime Command Unit![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Oh stop, you're going to embarass me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Transportation is not glorious like Security or Research or even Abductions, but it is no less vital to the operation of the city.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Though no amount of authority is going to bring back my wayward earrings...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Is it all right if I rant at you, Christine and Andrea?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Go right ahead, Prime Command Unit 1007![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] This Gala has been a pain in the rear-input port to me, if you take my meaning.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Not allowed to transmit anything over the network, yet somehow supposed to arrange for transport of hundreds of units using a single track?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] And I've spoken with the other Prime Command Units.[SOFTBLOCK] Apparently none of them issued the order to not use the network.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] With Unit 2855 missing, our security is a real mess.[SOFTBLOCK] Despite our frequent differences...[SOFTBLOCK] well, she was a Prime Command Unit for a reason![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Wait a minute, even Unit 1007 doesn't know who gave the network-silence order?)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (What does this mean?)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Unit 2855 is missing?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Mmm, forget I said that.[SOFTBLOCK] It's not public knowledge.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] But honestly, I don't think it's much of a secret anymore.[SOFTBLOCK] Any unit with two processor cycles can figure out something is wrong in the city.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Then again, perhaps I have vented on you poor units long enough.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Please enjoy the Sunrise Gala, and continue putting in excellent productivity.[SOFTBLOCK] Make sure I see you at the Sunset Gala, Christine and Andrea![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Absolutely, Prime Command Unit.[SOFTBLOCK] We'll be there...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Okay, I've identified a Prime Command Unit.[SOFTBLOCK] Making great progress!)") ]])
        
        --Repeats.
        else
        
            --Variables.
            local iGotPrimeEarrings = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotPrimeEarrings", "N")
            local iReturnedPrimeEarrings = VM_GetVar("Root/Variables/Chapter5/Scenes/iReturnedPrimeEarrings", "N")
        
            --No earrings:
            if(iGotPrimeEarrings == 0.0 and iReturnedPrimeEarrings == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Where are those blasted earrings?[SOFTBLOCK] My kingdom for my earrings![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Well not literally, but you know what I mean.") ]])
        
            --Return them.
            elseif(iGotPrimeEarrings == 1.0 and iReturnedPrimeEarrings == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iReturnedPrimeEarrings", "N", 1.0)
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Prime Command Unit, are these yours, perchance?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] My earrings![SOFTBLOCK] Where did you find them?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] A Drone Unit had them and thought they were stationery pins.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] THAT BLASTED -[SOFTBLOCK] Grrrr...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] I can't blame a Drone Unit for being stupid, it's my fault for not noticing.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Thank you for your help, Christine.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] I notice that Andrea is still with you.[SOFTBLOCK] Are you perhaps tandem units?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We are.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] I see.[SOFTBLOCK] Andrea, you are very lucky.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I know, Prime Command Unit![SOFTBLOCK] She's very special![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] I suppose I owe you a favour, Christine.[SOFTBLOCK] Enjoy the evening.[BLOCK][CLEAR]") ]])
                
                --Get permission for fun!
                if(iKnowToGetPermission == 1.0) then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPermission", "N", 1.0)
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Actually...[SOFTBLOCK] how do I put this...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] It's hard to find a place to be alone with my tandem unit...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Ah ha![SOFTBLOCK] Oh, how lucky you are![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] You don't know the half of it...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] There are some rooms on the west side reserved for those who want to watch the sunrise in more private accomodations.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Usually they're reserved for high-ranking units, but I think you may be able to make use of the supply room.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Oooh...[SOFTBLOCK] that's actually better than a formal room...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] H-[SOFTBLOCK]hey![SOFTBLOCK] Mind out of the gutter, Christine![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] I'll send the notification with my PDU.[SOFTBLOCK] Just use the doors on the west side.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Good evening, Lord Units.[SOFTBLOCK] Don't be so occupied that you miss the sunrise! Wah ha ha!") ]])
                
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We will, Prime Command Unit.") ]])
                end
            
            --Repeats.
            elseif(iReturnedPrimeEarrings == 1.0) then
            
                if(iKnowToGetPermission == 1.0 and iHasPermission == 0.0) then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPermission", "N", 1.0)
                    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Errr, Prime Command Unit, we have a favour to ask...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Mm?[SOFTBLOCK] You know how to play the game well, I see.[SOFTBLOCK] Get in good graces with a superior, and then use the favour for advancement?[SOFTBLOCK] You're ambitious![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Well, it's not social climbing I seek...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] How do I put this...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] It's hard to find a place to be alone with my tandem unit...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Ah ha![SOFTBLOCK] Oh, how lucky you are![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] She's very pretty, and I hope she's an intellect to match![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I have a hard time choosing which I like most...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Me too, actually![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] There are some rooms on the west side reserved for those who want to watch the sunrise in more private accomodations.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Usually they're reserved for high-ranking units, but I think you may be able to make use of the supply room.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Oooh...[SOFTBLOCK] that's actually better than a formal room...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] H-[SOFTBLOCK]hey![SOFTBLOCK] Mind out of the gutter, Christine![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] I'll send the notification with my PDU.[SOFTBLOCK] Just use the doors on the west side.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Good evening, Lord Units.[SOFTBLOCK] Don't be so occupied that you miss the sunrise! Wah ha ha!") ]])
            
                else
                
                    --Variables.
                    local iDancedWithSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iDancedWithSophie", "N")
                
                    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] I'm looking forward to seeing you two dance in the main hall.[SOFTBLOCK] I'm sure you'll be most graceful.[BLOCK][CLEAR]") ]])
                    if(iDancedWithSophie == 0.0) then
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] We Prime Command Units have scarcely a moment to ourselves, and thus rarely have tandem units.[SOFTBLOCK] Enjoy one another's company in our stead.") ]])
                    else
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sorry, Prime Command Unit, but we just came from there.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "1007:[E|Neutral] Oh, did I miss it?[SOFTBLOCK] I'd miss my own funeral at this rate.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Oh, I wouldn't say that...)[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll be attending to other things this evening.[SOFTBLOCK] Be seeing you.") ]])
                        
                    end
                end
            end
        end
    end

end