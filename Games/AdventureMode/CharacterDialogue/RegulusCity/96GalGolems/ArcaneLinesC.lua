--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Organic hors d'oeuvres![SOFTBLOCK] Mmm![SOFTBLOCK] Perfect!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Don't tell anyone, but these organic foods are basically Raiju rations that are seasoned.[SOFTBLOCK] I used to work in the biolabs, I saw it myself.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] There's a viewscreen behind the curtain that will show the sunset in a few hours.[SOFTBLOCK] I'm staking my spot now!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My goodness, who are you wearing?[SOFTBLOCK] You look fantabulous!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My tandem unit is embarassed because we bought the same dress.[SOFTBLOCK] But it's so strange, I went into one store, and she went into the one across the hall.[SOFTBLOCK] How did we come out with the same outfit?") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The organic sweets this year aren't as good as usual.[SOFTBLOCK] We've all seen chocolate and caviar, bring on the pizza, I say!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I suppose one advantage of being assigned to security is that I get to attend the Gala.[SOFTBLOCK] I've never received an invitation, but I always attend.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaH") then
        
        --Variables.
        local iSentComplaint          = VM_GetVar("Root/Variables/Chapter5/Scenes/iSentComplaint", "N")
        local iIdentifiedSecurityLead = VM_GetVar("Root/Variables/Chapter5/Scenes/iIdentifiedSecurityLead", "N")
        
        --Normal.
        if(iSentComplaint == 0.0 or iIdentifiedSecurityLead == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Did you need something, Lord Unit?[SOFTBLOCK] I'm busy.") ]])
            fnCutsceneBlocker()
        
        --Identify the security leader!
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIdentifiedSecurityLead", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal2State", "N", 1.0)
            AL_SetProperty("Flag Objective True", "2: Identify Security Head")
            
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "LatexDrone", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Goodness, what is it now?[SOFTBLOCK] What minor offense have you committed?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] THIS COMPLAINT IS TO BE HAND DELIVERED TO MY SUPERIOR OFFICER.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] ...[SOFTBLOCK] Bumped into a Lord Unit...[SOFTBLOCK] Very upset...[SOFTBLOCK] Oh dear...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Tch.[SOFTBLOCK] It's not my idea to use Drone Units, this is all I've been allocated.[SOFTBLOCK] Stupid drone![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I'll figure out your punishment later.[SOFTBLOCK] Stand there quietly while I update my PDU...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh, hello, Lord Unit.[SOFTBLOCK] Did you need something?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, no.[SOFTBLOCK] Just visiting.[SOFTBLOCK] I thought this room might be open.[SOFTBLOCK] Who might you be?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Lord Unit 592311, Lord Golem of Engineering, Sector 200.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] *Though at the moment, I am Lord Golem of Being Stressed Out by Idiot Subordinates, Sector 0.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] This room is a private room.[SOFTBLOCK] Not to be rude, but I'm rather busy at the moment.[SOFTBLOCK] Come back later.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Yeah that pretty much confirms that this is the local security chief here.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (I assume her designation is correct, but she doesn't talk like an engineer.[SOFTBLOCK] Probably a cover story.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Objective completed.[SOFTBLOCK] On to the next one.)") ]])
            fnCutsceneBlocker()
        
        end
        
    elseif(sActorName == "GolemGalaI" or sActorName == "GolemGalaJ") then
        
        --Variables.
        local iGotBullied = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotBullied", "N")
        
        --First time.
        if(iGotBullied == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGotBullied", "N", 1.0)
            
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "GolemFancyE", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "GolemFancyF", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] Who do we have here?[SOFTBLOCK] A pretender?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Eep![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] Tch, at least this one knows her place.[SOFTBLOCK] You, apparently, don't.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] What are you two on about?[SOFTBLOCK] We haven't even said anything yet![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] Oh, you don't need to say a thing.[SOFTBLOCK] We can smell it on you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] You stink like a repair bay.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] B-but...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] Oh it's talking![SOFTBLOCK] Look at it go![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You leave my tandem unit alone.[SOFTBLOCK] You have a problem, you bring it to me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] I think I upset the butch one.[SOFTBLOCK] Ha ha![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] Bah.[SOFTBLOCK] Listen up, pup.[SOFTBLOCK] I'll give you some advice, since you clearly don't belong here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] Units like you who apparently work with their hands?[SOFTBLOCK] Those hands with microscopic traces of lubricant?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] You give the rest of us a bad name.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] I'm not sure I can keep this up.[SOFTBLOCK] The stink is going to get on my dress.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] I'm a repair unit.[SOFTBLOCK] Your vocal synthesier is going to need maintenance if you don't shut the hell up![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] Oh please, don't strike with me with your big man-hands![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] Wah ha ha![SOFTBLOCK] Excellent![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Let's just go.[SOFTBLOCK] C'mon.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] I'll rip your head off and shove it down your tandem unit's oral intake![SOFTBLOCK] I'll -[SOFTBLOCK] I'll!!![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] Picking a fight with two Lord Units who outrank you?[SOFTBLOCK] I wonder what security will think.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] Be a shame to get kicked out of your first and soon to be only Gala, wouldn't it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] Do be a dear and kindly taff off.[SOFTBLOCK] Perhaps you can aid the cleaning units when your superiors are done enjoying themselves.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] *It's not worth it.[SOFTBLOCK] C'mon, Christine.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] *Okay, okay.[SOFTBLOCK] Can't lose my cool now.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] We'll be taking our leave now, Lords.[SOFTBLOCK] It'd be uncouth to keep inconveniencing you so.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Gosh, I remember my first fashion disaster.[SOFTBLOCK] I wish you two the best.[SOFTBLOCK] If you need help next year, I know a few units who are good with dressmaking.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I assume you're hiding in here because your outfits look like a Darkmatter's puke.[SOFTBLOCK] Aren't you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] You what, pigeon?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] Impertinant little -[SOFTBLOCK] Grrrr![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] You taffin' what?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Okay, *now* is the time to leave!)") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
            
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "GolemFancyE", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "GolemFancyF", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Pink:[E|Neutral] You gormless little trollop![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "White:[E|Neutral] Our dresses are peerless![SOFTBLOCK] You -[SOFTBLOCK] you have no fashion sense![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You seem upset.[SOFTBLOCK] I'll give you some space.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Just let 'em sputter and don't let them feel like they won.[SOFTBLOCK] There's no win scenario here, just walk away.)") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemGalaK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I prefer the night to the day.[SOFTBLOCK] The city is so wonderous to gaze upon in the dark...") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaL") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Some of these seats are reserved, but otherwise it's first come first serve.[SOFTBLOCK] How quaint.[SOFTBLOCK] Shouldn't the highest ranking units get preferred seating?") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaM") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'm not normally partial to organic cuisine, but this is some of the finest prepared food in the solar system.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaN") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Isn't it lovely being surrounded by high class units?[SOFTBLOCK] I almost regret having to return to my slave units tomorrow.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "CommandGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Oh my, I've not seen you before.[SOFTBLOCK] Is this your first Sunrise Gala?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It is, Command Unit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Heh, don't be so formal.[SOFTBLOCK] I'm just here for my fans.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I'm Unit 762.[SOFTBLOCK] The song you're hearing on this auto-piano was composed by me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] THE Unit 762?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Didn't you also write the score for 'Regulus Needs Women' and 'A Steamwork Orange'?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Yes, I've scored several videographs.[SOFTBLOCK] I've accumulated quite the library over time.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Are you Prime Command Unit of Arts or something?[SOFTBLOCK] It's my first year after conversion, so I'm not familiar with your work yet.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] A first year Lord Unit at the Sunrise Gala?[SOFTBLOCK] Impressive, that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I'm afraid artistic works have no Prime Command Unit, my dear.[SOFTBLOCK] I was just blessed with creativity and, after years of service, freed to persue my passions.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Long ago, I was in charge of construction of this very university.[SOFTBLOCK] It seems like a lifetime ago.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I composed in my spare time, and units who listen to music while doing repetitive tasks tend to increase output, so the Prime Command Units reassigned me to compose full time.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] It's wonderful to meet you, Command Unit![SOFTBLOCK] I've been a fan for many years![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Please enjoy the party, then![SOFTBLOCK] I want everyone to hear my music, and all you need to do to befriend me is listen![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Unfortunately, Unit 762 wasn't one of the Prime Command Units we're after...)") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "CommandGalaB") then
        
        --Variables.
        local iGalaMetPrimeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
        
        --First time.
        if(iGalaMetPrimeB == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N", 1.0)
            
            --If all three primes are identified, flag this.
            local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
            local iGalaMetPrimeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
            local iGalaMetPrimeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N")
            if(iGalaMetPrimeA == 1.0 and iGalaMetPrimeB == 1.0 and iGalaMetPrimeC == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N", 1.0)
                AL_SetProperty("Flag Objective True", "1: Identify Prime Command Units")
            end
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Hrmph.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Is something the matter, Command Unit?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I dislike formal functions like this.[SOFTBLOCK] I prefer field work.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] But I must keep up appearances for some foolish archaic reason.[SOFTBLOCK] I don't understand it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Machines do not require social interaction.[SOFTBLOCK] These units are massaging their egos when there is work to be done.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Tell me, Lord Unit.[SOFTBLOCK] What is there that is special about the sun rising over the city?[SOFTBLOCK] It is something I see every day, you know.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You do?[SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I suppose I have failed to introduce myself.[SOFTBLOCK] I am Unit 3014, Prime Command Unit of Abductions.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh my, we're sorry, Prime Command Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] We meant no disrespect.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] The formalities and petty grievences of the city are another reason I dislike it here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Speak freely and openly, Lord Units.[SOFTBLOCK] I prefer it that way.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Abductions, though?[SOFTBLOCK] That must be exciting work.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Tracking down humans, capturing them, dragging them off for conversion...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Watching their flesh turn to metal, their bodies and minds perfected...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] You remind me of me, in the old days.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Unfortunately, my work is largely administrative.[SOFTBLOCK] I operate our field headquarters on Pandemonium.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I do periodically get involved in frontline work, and let me tell you -[SOFTBLOCK] the thrill of conversion, there's nothing else like it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] But my job is more likely to be identifying and approving candidates for capture after surveillience.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You don't just grab any human you find?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] In the old days, we certainly did.[SOFTBLOCK] Sometimes, we still do, when we need more dumb labour units.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] But humans with solid work ethic and docile personalities make the best labourers, easily worth three of their undisciplined counterparts.[SOFTBLOCK] That's one of the reasons we started the breeding program, after all.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Plus there's the work of provisioning, administration, moving the headquarters, ugh.[SOFTBLOCK] The list goes on.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] But the smell of wild plant life, the shining sun, and the symphony of night creatures...[SOFTBLOCK] I love Pandemonium.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Regulus City has its charms...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Sterile, lifeless, mechanical.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Good traits, to be sure, but I'll still take a night watching bats chase moths in infrared.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Oh, but I suppose I've talked your aural receptors off.[SOFTBLOCK] Another habit, as intelligent conversation is a rare find, planetside.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Good evening then, Prime Command Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Okay, making progress on my objectives.[SOFTBLOCK] Prime Command Unit, identified!)") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] If you desire a change of career, Lord Unit, I would be pleased to have you in Abductions.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] That kind of enthusiasm is surprisingly rare.[SOFTBLOCK] Many units view conversion as a necessity at best.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Perhaps they didn't enjoy theirs as much as I did.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Perhaps, though the Golem Core is supposed to make the process as pleasurable as possible.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] We've been discussing allowing units to maintain more of their human memories, but there are technical hurdles to be overcome.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'd argue less.[SOFTBLOCK] Perhaps reprogramming them to believe they were raised in the breeding program instead of captured would reduce trauma.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] A novel position.[SOFTBLOCK] I'll present it to my officers at our next meeting.[SOFTBLOCK] Thank you, Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You're really into converting humans, aren't you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] What can I say?[SOFTBLOCK] I love being a Golem, and I want to share it with everyone!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "CommandGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I don't get together very often with my friends, so I treasure every Gala that comes up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why don't you take time off?[SOFTBLOCK] Aren't you in charge of your own schedule?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] No, the Prime Command Units are.[SOFTBLOCK] They're the ones who converse with Central Administration the most.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Obviously I have more discretion than, say, a Lord Unit.[SOFTBLOCK] But we are constantly evaluated by the Primes.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Taking time off for personal reasons is likely to get me demoted.[SOFTBLOCK] Personal pursuits are unbecoming of a perfect machine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] We do not age, tire, or become sick.[SOFTBLOCK] Therefore we are expected to maintain uptimes approaching 100 percent.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (The way she's speaking, she can't be a Prime Command Unit.[SOFTBLOCK] No need to get her designation, then.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Being a Command Unit sounds like a difficult burden...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] It is.[SOFTBLOCK] Maintaining order among petty Lord Units is probably the most demanding part of my job.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] But I was chosen because I am able to do it, so I do it for the city, and for Central Administration.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What about the Cause of Science?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] As I see it, Central Administration is the living manifestation of the Cause.[SOFTBLOCK] Serve it, serve the Cause.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I see.[SOFTBLOCK] Nobody has ever phrased it that way to me.[SOFTBLOCK] Good evening, Command Unit.") ]])
    
    --Generic golem that does not talk to you.
    elseif(string.sub(sActorName, 1, 8) == "GenGolem") then
    
        --Random roll.
        local iRoll = LM_GetRandomNumber(1, 24)
    
        --Dialogues.
        if(iRoll == 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) So then I said 'Not in this department, Slave Unit!'[SOFTBLOCK] Ha ha ha![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 2) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) I don't think I noticed it.[SOFTBLOCK] Go on, tell me more![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 3) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Not the right kind of assignment, if you catch my drift.[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 4) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Really?[SOFTBLOCK]  Three hundred at once?[SOFTBLOCK] Impressive![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 5) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) But then how did she get the cow [SOFTBLOCK] *off*[SOFTBLOCK] the roof?[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 6) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Ugh, sounds like a real headache for the cleanup crew.[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 7) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Oh, where did you get your nails done?[SOFTBLOCK] They're marvellous![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 8) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Yes yes, my budget got cut too...[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 9) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Do we really need so many?[SOFTBLOCK] Oh, yes, yes, I see.[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 10) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) And then it turned out she was a Raiju in disguise the whole time![SOFTBLOCK] I was floored![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 11) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) But that's not the worst part...[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 12) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Oh, I heard that about Unit 9923.[SOFTBLOCK] Tell me more.[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 13) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Seriously, she thinks that's enough for a month?[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 14) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Oh I had trouble walking, it was so good last night...[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 15) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) The repair units?[SOFTBLOCK] Pfft, unlikely![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 16) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Huh, I had heard they were wiped out.[SOFTBLOCK] Good to hear.[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 17) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) But I had already bought that doll for her![SOFTBLOCK] She was so embarassed![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 18) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) AND IT WAS SO SPICY OH MY GOSH!!![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 19) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) So this girl loses her memories and has to attend high school, but these two make her join a paranormal investigation club![SOFTBLOCK] Ha ha![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 20) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) And I said POW![SOFTBLOCK] Kane Kola Kancel![SOFTBLOCK] She was so mad![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 21) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) I was going over the numbers, and they are not good.[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 22) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) But she looked right at me and said 'That's my fetish'.[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 23) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Oh sure, let's just say, thank goodness we can't get STIs.[SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 24) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (Blah blah...) Dignity was beneath her, typical![SOFTBLOCK] (Blah blah...)") ]])
            fnCutsceneBlocker()
        end
    end

end