--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] See the sun boiling beneath the horizon...") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaB") then
        
        --Variables.
        local iGalaMetPrimeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N")
        
        --First time.
        if(iGalaMetPrimeC == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N", 1.0)
            
            --If all three primes are identified, flag this.
            local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
            local iGalaMetPrimeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
            local iGalaMetPrimeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N")
            if(iGalaMetPrimeA == 1.0 and iGalaMetPrimeB == 1.0 and iGalaMetPrimeC == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N", 1.0)
                AL_SetProperty("Flag Objective True", "1: Identify Prime Command Units")
            end
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Oh my, it appears we have company.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Apologies, Command Unit.[SOFTBLOCK] Is this room reserved?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] No, no.[SOFTBLOCK] I just hadn't expected to see someone...[SOFTBLOCK] low ranking...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Shall I simply say 'politics' and leave it at that?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Seems like a fair assessment to me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] No matter.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Have you come to watch the sunrise in private?[SOFTBLOCK] It seems some golem somewhere set up some chairs for us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] No need to be shy.[SOFTBLOCK] I am enchanted by your lovely wardrobe, who composed it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] It's a custom made piece.[SOFTBLOCK] I designed it myself![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Oh, lovely![SOFTBLOCK] Might I ask you to compose mine next year, unit...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Uhhh...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I am Christine, and this is Andrea.[SOFTBLOCK] We're from Sector 96.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] Briget.[SOFTBLOCK] Charmed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Briget?[SOFTBLOCK] Prime Command Unit 5608, 'Bloody' Briget?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] I see that nickname continues to follow me.[SOFTBLOCK] Yes, that is I.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'm not familiar with the name...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] According to the rumours...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] The rumours are true.[SOFTBLOCK] I am the Prime Command Unit assigned to the maintenance of public order.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] I regret that it became necessary.[SOFTBLOCK] Our operatives prefer to act quietly and behind the scenes.[SOFTBLOCK] Having to call security units is considered a point of failure.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] What happened?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] A group of unruly Slave Units stormed the habitation domes and sealed the entrances.[SOFTBLOCK] They demanded an 11-hour maximum working day and repelled all attempts to enter.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] The security units were unable to breach the domes without compromising their structural integrity, which was likely the objective of the insurgents.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Oh my...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] In the name of public order, I activated one of our reserve protocols.[SOFTBLOCK] We released a pheremone into the habitat, causing the Raijus great physical discomfort as it increased their electrical outputs well beyond normal levels.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] The pain drove them to desperation, and the Raijus eventually began to discharge into the circuitry in the dome, and its occupants.[SOFTBLOCK] Without actually compromising the structural integrity of the dome, the insurgents were suppressed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But over 80 Raijus died, and another 156 were badly injured.[SOFTBLOCK] All the insurgents were fried, as well as several breeding program humans trapped in the dome.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] For doing my job and maintaining public order, I was given the nickname 'Bloody' Briget.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] M-[SOFTBLOCK]my goodness![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Careful, now, Sophie.[SOFTBLOCK] Play along.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We weren't there, Andrea.[SOFTBLOCK] We can't have known the circumstances.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I have complete faith that Prime Command Unit 5608 made the correct decision with the available information.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] Finally![SOFTBLOCK] Someone who doesn't second-guess my choices![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] I have received immense criticism for my actions, but all of them from the perspective of the results.[SOFTBLOCK] Even Unit 2855, ever the stalwart, did not present an alternative approach.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] Precisely what the other Primes would have done, they did not say.[SOFTBLOCK] Only that I was too brutal.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] You, Christine, understand that what I did was necessary.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Further, no insurgent will ever dare to do that again.[SOFTBLOCK] Public Order will be maintained now that they know the lengths we will go to to maintain it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] Another good point.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] I did not think I would make a friend on a night like this.[SOFTBLOCK] Usually, I avoid public gatherings.[SOFTBLOCK] Most Lord Units try not to be associated with me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] Since you are here, I presume you are tandem units.[SOFTBLOCK] If you would like to watch the sunrise with us, you are more than welcome.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] We...[SOFTBLOCK] wanted to go somewhere private...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] I see.[SOFTBLOCK] Do not let me keep you.[SOFTBLOCK] Good evening, ladies.") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Briget:[E|Neutral] If you're looking to be alone, I think there must be a spare room around here for you.") ]])
        
        end
        
    elseif(sActorName == "GolemGalaE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Where has my tandem unit gotten to?[SOFTBLOCK] I should know better than to speak with my mouth full of shrimp!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemGalaF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'm hiding from my tandem unit.[SOFTBLOCK] She ate the shrimp and it's now far too spicy to kiss her!") ]])
        fnCutsceneBlocker()
    end
end