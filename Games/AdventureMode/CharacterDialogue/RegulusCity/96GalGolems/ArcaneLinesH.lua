--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "2856") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "SteamLord") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Greetings, friends, what may I help you with?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Suddenly she's nice?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Oh, oops, I had accidentally let my linguistic routines assume we were not facing an unprecedented crisis![SOFTBLOCK] MY MISTAKE.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] SHUT UP,[SOFTBLOCK] DON'T ASK QUESTIONS,[SOFTBLOCK] AND SEAL THE BREACHES IN THIS SECTOR,[SOFTBLOCK] BEFORE WE ALL DIE.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] ...[SOFTBLOCK] Would it help if I said please?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yes?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Please.[SOFTBLOCK] GET GOING,[SOFTBLOCK] RIGHT NOW.") ]])
        
    elseif(sActorName == "SecurityA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] COMMAND UNIT 2856 IS AN EXCELLENT LEADER.[SOFTBLOCK] THIS UNIT ENJOYS WORKING UNDER HER.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "SecurityB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] WATCHING FOR TARGETS...") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "SecurityC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] AREA CLEAR, NO HOSTILES ACTIVELY MURDERING THIS UNIT.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "SecurityD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THESE UNITS ARE BEYOND REPAIR.[SOFTBLOCK] PLEASE MOVE ON.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "SecurityE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT CANNOT BE SALVAGED...") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "SecurityF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT'S SUPERIOR OFFICER HAS BEEN RETIRED.[SOFTBLOCK] THIS UNIT IS...[SOFTBLOCK] NOT SURE...[SOFTBLOCK] WHAT TO DO...[SOFTBLOCK] THIS UNIT...[SOFTBLOCK] IS CRYING...") ]])
        fnCutsceneBlocker()
    end
end