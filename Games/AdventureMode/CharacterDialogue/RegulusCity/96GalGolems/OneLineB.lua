--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemGalaA") then
        
        --Variables.
        local iSpokeGalaLord96 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeGalaLord96", "N")
        if(iSpokeGalaLord96 == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeGalaLord96", "N", 1.0)
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Lord Unit 649728.[SOFTBLOCK] You're looking well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "649728:[E|Neutral] Lord Unit 771852.[SOFTBLOCK] I suppose this was inevitable, wasn't it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "649728:[E|Neutral] Your department's productivity is the envy of the sector.[SOFTBLOCK] Suiting that you'd get an invitation to the Sunrise Gala.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You didn't?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "649728:[E|Neutral] Not this year, but the fabrication department has been on a bit of a downswing.[SOFTBLOCK] Oh well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "649728:[E|Neutral] But who is this enchanting lady I see before me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] H-[SOFTBLOCK]hello![SOFTBLOCK] Lord Unit![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is my tandem unit.[SOFTBLOCK] She's from Sector 88.[SOFTBLOCK] You don't know her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You may call her -[SOFTBLOCK] Andrea.[SOFTBLOCK] And you can call me Christine.[SOFTBLOCK] Unit 771852 is so formal![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "649728:[E|Neutral] Sector 88, hmm?[SOFTBLOCK] You look familiar, my dear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] D-[SOFTBLOCK]do I?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] (Act synthetically, Sophie![SOFTBLOCK] You're going to blow this!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You are likely recognizing my dress more than my face.[SOFTBLOCK] This is an original Unit 78.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "649728:[E|Neutral] Unit 78 is still making dresses?[SOFTBLOCK] My stars![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "649728:[E|Neutral] You'll be the talk of the gala in something like that.[SOFTBLOCK] Good evening, Christine and Andrea.[SOFTBLOCK] Please give them Sector 96's best!") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "649728:[E|Neutral] I should probably be getting to my quarters.[SOFTBLOCK] My tandem unit will be cross if I'm late.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] A date?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "649728:[E|Neutral] Indeed.[SOFTBLOCK] Oh I'm going to struggle to walk tomorrow...") ]])
            fnCutsceneBlocker()
        
        end
        
    elseif(sActorName == "GolemGalaB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Since nobody is supervising, I'm going to practice this game until I can beat Unit 577422!") ]])
        
    elseif(sActorName == "GolemGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My servos are shot.[SOFTBLOCK] I just want to go to my quarters and rest, but these two are set on having a party.") ]])
        
    elseif(sActorName == "GolemGalaD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] When the other bots see the balloon I got, they're gonna flip![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Balloon?[SOFTBLOCK] Singular?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Yeah![SOFTBLOCK] It'll be so festive!") ]])
        
    elseif(sActorName == "GolemGalaE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Remember not to drink too much punch at the Gala, Christine.[SOFTBLOCK] Or whatever it is they serve there.") ]])
    end

end