--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iIsRemoving = VM_GetVar("Root/Variables/Chapter5/Quarters/iIsRemoving", "N")
	
	--Dialogue.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Lord Golem, what may I do for you?[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Instructions\", " .. sDecisionScript .. ", \"Instructions\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Finalize\", " .. sDecisionScript .. ", \"Finalize\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Change Object\",  " .. sDecisionScript .. ", \"Change Object\") ")
	if(iIsRemoving == 0.0) then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Remove Object\",  " .. sDecisionScript .. ", \"Remove Object\") ")
	else
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Place Object\",  " .. sDecisionScript .. ", \"Place Object\") ")
	end
	fnCutsceneBlocker()

--[Post Decision]
--Repeat Instructions.
elseif(sTopicName == "Instructions") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] To place an object in your quarters, position yourself in front of where you would like it and push the Activate key.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The object will appear in front of you.[SOFTBLOCK] Objects are on a grid, so you may need to move a bit to get it where you want it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Once you are satisfied with the object's position, speak to me and select 'Finalize'.[SOFTBLOCK] This will place it in your room permanently.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] If you change your mind, simply leave the room via the elevator and the object will not be placed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] If you would like to place a different object, speak to me and select 'Change Object'.[SOFTBLOCK] I will bring up a list of things for you to place.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You may also remove objects by speaking to me and selecting 'Remove Object'.[SOFTBLOCK] You will receive a confirmation message before an object is removed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] If multiple objects are in the same position, you may need to remove the newer ones before you can remove the older ones.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] A full refund will be credited to your account for any removed objects.[SOFTBLOCK] However, you must maintain a Defragmentation Pod in your quarters at all times.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Also, objects cannot be placed directly in front of the elevator here, and objects cannot be placed directly south of the Defragmentation Pod.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Please enjoy organizing your accomodations, Lord Golem.") ]])
	fnCutsceneBlocker()

--Finalize.
elseif(sTopicName == "Finalize") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Variables.
	local iTentativeID = VM_GetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N")
	local iTentativeX  = VM_GetVar("Root/Variables/Chapter5/Quarters/iTentativeX", "N")
	local iTentativeY  = VM_GetVar("Root/Variables/Chapter5/Quarters/iTentativeY", "N")
	
	--If the object has not been positioned yet.
	if(iTentativeX == -100.0 or iTentativeY == -100.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Apologies, Lord Golem, but it seems you haven't placed anything yet.[SOFTBLOCK] If you have changed your mind and would like to cancel, please exit the room via the elevator.") ]])
	
	--Finalize.
	else
	
		--Name.
		local sObjectName = "Nothing"
		if(iTentativeID == gci_Quarters_DefragPod) then sObjectName = "Defragmentation Pod" end
		if(iTentativeID == gci_Quarters_CounterL) then sObjectName = "Counter" end
		if(iTentativeID == gci_Quarters_CounterM) then sObjectName = "Counter" end
		if(iTentativeID == gci_Quarters_CounterR) then sObjectName = "Counter" end
		if(iTentativeID == gci_Quarters_OilMaker) then sObjectName = "Oil Machine" end
		if(iTentativeID == gci_Quarters_TV) then sObjectName = "Television Unit" end
		if(iTentativeID == gci_Quarters_ChairS) then sObjectName = "Chair" end
		if(iTentativeID == gci_Quarters_CouchSL) then sObjectName = "Couch" end
		if(iTentativeID == gci_Quarters_CouchSM) then sObjectName = "Couch" end
		if(iTentativeID == gci_Quarters_CouchSR) then sObjectName = "Couch" end
		if(iTentativeID == gci_Quarters_ChairN) then sObjectName = "Chair" end
		if(iTentativeID == gci_Quarters_CouchNL) then sObjectName = "Couch" end
		if(iTentativeID == gci_Quarters_CouchNM) then sObjectName = "Couch" end
		if(iTentativeID == gci_Quarters_CouchNR) then sObjectName = "Couch" end
		
		--Assemble.
		local sStatement = "\"Golem:[VOICE|Golem] A " .. sObjectName .. " will be placed at " .. iTentativeX .. " by " .. iTentativeY .. ". Is this correct?[BLOCK]\""
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Append\", " .. sStatement .. ") ")

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Confirm\", " .. sDecisionScript .. ", \"Confirm\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"Cancel\") ")
		fnCutsceneBlocker()
	end

--Confirm Finalize.
elseif(sTopicName == "Confirm") then

	--Tentatives.
	local iTentativeID = VM_GetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N")
	local iTentativeX  = VM_GetVar("Root/Variables/Chapter5/Quarters/iTentativeX", "N")
	local iTentativeY  = VM_GetVar("Root/Variables/Chapter5/Quarters/iTentativeY", "N")
	local iObjectsTotal= VM_GetVar("Root/Variables/Chapter5/Quarters/iObjectsTotal", "N")

	--Locate the next free slot, and place the object in it.
	for i = 0, 99, 1 do
		local sName = i
		if(i < 10) then sName = "0" .. i end
			
		--Check if an object exists in this slot.
		local iObjectID = VM_GetVar("Root/Variables/Chapter5/Quarters/iObject" .. sName .. "ID", "N")
		if(iObjectID == gci_Quarters_Nothing) then
			
			--Store.
			VM_SetVar("Root/Variables/Chapter5/Quarters/iObject" .. sName .. "ID", "N", iTentativeID)
			VM_SetVar("Root/Variables/Chapter5/Quarters/iObject" .. sName .. "X", "N", iTentativeX)
			VM_SetVar("Root/Variables/Chapter5/Quarters/iObject" .. sName .. "Y", "N", iTentativeY)
			
			--Spawn the object.
			TA_Create("Object" .. sName)
				TA_SetProperty("Clipping Flag", false)
				TA_SetProperty("Facing", gci_Face_South)
				TA_SetProperty("Activation Script", "Null")
				fnSetApartmentObjectGraphics(iTentativeID)
			DL_PopActiveObject()
			
			--Move the tentative object offscreen.
			fnCutsceneTeleport("Tentative", -100, -100)
			
			--Reposition the object using the autoroutines.
			fnCutsceneTeleport("Object" .. sName, iTentativeX + 0.25, iTentativeY - 1.00 + 0.50)
				
			--Special depth values. Must be set after moving the object.
			EM_PushEntity("Object" .. sName)
				TA_SetProperty("Rendering Depth", -0.500000 + (iTentativeY * 0.000100))
			DL_PopActiveObject()
			
			--Increase if we pass the max.
			if(iObjectsTotal <= i) then
				VM_SetVar("Root/Variables/Chapter5/Quarters/iObjectsTotal", "N", i)
			end
			break
		end
	end

	--Reset variables.
	VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_Nothing)
	VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeX", "N", -100.0)
	VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeY", "N", -100.0)
	
	--Clear.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Order confirmed.[SOFTBLOCK] Thank you, Lord Golem.[SOFTBLOCK] Would you like to place anything else in your quarters?[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesPlace\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\", " .. sDecisionScript .. ", \"NoReturn\") ")
	fnCutsceneBlocker()

--Cancel Finalize.
elseif(sTopicName == "Cancel") then
	WD_SetProperty("Hide")

--Place another object.
elseif(sTopicName == "YesPlace") then

	--Reboot the dialogue.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Very well.[SOFTBLOCK] What would you like to order?[BLOCK]") ]])
	
	--Decisions.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Defragmentation Pod\", " .. sDecisionScript .. ", \"OrderDefragPod\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Counter Left\",  " .. sDecisionScript .. ", \"OrderCounterL\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Counter Middle\",  " .. sDecisionScript .. ", \"OrderCounterM\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Counter Right\",  " .. sDecisionScript .. ", \"OrderCounterR\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Oil Making Machine\",  " .. sDecisionScript .. ", \"OrderOilMaker\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Television\",  " .. sDecisionScript .. ", \"OrderTV\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Chair (South)\",  " .. sDecisionScript .. ", \"OrderChairS\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Chair (North)\",  " .. sDecisionScript .. ", \"OrderChairN\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (South) Left\",  " .. sDecisionScript .. ", \"OrderCouchSL\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (South) Middle\",  " .. sDecisionScript .. ", \"OrderCouchSM\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (South) Right\",  " .. sDecisionScript .. ", \"OrderCouchSR\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (North) Left\",  " .. sDecisionScript .. ", \"OrderCouchNL\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (North) Middle\",  " .. sDecisionScript .. ", \"OrderCouchNM\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (North) Right\",  " .. sDecisionScript .. ", \"OrderCouchNR\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoReturn\") ")
	fnCutsceneBlocker()

--Don't place another object.
elseif(sTopicName == "NoReturn") then

	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Quarters/iIsPlacing", "N", 0.0)
	VM_SetVar("Root/Variables/Chapter5/Quarters/iIsRemoving", "N", 0.0)

	--Reboot the dialogue.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Very good.[SOFTBLOCK] Please accompany me to the main floor while your order is delivered.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:6.5x14.0x0") ]])

--Change Object.
elseif(sTopicName == "Change Object") then
	WD_SetProperty("Hide")
	
	--Determine the current object.
	local iTentativeID = VM_GetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N")
	local sObjectName = "Nothing"
	if(iTentativeID == gci_Quarters_DefragPod) then sObjectName = "Defragmentation Pod" end
	if(iTentativeID == gci_Quarters_CounterL) then sObjectName = "Counter" end
	if(iTentativeID == gci_Quarters_CounterM) then sObjectName = "Counter" end
	if(iTentativeID == gci_Quarters_CounterR) then sObjectName = "Counter" end
	if(iTentativeID == gci_Quarters_OilMaker) then sObjectName = "Oil Machine" end
	if(iTentativeID == gci_Quarters_TV) then sObjectName = "Television Unit" end
	if(iTentativeID == gci_Quarters_ChairS) then sObjectName = "Chair" end
	if(iTentativeID == gci_Quarters_CouchSL) then sObjectName = "Couch" end
	if(iTentativeID == gci_Quarters_CouchSM) then sObjectName = "Couch" end
	if(iTentativeID == gci_Quarters_CouchSR) then sObjectName = "Couch" end
	if(iTentativeID == gci_Quarters_ChairN) then sObjectName = "Chair" end
	if(iTentativeID == gci_Quarters_CouchNL) then sObjectName = "Couch" end
	if(iTentativeID == gci_Quarters_CouchNM) then sObjectName = "Couch" end
	if(iTentativeID == gci_Quarters_CouchNR) then sObjectName = "Couch" end
		
	--Assemble.
	local sStatement = "\"Golem:[VOICE|Golem] You were placing a " .. sObjectName .. ".[SOFTBLOCK] What would you like to change this to?[SOFTBLOCK] The object will remain where it was, but change its type.[BLOCK]\""
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Append\", " .. sStatement .. ") ")

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Defragmentation Pod\", " .. sDecisionScript .. ", \"OrderDefragPod\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Counter Left\",  " .. sDecisionScript .. ", \"OrderCounterL\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Counter Middle\",  " .. sDecisionScript .. ", \"OrderCounterM\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Counter Right\",  " .. sDecisionScript .. ", \"OrderCounterR\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Oil Making Machine\",  " .. sDecisionScript .. ", \"OrderOilMaker\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Television\",  " .. sDecisionScript .. ", \"OrderTV\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Chair (South)\",  " .. sDecisionScript .. ", \"OrderChairS\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Chair (North)\",  " .. sDecisionScript .. ", \"OrderChairN\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (South) Left\",  " .. sDecisionScript .. ", \"OrderCouchSL\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (South) Middle\",  " .. sDecisionScript .. ", \"OrderCouchSM\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (South) Right\",  " .. sDecisionScript .. ", \"OrderCouchSR\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (North) Left\",  " .. sDecisionScript .. ", \"OrderCouchNL\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (North) Middle\",  " .. sDecisionScript .. ", \"OrderCouchNM\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Couch (North) Right\",  " .. sDecisionScript .. ", \"OrderCouchNR\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--Ordering cases.
elseif(string.sub(sTopicName, 1, 5) == "Order") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Special.
	if(sTopicName == "OrderDefragPod") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_DefragPod)
	elseif(sTopicName == "OrderCounterL") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_CounterL)
	elseif(sTopicName == "OrderCounterM") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_CounterM)
	elseif(sTopicName == "OrderCounterR") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_CounterR)
	elseif(sTopicName == "OrderOilMaker") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_OilMaker)
	elseif(sTopicName == "OrderTV") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_TV)
	elseif(sTopicName == "OrderChairS") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_ChairS)
	elseif(sTopicName == "OrderChairN") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_ChairN)
	elseif(sTopicName == "OrderCouchSL") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_CouchSL)
	elseif(sTopicName == "OrderCouchSM") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_CouchSM)
	elseif(sTopicName == "OrderCouchSR") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_CouchSR)
	elseif(sTopicName == "OrderCouchNL") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_CouchNL)
	elseif(sTopicName == "OrderCouchNM") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_CouchNM)
	elseif(sTopicName == "OrderCouchNR") then
		VM_SetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N", gci_Quarters_CouchNR)
	end

	--Resolve.
	local iTentativeID = VM_GetVar("Root/Variables/Chapter5/Quarters/iTentativeID", "N")
	EM_PushEntity("Tentative")
		fnSetApartmentObjectGraphics(iTentativeID)
	DL_PopActiveObject()

--Remove Object.
elseif(sTopicName == "Remove Object") then

	--Flags.
	WD_SetProperty("Hide")
	VM_SetVar("Root/Variables/Chapter5/Quarters/iIsRemoving", "N", 1.0)
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You are now in removal mode.[SOFTBLOCK] Examine an object to remove it.[SOFTBLOCK] Please speak to me again if you would like to exit removal mode.") ]])
	fnCutsceneBlocker()
	
--Place Object.
elseif(sTopicName == "Place Object") then
	WD_SetProperty("Hide")
	VM_SetVar("Root/Variables/Chapter5/Quarters/iIsRemoving", "N", 0.0)
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You are now in placement mode.") ]])
	fnCutsceneBlocker()
	
--Close.
elseif(sTopicName == "NoClose") then
	WD_SetProperty("Hide")

end