--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "SecurityA" or sActorName == "SecurityB") then
        
        --Variables.
        local iSpokeTo2856Biolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N")
        
        --Not spoken to 2856 yet.
        if(iSpokeTo2856Biolabs == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Golem] COMMAND UNIT 2856 TOLD US TO TELL YOU TO SPEAK TO HER AS SOON AS POSSIBLE.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Golem] SO, SPEAK TO COMMAND UNIT 2856.[SOFTBLOCK] AS SOON AS POSSIBLE. THANK YOU.") ]])
        
        --Spoken to 2856.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Golem] THIS UNIT WILL REMAIN IN THIS AREA TO SAFEGUARD COMMAND UNIT 2856.[SOFTBLOCK] THANK YOU FOR YOUR PATIENCE.") ]])
            fnCutsceneBlocker()
        end
    
    --2856.
    elseif(sActorName == "2856") then
    
        --Variables.
        local iSpokeTo2856Biolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N")
        local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        
        --First time.
        if(iSpokeTo2856Biolabs == 0.0) then 
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N", 1.0)
            DialogueActor_Push("2856")
                DialogueActor_SetProperty("Remove Alias", "55")
                DialogueActor_SetProperty("Remove Alias", "2855")
            DL_PopActiveObject()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "SteamLord") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] So you survived, again.[SOFTBLOCK] Perhaps I was right to enlist your aid.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Do you have an alternative?[SOFTBLOCK] I think you're stuck with us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Considering the situation, a nuclear detonation is rapidly becoming more viable.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Oh, don't worry.[SOFTBLOCK] I intend to wait until after you fail before unleashing one.[SOFTBLOCK] We don't even have any stockpiled, it may take some time to synthesize.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Great.[SOFTBLOCK] Peachy.[SOFTBLOCK] Better stop Vivify, then.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Please do.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Distant explosion.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("2856", 1, 1)
            fnCutsceneFace("Christine", 1, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            fnCutsceneFace("2856", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "SteamLord") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] What was that?[SOFTBLOCK] Did you hear that?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Don't play dumb.[SOFTBLOCK] You know exactly what it was.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The vibration came through the ground, not the air, so it was external to the biolabs.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That was likely an explosive charge set off within the city.[SOFTBLOCK] It has begun.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Wh-[SOFTBLOCK]what?[SOFTBLOCK] But - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We detonated a set of charges to seal the basement.[SOFTBLOCK] Our sympathizers took that as the signal to attack.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] So you got what you wanted, didn't you?[SOFTBLOCK] Even if I ordered security units to help us, they're now going to be tied up fighting your rebels.[SOFTBLOCK] Nice work.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Now no help is coming,[SOFTBLOCK] it's all your fault,[SOFTBLOCK] AND WE ARE NO CLOSER TO STOPPING VIVIFY THAN BEFORE.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Damn it...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I don't like this, and I don't like having to rely on dumb bolts like you, 771852.[SOFTBLOCK] You are unreliable.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Most of my remaining teams are handling the evacuation and fighting a rearguard action.[SOFTBLOCK] I have...[SOFTBLOCK] six...[SOFTBLOCK] units in the biolabs.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] It looks like they made their way to the Raiju Ranch north of here.[SOFTBLOCK] YOU are going to clear a path for me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] We'll discuss our next move at the ranch.[SOFTBLOCK] Is that understood?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The opportunity to spend quality time with you?[SOFTBLOCK] Wouldn't miss it for the world.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Your sarcastic tendencies are noted.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Come on, team.") ]])
            fnCutsceneBlocker()
        
        else
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "SteamLord") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Maybe if I stare at this terminal hard enough, you'll GET BACK TO WORK.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Fine![SOFTBLOCK] We're leaving!") ]])
            fnCutsceneBlocker()
        end
    end

end