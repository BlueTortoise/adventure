--[Shop Setup]
--Script that builds what items this vendor is willing to sell you.

--Items with unlimited quantity.
AM_SetShopProperty("Add Item", "Adamantite Powder", true)
AM_SetShopProperty("Add Item", "Adamantite Flakes", true)

--Do not append these items:
local saDoNotAppendList = {"Tattered Rags", "Ruined Armor", "Broken Spear", "Emerald", "Aquamarine", "Amethyst", "Ardrite Gem", "Blurleen Gem", "Recycleable Junk"}

--Go across this variable to build the shop list.
local sCurrentItem = ""
local sItemList = VM_GetVar("Root/Variables/Chapter5/NarissaVendor/sShopInfo", "S")
local iLen = string.len(sItemList)
for p = 1, iLen, 1 do
	
	--Get the letter.
	local sLetter = string.sub(sItemList, p, p)
	
	--If the letter is not '|', append it.
	if(sLetter ~= "|") then
		sCurrentItem = sCurrentItem .. sLetter
	
	--Otherwise, this delimits an item.
	else
	
		--Check if the item is on the do-not-append list. If it is, skip it.
		local bSkip = false
		for i = 1, #saDoNotAppendList, 1 do
			if(saDoNotAppendList[i] == sCurrentItem) then
				bSkip = true
				break
			end
		end
	
		--Remove Adamantite from the list as well.
		if(string.sub(sCurrentItem, 1, 10) == "Adamantite") then
			bSkip = true
		end
	
		--If not skipping, append it:
		if(bSkip == false) then
			AM_SetShopProperty("Add Item", sCurrentItem)
		end
		
		--Reset the letters.
		sCurrentItem = ""
	end
end