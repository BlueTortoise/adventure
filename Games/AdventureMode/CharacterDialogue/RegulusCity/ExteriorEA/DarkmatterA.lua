--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    if(iHasDarkmatterForm == 0.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](The creature is trying to look as if it is ignoring me, but is quietly following my every move...)") ]])
    else
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[VOICE|DarkmatterGirl] Greetings, friend.[SOFTBLOCK] I will watch over the machine at this place.[SOFTBLOCK] I will keep her safe.") ]])
        
    end
end