--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    if(iHasDarkmatterForm == 0.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](The creature seems to be ignoring me.[SOFTBLOCK] It doesn't seem hostile.)") ]])
    else
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[VOICE|DarkmatterGirl] I'm so bored.[SOFTBLOCK] Maybe there's a supernova I can go watch...") ]])
        
    end
end