--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Form.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Mechanical forms that are not objectionable.
    if(sChristineForm == "Golem" or sChristineForm == "LatexDrone") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^The batteries are superb, but the cable tends to break.[SOFTBLOCK] The Darkmatters also tend to play with them and tangle them up.[SOFTBLOCK] How aggravating!^") ]])
    
    --Eldritch.
    elseif(sChristineForm == "Eldritch") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Command Unit, please keep your...[SOFTBLOCK] creature...[SOFTBLOCK] on a short leash...^") ]])
    
    --Electrosprite.
    elseif(sChristineForm == "Electrosprite") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^An -[SOFTBLOCK] oh my![SOFTBLOCK] You shouldn't show yourself to the Lord Golem, she doesn't know!^") ]])
    
    --Steam Droid.
    elseif(sChristineForm == "SteamDroid") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^You're with a Command Unit?[SOFTBLOCK] Are you some sort of mercenary?[SOFTBLOCK] We don't want any trouble, please.^") ]])
    
    --Darkmatter.
    elseif(sChristineForm == "Darkmatter") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^No![SOFTBLOCK] No![SOFTBLOCK] Bad Darkmatter![SOFTBLOCK] Do not tangle up those cables![SOFTBLOCK] Arrggh!^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Being told not to just makes me want to do it more...)") ]])
    end
end