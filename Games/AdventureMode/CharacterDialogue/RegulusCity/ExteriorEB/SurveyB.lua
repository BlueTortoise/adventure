--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Form.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Mechanical forms that are not objectionable.
    if(sChristineForm == "Golem" or sChristineForm == "LatexDrone") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^I am currently in charge of security for this survey team.[SOFTBLOCK] Do not worry, we all have up-to-date combat routines, and the Lord Unit is a superb close-quarters fighter.^") ]])
    
    --Eldritch.
    elseif(sChristineForm == "Eldritch") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^I am unfamiliar with this species, Command Unit.[SOFTBLOCK] Is it a threat to my team?^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] ^Negatory.[SOFTBLOCK] As you were, Unit.[SOFTBLOCK] This is under control.^") ]])
    
    --Electrosprite.
    elseif(sChristineForm == "Electrosprite") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Hm.[SOFTBLOCK] I've heard of you from the others.[SOFTBLOCK] So long as you do not cause problems, I will tolerate you, Electrosprite.^") ]])
    
    --Steam Droid.
    elseif(sChristineForm == "SteamDroid") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Don't make any sudden moves, mercenary, and we'll get along just fine.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ^Affirmative.[SOFTBLOCK] I'm not here to make trouble.^") ]])
    
    --Darkmatter.
    elseif(sChristineForm == "Darkmatter") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Shoo, shoo! Go on, get out of here, Darkmatter!^") ]])
    end
end