--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Form.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Mechanical forms that are not objectionable.
    if(sChristineForm == "Golem" or sChristineForm == "LatexDrone") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Survey work is rather pleasant, if dangerous, but the recent increase in tectonic activity means we have a long work order list...^") ]])
    
    --Eldritch.
    elseif(sChristineForm == "Eldritch") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Oh my, you look like those things we saw on the crater shelf!^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] ^My prisoner here is not of your concern.[SOFTBLOCK] Ignore her and the things you've seen.[SOFTBLOCK] For your own safety.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] ^I cannot guarantee any other Command Unit will be as lenient as I am.^") ]])
    
    --Electrosprite.
    elseif(sChristineForm == "Electrosprite") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Are you a friend of LIFO's?[SOFTBLOCK] She's the tandem unit of a friend of mine.[SOFTBLOCK] Not suggesting you all know each other or anything, that'd be weird.^") ]])
    
    --Steam Droid.
    elseif(sChristineForm == "SteamDroid") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Are we hiring Steam Droids to help with survey work now?[SOFTBLOCK] I knew we were short-staffed but this is going too far.^") ]])
    
    --Darkmatter.
    elseif(sChristineForm == "Darkmatter") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Oh no.[SOFTBLOCK] Darkmatters always make the long-range tachyon sensors short out...[SOFTBLOCK] Now I'll have to recalibrate it...^") ]])
    end
end