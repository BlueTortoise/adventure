--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local sChristineForm              = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iMetSurveyLordGolem         = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordGolem", "N")
	local iMetSurveyLordLatex         = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordLatex", "N")
	local iMetSurveyLordEldritch      = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordEldritch", "N")
	local iMetSurveyLordElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordElectrosprite", "N")
	local iMetSurveyLordSteamDroid    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordSteamDroid", "N")
	local iMetSurveyLordDarkmatter    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordDarkmatter", "N")
	local iKnowsAboutDarkmatters      = VM_GetVar("Root/Variables/Global/Christine/iKnowsAboutDarkmatters", "N")
	
	--Face the party.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--If Christine is a Golem:
	if(sChristineForm == "Golem") then
		
		--If this is the first time meeting the survey lord as a golem:
		if(iMetSurveyLordGolem == 0.0) then
		
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordGolem", "N", 1.0)
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Ah, greetings, fellow Lord Unit.[SOFTBLOCK] Are you perhaps on your way to Serenity Crater?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^That is just south of here, correct?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Affirmative.[SOFTBLOCK] They have been most helpful to my efforts.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^What are you doing out here?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I am administering Geological Survey Team 82.[SOFTBLOCK] Due to the recent tectonic activity in this area, we are re-mapping the terrain.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^The transit network has taken a lot of damage.[SOFTBLOCK] I expect it will be some time before we return to full efficiency.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Ah, very good.[SOFTBLOCK] I was just going to ask about the effects on productivity.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^As it stands, cargo is mostly hauled manually.[SOFTBLOCK] The Slave Units, of course, won't stop complaining.[SOFTBLOCK] Why, even I had to haul the battery pack here!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (The battery pack is the lightest part, you slacker...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Peh, I despise manual labour.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^As do we all.[SOFTBLOCK] Our bodies are simply not designed for it.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Our model variant has less shielding but the same density of motivators...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Of course.[SOFTBLOCK] The Slave Units should do it.[SOFTBLOCK] But, we must all pitch in for the Cause of Science.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I agree fully.[SOFTBLOCK] Speaking of...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^If you are on your way to Serenity Crater, do be careful.[SOFTBLOCK] The Darkmatters seem to be upset about something.^[BLOCK][CLEAR]") ]])
			
			--If Christine doesn't know what a Darkmatter is:
			if(iKnowsAboutDarkmatters == 0.0) then
				VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutDarkmatters", "N", 1.0)
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Those shadowy creatures are called Darkmatters?^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Affirmative, though many units often call them 'Darkmatter Girls'.[SOFTBLOCK] They are, however, certainly not girls.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^So what are they?^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I would receive a research commendation if I knew![SOFTBLOCK] They are virtually impossible to study, as they absorb almost all forms of radiation and seem to be able to disappear on a whim.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^They are not usually aggressive.[SOFTBLOCK] In fact, often they are quite charming.[SOFTBLOCK] But do not take any risks around them.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^We don't as a matter of course.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Oh, a command unit![SOFTBLOCK] We are being very productive here.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Not my concern.[SOFTBLOCK] Forget you saw me.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Covert operations.[SOFTBLOCK] I understand.[SOFTBLOCK] I will set a timer to purge my memories of you when I defragment.[SOFTBLOCK] Thank you for your service.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Good.[SOFTBLOCK] 771852, let's go.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Affirmative.^") ]])
			
			--Christine knows about Darkmatters.
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^What has the Darkmatters so riled up?^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I am not sure.[SOFTBLOCK] I believe Serenity Observatory put in a work order to investigate exactly that.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Isn't that what you were on your way there to see about?^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^What we intend to do is not your business.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Oh, a command unit![SOFTBLOCK] We are being very productive here.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Not my concern.[SOFTBLOCK] Forget you saw me.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Covert operations.[SOFTBLOCK] I understand.[SOFTBLOCK] I will set a timer to purge my memories of you when I defragment.[SOFTBLOCK] Thank you for your service.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^Good.[SOFTBLOCK] 771852, let's go.^[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Affirmative.^") ]])
			end
			fnCutsceneBlocker()
		
		else
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I wish you luck on whatever your assignment is.[SOFTBLOCK] Serve the Cause of Science.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^To the best of our ability, we will.^") ]])
		end
		
	--If Christine is a Latex Drone:
	elseif(sChristineForm == "LatexDrone") then
	
		--If Christine has met the Lord Golem before, as a Golem:
		if(iMetSurveyLordGolem == 1.0 and iMetSurveyLordLatex == 0.0) then
	
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordLatex", "N", 1.0)
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Hmm, you seem familiar.[SOFTBLOCK] Unit, what is your designation?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Uhh...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Most curious!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^*Retain your cover, Unit 771852.*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^*R-[SOFTBLOCK]right!*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^MY DESIGNATION IS UNIT 771852.[SOFTBLOCK] GREETINGS, LORD UNIT.[SOFTBLOCK] HOW MAY I SERVE YOU?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Ah, perhaps you linguistic generators were delayed.[SOFTBLOCK] Not unsurprising -[SOFTBLOCK] wait.[SOFTBLOCK] Unit 771852?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Have we met before?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^AFFIRMATIVE, LORD UNIT.[SOFTBLOCK] THIS UNIT HAS BEEN REPURPOSED.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Most unfortunate...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^I needed her this way for my purposes.[SOFTBLOCK] Do not question them.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Of course not, command unit![SOFTBLOCK] If you will it, then this is the ideal form for Unit 771852.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^THANK YOU FOR THE COMPLIMENT.[SOFTBLOCK] THIS UNIT IS GLAD IT COULD SERVE YOU ADEQUATELY.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^No, you didn't actually serve -[SOFTBLOCK] ugh.[SOFTBLOCK] Stupid latex drones!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^I wouldn't insult my personal servitor units.[SOFTBLOCK][EMOTION|55|Smug] Unless you wish to take her place...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^N-n-n-n-n-no![SOFTBLOCK] Unit 771852, you are an excellent unit![SOFTBLOCK] Please continue to serve your Command Unit to the best of your ability.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^COMMAND RECEIVED.[SOFTBLOCK] THIS UNIT WILL SERVE ITS COMMAND UNIT.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^COMMAND UNIT, HOW MAY I SERVE YOU?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^We have places to go, 771852.[SOFTBLOCK] Let's move out.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^And you -[SOFTBLOCK] forget you saw me.[SOFTBLOCK] I wouldn't want to have to come back and do cleanup.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I -[SOFTBLOCK] I will set a program to purge my memories of you during defragment!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] ^*Ha ha![SOFTBLOCK] That was fun!*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^*Maybe we should come back after she's defragmented and do it again.*^") ]])
			
		--First time meeting this Lord Golem as a Latex Drone:
		elseif(iMetSurveyLordGolem == 0.0 and iMetSurveyLordLatex == 0.0) then
	
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordLatex", "N", 1.0)
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^A Drone Unit?[SOFTBLOCK] What are you doing here?[SOFTBLOCK] Did you wander off from the LRT Facility?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Uhh...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Oh dear,[SOFTBLOCK] I hope I didn't ask too complex a question!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] (Grrrrrr!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^*Retain your cover, Unit 771852.*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^*R-[SOFTBLOCK]right!*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^MY DESIGNATION IS UNIT 771852.[SOFTBLOCK] GREETINGS, LORD UNIT.[SOFTBLOCK] HOW MAY I SERVE YOU?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Ah, much better.[SOFTBLOCK] At least you seem to be able to speak coherently.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] (GRRRR!!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^THANK YOU FOR THE COMPLIMENT, LORD UNIT.[SOFTBLOCK] AT LEAST YOU SEEM TO BE ABLE TO SPEAK COHERENTLY, AS WELL.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^What was that, you insolent -[SOFTBLOCK] disrepectful -[SOFTBLOCK] little -[SOFTBLOCK] -[SOFTBLOCK]!!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^THANK YOU FOR THE COMPLIMENT, LORD UNIT.[SOFTBLOCK] WHAT WAS THAT, YOU INSOLENT DISRESPECTFUL LITTLE, AS WELL.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Stupid, [SOFTBLOCK]stupid drone![SOFTBLOCK] Why are you bothering me?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^I see you are verbally abusing my Drone Unit.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^C-c-c-command unit![SOFTBLOCK] Hello![SOFTBLOCK] I was just admiring this unit![SOFTBLOCK] She is -[SOFTBLOCK] shiny?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^THANK YOU FOR THE COMPLIMENT.[SOFTBLOCK] YOU ARE -[SOFTBLOCK] SHINY?[SOFTBLOCK] AS WELL.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^That will be enough, Drone.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^AFFIRMATIVE, COMMAND UNIT.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (Ha ha ha ha ha ha!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^I would recommend against talking down to my servitors, Lord Unit.[SOFTBLOCK] She can be replaced at any time.[SOFTBLOCK] Specifically, by a unit who has displeased me.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^A thousand pardons, Command Unit![SOFTBLOCK] It will not occur again.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^See that it doesn't.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^We have places to go, 771852.[SOFTBLOCK] Let's move out.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^AFFIRMATIVE, COMMAND UNIT.[SOFTBLOCK] MOVING OUT.^") ]])
	
		--Repeat encounters.
		else
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I wish you luck on whatever your assignment is.[SOFTBLOCK] Serve the Cause of Science.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^AFFIRMATIVE.[SOFTBLOCK] THE CAUSE OF SCIENCE WILL BE SERVED.^") ]])
		end
	
	--Eldritch.
    elseif(sChristineForm == "Eldritch") then
    
        --First time.
        if(iMetSurveyLordEldritch == 0.0) then
	
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordEldritch", "N", 1.0)
            
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^My-[SOFTBLOCK]my goodness!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Security!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Well hello, metal one.[SOFTBLOCK] You're looking delectable this cycle.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^S-[SOFTBLOCK]someone!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^That's quite enough.[SOFTBLOCK] Down.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^As you wish, Command Unit.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Command Unit, what is this thing?[SOFTBLOCK] What made it?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^This thing is part of a new program.[SOFTBLOCK] It is top secret.[SOFTBLOCK] I expect you and your team will refrain from reporting on it, lest the reports be classified.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^These creatures, when tamed, make excellent attack dogs.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] ^Though I sometimes lose control of it, you understand.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Can I just nibble at her skin?[SOFTBLOCK] Surely she doesn't need all of it.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Please keep this creature away from me!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^You heard the Lord Golem.[SOFTBLOCK] Leave all of her components intact.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Phew...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] ^Unless she displeases me.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^As you wish, Command Unit.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^How can an organic emit radio waves...?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] ^You have a specific task as a geological survey team.[SOFTBLOCK] I suggest you limit queries to that task.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Yes Command Unit![SOFTBLOCK] Right away!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (Ha ha ha ha ha!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] ^We'll be off now.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Oh, and as a reminder...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^These creatures never rest, grow tired, and are single-minded when given an order.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^It'd be a shame if that order were to [SOFTBLOCK]*find*[SOFTBLOCK] someone.[SOFTBLOCK] Wouldn't it?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Affirmative, Command Unit![SOFTBLOCK] Suspending all further queries![SOFTBLOCK] Have a wonderful day!^") ]])
    
        --Repeats.
        else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Please have a wonderfully productive day, Command Unit!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (Ideally, as far away from me as possible!)") ]])
        end
    
    --Electrosprite.
	elseif(sChristineForm == "Electrosprite") then
    
        --First time.
        if(iMetSurveyLordElectrosprite == 0.0) then
            
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordElectrosprite", "N", 1.0)
            
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Now this...[SOFTBLOCK] What is this creature before me?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^PDU, begin taking scans.[SOFTBLOCK] Log under 'Unidentified Voidborne Partirhuman'.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (She doesn't know what an electrosprite is?[SOFTBLOCK] Time to be mean!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Hello bodied-creature.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Partirhuman species is capable of vocalization and use of short-range radio transmissions.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Hello, unbodied creature.[SOFTBLOCK] Does your kind have a name?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^I am a Trebolyte.[SOFTBLOCK] I am an abassador for my kind.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^I have been observing your 'Golem' kind for a long time.[SOFTBLOCK] My people are in need of a great hero to save them.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^I think you are the one we have been looking for.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Me?[SOFTBLOCK] I am a simple researcher.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Yes, you are.[SOFTBLOCK] But my people have little time.[SOFTBLOCK] You see, we have been slowly dying out for centuries.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^We are being gradually killed off by the Enedytes, or 'grounds' as you call them.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^They devour electricity and disperse it over a wide area.[SOFTBLOCK] We have been at war since the dawn of time.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^But you metal people, you combine both electricity and grounds, to make 'conductors'.[SOFTBLOCK] You could turn the tide of the war!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Please, come with me.[SOFTBLOCK] A grand adventure lies before you, chosen one.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I -[SOFTBLOCK] I cannot.[SOFTBLOCK] I'm sorry, you must have the wrong golem.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I am merely a researcher.[SOFTBLOCK] I survey geological formations.[SOFTBLOCK] I am no hero.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ^Are you sure?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Please continue looking, noble Trebolyte.[SOFTBLOCK] I am flattered, but certain I am not the one you need.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ^I see, perhaps I have erred...^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^I will continue my search.[SOFTBLOCK] I have heard there is a great warrior among you.[SOFTBLOCK] She fears nothing.[SOFTBLOCK] She will save us.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^I must find her to save my people...^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^*I got some good readings on these 'Trebolytes'...[SOFTBLOCK] But it's too risky to try capturing one.*^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Good luck, Trebolyte.[SOFTBLOCK] Find the one you seek.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (Meanwhile, I'll get a promotion for discovering a new partirhuman species!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (This sucker's report should throw off the Administration by muddying the waters a bit![SOFTBLOCK] Ha ha ha!)") ]])
            
        --Repeats.
        else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^*PDU, take readings.*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Good luck, noble Trebolyte.^") ]])
    
        end
    
    --Steam Droid.
    elseif(sChristineForm == "SteamDroid") then
    
        if(iMetSurveyLordSteamDroid == 0.0) then
            
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordSteamDroid", "N", 1.0)
            
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Tch, what have we here.[SOFTBLOCK] Begone, scavenger.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Take anything from my equipment and you'd best believe I will send my security units after you.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Well, that's no way to treat an ambassador!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^*Play along, 55.*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^*Affirmative.*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Insulting an ambassador?[SOFTBLOCK] I think I'll have to log this for disciplinary action.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^C-[SOFTBLOCK]c-[SOFTBLOCK]command Unit![SOFTBLOCK] I didn't see you there![SOFTBLOCK] My apologies!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^I'll let it slide this time.[SOFTBLOCK] I suppose I should expect as much from these uncouth Lord Units.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Uncouth![SOFTBLOCK] Why -[SOFTBLOCK] Command Unit, may I speak to you for a moment?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^*Why are we not recycling this low-life rapscallion?[SOFTBLOCK] Surely she is better melted down for scrap?*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^*We are attempting to foment a civil war amongst the Steam Droids.[SOFTBLOCK] By arming certain groups covertly, we can allow them to eliminate one another.*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^*So put up with this one and grant her requests.[SOFTBLOCK] Confirm order.*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^*...*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^*Affirmative, Command Unit.[SOFTBLOCK] Carrying out orders.*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I apologize, Ambassador.[SOFTBLOCK] May I get your name?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^CD-97.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^I was told I could expect to receive supplies.[SOFTBLOCK] Are we at the pickup site, Command Unit?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Negatory.[SOFTBLOCK] However, any equipment this Lord Golem could furnish that would not be missed would serve our purposes.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^It may even prevent me from filing her for disciplinary action.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^...[SOFTBLOCK] Oh![SOFTBLOCK] Yes!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I see you're using an Electrospear, CD-97.[SOFTBLOCK] Perhaps this foregrip that fell down a chasm and was unrecoverable would suit your needs?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] [SOUND|World|TakeItem]^This will do nicely.[SOFTBLOCK] Thank you.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (If it furthers your elimination, so much the better...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Very good.[SOFTBLOCK] Command Unit?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^There will be no logs of our encounter.[SOFTBLOCK] As you were, Lord Unit.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I do so love free stuff![SOFTBLOCK] Thanks for playing the part!)") ]])
            LM_ExecuteScript(gsItemListing, "Spear Foregrip")
    
        else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Good luck, Ambassador.[SOFTBLOCK] I hope you succeed at your endeavours.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (Specifically, wiping one another out.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Thank you for your aid, Lord Unit.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (Specifically, being so easy to trick![SOFTBLOCK] Ha ha ha!)") ]])
    
        end
    
    --Darkmatter.
	elseif(sChristineForm == "Darkmatter") then
    
        if(iMetSurveyLordDarkmatter== 0.0) then
            
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordDarkmatter", "N", 1.0)
            
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Darkmatter, please don't knock anything over.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Cannot -[SOFTBLOCK] rgh -[SOFTBLOCK] fight it...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Must -[SOFTBLOCK] mess -[SOFTBLOCK] with -[SOFTBLOCK] stuck up -[SOFTBLOCK] ponce![SOFTBLOCK] Arrgh!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Hardmatter.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Did you -[SOFTBLOCK] did you just talk?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^No, I did not.[SOFTBLOCK] Were you speaking to me?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^That Darkmatter -[SOFTBLOCK] it just sent a radio transmission!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^My receptors did not pick anything up.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Hardmatter.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Again![SOFTBLOCK] It spoke!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Do your receptors need a recalibration?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Hardmatter.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^We.[SOFTBLOCK] Know.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^OH MY GOODNESS!!!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Confess.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Command Unit![SOFTBLOCK] I -[SOFTBLOCK] I have been stealing resources for my personal pursuits!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I took three kilograms of organic butter rations and slathered myself with them as my tandem unit watched!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Confess.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Ah![SOFTBLOCK] No![SOFTBLOCK] I -[SOFTBLOCK] I also forced a Slave Unit to record her masturbating with the butter while I admired myself in the mirror!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] ^Uhh...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Confess.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Please, no more![SOFTBLOCK] And then I threatened the Slave Unit with repurposement if she told anyone![SOFTBLOCK] Waahhh![SOFTBLOCK] Let me go!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] ^That's more than enough, Lord Unit!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Atone.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Okay, okay.[SOFTBLOCK] I'll make it up to the Slave Unit -[SOFTBLOCK] I'll give her extra time off and ask if she'd like to be reassigned away from me!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I'm sorry, I swear I'll be good!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^...^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Acceptable.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Kinks.[SOFTBLOCK] Fine.[SOFTBLOCK] Harming.[SOFTBLOCK] Subordinates.[SOFTBLOCK] Not.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I understand!^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^We.[SOFTBLOCK] Always.[SOFTBLOCK] Watching.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Always.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^*sob*^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] ^I will be going now, Lord Unit.[SOFTBLOCK] As you were.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Well, hopefully she won't mistreat any Slave Units now...)") ]])
    
        else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Please, torment me no more, Darkmatter.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (I'll just smile idly and say nothing. )[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offend] (And point straight at her suddenly!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Waahh![SOFTBLOCK] No more, no more!^") ]])
    
        end
	
	end
end