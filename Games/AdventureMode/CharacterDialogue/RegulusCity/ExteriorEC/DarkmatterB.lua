--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    if(iHasDarkmatterForm == 0.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](I'm pretty sure this Darkmatter was rearranging the crates, but since she noticed me, she stopped.)") ]])
    else
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[VOICE|DarkmatterGirl] These crates are organized incorrectly.[SOFTBLOCK] I am reorganizing them to help our hardmatter friends.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[VOICE|DarkmatterGirl] Unfortunately I don't know how to organize them in three dimensions, so I expect to be here for a few thousand years.[SOFTBLOCK] I'll get it right eventually.") ]])
        
    end
end