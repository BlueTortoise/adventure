--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Greetings.[SOFTBLOCK] If you are here for an inspection, please be aware that the lights on the transit tracks turn red when a tram is passing.^[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ^Also we leave the area unpressurized due to the frequency of trams.[SOFTBLOCK] The facilities are adjusted for short-range radio transmissions.^") ]])
end