--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iMetWAFacilityLord = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetWAFacilityLord", "N")
	
	--Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Meeting the lord for the first time.
	if(iMetWAFacilityLord == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iMetWAFacilityLord", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Oh thank goodness, some intelligent conversation.[SOFTBLOCK] Fellow Lord Unit, your visit is appreciated.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Unbelievably, I must share my quarters with the Slave Units on duty here.[SOFTBLOCK] I suffer this indignity with quiet resolve, as you can see.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Quiet resolve involves complaining to the first unit to come by?[SOFTBLOCK] Sure.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^We must all make sacrifices for the Cause.[SOFTBLOCK] This includes Lord Units.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Without question, of course![SOFTBLOCK] Obviously, I would never allow them to actually use my quarters except for defragmentation.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^As much as I am loathe to admit it, allowing my Slave Units additional amenities has increased productivity in Sector 96.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Perhaps you should allow them to use the RVD and the furnishings here?[SOFTBLOCK] The improvement [SOFTBLOCK]*did*[SOFTBLOCK] look excellent on my reports.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^...[SOFTBLOCK] Did it?^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] ^Oh, spectacularly so.[SOFTBLOCK] Just look at the productivity charts for Unit 499323.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^I'll get my PDU to bring them up...[SOFTBLOCK] Oh![SOFTBLOCK] Truly?^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^...[SOFTBLOCK] If I must do this for the Cause of Science...^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^To suffer for a cause is martyrdom.[SOFTBLOCK] Remember that.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Thank you, Unit 771852.[SOFTBLOCK] If you need a favour at any time, I will do my best to aid you.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (Sucker![SOFTBLOCK] Have fun being nice to your Slave Units and increasing efficiency!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (...[SOFTBLOCK] Why do I have to trick Lord Units into doing their jobs properly...?)") ]])
		fnCutsceneBlocker()
	
	--Repeat.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ^Hello, Unit 771852.[SOFTBLOCK] I wish you well.[SOFTBLOCK] Serve the Cause of Science.^[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^To the best of my ability.[SOFTBLOCK] Thank you.^") ]])
		fnCutsceneBlocker()
	end
end