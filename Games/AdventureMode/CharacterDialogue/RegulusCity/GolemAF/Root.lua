--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Whenever I hit the lemon-lime button, an orange drink gets dispensed.[SOFTBLOCK] Perhaps my request got switched with another units?[SOFTBLOCK] Well, maybe not.[SOFTBLOCK] What kind of unit would prefer orange to lemon-lime?") ]])
	fnCutsceneBlocker()

end