--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--Hasn't talked to Sophie yet.
	if(iTalkedToSophie == 0.0) then
	
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh, hello, Lord Golem.[SOFTBLOCK] Please see the maintenance golem for your function assignment.[SOFTBLOCK] Thank you.") ]])
		fnCutsceneBlocker()

	--Has talked to Sophie.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I am being very productive, Lord Golem.[SOFTBLOCK] Thank you for taking an interest.") ]])
		fnCutsceneBlocker()

	end
end