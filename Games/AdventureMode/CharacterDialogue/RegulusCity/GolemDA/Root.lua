--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I am currently unassigned, Lord Golem.[SOFTBLOCK] I am thus reviewing engineering manuals to improve efficiency.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] 'She placed her oral cavity against her tandem's receiver and began to thrust in with her tongue.'[SOFTBLOCK] What sort of manual is this?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh dear![SOFTBLOCK] How did that get on there?[SOFTBLOCK] I -[SOFTBLOCK] I - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Carry on, unit.[SOFTBLOCK] And, please send a copy to my quarters so I might peruse this manual.[SOFTBLOCK] I want to improve my efficiency as well.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Right away, Lord Golem![SOFTBLOCK] And...[SOFTBLOCK] thank you...") ]])
	fnCutsceneBlocker()

end