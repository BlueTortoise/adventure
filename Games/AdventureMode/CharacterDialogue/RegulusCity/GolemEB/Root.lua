--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My Lord Golem keeps dragging me in here to see these bombastic actiongraphs.[SOFTBLOCK] I just keep telling her the explosions are nice, while I've been composing some poetry with my spare CPU time.[SOFTBLOCK] She won't even look at what I've written...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] You should publish it to the station archives.[SOFTBLOCK] Just, use an alternate login, so your Lord doesn't know it was you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ...[SOFTBLOCK] That is a very good idea.[SOFTBLOCK] Thank you, Lord Golem.[SOFTBLOCK] It seems not all of you are -[SOFTBLOCK] well, best not say it.") ]])
	fnCutsceneBlocker()

end