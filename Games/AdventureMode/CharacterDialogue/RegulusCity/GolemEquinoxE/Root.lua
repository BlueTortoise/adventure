--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iSawAuthenticatorCount = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawAuthenticatorCount", "N")
	local iSpokeWithDistressGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N")
    local iCompletedEquinox = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
    local iSawEquinoxReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxReward", "N")
	
	--Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--First time speaking with this golem, hasn't seen authenticator dialogue.
	if(iSawAuthenticatorCount == 0.0 and iSpokeWithDistressGolem == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Eek![SOFTBLOCK] No![SOFTBLOCK] Please don't hurt me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Calm down, we're not going to hurt you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That remains to be seen.[SOFTBLOCK] It might be a trick.[SOFTBLOCK] Don't trust her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: No, it's not a trick![SOFTBLOCK] I won't try anything, you've got to believe me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's all right, really.[SOFTBLOCK] We're friends.[SOFTBLOCK] We got a distress signal and came to investigate.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh, thank goodness![SOFTBLOCK] I sent that distress signal, but I got no response.[SOFTBLOCK] I thought maybe it had been blocked somehow.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your signal was suppressed.[SOFTBLOCK] We're the only ones who received it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It seems that all the systems in Regulus City were programmed to ignore distress calls from Equinox.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: So...[SOFTBLOCK] you're all the help that's coming?[SOFTBLOCK] No security teams?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm afraid so.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can you tell us what happened here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I -[SOFTBLOCK] I don't know for sure.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I was here delivering some spare parts for an experiment.[SOFTBLOCK] Then, a voice came over the speakers and said to initiate First Drummer protocols.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: And then everyone went berserk![SOFTBLOCK] The units here just started attacking each other...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I ran and hid in here...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Do you know what First Drummer is?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: No.[SOFTBLOCK] I'm sorry that I can't help you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unfortunate.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're -[SOFTBLOCK] security units.[SOFTBLOCK] Don't worry, we'll take care of everything.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just stay here and remain hidden.[SOFTBLOCK] We'll come for you when the all clear is sounded.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Please be careful.[SOFTBLOCK] Don't trust any of the units out there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We didn't before, we certainly won't now.") ]])
		fnCutsceneBlocker()

	--Successive dialogues.
	elseif(iSawAuthenticatorCount == 0.0 and iSpokeWithDistressGolem == 1.0) then
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I'll just stay here and try not to let anyone see me.[SOFTBLOCK] Good luck out there.") ]])
		fnCutsceneBlocker()
	
	--Saw authenticator, hasn't spoken with this golem previously.
	elseif(iSawAuthenticatorCount == 1.0 and iSpokeWithDistressGolem == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N", 2.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Eek![SOFTBLOCK] No![SOFTBLOCK] Please don't hurt me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Calm down, we're not going to hurt you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That remains to be seen.[SOFTBLOCK] It might be a trick.[SOFTBLOCK] Don't trust her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: No, it's not a trick![SOFTBLOCK] I won't try anything, you've got to believe me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's all right, really.[SOFTBLOCK] We're friends.[SOFTBLOCK] We got a distress signal and came to investigate.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh, thank goodness![SOFTBLOCK] I sent that distress signal, but I got no response.[SOFTBLOCK] I thought maybe it had been blocked somehow.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your signal was suppressed.[SOFTBLOCK] We're the only ones who received it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It seems that all the systems in Regulus City were programmed to ignore distress calls from Equinox.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: So...[SOFTBLOCK] you're all the help that's coming?[SOFTBLOCK] No security teams?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm afraid so.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can you tell us what happened here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I -[SOFTBLOCK] I don't know for sure.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I was here delivering some spare parts for an experiment.[SOFTBLOCK] Then, a voice came over the speakers and said to initiate First Drummer protocols.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: And then everyone went berserk![SOFTBLOCK] The units here just started attacking each other...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I ran and hid in here...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Do you know what First Drummer is?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: No.[SOFTBLOCK] I'm sorry that I can't help you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Fine then.[SOFTBLOCK] Disable your authenticator chip and we'll be on our way.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Come again?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's a lock on the door to the adminstrator's office here, and it replies to queries with how many authenticator chips are still active.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ours are suppressed.[SOFTBLOCK] Turn yours off, and we might be able to get in.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Uh, can I even do that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] Fortunately, I disabled it with this PDU while you were talking.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Stay here.[SOFTBLOCK] We're going to go speak with the administrator about this.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Be careful.[SOFTBLOCK] She's a trained combat unit.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] At least she'll present a challenge.") ]])
		fnCutsceneBlocker()
	
	--Saw authenticator, hasn't spoken with this golem previously.
	elseif(iSawAuthenticatorCount == 1.0 and iSpokeWithDistressGolem == 1.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N", 2.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Have you had any luck finding out what's going on?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes, we have.[SOFTBLOCK] Disable your authenticator chip and we'll be on our way.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Come again?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's a lock on the door to the adminstrator's office here, and it replies to queries with how many authenticator chips are still active.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ours are suppressed.[SOFTBLOCK] Turn yours off, and we might be able to get in.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Uh, can I even do that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] Fortunately, I disabled it with this PDU while you were talking.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Stay here.[SOFTBLOCK] We're going to go speak with the administrator about this.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Be careful.[SOFTBLOCK] She's a trained combat unit.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] At least she'll present a challenge.") ]])
		fnCutsceneBlocker()
	
	--Saw authenticator, has spoken with this golem previously.
	elseif(iSawAuthenticatorCount == 1.0 and iSpokeWithDistressGolem == 2.0) then
		
        --Finale.
        if(iSawEquinoxReward == 0.0 and iCompletedEquinox == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You're back![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We've dealt with the administrator.[SOFTBLOCK] The area is clear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: By clear, you mean...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] One-hundred percent of offending units are retired.[SOFTBLOCK] The facility is quiet.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ...[SOFTBLOCK] Oh my...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Go back to Regulus City.[SOFTBLOCK] Don't tell anyone you were here.[SOFTBLOCK] The administrator didn't have any records of you that I could see.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If they find out what you know, they'll reprogram you.[SOFTBLOCK] So don't let that happen.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: What -[SOFTBLOCK] why?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The administrators will pay for their crimes in time.[SOFTBLOCK] But you need to worry about yourself right now.[SOFTBLOCK] Stay safe.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I'll...[SOFTBLOCK] go get reassigned.[SOFTBLOCK] I'll stay quiet...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: But if you need anything...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your support will do.[SOFTBLOCK] We are planning an uprising at an unspecified time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh geez, I'm not a soldier.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Here, take some of my work credits.[SOFTBLOCK] You can use them to buy equipment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (Received 200 Work Credits)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Consider it war funding.[SOFTBLOCK] And I'll...[SOFTBLOCK] maybe try to round up some support.[SOFTBLOCK] We can't fight, but I know some golems who could maybe act as manufacturers...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] As I said, worry about your own safety for the moment.[SOFTBLOCK] Don't take any risks.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I won't...[SOFTBLOCK] And, thank you.[SOFTBLOCK] Thank you for saving my life...") ]])
            fnCutsceneBlocker()
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxReward", "N", 1.0)
            local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
            VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 200)
        
        --Repeats.
        elseif(iSawEquinoxReward == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I'll be heading out momentarily.[SOFTBLOCK] Thank you for saving me...") ]])
            fnCutsceneBlocker()
        
        --Hasn't completed Equinox yet.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Good luck.[SOFTBLOCK] I'll try not to attract any attention...") ]])
            fnCutsceneBlocker()
        end
	end
end