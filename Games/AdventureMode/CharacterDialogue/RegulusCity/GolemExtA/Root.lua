--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Normal case:
	if(sChristineForm == "Golem") then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Please take care, Lord Unit.[SOFTBLOCK] If you wish to linger, All My Processors is on...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Is this what you do all day?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Yes.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Do you ever get bored?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] No, Lord Unit.[SOFTBLOCK] There are 62,000 seasons of All My Processors to watch.[SOFTBLOCK] I'm not even caught up yet![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Woah...)") ]])
		fnCutsceneBlocker()

	--Steam droid:
	elseif(sChristineForm == "SteamDroid" or sChristineForm == "LatexDrone" or sChristineForm == "Doll") then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Please take care, Lord Unit.[SOFTBLOCK] If you wish to linger, All My Processors is on...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (She's so caught up in her show that she hasn't even noticed I'm not a golem...)") ]])
		fnCutsceneBlocker()

	--Exotic cases:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Please take care, Lord Unit.[SOFTBLOCK] If you wish to linger, All My Processors is on...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (She's so caught up in her show that she hasn't even noticed I'm not a robot...)") ]])
		fnCutsceneBlocker()
	end
end