--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
    --Golem:
    if(sChristineForm == "Golem") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Hello, Lord Unit.[SOFTBLOCK] I am currently assigned to watch duty out here.[SOFTBLOCK] If you are heading to the Equinox Laboratories, please go south from here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Have you gotten any...[SOFTBLOCK] unusual messages from there, recently?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] No.[SOFTBLOCK] In fact, I haven't gotten any messages from there at all.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] They don't relay much through here except experiment results, so that is no surprise.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] All right then...") ]])
        fnCutsceneBlocker()
    
    --Latex Drone:
    elseif(sChristineForm == "LatexDrone") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh, hello, Drone Unit![SOFTBLOCK] You're looking well.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Are you on the way to the Equinox Labs?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] PLEASE PROVIDE DIRECTIONS.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Head south.[SOFTBLOCK] It's hard to miss.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] NAVIGATING.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Command Unit, please keep an eye on her.[SOFTBLOCK] Don't let her get lost.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] She won't.[SOFTBLOCK] Come along, Drone.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Unit, have you heard any unusual radio traffic from the labs?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] No, nothing.[SOFTBLOCK] I only get results relayed every few days from their experiments.[SOFTBLOCK] It's not unusual for radio silence between then.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Have a nice visit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] HAVING A NICE VISIT IN PROGRESS.") ]])
        fnCutsceneBlocker()
    
    --Electrosprite:
    elseif(sChristineForm == "Electrosprite") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Huh, another test subject?[SOFTBLOCK] Some kind of electricity girl?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Just what are they experimenting on down there?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Equinox Labs is just south of here, Command Unit.[SOFTBLOCK] Mind the ledges.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Have you received any unusual radio traffic from the labs recently?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Recently?[SOFTBLOCK] No.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Be on your best behavior, electricity girl.[SOFTBLOCK] They're a little gruff down there, but they'll let you go once the experiments are over.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ...[SOFTBLOCK] Probably...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I don't like the sound of that at all...)") ]])
        fnCutsceneBlocker()
    
    --Eldritch:
    elseif(sChristineForm == "Eldritch") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Huh, another test subject?[SOFTBLOCK] Never seen an organic that doesn't need a vac-suit out here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Then again, that's probably why you're on the way to Equinox, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Equinox Labs is just south of here, Command Unit.[SOFTBLOCK] Mind the ledges.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Have you received any unusual radio traffic from the labs recently?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Recently?[SOFTBLOCK] No.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I hope the experiments go well.[SOFTBLOCK] Can you imagine genetically modifying humans to resist a vacuum?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We could make an army out of them![SOFTBLOCK] Call them Space Marines![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Sounds oddly familiar to me...)") ]])
        fnCutsceneBlocker()
    
    --Steam Droid:
    elseif(sChristineForm == "SteamDroid") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh, a Steam Droid.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] It's not my business to pass judgement but, I -[SOFTBLOCK] I hope you're not an experiment subject.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I've seen a couple of your kind sent to Equinox.[SOFTBLOCK] They don't get sent back to the city.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] The status of this Steam Droid is not your concern.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Have you received any unusual radio traffic from the labs recently?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Recently?[SOFTBLOCK] No.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] They route experimental data through my comms equipment here, but that's all I get from them.[SOFTBLOCK] It's often days between transmissions.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Good luck, Steam Droid.[SOFTBLOCK] I hope it isn't too painful...") ]])
        fnCutsceneBlocker()
    
    --Darkmatter:
    elseif(sChristineForm == "Darkmatter") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh, a -[SOFTBLOCK] a captured Darkmatter?[SOFTBLOCK] Are you taking her to Equinox, Command Unit?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Then again, she's probably just letting you lead her around.[SOFTBLOCK] They come in here every now and then, there's not much I can do to keep them out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] This Darkmatter is well-behaved enough.[SOFTBLOCK] We may yet get some data out of her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Have you received any unusual radio traffic from the labs recently?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Recently?[SOFTBLOCK] No.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] They route experimental data through my comms equipment here, but that's all I get from them.[SOFTBLOCK] It's often days between transmissions.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I hope you can get some good readings![SOFTBLOCK] Solving the Darkmatter mystery will put Equinox on the map, for sure!") ]])
        fnCutsceneBlocker()
    end
end