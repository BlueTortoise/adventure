--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	local iPipeGolemState = VM_GetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N")
	
	--Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--If on your way for a function assignment, the golem responds much differently.
	if(iTalkedToSophie == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 1.0)
		
		--First time talking to this golem.
		if(iPipeGolemState == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh no![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Lord Golem, I apologize![SOFTBLOCK] I've been very productive![SOFTBLOCK] Please - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] My current function is to report to Regulus City for further assignment.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: H-[SOFTBLOCK]huh?[SOFTBLOCK] Oh![SOFTBLOCK] You're a new unit![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: In that case, Regulus City is just a short walk north of here.[SOFTBLOCK] Head up the ladder, out the airlock, and around the building.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Proceeding to Regulus City for further assignment.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (Really dodged a pulse round there...)") ]])
			fnCutsceneBlocker()
		
		--Repeat.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: In that case, Regulus City is just a short walk north of here.[SOFTBLOCK] Head up the ladder, out the airlock, and around the building.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Proceeding to Regulus City for further assignment.") ]])
			fnCutsceneBlocker()
		end
		
	--Non-Golem Christine. 55 will always be present but the player can't do the quest.
	elseif(sChristineForm ~= "Golem") then
	
        --Human variant.
        if(sChristineForm == "Human") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: A human?[SOFTBLOCK] In here?[SOFTBLOCK] Will this day never end?[SOFTBLOCK] Now I have to convert you![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Mmmmm...[SOFTBLOCK] that'd be nice.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Stand down, unit.[SOFTBLOCK] This organic is in my custody.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh, thank goodness.[SOFTBLOCK] My plate is already full as it is.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ...[SOFTBLOCK] That expression doesn't make any sense for me, now that I think about it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Don't be such a buzzkill, Command Unit![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ...?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Are you serious?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *I want to be converted...[SOFTBLOCK] to feel the nanofluid cover me, fill me...*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] *Shut up, idiot.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That's quite enough, organic.[SOFTBLOCK] I will electrocute you if you misbehave.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Eep![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You have to be firm with these organics.[SOFTBLOCK] They're dumber than even the dumbest latex drone.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Heh.[SOFTBLOCK] Is she dumber than a scraprat?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Sometimes I wonder.[BLOCK][CLEAR]") ]])
            if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We'll take our leave now.[SOFTBLOCK] Back to work, unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Right away![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (I don't know how she's going to get that organic to Regulus City without a vac suit, but I guess it's not my problem.)") ]])
            fnCutsceneBlocker()
            
        --LatexDrone! Beep boop!
        elseif(sChristineForm == "LatexDrone") then
        
            --Sequence.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (Okay, act synthetic...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Greetings, Command Unit.[SOFTBLOCK] Your Drone Unit is looking well-polished.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] COMPLIMENT LOGGED.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Be silent, Drone Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] AFFIRMATIVE.[SOFTBLOCK] BEGINNING SILENCE ROUTINES.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] UNIT HAS BEEN SILENT FOR 1 CYCLE.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] UNIT HAS BEEN SILENT FOR 2 CYCLES.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] UNIT HAS BEEN SILENT FOR 3 CYCLES...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I'm very sorry to interrupt, Command Unit, but does your Drone need maintenance?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: The cognitive inhibitor might be overclocked.[SOFTBLOCK] I don't think I've seen one that dumb before.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] She is not here for her intelligence.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] UNIT IS HERE FOR ITS LOOKS.[SOFTBLOCK] UNIT IS PRETTY.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: So...[SOFTBLOCK] was there something I could help you with?[BLOCK][CLEAR]") ]])
            if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not at the moment.[SOFTBLOCK] Back to work, unit.") ]])
            fnCutsceneBlocker()
            
        --Darkmatter
        elseif(sChristineForm == "Darkmatter") then
        
            --Sequence.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Uhhhhh....[SOFTBLOCK] Command Unit?[SOFTBLOCK] There's a Darkmatter here...[SOFTBLOCK] Is she with you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You could say that.[SOFTBLOCK] She has decided to follow me around for reasons unknown to me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As per usual, my attempts to shoo her away have failed.[SOFTBLOCK] They are difficult to influence with physical weaponry.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You shot at it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Yes.[SOFTBLOCK] Yes I did.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Better not say anything or I'll blow my cover...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] It cowered and hid and I suspect it was crying starlight tears.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] (You're a laugh riot, 55!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Uh, sure.[SOFTBLOCK] Okay.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Was there something I could do for you, Command Unit?[BLOCK][CLEAR]") ]])
            if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not at the moment.[SOFTBLOCK] Back to work, unit.") ]])
            fnCutsceneBlocker()
            
        --Eldritch Dreamer
        elseif(sChristineForm == "Eldritch") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: !!![SOFTBLOCK] Oh my goodness![SOFTBLOCK] There -[SOFTBLOCK] there's[SOFTBLOCK] -[SOFTBLOCK] !![SOFTBLOCK] - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Do not be afraid, my friend.[SOFTBLOCK] I am not here to harm you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Huh?[SOFTBLOCK] Oh.[SOFTBLOCK] Hi.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: *Psst, organic![SOFTBLOCK] There's a Command Unit right behind you!*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: *Act productive!*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh for crying out loud...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This Command Unit is not here to punish you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Yet.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Don't scare the poor thing![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do what I want, when I want.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (Oh goodness, what have I done to deserve this?)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Does it have to involve victimizing some poor worker?[SOFTBLOCK] You scared her half to retirement![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Emotions are not considerations of machines.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Err, Units?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Is there something I can do for you?[SOFTBLOCK] I'm attempting to -[SOFTBLOCK] maximize productivity -[SOFTBLOCK] at the moment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My form doesn't intimidate you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Of course not, Unit.[SOFTBLOCK] Organics come in many shapes and forms.[SOFTBLOCK] I am not judgemental.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: *Just try not to set off that Command Unit or she'll have us both retired!*[BLOCK][CLEAR]") ]])
            if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will settle our differences elsewhere.[SOFTBLOCK] Back to work, unit.") ]])
            fnCutsceneBlocker()
            
        --Electrosprite!
        elseif(sChristineForm == "Electrosprite") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: FIFO?[SOFTBLOCK] Is that -[SOFTBLOCK] oh no you're not her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Do I remind you of someone -[SOFTBLOCK] someone special to you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Heh, a little.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Uh, should I assume that this Command Unit is...[SOFTBLOCK] cool?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She's a sympathizer.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: And is she your tandem unit?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] No.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Woah, calm down![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Sorry, I just assumed since, well, that's how Electrosprites come to be.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're just good friends.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: FIFO is sort of a -[SOFTBLOCK] synchro-buddy, I guess.[SOFTBLOCK] We're not an item, but we do synchronize.[SOFTBLOCK] Often.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: She's kind of odd.[SOFTBLOCK] I send her a message on the network, but she has other syncrho-buddies and always visits us in the exact order we messaged her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Is it weird that I share her with five other golems?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Not at all.[SOFTBLOCK] Love takens many forms.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You're all consenting adults, right?[SOFTBLOCK] If you can all get along, there's no harm in it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I still feel odd about it, but the synchronizations are so good...[SOFTBLOCK] I'd probably go crazy without her in my life.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: The Electrosprites becoming real is probably the best thing that's ever happened to Regulus City, in my opinion.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're so happy to help![SOFTBLOCK] Just be sure to keep us a secret from the Administration![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Of course![SOFTBLOCK] I'd never betray FIFO![SOFTBLOCK] I'd sooner be retired![BLOCK][CLEAR]") ]])
            if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] That confirms my initial assessment of the security situation.[SOFTBLOCK] We'll take our leave, then.") ]])
            fnCutsceneBlocker()
            
        --Steam Droid
        elseif(sChristineForm == "SteamDroid") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Well there's something you don't see everyday.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, don't worry.[SOFTBLOCK] I'm a friend.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: *I'm more worried about the Command Unit right behind you...*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, her?[SOFTBLOCK] She's cool.[SOFTBLOCK] Right, Command Unit?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Cool as ice.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Are you a -[SOFTBLOCK] security contractor, or something?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] To that effect, yes.[SOFTBLOCK] And she is currently on a secret mission.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Obviously you are to omit that detail in any reports you make.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Yes, Command Unit![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: So was there anything I could do for you?[BLOCK][CLEAR]") ]])
            if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not at the moment.[SOFTBLOCK] Back to work, unit.") ]])
            fnCutsceneBlocker()
            
        end
	
	--Normal.
	else
	
		--First time talking to the Golem after conversion, didn't talk to her the first pass:
		if(iPipeGolemState == 0.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 2.0)
			fnStandardMajorDialogue()
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Lord Golem![SOFTBLOCK] H-[SOFTBLOCK]Hello![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hello, unit![SOFTBLOCK] May I get your designation?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Unit 565102...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Please don't be too hard on me...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Why would I be hard on you?[SOFTBLOCK] Is something wrong?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: You didn't -[SOFTBLOCK] oh.[SOFTBLOCK] Err, well.[SOFTBLOCK] I can explain.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Everything was going well down here, but then this stupid scraprat bit through one of the pipes and wrecked the delicate balance of pressure I had come up with.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: ...[SOFTBLOCK] Oh, and then he kind of exploded.[SOFTBLOCK] They do that sometimes.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Oh dear![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: I just got done putting him back together, but now -[SOFTBLOCK] ugh![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Energy transfer is down by 70 percent and I have to get the pipe pressures realigned with the missing section removed.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: You didn't know all this?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sorry.[SOFTBLOCK] Did you put in a work order?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Oh I don't want to bother 499323.[SOFTBLOCK] She's always so backlogged, Maintenance and Repair in Sector 96 will probably take months to come help.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I am Unit 771852, Lord Golem of Maintenance and Repair, Sector 96.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: So you're here to fix the pipes?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I am often told I have great timing.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: I'll say![SOFTBLOCK] You're a lifesaver![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Oh and, please tell 499323 I said hi.[SOFTBLOCK] She's always very sweet when I go in for maintenance.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So.[SOFTBLOCK] How do I go about fixing the pipes?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Well, it's a little complex. I can explain how the pipes work, and then maybe you can give it a shot.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare") ]])
		
		--First time talking to the golem after conversion, talked to her the first pass:
		elseif(iPipeGolemState == 1.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 2.0)
			fnStandardMajorDialogue()
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You're back![SOFTBLOCK] It's been a while...[SOFTBLOCK] Did you get your function assignment?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I did![SOFTBLOCK] Thank you for the directions![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I was assigned as Lord Golem of Maintenance and Repair, Sector 96.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Wait.[SOFTBLOCK] They got a Lord Golem?[SOFTBLOCK] Really?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Wow, how long has it been?[SOFTBLOCK] Poor 499323 is always in there by herself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Uh, sorry.[SOFTBLOCK] I got a little off course.[SOFTBLOCK] Are you here to fix the pipes?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No.[SOFTBLOCK] Is there something wrong with them?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Well, yes![SOFTBLOCK] This stupid scraprat bit through a pipe and ruined my delicate balance of pressure alignment![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ...[SOFTBLOCK] And then he blew up.[SOFTBLOCK] I think they do that when they get flustered.[SOFTBLOCK] Or bored.[SOFTBLOCK] Maybe.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I put him back together, but that was the easy part...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I don't think I can fix the pipes by myself, and power transfer is down by 70 percent.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sounds like you're in need.[SOFTBLOCK] Lucky I came by.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I should put in a work order before I get started.[SOFTBLOCK] May I have your designation?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Unit 565102.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] All right, my PDU will file the work order.[SOFTBLOCK][EMOTION|Christine|Smirk] So.[SOFTBLOCK] How do I go about fixing the pipes?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Here, I'll try to explain...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare") ]])
		
		--We already talked to the golem, but haven't fixed the pipes.
		elseif(iPipeGolemState == 2.0) then
			fnStandardMajorDialogue()
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Any questions for me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare") ]])
	
		--Talking to the golem, the pipes have just been fixed.
		elseif(iPipeGolemState == 3.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 4.0)
			fnStandardMajorDialogue()
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Thanks again for all your help.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Hey, do you think I should maybe remove the teeth on my scraprat here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, I don't think so.[SOFTBLOCK] Maybe give him some pipe chunks to chew on so he doesn't bite the real ones?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: That's a good idea, maybe I'll do that.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: And then wonder why a scraprat was coded to want to chew on things...[SOFTBLOCK] oh well.[SOFTBLOCK] I guess it's fundamental to being a rat, isn't it?") ]])
			fnCutsceneBlocker()
		
		--Successive cases.
		else
			fnStandardMajorDialogue()
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "565102: Thanks for your help, Lord Unit.[SOFTBLOCK] Give Unit 499323 my best when you see her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Will do!") ]])
			fnCutsceneBlocker()
	
		end

	end

end