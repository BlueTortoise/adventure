--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment, the scraprat responds much differently.
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The scraprat smiles stupidly.[SOFTBLOCK] It seems to be giggling periodically.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This has nothing to do with my programming.[SOFTBLOCK] I should proceed to Regulus City for further assignment.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Scraprat:[VOICE|Drone] Scraprat primed and ready![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] No![SOFTBLOCK] Scraprat un-prime![SOFTBLOCK] Un-prime![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Phew...[SOFTBLOCK] They say that right before they explode...") ]])
		fnCutsceneBlocker()

	end

end