--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Face the party.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local iTalkedToWarden = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToWarden", "N")
	
	--First scene.
	if(iTalkedToWarden == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToWarden", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Warden: GREETINGS UNIT 2855.[SOFTBLOCK] WELCOME BACK.[SOFTBLOCK] ARE YOU HERE TO RESUME REHABILITATION?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Rehabilitation?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You recognize me, drone?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Warden: YES, UNIT 2855.[SOFTBLOCK] YOU WERE LAST HERE 31 DAYS AGO.[SOFTBLOCK] YOU SPENT MOST OF YOUR VISIT REHABILITATING UNIT 278101.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Warden: UNIT 287101 IS STILL IN HER CELL.[SOFTBLOCK] SHE HAS NOT COMMITTED ANY MORE CRIMES SINCE YOU LAST VISITED.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] May we see Unit 287101?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Warden: REQUEST ACCEPTED.[SOFTBLOCK] THE DOOR IS OPEN.[SOFTBLOCK] HAVE A NICE DAY.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *PDU, download the warden's auth codes discreetly, please.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: *Downloading...[SOFTBLOCK] done![SOFTBLOCK] Shall I steal anything else while I am at it?*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *No, that will be all.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Let's go see Unit 287101...") ]])
		fnCutsceneBlocker()

	--Repeat.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Warden:[VOICE|LatexDrone] EVERYTHING IN THE REHABILITATION BLOCK IS SPIFFY, COMMAND UNITS.[SOFTBLOCK] THANK YOU FOR VISITING.") ]])
		fnCutsceneBlocker()
	end
end