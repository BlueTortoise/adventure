--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iTalkedToMinesLord = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToMinesLord", "N")
	local iLowestMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowestMinesFloor", "N")
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Has not talked to the lord here:
	if(iTalkedToMinesLord == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToMinesLord", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Hmm, we have visitors?[SOFTBLOCK] To what do I owe the pleasure?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 771852 reporting.[SOFTBLOCK] Were you the one who put in the work request?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Ah, 771852![SOFTBLOCK] I'm glad you've taken up the request, because I'm at my wit's end.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] And...[SOFTBLOCK] A command unit?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am currently supervising Unit 771852's activities.[SOFTBLOCK] Please behave as though I am not here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Performance reviews, is it?[SOFTBLOCK] I can sympathize.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] I am Unit 692339, and I am currently in charge of security and maintenance here.[SOFTBLOCK] It is under both categories that I require your aid.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Security and maintenance, but not mining?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] This sector was nicknamed 'The Tellurium Mines' due to its very rich ore deposits of rare-earth metals, and the interesting pink sheen the tellurium ore has.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] The very high concentration ore has since been mined out, and active extraction will likely resume when other high-concentration ore elsewhere is depleted.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] Until then, our task is to maintain the transit network and mothballed mining equipment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I see.[SOFTBLOCK] So you don't have much of a security detachment?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] We did, but they were requisitioned recently.[SOFTBLOCK] It's now down to me and a handful of my Slave Units, who are not particularly well suited for violence.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] A spike in the number of security breaches has forced me to withdraw to this transit station.[SOFTBLOCK] The mines are effectively unpatrolled.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So you want me to deal with the security breaches.[SOFTBLOCK] That shouldn't be too difficult.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] That, and your repair skills will be invaluable.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] We have quick-access mineshafts that can be accessed via elevator.[SOFTBLOCK] Unfortunately, our elevators have been damaged recently.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] Roughly once every ten floors, you will find an elevator.[SOFTBLOCK] If you can repair it, you can return to that floor quickly.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] The repair grid indicates that parts are being removed wholesale, not damaged.[SOFTBLOCK] Obviously, I am not in a position to investigate.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right, so fix the elevators and deal with the security issues.[SOFTBLOCK] Right up my alley![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] Thank you, 771852.[SOFTBLOCK] I've set a terminal near the mine entrace southwest of here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] The terminal will track your progress and dispense work credits as you complete the assignments.[SOFTBLOCK] If there's anything else we can do to help, let us know.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll have it taken care of before you know it.") ]])
		fnCutsceneBlocker()
		
	--Already spoken to the lord:
	else
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] Hello, 771852.[SOFTBLOCK] How goes the mission?[BLOCK][CLEAR]") ]])
		
		if(iLowestMinesFloor < 1) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Haven't started yet.[SOFTBLOCK] I'm getting any additional information I can before heading out.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] I've instructed the Slave Units here to give you whatever assistance you need.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] Though it seems more likely that they will be asking you for assistance.[SOFTBLOCK] Please ignore their requests.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] Any assignments are to go through formal channels.[SOFTBLOCK] Please report any delinquints to me for discipline.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Yeah, not likely...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Of course.[SOFTBLOCK] We have regulations for a reason.[SOFTBLOCK] I'll be getting to it, then.") ]])
		elseif(iLowestMinesFloor < 10) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Made some initial progress.[SOFTBLOCK] There's a lot of ground to cover.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] I understand.[SOFTBLOCK] Do not take any risks unnecessarily, as retrieving a damanged unit from the mines is...[SOFTBLOCK] inefficient...") ]])
		elseif(iLowestMinesFloor < 20) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I've fixed one of the elevators.[SOFTBLOCK] You're right, parts are being taken wholesale.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] Hmmm, the Darkmatters have been known to randomly move or knock over objects...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Is she not aware of the creatures down there?)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, yes, of course.[SOFTBLOCK] It's probably them.") ]])
		elseif(iLowestMinesFloor < 30) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The security problems seem to get worse as I go deeper in the mines.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] To be expected, honestly.[SOFTBLOCK] I'm surprised you don't have a security escort with you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, this Command Unit is no slouch in combat herself.[SOFTBLOCK] We manage.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] Is that a Mk. V Pulse Diffractor?[SOFTBLOCK] My my, you are well equipped...") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The repair situation should be under control now, but the security situation...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I won't be able to resolve that myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] While I appreciate your efforts, I would not like you unnecessarily risking yourself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "692339:[E|Neutral] Hopefully when whatever crisis is afflicting the city is resolved, we will be able to send proper patrols in.[SOFTBLOCK] As it is, I thank you for your efforts.") ]])
	
		end
	end
end