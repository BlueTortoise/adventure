--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iTalkedPartsGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedPartsGolem", "N")
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--First conversation:
	if(iTalkedPartsGolem == 0.0) then
	
		--Flag
		VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedPartsGolem", "N", 1.0)
	
		--Dialogue
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] L-[SOFTBLOCK]lord Golem?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 771852 reporting.[SOFTBLOCK] How may I help you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Erm, are you asking me -[SOFTBLOCK] wait, you want to help me?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Of course, fellow Unit.[SOFTBLOCK] Your sector is in need of assistance, is it not?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] (Is she for real, right now?)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Erm, I don't think I'm supposed to put in informal requests, but all my official requests get denied.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We're very low on spare parts.[SOFTBLOCK] As in, I don't think we can fix the next tram that comes in for servicing.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But you can't requisition some?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Everything is prioritized for security purposes right now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Fabricate some?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Backed up with work orders for security units.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Listen, 771852, this is none of your business.[SOFTBLOCK] We'll be okay.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm a Maintenance and Repair Unit.[SOFTBLOCK] I know how you feel.[SOFTBLOCK] Seeing damaged equipment and being unable to fix it...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Isn't it just depressing?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Well, uh, informally...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] If you find any spare parts when you're in the mines, could you give me some?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I'll give you 6 work credits for every 1 Assorted Parts you provide.[SOFTBLOCK] Is that okay?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Really, you're providing work credits?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I've got a lot banked up, and it's not like I can spend them with the fabricators are booked solid.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't know if I can accept them...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Please?[SOFTBLOCK] I'd feel awful if you risked your chassis without any reward.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Just come talk to me if you have any parts, and I'll take them off your hands.[SOFTBLOCK] They'll be put to good use.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] And -[SOFTBLOCK] I guess I'll have to figure out some way of fudging the logs.[SOFTBLOCK] Parts appearing from nowhere would look bad...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] PDU, please transfer some of your network access algorithms to this unit's console.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Those should allow you to [SOFTBLOCK]-ahem-[SOFTBLOCK] discreetly edit the work logs.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Wow...[SOFTBLOCK] You trust me with this?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're all on the same team, fellow Unit.[SOFTBLOCK] Now, I'll see about getting you those parts.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Th-[SOFTBLOCK]thank you, Lord Golem![SOFTBLOCK] Thank you so much!") ]])
		fnCutsceneBlocker()
	
	--Future conversations.
	else
	
		--Parts count.
		local iPartsCount = AdInv_GetProperty("Item Count", "Assorted Parts")
		local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
		
		--If it's 1 or more, work credits dialogue.
		if(iPartsCount >= 1) then
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Do you have any spare parts, Lord Golem?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Here you go![SOUND|World|TakeItem][SOFTBLOCK] Make sure to edit the work logs.[BLOCK][CLEAR]") ]])
			
			--Variable string.
			local sString = "WD_SetProperty(\"Append\", \"Golem:[E|Neutral] Oh, of course.[SOFTBLOCK] That's " .. iPartsCount .. " parts for a total of " .. iPartsCount*6 .. " work credits.[SOFTBLOCK] Here you are.[BLOCK][CLEAR]\")"
			fnCutsceneInstruction(sString)
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We always need more parts, so come back if you find more.[SOFTBLOCK] Thanks!") ]])
			
			--Provide work credits.
			VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + (iPartsCount * 6))
			
			--Remove the parts from the inventory.
			for i = 1, iPartsCount, 1 do
				AdInv_SetProperty("Remove Item", "Assorted Parts")
			end
			
		--Otherwise, not enough:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Do you have any spare parts, Lord Golem?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Not at the moment, sorry.[SOFTBLOCK] I'll see what I can find.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Thanks so much for doing this.[SOFTBLOCK] You have no idea how much it means to me.") ]])
		end
	end
end