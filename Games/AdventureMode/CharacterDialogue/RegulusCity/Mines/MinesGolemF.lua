--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I swear the GPU on this thing is shot.[SOFTBLOCK] There's so much stutter and input lag - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Lord Unit![SOFTBLOCK] I was being -[SOFTBLOCK] productive![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Oh, don't worry about me.[SOFTBLOCK] I used to play video games too, when I was younger.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Really?[SOFTBLOCK] Have you ever played Mr. Needlemouse?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Can't say that I have.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Everyone says Needlemouse Mania is the best, but I really liked Mr. Needlemouse 2.[SOFTBLOCK] You should play it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Sure, next time I have some time off, I'll give it a whirl.") ]])
	fnCutsceneBlocker()
	
end