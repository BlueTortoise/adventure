--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemLordA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Stay back, human![SOFTBLOCK] I -[SOFTBLOCK] I have superior strength and I'm not afraid to use it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Yes you are.[SOFTBLOCK] You're shaking in your R-77 Integrated Footwear Modules![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Don't belch on me again![SOFTBLOCK] Eeek!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemLordB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Stop![SOFTBLOCK] I implore you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Humans never, ever stop shedding![SOFTBLOCK] I produce over 30,000 skin cells every day, and of course that has to fall off![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] You'll choke the city in fallen skin-cell dust, you brute![SOFTBLOCK] Have you no shame?") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemLordC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] You can't talk to me normally.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You're not getting past me, human![SOFTBLOCK] And -[SOFTBLOCK] that's about it, actually.[SOFTBLOCK] Please leave me alone!") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Eek![SOFTBLOCK] A human![SOFTBLOCK] Don't get your greasy skin on my chassis!") ]])
        
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Hey, human, what's it like being mostly water?[SOFTBLOCK] Do you hear yourself sloshing when you move?") ]])
        
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You won't get past us![SOFTBLOCK] Even your primitive ape-strength won't move 85 kilograms of metal woman!") ]])
        
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Don't make us hurt you, human.[SOFTBLOCK] Seriously.[SOFTBLOCK] We really don't want to fight.") ]])
        
    elseif(sActorName == "GolemF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Well, stopping a rogue human sure beats fabricator duty.[SOFTBLOCK] You keep terrorizing, human.") ]])
        
    elseif(sActorName == "GolemG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh my goodness, this is just like my video game:: Human Wave![SOFTBLOCK] Do you shoot slow-moving fireballs I have to dodge, too?") ]])
        
    elseif(sActorName == "GolemH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You probably can't harm us with your meat-fists, so I'll just stand here until you get tired and fall asleep.") ]])
        
    elseif(sActorName == "GolemI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'm going to just stand here and solve math problems that your organic brain could never fathom.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Curses![SOFTBLOCK] The only advantage organic computation had over synthetic was the limits of digital computing![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Once quantum and analog computing became advanced enough, we organics were totally obsoleted![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Uhhhh, yeah.[SOFTBLOCK] You're right.[SOFTBLOCK] So...[SOFTBLOCK] come quietly, please?") ]])
        
    elseif(sActorName == "GolemJ") then
        
    elseif(sActorName == "GolemK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Did you escape from the abductions teams or something?[SOFTBLOCK] How did you get here?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] My organic memories are incomplete and easily misled![SOFTBLOCK] I can barely remember what I did ten minutes ago![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Wow, too bad.[SOFTBLOCK] I can remember every moment of my life post-conversion with total accuracy.") ]])
    end
end