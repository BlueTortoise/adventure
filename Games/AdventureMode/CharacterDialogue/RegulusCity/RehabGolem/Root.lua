--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--First time.
	local iTalkedWithRehabGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N")
	if(iTalkedWithRehabGolem == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "278101: Unit is a good unit.[SOFTBLOCK] Unit is an obedient unit...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hello, Unit 278101...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "278101: Hello, Unit 2855.[SOFTBLOCK] This unit is ready for further rehabilitation.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There will be no need for that.[SOFTBLOCK] We're here to set you free.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "278101: This unit is unsure of your intended meaning.[SOFTBLOCK] It is obedient and ready for further rehabilitation.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 278101.[SOFTBLOCK] I am -[SOFTBLOCK] I am discharging you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "278101: Is this -[SOFTBLOCK] no, Unit does not question orders.[SOFTBLOCK] Unit will be discharged as intended.[SOFTBLOCK] Unit is obedient.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Listen, 278101 -[SOFTBLOCK] do you have a secondary designation I can use?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "278101: Unit 278101 has no secondary designation.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "278101: But if you wanted to, you could call her Mina...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Mina, I know you've been hurt here.[SOFTBLOCK] But, we're here to put an end to that.[SOFTBLOCK][EMOTION|Christine|Neutral] You're free now.[SOFTBLOCK] You can go back to your old life.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It's true.[SOFTBLOCK] There's no trick.[SOFTBLOCK] We mean it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: Really?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: You're not doing Good Unit, Bad Unit?[SOFTBLOCK] I have nothing I can be interrogated about.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral]  And, Unit 2855 has something she'd like to say, as well.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I do?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] *Say you're sorry!*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] You will receive a full pardon, 278101.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Say it!*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] *But I don't even remember what I did!*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Do you think that matters to her?*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Mina -[SOFTBLOCK] I'm sorry.[SOFTBLOCK] For what I did to you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: No you're not.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: I -[SOFTBLOCK] I'm sorry![SOFTBLOCK] This unit is obedient![SOFTBLOCK] This unit didn't mean it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] No, you have every right to disbelieve me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: I do?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Yes.[SOFTBLOCK] You don't have to think I'm being sincere, but I am.[SOFTBLOCK] I want to become a better person.[SOFTBLOCK] Some day I hope you can forgive me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She means it, Mina.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: I don't even know if this is a trick any more...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: I -[SOFTBLOCK] I'm an obedient unit.[SOFTBLOCK] I will go back to my quarters and get a new function assignment.[SOFTBLOCK] I won't break the law again.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] *Even when we're apologizing, she can't believe us...*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] *She's been isolated for months.[SOFTBLOCK] I wouldn't believe us either.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] PDU, send her pardon along to the warden.[SOFTBLOCK] She is to be let out at her earliest convenience.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The LRT facility is currently in lockdown, but we've cleared a path for you.[SOFTBLOCK] If you wish to wait until the lockdown is removed, that's up to you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: Thank you for your pardon, command unit.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mina: Please excuse me...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Mina leaves the prison.
		fnCutsceneMove("RehabGolem", 29.25, 12.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneFace("55", 0, 1)
		fnCutsceneWait(185)
		fnCutsceneBlocker()
		fnCutsceneFace("RehabGolem", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		fnCutsceneMove("RehabGolem", 17.25, 12.50)
		fnCutsceneMove("RehabGolem", 17.25, 23.50)
		fnCutsceneBlocker()
		fnCutsceneTeleport("RehabGolem", -100.25, -100.50)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] She didn't believe me...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She'll come around, eventually.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You'll have to visit her later.[SOFTBLOCK] Find a way to make amends on her terms.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] And if she never forgives you, you'll have to be strong enough to live with that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I'm a machine.[SOFTBLOCK] I am unbothered.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh, you really believe that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] If you were just a machine you wouldn't be struggling to find the words, would you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We have the warden's auth codes.[SOFTBLOCK] Let's get going.") ]])
		fnCutsceneBlocker()
	end
end