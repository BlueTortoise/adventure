--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Variables
    TA_SetProperty("Face Character", "PlayerEntity")
    local iMetFriendGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetFriendGolem", "N")
    if(iMetFriendGolem == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMetFriendGolem", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] L-L-L[SOFTBLOCK]-L-L[SOFTBLOCK]-LORD GOLEM.[SOFTBLOCK] HELLO![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] H-[SOFTBLOCK]How are y-y-[SOFTBLOCK]you!?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Woah, calm down, Linda![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You know this unit?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Christine, meet Linda.[SOFTBLOCK] She works in the habitation domes.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] E-[SOFTBLOCK]everything is fine, Lord Unit![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Linda, this is Christine.[SOFTBLOCK] You know, my *tandem unit*.[SOFTBLOCK] She's chill.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Outrageously chill?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Word![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] (I don't know why Christine told me to say those things, but she smiles whenever I do...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] How chill is chill?[SOFTBLOCK] Because I need someone to be extremely - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Something: [SOUND|World|Chirp]*Chirp*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Chirp?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] Ha ha![SOFTBLOCK] What a funny noise![SOFTBLOCK] My -[SOFTBLOCK] linguistic routines just thought I should - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Something: [SOUND|World|Chirp]*Chirp*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're maintenance robots, you know.[SOFTBLOCK] Do you need a checkup?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Linda, Christine is not going to punish you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] Sophie...[SOFTBLOCK] promise you won't be mad but...[SOFTBLOCK] Here...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] A bird?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It's pretty cute, but are you allowed to have organic pets?[SOFTBLOCK] Especially here?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] It's not a pet![SOFTBLOCK] Argh![SOFTBLOCK] I'm so doomed![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] Lord Unit Christine, I know what I did was wrong, but please listen to me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I don't think she's even capable of acknowledging me properly...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Slave Unit, explain your current activities.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] Okay, okay, okay...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] I was in the habitats, doing what I'm supposed to do.[SOFTBLOCK] I was very productive.[SOFTBLOCK] I was clearing away branches from the power conduction lines.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] And I saw this little fella.[SOFTBLOCK] He's marked as Pest Control Vector #449231.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] I think he flew into something because his wing was broken and he was just flopping around on the ground and I couldn't just *leave* him![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] So I kinda birdsnatched him and used some repair nanites to fix his wing...[SOFTBLOCK] And now I'm here...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are any nanites appropriated for maintaining organic pest control vectors?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] No![SOFTBLOCK] And if I put him back in the habitat my Lord Unit will find out and will probably order this little guy recycled![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay, so I think I see the problem.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie, time to work my Lord Unit magic.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] PDU, please send a message to Linda's Lord Unit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] *Oh no!*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] I apprehended this Slave Unit attempting to smuggle contraband out of the habitat and have given her a severe reprimand.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] As punishment for her activities, she is to spend one day per week working in my repair facilities in Sector 96 until I so deem her debt to society repaid.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] I will provide a budgetary recompense if requested.[SOFTBLOCK] Our repair bay has spare work credits allocated to it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] I will be happy to discuss the matter tomorrow in person if required. This Slave Unit will be spending it in my repair bay.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Your's cordially, Unit 771852. PDU, send message.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Slave Unit, your assignment tomorrow will be to construct a proper habitat for this organic pest control vector.[SOFTBLOCK] We have some schematics for cages.[SOFTBLOCK] I'll also need to obtain some food supplies.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] W-[SOFTBLOCK]what?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Look at his little wing.[SOFTBLOCK] The nanites fixed the tissue but he doesn't know how to use it yet.[SOFTBLOCK] See how he's fluttering?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] Yeah, I thought maybe they didn't work...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] He'll need help training to use his wing again.[SOFTBLOCK] It may take some time.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So you're going to make a habitat for him and come by once a week to visit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Whenever he's ready, you're going to reintroduce him to the habitation domes.[SOFTBLOCK] I'll fudge the paperwork for you, it's not hard with the programs I have.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] But what if...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm sorry, Linda.[SOFTBLOCK] I wish there was a way I could do this without it being by fiat.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You clearly know more about organic habitat management than I do, and I trust your judgement.[SOFTBLOCK] But our city has decided Lord Units like me count for more.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I have to fix this now this way, but someday I won't have to.[SOFTBLOCK] Do you follow me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] ...[SOFTBLOCK] I think so?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good.[SOFTBLOCK] Deliver this little guy to the Sector 96 Repair Bay in an hour and we'll find a spot for him.[SOFTBLOCK] Don't let anyone see him.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Something: [SOUND|World|Chirp]*Chirp*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Or hear him, in this case![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I told you she was the best.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] But what if someone sees me...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] The Lord Units in this sector never come to the back, right?[SOFTBLOCK] Just use the delivery trams.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Put the little guy in a box and take the tram right to our repair bay.[SOFTBLOCK] You can use our basement![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] Okay.[SOFTBLOCK] Uh, thanks, Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] And I'm sure little Herbert here thanks you, too.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie, please welcome our newest repair unit, Herbert![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Looking forward to working with both of you.") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Linda:[E|Neutral] I'll deliver little Herbert to the Sector 96 repair bay in an hour, like you said, Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] See you tomorrow, Linda![SOFTBLOCK] Making a habitat for him will be so fun!") ]])
        fnCutsceneBlocker()

    end
end