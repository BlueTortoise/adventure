--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "Golem119AA") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Can I get you a ChemFuel, Lord Unit?[SOFTBLOCK] Just -[SOFTBLOCK] remember not to spill it and contact a cleanup crew if you do...") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119AB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You're not supposed to be able to reach me.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119AC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] I've spent so much time chatting, my ChemFuel has grown cold![SOFTBLOCK] But, best not to throw it out, or it'll dissolve the disposal pipes.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119AD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] *Sigh*.[SOFTBLOCK] I didn't get invited to the Gala this year.[SOFTBLOCK] Oh well.[SOFTBLOCK] There's always next year.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I wouldn't be so sure of that...)") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119AE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] I transferred from the security forces last year and got put in charge of fabric synthesis in Sector 188.[SOFTBLOCK] Turns out its three times more stressful.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Why is that?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Errors in fabric synthesis mean errors in clothing.[SOFTBLOCK] Turns out most Lord Units care more about that than security breaches...") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119AF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] When I'm feeling extra generous, I even allow my subordinates to have a cup of ChemFuel between work shifts.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119AG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My Lord Golem is so generous, she allows me to lick the spilled ChemFuel off her shirt.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I told her I wasn't low on power but she insisted...") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119AH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Please enjoy your shopping experience, Lord Unit.[SOFTBLOCK] If a staff member can do anything for you, let us know immediately.") ]])
        fnCutsceneBlocker()
    end

end