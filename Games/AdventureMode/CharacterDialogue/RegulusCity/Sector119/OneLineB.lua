--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "Golem119BA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Shoes and dresses![SOFTBLOCK] Completely unique, no matter what anyone else says![SOFTBLOCK] Really, we mean it!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119BB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Shoes and dresses![SOFTBLOCK] Completely unique, no matter what anyone else says!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119BC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Flooring and special footwear here![SOFTBLOCK] Bring a special touch to your quarters or work area!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119BD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Custom made dolls![SOFTBLOCK] Handwoven by the talented Slave Units in Sector 256's fabrication bays!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119BE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Hardware, appliances here![SOFTBLOCK] Please let us get you the finest in convenience items!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "Golem119BF") then
        
        --Variables.
        local iSpokeToGolemStoreGolem= VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeToGolemStoreGolem", "N")
        if(iSpokeToGolemStoreGolem == 0.0) then
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeToGolemStoreGolem", "N", 1.0)
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Lord Unit?[SOFTBLOCK] May I -[SOFTBLOCK] help you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Just browsing, thank you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Though, is it my optical sensors malfunctioning, or is your inventory composed of items for Slave Units?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Of course it is, Lord Unit.[SOFTBLOCK] This is our shop![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Run by Slave Units, for Slave Units![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ...[SOFTBLOCK] My Lord Unit allows me to run the store because she thinks it's amusing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] But you get your own shop![SOFTBLOCK] That's wonderful![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Do you get a lot of customers?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] In the shop itself, no.[SOFTBLOCK] But we do fill orders sent to us from other sectors.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But if your Lord Unit ordered the store shut down...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I'd shut it down, of course.[SOFTBLOCK] I would never disobey my Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] *This area is probably monitored more heavily than most, Sophie.[SOFTBLOCK] Be careful what you say.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] *Always.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] That's great![SOFTBLOCK] I'm sure your Lord Unit knows what's best.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I think it's great.[SOFTBLOCK] Slave Units can spend their spare work credits and partake in the highest purpose in life![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Which...[SOFTBLOCK] is?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Looking great, of course![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] O-[SOFTBLOCK]of course, Lord Unit.[SOFTBLOCK] Were you interested in purchasing something for your subordinate?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *Or just taking our stock because we can't stop you...*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Anything catch your eye, Unit 499323?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] No thank you, Lord Unit.[SOFTBLOCK] Thank you for taking an interest, Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, all right.[SOFTBLOCK] Thank you for your time, service unit.") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Thank you for your kind words, Lord Unit.[SOFTBLOCK] If you would like to purchase anything for a subordinate, we do have an entry on the network.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We usually ship orders within two minutes of receiving them, so don't be shy![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Wow, two full minutes?[SOFTBLOCK] How inefficient![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] It's because Slave Units don't get shipping priority on the transit network, Lord Unit.[SOFTBLOCK] I apologize.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ah, I see.[SOFTBLOCK] Very good, then.") ]])
        end
            
    elseif(sActorName == "Golem119BG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Standard loadouts and equipment here![SOFTBLOCK] We replace malformed S-15 apparel as well!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119BH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Hair and nails![SOFTBLOCK] New styles![SOFTBLOCK] Look great for the upcoming gala!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119BI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Designer shoes and boots here![SOFTBLOCK] Superb pedal module coverings![SOFTBLOCK] Unparalleled comfort!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119BJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Only the finest and fanciest here![SOFTBLOCK] Superior materials make superior clothing!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119BK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ChemFuel needs to be synthesized on the spot, it can't be stored because it eats through plasteel containers in under an hour.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119BL") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Unit 99182 entered defragmentation after 62 consecutive hours of service.[SOFTBLOCK] She's not being lazy, Lord Unit.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119BM") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Grrr, how is it I always seem to get the heaviest crates to deliver?[SOFTBLOCK] What's in this one, gold?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Yes, this crate contains gold bricks.[SOFTBLOCK] Look at the shipping manifest.[SOFTBLOCK] It's flavouring for the ChemFuel shop.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Well it's still really heavy![SOFTBLOCK] Grragh!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119BN") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My Lord Unit brings me here all the time and just sits and talks with me.[SOFTBLOCK] I'm not sure what to make of it.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119BO") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I apologize for being on the sales floor, Lord Unit.[SOFTBLOCK] I'm merely dressing these mannequins and will be off before you know it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] You don't have to apologize.[SOFTBLOCK] It's fine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We're not supposed to be seen on the sales floor without authorization, Lord Unit.[SOFTBLOCK] It's my fault.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I should have had these mannequins prepared in the back earlier but I'm behind on my tasks.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Don't get your cables in a twist over it.[SOFTBLOCK] Your hard work is appreciated.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALA") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Oh, that videograph was positively scandalous![SOFTBLOCK] Can you imagine it -[SOFTBLOCK] a Slave Unit and Lord Unit -[SOFTBLOCK] in a tryst!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALB") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] My tandem unit simply can't appreciate the more risque types of erotica.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] After all, it's mere escapism.[SOFTBLOCK] It's not real.[SOFTBLOCK] They're actors.[SOFTBLOCK] It's erotic precisely because it's forbidden!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] That rotten Unit 208933 has been spying on my store and copying my techniques, I just know it!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] I'm convinced that Unit 339802 is copying my techniques by spying on my store![SOFTBLOCK] Don't you agree?") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Many Lord Units don't appreciate a fine rug and just how they tie the room together.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Normally we're the busiest store in the sector, but due to the upcoming Gala...[SOFTBLOCK] well, let's just say my Slave Units are appreciating the time off.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] I can't decide if I should get the squid or the cat or the totally accurate doll -[SOFTBLOCK] so many choices and they're all great!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Humans may be slow, boorish, and crude, but they are incredibly cute when in doll form.[SOFTBLOCK] So they have that going for them.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] My tandem unit is stressing herself about which doll to get me for my conversion anniversary.[SOFTBLOCK] Honestly, I just want to be with her.[SOFTBLOCK] The doll doesn't matter.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] We have replacements for all standard-issue gear.[SOFTBLOCK] You'll look just like the day you were converted!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] My goodness -[SOFTBLOCK] your Slave Unit![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] She has my explicit permission to grow her hair out, thank you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] And what hair it is![SOFTBLOCK] The curl, the bangs, oh my![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] You're not upset?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Honey, if your Lord Unit wasn't literally holding your hand I'd ask you to be transferred to my department.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] What product do you use?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Uh, the molten metal shavings that fly off when I'm spot-welding a stress fracture?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're repair units.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] If you ever want a change of career, just send me a message, darling.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Th-[SOFTBLOCK]thank you, Lord Unit![SOFTBLOCK] Thank you very much!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALL") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] I think I got the best assignment of my synthetic life.[SOFTBLOCK] Shoe salesrobot![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Why would you say that's the best assignment?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] My raging foot fetish, of course.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] ...[SOFTBLOCK] Oh don't look at me like that.[SOFTBLOCK] It's not weird.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALM") then
        
        --Variables.
        local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
        if(iStartedShoppingSequence == 1.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 2.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Slave Unit, what are you doing on the floor without permission?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I'm...[SOFTBLOCK] I'm...[SOFTBLOCK] not without...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I apologize, Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] What are you apologizing for?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I assume you are making a delivery.[SOFTBLOCK] Were you not aware of the rule that Slave Units are not to be on the sales floor without authorization?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I was aware, Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Hey![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I am not ignoring you, Lord Unit.[SOFTBLOCK] I merely maintain strict discipline in my store.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I presume that you do the same in your department.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I am sorry, Lord Unit.[SOFTBLOCK] Please punish me however you like.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] What's gotten into you, Sophie?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Sophie?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You didn't even ask her name or purpose before skipping right to the punishment?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] This Slave Unit is my -[SOFTBLOCK] my property.[SOFTBLOCK] She works in my department.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] She is here on my orders.[SOFTBLOCK] I will not tolerate you punishing my subordinates.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Ah.[SOFTBLOCK] I see.[SOFTBLOCK] I did not intend for that, Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] It is very unusual to see a non-staff Slave Unit here except on deliveries, and they are to deliver to the back rooms only.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She's here to -[SOFTBLOCK] carry my purchases.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Obviously I would not deign to do such simple labour.[SOFTBLOCK] You understand.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I do.[SOFTBLOCK] But aren't you worried she may damage or stain your items, though?[SOFTBLOCK] Slave Units are very clumsy.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Certainly, but this one is less clumsy than most.[SOFTBLOCK] Plus, I am perfectly capable of punishing her mistakes myself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] In addition, we are not here for pleasure, but business.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] You are?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I am Lord Golem of Maintenance and Repair, Sector 96.[SOFTBLOCK] Due to the fabricators in our sector being backlogged, it falls to us to do repairs on clothing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We've run out of fabrics and I can't requisition any, and I have a dozen dresses that need patches and alterations for the upcoming gala.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] A-[SOFTBLOCK]A true tragedy![SOFTBLOCK] I myself have had difficulty accessing the fabricators.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] How can I help you?[SOFTBLOCK] Perhaps some of my spare stock?[SOFTBLOCK] I believe I have some lefover fabric in the back...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (Oh wow, she bought it!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] My Slave Unit here has a list of what we need on her PDU.[SOFTBLOCK] Give her anything she requests.[SOFTBLOCK] I have more important things to attend to.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Send the charges to my department and I'll give you whatever I can spare in my budget.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh no, I couldn't take work credits for this![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Can you imagine...[SOFTBLOCK] arriving to the gala with a hole in your ensemble?[SOFTBLOCK] It's -[SOFTBLOCK] too awful to countenance.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I intend to make it an impossibility.[SOFTBLOCK] Thank you, Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *Order anything you think you'll need with my PDU, Sophie.[SOFTBLOCK] Are you all right?*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Yes, I am but -[SOFTBLOCK] I just -[SOFTBLOCK] I...*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *Let's talk about it later.[SOFTBLOCK] Just order what you need and let's get out of here as quick as we can.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *...*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *...*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Okay, that should do it.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *Let's head back to Sector 96.[SOFTBLOCK] C'mon.*") ]])
            fnCutsceneBlocker()
        
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I just want to thank you for your vital repair services, Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] It's truly a travesty that the administrators have prioritized firearms above fashion.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We live in dark times indeed...") ]])
            fnCutsceneBlocker()
        
        end
            
    elseif(sActorName == "Golem119ALN") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Sigh...[SOFTBLOCK] Another year, another gala, and again I go without a plus one...") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALO") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Have you ever heard of 'coffee'?[SOFTBLOCK] Apparently it's like ChemFuel, but even more addictive.[SOFTBLOCK] Not sure if I'm ready for that.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALP") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] My Slave Unit is the smartest and prettiest of the Slave Units...[SOFTBLOCK] Can you keep a secret, Lord Unit?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I believe so.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Sometimes I wish I had been converted to a Slave Unit all those years ago, just so I could be with her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] But this love is forbidden, and I would never tell her how I feel.[SOFTBLOCK] I don't want to hurt her...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Your secret is safe with me, but...[SOFTBLOCK] you should tell her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] You can be together in secret.[SOFTBLOCK] It can be done.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Do you think so?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I know so.[SOFTBLOCK] Follow your heart.[SOFTBLOCK] Or...[SOFTBLOCK] power core.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALQ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] These USBees are for sale as pets, but we don't have enough in the biolabs for our pollination efforts.[SOFTBLOCK] Maybe I should requisition some?") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALR") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Welcome to my store, the only pet shop on Regulus![SOFTBLOCK] And if you have an organic pet, we also do Raiju collaring and grooming!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALS") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] I don't know what to get...[SOFTBLOCK] My Slave Units will probably just trip over a motilvac, but they're so cute!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Golem119ALT") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] I'd love to have a human as a pet, but apparently that's frowned upon.[SOFTBLOCK] The administrators would just order her converted and used for labour...") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "CafeGolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] Welcome to Chez Fuel, may I take your order?[SOFTBLOCK] Piping hot ChemFuel in seconds, with hundreds of flavours available!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "CafeGolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|Golem] May I get you some chewable tires?[SOFTBLOCK] Or perhaps salted bolts?[SOFTBLOCK] Very crunchy!") ]])
        fnCutsceneBlocker()
    end

end