--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Variables.
    local iRanTrainingProgram    = VM_GetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N")
    local iSawProblemWithProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N")
    local iSecondAmandaScene     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSecondAmandaScene", "N")
    local iFinished198           = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
    
    --Normal:
    if(iSecondAmandaScene == 0.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I have access to the network from here.[SOFTBLOCK] If you wish to depart, board the tram.[SOFTBLOCK] Otherwise, I'm busy.") ]])
        fnCutsceneBlocker()

    --Begin sequence to end Sector 198.
    elseif(iSecondAmandaScene == 1.0 and iFinished198 == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRunFinalScene", "N", 1.0)
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you are quite done in this sector, I'd like to move my operations to another terminal.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In addition to being exposed in the event I am discovered, this one suffers from periodic power fluctuations.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The fluctuations have only begun recently, though it is unlikely that it is in relation to something I did.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That said, the maintenance work in here is indeed shoddy.[SOFTBLOCK] You're a repair unit, what is your evaluation?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Well - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Actually, terminate request.[SOFTBLOCK] Having you repair this terminal does nothing for the security concerns.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What was your purpose in this sector, anyway?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If my directory memory has not decayed due to cosmic ray interference -[SOFTBLOCK] and it has not -[SOFTBLOCK] then I recall that a Unit who worked with Unit 499323 works in this sector.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] 55 WILL YOU PLEASE LET ME SPEAK.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] I was not attempting to restrict you.[SOFTBLOCK] Perhaps your social programs need recalibration?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Sheesh...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I need your help.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Is this in some way related to your presence in this sector, or a more general query?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes, Christine, I also need your help.[SOFTBLOCK] We can accomplish more together than we can alone.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh for crying out -[SOFTBLOCK] wait...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 55, do you get really chatty when you're lonely?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] [SOFTBLOCK]Repeat.[SOFTBLOCK] Query.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sorry for just leaving you in here all alone, but it's best if you're not seen on the cameras.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If someone recognizes you, then a security team might be dispatched.[SOFTBLOCK] You know that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] [SOFTBLOCK]I am not.[SOFTBLOCK] Lonely.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, my mistake.[SOFTBLOCK] I thought you might be.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (She totally is.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I need your help to acquire some specialized parts.[SOFTBLOCK] Take a look at my PDU, it has a list.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] High-voltage capacitors, high-voltage cable, insulated ports, buffers...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What is the purpose of these?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll explain on the way.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We'll have to spend a few hours tracking them down, of course.[SOFTBLOCK] Might even need to fabricate some ourselves.[SOFTBLOCK] Is that acceptable?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Of course it is.[SOFTBLOCK] While your insinuations are frustrating, your strategic capabilities are not in question.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Great![SOFTBLOCK] Let's go check Sector 19's warehouse and see what we can dig up!") ]])
        fnCutsceneBlocker()
        
        --Fade out.
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue popup.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: Six hours later...") ]])
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Transition to 198B. The rest of the cutscene plays out there.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCity198B", "FORCEPOS:13.0x8.0x0") ]])
        fnCutsceneBlocker()

    --Sequence has been completed.
    else
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I suppose this terminal will do for my purposes.[SOFTBLOCK] The power fluctuations have, indeed, ended.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] And, thank you for visiting me.[SOFTBLOCK] I assume you are checking my status for security reasons.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Of course, 55.[SOFTBLOCK] Of course.") ]])
        fnCutsceneBlocker()
    
    end

end