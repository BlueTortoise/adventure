--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Variables.
    local iRanTrainingProgram    = VM_GetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N")
    local iSawProblemWithProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N")
    local iSecondAmandaScene     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSecondAmandaScene", "N")
    local iFinished198           = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
    
    --Common.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    --Hasn't run the training program yet.
    if(iRanTrainingProgram == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[VOICE|Amanda] Just head through this elevator and across the surface to the server room.[SOFTBLOCK] You should be able to run the training program from the consoles there.") ]])
        fnCutsceneBlocker()
    
    --Ran the training program but didn't see the problem.
    elseif(iRanTrainingProgram == 1.0 and iSawProblemWithProgram == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] Any luck in the server room?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I got the program to run correctly but I couldn't find the bug you reported.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] Well, neither have I, but the reports indicate that one of the NPCs acts very erratically, and then makes an illegal move.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] Which is bad, because your score is based on converting the NPCs.[SOFTBLOCK] We've had to do system workarounds since you need to clear the first level to move to the second one.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] I've heard from the other units that it's an NPC on the west side of the map.[SOFTBLOCK] Look around there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[SOFTBLOCK] I'll see what I can find.") ]])
        fnCutsceneBlocker()
    
    --Ran the training program and met the problem NPC.
    elseif(iRanTrainingProgram == 1.0 and iSawProblemWithProgram == 1.0 and iSecondAmandaScene == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iGotAmandaHint", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] Have you found the problem yet?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I played the simulation and saw an NPC that jumped across the river.[SOFTBLOCK] I couldn't follow it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] Good, that's the bug.[SOFTBLOCK] If you can reproduce it, you can fix it.[SOFTBLOCK] Check your PDU.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] The stack trace program...[SOFTBLOCK] indicates the NPC moved to a room for developers?[SOFTBLOCK] A secret room?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] Ah ha![SOFTBLOCK] That makes sense![SOFTBLOCK] Developers often put those in when making the program so they can test things.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] I guess the NPC probably just incorrectly put that room in its movement list![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (But the things that NPC said...[SOFTBLOCK] Is there more going on here?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How do I get to this debug room?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] Hmmm...[SOFTBLOCK] Okay, when you're at the text prompt, type 'warp debug room' and that should do it.[SOFTBLOCK] Turns out the variable for it was always left on in the last version.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] Leave the stack tracer on and maybe I can diagnose the problem.[SOFTBLOCK] Eee![SOFTBLOCK] I'm so excited![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] [SOFTBLOCK]*Ahem*[SOFTBLOCK] Thank you for your service, Unit 771852.[SOFTBLOCK] We're making good progress.") ]])
        fnCutsceneBlocker()

    --Second Amanda scene run, has not finished the sector.
    elseif(iSecondAmandaScene == 1.0 and iFinished198 == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you all right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] Just thinking.[SOFTBLOCK] What you said...[SOFTBLOCK] I have a lot to think about.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] I was so focused on becoming a Lord Unit that I didn't really think about what it meant.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] I mean, I'm talented.[SOFTBLOCK] That means I should be in a position to use those talents.[SOFTBLOCK] But I thought it'd be easier that way, not harder.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You've seen the results of a Lord Unit who doesn't do all the aspects of her job properly.[SOFTBLOCK] I don't think you want to be like 107822.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] I don't...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Then help me, and everything will work out for the best.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] But will it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We'll never find out if we don't try.") ]])
        fnCutsceneBlocker()

    --Finale completed.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] So what are you going to do now, Christine?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's a lot of work to be done.[SOFTBLOCK] We need allies, supplies, equipment, training...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] You have my support, and I know you have the support of the electrosprites and the golems who have met them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Amanda:[E|Neutral] But what you're doing...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dangerous, of course.[SOFTBLOCK] But I have right on my side.[SOFTBLOCK] I will not fail.") ]])
        fnCutsceneBlocker()

    end

end