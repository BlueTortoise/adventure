--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
    
    --Subdivide for all the entities sharing this script.
    if(sActorName == "Golem198A") then
        if(iFinished198 == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Hmmm, I only have one core left.[SOFTBLOCK] Do I go for the cute human or the smart one?[SOFTBLOCK] Choices, choices...") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Converting humans happens to be my thing, so my new tandem unit lets me convert her.[SOFTBLOCK] Over and over.[SOFTBLOCK] And I get to watch...") ]])
            fnCutsceneBlocker()
        
        end
        
    elseif(sActorName == "Golem198B") then
        if(iFinished198 == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I swear the keys are sticking on this keyboard.[SOFTBLOCK] I keep double-typing![SOFTBLOCK] Aarrgghh!") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I have to put up with this dumb busted keyboard, but visiting my special BubbleSort is always worth it.") ]])
            fnCutsceneBlocker()
        
        end
        
    elseif(sActorName == "Golem198C") then
        if(iFinished198 == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] They make us play this training simulation until we get perfect scores.[SOFTBLOCK] I've been stuck at 95 for a while now.[SOFTBLOCK] What am I doing wrong?") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I don't mind playing these simulations over and over anymore.[SOFTBLOCK] It's basically a special date with my new tandem unit.") ]])
            fnCutsceneBlocker()
        
        end
        
    elseif(sActorName == "Golem198D") then
        if(iFinished198 == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I don't remember much about being a human, but were we this weak and stupid?[SOFTBLOCK] Sheesh.[SOFTBLOCK] Glad I'm a golem now!") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My tandem unit visits me every night while I defragment.[SOFTBLOCK] Electricity girlfriends are the best!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "Golem198E") then
        if(iFinished198 == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Who do they think they're fooling with this dialogue?[SOFTBLOCK] It's atrocious, and some of it isn't even spelled right.[SOFTBLOCK] There's only one s in resource!") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I love my QuickSort so much, I'd glady die to protect her![SOFTBLOCK] Thank you for letting me meet her, Lord Unit!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "Golem198F") then
        if(iFinished198 == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I've heard this program was coded such a long time ago that the original coders have long since moved on, so we can't fix bugs anymore.") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I guess the bugs in the code turned out to be features.[SOFTBLOCK] In more ways than one!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "Golem198G") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Training isn't in session right now, Lord Golem.") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I have to get all the stations reset and ready for the next batch of Electrosprites.[SOFTBLOCK] I'm so glad I get to introduce so many golems to their true loves!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "Golem198H") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh, are you Sophie's Lord Unit?[SOFTBLOCK] Theresa speaks very highly of her, I think they were co-workers once.") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Of course you came by and saved the sector.[SOFTBLOCK] Just like Theresa said you would!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "Golem198I") then
        if(iFinished198 == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Yeah?[SOFTBLOCK] Then what did she say?[SOFTBLOCK] Oh my, scandalous!") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Really?[SOFTBLOCK] Well, I hope someone writes an Electrosprite plotline now!") ]])
            fnCutsceneBlocker()
        end
    elseif(sActorName == "Golem198J") then
        if(iFinished198 == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] And then, when Unit 89444 took over in season 701, they started making some pretty risque episodes!") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh, did you hear?[SOFTBLOCK] One of the writers of 'All My Processors' is one of us!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "Golem198K") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'm on the final level of the simulation.[SOFTBLOCK] There's...[SOFTBLOCK] so many humans...[SOFTBLOCK] they're coming from all sides![SOFTBLOCK] My gosh, they are billions![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Billions of humans?[SOFTBLOCK] Upgrade your defenses and get those cores ready![SOFTBLOCK] Convert them all!") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Ngh, I can't even attempt to beat the final level.[SOFTBLOCK] I just want to kiss all the humans in this game!") ]])
            fnCutsceneBlocker()
        
        end
    elseif(sActorName == "Golem198L") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The first three levels are really easy, but the final level is at least three times harder than the others.[SOFTBLOCK] How does any unit graduate with this?") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'll be honest.[SOFTBLOCK] The game is now a masturbation aid for everyone in the sector, and I wouldn't have it any other way.[SOFTBLOCK] My precious tandem unit happens to agree!") ]])
            fnCutsceneBlocker()
        
        end
        
    elseif(sActorName == "Golem198M") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'm stalling at the oil machine.[SOFTBLOCK] I really don't want to go back to the last level of the simulation, it's just brutal.") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I tried giving some oil to my Electrosprite tandem unit, but she just rubbed it all over her chest.[SOFTBLOCK] Then we synchronized...[SOFTBLOCK] A lot...") ]])
            fnCutsceneBlocker()
        
        end
    elseif(sActorName == "Golem198N") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Tch.[SOFTBLOCK] Even the dumbest Slave Units can pass the first level, but only the finest pass the final.[SOFTBLOCK] I, of course, aced it on my first try.") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Fear not, friends.[SOFTBLOCK] I may be a Lord Unit, but I'd sooner be retired than betray my beloved UDP2.[SOFTBLOCK] She is my everything.") ]])
            fnCutsceneBlocker()
        
        end
    elseif(sActorName == "Golem198O") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] How did Unit 751099 get an assignment here, anyway?[SOFTBLOCK] Are we so strapped for coders that we're accepting breeding program applicants, now?") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'm really surprised that things worked out so well.[SOFTBLOCK] If it had been anyone else helping us, the Electrosprites would have all died and I'd still be alone.") ]])
            fnCutsceneBlocker()
        end
    
    --[Electrosprites]
    elseif(sActorName == "ElectrospriteA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Electrosprite:[VOICE|Electrosprite] Wow![SOFTBLOCK] You can go up, and down, and in all kinds of crazy directions![SOFTBLOCK] This is so awesome!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "ElectrospriteB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Electrosprite:[VOICE|Electrosprite] I'm working on getting things ready for the next wave of sisters![SOFTBLOCK] They're gonna love it so much!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "ElectrospriteC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Electrosprite:[VOICE|Electrosprite] My tandem unit told me we can only have sex once per hour.[SOFTBLOCK] Waiting around is so lame![SOFTBLOCK] I'm going to try to talk her up to two per hour!") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "ElectrospriteD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Electrosprite:[VOICE|Electrosprite] I've been told I'm more refined than the other Electrosprites.[SOFTBLOCK] Maybe it's because I shared minds with a Lord Unit?") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "ElectrospriteE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Electrosprite:[VOICE|Electrosprite] My girlfriend always wanted to get stroked while she was working in the back here, so that's what I do![SOFTBLOCK] It's really fun, making her cum and trying to make her lose focus at a bad time![SOFTBLOCK] Ha ha ha!") ]])
        fnCutsceneBlocker()
    end

end