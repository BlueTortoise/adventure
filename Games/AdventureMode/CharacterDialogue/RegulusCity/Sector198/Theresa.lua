--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
        
    --Variables:
    local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
    
    --Hasn't finished the quest yet:
    if(iHasElectrospriteForm == 0.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Theresa:[VOICE|Golem] Just between you and me, Lord Unit 107822 is a lazy piece of junk.[SOFTBLOCK] She spends all her time defragmenting and passes off administrative duties on to me.") ]])
        fnCutsceneBlocker()
    
    --Has finished the subquest.
    else
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Theresa:[VOICE|Golem] So Unit 107822 just hated her job?[SOFTBLOCK] I guess we have more in common with the lords than we think.[SOFTBLOCK] Doesn't excuse her performance, though.") ]])
        fnCutsceneBlocker()

    end
end