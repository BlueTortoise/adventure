--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    
    --Not completed the questline yet.
    if(iCompletedSerenity == 0.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I don't understand why the darkmatters would start attacking units.[SOFTBLOCK] We've always gotten along just fine before, why now?") ]])
        fnCutsceneBlocker()
    
    --Completed the questline.
    else
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] So the darkmatters were just trying to protect us?[SOFTBLOCK] I guess they're a lot smarter than we thought.") ]])
        fnCutsceneBlocker()
    end
end