--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iCompletedSerenity      = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    local i300910ToldAboutCredits = VM_GetVar("Root/Variables/Chapter5/Scenes/i300910ToldAboutCredits", "N")
    
    --Special dialogue.
    if(iCompletedSerenity == 1.0 and i300910ToldAboutCredits == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/i300910ToldAboutCredits", "N", 1.0)
        
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: Oh, well, this might seem odd to mention, but I already flagged the work order completed.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: You should have 300 additional credits in your account now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, thank you very much.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: Considering everything that's happened, it's really the least I can do.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: If there's anything, and I do mean anything, that I or the Observatory can do to help, we'll do it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: Name it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Assist us in our rebellion.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: !!![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If not, then don't tell anyone what 55 just said.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: No, no...[SOFTBLOCK] We'll help. Serenity Crater Observatory...[SOFTBLOCK] will help you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You mean it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: 2855 gave us a full rundown on what they did in the crater and elsewhere.[SOFTBLOCK] She also informed us of your plans.[SOFTBLOCK] We know.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: And...[SOFTBLOCK] it pains me to say it.[SOFTBLOCK] We believe in the Cause of Science.[SOFTBLOCK] Everyone does.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So do we.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: But it has to have limits.[SOFTBLOCK] What if this had happened on Pandemonium?[SOFTBLOCK] What if more of those things ran amok?[SOFTBLOCK] How many humans would have died?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: We're not soldiers, but we'll help.[SOFTBLOCK] I know every Golem here agrees with me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This observatory is well-situated to resist assault.[SOFTBLOCK] I would recommend you acquire whatever supplies you can, discreetly.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You will also need to acquire an independent power supply.[SOFTBLOCK] The facility in the crater is unused, send a team down when the area is cleared.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: But...[SOFTBLOCK] when the shooting starts...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You and your researchers will have to retire enemy security teams.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sorry...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: So this is what it's come to.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: I guess this is part of being a Command Unit.[SOFTBLOCK] I -[SOFTBLOCK] I know some of my friends here will die.[SOFTBLOCK] But it's the right thing to do, and I can't escape that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: All I can do is prepare and try to minimize those losses.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] When Christine and I are prepared, I will assist you.[SOFTBLOCK] I can provide training and updated combat algorithms.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: Thank you...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: Now.[SOFTBLOCK] Was there anything else?[SOFTBLOCK] Something less grim, I hope.[BLOCK][CLEAR]") ]])
    
    
    --Normal dialogue.
    else
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910: If there's anything I can do to help, just let me know.[BLOCK][CLEAR]") ]])
    end

	--Topic unlocks.
	WD_SetProperty("Unlock Topic", "2855", 1)

	--[Topics]
	--Activate topics mode once the dialogue is complete.
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "300910") ]])

end