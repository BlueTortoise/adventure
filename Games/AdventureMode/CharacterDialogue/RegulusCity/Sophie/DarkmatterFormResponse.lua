--[Darkmatter Form Response]
--Called when talking to Sophie in Darkmatter form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenDarkmatter = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenDarkmatter", "N")

--First time Sophie has seen non-golem Christine.
if(iSophieKnowsAboutRunestone == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenDarkmatter", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hello, Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie?[SOFTBLOCK] You're not surprised?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] No, no I'm not.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Which is weird, but hear me out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You've been a Darkmatter for a long time, haven't you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] In a time before time, when the world was still being forged in the core of distant stars.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] How did you know?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Because I saw it.[SOFTBLOCK] I was there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I had a dream while I was defragmenting.[SOFTBLOCK] But when I searched my archives, the signals came out of nowhere.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] They were just suddenly in my head, and I sorted them into my long-term memory.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] We were in front of some enormous black hole.[SOFTBLOCK] You were...[SOFTBLOCK] this...[SOFTBLOCK] and we were being as intimate as we've ever been.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It wasn't a dream.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I know.[SOFTBLOCK] Somehow, I know.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] And it means you can't break up with me no matter how weird I get, because our love is foretold.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I wasn't going to, but you're right.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's nice not having to worry about you being shocked by this body.[SOFTBLOCK] Everyone else wants to study me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh, I want to study you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Can my material hands caress you with as much care as when you're hard?[SOFTBLOCK] I should write a research paper.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Let's serve the Cause of Science![SOFTBLOCK] Ha ha ha!") ]])
    fnCutsceneBlocker()

--First time as a Darkmatter. Basically the same, slight variations.
elseif(iSeenDarkmatter == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenDarkmatter", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hello, Christine.[SOFTBLOCK] I see you've become melted starlight.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You are as you always were, and always were meant to be.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie?[SOFTBLOCK] You're not surprised?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] No, no I'm not.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Which is weird, but hear me out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You've been a Darkmatter for a long time, haven't you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] In a time before time, when the world was still being forged in the core of distant stars.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] How did you know?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Because I saw it.[SOFTBLOCK] I was there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I had a dream while I was defragmenting.[SOFTBLOCK] But when I searched my archives, the signals came out of nowhere.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] They were just suddenly in my head, and I sorted them into my long-term memory.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] We were in front of some enormous black hole.[SOFTBLOCK] You were...[SOFTBLOCK] this...[SOFTBLOCK] and we were being as intimate as we've ever been.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It wasn't a dream.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I know.[SOFTBLOCK] Somehow, I know.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] And it means you can't break up with me no matter how weird I get, because our love is foretold.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I wasn't going to, but you're right.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's nice not having to worry about you being shocked by this body.[SOFTBLOCK] Everyone else wants to study me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh, I want to study you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Can my material hands caress you with as much care as when you're hard?[SOFTBLOCK] I should write a research paper.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Let's serve the Cause of Science![SOFTBLOCK] Ha ha ha!") ]])
    fnCutsceneBlocker()

--Repeats.
else

    --Special:
    local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
    if(iSteamDroid100RepState == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hey Christine, you're looking starry.[SOFTBLOCK] What can I do for you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Could you take a look at this with the micro-scanner?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] The -[SOFTBLOCK] piece of scrap you're holding?[SOFTBLOCK] Really?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A Series-X20 transducer.[SOFTBLOCK] You can't tell?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] It's in awful shape![SOFTBLOCK] Where did you get that?[SOFTBLOCK] Who would send that piece of junk to us?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101, if you must know.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Erm...[SOFTBLOCK] I'm searching my drives...[SOFTBLOCK] Where have I heard that name before?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] The same![SOFTBLOCK] I can attest that she's just as fierce in person.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hee hee![SOFTBLOCK] I'll bet![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[SOFTBLOCK] I'll just switch them and mark this one as scrapped.[SOFTBLOCK] Nobody will notice.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I love the duplicity![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] I hope this helps![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] My sexy maverick girlfriend...[SOFTBLOCK] Just be careful.[SOFTBLOCK] I've heard the caverns are dangerous.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I will, dearest.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Dearest?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uhhhh...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] No, it's okay.[SOFTBLOCK] I like it.[SOFTBLOCK] Good luck, dearest!") ]])
        return
    end

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'm wondering what position and angle your body's stars are from?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Is that our galaxy there, or another?[SOFTBLOCK] Perhaps I could back-reference the starmaps...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's not this dimension, but another.[SOFTBLOCK] Some of the light from it reaches Regulus from gaps in the weave of reality.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't want to go back there, ever, but this is a keepsake.[SOFTBLOCK] It's a place only I know of.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Fitting.[SOFTBLOCK] I wonder what I'd have if my body was starlight...") ]])
    fnCutsceneBlocker()

end
