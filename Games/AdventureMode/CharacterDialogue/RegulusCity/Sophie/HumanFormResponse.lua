--[Human Form Response]
--Called when talking to Sophie in human form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenHuman = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenHuman", "N")

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[ ================================== Normal Conversation Path ================================= ]
if(sTopicName == "No Special") then

    --First time Sophie has seen non-golem Christine.
    if(iSophieKnowsAboutRunestone == 0.0) then
        
        --Flag.
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenHuman", "N", 1.0)
         
         --Variables.
        local iMetAmanda = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetAmanda", "N")
        local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] O-[SOFTBLOCK]oh my![SOFTBLOCK] A human![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, erm.[SOFTBLOCK] Are you with the breeding program?[SOFTBLOCK] You're supposed to have your day pass on your shirt if you are.[BLOCK][CLEAR]") ]])
        
        --Doesn't know what a day pass is.
        if(iMetAmanda == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Day pass?[SOFTBLOCK] What's that?[BLOCK][CLEAR]") ]])
        
        --Does.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Day pass?[SOFTBLOCK] Oh yes, like what Amanda had.[SOFTBLOCK] Otherwise you probably think I'm a rogue human, don't you?[BLOCK][CLEAR]") ]])
        end
        
        --Rejoin.
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] ...[SOFTBLOCK] Christine?[SOFTBLOCK] Wait, no.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Sorry, human, but you look a lot like someone I know.[SOFTBLOCK] And you have the same accent.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Ooh, I should call 771852 and have her convert you![SOFTBLOCK] Are you perhaps related?[SOFTBLOCK] Wouldn't it be great to be converted by your sister?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie, I [SOFTBLOCK]*am*[SOFTBLOCK] 771852.[SOFTBLOCK] Don't you recognize me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] ...[SOFTBLOCK] What?[SOFTBLOCK] How is this possible?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, I have this runestone here, and I don't know how, but I can just focus on myself and poof.[SOFTBLOCK] I'm a human again.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] In fact I can transform to any form I've been in before.[SOFTBLOCK] 55 wants to use it to break into places we're not supposed to be.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] [SOFTBLOCK]This isn't an elaborate prank?[SOFTBLOCK] You're not Christine's sister or something?") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        --Flashwhite.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite", "Null")
        DL_PopActiveObject()
        fnCutsceneBlocker()

        fnCutsceneWait(75)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Lua variables.
        local iCounter = 0
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Does that answer your question?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] *Oh my gosh!*[SOFTBLOCK] You're -[SOFTBLOCK] this is incredible![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] .............[SOFTBLOCK] Christine, I am truly sorry, and I know this must be the most amazing thing ever for you...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's not really that amazing...[SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] But all I am thinking about right now is naughty things.[SOFTBLOCK] I'm so sorry![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] You could become so soft, and supple, and -[SOFTBLOCK] oh![SOFTBLOCK] My core's going to skip a cycle![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Please calm down, Sophie.[SOFTBLOCK] I just wanted to show you, not give you a system lock.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh, my dear 771852 is such a special unit![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] But you best not go showing that off anywhere else.[SOFTBLOCK] The administrators would take you away...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't worry, I've been careful to conceal it.[SOFTBLOCK] Only you and 2855 know.[BLOCK][CLEAR]") ]])
        if(iFinished198 == 1.0) then
            iCounter = iCounter + 1
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And some of the golems at the training center...[BLOCK][CLEAR]") ]])
        end
        if(iCompletedSerenity == 1.0) then
            iCounter = iCounter + 1
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And everyone at Serenity Crater Observatory...[BLOCK][CLEAR]") ]])
        end
        if(iSXUpgradeQuest >= 3.0) then
            iCounter = iCounter + 1
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And the Steam Droids...[BLOCK][CLEAR]") ]])
        end
        
        if(iCounter >= 2.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And probably some other people I forgot.[SOFTBLOCK] Yikes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] I'll keep your secret, don't worry.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] At this point it's hardly a secret.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Well that's -[SOFTBLOCK] Oh -[SOFTBLOCK] naughty thoughts again.[SOFTBLOCK] You'd best go before I turn off my inhibitors![BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] I'll keep your secret, don't worry.[SOFTBLOCK] Oh -[SOFTBLOCK] naughty thoughts again.[SOFTBLOCK] You'd best go before I turn off my inhibitors!") ]])
        end
        
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

    --Sophie knows about transformation but has not seen human form yet.
    elseif(iSeenHuman == 0.0) then

        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenHuman", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] O-[SOFTBLOCK]oh my![SOFTBLOCK] A human![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hell, Unit 449323.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Are you with the breeding program?[SOFTBLOCK] Remember you're supposed to have your security badge on.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Didn't anyone tell you we're under orders to convert unidentified humans on sight?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Unit 771852 didn't mention we're getting an intern or anything...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Oh, oh![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I suppose you figured it out, didn't you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Considering everything that's been happening lately, getting a human intern to help with repairs could really happen.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] They wouldn't be very efficient, but I've been talking to the other units.[SOFTBLOCK] Everyone is short-staffed.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Lots of units getting reassigned for security duty.[SOFTBLOCK] Having breeding program interns fill in for them would make sense.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Still seems too dangerous.[SOFTBLOCK] With all the sparks flying around in here, we should let them have a less dangerous job.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] C-[SOFTBLOCK]Christine, I -[SOFTBLOCK] phew...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What's the matter?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] My programming is telling me to convert you.[SOFTBLOCK] Phew, woah![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Every 1/6th of a second it bumps 'Convert Rogue Human' up my priority list.[SOFTBLOCK] I can't disable the algorithm, either![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Here, put this security pass on![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (It's actually just a metal clip...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Better?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] So much better.[SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It's a potent reflex.[SOFTBLOCK] I'm sure abductions units are used to it but I'm not.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Gosh, how long has it been since I last saw a human?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Archive scan complete.[SOFTBLOCK] Last human sighted was Unit 602332, assigned to deliver a broken sunlamp.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Broken sunlamp?[SOFTBLOCK] So, she brought it from the biolabs?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Probably.[SOFTBLOCK] That was years ago, she's likely a golem by now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Quick question...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Is your whole body squishy?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Not my bones or cartilage.[SOFTBLOCK] Plus, I'm mostly water, so under pressure I'll firm up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Or pop like a meat balloon.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Just... wondering...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

    --Repeats.
    else

        --Special:
        local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
        if(iSteamDroid100RepState == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] A hum-[SOFTBLOCK] ack, I did it again, 771852.[SOFTBLOCK] Sorry.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No problem.[SOFTBLOCK] Hey, can you take a look at this?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] The -[SOFTBLOCK] piece of scrap you're holding?[SOFTBLOCK] What is it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A Series-X20 transducer.[SOFTBLOCK] You can't tell?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] It's in awful shape![SOFTBLOCK] Where did you get that?[SOFTBLOCK] Who would send that piece of junk to us?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101, if you must know.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Erm...[SOFTBLOCK] I'm searching my drives...[SOFTBLOCK] Where have I heard that name before?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] The same![SOFTBLOCK] I can attest that she's just as fierce in person.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hee hee![SOFTBLOCK] I'll bet![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[SOFTBLOCK] I'll just switch them and mark this one as scrapped.[SOFTBLOCK] Nobody will notice.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I love the duplicity![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] I hope this helps![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] My sexy maverick girlfriend...[SOFTBLOCK] Just be careful.[SOFTBLOCK] I've heard the caverns are dangerous.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I will, dearest.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Dearest?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uhhhh...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] No, it's okay.[SOFTBLOCK] I like it.[SOFTBLOCK] Good luck, dearest!") ]])
            return
        end
        
        --Variables.
        local iSpecialHumanDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iSpecialHumanDate", "N")
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] A hum-[SOFTBLOCK] oh, sorry, 771852.[SOFTBLOCK] It's kind of a reflex.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I know, I got the same programming.[SOFTBLOCK] It's difficult to suppress.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Mmm, if we had spare nanofluid, I'd convert you, right here and now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Watching your body and mind get mechanized...[SOFTBLOCK] it's making my power core overclock...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Hey, do you find me attractive, even though you're a human?[SOFTBLOCK] Or are you only attracted to other humans?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *kiss*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You're the most beautiful thing in the world to me.[SOFTBLOCK] Human, golem, no matter.[SOFTBLOCK] Love is not constrained by form.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Awwwww...[BLOCK][CLEAR]") ]])
        if(iSpecialHumanDate == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Actually, Sophie...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I just had...[SOFTBLOCK] an idea...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] That look on your face...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Computing odds that I will not like this idea...[SOFTBLOCK] Done![SOFTBLOCK] Three in four![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh come on, you know that chance computation algorithm is filled with bugs and oversights.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Well then, tell me what you have planned![BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Special Date\", " .. sDecisionScript .. ", \"SpecialDate\") ")
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Right Now\",  " .. sDecisionScript .. ", \"NoDate\") ")
            fnCutsceneBlocker()
        
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Sophie, do you remember when you had me converted...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] A very special day I have marked in my memory drives. I will never, ever overwrite it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Were you thinking of doing something so crazy again?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Let's just reminisce about it? In graphic detail.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oh, okay! Would you like to do that right now?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Special Date\", " .. sDecisionScript .. ", \"SpecialDateRepeat\") ")
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Right Now\",  " .. sDecisionScript .. ", \"NoDateRepeat\") ")
            fnCutsceneBlocker()
        end

    end

--[ ===================================== Decision Responses ==================================== ]
--Start the special date.
elseif(sTopicName == "SpecialDate") then
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSpecialHumanDate", "N", 100.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanFirstScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanSecondScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N", 0.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Smirk") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay, here's what we do...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *whisper*[SOFTBLOCK] *whisper*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh?[SOFTBLOCK] Oh?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *whisper*[SOFTBLOCK] *whisper*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Ohhhhhh.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] This is dangerous,[SOFTBLOCK] and stupid,[SOFTBLOCK] and a terrible idea that might get both of us repurposed for purely selfish reasons.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] When do we start?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Why not right now?[SOFTBLOCK] Get your part set up and message my PDU when you're all set.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 75, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityBAlt", "FORCEPOS:27.0x25.0x0") ]])
    fnCutsceneBlocker()
    
--Start the special date, repeat.
elseif(sTopicName == "SpecialDateRepeat") then
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSpecialHumanDate", "N", 100.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanFirstScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanSecondScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanRepeat", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Smirk") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Tell me every little detail.[SOFTBLOCK] Don't leave anything out.[SOFTBLOCK] Even the parts you didn't see.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] This will be like an exact re-creation in my mind...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Well then listen closely!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 75, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityBAlt", "FORCEPOS:27.0x25.0x0") ]])
    fnCutsceneBlocker()

--Not right now.
elseif(sTopicName == "NoDate") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Smirk") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I had better go set a few things up first.[SOFTBLOCK] I won't keep you waiting long.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] You are such a dirty tease![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You love it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I do![SOFTBLOCK] Don't keep my waiting, because if my imagination routines come up with something better I might just make you do that instead![SOFTBLOCK] Hee hee!") ]])
    fnCutsceneBlocker()

--Not right now.
elseif(sTopicName == "NoDateRepeat") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Smirk") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I really, really want to, but the mission takes priority.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Oh, all right.[SOFTBLOCK] I was thinking maybe I'd upload my memories to a videograph so you could watch it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But keeping that around would be pretty risky, considering.[SOFTBLOCK] So it'll have to wait.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's a great idea, Sophie![SOFTBLOCK] Until next time!") ]])
    fnCutsceneBlocker()


end
