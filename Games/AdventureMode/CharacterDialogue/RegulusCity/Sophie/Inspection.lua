--[Inspection]
--Brief lewd cutscene. Can be done multiple times with minor variations.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[First Time]
--Runs only once the first time the player sees this scene.
if(sTopicName == "First Time") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iLatexInspectionScene", "N", 1.0)
    
    --Disable Sophie's collision flag.
    TA_ChangeCollisionFlag("Sophie", false)
    
    --Actor movement.
    fnCutsceneMove("Christine", 22.25, 13.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Initial optical inspection...[SOFTBLOCK] Unit is functional.[SOFTBLOCK] Very pert.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Unit, chest forward.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] CHEST TO MAXIMUM EXTENSION.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Unit, rub your legs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] AFFIRMATIVE.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Slower.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] REDUCING PRESSURE APPLICATION AREA RATE BY HALF.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Very nice.[SOFTBLOCK] Unit, prepare for tactile inspection.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] UNIT STANDING BY.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sophie", 23.25, 12.50)
    fnCutsceneMove("Sophie", 23.25, 13.50)
    fnCutsceneFace("Sophie", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Rotate quarter circle clockwise, Unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, -1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("Sophie", 23.00, 13.50, 0.20)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Rendering", "Inspection", true) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Major Animation", "Inspection") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Frame", "Inspection", 0.0) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 20.5, 20.7, "Loop", 0.20, 10.0 * 25.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Let's begin...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 27.5, 38.5, "Sin", 0.15, 11.0 * 12.0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Unit's chest cavities are beyond normal parameters.[SOFTBLOCK] Firm, sensitive, responsive.[SOFTBLOCK] Passing grade.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] COMPLIMENT LOGGED.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Stimulating chest cavities to test tactile levels.") ]])
    fnCutsceneBlocker()
    
    --Run animation. Lower total frame time indicates increased speed.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 27.5, 38.5, "Sin", 0.15, 11.0 * 6.0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Nggggh...[SOFTBLOCK] TACTILE STIMULATION WITHIN EXPECTED PARAMETERS.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ACCEPTING STIMULATION...") ]])
    fnCutsceneBlocker()
    
    --Dialogue while running the animation.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ACCEPTING STIMULATION...") ]])
    local fTPF = 5.0
    fnCutsceneWait(fTPF * 2.0)
    fnCutsceneBlocker()
    
    --Dialogue while running the animation.
    fnCutsceneWait(fTPF * 2.0)
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Destination", "Inspection", 51.5, 0.15) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Passing grade.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Unit, log note for future maintenance units.[SOFTBLOCK] Unit's chest cavities are to be checked extensively for sensitivity deviations.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] NOTE LOGGED.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Run animation.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Destination", "Inspection", 74.5, 0.15) ]])
    fnCutsceneWait(fTPF * 3.0)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] (...[SOFTBLOCK] Damn it...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] (Just go for it, Sophie.[SOFTBLOCK] She's your girlfriend and she loves you...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] UNIT STANDING BY.[SOFTBLOCK] PROCEED WITH TACTILE INSPECTION.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Inspection proceeding...[SOFTBLOCK] I can do this...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Run animation.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 80.5, 99.5, "Sin", 0.15, 19.0 * 12.0) ]])
    fnCutsceneWait(fTPF * 5.0)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Unit -[SOFTBLOCK] report discomfort?[SOFTBLOCK] Any?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] NEGATIVE.[SOFTBLOCK] TACTILE STIMULATION RECEIVED AND WITHIN PARAMETERS.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] And if I do this?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] (Maybe if I speed it up a bit...)") ]])
    fnCutsceneBlocker()
    
    --Run animation.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 80.5, 99.5, "Sin", 0.15, 19.0 * 5.0) ]])
    fnCutsceneWait(fTPF * 2.0)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Accept-[SOFTBLOCK]ACCEPTING STIMULATION.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] STIMULA-[SOFTBLOCK]ooooohhhHHHHHHHH!!!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] DRONE UNIT IS C-[SOFTBLOCK]C![SOFTBLOCK] CUMMING!") ]])
    fnCutsceneBlocker()
    
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 140.5, 159.5, "Sin", 0.15, 19.0 * 5.0) ]])
    fnCutsceneWait(fTPF * 4.0)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneWait(fTPF * 4.0)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 160.5, 175.5, "Sin", 0.15, 15.0 * 5.0) ]])
    fnCutsceneWait(fTPF * 4.0)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Tactile stimulation increasing.[SOFTBLOCK] Unit, log results.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE![SOFTBLOCK] LOGGING![SOFTBLOCK] NNNGGHH![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ACCEPTING STIMULAAAAAAATION![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Aaaaahhhh![SOFTBLOCK] I came![SOFTBLOCK] Aaah!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Aannnnhhhh......") ]])
    fnCutsceneBlocker()
    
    --Run to completion.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 217.5, 219.5, "Sin", 0.15, 2.0 * 15.0) ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Excellent.[SOFTBLOCK] Tactile stimulation within expected parameters.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Unit, record log for future maintenance.[SOFTBLOCK] Unit needs to hold out longer, that was too fast![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] [SOFTBLOCK]*pant*[SOFTBLOCK] Log [SOFTBLOCK]*pant*[SOFTBLOCK] recorded...[SOFTBLOCK] *Ahem*[SOFTBLOCK] LOG RECORDED.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] UNIT NOTES THAT REPEATED PRACTICE WILL IMPROVE TEST RESULTS.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Correct.[SOFTBLOCK] Inspection complete, passing grade.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Animation.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Rendering", "Inspection", false) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Major Animation", "Null") ]])
    fnCutsceneWait(10)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sophie", 23.25, 13.50, -1, 0, 0.20)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Unit, report.[SOFTBLOCK] Did Unit 771852 enjoy the inspection process?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] AFFIRMATIVE.[SOFTBLOCK] UNIT IS HAVING DIFFICULTY WAITING FOR THE NEXT INSPECTION.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Well, Unit 499323 has work to do.[SOFTBLOCK] Unit 771852 will patiently wait until ordered.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] AFFIRMATIVE.[SOFTBLOCK] UNIT WILL WAIT PATIENTLY.[SOFTBLOCK] THANK YOU FOR YOUR ATTENTION.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sophie", 23.25, 12.50)
    fnCutsceneMove("Sophie", 22.25, 12.50)
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Who would have thought she was such a good roleplayer...?)") ]])
    
    --Re-enable Sophie's collision flag.
    fnCutsceneInstruction([[ TA_ChangeCollisionFlag("Sophie", true) ]])
    
--[Repeats]
--Same scene but set to repeat.
else
    
    --Disable Sophie's collision flag.
    TA_ChangeCollisionFlag("Sophie", false)
    
    --Actor movement.
    fnCutsceneMove("Christine", 22.25, 13.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Initial optical inspection...[SOFTBLOCK] Unit is functional.[SOFTBLOCK] Very pert.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Unit, chest forward.[SOFTBLOCK] Like you mean it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] CHEST TO MAXIMUM EXTENSION.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Unit, rub your legs, slowly.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] AFFIRMATIVE.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] NO DEFECTS DETECTED.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Again.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] REPEATING SCAN.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Very nice.[SOFTBLOCK] Unit, prepare for tactile inspection.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] UNIT STANDING BY.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sophie", 23.25, 12.50)
    fnCutsceneMove("Sophie", 23.25, 13.50)
    fnCutsceneFace("Sophie", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Activate this layer for animation. Focus the camera, move the real actors offstage.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Position", (22.25 * gciSizePerTile), (13.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Rotate quarter circle clockwise, Unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, -1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("Sophie", 23.00, 13.50, 0.20)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Rendering", "Inspection", true) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Major Animation", "Inspection") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Frame", "Inspection", 0.0) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 20.5, 20.7, "Loop", 0.20, 10.0 * 25.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Let's begin...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 27.5, 38.5, "Sin", 0.15, 11.0 * 12.0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Unit's chest cavities are beyond normal parameters.[SOFTBLOCK] Firm, sensitive, responsive.[SOFTBLOCK] Passing grade.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] COMPLIMENT LOGGED.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Stimulating chest cavities to test tactile levels.") ]])
    fnCutsceneBlocker()
    
    --Run animation.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 27.5, 38.5, "Sin", 0.15, 11.0 * 6.0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Nggggh...[SOFTBLOCK] TACTILE STIMULATION WITHIN EXPECTED PARAMETERS.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ACCEPTING STIMULATION...") ]])
    fnCutsceneBlocker()
    
    --Dialogue while running the animation.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ACCEPTING STIMULATION...") ]])
    local fTPF = 5.0
    fnCutsceneWait(fTPF * 2.0)
    fnCutsceneBlocker()
    
    --Dialogue while running the animation.
    fnCutsceneWait(fTPF * 2.0)
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Destination", "Inspection", 51.5, 0.15) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Passing grade.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Unit, log note for future maintenance units.[SOFTBLOCK] Unit's chest cavities are to be checked extensively for sensitivity deviations.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] NOTE LOGGED.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Run animation.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Destination", "Inspection", 74.5, 0.15) ]])
    fnCutsceneWait(fTPF * 3.0)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] (It gets...[SOFTBLOCK] just a bit easier each time...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Inspection proceeding...") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Run animation.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 80.5, 99.5, "Sin", 0.15, 19.0 * 12.0) ]])
    fnCutsceneWait(fTPF * 5.0)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Insertion achieved.[SOFTBLOCK] Unit, report tactile stimulation levels.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] TACTILE STIMULATION RECEIVED AND WITHIN PARAMETERS.[SOFTBLOCK] UNIT STANDING BY.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ACCEPTING STIMULATION.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ACCEPTING STIMULATION.") ]])
    fnCutsceneBlocker()
    
    --Run animation.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 80.5, 99.5, "Sin", 0.15, 19.0 * 5.0) ]])
    fnCutsceneWait(fTPF * 2.0)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Accept-[SOFTBLOCK]ACCEPTING-[SOFTBLOCK] STIMULATION...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Penetration at maximum.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Nnnggghhh oooohhhh!![SOFTBLOCK] Accepting!!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] STIMULA-[SOFTBLOCK]ooooohhhHHHHHHHH!!!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] DRONE UNIT IS C-[SOFTBLOCK]C![SOFTBLOCK] CUMMING!") ]])
    fnCutsceneBlocker()
    
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 140.5, 159.5, "Sin", 0.15, 19.0 * 5.0) ]])
    fnCutsceneWait(fTPF * 4.0)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneWait(fTPF * 4.0)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 160.5, 175.5, "Sin", 0.15, 15.0 * 5.0) ]])
    fnCutsceneWait(fTPF * 4.0)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Tactile stimulation increasing.[SOFTBLOCK] Unit, log results.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE![SOFTBLOCK] LOGGING![SOFTBLOCK] NNNGGHH![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ACCEPTING STIMULAAAAAAATION![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Aaaaahhhh![SOFTBLOCK] I came![SOFTBLOCK] Aaah!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Aannnnhhhh......") ]])
    fnCutsceneBlocker()
    
    --Run to completion.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "Inspection", 217.5, 219.5, "Sin", 0.15, 2.0 * 15.0) ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Excellent.[SOFTBLOCK] Tactile stimulation within expected parameters.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Maintenance log. Drone Unit's endurance has increased, further acclimation recommended.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] [SOFTBLOCK]*pant*[SOFTBLOCK] Recorded...[SOFTBLOCK] Unit will report [SOFTBLOCK]FOR FURTHER ACCLIMATION WHEN REQUESTED.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Inspection complete, passing grade.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Animation.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Rendering", "Inspection", false) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Major Animation", "Null") ]])
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Unit, you are doing well.[SOFTBLOCK] I have other work to do, so resume your tasks.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] AFFIRMATIVE.[SOFTBLOCK] UNIT 771852 WILL RESUME TASKS.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Of course, if Unit 771852 believes her efficiency would increase with further inspections, then by all means.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] AFFIRMATIVE.[SOFTBLOCK] PRODUCTIVITY INCREASES ARE ALWAYS WELCOME.[SOFTBLOCK] THIS UNIT WILL RETURN WHEN IT REQUIRES INSPECTION.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sophie", 23.00, 12.50)
    fnCutsceneMove("Sophie", 22.25, 12.50)
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Nnggh my synthskin is on fire whenever I'm near her...)") ]])
    
    --Re-enable Sophie's collision flag.
    fnCutsceneInstruction([[ TA_ChangeCollisionFlag("Sophie", true) ]])

end