--[Latex Form Response]
--Called when talking to Sophie in latex drone form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenLatex = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenLatex", "N")

--Argument handling.
if(not fnArgCheck(1)) then return end
local sTopicName = LM_GetScriptArgument(0)

--[Normal Path]
--The topic "No Special" is used when first calling this from Sophie's dialogue.
if(sTopicName == "No Special") then

    --First time Sophie has seen non-golem Christine.
    if(iSophieKnowsAboutRunestone == 0.0) then
        
        --Flag.
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenLatex", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hello, drone unit.[SOFTBLOCK] I just need a second, I'll be right with you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oooh![SOFTBLOCK] I like your hair![SOFTBLOCK] Too bad Christine isn't here, she'd probably get all jealous.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I don't think she recognizes me.[SOFTBLOCK] Should I mess with her?)[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutsceneBlocker()

    --Hasn't seen the Latex form yet:
    elseif(iSeenLatex == 0.0 and iSophieKnowsAboutRunestone == 1.0) then
        
        --Flag.
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenLatex", "N", 1.0)
         
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] GREETINGS, UNIT 499323.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Drone Unit?[SOFTBLOCK] Do you need my attention for a delivery?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] THIS UNIT HAS BEEN REASSIGNED TO YOUR DEPARTMENT.[SOFTBLOCK] HOW MAY I SERVE YOU?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] (That accent...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Drone Unit, who assigned you here?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] THIS UNIT WAS ASSIGNED BY UNIT 771852 TO SERVE UNIT 499323 WITH HER WHOLE BEING.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Did Unit 771852 specify anything else?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] SHE ORDERED THIS UNIT TO LOVE AND PROTECT UNIT 499323.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] And?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] AND TO CHEER HER UP WHEN SHE'S DOWN, AND TO MAKE SURE NOBODY BULLIES HER.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] And?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] UHHH...[SOFTBLOCK] TO...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oh my, Drone Unit.[SOFTBLOCK] It seems your mental faculties are not up to par.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I have to give you an inspection.[SOFTBLOCK] That's what we do here in Maintenance and Repair, you know.[SOFTBLOCK] Always have tip-top equipment.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] AFFIRMATIVE.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Drone Unit, stand at attention.[SOFTBLOCK] Prepare for inspection.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Standard protocols apply.[SOFTBLOCK] Chest forward, arms at your side.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] AFFIRMATIVE, UNIT IS STANDING AT ATTENTION...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Run the first-time cutscene.
        LM_ExecuteScript(fnResolvePath() .. "Inspection.lua", "First Time")

    --Repeats.
    else

        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hey Christine![SOFTBLOCK] How are you?[SOFTBLOCK] Need anything?[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Just checking up on you\", " .. sDecisionScript .. ", \"Checking\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"An inspection\", " .. sDecisionScript .. ", \"Inspection\") ")
        local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
        if(iSteamDroid100RepState == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"This Transducer...\", " .. sDecisionScript .. ", \"Nothing\") ")
        else
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Nothing\", " .. sDecisionScript .. ", \"Nothing\") ")
        end
        fnCutsceneBlocker()

    end

--Second part of the latex scene, after choosing to mess with Sophie.
elseif(sTopicName == "Yes") then
        
    --Clean up remaining scene parts.
    WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] GREETINGS, SLAVE UNIT 499323.[SOFTBLOCK] THIS UNIT HAS UNFORTUNATE NEWS ABOUT 778152.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] H-[SOFTBLOCK]huh?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] W-[SOFTBLOCK]what...[SOFTBLOCK] what kind of news...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Look at that face...[SOFTBLOCK] but I know how to cheer her up!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] UNIT 771852 TASKED THIS UNIT WITH DELIVERING A MESSAGE.[SOFTBLOCK] SHE REPORTS SHE IS WHOLLY UNABLE TO COMPREHEND HOW MUCH SHE LOVES YOU.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Uhhh...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] UNIT 771852 IS UNABLE TO FUNCTION PROPERLY UNLESS SHE KNOWS YOU LOVE HER.[SOFTBLOCK] WOULD YOU LIKE ME TO RELAY A MESSAGE TO HER?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Oh, of course![SOFTBLOCK] Tell her I love her twice as much as she loves me![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] CRITICAL ERROR.[SOFTBLOCK] UNIT 771852 STATED HER LOVE WAS EQUAL TO UNIT 499323'S LOVE MULTIPLIED BY TWO.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] INFINITE LOOP -[SOFTBLOCK] SHUT DOWN.[SOFTBLOCK] ERROR.[SOFTBLOCK] ERROR![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Drone![SOFTBLOCK] Drone, execute IR-719![SOFTBLOCK] Abort command![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] COMMAND RECEIVED.[SOFTBLOCK] THIS UNIT WILL REPORT THAT UNIT 499323 DOES NOT LOVE UNIT 771852![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Arrggh, you stupid drone![SOFTBLOCK] You're...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You're absolutely doing this on purpose.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] [SOFTBLOCK]*Ahem*[SOFTBLOCK] ...[SOFTBLOCK] Yep.[SOFTBLOCK] You got me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I just couldn't help myself![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hee hee![SOFTBLOCK] So is that a disguise?[SOFTBLOCK] I should have recognized your accent.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Do you use that to break into places?[SOFTBLOCK] Sneak around all stealthy?[SOFTBLOCK] Nobody even knows?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Might be a good idea to shave off the hair then, even if it is really pretty.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This isn't a disguise.[SOFTBLOCK] I really am a Drone Unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Did you get repurposed?[SOFTBLOCK] Oh no![SOFTBLOCK] We -[SOFTBLOCK] we can fix this, don't worry![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Oh, don't panic.[SOFTBLOCK] Watch this!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ta-[SOFTBLOCK]da![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Christine, this is amazing![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] No wonder you're a super-spy![SOFTBLOCK] You can change forms and disguise yourself and fight people and outsmart everyone![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] D-[SOFTBLOCK]do me![SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] What was that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] N-[SOFTBLOCK]nothing![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] Got a little carried away.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] And don't frighten me like that again.[SOFTBLOCK] I thought you got caught and repurposed for a second there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] I was terrified that a bunch of security units were going to storm in here and grab me right then...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] ...[SOFTBLOCK] And I can't imagine what I'd do if you did get repurposed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I did undergo the process.[SOFTBLOCK] It's actually quite pleasurable.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Synthskin is several times more sensitive than the normal sensory suite.[SOFTBLOCK] Every movement, even the air rushing over my body...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Okay, okay.[SOFTBLOCK] Sheesh, Christine, you're a real emotional earthquake, aren't you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It's been working out so far...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Just don't let it be the real thing someday...[SOFTBLOCK] I don't think I could handle you being taken away...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You have my word.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

--[Checking up]
--Checking up on Sophie! Awww.
elseif(sTopicName == "Checking") then
        
    --Clean up remaining scene parts.
    WD_SetProperty("Hide")
        
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Just checking up on you.[SOFTBLOCK] Drone Units are programmed to maximize the comfort and productivity of other units.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Awww![SOFTBLOCK] I'm doing great, thanks for your concern.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I know you can't talk about it, but I hope everything is going well for your activities.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're doing our best out there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] See you soon, Sophie.") ]])
    
--[Inspection!]
elseif(sTopicName == "Inspection") then
        
    --Clean up remaining scene parts.
    WD_SetProperty("Hide")
        
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] THIS UNIT IS IN NEED OF AN INSPECTION...[SOFTBLOCK] If that's okay with you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Drone Units do not speak unless spoken to.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] AFFIRMATIVE.[SOFTBLOCK] STANDING BY.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] (This is actually really fun![SOFTBLOCK] Because my processor would melt if it was anyone but Christine!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Unit, stand at attention and prepare for inspection.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] AFFIRMATIVE.[SOFTBLOCK] UNIT WILL STAND AT ATTENTION.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --First time?
    local iLatexInspectionScene = VM_GetVar("Root/Variables/Chapter5/Sophie/iLatexInspectionScene", "N")
    if(iLatexInspectionScene == 0.0) then
        LM_ExecuteScript(fnResolvePath() .. "Inspection.lua", "First Time")
    else
        LM_ExecuteScript(fnResolvePath() .. "Inspection.lua", "Repeat")
    end

--[Nothing Special]
elseif(sTopicName == "Nothing") then
        
    --Clean up remaining scene parts.
    WD_SetProperty("Hide")
    
    --Special:
    local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
    if(iSteamDroid100RepState == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Could you take a look at this with the micro-scanner?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Did some Lord Unit pass off a delivery job on you?[SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hmm, it seems it was a disposal job, actually.[SOFTBLOCK] What was this thing?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It is Series-X20 transducer.[SOFTBLOCK] Or was, evidently.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] It's basically done for.[SOFTBLOCK] Who dumped this job on you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101, if you must know.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Erm...[SOFTBLOCK] I'm searching my drives...[SOFTBLOCK] Where have I heard that name before?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] The same![SOFTBLOCK] I can attest that she's just as fierce in person.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hee hee![SOFTBLOCK] I'll bet![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[SOFTBLOCK] I'll just switch them and mark this one as scrapped.[SOFTBLOCK] Nobody will notice.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I love the duplicity![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] I hope this helps![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] My sexy maverick girlfriend...[SOFTBLOCK] Just be careful.[SOFTBLOCK] I've heard the caverns are dangerous.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I will, dearest.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Dearest?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uhhhh...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] No, it's okay.[SOFTBLOCK] I like it.[SOFTBLOCK] Good luck, dearest!") ]])
        return
    end
        
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Nothing special, I was just in the area.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Well, I'll say you're looking rather pert.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Pert?[SOFTBLOCK] Oh![SOFTBLOCK] You think they're bigger in this form?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] No, but synthskin has a higher compressability and you're strapped down.[SOFTBLOCK] So they look...[SOFTBLOCK] big...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Very big...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Touch me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Yeah...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Mmmmm, tight and firm...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] My skin is about twelve times more sensitive than golem skin...[SOFTBLOCK] Oooooohohhhhooo!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] What if I were to rub them clockwise?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] NNnnnnngghhh![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Well then.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Why did you stop?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Gotta save something for later, right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You tease![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You know it.[SOFTBLOCK] Hee hee!") ]])
    fnCutsceneBlocker()

end