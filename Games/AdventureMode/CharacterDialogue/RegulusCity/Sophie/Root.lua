--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Christine's form.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
	
	--If Christine is not a golem, these scenes play. You can't go on dates with Sophie! Well, not normal ones.
	if(sChristineForm ~= "Golem") then
		
        --Human handler:
        if(sChristineForm == "Human") then
            LM_ExecuteScript(fnResolvePath() .. "HumanFormResponse.lua", "No Special")
        
        --Latex drone:
        elseif(sChristineForm == "LatexDrone") then
            LM_ExecuteScript(fnResolvePath() .. "LatexFormResponse.lua", "No Special")
        
        --Electrosprite:
        elseif(sChristineForm == "Electrosprite") then
            LM_ExecuteScript(fnResolvePath() .. "ElectrospriteFormResponse.lua")
        
        --Darkmatter:
        elseif(sChristineForm == "Darkmatter") then
            LM_ExecuteScript(fnResolvePath() .. "DarkmatterFormResponse.lua")
        
        --SteamDroid:
        elseif(sChristineForm == "SteamDroid") then
            LM_ExecuteScript(fnResolvePath() .. "SteamDroidFormResponse.lua")
        
        --Eldritch:
        elseif(sChristineForm == "Eldritch") then
            TA_SetProperty("Facing", gci_Face_North)
            LM_ExecuteScript(fnResolvePath() .. "EldritchFormResponse.lua")
        
        end
        
        --Stop the normal response line.
        return
	end
	
	--Variables.
	local iReceivedFunction        = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
	local iToldSophieAboutFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N")
	local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iSawSpecialAnnouncement  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N")
	local iMet55InBasement         = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N")
	local iSophieImportantMeeting  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N")
	local iMet55InLowerRegulus     = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	local iLeftRepairBay           = VM_GetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N")
	local iToldSophieAbout55       = VM_GetVar("Root/Variables/Chapter5/Scenes/iToldSophieAbout55", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
	
	--Hasn't received a function yet.
	if(iReceivedFunction == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Lord Golem, I know you're excited, but I have a few things to finish up here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You should go register with the work terminals and get a function assignment from them.[SOFTBLOCK] I put your credentials in the system.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[SOFTBLOCK] Where are the terminals located?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] There are several throughout the facility.[SOFTBLOCK] The closest ones are right across from the airlock you came in through.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I will go get a function assignment, then.[SOFTBLOCK] And, you should call me Christine, or 771852.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] S-[SOFTBLOCK]Sorry.[SOFTBLOCK] Force of habit.") ]])
	
	--Special: Christine just told Sophie she'd go look for 55. Dialogue changes for the duration.
	elseif(iSophieImportantMeeting == 2.0 and iMet55InLowerRegulus == 0.0 and iIsOnDate == 0.0) then
	
		--If the player has not left the maintenance bay yet:
		if(iLeftRepairBay == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Come back in a few minutes, 771852.[SOFTBLOCK] I'm going to doctor these reports a bit before I upload them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hey, that's weird.[SOFTBLOCK] You seem to have never had the encrypted memory to begin with.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "*click*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] In fact, there has never been any reason to take interest in Unit 771852.[SOFTBLOCK] How strange![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You called me the spymaster, but I'm starting to think I could learn a few things.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'll keep you safe in whatever ways I can.[SOFTBLOCK] Nobody will take you away.[SOFTBLOCK] I promise.") ]])

		--Player has left and come back:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Christine![SOFTBLOCK] Have you met up with 2855 yet?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Not yet.[SOFTBLOCK] I was looking for a meet-up with you, instead.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Even better.[SOFTBLOCK] Could I, perhaps, help you earn some work credits?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You mean go on a date?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hey, I can be evasive if I want to.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Take Sophie out on a date?)[BLOCK]") ]])
		
			--Decision stuff.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesSpecial\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not right now...\",  " .. sDecisionScript .. ", \"NoSpecial\") ")
			fnCutsceneBlocker()
		end
	
	--Has received a function. Not on a date, hasn't seen this before.
	elseif(iReceivedFunction == 1.0 and iToldSophieAboutFunction == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Unit 499323![SOFTBLOCK] I've got my function assignment![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Ah, lovely.[SOFTBLOCK] I just finished uploading your specs.[SOFTBLOCK] What function did you get?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I've become the Lord Golem of Maintenance and Repair in this sector.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Well that's -[SOFTBLOCK] what?[SOFTBLOCK] What!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] That's lovely news, isn't it?[SOFTBLOCK] We'll be performing our functions together![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] But you -[SOFTBLOCK] huh?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Look at that, it's in the system.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is that unusual?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Yes, it really is.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Christine, Maintenance and Repair hasn't had a Lord Golem in -[SOFTBLOCK] well, since I've worked here.[SOFTBLOCK] I've kind of been running the department for a while.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] But several departments are in need of a Lord Golem and got passed over.[SOFTBLOCK] Why would the administrators put you here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Perhaps the algorithms determined we have a high compatibility?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Yeah, maybe.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Sorry, I'm not ungrateful.[SOFTBLOCK] I just haven't had anyone looking over my shoulder in quite a while.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm, I won't be looking over your shoulder...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll be looking at...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] S-[SOFTBLOCK]stop![SOFTBLOCK] You'll get me all flustered![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You will, of course, need to teach me.[SOFTBLOCK] I only have the basics of fabrication and repair programmed in right now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Teach...[SOFTBLOCK] you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes.[SOFTBLOCK] Don't you need my help fixing things?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Lord Golems don't usually work, Christine.[SOFTBLOCK] Their job is ordinarily to make Slave Golems work at optimum efficiency.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, I am no ordinary Lord Golem.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] You sure aren't![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Do you want to celebrate?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] What?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Come on, let's go do something fun![SOFTBLOCK] Let's celebrate being co-workers and tandem units![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You mean, you want to do something other than work, right now?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If you've been working one-tenth as hard as it looks like you have, I think a break is in order.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My optimization routines indicate that breaks improve physical efficiency and emotional well-being.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] ...[SOFTBLOCK] And I really, really want to celebrate with you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Yay![SOFTBLOCK] Yay![SOFTBLOCK] Yayyayayayayay![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] Ahem.[SOFTBLOCK] I mean, okay.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Since you don't know your way around, I'll just suggest activities as we approach them.[SOFTBLOCK] Does that sound good?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It does![SOFTBLOCK] Let's go![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] All right 771852, lead the way!") ]])
		fnCutsceneBlocker()
	
		--Set Lua globals.
		gsFollowersTotal = 1
		gsaFollowerNames = {"Sophie"}
		giaFollowerIDs = {0}

		--Get Sophie's uniqueID. We also need to remove Sophie's dialogue flag and collisions.
		EM_PushEntity("Sophie")
			local iSophieID = RE_GetID()
			TA_SetProperty("Clipping Flag", false)
			TA_SetProperty("Activation Script", "Null")
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs = {iSophieID}
		AL_SetProperty("Follow Actor ID", iSophieID)
	
		--Move Sophie onto Christine's position.
		EM_PushEntity("Christine")
			local fChristineX, fChristineY = TA_GetProperty("Position")
		DL_PopActiveObject()
		fnCutsceneMove("Sophie", fChristineX / gciSizePerTile, fChristineY / gciSizePerTile)
		fnCutsceneBlocker()
	
		--Fold the party.
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	--Currently on a special assignment.
	elseif(iSawSpecialAnnouncement == 2.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hey Christine![SOFTBLOCK] I was just thinking about you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good things, I hope.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Always![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Do you want to go out with me?[SOFTBLOCK] I could take a break right now...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sorry, but I just got a special work order.[SOFTBLOCK] It said to meet a unit in the basement for further instructions.[SOFTBLOCK] It wasn't you, was it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Uh, no.[SOFTBLOCK] And I haven't seen anyone in here other than you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] The only way into the basement is that ladder or the hydraulic lift.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[SOFTBLOCK] Be on your guard...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is something wrong?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] N-[SOFTBLOCK]no.[SOFTBLOCK] Not at all. Maybe they meant the terminal down there.[SOFTBLOCK] Yeah, that's got to be it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] All right.[SOFTBLOCK] Thanks for your help.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[SOFTBLOCK] Please be careful...") ]])
		fnCutsceneBlocker()
	
    --Shopping!
    elseif(iStartedShoppingSequence == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Christine![SOFTBLOCK] Ready to go shopping?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Should I go shopping with Sophie?)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's go!\", " .. sDecisionScript .. ", \"ShoppingTime\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Quite Yet\",  " .. sDecisionScript .. ", \"NotThisSecond\") ")
		fnCutsceneBlocker()
        return
    
    --Have to talk to 55 first.
    elseif(iStartedShoppingSequence == 4.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hey, no peeking![SOFTBLOCK] Go bother Unit 2855![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hmm?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I'm drawing up plans for your dress, silly![SOFTBLOCK] And I want it to be a surprise, so no peeking![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Guess I better go talk to 55...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (But I really want to see what my dress will look like...)") ]])
		fnCutsceneBlocker()
        return
    
	--Not on a date with Sophie.
	elseif(iReceivedFunction == 1.0 and iToldSophieAboutFunction == 1.0 and iIsOnDate == 0.0) then
	
        --Special:
        local iManuTold55Plan = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTold55Plan", "N")
        local iHasLatexForm   = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
        if(iManuTold55Plan == 1.0 and iHasLatexForm == 0.0) then
            
            --Variables.
            local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
            
            --Flags.
             VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
             VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenLatex", "N", 1.0)
            
            --Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Good morning, 771852.[SOFTBLOCK] What -[SOFTBLOCK] what is that twinkle in your eye?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You noticed.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You're practically beaming![SOFTBLOCK] Did you just come up with a fun idea?[SOFTBLOCK] A joke?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Actually, I wanted you to transform me into a drone unit.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Whaaaaaaaaaaaaaaaaaaaaaaat!?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Tr-[SOFTBLOCK]transform you?[BLOCK][CLEAR]") ]])
            if(iSophieKnowsAboutRunestone == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, I've had this little runestone in my family for generations.[SOFTBLOCK] Apparently, it lets me transform myself.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] (She stands, helpless, in the tube.[SOFTBLOCK] I press the switch and the gel sprays...)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I just focus for a second, think about how I was, and -[SOFTBLOCK] poof.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] (Covered in crawling latex snaking its way up her nude body...)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] In fact, I can show you.[SOFTBLOCK] Do you want to see it?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] (Her skin and muscles turn into actuators covered by tight latex that strains with each motion...)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sophie?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] (Her face reshaped into a mask, her mind simplified.[SOFTBLOCK] She obeys my every command!)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sophie?[SOFTBLOCK] Are you all right?[SOFTBLOCK] Did I upset you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Wh-[SOFTBLOCK]what?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] What are we waiting for?[SOFTBLOCK] Transform into a human already![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, all right.[SOFTBLOCK] I was just - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Hurry![SOFTBLOCK] Please![SOFTBLOCK] Hee hee!") ]])
                fnCutsceneBlocker()
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Like, repurpose you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's for a special assignment.[SOFTBLOCK] Nobody would suspect a simple-minded drone unit, right?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I just transform myself into a human, you convert me...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] What are we waiting for?[SOFTBLOCK] Transform into a human already![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, all right.[SOFTBLOCK] I was just - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Hurry![SOFTBLOCK] Please![SOFTBLOCK] Hee hee!") ]])
                fnCutsceneBlocker()
            end
            
            --Wait.
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Flashwhite.
            Cutscene_CreateEvent("Flash Christine White", "Actor")
                ActorEvent_SetProperty("Subject Name", "Christine")
                ActorEvent_SetProperty("Flashwhite", "Null")
            DL_PopActiveObject()
            fnCutsceneBlocker()

            fnCutsceneWait(75)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
            fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Right, into the tube with you![SOFTBLOCK] We can use the one in the basement![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay![SOFTBLOCK] Be sure - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Tube tube tube tube!") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Call the TF cutscene here.
            fnCutsceneWait(25)
            fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
            fnCutsceneBlocker()
            LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_LatexVolunteer/Scene_Begin.lua")
            
            --Move scene to RegulusCityG.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iManuPostLatexScene", "N", 1.0)
            fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityG", "FORCEPOS:23.0x10.0x0") ]])
            fnCutsceneBlocker()
            return
        end
    
    
		--Special:
		local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
		if(iSteamDroid100RepState == 1.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Good morning, 771852.[SOFTBLOCK] Hey, what's that you've got there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A Series-X20 transducer.[SOFTBLOCK] You can't tell?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] It's in awful shape![SOFTBLOCK] Where did you get that?[SOFTBLOCK] Who would send that piece of scrap to us?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101, if you must know.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Erm...[SOFTBLOCK] I'm searching my drives...[SOFTBLOCK] Where have I heard that name before?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] The same![SOFTBLOCK] I can attest that she's just as fierce in person.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hee hee![SOFTBLOCK] I'll bet![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[SOFTBLOCK] I'll just switch them and mark this one as scrapped.[SOFTBLOCK] Nobody will notice.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I love the duplicity![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] I hope this helps![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] My sexy maverick girlfriend...[SOFTBLOCK] Just be careful.[SOFTBLOCK] I've heard the caverns are dangerous.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I will, dearest.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Dearest?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uhhhh...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] No, it's okay.[SOFTBLOCK] I like it.[SOFTBLOCK] Good luck, dearest!") ]])
			return
		end
        
        --Told Sophie about 55.
		if(iToldSophieAbout55 == 0.0 and iMet55InLowerRegulus >= 1.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAbout55", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] 771852![SOFTBLOCK] Did you -[SOFTBLOCK] did you meet with -[SOFTBLOCK] you know -[SOFTBLOCK] her?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You mean Unit 2855?[SOFTBLOCK] There's nobody in earshot.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I know.[SOFTBLOCK] I even put up some dampeners outside.[SOFTBLOCK] But -[SOFTBLOCK] it's so much more fun to talk in code, right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh![SOFTBLOCK] Yes, of course.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I met my old friend.[SOFTBLOCK] We've decided that Operation Green Fox is a go.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Okay, I know what you're trying to do, but Operation Green Fox?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Look.[SOFTBLOCK][SOFTBLOCK] Not everything that comes out of my vocal synthesizer is a treatise on Othello, okay?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I'm kidding![SOFTBLOCK] You should see your face![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] (What's Othello?[SOFTBLOCK] Hm, maybe I should just pretend like I know.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Now, it's probably best that I don't tell you *exactly* where we're going.[SOFTBLOCK] Or when.[SOFTBLOCK] For your safety.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I understand.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] ...[SOFTBLOCK] You are making me so hot right now.[SOFTBLOCK] I think I'm going to have a meltdown.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Please don't.[SOFTBLOCK] At least not until I've got you back to my quarters.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh, calm yourself, Sophie...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Ahem.[SOFTBLOCK] So.[SOFTBLOCK] Christine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Are you ready to get to work for the day?[SOFTBLOCK] I'll understand if you have...[SOFTBLOCK] sexy...[SOFTBLOCK] dirty...[SOFTBLOCK] spy stuff to do...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Should I start my work for the day?)[BLOCK]") ]])
            
            --Decision stuff.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"StartWork\") ")
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Yet...\",  " .. sDecisionScript .. ", \"NoWork\") ")
            fnCutsceneBlocker()
            return
        end
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Good morning, 771852.[SOFTBLOCK] Ready to get to work?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Should I start my work for the day?)[BLOCK]") ]])
	
		--Decision stuff.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"StartWork\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Yet...\",  " .. sDecisionScript .. ", \"NoWork\") ")
		fnCutsceneBlocker()
	
		--[=[
		--Roll a random number.
		local iRoll = LM_GetRandomNumber(0, 4)
	
		--The first time Christine talks to Sophie after seeing 55 in Lower Regulus, this plays.
		if(iToldSophieAbout55 == 0.0 and iMet55InLowerRegulus >= 1.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAbout55", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] 771852![SOFTBLOCK] Did you -[SOFTBLOCK] did you meet with -[SOFTBLOCK] you know -[SOFTBLOCK] her?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You mean Unit 2855?[SOFTBLOCK] There's nobody in earshot.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I know.[SOFTBLOCK] I even put up some dampeners outside.[SOFTBLOCK] But -[SOFTBLOCK] it's so much more fun to talk in code, right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh![SOFTBLOCK] Yes, of course.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I met my old friend.[SOFTBLOCK] We've decided that Operation Green Fox is a go.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Okay, I know what you're trying to do, but Operation Green Fox?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Look.[SOFTBLOCK][SOFTBLOCK] Not everything that comes out of my mouth is a treatise on Othello, okay?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'm kidding![SOFTBLOCK] You should see your face![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] (What's Othello?[SOFTBLOCK] Hm, maybe I should just pretend like I know.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Now, it's probably best that I don't tell you *exactly* where we're going.[SOFTBLOCK] Or when.[SOFTBLOCK] For your safety.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I understand.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] You are making me so hot right now.[SOFTBLOCK] I think I'm going to have a meltdown.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Please don't.[SOFTBLOCK] At least not until I've got you back to my quarters.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Ah![SOFTBLOCK] Speaking of...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're asking me out, aren't you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Can you blame a girl for trying?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Should I take Sophie out for a date?[SOFTBLOCK] She really looks like she wants me...)[BLOCK][CLEAR]") ]])
		
		--Conversation is random.
		elseif(iRoll == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sophie![SOFTBLOCK] How are you doing?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'm great, Christine![SOFTBLOCK] Thanks for asking.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'm really making a lot of headway on the backlog, despite the tool issues.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] So, erm, do you maybe want to go out with me tonight?[BLOCK][CLEAR]") ]])
		
		--Second conversation.
		elseif(iRoll == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hello, Christine![SOFTBLOCK] I'm glad you came in, I was thinking I needed a break.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I have excellent timing, it seems.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hee hee![SOFTBLOCK] It's one of your many good qualities![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Do you maybe want to go do something fun tonight?[BLOCK][CLEAR]") ]])
		
		--Third conversation.
		elseif(iRoll == 2.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hello 499323![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hello 771852![SOFTBLOCK] Phew, am I glad to see you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Some Lord Unit from sector 79 decided to send me extra repair duty today because their maintenance unit is out of commission.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I could really use some relief...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, I can think of some things we can do...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Should I show Sophie a good time?)[BLOCK][CLEAR]") ]])
		
		--Fourth conversation.
		elseif(iRoll == 3.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 499323, please report.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Productivity is at above average levels.[SOFTBLOCK] Current task is -[SOFTBLOCK] hey...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, please continue.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I can't focus with you rubbing my arm like that.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You seem unfocused.[SOFTBLOCK] Perhaps a few moments relaxing would clear your cache?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, of course![SOFTBLOCK] Want to go do something fun?[BLOCK][CLEAR]") ]])
		
		--Fifth conversation.
		elseif(iRoll == 4.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sophie![SOFTBLOCK] How are things going?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Great![SOFTBLOCK] My favourite wrench broke and that stupid sentry gun won't stop beeping at me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Meanwhile some idiot from Sector 112 sent me a vending machine that wasn't even broken.[SOFTBLOCK] So not only do they apparently dump their work on me, but they didn't even do that right![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I spent 45 minutes looking for a broken servo that wasn't even there![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You sound like you could use a distraction.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Yes![SOFTBLOCK] Yes![SOFTBLOCK] There's no way I'm going to get any more work done here anyway, I'm too frazzled.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But -[SOFTBLOCK] I feel a lot better just seeing you.[SOFTBLOCK] Everything will be okay.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Should I take Sophie out to calm her down?)[BLOCK]") ]])
		end
		
		--Decision stuff.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sorry...\",  " .. sDecisionScript .. ", \"No\") ")
		fnCutsceneBlocker()]=]
	
	--Finished a date.
	elseif(iReceivedFunction == 1.0 and iToldSophieAboutFunction == 1.0 and iIsOnDate == 3.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I just have a few things to finish up, 771852.[SOFTBLOCK] I won't stay up too late, I promise.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'll...[SOFTBLOCK] think of you when I'm defragging.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll think of you, too.[SOFTBLOCK] Good night.") ]])
		fnCutsceneBlocker()
	
	end

--Start work.
elseif(sTopicName == "StartWork") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Variables.
	local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
	VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 5)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right, let's get to it.[SOFTBLOCK] What's on our list for today?") ]])
	fnCutsceneBlocker()
	
	--Fade to black.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Hold on black. This is where we'd put interstitial cutscenes.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Reposition.
	fnCutsceneTeleport("Christine", 23.25, 12.50)
	fnCutsceneTeleport("Sophie", 22.25, 12.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneFace("Sophie", 0, -1)
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: 10 hours later...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (Received 5 work credits.)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneWait(25)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hand me that screwdriver, won't you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay, finish that toaster and we're done for the day.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Done, and done![SOFTBLOCK] Phew![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Job well done.[SOFTBLOCK] We're making some good progress on the backlog.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Time really flies by when I have someone to talk to.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] So, do you maybe want to keep the good times rolling...?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Sophie's pretty obviously asking me out.[SOFTBLOCK] Should I?)[BLOCK]") ]])
		
	--Decision stuff.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not tonight...\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()
	
--No work yet.
elseif(sTopicName == "NoWork") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I have a few things I need to do first.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh, okay.[SOFTBLOCK] Uhm, could you maybe walk a bit slower on your way out?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uhhh, why?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] You have a very nice sway.[SOFTBLOCK] I like to...[SOFTBLOCK] oops...[SOFTBLOCK] I'm being creepy aren't I?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] No, not at all![SOFTBLOCK] I'll consider it a compliment.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And, I'll come back when I have time.[SOFTBLOCK] Feel free to get started without me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Right away, Lord Unit!") ]])
	fnCutsceneBlocker()

--Go on a date with Sophie.
elseif(sTopicName == "Yes") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Of course![SOFTBLOCK] Come on, let's go do something fun![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Lead the way, tandem unit.") ]])
	fnCutsceneBlocker()
	
	--Set Lua globals.
	gsFollowersTotal = 1
	gsaFollowerNames = {"Sophie"}
	giaFollowerIDs = {0}

	--Get Sophie's uniqueID. We also need to remove Sophie's dialogue flag and collisions.
	EM_PushEntity("Sophie")
		local iSophieID = RE_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iSophieID}
	AL_SetProperty("Follow Actor ID", iSophieID)

	--Move Sophie onto Christine's position.
	EM_PushEntity("Christine")
		local fChristineX, fChristineY = TA_GetProperty("Position")
	DL_PopActiveObject()
	fnCutsceneMove("Sophie", fChristineX / gciSizePerTile, fChristineY / gciSizePerTile)
	fnCutsceneBlocker()

	--Fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Go on a date with Sophie, special dialogue during the "Meet 55 in the basement" part of the story.
elseif(sTopicName == "YesSpecial") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Sure, let's go do something fun![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, erm, maybe you should go meet up with Unit 2855 instead?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She can wait a few hours.[SOFTBLOCK] It's not like she's going to spoil.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] I'm not going to try to talk you out of it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Lead the way, you sexy maverick!") ]])
	fnCutsceneBlocker()
	
	--Set Lua globals.
	gsFollowersTotal = 1
	gsaFollowerNames = {"Sophie"}
	giaFollowerIDs = {0}

	--Get Sophie's uniqueID. We also need to remove Sophie's dialogue flag and collisions.
	EM_PushEntity("Sophie")
		local iSophieID = RE_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iSophieID}
	AL_SetProperty("Follow Actor ID", iSophieID)

	--Move Sophie onto Christine's position.
	EM_PushEntity("Christine")
		local fChristineX, fChristineY = TA_GetProperty("Position")
	DL_PopActiveObject()
	fnCutsceneMove("Sophie", fChristineX / gciSizePerTile, fChristineY / gciSizePerTile)
	fnCutsceneBlocker()

	--Fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
--No date.
elseif(sTopicName == "No") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm sorry Sophie, but I have a lot of things to do.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] Don't worry, I know how it is.[SOFTBLOCK] You probably have a lot of Lord Unit functions you have to do after hours.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sorry.[SOFTBLOCK] Maybe next time?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'm looking forward to it.[SOFTBLOCK] See you!") ]])
	fnCutsceneBlocker()

--No date, special dialogue during the "Meet 55" part of the story.
elseif(sTopicName == "NoSpecial") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Not right now, no.[SOFTBLOCK] I've got a date -[SOFTBLOCK] with 2855![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] For 0.555866 seconds you made my CPU stutter.[SOFTBLOCK] Don't scare me like that![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sorry![SOFTBLOCK] I belong wholly to you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] That's right![SOFTBLOCK] Now, go on.[SOFTBLOCK] My cute, sexy tandem unit is a rebel leader![SOFTBLOCK] This is so exciting![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I hope you don't talk like that when other units can hear you...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hee hee![SOFTBLOCK] Nope![SOFTBLOCK] Nobody will suspect a thing!") ]])
	fnCutsceneBlocker()
    
--Time for some shopping.
elseif(sTopicName == "ShoppingTime") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Yep, let's get going![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Splendid![SOFTBLOCK] We can take the tram to Sector 119![SOFTBLOCK] Lead the way!") ]])
	fnCutsceneBlocker()
	
	--Set Lua globals.
	gsFollowersTotal = 1
	gsaFollowerNames = {"Sophie"}
	giaFollowerIDs = {0}

	--Get Sophie's uniqueID. We also need to remove Sophie's dialogue flag and collisions.
	EM_PushEntity("Sophie")
		local iSophieID = RE_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iSophieID}
	AL_SetProperty("Follow Actor ID", iSophieID)

	--Move Sophie onto Christine's position.
	EM_PushEntity("Christine")
		local fChristineX, fChristineY = TA_GetProperty("Position")
	DL_PopActiveObject()
	fnCutsceneMove("Sophie", fChristineX / gciSizePerTile, fChristineY / gciSizePerTile)
	fnCutsceneBlocker()

	--Fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
--Not time for shopping.
elseif(sTopicName == "NotThisSecond") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Still tying up a few loose ends.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I've been just daydreaming about the pretty dresses I'm going to make...[SOFTBLOCK] Oooh![SOFTBLOCK] Don't keep a girl waiting!") ]])
	fnCutsceneBlocker()
end