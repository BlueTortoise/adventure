--[Steam Droid Form Response]
--Called when talking to Sophie in Steam Droid form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenSteamDroid = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenSteamDroid", "N")

--First time Sophie has seen non-golem Christine.
if(iSophieKnowsAboutRunestone == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenSteamDroid", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] So that's what the plan was, eh?[SOFTBLOCK] Hee hee![SOFTBLOCK] You look good as a Steam Droid![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks![SOFTBLOCK] I designed me...[SOFTBLOCK] myself![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It doesn't have all the conveniences of a Golem, but it's a custom work.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But I mean, no auto-stabilizers.[SOFTBLOCK] Primitive power system.[SOFTBLOCK] Corrosion problems.[SOFTBLOCK] No stasis locks.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I have to rely on gyroscopes for dodging.[SOFTBLOCK] I have low optical fidelity.[SOFTBLOCK] The list goes on.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But you're proud of how you turned out?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Absolutely![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] I'd love to go back to the blueprints and give it another shot, but it'd be a hobby thing at this point.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Maybe I could work on you a bit...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And make another steam lord?[SOFTBLOCK] No, I think we'll save the parts for other Steam Droids.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Yes, good thinking.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I haven't really improved the design too much, but I think I could streamline the upgrade process.[SOFTBLOCK] It's going to take a few iterations to get it down.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Plus requisitioning the parts discreetly might be tricky.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] SX-399 was the only critical case.[SOFTBLOCK] I'm sure there's lot of Steam Droids who could use an upgrade, but we shouldn't compromise ourselves.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] 55 said you can transform yourself back, right?") ]])
    fnCutsceneBlocker()

    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Keen![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Did I just say that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] SX-399 must have rubbed off on you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] That's what she said before she ran off.[SOFTBLOCK] How is she?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Everything went smoothly, more or less.[SOFTBLOCK] There was a close-call but love won out in the end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] It's so much fun doing this spy stuff -[SOFTBLOCK] if you need anything else from me, you let me know![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Gladly.") ]])
    fnCutsceneBlocker()

--First time as a Steam Droid. No transformation at the end.
elseif(iSeenSteamDroid == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenSteamDroid", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Huh!? -[SOFTBLOCK][EMOTION|Sophie|Smirk] oh, it's you, Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] So that's our mutual friend's plan was, eh?[SOFTBLOCK] Hee hee![SOFTBLOCK] You look good as a Steam Droid![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks![SOFTBLOCK] I designed me...[SOFTBLOCK] myself![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It doesn't have all the conveniences of a Golem, but it's a custom work.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But I mean, no auto-stabilizers.[SOFTBLOCK] Primitive power system.[SOFTBLOCK] Corrosion problems.[SOFTBLOCK] No stasis locks.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I have to rely on gyroscopes for dodging.[SOFTBLOCK] I have low optical fidelity.[SOFTBLOCK] The list goes on.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But you're proud of how you turned out?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Absolutely![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] I'd love to go back to the blueprints and give it another shot, but it'd be a hobby thing at this point.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Maybe I could work on you a bit...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And make another steam lord?[SOFTBLOCK] No, I think we'll save the parts for other Steam Droids.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Yes, good thinking.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I haven't really improved the design too much, but I think I could streamline the upgrade process.[SOFTBLOCK] It's going to take a few iterations to get it down.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Plus requisitioning the parts discreetly might be tricky.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] SX-399 was the only critical case.[SOFTBLOCK] I'm sure there's lot of Steam Droids who could use an upgrade, but we shouldn't compromise ourselves.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, 55 swore me to secrecy but -[SOFTBLOCK] well.[SOFTBLOCK] I can't keep secrets from you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] She said she was glad that you used your transformation ability so cleverly.[SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Of course she'd demand secrecy![SOFTBLOCK] Can't break the facade for even a second![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] How is she?[SOFTBLOCK] And SX-399?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] They get along pretty well.[SOFTBLOCK] 55 is...[SOFTBLOCK] well, I'm working on getting her to open up.[SOFTBLOCK] It's tough.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] It's so much fun doing this spy stuff -[SOFTBLOCK] if you need anything else from me, you let me know![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] And say hi for me to SX-399 if you see her again.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Gladly.") ]])
    fnCutsceneBlocker()

--Repeats.
else

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Out of curiosity, did you need the goggles for something?[SOFTBLOCK] Aren't your eyes glare-proof?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Steam Droids don't have all the useful bits, unfortunately.[SOFTBLOCK] These come in handy in case of gas or steam leaks.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Practical and pretty -[SOFTBLOCK] that's my Christine!") ]])
    fnCutsceneBlocker()

end
