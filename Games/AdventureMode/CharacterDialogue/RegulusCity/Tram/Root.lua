--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Cutscene.
local fnRunTram = function()
end

if(sTopicName ~= "Hello" and sTopicName ~= "Nevermind") then
	fnRunTram = function()

		--If 55 is not in the party, she appears here.
        local b55Joined = true
		local iIs55Following           = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
        local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
        local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
        local iIsGalaTime              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
        
        --Shopping sequence. Precludes everything else, 55 does not join up.
        if(iStartedShoppingSequence == 1.0 and iIsOnDate == 1.0 or iIsGalaTime > 0.0) then
            b55Joined = false
            
        --Shopping end. 55 does not join up.
        elseif(iStartedShoppingSequence == 2.0) then
            b55Joined = false
        
        --55 joins the party.
		elseif(iIs55Following == 0.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
            
            --In sector 198, 55 just walks up to the tram.
            local sCurrentTramArea = VM_GetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S")
            if(sCurrentTramArea == "RegulusCity198A") then
                
                --Remove 55's collision and activation script.
                EM_PushEntity("55")
                    TA_SetProperty("Clipping Flag", false)
                    TA_SetProperty("Activation Script", "Null")
                DL_PopActiveObject()
                
                --55 walks up to Christine.
                fnCutsceneFace("Christine", 1, 1)
                fnCutsceneMove("55", 25.25, 10.50)
                fnCutsceneFace("55", 0, -1)
                fnCutsceneBlocker()
                
                --Open the door.
                if(AL_GetProperty("Is Door Open", "Door") == false) then
                    fnCutsceneWait(5)
                    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
                    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door") ]])
                    fnCutsceneBlocker()
                    fnCutsceneWait(5)
                end
            
                --Move 55.
                fnCutsceneMove("55", 25.25, 8.50)
                fnCutsceneMove("55", 19.25, 8.50)
                fnCutsceneBlocker()
                fnCutsceneFace("Christine", 0, -1)
                fnCutsceneFace("55", 0, -1)
                fnCutsceneWait(25)
        
                --Set Lua globals.
                gsFollowersTotal = 1
                gsaFollowerNames = {"55"}
                giaFollowerIDs = {0}

                --Get 55's uniqueID.
                EM_PushEntity("55")
                    local i55ID = RE_GetID()
                DL_PopActiveObject()

                --Store it and tell her to follow.
                giaFollowerIDs = {i55ID}
                AL_SetProperty("Follow Actor ID", i55ID)
            
            --Otherwise, she spawns and approached Christine.
            else
			
                --Spawn her.
                fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_North, false, nil)
                
                --Dialogue.
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I will be right there.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                
                --Christine looks over.
                fnCutsceneFace("Christine", 1, 1)
                
                --55 looks around.
                fnCutsceneTeleport("55", 25.25, 8.50)
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutsceneFace("55", 1, 0)
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutsceneFace("55", -1, 0)
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                
                --55 joins Christine on the tram.
                fnCutsceneMove("55", 19.25, 8.50)
                fnCutsceneBlocker()
                fnCutsceneFace("Christine", 0, 1)
                fnCutsceneFace("55", 0, -1)
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Let's get going.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
        
                --Set Lua globals.
                gsFollowersTotal = 1
                gsaFollowerNames = {"55"}
                giaFollowerIDs = {0}

                --Get 55's uniqueID.
                EM_PushEntity("55")
                    local i55ID = RE_GetID()
                DL_PopActiveObject()

                --Store it and tell her to follow.
                giaFollowerIDs = {i55ID}
                AL_SetProperty("Follow Actor ID", i55ID)
			end
		end

		--Christine gets on the tram.
		fnCutsceneMove("Christine", 19.25, 7.00)
        if(b55Joined == false) then
            fnCutsceneMove("Sophie", 19.25, 7.00)
        else
            fnCutsceneMove("55", 19.25, 7.00)
        end
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
        if(b55Joined == false) then
            fnCutsceneFace("Sophie", 0, -1)
        else
            fnCutsceneFace("55", 0, -1)
        end
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Focus Position", (19.25 * gciSizePerTile), (7.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Teleport Christine offscreen.
		fnCutsceneTeleport("Christine", -100.25, -100.50)
        if(b55Joined == false) then
            fnCutsceneTeleport("Sophie", -100.25, -100.50)
        else
            fnCutsceneTeleport("55", -100.25, -100.50)
        end
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(55)
		fnCutsceneBlocker()
		
		--Tram moves offscreen.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|TramStart") ]])
		fnCutsceneMoveFace("TramA", 17.25 - 3.0, 6.50, -1, 0, 1.10)
		fnCutsceneMoveFace("TramB", 18.25 - 3.0, 6.50, -1, 0, 1.10)
		fnCutsceneMoveFace("TramC", 19.25 - 3.0, 6.50, -1, 0, 1.10)
		fnCutsceneMoveFace("TramD", 20.25 - 3.0, 6.50, -1, 0, 1.10)
		fnCutsceneMoveFace("TramE", 21.25 - 3.0, 6.50, -1, 0, 1.10)
		fnCutsceneMoveFace("TramF", 22.25 - 3.0, 6.50, -1, 0, 1.10)
		fnCutsceneBlocker()
		
		--Trams speeds up.
		fnCutsceneMoveFace("TramA", 17.25 - 30.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramB", 18.25 - 30.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramC", 19.25 - 30.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramD", 20.25 - 30.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramE", 21.25 - 30.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramF", 22.25 - 30.0, 6.50, -1, 0, 2.50)
		fnCutsceneBlocker()
		
		--Darken the screen.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneBlocker()
	end
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local sCurrentTramArea         = VM_GetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
    local iIsGalaTime              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iManuTookJob             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N")
    local iSawSector99Scene        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSector99Scene", "N")
    
    --If the player disembarked at Sector 48 during the Manufactory subquest:
    if(sCurrentTramArea == "RegulusManufactoryA") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The tram is out of service and will be redirected soon.[SOFTBLOCK] I should see if there's another way to Sector 99.)") ]])
        fnCutsceneBlocker()
        return
    
    --If the player is in Sector 119 but hasn't completed the subquest yet:
    elseif(iStartedShoppingSequence < 1.0 and sCurrentTramArea == "RegulusCity119A") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](We should go ask around the shops and try to find some dressmaking materials before we leave.)[BLOCK]") ]])
        fnCutsceneBlocker()
        return
    
    --Can't leave the gala.
    elseif(sCurrentTramArea == "RegulusArcaneA") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](No going back.[SOFTBLOCK] We have a job to do.)[BLOCK]") ]])
        fnCutsceneBlocker()
        return
    
    end
	
	--In all cases.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Where shall I take the tram to?)[BLOCK]") ]])
    
    --Gala. Only that is available.
    if(iIsGalaTime > 0.0) then
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Arcane University\", " .. sDecisionScript .. ", \"Arcane University\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Nevermind\",  " .. sDecisionScript .. ", \"Nevermind\") ")
        fnCutsceneBlocker()
        return
    end
    
    --Shopping sequence! Restricts options.
    if((iStartedShoppingSequence == 1.0 and iIsOnDate == 1.0) or (iStartedShoppingSequence == 2.0)) then
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        if(sCurrentTramArea ~= "RegulusCityF") then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 96\", " .. sDecisionScript .. ", \"Regulus City Sector 96\") ")
        end
        if(sCurrentTramArea ~= "RegulusCity119A") then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 119\", " .. sDecisionScript .. ", \"Regulus City Sector 119\") ")
        end
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Nevermind\",  " .. sDecisionScript .. ", \"Nevermind\") ")
        fnCutsceneBlocker()
        return
    
    --Shopping sequence is 3.0.
    elseif(iStartedShoppingSequence == 3.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I should take Sophie back to the repair bay before I go anywhere else.)") ]])
        fnCutsceneBlocker()
        return
    end

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	if(sCurrentTramArea ~= "RegulusCityF") then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 96\", " .. sDecisionScript .. ", \"Regulus City Sector 96\") ")
	end
	if(sCurrentTramArea ~= "RegulusCity15A") then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 15\", " .. sDecisionScript .. ", \"Regulus City Sector 15\") ")
	end
	if(sCurrentTramArea ~= "RegulusCity198A") then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 198\", " .. sDecisionScript .. ", \"Regulus City Sector 198\") ")
	end
	if(sCurrentTramArea ~= "RegulusExteriorStationE") then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Telemetry Facility\", " .. sDecisionScript .. ", \"Telemetry Facility\") ")
	end
	if(sCurrentTramArea ~= "TelluriumMinesA") then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Tellurium Mines\", " .. sDecisionScript .. ", \"Tellurium Mines\") ")
	end
    if(iManuTookJob == 1.0 and iSawSector99Scene == 0.0) then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sector 99\", " .. sDecisionScript .. ", \"Sector 99\") ")
    end
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Nevermind\",  " .. sDecisionScript .. ", \"Nevermind\") ")
	fnCutsceneBlocker()

--Regulus City. Leads to Regulus City F.
elseif(sTopicName == "Regulus City Sector 96") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
    --Scene that plays after the shopping sequence is over.
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    if(iStartedShoppingSequence == 2.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 3.0)
        LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Sophie Train Ride/Execution.lua")
    end
    
	--Transit to the next area.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityF", gsRoot .. "Maps/RegulusCity/RegulusCityF/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Sector 15.
elseif(sTopicName == "Regulus City Sector 15") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCity15A", gsRoot .. "Maps/RegulusCity/RegulusCity15A/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Sector 198.
elseif(sTopicName == "Regulus City Sector 198") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCity198A", gsRoot .. "Maps/RegulusCity/RegulusCity198A/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Sector 73.
elseif(sTopicName == "Tellurium Mines") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("TelluriumMinesA", gsRoot .. "Maps/RegulusMines/TelluriumMinesA/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Tellurium Mines, below Regulus City.
elseif(sTopicName == "Telemetry Facility") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusExteriorStationE", gsRoot .. "Maps/RegulusExterior/RegulusExteriorStationE/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Telemetry Facility, eastern part of Regulus.
elseif(sTopicName == "Telemetry Facility") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusExteriorStationE", gsRoot .. "Maps/RegulusExterior/RegulusExteriorStationE/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Sector 99, Manufactory sidequest.
elseif(sTopicName == "Sector 99") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusManufactoryA", gsRoot .. "Maps/RegulusManufactory/RegulusManufactoryA/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Regulus City. Leads to the shopping sector.
elseif(sTopicName == "Regulus City Sector 119") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCity119A", gsRoot .. "Maps/RegulusCity/RegulusCity119A/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()
    
--To the gala!
elseif(sTopicName == "Arcane University") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusArcaneA", gsRoot .. "Maps/RegulusArcane/RegulusArcaneA/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Nevermind. Cancels the action.
elseif(sTopicName == "Nevermind") then
	WD_SetProperty("Hide")

end