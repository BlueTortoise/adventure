--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Septima takes slot 5.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Septima", "NeutralUp") ]])
	
	--First-time dialogue.
	local iTalkedSeptima = VM_GetVar("Root/Variables/Chapter1/Scenes/iTalkedSeptima", "N")
	if(iTalkedSeptima == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedSeptima", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: We have much to discuss, Voidwalker.[SOFTBLOCK] Surely you are brimming with inquiry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I am -[SOFTBLOCK] but it's Mei, not Shirley.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Let's start with the big one.[SOFTBLOCK] How do I get home?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: You cannot.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] WHAT!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Please, calm yourself.[SOFTBLOCK] I do not place malice behind my words.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You've got to be kidding me![SOFTBLOCK] You brought me here - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Apologies, but we did no such thing.[SOFTBLOCK] The Rilmani have little power over The Still Plane.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] But you clearly can teleport with mirrors and - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Yes, we move between dimensions.[SOFTBLOCK] But only between the nearest four.[SOFTBLOCK] The Still Plane is unlike the others.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Mei, you have been brought here for a purpose.[SOFTBLOCK] It is a great one, and it is also why you cannot return to Earth.[SOFTBLOCK] Now, or possibly ever.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I don't care about some riddles!") ]])
		fnCutsceneBlocker()
		
		--Stop the music, reboot the dialogue.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])
		
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: The universe will end if you return to Earth.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Oh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Okay, you have my attention.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: There is a creature of unconscionable power, who predates the existence of reality itself.[SOFTBLOCK] It rivals the True God, or possibly is like the True God.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: That creature is called Nemesis in our language, for we dare not utter his true name.[SOFTBLOCK] That creature's power is bound by your runestone.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: If you were drawn to Pandemonium, it can only mean that he has finally found a way to regain the power locked away by your stone.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] The runestone brought me here?[SOFTBLOCK] But can't it take me back, then?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: The six sealing runes were sent to The Still Plane when the universe was split into five.[SOFTBLOCK] The Still Plane, where you are from, is isolated.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: No creature can travel between the two, meaning Nemesis' power was safely locked away from him.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: You exist for a reason, Mei.[SOFTBLOCK] Your reason is the rune's reason::[SOFTBLOCK] To keep Nemesis from breaking his seal.[SOFTBLOCK] If he returns, he will reduce the universe to entropic uniformity.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Your runestone exists to seal his power.[SOFTBLOCK] If he has regained it, you exist to restore the seal.[SOFTBLOCK] That is why you are here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] But I'm just a waitress...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Yes, you are.[SOFTBLOCK] You have been called upon to do the extraordinary.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: You are not alone.[SOFTBLOCK] We, the Rilmani, will aid you to the best of our ability.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: I have observed you in your daily life.[SOFTBLOCK] You are a good, kind, strong person.[SOFTBLOCK] You will succeed.[SOFTBLOCK] I have confidence in you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Well that makes one of us...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This is all so much to take in.[SOFTBLOCK] I keep thinking this must be some terrible dream.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: The return of Nemesis is beyond the most horrendous of scenarios.[SOFTBLOCK] To even look upon his form is to know depthless fear.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: But you, Mei, will be *his* nightmare.[SOFTBLOCK] I will personally train you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I just want to go home...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: We will teach you to use the scrying mirror.[SOFTBLOCK] You will be able to see your family, though you will not be able to talk to them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Truly, Mei, I am sorry that this is the situation you find yourself in.[SOFTBLOCK] Every effort will be made to comfort you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Well, if you say there's no way back -[SOFTBLOCK] I can't really tell if you're lying.[SOFTBLOCK] I've no choice but to believe you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And -[SOFTBLOCK] there's only one way to go::[SOFTBLOCK] forward.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Very well.[SOFTBLOCK] Do you have any further questions?[SOFTBLOCK] Or, shall we begin?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I have some questions...\", " .. sDecisionScript .. ", \"Questions\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's do this.\",  " .. sDecisionScript .. ", \"Training\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I want to look around.\",  " .. sDecisionScript .. ", \"Leave\") ")
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Hello again, Mei.[SOFTBLOCK] Did you have more questions, or are you ready to begin your training?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I have some questions...\", " .. sDecisionScript .. ", \"Questions\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's do this.\",  " .. sDecisionScript .. ", \"Training\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I want to look around.\",  " .. sDecisionScript .. ", \"Leave\") ")
		fnCutsceneBlocker()

	end

--Activates topics mode.
elseif(sTopicName == "Questions") then

	--Restart the music.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "NixNedar") ]])

	--Unlock Septima's topics.
	WD_SetProperty("Unlock Topic", "Name", 1)
	WD_SetProperty("Unlock Topic", "NewRilmani", 1)
	WD_SetProperty("Unlock Topic", "PrimaryWords", 1)
	WD_SetProperty("Unlock Topic", "Rilmani", 1)
	WD_SetProperty("Unlock Topic", "RilmaniLanguage", 1)
	WD_SetProperty("Unlock Topic", "Runestone", 1)
	WD_SetProperty("Unlock Topic", "StillPlane", 1)
	WD_SetProperty("Unlock Topic", "Voidwalker", 1)

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] There's a few things you could help clear up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: What would you like to know?[BLOCK][CLEAR]") ]])
	
	--Activate topics mode once the dialogue is complete.
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Septima") ]])

--End the chapter.
elseif(sTopicName == "Training") then

	--Restart the music.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "NixNedar") ]])
    
    --Chapter Complete!
    VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N", 1.0)

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Let's do this thing.[SOFTBLOCK] What do we do first?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: First, we will teach you sword techniques, and the way of the void.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: You have natural strength and grace.[SOFTBLOCK] With my teaching, you will master the Rilmani form of combat.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Your room is right over here.[SOFTBLOCK] Please, get comfortable.[SOFTBLOCK] I will see about getting you some fresh clothes.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "And so, Mei, Voidwalker, bearer of courage, would train with Septima to learn the ways of the Rilmani.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her story does not end here, but this chapter does.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "What will happen to her?[SOFTBLOCK] And what of the other five?[SOFTBLOCK] Sanya, Jeanne, Lotta, Christine, Talia?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chapters 2-5 unlocked...[SOFTBLOCK] Is what I'd say if they were complete.[SOFTBLOCK] In this version, Chapter 5 is playable.") ]])
	fnCutsceneBlocker()
	
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
    --Execute the cleaner script.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Cleanup/000 Entry Point.lua") ]])
	fnCutsceneBlocker()
    
    --Return to the "Nowhere" map.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
    fnCutsceneBlocker()

--Go wander a bit.
elseif(sTopicName == "Leave") then

	--Restart the music.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "NixNedar") ]])

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not done exploring yet.[SOFTBLOCK] Might as well get to know the place if I'm going to stay here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Certainly.[SOFTBLOCK] If you've not done so, please speak to Maram.[SOFTBLOCK] She has been waiting to see you.") ]])
	fnCutsceneBlocker()
end
