--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[VOICE|Slime] I call this piece 'Helmet I Found and Filled With Slime'.[SOFTBLOCK] What do you think?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] ...[SOFTBLOCK] Breathtaking?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[VOICE|Slime] Thanks![SOFTBLOCK] I worked really hard on it!") ]])
end
