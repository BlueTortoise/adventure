--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[VOICE|Slime] Have you met my jelly-belly buddies?[SOFTBLOCK] Aren't they so cute?![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[VOICE|Slime] Lots of slimes keep jellyfish as pets, but I got mine on display so everyone can see them![SOFTBLOCK] Look, this little guy is happy to see you!") ]])
end
