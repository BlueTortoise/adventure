--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[VOICE|Slime] Hi again![SOFTBLOCK] Want to go back to the river?[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()
	
--Go back.
elseif(sTopicName == "Yes") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[VOICE|Slime] All right![SOFTBLOCK] Follow me!") ]])
	fnCutsceneBlocker()
	
	--Transition to Evermoon East.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonE", "FORCEPOS:41.0x13.0x0") ]])
	fnCutsceneBlocker()
	
	--Wait.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Mei faces north.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face",  0, -1)
	DL_PopActiveObject()
	
--Stay.
elseif(sTopicName == "No") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[VOICE|Slime] Okay![SOFTBLOCK] Just let me know when you're ready!") ]])
	fnCutsceneBlocker()
end
