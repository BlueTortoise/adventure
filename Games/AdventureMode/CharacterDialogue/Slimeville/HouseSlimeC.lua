--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Common.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
	
	--If Mei is a slime:
	if(sMeiForm == "Slime") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Hey there, cutie![SOFTBLOCK] I love your cytoplasm![SOFTBLOCK] How do you get it so firm?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh...[SOFTBLOCK] Diet and exercise?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Why didn't I think of that![SOFTBLOCK] Duh![SOFTBLOCK] You're so smart!") ]])
	
	--If Mei is a human:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Hey, are you a human?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Yes?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Wow![SOFTBLOCK] A human visitor![SOFTBLOCK] We don't get those very often![SOFTBLOCK] Or, like, ever![SOFTBLOCK] I don't know the etiquette, TBH.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Would you like something to drink?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks\") ")
		fnCutsceneBlocker()
	
	end

--Drink something given to you by a slime. By the way, you're an idiot.
elseif(sTopicName == "Sure") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
	
	--Base.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Sure, why not?[BLOCK][CLEAR]") ]])
	
	--Florentina has a bit of a problem with this:
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Seriously, Mei?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Oh, come off it.[SOFTBLOCK] She seems really nice.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: I'm totally the nicest slime and junk![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] See?[SOFTBLOCK] Look at those puppy-dog eyes![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] If you get sick, it's not my problem.[BLOCK][CLEAR]") ]])
	end
	
	--Resume.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] *Glug*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Delicious![SOFTBLOCK] What was it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Good old plain water![SOFTBLOCK] It's all we slimes drink![BLOCK][CLEAR]") ]])
	
	--Florentina isn't going to eat any crow.
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] See, Florentina?[SOFTBLOCK] No need to be suspicious![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Oh yeah, of course it was water.[SOFTBLOCK] Thick, viscous, green water.[BLOCK][CLEAR]") ]])
	end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Oh, whoops.[SOFTBLOCK] I think I got some of my slime in there...[SOFTBLOCK] whoopsie doodle![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I feel...[SOFTBLOCK] kinda light headed...") ]])
	fnCutsceneBlocker()
	
	--Switch to scene mode.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[Voice|Mei] Oh no![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[Voice|Slime] I'm sorry![SOFTBLOCK] Here, let me help you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[Voice|Mei] You're just getting more slime on me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei began to panic, thrashing about as she tried to shake the slime off.[SOFTBLOCK] Her efforts were in vain, as the slime infection was coming from deep within her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The slime girl desperately padded at Mei, trying to wipe away what she could.[SOFTBLOCK] Everywhere she touched, more slime went.") ]])
	fnCutsceneBlocker()
	
	--Next scene.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[Voice|Mei] K-keep going...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The infection reached up to Mei's head.[SOFTBLOCK] Suddenly, the spread of the slime wasn't a problem.[SOFTBLOCK] In fact, it began to feel pleasurable...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[Voice|Slime] Oops![SOFTBLOCK] Oh that -[SOFTBLOCK] oh my![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[Voice|Mei] More slime...[SOFTBLOCK] rub me...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[Voice|Slime] Oh geez, I'm messing this up![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[Voice|Mei] More...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The slime took over Mei's body.[SOFTBLOCK] Her slime sister ran her goopy hands all over Mei's body, arousing her more and more with each passing moment.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She began to rub herself in tandem, swirling her own slime around.[SOFTBLOCK] The sensation was incredible, an orgasm more intense than any she had experienced as a human...") ]])
	fnCutsceneBlocker()
	
	--Finished.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Slime") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[Voice|Mei] Yeah...[SOFTBLOCK] yeah...[SOFTBLOCK] I'm feeling so much better...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She had become slime like the girl next to her.[SOFTBLOCK] Every inch of her body was now an erogenous zone, and she slid her slimy hands up and down her slimy membrane.[SOFTBLOCK] The squishing, slopping feeling brought her to climax as her slime sister withdrew.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The slime stepped back, head hung in shame.[SOFTBLOCK] Mei placed her slimy hand under the girl's chin and lifted her head up.[SOFTBLOCK] Mei smiled as broadly as she could, and brought solace to the poor slime girl.") ]])
	fnCutsceneBlocker()
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Ah geez I am sooooooo super sorry![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Whatever for?[SOFTBLOCK] I'm all slimy![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] And I got your smarty fruit in me![SOFTBLOCK] I'm a smarty slime![BLOCK][CLEAR]") ]])
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] ...[SOFTBLOCK] Dumbass.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] You're just jealous![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well, you can't be any dumber now that you're a slime.[SOFTBLOCK] So, no great loss there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thanks, Florentina![BLOCK][CLEAR]") ]])
	end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, thank you for this eye-opening experience.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: You're okay with this?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Sure![SOFTBLOCK] This is awesome![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] But I've got an adventure to get back to.[SOFTBLOCK] See you later!") ]])

--No thank you.
elseif(sTopicName == "No Thanks") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Erm, no thanks.[SOFTBLOCK] Not thirsty.[BLOCK][CLEAR]") ]])
	
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Oh, okay![SOFTBLOCK] See you later, human![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *You're smarter than I gave you credit for, Mei.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] *What?[SOFTBLOCK] I'm really not thirsty!*") ]])
	else	
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime: Oh, okay![SOFTBLOCK] See you later, human!") ]])
	end
end
