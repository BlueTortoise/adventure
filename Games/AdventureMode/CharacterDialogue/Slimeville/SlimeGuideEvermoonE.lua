--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)
	
--In all cases, mark the combat intro dialogues as complete.
VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables:
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMetSlimeGuide = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N")
	
	--If Florentina is present, set this flag to true:
	if(bIsFlorentinaPresent == true) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSawSmartSlimes", "N", 1.0)
	end
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
		
	--Common.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	
	--First time meeting the slime:
	if(iMetSlimeGuide == 0.0) then
		
		--Mei is a human:
		if(sMeiForm == "Human") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Hi hi hi there, human![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Oh my goodness![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Oh, you can put the sword away.[SOFTBLOCK] I'm not going to hurt you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] That remains to be seen.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I didn't know slimes could talk...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Hee hee![SOFTBLOCK] You're silly![SOFTBLOCK] Of course we can talk![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] But you gotta be super duper smart to talk, and most slimes are real ditzes![SOFTBLOCK] Tee hee hee![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Well, if you're not going to attack me then I guess it's all right...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Oooooohhhh![SOFTBLOCK] You know what'd be soooo super cool?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Okay I don't want to get in trouble so you have to promise to keep a secret.[SOFTBLOCK] Can you keep a secret?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Sure?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Cool![SOFTBLOCK] Cool![SOFTBLOCK] You seem like a really trustworthy human since you put that sword away![BLOCK][CLEAR]") ]])
			
			if(bIsFlorentinaPresent == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] We slimes have a triple-decker secret village where we all hang out and talk about stuff![SOFTBLOCK] Want to see it?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yes.[SOFTBLOCK] Yes we do.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] *You don't think it's a trap?*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] *Oh, probably.[SOFTBLOCK] But if it's real, I could make a bundle selling these slimes -[SOFTBLOCK] whatever it is that slimes buy.[SOFTBLOCK] Think about it!*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] *But is it worth the risk?*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] What are you silly-billies whispering about?[SOFTBLOCK] Do you want to see my secret slime town or what?[BLOCK]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] We slimes have a triple-decker secret village where we all hang out and talk about stuff![SOFTBLOCK] Want to see it?[BLOCK]") ]])
			end

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks\") ")
			fnCutsceneBlocker()
		
			--Set:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N", 1.0)
		
		--Mei is a slime:
		elseif(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Oooohh wow![SOFTBLOCK] I haven't seen you before![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Oh my goodness![SOFTBLOCK] You can talk!?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Uh, yeah.[SOFTBLOCK] You can talk too, can't you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] I thought I was the only one![SOFTBLOCK] Can we all talk?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Nope, hee hee![SOFTBLOCK] Only really smart slimes can talk![SOFTBLOCK] You gotta eat a lot of smarty fruits to get that far![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What's a smarty fruit?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Those are the big red ones that hang off of trees![SOFTBLOCK] You obviously ate a lot to be so eloquent![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Erm...[SOFTBLOCK] Of course I did![SOFTBLOCK] Doi![SOFTBLOCK] I was just making sure you knew![BLOCK][CLEAR]") ]])
			
			if(bIsFlorentinaPresent == true) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] *You didn't mention anything about smarty fruits to me!*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] *What makes you think I knew about this?[SOFTBLOCK] I thought slimes were all dumb![SOFTBLOCK] Present company included!*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] *Thanks a bunch...*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] What are you guys whispering about?[SOFTBLOCK] Can I whisper too?[SOFTBLOCK] Is it a secret?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] We were just discussing...[SOFTBLOCK] if you're super-cute or ultra-cute![BLOCK][CLEAR]") ]])
			end
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] You're, like, so nice![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Oh, does that mean you've never seen our village?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Want to see it?[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure Slime\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks Slime\") ")
			fnCutsceneBlocker()
		
			--Set:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N", 1.0)
		
		--Mei is anything else:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Heeyyyyy...[SOFTBLOCK] you're a human, aren't you?[SOFTBLOCK] You smell like one![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Oh my goodness![BLOCK][CLEAR]") ]])
			if(bIsFlorentinaPresent == true) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] You can talk?[BLOCK][CLEAR]") ]])
			end
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Oh don't look so surprised![SOFTBLOCK] Your costume is, like, super awesome.[SOFTBLOCK] I'm impressed![BLOCK][CLEAR]") ]])
			if(bIsFlorentinaPresent == true) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] *Costume?*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] *Just roll with it...*[BLOCK][CLEAR]") ]])
			end
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] But I don't think the other slimes will understand -[SOFTBLOCK] they're kinda dense, if you know what I mean![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] So ignoring the fact that you can talk...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Ooooh![SOFTBLOCK] I want to show you our village but I don't want to get in trouble![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] You should take off your costume![SOFTBLOCK] Go on, I won't peek.[SOFTBLOCK] I promise!") ]])
		
			--Set:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N", 1.0)
		
		end
		
	--Repeat visits:
	else
		
		--Mei is a human:
		if(sMeiForm == "Human") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Hey![SOFTBLOCK] Want to visit the slime village?[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure Repeat\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks Repeat\") ")
			fnCutsceneBlocker()
		
		--Mei is a slime:
		elseif(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Hi hi hi![SOFTBLOCK] Want to visit the slime village?[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure Repeat\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks Repeat\") ")
			fnCutsceneBlocker()
		
		--Mei is anything else:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Sorry, but you'll need to take off your cool costume if you want to visit the slime village!") ]])
		
		end
	
	end
	
elseif(sTopicName == "Sure") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, okay I guess.[SOFTBLOCK] You seem all right.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] But no sudden moves.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Sudden moves?[SOFTBLOCK] Have you never met a slime before?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Well, no, actually.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Oh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Well you're gonna love our cool village![SOFTBLOCK] Follow me!") ]])
	fnCutsceneBlocker()
	
	--Transition.
	local iSawSlimevilleIntro = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N")
	if(iSawSlimevilleIntro == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N", 1.0)
		fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonSlimeVillage", gsRoot .. "Chapter 1/Scenes/Slimeville_Entry/Scene_Begin.lua") ]])
	else
		fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonSlimeVillage", "FORCEPOS:22.0x39.5x0") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Mei faces north.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
	end
	
elseif(sTopicName == "Sure Slime") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Okay![SOFTBLOCK] Lead the way![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] This is so totally awesome![SOFTBLOCK] Follow me!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Transition.
	local iSawSlimevilleIntro = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N")
	if(iSawSlimevilleIntro == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N", 1.0)
		fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonSlimeVillage", gsRoot .. "Chapter 1/Scenes/Slimeville_Entry/Scene_Begin.lua") ]])
	else
		fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonSlimeVillage", "FORCEPOS:22.0x39.5x0") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Mei faces north.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
	end
	
elseif(sTopicName == "Sure Repeat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Okay![SOFTBLOCK] Follow me!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Transition.
	local iSawSlimevilleIntro = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N")
	if(iSawSlimevilleIntro == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N", 1.0)
		fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonSlimeVillage", gsRoot .. "Chapter 1/Scenes/Slimeville_Entry/Scene_Begin.lua") ]])
	else
		fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonSlimeVillage", "FORCEPOS:22.0x39.5x0") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Mei faces north.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
	end
	
elseif(sTopicName == "No Thanks") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I smell a trap...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] No thanks, Miss Slime.[SOFTBLOCK] Maybe some other time?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] Ohmygosh that rhymed![SOFTBLOCK] You're so cool![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] If you want to see it, I'll be here![SOFTBLOCK] Just holler!") ]])
	fnCutsceneBlocker()
	
elseif(sTopicName == "No Thanks Slime") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well I'm kinda busy at the moment.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] If you want to see it, I'll be here![SOFTBLOCK] Just holler!") ]])
	fnCutsceneBlocker()
	
elseif(sTopicName == "No Thanks Repeat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well I'm kinda busy at the moment.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slime:[E|Blue] If you want to see it, I'll be here![SOFTBLOCK] Just holler!") ]])
	fnCutsceneBlocker()
end
