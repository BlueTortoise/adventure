--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    
    if(iSXUpgradeQuest < 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] You're the human they found, right?[SOFTBLOCK] Sorry about that.[SOFTBLOCK] We've had to double the number of traps due to all the monsters in the mines recently.") ]])
    elseif(iSXUpgradeQuest == 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Out for a night on the town?[SOFTBLOCK] Have fun!") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] You were a golem all along...[SOFTBLOCK] Well, I suppose we'll take allies where we can find them.") ]])
    end
    
    --Common.
	fnCutsceneBlocker()
end