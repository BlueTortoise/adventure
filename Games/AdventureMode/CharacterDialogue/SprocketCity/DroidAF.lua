--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    
    if(iSXUpgradeQuest < 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Were you out in the mines?[SOFTBLOCK] I've heard there's a lot of strange monsters prowling around.[SOFTBLOCK] Be careful if you're going out.") ]])
    elseif(iSXUpgradeQuest == 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Nice work on the trap lines, SX-399.[SOFTBLOCK] You're doing the city proud.") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] So now the golems are helping us with the monsters in the mines?[SOFTBLOCK] Strange times.") ]])
    end
    
    --Common.
	fnCutsceneBlocker()
end