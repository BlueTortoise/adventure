--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    
    if(iSXUpgradeQuest < 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Oh, the human.[SOFTBLOCK] Sorry about the trap...") ]])
    elseif(iSXUpgradeQuest == 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Hey, SX-399![SOFTBLOCK] If you want, I think one of the computers had some photos of Darkmatters and kittens and puppies!") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] 771852 huh.[SOFTBLOCK] Well, if JX-101 says you're all right, you're all right.") ]])
    end
    
    --Common.
	fnCutsceneBlocker()
end