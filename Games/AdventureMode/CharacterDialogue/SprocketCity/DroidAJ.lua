--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    io.write("Value is " .. iSXUpgradeQuest .. "\n")
    
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    
    if(iSXUpgradeQuest < 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "AJ-99:[VOICE|Steam Droid] There's a secret path just to the east of the drop trap.[SOFTBLOCK] I'd hate to have to fish you out of the canyon again![SOFTBLOCK] Ha ha!") ]])
    elseif(iSXUpgradeQuest == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "AJ-99:[VOICE|Steam Droid] Oh, SX-399, I checked over the springs on the spike trap earlier.[SOFTBLOCK] Nice work with the torsion calibration, couldn't have done it better myself.") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "AJ-99:[VOICE|Steam Droid] I guess it's reassuring that our traps managed to stop a Command Unit, right?[SOFTBLOCK] Unfortunately, it happened to be the nicest Command Unit we stopped...") ]])
    end
    
    --Common.
	fnCutsceneBlocker()
end