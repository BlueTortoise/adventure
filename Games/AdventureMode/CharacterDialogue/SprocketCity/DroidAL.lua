--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iFixedSensor= VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedSensor", "N")
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	
    --Variables.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    
    --Reputation time.
    if(iSXUpgradeQuest < 2.0) then
        
        --Hasn't fixed it yet.
        if(iFixedSensor == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] I found this busted sensor out in the mines, and I've been trying to fix it.[SOFTBLOCK] No luck so far.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I guess nobody is going to know I'm a repair unit.[SOFTBLOCK] Let's change that!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I'm pretty handy with machines.[SOFTBLOCK] Mind if I take a look?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Knock yourself out.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Uh huh, busted transistor.[SOFTBLOCK] Luckily, the RM-75 modules have a spare set on the back panel.[SOFTBLOCK] See?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Oh, that's what that thing was?[SOFTBLOCK] They put spare parts inside the sensor?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] The transistors tend to be the first thing to go.[SOFTBLOCK] Particularly if you drop the sensor.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] All fixed![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Amazing![SOFTBLOCK] I've never seen a human as good as you are with repairs![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Well I'm not a - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I mean, it's a talent.[SOFTBLOCK] I've always been like this.[SOFTBLOCK] Some people read novels, I read repair manuals.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] I'll be sure to tell JX-101 you got this thing working.[SOFTBLOCK] I'm sure there's a lot of stuff you could help fix around here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Gained 25 Reputation!)") ]])
            fnCutsceneBlocker()
            
            VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedSensor", "N", 1.0)
            local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 25)
        
        --Already fixed it.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Thanks a lot![SOFTBLOCK] I'll tell JX-101 that you got this thing fixed![SOFTBLOCK] Thanks again!") ]])
            fnCutsceneBlocker()
        
        end
    elseif(iSXUpgradeQuest == 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Are you two out having fun?[SOFTBLOCK] Maybe you should play a game of Penkak.[SOFTBLOCK] One day you're going to beat your mother, and I want to be there to see it.") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] It figures you'd be a golem with repair skills like those.[SOFTBLOCK] I wonder if we can upgrade the other Steam Droids...") ]])
    end
    
    --Common.
	fnCutsceneBlocker()
	
end