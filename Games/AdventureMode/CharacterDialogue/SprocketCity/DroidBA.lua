--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iSaw319 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw319", "N")
	
	--First time.
	if(iSaw319 == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw319", "N", 1.0)
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 25)
		
		--Dialogue.
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] I swear I had a 3-19 Inserter around here somewhere...[SOFTBLOCK] but I can't find it anywhere![SOFTBLOCK] Argh![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] A 3-19 Inserter...[SOFTBLOCK] like the one in your hand?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] I'm such a lug nut.[SOFTBLOCK] I guess my circuits are getting frazzled.[SOFTBLOCK] Thanks, human.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Don't mention it.[SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Gained 25 Reputation!)") ]])
        fnCutsceneBlocker()
        
	--Repeats.
	else
		
		--Dialogue.
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] I'm trying to take inventory of all this junk. It's going to take me all week at this rate.") ]])
		fnCutsceneBlocker()
	
	
	end
end