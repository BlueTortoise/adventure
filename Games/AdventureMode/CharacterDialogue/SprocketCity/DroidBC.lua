--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Variables.
	local iSteamDroidReputation  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
	local iPlanHatched = VM_GetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N")
	if(iPlanHatched >= 1.0 and iSteamDroidReputation >= 500) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] First a Command Unit, and now a Lord Unit.[SOFTBLOCK] We've been getting a lot of prisoners lately...") ]])
        fnCutsceneBlocker()
	
    else
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] That Command Unit has been in system standby since she got here.[SOFTBLOCK] I'm not sure she'll ever wake up...") ]])
        fnCutsceneBlocker()
    end
end