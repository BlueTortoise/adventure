--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    
    if(iSXUpgradeQuest < 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Ah, is that Christine?[SOFTBLOCK] I can't see anything, I'm afraid.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Yes, it's me.[SOFTBLOCK] Have your optical receptors not been repaired yet?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] TT-233 said it will be a little while yet, apparently we're going to make new ones based on some schematics?[SOFTBLOCK] But, at least the nightmare is over...") ]])
    elseif(iSXUpgradeQuest == 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Hello there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Ah, is that Christine?[SOFTBLOCK] I can't see anything, I'm afraid.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[VOICE|JX-101] No no, this is SX-399.[SOFTBLOCK] Do you remember?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Ah, your sweet cherry.[SOFTBLOCK] Glad to see -[SOFTBLOCK] or rather, hear -[SOFTBLOCK] you again.[SOFTBLOCK] But you sound an awful lot like Christine...") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Hello, Christine.[SOFTBLOCK] My optical receptors are still disabled, but I can tell from your walk that you are...[SOFTBLOCK] happy.[SOFTBLOCK] I'm happy for you, whatever the cause.") ]])
    end
    
    --Common.
	fnCutsceneBlocker()
end