--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    
    if(iSXUpgradeQuest < 2.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Good show, 771852![SOFTBLOCK] Everything is turning out like it did last time, but slightly different![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why do you keep acting like you know what's going to happen?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Emptiness![SOFTBLOCK] You're not empty right now, but you will be soon.[SOFTBLOCK] Or maybe you were earlier, it's hard to keep track.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And what does that mean?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] There are periods where we cease to exist, and then suddenly exist again.[SOFTBLOCK] Of course we don't know about them because we're not there to see them, but they happen.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] And they happen again and again and again.[SOFTBLOCK] You see how much we can learn when we open ourselves to new ways of thinking?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's just crazed babble...") ]])
        fnCutsceneBlocker()
    elseif(iSXUpgradeQuest == 2.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh, oh![SOFTBLOCK] Very clever, very clever, makeup and sawdust.[SOFTBLOCK] But I see through it.[SOFTBLOCK] It's all clear![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Do not trouble yourself with this one, sweetie. She's not worth the effort.") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] It does not matter how many sides it has, most of the space is, in fact, empty.[SOFTBLOCK] The pressure of emptiness is so much higher than that of fullness, you see, so it holds its shape even in the void.[SOFTBLOCK] Ah ah.[SOFTBLOCK] No, that stings!") ]])
    end
    
    --Common.
	fnCutsceneBlocker()
	
end