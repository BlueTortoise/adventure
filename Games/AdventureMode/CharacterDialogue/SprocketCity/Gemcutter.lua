--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
    local iMetGemcutter = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N")
    
    --Common.
    TA_SetProperty("Face Character", "PlayerEntity")
	
	--Haven't met her yet.
	if(iMetGemcutter == 0.0) then
        
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Ah ha![SOFTBLOCK] Another daring adventurer seeks out the exquisite VN-19![SOFTBLOCK] Well met, comrade![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] You are, at current, skulking about in the shadows of Regulus City.[SOFTBLOCK] Darting between maverick robots, fighting when you cannot run.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] With daring and panache, you search for the rumoured gemcutter of note.[SOFTBLOCK] VN-19![SOFTBLOCK] There she is![SOFTBLOCK] We've found her![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Yes, yes,[SOFTBLOCK] so exciting,[SOFTBLOCK] I know![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Now, I care not for your allegiance.[SOFTBLOCK] Administrator, Maverick, Rebel...[SOFTBLOCK] There is only one reason you'd seek me out, and that is a desire to have the finest gems cut.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] ...[SOFTBLOCK] Right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, yes?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Superb![SOFTBLOCK] Well, I assume you know how this works.[SOFTBLOCK] Shall we get to it then?[BLOCK]") ]])
	
	--Repeats.
	else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] You return![SOFTBLOCK] What services might this artisan provide?[BLOCK]") ]])
	end

    --Decision setup.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Can you cut some gems?\", " .. sDecisionScript .. ", \"Gemcutting\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Tell me about yourself.\",  " .. sDecisionScript .. ", \"Talk\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I'm good, thanks.\",  " .. sDecisionScript .. ", \"Bye\") ")
    fnCutsceneBlocker()

--Activate gemcutting mode.
elseif(sTopicName == "Gemcutting") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Splendiferous![SOFTBLOCK] Let's get started!") ]])

	--Run the shop.
	fnCutsceneInstruction([[ AM_SetShopProperty("Show", "VN-19's Gem Shop", gsRoot .. "CharacterDialogue/SprocketCity/GemShopSetup.lua", gsRoot .. "CharacterDialogue/SprocketCity/GemShopTeardown.lua") ]])
	fnCutsceneBlocker()

--Talk.
elseif(sTopicName == "Talk") then
	
	--Dialogue.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Could you tell me a bit more about yourself?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Tell you...[SOFTBLOCK] about me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Whaddya wanna know about me fer?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well...[SOFTBLOCK] I just...[SOFTBLOCK] want to know more. You seem nice enough.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Ain't nobody gives a fig about me, toutse.[SOFTBLOCK] They want the gems.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] And I am happy to provide![SOFTBLOCK] Ah ha![SOFTBLOCK] There is sparingly little to say about me, but so much to say about gems![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Would you like to hear about how I learned to cut them?[SOFTBLOCK] You see, my mentor, Ali- [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But, gems aren't everything.[SOFTBLOCK] What are your hobbies?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Cutting gems.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] What did you want to be when you were growing up?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] A gemcutter.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What's 2+2?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Three Adamantite Flakes.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I think her circuits may be fried.[SOFTBLOCK] Or, she is an obsessive.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In any case, gems provide improvements to our combat potency.[SOFTBLOCK] The Administration overlooks their effects, as they cannot be mass-produced.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If she wants to cut them for us, let her.[SOFTBLOCK] It is to both our advantages.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay, fine then...") ]])

--Goodbye.
elseif(sTopicName == "Bye") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Next time.[SOFTBLOCK] See you later.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] The lure of my skills will draw you back here as it does to all others.[SOFTBLOCK] We will meet again!") ]])
	
end