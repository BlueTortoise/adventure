--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iMetJX101 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N")
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
	
	--First meeting.
	if(iMetJX101 == 0.0) then
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Ah, the human![SOFTBLOCK] I see your legs have been repaired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes, I feel much better now.[SOFTBLOCK] And you can call me Christine.[SOFTBLOCK] Certainly more polite than 'the human'![SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Well, as you already seemed to know, my name is JX-101.[SOFTBLOCK] I'm the leader of the Fist of Tomorrow.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: What you see around you is Sprocket City, one of the largest Steam Droid settlements on Regulus.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Fist of Tomorrow?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Do you like the name?[SOFTBLOCK] We used to be called Fist of the Future, but I think Tomorrow sounds better.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: We're pretty much the only group who are focused on the future of the Steam Droids, you see.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm afraid I'm not terribly familiar with your situation.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Hmm, just how did you get down here, anyway?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] As I said, I woke up in an airlock someplace.[SOFTBLOCK] After a lot of running and fighting with robots, I ended up in the mines.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And everywhere I went, I was being followed by that doll-like girl.[SOFTBLOCK] I think her name was 55 or something?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Heh, just true enough not to get me in trouble!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Ah, you were probably a victim of the golem abduction teams.[SOFTBLOCK] They send squads to Pandemonium to abduct humans and convert them to golems.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: If you had not escaped when you did, you would likely be one of their robotic slaves.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Being a robot?[SOFTBLOCK] Sounds awful![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] No offense...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: I don't mind.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: I suppose I can trust you, though not with any sensitive secrets.[SOFTBLOCK] You could still be converted if you were captured again, and I'd rather you didn't know anything compromising.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: I'll be frank, we're in trouble.[SOFTBLOCK] Big trouble.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Recently, strange creatures have been flooding into the mines.[SOFTBLOCK] We were already having difficulty locating enough spare parts and supplies to get by, but now it's extremely dangerous to leave the settlement.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Fist of Tomorrow is fighting for the future, but we're struggling to survive the present.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I see.[SOFTBLOCK] Is there anything I can do to help?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: That depends.[SOFTBLOCK] What sort of skills do you have?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'd say I'm pretty handy in a scrap.[SOFTBLOCK] I found this spear a while ago and I've gotten pretty good at using it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm also pretty good with machines.[SOFTBLOCK] I love to fix things and figure them out.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Very valuable skillsets...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Christine, if you have nowhere else to go, you may stay here.[SOFTBLOCK] We try to harbor any escaped humans we find, but resources are thin.[SOFTBLOCK] If you wish to stay, you must make yourself useful.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Since you consider yourself a fighter, I will consider it a personal favour if you would venture into the mines and locate spare parts for us.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All I have to do is not fall into any of your traps.[SOFTBLOCK] I was doing fine until then![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Yes, I apologize for that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Speaking of, that Command Unit that was chasing you is in the lockup at the north end of the city.[SOFTBLOCK] We're not quite sure what to do with her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Normally I'd have her scrapped, but if the golems find out we've captured her, they will retaliate...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Great, a political problem.[SOFTBLOCK] I better go talk with 55.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Thanks for your help, JX-101.[SOFTBLOCK] I'll see about making myself useful.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Good luck, Christine.[SOFTBLOCK] And be careful, the mines are very dangerous.") ]])
		fnCutsceneBlocker()
		
		VM_SetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N", 1.0)
		
	--All subsequent dialogues.
	else
		
		--Get reputation.
		local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
		local sString = "WD_SetProperty(\"Append\", \"JX-101: Well met, Christine.[SOFTBLOCK] What would you like to discuss? (Reputation:: " .. iSteamDroidReputation .. ")[BLOCK]\")"
        if(iCompletedSprocketCity == 1.0) then
            sString = "WD_SetProperty(\"Append\", \"JX-101: Well met, Christine.[SOFTBLOCK] What would you like to discuss?[BLOCK]\")"
        end
		
		--Dialogue.
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction(sString)
		
		--Variables.
		local iAskedAboutHelpingDroids = VM_GetVar("Root/Variables/Chapter5/Scenes/iAskedAboutHelpingDroids", "N")
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		
		--Asking JX-101 about helping out:
		if(iAskedAboutHelpingDroids == 0.0) then
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"How can I help?\", " .. sDecisionScript .. ", \"Help\") ")
		
		--Already asked, so put up the special options.
		else
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Turn In Items\", " .. sDecisionScript .. ", \"Items\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Special Assignments\", " .. sDecisionScript .. ", \"Special\") ")
			if(false) then fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"(Debug) +50 Rep\", " .. sDecisionScript .. ", \"Debug\") ") end
            
            --Additional options for mines subquests.
            local iMineAReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineAReward", "N")
            local iMineBReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBReward", "N")
            local iMineCReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineCReward", "N")
            local iMineDReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDReward", "N")
            
            --Variables.
            local iMineASmelters = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineASmelters", "N")
            local iMineBGotAmmo = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N")
            local iMineCFoundMemento = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineCFoundMemento", "N")
            local iMineDArtSupplies = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDArtSupplies", "N")
            local iMineDTerminal = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDTerminal", "N")
            local iMineDCrate = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDCrate", "N")
            
            --Reward for Mines A:
            if(iMineASmelters == 1.0 and iMineAReward == 0.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Abrissite\", " .. sDecisionScript .. ", \"Abrissite\") ")
            end
            
            --Reward for Mines B:
            if(iMineBGotAmmo == 2.0 and iMineBReward == 0.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sleeping Furiously\", " .. sDecisionScript .. ", \"Sleeping\") ")
            end
            
            --Reward for Mines C:
            if(iMineCFoundMemento == 1.0 and iMineCReward == 0.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Unicorn Memento\", " .. sDecisionScript .. ", \"Memento\") ")
            end
            
            --Reward for Mines D:
            if(iMineDArtSupplies == 2.0 and iMineDTerminal == 2.0 and iMineDCrate == 2.0 and iMineDReward == 0.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"PP-81\", " .. sDecisionScript .. ", \"ArtAppreciation\") ")
            end
		end
		
		--Last option is always "Goodbye".
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\",  " .. sDecisionScript .. ", \"Goodbye\") ")
		fnCutsceneBlocker()
	end

--Ask JX-101 about helping the Steam Droids.
elseif(sTopicName == "Help") then
	VM_SetVar("Root/Variables/Chapter5/Scenes/iAskedAboutHelpingDroids", "N", 1.0)
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] What can I do to help Sprocket City?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] To be specific, we need materials and tools.[SOFTBLOCK] If you find any Bent Tools or Recycleable Junk we can make use of.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I will give you 4 Reputation for each Junk and 6 for each Bent Tools.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] The citizens of Sprocket City could also likely use your assistance, as you claim to be mechanically minded.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I've also got a number of patrols in the mines that aren't reporting in, or areas worth investigating.[SOFTBLOCK] Sending out search parties is beyond our current capability.[SOFTBLOCK] Any assistance rendered is appreciated.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] We also may have special tasks.[SOFTBLOCK] Ask me about them.[SOFTBLOCK] Secrecy is important, so do not discuss the matters openly.[SOFTBLOCK] Even with other members of Fist of Tomorrow.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why wouldn't you tell them what you're doing?[SOFTBLOCK] Do you not trust them?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] With my life.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] But, if captured, the Golems have ways...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Say no more.[SOFTBLOCK] I understand.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You will need to develop a reputation before I would consider giving a special assignment out, you understand.[SOFTBLOCK] Support the city and we will do what we can to support you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Gotcha...") ]])

--Turning in reputation items.
elseif(sTopicName == "Items") then

	--Common.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])

	--Get how many items we have.
	local iJunkCount  = AdInv_GetProperty("Item Count", "Recycleable Junk")
	local iToolsCount = AdInv_GetProperty("Item Count", "Bent Tools")
	local iReputationGained = (iJunkCount * 4) + (iToolsCount * 6)
			
	--Remove the parts from the inventory.
	for i = 1, iJunkCount, 1 do
		AdInv_SetProperty("Remove Item", "Recycleable Junk")
	end
	for i = 1, iToolsCount, 1 do
		AdInv_SetProperty("Remove Item", "Bent Tools")
	end
	
    --Variables.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        
        --Add reputation.
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + iReputationGained)
        
        --Nothing to turn in.
        if(iJunkCount < 1 and iToolsCount < 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I hate to be a bother, but what sorts of items were you in need of again?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Specifically, Bent Tools (6 Rep) or Recycleable Junk (4 Rep).[SOFTBLOCK] I know it may not seem useful, but we are very resourceful.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Even *bent* tools?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Life as a Steam Droid is three parts ingenuity and one part compromise...") ]])
        
        --Only junk.
        elseif(iJunkCount >= 1 and iToolsCount < 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I found some junk that looks like it might be useful.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] [SOUND|World|TakeItem]Superb![SOFTBLOCK] I'll get this to my quartermaster immediately.[BLOCK][CLEAR]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " reputation.)\")"
            fnCutsceneInstruction(sString)
            
        --Only tools.
        elseif(iJunkCount < 1 and iToolsCount >= 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You might be able to fix these tools I found.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] [SOUND|World|TakeItem]We could get a few uses out of these, yes.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " reputation.)\")"
            fnCutsceneInstruction(sString)
        
        --Both.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tools and some junk...[SOFTBLOCK] Is this helpful?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] [SOUND|World|TakeItem]Excellent, this should alleviate our supply crisis...[SOFTBLOCK] if only for a few moments...[BLOCK][CLEAR]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " reputation.)\")"
            fnCutsceneInstruction(sString)
            
        end
        
    --After completing the quest chain...
    else
    
        --Add work credits.
        iReputationGained = iReputationGained / 2
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + iReputationGained)
        
        --Nothing to turn in.
        if(iJunkCount < 1 and iToolsCount < 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I hate to be a bother, but what sorts of items were you in need of again?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Specifically, Bent Tools (2 credits) or Recycleable Junk (2 credits).[SOFTBLOCK] I know it may not seem useful, but we are very resourceful.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Even *bent* tools?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Life as a Steam Droid is three parts ingenuity and one part compromise...") ]])
        
        --Only junk.
        elseif(iJunkCount >= 1 and iToolsCount < 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I found some junk that looks like it might be useful.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] [SOUND|World|TakeItem]Superb![SOFTBLOCK] I'll get this to my quartermaster immediately.[BLOCK][CLEAR]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " credits.)\")"
            fnCutsceneInstruction(sString)
            
        --Only tools.
        elseif(iJunkCount < 1 and iToolsCount >= 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You might be able to fix these tools I found.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] [SOUND|World|TakeItem]We could get a few uses out of these, yes.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " credits.)\")"
            fnCutsceneInstruction(sString)
        
        --Both.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tools and some junk...[SOFTBLOCK] Is this helpful?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] [SOUND|World|TakeItem]War funding.[SOFTBLOCK] We'll make use of these.[BLOCK][CLEAR]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " credits.)\")"
            fnCutsceneInstruction(sString)
            
        end

    end

--Special assignments.
elseif(sTopicName == "Special") then

	--Variables.
	local iSteamDroidReputation  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
	local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
	local iSteamDroid250RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
	local iSteamDroid500RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N")

	--Common.
	if(iSteamDroid250RepState ~= 2.0) then
		WD_SetProperty("Hide")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	end

	--[X20 Transducer Quest]
	--First quest: Assignment.
	if(iSteamDroid100RepState == 0.0) then

		--Not enough reputation:
		if(iSteamDroidReputation < 100) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is there anything special you needed me to do?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] At the moment, no.[SOFTBLOCK] I'll let you know if anything comes up.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Next quest requires 100 Reputation.)") ]])
			
		--Give the quest:
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You mentioned special assignments.[SOFTBLOCK] Was there anything you needed?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Considering your capabilities, yes.[SOFTBLOCK] I think there is.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Here, take this.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] [SOUND|World|TakeItem]Er, if I don't miss my guess, that is a Series-X20 Transducer, right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Your knowledge of electronics is uncanny.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's a talent of mine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Having thousands of blueprints and manuals downloaded into your mechanized brain doesn't hurt either...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] We scavenged this device from the mines.[SOFTBLOCK] We've got only a few, and they're vital for monitoring steam pressure remotely.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] In locations where we can't look at the pipes directly, we use these to identify potential leaks.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This one's broken...[SOFTBLOCK] but I can't say how.[SOFTBLOCK] I'd need a micro-wave scanner.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Do you think you can fix it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Let me hold on to it, and I'll see what I can do.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I must admit it is most likely that you are the most capable repair unit here.[SOFTBLOCK] If you can fix it, Sprocket City will be in your debt.") ]])
		
		end

	--Received the quest, haven't completed it yet.
	elseif(iSteamDroid100RepState == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] How goes the X20 Transducer, Christine?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Still working on it, I'll get back to you when I've figured something out.") ]])

	--Quest completed.
	elseif(iSteamDroid100RepState == 2.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 3.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You've got a smile on your face.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] One Series-X20 Transducer, fixed and ready to go![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Capital![SOFTBLOCK] How did you get it fixed?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (I hope I'm as good at lying as I am at fixing things...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I snuck into the workshop at the top of the mines and used their scanner.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You what!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It was risky, but it was the only way.[SOFTBLOCK] I put everything back, the Golems won't suspect a thing.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Let us hope not.[SOFTBLOCK] The Golems will take any excuse to exterminate us, I assure you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Still, this transducer will be of great assistance.[SOFTBLOCK] Thank you, Christine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Gained 50 Reputation!)") ]])
		
		--Add reputation.
		local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 50)

	--[Second Quest: Meet SX-399]
	--Quest assignment.
	elseif(iSteamDroid250RepState == 0.0) then

		--Not enough reputation:
		if(iSteamDroidReputation < 250) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is there anything special you needed me to do?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] At the moment, no.[SOFTBLOCK] But, I've got my eye out.[SOFTBLOCK] Return later.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Next quest requires 250 Reputation.)") ]])
			
		--Give the quest:
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Have any other special assignments come up?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] *Sigh* No, but...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Nobody starts with a sigh unless it's serious.[SOFTBLOCK] Do you want to talk about it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I'm sorry, Christine.[SOFTBLOCK] It's a personal matter.[SOFTBLOCK] I don't want to involve you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Let me guess...[SOFTBLOCK] It's about your daughter?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] That, my friend, is too accurate to be a guess.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Before all this, I was an English teacher.[SOFTBLOCK] I've dealt with many a teenager and frustrated parent.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Is it so universal?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Everything changes but the sigh.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] ...[SOFTBLOCK] We had an argument recently, I'm afraid.[SOFTBLOCK] She hasn't come out of her room since.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Naturally, you want me to go talk to her.[SOFTBLOCK] Understood.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] This isn't an official assignment.[SOFTBLOCK] Don't be so formal.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Honestly, I'd rather be shooting at rogue servitors right about now...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'll take care of it.[SOFTBLOCK] Don't worry.[SOFTBLOCK] And if you're like all those other parents, you're going to worry nonetheless.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Your predictive abilities are uncanny...") ]])
		
		end

	--Repeats.
	elseif(iSteamDroid250RepState == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I love my daughter, Christine.[SOFTBLOCK] She is my whole world.[SOFTBLOCK] Is it bad that we don't get along?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No, it's part of growing up.[SOFTBLOCK] I'll talk to her and see what I can do.") ]])

	--Repeats.
	elseif(iSteamDroid250RepState == 2.0) then
	
		--Flags.
		WD_SetProperty("Hide")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N", 3.0)
		
		--Movement.
		fnCutsceneMove("Christine", 53.25, 23.50)
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Steam") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Mother, I did not mean to shout.[SOFTBLOCK] But - [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're all friends here, dear.[SOFTBLOCK] Go on.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I am trying my best, but I do not want Sprocket City to be my cage.[SOFTBLOCK] I have a right to contribute to society.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I suppose you do, don't you?[SOFTBLOCK] But if anything happened to you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] We've had this conversation before.[SOFTBLOCK] I already know how it ends.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Is there anything SX-399 can do around town to help?[SOFTBLOCK] Anything at all?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Please?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Perhaps you could monitor and reset the traps around the entrances...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Really?[SOFTBLOCK] Trap duty?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] But you have to be back before 19::00 and I'll have the sentries checking up on you and - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I won't let you down, mother![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] *kiss*") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--SX-399 Moves away.
		fnCutsceneMove("SX399", 53.25, 22.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneFace("JX101", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneMove("SX399", 56.25, 22.50)
		fnCutsceneFace("Christine", 1, -1)
		fnCutsceneFace("JX101", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneMove("SX399", 56.25, 16.50)
		fnCutsceneMove("SX399", 53.25, 16.50)
		fnCutsceneFace("SX399", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "FistOfTomorrowHQDoor") ]])
		fnCutsceneBlocker()
		fnCutsceneMove("SX399", 53.25, 18.50)
		fnCutsceneMove("SX399", 37.25, 18.50)
		fnCutsceneTeleport("SX399", -100, -100)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I admit it has been some time since she kissed me on the cheek like that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She's ready to be her own person.[SOFTBLOCK] She needs the space to make that a reality.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Her life is moving into its next stage.[SOFTBLOCK] It won't be the same as before, but different isn't the same as bad.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] ...[SOFTBLOCK] Has she told you of her special situation?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] In passing, but not detail.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I see...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Please do not trouble yourself on this matter.[SOFTBLOCK] I'm happy enough that she's talking to me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Happy to help![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Gained 50 Reputation!)") ]])
		fnCutsceneBlocker()
		
		--Add reputation.
		local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 50)
	
	--[Black Site Quest]
	--Quest assignment.
	elseif(iSteamDroid500RepState == 0.0) then

        --Variables:
		local iSXMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
		local iSpokeWithTT233 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N")

		--Hasn't done the planning phase yet.
		local iPlanHatched = VM_GetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N")
		if(iPlanHatched == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101, have you any other special tasks for me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Afraid not.[SOFTBLOCK] You've done well so far.[SOFTBLOCK] Keep it up.[BLOCK][CLEAR]") ]])
            if(iSXMet55 == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (I should go talk with SX-399 and 55 to figure out our plan...)") ]])
            elseif(iSXMet55 == 1.0 and iSpokeWithTT233 == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (I should speak with TT-233 about SX-399's condition...)") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (I should go talk with SX-399 and 55, and let them know what I found out.)") ]])
            end

		--Not enough reputation:
		elseif(iSteamDroidReputation < 500) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101, have you any other special tasks for me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Afraid not.[SOFTBLOCK] You've done well so far.[SOFTBLOCK] Keep it up.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Next quest requires 500 Reputation.)") ]])
			
		--Give the quest:
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Have any other special assignments come up?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Hmmmm...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Yes, yes, I suppose I must call upon you again...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Now, Christine, I know you're asking to help.[SOFTBLOCK] I know you've given Sprocket City so much...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] And I want to emphasize that you can say no.[SOFTBLOCK] What I am about to ask you is extremely dangerous.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (I guess 55 got the information to her...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Whatever it is, I'm ready.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] The administration of Regulus City routinely captures and imprisons those who would oppose it.[SOFTBLOCK] This includes us, escaped humans, or maverick golems.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Because they must maintain the facade of an idyllic robotic paradise, such...[SOFTBLOCK] undesirables...[SOFTBLOCK] are imprisoned in locations kept secret even from their own population.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] One such location has recently fallen into our hands.[SOFTBLOCK] We found a badly beaten Lord Unit who must have gotten lost in the mines.[SOFTBLOCK] Her PDU pointed us straight to it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Oh, very subtle, 55...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What of the Lord Unit?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Why do you concern yourself so with the lives of our enemies?[SOFTBLOCK] This Lord Unit, had she the chance, would kidnap you and transform you into her personal bootlicker.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Enemy or not, every death is a tragedy.[SOFTBLOCK] Please don't retire her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] We will not stoop to the level of the golems.[SOFTBLOCK] We do not retire our prisoners unless they pose an immediate danger, as the Command Unit you encountered evidently did.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Now, as to the detention site, we do not believe the golems know we know of it.[SOFTBLOCK] With the advantage of surprise, I believe we can liberate the prisoners within.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] The site is located at a disused tram repair station a few kilometers from here.[SOFTBLOCK] I will update your PDU with a map.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (...[SOFTBLOCK] Looks like this is under Sector 12.[SOFTBLOCK] I hope 55 found an old workshop.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] When do we leave?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] We will not be moving together, we will split into small groups and meet up near the site.[SOFTBLOCK] This will reduce the likelihood of the golems sighting our raiding party and mobilizing a counterattack.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You should be able to make your way along the rail track just outside Sprocket City.[SOFTBLOCK] Will you be all right getting there under your own power?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sure thing. I'll meet you there.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Christine...[SOFTBLOCK] Thank you for your sacrifice.[SOFTBLOCK] Few people, and even fewer humans, would be willing to risk themselves for the sake of another as you do.[SOFTBLOCK] Good hunting.") ]])
		
		end
	
	--Repeats.
	elseif(iSteamDroid500RepState == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I have sent the signal to marshall our forces.[SOFTBLOCK] I will be heading out momentarily myself.") ]])
	
	--[No More Quests]
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is there anything special you needed me to do?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Christine, I realize that you enjoy helping, but at this point I think Sprocket City can go no further in debt to you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] From the bottom of my power core, thank you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Though if you do find any additional equipment in the mines, we will still take it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] In fact, I think I can scrounge up work credits for you as a reward.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Work credits?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] My patrols periodically find credits chips that have been discarded.[SOFTBLOCK] We can't make much use of them, but perhaps you can.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Great![SOFTBLOCK] We could use the credits to purchase equipment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Exactly what I was thinking.[SOFTBLOCK] Use Regulus City's resources against them.[SOFTBLOCK] Good hunting, Christine.") ]])
	end

--Debug, adds +50 Rep.
elseif(sTopicName == "Debug") then
	WD_SetProperty("Hide")
	local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 50)

--[Mine Rewards Topics]
elseif(sTopicName == "Abrissite") then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineAReward", "N", 1.0)
	WD_SetProperty("Hide")
    fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Have you ever heard of abrissite, JX-101?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Rare mineral of some sort.[SOFTBLOCK] The golems used to mine it back when this was an active dig site.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] We don't have a lot of geologists in the Steam Droid population.[SOFTBLOCK] Most of them were upgraded ages ago.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I found a sample of the stuff.[SOFTBLOCK] It's this pink ore here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Apparently it can be smelted into a few useful metals.[SOFTBLOCK] My PDU has more.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] This pink rock is abrissite![SOFTBLOCK] Curses![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I never knew we could make use of it, I figured it was slag.[SOFTBLOCK] There's a dozen sites we could have been mining ourselves this whole time![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But didn't Regulus City mine the good ore veins?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Yes and no.[SOFTBLOCK] You sometimes see nodules of it exposed due to cavern collapses.[SOFTBLOCK] Plus we could use the lower-grade ores that the golems passed over. We're always in need of metals.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] And of course we've been sitting on it the whole time.[SOFTBLOCK] Knowledge is just as valuable as the ore itself.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Thank you greatly, Christine.[SOFTBLOCK] This will help immensely.[SOFTBLOCK] Sprocket City is in your debt.[BLOCK][CLEAR]") ]])
    
    --Rewards.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 75)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (+75 Reputation!)") ]])
    else
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 25)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (+25 Work Credits!)") ]])
    end
    
elseif(sTopicName == "Sleeping") then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBReward", "N", 2.0)
	WD_SetProperty("Hide")
    fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] What if I said 'Sleeping Furiously' to you, JX-101?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I'd say you were either a great linguist, or someone to whom we owe a debt of gratitude.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] It's a phrase we use to indicate someone helped us in the mines, but it also means we're not going to say who or how.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I'm not going to ask who you helped, how you helped them, or anything like that.[SOFTBLOCK] But you helped us, so Fist of Tomorrow will help you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Great![SOFTBLOCK] Thanks, JX-101![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] And since you've heard it, you've earned the right to use the phrase as well.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Oh, and another phrase.[SOFTBLOCK] 'Colorless Green Dreams'.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Does it mean the same thing?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] No.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] If someone has betrayed you and you want us to deal with them, give them that phrase.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I've never had to deal with someone like that myself, but suffice is to say it won't be a pleasant experience.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Now here's a reward for you.[SOFTBLOCK] Thanks again.[BLOCK][CLEAR]") ]])
    
    --Rewards.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 125)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (+125 Reputation!)") ]])
    else
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 60)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (+60 Work Credits!)") ]])
    end
    
elseif(sTopicName == "Memento") then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCReward", "N", 3.0)
	WD_SetProperty("Hide")
    fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I found this while in the mines.[SOFTBLOCK] Does it look like anything you recognize?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Where did you find it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A Steam Droid settlement.[SOFTBLOCK] Pretty small.[SOFTBLOCK] It had been overrun, but I think everyone got out safely.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Might have been Trilby Junction.[SOFTBLOCK] In which case, I think this belongs to one of the Steam Droids who passed through.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] [SOUND|World|TakeItem]I would guess it has some sentimental value.[SOFTBLOCK] In any case, I'll pass it along to the next team heading out past Cider Gorge.[SOFTBLOCK] That's where they wound up settling.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Just doing my part.[BLOCK][CLEAR]") ]])
    
    --Rewards.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 75)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (+75 Reputation!)") ]])
    else
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 25)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (+25 Work Credits!)") ]])
    end
    
elseif(sTopicName == "ArtAppreciation") then
	WD_SetProperty("Hide")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDReward", "N", 4.0)
    fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Do you know an artist named PP-81?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I've heard the name.[SOFTBLOCK] And yes, I heard you helped her out.[SOFTBLOCK] She was telling anyone who'd listen, apparently.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] As long as she made it back safely.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I'd heard she was a recluse, which is not a safe thing to be in the mines.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But you tolerate artists?[SOFTBLOCK] Especially when supplies are so hard to get?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] That is, unfortunately, a common attitude.[SOFTBLOCK] But, we do tolerate and even encourage artists.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Strange as it may seem, we have to set aside something for them even if times are dire.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Art is the only reason we keep going.[SOFTBLOCK] If it was just endless struggle and violence, I'm not sure any of us would bother.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yeah, I guess you're right.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just try to keep her away from all the creatures that are going to eat her.[SOFTBLOCK] She seemed a little loopy.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] An artist without a few loose bolts is no artist at all...[BLOCK][CLEAR]") ]])
    
    --Rewards.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 125)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (+125 Reputation!)") ]])
    else
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 60)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (+60 Work Credits!)") ]])
    end

--[Goodbye]
elseif(sTopicName == "Goodbye") then
	WD_SetProperty("Hide")
    fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101: Good hunting, my friend.") ]])
end