--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Common
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
    local iSXUpgradeQuest        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
    --Upgrade quest.
    if(iSXUpgradeQuest == 2.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[VOICE|JX-101] Don't bother the prisoner, sweetie.[SOFTBLOCK] She'll probably just insult you.") ]])
        return
    end
    
    --[Human]
    if(sChristineForm == "Human") then
    
        --Form variables.
        local iSpokeSprocketLordUnitHuman = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitHuman", "N")
    
        --First time.
        if(iSpokeSprocketLordUnitHuman == 0.0) then
            
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitHuman", "N", 1.0)
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: And what do we have here?[SOFTBLOCK] A human, in the mines?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you well, Lord Unit?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Most curious.[SOFTBLOCK] Not only are you mixing with this rabble, but you are well spoken.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I must protest these conditions of captivity.[SOFTBLOCK] You have failed to provide me with a means of recharging myself, and my core is dangerously depleted.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: See to it that my needs are seen to post-haste, human![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'll speak to the guards.[SOFTBLOCK] One moment.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Christine", 25.25, 6.50)
            fnCutsceneMove("Christine", 25.25, 8.50)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneFace("Christine", -1, 0)
            fnCutsceneFace("DroidBC", 1, 0)
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Pardon me, but have you allocated any energy rations to this prisoner?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: Energy rations?[SOFTBLOCK] No.[SOFTBLOCK] We don't even know what to give her, and she shouts at us when we ask.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Golem power cores can metabolize nearly any material with a matter-energy conversion.[SOFTBLOCK] Almost anything will work, but carbohydrates are ideal as the process is still subject to entropy.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: ...[SOFTBLOCK] Come again?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oil.[SOFTBLOCK] Get her one liter of oil every two days.[SOFTBLOCK] That should be enough.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: Oh, sure thing.[SOFTBLOCK] You just tell her not to be so damn finicky and we'll get her the oil.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: And -[SOFTBLOCK] Christine, don't let the prisoner hold you hostage like last time.[SOFTBLOCK] Okay?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Deal.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Christine", 25.25, 5.50)
            fnCutsceneBlocker()
            fnCutsceneFace("LordUnit", 0, 1)
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I've made arrangements for you.[SOFTBLOCK] You should be taken care of now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Tch, if you think you can earn favour with me, you are mistaken.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: When, not if, [SOFTBLOCK]*when*[SOFTBLOCK], I return to Regulus City, I will send a team to have all of your friends retired.[SOFTBLOCK] You, of course, I will personally convert.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Let's see...[SOFTBLOCK] I think you would do well as my dedicated cleaning Slave Unit.[SOFTBLOCK] Should I sully myself in the course of my duties, it will be your task to clean my chassis.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ...[SOFTBLOCK][SOFTBLOCK] With your tongue.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: What do you think of that, human?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Kinky\", " .. sDecisionScript .. ", \"Kinky\") ")
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sod Off\",  " .. sDecisionScript .. ", \"Sod Off\") ")
            fnCutsceneBlocker()
        
        --Repeat conversations.
        else
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: If you think you can earn yourself favour with me by showing concern, you are mistaken.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And if I show you concern for no reason?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Then I will commend you on such a devious trick.[SOFTBLOCK] And, I assure you, it will fail.[SOFTBLOCK] Good day, human.") ]])
            fnCutsceneBlocker()
        
        end
    
    --[Golem]
    elseif(sChristineForm == "Golem") then
    
        --Form variables.
        local iSpokeSprocketLordUnitGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitGolem", "N")
    
        --First time.
        if(iSpokeSprocketLordUnitGolem == 0.0) then
            
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitGolem", "N", 1.0)
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh, a Lord Unit.[SOFTBLOCK] A liberator, or fellow prisoner?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Neither.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Jailor.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Explain yourself.[SOFTBLOCK] Promptly.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Unit 2855 and I have conspired to put you here.[SOFTBLOCK] We're working with the Steam Droids.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] More importantly, there's nothing you can do about it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Unit 2855?[SOFTBLOCK] The missing Prime Command Unit?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The same.[SOFTBLOCK] You didn't see my face when I was caving in your cranial chassis.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: And a Lord Golem has turned traitor...[SOFTBLOCK] Do you realize what you're turning your back on?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Please, do tell.[SOFTBLOCK] Tell me what about Regulus City is so wonderful that you'd never betray it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Is it, perhaps, the Black Sites where undesirables are disappeared and tortured on?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Maybe it's the thousands of Slave Units who are worked to the breaking point and denied any sort of self-determination?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh, or maybe it's the utopia we live in -[SOFTBLOCK] but only we Lord Units, and not the underclass who makes the utopia possible?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We perfect machines who have transcended our flawed human bodies but not our flawed human social structures.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You must be young.[SOFTBLOCK] Young that you do not understand anything yet.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: All of those things are crimes.[SOFTBLOCK] Yes, that is true and I will not deny it.[SOFTBLOCK] Some Lord Units will, I will not.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: But what alternative do you propose?[SOFTBLOCK] Do you not realize that the so-called democratic societies of Pandemonium all ended in tyranny one way or the other?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Is not our carefully managed, computerized tyranny far greater than any they could design?[SOFTBLOCK] Even the lowly Slave Units have a standard of living far in excess of the peasants who toil Pandemonium's fields.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: No, we bring so much that even the crumbs of utopia are superior to the whole loaf of desperate poverty.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] That's almost a good argument -[SOFTBLOCK] but it's really apology.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You came up with that excuse because you happen to be the one who benefits from it.[SOFTBLOCK] If you had been a Slave Unit, would you really support your own oppression?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You don't have to answer, because the thousands of Slave Units have already staked their position for you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: They are petty and put their personal positions ahead of the greater good.[SOFTBLOCK] The Cause of Science.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] And who asked them to do that?[SOFTBLOCK] Nobody, you forced them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I serve the cause loyally, and many of them would, too.[SOFTBLOCK] But you decided for them and made their lives a living hell.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: In the end, they, I, none of us matters.[SOFTBLOCK] That I was allowed to serve the Cause in a capacity that offered me comforts is something I will not turn down.[SOFTBLOCK] It doesn't matter.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Advancing the Cause is what matters.[SOFTBLOCK] Central Administration is far wiser than we lowly golems, and that is the society we have been told advances it most.[SOFTBLOCK] You disagree, but you are wrong.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Then we'll agree to disagree, won't we?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't worry, soon our disagreements will be solved with pulse munitions instead of words.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You and the Steam Droids seek a civil war?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Seek?[SOFTBLOCK] Hardly.[SOFTBLOCK] We have no other choice.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] If you make peaceful revolution impossible, then violent revolution becomes inevitable.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Such a young fool.[SOFTBLOCK] I have no more words to waste on your aural receptors.[SOFTBLOCK] Be gone.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Gladly.[SOFTBLOCK] Enjoy your accomodations, Lord Unit, because they are anything but temporary.") ]])
            fnCutsceneBlocker()
        
        --Repeat conversations.
        else
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I would talk some sense into you, but you are beyond that.[SOFTBLOCK] Hope that your revolution succeeds, because if it does not, I will come for you.[BLOCK][CLEAR]")  ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] My ancestors did a lot of awful things, but one of the good things they did was opposing people like you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And you know what?[SOFTBLOCK] The authoritarians lost in the end.[SOFTBLOCK] They always do.[SOFTBLOCK] I can't wait to see the look on your face when I deliver you that news.") ]])
            fnCutsceneBlocker()
        
        end
    
    --[Steam Droid]
    elseif(sChristineForm == "SteamDroid") then
    
        --Form variables.
        local iSpokeSprocketLordUnitSteamDroid = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitSteamDroid", "N")
    
        --First time.
        if(iSpokeSprocketLordUnitSteamDroid == 0.0) then
            
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitSteamDroid", "N", 1.0)
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: What do you want, obsolete?[SOFTBLOCK] It may not appear that way, but I am quite busy.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Doing what, stewing in a jail cell?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Organizing my hard drive, if you must know.[SOFTBLOCK] There are a number of philosophical problems I had yet to work through.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh, a hard drive is the storage medium we golems use for long-term data storage.[SOFTBLOCK] Not as fast to access but the sheer amount of data we can store is much higher.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You think you're funny, but you're not.[SOFTBLOCK] I know what a hard drive is.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Oh, that's good.[SOFTBLOCK] I can explain any other technical terms you want, feel free to ask.[SOFTBLOCK] I'll even speak slowly so you can understand the complex parts.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Bully.[SOFTBLOCK] But I know how to deal with you.[SOFTBLOCK] Never let them take the reins in the conversation.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: By the way - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You talk an awful lot, for someone who's busy.[SOFTBLOCK] Are you lonely?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Do you want me to make a little Slave Golem mannequin for you to beat up on?[SOFTBLOCK] Would that make you feel more at home?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Or maybe a little Command Unit mannequin, to kiss the feet of?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, I know![SOFTBLOCK] Maybe a Steam Droid mannequin, who can also throw you in jail?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You -[SOFTBLOCK] impertinant - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Gosh, how must it feel to be outsmarted and captured by inferior units?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] After all, our weak steam-powered frames couldn't defeat you in a fair fight.[SOFTBLOCK] We'd have to outsmart you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I was ambushed by a Command Unit, though I did not see her face.[SOFTBLOCK] I assume she is the one behind you.[LOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Quite right, she's working with us.[SOFTBLOCK] See, outsmarting you wasn't that hard was it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] No, it wasn't.[SOFTBLOCK] She was almost comically easy to take down.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Grrrr...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hey, Command Unit friend of mine -[SOFTBLOCK] you know what we should do?[SOFTBLOCK] Something that this Lord Unit here is apparently too weak, clumsy, or scared to do?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Leave this cell.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's pretty easy for us, but she apparently can't do it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You will regret the day you crossed me, obsolete droid![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I doubt it.[SOFTBLOCK] In any case, that day is not today.[SOFTBLOCK] C'mon, Command Unit.[SOFTBLOCK] Let's go let her be superior all by her lonesome.") ]])
            fnCutsceneBlocker()
        
        --Repeat conversations.
        else
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Stupid obsolete unit![SOFTBLOCK] Release me at once![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sorry, I can't hear you over the sound of you being in prison.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That statement did not make sense.[SOFTBLOCK] Being in prison doesn't make a sound.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Are you sure?[SOFTBLOCK] Because she's making a real racket![SOFTBLOCK] Ha ha!") ]])
            fnCutsceneBlocker()
        
        end
    
    --[Darkmatter]
    elseif(sChristineForm == "Darkmatter") then
    
        --Form variables.
        local iSpokeSprocketLordUnitDarkmatter = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitDarkmatter", "N")
    
        --First time.
        if(iSpokeSprocketLordUnitDarkmatter == 0.0) then
            
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitDarkmatter", "N", 1.0)
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: A darkmatter, in here?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Heh, it's not like she can tell anyone we Darkmatters can talk.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hello, Lord Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You can speak!?[SOFTBLOCK] Incredible![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All Darkmatters can speak.[SOFTBLOCK] We have a tremendous amount of knowledge to share with your kind.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Please, liberate me from this cell![SOFTBLOCK] Whatever you need, I can give it to you at Regulus City in exchange for your knowledge![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You are being held here?[SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Simply phase through the walls as my kind do.[SOFTBLOCK] Surely you can liberate yourself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] My patience grows thin.[SOFTBLOCK] If you cannot perform even this simple task, then perhaps you are not worthy of our knowledge.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: No![SOFTBLOCK] Please wait![SOFTBLOCK] I'll -[SOFTBLOCK] I can't -[SOFTBLOCK] No!") ]])
            fnCutsceneBlocker()
        
        --Repeat conversations.
        else
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Better not say anything else.[SOFTBLOCK] She's pretty upset as it is.)") ]])
            fnCutsceneBlocker()
        
        end
    
    --[Electrosprite]
    elseif(sChristineForm == "Electrosprite") then
    
        --Form variables.
        local iSpokeSprocketLordUnitElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitElectrosprite", "N")
    
        --First time.
        if(iSpokeSprocketLordUnitElectrosprite == 0.0) then
            
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitElectrosprite", "N", 1.0)
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: And just what sort of debased creature are you?[SOFTBLOCK] You're practically naked![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Like what you see, honey?[SOFTBLOCK] There's lots where that came from![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: To openly display yourself like this...[SOFTBLOCK] You sicken me.[SOFTBLOCK] Show some dignity.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I'm sure it's very dignified to spit insults at anyone who comes by.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You know what's really dignified?[SOFTBLOCK] Rising above it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hurl all the insults you can bear, Lord Unit.[SOFTBLOCK] You cannot harm me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ...[SOFTBLOCK] I lost my composure.[SOFTBLOCK] Hrmph.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Begone, creature.[SOFTBLOCK] I have things to think about.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Annoying stuck-up ponces never gets old, does it?)") ]])
            fnCutsceneBlocker()
        
        --Repeat conversations.
        else
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Have you an apology for me, Lord Golem?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: It takes all the patience I have to hold my tongue, and my patience is in short supply.[SOFTBLOCK] Leave.") ]])
            fnCutsceneBlocker()
        
        end
    
    --[Latex Drone]
    elseif(sChristineForm == "LatexDrone") then
    
        --Form variables.
        local iSpokeSprocketLordUnitLatexDrone = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitLatexDrone", "N")
    
        --First time.
        if(iSpokeSprocketLordUnitLatexDrone == 0.0) then
            
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitLatexDrone", "N", 1.0)
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] HELLO, LORD UNIT.[SOFTBLOCK] THIS DRONE UNIT IS HERE TO SERVE YOU.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Hmm, were you captured, Drone?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] NEGATIVE.[SOFTBLOCK] THIS UNIT HAS ALLIED HERSELF WITH THE STEAM DROIDS.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: But you say you're here to serve me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] HOW CAN THIS UNIT MAKE YOU MORE COMFORTABLE?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Aid in my escape.[SOFTBLOCK] Distract the guards.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] AFFIRMATIVE.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Christine", 25.25, 6.50)
            fnCutsceneMove("Christine", 25.25, 8.50)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneFace("Christine", -1, 0)
            fnCutsceneFace("DroidBC", 1, 0)
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] EXCUSE ME, GUARD.[SOFTBLOCK] THIS UNIT WAS ORDERED BY THE PRISONER TO DISTRACT YOU.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] SHE IS OBVIOUSLY PLANNING AN ESCAPE.[SOFTBLOCK] KEEP AN EYE ON HER.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: You got it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] THANK YOU FOR YOUR TIME, STEAM DROID FRIEND.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: *Is this part of a bit, Christine?*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *Yep.[SOFTBLOCK] Messing with the prisoner.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: *Oh please don't, she never stops complaining as it is...*") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Christine", 25.25, 5.50)
            fnCutsceneBlocker()
            fnCutsceneFace("LordUnit", 0, 1)
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] THE GUARDS HAVE BEEN INFORMED OF YOUR PLANS TO ESCAPE.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] THIS UNIT NOTES THAT TRICKING YOU WAS DISTRESSINGLY EASY.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You stupid drone![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Actually, I had my inhibitor turned off.[SOFTBLOCK] So, calling me stupid?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Better look in the mirror first, right?[SOFTBLOCK] Ha ha ha ha![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: *growl*") ]])
            fnCutsceneBlocker()
        
        --Repeat conversations.
        else
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] HELLO, LORD UNIT, HOW MAY THIS UNIT SERVE YOU?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You think I'm going to fall for the same trick twice?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Given how you fell for the first one so hard?[SOFTBLOCK] Yes![SOFTBLOCK] Ha ha ha ha![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: *stupid dumb idiot drones...*") ]])
            fnCutsceneBlocker()
        
        end
    
    --[Eldritch Dreamer]
    elseif(sChristineForm == "Eldritch") then
    
        --Form variables.
        local iSpokeSprocketLordUnitEldritch = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitEldritch", "N")
    
        --First time.
        if(iSpokeSprocketLordUnitEldritch == 0.0) then
            
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitEldritch", "N", 1.0)
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Greetings, metal girl.[SOFTBLOCK] You're looking delicious.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: And just what are you supposed to be?[SOFTBLOCK] One of the creatures from the mines?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Something like that, yes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: The Steam Droids have allied with disgusting creatures like yourself?[SOFTBLOCK] I'm not surprised.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, we are not allies.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] The Steam Droids have merely struck a deal.[SOFTBLOCK] Food, in exchange for information.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: So why are you here, bothering me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Simple.[SOFTBLOCK] My kind enjoy eating metallic brains.[SOFTBLOCK] The denser, the better.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Yours is looking...[SOFTBLOCK] delicious...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Guards![SOFTBLOCK] I am under your protection, please![SOFTBLOCK] Do not let this creature devour me![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh I'm not going to eat you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] [SOFTBLOCK][SOFTBLOCK][SOFTBLOCK]Just your brain.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Heeeeelp![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] That's enough, Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Suit yourself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ...[SOFTBLOCK] Just a nibble?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Aieeeee!!![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Ha ha ha ha![SOFTBLOCK] Perfect![SOFTBLOCK] Scream for me more![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Control yourself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Right, right.[SOFTBLOCK] Of course.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (I hope that was more my desire to annoy stuck-up people, and not my body telling me to really eat brains...)") ]])
            fnCutsceneBlocker()
        
        --Repeat conversations.
        else
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You're smelling...[SOFTBLOCK] tasty...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Eeeeeeek!![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (It never gets old!)") ]])
            fnCutsceneBlocker()
        
        end

    end

--[Post Decision]
elseif(sTopicName == "Kinky") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I have a girlfriend already, Lord Unit, but if I can convince her...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I -[SOFTBLOCK] wha -[SOFTBLOCK] you -[SOFTBLOCK] (data retrieval failure) -[SOFTBLOCK] you what?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You aren't into threesomes?[SOFTBLOCK] That's a shame...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You -[SOFTBLOCK] oh, this must be some interrogation trick.[SOFTBLOCK] It will not work on me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, certainly.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] But I wouldn't mind cleaning you top to bottom...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: !!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Now, please be polite to the Steam Droids here.[SOFTBLOCK] They will take care of you until...[SOFTBLOCK] well, you'll see.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I will not fall victim to your techniques.[SOFTBLOCK] You will get no secrets from me.[SOFTBLOCK] Good day, human.") ]])

--[Post Decision]
elseif(sTopicName == "Sod Off") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] At this point, I'd need to draw you a diagram to show you all the ways you can go sodomize yourself.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: You impertinant little rapscallion![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Lord Unit, please.[SOFTBLOCK] Dignity.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: I admit I lost my composure.[SOFTBLOCK] But you - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A test.[SOFTBLOCK] We must conduct ourselves with poise.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: We...?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Now, please be polite to the Steam Droids here.[SOFTBLOCK] They will take care of you until...[SOFTBLOCK] well, you'll see.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Hrmpf.[SOFTBLOCK] If this is an interrogation technique, it will not work.[SOFTBLOCK] Good day, human.") ]])
end