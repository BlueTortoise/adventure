--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for different NPCs.
    if(sActorName == "DroidGuardA") then
        
        --Variables.
        local iMinesGDroidKnowsChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/iMinesGDroidKnowsChristine", "N")
        local iHasSteamDroidForm = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
        if(iMinesGDroidKnowsChristine == 0.0 and iHasSteamDroidForm == 1.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMinesGDroidKnowsChristine", "N", 1.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey, if I told you I was Christine, and that this was 55, would that mean anything to you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] ...[SOFTBLOCK] Seriously?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, yeah, that'd make sense.[SOFTBLOCK] I've heard about you.[SOFTBLOCK] Transforming hero of Sprocket City.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sorry about earlier, we had to keep my identity a secret.[SOFTBLOCK] I wasn't publically with 55 at the time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It was unlikely I would be accepted by Steam Droid society, regardless of my sincerity.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Can't fault you for that, I suppose.[SOFTBLOCK] I was ready to plug you when I saw you come down the ladder.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Just stay alive, yeah?[SOFTBLOCK] It's rare to find new friends in the mines, I'd hate to lose you to a bunch of wandering creeps.") ]])
            fnCutsceneBlocker()
            
            local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
            if(iFixedElevator40 == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
            end
        
        --Does not know Christine, has not finished Sprocket City.
        elseif(iMinesGDroidKnowsChristine == 0.0 and iHasSteamDroidForm == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Try not to stay close to the creeps for too long.[SOFTBLOCK] I don't much care for what happens to those who do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Don't give me a reason to shoot you, yeah?") ]])
            fnCutsceneBlocker()
            
            local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
            if(iFixedElevator40 == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
            end
        
        --Knows Christine.
        elseif(iMinesGDroidKnowsChristine == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Good hunting out there, kiddo.[SOFTBLOCK] Make Sprocket City proud!") ]])
            fnCutsceneBlocker()
            
            local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
            if(iFixedElevator40 == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
            end
        
        end

    elseif(sActorName == "DroidGuardB") then
        
        --Variables.
        local iMinesGDroidKnowsChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/iMinesGDroidKnowsChristine", "N")
        
        --Does not know Christine
        if(iMinesGDroidKnowsChristine == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] You've probably already faced a lot of weird things on the way here.[SOFTBLOCK] Stay normal.") ]])
            fnCutsceneBlocker()
        
        --Knows Christine.
        elseif(iMinesGDroidKnowsChristine == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Jeez, if the famous Christine and 55 are here, that means we're in real trouble, doesn't it?[SOFTBLOCK] No offense.") ]])
            fnCutsceneBlocker()
        
        end
    
    elseif(sActorName == "DroidMedic") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] This golem is a little beaten up, but her internals are fine.[SOFTBLOCK] I told her to go into standby to conserve power.") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "GolemA") then
        
        --Variables.
        local iTalkedToGGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToGGolem", "N")
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --First time.
        if(iTalkedToGGolem == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToGGolem", "N", 1.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Pack") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Hello, Lord Golem. So this is it?[BLOCK][CLEAR]") ]])
            if(sChristineForm == "Golem") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] But you are not just a Lord Golem, are you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] I can see the real you.[SOFTBLOCK] I can see all of them, all at once.[SOFTBLOCK] I'm sorry...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yes, I can see that there in no point in lying.[SOFTBLOCK] You've been touched, haven't you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Touched?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Just a little.[SOFTBLOCK] We were running, and one of them grabbed my arm.[SOFTBLOCK] I pulled and I pulled, and then I saw something.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] In the dark, a shadow that hid nothing, I saw three eyes.[SOFTBLOCK] They were looking me up and down.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] I've been seeing things since then, things as they are, or could be.[SOFTBLOCK] And the eyes -[SOFTBLOCK] I still see the eyes...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm sorry, I don't think I can help you.[BLOCK][CLEAR]") ]])
            
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ah, but I am not a Lord Golem...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] You are.[SOFTBLOCK] I can see the real you.[SOFTBLOCK] I can see all of them, all at once.[SOFTBLOCK] I'm sorry...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yes, I can see that.[SOFTBLOCK] You've been touched, haven't you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Touched?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Just a little.[SOFTBLOCK] We were running, and one of them grabbed my arm.[SOFTBLOCK] I pulled and I pulled, and then I saw something.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] In the dark, a shadow that hid nothing, I saw three eyes.[SOFTBLOCK] They were looking me up and down.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] I've been seeing things since then, things as they are, or could be.[SOFTBLOCK] And the eyes -[SOFTBLOCK] I still see the eyes...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm sorry, I don't think I can help you.[BLOCK][CLEAR]") ]])
            
            end
            
            --Rejoin.
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Are you here to retire us?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] No...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] I was asking Unit 2855.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am not.[SOFTBLOCK] If you are mavericks, I am no longer with the security services.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Hmmm, well.[SOFTBLOCK] If you were to try to retire us, we wouldn't stop you.[SOFTBLOCK] We're done running and fighting.[SOFTBLOCK] Done.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] What happened?[SOFTBLOCK] How did you get here?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] We used to work in the Cryogenics Facility, south of Regulus City.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] ...![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] [EMOTION|Christine|Sad]Yes, yes, I understand.[SOFTBLOCK] You know what happened there.[SOFTBLOCK] You're the one she was talking about.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Our Lord Golem, Unit 609144, told us to follow her.[SOFTBLOCK] She seemed so complacent.[SOFTBLOCK] The security teams were on their way, but she just said 'Follow me'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] We left when the shooting started.[SOFTBLOCK] A few stray pulse shots whizzed by us but the Lord Golem just kept walking.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] We followed her down the tram tracks, through the caves, deeper and deeper and deeper.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] I was scared, but I was too scared to run away.[SOFTBLOCK] We just kept following.[SOFTBLOCK] And then the rock became some kind of meat, and Unit 609144 started laughing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] She laughed and those things just came from the walls.[SOFTBLOCK] They just melted out and surrounded us.[SOFTBLOCK] I bolted.[SOFTBLOCK] My two friends here ran as soon as I did.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] And we ran and hid and climbed...[SOFTBLOCK] We've been doing it for so long my chronometer lost track of time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] These Steam Droids found us.[SOFTBLOCK] I think they want to shoot us, but I don't care any more.[SOFTBLOCK] All I needed to do was make sure you, Christine, knew.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] She's still down there.[SOFTBLOCK] On floor 50.[SOFTBLOCK] Waiting for you.[SOFTBLOCK] I can still hear her giggling.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] She knows who I am?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Yes.[SOFTBLOCK] She said your name when we first left Cryogenics.[SOFTBLOCK] Said we were going to wait for you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (But that would be before I was even on Regulus...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A cryptic warning and an invitation to a trap?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Unit 2855, please listen.[SOFTBLOCK] You will need to be able to defend yourself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You would imply I cannot?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] You won't be able to avoid her.[SOFTBLOCK] You'll need to hide.[SOFTBLOCK] To take cover.[SOFTBLOCK] Please, please.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] If you don't stop her, I'll never stop hearing her laugh...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hrmph.[SOFTBLOCK] Taking cover is unbecoming of a Command Unit.[SOFTBLOCK] My sensors allow me to evade most strikes and my chassis can absorb hard blows.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's not bad advice, 55.[SOFTBLOCK] I think this unit knows what she's talking about.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I just hope you'll be able to find cover when we find this Unit 609144...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] So long as I'm near you, cover will not be in short supply.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Gee, thanks, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Please stop her, Christine.[SOFTBLOCK] Please...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I haven't met a robot yet who I couldn't best.[SOFTBLOCK] You won't suffer much longer.[SOFTBLOCK] Let's go, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (55 has learned the 'Take Cover' ability.)") ]])
            VM_SetVar("Root/Variables/Global/2855/iSkillbook06", "N", 1.0)
            AdvCombat_SetProperty("Push Party Member", "55")
                LM_ExecuteScript(gsRoot .. "Abilities/55/000 Initializer.lua", "Take Cover")
            DL_PopActiveObject()
        
        --Repeats.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Pack") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Please, Christine.[SOFTBLOCK] We have nothing to offer you but our gratitude.[SOFTBLOCK] Please...") ]])
        
        end
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We're sick of running and hiding...") ]])
        fnCutsceneBlocker()
        
    end
end