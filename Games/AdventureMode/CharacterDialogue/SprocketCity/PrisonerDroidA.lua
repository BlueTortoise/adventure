--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Dialogue.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] W-who's there?[SOFTBLOCK] Who are you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Relax, we're friends.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] I -[SOFTBLOCK] I -[SOFTBLOCK] I can't see anything.[SOFTBLOCK] My optical receptors are offline.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] RI-15, is that you?[SOFTBLOCK] We thought you were retired![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "RI-15:[E|Neutral] Do I hear JX-101?[SOFTBLOCK] Where am I?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] It's me, and it's okay now.[SOFTBLOCK] We're going to get you out of here. Follow us.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "RI-15:[E|Neutral] Thank gears you've come, JX.[SOFTBLOCK] My receptors...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's not safe here.[SOFTBLOCK] Take my hand and follow me, we'll get you out of here and get you fixed up.") ]])
	fnCutsceneBlocker()
	
	--Get Christine's position.
	EM_PushEntity("Christine")
		local iX, iY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Move the droid to her.
	fnCutsceneMove("DroidA", iX / 16.0, iY / 16.0)
	fnCutsceneBlocker()
	fnCutsceneTeleport("DroidA", -100.0, -100.0)
	fnCutsceneBlocker()
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N", 1.0)
		
	--Check ending case.
	local iRescuedDroidA = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N")
	local iRescuedDroidB = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidB", "N")
	local iRescuedDroidC = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N")
	local iRescuedGolemD = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N")
	local iFoundSchematics = VM_GetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N")
	if(iRescuedDroidA == 1.0 and iRescuedDroidB == 1.0 and iRescuedDroidC == 1.0 and iRescuedGolemD == 1.0 and iFoundSchematics == 1.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 9.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] All right Christine, we've done enough.[SOFTBLOCK] I believe we've swept the site thoroughly.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Let's get back to the entrance and get out of here before Regulus City figures out what we've done.") ]])
		fnCutsceneBlocker()
	end

end