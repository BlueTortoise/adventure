--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iLRTBossResult = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N")
	local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
	
	--Dialogue.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Wah ha ha![SOFTBLOCK] You're pretending, but I can see the real you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] 771852![SOFTBLOCK] Nobody knows but me, ha ha ha ha![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] She's spouting gibberish.[SOFTBLOCK] Do you think she was an experiment, too?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh, bronze and green.[SOFTBLOCK] Not improved yet.[SOFTBLOCK] Is this the first time we've met?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Hello, I am Lord Unit 820189011919089211947![SOFTBLOCK] Pleased to meet you, 771852 and 101![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That designation is waaay past the valid range...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I touched it, you see.[SOFTBLOCK] It got in through my hand, but that's fine.[SOFTBLOCK] It taught me so much.[BLOCK][CLEAR]") ]])
	if(iLRTBossResult == 0.0 and iCompletedSerenity == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Ah, you don't know what I'm talking about.[SOFTBLOCK] But you will.[SOFTBLOCK] You will, Christine Dormer.[BLOCK][CLEAR]") ]])
	elseif(iCompletedSerenity == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh don't be coy.[SOFTBLOCK] You know what I'm referring to.[SOFTBLOCK] You saw it in Serenity Crater, didn't you?[SOFTBLOCK] Or has that happened yet?[BLOCK][CLEAR]") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] And -[SOFTBLOCK] you're her pupil?[SOFTBLOCK] The one she was talking about?[SOFTBLOCK] Oh, I'd be jealous, but we're beyond that, aren't we.[SOFTBLOCK] Aren't we?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *Vivify sends her regards, Christine.*[BLOCK][CLEAR]") ]])
	end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] !!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Tease tease tease![SOFTBLOCK] But that's enough.[SOFTBLOCK] I'm very sleepy.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I'll put a slug in your processor so you can sleep forever![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101, STOP![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Don't you see, she's a victim just as much as anyone else![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] She got what's coming to her.[SOFTBLOCK] She's clearly insane as a result of whatever sick experiments they're doing here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And we have to help her![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just -[SOFTBLOCK] please take her back to Sprocket City.[SOFTBLOCK] Maybe she can be cured?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] And have a Lord Golem in there, a ticking time bomb?[SOFTBLOCK] She's crazy and dangerous.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Do it for me, please, I'm begging you![SOFTBLOCK] Hasn't there been enough death in this place?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] ...[SOFTBLOCK] Fine. I consider this a personal favour.[SOFTBLOCK] I would not do this for just anyone.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh, this is the part where you take me to a bronze place.[SOFTBLOCK] I love this part![SOFTBLOCK] And then you upgrade the broken robot who marries the pale robot, right?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just shut up and follow me.") ]])
	fnCutsceneBlocker()
	
	--Get Christine's position.
	EM_PushEntity("Christine")
		local iX, iY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Move the droid to her.
	fnCutsceneMove("DroidD", iX / gciSizePerTile, iY / gciSizePerTile)
	fnCutsceneBlocker()
	fnCutsceneTeleport("DroidD", -100.0, -100.0)
	fnCutsceneBlocker()
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N", 1.0)
		
	--Check ending case.
	local iRescuedDroidA = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N")
	local iRescuedDroidB = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidB", "N")
	local iRescuedDroidC = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N")
	local iRescuedGolemD = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N")
	local iFoundSchematics = VM_GetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N")
	if(iRescuedDroidA == 1.0 and iRescuedDroidB == 1.0 and iRescuedDroidC == 1.0 and iRescuedGolemD == 1.0 and iFoundSchematics == 1.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 9.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] All right Christine, we've done enough.[SOFTBLOCK] I believe we've swept the site thoroughly.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Let's get back to the entrance and get out of here before Regulus City figures out what we've done.") ]])
		fnCutsceneBlocker()
	end

end