--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Psychologist:[VOICE|Steam Droid] This patient is completely lucid, but speaks incoherently about things seemingly at random.[SOFTBLOCK] She starves herself of power for days and then binges on oil, and scrawls gibberish on the walls with nail polish.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Psychologist:[VOICE|Steam Droid] I'm not educated on golem system architecture, but nothing seems to be wrong on the outside.[SOFTBLOCK] It is most troubling.") ]])
	fnCutsceneBlocker()
end