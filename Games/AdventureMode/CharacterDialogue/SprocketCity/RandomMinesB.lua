--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "DroidLeader") then
        
        --Variables.
        local iMineBGotAmmo = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N")
        
        --No ammo yet.
        if(iMineBGotAmmo == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] I notice a dearth of sweet munitions in your hands.[SOFTBLOCK] It's south across that chasm.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] You'll have to go around, of course.[SOFTBLOCK] Because nothing in the mines is properly maintained.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Now move out.") ]])
            fnCutsceneBlocker()
        
        --Got the ammo, turning it in.
        elseif(iMineBGotAmmo == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N", 2.0)
            
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Do my olfactory sensors detect nitrocellulose?[SOFTBLOCK] And it's not even my birthday![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] A primitive ball-propellant, though certainly effective.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Hey, 750 meters per second is good enough for day-to-day violence.[SOFTBLOCK] Not all of us get to play with expensive toys.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Though if I could convince you to part with that Pulse Diffractor...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unlikely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Well, you delivered the goods.[SOFTBLOCK] Now, let me deliver mine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] You tell JX-101 I said 'Sleeping Furiously'.[SOFTBLOCK] She'll know what it means.[BLOCK][CLEAR]") ]])
            
            --Hasn't met JX-101:
            local iMetJX101 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N")
            if(iMetJX101 == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A steam droid leader.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Us steam droids all know each other.[SOFTBLOCK] You say that passphrase, they'll know who its for and what it means.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] If you're a friend of the steam droids, you'll meet her.[SOFTBLOCK] Sooner rather than later.[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] A code phrase?[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] We're not in position to give you anything direct, not here.[SOFTBLOCK] But the phrase I just gave you will serve you well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Stay alive long enough and we might just meet again.[SOFTBLOCK] Good hunting out there.") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
            
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Thanks again for the ammo.[SOFTBLOCK] You stay safe out there.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] And if you're not looking to stay safe, try to take as many of these freaks with you as you can.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Uhm, we will?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Hey, if we all do the same the mines will be a lot safer for whoever makes it through.") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "DroidA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Try not to let them sneak up on you.[SOFTBLOCK] In the mines, getting ganged on is a death sentence.") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "DroidB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Really wish I could get my hands on a pulse carbine.[SOFTBLOCK] Those things cut through flesh like butter.") ]])
        fnCutsceneBlocker()
    end
end