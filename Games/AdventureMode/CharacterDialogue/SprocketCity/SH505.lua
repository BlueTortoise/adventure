--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iMetSH505 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSH505", "N")
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	
    --Impersonation.
    if(iSXUpgradeQuest == 2.0) then
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] Ugh, leave me alone.[SOFTBLOCK] I'm not in a good mood right now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] My straightjacket is supposed to arrive tomorrow.[SOFTBLOCK] That might cheer me up.") ]])
    
	--First meeting.
	elseif(iMetSH505 == 0.0) then
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] Oh look, there's that human the other droids were talking about.[SOFTBLOCK] What do you want?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Hmm, I can't quite place your accent.[SOFTBLOCK] Where are you from?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] Lerbow.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Must be someplace on Pandemonium...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] So what are you working on?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] This one is called 'Horse Bondage VII'.[SOFTBLOCK] It's -[SOFTBLOCK] well, I don't think it needs an explanation.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Well you're very up front about it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] Should I not be?[SOFTBLOCK] I like what I like, and I draw it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I wasn't putting you down![SOFTBLOCK] I like it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] I would say thank you, but the damage is already done.[SOFTBLOCK] Now I am depressed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Come on, cheer up![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] ...[SOFTBLOCK] Mrow...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] What can I do to make it up to you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] Nothing.[SOFTBLOCK] You can't cure it.[SOFTBLOCK] Mrow.") ]])
		fnCutsceneBlocker()
		
		VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSH505", "N", 1.0)
	else
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] ...[SOFTBLOCK] Mrow...") ]])
		fnCutsceneBlocker()
	end
end