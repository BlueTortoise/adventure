--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iSpokeWith55AboutSX         = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWith55AboutSX", "N")
	local iPlanHatched                = VM_GetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N")
	local iSXMet55                    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
	local iIs55Following              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
	local sChristineForm              = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iSteamDroid250RepState      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
	local iSXKnowsChristineTransforms = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N")
	local iSteamDroid500RepState      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N")
	
	--Talking to SX-399 after the first meeting.
	if(iSteamDroid250RepState < 3.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] *Are you sure this will work?*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Be polite but assertive.[SOFTBLOCK] She needs to respect your independence.*") ]])
	
	--Has not met 55 yet:
	elseif(iSXMet55 == 0.0 and iIs55Following == 1.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N", 1.0)
	
		--If Christine is a human during this:
		if(sChristineForm == "Human" or iSXKnowsChristineTransforms == 1.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Hey Christine -[SOFTBLOCK] and hello to you, ma'am.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] *Now would be the ideal time to kidnap her.*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55, for the last time we are not kidnapping her![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, you can kidnap me if you want...[SOFTBLOCK] Just have me back in time for my recharge, and I'll go anywhere with you...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] She seems to want us to take her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Just you, babe...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, I'm sorry, Christine.[SOFTBLOCK] Are you together?[SOFTBLOCK] I don't want to get in the way...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 55 is my good friend, but my heart lies elsewhere.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So you're saying you don't mind?[SOFTBLOCK] Radical![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] 55, was it?[SOFTBLOCK] Don't be a stranger, honey.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] !!![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ha ha![SOFTBLOCK] I'm just being friendly.[SOFTBLOCK] Any friend of Christine's is a friend of mine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So, Christine, what *is* 55?[SOFTBLOCK] Some kind of doll girl?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am Command Unit 2855.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Keen![SOFTBLOCK] So that's what a Command Unit looks like![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Mother tells me they're the worst of the worst, but I guess you're not all bad.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She's a bit of a special case.[SOFTBLOCK] The other ones aren't like her, at all.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Lucky, then, that I don't give a fig about them.[SOFTBLOCK] So, what brings you here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, I just thought I might introduce you two.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] And...[SOFTBLOCK] please don't tell anyone else.[SOFTBLOCK] I don't want to cause any trouble.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Sprocket City welcomes all comers.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] No, it does not.[SOFTBLOCK] I am not interested in residence.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The other Steam Droids will doubtless not react well to me.[SOFTBLOCK] Command Units are their most bitter of enemies.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Okay then, I won't tell.[SOFTBLOCK] Hey, more for me...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, why does she keep pressing herself up against me like that?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] ...[SOFTBLOCK] Stop![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *SX, you better stop.[SOFTBLOCK] You're being too forward.*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] *I'm soooo thirsty, and she's the hottest robot I've ever seen.*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] My auditory receivers are perfectly capable of hearing you regardless of volume, you realize.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay, SX-399, I'll be honest here.[SOFTBLOCK] I think we can trust you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55 and I are looking for allies.[SOFTBLOCK] We're planning a revolt against the administration of Regulus City.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We came here in order to meet your mother and try to get her on our side.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Hm, so you only came to my room for that?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No![SOFTBLOCK] We help anyone in need![SOFTBLOCK] Right, 55?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Ngh -[SOFTBLOCK] She is squeezing my arm.[SOFTBLOCK] Stop that![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unfortunately we fell into that trap, and now JX-101 thinks 55 is both villainous, and dead.[SOFTBLOCK] I'm not really sure how to proceed.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Quite a bind you're in.[SOFTBLOCK] I'd love to help, but I need to go in for recharging soon.[SOFTBLOCK] I -[SOFTBLOCK] I hope your rebellion works out.[SOFTBLOCK] Come visit me when I wake up again.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Wait...[SOFTBLOCK] You mentioned your condition.[SOFTBLOCK] Do you know why you have to spend so much recharging?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Something wrong with my power core.[SOFTBLOCK] I think it's a leak, but I have to be asleep to open me up.[SOFTBLOCK] You should ask TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ha, wait a minute now.[SOFTBLOCK] Are you thinking of fixing me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I am pretty good with machines.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Good luck with that.[SOFTBLOCK] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well I'm not just any old mechanic.[SOFTBLOCK] I'm going to go talk to TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Yeah, 55, you stay out here with me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] !!!!!!!![SOFTBLOCK] Release me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
	
		--Golem:
		elseif(sChristineForm == "Golem") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Hey Christine.[SOFTBLOCK] Anything interesting out in the mines?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Did I -[SOFTBLOCK] oops, I should have transformed before speaking to you.[SOFTBLOCK] Don't be afraid![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I'm not afraid of anything.[SOFTBLOCK] By the way, what happened?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I am a Golem, SX-399.[SOFTBLOCK] I have been since before we met.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Keen![SOFTBLOCK] So this is what a Golem looks like![SOFTBLOCK] Nice dress, by the way.[SOFTBLOCK] Do you have more like it?[SOFTBLOCK] I think it'd look pretty good on me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, we should acquire her while she is away from the other Steam Droids.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Hot stuff, you can acquire me all you want.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wait, that sounded stupid.[SOFTBLOCK] What does acquire mean?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *Sigh*[SOFTBLOCK] 55 wants to kidnap you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] 55, hm?[SOFTBLOCK] Well I wouldn't mind going someplace private with you, if you know what I mean.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ..![SOFTBLOCK] Unhand me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] No, 55, no![SOFTBLOCK] She's being friendly![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So if you're a Golem, does that mean 55 is a Command Unit?[SOFTBLOCK] Radical![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Christine, you are just the coolest![SOFTBLOCK] Mother was totally wrong about the Golems![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, not quite.[SOFTBLOCK] You see, 55 and I are mavericks.[SOFTBLOCK] We're planning a revolt against Regulus City's administration.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That's the whole reason we came into the mines, you see.[SOFTBLOCK] We were looking for JX-101.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We believe the Steam Droids will assist us.[SOFTBLOCK] We share the same objectives.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Yeah, no.[SOFTBLOCK] Pretty sure mother would have you shot on sight.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Well, unless you looked like a human.[SOFTBLOCK] Was that a disguise?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ah -[SOFTBLOCK] no.[SOFTBLOCK] I can change between forms.[SOFTBLOCK] It is because of this runestone here, see?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Very cool![SOFTBLOCK] Can I see?") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tadaa![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Awwwwesome![SOFTBLOCK] Can 55 do that too?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, just me.[SOFTBLOCK] It's magic, but I don't know exactly how it works.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So, do you want me to speak to mother for you?[SOFTBLOCK] I'm not sure she'd believe me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That, unfortunately, is the problem.[SOFTBLOCK] I highly doubt she will appreciate all the duplicity.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] From what I've seen of her, she hates Golems with a passion.[SOFTBLOCK] She'd likely assume I was converted and have me retired.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I was about to say that, actually.[SOFTBLOCK] Mother is extremely stubborn.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[SOFTBLOCK] That condition of yours...[SOFTBLOCK] Do you know why you have to spend so much recharging?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Something wrong with my power core.[SOFTBLOCK] I think it's a leak, but I have to be asleep to open me up.[SOFTBLOCK] You should ask TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ha, wait a minute now.[SOFTBLOCK] Are you thinking of fixing me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I *am* the Lord Golem of Maintenance and Repair, Sector 96.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Well that explains why the other Steam Droids say you're good with repairs.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] But...[SOFTBLOCK] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well I'm not just any old mechanic.[SOFTBLOCK] I'm going to go talk to TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] All right.[SOFTBLOCK] You go do that, and 55 and I will stay out here...[SOFTBLOCK] and...[SOFTBLOCK] bond...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] !!!!!!!![SOFTBLOCK] Release me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
		--Latex Drone:
		elseif(sChristineForm == "LatexDrone") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wow, Christine.[SOFTBLOCK] Didn't know you were into that, but more power to you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You -[SOFTBLOCK] you recognize me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] It's the hair, I'd recognize it anywhere.[SOFTBLOCK] By the way, what happened?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I am a Drone Unit, SX-399.[SOFTBLOCK] It's one of the model variants of the Golems.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Tubular![SOFTBLOCK] So, who's your friend there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is 55.[SOFTBLOCK] Say hello, 55.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not appreciate how this Steam Droid is looking at me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I don't mean to be off-putting.[SOFTBLOCK] I just -[SOFTBLOCK] like what I see.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What are the connotations behind that[SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So![SOFTBLOCK] SX-399![SOFTBLOCK] I figure we can trust you, right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Of course![SOFTBLOCK] It's because of you that I'm not being watched every minute of every day.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Great![SOFTBLOCK] So, I'll admit this, then.[SOFTBLOCK] The reason we came to Sprocket City was that 55 and I are planning a rebellion against Regulus City.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh my gosh, seriously?[SOFTBLOCK] That is so stellar![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] And I'm guessing you needed the help of the Steam Droids?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You don't like the Administration, and neither do we.[SOFTBLOCK] We should work together.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unfortunately, your mother is very hostile towards Golems...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Obviously.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Which is why I had to appear as a human, you see.[SOFTBLOCK] But that only made things worse.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Because we fell into the trap you placed, 55 had to pretend like she was holding me hostage to escape.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So now JX-101 would probably have her shot on sight, and I don't know how to approach JX-101 without her assuming I'm a spy...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Well, why don't you just get 55 to put on a disguise like you did?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh -[SOFTBLOCK] no.[SOFTBLOCK] It was not a disguise...") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tadaa![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I can transform myself.[SOFTBLOCK] It's magic, you see.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Awwwwesome![SOFTBLOCK] You're rad to the max, Christine![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So, do you want me to speak to mother for you?[SOFTBLOCK] I'm not sure she'd believe me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That, unfortunately, is the problem.[SOFTBLOCK] I highly doubt she will appreciate all the duplicity.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[SOFTBLOCK] She'd likely assume I was converted and have me retired.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I was about to say that, actually.[SOFTBLOCK] Mother is extremely stubborn.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[SOFTBLOCK] That condition of yours...[SOFTBLOCK] Do you know why you have to spend so much recharging?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Something wrong with my power core.[SOFTBLOCK] I think it's a leak, but I have to be asleep to open me up.[SOFTBLOCK] You should ask TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ha, wait a minute now.[SOFTBLOCK] Are you thinking of fixing me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, you see, I'm a repair Unit. I run a whole repair shop, actually.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Well that explains why the other Steam Droids say you're good with repairs.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] But...[SOFTBLOCK] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well I'm not just any old mechanic.[SOFTBLOCK] I'm going to go talk to TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] All right.[SOFTBLOCK] You go do that, and 55 and I will stay out here...[SOFTBLOCK] and...[SOFTBLOCK] bond...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] !!!!!!!![SOFTBLOCK] Release me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Darkmatter!
        elseif(sChristineForm == "Darkmatter") then
		
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Christine![SOFTBLOCK] And -[SOFTBLOCK] you're one of those space girls I've seen on the computers?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Yes I am![SOFTBLOCK] We're called Darkmatters, by the way.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Sweet![SOFTBLOCK] Do you like to knock things off of tables, too?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Say, I haven't been to the surface in...[SOFTBLOCK] decades.[SOFTBLOCK] Are the stars I see in your hair a reflection of the night sky?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oooh ooh![SOFTBLOCK] Can you phase through stuff, too?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sheesh, SX-399.[SOFTBLOCK] Calm down.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Aw, but don't you miss being a human?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Well...[SOFTBLOCK] you see...") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tadaa![SOFTBLOCK] I can transform![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh cool cool![SOFTBLOCK] That's so radical![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So does that mean you're both a human and a darkmatter?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Actually, I'm a golem.[SOFTBLOCK] I just transform into a human when it's helpful.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Huh.[SOFTBLOCK] So if you're a golem, what does that make her?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Oh, I forgot![SOFTBLOCK] How rude of me![SOFTBLOCK] This is Unit 2855.[SOFTBLOCK] She's a Command Unit.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Greetings.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Keen![SOFTBLOCK] Wow, I've never seen one of those before![SOFTBLOCK] Are they all this cute?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Well, no, because most of them want you dead.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, I know.[SOFTBLOCK] Mother tells me all sorts of stories.[SOFTBLOCK] I've just never been fortunate enough to see one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Fortunate?[SOFTBLOCK] The stories your mother tells are likely negative.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Correct.[SOFTBLOCK] But so far, one-hundred percent of the Command Units I have seen are really cute.[SOFTBLOCK] I'm going to chalk that up as a positive.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You see, the stories are the problem.[SOFTBLOCK] We're mavericks. [SOFTBLOCK]We want to overthrow Regulus City's administration.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But then we walked into that trap and I accidentally transformed into a human and things have gotten out of control.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, I get it![SOFTBLOCK] If mother knew you were a golem, she'd probably think you were a spy.[SOFTBLOCK] Makes sense.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So, do you want me to speak to mother for you?[SOFTBLOCK] I'm not sure she'd believe me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That, unfortunately, is the problem.[SOFTBLOCK] I highly doubt she will appreciate all the duplicity.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[SOFTBLOCK] She'd likely assume I was converted and have me retired.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I was about to say that, actually.[SOFTBLOCK] Mother is extremely stubborn.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[SOFTBLOCK] That condition of yours...[SOFTBLOCK] Do you know why you have to spend so much recharging?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Something wrong with my power core.[SOFTBLOCK] I think it's a leak, but I have to be asleep to open me up.[SOFTBLOCK] You should ask TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ha, wait a minute now.[SOFTBLOCK] Are you thinking of fixing me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, you see, I'm a repair Unit. I run a whole repair shop, actually.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Well that explains why the other Steam Droids say you're good with repairs.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] But...[SOFTBLOCK] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well I'm not just any old mechanic.[SOFTBLOCK] I'm going to go talk to TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] All right.[SOFTBLOCK] You go do that, and 55 and I will stay out here...[SOFTBLOCK] and...[SOFTBLOCK] bond...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] !!!!!!!![SOFTBLOCK] Release me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
            
        --Dreamer!
        elseif(sChristineForm == "Eldritch") then
		
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Christine![SOFTBLOCK] Have a run in with the creepy crawlies?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You can tell it's me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] You don't see a full head of hair like that every day.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Funny, none of the other...[SOFTBLOCK] things...[SOFTBLOCK] ever talk.[SOFTBLOCK] I hear stories about them but they don't talk.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Can all of you talk?[SOFTBLOCK] Are you still you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes, but I'm rather unique...") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tadaa![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Woooooaaaahhhhh!!![SOFTBLOCK] Radical, Christine![SOFTBLOCK] Just radical![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So, uh, were you a golem before?[SOFTBLOCK] I'm a little confused.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I just transformed into a human, actually.[SOFTBLOCK] I was always a golem.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So then what's your friend there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] That's 55.[SOFTBLOCK] She's a Command Unit.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hello.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Keen![SOFTBLOCK] Wow, I've never seen one of those before![SOFTBLOCK] Are they all this cute?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Well, no, because most of them want you dead.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, I know.[SOFTBLOCK] Mother tells me all sorts of stories.[SOFTBLOCK] I've just never been fortunate enough to see one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Fortunate?[SOFTBLOCK] The stories your mother tells are likely negative.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Correct.[SOFTBLOCK] But so far, one-hundred percent of the Command Units I have seen are really cute.[SOFTBLOCK] I'm going to chalk that up as a positive.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You see, the stories are the problem.[SOFTBLOCK] We're mavericks. [SOFTBLOCK]We want to overthrow Regulus City's administration.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But then we walked into that trap and I accidentally transformed into a human and things have gotten out of control.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, I get it![SOFTBLOCK] If mother knew you were a golem, she'd probably think you were a spy.[SOFTBLOCK] Makes sense.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So, do you want me to speak to mother for you?[SOFTBLOCK] I'm not sure she'd believe me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That, unfortunately, is the problem.[SOFTBLOCK] I highly doubt she will appreciate all the duplicity.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[SOFTBLOCK] She'd likely assume I was converted and have me retired.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I was about to say that, actually.[SOFTBLOCK] Mother is extremely stubborn.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[SOFTBLOCK] That condition of yours...[SOFTBLOCK] Do you know why you have to spend so much recharging?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Something wrong with my power core.[SOFTBLOCK] I think it's a leak, but I have to be asleep to open me up.[SOFTBLOCK] You should ask TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ha, wait a minute now.[SOFTBLOCK] Are you thinking of fixing me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, you see, I'm a repair Unit. I run a whole repair shop, actually.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Well that explains why the other Steam Droids say you're good with repairs.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] But...[SOFTBLOCK] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well I'm not just any old mechanic.[SOFTBLOCK] I'm going to go talk to TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] All right.[SOFTBLOCK] You go do that, and 55 and I will stay out here...[SOFTBLOCK] and...[SOFTBLOCK] bond...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] !!!!!!!![SOFTBLOCK] Release me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Shocking!
        elseif(sChristineForm == "Electrosprite") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Christine![SOFTBLOCK] I love your hair![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You can tell it's me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] You stuck your hand in a power converter didn't you?[SOFTBLOCK] But it didn't change your jawline.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] It's a very strong jawline.[SOFTBLOCK] Inspires real confidence.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I was expecting you to freak out a bit more...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] It takes a lot to rattle me -[SOFTBLOCK] such as your friend there not being single.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Oh?)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 55, meet SX-399.[SOFTBLOCK] SX-399, meet 55.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Why is she looking at me this way?[SOFTBLOCK] Christine?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Noreasonatall - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So, SX-399, we figured you're a trustworthy sort![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ohhhh, you're about to ask me to keep a secret.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Nobody says [SOFTBLOCK]'Hey you're trustworthy'[SOFTBLOCK] if they're not about to ask you to keep a secret.[SOFTBLOCK] Nobody.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well we are, so good guess.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Brace yourself!") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tadaa![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I can transform myself.[SOFTBLOCK] It's magic, you see.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Best.[SOFTBLOCK] Secret.[SOFTBLOCK] EVER![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] This is so cool, Christine![SOFTBLOCK] And 55?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sorry, only I can do that.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Bummer.[SOFTBLOCK] Oh well.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So what is 55, then?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Uh, she's a doll, SX-399.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Command Unit...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, I get it now![SOFTBLOCK] That explains all the commotion earlier![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] You and 55 are, like, renegades or something and you don't think mother will trust her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] How far off am I?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Quite close, actually.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Do you want me to speak to mother for you?[SOFTBLOCK] I'm not sure she'd believe me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That, unfortunately, is the problem.[SOFTBLOCK] I highly doubt she will appreciate all the duplicity.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[SOFTBLOCK] She really doesn't like golems or dolls.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Command Unit![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] [EMOTION|2855|Neutral]I was about to say that, actually.[SOFTBLOCK] Mother is extremely stubborn.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[SOFTBLOCK] That condition of yours...[SOFTBLOCK] Do you know why you have to spend so much recharging?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Something wrong with my power core.[SOFTBLOCK] I think it's a leak, but I have to be asleep to open me up.[SOFTBLOCK] You should ask TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ha, wait a minute now.[SOFTBLOCK] Are you thinking of fixing me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, you see, I'm a repair unit in Regulus City.[SOFTBLOCK] I run a whole repair shop, actually.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Well that explains why the other Steam Droids say you're good with repairs.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] But...[SOFTBLOCK] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well I'm not just any old mechanic.[SOFTBLOCK] I'm going to go talk to TT-233.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] All right.[SOFTBLOCK] You go do that, and 55 and I will stay out here...[SOFTBLOCK] and...[SOFTBLOCK] bond...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] !!!!!!!![SOFTBLOCK] Release me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
            
        --Unhandled form.
        else
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wow, Christine.[SOFTBLOCK] You're in a form Salty hasn't written a handler for yet![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] That lazybones had to release a prototype but didn't have time to finish and test all the forms. Sorry.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] He already knows so you don't need to file a bug report. This will get updated soon.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Okay. We'll just pretend like the cutscene ran anyay.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Your next task is to speak to TT-233, the Steam Droid doctor in the Fist of Tomorrow headquarters.") ]])
        
		end
	
	--Time to come up with a plan.
	elseif(iSpokeWith55AboutSX == 1.0 and iPlanHatched == 0.0) then
		
		--If 55 isn't following:
		if(iIs55Following == 0.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Something on your mind, Christine?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, but I want 55 here before I tell you.[SOFTBLOCK] It's important.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I'd best go get 55, she wouldn't forgive me if I left her out of this...)") ]])
			
		--55 is following.
		else
		
			--Execute.
			TA_SetProperty("Face Character", "PlayerEntity")
			LM_ExecuteScript(fnResolvePath() .. "SX399Plan.lua")
			
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
			VM_SetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N", 1.0)
		end
	
	--55 isn't following but SX-399 doesn't know Christine can transform:
	elseif(iSXKnowsChristineTransforms == 0.0 and sChristineForm ~= "Human") then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
		--Golem:
		if(sChristineForm == "Golem") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Hey Christine, nice dress.[SOFTBLOCK] Have you got any in my size?[SOFTBLOCK] I think it might look good on me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Now, don't be alarmed, SX-399...[SOFTBLOCK] I'm not like the other Golems.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ohhhh, I was wondering about that.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I didn't actually know what a Golem looks like.[SOFTBLOCK] But, I must say, you look lovely.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Why thank you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Did you get converted or something?[SOFTBLOCK] Mother said that's what they do to humans they capture.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] If you're one of their slaves...[SOFTBLOCK] No, I must be missing something here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This, actually.") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I can transform myself, you see.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ohmygoshthisissokeen![SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This magical runestone here.[SOFTBLOCK] I don't know how it works, but it does.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wow![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But you can't tell anyone.[SOFTBLOCK] Your mother despises Golems and I don't want to upset her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I don't think anyone would believe me, but don't worry.[SOFTBLOCK] Your secret is safe with me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Very good.[SOFTBLOCK] Thank you.") ]])
			fnCutsceneBlocker()
			
		--LatexDrone:
		elseif(sChristineForm == "LatexDrone") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wow, nice bondage outfit, Christine.[SOFTBLOCK] Not that there's anything wrong with that.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Now, don't be alarmed, SX-399...[SOFTBLOCK] I'm not like the other Golems.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ohhhh, I was wondering about that.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I didn't actually know what a Golem looks like.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, well I'm what's called a Drone Unit.[SOFTBLOCK] It's one of our model variants.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Did you get converted or something?[SOFTBLOCK] Mother said that's what they do to humans they capture.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] If you're one of their slaves...[SOFTBLOCK] No, I must be missing something here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This, actually.") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I can transform myself, you see.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ohmygoshthisissokeen![SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This magical runestone here.[SOFTBLOCK] I don't know how it works, but it does.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wow![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But you can't tell anyone.[SOFTBLOCK] Your mother despises Golems and I don't want to upset her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I don't think anyone would believe me, but don't worry.[SOFTBLOCK] Your secret is safe with me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Very good.[SOFTBLOCK] Thank you.") ]])
			fnCutsceneBlocker()
	
        --Darkmatter!
        elseif(sChristineForm == "Darkmatter") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Well there's something you don't see every day. Is that a black hole on your chest?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Now, don't be alarmed, SX-399...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, it is! It's a real black hole![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, I mean, I'm a friend. I'm Christine![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Yes, and? It's pretty obvious.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, good![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I didn't even know they made more Darkmatters! I'm impressed![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Get ready to be more impressed...") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I can transform myself, you see.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ohmygoshthisissokeen![SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This magical runestone here.[SOFTBLOCK] I don't know how it works, but it does.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wow![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Though I'd appreciate it if you kept it a secret.[SOFTBLOCK] I'm actually a golem, I just turned into a human for...[SOFTBLOCK] reasons.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I don't think anyone would believe me, but don't worry.[SOFTBLOCK] Your secret is safe with me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Very good.[SOFTBLOCK] Thank you.") ]])
			fnCutsceneBlocker()
            
        --Dreamer!
        elseif(sChristineForm == "Eldritch") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Hey, Christine![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Oh come on![SOFTBLOCK] I picked my scariest and weirdest form![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Scariest?[SOFTBLOCK] Oh that's a stretch.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Mother when she mentions her war stories?[SOFTBLOCK] Now that's a scary form.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How so?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Not scary in the angry way, but in the sad kind of way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] She gets quiet.[SOFTBLOCK] She starts to mumble.[SOFTBLOCK] She's always so strong and then that comes up.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, father was like that sometimes, too.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Was he in war?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, he was.[SOFTBLOCK] But he would have been on the other side.[SOFTBLOCK] He would have been with the oppressors.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hey![SOFTBLOCK] Forget about that, watch this!") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I can transform myself, you see.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wow, keen![SOFTBLOCK] That's amazing![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This magical runestone here.[SOFTBLOCK] I don't know how it works, but it does.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wow![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] By the way, what was that thing you were before?[SOFTBLOCK] I've heard a lot of stories from the patrols about weird creepies in the mines.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I think the best term is 'dreamers', but they don't really have a name.[SOFTBLOCK] They're beyond names.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] That's kind of cool in a way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Though I'd appreciate it if you kept it a secret.[SOFTBLOCK] I'm actually a golem, I just turned into a human for...[SOFTBLOCK] reasons.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I don't think anyone would believe me, but don't worry.[SOFTBLOCK] Your secret is safe with me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Very good.[SOFTBLOCK] Thank you.") ]])
			fnCutsceneBlocker()
            
        --Shocking!
        elseif(sChristineForm == "Electrosprite") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Of all the things I expected to happen today, seeing you waltz up to me as a lightbulb was not one of them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, good.[SOFTBLOCK] You can tell it's me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Well yeah.[SOFTBLOCK] You've got an air to you that's hard to mistake.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Such a relief![SOFTBLOCK] I was worried you'd not believe me and we'd spend a hour doing ridiculous quizzes.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So uh, what are you?[SOFTBLOCK] Did you run into a rogue lightbulb girl and she transformed you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm an electrosprite.[SOFTBLOCK] They're a species of intelligent electricity from a long-running computer experiment.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Really?[SOFTBLOCK] So the characters in video games are real people?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Not yet, unfortunately...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Brief change of topic.[SOFTBLOCK] Watch this!") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tadaa![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] What in the world did I just watch happen?[SOFTBLOCK] How are you human again?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This magical runestone here.[SOFTBLOCK] I don't know how it works, but it does.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Wow![SOFTBLOCK] That's pretty neat![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So are you like, a human underneath the lightbulb?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Golem, actually![SOFTBLOCK] I just change forms when it's useful in a fight or otherwise.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Though I'd appreciate it if you kept it a secret.[SOFTBLOCK] Your steam droid compatriots are not fans of golems.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I don't think anyone would believe me, but don't worry.[SOFTBLOCK] Your secret is safe with me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Very good.[SOFTBLOCK] Thank you.") ]])
			fnCutsceneBlocker()
        
        --Unhandled case.
        else
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Hey Christine![SOFTBLOCK] Looks like you're in a form that isn't handled yet.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Yeah.[SOFTBLOCK] Salty knows already and will be updating this later.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Typical.[SOFTBLOCK] Hey![SOFTBLOCK] Transform for me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay.") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Bim bop pow![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Tubular![SOFTBLOCK] Now the flag is set.[SOFTBLOCK] Resume playing the game!") ]])
			fnCutsceneBlocker()
    
		end
	
	--Black site is completed:
	elseif(iSteamDroid500RepState == 2.0) then
	
		--55 is here:
		if(iIs55Following == 1.0) then
			
			--Variables.
			local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
			if(iSXUpgradeQuest == 1.0) then
				
				--Dialogue.
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Ready to get this thing going?[BLOCK]") ]])

				--Decision script is this script. It must be surrounded by quotes.
				local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
				fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
				fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's Go\", " .. sDecisionScript .. ", \"Yes\") ")
				fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"No\") ")
				fnCutsceneBlocker()
			
			--First time.
            elseif(iSXUpgradeQuest < 3.0) then
            
				--Flag.
				VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 1.0)
				
				--Dialogue.
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Everyone else has already come back from the job.[SOFTBLOCK] They said you were the rear guard.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So...[SOFTBLOCK] did you...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Mission successful.[SOFTBLOCK] We located the blueprints we require.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, about that.[SOFTBLOCK] Did you know that they'd be there?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A guess.[SOFTBLOCK] I managed to access some transit manifests and noticed that a disproportionately high number of Steam Droid prisoners were diverted to that site.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There must have been a reason.[SOFTBLOCK] I did not think they would set up the site in a disused workshop.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We saw some things in there...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Mother said you rescued four prisoners while you were in there.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] If this doesn't work out, or something goes wrong, then I just want you to know that you're real heroes.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are not heroes.[SOFTBLOCK] We are performing the tasks laid out for us in the most efficient manner possible.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Humble, tough, and cute.[SOFTBLOCK] Shame you're not much of a sweet-talker.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] !!![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Uh oh, better change the subject!)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So, I took a photo of the blueprints we found.[SOFTBLOCK] Sophie thinks she can scavenge up some parts, but I can't make the power core until I figure out what these symbols mean.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Hmmm, oh![SOFTBLOCK] Ha ha, these are how we used to denote volume, pressure, and temperature way back when.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Just do this, do that, carry the y...[SOFTBLOCK] There.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] I feel like such a dope.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ...[SOFTBLOCK] Therefore, we can begin the transformation.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Yes, but I'll need to supervise.[SOFTBLOCK] I'll know how to revert you if you make a mistake.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So, are you ready?[BLOCK]") ]])

				--Decision script is this script. It must be surrounded by quotes.
				local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
				fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
				fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's Go\", " .. sDecisionScript .. ", \"Yes\") ")
				fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"No\") ")
				fnCutsceneBlocker()
                
            --Finale has completed, SX-399 is a Steam Lord.
            else
				
				--Dialogue.
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Hey guys![SOFTBLOCK] How are you?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I was meaning to ask you the same.[SOFTBLOCK] The new body is holding up well?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] I've never felt better, and I'm the envy of the town![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] ...[SOFTBLOCK] You know, I don't think I've ever had the chance to brag about something.[SOFTBLOCK] Do you think the other droids resent me?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Doubtless some of them do, but you can't let that hold you back.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If you're kind and compassionate, that resentment will fade.[SOFTBLOCK] If you are cruel and vain, it will fester.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] How they see you will, eventually, reflect how you are.[SOFTBLOCK] Be the person they love.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Are you speaking from experience here?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] I wish I was.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] In truth, there will be those who resent you forever.[SOFTBLOCK] You can do nothing about that.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's not that they hate you, it's that they hate what they think you are, and they hate it so much that they don't care if it's real or not.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You could have them imprisoned.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55, no![SOFTBLOCK] Sheesh![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Those who speak ill of SX-399 are a problem to be eliminated.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Really?[SOFTBLOCK] Because this sounds like an overprotective girlfriend talking.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Speaking of...[SOFTBLOCK] *rub*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] Ngh![SOFTBLOCK] No![SOFTBLOCK] I retract my statement![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will not act on this without explicit orders.[SOFTBLOCK] The Steam Droids may think what they like.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Your reinforced chassis is putty in my hands...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Come on, 55.[SOFTBLOCK] We better get back to it before you melt.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Good luck out there.[SOFTBLOCK] I'll keep to my training, and some day, I'll be the one who saves you two!") ]])
            
			end
	
		--55 is not here, go get her!
		else
			
			--Variables.
			local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
			if(iSXUpgradeQuest == 1.0) then
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] You had better go get 55, she can protect us while you're making your power core.") ]])
				fnCutsceneBlocker()
				
            elseif(iSXUpgradeQuest < 3.0) then
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Everyone else has already come back from the job.[SOFTBLOCK] They said you were the rear guard.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So...[SOFTBLOCK] did you...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yep, I'll go get 55 and we can begin.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Just, keen![SOFTBLOCK] Eee![SOFTBLOCK] I feel lighter than air!") ]])
				fnCutsceneBlocker()
                
            --Finale has completed, SX-399 is a Steam Lord.
            else
				
				--Dialogue.
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "SteamLord") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Uh, it really shouldn't be possible to see this dialogue.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Better report it as a bug.") ]])
				fnCutsceneBlocker()
			end
		end
	
	--No special scenes:
	else
	
		--The plan has been hatched:
		if(iPlanHatched == 1.0) then
			
			--55 is here:
			if(iIs55Following == 1.0) then
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] This plan...[SOFTBLOCK] It'll work, right?[SOFTBLOCK] I won't have to...[SOFTBLOCK] to...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It will work.[SOFTBLOCK] We will save you.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Thank you so much...") ]])
				fnCutsceneBlocker()
			
			--55 is not here:
			else
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Christine, everything changed the moment I met you and 55.[SOFTBLOCK] If this plan doesn't work, or something happens...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It will work.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Yes, but -[SOFTBLOCK] just, thank you.[SOFTBLOCK] Thank you for everything.") ]])
				fnCutsceneBlocker()
	
			end
	
		--If 55 is following:
		elseif(iIs55Following == 1.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Stick around for a while, 55.[SOFTBLOCK] Have you got any stories about your adventures?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...") ]])
			fnCutsceneBlocker()
			
		--55 isn't following.
		elseif(iSXMet55 == 1.0 and iIs55Following == 0.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Christine...[SOFTBLOCK] Do you think 55 likes me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's difficult to get a read on her, honestly.[SOFTBLOCK] You need to be patient with her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] When I touch her, she goes stiff and acts all weird.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She's not used to affection, believe me...") ]])
			fnCutsceneBlocker()
		
		elseif(iSteamDroid250RepState == 3.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N", 4.0)
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Christine! Hello![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How are things going with the traps?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I'll be honest.[SOFTBLOCK] This isn't the most entertaining work there is.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] But I saw BI-102 earlier, and she nodded at me![SOFTBLOCK] Like I was doing a good job![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Great![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I'm still getting used to this.[SOFTBLOCK] I have to run along the trap lines and make sure they're all lubricated and that the springs aren't oversensitive.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] But I get to set my own hours and I don't have to sneak out![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You've got responsibility now, so no slacking off.[SOFTBLOCK] Sprocket City is counting on you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I'll keep the city safe, don't worry.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] And thank you for talking to mother for me.[SOFTBLOCK] She can be very obstinate.") ]])
			fnCutsceneBlocker()
				
		elseif(iSteamDroid250RepState == 4.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] BI-102 mentioned you fell into this trap earlier.[SOFTBLOCK] Is that true?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Errrr....[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I'm amazed you survived![SOFTBLOCK] You must be as sturdy as a golem![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You don't know the half of it...") ]])
			fnCutsceneBlocker()
		end
	end

--Begin the Steam Droid transformation.
elseif(sTopicName == "Yes") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Let's get to work.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will retrieve the parts from Unit 499323.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] There's a spot in the mines we can use, all we need to do is tap into Sprocket City's steam pipes.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] 55, meet us there when you've got the parts.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 1.5)
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Remove 55 from the party.
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AL_SetProperty("Unfollow Actor Name",  "55")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
	
	--Execute Steam Droid transformation.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_SteamDroidFirst/Scene_Begin.lua") ]])
	fnCutsceneBlocker()

--Cancel.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I have a few loose-ends to tie up.[SOFTBLOCK] I'll be back.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Don't leave me waiting too long, hun.") ]])
end