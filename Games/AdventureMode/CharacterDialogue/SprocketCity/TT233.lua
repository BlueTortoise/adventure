--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Common.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local iSXMet55        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
	local iSpokeWithTT233 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N")
	
	--Normal dialogue.
	if(iSXMet55 == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[VOICE|TT-233] I'm not really much of a doctor, I used to work as a veterinarian.[SOFTBLOCK] You're lucky we found a shipment of repair nanites that would work on organic tissue.") ]])
		fnCutsceneBlocker()
	
	--Dialogue about SX-399.
	elseif(iSXMet55 == 1.0 and iSpokeWithTT233 == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "TT-233", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: Good morrow, Christine.[SOFTBLOCK] How are your legs feeling?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A little tired from all the running around, but otherwise wonderful.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Say, I heard about SX-399's condition...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: Listen, I know you want to help, but it's best to stay out of it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, I'm good at repairs.[SOFTBLOCK] I could give it a try.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: Oh...[SOFTBLOCK] What exactly have you heard?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I spoke to SX-399 and she said she needs to recharge for most of the year because her core is leaking.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: Yes...[SOFTBLOCK] That's what we told her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: It's for her own good that she believes that...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: *Sigh*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: Christine, please don't get her hopes up.[SOFTBLOCK] Believe me when I say that we've tried.[SOFTBLOCK] We can't fix her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: It's a wonder she's alive at all.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Really?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: We Steam Droids are made of old, flawed technology.[SOFTBLOCK] We break down every now and then, but we can usually be fixed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: But, SX-399 is different.[SOFTBLOCK] I don't know if it's because she was converted when she was so young, or maybe her body just couldn't handle the change.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: She's fundamentally damaged.[SOFTBLOCK] If you replace her power core, it will break again within the day.[SOFTBLOCK] We've tried.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh dear...[SOFTBLOCK] What about the connectors?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: Fried.[SOFTBLOCK] I think it's her full system distribution, but the only way to find out would...[SOFTBLOCK] kill her...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But there's got to be something we can do.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: No.[SOFTBLOCK] There isn't.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: My estimate is that she's got one, maybe two more cycles before she just...[SOFTBLOCK] doesn't wake up again.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: She'd still be alive, but the distribution system wouldn't produce enough power to keep her conscious.[SOFTBLOCK] She's been slowly losing power cohesion more each year.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And you haven't told her?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: JX-101's orders.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: Do you want to be the one who tells a girl she's got maybe ten days of lucidity before she dies?[SOFTBLOCK] Better that she goes to sleep one day, happy and secure, than deal with the anxiety.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But you can't just keep her in the dark like that![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: Not my call.[SOFTBLOCK] We respect JX-101's decision.[SOFTBLOCK] I happen to agree with it.[SOFTBLOCK] Even if you don't, you best not tell her.[SOFTBLOCK] Not if you want to live in Sprocket City.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Yeah, JX-101 wouldn't help our rebellion if I did that...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay, I won't tell her.[SOFTBLOCK] But I'm not giving up.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233: Believe me, I want you to succeed.[SOFTBLOCK] But...[SOFTBLOCK] We've been trying for a long time...") ]])
		fnCutsceneBlocker()
		
	--Dialogue about SX-399.
	elseif(iSXMet55 == 1.0 and iSpokeWithTT233 == 1.0) then
		
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        
        if(iSXUpgradeQuest < 2.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[VOICE|TT-233] Please don't tell SX-399 the truth.[SOFTBLOCK] It's for the best.") ]])
        elseif(iSXUpgradeQuest == 2.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[VOICE|TT-233] Oh, SX-399![SOFTBLOCK] I thought you'd be recharging by now.[SOFTBLOCK] Don't stay up too late.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[VOICE|TT-233] Christine, I'm astounded by what you've done.[SOFTBLOCK] SX-399 owes you her life, and I owe you an apology.[SOFTBLOCK] I guess it really was possible to upgrade a Steam Droid.") ]])
        end
        
        --Common.
        fnCutsceneBlocker()
    
	end
end