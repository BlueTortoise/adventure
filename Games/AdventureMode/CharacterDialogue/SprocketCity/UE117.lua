--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iMetUE117 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetUE117", "N")
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	
    --Impersonation.
    if(iSXUpgradeQuest == 2.0) then
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SH-505:[VOICE|Steam Droid] 'ello, loves?[SOFTBLOCK] Enjoying the art studio?") ]])
	
	--First meeting.
	elseif(iMetUE117 == 0.0) then
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "UE-117:[VOICE|Steam Droid] 'ello there, name's UE-117.[SOFTBLOCK] Pleased to meet ye.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Oh my goodness, is that a Portsmouth accent?[SOFTBLOCK] Are you from the UK?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "UE-117:[VOICE|Steam Droid] Never 'eard of it.[SOFTBLOCK] What's a UK?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Oh, nevermind.[SOFTBLOCK] Must be a coincidence.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] So, what's this piece here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "UE-117:[VOICE|Steam Droid] I call it 'Harley the Dog'.[SOFTBLOCK] He's a chihuahua.[SOFTBLOCK] You like it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] It's just lovely.[SOFTBLOCK] Well done![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "UE-117:[VOICE|Steam Droid] Wunnaful![SOFTBLOCK] I've just started carving, but I'm getting better every day.") ]])
		fnCutsceneBlocker()
		
		VM_SetVar("Root/Variables/Chapter5/Scenes/iMetUE117", "N", 1.0)
	else
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "UE-117:[VOICE|Steam Droid] Most 'a these pieces are by SH-505.[SOFTBLOCK] I prefer to do pixel work and carvings.") ]])
		fnCutsceneBlocker()
	end
end