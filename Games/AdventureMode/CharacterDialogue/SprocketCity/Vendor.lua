--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketHats = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketHats", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0 or iSprocketHats == 1.0) then
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] I've got stuff from Gearsville to Steamston.[SOFTBLOCK] What can I get you?") ]])
        fnCutsceneInstruction([[ AM_SetShopProperty("Show", "Steam Droid Merchant", gsRoot .. "CharacterDialogue/SprocketCity/Shop Setup.lua", gsRoot .. "CharacterDialogue/SprocketCity/Shop Teardown.lua") ]])
        fnCutsceneBlocker()
	
	--HAAAAAAAAAAAAAAATS
	else
		TA_SetProperty("Face Character", "PlayerEntity")
		LM_ExecuteScript(gsRoot .. "Maps/RegulusMines/SprocketCityA/SpecialScripts.lua", "Hats")
	end
	
end