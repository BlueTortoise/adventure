--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.
-- Note that this is used for chat dialogues, not the part in Sector 198.

--[Arguments]
--Argument Listing:
-- 0: sTopicName  - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
-- 1: sIsFireside - Optional. If it exists, this was activated from the AdventureMenu at a save point. In those cases, the 
--                  Active Object is undefined.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName  = LM_GetScriptArgument(0)
local sIsFireside = LM_GetScriptArgument(1)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Change music to Florentina's theme.
	glLevelMusic = AL_GetProperty("Music")
	AL_SetProperty("Music", "SprocketCity")
	
	--Position party opposite one another.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
	
	--Florentina's introduction.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Christine, is it a bad time to ask you for some advice?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Never a bad time, my dear![SOFTBLOCK] What can I help you with?[BLOCK][CLEAR]") ]])

	--[Topics]
	--Activate topics mode once the dialogue is complete.
    fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "SX399") ]])
end
