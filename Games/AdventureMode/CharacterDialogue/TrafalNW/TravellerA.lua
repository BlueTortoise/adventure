--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iPlatina = AdInv_GetProperty("Platina")
	local iHasCatalystTone = VM_GetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N")
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Dialogue.
	WD_SetProperty("Show")
	
	--If Mei has the Platinum Compass...
	if(iHasCatalystTone == 1.0) then
		WD_SetProperty("Append", "Trader:[VOICE|MercF] This delay is going to eat into our profits...")
		
	--Opportunity to buy the compass.
	else
	
		--Dialogue.
		WD_SetProperty("Append", "Trader:[VOICE|MercF] Ugh, there was a rockslide.[SOFTBLOCK] Now I've got all this stock and we're going to have to climb or take the long way.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Trader:[VOICE|MercF] You...[SOFTBLOCK] wouldn't happen to want to purchase a Platinum Compass would you?[SOFTBLOCK] It sounds a chime if there's a Catalyst nearby.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Trader:[VOICE|MercF] Very useful if you're an adventuring sort.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[VOICE|Mei] Just how much would this compass be?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Trader:[VOICE|MercF] 700 Platina.[SOFTBLOCK] I can't go lower than that, sorry.[SOFTBLOCK] The trading company would kill me.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[VOICE|Mei] But you need to get rid of it, right?[SOFTBLOCK] It's too heavy?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Trader:[VOICE|MercF] Oh, no.[SOFTBLOCK] It's a compass.[SOFTBLOCK] I just hate that damn chime when I'm out walking.[BLOCK][CLEAR]")
		
		--Mei doesn't have enough Platina...
		if(iPlatina < 700) then
            WD_SetProperty("Append", "Trader:[VOICE|MercF] So do you want it or what?[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[VOICE|Mei] Well, uh, you see...[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Trader:[VOICE|MercF] Don't sweat it if you don't have the cash.[SOFTBLOCK] We're going to be here a while...")
		
		--Mei has the cash.
		else
            WD_SetProperty("Append", "Trader:[VOICE|MercF] So do you want it or what?[BLOCK]")
		
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Buy\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Dont\") ")
			fnCutsceneBlocker()
		end
	end
	
--"Buy", purchase the compass.
elseif(sTopicName == "Buy") then

	--Dialogue.
	WD_SetProperty("Hide")
	WD_SetProperty("Show")
	WD_SetProperty("Append", "Trader:[SOUND|Menu|BuyOrSell][VOICE|MercF] Superb![SOFTBLOCK] Enjoy your treasure hunting![BLOCK][CLEAR]")
	WD_SetProperty("Append", "*The compass will play a tone when you enter an area that has a catalyst.*")
	
	--Vars.
	AdInv_SetProperty("Remove Platina", 700)
	VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 1.0)
	
--"Dont", don't purchase the compass.
elseif(sTopicName == "Dont") then

	--Dialogue.
	WD_SetProperty("Hide")
	WD_SetProperty("Show")
	WD_SetProperty("Append", "Trader:[VOICE|MercF] Hmm, well, if you change your mind...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Trader:[VOICE|MercF] We're not exactly in a rush here.[SOFTBLOCK] Good luck.")
end
