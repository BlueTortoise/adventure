--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local iMetAlicia = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetAlicia", "N")
	local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	
	--First meeting.
	if(iMetAlicia == 0.0) then
		
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMetAlicia", "N", 1.0)
		
		--Florentina is not here:
		if(iHasSeenTrannadarFlorentinaScene == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Ah, a customer![SOFTBLOCK] I hope you've got a good story, as I'm not serving boring clients.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're running a shop?[SOFTBLOCK] Way out here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Huh.[SOFTBLOCK] You haven't heard of me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Alicia of Jeffespeir?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nope.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] World-famous gemcutter?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nothing.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] The recluse who'd turn down a duchess for being tawdry?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Sorry.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Well that is a special kind of ignorance.[SOFTBLOCK] Where are you from that you've not heard of Alicia Neuroth?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hong Kong.[SOFTBLOCK] Hey, you wouldn't know how to get there, would you?[SOFTBLOCK] I'm a little lost.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] ...[SOFTBLOCK] I have traversed every corner of this world and never heard that name before.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] ...[SOFTBLOCK] And thus I am intrigued.[SOFTBLOCK] So, I may decide to cut gems for you after all.[SOFTBLOCK] Well played.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Erm, why would I need gems cut again?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Just -[SOFTBLOCK] fascinating![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Your sword, have you noticed the little notch there on the grip?[SOFTBLOCK] You can insert gems into those.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] It's not just for appearance's sake, mind.[SOFTBLOCK] The magic instilled into the gem flows around the sword and enhances it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Oh so [SOFTBLOCK]*that's*[SOFTBLOCK] what those are for.[SOFTBLOCK] I've been kind of winging it since I got here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Well wing it no more, my friend, for you have the finest gem cutter in the lands at your service.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Okay, so how do I get a gem cut?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] I have plenty of raw gems,[SOFTBLOCK] for a reasonable price, of course.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] But the cutting process requires Adamantite.[SOFTBLOCK] With it, I can reshape a gem, and fit it into another.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] A gem placed inside another has the properties of both, but there is a catch.[SOFTBLOCK] No amount of adamantite can merge two gems that share a color.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] And when two gems merge, they merge colors as well.[SOFTBLOCK] So, it's my job to make your weapons more powerful by making your gems more powerful.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh, excellent![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] So, you live out here all by yourself and you let customers come to you.[SOFTBLOCK] I get it now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] By myself, no.[SOFTBLOCK] My friend, I live here with my wife.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] She's at the lake south of here fishing.[SOFTBLOCK] You should go say hello![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Okey dokey.[SOFTBLOCK] Thanks for the advice, Ms. Alicia.") ]])
			fnCutsceneBlocker()
		
		--Florentina is here:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Florentina, back at it again, are you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do you know this lady, Florentina?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Oh yeah.[SOFTBLOCK] This is Neuroth the famous gemcutter.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Famous for being a real headache, that is.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] My first name is Alicia.[SOFTBLOCK] You'd do well to learn it, glumprongbutt.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Okay, that's it.[SOFTBLOCK] You wanna fight?[SOFTBLOCK] Let's go![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Florentina, no![SOFTBLOCK] Stop![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Any time, any place, mulchbreath![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Both of you, grow up![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] So it's clear there's some bad blood here.[SOFTBLOCK] What did you do to get each other so angry?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] I paid Florentina here fifty platina for a batch of fresh eggs, and when I got them, they were rotten![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You paid for them on monday and picked them up two sundays later.[SOFTBLOCK] It's not my fault they went bad![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Give[SOFTBLOCK] me[SOFTBLOCK] my[SOFTBLOCK] EGGS![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Make me![SOFTBLOCK][SOUND|World|Thump][BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Stop it![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Miss Alicia, I am terribly sorry about the eggs.[SOFTBLOCK] I'll cover the difference.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] ...[SOFTBLOCK] You will?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] You -[SOFTBLOCK] will?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Honestly, throwing a punch over a bunch of eggs![SOFTBLOCK] Are we children or adults?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] ...[SOFTBLOCK] Children![SOFTBLOCK][SOUND|World|Thump][BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Owch![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Florentina![SOFTBLOCK][SOUND|World|Thump][BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Booogh!![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Well, that was just a superb hit![SOFTBLOCK] Where'd you learn to punch like that?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uh, I didn't.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] ...[SOFTBLOCK] Sorry, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] Puh...[SOFTBLOCK] Didn't hurt...[SOFTBLOCK] Ooh oww...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] What's your name, madam?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Mei.[SOFTBLOCK] I hope you can forgive the violence.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] ...[SOFTBLOCK] Well, as you've heard from your blue friend, I am Alicia Neuroth, gemcutter.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] I do business with who I care to, and people seek me out from across the land.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, I see.[SOFTBLOCK] So this is your workshop?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Quite![SOFTBLOCK] I live here with my wife, Ginny.[SOFTBLOCK] She's out fishing right now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Now, I happen to have taken a liking to you, so.[SOFTBLOCK] Gems.[SOFTBLOCK] Would you like one cut?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And why would I need a gem cut again?[SOFTBLOCK] Sorry if I'm ignorant.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] Ugh...[SOFTBLOCK] right in the stomach...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Oh, I really got you didn't I?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] Thatdidn'thurtshutup...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Gems can be socketed into equipment.[SOFTBLOCK] That's what those little slots on your sword's hilt are for.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] The magic of the gem flows into the weapon, enhancing it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Attach too many gems to a weapon and they interfere with each other.[SOFTBLOCK] Don't socket more into a weapon than it can handle.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] As you might imagine, a finely-cut gem is therefore worth a great deal.[SOFTBLOCK] Quantity is nothing, quality is everything.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] I have some simple gems here for sale, but what I really need is Adamantite.[SOFTBLOCK] With that I can draw out the true power of the gemstone.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] So, I get you the Adamantite, and a gem, and you can make it a better gem?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Sort of -[SOFTBLOCK] you bring me *two* gems, and I can make *one* better gem, by merging them together.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] A merged gem has the properties of both, and the colors. No amount of adamantite can merge two gems of the same color.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] You've only go so many sockets, so you can see how a gemcutter like me can give you a crucial edge.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Of course, if you're a head for battle, then you'll spare no expense.[SOFTBLOCK] I imagine you know, by now, how slim the margin between life and death can be.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Thank you very much, Miss Neuroth![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Any time.[SOFTBLOCK] And, Florentina?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] What?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Try not to rub off on this very nice girl.[SOFTBLOCK] She's sweet.[SOFTBLOCK] Keep it that way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Yeah, real sweet.[SOFTBLOCK] And then she sucker punches you...") ]])
		
		end
		
	--Successive meetings.
	else
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Hello![SOFTBLOCK] What can I do for you?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Can you cut some gems?\",  " .. sDecisionScript .. ", \"Gemcutting\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"How does gemcutting work?\", " .. sDecisionScript .. ", \"Instructions\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Just saying hi.\", " .. sDecisionScript .. ", \"Hi\") ")
		fnCutsceneBlocker()
	
	end

--Opens the gemcutting interface.
elseif(sTopicName == "Gemcutting") then

	--Dialogue.
	WD_SetProperty("Hide")
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Could I bother you for some gem cutting?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] No bother at all, let's see what I can do with your gems!") ]])

	--Run the shop.
	fnCutsceneInstruction([[ AM_SetShopProperty("Show", "Alicia's Gem Shop", gsRoot .. "CharacterDialogue/Trannadar/AliciaShopSetup.lua", "Null") ]])
	fnCutsceneBlocker()

--Description of how gemcutting works.
elseif(sTopicName == "Instructions") then
	
	--Dialogue.
	WD_SetProperty("Hide")
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Not to be annoying, but can you give me some details on gemcutting?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Well, let's see.[SOFTBLOCK] I suppose I can go over the basics for you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] There are lots of gems out there with magic in them.[SOFTBLOCK] Yemite, Rubose, Glintsteel...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] If you can't find any, I sell some raw ones, too.[SOFTBLOCK] Sometimes Ginny finds them washed up on the lakeshore.[SOFTBLOCK] She really likes shiny things, don't get me started![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Every gem has a color, and some properties, like an increase in attack power.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] The color is important, as I cannot merge together two gems that share a color.[SOFTBLOCK] And when I merge gems, it gains the color of both.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] With each cut, the gem gains more properties, and more colors, and takes more pure adamantite to cut again.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Since there's a limit to how many gems you can socket into a piece of equipment, the better the gems, the better the gear.[SOFTBLOCK] So, be on the lookout for rare and pure adamantite.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Socketing powerful gems into a weapon can make even a rusty piece of junk into a deadly implement.[SOFTBLOCK] In the right hands, of course.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Oh, and if you unequip something, the gems are unsocketed automatically.[SOFTBLOCK] So, you don't need to worry about accidentally selling something with gems in it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Great![SOFTBLOCK] Thanks, Alicia![SOFTBLOCK] I think I get it now.") ]])

--Effectively the same as goodbye.
elseif(sTopicName == "Hi") then
	
	--Variables.
	local iMetGinny       = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetGinny", "N")
	local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
	
	--Dialogue.
	WD_SetProperty("Hide")
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I was just in the area and thought I'd say hello.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] It's nice to talk about something other than business sometimes.[SOFTBLOCK] Have you met Ginny yet?[BLOCK][CLEAR]") ]])
	
	--Switching dialogue.
	if(iMetGinny == 1.0) then
		if(iHasWerecatForm == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh yes, she's a very strong and pretty fang![SOFTBLOCK] I'm a little jealous![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] You know, she sometimes calls me a fang by mistake.[SOFTBLOCK] And then she gets all nervous.[SOFTBLOCK] If you've never seen a werecat get flustered, it's quite a sight.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] But don't let me keep you, good luck out there!") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] She's the very nice werecat at the lake?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Indeed![SOFTBLOCK] She prefers fishing to the more conventional hunting of the other werecats, and I happen to love trout.[SOFTBLOCK] A match made in heaven![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] Ah, but I've prattled long enough.[SOFTBLOCK] Good luck on your adventure.") ]])
		end
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We're not acquainted, sorry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] She spends most of her day fishing at the dock south of here, we even set up a hut for her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alicia:[E|Neutral] If you're going down that way, say hello to her.[SOFTBLOCK] Good luck, my friend!") ]])
	end
end
