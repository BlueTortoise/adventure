--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local iMetGinny = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetGinny", "N")
	local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	
	--First meeting.
	if(iMetGinny == 0.0) then
		
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ginny", "Neutral") ]])
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMetGinny", "N", 1.0)
		
		--Florentina is not here:
		if(iHasSeenTrannadarFlorentinaScene == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat:[E|Neutral] Oh, hello.[SOFTBLOCK] Are you here to purchase fresh fish?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] You're -[SOFTBLOCK] are you Ginny?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Does my reputation precede me?[SOFTBLOCK] I am a fine fisher, I suppose, but not that fine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, when Alicia mentioned her wife...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Ah, Alicia sent you![SOFTBLOCK] Very good, but it's not time for dinner quite yet.[SOFTBLOCK] Tell her I'll be at least another hour.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] So...[SOFTBLOCK] Uh...[SOFTBLOCK] Did you marry her before or after...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Ha ha ha![SOFTBLOCK] My friend, I can see on your face that you think our relationship is unusual.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] N-[SOFTBLOCK]no![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] I don't want to be insensitive...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] I take no offense, ha ha ha![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] If you must know, I met her only a few winters ago.[SOFTBLOCK] She is quite a human![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] And no, I am not interested in turning her, or anyone else.[SOFTBLOCK] While she would make a fine fang, she has no interest in it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, so...[SOFTBLOCK] Okay.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] If you've heard any rumours about her, they are probably false.[SOFTBLOCK] There are many humans who despise partirhumans, and relationships between us and them -[SOFTBLOCK] forbidden.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Not so different to where I'm from...[SOFTBLOCK] but in a different way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] I used to think the same way.[SOFTBLOCK] But, then I met her.[SOFTBLOCK] She changed my mind, for the better.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] And no, she is at no risk living out here in the woods.[SOFTBLOCK] Any other who casts a hostile glance at her would have to deal with me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Though I prefer fishing to fighting.[SOFTBLOCK] Speaking of...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Ah, I'm not hungry right now.[SOFTBLOCK] But thank you for the offer.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Well, then back to work.[SOFTBLOCK] Come to me, fishies!") ]])
			fnCutsceneBlocker()
		
		--Florentina is here:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat:[E|Neutral] Oh, hello.[SOFTBLOCK] Are you here to purchase fresh fish?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] You're -[SOFTBLOCK] are you Ginny?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh my, the rumours really were true.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Rumours?[SOFTBLOCK] Oh, you are Florentina, yes?[SOFTBLOCK] Alicia mentioned you.[SOFTBLOCK] The Alraune with an eyepatch.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Uh oh...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Ha ha![SOFTBLOCK] If Alicia has a problem with you, she is strong enough to solve it herself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] But of course, there are two of you.[SOFTBLOCK] A two-on-two may be fun![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Uhhhh, pass?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Suit yourself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] So, er, did you marry Alicia before -[SOFTBLOCK] or after...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Do you think our relationship unusual?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] N-[SOFTBLOCK]no![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] I don't want to be insensitive...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] I take no offense, ha ha![SOFTBLOCK] There are those, human and otherwise, who view such relationships with contempt.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] I counted myself among them, but then I met Alicia.[SOFTBLOCK] She is a special human, and my mind was changed.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Kind of reminds me of home...[SOFTBLOCK] I guess things aren't so different.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] I've learned much by keeping my mind open.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You know what?[SOFTBLOCK] Good for you, Ginny.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] People are always looking for an excuse to hate, like they need permission.[SOFTBLOCK] Nuts to them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Hmm, maybe the things Alicia said about you were not true after all...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh they probably were.[SOFTBLOCK] I'm a jerk.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Ha ha ha ha![SOFTBLOCK] Very good![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] But if you are not here to purchase fish, I must return to work.[SOFTBLOCK] Come to me, fishies!") ]])
		
		end
		
	--Successive meetings.
	else
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ginny", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ginny:[E|Neutral] Heh, I've caught many fish today, but none of the rare species.[SOFTBLOCK] My luck will turn around soon, though!") ]])
	
	end
end
