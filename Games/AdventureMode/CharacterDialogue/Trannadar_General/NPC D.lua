--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Dialogue.
	WD_SetProperty("Show")
	WD_SetProperty("Append", "Merc:[VOICE|MercF] Florentina's a real card shark.[SOFTBLOCK] I lost my shirt to that poker face![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Merc:[VOICE|MercF] Luckily, I got it back on an installment plan.[SOFTBLOCK] Better not miss a payment...")
end
