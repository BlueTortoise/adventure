--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	WD_SetProperty("Show")
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Dialogue for non-Alraune:
	if(sMeiForm ~= "Alraune") then
		WD_SetProperty("Append", "Alraune:[VOICE|Alraune] Don't mind me, I mean you no harm.[SOFTBLOCK] I'm just tending to the trees around here.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Alraune:[VOICE|Alraune] The humans at this trading post use them for firewood.[SOFTBLOCK] I try to make sure they trim off dead branches and not live ones.")
	
	--Dialogue for Alraune:
	else
		WD_SetProperty("Append", "Alraune:[VOICE|Alraune] Hello, leaf-sister.[SOFTBLOCK] I'm making sure the humans only use dead branches for firewood.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Alraune:[VOICE|Alraune] Honestly, it can be so frustrating to deal with them sometimes...")
	end
end
