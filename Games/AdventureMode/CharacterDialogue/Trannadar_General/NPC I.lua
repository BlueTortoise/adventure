--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	WD_SetProperty("Show")
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Dialogue.
	WD_SetProperty("Append", "Lady:[VOICE|MercF] Hey, you're not here to pick up this statue, are you?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Lady:[VOICE|MercF] I'm supposed to deliver it but I'm a few days early.[SOFTBLOCK] Nobody has come to pick it up.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Lady:[VOICE|MercF] Who would commission a statue of a corgi, though?")
	
end
