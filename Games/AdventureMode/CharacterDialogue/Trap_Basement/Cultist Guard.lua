--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Dialogue.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

	--If Mei is nonhuman:
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sMeiForm ~= "Human") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Hey![SOFTBLOCK] You're Tess, right?[SOFTBLOCK] Special mission and all that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Y-[SOFTBLOCK]yeah![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: That's a nice disguise![SOFTBLOCK] You must be really good at being a spy![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Thanks![SOFTBLOCK] I -[SOFTBLOCK] made it myself?[SOFTBLOCK] Sort of?[BLOCK][CLEAR]") ]])
		
		--Florentina is not present:
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: I hope it's going well![SOFTBLOCK] Good luck!") ]])
		
		--Florentina is present:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: And you even have a prisoner already![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] T-[SOFTBLOCK]totally![SOFTBLOCK] I...[SOFTBLOCK] tamed...[SOFTBLOCK] this Alraune![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] TAMED!?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Great work, Tess![SOFTBLOCK] The Sister Superior will be so proud!") ]])
		end
	
	--If Mei is a human:
	else

		--Florentina is not present:
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Hey![SOFTBLOCK] You're not supposed to take off your robes![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] W-[SOFTBLOCK]wait, I'm on a special mission![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Oh.[SOFTBLOCK] Okay.[SOFTBLOCK] Carry on then.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (I guess these guys will believe anything...)") ]])
		
		--Florentina is present:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Hey![SOFTBLOCK] You're Tess, right?[SOFTBLOCK] Special mission and all that?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] T-[SOFTBLOCK]totally![SOFTBLOCK] Yep![SOFTBLOCK] I even captured and tamed an Alraune![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] TAMED!?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Great work, Tess![SOFTBLOCK] The Sister Superior will be so proud!") ]])
		end
	end
end
