--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--[System]
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSpecialAcolyte", "N", 1.0)
	
	--[Dialogue]
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: ![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] ![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: !!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] !!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] !!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: !!!!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] !!!!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Are we waiting for something?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Thank the Great Swamp you came![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: I must have hit the wrong switch![SOFTBLOCK] I've been stuck in here for hours, and nobody has come to let me out![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, we - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: That's twice you've helped me out, Initiate Tess.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Tess?[SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh yeah![SOFTBLOCK] That's...[SOFTBLOCK] me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] What the hey...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Well I see your secret mission is going well.[SOFTBLOCK] You've already captured an Alraune![SOFTBLOCK] Great work![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Oh yeah, totally.[SOFTBLOCK] This is my prisoner.[SOFTBLOCK] Right, prisoner?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Yeah.[SOFTBLOCK] Sure.[SOFTBLOCK] That.[BLOCK][CLEAR]") ]])
	if(sMeiForm == "Alraune") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: So is that like a costume or something?[SOFTBLOCK] It's very realistic![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] It's a disguise.[SOFTBLOCK] I am a master of disguise.[BLOCK][CLEAR]") ]])
	elseif(sMeiForm == "Slime") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: So did you disguise yourself as a slime and infiltrate the trading post?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Well they didn't see through it, but you did.[SOFTBLOCK] You're super observant![BLOCK][CLEAR]") ]])
	elseif(sMeiForm == "Bee") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Where did you get that cool bee disguise?[SOFTBLOCK] The wings even move and everything![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I can't give away all my secrets![BLOCK][CLEAR]") ]])
	elseif(sMeiForm == "Werecat") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: So that's a werecat costume, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, I actually am a werecat.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: That's some next-level infiltration stuff![SOFTBLOCK] I would never have thought of it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] The trick is to not act suspiciously.[BLOCK][CLEAR]") ]])
	elseif(sMeiForm == "Ghost") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: I almost didn't recognize you under the ghost costume.[SOFTBLOCK] That's a costume, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Absolutely.[SOFTBLOCK] Please don't ask how I'm floating...[BLOCK][CLEAR]") ]])
	end
		
		
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Awesome![SOFTBLOCK] I got a bit of a promotion, too.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hey, since I'm totally a cultist, uh...[SOFTBLOCK] can you tell me what ritual is going on in here?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: I dunno, it's a bit above my pay grade.[SOFTBLOCK] I heard it's big, though.[SOFTBLOCK] We're supposed to be getting pure ones to help us against the unbelievers.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I totally know what a pure one is, but if you could just...[SOFTBLOCK] inform my prisoner here?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: They are beings wrought from the Holy Peat.[SOFTBLOCK] Instruments of His will![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] *This sounds really bad!*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] *The better that we stop them now, then.*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Now if you'll excuse me...[SOFTBLOCK] There's not exactly a lavatory down here...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] One more thing...[SOFTBLOCK] How exactly do we get through this maze?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Oh, you need two people.[SOFTBLOCK] You have to use the eastern switch like it's a floodgate.[SOFTBLOCK] The switch on the other side will let the other person through.[SOFTBLOCK] Go give it a try![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, okay![SOFTBLOCK] Great![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: I tell you, Tess.[SOFTBLOCK] You and me?[SOFTBLOCK] We are going places![SOFTBLOCK] See you later!") ]])
	fnCutsceneBlocker()
	
	--[Movement]
	--Cultist walks off to the right.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Cultist")
		ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (8.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Cultist keeps walking. Mei and Florentina turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Cultist")
		ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (8.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Cultist walks south.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Cultist")
		ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (12.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Cultist")
		ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (12.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Cultist")
		ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (20.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Cultist despawns.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Cultist")
		ActorEvent_SetProperty("Teleport To", (-100 * gciSizePerTile), (-100 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--[Dialogue]
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So are you going to explain yourself?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Those guys are reaaaally stupid.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I may have tricked them into thinking I was one of them, and then escaping.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Oh, really?[SOFTBLOCK] I like that...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Okay, she said to use the eastern switch like it's a floodgate?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I think I know which one she was talking about.[SOFTBLOCK] Let's go take a look.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (How could she have hit the wrong switch?[SOFTBLOCK] There are no switches in this room...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (What an idiot!)") ]])
	fnCutsceneBlocker()
end
