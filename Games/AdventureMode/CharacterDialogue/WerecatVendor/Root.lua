--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--In all cases, mark the combat intro dialogues as complete.
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
	local iTalkedWerecatVendor = VM_GetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N")
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--NPC takes slot 5.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
	
	--Mei has not spoken to the vendor before:
	if(iTalkedWerecatVendor == 0.0) then
		
		--Florentina is not present:
		if(bIsFlorentinaPresent == false) then
		
			--Mei is a slime:
			if(sMeiForm == "Slime") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Hss![SOFTBLOCK] Away, filthy slime![SOFTBLOCK] Away![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Ack![SOFTBLOCK] What a jerk!)") ]])
			
			--Mei is a werecat:
			elseif(sMeiForm == "Werecat") then
		
				--Flags.
				VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N", 1.0)
				
				--Dialogue.
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr, kinfang! Found some good stuff, very good.[SOFTBLOCK] Want to take a look?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What are you selling?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Mmmm, look![SOFTBLOCK] Small brick, but it makes light when you tap it.[SOFTBLOCK] See?[SOFTBLOCK] No magic![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![SOFTBLOCK] This is a cell phone![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I forgot mine at work![SOFTBLOCK] Oh dear, I hope nobody tried to steal it.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Kinfang not amazed?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You don't even know what this is, do you?[SOFTBLOCK] Here, let me show you...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] See?[SOFTBLOCK] It takes photos![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: But not magic![SOFTBLOCK] How did it record my face?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's got a camera right - [SOFTBLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Hss![SOFTBLOCK] Kinfang![SOFTBLOCK] Brick is cursed![SOFTBLOCK] Smash it![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hey, wait![SOFTBLOCK] I'll -[SOFTBLOCK] I'll dispose of it for you.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: You would take a risk for me?[SOFTBLOCK] Brick is dangerous![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Only in the wrong hands.[SOFTBLOCK] I'll handle it.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Kinfang honors this one.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (All right, let's see...)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (This phone belongs to someone named Sanya.[SOFTBLOCK] I guess she didn't put a lock screen on it.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (She also doesn't take a lot of photos.[SOFTBLOCK] Seems she's from someplace in Europe.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (There's not much else to find on here, unless I really feel like playing Enraged Avians.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (But it does mean that there have been other people from Earth here, and recently too!)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Kinfang, where did you find this brick?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Oh, you want to scavenge with me?[SOFTBLOCK] Mine![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Er, no.[SOFTBLOCK] The opposite.[SOFTBLOCK] I want to scavenge anywhere I will have no chance of finding one of these cursed bricks.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ah![SOFTBLOCK] Clever, clever![SOFTBLOCK] Found it in the rockslide near the mountain south of here![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Crud.[SOFTBLOCK] I guess that's out.[SOFTBLOCK] If I can't find a way back to Earth, maybe I should go find this Sanya person...)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, Kinfang, I'll go destroy this brick.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Eck, now I don't have anything else to sell...") ]])
				
				--Topic.
				WD_SetProperty("Unlock Topic", "Cell Phone", 1)
			
			--All other cases:
			else
		
				--Flags.
				VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N", 1.0)
				
				--Dialogue.
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr, look, look! Found some good stuff, very good.[SOFTBLOCK] Want to take a look?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What are you selling?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Mmmm, look![SOFTBLOCK] Small brick, but it makes light when you tap it.[SOFTBLOCK] See?[SOFTBLOCK] No magic![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![SOFTBLOCK] This is a cell phone![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I forgot mine at work![SOFTBLOCK] Oh dear, I hope nobody tried to steal it.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: You're not amazed?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You don't even know what this is, do you?[SOFTBLOCK] Here, let me show you...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] See?[SOFTBLOCK] It takes photos![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: But not magic![SOFTBLOCK] How did it record my face?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's got a camera right - [SOFTBLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Hss![SOFTBLOCK] Filthy trap![SOFTBLOCK] Brick is cursed![SOFTBLOCK] Smash it![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hey, wait![SOFTBLOCK] I'll -[SOFTBLOCK] I'll dispose of it for you.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: You would take a risk for me?[SOFTBLOCK] Brick is dangerous![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Only in the wrong hands.[SOFTBLOCK] I'll handle it.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: You honor this one.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (All right, let's see...)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (This phone belongs to someone named Sanya.[SOFTBLOCK] I guess she didn't put a lock screen on it.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (She also doesn't take a lot of photos.[SOFTBLOCK] Seems she's from someplace in Europe.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (There's not much else to find on here, unless I really feel like playing Enraged Avians.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (But it does mean that there have been other people from Earth here, and recently too!)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Ms. Werecat, where did you find this brick?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Oh, you want to scavenge in my territory?[SOFTBLOCK] Mine![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Er, no.[SOFTBLOCK] The opposite.[SOFTBLOCK] I want to scavenge anywhere I will have no chance of finding one of these cursed bricks.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ah![SOFTBLOCK] Clever, clever![SOFTBLOCK] Found it in the rockslide near the mountain south of here![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Crud.[SOFTBLOCK] I guess that's out.[SOFTBLOCK] If I can't find a way back to Earth, maybe I should go find this Sanya person...)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, Werecat, I'll go destroy this brick.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Eck, now I don't have anything else to sell...") ]])
				
				--Topic.
				WD_SetProperty("Unlock Topic", "Cell Phone", 1)
		
			end
		
		--Florentina is present:
		else
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N", 1.0)
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Plant trader![SOFTBLOCK] Look![SOFTBLOCK] Good stuff, good stuff![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You know this cat, Florentina?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Yeah, she's a scavenger.[SOFTBLOCK] Likes to pawn her crap off at my shop.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You better have something good this time, cat.[SOFTBLOCK] I don't want any more broken junk.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Good stuff![SOFTBLOCK] Look, brick makes light when you tap it![SOFTBLOCK] See?[SOFTBLOCK] No magic![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well well well, that [SOFTBLOCK]*is*[SOFTBLOCK] pretty nifty.[SOFTBLOCK] That could fetch a bit for someone looking to rob a sorcerer.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Wait a minute![SOFTBLOCK] This is a cell phone![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] You know what this is?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, we have these on Earth, I -[SOFTBLOCK][EMOTION|Mei|Offended] hey![SOFTBLOCK] Rob a sorcerer?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Err...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Anyway, look.[SOFTBLOCK] You just tap here and here...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Say cheese![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: ..![SOFTBLOCK] It recorded my face![SOFTBLOCK] Hss![SOFTBLOCK] Cursed brick, cursed![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's just a camera...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well if it's cursed, you won't be charging us much for it now will you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Keep it![SOFTBLOCK] Take it far from me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hold on a second.[SOFTBLOCK] Where did you get this thing?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Rockslide far south of here, far south.[SOFTBLOCK] Found it there![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hm, might have washed down from the glacier.[SOFTBLOCK] Unfortunately the path is blocked.[SOFTBLOCK] It'll be a while before anyone clears it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm just surprised it's still in working order.[SOFTBLOCK] Let's see who this belongs to...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] 'Sanya Pavletic'.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Friend of yours?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, no.[SOFTBLOCK] She's from Europe someplace.[SOFTBLOCK][EMOTION|Mei|Happy] But she was from Earth![SOFTBLOCK] There are others like me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] If you want to go mountain climbing...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Not right now.[SOFTBLOCK] But, if we can't find a way back to Earth, then finding out what happened to this Sanya person might be a good lead to follow.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well hey now, if there's any more of this Earth stuff you think I could sell...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Ugh...") ]])
				
			--Topic.
			WD_SetProperty("Unlock Topic", "Cell Phone", 1)
	
		end
	
	--Already talked to the werecat:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: No more good stuff to sell...") ]])
	
	end
end
