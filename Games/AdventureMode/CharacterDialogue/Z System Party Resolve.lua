--[System Party Resolve]
--When the player opens up the AdventureMenu at a campfire, this script is fired. It builds a list of
-- characters that the party leader may speak to in a dialogue. In Chapter 1, this is just Florentina.
--If nobody can be spoken to, "Chat" is greyed out on the menu. The menu is implicitly cleared when 
-- this is called. Members need to be re-added each time it is called.
local sCurrentLevel = AL_GetProperty("Name")
if(sCurrentLevel == "Nowhere") then return end

--Run through the follower listing.
for i = 1, gsFollowersTotal, 1 do

	--If Florentina is present, add her.
	if(gsaFollowerNames[i] == "Florentina") then
		AM_SetProperty("Chat Member", "Florentina", gsRoot .. "CharacterDialogue/Florentina/Root.lua", "Root/Images/AdventureUI/CampfireMenu/CHATICO|Florentina")
	end
	if(gsaFollowerNames[i] == "55") then
		AM_SetProperty("Chat Member", "2855", gsRoot .. "CharacterDialogue/55/Root.lua", "Root/Images/AdventureUI/CampfireMenu/CHATICO|55")
	end
    
    --In the biolabs, SX-399 and Sophie get added. You can get SX-399 before the biolabs but can't chat with her until then.
    local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
    if(iReachedBiolabs == 1.0) then
        if(gsaFollowerNames[i] == "SX399") then
            AM_SetProperty("Chat Member", "SX-399", gsRoot .. "CharacterDialogue/Topic_SX399/Root.lua", "Root/Images/AdventureUI/CampfireMenu/CHATICO|SX399")
        end
        if(gsaFollowerNames[i] == "Sophie") then
            AM_SetProperty("Chat Member", "Sophie", gsRoot .. "CharacterDialogue/Topic_Sophie/Root.lua", "Root/Images/AdventureUI/CampfireMenu/CHATICO|Sophie")
        end
    end
end

