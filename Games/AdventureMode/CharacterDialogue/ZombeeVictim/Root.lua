--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iConvertedVictimBee = VM_GetVar("Root/Variables/Chapter1/Scenes/iConvertedVictimBee", "N")
	
	--If the victim has not been converted yet:
	if(iConvertedVictimBee == 0.0) then
	
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])

		--Talking.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Bee][E|MC] (Drone obeys.[SOFTBLOCK] Bee captured.[SOFTBLOCK] Bee will be converted.)[BLOCK]") ]])
	
		--Fade to black.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"DroneObeys\") ")
		fnCutsceneBlocker()
	
	--Already converted.
	else
	
		--Set facing.
		TA_SetProperty("Face Character", "PlayerEntity")
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Zombee", "Neutral") ]])

		--Talking.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Bee][E|MC] (Drone obeys.[SOFTBLOCK] Bee already converted.[SOFTBLOCK] Drone obeys.[SOFTBLOCK] Capture bees.[SOFTBLOCK] Convert bees.[SOFTBLOCK] Drone obeys.)") ]])
		fnCutsceneBlocker()
	end

--Conversion sequence.
elseif(sTopicName == "DroneObeys") then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iConvertedVictimBee", "N", 1.0)

	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Scene.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The worker bee was wounded and helpless.[SOFTBLOCK] The drone obeyed.[SOFTBLOCK] The drone held the worker bee down, pushing her face close to the corrupted honey.[SOFTBLOCK] The drone obeyed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slowly, surely, a ghostly hand emerged from the honey.[SOFTBLOCK] The drone held the worker bee down.[SOFTBLOCK] The drone obeyed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The hand pressed against the worker bee's face.[SOFTBLOCK] The bee tried to resist, but the drone held her down.[SOFTBLOCK] The drone obeyed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The hand violated the worker bee's essence.[SOFTBLOCK] The drone obeyed.[SOFTBLOCK] The worker bee was converted.[SOFTBLOCK] The drone obeyed.") ]])
	fnCutsceneBlocker()
	
	--Switch the bee to use the zombee sprites.
	fnCutsceneInstruction([[ 
	EM_PushEntity("ZombeeNPCD")
		fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
	DL_PopActiveObject() ]])

	--Fade back in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Zombee", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Bee][E|MC] (Drone obeys.[SOFTBLOCK] Bee converted.[SOFTBLOCK] Drone obeys.[SOFTBLOCK] Capture bees.[SOFTBLOCK] Convert bees.[SOFTBLOCK] Drone obeys.)") ]])
	fnCutsceneBlocker()

end
