--[ ========================================= Normal AI ========================================= ]
--The Normal AI, selects abilities at random based on the weight of the ability. It then selects
-- targets at random based on their internal "Threat" rating. This AI is not specific to a given
-- enemy or chapter.
--This AI also has built-in capabilities to become Fascinated/Confused/etc. This is handled via tags.
--The AdvCombatEntity that owns the AI is the active object when executing this script.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ======================================= Combat Begins ======================================= ]
--Called when combat begins, but before turn order is resolved for the 0th turn. If this AI was
-- arriving as reinforcements, this will be called at the start of the turn when they arrive before
-- turn order is resolved.
--If arriving in the middle of combat, the AI can determine they are a reinforcement by checking 
-- the turn counter. If it's 0, they were not a reinforcement.
if(iSwitchType == gciAI_CombatStart) then
    
    --Create a DataLibrary entry for this ID.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")

-- |[ ====================================== Turn Begins ======================================= ]|
--Called when the turn begins, after turn order is resolved.
elseif(iSwitchType == gciAI_TurnStart) then

-- |[ ==================================== AI Begins Action ==================================== ]|
--Called when a new Action begins, *not necessarily the action of the owning entity*. If it is the
-- action of the owning entity, we need to decide what action to perform. Otherwise, set variables
-- if the AI requires counting actions.
elseif(iSwitchType == gciAI_ActionBegin) then

    --[Threat Update]
    --Even if this is not the AI's turn, update its threat effects. This is to be sure the player
    -- has accurate values.
    LM_ExecuteScript(LM_GetCallStack(0), gciAI_UpdateThreat)

    --[Events]
    --Make sure the AI needs to spawn events. This script is called at the start of a turn in case
    -- the AI needs to execute effects and whatnot, but this flag is true if it needs to perform
    -- an action.
    local bCanSpawnEvent = AdvCombat_GetProperty("Is AI Spawning Events")
    if(bCanSpawnEvent == false) then return end
    
    --[AI Routing]
    --This subroutine will return which AI should control the AI. It will return "Normal" if there
    -- are no tags present or the roll failed.
    local sAIResult = fnCheckAITags()
    
    --AI is Ambushed. Causes them to waste a turn.
    if(AdvCombatEntity_GetProperty("Is Ambushed") == true) then
        AdvCombatEntity_SetProperty("Ambushed", false)
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AmbushAbilitySlot)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
        return
    end

    --Switch AIs.
    if(sAIResult == "Fascinate") then
    elseif(sAIResult == "Confound") then
    elseif(sAIResult == "Berserk") then
    elseif(sAIResult == "Confuse") then
        LM_ExecuteScript(gsRoot .. "Combat/AIs/001 Confused AI.lua", gciAI_ActionBegin)
        return
    elseif(sAIResult == "Charm") then

    end

    --[Description]
    --The AI selects a random ability, runs its target algorithm, and selects a random target.
    -- The targets are weighed by the amount of threat the targets have. The highest threat
    -- has a 70% chance to be picked, second-highest is 20%, and all other targets compete
    -- for the remaining 10%.

    --[Check Turn]
    --Compare the ID of the owner with the ID of the acting entity.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    if(iOwnerID ~= iActingID) then return end

    --[Stun]
    --Standard stun handler. Will return true if this entity is stunned.
    local bIsStunned = fnStunHandleActionBegin()
    if(bIsStunned == true) then

        --Animation.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_StunAbilitySlot)

        --Stop action here.
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
        return
    end

    --Get how many abilities are available.
    local iAbilitiesTotal = AdvCombatEntity_GetProperty("Abilities Total")
    
    --If it's zero, pass turn:
    if(iAbilitiesTotal == 0) then
        return
    end

    --Roll. Start at gciAI_AbilityRollStart since there are special system abilities, like "Stunned", below that.
    local iAbilityRoll = LM_GetRandomNumber(gciAI_AbilityRollStart, iAbilitiesTotal-1)
    AdvCombat_SetProperty("Set Ability As Active", iAbilityRoll)
    
    --Next, paint targets. This will call a script.
    AdvCombat_SetProperty("Run Active Ability Target Script")
    
    --Get target clusters.
    local iMarkedCluster = 0
    local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
    
    --If there is exactly one cluster, mark that cluster.
    if(iTotalTargetClusters == 1) then
        AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
    
    --Multiple clusters, determine threat and roll.
    else
    
        --We need to check threat for each cluster. Build a list of average threats for each
        -- element in each cluster.
        local iHighestThreat = -1
        local iHighestThreatSlot = 1
        local iSecondHighestThreatSlot = 1
        local iaThreats = {}
        for i = 1, iTotalTargetClusters, 1 do
            
            --Zero base threat.
            iaThreats[i] = {0, i}
            
            --For each target:
            local iTotalTargets = AdvCombat_GetProperty("Total Targets In Cluster", i-1)
            for p = 1, iTotalTargets, 1 do
            
                --Get this target's ID.
                local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", i-1, p-1)
            
                --Get the threat associated with the target. It can be 0.
                local iThreat = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iTargetID.. "_Threat", "N")
                iaThreats[i][1] = iaThreats[i][1] + (iThreat / iTotalTargets)
            
            end
        
            --Check if this is the highest threat.
            if(iaThreats[i][1] > iHighestThreat) then
                iHighestThreat = iaThreats[i][1]
                iSecondHighestThreatSlot = iHighestThreatSlot
                iHighestThreatSlot = i
            end
        end
            
        --All target clusters have been populated with their average threat values. Perform a roll.
        local iSlotRoll = LM_GetRandomNumber(1, 100)
        
        --There is a 70% chance to pick the cluster with the highest threat:
        if(iSlotRoll <= 70) then
            iMarkedCluster = iHighestThreatSlot - 1
            AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
            
        --There is a 20% chance to pick the cluster with the second highest threat:
        elseif(iSlotRoll <= 90 or iTotalTargetClusters == 2) then
            iMarkedCluster = iSecondHighestThreatSlot - 1
            AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
            
        --All remaining clusters are equally divided into the last 10%.
        else
        
            --Assemble a new list that does not contain the two highest threats and pick one element at random.
            local iNewThreatsTotal = 0
            local iaNewThreats = {}
            for i = 1, iTotalTargetClusters, 1 do
                if(iaThreats[i][2] ~= iHighestThreatSlot and iaThreats[i][2] ~= iSecondHighestThreatSlot) then
                    iNewThreatsTotal = iNewThreatsTotal + 1
                    iaNewThreats[iNewThreatsTotal] = iaThreats[i][2]
                end
            end
            
            --Roll.
            local iSlot = LM_GetRandomNumber(1, iNewThreatsTotal)
            iMarkedCluster = iaNewThreats[iSlot] - 1
            AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
            
        end
    end
    
    --Each target in the selected cluster loses 40% of its threat. This keeps the threat values cycling.
    local iTotalTargets = AdvCombat_GetProperty("Total Targets In Cluster", iMarkedCluster)
    for p = 1, iTotalTargets, 1 do
    
        --Get this target's ID.
        local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", iMarkedCluster, p-1)
    
        --Get the threat associated with the target. It can be 0.
        local iThreat = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iTargetID.. "_Threat", "N")
        iThreat = iThreat * 0.60
        VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iTargetID.. "_Threat", "N", iThreat)
    
    end
    
    --Run the ability.
    AdvCombat_SetProperty("Mark Handled Action")
    AdvCombat_SetProperty("Run Active Ability On Active Targets")

--[ ====================================== AI Ends Action ======================================= ]
--Called after an action ends, before the turn order is popped.
elseif(iSwitchType == gciAI_ActionEnd) then

--[ ========================================= Turn Ends ========================================= ]
--Called after the turn ends but before turn order is resolved for the next turn or reinforcements
-- arrive, if any do.
elseif(iSwitchType == gciAI_TurnEnd) then

--[ ====================================== AI is Defeated ======================================= ]
--Called when this entity is KO'd. Note that an enemy can be revived (in theory) which can cause
-- this to execute each time they are KO'd.
elseif(iSwitchType == gciAI_KnockedOut) then

--[ ======================================== Combat Ends ======================================== ]
--Called when combat ends but before XP/JP/etc are awarded.
elseif(iSwitchType == gciAI_CombatEnds) then

--[ ================================== Application Pack Before ================================== ]
--Called right before an application pack is processed, allowing querying and modification.
elseif(iSwitchType == gciAI_Application_Start) then

--[ ================================== Application Pack After =================================== ]
--Called right after an application pack is processed, allowing querying.
elseif(iSwitchType == gciAI_Application_End) then

    --[AI Routing]
    --This subroutine will return which AI should control the AI. It will return "Normal" if there
    -- are no tags present or the roll failed.
    local sAIResult = fnCheckAITags()

    --Switch AIs.
    if(sAIResult == "Fascinate") then
    elseif(sAIResult == "Confound") then
    elseif(sAIResult == "Berserk") then
    elseif(sAIResult == "Confuse") then
        LM_ExecuteScript(gsRoot .. "Combat/AIs/001 Confused AI.lua", gciAI_Application_End)
        return
    elseif(sAIResult == "Charm") then
    end

    --[Description]
    --After an application ends, this AI computes how much threat the application generated and
    -- applies it to the relevent entities. In general, the AI provides higher threat when targeted
    -- itself and generates less threat when an ally is hit.

    --[ ========== Setup ========== ]
    --Get Variables.
    local iUniqueID = RO_GetID()
    local iOriginatorID = AdvCombat_GetProperty("Application Get Originator ID")
    local iTargetID     = AdvCombat_GetProperty("Application Get Target ID")
    local sAppString    = AdvCombat_GetProperty("Application Get String")
    
    --Get the party we're in, versus the originator party.
    local iAIParty         = AdvCombat_GetProperty("Party Of ID", iUniqueID)
    local iTargetParty     = AdvCombat_GetProperty("Party Of ID", iTargetID)
    local iOriginatorParty = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    if(iOriginatorID == 0) then return end
    
    --[Threat Multiplier]
    --Push the entity. We need their threat multiplier.
    AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
        local iThreatMultiplier = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_ThreatMultiplier)
    DL_PopActiveObject()
        
    --If the threat multiplier is 0, do nothing.
    if(iThreatMultiplier <= 0.0) then return end
    
    --[Subdivision]
    --Break the string into chunks. This is the same logic used by the C++ code.
    local saArray = fnSubdivide(sAppString, "|")
    if(saArray[1] == nil or saArray[1] == "") then return end
    
    --[ ========== We Are The Target ========== ]
    --If the target is this AI's entity:
    if(iTargetID == iUniqueID) then
    
        --If the action came from the other party:
        if(iAIParty ~= iOriginatorParty) then
                
            --Push the entity. We need their threat multiplier.
            AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
                local iThreatMultiplier = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_ThreatMultiplier)
            DL_PopActiveObject()
                
            --If the threat multiplier is 0, do nothing.
            if(iThreatMultiplier <= 0.0) then return end
    
            --Is this a damage-type application:
            if(saArray[1] == "Damage") then
                
                --Resolve the threat value to inflict. First, get the damage.
                local iThreat = tonumber(saArray[2])
                
                --Multiply by the threat multiplier. Multiplier is a percent.
                iThreat = iThreat * (iThreatMultiplier / 100.0) * gcfThreat_DamageMe
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N")
                iOriginatorThreat = iOriginatorThreat + iThreat
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
            
            --Application of an effect:
            elseif(saArray[1] == "Effect") then
            
                --Get the originator's attack power. Effects are considered to emulate that amount of "damage".
                AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
                    local iThreat = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
                DL_PopActiveObject()
            
                --Multiply by the threat multiplier. Multiplier is a percent.
                iThreat = iThreat * (iThreatMultiplier / 100.0) * gcfThreat_EffectMe
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N")
                iOriginatorThreat = iOriginatorThreat + iThreat
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
            
            --Special:
            elseif(saArray[1] == "AI_APPLICATION" and #saArray >= 2) then
            
                --If the second slot is "Threat", this is an application pack that just applies threat and nothing else.
                if(saArray[2] == "Threat" and #saArray >= 3) then
            
                    --Third argument is a flat amount of threat to apply. If a multiplier is desired it needs to be applied before this point.
                    local iThreatApply = tonumber(saArray[3])
                    if(iThreatApply > 0.0) then
                        
                        --Get the current threat. It comes back zero if this is the first time it was tracked.
                        local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N")
                        iOriginatorThreat = iOriginatorThreat + iThreatApply
                        
                        --Store.
                        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
                    end
                end
            end
    
        --Came from the same party:
        else
    
        end

    --[ ========== We Are Not The Target ========== ]
    --If the target is not this entity:
    else
        
        --If the target is in the same party as us, and the originator is not:
        if(iAIParty == iTargetParty and iAIParty ~= iOriginatorParty) then

            --Is this a damage-type application:
            if(saArray[1] == "Damage") then
                
                --Resolve the threat value to inflict. First, get the damage.
                local iThreat = tonumber(saArray[2])
                
                --Multiply by the threat multiplier. Multiplier is a percent.
                iThreat = iThreat * (iThreatMultiplier / 100.0) * gcfThreat_DamageAlly
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N")
                iOriginatorThreat = iOriginatorThreat + iThreat
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
            
            --Application of an effect:
            elseif(saArray[1] == "Effect") then
            
                --Get the originator's attack power. Effects are considered to emulate that amount of "damage".
                AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
                    local iThreat = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
                DL_PopActiveObject()
            
                --Multiply by the threat multiplier. Multiplier is a percent.
                iThreat = iThreat * (iThreatMultiplier / 100.0) * gcfThreat_EffectAlly
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N")
                iOriginatorThreat = iOriginatorThreat + iThreat
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
            end

        --If the target is in the same party as us, and the originator is as well:
        elseif(iAIParty == iTargetParty and iAIParty == iOriginatorParty) then

        --If the target is not in the same party as us, and the originator is not:
        elseif(iAIParty ~= iTargetParty and iAIParty == iOriginatorParty) then

            --Is this a healing-type application:
            if(saArray[1] == "Healing") then
                
                --Resolve the threat value to inflict. First, get the healing.
                local iHealing = tonumber(saArray[2])
                
                --Multiply by the threat multiplier. Multiplier is a percent.
                iHealing = iHealing * (iThreatMultiplier / 100.0) * gcfThreat_Heal
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N")
                iOriginatorThreat = iOriginatorThreat + iHealing
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
            end

        --If the target is not in the same party as us, and the originator is:
        else

        end
    end
    
-- |[ ===================================== Threat Update ====================================== ]|
--Call this whenever you need to update the threat values for this AI.
elseif(iSwitchType == gciAI_UpdateThreat) then

    -- |[Setup]|
    --Variables
    local iOwnerID = RO_GetID()

    -- |[Threat Scanner]|
    --Scan the player's party and create effects to handle threat vs. each entity. These are not
    -- the actual threat values, just an effect displaying them.
    local iCombatPartySize = AdvCombat_GetProperty("Combat Party Size")
    for i = 0, iCombatPartySize-1, 1 do

        --Push the party member for checking.
        AdvCombat_SetProperty("Push Combat Party Member By Slot", i)

            --Get the ID of the party member in question.
            local iPartyID = RO_GetID()

            --Check if the effect exists. A variable in its DataLibrary section will store the ID
            -- of its target.
            local bEffectExists = false
            local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", iOwnerID)
            for p = 0, iEffectsTotal-1, 1 do
                AdvCombat_SetProperty("Push Temp Effects", p)
                    local iEffectID = RO_GetID()
                    local iThreatTarget = VM_GetVar("Root/Variables/Combat/" .. iEffectID .. "/iThreatTarget", "N")
                    
                    --If this effect is the ThreatVs for this target, then update its display with the threat value.
                    if(iThreatTarget == iPartyID) then
                        bEffectExists = true
                        local iThreat = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iPartyID.. "_Threat", "N")
                        LM_ExecuteScript(gsThreatEffectPath, gciEffect_ThreatVs_SetValues, iThreat)
                    end
                DL_PopActiveObject()
                if(bEffectExists) then break end
            end
            
            --Clean up temp effects list.
            AdvCombat_SetProperty("Clear Temp Effects")
        
            --Create an effect if none exists.
            if(bEffectExists == false) then
                AdvCombat_SetProperty("Create Effect", iOwnerID)
                
                    --Variables.
                    local iEffectID = RO_GetID()
                    
                    --Run the setup scripts.
                    local iThreat = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iPartyID.. "_Threat", "N")
                    LM_ExecuteScript(gsThreatEffectPath, gciEffect_Create, iOwnerID, iPartyID)
                    LM_ExecuteScript(gsThreatEffectPath, gciEffect_ThreatVs_SetValues, iThreat)
                    
                DL_PopActiveObject()
            end
        
        DL_PopActiveObject()
    end
    
end
