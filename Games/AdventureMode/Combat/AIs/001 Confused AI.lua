--[ ======================================== Confused AI ======================================== ]
--The "Confused" AI, overrides target selection. Any ability that hits a single target will always
-- pick from all available targets, friendly and enemy. Any ability that hits a group will either
-- hit all allies or all friendlies. Target-all abilities are unaffected.
--The AdvCombatEntity that owns the AI is the active object when executing this script.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ======================================= Combat Begins ======================================= ]
--Called when combat begins, but before turn order is resolved for the 0th turn. If this AI was
-- arriving as reinforcements, this will be called at the start of the turn when they arrive before
-- turn order is resolved.
--If arriving in the middle of combat, the AI can determine they are a reinforcement by checking 
-- the turn counter. If it's 0, they were not a reinforcement.
if(iSwitchType == gciAI_CombatStart) then
    
    --Create a DataLibrary entry for this ID.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")

--[ ======================================== Turn Begins ======================================== ]
--Called when the turn begins, after turn order is resolved.
elseif(iSwitchType == gciAI_TurnStart) then

--[ ===================================== AI Begins Action ====================================== ]
--Called when a new Action begins, *not necessarily the action of the owning entity*. If it is the
-- action of the owning entity, we need to decide what action to perform. Otherwise, set variables
-- if the AI requires counting actions.
elseif(iSwitchType == gciAI_ActionBegin) then

    --[Description]
    --The Confused AI takes whatever ability it was going to use, and modifies the targeting algorithm
    -- to hit all friends and enemies, instead of just friends or just enemies. Abilities which
    -- already hit all targets, or just hit themselves, do not change.
    --The AI ignores threat and does not accumulate threat.

    --[Check Turn]
    --Compare the ID of the owner with the ID of the acting entity.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    if(iOwnerID ~= iActingID) then return end

    --[Stun]
    --Standard stun handler. Will return true if this entity is stunned.
    local bIsStunned = fnStunHandleActionBegin()
    if(bIsStunned == true) then

        --Animation.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_StunAbilitySlot)

        --Stop action here.
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
        return
    end

    --Get how many abilities are available.
    local iAbilitiesTotal = AdvCombatEntity_GetProperty("Abilities Total")
    
    --If it's zero, pass turn:
    if(iAbilitiesTotal == 0) then return end

    --[Select Ability]
    --Roll. Start at gciAI_AbilityRollStart since there are special system abilities, like "Stunned", below that.
    local iAbilityRoll = LM_GetRandomNumber(gciAI_AbilityRollStart, iAbilitiesTotal-1)
    AdvCombat_SetProperty("Set Ability As Active", iAbilityRoll)
    
    --Next, paint targets. This will call a script. The script will run a target routine, which is stored.
    AdvCombat_GetProperty("Last Target Code")
    AdvCombat_SetProperty("Run Active Ability Target Script")
    local sUsedTargetCode = AdvCombat_GetProperty("Last Target Code")
    
    --[Self Target]
    --Proceeds as normal. 
    if(sUsedTargetCode == "Target Self") then
        AdvCombat_SetProperty("Set Target Cluster As Active", 0)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
    
    --[Single Target]
    --Expand the target to include everyone and pick a random one, ignoring threat.
    elseif(sUsedTargetCode == "Target Enemies Single" or sUsedTargetCode == "Target Allies Single") then
        AdvCombat_SetProperty("Run Target Macro", "Target All Single", iOwnerID)
    
    --If the target code was all-single, we can just use that without re-running anything.
    elseif(sUsedTargetCode == "Target All Single") then
        local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
        local iTargetCluster = LM_GetRandomNumber(0, iTotalTargetClusters-1)
        AdvCombat_SetProperty("Set Target Cluster As Active", iTargetCluster)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
    
    --[Group Target]
    --Targets all allies or enemies, made to choose between the two instead.
    elseif(sUsedTargetCode == "Target Enemies All" or sUsedTargetCode == "Target Allies All") then
        AdvCombat_SetProperty("Run Target Macro", "Target Parties", iOwnerID)
        local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
        local iTargetCluster = LM_GetRandomNumber(0, iTotalTargetClusters-1)
        AdvCombat_SetProperty("Set Target Cluster As Active", iTargetCluster)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
    
    --Target parties, don't re-run anything.
    elseif(sUsedTargetCode == "Target Parties") then
        local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
        local iTargetCluster = LM_GetRandomNumber(0, iTotalTargetClusters-1)
        AdvCombat_SetProperty("Set Target Cluster As Active", iTargetCluster)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
    
    --[Target Everyone]
    --Abilities that hit everyone, friend and foe, do not change.
    elseif(sUsedTargetCode == "Target All") then
        AdvCombat_SetProperty("Set Target Cluster As Active", 0)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
    end

    --[Target Selection]
    --Only one target cluster, use the 0th.
    local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
    if(iTotalTargetClusters <= 1) then
        AdvCombat_SetProperty("Set Target Cluster As Active", 0)
    
    --Roll a random cluster.
    else
        local iTargetCluster = LM_GetRandomNumber(0, iTotalTargetClusters-1)
        AdvCombat_SetProperty("Set Target Cluster As Active", iTargetCluster)
    end

    --Run handler.
    AdvCombat_SetProperty("Mark Handled Action")
    AdvCombat_SetProperty("Run Active Ability On Active Targets")

--[ ====================================== AI Ends Action ======================================= ]
--Called after an action ends, before the turn order is popped.
elseif(iSwitchType == gciAI_ActionEnd) then

--[ ========================================= Turn Ends ========================================= ]
--Called after the turn ends but before turn order is resolved for the next turn or reinforcements
-- arrive, if any do.
elseif(iSwitchType == gciAI_TurnEnd) then

--[ ====================================== AI is Defeated ======================================= ]
--Called when this entity is KO'd. Note that an enemy can be revived (in theory) which can cause
-- this to execute each time they are KO'd.
elseif(iSwitchType == gciAI_KnockedOut) then

--[ ======================================== Combat Ends ======================================== ]
--Called when combat ends but before XP/JP/etc are awarded.
elseif(iSwitchType == gciAI_CombatEnds) then

--[ ================================== Application Pack Before ================================== ]
--Called right before an application pack is processed, allowing querying and modification.
elseif(iSwitchType == gciAI_Application_Start) then

--[ ================================== Application Pack After =================================== ]
--Called right after an application pack is processed, allowing querying.
elseif(iSwitchType == gciAI_Application_End) then
    --This AI does not handle threat.

end
