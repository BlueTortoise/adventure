--[ =========================================== Attack ========================================== ]
--Strike with the equipped weapon.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(1, "N")

--[ ======================================== UI Preview ========================================= ]
--Called when the ability is created and added to the job prototype. This shows the ability's name
-- and icon, to inform the player what abilities are on the job.
if(iSwitchType == 0) then


--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. The ability is
-- stored in the AdvCombatEntity.
elseif(iSwitchType == 1) then


--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == 2) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == 3) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == 4) then

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == 5) then

--[ ==================================== Combat: Pick Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == 6) then

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == 7) then

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == 8) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == 9) then

end
