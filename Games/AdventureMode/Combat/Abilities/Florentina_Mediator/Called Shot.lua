--[ ======================================== Called Shot ======================================== ]
--[Description]
--Next attack is guaranteed to hit/crit on the target. Costs 2 CP.

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Variables
    local sJobName = "Mediator"
    local sSkillName = "Called Shot"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = sJobName .. "Internal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Called Shot")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Direct)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Special)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Florentina|CalledShot")
        AdvCombatAbility_SetProperty("CP Icon",      "Root/Images/AdventureUI/Abilities/Cmb2")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Next attack against the marked target is a critical hit.\n\nRequires 2[IMG0]. Single enemy. Free Action.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 1)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/ComboPoint")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    AdvCombatAbility_SetProperty("Usable", false)
    if(fnCheckCP(2) == false) then return end
    
    --All checks passed.
    AdvCombatAbility_SetProperty("Usable", true)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    AdvCombatAbility_SetProperty("Usable", false)
    if(fnCheckCP(2) == false) then return end
    
    --Cooldown.
    local iCooldown = fnModifyCooldown(-1)
    if(iCooldown > 0) then return end
    
    --Ability requires a free action to be available.
    AdvCombatAbility_SetProperty("Push Owner")
        local iFreeActionsNow = AdvCombatEntity_GetProperty("Free Actions Available")
    DL_PopActiveObject()
    if(iFreeActionsNow < 1) then return end
    
    --Must respect the actions cap, meaning so many free actions can be performed.
    AdvCombatAbility_SetProperty("Push Owner")
        local iFreeActionsTotal = AdvCombatEntity_GetProperty("Free Actions Performed")
        local iFreeActionsCap   = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_FreeActionMax)
    DL_PopActiveObject()
    if(iFreeActionsTotal >= iFreeActionsCap) then return end
    
    --All checks passed.
    AdvCombatAbility_SetProperty("Usable", true)

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Enemies Single", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    --[ ========== Setup ========== ]
    --This routine create a package that contains all the default values needed to build a Prediction Box. We
    -- then change any values that aren't default.
    local zPackage = fnCreatePredictionPack()
    zPackage.zaEffectModules[1] = fnCreateEffectModule()
    zPackage.zaEffectModules[1].bIsBuff = true
    zPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zPackage.zaEffectModules[1].iEffectStrength = 1000
    zPackage.zaEffectModules[1].sResultScript = "Next Attack Crits"
    zPackage.zaEffectModules[1].saResultImages = {""}

    --[ ========== Other Values ========== ]
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --[ ========== For Each Target ==========]
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zPackage)
    end

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then
    
    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID  = RO_GetID()
        fnModifyCP(-2)
    DL_PopActiveObject()
    
    --[========== Ability Package ========== ]
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Terrify")
    AdvCombatAbility_SetProperty("Cooldown", 1)
    AdvCombat_SetProperty("Set As Free Action")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    zaAbilityPackage.bEffectAlwaysApplies = true
    
    --[========== Effect Package ========== ]
    --Create an effect package and populate it.
    local zaEffectPackage = fnConstructDefaultEffectPackage()
    zaEffectPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides.
    zaEffectPackage.sApplyAnimation = "Null"
    zaEffectPackage.sApplySound     = "Null"
    zaEffectPackage.sEffectPath     = gsRoot .. "Combat/Effects/Florentina_Mediator/Called Shot.lua"
    zaEffectPackage.sEffectCritPath = gsRoot .. "Combat/Effects/Florentina_Mediator/Called Shot.lua"
    zaEffectPackage.sApplyText      = "Called Shot!"
    zaEffectPackage.sApplyTextCol   = "Color:Purple"
    
    --[ ========== For Each Target ==========]
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --[ ========== Target Info ========== ]
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
            local bIsNormalTarget = AdvCombatEntity_GetProperty("Is Normal Target")
        DL_PopActiveObject()
        
        --Store the timer offset.
        local iTimerOffset = giCombatTimerOffset
        giCombatTimerOffset = 0
        
        --Not a normal target. This can happen if the target was hit by another event before we get to this execution
        -- section, in which case they might be time-stopped or KO'd or somesuch.
        if(bIsNormalTarget == false) then return end
        
        --Animate the ability.
        local iTimer = (gciAbility_TicksOffsetPerTarget * (i-1)) + iTimerOffset
        if(zaAbilityPackage.bDoesNotAnimate == false) then
            iTimer = zaAbilityPackage.fnAbilityAnimate(iTargetUniqueID, iTimer, zaAbilityPackage, false)
        end
    
        --If there is an effect, apply it.
        if(zaEffectPackage ~= nil) then
            iTimer = zaEffectPackage.fnHandler(iOriginatorID, iTargetUniqueID, iTimer, zaEffectPackage, true, false)
        end
        fnDefaultEndOfApplications(iTimer)

    end

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

end
