--[ ======================================== Pick Pocket ======================================== ]
--[Description]
--Inflict -50 Acc for 3 turns, strength

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Variables
    local sJobName = "TreasureHunter"
    local sSkillName = "Pick Pocket"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = sJobName .. "Internal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Pick Pocket")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Free_Debuff)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Free_Active)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Florentina|PickPocket")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Steals platina equal to 0.50x [IMG0].\n[IMG1][IMG2][IMG3] -5/1[IMG4].\nCan steal once per target. Free action.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 5)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
        AdvCombatAbility_SetProperty("Description Image", 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/EffectIndicator/EffectHostile")
        AdvCombatAbility_SetProperty("Description Image", 2, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StrengthIndicator/Str5")
        AdvCombatAbility_SetProperty("Description Image", 3, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Protection")
        AdvCombatAbility_SetProperty("Description Image", 4, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Turns")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    fnAbilityStandardBeginAction(0, true, 1, true)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    fnAbilityStandardBeginFreeAction(0, true, 1, true)

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Enemies Single", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    --[ ========== Setup ========== ]
    --This routine create a package that contains all the default values needed to build a Prediction Box. We
    -- then change any values that aren't default.
    local zPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zPackage.bHasHitRateModule = true
    zPackage.zHitRateModule.iMissRate = 5
    
    --Create an Effect module
    zPackage.zaEffectModules[1] = fnCreateEffectModule()
    zPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zPackage.zaEffectModules[1].iEffectStrength = 5
    zPackage.zaEffectModules[1].iSeverity = 5
    zPackage.zaEffectModules[1].sResultScript = "-[SEV] [IMG1] / 1[IMG2]"
    zPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Protection", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    zPackage.zaEffectModules[1].saApplyTagBonus = {}
    zPackage.zaEffectModules[1].saApplyTagMalus = {}
    zPackage.zaEffectModules[1].saSeverityTagBonus = {}
    zPackage.zaEffectModules[1].saSeverityTagMalus = {}
    
    --Additional Lines.
    zPackage.zaAdditionalLines = {}

    --[ ========== Other Values ========== ]
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        local bIsStickyFingersEquipped = AdvCombatEntity_GetProperty("Is Ability Equipped", "TreasureHunter|Sticky Fingers")
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --[ ========== For Each Target ==========]
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Check if we already pickpocketed the enemy.
        local iHasPickpocketed = VM_GetVar("Root/Variables/Combat/" .. iTargetID .. "/iWasPickpocketed", "N")
        if(iHasPickpocketed == 0.0) then
            
            --Determine amount stolen.
            local iPickPocketPower = math.floor(iAttackPower * 0.50)
            if(bIsStickyFingersEquipped) then iPickPocketPower = math.floor(iPickPocketPower * 1.15) end
            if(iPickPocketPower < 1) then iPickPocketPower = 1 end
            
            --Display.
            zPackage.zaAdditionalLines[1] = {}
            zPackage.zaAdditionalLines[1].sString = ""
            zPackage.zaAdditionalLines[1].saSymbols = {}
            zPackage.zaAdditionalLines[2] = {}
            zPackage.zaAdditionalLines[2].sString = "Steal " .. iPickPocketPower .. " platina."
            zPackage.zaAdditionalLines[2].saSymbols = {}
            if(bIsStickyFingersEquipped) then
                zPackage.zaAdditionalLines[3] = {}
                zPackage.zaAdditionalLines[3].sString = "+15%% bonus from Sticky Fingers!"
                zPackage.zaAdditionalLines[3].saSymbols = {}
            end
        
        --Already pickpocketed.
        else
            zPackage.zaAdditionalLines[1].sString = "Already pickpocketed!"
            zPackage.zaAdditionalLines[1].saSymbols = {}
        end
        
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zPackage)
    end

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
    DL_PopActiveObject()
    
    --[========== Ability Package ========== ]
    --Apply cooldown for this ability. Mark the action as free.
    AdvCombatAbility_SetProperty("Cooldown", 1)
    AdvCombat_SetProperty("Set As Free Action")
    
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Pierce")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    zaAbilityPackage.bDoesNoDamage = true
    zaAbilityPackage.bNeverGlances = true
    zaAbilityPackage.bDoesNoStun = true
    zaAbilityPackage.bNeverCrits = true
    
    --No benefit from the "Called Shot" ability.
    zaAbilityPackage.saTags = {"Florentina No Called Shot"}
    
    --[========== Effect Package ========== ]
    --Create an effect package and populate it.
    local zaEffectPackage = fnConstructDefaultEffectPackage()
    zaEffectPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides.
    zaEffectPackage.iEffectStr      = 5
    zaEffectPackage.iEffectCritStr  = 8
    zaEffectPackage.iEffectType     = gciDamageType_Piercing
    zaEffectPackage.sEffectPath     = gsRoot .. "Combat/Effects/Florentina_TreasureHunter/Pick Pocket.lua"
    zaEffectPackage.sEffectCritPath = gsRoot .. "Combat/Effects/Florentina_TreasureHunter/Pick Pocket.lua"
    zaEffectPackage.sApplyText      = "Vulnerable!"
    zaEffectPackage.sApplyTextCol   = "Color:Purple"
    zaEffectPackage.sResistText     = "Resisted!"
    zaEffectPackage.sResistTextCol  = "Color:White"
    
    --Tags.
    zaEffectPackage.saApplyBonusTags    = {}
    zaEffectPackage.saApplyMalusTags    = {}
    zaEffectPackage.saSeverityBonusTags = {}
    zaEffectPackage.saSeverityMalusTags = {}
    
    --[ ========== For Each Target ==========]
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, zaEffectPackage)
        
        --Get if the target was pickpocketed:
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
        DL_PopActiveObject()
        
        --Check if we already pickpocketed the enemy.
        local iHasPickpocketed = VM_GetVar("Root/Variables/Combat/" .. iTargetUniqueID .. "/iWasPickpocketed", "N")
        if(iHasPickpocketed == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Combat/" .. iTargetUniqueID .. "/iWasPickpocketed", "N", 1.0)
            
            --Determine amount stolen.
            local iPickPocketPower = math.floor(iAttackPower * 0.50)
            if(iPickPocketPower < 1) then iPickPocketPower = 1 end
            
            --Store.
            DL_AddPath("Root/Variables/Combat/Default/")
            VM_SetVar("Root/Variables/Combat/Default/iPickPocket", "N", iPickPocketPower)
            
            --Report the steal value.
            local sScriptPath = LM_GetCallStack(0)
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, 45, "Run Script|" .. sScriptPath .. "|N:-1000")
        end
    end
    
--[ ========================================== Cutscene ========================================= ]
--Special
elseif(iSwitchType == -1000) then

    --Stole string.
    local iStoleValue = VM_GetVar("Root/Variables/Combat/Default/iPickPocket", "N")
    local sString = "WD_SetProperty(\"Append\", \"Florentina: Stole " .. iStoleValue .. " platina!\")"

    --Base.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction(sString)
    fnCutsceneBlocker()
        
--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

end
