--[ ====================================== Call Florentina ====================================== ]
--[Description]
--Calls Florentina!

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create) then
    AdvCombatEntity_SetProperty("Create Ability", "Common|Call Florentina")
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Call Florentina")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Buff)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Active)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Florentina|Botany")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Calls Florentina to help!")
        AdvCombatAbility_SetProperty("Allocate Description Images", 1)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Protection")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then

    --If Florentina does not exist, stop here.
    AdvCombatAbility_SetProperty("Usable", false)
    if(AdvCombat_GetProperty("Does Party Member Exist", "Florentina") == false) then return end

    --Get Florentina's ID for queries.
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        local iFlorentinaID = RO_GetID()
    DL_PopActiveObject()
    
    --If Florentina is in the player party, we can't call her. She could also theoretically be in the enemy
    -- party. The only way to call her is if she's in gciACPartyGroup_None
    local iFlorentinaInParty = AdvCombat_GetProperty("Party Of ID", iFlorentinaID)
    if(iFlorentinaInParty ~= gciACPartyGroup_None) then return end

    --Florentina is in the party roster. This ability is usable.
    AdvCombatAbility_SetProperty("Usable", true)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then

    --If Florentina does not exist, stop here.
    AdvCombatAbility_SetProperty("Usable", false)
    if(AdvCombat_GetProperty("Does Party Member Exist", "Florentina") == false) then return end

    --Get Florentina's ID for queries.
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        local iFlorentinaID = RO_GetID()
    DL_PopActiveObject()
    
    --If Florentina is in the player party, we can't call her. She could also theoretically be in the enemy
    -- party. The only way to call her is if she's in gciACPartyGroup_None
    local iFlorentinaInParty = AdvCombat_GetProperty("Party Of ID", iFlorentinaID)
    if(iFlorentinaInParty ~= gciACPartyGroup_None) then return end

    --Florentina is in the party roster. This ability is usable.
    AdvCombatAbility_SetProperty("Usable", true)

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Self", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Get Florentina's ID.
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        local iFlorentinaID = RO_GetID()
    DL_PopActiveObject()
    
    --Begin
    local iTimer = 0
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iFlorentinaID, iTimer, "Change Party|" .. gciACPartyGroup_Party)
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iFlorentinaID, iTimer, "Position|" .. gciACPoscode_Party)
    iTimer = iTimer + gciAC_StandardMoveTicks
    
    --Dialogue.
    local sScriptPath = LM_GetCallStack(0)
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iFlorentinaID, iTimer, "Run Script|" .. sScriptPath .. "|N:-1000")
    iTimer = iTimer + 15
    
    --End
    AdvCombat_SetProperty("Event Timer", iTimer)

--[ ==================================== Florentina Shows Up ==================================== ]
--Special
elseif(iSwitchType == -1000) then

    --Debug script.
    WD_SetProperty("Show")
    WD_SetProperty("Major Sequence", true)
    WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") 
    WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral")
    WD_SetProperty("Append", "Florentina:[E|Neutral] You rang?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Mei:[E|Neutral] It's a sample cutscene.")

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

--[ ==================================== Combat: Event Queued ==================================== ]
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then
end
