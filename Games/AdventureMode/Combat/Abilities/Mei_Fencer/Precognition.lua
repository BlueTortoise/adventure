--[ ======================================= Precognition ======================================== ]
--[Description]
--Chance to counter-attack an enemy that targets Mei once per turn.

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Variables
    local sJobName = "Fencer"
    local sSkillName = "Precognition"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = "FencerInternal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", sSkillName)
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Direct)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Passive)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Mei|Precognition")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "15%% chance to attack an enemy that targets Mei.\nAttack lands before enemy attack begins.\nOnly activates once per turn.\nAttack deals [1.0x [IMG0]] as weapon-damage.\n\nPassive.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 1)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_Counterattack)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 0.0)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", 0.0)

    --Spawn an Effect that will buff Slash resist. This is not animated.
    local sEffectPath = gsRoot .. "Combat/Effects/Mei_Fencer/Precognition.lua"

    --Get the owner for their ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Spawn the effect.
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. sEffectPath .. "|Originator:" .. iOriginatorID)

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then
    local iUniqueID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 0.0)

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    local iUniqueID = RO_GetID()
    local iTargetID = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N")
    AdvCombat_SetProperty("Create Target Cluster", "Precog Cluster", "Attacker")
    AdvCombat_SetProperty("Add Target To Cluster", "Precog Cluster", iTargetID)
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then
    --Never performs prediction routines.

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID    = RO_GetID()
        local sAttackAnimation = AdvCombatEntity_GetProperty("Weapon Attack Animation")
        local sAttackSound     = AdvCombatEntity_GetProperty("Weapon Attack Sound")
        local sCriticalSound   = AdvCombatEntity_GetProperty("Weapon Critical Sound")
    DL_PopActiveObject()
    
    --Switch the weapon values to use defaults if any were in error. This can occur if the equipped
    -- weapon was configured incorrectly or did not exist.
    if(sAttackAnimation == "Null") then sAttackAnimation = gsDefaultAttackAnimation end
    if(sAttackSound     == "Null") then sAttackSound     = gsDefaultAttackSound     end
    if(sCriticalSound   == "Null") then sCriticalSound   = gsDefaultCriticalSound   end
    
    --[========== Ability Package ========== ]
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Weapon")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides. We only edit parts that may not be standard.
    zaAbilityPackage.iMissThreshold   = 5
    zaAbilityPackage.sAttackAnimation = sAttackAnimation
    zaAbilityPackage.sAttackSound     = sAttackSound
    zaAbilityPackage.sCriticalSound   = sCriticalSound
    zaAbilityPackage.bCauseBlackFlash = true
    
    --[ ========== For Each Target ==========]
    --Get how many targets were painted by this ability, iterate across them. Call the standard
    -- execution subroutine.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, nil)
    end

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

--[ ==================================== Combat: Event Queued ==================================== ]
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then

    --If this is the equipped version of Precognition, check if Mei has the internal version. If so,
    -- increase firing chance by 30%.
    local iChanceBonus = 0
    local sInternalName = AdvCombatAbility_GetProperty("Internal Name")
    if(sInternalName == "Fencer|Precognition") then
        
        --Check if the internal version is equipped as well.
        AdvCombatAbility_SetProperty("Push Owner")
            local bIsInternalEquipped = AdvCombatEntity_GetProperty("Is Ability Equipped", "FencerInternal|Precognition")
        DL_PopActiveObject()
        
        --If the internal version is equipped, set this flag.
        if(bIsInternalEquipped) then
            iChanceBonus = 15
        end
        
    --If this is the internal version of precognition, check if Mei has the equipped version. If so,
    -- don't do anything.
    else
        
        --Check if the equipped version is equipped as well.
        AdvCombatAbility_SetProperty("Push Owner")
            local bIsInternalEquipped = AdvCombatEntity_GetProperty("Is Ability Equipped", "Fencer|Precognition")
        DL_PopActiveObject()

        --If so, stop here.
        if(bIsInternalEquipped) then
            return
        end

    end

    --Check if the ability already executed this turn:
    local iUniqueID = RO_GetID()
    local iExecThisTurn = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N")
    if(iExecThisTurn == 1.0) then
        return
    end
    
    --Get the ID of the owner of this ability.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    local iOwnerGroup = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    --Get the originator, and check if it's a hostile entity.
    local iOriginatorID = AdvCombat_GetProperty("Query Originator ID")
    local iOriginatorGroup = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    
    --To be valid, the owner must be in a different party than the originator, and both must be in either
    -- the player's active party or the enemy's active party.
    if(iOwnerGroup == iOriginatorGroup) then
        return
    end
    if(iOwnerGroup ~= gciACPartyGroup_Party and iOwnerGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    if(iOriginatorGroup ~= gciACPartyGroup_Party and iOriginatorGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    
    --Check the target cluster. If the owner is found in the target cluster, we can counterattack.
    local bIsInTargetCluster = false
    local iTargetsTotal = AdvCombat_GetProperty("Query Targets Total")
    for i = 1, iTargetsTotal, 1 do
        local iTargetID = AdvCombat_GetProperty("Query Targets ID", i-1)
        if(iTargetID == iOwnerID) then
            bIsInTargetCluster = true
            break
        end
    end
    
    if(bIsInTargetCluster == false) then
        return
    end
    
    --Lastly, roll. This ability has a 15% chance to fire.
    local iRoll = LM_GetRandomNumber(1, 100)
    
    --Collect tags applying to Mei.
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
        local iAdditive    = AdvCombatEntity_GetProperty("Tag Count", "Counterattack +")
        local iSubtractive = AdvCombatEntity_GetProperty("Tag Count", "Counterattack -")
    DL_PopActiveObject()
    
    --The additive tags subtract from the roll since lower is better.
    iRoll = iRoll - iAdditive + iSubtractive - iChanceBonus
    
    --Set this to true to make Precognition always activate. Use for debug.
    if(false) then
        iRoll = 0
    end
    if(iRoll <= 15) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 1.0)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", iOriginatorID)
        AdvCombatAbility_SetProperty("Enqueue This Ability As Event", -1)
    end
end
