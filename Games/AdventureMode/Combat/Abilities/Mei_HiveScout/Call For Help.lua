--[ ======================================= Call For Help ======================================= ]
--[Description]
--Calls a Bee ally for the rest of the battle! Costs CP.

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Variables
    local sJobName = "HiveScout"
    local sSkillName = "CallForHelp"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = "HiveScoutInternal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Call For Help")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Buff)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Active)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Mei|CallForHelp")
        AdvCombatAbility_SetProperty("CP Icon",      "Root/Images/AdventureUI/Abilities/Cmb4")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Calls a friendly bee girl to help!\n\nCosts 4[IMG0].\nUsable once per battle.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 1)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/ComboPoint")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHasUsed", "N", 0.0)

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then

    --Neutralize.
    AdvCombatAbility_SetProperty("Usable", false)

    --Can only be used once per battle.
    local iUniqueID = RO_GetID()
    local iHasUsed = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHasUsed", "N")
    if(iHasUsed == 1.0) then return end

    --Player must have 4 CP.
    if(fnCheckCP(4) == false) then return end
    
    --Cannot be called if there are 6 entities in the active party.
    if(AdvCombat_GetProperty("Combat Party Size") >= 4) then return end

    --Ability is usable.
    AdvCombatAbility_SetProperty("Usable", true)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then

    --Neutralize.
    AdvCombatAbility_SetProperty("Usable", false)

    --Can only be used once per battle.
    local iUniqueID = RO_GetID()
    local iHasUsed = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHasUsed", "N")
    if(iHasUsed == 1.0) then return end

    --Player must have 4 CP.
    if(fnCheckCP(4) == false) then return end
    
    --Cannot be called if there are 6 entities in the active party.
    if(AdvCombat_GetProperty("Combat Party Size") >= 4) then return end

    --Ability is usable.
    AdvCombatAbility_SetProperty("Usable", true)

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Self", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then
    
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Box.
    local sBoxName = "Prd0x" .. iOwnerID
    AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
    AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
    AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
    AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "Summon a bee ally to help you!")

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(-4)
    DL_PopActiveObject()
    
    --Mark the ability as used.
    local iAbilityID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iAbilityID .. "/iHasUsed", "N", 1.0)
    
    --[========== Ability Package ========== ]
    --Apply cooldown for this ability. Mark the action as free.
    AdvCombatAbility_SetProperty("Cooldown", 1)
    AdvCombat_SetProperty("Set As Free Action")
    
    --Run the script to create the allied bee. The value gsLastEnemyName will be populated with the unique name.
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/100 Ally Bee.lua")
    if(giLastEnemyID == nil) then return end
    
    --Begin
    local iTimer = 0
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, giLastEnemyID, iTimer, "Change Party|" .. gciACPartyGroup_Party)
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, giLastEnemyID, iTimer, "Position|" .. gciACPoscode_Party)
    iTimer = iTimer + gciAC_StandardMoveTicks
    
    --Dialogue.
    local sScriptPath = LM_GetCallStack(0)
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, giLastEnemyID, iTimer, "Run Script|" .. sScriptPath .. "|N:-1000")
    iTimer = iTimer + 15
    
    --End
    AdvCombat_SetProperty("Event Timer", iTimer)
    
    --Flag reset.
    giTargetID = giLastEnemyID
    giLastEnemyID = nil
            
--[ =========================================  Cutscene ========================================= ]
--Special
elseif(iSwitchType == -1000) then

    --[ ===== Error Check =====]
    --Make sure the target exists.
    if(giTargetID == nil) then return end
    
    --[ ===== Cutscene Handling =====]
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])
    
    --If Mei is a bee:
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    if(sMeiForm == "Bee") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Bee:[E|Neutral] Bzzz bzz? (You called?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Bzzz! BzzZzzZZz! (Yes! Thank you for coming, please help me!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Bee:[E|Neutral] zzzzZZ! (For the hive!)") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Bee:[E|Neutral] Bzzz bzz?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uhhhh... bzzz! Bbzz? I think I said that wrong...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Bee:[E|Neutral] zzzzZZ!") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AdvCombat_SetProperty("Position Party Normally") ]])
    fnCutsceneBlocker()
    
    --Clean up.
    giTargetID = nil

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

end
