--[ ======================================= Duster Blast ======================================== ]
--[Description]
--Inflict -35 Acc for 3 turns and piercing damage.

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Variables
    local sJobName = "Maid"
    local sSkillName = "DusterBlast"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = "MaidInternal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Duster Blast")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Debuff)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Active)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Mei|DusterBlast")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Inflicts [0.25x [IMG0]] as [IMG2].\nBase Hit Rate: 125%%\n[IMG3][IMG4][IMG2] -35[IMG1]. 3[IMG7].\n\n20[IMG5]. All Enemies.\nGenerates 1[IMG6].")
        AdvCombatAbility_SetProperty("Allocate Description Images", 8)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
        AdvCombatAbility_SetProperty("Description Image", 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Accuracy")
        AdvCombatAbility_SetProperty("Description Image", 2, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/Piercing")
        AdvCombatAbility_SetProperty("Description Image", 3, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/EffectIndicator/EffectHostile")
        AdvCombatAbility_SetProperty("Description Image", 4, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StrengthIndicator/Str6")
        AdvCombatAbility_SetProperty("Description Image", 5, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Mana")
        AdvCombatAbility_SetProperty("Description Image", 6, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/ComboPoint")
        AdvCombatAbility_SetProperty("Description Image", 7, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Turns")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    fnAbilityStandardBeginAction(20, false, 0, false)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    fnAbilityStandardBeginFreeAction(20, false, 0, false)

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Enemies All", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    --[ ========== Setup ========== ]
    --This routine create a package that contains all the default values needed to build a Prediction Box. We
    -- then change any values that aren't default.
    local zPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zPackage.bHasHitRateModule = true
    zPackage.zHitRateModule.iMissRate = -25
    
    --Crit Module
    zPackage.bHasCritModule = true
    zPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zPackage.bHasDamageModule = true
    zPackage.zDamageModule.iDamageType = gciDamageType_Piercing
    zPackage.zDamageModule.fDamageFactor = 0.25
    
    --Create an Effect module
    zPackage.zaEffectModules[1] = fnCreateEffectModule()
    zPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zPackage.zaEffectModules[1].iEffectStrength = 6
    zPackage.zaEffectModules[1].iSeverity = 35
    zPackage.zaEffectModules[1].sResultScript = "-[SEV] [IMG1] / 3[IMG2]"
    zPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Accuracy", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    zPackage.zaEffectModules[1].saApplyTagBonus = {"Blind Apply +"}
    zPackage.zaEffectModules[1].saApplyTagMalus = {"Blind Apply -"}
    zPackage.zaEffectModules[1].saSeverityTagBonus = {"Blind Effect +"}
    zPackage.zaEffectModules[1].saSeverityTagMalus = {"Blind Effect -"}

    --[ ========== Other Values ========== ]
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --[ ========== For Each Target ==========]
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zPackage)
    end

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(1)
        fnModifyMP(-20)
    DL_PopActiveObject()
    
    --[========== Ability Package ========== ]
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Pierce")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides. We only edit parts that may not be standard.
    zaAbilityPackage.iDamageType    = gciDamageType_Piercing
    zaAbilityPackage.iMissThreshold = -25
    zaAbilityPackage.fDamageFactor  = 0.25
    
    --[========== Effect Package ========== ]
    --Create an effect package and populate it.
    local zaEffectPackage = fnConstructDefaultEffectPackage()
    zaEffectPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides.
    zaEffectPackage.iEffectStr      = 6
    zaEffectPackage.iEffectCritStr  = 9
    zaEffectPackage.iEffectType     = gciDamageType_Piercing
    zaEffectPackage.sEffectPath     = gsRoot .. "Combat/Effects/Mei_Maid/Duster Blast.lua"
    zaEffectPackage.sEffectCritPath = gsRoot .. "Combat/Effects/Mei_Maid/Duster Blast Crit.lua"
    zaEffectPackage.sApplyText      = "Blinded!"
    zaEffectPackage.sApplyTextCol   = "Color:Purple"
    zaEffectPackage.sResistText     = "Resisted!"
    zaEffectPackage.sResistTextCol  = "Color:White"
    
    --Tags.
    zaEffectPackage.saApplyBonusTags    = {"Blind Apply +"}
    zaEffectPackage.saApplyMalusTags    = {"Blind Apply -"}
    zaEffectPackage.saSeverityBonusTags = {"Blind Effect +"}
    zaEffectPackage.saSeverityMalusTags = {"Blind Effect -"}
    
    --[ ========== For Each Target ==========]
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, zaEffectPackage)
    end

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

end
