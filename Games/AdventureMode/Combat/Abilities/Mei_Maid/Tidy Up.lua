--[ ========================================== Tidy Up ========================================== ]
--[Description]
--Removes all poisons and corrode effects, adds Fast to the party.

--[Notes]
--For Code gciAbility_Create, the ability is already active, having been created by the owning item.
--For Code gciAbility_AssumeJob, this is never called.
--For all other codes, the active object is the AdvCombatAbility itself. The owner can be pushed.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created at the start of combat.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Variables
    local sJobName = "Maid"
    local sSkillName = "TidyUp"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = "MaidInternal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Tidy Up")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Buff)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Active)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Mei|TidyUp")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Removes all [IMG0] and [IMG1] DoTs.\nProvides [Fast] for 1[IMG4].\n20[IMG2]. Generates 1[IMG3]. All Allies.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 5)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/Poisoning")
        AdvCombatAbility_SetProperty("Description Image", 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/Corroding")
        AdvCombatAbility_SetProperty("Description Image", 2, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Mana")
        AdvCombatAbility_SetProperty("Description Image", 3, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/ComboPoint")
        AdvCombatAbility_SetProperty("Description Image", 4, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Turns")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then
        fnAbilityStandardBeginAction(20, false, 0, false)
    end

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then
        fnAbilityStandardBeginAction(20, false, 0, false)
    end

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Allies All", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    --[ ========== Setup ========== ]
    --This routine create a package that contains all the default values needed to build a Prediction Box. We
    -- then change any values that aren't default.
    local zPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zPackage.zaEffectModules[1] = fnCreateEffectModule()
    zPackage.zaEffectModules[1].bIsBuff = true
    zPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zPackage.zaEffectModules[1].iEffectStrength = 1000
    zPackage.zaEffectModules[1].sResultScript = "+[Fast], Clear [IMG0] and [IMG1] DoTs"
    zPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DamageTypeIcons/Poisoning", "Root/Images/AdventureUI/DamageTypeIcons/Corroding"}

    --[ ========== Other Values ========== ]
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --[ ========== For Each Target ==========]
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zPackage)
    end

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyMP(-20)
    DL_PopActiveObject()
    
    --[========== Ability Package ========== ]
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Haste")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides. We only edit parts that may not be standard.
    zaAbilityPackage.bDoesNoDamage = true
    zaAbilityPackage.bDoesNoStun = true
    zaAbilityPackage.bAlwaysHits = true
    zaAbilityPackage.bEffectAlwaysApplies = true
    
    --Additional Paths
    zaAbilityPackage.saExecPaths = {LM_GetCallStack(0)}
    zaAbilityPackage.iaExecCodes = {gciAbility_SpecialStart}
    
    --[========== Effect Package ========== ]
    --Create an effect package and populate it.
    local zaEffectPackage = fnConstructDefaultEffectPackage()
    zaEffectPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides.
    zaEffectPackage.sApplyAnimation = "Null"
    zaEffectPackage.sApplySound     = "Null"
    zaEffectPackage.sEffectPath     = gsRoot .. "Combat/Effects/Mei_Maid/Tidy Up.lua"
    zaEffectPackage.sEffectCritPath = gsRoot .. "Combat/Effects/Mei_Maid/Tidy Up.lua"
    zaEffectPackage.sApplyText      = "Fast"
    zaEffectPackage.sApplyTextCol   = "Color:Blue"
    
    --[ ========== For Each Target ==========]
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, zaEffectPackage)
    end

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

--[ ==================================== Combat: Event Queued ==================================== ]
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then

--[ ===================== Combat: Special - Remove Poison and Corrode DoTs ====================== ]
elseif(iSwitchType == gciAbility_SpecialStart) then

    --Argument handling.
    if(iArgumentsTotal < 3) then return end
    local iOriginatorID   = LM_GetScriptArgument(1, "N")
    local iTargetUniqueID = LM_GetScriptArgument(2, "N")
    
    --Setup.
    local iEffectCount = 0
    local iaEffectIDList = {}
    
    --Get a list of all effects referencing the target. Remove any with the matching tag.
    local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", iTargetUniqueID)
    for i = 0, iEffectsTotal-1, 1 do
        AdvCombat_SetProperty("Push Temp Effects", i)
        
            --Tag is present:
            local iCount = AdvCombatEffect_GetProperty("Get Tag Count", "Poison DoT") + AdvCombatEffect_GetProperty("Get Tag Count", "Corrode DoT")
            if(iCount > 0) then
                iEffectCount = iEffectCount + 1
                iaEffectIDList[iEffectCount] = RO_GetID()
            end
        DL_PopActiveObject()
    end
    
    --Clean up.
    AdvCombat_SetProperty("Clear Temp Effects")
    
    --If there was at least one effect on the list to remove, add application packs for the removal.
    if(iEffectCount > 0) then
    
        local iAnimTicks = fnGetAnimationTiming("Healing")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Play Sound|" .. pzaAbilityPackage.sAttackSound)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Create Animation|Healing|AttackAnim0")
        giStoredTimer = giStoredTimer + iAnimTicks
    
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, giStoredTimer, "Text|Cured!|Color:Green")
        for i = 1, iEffectCount, 1 do
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, giStoredTimer, "Remove Effect|" .. iaEffectIDList[i])
        end
        giStoredTimer = giStoredTimer + gciApplication_TextTicks
    end
end
