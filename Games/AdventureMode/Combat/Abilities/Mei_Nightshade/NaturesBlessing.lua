--[ ===================================== Nature's Blessing ===================================== ]
--[Description]
--Regenerate 10% of max HP per turn.

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Variables
    local sJobName = "Nightshade"
    local sSkillName = "NaturesBlessing"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = "NightshadeInternal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Nature's Blessing")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Direct)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Passive)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Mei|NaturesBlessing")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Regenerate 4%% of Max [IMG0] at the start of your turn.\n\nPassive.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 1)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Health")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BeginAction, true)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == false) then return end
    --Get ID.
    AdvCombat_SetProperty("Push Acting Entity")
        local iActingEntityID = RO_GetID()
        local iMaxHP = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    --Run.
    local iHealing = math.floor(iMaxHP * 0.04)
    if(iHealing < 1) then iHealing = 1 end
    local sHealingString = "Healing|" .. iHealing
    
    --Animation.
    local iTimer = 0
    AdvCombat_SetProperty("Register Application Pack", iActingEntityID, iActingEntityID, iTimer, "Play Sound|Combat\\|Heal")
    AdvCombat_SetProperty("Register Application Pack", iActingEntityID, iActingEntityID, iTimer, "Create Animation|Healing|AttackAnim0")
    iTimer = iTimer + fnGetAnimationTiming("Healing")
    
    --Healing.
    AdvCombat_SetProperty("Register Application Pack", iActingEntityID, iActingEntityID, iTimer, sHealingString)
    iTimer = iTimer + gciApplication_TextTicks
    
    --Add padding.
    iTimer = iTimer + gciApplication_EventPaddingTicks
    
    --Clamp. There is a minimum number of ticks every Event must occupy.
    if(iTimer < gciCombatTicks_StandardActionMinimum) then iTimer = gciCombatTicks_StandardActionMinimum end
    
    --Send to C++ state.
    AdvCombat_SetProperty("Event Timer", iTimer)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then
    --Never performs prediction routines.

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

--[ ==================================== Combat: Event Queued ==================================== ]
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then
end
