--[ ======================================== Spore Cloud ======================================== ]
--[Description]
--All party members have evade increased, all enemies are afflicted with a poison DoT.

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Variables
    local sJobName = "Nightshade"
    local sSkillName = "SporeCloud"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = "NightshadeInternal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Spore Cloud")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Debuff)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Active)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Mei|SporeCloud")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "[IMG0][IMG1] DoT of [0.75x [IMG2]] [IMG3] over 3[IMG4], all enemies.\n[IMG5]+25[IMG6] for 3[IMG4], all allies.\nGenerates 1[IMG8].\n\n30[IMG7]. Cooldown 3[IMG4].")
        AdvCombatAbility_SetProperty("Allocate Description Images", 9)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/EffectIndicator/EffectHostile")
        AdvCombatAbility_SetProperty("Description Image", 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StrengthIndicator/Str5")
        AdvCombatAbility_SetProperty("Description Image", 2, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
        AdvCombatAbility_SetProperty("Description Image", 3, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/Poison")
        AdvCombatAbility_SetProperty("Description Image", 4, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Turns")
        AdvCombatAbility_SetProperty("Description Image", 5, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/EffectIndicator/EffectFriendly")
        AdvCombatAbility_SetProperty("Description Image", 6, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Evade")
        AdvCombatAbility_SetProperty("Description Image", 7, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Mana")
        AdvCombatAbility_SetProperty("Description Image", 8, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/ComboPoint")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then
        fnAbilityStandardBeginAction(30, true, 0, false)
    end

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then
        fnAbilityStandardBeginAction(30, true, 0, false)
    end

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target All", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    --[ ========== Setup ==========]
    --This ability targets everyone, and therefore has two prediction box routines. One for allies, one for enemies.
    -- First, get values.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    local iOwnerParty = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    --[ ========== Offensive Package ========== ]
    --Used for the damaging part.
    local zaOffensePackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zaOffensePackage.zaEffectModules[1] = fnCreateEffectModule()
    zaOffensePackage.zaEffectModules[1].bIsDamageOverTime = true
    zaOffensePackage.zaEffectModules[1].iEffectType = gciDamageType_Poisoning
    zaOffensePackage.zaEffectModules[1].iDoTDamageType = gciDamageType_Poisoning
    zaOffensePackage.zaEffectModules[1].iEffectStrength = 5
    zaOffensePackage.zaEffectModules[1].fDotAttackFactor = 0.25
    zaOffensePackage.zaEffectModules[1].sResultScript = "[DAM][IMG1] for 3[IMG2]"
    zaOffensePackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DamageTypeIcons/Poisoning", "Root/Images/AdventureUI/DebuffIcons/Clock"}

    --[ ========== Support Package ========== ]
    --Used for allies.
    local zaSupportPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zaSupportPackage.zaEffectModules[1] = fnCreateEffectModule()
    zaSupportPackage.zaEffectModules[1].bIsBuff = true
    zaSupportPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zaSupportPackage.zaEffectModules[1].iEffectStrength = 1000
    zaSupportPackage.zaEffectModules[1].sResultScript = "+25[IMG1] / 3[IMG2]"
    zaSupportPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Evade", "Root/Images/AdventureUI/DebuffIcons/Clock"}

    --[ ========== Apply ========== ]
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --Iterate.
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Get the ID of the target.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        local iTargetParty = AdvCombat_GetProperty("Party Of ID", iTargetID)
        
        --Is it in the same party as the owner?
        if(iTargetParty == iOwnerParty) then
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zaSupportPackage)
        --Hostile.
        else
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zaOffensePackage)
        end
    end

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(1)
        fnModifyMP(-30)
    DL_PopActiveObject()
    AdvCombatAbility_SetProperty("Cooldown", 3)
    
    --[========== Offensive Package ========== ]
    --Create an ability package to be applied to unfriendly entities.
    local zaOffensePackage = fnConstructDefaultAbilityPackage("Null")
    zaOffensePackage.bDoesNoDamage = true
    zaOffensePackage.bDoesNoStun = true
    zaOffensePackage.bDoesNotAnimate = true
    zaOffensePackage.bAlwaysHits = true
    zaOffensePackage.bAlwaysCrits = false
    zaOffensePackage.bNeverCrits = true
    zaOffensePackage.bEffectAlwaysApplies = false
    
    --Offensive Effect Package
    local zaOffenseEffectPackage = fnConstructDefaultEffectPackage()
    zaOffenseEffectPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides.
    zaOffenseEffectPackage.iEffectStr      = 5
    zaOffenseEffectPackage.iEffectCritStr  = 5
    zaOffenseEffectPackage.iEffectType     = gciDamageType_Poisoning
    zaOffenseEffectPackage.sEffectPath     = gsRoot .. "Combat/Effects/Mei_Nightshade/SporeCloudOffense.lua"
    zaOffenseEffectPackage.sEffectCritPath = gsRoot .. "Combat/Effects/Mei_Nightshade/SporeCloudOffense.lua"
    zaOffenseEffectPackage.sApplyText      = "Poisoned!"
    zaOffenseEffectPackage.sApplyTextCol   = "Color:Red"
    zaOffenseEffectPackage.sResistText     = "Resisted!"
    zaOffenseEffectPackage.sResistTextCol  = "Color:White"
    
    --Tags.
    zaOffenseEffectPackage.saApplyBonusTags    = {"Poison Apply +"}
    zaOffenseEffectPackage.saApplyMalusTags    = {"Poison Apply -"}
    zaOffenseEffectPackage.saSeverityBonusTags = {"Poison Effect +"}
    zaOffenseEffectPackage.saSeverityMalusTags = {"Poison Effect -"}
    
    --[========== Support Package ========== ]
    --For friendlies, use this package.
    local zaSupportPackage = fnConstructDefaultAbilityPackage("Null")
    zaSupportPackage.bDoesNoDamage = true
    zaSupportPackage.bDoesNoStun = true
    zaSupportPackage.bDoesNotAnimate = true
    zaSupportPackage.bAlwaysHits = true
    zaSupportPackage.bAlwaysCrits = false
    zaSupportPackage.bNeverCrits = true
    zaSupportPackage.bEffectAlwaysApplies = true
    
    --Support Effect Package
    local zaSupportEffectPackage = fnConstructDefaultEffectPackage()
    zaSupportEffectPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides.
    zaSupportEffectPackage.iEffectType     = gciDamageType_Poisoning
    zaSupportEffectPackage.sEffectPath     = gsRoot .. "Combat/Effects/Mei_Nightshade/SporeCloudSupport.lua"
    zaSupportEffectPackage.sEffectCritPath = gsRoot .. "Combat/Effects/Mei_Nightshade/SporeCloudSupport.lua"
    zaSupportEffectPackage.sApplySound     = "Combat\\|Impact_Buff"
    zaSupportEffectPackage.sApplyText      = "Evade Up!"
    zaSupportEffectPackage.sApplyTextCol   = "Color:Green"
    zaSupportEffectPackage.sResistText     = "Resisted!"
    zaSupportEffectPackage.sResistTextCol  = "Color:White"
    
    --[ ========== Animation ==========]
    --Spawn several poison effects at various spots on the field.
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID,  0, "Create Animation|Poison|AttackAnim0|UseX:150|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID,  0, "Play Sound|Combat\\|Impact_Bleed")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 20, "Create Animation|Poison|AttackAnim0|UseX:750|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 20, "Play Sound|Combat\\|Impact_Bleed")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 40, "Create Animation|Poison|AttackAnim0|UseX:350|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 40, "Play Sound|Combat\\|Impact_Bleed")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 60, "Create Animation|Poison|AttackAnim0|UseX:550|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 60, "Play Sound|Combat\\|Impact_Bleed")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 80, "Create Animation|Poison|AttackAnim0|UseX:950|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 80, "Play Sound|Combat\\|Impact_Bleed")
    
    --[ ========== For Each Target ==========]
    --Party of the originator.
    local iOriginatorParty = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    
    --Compute how large an offset is needed. Each action takes gciApplication_TextTicks, so multiply
    -- that by the number of targets.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    local iOffset = 65 - (gciApplication_TextTicks * iTargetsTotal)
    if(iOffset < 0) then iOffset = 0 end
    
    --Get how many targets were painted by this ability, iterate across them.
    for i = 1, iTargetsTotal, 1 do
        
        --Get party of target.
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
        DL_PopActiveObject()
        local iTargetParty = AdvCombat_GetProperty("Party Of ID", iTargetUniqueID)
        
        --Timer offset to allow animations to play. Gets reset after each call of fnStandardExecution().
        giCombatTimerOffset = iOffset
        
        --Friendly:
        if(iOriginatorParty == iTargetParty) then
            fnStandardExecution(iOriginatorID, i-1, zaSupportPackage, zaSupportEffectPackage)
            
        --Hostile:
        else
            fnStandardExecution(iOriginatorID, i-1, zaOffensePackage, zaOffenseEffectPackage)
        end
    end

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

end
