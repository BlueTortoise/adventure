--[ ======================================== Glory Kill ========================================= ]
--[Description]
--Normal attack, provides adrenaline if the target is KOd from it.

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Setup.
    local sJobName = "Prowler"
    local sSkillName = "GloryKill"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = "ProwlerInternal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Glory Kill")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Direct)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Active)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Mei|GloryKill")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Inflicts [1.00x [IMG0]] as Weapon Damage.\nGenerates 10%%[IMG3] as [IMG4] if target is KOd by attack.\nBase Hit Rate: 95%%\n20[IMG2] Cost. Generates 1 [IMG5].")
        AdvCombatAbility_SetProperty("Allocate Description Images", 6)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
        AdvCombatAbility_SetProperty("Description Image", 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Strike")
        AdvCombatAbility_SetProperty("Description Image", 2, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Mana")
        AdvCombatAbility_SetProperty("Description Image", 3, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Health")
        AdvCombatAbility_SetProperty("Description Image", 4, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Adrenaline")
        AdvCombatAbility_SetProperty("Description Image", 5, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/ComboPoint")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_PostAction, true) --Because we need to check if the target was KO'd.
        AdvCombatAbility_SetProperty("Script Response", gciAbility_CombatEnds, true) --Because we need to check if the target was KO'd.
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then
        fnAbilityStandardBeginAction(20, false, 0, false)
    
    else

        --Check if the ID set was tripped.
        local iTargetID     = VM_GetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N")
        local iOriginatorID = VM_GetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N")
        
        --Reset.
        VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N", 0)
        VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N", 0)
        if(iTargetID == 0) then return end
        
        --Check if the target is on the graveyard.
        if(AdvCombat_GetProperty("Party Of ID", iTargetID) == gciACPartyGroup_Graveyard) then
            
            --Award Adrenaline to originator.
            AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
                local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            DL_PopActiveObject()
            
            --Application pack.
            local iAward = math.floor(iHPMax * 0.10)
            if(iAward < 1) then iAward = 1 end
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 15, "Adrenaline|"..iAward)
            
        end
    
    end

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then
        fnAbilityStandardBeginAction(20, false, 0, false)
    end

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Enemies Single", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    --[ ========== Other Values ========== ]
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    --[ ========== Setup ========== ]
    --This routine create a package that contains all the default values needed to build a Prediction Box. We
    -- then change any values that aren't default.
    local zPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zPackage.bHasHitRateModule = true
    zPackage.zHitRateModule.iMissRate = 5
    
    --Crit Module
    zPackage.bHasCritModule = true
    zPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zPackage.bHasDamageModule = true
    zPackage.zDamageModule.bUseWeaponType = true
    zPackage.zDamageModule.fDamageFactor = 1.00
    
    --Extra strings
    
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --[ ========== For Each Target ==========]
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zPackage)
    end
    
    --[ ========== For Owner ==========]
    local iAdrenaline = math.floor(iHPMax * 0.10)
    local sBoxName = "Prd" .. 0 .. "x" .. iOwnerID
    AdvCombat_SetProperty("Prediction Create Box",  sBoxName)
    AdvCombat_SetProperty("Prediction Set Host",    sBoxName, iOwnerID)
    AdvCombat_SetProperty("Prediction Set Offsets", sBoxName, 0, -100)
    AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "On enemy KO: +" .. iAdrenaline .. "[IMG0]")
    AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Adrenaline")

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        local sAttackAnimation = AdvCombatEntity_GetProperty("Weapon Attack Animation")
        local sAttackSound     = AdvCombatEntity_GetProperty("Weapon Attack Sound")
        local sCriticalSound   = AdvCombatEntity_GetProperty("Weapon Critical Sound")
        fnModifyCP(1)
        fnModifyMP(-20)
    DL_PopActiveObject()
    
    --Switch the weapon values to use defaults if any were in error. This can occur if the equipped
    -- weapon was configured incorrectly or did not exist.
    if(sAttackAnimation == "Null") then sAttackAnimation = gsDefaultAttackAnimation end
    if(sAttackSound     == "Null") then sAttackSound     = gsDefaultAttackSound     end
    if(sCriticalSound   == "Null") then sCriticalSound   = gsDefaultCriticalSound   end
    
    --[========== Ability Package ========== ]
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Weapon")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides. We only edit parts that may not be standard.
    zaAbilityPackage.iMissThreshold = 5
    zaAbilityPackage.fDamageFactor  = 0.80
    zaAbilityPackage.sAttackAnimation = sAttackAnimation
    zaAbilityPackage.sAttackSound     = sAttackSound
    zaAbilityPackage.sCriticalSound   = sCriticalSound
    
    --[ ========== For Each Target ==========]
    --Get how many targets were painted by this ability, iterate across them. Call the standard
    -- execution subroutine.
    DL_AddPath("Root/Variables/Combat/WerecatGloryKill/")
    VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N", iOriginatorID)
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, nil)
        
        --Mark the target's ID.
        AdvCombat_SetProperty("Push Target", i-1) 
            VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N", RO_GetID())
        DL_PopActiveObject()
    end

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

    --Check if the ID set was tripped.
    local iTargetID     = VM_GetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N")
    local iOriginatorID = VM_GetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N")
    
    --Reset.
    VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N", 0)
    VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N", 0)
    if(iTargetID == 0) then return end
    
    --Check if the target is on the graveyard.
    if(AdvCombat_GetProperty("Party Of ID", iTargetID) == gciACPartyGroup_Graveyard) then
        
        --Award Adrenaline to originator.
        AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
        
            --Get stats.
            local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local iAdrenaline = AdvCombatEntity_GetProperty("Adrenaline")
            
            --Application pack.
            local iAward = math.floor(iHPMax * 0.10)
            if(iAward < 1) then iAward = 1 end
            
            --Set.
            AdvCombatEntity_SetProperty("Adrenaline", iAdrenaline + iAward)
        DL_PopActiveObject()
    end

--[ ==================================== Combat: Event Queued ==================================== ]
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then
end
