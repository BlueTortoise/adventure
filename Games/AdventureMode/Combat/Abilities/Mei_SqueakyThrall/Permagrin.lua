--[ ========================================= Permagrin ========================================= ]
--[Description]
--Enemies that have 3 stacks of [Rubberified] are turned into rubber permagrins and join the party.

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create) then
    AdvCombatEntity_SetProperty("Create Ability", "SqueakyThrall|Permagrin")
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Permagrin")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Direct)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Active)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Mei|Permagrin")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Turns an enemy with 3 stacks of [Rubberified]\ninto a squeaky permagrin ally.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 0)
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    AdvCombatAbility_SetProperty("Usable", true)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    AdvCombatAbility_SetProperty("Usable", true)

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Enemies Single", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --[ ========== For Each Target ==========]
    --Get how many targets were painted by this ability, iterate across them. Call the standard
    -- execution subroutine.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Target ID.
        local iTimer = (gciAbility_TicksOffsetPerTarget * (i-1))
        local iTargetID = AdvCombat_GetProperty("ID Of Target", i-1)
        
        --Check target stacks.
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
            local iRubberifiedStacks = AdvCombatEntity_GetProperty("Tag Count", "Rubberified")
        DL_PopActiveObject()
        
        --[Not Enough Stacks]
        if(iRubberifiedStacks < 3) then
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Play Sound|Combat\\|AttackMiss")
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Text|Failed!|Color:White")
            iTimer = iTimer + gciApplication_TextTicks
            AdvCombat_SetProperty("Event Timer", iTimer)
        
        --[Rubberified!]
        else
        
            --Store the ID of the entity.
            giTargetID = iTargetUniqueID
        
            --Change parties.
            local iTimer = (i-1) * 15
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTimer, "Change Party|" .. gciACPartyGroup_Party)
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTimer, "Position|" .. gciACPoscode_Party)
            iTimer = iTimer + gciAC_StandardMoveTicks
            
            --Dialogue.
            local sScriptPath = LM_GetCallStack(0)
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTimer, "Run Script|" .. sScriptPath .. "|N:-1000")
            iTimer = iTimer + 15
            
            --End
            AdvCombat_SetProperty("Event Timer", iTimer)
        
        end
        
    end
    
--[ ===================================== Permagrin Cutscene ==================================== ]
--Special
elseif(iSwitchType == -1000) then

    --[ ===== Error Check =====]
    --Make sure the target exists.
    if(giTargetID == nil) then return end
    
    --[ ===== AI Modifications =====]
    --Switch the AI of the target to make it player-controlled. Change its abilities.
    AdvCombat_SetProperty("Push Entity By ID", giTargetID)
    
        --Clear AI and existing abilities.
        AdvCombatEntity_SetProperty("AI Script", "Null")
        fnClearAllSlots()
        
        --Provide abilities. These are identical to Mei's.
        local sJobAbilityPath = gsRoot .. "Combat/Abilities/Mei_SqueakyThrall/"
        LM_ExecuteScript(sJobAbilityPath .. "Spread Rubber.lua", gciAbility_Create)
        LM_ExecuteScript(sJobAbilityPath .. "Permagrin.lua",     gciAbility_Create)
        
        --Place abilities.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "SqueakyThrall|SpreadRubber")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "SqueakyThrall|Permagrin")
        
    DL_PopActiveObject()
    
    --[ ===== Cutscene Handling =====]
    --Setup.
    local saActorList = {}
    local saSlots = {}
    for i = 0, 6, 1 do
        saActorList[i] = "Null"
        saSlots[i] = "Null"
    end
    
    --Scan across the player's party. Store the dialogue actors associated with the characters.
    local iCombatPartySize = AdvCombat_GetProperty("Combat Party Size")
    for i = 0, iCombatPartySize-1, 1 do
        AdvCombat_SetProperty("Push Combat Party Member By Slot", i)
            local iUniqueID = RO_GetID()
            saActorList[i] = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDialogueActor", "S")
        DL_PopActiveObject()
    end
    
    --Place party members based on how many there are. Note that the entity will already have joined the party.
    if(iCombatPartySize == 2) then
        saSlots[1] = saActorList[0]
    elseif(iCombatPartySize == 3) then
        saSlots[0] = saActorList[1]
        saSlots[2] = saActorList[0]
    elseif(iCombatPartySize == 4) then
        saSlots[0] = saActorList[2]
        saSlots[1] = saActorList[1]
        saSlots[2] = saActorList[0]
    end
    
    --Determine which cutscene we need to run.
    local sDialogueActor = VM_GetVar("Root/Variables/Combat/" .. giTargetID .. "/sDialogueActor", "S")
    
    --[Cultist F]
    if(sDialogueActor == "CultistF") then

        --Change properties:
        VM_SetVar("Root/Variables/Combat/" .. giTargetID .. "/sDialogueActor", "S", "RubberCultistF")

        --Base.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        for i = 0, 6, 1 do
            local sString = "WD_SetProperty(\"Actor In Slot\", "..i..", \"" .. saSlots[i] .. "\", \"Neutral\")"
            fnCutsceneInstruction(sString)
        end
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist:[E|Neutral] What the hell is this stuff![SOFTBLOCK] Aaaack![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *Squeak*[SOFTBLOCK] (Spread...[SOFTBLOCK] Spread...[SOFTBLOCK] Spread...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist:[E|Neutral] Get it -[SOFTBLOCK] off...[SOFTBLOCK] Spread...") ]])
        fnCutsceneBlocker()
        
        --Update the party.
        for i = 0, 6, 1 do 
            saSlots[i] = "Null"
        end
        if(iCombatPartySize == 2) then
            saSlots[0] = "RubberCultistF"
            saSlots[2] = saActorList[0]
        elseif(iCombatPartySize == 3) then
            saSlots[0] = "RubberCultistF"
            saSlots[1] = saActorList[1]
            saSlots[2] = saActorList[0]
        elseif(iCombatPartySize == 4) then
            saSlots[6] = "RubberCultistF"
            saSlots[0] = saActorList[2]
            saSlots[1] = saActorList[1]
            saSlots[2] = saActorList[0]
        end
        
        --Enemy has joined the party.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
        for i = 0, 6, 1 do
            local sString = "WD_SetProperty(\"Actor In Slot\", "..i..", \"" .. saSlots[i] .. "\", \"Neutral\")"
            fnCutsceneInstruction(sString)
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist:[E|Neutral] *Squeak*[SOFTBLOCK] (Spread...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Spread...)") ]])
        fnCutsceneBlocker()
    end
    
    --Clean up.
    giTargetID = nil

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

--[ ==================================== Combat: Event Queued ==================================== ]
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then
end
