--[ ======================================== Concentrate ======================================== ]
--[Description]
--Increases attack power by 50% for 1 turn. 3 charges.

--[Notes]
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --A second argument is needed. This is the internal name of the ability.
    if(iArgumentsTotal < 2) then return end
    local sAbilityName = LM_GetScriptArgument(1)
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sAbilityName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Pepper Pie")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Free_Direct)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Increases [IMG0] by 50%% for 1[IMG1].\nFree Action.\n3 uses per battle.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 2)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
        AdvCombatAbility_SetProperty("Description Image", 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Turns")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

--[ ========================================= Job Taken ========================================= ]
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

--[ =================================== Combat: Combat Begins =================================== ]
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N", 3.0)

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    local iUniqueID = RO_GetID()
    local iChargesLeft = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N")
    if(iChargesLeft < 1) then
        AdvCombatAbility_SetProperty("Usable", false)
    else
        fnAbilityStandardBeginAction(0, true, 1, true)
    end

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    local iUniqueID = RO_GetID()
    local iChargesLeft = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N")
    if(iChargesLeft < 1) then
        AdvCombatAbility_SetProperty("Usable", false)
    else
        fnAbilityStandardBeginFreeAction(0, true, 1, true)
    end

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

--[ =================================== Combat: Paint Targets =================================== ]
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Allies Single", AdvCombatAbility_GetProperty("Owner ID"))
    
--[ ============================== Combat: Paint Targets Response =============================== ]
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
--[ ================================ Combat: Build Prediction Box =============================== ]
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    --[ ========== Setup ========== ]
    --This routine create a package that contains all the default values needed to build a Prediction Box. We
    -- then change any values that aren't default.
    local zPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zPackage.zaEffectModules[1] = fnCreateEffectModule()
    zPackage.zaEffectModules[1].bIsBuff = true
    zPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zPackage.zaEffectModules[1].iEffectStrength = 1000
    zPackage.zaEffectModules[1].sResultScript = "+50%% [IMG1] / 1[IMG2]"
    zPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Attack", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --[ ========== Other Values ========== ]
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --[ ========== For Each Target ==========]
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zPackage)
    end

--[ ==================================== Combat: Can Execute ==================================== ]
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

--[ ================================= Combat: Execute on Targets ================================ ]
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --[========== Originator Statistics ========== ]
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Remove charges.
    local iUniqueID = RO_GetID()
    local iChargesLeft = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N", iChargesLeft - 1)
    
    --[========== Ability Package ========== ]
    --Apply cooldown for this ability. Mark the action as free.
    AdvCombatAbility_SetProperty("Cooldown", 2)
    AdvCombat_SetProperty("Set As Free Action")
    
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Buff")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides. We only edit parts that may not be standard.
    zaAbilityPackage.bDoesNoDamage = true
    zaAbilityPackage.bDoesNoStun = true
    zaAbilityPackage.bAlwaysHits = true
    zaAbilityPackage.bEffectAlwaysApplies = true
    
    --[========== Effect Package ========== ]
    --Create an effect package and populate it.
    local zaEffectPackage = fnConstructDefaultEffectPackage()
    zaEffectPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides.
    zaEffectPackage.sApplyAnimation = "Null"
    zaEffectPackage.sApplySound     = "Null"
    zaEffectPackage.sEffectPath     = gsRoot .. "Combat/Effects/X Items/Pepper Pie.lua"
    zaEffectPackage.sEffectCritPath = gsRoot .. "Combat/Effects/X Items/Pepper Pie.lua"
    zaEffectPackage.sApplyText      = "+Attack"
    zaEffectPackage.sApplyTextCol   = "Color:Blue"
    
    --[ ========== For Each Target ==========]
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, zaEffectPackage)
    end

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

end
