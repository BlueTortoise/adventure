--[ ======================================= Debuff ====================================== ]
--Debuff animation, no special properties.
AdvCombat_SetProperty("Create Animation", "Debuff")

    AdvCombatAnimation_SetProperty("Offsets", -192, -192)
    AdvCombatAnimation_SetProperty("Ticks Per Frame", 1.0)
    AdvCombatAnimation_SetProperty("Allocate Frames", 24)
    for i = 0, 24-1, 1 do
		local sIndex = string.format("%02i", i)
        AdvCombatAnimation_SetProperty("Set Frame", i, "Root/Images/CombatAnimations/Debuff/" .. sIndex)
    end

DL_PopActiveObject()

--Add an entry to the timing list.
giAnimationTimingsTotal = giAnimationTimingsTotal + 1
gzaAnimationTimings[giAnimationTimingsTotal] = {}
gzaAnimationTimings[giAnimationTimingsTotal].sName = "Debuff"
gzaAnimationTimings[giAnimationTimingsTotal].iTicks = 19