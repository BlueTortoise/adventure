--[ ======================================= Strike ====================================== ]
--Strike animation, no special properties.
AdvCombat_SetProperty("Create Animation", "Strike")

    AdvCombatAnimation_SetProperty("Offsets", -192, -192)
    AdvCombatAnimation_SetProperty("Ticks Per Frame", 1.0)
    AdvCombatAnimation_SetProperty("Allocate Frames", 10)
    for i = 0, 10-1, 1 do
		local sIndex = string.format("%02i", i)
        AdvCombatAnimation_SetProperty("Set Frame", i, "Root/Images/CombatAnimations/Strike/" .. sIndex)
    end

DL_PopActiveObject()

--Add an entry to the timing list.
giAnimationTimingsTotal = giAnimationTimingsTotal + 1
gzaAnimationTimings[giAnimationTimingsTotal] = {}
gzaAnimationTimings[giAnimationTimingsTotal].sName = "Strike"
gzaAnimationTimings[giAnimationTimingsTotal].iTicks = 10