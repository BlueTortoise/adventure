--[Animation Routing]
--Called at Adventure startup after graphics are loaded. Creates AdvCombatAnimations for all standard animations. Special animations
-- need to be created when loaded.
if(gbHasCreatedStandardAnimations ~= nil) then return end

--Path
local sBasePath = fnResolvePath()

--Reset these globals.
giAnimationTimingsTotal = 0
gzaAnimationTimings = {}

--Execute
LM_ExecuteScript(sBasePath .. "Bleed.lua")
LM_ExecuteScript(sBasePath .. "Blind.lua")
LM_ExecuteScript(sBasePath .. "Buff.lua")
LM_ExecuteScript(sBasePath .. "Claw Slash.lua")
LM_ExecuteScript(sBasePath .. "Corrode.lua")
LM_ExecuteScript(sBasePath .. "Debuff.lua")
LM_ExecuteScript(sBasePath .. "Flames.lua")
LM_ExecuteScript(sBasePath .. "Freeze.lua")
LM_ExecuteScript(sBasePath .. "Gun Shot.lua")
LM_ExecuteScript(sBasePath .. "Haste.lua")
LM_ExecuteScript(sBasePath .. "Healing.lua")
LM_ExecuteScript(sBasePath .. "Laser Shot.lua")
LM_ExecuteScript(sBasePath .. "Light.lua")
LM_ExecuteScript(sBasePath .. "Pierce.lua")
LM_ExecuteScript(sBasePath .. "Poison.lua")
LM_ExecuteScript(sBasePath .. "ShadowA.lua")
LM_ExecuteScript(sBasePath .. "ShadowB.lua")
LM_ExecuteScript(sBasePath .. "ShadowC.lua")
LM_ExecuteScript(sBasePath .. "Shock.lua")
LM_ExecuteScript(sBasePath .. "Slow.lua")
LM_ExecuteScript(sBasePath .. "Strike.lua")
LM_ExecuteScript(sBasePath .. "Sword Slash.lua")
LM_ExecuteScript(sBasePath .. "Terrify.lua")
    