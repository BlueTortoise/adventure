--[ ====================================== Dialogue Resolve ===================================== ]
--Called whenever dialogue can occur after the party wins a battle. If it does not activate dialogue,
-- the victory screen proceeds as normal.
io.write("Called post-battle dialogue.\n")
