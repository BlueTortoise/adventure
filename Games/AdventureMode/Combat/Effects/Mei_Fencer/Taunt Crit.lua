--[ =========================================== Taunt =========================================== ]
--[Description]
--Applied by Mei's Fencer ability Taunt. Reduces accuracy by 20 for 5 turns.

--[Notes]
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the Effect is initialized. This may be at combat start, or it may be when the matching
-- ability is used. This is *not* called for a passive effect for the Status UI!
--Local variables, if any, should be set up here. The Active Object is the newly created AdvCombatEffect.
if(iSwitchType == gciEffect_Create) then
    AdvCombatEffect_SetProperty("Script", LM_GetCallStack(0))
    
    --Get the unique ID. Create a DataLibrary path to store variables for this effect. These are
    -- cleared when combat ends.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    
    --Duration.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N", 8.0)
    
    --Images.
    AdvCombatEffect_SetProperty("Back Image",  gsAbility_Backing_Debuff)
    AdvCombatEffect_SetProperty("Frame Image", gsAbility_Frame_Active)
    AdvCombatEffect_SetProperty("Front Image", "Root/Images/AdventureUI/Abilities/Mei|Taunt")
    
--[ ======================================== Apply Stats ======================================== ]
--Called when the given Effect needs to apply its stats to a CombatStatistics package. This may be
-- when it is created, when the status UI needs it, or any number of other locations. If the flag is
-- gciEffect_UnApplyStats, then the effects are being removed because the effect expired.
--The AdvCombatEffect is the Active Effect. The second argument passed in will be the ID of the target.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    --Get ID of target.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    if(iTargetID < 1) then return end

    --Switch multiplier to apply-unapply stat changes smoothly.
    local fMultiplier = 1.0
    if(iSwitchType == gciEffect_UnApplyStats) then fMultiplier = -1.0 end

    --Push. Apply stats.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        local iAccBonus = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_TempEffect, gciStatIndex_Accuracy)
        iAccBonus = iAccBonus + (-20 * fMultiplier)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_TempEffect, gciStatIndex_Accuracy, iAccBonus)
    DL_PopActiveObject()

    --Duration.
    local iUniqueID = RO_GetID()
    local iDuration = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N")

    --Place a notifier in the entity's HP display. This only applies to enemies.
    AdvCombatEffect_SetProperty("Short Text", iTargetID, "[IMG0] (-20 [IMG1]) (" .. iDuration .. "[IMG2])")
    AdvCombatEffect_SetProperty("Allocate Short Text Images", iTargetID, 3)
    AdvCombatEffect_SetProperty("Short Text Image", iTargetID, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Blind")
    AdvCombatEffect_SetProperty("Short Text Image", iTargetID, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Accuracy")
    AdvCombatEffect_SetProperty("Short Text Image", iTargetID, 2, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iTargetID)

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved. This is called after Abilities have
-- run their updates. If an Effect is created by an ability during its update, it does *not* run its
-- turn-begin code until the next turn.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this Effect begins their turn.
elseif(iSwitchType == gciAbility_BeginAction) then

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then
    
    --Get the ID of the acting entity.
    AdvCombat_SetProperty("Push Acting Entity")
        local iActingEntityID = RO_GetID()
    DL_PopActiveObject()
    
    --If the ID is not on the target list, we don't care.
    if(AdvCombatEffect_GetProperty("Is ID on Target List", iActingEntityID) == false) then
        return
    end
    
    --Decrement the duration counter.
    local iUniqueID = RO_GetID()
    local iDuration = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N")
    iDuration = iDuration - 1
    
    --Duration zeroes out:
    if(iDuration < 1) then
        AdvCombatEffect_SetProperty("Flag Remove Now")
    
    --Otherwise, store the decrement.
    else
        --Store.
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N", iDuration)

        --Change notifier. Images do not need to be changed, just the text.
        AdvCombatEffect_SetProperty("Short Text", iActingEntityID, "[IMG0] (-20 [IMG1]) (" .. iDuration .. "[IMG2])")
        AdvCombatEffect_SetProperty("Crossload Short Text Images", iActingEntityID)
    end

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end
