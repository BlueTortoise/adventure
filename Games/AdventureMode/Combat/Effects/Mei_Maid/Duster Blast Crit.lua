--[ ======================================= Duster Blast ======================================== ]
--[Description]
--Reduces accuracy by 35 for 3 turns, [Blind]. Critical adds 2 to duration.

--[Notes]
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ====================================== Local Variables ====================================== ]
--[System]
--This effect uses a standardized StatMod script. These are its common variables.
gzaStatModStruct.sDisplayName = "Duster Blast"
gzaStatModStruct.iDuration    = 5
gzaStatModStruct.iBacking     = gsAbility_Backing_Debuff
gzaStatModStruct.iFrame       = gsAbility_Frame_Active
gzaStatModStruct.sFrontImg    = "Root/Images/AdventureUI/Abilities/Mei|DusterBlast"

--[Stat Effects]
--This string stores the actual stat modifiers.
gzaStatModStruct.sStatString = "Acc|Flat|30"

--[Strings for Combat Inspector]
--The first two strings are the header left and right side.
gzaStatModStruct.saStrings = {}
gzaStatModStruct.saStrings[1] = "[Debuff]Duster Blast (Critical)"
gzaStatModStruct.saStrings[2] = "[DnN][Acc] /[Dur][Turns]"

--These are description strings.
gzaStatModStruct.saStrings[3] = "Unleash the power of the feather duster!"
gzaStatModStruct.saStrings[4] = "Decreases accuracy by 35 for 5 turns."
gzaStatModStruct.saStrings[5] = "[Debuff] [VARN:Acc][Acc] ([Dur][Turns])"
gzaStatModStruct.saStrings[6] = nil

--[Short Text]
--Appears above the enemy portrait in combat. Not used for friendlies, but allies can switch sides.
gzaStatModStruct.sShortText = "[Debuff] [VARN:Acc][Acc] ([Dur][Turns])"

--[ ========================================== Creation ========================================= ]
--Called when the Effect is initialized. This may be at combat start, or it may be when the matching
-- ability is used. This is *not* called for a passive effect for the Status UI!
--Local variables, if any, should be set up here. The Active Object is the newly created AdvCombatEffect.
if(iSwitchType == gciEffect_Create) then
    
    --Store severity, pass to subscript.
    local fSeverity = 1.0
    if(iArgumentsTotal >= 2) then fSeverity = LM_GetScriptArgument(1, "N") end
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)
    
--[ ======================================== Apply Stats ======================================== ]
--Called when the given Effect needs to apply its stats to a CombatStatistics package. This may be
-- when it is created, when the status UI needs it, or any number of other locations. If the flag is
-- gciEffect_UnApplyStats, then the effects are being removed because the effect expired.
--The AdvCombatEffect is the Active Effect. The second argument passed in will be the ID of the target.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved. This is called after Abilities have
-- run their updates. If an Effect is created by an ability during its update, it does *not* run its
-- turn-begin code until the next turn.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this Effect begins their turn.
elseif(iSwitchType == gciAbility_BeginAction) then

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end
