--[ ========================================== Undying ========================================== ]
--[Description]
--Increases all resistances by 3 when HP is under 50%.

--[Notes]
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ====================================== Local Variables ====================================== ]
--[System]
--This effect uses a standardized StatMod script. These are its common variables.
gzaStatModStruct.sDisplayName = "Undying"
gzaStatModStruct.iDuration    = -1
gzaStatModStruct.iBacking     = gsAbility_Backing_Buff
gzaStatModStruct.iFrame       = gsAbility_Frame_Passive
gzaStatModStruct.sFrontImg    = "Root/Images/AdventureUI/Abilities/Mei|Undying"

--[Stat Effects]
--This string stores the actual stat modifiers.
gzaStatModStruct.sStatString = ""

--[Strings for Combat Inspector]
--The first two strings are the header left and right side.
gzaStatModStruct.saStrings = {}
gzaStatModStruct.saStrings[1] = "[Buff]Undying"
gzaStatModStruct.saStrings[2] = "[UpN][All Resistances]"

--These are description strings.
gzaStatModStruct.saStrings[3] = "Death is nothing to those who embrace it."
gzaStatModStruct.saStrings[4] = "+3 all resistances when under 50%% health."
gzaStatModStruct.saStrings[5] = "[Buff] +3[All Resistances]"
gzaStatModStruct.saStrings[6] = nil

--[Short Text]
--Appears above the enemy portrait in combat. Not used for friendlies, but allies can switch sides.
gzaStatModStruct.sShortText = "" --Passives do not have short text.

--[ ========================================== Creation ========================================= ]
--Called when the Effect is initialized. This may be at combat start, or it may be when the matching
-- ability is used. This is *not* called for a passive effect for the Status UI!
--Local variables, if any, should be set up here. The Active Object is the newly created AdvCombatEffect.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
    --Track whether or not the effect is currently applying.
    local iUniqueID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N", 0.0)
    AdvCombatEffect_SetProperty("Set Hidden On UI", true)
    
--[ ======================================== Apply Stats ======================================== ]
--Called when the given Effect needs to apply its stats to a CombatStatistics package. This may be
-- when it is created, when the status UI needs it, or any number of other locations. If the flag is
-- gciEffect_UnApplyStats, then the effects are being removed because the effect expired.
--The AdvCombatEffect is the Active Effect. The second argument passed in will be the ID of the target.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    --[Setup]
    --Get ID of this Effect.
    local iUniqueID = RO_GetID()
    
    --Get ID of target.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    if(iTargetID < 1) then return end
    
    --[Display]
    --Run subroutines to upload the description strings.
    AdvCombatEffect_SetProperty("Allocate Description Strings", iTargetID, #gzaStatModStruct.saStrings)
    LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetInspectorTitle, gzaStatModStruct.saStrings[1], gzaStatModStruct.saStrings[2], iTargetID)
    for i = 3, #gzaStatModStruct.saStrings, 1 do
        LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetDescription, iTargetID, i-1, gzaStatModStruct.saStrings[i])
    end

    --[Control]
    --Apply stats never actually fires. This section only is needed for removing the effect when combat ends.
    if(iSwitchType == gciEffect_ApplyStats) then return end

    --If the effect hasn't applied yet, do nothing. Can't remove what wasn't applied.
    local iIsApplied = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N")
    if(iIsApplied == 0.0) then return end

    --Push, remove stats.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        for i = gciStatIndex_Resist_Start, gciStatIndex_Resist_End, 1 do
            fnApplyStatBonus(i, -3)
        end
    DL_PopActiveObject()

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved. This is called after Abilities have
-- run their updates. If an Effect is created by an ability during its update, it does *not* run its
-- turn-begin code until the next turn.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this Effect begins their turn.
elseif(iSwitchType == gciAbility_BeginAction) then

    --Variables.
    local bIsHidden = true
    local iUniqueID = RO_GetID()
    local iIsApplied = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N")
    
    --Get the target's ID.
    local iTargetID = AdvCombatEffect_GetProperty("Get ID Of Target", 0)
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)

        --Get HP.
        local iHP    = AdvCombatEntity_GetProperty("Health")
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)

        --Over 50%.
        if(iHP >= iHPMax * 0.50) then
            bIsHidden = true

            --If the ability is not applied, do nothing:
            if(iIsApplied == 0.0) then
            
            --Remove it.
            else
                
                --Set flag.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N", 0)
                
                --Remove resistance.
                for i = gciStatIndex_Resist_Start, gciStatIndex_Resist_End, 1 do
                    fnApplyStatBonus(i, -3)
                end
            end

        --Under 50%.
        else
            bIsHidden = false

            --If the ability is not applied, apply it:
            if(iIsApplied == 0.0) then
                
                --Set flag.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N", 1)
                
                --Apply resistance.
                for i = gciStatIndex_Resist_Start, gciStatIndex_Resist_End, 1 do
                    fnApplyStatBonus(i, 3)
                end
            
            --Do nothing.
            else
                
            end
        end
    DL_PopActiveObject()
    
    --Modify visibility.
    AdvCombatEffect_SetProperty("Set Hidden On UI", bIsHidden)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end
