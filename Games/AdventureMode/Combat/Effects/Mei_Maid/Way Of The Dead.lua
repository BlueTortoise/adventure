--[ ===================================== Way of the Dead ====================================== ]
--[Description]
--Increases Slash, Pierce, and Strike resist by 3. Passive.

--[Notes]
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ====================================== Local Variables ====================================== ]
--[System]
--This effect uses a standardized StatMod script. These are its common variables.
gzaStatModStruct.sDisplayName = "Way of the Dead"
gzaStatModStruct.iDuration    = -1
gzaStatModStruct.iBacking     = gsAbility_Backing_Buff
gzaStatModStruct.iFrame       = gsAbility_Frame_Passive
gzaStatModStruct.sFrontImg    = "Root/Images/AdventureUI/Abilities/Mei|WayOfTheDead"

--[Stat Effects]
--This string stores the actual stat modifiers.
gzaStatModStruct.sStatString = "ResSls|Flat|3 ResStk|Flat|3 ResPrc|Flat|3"

--[Strings for Combat Inspector]
--The first two strings are the header left and right side.
gzaStatModStruct.saStrings = {}
gzaStatModStruct.saStrings[1] = "[Buff]Way of the Dead"
gzaStatModStruct.saStrings[2] = "[UpN][Slsh] [UpN][Prc] [UpN][Stk]"

--These are description strings.
gzaStatModStruct.saStrings[3] = "Your dissociated ghostly body is resistant to physical"
gzaStatModStruct.saStrings[4] = "attacks. +3 Strike, Slash, and Pierce resist. Passive."
gzaStatModStruct.saStrings[5] = "[Buff] +3[Slsh] +3[Prc] +3[Stk]"
gzaStatModStruct.saStrings[6] = nil

--[Short Text]
--Appears above the enemy portrait in combat. Not used for friendlies, but allies can switch sides.
gzaStatModStruct.sShortText = ""

--[ ========================================== Creation ========================================= ]
--Called when the Effect is initialized. This may be at combat start, or it may be when the matching
-- ability is used. This is *not* called for a passive effect for the Status UI!
--Local variables, if any, should be set up here. The Active Object is the newly created AdvCombatEffect.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
--[ ======================================== Apply Stats ======================================== ]
--Called when the given Effect needs to apply its stats to a CombatStatistics package. This may be
-- when it is created, when the status UI needs it, or any number of other locations. If the flag is
-- gciEffect_UnApplyStats, then the effects are being removed because the effect expired.
--The AdvCombatEffect is the Active Effect. The second argument passed in will be the ID of the target.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved. This is called after Abilities have
-- run their updates. If an Effect is created by an ability during its update, it does *not* run its
-- turn-begin code until the next turn.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this Effect begins their turn.
elseif(iSwitchType == gciAbility_BeginAction) then

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end
