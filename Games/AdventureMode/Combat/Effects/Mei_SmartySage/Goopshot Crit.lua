--[ ============================================ Rend =========================================== ]
--[Description]
--Deals bleeding damage equal to 2.00 of user's attack power over 5 turns.

--[Notes]
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Called when the Effect is initialized. This may be at combat start, or it may be when the matching
-- ability is used. This is *not* called for a passive effect for the Status UI!
--Local variables, if any, should be set up here. The Active Object is the newly created AdvCombatEffect.
if(iSwitchType == gciEffect_Create) then
    AdvCombatEffect_SetProperty("Script", LM_GetCallStack(0))
    
    --Get the unique ID. Create a DataLibrary path to store variables for this effect. These are
    -- cleared when combat ends.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    
    --Duration.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N", 5.0)
    
    --Images.
    AdvCombatEffect_SetProperty("Back Image",  gsAbility_Backing_DoT)
    AdvCombatEffect_SetProperty("Frame Image", gsAbility_Frame_Active)
    AdvCombatEffect_SetProperty("Front Image", "Root/Images/AdventureUI/Abilities/Mei|PollenRend")
    
    --Get the attack power of the originator at time of application.
    AdvCombatEffect_SetProperty("Push Originator")
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "N", RO_GetID())
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "N", iAttackPower)
    DL_PopActiveObject()
    
--[ ======================================== Apply Stats ======================================== ]
--Called when the given Effect needs to apply its stats to a CombatStatistics package. This may be
-- when it is created, when the status UI needs it, or any number of other locations. If the flag is
-- gciEffect_UnApplyStats, then the effects are being removed because the effect expired.
--The AdvCombatEffect is the Active Effect. The second argument passed in will be the ID of the target.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    --Ability does not care about un-apply stats.
    if(iSwitchType == gciEffect_UnApplyStats) then return end
    
    --Get ID of target.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    if(iTargetID < 1) then return end

    --Push. Get stats.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        local iPoisonResist = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Poison)
    DL_PopActiveObject()

    --Get variables.
    local iUniqueID = RO_GetID()
    local iDuration              = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N")
    local iOriginatorAttackPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "N")
    
    --Compute the damage-per-turn. We deal 0.40 for 5 turns, 2.00 total.
    local iDamagePerTurn = fnComputeCombatDamage(iOriginatorAttackPower * 0.40, 0, iPoisonResist)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDamagePerTurn", "N", iDamagePerTurn)

    --Place a notifier in the entity's HP display. This only applies to enemies.
    AdvCombatEffect_SetProperty("Short Text", iTargetID, iDamagePerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Short Text Images", iTargetID, 2)
    AdvCombatEffect_SetProperty("Short Text Image", iTargetID, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/Poisoning")
    AdvCombatEffect_SetProperty("Short Text Image", iTargetID, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iTargetID)

--[ ==================================== Combat: Turn Begins ==================================== ]
--Called when the turn begins after turn order is resolved. This is called after Abilities have
-- run their updates. If an Effect is created by an ability during its update, it does *not* run its
-- turn-begin code until the next turn.
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--Called when the character that possesses this Effect begins their turn.
elseif(iSwitchType == gciAbility_BeginAction) then

    --[Duration Decrement]
    --Get the ID of the acting entity.
    AdvCombat_SetProperty("Push Acting Entity")
        local iActingEntityID = RO_GetID()
    DL_PopActiveObject()
    
    --If the ID is not on the target list, we don't care.
    if(AdvCombatEffect_GetProperty("Is ID on Target List", iActingEntityID) == false) then
        return
    end
    
    --Decrement the duration counter.
    local iUniqueID = RO_GetID()
    local iDuration = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N")
    iDuration = iDuration - 1
    
    --[Ending Case]
    --Duration zeroes out:
    if(iDuration < 1) then
        AdvCombatEffect_SetProperty("Flag Remove Now")
    
    --Otherwise, store the decrement.
    else
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N", iDuration)
    end

    --[Apply Damage]
    --Get variables.
    local iOriginatorID          = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "N")
    local iOriginatorAttackPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "N")
    local iDamagePerTurn         = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDamagePerTurn", "N")

    --Place a notifier in the entity's HP display. This only applies to enemies.
    AdvCombatEffect_SetProperty("Short Text", iActingEntityID, iDamagePerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Short Text Images", iActingEntityID, 2)
    AdvCombatEffect_SetProperty("Short Text Image", iActingEntityID, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/Poisoning")
    AdvCombatEffect_SetProperty("Short Text Image", iActingEntityID, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iActingEntityID)
    
    --[Event Creation]
    --Default package.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Poison")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --[Application Sequence]
    --Setup.
    local iTimer = 0
    
    --DoTs bypass shields by default. Add this as an extra parameter to the damage function.
    local sDamageParams = "PrioritySh:0"
    
    --Ability strikes, displays the hit, applies/resist effect, finishes up.
    iTimer = zaAbilityPackage.fnAbilityAnimate(iActingEntityID, iTimer, zaAbilityPackage, false)
    iTimer = zaAbilityPackage.fnDamageAnimate(iOriginatorID, iActingEntityID, iTimer, iDamagePerTurn, sDamageParams)
    fnDefaultEndOfApplications(iTimer)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end
