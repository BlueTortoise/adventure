--[ ================================= Standard Damage Over Time ================================= ]
--[Description]
--Standard DoT script. Just call with appropriate arguments!

--[Notes]
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--LM_ExecuteScript(gsStandardDoTPath, gciEffect_Create, sDisplayName, iUniqueID, iDuration, sAbilityIcon, sTagA, iTagACnt, ...)
if(iSwitchType == gciEffect_Create and iArgumentsTotal >= 5) then
    
    --Arguments.
    local sDisplayName = LM_GetScriptArgument(1)
    local iUniqueID    = LM_GetScriptArgument(2, "N")
    local iDuration    = LM_GetScriptArgument(3, "N")
    local sAbilityIcon = LM_GetScriptArgument(4)
    
    --Create a DataLibrary entry for this, specify the duration.
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/sDisplayName", "S", sDisplayName)
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iDuration", "N", iDuration)
    
    --Display Name
    AdvCombatEffect_SetProperty("Display Name", sDisplayName)
    
    --Images.
    AdvCombatEffect_SetProperty("Back Image",  gsAbility_Backing_DoT)
    AdvCombatEffect_SetProperty("Frame Image", gsAbility_Frame_Active)
    AdvCombatEffect_SetProperty("Front Image", "Root/Images/AdventureUI/Abilities/" .. sAbilityIcon)
    
    --Tags.
    for i = 4, iArgumentsTotal, 2 do
        AdvCombatEffect_SetProperty("Add Tag", LM_GetScriptArgument(i+0), LM_GetScriptArgument(i+1, "N"))
    end
    
    --Get the attack power of the originator at time of application.
    AdvCombatEffect_SetProperty("Push Originator")
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "N", RO_GetID())
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "N", iAttackPower)
    DL_PopActiveObject()
    
--[ ======================================== Apply Stats ======================================== ]
--LM_ExecuteScript(gsStandardDoTPath, gciEffect_ApplyStats or gciEffect_UnApplyStats, iTargetID, iResistIndex, fDamageFactor, sTypeIndicator)
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 5) then

    --Arguments.
    local iTargetID      = LM_GetScriptArgument(1, "N")
    local iResistIndex   = LM_GetScriptArgument(2, "N")
    local fDamageFactor  = LM_GetScriptArgument(3, "N")
    local sTypeIndicator = LM_GetScriptArgument(4)
    
    --Target ID out of range.
    if(iTargetID < 1) then return end
    
    --Tag Structure
    local zaTags = fnCreateTagStruct()

    --Push. Get stats.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        local iResistance = AdvCombatEntity_GetProperty("Statistic", iResistIndex)
        zaTags.iBleedDamageTakenUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken +")
        zaTags.iBleedDamageTakenDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken -")
    DL_PopActiveObject()

    --Get variables.
    local iUniqueID = RO_GetID()
    local iOriginatorID          = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "N")
    local iDuration              = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N")
    local iOriginatorAttackPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "N")
    
    --Tags from originator.
    AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
        zaTags.iBleedDamageDealtUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt +")
        zaTags.iBleedDamageDealtDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt -")
    DL_PopActiveObject()
    
    --Compute the damage-per-turn. The damage factor is dealt each turn.
    local iDamagePerTurn = fnComputeCombatDamage(iOriginatorAttackPower * fDamageFactor, 0, iResistance)
    
    --Damage bonus by type.
    if(iResistIndex == gciStatIndex_Resist_Bleed) then
        local fBonusPct = 0.01 * (zaTags.iBleedDamageTakenUpTags + zaTags.iBleedDamageDealtUpTags - zaTags.iBleedDamageTakenDnTags - zaTags.iBleedDamageDealtDnTags)
        iDamagePerTurn = math.floor(iDamagePerTurn * (1.0 + fBonusPct))
    end
    
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDamagePerTurn", "N", iDamagePerTurn)

    -- |[HP Notifier]|
    --Place a notifier in the entity's HP display. This only applies to enemies.
    AdvCombatEffect_SetProperty("Short Text", iTargetID, iDamagePerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Short Text Images",  iTargetID, 2)
    AdvCombatEffect_SetProperty("Short Text Image",            iTargetID, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/" .. sTypeIndicator)
    AdvCombatEffect_SetProperty("Short Text Image",            iTargetID, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iTargetID)

    -- |[Inspector Notifier Lft]|
    --Allocate.
    AdvCombatEffect_SetProperty("Allocate Description Strings", iTargetID, 3)
    
    --Set the description text and populate any remaps.
    local sDisplayName = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDisplayName", "S")
    AdvCombatEffect_SetProperty("Description Text",             iTargetID, 0, "[IMG0]" .. sDisplayName)
    AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 0, 1)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Debuff")
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 0)
    io.write("Display name was " .. sDisplayName .. "\n")
    
    -- |[Inspector Notifier Rgt]|
    --Set the description text and populate any remaps.
    AdvCombatEffect_SetProperty("Description Text",             iTargetID, 1, iDamagePerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 1, 2)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 1, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/" .. sTypeIndicator)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 1, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 1)
    
    -- |[Description]|
    AdvCombatEffect_SetProperty("Description Text",             iTargetID, 2, "Damage-over-time. Deals " .. iDamagePerTurn .. "/turn as [IMG0], " .. iDuration .. "[IMG1].")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 2, 2)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 2, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/" .. sTypeIndicator)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 2, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 2)

--[ ==================================== Combat: Turn Begins ==================================== ]
elseif(iSwitchType == gciAbility_BeginTurn) then

--[ ============================== Combat: Character Action Begins ============================== ]
--LM_ExecuteScript(gsStandardDoTPath, gciAbility_BeginAction, sTypeIndicator, sAnimation)
elseif(iSwitchType == gciAbility_BeginAction and iArgumentsTotal >= 3) then

    --Arguments.
    local sTypeIndicator = LM_GetScriptArgument(1)
    local sAnimation     = LM_GetScriptArgument(2)

    --[Duration Decrement]
    --Get the ID of the acting entity.
    AdvCombat_SetProperty("Push Acting Entity")
        local iActingEntityID = RO_GetID()
    DL_PopActiveObject()
    
    --If the ID is not on the target list, we don't care.
    if(AdvCombatEffect_GetProperty("Is ID on Target List", iActingEntityID) == false) then
        return
    end
    
    --Decrement the duration counter.
    local iUniqueID = RO_GetID()
    local iDuration = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N")
    iDuration = iDuration - 1
    
    --[Ending Case]
    --Duration zeroes out:
    if(iDuration < 1) then
        AdvCombatEffect_SetProperty("Flag Remove Now")
    
    --Otherwise, store the decrement.
    else
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N", iDuration)
    end

    --[Apply Damage]
    --Get variables.
    local iOriginatorID          = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "N")
    local iOriginatorAttackPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "N")
    local iDamagePerTurn         = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDamagePerTurn", "N")

    --[HP Bar Notifier]
    --Place a notifier in the entity's HP display. This only applies to enemies.
    AdvCombatEffect_SetProperty("Short Text", iActingEntityID, iDamagePerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Short Text Images", iActingEntityID, 2)
    AdvCombatEffect_SetProperty("Short Text Image", iActingEntityID, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/" .. sTypeIndicator)
    AdvCombatEffect_SetProperty("Short Text Image", iActingEntityID, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iActingEntityID)
    
    --[Inspector Notifier Rgt]
    --Set the description text and populate any remaps.
    AdvCombatEffect_SetProperty("Description Text",             iActingEntityID, 1, iDamagePerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iActingEntityID, 1, 2)
    AdvCombatEffect_SetProperty("Description Image",            iActingEntityID, 1, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/" .. sTypeIndicator)
    AdvCombatEffect_SetProperty("Description Image",            iActingEntityID, 1, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Description Images", iActingEntityID, 1)
    
    --[Event Creation]
    --Default package.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage(sAnimation)
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --[Application Sequence]
    --Setup.
    local iTimer = 0
    
    --DoTs bypass shields by default. Add this as an extra parameter to the damage function.
    local sDamageParams = "PrioritySh:0"
    
    --Ability strikes, displays the hit, applies/resist effect, finishes up.
    iTimer = zaAbilityPackage.fnAbilityAnimate(iActingEntityID, iTimer, zaAbilityPackage, false)
    iTimer = zaAbilityPackage.fnDamageAnimate(iOriginatorID, iActingEntityID, iTimer, iDamagePerTurn, sDamageParams)
    fnDefaultEndOfApplications(iTimer)

--[ =============================== Combat: Character Free Action =============================== ]
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

--[ =============================== Combat: Character Action Ends =============================== ]
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end
