--[ ========================================== StatMod ========================================== ]
--[Description]
--A standardized handler for an Effect that modifies stats up or down. This can be called in place
-- of the ability's base script and will handle the execution.
--The structure gzaStatModStruct stores stats specified by the calling script and performs logic
-- based on them. You can also unpack these scripts and modify them in the caller script to perform
-- more complicated logic.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ========================================== Creation ========================================= ]
--Create will create the effect. Populate with the needed variables before calling.
--LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, (fSeverity))
if(iSwitchType == gciEffect_Create) then
    
    --System
    AdvCombatEffect_SetProperty("Display Name", gzaStatModStruct.sDisplayName)
    AdvCombatEffect_SetProperty("Script", LM_GetCallStack(1))
    
    --Get the unique ID. Create a DataLibrary path to store variables for this effect. These are
    -- cleared when combat ends.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    
    --Common variables.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N", gzaStatModStruct.iDuration)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/fSeverity", "N", 1.0)
    
    --Images.
    AdvCombatEffect_SetProperty("Back Image",  gzaStatModStruct.iBacking)
    AdvCombatEffect_SetProperty("Frame Image", gzaStatModStruct.iFrame)
    AdvCombatEffect_SetProperty("Front Image", gzaStatModStruct.sFrontImg)
    
    --Severity. Optional argument.
    if(iArgumentsTotal >= 2) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/fSeverity", "N", LM_GetScriptArgument(1, "N"))
    end
    
--[ ==================================== Apply/Unapply Stats ==================================== ]
--Called in lieu of the standard apply/unapply routine. Effectively calls the apply/unapply routines
-- below and then calls the description routines below.
--LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    --[Variables]
    --ID of Effect.
    local iUniqueID = RO_GetID()

    --Get ID of target.
    local iTargetID = LM_GetScriptArgument(1, "N")
    if(iTargetID < 1) then return end
    
    --[Stat Application]
    --Apply/Unapply stats. Stop here if un-apply as a description is not needed when removing effects.
    if(sStatString ~= "") then
        LM_ExecuteScript(gsStandardStatPath, iSwitchType+gciEffect_StatMod_ConvertToSubroutine, iTargetID, gzaStatModStruct.sStatString)
    end
    if(iSwitchType == gciEffect_UnApplyStats) then return end
    
    --[Short Text]
    --Place a notifier in the entity's HP display. This only applies to enemies.
    LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetShortText, gzaStatModStruct.sShortText, iTargetID)
    
    --[Upload]
    --Allocate how many strings are needed.
    AdvCombatEffect_SetProperty("Allocate Description Strings", iTargetID, #gzaStatModStruct.saStrings)
    
    --Run the subroutine for the headers.
    LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetInspectorTitle, gzaStatModStruct.saStrings[1], gzaStatModStruct.saStrings[2], iTargetID)
    
    --Run the subroutine for the description text. There are a variable set of strings, so iterate.
    for i = 3, #gzaStatModStruct.saStrings, 1 do
        LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetDescription, iTargetID, i-1, gzaStatModStruct.saStrings[i])
    end

--[ ============================== Apply/Unapply Stats Subroutine =============================== ]
--Stat application or removal uses a string to indicate what stats are being modified. Format:
-- "Stat|Flat|Modval", "Stat|Pct|ModPct"
--They are split by a " " to allow multiple stats in a single line.
--Ex: "Stat|Flat|Modval Stat|Pct|ModPct"
--Calling pattern:
--LM_ExecuteScript(gsStandardStatPath, gciEffect_ApplyStats, piTargetID, "Stat|Flat|Modval:Stat2|Pct|Modval")
elseif((iSwitchType == gciEffect_StatMod_ApplyStats or iSwitchType == gciEffect_StatMod_UnapplyStats) and iArgumentsTotal >= 3) then

    --[Variables]
    --ID of Effect.
    local iUniqueID = RO_GetID()

    --Get ID of target.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    if(iTargetID < 1) then return end
    
    --Severity value.
    local fSeverity = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/fSeverity", "N")

    --Switch multiplier to apply-unapply stat changes smoothly.
    local fMultiplier = 1.0
    if(iSwitchType == gciEffect_StatMod_UnapplyStats) then fMultiplier = -1.0 end
    
    --[Subdivide the Stat String]
    --Break it into parts via the ":" divisor.
    local sStatString = LM_GetScriptArgument(2)
    local saStatStrings = fnSubdivide(sStatString, " ")
    
    --[Stat Application]
    --Push. Apply/Unapply stats.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
    
        --For Each Stat:
        for i = 1, #saStatStrings, 1 do
        
            --Get the string.
            local sBaseString = saStatStrings[i]
            
            --Subdivide it. We expect 3 parts.
            local saSubstrings = fnSubdivide(sBaseString, "|")
            
            --The third part is an amount. It's a floating-point.
            local fAmount = tonumber(saSubstrings[3])
        
            --Iterate across the modifiers.
            for p = 1, giModTableTotal, 1 do
        
                --Match?
                if(saSubstrings[1] == gzaModTable[p][1]) then
        
                    --[Apply]
                    --To apply the stat, we compute and store the bonus. Some modifiers need no computation.
                    if(iSwitchType == gciEffect_StatMod_ApplyStats) then
                        
                        --Storage value.
                        local iBonus = 0
                        
                        --Flat value, no computation.
                        if(saSubstrings[2] == "Flat") then
                            iBonus = math.floor(fAmount)
        
                        --Flat modifier, with a severity value added on.
                        elseif(saSubstrings[2] == "FlatSev") then
                            iBonus = math.floor(fAmount * fSeverity)
                        
                        --Percentage modifier.
                        elseif(saSubstrings[2] == "Pct") then
                            
                            --Compute.
                            iBonus = fnComputeStatBonusPct(gzaModTable[p][2], fAmount)
                            
                            --If the amount is positive but the bonus comes out as 0, set it to 1.
                            if(fAmount > 0.0 and iBonus < 1) then
                                iBonus = 1
                                
                            --If the amount is negative but the bonus comes out as 0, set it to -1.
                            elseif(fAmount < 0.0 and iBonus > -1) then
                                iBonus = -1
                            end
                        end
        
                        --Once computation is done, apply and store the value.
                        fnApplyStatBonus(gzaModTable[p][2], iBonus)
                        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/" .. gzaModTable[p][1], "N", iBonus)
                    
                    --[Unapply]
                    --Removes the stat stored in the DataLibrary.
                    else
                        local iBonus = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/" .. gzaModTable[p][1], "N")
                        fnApplyStatBonus(gzaModTable[p][2], iBonus * -1.0)
                    end
                    
                    --Stop execution in this type.
                    break
                end
            end
        end
        
    DL_PopActiveObject()

--[ ======================================= Set Short Text ====================================== ]
--This text appears above the enemy HP bar when the effect is active. It uses the StarlightString format.
-- The table gsaStatIcons has a set of markdowns that can be used in this text.
--LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetShortText, psString, piTargetID)
elseif(iSwitchType == gciEffect_StatMod_SetShortText and iArgumentsTotal >= 3) then

    --[Setup]
    --First argument is the string required. Second is the target's ID.
    local sString   = LM_GetScriptArgument(1)
    local iTargetID = LM_GetScriptArgument(2, "N")
    
    --Variables that may be needed.
    local iUniqueID = RO_GetID()
    
    --[Save]
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sShortText", "S", sString)
    
    --[Subroutine]
    --Call subroutine, it does most of the work.
    local sFinalString, iRemapsTotal, saRemaps = fnMarkdownHandlerStats(sString, iUniqueID, iTargetID)

    --[Upload]
    --Set the short text to the finalized string.
    AdvCombatEffect_SetProperty("Short Text", iTargetID, sFinalString)
    
    --If there are no images that need remapping, we're done.
    if(iRemapsTotal < 1) then return end
    AdvCombatEffect_SetProperty("Allocate Short Text Images", iTargetID, iRemapsTotal)
    
    --Populate the images from the stored array.
    for i = 0, iRemapsTotal-1, 1 do
        AdvCombatEffect_SetProperty("Short Text Image", iTargetID, i, gcfAbilityImgOffsetY, saRemaps[i])
    end
    
    --Crossload and we're done.
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iTargetID)

--[ =================================== Set Inspector Heading =================================== ]
--This text appears in the combat inspector as the left and right alignment. The left side is usually a buff or debuff
-- while the right is usually which stats are affected with arrows up or down.
--The same markdown language as the short text is used.
--LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetInspectorTitle, psStringLft, psStringRgt, piTargetID)
elseif(iSwitchType == gciEffect_StatMod_SetInspectorTitle and iArgumentsTotal >= 4) then

    --[Setup]
    --Target ID and strings are mandatory.
    local sStringLft = LM_GetScriptArgument(1)
    local sStringRgt = LM_GetScriptArgument(2)
    local iTargetID  = LM_GetScriptArgument(3, "N")
    
    --Variables that may be needed.
    local iUniqueID = RO_GetID()
    
    --[Save]
    --Save the highest index if it's less than 1.
    local iHighest = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHighest", "N")
    if(iHighest < 1) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHighest", "N", 1)
    end
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sString0", "S", sStringLft)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sString1", "S", sStringRgt)
    
    --[Subroutine]
    --Call subroutine, it does most of the work.
    local sFinalStringLft, iRemapsTotalLft, saRemapsLft = fnMarkdownHandlerStats(sStringLft, iUniqueID, iTargetID)
    local sFinalStringRgt, iRemapsTotalRgt, saRemapsRgt = fnMarkdownHandlerStats(sStringRgt, iUniqueID, iTargetID)

    --[Upload Left]
    --Set the description text and populate any remaps.
    AdvCombatEffect_SetProperty("Description Text",            iTargetID, 0, sFinalStringLft)
    AdvCombatEffect_SetProperty("Allocate Description Images", iTargetID, 0, iRemapsTotalLft)
    for i = 0, iRemapsTotalLft-1, 1 do
        AdvCombatEffect_SetProperty("Description Image", iTargetID, 0, i, gcfAbilityImgOffsetY, saRemapsLft[i])
    end
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 0)
    
    --[Upload Right]
    --Set the description text and populate any remaps.
    AdvCombatEffect_SetProperty("Description Text",            iTargetID, 1, sFinalStringRgt)
    AdvCombatEffect_SetProperty("Allocate Description Images", iTargetID, 1, iRemapsTotalRgt)
    for i = 0, iRemapsTotalRgt-1, 1 do
        AdvCombatEffect_SetProperty("Description Image", iTargetID, 1, i, gcfAbilityImgOffsetY, saRemapsRgt[i])
    end
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 1)

--[ =============================== Set Inspector Description Box =============================== ]
--This text appears in the combat inspector in the bottom right description box. It should provide a
-- more advanced description and specific values of what the effect does. A variable number of strings
-- can be passed, there is no hard cap.
--The same markdown language as the short text is used.
--LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetDescription, piTargetID, iStringIndex, psString)
elseif(iSwitchType == gciEffect_StatMod_SetDescription and iArgumentsTotal >= 4) then

    --[Setup]
    --Get the target ID.
    local iTargetID    = LM_GetScriptArgument(1, "N")
    local iStringIndex = LM_GetScriptArgument(2, "N")
    local sString      = LM_GetScriptArgument(3)
    
    --Variables that may be needed.
    local iUniqueID = RO_GetID()
    
    --[Save]
    --Save the highest index.
    local iHighest = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHighest", "N")
    if(iHighest < iStringIndex) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHighest", "N", iStringIndex)
    end
    
    --Save the string.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sString" .. iStringIndex, "S", sString)
    
    --[Subroutine]
    --Run the string through the markdown.
    local sFinalString, iRemapsTotal, saRemaps = fnMarkdownHandlerStats(sString, iUniqueID, iTargetID)
        
    --Upload. We start on the 2nd string since 0 and 1 are used for the headers.
    AdvCombatEffect_SetProperty("Description Text",            iTargetID, iStringIndex, sFinalString)
    AdvCombatEffect_SetProperty("Allocate Description Images", iTargetID, iStringIndex, iRemapsTotal)
    for i = 0, iRemapsTotal-1, 1 do
        AdvCombatEffect_SetProperty("Description Image", iTargetID, iStringIndex, i, gcfAbilityImgOffsetY, saRemaps[i])
    end
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, iStringIndex)
        
--[ ======================================== Post-Action ======================================== ]
--Called after a character's turn, tick off the duration and replenish the descriptions. If the effect
-- runs out, flag it to expire.
--LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction, iAlwaysRun[optional])
elseif(iSwitchType == gciEffect_PostAction) then
    
    --[Always Run]
    --Optional argument, bypasses the case of this not being a character's turn. Used for end-of-turn
    -- instead of end-of-action cases.
    local iAlwaysRun = 0.0
    if(iArgumentsTotal >= 2) then
        iAlwaysRun = LM_GetScriptArgument(1, "N")
    end
    
    --[Setup]
    --Get the ID of the acting entity.
    AdvCombat_SetProperty("Push Acting Entity")
        local iActingEntityID = RO_GetID()
    DL_PopActiveObject()
    
    --If the ID is not on the target list, we don't care.
    if(AdvCombatEffect_GetProperty("Is ID on Target List", iActingEntityID) == false and iAlwaysRun == 0.0) then
        return
    end
    
    --[Duration]
    --Decrement the duration counter. If it starts at -1, the effect does not expire.
    local iUniqueID = RO_GetID()
    local iDuration = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N")
    if(iDuration == -1) then return end
    
    --Decrement.
    iDuration = iDuration - 1
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N", iDuration)
    
    --[Remove]
    --Duration zeroes out, flag the effect to be removed.
    if(iDuration < 1) then
        AdvCombatEffect_SetProperty("Flag Remove Now")
        return
    end
    
    --[Descriptions]
    --If the effect was not removed, its duration was decremented. Update the relevant strings with
    -- the modified duration change. First, update the short text.
    local sShortText = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sShortText", "S")
    local sFinalString, iRemapsTotal, saRemaps = fnMarkdownHandlerStats(sShortText, iUniqueID, iActingEntityID)
    AdvCombatEffect_SetProperty("Short Text", iActingEntityID, sFinalString)
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iActingEntityID)
    
    --Next, for each description string, update it.
    local iHighest = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHighest", "N")
    for i = 0, iHighest, 1 do
        
        --Get and process the string.
        local sString = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sString"..i, "S")
        sFinalString, iRemapsTotal, saRemaps = fnMarkdownHandlerStats(sString, iUniqueID, iActingEntityID)
        
        --Upload.
        AdvCombatEffect_SetProperty("Description Text",             iActingEntityID, i, sFinalString)
        AdvCombatEffect_SetProperty("Crossload Description Images", iActingEntityID, i)
    end

--[ ===================================== Combat: Turn Ends ===================================== ]
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

--[ ==================================== Combat: Combat Ends ==================================== ]
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end
