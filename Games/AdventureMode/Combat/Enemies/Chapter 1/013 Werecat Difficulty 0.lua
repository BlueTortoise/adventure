--[ =================================== Werecat, Difficulty 0 =================================== ]
--Normal werecat found in the Evermoon forest. Fast, weak to bleeds.

--[ ====================================== Topic Handling ======================================= ]
--Regardless of mugging or fighting an enemy, any topics created should be handled here.
WD_SetProperty("Unlock Topic", "Werecats", 1)

--[ ===================================== Common Statistics ===================================== ]
--Setup common values regardless of functionality required from this file.
local iPlatina = 10
local iExperience = 10
local iJobPoints = 10

--Item Listing.
local zaItemList = {}
zaItemList[1] = {1, 20, "Tattered Rags"}

--[ ======================================== Mug Handling ======================================= ]
--If being mugged, provide a percentage of the base values immediately.
if(TA_GetProperty("Is Mugging Check") == true) then
    
    --Cash, XP, JP.
    TA_SetProperty("Mug Platina",    iPlatina    * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", iExperience * gcfMugExperienceRate)
    TA_SetProperty("Mug Job Points", iJobPoints  * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    for i = 1, #zaItemList, 1 do
        if(iItemRoll >= zaItemList[i][1] and iItemRoll <= zaItemList[i][2]) then
            TA_SetProperty("Mug Item", zaItemList[i][3])
        end
    end
    
    --Stop execution.
    return
end

--[ ====================================== Combat Encounter ===================================== ]
--Add this entity to the combat roster.
if(true) then
    
    --Variable setting.
    local sMeiSeenPartirhuman = VM_GetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S")
    if(sMeiSeenPartirhuman == "Nothing") then
        VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Werecat")
    end
    
	--Increment the Cassandra werecat encounter number, if the event is taking place.
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
	if(iStartedCassandraEvent == 1.0) then
		
		--Set the override for the next encounter.
		AdvCombat_SetProperty("Next Combat Music", "TimeSensitive", -1.0)
		
		--Increment.
		local iCassandraEncounters = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N")
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N", iCassandraEncounters + 1.0)
		
		--Set the time of day.
		fnSetCassandraTime()
	end

    --[Setup]
    local iEnemyID = AdvCombat_GetProperty("Generate Unique ID")
    local sEnemyUniqueName = string.format("Autogen|%02i", iEnemyID)
    AdvCombat_SetProperty("Register Enemy", sEnemyUniqueName, 0)

        --[System]
        --Name
        AdvCombatEntity_SetProperty("Display Name", "Slime")

        --DataLibrary Variables
        local iUniqueID = RO_GetID()
        DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
        
        --During dialogue, which actor represents this character.
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDialogueActor", "S", "Werecat")
        
        --AI
        AdvCombatEntity_SetProperty("AI Script", gsRoot .. "Combat/AIs/000 Normal AI.lua")

        --[Display]
        --Images
        AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/Werecat")
        AdvCombatEntity_SetProperty("Turn Icon", "Root/Images/AdventureUI/TurnPortraits/Werecat")
        
        --UI Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally, -192, -119)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main, -132,  452)

        --[Stats and Resistances]
        --Statistics
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,      50)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,     100)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,       0)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     12)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 10)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,    0)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      10)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap,    50)
        
        --Resistances
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     0)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist - 3)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist - 3)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist - 4)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist - 5)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciNormalResist)
        
        --Sum.
        AdvCombatEntity_SetProperty("Recompute Stats")
        AdvCombatEntity_SetProperty("Health Percent", 1.00)
        
        --[Tags]
        --Resistant to poison effects.
        AdvCombatEntity_SetProperty("Add Tag", "Bleed Effect +", 3)
        AdvCombatEntity_SetProperty("Add Tag", "Bleed Damage +", 3)
        AdvCombatEntity_SetProperty("Add Tag", "Bleed Apply +", 3)
        
        --Can be transformed into a squeaky thrall.
        AdvCombatEntity_SetProperty("Add Tag", "Is Rubberable", 1)

        --[Abilities]
        --System Abilities
        fnEnemyStandardSystemAbilities()

        --Register abilities.
        local sJobAbilityPath = gsRoot .. "Combat/Abilities/Chapter 1 Enemies/"
        LM_ExecuteScript(sJobAbilityPath .. "Claw Attack.lua", gciAbility_Create)
        
        --Set ability slots.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Enemy|ClawAttack")
        
        --[Rewards Handling]
        --Factor for mugging.
        local fXPFactor = 1.0
        local fJPFactor = 1.0
        local fPLFactor = 1.0
        if(TA_GetProperty("Was Mugged") == true) then
            fXPFactor = 1.0 - gcfMugExperienceRate
            fJPFactor = 1.0 - gcfMugPlatinaRate
            fPLFactor = 1.0 - gcfMugJPRate
        end
        
        --Rewards
        AdvCombatEntity_SetProperty("Reward XP",      math.floor(iExperience * fXPFactor))
        AdvCombatEntity_SetProperty("Reward JP",      math.floor(iJobPoints  * fJPFactor))
        AdvCombatEntity_SetProperty("Reward Platina", math.floor(iPlatina    * fPLFactor))
        AdvCombatEntity_SetProperty("Reward Doctor", -1)
        
        --Item rewards. These only apply if the entity was not mugged.
        if(TA_GetProperty("Was Mugged") == false) then
            local iRoll = LM_GetRandomNumber(1, 100)
            for i = 1, #zaItemList, 1 do
                if(iRoll >= zaItemList[i][1] and iRoll <= zaItemList[i][2]) then
                    AdvCombatEntity_SetProperty("Reward Item", zaItemList[i][3])
                end
            end
        end
        
    DL_PopActiveObject()
end