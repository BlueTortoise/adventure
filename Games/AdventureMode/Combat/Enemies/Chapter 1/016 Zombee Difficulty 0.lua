--[ ==================================== Zombee, Difficulty 0 =================================== ]
--Zombee found in the corrupted beehive.

--[ ====================================== Topic Handling ======================================= ]
--Regardless of mugging or fighting an enemy, any topics created should be handled here.

--[ ===================================== Common Statistics ===================================== ]
--Setup common values regardless of functionality required from this file.
local iPlatina = 10
local iExperience = 10
local iJobPoints = 10

--Item Listing.
local zaItemList = {}
zaItemList[1] = {1, 20, "Tattered Rags"}

--[ ======================================== Mug Handling ======================================= ]
--If being mugged, provide a percentage of the base values immediately.
if(TA_GetProperty("Is Mugging Check") == true) then
    
    --Cash, XP, JP.
    TA_SetProperty("Mug Platina",    iPlatina    * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", iExperience * gcfMugExperienceRate)
    TA_SetProperty("Mug Job Points", iJobPoints  * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    for i = 1, #zaItemList, 1 do
        if(iItemRoll >= zaItemList[i][1] and iItemRoll <= zaItemList[i][2]) then
            TA_SetProperty("Mug Item", zaItemList[i][3])
        end
    end
    
    --Stop execution.
    return
end

--[ ====================================== Combat Encounter ===================================== ]
--Add this entity to the combat roster.
if(true) then

    --[Setup]
    local iEnemyID = AdvCombat_GetProperty("Generate Unique ID")
    local sEnemyUniqueName = string.format("Autogen|%02i", iEnemyID)
    AdvCombat_SetProperty("Register Enemy", sEnemyUniqueName, 0)

        --[System]
        --Name
        AdvCombatEntity_SetProperty("Display Name", "Zombee")

        --DataLibrary Variables
        local iUniqueID = RO_GetID()
        DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
        
        --During dialogue, which actor represents this character.
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDialogueActor", "S", "Zombee")
        
        --AI
        AdvCombatEntity_SetProperty("AI Script", gsRoot .. "Combat/AIs/000 Normal AI.lua")

        --[Display]
        --Images
        AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/Zombee")
        AdvCombatEntity_SetProperty("Turn Icon", "Root/Images/AdventureUI/TurnPortraits/Zombee")
        
        --UI Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally, -176, -129)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main, -107,  415)

        --[Stats and Resistances]
        --Statistics
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,      50)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,     100)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,       0)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     12)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 10)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,    0)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      10)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap,    50)
        
        --Resistances
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     0)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist + 3)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist + 3)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist + 3)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist - 3)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist - 6)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist - 6)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist + 5)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciResistanceForImmunity)
        
        --Sum.
        AdvCombatEntity_SetProperty("Recompute Stats")
        AdvCombatEntity_SetProperty("Health Percent", 1.00)
        
        --[Tags]
        --No tagged resistances.
        --AdvCombatEntity_SetProperty("Add Tag", "Poison Effect -", 3)
        
        --Can NOT be transformed into a squeaky thrall.
        AdvCombatEntity_SetProperty("Add Tag", "Is Rubberable", 0)

        --[Abilities]
        --System Abilities
        fnEnemyStandardSystemAbilities()

        --Register abilities.
        local sJobAbilityPath = gsRoot .. "Combat/Abilities/Chapter 1 Enemies/"
        LM_ExecuteScript(sJobAbilityPath .. "Claw Attack.lua", gciAbility_Create)
        
        --Set ability slots.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Enemy|ClawAttack")
        
        --[Rewards Handling]
        --Factor for mugging.
        local fXPFactor = 1.0
        local fJPFactor = 1.0
        local fPLFactor = 1.0
        if(TA_GetProperty("Was Mugged") == true) then
            fXPFactor = 1.0 - gcfMugExperienceRate
            fJPFactor = 1.0 - gcfMugPlatinaRate
            fPLFactor = 1.0 - gcfMugJPRate
        end
        
        --Rewards
        AdvCombatEntity_SetProperty("Reward XP",      math.floor(iExperience * fXPFactor))
        AdvCombatEntity_SetProperty("Reward JP",      math.floor(iJobPoints  * fJPFactor))
        AdvCombatEntity_SetProperty("Reward Platina", math.floor(iPlatina    * fPLFactor))
        AdvCombatEntity_SetProperty("Reward Doctor", -1)
        
        --Item rewards. These only apply if the entity was not mugged.
        if(TA_GetProperty("Was Mugged") == false) then
            local iRoll = LM_GetRandomNumber(1, 100)
            for i = 1, #zaItemList, 1 do
                if(iRoll >= zaItemList[i][1] and iRoll <= zaItemList[i][2]) then
                    AdvCombatEntity_SetProperty("Reward Item", zaItemList[i][3])
                end
            end
        end
        
    DL_PopActiveObject()
end