--[ ================================== Chapter 1 Enemy Aliases ================================== ]
--Called at chapter boot. Creates a set of aliases for enemy names.
AdvCombat_SetProperty("Clear Enemy Aliases")
AdvCombat_SetProperty("Create Enemy Path", "Cultist Female Difficulty 0", gsRoot .. "Combat/Enemies/Chapter 1/001 Cultist F Difficulty 0.lua")
AdvCombat_SetProperty("Create Enemy Path", "Cultist Male Difficulty 0",   gsRoot .. "Combat/Enemies/Chapter 1/002 Cultist M Difficulty 0.lua")
AdvCombat_SetProperty("Create Enemy Path", "Alraune Difficulty 0",        gsRoot .. "Combat/Enemies/Chapter 1/010 Alraune Difficulty 0.lua")
AdvCombat_SetProperty("Create Enemy Path", "Bee Difficulty 0",            gsRoot .. "Combat/Enemies/Chapter 1/011 Bee Difficulty 0.lua")
AdvCombat_SetProperty("Create Enemy Path", "Slime Difficulty 0",          gsRoot .. "Combat/Enemies/Chapter 1/012 Slime Difficulty 0.lua")
AdvCombat_SetProperty("Create Enemy Path", "Werecat Difficulty 0",        gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua")
AdvCombat_SetProperty("Create Enemy Path", "Ghost Difficulty 0",          gsRoot .. "Combat/Enemies/Chapter 1/014 Ghost Difficulty 0.lua")
AdvCombat_SetProperty("Create Enemy Path", "Skullcrawler Difficulty 0",   gsRoot .. "Combat/Enemies/Chapter 1/015 Skullcrawler Difficulty 0.lua")
AdvCombat_SetProperty("Create Enemy Path", "Zombee Difficulty 0",         gsRoot .. "Combat/Enemies/Chapter 1/016 Zombee Difficulty 0.lua")

--Create the aliases. These are the strings that are stored in the .slf files.
AdvCombat_SetProperty("Create Enemy Alias", "CultistF",     "Cultist Female Difficulty 0")
AdvCombat_SetProperty("Create Enemy Alias", "CultistM",     "Cultist Male Difficulty 0")
AdvCombat_SetProperty("Create Enemy Alias", "Alraune",      "Alraune Difficulty 0")
AdvCombat_SetProperty("Create Enemy Alias", "Bee",          "Bee Difficulty 0")
AdvCombat_SetProperty("Create Enemy Alias", "Slime",        "Slime Difficulty 0")
AdvCombat_SetProperty("Create Enemy Alias", "Werecat",      "Werecat Difficulty 0")
AdvCombat_SetProperty("Create Enemy Alias", "Ghost",        "Ghost Difficulty 0")
AdvCombat_SetProperty("Create Enemy Alias", "SkullCrawler", "Skullcrawler Difficulty 0")
AdvCombat_SetProperty("Create Enemy Alias", "Zombee",       "Zombee Difficulty 0")