-- |[ ================================= Common Enemy Functions ================================= ]|
--Functions used to standardize enemy construction.

--[fnEnemyStandardSystemAbilities]
--Gives a newly created enemy their stunned/ambushed reaction abilities. Almost every enemy uses
-- these as their first ability slots.
fnEnemyStandardSystemAbilities = function()
    
    --The 0th ability is always the stun ability. Replacing this means changing enemy behavior when they get stunned!
    LM_ExecuteScript(gsRoot .. "Combat/Abilities/Z Common/Stunned.lua", gciAbility_Create)
    
    --1st ability is the Ambush handler. Change this to change how the enemy reacts when ambushed.
    LM_ExecuteScript(gsRoot .. "Combat/Abilities/Z Common/Ambushed.lua", gciAbility_Create)
    
end

