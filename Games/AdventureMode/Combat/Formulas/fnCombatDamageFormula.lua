--[ =================================== Combat Damage Formula =================================== ]
--Function that describes the standard combat damage formula. This is type-independent. If computing
-- damage for a DoT, just pass 0 for the protection value.
--Under normal circumstances, 0 is a valid response. Most abilities will then zero-check and up
-- the total damage to 1, but that is not done here.
fnComputeCombatDamage = function(piAttackPower, piProtection, piResistance)
    
    --Argument check.
    if(piAttackPower == nil) then return 0 end
    if(piProtection  == nil) then return 0 end
    if(piResistance  == nil) then return 0 end
    
    --Compute the damage factor. We use the formula 0.95 ^ Resistance. Negative resistance can increase
    -- damage past 100%.
    local fDamageFactor = math.pow(0.95, piProtection + piResistance)
    
    --Multiply.
    local fFinalDamage = math.floor(piAttackPower * fDamageFactor)
    
    --Clamp.
    if(fFinalDamage < 0.0) then fFinalDamage = 0 end
    
    --Done.
    return fFinalDamage
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end
