--[ ===================================== Compute Crit Rate ===================================== ]
--Computes and returns the crit rate for a given attacker and defender. The value returned is a percent
-- indicator from 0 to 100.
--This is independent of the ability being used. If an ability does something exotic, like rolling 
-- twice, that needs to be handled in the calling script.
fnComputeCritRate = function(piAccBonus, piAccMalus, piBaseCrit)
    
    --Argument check.
    if(piAccBonus == nil) then return -1 end
    if(piAccMalus == nil) then return -1 end
    if(piBaseCrit == nil) then return -1 end
    
    --Compute.
    local iCritRate = 100 + piAccBonus - piAccMalus - piBaseCrit
    
    --Clamp.
    if(iCritRate <   0) then iCritRate =   0 end
    if(iCritRate > 100) then iCritRate = 100 end
    
    --Return.
    return iCritRate
end

--Alternative version which uses the IDs of the entities.
fnComputeCritRateID = function(piAttackerID, piDefenderID, piBaseCrit)
    
    --Argument check.
    if(piAttackerID == nil) then return -1 end
    if(piDefenderID == nil) then return -1 end
    if(piBaseCrit   == nil) then return -1 end
    
    --Push and query attacker.
    AdvCombat_SetProperty("Push Entity By ID", piAttackerID)
        local iAccBonus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Accuracy)
    DL_PopActiveObject()
    
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
        local iAccMalus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Evade)
    DL_PopActiveObject()
    
    --Return.
    return fnComputeCritRate(iAccBonus, iAccMalus, piBaseCrit)
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end
