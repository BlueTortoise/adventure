--[ =================================== Compute Damage Range ==================================== ]
--[Scattering Function]
--Takes in a damage number and outputs the low and high range when scattered.
fnScatterDamage = function(piDamage, piScatterRange)
    
    --Arg check.
    if(piDamage       == nil) then return 0, 0 end
    if(piScatterRange == nil) then return 0, 0 end
    
    --Zero damage.
    if(piDamage < 1) then return 0, 0 end
    
    --No scatter.
    if(piScatterRange < 1) then return piDamage, piDamage end
    
    --Set high and low values.
    local iLoDamage = piDamage
    local iHiDamage = piDamage
        
    --The number of possible elements in the scatter roll is 2x scatter + 1. The +1 is for the zero. A scatter of 1
    -- therefore has 3 possibilities: -1, 0, 1.
    local fLoRange = (100 - piScatterRange) / 100.0
    local fHiRange = (100 + piScatterRange) / 100.0
        
    --Modify.
    iLoDamage = math.floor(iLoDamage * fLoRange)
    iHiDamage = math.floor(iHiDamage * fHiRange)

    --Clamp.
    if(iLoDamage < 1 and bAtLeastOneNonImmuneType) then
        iLoDamage = 1
    end
    if(iHiDamage < 1 and bAtLeastOneNonImmuneType) then
        iHiDamage = 1
    end
    
    --Return.
    return iLoDamage, iHiDamage
end

--Given attack power and resistance, returns the minimum and maximum damage dealt.
fnComputeDamageRangeType = function(piAttackPower, piResistance, pbIsTargetImmune, piScatterRange)
    
    --Argument check.
    if(piAttackPower    == nil) then return 0, 0 end
    if(piResistance     == nil) then return 0, 0 end
    if(pbIsTargetImmune == nil) then return 0, 0 end
    if(piScatterRange   == nil) then return 0, 0 end
    
    --Attack power is zero.
    if(piAttackPower < 1) then return 0, 0 end
    
    --Target is immune!
    if(pbIsTargetImmune) then return 0, 0 end
    
    --Compute. This is the zero-scatter damage.
    local iDamage = fnComputeCombatDamage(piAttackPower, 0, piResistance)
    
    --Now scatter it.
    return fnScatterDamage(iDamage, piScatterRange)
end

--Given IDs and a resistance type, returns the minimum and maximum damage dealt.
fnComputeDamageRangeTypeID = function(piAttackerID, piDefenderID, piAttackType, piScatterRange)

    --[Setup]
    --Argument check.
    if(piAttackerID   == nil) then return 0, 0 end
    if(piDefenderID   == nil) then return 0, 0 end
    if(piAttackType   == nil) then return 0, 0 end
    if(piScatterRange == nil) then return 0, 0 end
    
    --[Attacker Statistics]
    --Push and query attacker.
    AdvCombat_SetProperty("Push Entity By ID", piAttackerID)
        local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
    DL_PopActiveObject()
    
    --[Defender Statistics]
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
        local iTargetProtection = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Protection)
        local iTargetResistance = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start+piAttackType)
        local bIsTargetImmune   = fnIsEntityImmuneToDamageType(piAttackType)
    DL_PopActiveObject()
    
    --Subroutine.
    return fnComputeDamageRangeType(iOriginatorAttackPower, iTargetProtection + iTargetResistance, bIsTargetImmune, piScatterRange)
end

--Abilities that deal "Weapon" damage deal a variable type, which can include several types. This algorithm determines them and sums them together.
fnComputeDamageRangeWeapon = function(piAttackerID, piDefenderID, piScatterRange)
    
    --[Setup]
    --Argument check.
    if(piAttackerID   == nil) then return 0, 0 end
    if(piDefenderID   == nil) then return 0, 0 end
    if(piScatterRange == nil) then return 0, 0 end
    
    --Tag Structure
    local zaTags = fnCreateTagStruct()
    
    --[Attacker Statistics]
    --Push and query attacker.
    AdvCombat_SetProperty("Push Entity By ID", piAttackerID)
        local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        local fOriginatoraDamageFactors = fnBuildWeaponDamage()
        
        --Tags.
        zaTags.iBleedDamageDealtUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt +")
        zaTags.iBleedDamageDealtDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt -")
    DL_PopActiveObject()
    
    --[Defender Statistics]
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)

        --Get variables.
        local fX, fY = AdvCombatEntity_GetProperty("Position")
        local iTargetProtection = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Protection)
        
        --Get resistance factors.
        local iaTargetResistances = {}
        local baTargetImmunities = {}
        for i = 0, gciDamageType_Total-1, 1 do
            iaTargetResistances[i] = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start+i)
            baTargetImmunities[i] = fnIsEntityImmuneToDamageType(i)
        end
        
        --Tags.
        zaTags.iBleedDamageTakenUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken +")
        zaTags.iBleedDamageTakenDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken -")

    --Clean.
    DL_PopActiveObject()
        
    --[Damage Computation]
    --Iterate across the damage types.
    local bAtLeastOneNonImmuneType = false
    local iTotalDamage = 0
    for i = 0, gciDamageType_Total-1, 1 do
        
        --If the value is nonzero:
        if(fOriginatoraDamageFactors[i] > 0.0) then
            
            --Check if the target is immune to this damage type:
            if(baTargetImmunities[i] == true) then
            
            --Not immune. Add damage.
            else
            
                --Set this flag.
                bAtLeastOneNonImmuneType = true
            
                --Compute.
                local fBonusPct = 1.0
                local iDamage = fnComputeCombatDamage(iOriginatorAttackPower * fOriginatoraDamageFactors[i], iTargetProtection, iaTargetResistances[i])
                
                --Add bleed damage from tags.
                if(i == gciDamageType_Bleeding) then
                    fBonusPct = fBonusPct + (0.01 * (zaTags.iBleedDamageTakenUpTags + zaTags.iBleedDamageDealtUpTags - zaTags.iBleedDamageTakenDnTags - zaTags.iBleedDamageDealtDnTags))
                end
                
                if(iDamage > 0) then
                    iTotalDamage = iTotalDamage + (iDamage * fBonusPct)
                end
            end
        end
    end

    --[Finish]
    --If there were no non-immune types, return zero damage.
    if(bAtLeastOneNonImmuneType == false) then return -100, -100 end
    
    --Once total damage is computer, return the scattered value.
    return fnScatterDamage(iTotalDamage, piScatterRange)
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end
