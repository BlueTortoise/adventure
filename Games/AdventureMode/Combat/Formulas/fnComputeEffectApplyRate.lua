--[ ====================================== Compute Effect Apply Rate ===================================== ]
--Computes and returns the change for a given Effect to apply to a given defender. Returned value is an integer
-- from 0 to 100.
fnComputeEffectApplyRate = function(piResistance, piStrength)
    
    --[Argument check]
    if(piResistance == nil) then return 0 end
    if(piStrength   == nil) then return 0 end
    
    --[Finalize]
    local iEffectiveResist = piResistance - piStrength
    local iApplyRate = (10 - iEffectiveResist) * 10
    if(iApplyRate <   0) then iApplyRate =   0 end
    if(iApplyRate > 100) then iApplyRate = 100 end
    return iApplyRate
end

--Given the target ID, returns the application rate.
fnComputeEffectApplyRateID = function(piDefenderID, piStrength, piType)
    
    --Arg check.
    if(piDefenderID == nil) then return 0 end
    if(piStrength   == nil) then return 0 end
    if(piType       == nil) then return 0 end

    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
        local iResistance = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start + piType)
    DL_PopActiveObject()
    
    --Finish up.
    return fnComputeEffectApplyRate(iResistance, piStrength)
end


--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end
