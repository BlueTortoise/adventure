--[ ===================================== Standard Accuracy ===================================== ]
--The 'Standard' accuracy routine. Given an attacker and a target, rolls to hit with a given bass
-- miss rate, which can be zero or negative.
--If the attack rolls under the miss rate, it misses. If the total roll is over the crit rate, it crits.
--The values gbAttackHit and gbAttackCrit are populated with the results.
fnStandardAccuracy = function(piAttackerID, piDefenderID, piBaseMiss, piBaseCrit)
    
    --Argument check.
    if(piAttackerID == nil) then return end
    if(piDefenderID == nil) then return end
    if(piBaseMiss   == nil) then return end
    if(piBaseCrit   == nil) then return end
    
    --Reset variables.
    gbAttackHit = false
    gbAttackCrit = false
    gbAttackGlances = false

    --[Attacker Statistics]
    --Push and query attacker.
    AdvCombat_SetProperty("Push Entity By ID", piAttackerID)
        local iAccBonus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Accuracy)
        local iAlwaysHitTags  = AdvCombatEntity_GetProperty("Tag Count", "Always Hit")
        local iNeverHitTags   = AdvCombatEntity_GetProperty("Tag Count", "Never Hit")
        local iAlwaysCritTags = AdvCombatEntity_GetProperty("Tag Count", "Always Crit")
        local iNeverCritTags  = AdvCombatEntity_GetProperty("Tag Count", "Never Crit")
    DL_PopActiveObject()
    
    --[Defender Statistics]
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
        local iAccMalus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Evade)
    DL_PopActiveObject()
    
    --[Always Hit]
    local iAccuracyRoll = 0
    if(iAlwaysHitTags > iNeverHitTags) then
        gbAttackHit = true
    
    --[Never Hit]
    elseif(iNeverHitTags > iAlwaysHitTags) then
        gbAttackHit = false
        return
    
    --[Run Computations]
    else
        local iHitRate = fnComputeHitRate(iAccBonus, iAccMalus, piBaseMiss)
        iAccuracyRoll = LM_GetRandomNumber(1, 100)
        
        --Get the difference between the accuracy and hit rate
        local iDiff = iAccuracyRoll - iHitRate
        if(false) then
            io.write("Report:\n")
            io.write(" Acc: " .. iAccuracyRoll .. "\n")
            io.write(" Hit: " .. iHitRate .. "\n")
            io.write(" Dif: " .. iDiff .. "\n")
        end
        
        --If the difference is positive, but less than 25, the attack is a glancing blow.
        -- Glancing blows cannot crit, but that is handled later.
        if(iDiff > 0 and iDiff <= 25) then
            gbAttackGlances = true
            --io.write(" Attack glanced.\n")
        
        --If the roll is above the glance range, it's a full miss.
        elseif(iDiff > 25) then
            --io.write(" Attack missed.\n")
            return
        end
        
        --All other cases, hit.
        gbAttackHit = true
           -- io.write(" Attack hit.\n")
    
    end
    
    --[Always Crit]
    if(iAlwaysCritTags > iNeverCritTags) then
        gbAttackCrit = true
    
    --[Never Crit]
    elseif(iNeverCritTags > iAlwaysCritTags) then
        return
    
    --[Run Computations]
    else
        local iCritRate = fnComputeCritRate(iAccBonus, iAccMalus, piBaseCrit)
        if(iAccuracyRoll > iCritRate) then return end
        gbAttackCrit = true
    end
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end
