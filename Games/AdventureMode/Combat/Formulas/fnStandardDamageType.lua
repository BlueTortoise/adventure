--[ =================================== Standard Damage Type ==================================== ]
--The 'Standard' damage computation, given an originator, a target, and a scatter range. This does not
-- handle any animation. The global damage and attack type values are populated so the caller can
-- handle animating this.
--Unlike fnStandardAttack, this uses only one damage type. They are otherwise very similar.

--The value giStandardDamage is populated with the damage.
--The value gbStandardImmune is populated with whether or not the target was immune to the damage type.

--The argument piAttackType should be one of gciDamageType_[x].
--The argument piScatterRange is a positive integer. Pass 0 for no scatter.
fnStandardDamageType = function(piAttackerID, piDefenderID, piAttackType, piScatterRange)
    
    --[Setup]
    --Argument check.
    if(piAttackerID   == nil) then return end
    if(piDefenderID   == nil) then return end
    if(piAttackType   == nil) then return end
    if(piScatterRange == nil) then return end
    
    --Set globals.
    giStandardDamage = 0
    gbStandardImmune = false
    giStandardAttackType = piAttackType
    
    --[Attacker Statistics]
    --Push and query attacker.
    AdvCombat_SetProperty("Push Entity By ID", piAttackerID)
        local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
    DL_PopActiveObject()
    
    --[Defender Statistics]
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
        local iTargetProtection = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Protection)
        local iTargetResistance = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start+piAttackType)
        local bIsTargetImmune   = fnIsEntityImmuneToDamageType(piAttackType)
    DL_PopActiveObject()
    
    --Target is immune!
    if(bIsTargetImmune) then
        gbStandardImmune = true
        return
    end
    
    --Compute.
    local iDamage = fnComputeCombatDamage(iOriginatorAttackPower * 1.0, iTargetProtection, iTargetResistance)
    
    --No scattering, we're done.
    if(piScatterRange < 1.0) then
        giStandardDamage = iDamage
    end
    
    --Apply scatter.
    local iLoDamage, iHiDamage = fnScatterDamage(iDamage, piScatterRange)
    
    --Roll three numbers and divide by the max. This gives a bell-curve rather than a flat roll.
    local iRollA = LM_GetRandomNumber(0, 100)
    local iRollB = LM_GetRandomNumber(0, 100)
    local iRollC = LM_GetRandomNumber(0, 100)
    local fFinalPercent = (iRollA + iRollB + iRollC) / 300.0
    
    --Compute the final damage.
    local iFinalDamage = math.floor(iLoDamage + ((iHiDamage - iLoDamage) * fFinalPercent))

    --[Finish Up]
    --Clamp to 0 damage.
    if(iFinalDamage < 0) then iFinalDamage = 0 end
    
    --Populate globals.
    giStandardDamage = iFinalDamage
    giStandardAttackType = iHighestDamageIndex
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end
