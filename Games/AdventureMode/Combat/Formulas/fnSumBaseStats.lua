--[ ======================================= Sum Base Stat ======================================= ]
--When an Effect applies a multiplier to a stat, such as 150% attack power, it takes the base value
-- for the multiplier to affect as a sum of several other stats. These stats are:
--Base Value
--Job Bonus
--Equipment Bonus
--Permanent Effect, if present

--Sums and returns the base stats for a given index. The ID is of the entity in question.
function fnSumBaseStat(piEntityID, piStatIndex)
    if(piEntityID  == nil) then return 0 end
    if(piStatIndex == nil) then return 0 end

    --Push
    local iSum = 0
    AdvCombat_SetProperty("Push Entity By ID", piEntityID)
        iSum = iSum + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Base,        piStatIndex)
        iSum = iSum + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Job,         piStatIndex)
        iSum = iSum + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Equipment,   piStatIndex)
        iSum = iSum + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_PermaEffect, piStatIndex)
    DL_PopActiveObject()
    
    --Done.
    return iSum
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end