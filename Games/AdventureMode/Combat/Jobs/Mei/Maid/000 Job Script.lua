--[ ============================================ Maid =========================================== ]
--Mei's ghost class.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ======================================== Job Creation ======================================= ]
--Called when the chapter is initialized, the job registers itself to its owner and populates its
-- abilities. This is only called once.
--The owner should be the active object.
if(iSwitchType == gciJob_Create) then
    
    --Create the job and set its stats.
    AdvCombatEntity_SetProperty("Create Job", "Maid")
    
        --Name as it appears in the UI.
        AdvCombatJob_SetProperty("Display Name", "Maid")
    
        --Script path to this job
        AdvCombatJob_SetProperty("Script Path", LM_GetCallStack(0))
        
        --Responses.
        AdvCombatJob_SetProperty("Script Response", gciJob_SwitchTo, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Level, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Master, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_JobAssembleSkillList, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_BeginCombat, true)
        
    DL_PopActiveObject()
    
    --Register the job's abilities to the entity. Abilities with "JOBNAME|ABILITYNAME" are abilities
    -- used by the job itself, while "UNLOCK-JOBNAME|ABILITYNAME" are those the player can purchase.
    --Even if two such abilities share the same name, they don't have to be the same ability.
    local sJobAbilityPath = gsRoot .. "Combat/Abilities/Mei_Maid/"
    LM_ExecuteScript(sJobAbilityPath .. "Duster Blast.lua",      gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "First Aid.lua",         gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Icy Hand.lua",          gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Tidy Up.lua",           gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Translucent Smile.lua", gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Undying.lua",           gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Way Of The Dead.lua",   gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Freezing Blade.lua",    gciAbility_Create)
    
    --Internal versions.
    LM_ExecuteScript(sJobAbilityPath .. "Duster Blast.lua",      gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Icy Hand.lua",          gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "First Aid.lua",         gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Tidy Up.lua",           gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Translucent Smile.lua", gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Undying.lua",           gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Freezing Blade.lua",    gciAbility_CreateSpecial)
   
--[ ======================================= Job Assumption ====================================== ]
--Called when the player switches to the class. Clears any existing job properties, sets graphics
-- and stats as needed.
--This can be called during combat, as some characters can switch jobs in-battle.
--The calling AdvCombatEntity is expected to be the active object.
elseif(iSwitchType == gciJob_SwitchTo) then

    --Change images.
    VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Ghost")
    LM_ExecuteScript(gsCostumeAutoresolve, "Mei_Ghost")
    
    --Push the owning entity.
    AdvCombatJob_SetProperty("Push Owner")

        --[Statistics]
        --Get health percentage.
        local fHPPercent = AdvCombatEntity_GetProperty("Health") / AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        
        --Set job statistics.
        AdvCombatEntity_SetProperty("Compute Level Statistics", -1)
        
        --Apply change in max HP.
        AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
        
        --Clear ability slots.
        fnClearJobSlots()
        
        --[Top Bar]
        --Set basic actions bar.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Common|Attack")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Common|Parry")
        
        --4-CP ability.
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 0, "MaidInternal|Freezing Blade")
        
        --Items, Retreat, Surrender, Etc.
        fnPlaceJobCommonSkills()
        
        --[Class Bar]
        --Set class-specific bar.
        local iSkillbookTotal = VM_GetVar("Root/Variables/Global/Mei/iSkillbookTotal", "N")
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 1, "MaidInternal|DusterBlast")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 1, "MaidInternal|IcyHand")
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 1, "Common|Locked")
        AdvCombatEntity_SetProperty("Set Ability Slot", 3, 1, "Common|Locked")
        AdvCombatEntity_SetProperty("Set Ability Slot", 4, 1, "Common|Locked")
        AdvCombatEntity_SetProperty("Set Ability Slot", 5, 1, "Common|Locked")
        
        --Unlocked abilities.
        if(iSkillbookTotal >= 1.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 2, 1, "MaidInternal|FirstAid")
        end
        if(iSkillbookTotal >= 2.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 3, 1, "MaidInternal|TidyUp")
        end
        if(iSkillbookTotal >= 3.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 4, 1, "MaidInternal|TranslucentSmile")
        end
        if(iSkillbookTotal >= 4.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 5, 1, "MaidInternal|Undying")
        end
        
        --[Job Changes]
        LM_ExecuteScript(gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua")

    DL_PopActiveObject()

--[ ======================================= Job Level Up ======================================== ]
--Called when the job reaches a level. The second argument is the level in question. Remember that
-- level 1 on the UI is level 0 in the scripts.
--When assuming the job, this is called by the program to reset stats. There is no need to call it
-- during the Job Assumption part of this script.
elseif(iSwitchType == gciJob_Level) then

    --[Setup]
    --Argument check.
    if(iArgumentsTotal < 2) then 
        Debug_ForcePrint("Error: No level specified for Job Level Up. " .. LM_GetCallStack(0) .. "\n") 
        return
    end

    --First argument is what level we reached.
    local iLevelReached = LM_GetScriptArgument(1, "N")
    
    --[Base Values]
    --These are the flat bonuses the class gets.
    giaHPArray[0]  = 0
    giaAtkArray[0] = 0
    giaAccArray[0] = 0
    giaEvdArray[0] = 0
    giaIniArray[0] = 0
    
    --[Function Handler]
    --Call this subroutine to auto-compute the stats and upload them.
    AdvCombat_SetProperty("Clear Level Up Struct")
    fnStandardJobStatHandler(gsRoot .. "Combat/Stat Profiles/Mei/Maid.lua", iLevelReached)
    
    --[Resistances]
    --Flat values.
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Protection,     0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Slash,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Strike,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Pierce,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Flame,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Freeze,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Shock,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Crusade, 0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Obscure, 0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Bleed,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Poison,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Corrode, 0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Terrify, 0)
    
--[ ======================================== Job Mastered ======================================= ]
--Called when the player masters the class and gets the mastery bonus applied. Also called when the
-- character changes class and has the mastery bonus applied to that class.
elseif(iSwitchType == gciJob_Master) then
    
--[ ==================================== Assemble Skill List ==================================== ]
--Once all abilities are registered to the parent entity, builds a list of which skills are associated
-- with this job. These skills show up on the UI and can be purchased by the player. Note that a job
-- can contain the same ability as another job. Purchasing it in either will unlock both versions.
-- This is not explicitly a problem, it means the same ability can be bought with JP from multiple
-- jobs. It is not part of the standard for Adventure Mode though.
elseif(iSwitchType == gciJob_JobAssembleSkillList) then

    --Setup.
    local sJobName = "Maid"
    
    --Register abilities in the order they should appear in the skills menu.
    AdvCombatEntity_SetProperty("Register Ability To Job", "Maid|DusterBlast",      sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Maid|FirstAid",         sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Maid|IcyHand",          sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Maid|TidyUp",           sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Maid|TranslucentSmile", sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Maid|Undying",          sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Maid|WayOfTheDead",     sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Maid|Freezing Blade",   sJobName)
    
--[ ======================================= Combat Start ======================================== ]
--Called when combat begins. The job should populate passive abilities here. The AdvCombatEntity
-- will be atop the activity stack.
elseif(iSwitchType == gciJob_BeginCombat) then
    AdvCombatJob_SetProperty("Push Owner")
        fnPlaceJobCommonSkills()
        fnPlaceJobCommonSkillsCombatStart()
    DL_PopActiveObject()
    
end
