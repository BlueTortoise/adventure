--[ ======================================== Smarty Sage ======================================== ]
--Mei's slime class.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ ======================================== Job Creation ======================================= ]
--Called when the chapter is initialized, the job registers itself to its owner and populates its
-- abilities. This is only called once.
--The owner should be the active object.
if(iSwitchType == gciJob_Create) then
    
    --Create the job and set its stats.
    AdvCombatEntity_SetProperty("Create Job", "Smarty Sage")
    
        --Name as it appears in the UI.
        AdvCombatJob_SetProperty("Display Name", "Smarty Sage")
    
        --Script path to this job
        AdvCombatJob_SetProperty("Script Path", LM_GetCallStack(0))
        
        --Responses.
        AdvCombatJob_SetProperty("Script Response", gciJob_SwitchTo, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Level, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Master, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_JobAssembleSkillList, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_BeginCombat, true)
        
    DL_PopActiveObject()
    
    --Register the job's abilities to the entity. Abilities with "JOBNAME|ABILITYNAME" are abilities
    -- used by the job itself, while "UNLOCK-JOBNAME|ABILITYNAME" are those the player can purchase.
    --Even if two such abilities share the same name, they don't have to be the same ability.
    local sJobAbilityPath = gsRoot .. "Combat/Abilities/Mei_SmartySage/"
    LM_ExecuteScript(sJobAbilityPath .. "Absorb.lua",           gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Big Dumb Grin.lua",    gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Firm Up.lua",          gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Goopshot.lua",         gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Jiggly Chest.lua",     gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Splash.lua",           gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Slimy Strike.lua",     gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Way Of The Slime.lua", gciAbility_Create)
    
    --Internal versions.
    LM_ExecuteScript(sJobAbilityPath .. "Splash.lua",        gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Goopshot.lua",      gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Absorb.lua",        gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Firm Up.lua",       gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Jiggly Chest.lua",  gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Big Dumb Grin.lua", gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Slimy Strike.lua",  gciAbility_CreateSpecial)
   
--[ ======================================= Job Assumption ====================================== ]
--Called when the player switches to the class. Clears any existing job properties, sets graphics
-- and stats as needed.
--This can be called during combat, as some characters can switch jobs in-battle.
--The calling AdvCombatEntity is expected to be the active object.
elseif(iSwitchType == gciJob_SwitchTo) then

    --Change images.
    VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Slime")
    LM_ExecuteScript(gsCostumeAutoresolve, "Mei_Slime")
    
    --Push the owning entity.
    AdvCombatJob_SetProperty("Push Owner")

        --[Statistics]
        --Get health percentage.
        local fHPPercent = AdvCombatEntity_GetProperty("Health") / AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        
        --Set job statistics.
        AdvCombatEntity_SetProperty("Compute Level Statistics", -1)
        
        --Apply change in max HP.
        AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
        
        --Clear ability slots.
        fnClearJobSlots()
        
        --[Top Bar]
        --Set basic actions bar.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Common|Attack")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Common|Parry")
        
        --4-CP ability.
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 0, "SmartySageInternal|Slimy Strike")
        
        --Items, Retreat, Surrender, Etc.
        fnPlaceJobCommonSkills()
        
        --[Class Bar]
        --Set class-specific bar.
        local iSkillbookTotal = VM_GetVar("Root/Variables/Global/Mei/iSkillbookTotal", "N") 
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 1, "SmartySageInternal|Splash")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 1, "SmartySageInternal|Goopshot")
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 1, "Common|Locked")
        AdvCombatEntity_SetProperty("Set Ability Slot", 3, 1, "Common|Locked")
        AdvCombatEntity_SetProperty("Set Ability Slot", 4, 1, "Common|Locked")
        AdvCombatEntity_SetProperty("Set Ability Slot", 5, 1, "Common|Locked")
        
        --Unlocked abilities.
        if(iSkillbookTotal >= 1.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 2, 1, "SmartySageInternal|Absorb")
        end
        if(iSkillbookTotal >= 2.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 3, 1, "SmartySageInternal|FirmUp")
        end
        if(iSkillbookTotal >= 3.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 4, 1, "SmartySageInternal|JigglyChest")
        end
        if(iSkillbookTotal >= 4.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 5, 1, "SmartySageInternal|BigDumbGrin")
        end
        
        --[Job Changes]
        LM_ExecuteScript(gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua")

    DL_PopActiveObject()

--[ ======================================= Job Level Up ======================================== ]
--Called when the job reaches a level. The second argument is the level in question. Remember that
-- level 1 on the UI is level 0 in the scripts.
--When assuming the job, this is called by the program to reset stats. There is no need to call it
-- during the Job Assumption part of this script.
elseif(iSwitchType == gciJob_Level) then

    --[Setup]
    --Argument check.
    if(iArgumentsTotal < 2) then 
        Debug_ForcePrint("Error: No level specified for Job Level Up. " .. LM_GetCallStack(0) .. "\n") 
        return
    end

    --First argument is what level we reached.
    local iLevelReached = LM_GetScriptArgument(1, "N")
    
    --[Base Values]
    --These are the flat bonuses the class gets.
    giaHPArray[0]  = 0
    giaAtkArray[0] = 0
    giaAccArray[0] = 0
    giaEvdArray[0] = 0
    giaIniArray[0] = 0
    
    --[Function Handler]
    --Call this subroutine to auto-compute the stats and upload them.
    AdvCombat_SetProperty("Clear Level Up Struct")
    fnStandardJobStatHandler(gsRoot .. "Combat/Stat Profiles/Mei/Smarty Sage.lua", iLevelReached)
    
    --[Resistances]
    --Flat values.
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Protection,     0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Slash,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Strike,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Pierce,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Flame,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Freeze,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Shock,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Crusade, 0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Obscure, 0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Bleed,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Poison,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Corrode, 0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Terrify, 0)
    
--[ ======================================== Job Mastered ======================================= ]
--Called when the player masters the class and gets the mastery bonus applied. Also called when the
-- character changes class and has the mastery bonus applied to that class.
elseif(iSwitchType == gciJob_Master) then
    
--[ ==================================== Assemble Skill List ==================================== ]
--Once all abilities are registered to the parent entity, builds a list of which skills are associated
-- with this job. These skills show up on the UI and can be purchased by the player. Note that a job
-- can contain the same ability as another job. Purchasing it in either will unlock both versions.
-- This is not explicitly a problem, it means the same ability can be bought with JP from multiple
-- jobs. It is not part of the standard for Adventure Mode though.
elseif(iSwitchType == gciJob_JobAssembleSkillList) then

    --Setup.
    local sJobName = "Smarty Sage"
    
    --Register abilities in the order they should appear in the skills menu.
    AdvCombatEntity_SetProperty("Register Ability To Job", "SmartySage|Splash", sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "SmartySage|Goopshot", sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "SmartySage|Absorb", sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "SmartySage|FirmUp", sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "SmartySage|JigglyChest", sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "SmartySage|BigDumbGrin", sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "SmartySage|WayOfTheSlime", sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "SmartySage|Slimy Strike", sJobName)

--[ ======================================= Combat: Begin ======================================= ]
--Called when combat begins. This is called after entity and before Ability update calls, and before
-- turn-order resolution.
elseif(iSwitchType == gciJob_BeginCombat) then
    AdvCombatJob_SetProperty("Push Owner")
        fnPlaceJobCommonSkills()
        fnPlaceJobCommonSkillsCombatStart()
    DL_PopActiveObject()
    
--[ ===================================== Combat: New Turn ====================================== ]
--Called when turn order is sorted and a new turn begins.
elseif(iSwitchType == gciJob_BeginTurn) then

--[ ===================================== Combat: New Action ==================================== ]
--Called when an action begins. Note that the action is not necessarily that of the active entity.
elseif(iSwitchType == gciJob_BeginAction) then
    
--[ ========================== Combat: New Action (After Free Action) =========================== ]
--Called when an action begins after a Free Action has expired.
elseif(iSwitchType == gciJob_BeginFreeAction) then

--[ ===================================== Combat: End Action ==================================== ]
--Called when an action ends. As above, not necessarily that of the active entity.
elseif(iSwitchType == gciJob_EndAction) then

--[ ===================================== Combat: End Turn ====================================== ]
--Called when a turn ends, before turn order is determined for the next turn.
elseif(iSwitchType == gciJob_EndTurn) then

--[ ===================================== Combat: End Combat ==================================== ]
--Called when combat ends for any reason. This includes, victory, defeat, retreat, or other.
elseif(iSwitchType == gciJob_EndCombat) then

--[ =================================== Combat: Event Queued ==================================== ]
--Called when a main event is enqueued. An entity typically performs one event per action. This is
-- called before after Entity but before Abilities respond for this character.
elseif(iSwitchType == gciJob_EventQueued) then
    
end
