--[ =================================== Build Secondary Menu ==================================== ]
--Mei can change classes in combat. This subroutine determines the current class and populates
-- what classes are available.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--Variables.
local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
local iHasBeeForm     = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
local iHasSlimeForm   = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N")
local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
local iHasGhostForm   = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
local iHasZombeeForm  = VM_GetVar("Root/Variables/Global/Mei/iHasZombeeForm", "N")

--Fencer. Always available.
if(sMeiForm ~= "Human") then
    AdvCombatEntity_SetProperty("Set Secondary Slot", 0, 0, "JobChange|Fencer")
end

--Nightshade. Requires alraune form.
if(sMeiForm ~= "Alraune" and iHasAlrauneForm == 1.0) then
    AdvCombatEntity_SetProperty("Set Secondary Slot", 1, 0, "JobChange|Nightshade")
end

--Hive Scout. Requires bee form.
if(sMeiForm ~= "Bee" and iHasBeeForm == 1.0) then
    AdvCombatEntity_SetProperty("Set Secondary Slot", 2, 0, "JobChange|HiveScout")
end

--SmartySage. Requires slime form.
if(sMeiForm ~= "Slime" and iHasSlimeForm == 1.0) then
    AdvCombatEntity_SetProperty("Set Secondary Slot", 3, 0, "JobChange|SmartySage")
end

--Prowler. Requires werecat form.
if(sMeiForm ~= "Werecat" and iHasWerecatForm == 1.0) then
    AdvCombatEntity_SetProperty("Set Secondary Slot", 4, 0, "JobChange|Prowler")
end

--Maid. Requires ghost form.
if(sMeiForm ~= "Ghost" and iHasGhostForm == 1.0) then
    AdvCombatEntity_SetProperty("Set Secondary Slot", 5, 0, "JobChange|Maid")
end

--Zombee.
if(sMeiForm ~= "Zombee" and iHasZombeeForm == 1.0) then
    AdvCombatEntity_SetProperty("Set Secondary Slot", 0, 1, "JobChange|Zombee")
end