--[ ====================================== Apply Stat Bonus ===================================== ]
--Shorthand to quickly apply a stat bonus to a given stat. Frequently used in effect script.
function fnApplyStatBonus(piStatIndex, piAmount)
    local iBonus = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_TempEffect, piStatIndex)
    iBonus = iBonus + piAmount
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_TempEffect, piStatIndex, iBonus)
    AdvCombatEntity_SetProperty("Recompute Stats")
end

--Version that applies a stat bonus as a percentage of a base. Returns the applied bonus.
function fnApplyStatBonusPct(piStatIndex, pfPercent)
    
    --Base value.
    local iBaseVal =      AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Base,      piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Job,       piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Equipment, piStatIndex)
    
    --Bonus as percentage of base value.
    local iBonus = math.floor(iBaseVal * pfPercent)
    
    --Current value.
    local iCurrentVal = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_TempEffect, piStatIndex)
    iCurrentVal = iCurrentVal + iBonus
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_TempEffect, piStatIndex, iCurrentVal)
    
    --Recompute stats, return the bonus value.
    AdvCombatEntity_SetProperty("Recompute Stats")
    return iBonus
end

--Version that computes and returns the value, but does not modify it.
function fnComputeStatBonusPct(piStatIndex, pfPercent)
    
    --Base value.
    local iBaseVal =      AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Base,      piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Job,       piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Equipment, piStatIndex)
    
    --Bonus as percentage of base value.
    local iBonus = math.floor(iBaseVal * pfPercent)
    return iBonus
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end