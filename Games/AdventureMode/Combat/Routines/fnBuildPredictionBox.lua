--[ =================================== Prediction Structure ==================================== ]
--Creates a package for the prediction box with default values and passes it back.
function fnCreatePredictionPack()
    
    --Setup.
    local zPackage = {}
    
    --Hit-Rate Module
    zPackage.bHasHitRateModule = false
    zPackage.zHitRateModule = {}
    zPackage.zHitRateModule.iHitRate = 0
    zPackage.zHitRateModule.iAccuracyBonus = 0
    zPackage.zHitRateModule.iAccuracyMalus = 0
    zPackage.zHitRateModule.iMissRate = 0
    
    --Damage Module
    zPackage.bHasDamageModule = false
    zPackage.zDamageModule = {}
    zPackage.zDamageModule.iDamageLo = 0
    zPackage.zDamageModule.iDamageHi = 0
    zPackage.zDamageModule.sDamageIco = {}
    zPackage.zDamageModule.bUseWeaponType = false
    zPackage.zDamageModule.iDamageType = gciDamageType_Slashing
    zPackage.zDamageModule.fDamageFactor = 1.00
    zPackage.zDamageModule.iScatterRange = 15
    
    --Stun Module
    zPackage.bHasStunModule = false
    zPackage.zStunModule = {}
    zPackage.zStunModule.iStunBase = 0
    
    --Healing Module
    zPackage.bHasHealingModule = false
    zPackage.zHealingModule = {}
    zPackage.zHealingModule.iHealingFinal = 0
    zPackage.zHealingModule.iHealingFixed = 0
    zPackage.zHealingModule.iHealingPercent = 0.0
    
    --Lifesteal Module
    zPackage.bHasLifestealModule = false
    zPackage.zLifestealModule = {}
    zPackage.zLifestealModule.fStealPercent = 0.0
    zPackage.zLifestealModule.iLifestealLo = 0.0
    zPackage.zLifestealModule.iLifestealHi = 0.0
    
    --Crit Module
    zPackage.bHasCritModule = false
    zPackage.zCritModule = {}
    zPackage.zCritModule.iCritRate = 0
    zPackage.zCritModule.iAccuracyBonus = 0
    zPackage.zCritModule.iAccuracyMalus = 0
    zPackage.zCritModule.iCritThreshold = 100
    zPackage.zCritModule.fCritBonus = 1.25
    zPackage.zCritModule.iCritStun = 25
    
    --Effects
    zPackage.zaEffectModules = {}
    
    --Tags
    zPackage.saTags = {}
    
    --Additional Lines
    zPackage.zaAdditionalLines = {}
    
    --Finish.
    return zPackage
end

--Creates an effect module for the prediction package.
function fnCreateEffectModule()
    
    --Setup
    zModule = {}
    
    --Resolved Variable
    zModule.iEffectApplyRate = 0
    zModule.iUserAttackPower = 0
    zModule.iDamageTotal = 0
    
    --Chance-To-Apply
    zModule.bIsBuff = false
    zModule.iEffectType = gciDamageType_Slashing
    zModule.iEffectStrength = 0
    
    --DoT
    zModule.bIsDamageOverTime = false
    zModule.fDotAttackFactor = 1.0
    zModule.iDoTDamageType = gciDamageType_Slashing
    
    --Severity
    zModule.iSeverity = 0
    
    --Display
    zModule.sResultScript = ""
    zModule.saResultImages = {}
    
    --Tag Handling
    zModule.saApplyTagBonus = {}
    zModule.saApplyTagMalus = {}
    zModule.fApplyFactor = 10
    zModule.fApplyBonus = 0
    zModule.saSeverityTagBonus = {}
    zModule.saSeverityTagMalus = {}
    zModule.fSeverityFactor = 0.10
    zModule.fSeverityBonus = 1.00
    
    --Finish
    return zModule
end

-- |[ ================================== Build Prediction Box ================================== ]|
--Builds a prediction box for the provided package.
function fnBuildPredictionBox(piOriginatorID, piTargetID, piClusterID, pzPredictionPack)
    
    -- |[ ========== Argument Check ========== ]|
    if(piOriginatorID   == nil) then return end
    if(piTargetID       == nil) then return end
    if(piClusterID      == nil) then return end
    if(pzPredictionPack == nil) then return end
    
    --Get variables.
    local iEffectModules = #pzPredictionPack.zaEffectModules
    
    --Setup
    local iAlwaysHitTags  = 0
    local iNeverHitTags   = 0
    local iAlwaysCritTags = 0
    local iNeverCritTags  = 0
    
    --Tag Structure
    local zaTags = fnCreateTagStruct()
    
    -- |[ ========== Originator Variables ==========]|
    AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
    
        --[Tag Handling]
        --Special tags. Often ability specific.
        AdvCombat_SetProperty("Push Entity By ID", piTargetID) 
        
            --[Called Shot]
            --Florentina ability. Target is always-hit, always-crit. Counts as one always-hit and always-crit.
            local iCalledShotCount = AdvCombatEntity_GetProperty("Tag Count", "Florentina Called Shot")
            if(iCalledShotCount > 0) then
                
                --Check if the counter-tag exists.
                local bNoCalledShot = false
                for i = 1, #pzPredictionPack.saTags, 1 do
                    if(pzPredictionPack.saTags[i] == "Florentina No Called Shot") then
                        bNoCalledShot = true
                    end
                end
                
                if(bNoCalledShot == false) then
                    iAlwaysHitTags = iAlwaysHitTags + iCalledShotCount
                    iAlwaysCritTags = iAlwaysCritTags + iCalledShotCount
                end
            end
        DL_PopActiveObject()
    
        --Hit-rate Module
        if(pzPredictionPack.bHasHitRateModule) then
            pzPredictionPack.zHitRateModule.iAccuracyBonus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Accuracy)
            iAlwaysHitTags  = iAlwaysHitTags  + AdvCombatEntity_GetProperty("Tag Count", "Always Hit")
            iNeverHitTags   = iNeverHitTags   + AdvCombatEntity_GetProperty("Tag Count", "Never Hit")
            iAlwaysCritTags = iAlwaysCritTags + AdvCombatEntity_GetProperty("Tag Count", "Always Crit")
            iNeverCritTags  = iNeverCritTags  + AdvCombatEntity_GetProperty("Tag Count", "Never Crit")
        end
        
        --Damage Module
        if(pzPredictionPack.bHasDamageModule) then
            zaTags.iBleedDamageDealtUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt +")
            zaTags.iBleedDamageDealtDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt -")
        end
    
        --Critical Hit Module
        if(pzPredictionPack.bHasCritModule) then
            pzPredictionPack.zCritModule.iAccuracyBonus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Accuracy)
        end
    
        --Effect Modules. DoTs need to store the user's attack power.
        for i = 1, iEffectModules, 1 do
            if(pzPredictionPack.zaEffectModules[i].bIsDamageOverTime) then
                pzPredictionPack.zaEffectModules[i].iUserAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
            end
        end
    DL_PopActiveObject()
    
    -- |[========== Target Variables ========== ]|
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
    
        --Hit-rate Module
        if(pzPredictionPack.bHasHitRateModule) then
            pzPredictionPack.zHitRateModule.iAccuracyMalus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Evade)
        end
        
        --Damage Module
        if(pzPredictionPack.bHasDamageModule) then
            zaTags.iBleedDamageTakenUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken +")
            zaTags.iBleedDamageTakenDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken -")
        end
        
        --Stun Module
        if(pzPredictionPack.bHasStunModule) then
            --Tags here
        end
    
        --Healing Module
        if(pzPredictionPack.bHasHealingModule) then
            local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            pzPredictionPack.zHealingModule.iHealingFinal = pzPredictionPack.zHealingModule.iHealingFixed + math.floor(iHPMax * pzPredictionPack.zHealingModule.iHealingPercent)
            if(pzPredictionPack.zHealingModule.iHealingFinal < 1.0) then
                pzPredictionPack.bHasHealingModule = false
            end
        end
        
        --Critical Hit Module
        if(pzPredictionPack.bHasCritModule) then
            pzPredictionPack.zCritModule.iAccuracyMalus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Evade)
        end
    
        --Effect Modules.
        for i = 1, iEffectModules, 1 do
            
            --DoTs can compute damage with the target's resistance.
            if(pzPredictionPack.zaEffectModules[i].bIsDamageOverTime) then
                
                --Get resistance, compute total damage, store it.
                local iResistance = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start + pzPredictionPack.zaEffectModules[i].iDoTDamageType)
                local iAttackPower = pzPredictionPack.zaEffectModules[i].iUserAttackPower * pzPredictionPack.zaEffectModules[i].fDotAttackFactor
                pzPredictionPack.zaEffectModules[i].iDamageTotal = fnComputeCombatDamage(iAttackPower, 0, iResistance)
                
                --Modify by bleed bonus.
                if(pzPredictionPack.zaEffectModules[i].iDoTDamageType == gciDamageType_Bleeding) then
                    local fBonusPct = 0.01 * (zaTags.iBleedDamageTakenUpTags + zaTags.iBleedDamageDealtUpTags - zaTags.iBleedDamageTakenDnTags - zaTags.iBleedDamageDealtDnTags)
                    pzPredictionPack.zaEffectModules[i].iDamageTotal = math.floor(pzPredictionPack.zaEffectModules[i].iDamageTotal * (1.0 + fBonusPct))
                end
                
            end
            
            --Collect tag bonuses for chance-to-apply.
            local fApplyBonus = fnComputeTagBonus(pzPredictionPack.zaEffectModules[i].saApplyTagBonus, pzPredictionPack.zaEffectModules[i].fApplyFactor)
            local fApplyMalus = fnComputeTagBonus(pzPredictionPack.zaEffectModules[i].saApplyTagMalus, pzPredictionPack.zaEffectModules[i].fApplyFactor)
            pzPredictionPack.zaEffectModules[i].fApplyBonus = fApplyBonus - fApplyMalus
            
            --Collect tag bonuses for severity.
            local fSeverityBonus = fnComputeTagBonus(pzPredictionPack.zaEffectModules[i].saSeverityTagBonus, pzPredictionPack.zaEffectModules[i].fSeverityFactor)
            local fSeverityMalus = fnComputeTagBonus(pzPredictionPack.zaEffectModules[i].saSeverityTagMalus, pzPredictionPack.zaEffectModules[i].fSeverityFactor)
            pzPredictionPack.zaEffectModules[i].fSeverityBonus = 1.0 + fSeverityBonus - fSeverityMalus
            
        end
    DL_PopActiveObject()
    
    -- |[ ========== Creation ========== ]|
    --Create a box for this target.
    local sBoxName = "Prd" .. piClusterID .. "x" .. piTargetID
    AdvCombat_SetProperty("Prediction Create Box",  sBoxName)
    AdvCombat_SetProperty("Prediction Set Host",    sBoxName, piTargetID)
    AdvCombat_SetProperty("Prediction Set Offsets", sBoxName, 0, -100)
    
    -- |[ ========== Line Counting And Computations ========== ]|
    --Each module needs a line. Count them out here.
    local iTotalLines = 0
    
    --[Hit Rate]
    --Compute hit-rate if needed.
    if(pzPredictionPack.bHasHitRateModule == true) then
        
        --Compute.
        iTotalLines = iTotalLines + 1
        pzPredictionPack.zHitRateModule.iHitRate = fnComputeHitRate(pzPredictionPack.zHitRateModule.iAccuracyBonus, pzPredictionPack.zHitRateModule.iAccuracyMalus, pzPredictionPack.zHitRateModule.iMissRate)
        
        --Tag Handling.
        if(iAlwaysHitTags > iNeverHitTags) then
            pzPredictionPack.zHitRateModule.iHitRate = 100
        elseif(iNeverHitTags > iAlwaysHitTags) then
            pzPredictionPack.zHitRateModule.iHitRate = 0
        end
        
        --Special: Hit rate is zero, so show a special prediction box.
        if(pzPredictionPack.zHitRateModule.iHitRate < 1) then
            AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "[IMG0]Accuracy: Guaranteed Miss")
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Accuracy")
            return
        end
    end
    
    --[Damage]
    --Show damage if needed.
    if(pzPredictionPack.bHasDamageModule == true) then
        
        --Line add.
        iTotalLines = iTotalLines + 1
        
        --If we're flagged to use the weapon:
        if(pzPredictionPack.zDamageModule.bUseWeaponType == true) then
            fnBuildWeaponDamageIcons(piOriginatorID)
            local iLoDamage, iHiDamage = fnComputeDamageRangeWeapon(piOriginatorID, piTargetID, pzPredictionPack.zDamageModule.iScatterRange, zaTags)
            pzPredictionPack.zDamageModule.iDamageLo = iLoDamage
            pzPredictionPack.zDamageModule.iDamageHi = iHiDamage
        
        --Otherwise, single damage type:
        else
            pzPredictionPack.zDamageModule.sDamageIco[1] = fnGetIconByDamageType(pzPredictionPack.zDamageModule.iDamageType)
            local iLoDamage, iHiDamage = fnComputeDamageRangeTypeID(piOriginatorID, piTargetID, pzPredictionPack.zDamageModule.iDamageType, pzPredictionPack.zDamageModule.iScatterRange)
            pzPredictionPack.zDamageModule.iDamageLo = iLoDamage
            pzPredictionPack.zDamageModule.iDamageHi = iHiDamage
            
            --[Bleed]
            if(pzPredictionPack.zDamageModule.iDamageType == gciDamageType_Bleeding) then
                local fBonusPct = 0.01 * (zaTags.iBleedDamageTakenUpTags + zaTags.iBleedDamageDealtUpTags - zaTags.iBleedDamageTakenDnTags - zaTags.iBleedDamageDealtDnTags)
                pzPredictionPack.zDamageModule.iDamageLo = pzPredictionPack.zDamageModule.iDamageLo * (1.0 + fBonusPct)
                pzPredictionPack.zDamageModule.iDamageHi = pzPredictionPack.zDamageModule.iDamageHi * (1.0 + fBonusPct)
            end
        end
        
        --Modify by damage percentage.
        pzPredictionPack.zDamageModule.iDamageLo = math.floor(pzPredictionPack.zDamageModule.iDamageLo * pzPredictionPack.zDamageModule.fDamageFactor)
        pzPredictionPack.zDamageModule.iDamageHi = math.floor(pzPredictionPack.zDamageModule.iDamageHi * pzPredictionPack.zDamageModule.fDamageFactor)
        
    end

    --Stun Module
    if(pzPredictionPack.bHasStunModule) then
        iTotalLines = iTotalLines + 1
    end
    
    --[Healing]
    if(pzPredictionPack.bHasHealingModule == true) then
        iTotalLines = iTotalLines + 1
    end
    
    --[Critical Strike]
    --Compute critical strike if needed.
    if(pzPredictionPack.bHasCritModule == true) then
        
        --Compute.
        iTotalLines = iTotalLines + 1
        pzPredictionPack.zCritModule.iCritRate = fnComputeCritRate(pzPredictionPack.zCritModule.iAccuracyBonus, pzPredictionPack.zCritModule.iAccuracyMalus, pzPredictionPack.zCritModule.iCritThreshold)
        
        --Tag Handling.
        if(iAlwaysCritTags > iNeverCritTags) then
            pzPredictionPack.zCritModule.iCritRate = 100
        elseif(iNeverCritTags > iAlwaysCritTags) then
            pzPredictionPack.zCritModule.iCritRate = 0
        end
        
        --If the crit rate comes back as zero, this package doesn't register.
        if(pzPredictionPack.zCritModule.iCritRate < 1) then
            iTotalLines = iTotalLines - 1
            pzPredictionPack.bHasCritModule = false
        end
    end
    
    --[Effect Modules]
    --For each effect module...
    for i = 1, iEffectModules, 1 do
        iTotalLines = iTotalLines + 1
        pzPredictionPack.zaEffectModules[i].iEffectApplyRate = fnComputeEffectApplyRateID(piTargetID, pzPredictionPack.zaEffectModules[i].iEffectStrength, pzPredictionPack.zaEffectModules[i].iEffectType)
        pzPredictionPack.zaEffectModules[i].iEffectApplyRate = pzPredictionPack.zaEffectModules[i].iEffectApplyRate + pzPredictionPack.zaEffectModules[i].fApplyBonus
        
        --100% clamp:
        if(pzPredictionPack.zaEffectModules[i].iEffectApplyRate > 100) then pzPredictionPack.zaEffectModules[i].iEffectApplyRate = 100 end
        
        --If the apply rate is zero, remove the line.
        if(pzPredictionPack.zaEffectModules[i].iEffectApplyRate < 1) then
            iTotalLines = iTotalLines - 1
            pzPredictionPack.zaEffectModules[i].iEffectApplyRate = 0
        
        --If the effect is a damage-over-time, and the damage came out as zero, skip the line.
        elseif(pzPredictionPack.zaEffectModules[i].bIsDamageOverTime and pzPredictionPack.zaEffectModules[i].iDamageTotal < 1) then
            iTotalLines = iTotalLines - 1
            pzPredictionPack.zaEffectModules[i].iEffectApplyRate = 0
        end
    end
    
    --[Additional Lines]
    iTotalLines = iTotalLines + #pzPredictionPack.zaAdditionalLines
    
    -- |[ ========== Line Assembly ========== ]|
    --[Allocate Lines]
    if(iTotalLines < 1) then return end
    local i = 0
    AdvCombat_SetProperty("Prediction Allocate Strings", sBoxName, iTotalLines)
    
    --[Hit Rate]
    --Show the hit chance.
    if(pzPredictionPack.bHasHitRateModule == true) then
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, "[IMG0]Accuracy: " .. pzPredictionPack.zHitRateModule.iHitRate .. "%%")
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Accuracy")
        i = i + 1
    end
    
    --[Damage]
    --Show damage if needed.
    if(pzPredictionPack.bHasDamageModule == true) then
        
        --Build a string.
        local iMembers = #pzPredictionPack.zDamageModule.sDamageIco
        local sString = "[IMG0]Damage: " .. pzPredictionPack.zDamageModule.iDamageLo .. "-" .. pzPredictionPack.zDamageModule.iDamageHi
        
        --Totally immune to damage:
        if(pzPredictionPack.zDamageModule.iDamageLo == 0.0 and pzPredictionPack.zDamageModule.iDamageHi == 0.0) then
            sString = "[IMG0]Immune to: "
        end
            
            --Add on the icon spots for each member of the array.
            for p = 1, iMembers, 1 do
                sString = sString .. "[IMG" .. p .. "]"
            end
            
            --Now place the string and build the icons.
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, sString)
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1+iMembers)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
            for p = 1, iMembers, 1 do
                AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, p, gcfAbilityImgOffsetY, pzPredictionPack.zDamageModule.sDamageIco[p])
            end
        
        --Next.
        i = i + 1
    end

    --[Stun]
    --Stun Module
    if(pzPredictionPack.bHasStunModule) then
    
        --Compute expected stun damage.
        local iExpectedStun = fnComputeStunID(pzPredictionPack.zStunModule.iStunBase, piTargetID)
    
        --Lines.
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, "[IMG0]Stun: " .. iExpectedStun)
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Stun")
    
        --Next.
        i = i + 1
    end
    
    --[Healing]
    --Show healing if needed.
    if(pzPredictionPack.bHasHealingModule == true) then
        
        --Lines.
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, "[IMG0]Healing: " .. pzPredictionPack.zHealingModule.iHealingFinal)
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Health")
    
        --Next.
        i = i + 1
    end
    
    --[Lifesteal]
    if(pzPredictionPack.bHasLifestealModule and pzPredictionPack.bHasDamageModule == true) then
        pzPredictionPack.zLifestealModule.iLifestealLo = math.floor(pzPredictionPack.zDamageModule.iDamageLo * pzPredictionPack.zLifestealModule.fStealPercent)
        pzPredictionPack.zLifestealModule.iLifestealHi = math.floor(pzPredictionPack.zDamageModule.iDamageHi * pzPredictionPack.zLifestealModule.fStealPercent)
    end
    
    --[Critical Strike]
    --Show critical strike if it's over zero.
    if(pzPredictionPack.bHasCritModule == true) then
        
        --Common variables.
        local iCritRate = pzPredictionPack.zCritModule.iCritRate
        local fCritBonus = pzPredictionPack.zCritModule.fCritBonus
        local iCritStun = pzPredictionPack.zCritModule.iCritStun
        
        --Critical strike has no stun bonus:
        if(iCritStun < 1) then
            
            --Critical strike, but no damage base:
            if(pzPredictionPack.bHasDamageModule == false) then
            
            --Critical strike, has a damage module.
            else
                
                --Compute new damage range.
                local iLo = math.floor(pzPredictionPack.zDamageModule.iDamageLo * fCritBonus)
                local iHi = math.floor(pzPredictionPack.zDamageModule.iDamageHi * fCritBonus)
            
                --Set strings.
                AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, iCritRate .. "%% Critical. [IMG0]" .. iLo .. "-" .. iHi)
                AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
                AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
            end
        
        --Critical strike applies stun.
        else
            
            --Critical strike, but no damage base:
            if(pzPredictionPack.bHasDamageModule == false) then
            
            --Critical strike, has a damage module.
            else
                
                --Compute new damage range.
                local iLo = math.floor(pzPredictionPack.zDamageModule.iDamageLo * fCritBonus)
                local iHi = math.floor(pzPredictionPack.zDamageModule.iDamageHi * fCritBonus)
                
                --How much crit stun to apply.
                local iExpectedStun = fnComputeStunID(iCritStun, piTargetID)
                
                --If there is no base stun:
                if(pzPredictionPack.bHasStunModule == false) then
                    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, iCritRate .. "%% Critical. [IMG0]" .. iLo .. "-" .. iHi .. ". " .. iExpectedStun .. "[IMG1].")
                    AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 2)
                    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
                    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Stun")
                
                --Has a base stun, so "add" stun.
                else
                    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, iCritRate .. "%% Critical. [IMG0]" .. iLo .. "-" .. iHi .. ". +" .. iExpectedStun .. "[IMG1].")
                    AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 2)
                    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Attack")
                    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Stun")
                end
            end
        end
        i = i + 1
    end
    
    --[Effect Modules]
    --For each effect module...
    for p = 1, iEffectModules, 1 do
        
        --If the module has a non-zero application rate...
        if(pzPredictionPack.zaEffectModules[p].iEffectApplyRate > 0) then
            
            --Scan the string for the special sequence "[DAM]". If it is found, it needs to be replaced with the damage,
            -- even if the damage value is zero.
            --The %'s are escape sequences.
            local sUseString = pzPredictionPack.zaEffectModules[p].sResultScript
            local iIndexOfSequence, iDummy = string.find(sUseString, "[DAM]", 1, true)
            while(iIndexOfSequence ~= nil) do
                
                --Get the letters before and after the sequence.
                local sStringBefore = string.sub(sUseString, 1, iIndexOfSequence-1)
                local sStringAfter = string.sub(sUseString, iIndexOfSequence + 5)
                
                --Replace.
                sUseString = sStringBefore .. pzPredictionPack.zaEffectModules[p].iDamageTotal .. sStringAfter
                
                --Check if there's another sequence.
                iIndexOfSequence = string.find(sUseString, "[DAM]", 1, true)
            end
            
            --Scan the string for the sequence "[SEV]". If it found, it needs to be replaced with the severity, even if the value is zero.
            local iUseSeverity = pzPredictionPack.zaEffectModules[p].iSeverity * pzPredictionPack.zaEffectModules[p].fSeverityBonus
            iIndexOfSequence, iDummy = string.find(sUseString, "[SEV]", 1, true)
            while(iIndexOfSequence ~= nil) do
                
                --Get the letters before and after the sequence.
                local sStringBefore = string.sub(sUseString, 1, iIndexOfSequence-1)
                local sStringAfter = string.sub(sUseString, iIndexOfSequence + 5)
                
                --Replace.
                sUseString = sStringBefore .. iUseSeverity .. sStringAfter
                
                --Check if there's another sequence.
                iIndexOfSequence = string.find(sUseString, "[SEV]", 1, true)
            end
            
            --Common. Set images. The 0th image is always the EffectHostile/EffectFriendly image.
            local iImagesTotal = #pzPredictionPack.zaEffectModules[p].saResultImages
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1 + iImagesTotal)
            for x = 1, iImagesTotal, 1 do
                AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, x, gcfAbilityImgOffsetY, pzPredictionPack.zaEffectModules[p].saResultImages[x])
            end
            
            --Negative Effect / Debuff. Has a listed chance to apply.
            if(pzPredictionPack.zaEffectModules[p].bIsBuff == false) then
                AdvCombat_SetProperty("Prediction Set String Text",  sBoxName, i, "[IMG0]" .. pzPredictionPack.zaEffectModules[p].iEffectApplyRate .. "%% - " .. sUseString)
                AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/EffectIndicator/EffectHostile")
            
            --Positive Effect / Buff. Just shows the effect indicator, always applies.
            else
                AdvCombat_SetProperty("Prediction Set String Text",  sBoxName, i, "[IMG0]" .. sUseString)
                AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/EffectIndicator/EffectFriendly")
            end
            
            i = i + 1
        end
    end
    
    --[Additional Lines]
    --Optional extra lines added manually.
    for p = 1, #pzPredictionPack.zaAdditionalLines, 1 do
        
        local iImageCount = #pzPredictionPack.zaAdditionalLines[p].saSymbols
        
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, pzPredictionPack.zaAdditionalLines[p].sString)
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, iImageCount)
        for o = 1, iImageCount, 1 do
            AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, o-1, gcfAbilityImgOffsetY, pzPredictionPack.zaAdditionalLines[p].saSymbols[o])
        end
        i = i + 1
    end
    
end
