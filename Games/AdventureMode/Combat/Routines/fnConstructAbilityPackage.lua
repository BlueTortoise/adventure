--[ ====================================== Ability Package ====================================== ]
--Used for almost every ability, this contains the chance to hit/crit, animation, sound, etc.
gzaAbiPacks = {}
local function fnAddPack(psLookup, psAttackAnim, psAttackSound, psCriticalSound)
    local i = #gzaAbiPacks + 1
    gzaAbiPacks[i] = {}
    gzaAbiPacks[i].sLookup = psLookup
    gzaAbiPacks[i].sAttackAnim = psAttackAnim
    gzaAbiPacks[i].sAttackSound = psAttackSound
    gzaAbiPacks[i].sCriticalSound = psCriticalSound
end

--Build list.
fnAddPack("Bleed",       "Bleed",       "Combat\\|Impact_Bleed",      "Combat\\|Impact_Bleed_Crit")
fnAddPack("Blind",       "Blind",       "Combat\\|Impact_Pierce",     "Combat\\|Impact_Pierce_Crit")
fnAddPack("Buff",        "Buff",        "Combat\\|Impact_Buff",       "Combat\\|Impact_Buff")
fnAddPack("Claw Slash",  "Claw Slash",  "Combat\\|Impact_Slash",      "Combat\\|Impact_Slash_Crit")
fnAddPack("Corrode",     "Corrode",     "Combat\\|Impact_Corrosive",  "Combat\\|Impact_Corrosive_Crit")
fnAddPack("Debuff",      "Debuff",      "Combat\\|Impact_Debuff",     "Combat\\|Impact_Debuff")
fnAddPack("Flames",      "Flames",      "Combat\\|Impact_Flames",     "Combat\\|Impact_Flames_Crit")
fnAddPack("Freeze",      "Freeze",      "Combat\\|Impact_Ice",        "Combat\\|Impact_Ice_Crit")
fnAddPack("Gun Shot",    "Gun Shot",    "Combat\\|Impact_Laser",      "Combat\\|Impact_Laser_Crit")
fnAddPack("Haste",       "Haste",       "Combat\\|Impact_Buff",       "Combat\\|Impact_Buff")
fnAddPack("Healing",     "Healing",     "Combat\\|Heal",              "Combat\\|Heal")
fnAddPack("Laser Shot",  "Laser Shot",  "Combat\\|Impact_Laser",      "Combat\\|Impact_Laser_Crit")
fnAddPack("Light",       "Light",       "Combat\\|Impact_Laser",      "Combat\\|Impact_Laser_Crit")
fnAddPack("Pierce",      "Pierce",      "Combat\\|Impact_Pierce",     "Combat\\|Impact_Pierce_Crit")
fnAddPack("Poison",      "Poison",      "Combat\\|Impact_Bleed",      "Combat\\|Impact_Bleed_Crit")
fnAddPack("Shadow A",    "Shadow A",    "Combat\\|Impact_Corrosive",  "Combat\\|Impact_Corrosive_Crit")
fnAddPack("Shadow B",    "Shadow B",    "Combat\\|Impact_Corrosive",  "Combat\\|Impact_Corrosive_Crit")
fnAddPack("Shadow C",    "Shadow C",    "Combat\\|Impact_Corrosive",  "Combat\\|Impact_Corrosive_Crit")
fnAddPack("Shock",       "Shock",       "Combat\\|Impact_Slash",      "Combat\\|Impact_Slash_Crit")
fnAddPack("Slow",        "Slow",        "Combat\\|Impact_Debuff",     "Combat\\|Impact_Debuff")
fnAddPack("Strike",      "Strike",      "Combat\\|Impact_Strike",     "Combat\\|Impact_Strike_Crit")
fnAddPack("Sword Slash", "Sword Slash", "Combat\\|Impact_CrossSlash", "Combat\\|Impact_CrossSlash_Crit")
fnAddPack("Terrify",     "Terrify",     "Combat\\|Impact_Slash",      "Combat\\|Impact_Slash_Crit")
fnAddPack("JobChange",   "Null",        "Null",                       "Null")

--[Structure]
fnConstructDefaultAbilityPackage = function(psType)
    
    --Setup.
    zaPackage = {}
    
    --System
    zaPackage.iOriginatorID = 0
    
    --Special. Causes a black flash. Used when non-player entities attack, or player entities counter-attack.
    zaPackage.bCauseBlackFlash = false
    
    --Modules. Used for fnStandardExecution().
    zaPackage.bDoesNoDamage = false
    zaPackage.bNeverGlances = false
    zaPackage.bDoesNoStun = false
    zaPackage.bDoesNotAnimate = false
    zaPackage.bAlwaysHits = false
    zaPackage.bAlwaysCrits = false
    zaPackage.bNeverCrits = false
    zaPackage.bEffectAlwaysApplies = false
    zaPackage.bTargetCanBeKOd = false
    
    --Miss/Crit Threshold
    zaPackage.iMissThreshold = 0
    zaPackage.iCritThreshold = 100
    
    --Damage Scatter Range, Factor
    zaPackage.bUseWeapon = false
    zaPackage.bDealsOneDamageMinimum = true
    zaPackage.iDamageType = gciDamageType_Slashing
    zaPackage.fDamageFactor = 1.00
    zaPackage.fCriticalFactor = 1.25
    zaPackage.iCriticalStunDamage = 25
    zaPackage.iScatterRange = 15
    
    --Healing
    zaPackage.iHealingFinal = 0
    zaPackage.iHealingBase = 0
    zaPackage.fHealingFactor = 0.0
    
    --Self-healing. Heals the user, not the target. If target and user are the same, adds them.
    zaPackage.iSelfHealingFinal = 0
    zaPackage.iSelfHealingBase = 0
    zaPackage.fSelfHealingFactor = 0.0
    
    --Lifesteal. Adds to self-healing.
    zaPackage.fLifestealFactor = 0.0
    
    --Stun Application
    zaPackage.iBaseStunDamage = 0
    
    --Job Change
    zaPackage.sChangeJobTo = "Null"
    
    --Tags
    zaPackage.saTags = {}
    
    --Additional Paths
    zaPackage.saExecPaths = {}
    zaPackage.iaExecCodes = {}
    
    --Function
    zaPackage.fnAbilityAnimate = fnDefaultAbilityAnimate
    zaPackage.fnDamageAnimate  = fnDefaultDamageAnimate
    zaPackage.fnHealingAnimate = fnDefaultHealingAnimate
    zaPackage.fnStunAnimate    = fnDefaultStunAnimate
    zaPackage.fnJobAnimate     = fnDefaultJobAnimate
    
    --Set default animation/sound. If no matching type pack is found, the defaults are used.
    zaPackage.sAttackAnimation = gsDefaultAttackAnimation
    zaPackage.sAttackSound     = gsDefaultAttackSound
    zaPackage.sCriticalSound   = gsDefaultCriticalSound
    
    --Weapon-type. Does not populate since the weapon's type will override this later.
    if(psType == "Weapon") then
        zaPackage.bUseWeapon = true
    
    --Otherwise, iterate across the array of types.
    else
        for i = 1, #gzaAbiPacks, 1 do
            if(gzaAbiPacks[i].sLookup == psType) then
                zaPackage.sAttackAnimation = gzaAbiPacks[i].sAttackAnim
                zaPackage.sAttackSound     = gzaAbiPacks[i].sAttackSound
                zaPackage.sCriticalSound   = gzaAbiPacks[i].sCriticalSound
                break
            end
        end
    end
    
    --Done
    return zaPackage
end

--[Animation Application]
fnDefaultAbilityAnimate = function(piTargetID, piTimer, pzaAbilityPackage, pbIsCritical)
    
    --Critical strikes change the sound effect.
    if(pbIsCritical == false) then
        if(pzaAbilityPackage.sAttackSound ~= "Null") then
            AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaAbilityPackage.sAttackSound)
        end
    else
        if(pzaAbilityPackage.sCriticalSound ~= "Null") then
            AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaAbilityPackage.sCriticalSound)
        end
    end
    
    --Common.
    local iAnimTicks = 0
    if(pzaAbilityPackage.sAttackAnimation ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Create Animation|" .. pzaAbilityPackage.sAttackAnimation .. "|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming(pzaAbilityPackage.sAttackAnimation)
    end
    
    --Handle timer.
    piTimer = piTimer + iAnimTicks
    return piTimer
end

--[Damage Application]
fnDefaultDamageAnimate = function(piOriginatorID, piTargetID, piTimer, iDamage, sOptionalDamageArgs)
    
    --sOptionalDamageArgs can legally be nil.
    local sDamageString = "Damage|" .. iDamage
    if(sOptionalDamageArgs ~= nil) then
        sDamageString = sDamageString .. "|" .. sOptionalDamageArgs
    end
    
    --If the damage was zero, show text instead:
    if(iDamage < 1) then
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "Text|0")
        
    --Deal damage.
    else
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sDamageString)
    end
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

fnDefaultHealingAnimate = function(piOriginatorID, piTargetID, piTimer, piHealing)
    
    --sOptionalDamageArgs can legally be nil.
    local sHealingString = "Healing|" .. piHealing
    
    --Apply.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sHealingString)
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

--[Stun Application]
--Note: The stun damage is not the amount received, it's the final value. Clamp it before you send it here.
-- Stun does not require time to animate.
--The actual application of stun takes no time, but the text does. If the start and end values are the same,
-- no animation will occur.
fnDefaultStunAnimate = function(piOriginatorID, piTargetID, piTimer, piStunFinal)
    
    --Check the targets current stun. If it's the same as the destination stun, do nothing.
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
        local iStunCur = AdvCombatEntity_GetProperty("Stun")
    DL_PopActiveObject()
    if(iStunCur == piStunFinal) then return piTimer end
    
    --Otherwise, apply the stun.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "Stun|" .. piStunFinal)
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "Text|Stun +" .. piStunFinal-iStunCur .. "|Color:Yellow")
    piTimer = piTimer + gciApplication_TextTicks
    return piTimer
end

--[Job Change Application]
fnDefaultJobAnimate = function(piOriginatorID, piTargetID, piTimer, sJobToChangeTo)
    
    --Flash to white.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "FlashWhite")
    piTimer = piTimer + gciApplication_FlashWhiteTicks + gciApplication_FlashWhiteHold
    
    --Change jobs.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "JobChange|" .. sJobToChangeTo)
    piTimer = piTimer + gciApplication_FlashWhiteHold
    
    --Undo the flash.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "UnflashWhite")
    piTimer = piTimer + gciApplication_FlashWhiteTicks
    return piTimer
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end