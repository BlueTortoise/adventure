--[ =========================== Construct Default Application Package =========================== ]
--Returns a package that contains the default properties for an ability trying to apply a set of
-- applications. They also come with pre-built functions that can call normal application sets for
-- these routines, but they don't need to be used.
--There are a few subtypes for this function, for Miss/Hit/Effect/DoT/etc.

--[ ==================================== Application Cleanup ==================================== ]
--Default end-of-application set.
fnDefaultEndOfApplications = function(piTimer)
    
    --Add padding.
    piTimer = piTimer + gciApplication_EventPaddingTicks
    
    --Clamp. There is a minimum number of ticks every Event must occupy.
    if(piTimer < gciCombatTicks_StandardActionMinimum) then piTimer = gciCombatTicks_StandardActionMinimum end
    
    --Send to C++ state.
    AdvCombat_SetProperty("Event Timer", piTimer)
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end
