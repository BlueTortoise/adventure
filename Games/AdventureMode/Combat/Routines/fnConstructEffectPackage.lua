--[ ======================================= Effect Package ====================================== ]
--[Structure]
--Used for an effect that can be applied with a strength.
fnConstructDefaultEffectPackage = function()
    
    --Setup.
    zaPackage = {}
    
    --System
    zaPackage.iOriginatorID = 0
    
    --Strength
    zaPackage.iEffectStr     = 0
    zaPackage.iEffectCritStr = 0
    zaPackage.iEffectType    = gciDamageType_Slashing
    
    --Tags
    zaPackage.saApplyBonusTags = {}
    zaPackage.saApplyMalusTags = {}
    zaPackage.fApplyFactor = 1
    zaPackage.iTagApplyBonus = 0
    zaPackage.saSeverityBonusTags = {}
    zaPackage.saSeverityMalusTags = {}
    zaPackage.fSeverityFactor = 0.10
    zaPackage.fTagSeverityBonus = 0
    
    --Path to the effects.
    zaPackage.sEffectPath     = "Null"
    zaPackage.sEffectCritPath = "Null"
    
    --Successfully Applied Effect
    zaPackage.sApplyText      = "Applied"
    zaPackage.sApplyTextCol   = "Color:White"
    zaPackage.sApplyAnimation = "Null"
    zaPackage.sApplySound     = "Combat\\|Impact_Debuff"
    
    --Resisted Effect
    zaPackage.sResistText      = "Resisted"
    zaPackage.sResistTextCol   = "Color:White"
    zaPackage.sResistAnimation = "Null"
    zaPackage.sResistSound     = "Combat\\|AttackMiss"
    
    --Functions
    zaPackage.fnApplyEffect  = fnDefaultEffectAnimate
    zaPackage.fnResistEffect = fnDefaultResistAnimate
    zaPackage.fnHandler      = fnDefaultApplyOrResistSwitch
    
    --Done
    return zaPackage
end

--Applies the effect.
fnDefaultEffectAnimate = function(piOriginatorID, piTargetID, piTimer, pzaEffectPackage, pbIsCritical)

    --Effect string:
    local sEffectString = "Effect|" .. pzaEffectPackage.sEffectPath
    
    --Add originator and effect severity.
    sEffectString = sEffectString .. "|Originator:" .. piOriginatorID
    sEffectString = sEffectString .. "|Severity:" .. pzaEffectPackage.fTagSeverityBonus

    --Normal and Critical use different effects.
    if(pbIsCritical == false) then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, sEffectString)
    else
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, sEffectString)
    end
    
    --How many ticks to elapse. If none of the animations or text are set, no time is required.
    local iAnimTicks = 0
        
    --Animation.
    if(zaPackage.sApplyAnimation ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Create Animation|" .. pzaEffectPackage.sApplyAnimation .. "|EffectAnim0")
        iAnimTicks = gciApplication_TextTicks
    end
    
    --Sound effect.
    if(zaPackage.sApplySound ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaEffectPackage.sApplySound)
    end
    
    --Text.
    if(zaPackage.sApplyText ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Text|" .. pzaEffectPackage.sApplyText .. "|" .. pzaEffectPackage.sApplyTextCol)
        iAnimTicks = gciApplication_TextTicks
    end
    
    --Finish up.
    piTimer = piTimer + iAnimTicks
    return piTimer
end

--Resists the effect.
fnDefaultResistAnimate = function(piOriginatorID, piTargetID, piTimer, pzaEffectPackage)

    --How many ticks to elapse. If none of the animations or text are set, no time is required.
    local iAnimTicks = 0
        
    --Animation.
    if(zaPackage.sApplyAnimation ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Create Animation|" .. pzaEffectPackage.sResistAnimation .. "|EffectAnim0")
        iAnimTicks = gciApplication_TextTicks
    end
    
    --Sound effect.
    if(zaPackage.sApplySound ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaEffectPackage.sResistSound)
    end
    
    --Text.
    if(zaPackage.sApplyText ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Text|" .. pzaEffectPackage.sResistText .. "|" .. pzaEffectPackage.sResistTextCol)
        iAnimTicks = gciApplication_TextTicks
    end
    
    --Finish up.
    piTimer = piTimer + iAnimTicks
    return piTimer
end

--Handles apply/resist for the effect.
fnDefaultApplyOrResistSwitch = function(piOriginatorID, piTargetID, piTimer, pzaEffectPackage, pbApplied, pbIsCritical)
    if(pbApplied) then
        piTimer = pzaEffectPackage.fnApplyEffect(piOriginatorID, piTargetID, piTimer, pzaEffectPackage, pbIsCritical)
    else
        piTimer = pzaEffectPackage.fnResistEffect(piOriginatorID, piTargetID, piTimer, pzaEffectPackage)
    end
    return piTimer
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end