--[ ===================================== Create Tag Struct ===================================== ]
--Creates a table which holds an integer for every type of tag in the game. Modify it and pass it
-- between functions.
fnCreateTagStruct = function()
    local zaTagStruct = {}
    zaTagStruct.iBleedDamageTakenUpTags = 0
    zaTagStruct.iBleedDamageTakenDnTags = 0
    zaTagStruct.iBleedDamageDealtUpTags = 0
    zaTagStruct.iBleedDamageDealtDnTags = 0
    return zaTagStruct
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end