--[ =================================== Get Animation Timing ==================================== ]
--Returns how many ticks long the requested animation is. Returns 0 on error.
fnGetAnimationTiming = function(psAnimName)
    
    --Arg check.
    if(psAnimName == nil) then return 0 end
    
    --Scan.
    for i = 1, giAnimationTimingsTotal, 1 do
        if(gzaAnimationTimings[i].sName == psAnimName) then
            return gzaAnimationTimings[i].iTicks
        end
    end
    
    --Not found.
    return 0
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end
