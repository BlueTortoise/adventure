--[ ================================== Get Icon By Damage Type ================================== ]
--Returns the DLPath of the icon given the damage type, or "Null" if out of range.
function fnGetIconByDamageType(piDamageType)
    if(piDamageType == gciDamageType_Slashing)   then return "Root/Images/AdventureUI/DamageTypeIcons/Slashing" end
    if(piDamageType == gciDamageType_Piercing)   then return "Root/Images/AdventureUI/DamageTypeIcons/Piercing" end
    if(piDamageType == gciDamageType_Striking)   then return "Root/Images/AdventureUI/DamageTypeIcons/Striking" end
    if(piDamageType == gciDamageType_Flaming)    then return "Root/Images/AdventureUI/DamageTypeIcons/Flaming" end
    if(piDamageType == gciDamageType_Freezing)   then return "Root/Images/AdventureUI/DamageTypeIcons/Freezing" end
    if(piDamageType == gciDamageType_Shocking)   then return "Root/Images/AdventureUI/DamageTypeIcons/Shocking" end
    if(piDamageType == gciDamageType_Crusading)  then return "Root/Images/AdventureUI/DamageTypeIcons/Crusading" end
    if(piDamageType == gciDamageType_Obscuring)  then return "Root/Images/AdventureUI/DamageTypeIcons/Obscuring" end
    if(piDamageType == gciDamageType_Bleeding)   then return "Root/Images/AdventureUI/DamageTypeIcons/Bleeding" end
    if(piDamageType == gciDamageType_Poisoning)  then return "Root/Images/AdventureUI/DamageTypeIcons/Poisoning" end
    if(piDamageType == gciDamageType_Corroding)  then return "Root/Images/AdventureUI/DamageTypeIcons/Corroding" end
    if(piDamageType == gciDamageType_Terrifying) then return "Root/Images/AdventureUI/DamageTypeIcons/Terrifying" end
    return "Null"
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end
