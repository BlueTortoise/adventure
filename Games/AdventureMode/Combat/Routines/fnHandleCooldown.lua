--[ ===================================== Cooldown Handling ===================================== ]
--Modifies the cooldown value of the active AdvCombatAbility by 1. Returns the cooldown, which
-- clamps at 0. Pass 0 to simply query the cooldown.
function fnModifyCooldown(piToModify)
    
    --Arg check.
    if(piToModify == nil) then piToModify = 0 end
    
    --Query.
    local iCooldown = AdvCombatAbility_GetProperty("Cooldown")
    
    --Modify.
    iCooldown = iCooldown + piToModify
    
    --Clamp.
    if(iCooldown < 0) then iCooldown = 0 end
    
    --Upload new value, pass it back.
    AdvCombatAbility_SetProperty("Cooldown", iCooldown)
    return iCooldown
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end