--[ ===================================== Markdown Handlers ===================================== ]
--Markdown handler used for StarlightStrings that have images and text next to each other.
-- Replaces special [tags] with numbers or variables.
--Markdown handlers often use global lists like gsaStatIcons to make a standard reusable set of
-- aliases. Make sure you're using the correct function!

--This is the version of the markdown handler used for gsStandardStatPath. It contains the [VARN:##]
-- tag which allows usage of numeric variables and the [Dur] tag for effect duration.
--Returns the finalized string and an integer indicating how many remaps there are and a matching
-- table with the remap strings.
fnMarkdownHandlerStats = function(psString, piEffectID, piTargetID)
    
    --[Setup]
    --Debug.
    --io.write("Beginning Stat Markdown Handler.\n")
    --io.write(" Base : "..psString.."\n")
    
    --[Variables]
    --Duration. Often used for effects.
    local iDuration = VM_GetVar("Root/Variables/Combat/" .. piEffectID .. "/iDuration", "N")
    
    --This table will store the remaps needed.
    local iRemapsTotal = 0
    local saRemaps = {}
    
    --Final version of the string.
    local sFinalString = ""
    
    --Used for skipping letters when a tag gets replaced.
    local iSkips = 0
    
    --[Iteration]
    --Iterate across the string and build a list of images. These will need to be replaced. Duplicates are not a huge
    -- issue since these are small and temporary objects.
    local iLen = string.len(psString)
    for i = 1, iLen, 1 do
        
        --Store.
        local sLetter = string.sub(psString, i, i)
        
        --Skip this letter.
        if(iSkips > 0) then
            iSkips = iSkips - 1
        
        --Duration.
        elseif(string.sub(psString, i, i+4) == "[Dur]") then
            sFinalString = sFinalString .. iDuration
            iSkips = 4
        
        --Populate with a variable, expected to be a number.
        elseif(string.sub(psString, i, i+5) == "[VARN:") then
            
            --Scan ahead to locate the terminating brace.
            for p = i+6, iLen, 1 do
                local sSubLetter = string.sub(psString, p, p)
                if(sSubLetter == "]") then
                    
                    --Get the variable name.
                    local sVarName = string.sub(psString, i+6, p-1)
        
                    --Now get the variable from the DataLibrary.
                    local iValue = VM_GetVar("Root/Variables/Combat/" .. piEffectID .. "/" .. sVarName, "N")
                    sFinalString = sFinalString .. iValue
                    iSkips = p-i
                    break
                end
            end
        
        --Check for a stat remap.
        elseif(sLetter == "[") then
            
            --Iterate across the stat icons and look for a match.
            local bMatch = false
            for p = 1, giStatIconsTotal, 1 do
                local iRemapLen = string.len(gsaStatIcons[p][1]) - 1
                local sSubString = string.sub(psString, i, i+iRemapLen)
                if(sSubString == gsaStatIcons[p][1]) then
                    sFinalString = sFinalString .. "[IMG" .. iRemapsTotal .. "]"
                    saRemaps[iRemapsTotal] = gsaStatIcons[p][2]
                    
                    --Next. Skip the length of the tag.
                    iRemapsTotal = iRemapsTotal + 1
                    iSkips = iRemapLen
                    bMatch = true
                    break
                end
            end
            
            --No match. Append the brace.
            if(bMatch == false) then
                sFinalString = sFinalString .. sLetter
            end
    
        --Copy the letter.
        else
            sFinalString = sFinalString .. sLetter
        end
    end

    --[Finish Up]
    --Pass back the string, total remaps, and array of remap strings.
    return sFinalString, iRemapsTotal, saRemaps
        
end
