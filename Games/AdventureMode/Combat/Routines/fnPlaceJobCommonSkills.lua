--[ ================================== Place Job Common Skills ================================== ]
--Common code to replace two slots on the player interface with the equipped items, as well as 
-- Surrender/Retreat.
--Most characters have two equippable items and keep everything in the same slots. The AdvCombatJob
-- should be the active object.
function fnPlaceJobCommonSkills()
    
    --Equipment Change. Remove abilities, make sure there's a weapon in the named slot.
    if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Weapon Backup A") ~= "Null") then
        AdvCombatEntity_SetProperty("Set Ability Slot", 4, 0, "Common|WeaponSwapA")
    else
        AdvCombatEntity_SetProperty("Set Ability Slot", 4, 0, "Null")
    end
    if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Weapon Backup B") ~= "Null") then
        AdvCombatEntity_SetProperty("Set Ability Slot", 5, 0, "Common|WeaponSwapB")
    else
        AdvCombatEntity_SetProperty("Set Ability Slot", 5, 0, "Null")
    end
    
    --Item A
    AdvCombatEntity_SetProperty("Remove Ability", "Item|A")
    if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Item A") ~= "Null") then
        AdvCombatEntity_SetProperty("Push Item In Slot S", "Item A")
            local sAbilityPath = AdItem_GetProperty("Ability Path")
        DL_PopActiveObject()
        if(sAbilityPath ~= "Null") then
            LM_ExecuteScript(sAbilityPath, gciAbility_Create, "Item|A")
            AdvCombatEntity_SetProperty("Set Ability Slot", 6, 0, "Item|A")
        end
    end
    
    --Item B
    AdvCombatEntity_SetProperty("Remove Ability", "Item|B")
    if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Item B") ~= "Null") then
        AdvCombatEntity_SetProperty("Push Item In Slot S", "Item B")
            local sAbilityPath = AdItem_GetProperty("Ability Path")
        DL_PopActiveObject()
        if(sAbilityPath ~= "Null") then
            LM_ExecuteScript(sAbilityPath, gciAbility_Create, "Item|B")
            AdvCombatEntity_SetProperty("Set Ability Slot", 7, 0, "Item|B")
        end
    end
    
    --Common.
    AdvCombatEntity_SetProperty("Set Ability Slot", 8, 0, "System|Retreat")
    AdvCombatEntity_SetProperty("Set Ability Slot", 9, 0, "System|Surrender")
end

--Run common skill initializers if they need it.
function fnPlaceJobCommonSkillsCombatStart()
    
    if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Item A") ~= "Null") then
        
        --Push the item to get its path.
        AdvCombatEntity_SetProperty("Push Item In Slot S", "Item A")
            local sAbilityPath = AdItem_GetProperty("Ability Path")
        DL_PopActiveObject()
        
        --Path was not null. Push the ability spawned from the item and execute its startup.
        if(sAbilityPath ~= "Null") then
            AdvCombatEntity_SetProperty("Push Ability In Slot", 6, 0)
                LM_ExecuteScript(sAbilityPath, gciAbility_BeginCombat)
            DL_PopActiveObject()
        end
    end
    if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Item B") ~= "Null") then
        
        --Push the item to get its path.
        AdvCombatEntity_SetProperty("Push Item In Slot S", "Item B")
            local sAbilityPath = AdItem_GetProperty("Ability Path")
        DL_PopActiveObject()
        
        --Path was not null. Push the ability spawned from the item and execute its startup.
        if(sAbilityPath ~= "Null") then
            AdvCombatEntity_SetProperty("Push Ability In Slot", 7, 0)
                LM_ExecuteScript(sAbilityPath, gciAbility_BeginCombat)
            DL_PopActiveObject()
        end
    end
end

--Clears all job slots.
function fnClearJobSlots()
        
    --Clear 'standard' slots.
    for x = 0, gciAbilityGrid_XSize-1, 1 do
        AdvCombatEntity_SetProperty("Set Ability Slot", x, 0, "Null")
    end
    
    --Clear class-specific slots.
    for x = 0, gciAbilityGrid_Class_Abilities-1, 1 do
        AdvCombatEntity_SetProperty("Set Ability Slot", x, 1, "Null")
    end
    
    --Clear job-changes.
    for x = 0, gciAbilityGrid_XSize-1, 1 do
        for y = 0, gciAbilityGrid_YSize-1, 1 do
            AdvCombatEntity_SetProperty("Set Secondary Slot", x, y, "Null")
        end
    end
    
end

--Clears all skill slots. Usually used for special TFs or enemies.
function fnClearAllSlots()
    for x = 0, gciAbilityGrid_XSize-1, 1 do
        for y = 0, gciAbilityGrid_YSize-1, 1 do
            AdvCombatEntity_SetProperty("Set Ability Slot", x, y, "Null")
        end
    end
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end