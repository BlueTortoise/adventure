--[ ================================ Set Ability Response Flags ================================= ]
--Function that sets a standard set of ability response flags. Most abilities fall into a small set
-- of categories, like direct-action, DoT, passive, etc. You can always configure an ability manually.
--The AdvCombatAbility should be the active object.
--piStandardCode is one of the gciAbility_ResponseStandard_[x] series.
function fnSetAbilityResponseFlags(piStandardCode)
    
    --Arg check.
    if(piStandardCode == nil) then return end
    
    --Direct attack. Usually, the player selects the ability, then a target, then the ability executes.
    if(piStandardCode == gciAbility_ResponseStandard_DirectAction) then
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BeginAction,        true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BeginFreeAction,    true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_PaintTargets,       true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BuildPredictionBox, true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_Execute,            true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_QueryCanRun,        true)
    
    --Passive. Typically provides a status bonus.
    elseif(piStandardCode == gciAbility_ResponseStandard_Passive) then
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BeginCombat,    true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_GUIApplyEffect, true)
    
    --Counterattack. Activates events on response instead of active selection.
    elseif(piStandardCode == gciAbility_ResponseStandard_Counterattack) then
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BeginCombat,          true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BeginTurn,            true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_PaintTargetsResponse, true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_Execute,              true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_EventQueued,          true)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_QueryCanRun,          true)
    end
end
