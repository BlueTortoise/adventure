--[ =================================== Standard Begin Action =================================== ]
--Routine used for gciAbility_BeginAction calls from abilities. Flags the ability as usable or not
-- based on parameters.
function fnAbilityStandardBeginAction(piRequiredMP, pbRespectCooldown, piRequiredFreeActions, pbRespectActionCap)
    
    --By default, flag the ability as unusable. If anything returns out before the end, the ability
    -- remains unusable.
    AdvCombatAbility_SetProperty("Usable", false)
    
    --Decrement cooldown if present.
    local iCooldown = fnModifyCooldown(-1)
    
    --Argument Check
    if(piRequiredMP == nil)          then return end
    if(pbRespectCooldown == nil)     then return end
    if(piRequiredFreeActions == nil) then return end
    if(pbRespectActionCap == nil)    then return end
    
    --Required MP is nonzero. Check that.
    if(piRequiredMP > 0) then
        if(fnCheckMP(piRequiredMP) == false) then return end
    end
    
    --Cooldown is respected. Must be zero.
    if(pbRespectCooldown == true) then
        if(iCooldown > 0) then return end
    end
    
    --Ability requires a free action to be available.
    if(piRequiredFreeActions > 0) then
        AdvCombatAbility_SetProperty("Push Owner")
            local iFreeActionsNow   = AdvCombatEntity_GetProperty("Free Actions Available")
        DL_PopActiveObject()
        if(iFreeActionsNow < 1) then return end
    end
    
    --Must respect the actions cap, meaning so many free actions can be performed.
    if(pbRespectActionCap == true) then
        AdvCombatAbility_SetProperty("Push Owner")
            local iFreeActionsTotal = AdvCombatEntity_GetProperty("Free Actions Performed")
            local iFreeActionsCap   = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_FreeActionMax)
        DL_PopActiveObject()
        if(iFreeActionsTotal >= iFreeActionsCap) then return end
    end
    
    --All checks passed. Ability is usable.
    AdvCombatAbility_SetProperty("Usable", true)
end

--Identical to the above, but does not decrement the cooldown.
function fnAbilityStandardBeginFreeAction(piRequiredMP, pbRespectCooldown, piRequiredFreeActions, pbRespectActionCap)
    
    --By default, flag the ability as unusable. If anything returns out before the end, the ability
    -- remains unusable.
    AdvCombatAbility_SetProperty("Usable", false)
    
    --Decrement cooldown if present.
    local iCooldown = fnModifyCooldown(0)
    
    --Argument Check
    if(piRequiredMP == nil)          then return end
    if(pbRespectCooldown == nil)     then return end
    if(piRequiredFreeActions == nil) then return end
    if(pbRespectActionCap == nil)    then return end
    
    --Required MP is nonzero. Check that.
    if(piRequiredMP > 0) then
        if(fnCheckMP(piRequiredMP) == false) then return end
    end
    
    --Cooldown is respected. Must be zero.
    if(pbRespectCooldown == true) then
        if(iCooldown > 0) then return end
    end
    
    --Ability requires a free action to be available.
    if(piRequiredFreeActions > 0) then
        AdvCombatAbility_SetProperty("Push Owner")
            local iFreeActionsNow   = AdvCombatEntity_GetProperty("Free Actions Available")
        DL_PopActiveObject()
        if(iFreeActionsNow < 1) then return end
    end
    
    --Must respect the actions cap, meaning so many free actions can be performed.
    if(pbRespectActionCap == true) then
        AdvCombatAbility_SetProperty("Push Owner")
            local iFreeActionsTotal = AdvCombatEntity_GetProperty("Free Actions Performed")
            local iFreeActionsCap   = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_FreeActionMax)
        DL_PopActiveObject()
        if(iFreeActionsTotal >= iFreeActionsCap) then return end
    end
    
    --All checks passed. Ability is usable.
    AdvCombatAbility_SetProperty("Usable", true)
end