--[ ===================================== Standard Execution ==================================== ]
--"Standard" implementation of the gciAbility_Execute code for abilities. Requires the index of
-- the target, an ability package, and optional effect packages. The effect packages can be nil.
function fnStandardExecution(piOriginatorID, piTargetIndex, pzAbilityPack, pzEffectPack, pzEffectPackB, pzEffectPackC)
    
    --[ ========== Target Info ========== ]
    AdvCombat_SetProperty("Push Target", piTargetIndex) 
        local iTargetUniqueID = RO_GetID()
        local bIsNormalTarget = AdvCombatEntity_GetProperty("Is Normal Target")
    DL_PopActiveObject()
    
    --Store the timer offset.
    local iTimerOffset = giCombatTimerOffset
    giCombatTimerOffset = 0
    
    --Not a normal target. This can happen if the target was hit by another event before we get to this execution
    -- section, in which case they might be time-stopped or KO'd or somesuch.
    if(bIsNormalTarget == false) then
        
        --Target can be a non-normal target.
        if(pzAbilityPack.bTargetCanBeKOd == true) then
        
        --Checks failed, cannot execute.
        else
            return 
        end
    end
    
    --[ ========== Tag Handling ========== ]
    --Special tags. Often ability-specific.
    AdvCombat_SetProperty("Push Target", piTargetIndex) 
    
        --[Called Shot]
        --Florentina ability. Target is always-hit, always-crit. Effect that applied the tag will be removed.
        -- Abilities marked with "Florentina No Called Shot" ignore this.
        local iCalledShotCount = AdvCombatEntity_GetProperty("Tag Count", "Florentina Called Shot")
        if(iCalledShotCount > 0) then
            
            --Check if the counter-tag exists.
            local bNoCalledShot = false
            for i = 1, #pzAbilityPack.saTags, 1 do
                if(pzAbilityPack.saTags[i] == "Florentina No Called Shot") then
                    bNoCalledShot = true
                end
            end
            
            --The "No Called Shot" tag was found.
            if(bNoCalledShot == true) then
                --Debug
            
            --Apply.
            else
                pzAbilityPack.bAlwaysHits = true
                pzAbilityPack.bAlwaysCrits = true
                AdvCombatEntity_SetProperty("Remove Tag", "Florentina Called Shot", iCalledShotCount)
                
                --Consume the first effect found with the tag ID.
                local iZerothID = AdvCombatEntity_GetProperty("Effect ID With Tag", "Florentina Called Shot", 0)
                if(iZerothID ~= 0) then
                    AdvCombat_SetProperty("Remove Effect", iZerothID)
                end
            end
        end
    DL_PopActiveObject()
    
    --[ ========== Accuracy ========== ]
    --Use the subroutine. Populates globals.
    if(pzAbilityPack.bAlwaysHits == false) then
        fnStandardAccuracy(piOriginatorID, iTargetUniqueID, pzAbilityPack.iMissThreshold, pzAbilityPack.iCritThreshold)
        if(pzAbilityPack.bAlwaysCrits) then gbAttackCrit = true end
        if(pzAbilityPack.bNeverCrits) then gbAttackCrit = false end
        
        --If the attack was a glancing blow, but the ability never glances, it becomes a miss.
        if(pzAbilityPack.bNeverGlances and gbAttackGlances) then
            gbAttackHit = false
            gbAttackCrit = false
        
        --If the attack was a glancing blow, and it can glance, disable crits. If the always-crit flag is active, the attack still crits.
        elseif(gbAttackGlances) then
            gbAttackCrit = false
            if(pzAbilityPack.bAlwaysCrits) then gbAttackCrit = true end
        end
    
    --Attack always hits, save some time and don't run the subroutine. Ignoring accuracy means the ability only crits
    -- if explicitly flagged to.
    else
        gbAttackHit = true
        gbAttackCrit = pzAbilityPack.bAlwaysCrits
        if(pzAbilityPack.bNeverCrits) then gbAttackCrit = false end
    end
    
    --[ ========== Missed ========== ]
    --Attack missed.
    if(gbAttackHit == false) then
        
        --Setup.
        local iTimer = (gciAbility_TicksOffsetPerTarget * piTargetIndex)
        local zaMissPackage = fnConstructDefaultMissPackage()
        zaMissPackage.iOriginatorID = piOriginatorID
    
        --Black Flash. Only occurs if flagged and on the 0th target.
        if(pzAbilityPack.bCauseBlackFlash and piTargetIndex == 0) then
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piOriginatorID, piTimer, "Flash Black")
            iTimer = iTimer + gciAbility_BlackFlashTicks
        end
        
        --Ability strikes, then misses, then finishes up.
        --iTimer = pzAbilityPack.fnAbilityAnimate(iTargetUniqueID, iTimer, pzAbilityPack, false)
        iTimer = zaMissPackage.fnMissAnimate(iTargetUniqueID, iTimer, pzAbilityPack, zaMissPackage)
        fnDefaultEndOfApplications(iTimer)
        return
    end

    --[ ========== Damage Computation ========== ]
    --Compute damage using weapon damage.
    if(pzAbilityPack.bDoesNoDamage == false) then
        
        if(pzAbilityPack.bUseWeapon) then
            fnStandardAttack(piOriginatorID, iTargetUniqueID, pzAbilityPack.iScatterRange)
        else
            fnStandardDamageType(piOriginatorID, iTargetUniqueID, pzAbilityPack.iDamageType, pzAbilityPack.iScatterRange)
        end
        
        --Damage factor from this ability.
        giStandardDamage = giStandardDamage * pzAbilityPack.fDamageFactor
        
        --If the damage came back zero, and we're flagged to deal at least one damage, AND the target was not immune to at least
        -- one damage type used:
        if(giStandardDamage == 0 and pzAbilityPack.bDealsOneDamageMinimum and gbStandardImmune == false) then
            giStandardDamage = 1
        end

        --If the ability glanced, it does half damage.
        if(gbAttackGlances == true and giStandardDamage > 0) then
            giStandardDamage = math.floor(giStandardDamage * 0.5)
            if(giStandardDamage < 1) then giStandardDamage = 1 end

        --If the ability critted, multiply damage by the critical factor.
        elseif(gbAttackCrit == true and giStandardDamage > 0) then
            giStandardDamage = math.floor(giStandardDamage * pzAbilityPack.fCriticalFactor)
        end
    end
    
    --[ ========== Lifesteal Computation ========== ]
    --If the lifesteal is nonzero, and damage is dealt, add the lifesteal to the self-healing.
    if(pzAbilityPack.bDoesNoDamage == false and pzAbilityPack.fLifestealFactor > 0.0) then
        pzAbilityPack.iSelfHealingBase = pzAbilityPack.iHealingBase + math.floor(giStandardDamage * pzAbilityPack.fLifestealFactor)
    end
    
    --[ ========== Healing Computation ========== ]
    --If the target is the same as the user, then self-healing adds to the healing component.
    if(iTargetUniqueID == piOriginatorID) then
        pzAbilityPack.iHealingBase = pzAbilityPack.iHealingBase + pzAbilityPack.iSelfHealingBase
        pzAbilityPack.fHealingFactor = pzAbilityPack.fHealingFactor + pzAbilityPack.fSelfHealingFactor
        pzAbilityPack.iSelfHealingBase = 0
        pzAbilityPack.fSelfHealingFactor = 0.0
    
    --If the target and user are not the same, then self-healing compute here.
    else
    
        --Get user max HP.
        AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
            local iMaxHPUser = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        DL_PopActiveObject()
    
        --Set. Unlike healing, there is no clamp for self-healing, it can be zero.
        pzAbilityPack.iSelfHealingFinal = pzAbilityPack.iSelfHealingBase + math.floor(pzAbilityPack.fSelfHealingFactor * iMaxHPUser)
    
    end
    
    --Healing component. Calculate the final healing value.
    if(pzAbilityPack.iHealingBase ~= 0 or pzAbilityPack.fHealingFactor ~= 0.0) then
        AdvCombat_SetProperty("Push Target", piTargetIndex) 
            local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            pzAbilityPack.iHealingFinal = pzAbilityPack.iHealingBase + math.floor(iHPMax * pzAbilityPack.fHealingFactor)
        DL_PopActiveObject()
        if(pzAbilityPack.iHealingFinal < 1) then pzAbilityPack.iHealingFinal = 1 end
    end
    
    --[ ========== Stun Computation ========== ]
    local iStunToApply = 0
    local iFinalStun = 0
    if(pzAbilityPack.bDoesNoStun == false) then
        iStunToApply = pzAbilityPack.iBaseStunDamage
        if(gbAttackCrit == true) then iStunToApply = iStunToApply + pzAbilityPack.iCriticalStunDamage end
        iFinalStun = fnApplyStunID(iStunToApply, iTargetUniqueID)
    end
                
    --[Effect Computation]
    --Variables
    local bEffectApplied  = false
    local bEffectAppliedB = false
    local bEffectAppliedC = false
    
    --Check if the effect applied.
    if(pzEffectPack ~= nil) then
        
        --Bonus from tags
        AdvCombat_SetProperty("Push Target", piTargetIndex) 
            
            --Collect tag bonuses for chance-to-apply.
            local fApplyBonus = fnComputeTagBonus(pzEffectPack.saApplyBonusTags, pzEffectPack.fApplyFactor)
            local fApplyMalus = fnComputeTagBonus(pzEffectPack.saApplyMalusTags, pzEffectPack.fApplyFactor)
            pzEffectPack.iTagApplyBonus = fApplyBonus - fApplyMalus
            
            --Collect tag bonuses for severity.
            local fSeverityBonus = fnComputeTagBonus(pzEffectPack.saSeverityBonusTags, pzEffectPack.fSeverityFactor)
            local fSeverityMalus = fnComputeTagBonus(pzEffectPack.saSeverityMalusTags, pzEffectPack.fSeverityFactor)
            pzEffectPack.fTagSeverityBonus = 1.0 + fSeverityBonus - fSeverityMalus
        
        DL_PopActiveObject()
        
        --Effect needs to compute chance:
        if(pzAbilityPack.bEffectAlwaysApplies == false) then
            
            --Base
            local iEffectStr = pzEffectPack.iEffectStr
            if(gbAttackCrit) then iEffectStr = pzEffectPack.iEffectCritStr end
            
            --Check.
            bEffectApplied = fnStandardEffectApply(iTargetUniqueID, iEffectStr + pzEffectPack.iTagApplyBonus, pzEffectPack.iEffectType)
        
        --Always applies:
        else
            bEffectApplied = true
        end
    end
    
    --Package B.
    if(pzEffectPackB ~= nil) then
        
        --Bonus from tags
        AdvCombat_SetProperty("Push Target", piTargetIndex) 
            
            --Collect tag bonuses for chance-to-apply.
            local fApplyBonus = fnComputeTagBonus(pzEffectPackB.saApplyBonusTags, pzEffectPackB.fApplyFactor)
            local fApplyMalus = fnComputeTagBonus(pzEffectPackB.saApplyMalusTags, pzEffectPackB.fApplyFactor)
            pzEffectPackB.iTagApplyBonus = fApplyBonus - fApplyMalus
            
            --Collect tag bonuses for severity.
            local fSeverityBonus = fnComputeTagBonus(pzEffectPackB.saSeverityBonusTags, pzEffectPackB.fSeverityFactor)
            local fSeverityMalus = fnComputeTagBonus(pzEffectPackB.saSeverityMalusTags, pzEffectPackB.fSeverityFactor)
            pzEffectPackB.fTagSeverityBonus = 1.0 + fSeverityBonus - fSeverityMalus
        
        DL_PopActiveObject()
        
        --Effect needs to compute chance:
        if(pzAbilityPack.bEffectAlwaysApplies == false) then
            
            --Base
            local iEffectStr = pzEffectPackB.iEffectStr
            if(gbAttackCrit) then iEffectStr = pzEffectPackB.iEffectCritStr end
            
            --Check.
            bEffectAppliedB = fnStandardEffectApply(iTargetUniqueID, iEffectStr + pzEffectPackB.iTagApplyBonus, pzEffectPackB.iEffectType)
        
        --Always applies:
        else
            bEffectAppliedB = true
        end
    end
    
    --Package C.
    if(pzEffectPackC ~= nil) then
        
        --Bonus from tags
        AdvCombat_SetProperty("Push Target", piTargetIndex) 
            
            --Collect tag bonuses for chance-to-apply.
            local fApplyBonus = fnComputeTagBonus(pzEffectPackC.saApplyBonusTags, pzEffectPackC.fApplyFactor)
            local fApplyMalus = fnComputeTagBonus(pzEffectPackC.saApplyMalusTags, pzEffectPackC.fApplyFactor)
            pzEffectPackC.iTagApplyBonus = fApplyBonus - fApplyMalus
            
            --Collect tag bonuses for severity.
            local fSeverityBonus = fnComputeTagBonus(pzEffectPackC.saSeverityBonusTags, pzEffectPackC.fSeverityFactor)
            local fSeverityMalus = fnComputeTagBonus(pzEffectPackC.saSeverityMalusTags, pzEffectPackC.fSeverityFactor)
            pzEffectPackC.fTagSeverityBonus = 1.0 + fSeverityBonus - fSeverityMalus
        
        DL_PopActiveObject()
        
        --Effect needs to compute chance:
        if(pzAbilityPack.bEffectAlwaysApplies == false) then
            
            --Base
            local iEffectStr = pzEffectPackC.iEffectStr
            if(gbAttackCrit) then iEffectStr = pzEffectPackC.iEffectCritStr end
            
            --Check.
            bEffectAppliedC = fnStandardEffectApply(iTargetUniqueID, iEffectStr + pzEffectPackC.iTagApplyBonus, pzEffectPackC.iEffectType)
        
        --Always applies:
        else
            bEffectAppliedC = true
        end
    end
    
    --[ ========== Application Sequence ========== ]
    --Setup.
    local iTimer = (gciAbility_TicksOffsetPerTarget * piTargetIndex) + iTimerOffset
    
    --Black Flash. Only occurs if flagged and on the 0th target.
    if(pzAbilityPack.bCauseBlackFlash and piTargetIndex == 0) then
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piOriginatorID, piTimer, "Flash Black")
        iTimer = iTimer + gciAbility_BlackFlashTicks
    end
    
    --Animate the ability, if it exists.
    if(pzAbilityPack.bDoesNotAnimate == false) then
        iTimer = pzAbilityPack.fnAbilityAnimate(iTargetUniqueID, iTimer, pzAbilityPack, gbAttackCrit)
    end
    
    --If the ability deals damage, handle that here.
    if(pzAbilityPack.bDoesNoDamage == false) then
        
        --If the attack did zero damage, indicate immunity.
        if(giStandardDamage == 0) then
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, iTimer, "Play Sound|Combat\\|Glance")
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, iTimer, "Text|Immune!")
            iTimer = iTimer + gciApplication_TextTicks
        
        --Normal case:
        else
        
            local sExtraString = nil
            if(gbAttackGlances) then sExtraString = "Glance" end
            iTimer = pzAbilityPack.fnDamageAnimate(piOriginatorID, iTargetUniqueID, iTimer, giStandardDamage, sExtraString)
        end
    end
    
    --If the ability has healing, handle that here.
    if(pzAbilityPack.iHealingFinal > 0) then
        iTimer = pzAbilityPack.fnHealingAnimate(piOriginatorID, iTargetUniqueID, iTimer, pzAbilityPack.iHealingFinal)
    end
    
    --If the ability has self-healing, handle that here.
    if(pzAbilityPack.iSelfHealingFinal > 0) then
        iTimer = pzAbilityPack.fnHealingAnimate(piOriginatorID, piOriginatorID, iTimer, pzAbilityPack.iSelfHealingFinal)
    end
    
    --If the ability applies stun, do that here. Note that an ability that stuns on crit but has a base 0 will not
    -- animate stun unless the value was nonzero.
    if(pzAbilityPack.bDoesNoStun == false) then
        iTimer = pzAbilityPack.fnStunAnimate(piOriginatorID, iTargetUniqueID, iTimer, iFinalStun)
    end
    
    --If there is an effect, apply it.
    if(pzEffectPack ~= nil) then
        iTimer = pzEffectPack.fnHandler(piOriginatorID, iTargetUniqueID, iTimer, pzEffectPack, bEffectApplied, gbAttackCrit)
    end
    
    --If there is an effect, apply it.
    if(pzEffectPackB ~= nil) then
        iTimer = pzEffectPack.fnHandler(piOriginatorID, iTargetUniqueID, iTimer, pzEffectPackB, bEffectAppliedB, gbAttackCrit)
    end
    
    --If there is an effect, apply it.
    if(pzEffectPackC ~= nil) then
        iTimer = pzEffectPack.fnHandler(piOriginatorID, iTargetUniqueID, iTimer, pzEffectPackC, bEffectAppliedC, gbAttackCrit)
    end
    
    --Job Change
    if(pzAbilityPack.sChangeJobTo ~= "Null") then
        iTimer = pzAbilityPack.fnJobAnimate(piOriginatorID, iTargetUniqueID, iTimer, pzAbilityPack.sChangeJobTo)
    end
    
    --For each of the special execution cases, call a script.
    local iTotal = #pzAbilityPack.saExecPaths
    for i = 1, iTotal, 1 do
        
        --Resolve the code. If it's nil, it defaults to gciAbility_SpecialStart.
        local iCode = pzAbilityPack.iaExecCodes[i]
        if(iCode == nil) then iCode = gciAbility_SpecialStart end
        
        --Call.
        giStoredTimer = iTimer
        LM_ExecuteScript(pzAbilityPack.saExecPaths[i], iCode, piOriginatorID, iTargetUniqueID)
        iTimer = giStoredTimer
        
    end
    
    --Finish up.
    fnDefaultEndOfApplications(iTimer)
    
end