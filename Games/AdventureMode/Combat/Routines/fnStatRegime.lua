-- |[ ======================================= fnStatRegime ======================================== ]|
--Curve types.
gciCurveLinear = 0
gciCurveSquare = 1
gciCurveCubic = 2
gciCurveRevLinear = 3
gciCurveRevSquare = 4
gciCurveRevCubic = 5

--Function that is used for computing exactly what the value of a statistic is at a given level.
fnRunRegime = function(piaArray, piLevStart, piLevEnd, piGrowLo, piGrowHi, piCurveType)
    
    for i = piLevStart, piLevEnd, 1 do
        local fPercent = (i-piLevStart)/(piLevEnd-piLevStart)
        local iGrowRange = (piGrowHi - piGrowLo)
        local iGrowRate = 0
        
        --Resolve curve type.
        if(piCurveType == gciCurveLinear) then
            iGrowRate = piGrowLo + (iGrowRange * fPercent)
        elseif(piCurveType == gciCurveSquare) then
            iGrowRate = piGrowLo + (iGrowRange * fPercent * fPercent)
        elseif(piCurveType == gciCurveCubic) then
            iGrowRate = piGrowLo + (iGrowRange * fPercent * fPercent * fPercent)
        elseif(piCurveType == gciCurveRevLinear) then
            fPercent = 1.0 - fPercent
            iGrowRate = piGrowLo + (iGrowRange * fPercent)
        elseif(piCurveType == gciCurveRevSquare) then
            fPercent = 1.0 - fPercent
            iGrowRate = piGrowLo + (iGrowRange * fPercent * fPercent)
        elseif(piCurveType == gciCurveRevCubic) then
            fPercent = 1.0 - fPercent
            iGrowRate = piGrowLo + (iGrowRange * fPercent * fPercent * fPercent)
        end
        
        piaArray[i] = piaArray[i-1] + iGrowRate
    end
end

--Function that does standardized regime handling for a job. Uses a stat profile script.
fnStandardJobStatHandler = function(psScriptPath, piLevel)
    
    --[Argument Check]
    if(psScriptPath == nil) then return end
    if(piLevel      == nil) then return end
    
    --[Profile Script]
    --Call this script to populate the global arrays.
    LM_ExecuteScript(psScriptPath, -1)
    
    --[Sum And Upload]
    --Iterate across to the given level.
    local iHPBonus = 0
    local iAtkBonus = 0
    local iAccBonus = 0
    local iEvdBonus = 0
    local iIniBonus = 0
    for i = 0, piLevel, 1 do
        iHPBonus  = iHPBonus  + giaHPArray[i]
        iAtkBonus = iAtkBonus + giaAtkArray[i]
        iAccBonus = iAccBonus + giaAccArray[i]
        iEvdBonus = iEvdBonus + giaEvdArray[i]
        iIniBonus = iIniBonus + giaIniArray[i]
    end
    
    --[Catalysts]
    --Add catalyst bonuses.
    local iHpCatalysts  = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
    local iAtkCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
    local iAccCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
    local iEvdCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Dodge)
    local iIniCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
    iHPBonus  = iHPBonus  + (math.floor(iHpCatalysts  / gciCatalyst_Health_Needed)     * gciCatalyst_Health_Bonus)
    iAtkBonus = iAtkBonus + (math.floor(iAtkCatalysts / gciCatalyst_Attack_Needed)     * gciCatalyst_Attack_Bonus)
    iAccBonus = iAccBonus + (math.floor(iAccCatalysts / gciCatalyst_Accuracy_Needed)   * gciCatalyst_Accuracy_Bonus)
    iEvdBonus = iEvdBonus + (math.floor(iEvdCatalysts / gciCatalyst_Dodge_Needed)      * gciCatalyst_Dodge_Bonus)
    iIniBonus = iIniBonus + (math.floor(iIniCatalysts / gciCatalyst_Initiative_Needed) * gciCatalyst_Initiative_Bonus)

    --[Upload]
    --Final tally. Upload to the temp structure.
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_HPMax,      math.floor(iHPBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Attack,     math.floor(iAtkBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Accuracy,   math.floor(iAccBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Evade,      math.floor(iEvdBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Initiative, math.floor(iIniBonus))
    
end

--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end