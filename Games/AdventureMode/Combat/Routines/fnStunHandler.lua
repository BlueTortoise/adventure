--[ ======================================= Stun Handlers ======================================= ]
--Functions designed for stun control. These all expect an AdvCombatEntity to be atop the Activity Stack.

--[ ======================================== Action Begin ======================================= ]
--Called when an entity begins its action. Returns true if the entity is stunned, false if not.
-- Handles incrementing/decrementing stun values.
function fnStunHandleActionBegin()
    
    --[ ========== Setup ========== ]
    --By default, if the entity is not stunnable, ignore this.
    local bIsStunnable = AdvCombatEntity_GetProperty("Is Stunnable")
    if(bIsStunnable == false) then return end
    
    --Get the current stun values:
    local iStun = AdvCombatEntity_GetProperty("Stun")
    local iStunResist = AdvCombatEntity_GetProperty("Stun Resist")
    local iStunResistCounter = AdvCombatEntity_GetProperty("Stun Resist Counter")
    local iStunCap = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_StunCap)
    
    --Derived
    local iStunMax = iStunCap * gciStun_CapMaxFactor
    
    --[ ========== Currently Stunned ========== ]
    if(iStun >= iStunCap) then
    
        --Decrement the stun value by the stun cap.
        iStun = iStun - iStunCap
        
        --Increment the stun resist by 1, reset the timer to 0.
        iStunResist = iStunResist + 1
        iStunResistCounter = 0
    
        --Clamp. Stun resist never passes gciStun_ResistMax.
        if(iStunResist >= gciStun_ResistMax) then iStunResist = gciStun_ResistMax end
        
        --Clamp the stun values.
        iStun = fnStunClamp(iStun, iStunMax)
        
        --Upload data.
        AdvCombatEntity_SetProperty("Stun", iStun)
        AdvCombatEntity_SetProperty("Stun Resist", iStunResist)
        AdvCombatEntity_SetProperty("Stun Resist Timer", iStunResistCounter)
    
        --Return true to indicate the entity was stunned.
        return true
    end
    
    --[ ========== Not Currently Stunned ========== ]
    --Decrement the stun value by 1/gciStun_DecrementCapDivisor'th of the stun cap.
    iStun = iStun - (iStunCap / gciStun_DecrementCapDivisor)
        
    --Clamp the stun values.
    iStun = fnStunClamp(iStun, iStunMax)
    
    --Increment the stun-resist counter if stun resist is non-zero.
    if(iStunResist > 0) then
        iStunResistCounter = iStunResistCounter + 1
        
        --If the counter reaches gciStun_ResistTurnDecrement, decrement stun resist.
        if(iStunResistCounter >= gciStun_ResistTurnDecrement) then
            iStunResist = iStunResist - 1
            iStunResistCounter = 0
        end
    
    --Zero stun-resist means the counter is locked to zero.
    else
        iStunResist = 0
        iStunResistCounter = 0
    end
    
    --Upload data.
    AdvCombatEntity_SetProperty("Stun", iStun)
    AdvCombatEntity_SetProperty("Stun Resist", iStunResist)
    AdvCombatEntity_SetProperty("Stun Resist Timer", iStunResistCounter)
    
    --Return false, we're not stunned.
    return false
end

--[ ======================================= Compute Stun ======================================== ]
--Computes how much stun an entity will take.
function fnComputeStun(piStunCurrent, piStunMaximum, piStunToApply, piResistLevel)
    
    --Argument check.
    if(piStunCurrent == nil) then return 0 end
    if(piStunMaximum == nil) then return 0 end
    if(piStunToApply == nil) then return 0 end
    if(piResistLevel == nil) then return 0 end
    
    --Clamp the resist level.
    if(piResistLevel < gciStun_ResistLo) then piResistLevel = gciStun_ResistLo end
    if(piResistLevel > gciStun_ResistHi) then piResistLevel = gciStun_ResistHi end
    
    --Amount to apply is multiplied by resist level. If it goes below 1, stop here.
    piStunToApply = math.floor(piStunToApply * gciStun_ResistApply[piResistLevel])
    if(piStunToApply < 1.0) then return piStunCurrent end
    
    --Pass it back.
    return piStunToApply
end

--Requires a target ID to resolve the values needed.
function fnComputeStunID(piStunToApply, piTargetID)
    
    --Arg check.
    if(piStunToApply == nil) then return 0 end
    if(piTargetID    == nil) then return 0 end
    
    --Get properties.
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
        local iStunCur = AdvCombatEntity_GetProperty("Stun")
        local iStunResist = AdvCombatEntity_GetProperty("Stun Resist")
        local iStunCap = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_StunCap)
        local iStunMax = iStunCap * gciStun_CapMaxFactor
    DL_PopActiveObject()
    
    --Call primary routine.
    return fnComputeStun(iStunCur, iStunMax, piStunToApply, iStunResist)
end

--[ ========================================= Apply Stun ======================================== ]
--[Base Function]
--Given the current, max, to-apply, and resist level, computes and returns the final stun value. 
-- This includes clamping.
--Returns 0 on error.
function fnApplyStun(piStunCurrent, piStunMaximum, piStunToApply, piResistLevel)
    
    --Argument check.
    if(piStunCurrent == nil) then return 0 end
    if(piStunMaximum == nil) then return 0 end
    if(piStunToApply == nil) then return 0 end
    if(piResistLevel == nil) then return 0 end
    
    --Clamp the resist level.
    if(piResistLevel < gciStun_ResistLo) then piResistLevel = gciStun_ResistLo end
    if(piResistLevel > gciStun_ResistHi) then piResistLevel = gciStun_ResistHi end
    
    --Amount to apply is multiplied by resist level. If it goes below 1, stop here.
    piStunToApply = math.floor(piStunToApply * gciStun_ResistApply[piResistLevel])
    if(piStunToApply < 1.0) then return piStunCurrent end
    
    --Add the stun.
    piStunCurrent = piStunCurrent + piStunToApply
    
    --Clamp it against the maximum.
    fnStunClamp(piStunCurrent, piStunMaximum)
    
    --Pass it back.
    return piStunCurrent
end

--[Overload]
--Requires a target ID to resolve the values needed.
function fnApplyStunID(piStunToApply, piTargetID)
    
    --Arg check.
    if(piStunToApply == nil) then return 0 end
    if(piTargetID    == nil) then return 0 end
    
    --Get properties.
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
        local iStunCur = AdvCombatEntity_GetProperty("Stun")
        local iStunResist = AdvCombatEntity_GetProperty("Stun Resist")
        local iStunCap = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_StunCap)
        local iStunMax = iStunCap * gciStun_CapMaxFactor
    DL_PopActiveObject()
    
    --Call primary routine.
    return fnApplyStun(iStunCur, iStunMax, piStunToApply, iStunResist)
end

--[ ========================================= Stun Clamp ======================================== ]
--[Base Function]
--Returns the final stun value after clamping it, or 0 on error.
function fnStunClamp(piStun, piStunMax)
    
    --Arg check.
    if(piStun    == nil) then return 0 end
    if(piStunMax == nil) then return 0 end
    
    --Clamp, return.
    if(piStun <         0) then piStun =         0 end
    if(piStun > piStunMax) then piStun = piStunMax end
    return piStun
end

--[Overload]
--Given a target ID, returns the stun, clamped. Returns 0 on error.
function fnStunClampID(piStun, piTargetID)
    
    --Arg check.
    if(piStun     == nil) then return 0 end
    if(piTargetID == nil) then return 0 end
    
    --Get stun max.
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
        local iStunCap = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_StunCap)
        local iStunMax = iStunCap * gciStun_CapMaxFactor
    DL_PopActiveObject()
    
    --Return it.
    return fnStunClamp(piStun, iStunMax)
end

--[ ======================================= Miscellaneous ======================================= ]
--Add this to the global path set.
if(gbIsBuildingFunctionPaths == true) then
    local iIndex = #gsaFunctionPaths + 1
    gsaFunctionPaths[iIndex] = LM_GetCallStack(0)
end