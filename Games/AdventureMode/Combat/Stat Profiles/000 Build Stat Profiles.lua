-- |[ ==================================== Build Stat Profiles ==================================== ]|
--Used for the debug menu to compute stat profiles. This builds a list of which characters exist
-- and should have profiles available for their stats. It is called each time the profile menu is
-- opened, so you can add new characters on the fly.
local sBasePath = fnResolvePath()

-- |[Descriptions]|
--Individual jobs for individual characters have varying stats versus an average. The final of those
-- stats is what is usually compared between the two, as two jobs may have the same final value but
-- a different growth curve.
--The stats are scored according to this set of values
--==Lowest== Very Low, Low, Fairly Low, Below Average, Average, Above Average, Fairly High, High, Very High ==Highest==

--Most jobs "sum" to the same value of having average across the five varying stats, but not all do.
-- Some jobs may have lower stats but better resistances, or use abilities to make up for it.
--In addition, some characters have lower stats across the board for various reasons (like Jeanne).
--These are the comparison values for the stats:
--==Lowest==   VL|  LO|  FL|  BA| AVG|  AA|  FH|  HI|   VH ==Highest==
--Max HP    | 300| 400| 500| 575| 650| 725| 800| 950|1000+
--Attack    | 100| 130| 150| 170| 190| 220| 250| 280| 310+
--Accuracy  |  70|  80|  90| 110| 120| 130| 145| 160| 170+
--Evade     |  70|  80|  90| 110| 120| 130| 145| 160| 170+
--Initiative|  10|  15|  20|  25|  30|  35|  40|  45|  50

-- |[Lists]|
--Build profile lists to make it easy to edit them.
local saProfiles = {}
local fnAddProfile = function(psName, psPath)
    local i = #saProfiles
    saProfiles[i+1] = {}
    saProfiles[i+1].sName = psName
    saProfiles[i+1].sPath = psPath
end

-- |[Mei]|
--Job  | Fencer| Nightshade| Prowler| Smarty Sage| Hive Scout| Maid| Zombee
--HP   |    AVG|         BA|      FL|          FH|         BA|   FH|    AVG
--Atk  |     AA|        AVG|      HI|         AVG|        AVG|   BA|     FH
--Acc  |    AVG|        AVG|     AVG|         AVG|         AA|  AVG|     FH
--Evd  |     BA|         AA|      HI|          BA|        AVG|  AVG|     FH
--Ini  |    AVG|         AA|      HI|          BA|        AVG|   BA|    AVG
fnAddProfile("Mei Fencer",      sBasePath .. "Mei/Fencer.lua")
fnAddProfile("Mei Nightshade",  sBasePath .. "Mei/Nightshade.lua")
fnAddProfile("Mei Prowler",     sBasePath .. "Mei/Prowler.lua")
fnAddProfile("Mei Smarty Sage", sBasePath .. "Mei/Smarty Sage.lua")
fnAddProfile("Mei Hive Scout",  sBasePath .. "Mei/Hive Scout.lua")
fnAddProfile("Mei Maid",        sBasePath .. "Mei/Maid.lua")
fnAddProfile("Mei Zombee",      sBasePath .. "Mei/Zombee.lua")

-- |[Florentina]|
--Job  | Merchant| Mediator| Treasure Hunter
--HP   |       BA|       BA|             AVG
--Atk  |      AVG|      AVG|             AVG
--Acc  |       AA|      AVG|              AA
--Evd  |       AA|       AA|              BA
--Ini  |       AA|       AA|              AA
fnAddProfile("Florentina Merchant",        sBasePath .. "Florentina/Merchant.lua")
fnAddProfile("Florentina Mediator",        sBasePath .. "Florentina/Mediator.lua")
fnAddProfile("Florentina Treasure Hunter", sBasePath .. "Florentina/Treasure Hunter.lua")

-- |[ ======================================= Execution ======================================== ]|
--Allocate.
ADebug_SetProperty("Allocate Profiles", #saProfiles)

--Iterate and populate.
for i = 1, #saProfiles, 1 do
    ADebug_SetProperty("Set Profile Base", i-1, saProfiles[i].sName, saProfiles[i].sPath)
end
