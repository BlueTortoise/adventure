-- |[ ================================== Florentina Merchant =================================== ]|
--Statistical profile.
if(fnArgCheck(1) == false) then return end

--Slot.
local iSlot = LM_GetScriptArgument(0, "N")

--Zero off the first level. Don't do this if the slot value is -1, as these are the base values
-- for the job and those are set in the job script.
if(iSlot ~= -1) then
    giaHPArray  = {}
    giaAtkArray = {}
    giaAccArray = {}
    giaEvdArray = {}
    giaIniArray = {}

    giaHPArray[0] = 0
    giaAtkArray[0] = 0
    giaAccArray[0] = 0
    giaEvdArray[0] = 0
    giaIniArray[0] = 0
end

--[Description]
--HP:  BA
--Atk: AV
--Acc: AV
--Evd: AA
--Ini: AA

-- |[ ========================================= Max HP ========================================= ]|
fnRunRegime(giaHPArray,  1, 18, 14, 19, gciCurveRevSquare)
fnRunRegime(giaHPArray, 19, 35,  9, 11, gciCurveRevCubic)
fnRunRegime(giaHPArray, 36, 60,  3,  6, gciCurveLinear)
fnRunRegime(giaHPArray, 61, 99,  0,  3, gciCurveSquare)

-- |[ ====================================== Attack Power ====================================== ]|
fnRunRegime(giaAtkArray,  1, 16,  3, 7, gciCurveSquare)
fnRunRegime(giaAtkArray, 17, 35,  2, 5, gciCurveSquare)
fnRunRegime(giaAtkArray, 36, 67,  1, 3, gciCurveSquare)
fnRunRegime(giaAtkArray, 68, 99,  0, 1, gciCurveRevSquare)

-- |[ ======================================== Accuracy ======================================== ]|
fnRunRegime(giaAccArray,  1, 14, 2.0, 2.9, gciCurveSquare)
fnRunRegime(giaAccArray, 15, 34, 1.1, 1.6, gciCurveSquare)
fnRunRegime(giaAccArray, 35, 62, 1.4, 2.2, gciCurveCubic)
fnRunRegime(giaAccArray, 63, 72, 1.0, 1.2, gciCurveRevCubic)
fnRunRegime(giaAccArray, 73, 99, 0.2, 0.8, gciCurveRevCubic)

-- |[ ========================================= Evade ========================================== ]|
fnRunRegime(giaEvdArray,  1, 24, 2.1, 3.2, gciCurveRevSquare)
fnRunRegime(giaEvdArray, 25, 42, 1.5, 2.3, gciCurveSquare)
fnRunRegime(giaEvdArray, 43, 67, 1.0, 1.2, gciCurveSquare)
fnRunRegime(giaEvdArray, 68, 99, 0.3, 0.7, gciCurveLinear)

-- |[ ======================================= Initiative ======================================= ]|
fnRunRegime(giaIniArray,  1, 20, 0.3, 0.8, gciCurveRevSquare)
fnRunRegime(giaIniArray, 21, 46, 0.4, 0.7, gciCurveLinear)
fnRunRegime(giaIniArray, 47, 99, 0.2, 0.3, gciCurveSquare)

-- |[ ========================================= Report ========================================= ]|
--If the slot value is -1, this is being used to compute job statistics and not debug. Stop.
if(iSlot == -1) then return end

--Write.
io.write("Florentina - Mediator. Stat Report:\n")
io.write(" Max HP:     " .. math.floor(giaHPArray[99]) .. "\n")
io.write(" Attack:     " .. math.floor(giaAtkArray[99]) .. "\n")
io.write(" Accuracy:   " .. math.floor(giaAccArray[99]) .. "\n")
io.write(" Evade:      " .. math.floor(giaEvdArray[99]) .. "\n")
io.write(" Initiative: " .. math.floor(giaIniArray[99]) .. "\n")

-- |[ ====================================== Data Upload ======================================= ]|
--Iterate.
for i = 0, 99, 1 do
    ADebug_SetProperty("Set Profile Stats", iSlot, i, math.floor(giaHPArray[i]), math.floor(giaAtkArray[i]), math.floor(giaAccArray[i]), math.floor(giaEvdArray[i]), math.floor(giaIniArray[i]))
end