--[ ========================================= XP Table ========================================== ]
--Sets the XP table for chapter 1.
--io.write("Setting XP Table.\n")

--Base. Level 0 always requires 0 Exp.
local iMaxLevels = 100
AdvCombatEntity_SetProperty("Max Levels", iMaxLevels)
AdvCombatEntity_SetProperty("Exp For Level",  0,    0) --Always zero.

local iPrvExp = 0
local iCurExp = 32
local fExpFactor = 1.45
for i = 1, iMaxLevels-1, 1 do
    
    --Level requires previous + current:
    AdvCombatEntity_SetProperty("Exp For Level",  i, iPrvExp + iCurExp)
    --io.write(string.format("Lv: %3i is %9i, dif %8i. %5.2f\n", i, iPrvExp + iCurExp, iCurExp, fExpFactor))
    iPrvExp = iPrvExp + iCurExp
    
    --Amount of XP needed for the next level is multiplied by the factor.
    iCurExp = math.floor(iCurExp * fExpFactor)
    
    --Factor decays over time.
    fExpFactor = fExpFactor * 0.994
end

--Finish up.
AdvCombatEntity_SetProperty("Check Level Validity")