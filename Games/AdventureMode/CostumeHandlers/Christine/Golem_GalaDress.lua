--[Golem Gala Dress]
--Christine in her dress for the Sunrise Gala.

--[Variables]
--Flags.
VM_SetVar("Root/Variables/Global/Christine/iWearingGolemDress", "N", 1.0)
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S", "Gala")

--[Form Check]
--Sprite/Portrait/UI is only modified if Christine is actually a golem right now.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
if(sChristineForm ~= "Golem") then return end

--[Sprite]
if(EM_Exists("Christine") == true) then
	EM_PushEntity("Christine")
        --Base.
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_GolemDress/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Christine_Golem|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Christine_Golem|Crouch")
        TA_SetProperty("Add Special Frame", "Laugh0",  "Root/Images/Sprites/Special/Christine|Laugh0")
        TA_SetProperty("Add Special Frame", "Laugh1",  "Root/Images/Sprites/Special/Christine|Laugh1")
        TA_SetProperty("Add Special Frame", "Sad",     "Root/Images/Sprites/Special/Christine|Sad")
	DL_PopActiveObject()
end

--[Combat Portraits, UI Images]
AC_PushPartyMember("Christine")

    --Combat UI
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Christine_Golem")
	ACE_SetProperty("Card Portrait",  "Root/Images/AdventureUI/BigPortraits/Christine_Golem")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Christine_Golem_Dress")

    --Vendor UI
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_GolemDress/SW|0", 0)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_GolemDress/SW|1", 1)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_GolemDress/SW|2", 2)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_GolemDress/SW|3", 3)
    
    --Character Sprite Lookup
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == "Christine") then
            gsCharSprites[i][2] = "Root/Images/Sprites/Christine_GolemDress/SW|0"
            break
        end
    end

    --UI Positions.
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   141, 158)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -250, 256)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1082, 270)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -216, -46)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,  -120, 515)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 419, 488 + 45)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    -93,  66)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,      44, 122)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     775, 122)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  797,  77)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   394, 243)

DL_PopActiveObject()
	
--[Dialogue Portraits]
if(DialogueActor_Exists("Christine") == false) then return end
DialogueActor_Push("Christine")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/GolemGala|Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/GolemGala|Happy", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/GolemGala|Blush", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/GolemGala|Smirk", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/GolemGala|Sad", true)
    DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/GolemGala|Scared", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/GolemGala|Offended", true)
    DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/GolemGala|Serious", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/GolemGala|Cry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/GolemGala|Laugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/GolemGala|Angry", true)
    DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/GolemGala|PDU", true)
DL_PopActiveObject()
