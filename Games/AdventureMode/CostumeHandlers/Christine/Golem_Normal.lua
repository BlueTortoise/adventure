--[Golem Normal]
--Christine in golem form wearing her usual lord outfit.

--[Variables]
--Get variables.
local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")

--Flag.
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S", "Normal")

--[Form Check]
--Sprite/Portrait/UI is only modified if Christine is actually a golem right now.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
if(sChristineForm ~= "Golem") then return end

--[Sprite]
if(EM_Exists("Christine") == true) then
	EM_PushEntity("Christine")
        --Base.
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_Golem/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Christine_Golem|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Christine_Golem|Crouch")
        TA_SetProperty("Add Special Frame", "Laugh0",  "Root/Images/Sprites/Special/Christine|Laugh0")
        TA_SetProperty("Add Special Frame", "Laugh1",  "Root/Images/Sprites/Special/Christine|Laugh1")
        TA_SetProperty("Add Special Frame", "Sad",     "Root/Images/Sprites/Special/Christine|Sad")
	DL_PopActiveObject()
end

--[Combat Portraits, UI Images]
AC_PushPartyMember("Christine")

    --Combat UI
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Christine_Golem")
	ACE_SetProperty("Card Portrait",  "Root/Images/AdventureUI/BigPortraits/Christine_Golem")
    
    --Combat Field Portrait. Has Normal/Serious variations.
    if(iTalkedToSophie == 1.0) then
        ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Christine_Golem")
    
	--During the initial stages of the golem transformation, this portrait is used.
	else
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Christine_Golem_Forward")
	end

    --Vendor UI
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Golem/SW|0", 0)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Golem/SW|1", 1)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Golem/SW|2", 2)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Golem/SW|3", 3)
    
    --Character Sprite Lookup
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == "Christine") then
            gsCharSprites[i][2] = "Root/Images/Sprites/Christine_Golem/SW|0"
            break
        end
    end

    --UI Positions for "Normal" Christine.
	if(iTalkedToSophie == 1.0) then
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   150, 164)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -232, 245)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1108, 270)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -197, -55)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,  -104, 511)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 433, 493 + 45)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    -81,  54)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,      82,  89)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     811,  88)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  801,  76)
        ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   401, 240)
    
	--Forward-facing, uses a different rendering set. This is used after Christine becomes a golem and before speaking to Sophie.
	else
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   142, 166)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -248, 256)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1088, 256)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -203, -53)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,  -117, 515)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 425, 497 + 45)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    239, 108)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,      55, 126)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     783, 126)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  797,  80)
		ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   394, 244)
    end

DL_PopActiveObject()
	
--[Dialogue Portraits]
if(DialogueActor_Exists("Christine") == false) then return end
DialogueActor_Push("Christine")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/Golem|Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/Golem|Happy", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/Golem|Blush", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/Golem|Smirk", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/Golem|Sad", true)
    DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/Golem|Scared", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/Golem|Offended", true)
    DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/Golem|Serious", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/Golem|Cry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/Golem|Laugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/Golem|Angry", true)
    DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/Golem|PDU", true)
DL_PopActiveObject()
