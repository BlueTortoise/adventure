--[Human Nude]
--Christine in her nothing. Sprites are incomplete.

--[Variables]
--Flag.
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeHuman", "S", "Nude")

--[Form Check]
--Sprite/Portrait/UI is only modified if Christine is actually a human right now.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
if(sChristineForm ~= "Human") then return end

--[Sprite]
if(EM_Exists("Christine") == true) then
	EM_PushEntity("Christine")
        --Base
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_Human/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded",  "Root/Images/Sprites/Special/Christine_Human|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",   "Root/Images/Sprites/Special/Christine_Human|Crouch")
		TA_SetProperty("Add Special Frame", "BedSleep", "Root/Images/Sprites/Special/Christine_Human|BedSleep")
		TA_SetProperty("Add Special Frame", "BedWake",  "Root/Images/Sprites/Special/Christine_Human|BedWake")
		TA_SetProperty("Add Special Frame", "BedFull",  "Root/Images/Sprites/Special/Christine_Human|BedFull")
		TA_SetProperty("Add Special Frame", "BedWakeR", "Root/Images/Sprites/Special/Christine_Human|BedWakeR")
		TA_SetProperty("Add Special Frame", "BedFullR", "Root/Images/Sprites/Special/Christine_Human|BedFullR")
    DL_PopActiveObject()
end

--[Combat Portraits, UI Images]
AC_PushPartyMember("Christine")

    --Combat UI
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Christine_Human")
    ACE_SetProperty("Card Portrait",  "Root/Images/AdventureUI/BigPortraits/Christine_Human")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Christine_Human")

    --Vendor UI
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Human/SW|0", 0)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Human/SW|1", 1)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Human/SW|2", 2)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Human/SW|3", 3)
    
    --Character Sprite Lookup
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == "Christine") then
            gsCharSprites[i][2] = "Root/Images/Sprites/Christine_Human/SW|0"
            break
        end
    end

    --UI Positions
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   235, 169)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -147, 252)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1187, 252)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -117, -44)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,   -21, 510)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 520, 499 + 45)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,      3,  63)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,     165,  94)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     897,  95)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  843,  78)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   441, 245)

DL_PopActiveObject()
	
--[Dialogue Portraits]
if(DialogueActor_Exists("Christine") == false) then return end
DialogueActor_Push("Christine")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/HumanNude|Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/HumanNude|Blush", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/HumanNude|Happy", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/HumanNude|Smirk", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/HumanNude|Sad", true)
    DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/HumanNude|Scared", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/HumanNude|Offended", true)
    DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/HumanNude|Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/HumanNude|Cry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/HumanNude|Laugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/HumanNude|Angry", true)
    DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/HumanNude|PDU", true)
DL_PopActiveObject()
