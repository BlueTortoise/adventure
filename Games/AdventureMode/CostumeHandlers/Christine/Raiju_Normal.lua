--[Raiju Normal]
--Christine in her violet sweater, as a raiju.

--[Variables]
--Flags.
VM_SetVar("Root/Variables/Global/Christine/iIsRaijuClothed", "N", 1.0)
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeRaiju", "S", "Normal")

--[Form Check]
--Sprite/Portrait/UI is only modified if Christine is actually a golem right now.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
if(sChristineForm ~= "Raiju") then return end

--[Sprite]
if(EM_Exists("Christine") == true) then
	EM_PushEntity("Christine")
		--Base.
		fnSetCharacterGraphics("Root/Images/Sprites/Christine_RaijuClothes/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Christine_Raiju|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Christine_Raiju|Crouch")
	DL_PopActiveObject()
end

--[Combat Portraits, UI Images]
AC_PushPartyMember("Christine")

    --Combat UI
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Christine_Raiju")
    ACE_SetProperty("Card Portrait",  "Root/Images/AdventureUI/BigPortraits/Christine_Human")
	ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Christine_RaijuClothed")

    --Vendor UI
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_RaijuClothes/SW|0", 0)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_RaijuClothes/SW|1", 1)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_RaijuClothes/SW|2", 2)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_RaijuClothes/SW|3", 3)
    
    --Character Sprite Lookup
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == "Christine") then
            gsCharSprites[i][2] = "Root/Images/Sprites/Christine_RaijuClothes/SW|0"
            break
        end
    end

    --UI Positions
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   206, 133)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -174, 217)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1162, 219)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -146, -84)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,   -51, 472)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 489, 463 + 45)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    -29,  25)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,     135,  59)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     853,  59)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  830,  64)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   426, 227)

DL_PopActiveObject()
	
--[Dialogue Portraits]
if(DialogueActor_Exists("Christine") == false) then return end
DialogueActor_Push("Christine")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Blush", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Happy", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Smirk", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Sad", true)
    DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Scared", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Offended", true)
    DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Cry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Laugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Angry", true)
    DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/RaijuClothes|PDU", true)
DL_PopActiveObject()
