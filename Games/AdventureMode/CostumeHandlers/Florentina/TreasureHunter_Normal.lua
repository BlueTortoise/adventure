--[ ================================ Florentina, Alraune, Normal ================================ ]
--[Dialogue]
DialogueActor_Push("Florentina")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Neutral",  true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Happy",    true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Blush",    true)
    DialogueActor_SetProperty("Add Emotion", "Facepalm", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Facepalm", true)
    DialogueActor_SetProperty("Add Emotion", "Surprise", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Surprise", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Offended", true)
    DialogueActor_SetProperty("Add Emotion", "Confused", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Confused", true)
DL_PopActiveObject()

--[Sprite]
if(EM_Exists("Florentina") == true) then
	EM_PushEntity("Florentina")
	
		--Base.
		fnSetCharacterGraphics("Root/Images/Sprites/FlorentinaTH/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/FlorentinaTH|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/FlorentinaTH|Crouch")
		TA_SetProperty("Add Special Frame", "Sleep",   "Root/Images/Sprites/Special/Florentina|Sleep")
	DL_PopActiveObject()
end
        
--[Lua Sprite Lookup]
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == "Florentina") then
        gsCharSprites[i][2] = "Root/Images/Sprites/FlorentinaTH/SW|0"
        break
    end
end

--[Combat Entity]
if(AdvCombat_GetProperty("Does Party Member Exist", "Florentina") == true) then
    AdvCombat_SetProperty("Push Party Member", "Florentina")
    
        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",     "Root/Images/Portraits/Combat/Florentina_TreasureHunter")
        AdvCombatEntity_SetProperty("Combat Countermask",  "Null")
        AdvCombatEntity_SetProperty("Victory Countermask", "Root/Images/Portraits/Combat/FlorentinaCountermask")
        AdvCombatEntity_SetProperty("Turn Icon",           "Root/Images/AdventureUI/TurnPortraits/FlorentinaTH")
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, "Root/Images/Sprites/FlorentinaTH/SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, "Root/Images/Sprites/FlorentinaTH/SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, "Root/Images/Sprites/FlorentinaTH/SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, "Root/Images/Sprites/FlorentinaTH/SW|3")
	
        --UI Rendering Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   102, 135)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -259, 222)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1074, 222)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -226, -82)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,  -158, 477)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 384, 464 + 45)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,   -128,  28)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,      38,  63)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     769,  65)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  606,  83)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   374, 220)
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fIndexX = 0
        local fIndexY = 6
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
end
