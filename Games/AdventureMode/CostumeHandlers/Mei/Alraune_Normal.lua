--[ ===================================== Mei, Human, Normal ==================================== ]
--[Dialogue]
DialogueActor_Push("Mei")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/MeiDialogue/AlrauneNeutral", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/MeiDialogue/AlrauneNeutral", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/MeiDialogue/AlrauneHappy", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/MeiDialogue/AlrauneBlush", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/MeiDialogue/AlrauneSad", true)
    DialogueActor_SetProperty("Add Emotion", "Surprise", "Root/Images/Portraits/MeiDialogue/AlrauneSurprise", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/MeiDialogue/AlrauneOffended", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/MeiDialogue/AlrauneCry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/MeiDialogue/AlrauneLaugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/MeiDialogue/AlrauneAngry", true)
    DialogueActor_SetProperty("Add Emotion", "MC",       "Root/Images/Portraits/MeiDialogue/AlrauneMC", true)
DL_PopActiveObject()

--[Sprite]
if(EM_Exists("Mei") == true) then
	EM_PushEntity("Mei")
	
		--Base
		fnSetCharacterGraphics("Root/Images/Sprites/Mei_Alraune/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/MeiAlraune|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/MeiAlraune|Crouch")
	DL_PopActiveObject()
end
        
--[Lua Sprite Lookup]
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == "Mei") then
        gsCharSprites[i][2] = "Root/Images/Sprites/Mei_Alraune/SW|0"
        break
    end
end

--[Combat Entity]
if(AdvCombat_GetProperty("Does Party Member Exist", "Mei") == true) then
    AdvCombat_SetProperty("Push Party Member", "Mei")
    
        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    "Root/Images/Portraits/Combat/Mei_Alraune")
        AdvCombatEntity_SetProperty("Combat Countermask", "Null")
        AdvCombatEntity_SetProperty("Turn Icon",          "Root/Images/AdventureUI/TurnPortraits/Mei_Alraune")
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, "Root/Images/Sprites/Mei_Alraune/SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, "Root/Images/Sprites/Mei_Alraune/SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, "Root/Images/Sprites/Mei_Alraune/SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, "Root/Images/Sprites/Mei_Alraune/SW|3")
	
        --UI Rendering Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   208, 174)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -151, 257)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1186, 257)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -123, -43)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,   -45, 513)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 502, 499 + 45)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    -19,  61)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,     161,  95)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     893,  93)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  674,  99)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   428, 246)
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fIndexX = 1
        local fIndexY = 0
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
end
