--[ ====================================== Mei, Bee, Normal ===================================== ]
--If not in this form, just flag the variable.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
if(sMeiForm ~= "Bee") then
    VM_SetVar("Root/Variables/Costumes/Mei/sCostumeBee", "S", "Normal")
    return
end

--[Dialogue]
DialogueActor_Push("Mei")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/MeiDialogue/BeeNeutral", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/MeiDialogue/BeeSmirk", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/MeiDialogue/BeeHappy", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/MeiDialogue/BeeBlush", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/MeiDialogue/BeeSad", true)
    DialogueActor_SetProperty("Add Emotion", "Surprise", "Root/Images/Portraits/MeiDialogue/BeeSurprise", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/MeiDialogue/BeeOffended", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/MeiDialogue/BeeCry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/MeiDialogue/BeeLaugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/MeiDialogue/BeeAngry", true)
    DialogueActor_SetProperty("Add Emotion", "MC",       "Root/Images/Portraits/MeiDialogue/BeeMC", true)
DL_PopActiveObject()

--[Sprite]
if(EM_Exists("Mei") == true) then
	EM_PushEntity("Mei")
	
		--Base.
		fnSetCharacterGraphics("Root/Images/Sprites/Mei_Bee/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", true)
		TA_SetProperty("Y Oscillates", true)
		TA_SetProperty("No Footstep Sound", true)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/MeiBee|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/MeiBee|Crouch")
	DL_PopActiveObject()
end
        
--[Lua Sprite Lookup]
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == "Mei") then
        gsCharSprites[i][2] = "Root/Images/Sprites/Mei_Bee/SW|0"
        break
    end
end

--[Combat Portraits, UI Images]
if(AdvCombat_GetProperty("Does Party Member Exist", "Mei") == true) then
    AdvCombat_SetProperty("Push Party Member", "Mei")

        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    "Root/Images/Portraits/Combat/Mei_Bee")
        AdvCombatEntity_SetProperty("Combat Countermask", "Null")
        AdvCombatEntity_SetProperty("Turn Icon",          "Root/Images/AdventureUI/TurnPortraits/Mei_Bee")
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, "Root/Images/Sprites/Mei_Bee/SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, "Root/Images/Sprites/Mei_Bee/SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, "Root/Images/Sprites/Mei_Bee/SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, "Root/Images/Sprites/Mei_Bee/SW|3")
	
        --UI Rendering Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   126, 157)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -258, 244)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1101, 244)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -215, -64)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,  -141, 493)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 408, 485 + 45)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,   -108,  46)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,      64,  77)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     777,  84)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  612,  85)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   381, 238)
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fIndexX = 2
        local fIndexY = 0
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
end