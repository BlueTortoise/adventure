--[ ===================================== Mei, Slime, Normal ==================================== ]
--[Dialogue]
DialogueActor_Push("Mei")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/MeiDialogue/SlimeNeutral", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/MeiDialogue/SlimeNeutral", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/MeiDialogue/SlimeHappy", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/MeiDialogue/SlimeBlush", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/MeiDialogue/SlimeSad", true)
    DialogueActor_SetProperty("Add Emotion", "Surprise", "Root/Images/Portraits/MeiDialogue/SlimeSurprise", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/MeiDialogue/SlimeOffended", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/MeiDialogue/SlimeCry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/MeiDialogue/SlimeLaugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/MeiDialogue/SlimeAngry", true)
    DialogueActor_SetProperty("Add Emotion", "MC",       "Root/Images/Portraits/MeiDialogue/MC", true)
DL_PopActiveObject()

--[Sprite]
if(EM_Exists("Mei") == true) then
	EM_PushEntity("Mei")
	
		--Base
		fnSetCharacterGraphics("Root/Images/Sprites/Mei_Slime/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/MeiSlime|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/MeiSlime|Crouch")
	DL_PopActiveObject()
end
        
--[Lua Sprite Lookup]
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == "Mei") then
        gsCharSprites[i][2] = "Root/Images/Sprites/Mei_Slime/SW|0"
        break
    end
end

--[Combat Entity]
if(AdvCombat_GetProperty("Does Party Member Exist", "Mei") == true) then
    AdvCombat_SetProperty("Push Party Member", "Mei")
    
        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    "Root/Images/Portraits/Combat/Mei_Slime")
        AdvCombatEntity_SetProperty("Combat Countermask", "Null")
        AdvCombatEntity_SetProperty("Turn Icon",          "Root/Images/AdventureUI/TurnPortraits/Mei_Slime")
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, "Root/Images/Sprites/Mei_Slime/SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, "Root/Images/Sprites/Mei_Slime/SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, "Root/Images/Sprites/Mei_Slime/SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, "Root/Images/Sprites/Mei_Slime/SW|3")
	
        --UI Rendering Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   165, 172)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -195, 257)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1136, 257)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -168, -47)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,   -92, 519)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 449, 503 + 45)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    -62,  61)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,     112,  98)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     849,  92)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  635, 101)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   408, 246)
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fIndexX = 4
        local fIndexY = 0
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
end
