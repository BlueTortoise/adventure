--[Golem Gala Dress]
--Sophie in her lovely moth-themed gala dress.

--[Variables]
--Flag.
VM_SetVar("Root/Variables/Costumes/Sophie/sCostumeGolem", "S", "Gala")

--[Sprite]
if(EM_Exists("Sophie") == true) then
	EM_PushEntity("Sophie")
        fnSetCharacterGraphics("Root/Images/Sprites/SophieDress/", true)
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Cry0", "Root/Images/Sprites/Special/Sophie|Cry0")
        TA_SetProperty("Add Special Frame", "Cry1", "Root/Images/Sprites/Special/Sophie|Cry1")
	DL_PopActiveObject()
end

--[Combat Portraits, UI Images]
--Sophie is not a combat character.

--[Sprites]
--Character Sprite Lookup
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == "Sophie") then
        gsCharSprites[i][2] = "Root/Images/Sprites/SophieDress/SW|0"
        break
    end
end
	
--[Dialogue Portraits]
if(DialogueActor_Exists("Sophie") == false) then return end
DialogueActor_Push("Sophie")
    DialogueActor_SetProperty("Add Emotion", "Neutral",   "Root/Images/Portraits/SophieDialogueGala/Neutral",   true)
    DialogueActor_SetProperty("Add Emotion", "Happy",     "Root/Images/Portraits/SophieDialogueGala/Happy",     true)
    DialogueActor_SetProperty("Add Emotion", "Blush",     "Root/Images/Portraits/SophieDialogueGala/Blush",     true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",     "Root/Images/Portraits/SophieDialogueGala/Smirk",     true)
    DialogueActor_SetProperty("Add Emotion", "Sad",       "Root/Images/Portraits/SophieDialogueGala/Sad",       true)
    DialogueActor_SetProperty("Add Emotion", "Surprised", "Root/Images/Portraits/SophieDialogueGala/Surprised", true)
    DialogueActor_SetProperty("Add Emotion", "Offended",  "Root/Images/Portraits/SophieDialogueGala/Offended",  true)
DL_PopActiveObject()
