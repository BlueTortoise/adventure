--[ ====================================== Form Autoresolve ===================================== ]
--Used to help standardize form and costume handlers, such that the form handler just calls the costume handler
-- and we don't need to maintain multiple areas of code. This script, given a single string, calls the appropriate handler.
--With this script, switching forms automatically dresses in the appropriate costume.
--The string is always "Character_Form". Ex: "Christine_Human", "Mei_Alraune"
local sBasePath = fnResolvePath()
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sHandlerName = LM_GetScriptArgument(0)

--[ ========================================= Path Groupings ========================================= ]
--Only build this the first time.
if(gzaCostumeGroups == nil) then

    --[Setup]
    local iLastCreatedGroup = 0
    local iLastCreatedPath = 0
    gzaCostumeGroups = {}

    --[Grouping Creator Function]
    local fnAddCostumeGroup = function(sGroupName, sFormName, sCharFormPath, sCharCostumePath)

        --Add space.
        iLastCreatedPath = 0
        iLastCreatedGroup = iLastCreatedGroup + 1
        gzaCostumeGroups[iLastCreatedGroup] = {}

        --Fill.
        gzaCostumeGroups[iLastCreatedGroup].sHandlerName = sGroupName
        gzaCostumeGroups[iLastCreatedGroup].sFormName = sFormName
        gzaCostumeGroups[iLastCreatedGroup].sCharacterFormPath = sCharFormPath
        gzaCostumeGroups[iLastCreatedGroup].sCharacterCostumePath = sCharCostumePath
        gzaCostumeGroups[iLastCreatedGroup].zaKeyValues = {}
    end

    --[Path Creator Function]
    local fnAddCostumePath = function(sName, sPath)
        if(gzaCostumeGroups[iLastCreatedGroup] == nil) then return end
        
        --Add space.
        iLastCreatedPath = iLastCreatedPath + 1
        gzaCostumeGroups[iLastCreatedGroup].zaKeyValues[iLastCreatedPath] = {}
        
        --Fill.
        gzaCostumeGroups[iLastCreatedGroup].zaKeyValues[iLastCreatedPath].sName = sName
        gzaCostumeGroups[iLastCreatedGroup].zaKeyValues[iLastCreatedPath].sPath = sPath
    end

    --[ ======================================= Christine ======================================= ]
    --Christine, Human
    fnAddCostumeGroup("Christine_Human", "Human", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeHuman")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Human_Normal.lua")
    fnAddCostumePath("Nude",   sBasePath .. "Christine/Human_Nude.lua")

    --Christine, Golem
    fnAddCostumeGroup("Christine_Golem", "Golem", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeGolem")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Golem_Normal.lua")
    fnAddCostumePath("Gala",   sBasePath .. "Christine/Golem_GalaDress.lua")
    
    --Christine, Raiju
    fnAddCostumeGroup("Christine_Raiju", "Raiju", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeRaiju")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Raiju_Normal.lua")
    fnAddCostumePath("Nude",   sBasePath .. "Christine/Raiju_Nude.lua")
    

    --[ ========================================= Sophie ======================================== ]
    --Golem
    fnAddCostumeGroup("Sophie_Golem", "Golem", "TRUE", "Root/Variables/Costumes/Sophie/sCostumeGolem")
    fnAddCostumePath("Normal", sBasePath .. "Sophie/Golem_Normal.lua")
    fnAddCostumePath("Nude",   sBasePath .. "Sophie/Golem_GalaDress.lua")
    
    --[ ========================================= Mei ======================================== ]
    fnAddCostumeGroup("Mei_Human", "Human", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeHuman")
    fnAddCostumePath("Normal",   sBasePath .. "Mei/Human_Normal.lua")
    fnAddCostumePath("Mindless", sBasePath .. "Mei/Human_Mindless.lua")
    
    fnAddCostumeGroup("Mei_Alraune", "Alraune", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeAlraune")
    fnAddCostumePath("Normal",   sBasePath .. "Mei/Alraune_Normal.lua")
    fnAddCostumePath("Mindless", sBasePath .. "Mei/Alraune_Mindless.lua")
    
    fnAddCostumeGroup("Mei_Bee", "Bee", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeBee")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Bee_Normal.lua")
    fnAddCostumePath("Queen",  sBasePath .. "Mei/Bee_Queen.lua")
    
    fnAddCostumeGroup("Mei_Ghost", "Ghost", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeGhost")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Ghost_Normal.lua")
    
    fnAddCostumeGroup("Mei_Rubber", "Rubber", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeRubber")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Rubber_Normal.lua")
    
    fnAddCostumeGroup("Mei_Slime", "Slime", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeSlime")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Slime_Normal.lua")
    
    fnAddCostumeGroup("Mei_Werecat", "Werecat", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeWerecat")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Werecat_Normal.lua")
    
    fnAddCostumeGroup("Mei_Zombee", "Zombee", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeZombee")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Zombee_Normal.lua")
    
    --[ ======================================= Florentina ====================================== ]
    fnAddCostumeGroup("Florentina_Merchant", "Merchant", "Root/Variables/Global/Florentina/sCurrentJob", "Root/Variables/Costumes/Florentina/sCostumeAlraune")
    fnAddCostumePath("Normal", sBasePath .. "Florentina/Merchant_Normal.lua")
    
    fnAddCostumeGroup("Florentina_TreasureHunter", "TreasureHunter", "Root/Variables/Global/Florentina/sCurrentJob", "Root/Variables/Costumes/Florentina/sCostumeAlraune")
    fnAddCostumePath("Normal", sBasePath .. "Florentina/TreasureHunter_Normal.lua")
    
    fnAddCostumeGroup("Florentina_Mediator", "Mediator", "Root/Variables/Global/Florentina/sCurrentJob", "Root/Variables/Costumes/Florentina/sCostumeAlraune")
    fnAddCostumePath("Normal", sBasePath .. "Florentina/Mediator_Normal.lua")
end

--[ ========================================= Execution ========================================= ]
local bFoundMatch = false
local iTotalGroups = #gzaCostumeGroups
for i = 1, iTotalGroups, 1 do
    
    --Match:
    if(gzaCostumeGroups[i].sHandlerName == sHandlerName) then
    
        --Get the form. It needs to match.
        local sCurrentForm = VM_GetVar(gzaCostumeGroups[i].sCharacterFormPath, "S")
        if(gzaCostumeGroups[i].sCharacterFormPath == "TRUE" or sCurrentForm == gzaCostumeGroups[i].sFormName) then
    
            --Get the current costume.
            local sCurrentCostume = VM_GetVar(gzaCostumeGroups[i].sCharacterCostumePath, "S")
    
            --Scan for matches.
            local iScanCostumes = #gzaCostumeGroups[i].zaKeyValues
            for p = 1, iScanCostumes, 1 do
                
                --Match found!
                if(sCurrentCostume == gzaCostumeGroups[i].zaKeyValues[p].sName) then
                    bFoundMatch = true
                    LM_ExecuteScript(gzaCostumeGroups[i].zaKeyValues[p].sPath)
                    break
                end
            end
        end
        
        --Stop iterating on a handler match.
        break
    end
end

--[=[

--Human.
if(sHandlerName == "Christine_Human") then
    
    --Only actually runs if Christine is currently a human.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm ~= "Human") then return end
    
    --Variables.
    local sCostumeHuman = VM_GetVar("Root/Variables/Costumes/Christine/sCostumeHuman", "S")
    
    --Normal:
    if(sCostumeHuman == "Normal") then
        LM_ExecuteScript(sBasePath .. "Christine/Human_Normal.lua")
    --Nude:
    elseif(sCostumeHuman == "Nude") then
        LM_ExecuteScript(sBasePath .. "Christine/Human_Nude.lua")
    end

--Golem.
elseif(sHandlerName == "Christine_Golem") then
    
    --Only actually runs if Christine is currently a golem.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm ~= "Golem") then return end
    
    --Variables.
    local sCostumeGolem = VM_GetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S")
    
    --Normal:
    if(sCostumeGolem == "Normal") then
        LM_ExecuteScript(sBasePath .. "Christine/Golem_Normal.lua")
    --Gala Dress:
    elseif(sCostumeGolem == "Gala") then
        LM_ExecuteScript(sBasePath .. "Christine/Golem_GalaDress.lua")
    end

--Raiju.
elseif(sHandlerName == "Christine_Raiju") then
    
    --Only actually runs if Christine is currently a raiju.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm ~= "Raiju") then return end
    
    --Variables.
    local sCostumeRaiju = VM_GetVar("Root/Variables/Costumes/Christine/sCostumeRaiju", "S")
    
    --Normal:
    if(sCostumeRaiju == "Normal") then
        LM_ExecuteScript(sBasePath .. "Christine/Raiju_Normal.lua")
    --Nude:
    elseif(sCostumeRaiju == "Nude") then
        LM_ExecuteScript(sBasePath .. "Christine/Raiju_Nude.lua")
    end

--[ =========================================== Sophie ========================================== ]
--Golem, her only form.
elseif(sHandlerName == "Sophie_Golem") then
    
    --Variables.
    local sCostumeGolem = VM_GetVar("Root/Variables/Costumes/Sophie/sCostumeGolem", "S")
    
    --Normal:
    if(sCostumeGolem == "Normal") then
        LM_ExecuteScript(sBasePath .. "Sophie/Golem_Normal.lua")
    --Gala Dress:
    elseif(sCostumeGolem == "Gala") then
        LM_ExecuteScript(sBasePath .. "Sophie/Golem_GalaDress.lua")
    end


--[ =========================================== Mei ========================================== ]
--Bee.
elseif(sHandlerName == "Mei_Bee") then
    
    --Variables.
    local sCostumeGolem = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeBee", "S")
    
    --Normal:
    if(sCostumeGolem == "Normal") then
        LM_ExecuteScript(sBasePath .. "Mei/Bee_Normal.lua")
    --Royalty!
    elseif(sCostumeGolem == "Queen") then
        LM_ExecuteScript(sBasePath .. "Mei/Bee_Queen.lua")
    end
    
end
]=]