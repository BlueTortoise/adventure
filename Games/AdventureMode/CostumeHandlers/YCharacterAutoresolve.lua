--[ ================================ Character Costume Resolver ================================= ]
--Used to help standardize form and costume handlers, simply pass in the character's name and the
-- correct job will be resolved along with the correct costume.
--Why isn't a given job simply mapped directly to a costume? Some jobs (such as 55's algorithms)
-- all share the same form, so this script maps jobs to form to costume.
local sBasePath = fnResolvePath()
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sCharacterName = LM_GetScriptArgument(0)

--Setup.
local sCharacterFinalName = "Mei"
local sCharacterFinalJob = "Human"

--[ ================================= Resolve Character And Job ================================= ]
--[Chapter 1]
if(sCharacterName == "Mei") then
    
    --Set internal name, get job.
    sCharacterFinalName = "Mei"
    AdvCombat_SetProperty("Push Party Member", "Mei")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Fencer") then
        sCharacterFinalJob = "Human"
    elseif(sCurrentJob == "Nightshade") then
        sCharacterFinalJob = "Alraune"
    elseif(sCurrentJob == "Hive Scout") then
        sCharacterFinalJob = "Bee"
    elseif(sCurrentJob == "Maid") then
        sCharacterFinalJob = "Ghost"
    elseif(sCurrentJob == "Squeaky Thrall") then
        sCharacterFinalJob = "Rubber"
    elseif(sCurrentJob == "Smarty Sage") then
        sCharacterFinalJob = "Slime"
    elseif(sCurrentJob == "Prowler") then
        sCharacterFinalJob = "Werecat"
    elseif(sCurrentJob == "Zombee") then
        sCharacterFinalJob = "Zombee"
    end
    
elseif(sCharacterName == "Florentina") then
    
    --Set internal name, get job.
    sCharacterFinalName = "Florentina"
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Merchant") then
        sCharacterFinalJob = "Human"
    elseif(sCurrentJob == "Treasure Hunter") then
        sCharacterFinalJob = "TreasureHunter"
    elseif(sCurrentJob == "Mediator") then
        sCharacterFinalJob = "Mediator"
    end
    
--[Chapter 5]
elseif(sCharacterName == "Sophie") then
    sCharacterFinalName = "Sophie"
    sCharacterFinalJob = "Golem"
end

--[ ========================================= Finish Up ========================================= ]
--Fire costume autoresolve.
LM_ExecuteScript(gsCostumeAutoresolve, sCharacterFinalName .. "_" .. sCharacterFinalJob)