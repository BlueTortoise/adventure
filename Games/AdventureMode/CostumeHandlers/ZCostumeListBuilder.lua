--[ ==================================== Costume Routing File =================================== ]
--Whenever the costumes option on the campfire menu is selected, this file runs and builds a list
-- of costumes for available characters.
--Note that the characters don't have to be in the party, necessarily. For example, and for no
-- obvious reason, Unit 2856 has a nude costume. The script checks for chapter and usually quest
-- variables to determine who should be on the list.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

--[Password Case]
--If this script is called with an argument, it is a password attempting to unlock a costume.
local sPassword = "Null"
local iArgs = LM_GetNumOfArgs()
if(iArgs == 1) then
    sPassword = LM_GetScriptArgument(0)
end

--[ ========================================= Chapter 1 ========================================= ]
if(iCurrentChapter == 1.0) then
    
    --[Password]
    --Passwords for chapter 1.
    if(sPassword == "Carry Fire") then
        VM_SetVar("Root/Variables/Costumes/Mei/iQueenBee", "N", 1.0)
        AM_SetProperty("Flag Handled Password")
        return
    elseif(sPassword ~= "Null") then
        return
    end

    --[Mei]
    --Forms.
    local iHasBeeForm  = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
    
    --Unlocked Costumes.
    local iMei_QueenBee   = VM_GetVar("Root/Variables/Costumes/Mei/iQueenBee", "N")
    
    --Bee.
    if(iHasBeeForm == 1.0) then
        AM_SetProperty("Register Costume Heading", "MeiBee", "Mei", "Bee", "Root/Images/Sprites/Mei_Bee/South|0")
        AM_SetProperty("Register Costume Entry",   "MeiBee", "Normal", "Root/Images/Sprites/Mei_Bee/South|0", "CostumeHandlers/Mei/Bee_Normal.lua")
        if(iMei_QueenBee == 1.0 or true) then
            AM_SetProperty("Register Costume Entry", "MeiBee", "Queen", "Root/Images/Sprites/Mei_BeeQueen/South|0", "CostumeHandlers/Mei/Bee_Queen.lua")
        end
    end
    
--[ ========================================= Chapter 5 ========================================= ]
elseif(iCurrentChapter == 5.0) then

    --[Password]
    --Flag to unlock the terminal in Sector 99.
    if(sPassword ~= "Null") then
        if(sPassword == "My Best Friend") then
            AM_SetProperty("Flag Handled Password")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iTerminalHasBackers", "N", 1.0)
        end
        
        return
    end
    
    --If the password was anything else, ignore it.
    if(sPassword ~= "Null") then return end

    --[Special Bypass]
    --The player cannot change costumes at the gala for any reason.
    local iWearingGolemDress = VM_GetVar("Root/Variables/Global/Christine/iWearingGolemDress", "N")
    local iReachedBiolabs    = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
    if(iWearingGolemDress == 1.0 and iReachedBiolabs == 0.0) then return end
    
    --Player cannot change costumes during the dream sequence. There is only one savepoint here.
    local sRoomName = AL_GetProperty("Name")
    if(sRoomName == "RegulusFlashbackA") then return end

    --[Christine]
    --Forms.
    local iHasDollForm  = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
    local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    local iHasRaijuForm = VM_GetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N")
    
    --Unlocked Costumes.
    local iChristine_GolemGala   = VM_GetVar("Root/Variables/Costumes/Christine/iGolemGala", "N")
    local iChristine_DollSweater = VM_GetVar("Root/Variables/Costumes/Christine/iDollSweater", "N")
    local iChristine_DollNude    = VM_GetVar("Root/Variables/Costumes/Christine/iDollNude", "N")
    local iChristine_HumanNude   = VM_GetVar("Root/Variables/Costumes/Christine/iHumanNude", "N")
    local iChristine_RaijuNude   = VM_GetVar("Root/Variables/Costumes/Christine/iRaijuNude", "N")
    
    --Golem. Also restricts access to human, since if Christine does not have golem form, she does not have her female form yet.
    if(iHasGolemForm == 1.0) then
        AM_SetProperty("Register Costume Heading", "ChristineHuman", "Christine", "Human", "Root/Images/Sprites/Christine_Human/South|0")
        AM_SetProperty("Register Costume Entry", "ChristineHuman", "Normal", "Root/Images/Sprites/Christine_Human/South|0", "CostumeHandlers/Christine/Human_Normal.lua")
        AM_SetProperty("Register Costume Entry", "ChristineHuman", "Nude",   "Root/Images/Sprites/Christine_Human/South|0", "CostumeHandlers/Christine/Human_Nude.lua")
            
        AM_SetProperty("Register Costume Heading", "ChristineGolem", "Christine", "Golem", "Root/Images/Sprites/Christine_Golem/South|0")
        AM_SetProperty("Register Costume Entry", "ChristineGolem", "Normal", "Root/Images/Sprites/Christine_Golem/South|0", "CostumeHandlers/Christine/Golem_Normal.lua")
        if(iChristine_GolemGala == 1.0) then
            AM_SetProperty("Register Costume Entry", "ChristineGolem", "Gala Dress", "Root/Images/Sprites/Christine_GolemDress/South|0", "CostumeHandlers/Christine/Golem_GalaDress.lua")
        end
    end
    
    --Raiju.
    if(iHasRaijuForm == 1.0) then
        AM_SetProperty("Register Costume Heading", "ChristineRaiju", "Christine", "Raiju", "Root/Images/Sprites/Christine_RaijuClothes/South|0")
        AM_SetProperty("Register Costume Entry", "ChristineRaiju", "Normal", "Root/Images/Sprites/Christine_RaijuClothes/South|0", "CostumeHandlers/Christine/Raiju_Normal.lua")
        AM_SetProperty("Register Costume Entry", "ChristineRaiju", "Nude",   "Root/Images/Sprites/Christine_Raiju/South|0",        "CostumeHandlers/Christine/Raiju_Nude.lua")
    end
    
    --Doll.
    if(iHasDollForm == 1.0) then
        AM_SetProperty("Register Costume Heading", "ChristineDoll", "Christine", "Doll", "Root/Images/Sprites/Christine_Doll/South|0")
        AM_SetProperty("Register Costume Entry", "ChristineDoll", "Normal",  "Root/Images/Sprites/Christine_Doll/South|0", "CostumeHandlers/Christine/Doll_Normal.lua")
        AM_SetProperty("Register Costume Entry", "ChristineDoll", "Sweater", "Root/Images/Sprites/Christine_Doll/South|0", "CostumeHandlers/Christine/Doll_Sweater.lua")
        AM_SetProperty("Register Costume Entry", "ChristineDoll", "Nude",    "Root/Images/Sprites/Christine_Doll/South|0", "CostumeHandlers/Christine/Doll_Nude.lua")
    end

    --[55]
    --[SX-399]
    --[Sophie]
    local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 1.0) then
        local iSophie_GolemGala = VM_GetVar("Root/Variables/Costumes/Sophie/iGolemGala", "N")
        AM_SetProperty("Register Costume Heading", "SophieGolem", "Sophie", "Golem", "Root/Images/Sprites/Sophie/South|0")
        AM_SetProperty("Register Costume Entry", "SophieGolem", "Normal", "Root/Images/Sprites/Sophie/South|0", "CostumeHandlers/Sophie/Golem_Normal.lua")
        if(iSophie_GolemGala == 1.0) then
            AM_SetProperty("Register Costume Entry", "SophieGolem", "Gala Dress", "Root/Images/Sprites/SophieDress/South|0", "CostumeHandlers/Sophie/Golem_GalaDress.lua")
        end
    end
    
    --[56]
    local i56_DollNude = VM_GetVar("Root/Variables/Costumes/56/iDollNude", "N")



--[ ======================================= Error Handler ======================================= ]
else
end
