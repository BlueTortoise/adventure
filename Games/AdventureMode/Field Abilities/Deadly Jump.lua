--[ ======================================== Deadly Jump ======================================== ]
--Prowler ability. Causes a forward leap, if any enemies are hit a battle starts and the enemies
-- start at 100 stun value.
--During cancel mode, the movement is locked out but everything else runs as normal.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ =================================== Create Field Ability ==================================== ]
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Deadly Jump", "Root/Special/Combat/FieldAbilities/DeadlyJump")
        FieldAbility_SetProperty("Display Name", "Deadly Jump")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", 600)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Mei|Pounce")
        FieldAbility_SetProperty("Allocate Display Strings", 2)
        FieldAbility_SetProperty("Set Display Strings", 0, "Leap forward quickly. Colliding with an enemy will trigger a battle with no ambush,")
        FieldAbility_SetProperty("Set Display Strings", 1, "but all enemies will begin the battle with 100 Stun. 10 second cooldown.")
    DL_PopActiveObject()
    
--[ ========================================== Execute ========================================== ]
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run or iSwitchType == gciFieldAbility_Cancel) then
    
    --[Setup]
    --Variables.
    local iSegments = 19
    local iTimer = AL_GetProperty("Field Ability Timer")
    local fDistance = (5.0 * gciSizePerTile) / iSegments
    
    --Completion percentage.
    local fPercent = iTimer / iSegments
    
    --Verticality.
    local fAngle = fPercent * 3.1415926
    local fYOffset = math.sin(fAngle) * -8.0
    
    --[Zeroth Tick]
    --Do additional setup on the first tick.
    if(iTimer == 0) then
    
        --Mark the player's controls as locked out.
        AL_SetProperty("Set Field Ability Lock Controls", true)
        
        --Increase the player's hitbox size for encounters to 16, default is 8.
        TA_SetProperty("Combat Initiate Range", 16.0)
        AL_SetProperty("Fold Party")
    
    --[Running Ticks]
    elseif(iTimer < iSegments) then
    
        --Resolve the party leader's current position and facing.
        EM_PushEntity(gsPartyLeaderName)
        
            --Get variables.
            local iFacing = TA_GetProperty("Facing")
            
            --Mark special frame.
            TA_SetProperty("Set Special Frame", "DeadlyJump"..iFacing)
            
            --Order the entity to move. This respects collisions.
            if(iSwitchType == gciFieldAbility_Run) then
                TA_SetProperty("Emulate Movement", iFacing, fDistance)
            end
            TA_SetProperty("Y Vertical Offset", fYOffset)
            
        DL_PopActiveObject()
        
        --For all party members, match the offset.
        for i = 1, #gsaFollowerNames, 1 do
            EM_PushEntity(gsaFollowerNames[i])
                TA_SetProperty("Y Vertical Offset", fYOffset)
            DL_PopActiveObject()
        end
        AL_SetProperty("Fold Party")
    
    --[Finale]
    --On the final tick, order this object to self-destruct.
    elseif(iTimer >= iSegments) then
        
        --On the exact ending tick:
        if(iTimer == iSegments) then
            EM_PushEntity(gsPartyLeaderName)
                TA_SetProperty("Set Special Frame", "Null")
            DL_PopActiveObject()
            AL_SetProperty("Set Field Ability Lock Controls", false)
        
        --The ability does not expire for another 5 ticks.
        elseif(iTimer >= iSegments + 20) then
        
            --Mark ability for removal.
            AL_SetProperty("Set Field Ability Expired")
        
            --Reset.
            TA_SetProperty("Combat Initiate Range", -1.0)
        end
    end
    
--[ ====================================== Touch an Enemy ======================================= ]
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    
    --Deadly Jump precludes ambushes since it stuns enemies instead.
    AdvCombat_SetProperty("Remove Initiative")
    
    --Register an extra script to execute during combat.
    AdvCombat_SetProperty("Register Extra Script", gsRoot .. "Field Abilities/Deadly Jump Combat.lua")
    
    --Clear combat range.
    TA_SetProperty("Combat Initiate Range", -1.0)
    
--[ ======================================= Modify Actor ======================================== ]
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then

    --Modify the entity to have a visual tag above them.
    TA_SetProperty("Display String", "Stunned!", true)

end
