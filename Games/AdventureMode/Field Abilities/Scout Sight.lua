--[ ======================================== Scout Sight ======================================== ]
--Toggled ability. Zooms the camera out.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--[ =================================== Create Field Ability ==================================== ]
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Scout Sight", "Root/Special/Combat/FieldAbilities/ScoutSight")
        FieldAbility_SetProperty("Display Name", "Scout Sight")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", 1)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Mei|ScoutsHonor")
        FieldAbility_SetProperty("Allocate Display Strings", 2)
        FieldAbility_SetProperty("Set Display Strings", 0, "Zoom the camera out so you can see more of your surroundings.")
        FieldAbility_SetProperty("Set Display Strings", 1, "Toggled ability.")
    DL_PopActiveObject()
    
    --Toggle Variable
    VM_SetVar("Root/Variables/Combat/FieldAbilities/iScoutSightActive", "N", 0)
    VM_SetVar("Root/Variables/Combat/FieldAbilities/iScoutSightZoomTimer", "N", 0)
    
--[ ========================================== Execute ========================================== ]
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run) then
    
    --[Setup]
    --Variables.
    local iExecTicks = 15
    local iTimer = AL_GetProperty("Field Ability Timer")
    local fZoomFactor = -1.0
    
    --Camera computations.
    local fCameraDefaultZoom = AL_GetProperty("Default Camera Scale")
    
    --[Zeroth Tick]
    --Do additional setup on the first tick.
    if(iTimer == 0) then
        
        --Toggle whether the ability is active or not.
        local iScoutSightActive = VM_GetVar("Root/Variables/Combat/FieldAbilities/iScoutSightActive", "N")
        if(iScoutSightActive == 0.0) then
            VM_SetVar("Root/Variables/Combat/FieldAbilities/iScoutSightActive", "N", 1)
        else
            VM_SetVar("Root/Variables/Combat/FieldAbilities/iScoutSightActive", "N", 0)
        end
    end
    
    --Get variables.
    local iScoutSightActive    = VM_GetVar("Root/Variables/Combat/FieldAbilities/iScoutSightActive", "N")
    local iScoutSightZoomTimer = VM_GetVar("Root/Variables/Combat/FieldAbilities/iScoutSightZoomTimer", "N")
    
    --Zooming out:
    if(iScoutSightActive == 1.0) then
        if(iScoutSightZoomTimer < iExecTicks) then
            local fPercent = (iScoutSightZoomTimer+1) / iExecTicks
            VM_SetVar("Root/Variables/Combat/FieldAbilities/iScoutSightZoomTimer", "N", iScoutSightZoomTimer+1)
            local fScale = fCameraDefaultZoom + (fPercent * fZoomFactor)
            AL_SetProperty("Camera Zoom", fScale)
        end
        
    --Zooming in:
    else
        if(iScoutSightZoomTimer > 0) then
            local fPercent = (iScoutSightZoomTimer-1) / iExecTicks
            VM_SetVar("Root/Variables/Combat/FieldAbilities/iScoutSightZoomTimer", "N", iScoutSightZoomTimer-1)
            local fScale = fCameraDefaultZoom + (fPercent * fZoomFactor)
            AL_SetProperty("Camera Zoom", fScale)
        end
    end
    
    --[Finale]
    --On the final tick, order this object to self-destruct.
    if(iTimer >= iExecTicks) then
        AL_SetProperty("Set Field Ability Expired")
    end
    
--[ ===================================== Cancel Execution ====================================== ]
--Called when the ability cancels out for activating a cutscene spot.
elseif(iSwitchType == gciFieldAbility_Cancel) then

    --Variables.
    local iExecTicks = 15
    local fZoomFactor = -1.0
    local fCameraDefaultZoom = AL_GetProperty("Default Camera Scale")
    
    --Immediately return to normal zoom.
    VM_SetVar("Root/Variables/Combat/FieldAbilities/iScoutSightActive", "N", 0)
    local iScoutSightZoomTimer = VM_GetVar("Root/Variables/Combat/FieldAbilities/iScoutSightZoomTimer", "N")
    
    --Zooming in.
    if(iScoutSightZoomTimer > 0) then
        local fPercent = (iScoutSightZoomTimer-1) / iExecTicks
        VM_SetVar("Root/Variables/Combat/FieldAbilities/iScoutSightZoomTimer", "N", iScoutSightZoomTimer-1)
        local fScale = fCameraDefaultZoom + (fPercent * fZoomFactor)
        AL_SetProperty("Camera Zoom", fScale)
    
    --Done.
    else
        AL_SetProperty("Set Field Ability Expired")
    end

--[ ====================================== Touch an Enemy ======================================= ]
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    --Never activates.
    
--[ ======================================= Modify Actor ======================================== ]
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then
    --Never activates.

end
