--[ ================================ Assemble Field Ability List ================================ ]
--Assemble a list of all unlocked field abilities. Called whenever the field ability menu is opened.
if(AdvCombat_GetProperty("Is Ability Unlocked", "Mei", "Prowler", "Prowler|Deadly Jump") == true or true) then
    AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/DeadlyJump")
end
AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/WraithForm")
AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/ScoutSight")
AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/PickLock")