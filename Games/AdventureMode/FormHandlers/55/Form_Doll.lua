--[55 Doll]
--55 is always a Doll, this is just a formality to make the code more generalized.

--[Sprite]
if(EM_Exists("55") == true) then
	EM_PushEntity("55")
	
		--Base
		fnSetCharacterGraphics("Root/Images/Sprites/55/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Chair0", "Root/Images/Sprites/Special/55|Chair0")
        TA_SetProperty("Add Special Frame", "Chair1", "Root/Images/Sprites/Special/55|Chair1")
        TA_SetProperty("Add Special Frame", "Chair2", "Root/Images/Sprites/Special/55|Chair2")
        TA_SetProperty("Add Special Frame", "Chair3", "Root/Images/Sprites/Special/55|Chair3")
        TA_SetProperty("Add Special Frame", "Chair4", "Root/Images/Sprites/Special/55|Chair4")
        TA_SetProperty("Add Special Frame", "Crouch", "Root/Images/Sprites/Special/55|Crouch")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/55|Wounded")
        TA_SetProperty("Add Special Frame", "Downed", "Root/Images/Sprites/Special/55|Downed")
        TA_SetProperty("Add Special Frame", "Stealth","Root/Images/Sprites/Special/55|Stealth")
        TA_SetProperty("Add Special Frame", "Draw0",  "Root/Images/Sprites/Special/55|Draw0")
        TA_SetProperty("Add Special Frame", "Draw1",  "Root/Images/Sprites/Special/55|Draw1")
        TA_SetProperty("Add Special Frame", "Draw2",  "Root/Images/Sprites/Special/55|Draw2")
        TA_SetProperty("Add Special Frame", "Exec0",  "Root/Images/Sprites/Special/55|Exec0")
        TA_SetProperty("Add Special Frame", "Exec1",  "Root/Images/Sprites/Special/55|Exec1")
        TA_SetProperty("Add Special Frame", "Exec2",  "Root/Images/Sprites/Special/55|Exec2")
        TA_SetProperty("Add Special Frame", "Exec3",  "Root/Images/Sprites/Special/55|Exec3")
        TA_SetProperty("Add Special Frame", "Exec4",  "Root/Images/Sprites/Special/55|Exec4")
        TA_SetProperty("Add Special Frame", "Exec5",  "Root/Images/Sprites/Special/55|Exec5")
	DL_PopActiveObject()
end

--[Combat Statistics]
AC_PushPartyMember("55")

	--[Statistics]
	--Catalyst Flag.
	ACE_SetProperty("Benefits From Catalysts", true)
	
	--Unfinalize the stats to prevent debug spam.
	ACE_SetProperty("Unfinalize Stats")
	
	--HP Computation
	local iHP = ACE_GetProperty("Health")
	local iHPMax = ACE_GetProperty("Health Max")
	local fHPPercent = iHP / iHPMax

	--Base Statistics
    ACE_SetProperty("Health Max", 37)
	ACE_SetProperty("Health Percent", fHPPercent)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 15)
	ACE_SetProperty("Speed", 7)
	ACE_SetProperty("Protection", 0)
	
	--Level Modifiers
	ACE_SetProperty("Vitality", 3.75, 0.25)
	ACE_SetProperty("Offense",  4.2,  0.35)
	ACE_SetProperty("Deftness", 6.1, 0.25)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.05)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.25)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   1.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--[Rendering]
	--Combat Rendering Paths
	ACE_SetProperty("Form Name", "Doll")
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/55")
    ACE_SetProperty("Card Portrait",  "Root/Images/AdventureUI/BigPortraits/55")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/55")
    ACE_SetProperty("Countermask",    "Null")
    
    --Sprites for Vendor UI
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/55/SW|0", 0)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/55/SW|1", 1)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/55/SW|2", 2)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/55/SW|3", 3)
    
    --Character Sprite Lookup
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == "55") then
            gsCharSprites[i][2] = "Root/Images/Sprites/55/SW|0"
            break
        end
    end
	
	--UI Rendering Positions
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   141,  40)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -232, 125)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1123, 125)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -194,-183)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,  -117, 382)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 391, 370 + 45)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,   -120, -65)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,      89, -31)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     821, -31)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  779,  16)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   386, 178)
    
    --Face Table
    local fIndexX = 1
    local fIndexY = 6
    ACE_SetProperty("Face Table Image", "Root/Images/AdventureUI/ItemIcons/CharacterFaces")
    ACE_SetProperty("Face Table Dim", fIndexX * gci_FaceTable_Size, fIndexY * gci_FaceTable_Size, (fIndexX+1) * gci_FaceTable_Size, (fIndexY+1) * gci_FaceTable_Size)
	
	--Finalize the stats, triggering a recomputation.
	ACE_SetProperty("Finalize Stats")

DL_PopActiveObject()