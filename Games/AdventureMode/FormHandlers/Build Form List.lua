--[Build Form List]
--Builds the list of forms that can be accessed from the debug manager. These are in the format of "[Charactername]/Form_[Formname]".
-- Each form handler is supposed to operate on one character.
local saMeiList = {"Mei/Form_Alraune", "Mei/Form_Bee", "Mei/Form_Ghost", "Mei/Form_Human", "Mei/Form_Rubber", "Mei/Form_Slime", "Mei/Form_Werecat", "Mei/Form_Zombee"}
local saFlorentinaList = {"Florentina/Job_Merchant", "Florentina/Job_Mediator", "Florentina/Job_TreasureHunter"}
local saChristineList = {"Christine/Form_Human", "Christine/Form_Darkmatter", "Christine/Form_Eldritch", "Christine/Form_Electrosprite", "Christine/Form_Golem", "Christine/Form_Latex", "Christine/Form_SteamDroid", "Christine/Form_Raiju", "Christine/Form_Doll"}

--Attach the lists.
local saListList = {}
saListList[1] = saMeiList
saListList[2] = saFlorentinaList
saListList[3] = saChristineList

--Sum.
local iTotalForms = 0
for i = 1, #saListList, 1 do
    iTotalForms = iTotalForms + #saListList[i]
end

--Allocate forms.
ADebug_SetProperty("Forms Total", iTotalForms)

--Set.
local o = 1
for i = 1, #saListList, 1 do
    for p = 1, #saListList[i], 1 do
        ADebug_SetProperty("Form Path", o-1, saListList[i][p])
        o = o + 1
    end
end