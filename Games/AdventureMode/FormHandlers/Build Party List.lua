--[Build Party List]
--Builds a list of all party members that can transform when the menu is selected. While this is almost
-- always just the runebearers in the party, there are a couple of exception cases.
--It is assumed the party list is cleared right before calling this.

--[Party Members]
--Build a general-purpose list.
local iPartyListSize = 0
local sPartyList = {}

--Main character becomes the zero entry.
iPartyListSize = iPartyListSize + 1
sPartyList[iPartyListSize] = {gsPartyLeaderName, "Null"}

--Run through the follower listing, add the followers to the end of the list.
for i = 1, gsFollowersTotal, 1 do
    iPartyListSize = iPartyListSize + 1
    sPartyList[iPartyListSize] = {gsaFollowerNames[i], "Null"}
end

--[Image Cross Reference]
--This is a list of all entries for all characters, indicating what image they should use. Characters who can transform
-- use logic to resolve it.
--Character may be marked with "DELIST" which orders the list to ignore them.
local iImgListSize = 0
local sImgList = {}

--Finder function. Finds the character's current sprite.
local fnGetSprite = function(sCharName)
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == sCharName) then
            return gsCharSprites[i][2]
        end
    end
    return "Null"
end

--Adder function, appends the character data to the end of the list.
local fnAddImgList = function(sCharName)
    iImgListSize = iImgListSize + 1
    sImgList[iImgListSize] = {sCharName, fnGetSprite(sCharName)}
end

--Add all the characters who can ever be followers.
fnAddImgList("Mei")
fnAddImgList("Florentina")
fnAddImgList("Christine")
fnAddImgList("55")
fnAddImgList("JX101")
fnAddImgList("SX399")
fnAddImgList("Sophie")

--[Data Upload]
--Now upload the data.
for i = 1, iPartyListSize, 1 do
    
    --Determine the image.
    local sImgPath = "Null"
    for p = 1, iImgListSize, 1 do
        if(sImgList[p][1] == sPartyList[i][1]) then
            sImgPath = sImgList[p][2]
            break
        end
    end
    
    --Set.
    AM_SetFormProperty("Add Party Member", sPartyList[i][1], sImgPath)
end
