--[Christine Darkmatter]
--Changes Christine's form to that of a Darkmatter. More fragile, but good resistances.

--[Script Variables]
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Darkmatter")
VM_SetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N", 1.0)

--[World]
--Any enemies on the map of this type should now ignore Christine.
AL_PulseIgnore("Darkmatter")

--[Sprite]
if(EM_Exists("Christine") == true) then
	EM_PushEntity("Christine")
	
		--Base.
		fnSetCharacterGraphics("Root/Images/Sprites/Christine_Darkmatter/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", true)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Christine_DarkM|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Christine_DarkM|Crouch")
	DL_PopActiveObject()
end

--[Combat Statistics]
AC_PushPartyMember("Christine")

	--[Statistics]
	--Catalyst Flag.
	ACE_SetProperty("Benefits From Catalysts", true)
	
	--Unfinalize the stats to prevent debug spam.
	ACE_SetProperty("Unfinalize Stats")
	
	--Name reset.
	ACE_SetProperty("Display Name", "Null")
	ACE_SetProperty("Form Name", "Darkmatter")
	
	--HP Computation
	local iHP = ACE_GetProperty("Health")
	local iHPMax = ACE_GetProperty("Health Max")
	local fHPPercent = iHP / iHPMax

	--Base Statistics
    ACE_SetProperty("Health Max", gcfChristineBaseHP - 45)
	ACE_SetProperty("Health Percent", fHPPercent)
	
	--Derived Statistics, Base
	ACE_SetProperty("Damage", gcfChristineBaseDamage)
	ACE_SetProperty("Speed", gcfChristineBaseSpeed + 1)
	ACE_SetProperty("Protection", -10)
	
	--Level Modifiers
	ACE_SetProperty("Vitality", gcfChristineBaseVitality, gcfChristineVitalityGrow)
	ACE_SetProperty("Offense",  gcfChristineBaseOffense,  gcfChristineOffenseGrow)
	ACE_SetProperty("Deftness", gcfChristineBaseDeftness, gcfChristineDeftnessGrow)
	
	--Damage Factors
    ACE_SetProperty("Resistance", gciFactor_Slash,     0.90)
    ACE_SetProperty("Resistance", gciFactor_Pierce,    0.90)
    ACE_SetProperty("Resistance", gciFactor_Strike,    0.90)
    ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
    ACE_SetProperty("Resistance", gciFactor_Ice,       0.10)
    ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
    ACE_SetProperty("Resistance", gciFactor_Holy,      1.50)
    ACE_SetProperty("Resistance", gciFactor_Shadow,    0.50)
    ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
    ACE_SetProperty("Resistance", gciFactor_Blind,     2.00)
    ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   0.50)
    ACE_SetProperty("Resistance", gciFactor_Terrify,   2.00)
	
	--[Rendering]
	--Set rendering properties.
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Christine_Darkmatter")
    ACE_SetProperty("Card Portrait",  "Root/Images/AdventureUI/BigPortraits/Christine_Darkmatter")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Christine_Darkmatter")
    
    --Vendor UI
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Darkmatter/SW|0", 0)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Darkmatter/SW|1", 1)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Darkmatter/SW|2", 2)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Darkmatter/SW|3", 3)
    
    --Character Sprite Lookup
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == "Christine") then
            gsCharSprites[i][2] = "Root/Images/Sprites/Christine_Darkmatter/SW|0"
            break
        end
    end
	
	--Dialogue Portraits
	DialogueActor_Push("Christine")
		DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/Darkmatter|Neutral", true)
		DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/Darkmatter|Blush", true)
		DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/Darkmatter|Happy", true)
		DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/Darkmatter|Smirk", true)
		DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/Darkmatter|Sad", true)
		DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/Darkmatter|Scared", true)
		DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/Darkmatter|Offended", true)
		DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/Darkmatter|Neutral", true)
		DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/Darkmatter|Cry", true)
		DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/Darkmatter|Laugh", true)
		DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/Darkmatter|Angry", true)
		DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/Darkmatter|PDU", true)
	DL_PopActiveObject()
	
	--UI Rendering Positions
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   186, 139)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -176, 223)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1163, 223)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -155, -82)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,   -68, 480)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 466, 472 + 45)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    -46,  28)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,     134,  59)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     867,  62)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  820,  69)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   413, 228)
    
    --Face Table
    local fIndexX = 6
    local fIndexY = 4
    ACE_SetProperty("Face Table Image", "Root/Images/AdventureUI/ItemIcons/CharacterFaces")
    ACE_SetProperty("Face Table Dim", fIndexX * gci_FaceTable_Size, fIndexY * gci_FaceTable_Size, (fIndexX+1) * gci_FaceTable_Size, (fIndexY+1) * gci_FaceTable_Size)
    
    --[Ability Icons]
	if(ACE_GetProperty("Has Ability", "Techniques") == true) then
		ACE_PushAction("Techniques")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|FemaleTech")
        DL_PopActiveObject()
    end
	
	--Finalize the stats, triggering a recomputation.
	ACE_SetProperty("Finalize Stats")

DL_PopActiveObject()