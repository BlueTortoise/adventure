--[Christine Doll]
--Doll Christine! She's so pretty!

--[Script Variables]
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Doll")
VM_SetVar("Root/Variables/Global/Christine/iHasDollForm", "N", 1.0)

--[World]
--Any enemies on the map of this type should now ignore Christine.
AL_PulseIgnore("Doll")

--[Sprite]
if(EM_Exists("Christine") == true) then
	EM_PushEntity("Christine")
	
        --Graphics.
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_Doll/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded",   "Root/Images/Sprites/Special/Christine_Doll|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",    "Root/Images/Sprites/Special/Christine_Doll|Crouch")
        TA_SetProperty("Add Special Frame", "FlatbackC", "Root/Images/Sprites/Special/Christine_Doll|FlatbackC")
        TA_SetProperty("Add Special Frame", "FlatbackO", "Root/Images/Sprites/Special/Christine_Doll|FlatbackO")
	DL_PopActiveObject()
end

--[Combat Statistics]
AC_PushPartyMember("Christine")

	--[Statistics]
	--Catalyst Flag.
	ACE_SetProperty("Benefits From Catalysts", true)
	
	--Unfinalize the stats to prevent debug spam.
	ACE_SetProperty("Unfinalize Stats")
	
	--Name reset.
	ACE_SetProperty("Display Name", "Null")
	ACE_SetProperty("Form Name", "Doll")
	
	--HP Computation
	local iHP = ACE_GetProperty("Health")
	local iHPMax = ACE_GetProperty("Health Max")
	local fHPPercent = iHP / iHPMax

	--Base Statistics
    ACE_SetProperty("Health Max", gcfChristineBaseHP)
	ACE_SetProperty("Health Percent", fHPPercent)
	
	--Derived Statistics, Base
	ACE_SetProperty("Damage", gcfChristineBaseDamage)
	ACE_SetProperty("Speed", gcfChristineBaseSpeed)
	ACE_SetProperty("Protection", 10)
	
	--Level Modifiers
	ACE_SetProperty("Vitality", gcfChristineBaseVitality, gcfChristineVitalityGrow)
	ACE_SetProperty("Offense",  gcfChristineBaseOffense,  gcfChristineOffenseGrow)
	ACE_SetProperty("Deftness", gcfChristineBaseDeftness, gcfChristineDeftnessGrow)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   1.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--[Rendering]
	--Combat Rendering Paths
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Christine_Doll")
	ACE_SetProperty("Card Portrait",  "Root/Images/AdventureUI/BigPortraits/Christine_Doll")
		
	--Field portrait. For most of the game, use the side-facing portrait.
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Christine_Doll")
    
    --Sprites for Vendor UI
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Doll/SW|0", 0)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Doll/SW|1", 1)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Doll/SW|2", 2)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/Christine_Doll/SW|3", 3)
    
    --Character Sprite Lookup
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == "Christine") then
            gsCharSprites[i][2] = "Root/Images/Sprites/Christine_Doll/SW|0"
            break
        end
    end
	
	--Dialogue Portraits
	DialogueActor_Push("Christine")
        DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/Doll|Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/Doll|Happy", true)
        DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/Doll|Blush", true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/Doll|Smirk", true)
        DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/Doll|Sad", true)
        DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/Doll|Scared", true)
        DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/Doll|Offended", true)
        DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/Doll|Serious", true)
        DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/Doll|Cry", true)
        DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/Doll|Laugh", true)
        DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/Doll|Angry", true)
        DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/Doll|PDU", true)
	DL_PopActiveObject()
	
	--UI Rendering Positions. This is the non-serious case.
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   150, 164)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -232, 245)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1108, 270)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -197, -55)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,  -104, 511)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 433, 493 + 45)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    -81,  54)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,      82,  89)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     811,  88)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  801,  76)
    ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   401, 240)
    
    --Face Table
    local fIndexX = 13
    local fIndexY = 4
    ACE_SetProperty("Face Table Image", "Root/Images/AdventureUI/ItemIcons/CharacterFaces")
    ACE_SetProperty("Face Table Dim", fIndexX * gci_FaceTable_Size, fIndexY * gci_FaceTable_Size, (fIndexX+1) * gci_FaceTable_Size, (fIndexY+1) * gci_FaceTable_Size)
    
    --[Ability Icons]
	if(ACE_GetProperty("Has Ability", "Techniques") == true) then
		ACE_PushAction("Techniques")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|GolemTech")
        DL_PopActiveObject()
    end
	
	--Finalize the stats, triggering a recomputation.
	ACE_SetProperty("Finalize Stats")

DL_PopActiveObject()