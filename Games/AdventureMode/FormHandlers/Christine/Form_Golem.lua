--[Christine Golem]
--Changes Christine's form to that of a golem. They can walk in space!
--Higher durability but lower speed and very vulnerable to lightning.

--[Script Variables]
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Golem")
VM_SetVar("Root/Variables/Global/Christine/iHasGolemForm", "N", 1.0)

--Get variables.
local iWearingGolemDress = VM_GetVar("Root/Variables/Global/Christine/iWearingGolemDress", "N")

--[World]
--Any enemies on the map of this type should now ignore Christine.
AL_PulseIgnore("Golem")

--[Appearance]
--Costume handler will set the sprite, emotes, and UI positions.
LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Golem")

--[Combat Statistics]
AC_PushPartyMember("Christine")

	--[Statistics]
	--Catalyst Flag.
	ACE_SetProperty("Benefits From Catalysts", true)
	
	--Unfinalize the stats to prevent debug spam.
	ACE_SetProperty("Unfinalize Stats")
	
	--Name reset.
	ACE_SetProperty("Display Name", "Null")
	ACE_SetProperty("Form Name", "Golem")
	
	--HP Computation
	local iHP = ACE_GetProperty("Health")
	local iHPMax = ACE_GetProperty("Health Max")
	local fHPPercent = iHP / iHPMax

	--Base Statistics
    ACE_SetProperty("Health Max", gcfChristineBaseHP + 35)
	ACE_SetProperty("Health Percent", fHPPercent)
	
	--Derived Statistics, Base
	ACE_SetProperty("Damage", gcfChristineBaseDamage)
	ACE_SetProperty("Speed", gcfChristineBaseSpeed - 2)
	ACE_SetProperty("Protection", 10)
	
	--Level Modifiers
	ACE_SetProperty("Vitality", gcfChristineBaseVitality, gcfChristineVitalityGrow)
	ACE_SetProperty("Offense",  gcfChristineBaseOffense,  gcfChristineOffenseGrow)
	ACE_SetProperty("Deftness", gcfChristineBaseDeftness, gcfChristineDeftnessGrow)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    0.70)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.30)
	ACE_SetProperty("Resistance", gciFactor_Fire,      0.80)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.50)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.50)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   2.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--[Rendering]
    --Face Table
    local fIndexX = 9
    local fIndexY = 4
    ACE_SetProperty("Face Table Image", "Root/Images/AdventureUI/ItemIcons/CharacterFaces")
    ACE_SetProperty("Face Table Dim", fIndexX * gci_FaceTable_Size, fIndexY * gci_FaceTable_Size, (fIndexX+1) * gci_FaceTable_Size, (fIndexY+1) * gci_FaceTable_Size)
    
    --[Ability Icons]
	if(ACE_GetProperty("Has Ability", "Techniques") == true) then
		ACE_PushAction("Techniques")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|GolemTech")
        DL_PopActiveObject()
    end
	
	--Finalize the stats, triggering a recomputation.
	ACE_SetProperty("Finalize Stats")

DL_PopActiveObject()