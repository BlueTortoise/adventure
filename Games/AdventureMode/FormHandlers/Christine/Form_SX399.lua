--[Christine SX-399]
--Used during a non-combat sequence where Christine impersonates SX-399.

--[Script Variables]
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "SteamDroid")
VM_SetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N", 1.0)

--[World]
--Any enemies on the map of this type should now ignore Christine.
AL_PulseIgnore("SteamDroid")

--[Sprite]
if(EM_Exists("Christine") == true) then
	EM_PushEntity("Christine")
	
		--Base.
		fnSetCharacterGraphics("Root/Images/Sprites/SX399/", false)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Christine_Golem|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Christine_Golem|Crouch")
        TA_SetProperty("Add Special Frame", "Laugh0",  "Root/Images/Sprites/Special/Christine|Laugh0")
        TA_SetProperty("Add Special Frame", "Laugh1",  "Root/Images/Sprites/Special/Christine|Laugh1")
        TA_SetProperty("Add Special Frame", "Sad",     "Root/Images/Sprites/Special/Christine|Sad")
	DL_PopActiveObject()
end

--[Combat Statistics]
AC_PushPartyMember("Christine")

	--[Statistics]
	--Catalyst Flag.
	ACE_SetProperty("Benefits From Catalysts", true)
	
	--Unfinalize the stats to prevent debug spam.
	ACE_SetProperty("Unfinalize Stats")
	
	--Name reset.
	ACE_SetProperty("Display Name", "Null")
	ACE_SetProperty("Form Name", "SteamDroid")
	
	--HP Computation
	local iHP = ACE_GetProperty("Health")
	local iHPMax = ACE_GetProperty("Health Max")
	local fHPPercent = iHP / iHPMax

	--Base Statistics
    ACE_SetProperty("Health Max", gcfChristineBaseHP + 15)
	ACE_SetProperty("Health Percent", fHPPercent)
	
	--Derived Statistics, Base
	ACE_SetProperty("Damage", gcfChristineBaseDamage)
	ACE_SetProperty("Speed", gcfChristineBaseSpeed - 1)
	ACE_SetProperty("Protection", 5)
	
	--Level Modifiers
	ACE_SetProperty("Vitality", gcfChristineBaseVitality, gcfChristineVitalityGrow)
	ACE_SetProperty("Offense",  gcfChristineBaseOffense,  gcfChristineOffenseGrow)
	ACE_SetProperty("Deftness", gcfChristineBaseDeftness, gcfChristineDeftnessGrow)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   1.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--[Rendering]
	--Combat Rendering Paths
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Christine_SteamDroid")
	ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Christine_SteamDroid")
	
	--Dialogue Portraits
	DialogueActor_Push("Christine")
		DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/SX399Dialogue/Steam", true)
		DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/SX399Dialogue/Steam", true)
	DL_PopActiveObject()
	
	--UI Rendering Positions.
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,    23, 169)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -171, 268)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right,  817, 250)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -149, -53)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,   -62, 489)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 482, 545)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   252, 240)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  479, 107)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    116 + 171, 117)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,     -61 + 171, 141)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     692 + 171, 141)
	
	--Finalize the stats, triggering a recomputation.
	ACE_SetProperty("Finalize Stats")

DL_PopActiveObject()