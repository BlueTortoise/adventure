--[ ==================================== Florentina Mediator ==================================== ]
--Script called when Florentina changes jobs to Mediator.

--[Combat Statistics]
--Change Florentina's class. This will automatically handle sprite changes, and marks the active
-- job in the script variables.
AdvCombat_SetProperty("Push Party Member", "Florentina")
    AdvCombatEntity_SetProperty("Active Job", "Mediator")
DL_PopActiveObject()