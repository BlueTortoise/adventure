-- |[ ====================================== Form Resolver ===================================== ]|
--This script will be fired whenever the player uses the "Forms" menu at a save point. It will be fired with its
-- first argument being the name of the party member selected.
local iArgs = LM_GetNumOfArgs()
if(iArgs < 1) then return end

--Debug boolean. If set to true, the form handler itself is called to instantly perform the transformation. There
-- will be no cutscenes activated.
local bUseDirectFormHandlers = false

--Argument resolve.
local sEntityName = LM_GetScriptArgument(0)
local sCurrentForm = "Null"
local sChapter = "Null"
local sClassRoot = "Null"

-- |[ ======================================== Function ======================================== ]|
local iTotalForms = 0
local zaFormList = {}
local fnAddTransformation = function(sVariablePath, sName, sImgPrefix, sJobScriptPath)
	iTotalForms = iTotalForms + 1
	zaFormList[iTotalForms] = {}
	zaFormList[iTotalForms].sName = sName
	zaFormList[iTotalForms].sVariablePath = sVariablePath
	zaFormList[iTotalForms].sImgPath = "Root/Images/Sprites/" .. sImgPrefix .. "/SW|0"
	zaFormList[iTotalForms].sJobScriptPath = sJobScriptPath
end

-- |[ ========================================= Lookups ======================================== ]|
--Mei's transformation cases.
if(sEntityName == "Mei") then
	
	--Current form.
	sCurrentForm = VM_GetVar("Root/Variables/Global/" .. sEntityName .. "/sForm", "S")
    sChapter = "Chapter 1/Scenes"
    sClassRoot = gsRoot .. "Combat/Jobs/Mei/"
	
	--Form Listing:
    local sVar = "Root/Variables/Global/Mei/"
	fnAddTransformation("TRUE",                    "Human",   "Mei_Human",   sClassRoot .. "Fencer/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasAlrauneForm", "Alraune", "Mei_Alraune", sClassRoot .. "Nightshade/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasBeeForm",     "Bee",     "Mei_Bee",     sClassRoot .. "Hive Scout/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasSlimeForm",   "Slime",   "Mei_Slime",   sClassRoot .. "Smarty Sage/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasGhostForm",   "Ghost",   "Mei_Ghost",   sClassRoot .. "Maid/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasWerecatForm", "Werecat", "Mei_Werecat", sClassRoot .. "Prowler/000 Job Script.lua")
    
    --Debug
	--fnAddTransformation("TRUE", "Rubber", "Mei_Human")
	--fnAddTransformation("TRUE", "Zombee", "Mei_Bee_MC")

--[Christine]
--Christine's transformation cases.
elseif(sEntityName == "Christine") then
	
	--Completely lock out all TFs if we haven't ended Regulus City yet.
	local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	if(iMet55InLowerRegulus == 0.0) then return end
	
	--Lock out TFs if Christine is in one of the sensitive areas.
	local sLevelName = AL_GetProperty("Name")
	if(sLevelName == "SprocketCityA") then return end
	if(sLevelName == "RegulusFlashbackA") then return end
	
	--Current Form.
	sCurrentForm = VM_GetVar("Root/Variables/Global/" .. sEntityName .. "/sForm", "S")
    sChapter = "Chapter5Scenes"
	
	--Form Listing:
    local sVar = "Root/Variables/Global/Christine/"
	fnAddTransformation("TRUE",                          "Human",         "Christine_Human")
	fnAddTransformation(sVar .. "iHasGolemForm",         "Golem",         "Christine_Golem")
	fnAddTransformation(sVar .. "iHasLatexForm",         "LatexDrone",    "Christine_Latex")
	fnAddTransformation(sVar .. "iHasDarkmatterForm",    "Darkmatter",    "Christine_Darkmatter")
	fnAddTransformation(sVar .. "iHasEldritchForm",      "Dreamer",       "Christine_DreamGirl")
	fnAddTransformation(sVar .. "iHasElectrospriteForm", "Electrosprite", "Christine_Electrosprite")
	fnAddTransformation(sVar .. "iHasRaijuForm",         "Raiju",         "Christine_RaijuClothes")
	fnAddTransformation(sVar .. "iHasSteamDroidForm",    "SteamDroid",    "Christine_SteamDroid")
	fnAddTransformation(sVar .. "iHasDollForm",          "Doll",          "Christine_Doll")

--[Florentina]
--Can change jobs via this menu.
elseif(sEntityName == "Florentina") then

    --Job listing.
    local iHasJob_Merchant       = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Merchant", "N")
    local iHasJob_Mediator       = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Mediator", "N")
    local iHasJob_TreasureHunter = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_TreasureHunter", "N")
    local sCurrentJob            = VM_GetVar("Root/Variables/Global/Florentina/sCurrentJob", "S")
    
    --Variables.
    sClassRoot = gsRoot .. "Combat/Jobs/Florentina/"
    
    --Merchant:
    if(iHasJob_Merchant == 1.0 and sCurrentJob ~= "Merchant") then
        AM_SetFormProperty("Add Form", "Merchant", gsRoot .. "FormHandlers/Florentina/Job_Merchant.lua", sClassRoot .. "Merchant/000 Job Script.lua", "Root/Images/Sprites/Florentina/SW|0")
    end
    
    --Mediator:
    if(iHasJob_Mediator == 1.0 and sCurrentJob ~= "Mediator") then
        AM_SetFormProperty("Add Form", "Mediator", gsRoot .. "FormHandlers/Florentina/Job_Mediator.lua", sClassRoot .. "Mediator/000 Job Script.lua", "Root/Images/Sprites/FlorentinaMed/SW|0")
    end
    
    --Treasure Hunter:
    if(iHasJob_TreasureHunter == 1.0 and sCurrentJob ~= "TreasureHunter") then
        AM_SetFormProperty("Add Form", "Treasure Hunter", gsRoot .. "FormHandlers/Florentina/Job_TreasureHunter.lua", sClassRoot .. "TreasureHunter/000 Job Script.lua", "Root/Images/Sprites/FlorentinaTH/SW|0")
    end
    return

--Error, name not recognized. Usually means the member has no TFs/Jobs.
else
	--Debug_ForcePrint("Form Resolver: Error, no entity " .. sEntityName .. " handled.\n")

end

-- |[ ========================================= Upload ========================================= ]|
--Once the characters have built their listing, run through the list and upload the data to the UI.
for i = 1, iTotalForms, 1 do
    
    --Check the form name against this one. If they match, this is the comparison form.
    if(sCurrentForm == zaFormList[i].sName) then
    
    --Check availability:
    else
        --Check variables.
        local iHasForm = 0.0
        if(zaFormList[i].sVariablePath == "TRUE" or true) then
            iHasForm = 1.0
        else
            iHasForm = VM_GetVar(zaFormList[i].sVariablePath, "N")
        end
        
        --If the form is unlocked, add it to the list.
        if(iHasForm == 1.0) then
            
            --Direct handler.
            if(bUseDirectFormHandlers) then
                AM_SetFormProperty("Add Form", zaFormList[i].sName, gsRoot .. "FormHandlers/" .. sEntityName .. "/Form_" .. zaFormList[i].sName .. ".lua", zaFormList[i].sJobScriptPath, zaFormList[i].sImgPath)
    
            --Normal case. Can trigger cutscenes.
            else
                AM_SetFormProperty("Add Form", zaFormList[i].sName, gsRoot .. sChapter .. "/100 Transform/Transform_" .. sEntityName .. "To" .. zaFormList[i].sName .. "/Scene_Begin.lua", zaFormList[i].sJobScriptPath, zaFormList[i].sImgPath)
            end
        end
    end
end
