--[JX-101 Steam Droid]
--JX-101 joins the party for a brief period.

--[Sprite]
if(EM_Exists("JX101") == true) then
	EM_PushEntity("JX101")
	
		--Base
		fnSetCharacterGraphics("Root/Images/Sprites/JX101/", false)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        --TA_SetProperty("Add Special Frame", "Downed", "Root/Images/Sprites/Special/JX101|Downed")
	DL_PopActiveObject()
end

--[Combat Statistics]
AC_PushPartyMember("JX-101")

	--[Statistics]
	--Catalyst Flag.
	ACE_SetProperty("Benefits From Catalysts", true)
	
	--Unfinalize the stats to prevent debug spam.
	ACE_SetProperty("Unfinalize Stats")
	
	--HP Computation
	local iHP = ACE_GetProperty("Health")
	local iHPMax = ACE_GetProperty("Health Max")
	local fHPPercent = iHP / iHPMax

	--Base Statistics
    ACE_SetProperty("Health Max", 135)
	ACE_SetProperty("Health Percent", fHPPercent)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 25)
	ACE_SetProperty("Speed", 6)
	ACE_SetProperty("Protection", 0)
	
	--Level Modifiers
	ACE_SetProperty("Vitality", 3.75, 0.25)
	ACE_SetProperty("Offense",  4.20,  0.35)
	ACE_SetProperty("Deftness", 6.10, 0.25)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      0.90)
	ACE_SetProperty("Resistance", gciFactor_Ice,       0.90)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.25)
	ACE_SetProperty("Resistance", gciFactor_Blind,     2.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   3.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--JX-101 does not gain experience from combat.
	ACE_SetProperty("Gains No EXP", true)
	
	--[Rendering]
	--Combat Rendering Paths
	ACE_SetProperty("Form Name", "SteamDroid")
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/JX-101")
    ACE_SetProperty("Card Portrait",  "Root/Images/AdventureUI/BigPortraits/JX-101")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/JX-101")
    ACE_SetProperty("Countermask",    "Null")
    
    --Vendor UI
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/JX101/SW|0", 0)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/JX101/SW|1", 1)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/JX101/SW|2", 2)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/JX101/SW|3", 3)
    
    --Character Sprite Lookup
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == "JX101") then
            gsCharSprites[i][2] = "Root/Images/Sprites/JX101/SW|0"
            break
        end
    end
	
	--UI Rendering Positions
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   228, 150)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -167, 237)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1176, 237)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -125, -66)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,   -40, 491)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 504, 480 + 45)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    -14,  45)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,     154,  76)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     865,  78)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  835,  69)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   430, 233)
    
    --Face Table
    local fIndexX = 3
    local fIndexY = 6
    ACE_SetProperty("Face Table Image", "Root/Images/AdventureUI/ItemIcons/CharacterFaces")
    ACE_SetProperty("Face Table Dim", fIndexX * gci_FaceTable_Size, fIndexY * gci_FaceTable_Size, (fIndexX+1) * gci_FaceTable_Size, (fIndexY+1) * gci_FaceTable_Size)
	
	--Finalize the stats, triggering a recomputation.
	ACE_SetProperty("Finalize Stats")

DL_PopActiveObject()