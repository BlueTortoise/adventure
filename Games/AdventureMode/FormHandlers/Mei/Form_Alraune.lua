--[ ======================================== Mei Alraune ======================================== ]
--Script called when Mei shapeshifts to Alraune at a save point.

--[Script Variables]
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Alraune")
VM_SetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N", 1.0)

--[World]
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Alraune")

--[Combat Statistics]
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Nightshade")
DL_PopActiveObject()