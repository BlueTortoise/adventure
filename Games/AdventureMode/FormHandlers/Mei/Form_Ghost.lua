--[ ========================================= Mei Ghost ========================================= ]
--Script called when Mei shapeshifts to Ghost at a save point.

--[Script Variables]
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Ghost")
VM_SetVar("Root/Variables/Global/Mei/iHasGhostForm", "N", 1.0)

--[World]
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Ghost")

--[Combat Statistics]
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Maid")
DL_PopActiveObject()