--[ ======================================== Mei Werecat ======================================== ]
--Script called when Mei shapeshifts to Werecat at a save point.

--[Script Variables]
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Werecat")
VM_SetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N", 1.0)

--[World]
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Werecat")

--[Combat Statistics]
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Prowler")
DL_PopActiveObject()