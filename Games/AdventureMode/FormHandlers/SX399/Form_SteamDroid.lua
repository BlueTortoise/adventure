--[SX-399 Steam Droid]
--SX-399 joins the party as a Steam Lord in the post-gala sequence.

--[Sprite]
if(EM_Exists("SX399") == true) then
	EM_PushEntity("SX399")
	
		--Base
		fnSetCharacterGraphics("Root/Images/Sprites/SX399Lord/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/SX399/Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/SX399/Crouch")
        TA_SetProperty("Add Special Frame", "Laugh0",  "Root/Images/Sprites/SX399/Laugh0")
        TA_SetProperty("Add Special Frame", "Laugh1",  "Root/Images/Sprites/SX399/Laugh1")
	DL_PopActiveObject()
end

--[Combat Statistics]
AC_PushPartyMember("SX-399")

	--[Statistics]
	--Catalyst Flag.
	ACE_SetProperty("Benefits From Catalysts", true)
	
	--Unfinalize the stats to prevent debug spam.
	ACE_SetProperty("Unfinalize Stats")
	
	--HP Computation
	local iHP = ACE_GetProperty("Health")
	local iHPMax = ACE_GetProperty("Health Max")
	local fHPPercent = iHP / iHPMax

	--Base Statistics
    ACE_SetProperty("Health Max", 114)
	ACE_SetProperty("Health Percent", fHPPercent)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 15)
	ACE_SetProperty("Speed", 5)
	ACE_SetProperty("Protection", 0)
	
	--Level Modifiers
	ACE_SetProperty("Vitality", 4.75, 0.35)
	ACE_SetProperty("Offense",  5.20,  0.25)
	ACE_SetProperty("Deftness", 5.10, 0.15)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      0.90)
	ACE_SetProperty("Resistance", gciFactor_Ice,       0.90)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.25)
	ACE_SetProperty("Resistance", gciFactor_Blind,     2.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   3.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--[Rendering]
	--Combat Rendering Paths
	ACE_SetProperty("Form Name", "SteamLord")
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/SX-399")
    ACE_SetProperty("Card Portrait",  "Root/Images/AdventureUI/BigPortraits/SX-399")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/SX-399")
    ACE_SetProperty("Countermask",    "Null")
    
    --Sprites for Vendor UI
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/SX399Lord/SW|0", 0)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/SX399Lord/SW|1", 1)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/SX399Lord/SW|2", 2)
    ACE_SetProperty("Vendor Sprite", "Root/Images/Sprites/SX399Lord/SW|3", 3)
    
    --Character Sprite Lookup
    for i = 1, giCharSpritesTotal, 1 do
        if(gsCharSprites[i][1] == "SX399") then
            gsCharSprites[i][2] = "Root/Images/Sprites/SX399Lord/SW|0"
            break
        end
    end
	
	--UI Rendering Positions
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Main,   179, 150)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Left,  -186, 239)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status_Right, 1150, 239)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,  -151, -72)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Main,   -80, 500)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu_Main, 460, 485 + 45)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Main,    -56,  38)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Lft,     136,  72)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equip_Rgt,     865,  72)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Victory_Main,  812,  72)
	ACE_SetProperty("UI Render Position", gci_ACE_RenderSlot_Doctor_Main,   413, 235)
    
    --Face Table
    local fIndexX = 2
    local fIndexY = 6
    ACE_SetProperty("Face Table Image", "Root/Images/AdventureUI/ItemIcons/CharacterFaces")
    ACE_SetProperty("Face Table Dim", fIndexX * gci_FaceTable_Size, fIndexY * gci_FaceTable_Size, (fIndexX+1) * gci_FaceTable_Size, (fIndexY+1) * gci_FaceTable_Size)
	
	--Finalize the stats, triggering a recomputation.
	ACE_SetProperty("Finalize Stats")

DL_PopActiveObject()