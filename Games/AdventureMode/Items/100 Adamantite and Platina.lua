--[Adamantite and Platina]
--Used by the main item list, this handles the miscellaneous Adamantite and Platina items.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

--[Adamantite Items]
--These items stack. This can be bypassed if you need an item to be created anyway (usually for shop inventories).
-- Sample: "Adamantite Powder x1"
-- The x is important. It denotes the quantity start. In the case of 1, use x1!
if(string.sub(gsItemName, 1, 11) == "Adamantite " and gfBypassAdamantite == 0.0) then

	--Setup.
	local iSection = -1
	local iQuantity = 0

	--Find out what the subtype is.
	if(string.sub(gsItemName, 12, 17) == "Powder") then
		iSection = gciCraft_Adamantite_Powder
		iQuantity = tonumber(string.sub(gsItemName, 20))
	elseif(string.sub(gsItemName, 12, 17) == "Flakes") then
		iSection = gciCraft_Adamantite_Flakes
		iQuantity = tonumber(string.sub(gsItemName, 20))
	elseif(string.sub(gsItemName, 12, 16) == "Shard") then
		iSection = gciCraft_Adamantite_Shard
		iQuantity = tonumber(string.sub(gsItemName, 19))
	elseif(string.sub(gsItemName, 12, 16) == "Piece") then
		iSection = gciCraft_Adamantite_Piece
		iQuantity = tonumber(string.sub(gsItemName, 19))
	elseif(string.sub(gsItemName, 12, 16) == "Chunk") then
		iSection = gciCraft_Adamantite_Chunk
		iQuantity = tonumber(string.sub(gsItemName, 19))
	elseif(string.sub(gsItemName, 12, 14) == "Ore") then
		iSection = gciCraft_Adamantite_Ore
		iQuantity = tonumber(string.sub(gsItemName, 17))
	end

	--Send this information to the inventory.
	AdInv_SetProperty("Add Crafting Material", iSection, iQuantity)
	gbFoundItem = true
	return
end

--[Cash]
--The currency of Pandemonium is Platina, at least for the game's purposes.
-- Will be of the format Platina x[NUM].
if(string.sub(gsItemName, 1, 8) == "Platina ") then

	local iPlatinaAmount = string.sub(gsItemName, 10)
	AdInv_SetProperty("Add Platina", iPlatinaAmount)
	gbFoundItem = true
	return
end

--[Adamantite Items]
--These can only be created for the purposes of shops. If the player buys one, it gets added to their
-- inventory and cannot be resold.
if(gsItemName == "Adamantite Powder") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", "Oxide powder of the rare adamantite metal, which is unique to this world. Can be used to upgrade items and equipment.")
		AdItem_SetProperty("Value", 50)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmPowder")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Flakes") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", "Assorted metallic flakes of the rare adamantite metal. Can be used to upgrade items and equipment.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmFlakes")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Shard") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", "A broken shard of the rare adamantite metal. Can be used to upgrade items and equipment.")
		AdItem_SetProperty("Value", 450)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmShards")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Piece") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", "A fist-sized piece of the rare adamantite metal. Can be used to upgrade items and equipment.")
		AdItem_SetProperty("Value", 700)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmPieces")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Chunk") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", "A solid chunk of the rare adamantite metal. Can be used to upgrade items and equipment.")
		AdItem_SetProperty("Value", 1200)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmChunks")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Ore") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", "Base ore of adamantite. Extremely rare, as adamantite veins cannot be located through traditional means. Can be used to upgrade items and equipment.")
		AdItem_SetProperty("Value", 2000)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmOre")
	DL_PopActiveObject()
	gbFoundItem = true
end