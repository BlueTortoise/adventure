--[Mei Items]
--Items that only Mei can equip for various reasons. Mei can use her work clothes, light armor, and katanas.
-- This listing also includes her runestone, like all chapter protagonists.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

--[ =================================== Weapon Standardization ================================== ]
--Sets standard equippable case, slots, and weapon animations.
local fnStandardWeapon = function()
    
    --Can be used as a weapon.
    AdItem_SetProperty("Is Equipment", true)
    AdItem_SetProperty("Add Equippable Character", "Mei")
    AdItem_SetProperty("Add Equippable Slot", "Weapon")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
    
    --Animations and Sound
    AdItem_SetProperty("Equipment Attack Animation", "Sword Slash")
    AdItem_SetProperty("Equipment Attack Sound", "Combat\\|Impact_CrossSlash")
    AdItem_SetProperty("Equipment Critical Sound", "Combat\\|Impact_CrossSlash_Crit")
end

--[ ======================================== Unique Items ======================================= ]
--Mei's runestone. Restores HP in combat, recharges.
if(gsItemName == "Silver Runestone") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A runestone with a silver spiral marking on it. Restores HP and increases accuracy when used in combat.\nKey Item.")
		AdItem_SetProperty("Value", -1)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Item A")
		AdItem_SetProperty("Add Equippable Slot", "Item B")
        AdItem_SetProperty("Ability Path", gsRoot .. "Combat/Abilities/X Items/Silver Runestone.lua")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/RuneMei")
	DL_PopActiveObject()
	gbFoundItem = true
	
--Special Bee Honey. Restores HP in combat!
elseif(gsItemName == "Everlasting Honey") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Delicious honey for long-range scouting drones. Restores HP in combat and never runs out!")
		AdItem_SetProperty("Value", -1)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Item A")
		AdItem_SetProperty("Add Equippable Slot", "Item B")
        AdItem_SetProperty("Ability Path", gsRoot .. "Combat/Abilities/X Items/Everlasting Honey.lua")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarYellow")
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Weapons (Tier 0) ===================================== ]
--[Rusty Katana]
--Rusty Katana, Mei finds this at the start of the game.
elseif(gsItemName == "Rusty Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A beaten up katana. It still has an edge, but it won't cut easily. Still better than nothing.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiA")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 12)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

--[Rusty Jian]
--Rusty Jian. Can be upgraded. Provides a speed boost.
elseif(gsItemName == "Rusty Jian") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A popular straight sword often used by officers. Its blade is rusted, but it will still cut. Increases combat dexterity.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiA")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 10)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 3)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Weapons (Tier 1) ===================================== ]
--[Steel Katana]
--A straight upgrade over the Rusty Katana.
elseif(gsItemName == "Steel Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A katana sword, with a sharp edge and a relatively short reach. Fairly light for its size.")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiA")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 25)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 1)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true
	
--[Steel Jian]
--Steel Jian, straight upgrade over the Rusty Jian.
elseif(gsItemName == "Steel Jian") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A popular straight sword often used by officers. Its blade is light and easy to handle. Increases combat dexterity.")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiA")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 23)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 3)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true

--[Serrated Katana]
--A katana that increases the damage of Mei's Rend ability, but does less damage than the Steel Katana.
elseif(gsItemName == "Serrated Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A katana sword with an edge that is filed irregularly to give it an uneven, tearing cut. Deals damage as bleeding.")
		AdItem_SetProperty("Value", 150)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiA")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 20)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 3)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Bleeding + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
    
        --Override to use bleeding attack.
        AdItem_SetProperty("Equipment Attack Animation", "Bleed")
        AdItem_SetProperty("Equipment Attack Sound", "Combat\\|Impact_CrossSlash")
        AdItem_SetProperty("Equipment Critical Sound", "Combat\\|Impact_CrossSlash_Crit")
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Weapons (Tier 2) ===================================== ]
--[Wildflower's Katana]
--Granted to friends of the forest.
elseif(gsItemName == "Wildflower's Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A pretty blue gem adorns the hilt, and vines seem to grow from it. The blade is alive, and in tune with the forest.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiA")
		AdItem_SetProperty("Gem Slots", 2)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,    35)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,   3)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,      3)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 7)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 1)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Armors (Tier 0) ====================================== ]
--[Mei's Work Uniform]
--Not really armor. Provides minimal protection.
elseif(gsItemName == "Mei's Work Uniform") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Mei's", "Work Uniform")
		AdItem_SetProperty("Description", "Clothes from the themed restaurant Mei works for. Aimed at tourists, it's not normal street apparel.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Armors (Tier 1) ====================================== ]
--[Fencer's Raiment]
--Clothing that provides less protection than most, but increases damage output.
elseif(gsItemName == "Fencer's Raiment") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A light cloth robe infused with simple magic. Easy to move in, it increases damage dealt by Mei's weapons.")
		AdItem_SetProperty("Value", 75)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,    10)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,   3)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,      3)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

--[Striker's Tunic]
--Clothing that provides less protection than most, but increases damage output.
elseif(gsItemName == "Striker's Tunic") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A magic robe that greatly increases combat accuracy, but doesn't provide much protection.")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     5)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,  10)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

--[Tiger Dancer's Dress]
--A dress worn by Tiger Dancers, duh.
elseif(gsItemName == "Tiger Dancer's Dress") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Tiger Dancer's", "Dress")
		AdItem_SetProperty("Description", "A dress supposedly worn by the Tiger Dancers of Quantir. Fierce warrior women who danced through the battlefield, carving through the enemy lines, they are said to be long extinct.")
		AdItem_SetProperty("Value", 300)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,    15)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,   5)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Armors (Tier 2) ====================================== ]
--[Dress of the Slime Dancer]
--Bigger increase in damage.
elseif(gsItemName == "Dress of the Slime Dancer") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Dress of the", "Slime Dancer")
		AdItem_SetProperty("Description", "While wearing clothes is considered odd for most slimes, their dancers find it helps keep from slinging goo everywhere.")
		AdItem_SetProperty("Value", 400)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,    10)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,      3)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,   3)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true

--[ ==================================== Accessories (Tier 1) =================================== ]
--[Arm Brace]
--Accessory, provides a bit of protection for Mei.
elseif(gsItemName == "Arm Brace") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "This light leather brace fits around the forearm to provide protection from attacks.")
		AdItem_SetProperty("Value", 75)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccBracer")
	DL_PopActiveObject()
	gbFoundItem = true

--[Alacrity Bracer]
--Accessory, provides initiative!
elseif(gsItemName == "Alacrity Bracer") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A light leather armbrace that seems to be enchanted. Increases initiative.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccBracer")
	DL_PopActiveObject()
	gbFoundItem = true

--[Haemophiliac's Pendant]
elseif(gsItemName == "Haemophiliac's Pendant") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "For those who just love bleeding. Specifically, the bleeding of others. Increases bleed damage dealt by 25%%.")
		AdItem_SetProperty("Value", 180)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
		AdItem_SetProperty("Add Tag", "Bleed Damage Dealt +", 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
	DL_PopActiveObject()
	gbFoundItem = true
	
--[ ==================================== Accessories (Tier 2) =================================== ]
--[Moonglow Collar]
--Awww, kitty!
elseif(gsItemName == "Moonglow Collar") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A collar that seems to shine with the light of the moon.")
		AdItem_SetProperty("Value", 150)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 7)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade, 12)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRingGldD")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true
	
--[Translucent Feather Duster]
--It never needs to be cleaned! Thanks, ghost magic!
elseif(gsItemName == "Translucent Feather Duster") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Translucent", "Feather Duster")
		AdItem_SetProperty("Description", "Due to magic, it never needs to be cleaned! It can dust shelves for eternity!")
		AdItem_SetProperty("Value", 150)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Evade, 4)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy, 2)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
	DL_PopActiveObject()
	gbFoundItem = true

end