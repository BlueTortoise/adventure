--[Florentina Items]
--Items that only Florentina can equip for various reasons. Florentina uses hunting knives and light armor, and has a unique Smoking Pipe.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

--[ =================================== Weapon Standardization ================================== ]
--Sets standard equippable case, slots, and weapon animations.
local fnStandardWeapon = function()
    
    --Can be used as a weapon.
    AdItem_SetProperty("Is Equipment", true)
    AdItem_SetProperty("Add Equippable Character", "Florentina")
    AdItem_SetProperty("Add Equippable Slot", "Weapon")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
    
    --Animations and Sound
    AdItem_SetProperty("Equipment Attack Animation", "Sword Slash")
    AdItem_SetProperty("Equipment Attack Sound", "Combat\\|Impact_CrossSlash")
    AdItem_SetProperty("Equipment Critical Sound", "Combat\\|Impact_CrossSlash_Crit")
end

--[ ======================================== Unique Items ======================================= ]
--Florentina's Smoking Pipe. It probably has Alraune drugs in it.
if(gsItemName == "Florentina's Pipe") then
    
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Florentina's pipe. It's anyone's guess what's in it...\nKey Item.")
		AdItem_SetProperty("Value", -1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
		AdItem_SetProperty("Gem Slots", 0)
        
        --Accessory.
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 5)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 1)
        
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Weapons (Tier 0) ===================================== ]
--[Hunting Knife]
--Florentina starts with a good-quality hunting knife.
elseif(gsItemName == "Hunting Knife") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A large, flat blade, with a serrated edge on one side. Light, easy to use, and dangerous in the right hands.")
		AdItem_SetProperty("Value", 50)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaA")
		AdItem_SetProperty("Gem Slots", 0)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 12)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Weapons (Tier 1) ===================================== ]
--[Wildflower's Knife]
--A knife explicitly made for Alraunes.
elseif(gsItemName == "Wildflower's Knife") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Odd blue gems adorn the hilt, and a vine seems to be growing over it. It's also really sharp, so Florentina is happy with it.")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaA")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,  16)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,    3)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy, 3)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
    
	DL_PopActiveObject()
	gbFoundItem = true

--[Butterfly Knife]
--Good for stabbin'.
elseif(gsItemName == "Butterfly Knife") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A knife that can be concealed in its handle and revealed with an elegant flipping motion. Good for murder. Boosts combat dexterity.")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaA")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     12)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,       7)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    7)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Weapons (Tier 2) ===================================== ]
--[Haggler's Backup Plan]
--Oh, I get it.
elseif(gsItemName == "Haggler's Backup Plan") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Negotiations sometimes break down. It's best to have a 'backup plan'.")
		AdItem_SetProperty("Value", 235)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaA")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     21)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,       2)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    5)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true

--[ ====================================== Armors (Tier 0) ====================================== ]
--[Flowery Tunic]
--Starting armor for Florentina in Chapter 1.
elseif(gsItemName == "Flowery Tunic") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Florentina's clothing. Stylish, light, comfortable, but not particularly protective.")
		AdItem_SetProperty("Value", 75)
		AdItem_SetProperty("Gem Slots", 0)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 1)
	DL_PopActiveObject()
	gbFoundItem = true
	
--[ ====================================== Armors (Tier 1) ====================================== ]
--[Hemp Skirt]
--Starting armor for Florentina in Chapter 1.
elseif(gsItemName == "Hemp Skirt") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A skirt made of hemp, which smells a lot like whatever's in Florentina's pipe, actually.")
		AdItem_SetProperty("Value", 140)
		AdItem_SetProperty("Gem Slots", 0)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true
	
--[Merchant's Tunic]
--Starting armor for Florentina in Chapter 1.
elseif(gsItemName == "Merchant's Tunic") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "It fits Florentina perfectly, therefore, it's a merchant's tunic. Figure out the logic on your own time.")
		AdItem_SetProperty("Value", 140)
		AdItem_SetProperty("Gem Slots", 1)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 5)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true
	
--[ ====================================== Armors (Tier 2) ====================================== ]
--[Regalia of a Smoothtalker]
--Starting armor for Florentina in Chapter 1.
elseif(gsItemName == "Regalia of a Smoothtalker") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "She'll convince you up is down. That way the money will fall out of your pockets.")
		AdItem_SetProperty("Name Lines", "Regalia of a", "Smoothtalker")
		AdItem_SetProperty("Value", 210)
		AdItem_SetProperty("Gem Slots", 1)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 5)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade, 3)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy, 3)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 5)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true

--[ ==================================== Accessories (Tier 1) =================================== ]
--[Methlace]
--Accessory for Florentina. Increases initiative.
elseif(gsItemName == "Methlace") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Turns out most drugs are fairly mild when taken by Alraunes. A necklace that increases combat initiative.")
		AdItem_SetProperty("Value", 150)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
	DL_PopActiveObject()
	gbFoundItem = true

end