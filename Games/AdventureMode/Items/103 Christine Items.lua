--[Christine Items]
--Items that only Christine can equip for various reasons. Christine uses electrospears and heavy armor.
-- This listing also includes her runestone, like all chapter protagonists.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

--[Unique Items]
--Christine's runestone. Restores HP in combat, also increases attack power.
if(gsItemName == "Violet Runestone") then
	
	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
	
	--Create.
	AdInv_CreateItem(gsItemName)
	
		--If Christine has Golem form, change the description.
		if(iHasGolemForm == 0.0) then
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to Chris by his recently deceased aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.\nKey Item.")
		else
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.\nKey Item.")
		end
		
		--Standard properties.
		AdItem_SetProperty("Value", -1)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Item)
		AdItem_SetProperty("Cooldown", 2)
        AdItem_SetProperty("Create Action")
			ACA_SetProperty("Description", "Heal yourself for 35 HP.\n Increases attack by 30%/1 turn.")
			ACA_SetProperty("Targetting", gci_Target_Self)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Self Healing", 35)
			ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_Self, 30, 1)
			fnStandardAttackAnim("Healing")
			fnStandardAbilitySounds("Healing")
            ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
            ACAC_SetProperty("Combo Image",   "Null")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Item|VioletRunestone")
		DL_PopActiveObject()
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/RuneChristine")
	DL_PopActiveObject()
	gbFoundItem = true

--[Tazer]
--Tazer, found at the start of the chapter. Cannot be upgraded. Lost when becoming Christine.
elseif(gsItemName == "Tazer") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "It looks vaguely like a cattle prod with a handle. Better than nothing.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 5)
		AdItem_SetProperty("Protection Boost", 0)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepChristineA")
	DL_PopActiveObject()
	gbFoundItem = true

--[Chris's Clothes]
--Cannot be upgraded, not really armor. Provides minimal protection. Cannot be upgraded. Lost when becoming Christine.
elseif(gsItemName == "Schoolmaster's Suit") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Clothes that Chris wore at his job as an English Teacher. Not suited for combat.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Damage Boost", 0)
		AdItem_SetProperty("Protection Boost", 1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true

--[Carbonweave Electrospear]
--Christine receives this upon becoming a golem. Better than the tazer. This is the "Stock" Electrospear with no upsides or downsides.
elseif(gsItemName == "Carbonweave Electrospear") then
	
	--Get an empty equipment pack.
	local zEqpPrototype = fnCreateEquipmentPack(3)
	if(zEqpPrototype == nil) then return end
	
	--Modify the values that change from the default.
	zEqpPrototype.iaValues = {200, 350, 600} --Cash Value
	zEqpPrototype.iaDamVls = {  16, 25,  32} --Damage
	
	--Get the item's boost value. Add 1 to make it an array index.
	local iRank = fnGetEquipmentRank(gsItemName, "Carbonweave Electrospear") + 1
	
	--Create the item.
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A carbon-fibre spear fitted with a fusion battery and a metal discharge surface on the point. Light and strong, it is a powerful close-range weapon.")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 16)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepChristineA")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true

--[Silksteel Electrospear]
--A very powerful electrospear.
elseif(gsItemName == "Silksteel Electrospear") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A model of electrospear made out of woven threads of steel. It holds together like a spider-web and is extraordinarily strong.")
		AdItem_SetProperty("Value", 400)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 41)
		AdItem_SetProperty("Evasion Boost", 3)
		AdItem_SetProperty("Accuracy Boost", 3)
		AdItem_SetProperty("Initiative Boost", 0)
		AdItem_SetProperty("Protection Boost", 0)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepChristineA")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true

--[Yttrium Electrospear]
--But Y though? (For reference, MarioneTTe made that joke, not Salty. Direct your ire at him.)
elseif(gsItemName == "Yttrium Electrospear") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "An electrospear whose tip is coated with a superconductive yttrium alloy.")
		AdItem_SetProperty("Value", 650)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 62)
		AdItem_SetProperty("Evasion Boost", 5)
		AdItem_SetProperty("Accuracy Boost", 5)
		AdItem_SetProperty("Initiative Boost", 0)
		AdItem_SetProperty("Protection Boost", 0)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepChristineA")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true

--[Tellurine Electrospear]
--Best in the chapter by a wide margin. Received for completing the Tellurium Mines.
elseif(gsItemName == "Tellurine Electrospear") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "An electrospear made out of an alloy that doesn't exist yet. Extremely powerful.")
		AdItem_SetProperty("Value", 1200)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 83)
		AdItem_SetProperty("Evasion Boost", 7)
		AdItem_SetProperty("Accuracy Boost", 7)
		AdItem_SetProperty("Initiative Boost", 1)
		AdItem_SetProperty("Protection Boost", 5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepChristineA")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true

--[Flowing Dress]
--A beautiful dress given to Christine when she becomes a golem. Can be upgraded. Counts as Medium Armor.
elseif(gsItemName == "Flowing Dress") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A beautiful dress given to 771852 upon conversion.")
		AdItem_SetProperty("Value", 75)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 15)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true

--[Kinetic Capacitor]
--Boosts damage on the electrospear.
elseif(gsItemName == "Kinetic Capacitor") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A special module added to the battery of an electric weapon. Stores up charge from the user's movement through passive electrical fields. Increases damage.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
		AdItem_SetProperty("Damage Boost", 10)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemTech")
	DL_PopActiveObject()
	gbFoundItem = true

--[Viewfinder Module]
--Increases accuracy.
elseif(gsItemName == "Viewfinder Module") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Removable module for 771852's PDU that highlights enemy weak points and predicts their movements. Increases accuracy.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
        AdItem_SetProperty("Accuracy Boost", 7)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemTech")
	DL_PopActiveObject()
	gbFoundItem = true

--[Sure-Grip Gloves]
--Increases damage and accuracy. This may get moved to other files later, but only Christine can use it in Chapter 5.
elseif(gsItemName == "Sure-Grip Gloves") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Gloves with high-friction padding in the palms. Vastly increases weapon grip, boosting damage and accuracy.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
        AdItem_SetProperty("Accuracy Boost", 3)
		AdItem_SetProperty("Damage Boost", 5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccGloves")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

--[Spear Foregrip]
--Increases accuracy.
elseif(gsItemName == "Spear Foregrip") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A high-friction grip near the head of the spear. Simple, but very effective for precision strikes.")
		AdItem_SetProperty("Value", 220)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
        AdItem_SetProperty("Accuracy Boost", 9)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRingSlvA")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

end