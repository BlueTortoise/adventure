--[55's Items]
--Items that only 55 can equip for various reasons. 55 uses pulse diffractors and light armor.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

--[Pulse Diffractor]
--Pulse Diffractor, a close-range pulse cannon that spreads out. Powerful!
if(gsItemName == "Pulse Diffractor") then
	
	--Create the item.
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A short-range Mk V pulse diffractor. The diffraction array scatters the pulse round to cover a wide area, and can optionally be narrowed to pulverize a single target. Powerful at close range.")
		AdItem_SetProperty("Name Lines", "Mk VI Pulse", "Diffractor")
		AdItem_SetProperty("Value", 180)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 19)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Wep55A")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true
    
--[Mk VI Pulse Diffractor]
--Upgraded pulse diffractor. Yeah it's a prototype, but it can blow a door off its hinges. Some radiation leaking is worth the risk!
elseif(gsItemName == "Mk VI Pulse Diffractor") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "The prototype MK VI Pulse Diffractor. While the radiation shielding arrays are not yet optimized and it clanks oddly when firing, it is otherwise superior to the Mk V variant.")
		AdItem_SetProperty("Name Lines", "Mk VI Pulse", "Diffractor")
		AdItem_SetProperty("Value", 400)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 33)
		AdItem_SetProperty("Evasion Boost", 3)
		AdItem_SetProperty("Accuracy Boost", 3)
		AdItem_SetProperty("Initiative Boost", 1)
		AdItem_SetProperty("Protection Boost", 0)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Wep55A")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true
    
--[R-77 Pulse Diffractor]
--Using a special rotational coil instead of the linear magrail, this short-barreled pulse diffractor packs a punch and is excellent in close quarters.
elseif(gsItemName == "R-77 Pulse Diffractor") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A Mk V pulse diffractor using an experiment R-77 coiled magrail. EM waves shouldn't damage the wielder... probably...")
		AdItem_SetProperty("Name Lines", "R-77Pulse", "Diffractor")
		AdItem_SetProperty("Value", 650)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 52)
		AdItem_SetProperty("Evasion Boost", 5)
		AdItem_SetProperty("Accuracy Boost", 5)
		AdItem_SetProperty("Initiative Boost", 1)
		AdItem_SetProperty("Protection Boost", 0)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Wep55A")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true
    
--[Hotshot Pulse Diffractor]
--Pulse diffractor with the heat override disabled. Takes an expert to keep it from melting but provides impressive firepower.
elseif(gsItemName == "Hotshot Pulse Diffractor") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "The Mk V pulse diffractor with the heat override shield replaced by additional mag accelerators. Requires a crack operator to keep from overloading, but deals incredible damage.")
		AdItem_SetProperty("Name Lines", "Hotshot Pulse", "Diffractor")
		AdItem_SetProperty("Value", 900)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 72)
		AdItem_SetProperty("Evasion Boost", 7)
		AdItem_SetProperty("Accuracy Boost", 7)
		AdItem_SetProperty("Initiative Boost", 1)
		AdItem_SetProperty("Protection Boost", 0)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Wep55A")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true

--[55's Clothes]
--Cannot be upgraded, not really armor. Provides minimal protection.
elseif(gsItemName == "Command Unit Garb") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Command Unit", "Garb")
		AdItem_SetProperty("Description", "Reinforced steelweave clothes, they fit tightly around 55's frame.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Damage Boost", 0)
		AdItem_SetProperty("Protection Boost", 4)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true

--[Wide-Spectrum Scanner]
--For 55's Pulse Diffractor, increases accuracy and damage.
elseif(gsItemName == "Wide-Spectrum Scanner") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Attached to 55's Pulse Diffractor, shows a wide range of spectral readouts. Makes identifying enemy weak points take 0.001452 seconds less than usual.")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
		AdItem_SetProperty("Damage Boost", 7)
		AdItem_SetProperty("Accuracy Boost",  4)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemScope")
	DL_PopActiveObject()
	gbFoundItem = true

--[Pulse Radiation Dampener]
--For 55's Pulse Diffractor, reduces undesired radiation scatter. Hit exactly what you're aiming at, AND the wall behind them, but nothing else!
elseif(gsItemName == "Pulse Radiation Dampener") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Pulse Radiation", "Dampener")
		AdItem_SetProperty("Description", "Attached to 55's Pulse Diffractor, reduces undesired muzzle EM radiation scatter when firing. Increases damage to target and reduces damage to nearby furniture and walls (unfortunately...)")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
		AdItem_SetProperty("Damage Boost", 12)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemSuppressor")
	DL_PopActiveObject()
	gbFoundItem = true

--[Magrail Harmonizer Module]
--For 55's Pulse Diffractor, fixes those nasty magrail desyncs. Don't you hate having to replace the focusing lenses every 6000 shots? Well no more!
elseif(gsItemName == "Magrail Harmonizer Module") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Magrail Harmonizer", "Module")
		AdItem_SetProperty("Description", "Attached to 55's Pulse Diffractor, prevents the focusing lenses from going out of sync due to non-harmonic magrail radiation. Isn't that the worst?")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
		AdItem_SetProperty("Damage Boost", 10)
		AdItem_SetProperty("Accuracy Boost",  15)
        AdItem_SetProperty("Initiative Boost", 1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemTech")
	DL_PopActiveObject()
	gbFoundItem = true

--[Smoke Bomb]
--Item 55 can use in combat. Increases the party's evade once per combat.
elseif(gsItemName == "Smoke Bomb") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Description", "A smoke-producing explosive powder that increases the entire party's evade by 35 for 2 turns. Can be used once per battle.")
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Item)
        AdItem_SetProperty("Charges Max", 1)
        AdItem_SetProperty("Recharges Every Battle", true)
        AdItem_SetProperty("Charges Dont Refresh on Rest", false)
        AdItem_SetProperty("Create Action")
			ACA_SetProperty("Description", "Increase the entire party's evade by 35 for 2 turns.")
			ACA_SetProperty("Targetting", gci_Target_All_Allies)
			ACA_SetProperty("New Effect", gciEffect_Evade, gci_Target_All_Allies, 35, 2)
            ACA_SetProperty("Add Duration On Nonself", true)
			fnStandardAttackAnim("Buff")
			fnStandardAbilitySounds("Buff")
            ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
            ACAC_SetProperty("Combo Image",   "Null")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
		DL_PopActiveObject()
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemFlashbang")
	DL_PopActiveObject()
	gbFoundItem = true

end