--[JX-101's Items]
--Items used by JX-101. She can only use what she comes with and can't equip anything else.

--[MkV Pulse Pistol]
--She took what 55 said to heart.
if(fnIsEquipmentMatch(gsItemName, "MkV Pulse Pistol")) then
	
	--Get an empty equipment pack.
	local zEqpPrototype = fnCreateEquipmentPack(1)
	if(zEqpPrototype == nil) then return end
	
	--Modify the values that change from the default.
	zEqpPrototype.iaValues = {180} --Cash Value
	zEqpPrototype.iaDamVls = { 32} --Damage
	
	--Descriptions.
	local sConstant = "A one-handed EM-Rail pistol, a clear improvement over the flawed Mk. IV variety. JX-101 has customized it to her needs."
	zEqpPrototype.saDescriptions[1] = sConstant
	
	--Get the item's boost value. Add 1 to make it an array index.
	local iRank = fnGetEquipmentRank(gsItemName, "MkV Pulse Pistol") + 1
	
	--Create the item.
	AdInv_CreateItem(gsItemName)
	
		--Common properties.
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "JX-101")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepJX101")
		
		--Upload all the data from the prototype structure.
        --Stats here
		AdItem_SetProperty("Name Lines", "Mk. V Pulse", "Pistol")
	
	--Finish up.
	DL_PopActiveObject()
	gbFoundItem = true

--[JX-101's Clothes]
--Cannot be upgraded.
elseif(gsItemName == "JX-101's Cuirass") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A heavy steel cuirass, forged to fit JX-101's body.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "JX-101")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Damage Boost", 10)
		AdItem_SetProperty("Protection Boost", 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenMedium")
	DL_PopActiveObject()
	gbFoundItem = true

end