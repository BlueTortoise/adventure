--[SX-399's Items]
--Items used by SX-399. She uses medium armor and fission carbines.

--[Fission Carbine]
--A heavy-hitting close-range weapon designed to literally melt the target. Great against vehicles, robots, people, wildlife, rocks, and anything else that melts under 6000C.
if(gsItemName == "Fission Carbine") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A melee-range specialist weapon using controlled nuclear explosions to effectively melt the target. Effective against things that melt under 6000C, which is everything.")
		AdItem_SetProperty("Value", 360)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepSX399A")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

--[Fission Rifle]
--Bigger critical mass = more melt.
elseif(gsItemName == "Fission Rifle") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Similar to the Fission Carbine, but with a larger critical mass and longer barrel. Melts things with alarming efficiency. As in, will set off the fire alarms.")
		AdItem_SetProperty("Value", 650)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 67)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepSX399A")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

--[Plutonite Fission Carbine]
--The real heavy stuff.
elseif(gsItemName == "Plutonite Fission Carbine") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A fission carbine with a plutonite bursting chamber. Radioactive, yes, but massively increases heat output.")
        AdItem_SetProperty("Name Lines", "Plutonite", "Fission Carbine")
		AdItem_SetProperty("Value", 1125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Weapon)
		AdItem_SetProperty("Damage Boost", 115)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepSX399A")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true

--[SX-399's Clothes]
--Starting bronze chestguard for our favourite/only Steam Lord.
elseif(gsItemName == "Bronze Chestguard") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Recently-forged bronze chestpiece for SX-399. Not much protection, but her chassis doesn't need it!")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Damage Boost", 0)
		AdItem_SetProperty("Protection Boost", 10)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true

--[Sweet Flame Decals]
--This looks so sick, yo.
elseif(gsItemName == "Sweet Flame Decals") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Plastic flame decals that go on the side of your fission carbine. Look cool while you *melt people*.")
        AdItem_SetProperty("Name Lines", "Sweet Flame", "Decals")
		AdItem_SetProperty("Value", 115)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
		AdItem_SetProperty("Damage Boost", 15)
		AdItem_SetProperty("Accuracy Boost", 5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
	DL_PopActiveObject()
	gbFoundItem = true

--[Underslung Flamethrower]
--Oh my god you've gone too far.
elseif(gsItemName == "Underslung Flamethrower") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A miniature flamethrower that can be attached beneath a fission weapon's barrel. Deals 100 fire damage to a single target once per battle.")
		AdItem_SetProperty("Value", 165)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Item)
		AdItem_SetProperty("Cooldown", 3)
        AdItem_SetProperty("Create Action")
			ACA_SetProperty("Description", "Deals 100 Flaming damage to a single target. 3 turn cooldown.")
            ACA_SetProperty("Damage Amount", gciFactor_Fire, -100.00)
            ACA_SetProperty("Damage No Scatter", true)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			fnStandardAttackAnim("LaserShot")
			fnStandardAbilitySounds("LaserShot")
            ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
            ACAC_SetProperty("Combo Image",   "Null")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
		DL_PopActiveObject()
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemSuppressor")
	DL_PopActiveObject()
	gbFoundItem = true

end