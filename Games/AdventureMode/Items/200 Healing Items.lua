--[Healing Items]
--General-purpose healing items.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

--[ =================================== Slot Standardization ==================================== ]
local saUseByEveryone = {"Mei", "Florentina"}
local fnSetCombatItem = function(psaUsableList)
    if(psaUsableList == nil) then return end
    
    AdItem_SetProperty("Is Equipment", true)
    for i = 1, #psaUsableList, 1 do
		AdItem_SetProperty("Add Equippable Character", psaUsableList[i])
    end
    AdItem_SetProperty("Add Equippable Slot", "Item A")
    AdItem_SetProperty("Add Equippable Slot", "Item B")
end

--[ ======================================== Item Listing ======================================= ]
--[Healing Tincture]
--Can be used by everyone, restores some HP.
if(gsItemName == "Healing Tincture") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A magic tincture that restores health of the user in combat. Restores 20HP, 1 charge per battle.")
		AdItem_SetProperty("Value", 75)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Ability Path", gsRoot .. "Combat/Abilities/X Items/Healing Tincture.lua")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemPotionRed")
	DL_PopActiveObject()
	gbFoundItem = true

--[Palliative]
--Palliative. Removes poison and bleed.
elseif(gsItemName == "Palliative") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A tonic that may or may not actually work, but it seems to relieve and prevent the symptoms of poison. Also cures and prevents bleeding... somehow. 1 charge per battle.")
		AdItem_SetProperty("Value", 150)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Ability Path", gsRoot .. "Combat/Abilities/X Items/Palliative.lua")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarGreen")
	DL_PopActiveObject()
	gbFoundItem = true

--[Pepper Pie]
--Increases attack power by 50 for one turn. Tastes "fairly good".
elseif(gsItemName == "Pepper Pie") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A powerful potion placed into a pie. Increases attack power by 50%% for 1 turn, and tastes better than sex with a supermodel. 3 charges per battle.")
		AdItem_SetProperty("Value", 500)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Ability Path", gsRoot .. "Combat/Abilities/X Items/Pepper Pie.lua")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/PepperPie")
	DL_PopActiveObject()
	gbFoundItem = true

--[Smelling Salts]
--Revives a downed ally with 30% HP.
elseif(gsItemName == "Smelling Salts") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Revives a downed party member with 30%% health. 1 charge per battle.")
		AdItem_SetProperty("Value", 100)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Ability Path", gsRoot .. "Combat/Abilities/X Items/Smelling Salts.lua")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemPotionRed")
	DL_PopActiveObject()
	gbFoundItem = true

--[=[
--[Nanite Injection]
--Healing item. Restores HP.
elseif(gsItemName == "Nanite Injection") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "An opaque, white fluid containing billions of nanites. These will rapidly repair flesh and alloy alike when injected, as well as briefly increasing attack power by 20%%. Also restores 35 HP. Can be used on an ally. 1 charge, recharges after combat.")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Item)
        AdItem_SetProperty("Charges Max", 1)
        AdItem_SetProperty("Recharges Every Battle", true)
        AdItem_SetProperty("Charges Dont Refresh on Rest", false)
        AdItem_SetProperty("Create Action")
			ACA_SetProperty("Description", "Increase attack power by 20 for one turn.\nRestores 35 HP.")
			ACA_SetProperty("Target Healing", 35)
            ACA_SetProperty("Add Duration On Nonself", true)
			ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_Single_Ally, 20, 1)
			ACA_SetProperty("Targetting", gci_Target_Single_Ally)
			fnStandardAttackAnim("Healing")
			fnStandardAbilitySounds("Healing")
            ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
            ACAC_SetProperty("Combo Image",   "Null")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
		DL_PopActiveObject()
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemInjector")
	DL_PopActiveObject()
	gbFoundItem = true

--[Explication Spike]
--Single-use item, causes the user to focus very intently for 1 turn, greatly improving accuracy.
elseif(gsItemName == "Explication Spike") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "An electric 'spike', or single-use hacking tool. When applied to one's CPU/brain, greatly improves accuracy for one turn. Organics even report only mild migraines as a side-effect! 1 charge, recharges after combat.")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Item)
        AdItem_SetProperty("Charges Max", 1)
        AdItem_SetProperty("Recharges Every Battle", true)
        AdItem_SetProperty("Charges Dont Refresh on Rest", false)
        AdItem_SetProperty("Create Action")
			ACA_SetProperty("Description", "Increase accuracy by 40 for one turn.")
			ACA_SetProperty("New Effect", gciEffect_Accuracy, gci_Target_Self, 40, 1)
			ACA_SetProperty("Targetting", gci_Target_Self)
			fnStandardAttackAnim("Buff")
			fnStandardAbilitySounds("Buff")
            ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
            ACAC_SetProperty("Combo Image",   "Null")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
		DL_PopActiveObject()
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemTech")
	DL_PopActiveObject()
	gbFoundItem = true

--[Regeneration Mist]
--Nanites in an aerosol? No, I can't see any way that could possibly go wrong. Continue.
elseif(gsItemName == "Regeneration Mist") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Repair nanites loaded into an aerosol container. Not actually a fluid, so it still works in a vacuum. Heals the entire party for 50 HP. 1 charge, recharges after combat.")
		AdItem_SetProperty("Value", 175)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Item)
        AdItem_SetProperty("Charges Max", 1)
        AdItem_SetProperty("Recharges Every Battle", true)
        AdItem_SetProperty("Charges Dont Refresh on Rest", false)
        AdItem_SetProperty("Create Action")
			ACA_SetProperty("Description", "Restores 50 HP to the entire party.")
			ACA_SetProperty("Target Healing", 50)
			ACA_SetProperty("Targetting", gci_Target_All_Allies)
			fnStandardAttackAnim("Healing")
			fnStandardAbilitySounds("Healing")
            ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
            ACAC_SetProperty("Combo Image",   "Null")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
		DL_PopActiveObject()
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemInjector")
	DL_PopActiveObject()
	gbFoundItem = true

--[Emergency Medkit]
--Special medkit that can revive a downed character. Potent!
elseif(gsItemName == "Emergency Medkit") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A package of self-assembling repair nanites, can be used multiple times per combat. Heals 75HP and can revive KO'd teammates.")
		AdItem_SetProperty("Value", 175)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Item)
		AdItem_SetProperty("Cooldown", 3)
        AdItem_SetProperty("Create Action")
			ACA_SetProperty("Description", "Restores 75 HP to a single target. 3 turn cooldown. Can revive downed teammates.")
			ACA_SetProperty("Target Healing", 75)
			ACA_SetProperty("Targetting", gci_Target_Single_Ally + gci_Target_Allow_Downed)
			fnStandardAttackAnim("Healing")
			fnStandardAbilitySounds("Healing")
            ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
            ACAC_SetProperty("Combo Image",   "Null")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
		DL_PopActiveObject()
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemMedkit")
	DL_PopActiveObject()
	gbFoundItem = true

]=]
end