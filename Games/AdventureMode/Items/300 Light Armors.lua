--[Light Armor Listing]
--Light armors, usually providing less protection but sometimes initiative, speed, or other
-- boosts.
--Light armor can be used by:
--1: Mei, Florentina
--2: Izuna
--3: Jeanne, Gallina
--4: Junia, Edea
--5: 55
--6: Maram

--[ =================================== Slot Standardization ==================================== ]
local saUseByEveryone = {"Mei", "Florentina"}
local fnSetCombatItem = function(psaUsableList)
    if(psaUsableList == nil) then return end
    
    AdItem_SetProperty("Is Equipment", true)
    for i = 1, #psaUsableList, 1 do
		AdItem_SetProperty("Add Equippable Character", psaUsableList[i])
    end
    AdItem_SetProperty("Add Equippable Slot", "Body")
end

--[ ======================================== Item Listing ======================================= ]
--[Troubadour's Robe]
--Light armor.
if(gsItemName == "Troubadour's Robe") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Light, simple clothing worn by composers and artists the world over.")
		AdItem_SetProperty("Value", 45)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true
	
--[Traveller's Vest]
--Light armor.
elseif(gsItemName == "Traveller's Vest") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A simple cloth vest used by many ordinary citizens.")
		AdItem_SetProperty("Value", 67)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     2)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true
    
--[=[
--[Dispersion Cloak]
--A cloak made in Regulus City which produces a weak deflection field.
elseif(gsItemName == "Dispersion Cloak") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A light, partially transparent cloak that produces a weak deflection field from latent static charge. It provide little protection but is very natural to move in.")
		AdItem_SetProperty("Value", 75)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 5)
		AdItem_SetProperty("Accuracy Boost", 3)
		AdItem_SetProperty("Evasion Boost", 3)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
	DL_PopActiveObject()
	gbFoundItem = true

--[Adaptive Cloth Vest]
--Armored cloth vest made from Regulus City's advanced 'adaptive' fiber. Has a gem slot.
elseif(gsItemName == "Adaptive Cloth Vest") then

	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Made from quantum-destabilized fibers, this cloth vest is very light but offers protection far above its density class. The vest changes its molecular structure very slightly when struck, to better protect its wearer.")
        AdItem_SetProperty("Name Lines", "Adaptive", "Cloth Vest")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 5)
		AdItem_SetProperty("Accuracy Boost", 3)
		AdItem_SetProperty("Evasion Boost", 3)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true
	
--[Battle Skirt]
--Skirt - but like, combat skirt. Look feminine and badass at the same time!
elseif(gsItemName == "Battle Skirt") then

	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A skirt made of synthweave with armoured plates sewn beneath it. Easy to move in, improves agility. Looks cute.")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 5)
		AdItem_SetProperty("Accuracy Boost", 8)
		AdItem_SetProperty("Evasion Boost", 8)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true
	
--[Hyperweave Chemise]
--Expensive - but useful.
elseif(gsItemName == "Hyperweave Chemise") then

	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A simple chemise made from hyperweave. The incredible properties of hyperweave make this fashionable shirt as strong as a bulletproof vest.")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 10)
		AdItem_SetProperty("Accuracy Boost", 5)
		AdItem_SetProperty("Evasion Boost", 5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true
	
--[Neon Tanktop]
--Okay come on now.
elseif(gsItemName == "Neon Tanktop") then

	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A simple cloth tanktop that fluoresces under most lights, giving off a cool neon glow. Provides no protection but looks intimidating.")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 0)
        AdItem_SetProperty("Damage Boost", 15)
		AdItem_SetProperty("Accuracy Boost", 3)
		AdItem_SetProperty("Evasion Boost", 3)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenLight")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

]=]
end