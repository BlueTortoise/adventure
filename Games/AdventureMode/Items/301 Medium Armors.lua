--[Medium Armor Listing]
--Medium armors. Provides some protection and possibly other bonuses. Usually no maluses.
--Medium armor can be used by:
--1: Mei
--2: Sanya
--3: Aquillia
--4: Lotta
--5: Christine, SX-399
--6: Talia, Elina

--[ =================================== Slot Standardization ==================================== ]
local saUseByEveryone = {"Mei"}
local fnSetCombatItem = function(psaUsableList)
    if(psaUsableList == nil) then return end
    
    AdItem_SetProperty("Is Equipment", true)
    for i = 1, #psaUsableList, 1 do
		AdItem_SetProperty("Add Equippable Character", psaUsableList[i])
    end
    AdItem_SetProperty("Add Equippable Slot", "Body")
end

--[ ======================================== Item Listing ======================================= ]
--[Light Leather Vest Series]
--A comfortable leather vest.
if(gsItemName == "Light Leather Vest") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A hand-crafted leather vest that fits comfortably and will stop a hit or two.")
		AdItem_SetProperty("Value", 75)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenMedium")
		AdItem_SetProperty("Name Lines", "Light Leather", "Vest")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

--[Plated Leather Vest]
--Medium armor.
elseif(gsItemName == "Plated Leather Vest") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Plated Leather", "Vest")
		AdItem_SetProperty("Description", "A leather vest, reinforced with metal plates in key areas. Light enough to move in but strong enough to absorb a blow.")
		AdItem_SetProperty("Value", 300)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Statistic", gciStatIndex_HPMax,     15)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenMedium")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

--[=[

--[Ceramic Weave Vest]
--A vest with ceramics woven around the fibers. Can be upgraded.
elseif(gsItemName =="Ceramic Weave Vest") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "This vest is composed of carbon nanoweave fibers with heat-dissipation ceramic plates attached.")
		AdItem_SetProperty("Value", 75)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 8)
        AdItem_SetProperty("Initiative Boost", -1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenMedium")
		AdItem_SetProperty("Name Lines", "Ceramic Weave", "Vest")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true
	
--[Brass Polymer Chestguard]
--Metallic brass chestguard, but woven to be strong.
elseif(gsItemName == "Brass Polymer Chestguard") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Using similar technology to silksteel, this chestguard made of woven brass polymer is an order of magnitude stronger than a forged brass counterpart, while being lighter to boot.")
		AdItem_SetProperty("Value", 125)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 15)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenMedium")
		AdItem_SetProperty("Name Lines", "Brass Polymer", "Chestguard")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true
]=]
end