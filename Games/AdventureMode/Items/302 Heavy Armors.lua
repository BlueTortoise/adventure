--[Heavy Armor Listing]
--Heavy armor, usually provides more protection than the equivalent mediums and lights, but has maluses
-- on the other side. While the initiative penalty decreases as the items get upgraded (usually), 
-- a lighter piece of armor at the same upgrade level almost always has less of a penalty, or none.
--Here's who can use heavy armor in the various chapters:
--1: 
--2: Sanya, Empress
--3: 
--4: 
--5: Christine
--6: 

--[Neutronium Jacket]
--Armor chestpiece with neutronium in it. Ultra-science or magic keeps it a dense solid as opposed to a fluid.
if(gsItemName == "Neutronium Jacket") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "An armored jacket of sorts, composed of quantum-stasis neutronium within carbon fiber. While less than one nanolitre of neutronium is actually present, the armor is extremely dense and provides excellent protection.")
		AdItem_SetProperty("Value", 300)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 25)
        AdItem_SetProperty("Initiative Boost", -2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenHeavy")
		AdItem_SetProperty("Gem Slots", 1)
	DL_PopActiveObject()
	gbFoundItem = true

--[Titanweave Vest]
--Armor woven from quartz-titanium silksteel. Hard, heavy, strong.
elseif(gsItemName == "Titanweave Vest") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A crystallize vest woven with silksteel embedded with quartz and titanium. Heavy but absurdly strong.")
		AdItem_SetProperty("Value", 700)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Armor)
		AdItem_SetProperty("Protection Boost", 35)
        AdItem_SetProperty("Initiative Boost", -2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmGenHeavy")
		AdItem_SetProperty("Gem Slots", 2)
	DL_PopActiveObject()
	gbFoundItem = true

end