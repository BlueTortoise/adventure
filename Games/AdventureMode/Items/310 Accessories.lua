--[Accessory Listing]
--Accessories usable by various characters. There is no particular pattern here, other than that 
-- any accessory usable by only one character should go in their unique items file.

--[ =================================== Slot Standardization ==================================== ]
local saUseByEveryone = {"Mei", "Florentina"}
local fnSetCombatItem = function(psaUsableList)
    if(psaUsableList == nil) then return end
    
    AdItem_SetProperty("Is Equipment", true)
    for i = 1, #psaUsableList, 1 do
		AdItem_SetProperty("Add Equippable Character", psaUsableList[i])
    end
    AdItem_SetProperty("Add Equippable Slot", "Accessory A")
    AdItem_SetProperty("Add Equippable Slot", "Accessory B")
end

--[ ========================================= Chapter 1 ========================================= ]
--[Sphalite Ring]
if(gsItemName == "Sphalite Ring") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A ring forged from Sphalite. It seems to hum when touched.")
		AdItem_SetProperty("Value", 100)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRingSlvA")
	DL_PopActiveObject()
	gbFoundItem = true
	
--[Decorative Bracer]
--Extra armor, increases damage slightly.
elseif(gsItemName == "Decorative Bracer") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A pretty cloth bracer that fits snugly on the wrist.")
		AdItem_SetProperty("Value", 125)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 4)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccBracer")
	DL_PopActiveObject()
	gbFoundItem = true

--[Jade Eye Ring]
--Accessory, boosts damage and attack.
elseif(gsItemName == "Jade Eye Ring") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Jade Eye", "Ring")
		AdItem_SetProperty("Description", "Golden ring with a cut jewel socketed in it. The jewel resembles an eye.")
		AdItem_SetProperty("Value", 150)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 3)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRingGldB")
	DL_PopActiveObject()
	gbFoundItem = true

--[Enchanted Ring]
--+HP, +Dam
elseif(gsItemName == "Enchanted Ring") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A simple copper band, enchanted with a minor power spell. Increases health and damage.")
		AdItem_SetProperty("Value", 250)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Statistic", gciStatIndex_HPMax,  15)
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 10)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRingSlvC")
	DL_PopActiveObject()
	gbFoundItem = true

--[Jade Necklace]
--+Acc, +Evd
elseif(gsItemName == "Jade Necklace") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Polished jade on a silver band. Has a minor agility enchantment.")
		AdItem_SetProperty("Value", 250)
        fnSetCombatItem(saUseByEveryone)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy, 5)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,    5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
	DL_PopActiveObject()
	gbFoundItem = true

--[=[
--[ ========================================= Chapter 5 ========================================= ]
--[Distribution Frame]
--Increases protection and initiative.
elseif(gsItemName == "Distribution Frame") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A thin exoskeleton designed to fit under clothing. Redistributes kinetic energy, reducing damage and increasing combat initiative.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
		AdItem_SetProperty("Protection Boost", 5)
		AdItem_SetProperty("Initiative Boost", 1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
	DL_PopActiveObject()
	gbFoundItem = true

--[Integrated Gunsight]
--+Acc, +Dam
elseif(gsItemName == "Integrated Gunsight") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A sight that uses laser pulses to transmit telemetry from the weapon to its users' processor. Increases accuracy and damage.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
		AdItem_SetProperty("Accuracy Boost", 5)
		AdItem_SetProperty("Damage Boost", 10)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemScope")
	DL_PopActiveObject()
	gbFoundItem = true

--[Tungsten Suppressor]
--+Ini, +Acc, +Evd
elseif(gsItemName == "Tungsten Suppressor") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A noise-diffusion attachment that enhances conductivity in pulse and fission weapons. Requires manual operation but, for a machine, that is hardly an issue.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
		AdItem_SetProperty("Initiative Boost", 1)
		AdItem_SetProperty("Accuracy Boost", 10)
		AdItem_SetProperty("Evasion Boost", 10)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemSuppressor")
	DL_PopActiveObject()
	gbFoundItem = true

--[Insulated Boots]
--+Prt, +Ini
elseif(gsItemName == "Insulated Boots") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Thermally insulated rubber boots. Look great, protect yourself - these have everything!")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Christine")
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Accessory)
		AdItem_SetProperty("Protection Boost", 5)
		AdItem_SetProperty("Initiative Boost", 1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccBoots")
	DL_PopActiveObject()
	gbFoundItem = true

]=]
end