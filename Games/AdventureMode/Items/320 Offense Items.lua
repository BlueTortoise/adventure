--[Offense Items]
--Equippable items that provide some offensive attack. Can also be a buff.

--[ ========================================= Chapter 1 ========================================= ]
--[ ========================================= Chapter 5 ========================================= ]
--[Magnesium Grenade]
--Hits all enemies for stun damage.
if(gsItemName == "Magnesium Grenade") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A narrow blasting charge with a magnesium core. Creates a bright flash of light and lots of disorienting noise. Deals stun to all enemies.")
		AdItem_SetProperty("Value", 175)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Item)
        AdItem_SetProperty("Charges Max", 1)
        AdItem_SetProperty("Recharges Every Battle", true)
        AdItem_SetProperty("Charges Dont Refresh on Rest", false)
        AdItem_SetProperty("Create Action")
			ACA_SetProperty("Description", "Deals 50 stun to all targets.")
			ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
            ACA_SetProperty("New Effect", gciEffect_Stun, gci_Target_All_Hostiles, 50, 1)
			fnStandardAttackAnim("Strike")
			fnStandardAbilitySounds("Strike")
            ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
            ACAC_SetProperty("Combo Image",   "Null")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
		DL_PopActiveObject()
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemFlashbang")
	DL_PopActiveObject()
	gbFoundItem = true
    
--[Recoil Dampener]
--Increases accuracy and damage for 1 turn on self.
elseif(gsItemName == "Recoil Dampener") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A memory-gel pack that can be attached to a firearm. Greatly decreases recoil for the next shot, but takes time to reform itself.")
		AdItem_SetProperty("Value", 175)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "55")
		AdItem_SetProperty("Add Equippable Character", "SX-399")
		AdItem_SetProperty("Equip Slot", gciAdItem_Equip_Item)
		AdItem_SetProperty("Cooldown", 3)
        AdItem_SetProperty("Create Action")
			ACA_SetProperty("Description", "+50Atk, +10Acc for 1 turn. 3 turn cooldown.")
			ACA_SetProperty("Targetting", gci_Target_Self)
            ACA_SetProperty("New Effect", gciEffect_DamageFixed, gci_Target_Self, 50, 1)
            ACA_SetProperty("New Effect", gciEffect_Accuracy,    gci_Target_Self, 10, 1)
			fnStandardAttackAnim("Buff")
			fnStandardAbilitySounds("Buff")
            ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
            ACAC_SetProperty("Combo Image",   "Null")
            ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
		DL_PopActiveObject()
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemScope")
	DL_PopActiveObject()
	gbFoundItem = true

end