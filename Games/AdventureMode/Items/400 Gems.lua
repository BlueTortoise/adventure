-- |[ ====================================== Gem Listing ======================================= ]|
--Gems. Can be upgraded with Adamantite, allowing them to be merged together. Two gems cannot be
-- merged together if they share colors.

-- |[ ======================================= Gem Colors ======================================= ]|
--[Grey]
--Glintsteel == Accuracy, Evade

--[Red]
--Yemite     == Attack

--[Blue]
--Ardrite    == BleedDam, PoisonDam

--[Orange]
--Rubose     == Protection

--[Violet]
--Blurleen   == SlashRes, StrikeRes

--[Yellow]
--Qederite   == Initiative

--[Other Colors]
--None yet

-- |[ ======================================= Grey Gems ======================================== ]|
--[Tier 1]
if(gsItemName == "Glintsteel Gem") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A small grey gem that sparks with static electricity. Can be socketed into equipment or merged with other gems.")
		AdItem_SetProperty("Is Gem", gciGemColor_Grey)
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Statistic", gciStatIndex_Accuracy, 2)
		AdItem_SetProperty("Statistic", gciStatIndex_Evade,    1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|G")
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ======================================== Red Gems ======================================== ]|
--[Tier 1]
elseif(gsItemName == "Yemite Gem") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A small red gem that glows faintly in the dark. Can be socketed into equipment or merged with other gems.")
		AdItem_SetProperty("Is Gem", gciGemColor_Red)
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Statistic", gciStatIndex_Attack, 3)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|R")
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ======================================== Blue Gems ======================================= ]|
--[Tier 1]
elseif(gsItemName == "Ardrite Gem") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A small blue gem that seems to squish when squeezed. +3%% Bleed Damage dealt, +3%% Poison Damage dealt. Can be socketed into equipment or merged with other gems.")
		AdItem_SetProperty("Is Gem", gciGemColor_Blue)
		AdItem_SetProperty("Value", 200)
        AdItem_SetProperty("Add Tag", "Bleed Damage Dealt +",  3)
        AdItem_SetProperty("Add Tag", "Poison Damage Dealt +", 3)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|B")
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ====================================== Orange Gems ======================================= ]|
--[Tier 1]
elseif(gsItemName == "Rubose Gem") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A small orange gem that is warm to the touch. Can be socketed into equipment or merged with other gems.")
		AdItem_SetProperty("Is Gem", gciGemColor_Orange)
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Statistic", gciStatIndex_Protection, 1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|O")
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ====================================== Violet Gems ======================================= ]|
--[Tier 1]
elseif(gsItemName == "Blurleen Gem") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A small violet gem which has fluid trapped inside it. Can be socketed into equipment or merged with other gems.")
		AdItem_SetProperty("Is Gem", gciGemColor_Violet)
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Statistic", gciStatIndex_Resist_Slash,  1)
		AdItem_SetProperty("Statistic", gciStatIndex_Resist_Strike, 1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|V")
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ====================================== Yellow Gems ======================================= ]|
--[Tier 1]
elseif(gsItemName == "Qederite Gem") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A small yellow gem that becomes signifigantly harder when pressed. Can be socketed into equipment or merged with other gems.")
		AdItem_SetProperty("Is Gem", gciGemColor_Yellow)
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 4)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|Y")
	DL_PopActiveObject()
	gbFoundItem = true
end