--[Miscellaneous Items]
--Items that don't serve much purpose. Act as flavor. Many of these can be sold for a little extra cash.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

--The remnants of wearable clothes.
if(gsItemName == "Tattered Rags") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Once clothes, now ruined. Almost worthless, sell it for scrap cloth.")
		AdItem_SetProperty("Value", 10)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JunkA")
	DL_PopActiveObject()
	gbFoundItem = true
	
--The remnants of some armor.
elseif(gsItemName == "Ruined Armor") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Scraps of armor, bent and rusted. Useless, but can be sold for a pittance.")
		AdItem_SetProperty("Value", 10)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JunkA")
	DL_PopActiveObject()
	gbFoundItem = true
	
--Wrecked weapon.
elseif(gsItemName == "Broken Spear") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A spear, snapped in half. Not much good in combat, but could be sold for scrap to a vendor.")
		AdItem_SetProperty("Value", 10)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JunkA")
	DL_PopActiveObject()
	gbFoundItem = true
	
--Green Gemstone: Emerald
elseif(gsItemName == "Emerald") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A green gem, unpolished and rough. Can be sold for a little extra platina. Not socketable.")
		AdItem_SetProperty("Value", 50)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JunkA")
	DL_PopActiveObject()
	gbFoundItem = true
	
--Blue Gemstone: Aquamarine
elseif(gsItemName == "Aquamarine") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A blue gem, unpolished and rough. Can be sold for a little extra platina. Not socketable.")
		AdItem_SetProperty("Value", 65)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JunkA")
	DL_PopActiveObject()
	gbFoundItem = true
	
--Violet Gemstone: Amethyst
elseif(gsItemName == "Amethyst") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A violet gem, unpolished and rough. Can be sold for a little extra platina. Not socketable.")
		AdItem_SetProperty("Value", 100)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JunkA")
	DL_PopActiveObject()
	gbFoundItem = true
	
end