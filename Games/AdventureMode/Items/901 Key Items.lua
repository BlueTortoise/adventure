--[Key Items]
--Items that have no purpose in combat or whatnot, but are often checked by scripts for accessability.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

--Prying bar, used to open doors.
if(gsItemName == "Pry Bar") then
	
	--If the player already has one, this changes to Platina x50.
	local iPryBarCount = AdInv_GetProperty("Item Count", "Pry Bar")
	if(iPryBarCount > 0) then
		AdInv_SetProperty("Add Platina", 50)
		gbFoundItem = true
		
	--Otherwise, the player gets a Pry Bar.
	else
		AdInv_CreateItem(gsItemName)
			AdItem_SetProperty("Description", "A curved metal bar used for prying open crates or stuck doors.\nKey Item.")
			AdItem_SetProperty("Value", 50)
			AdItem_SetProperty("Is Key Item", true)
			AdItem_SetProperty("Is Unique", true)
			AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Crowbar")
		DL_PopActiveObject()
		gbFoundItem = true
	end
	
--Cultist Key. Escape the starting dungeon.
elseif(gsItemName == "Cultist Key") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "It's decorated with yellow and purple hands, etched and painted with extreme care.\nKey Item.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/KeyBronze")
	DL_PopActiveObject()
	gbFoundItem = true

--Hacksaw. Give this to Claudia.
elseif(gsItemName == "Hacksaw") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A hacksaw, probably with an enchanted blade. It'd make short work of metal bars.\nKey Item.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Hacksaw")
	DL_PopActiveObject()
	gbFoundItem = true

--Oars. Allows usage of the canoes that can be found in some areas.
elseif(gsItemName == "Boat Oars") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Boat oars. Could be used in conjunction with a boat... obviously.\nKey Item.")
		AdItem_SetProperty("Value", 50)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/BoatOars")
	DL_PopActiveObject()
	gbFoundItem = true

--Quantir Mansion Key. Unlocks a bunch of the doors in the Quantir Mansion. Duh.
elseif(gsItemName == "Quantir Mansion Key") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Quantir Mansion", "Key")
		AdItem_SetProperty("Description", "A key that fits the locks in the Quantir Mansion. Seems to have belonged to the serving staff.\nKey Item.")
		AdItem_SetProperty("Value", 50)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/KeySilver")
	DL_PopActiveObject()
	gbFoundItem = true

--Quantir Sewer Key. Unlocks the sewer door.
elseif(gsItemName == "Quantir Sewer Key") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Quantir Sewer", "Key")
		AdItem_SetProperty("Description", "A spare key that opens the sewers in the Quantir Mansion. Seems to have belonged to the serving staff.\nKey Item.")
		AdItem_SetProperty("Value", 50)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/KeySilver")
	DL_PopActiveObject()
	gbFoundItem = true

--Plot-crucial item. Found in the Quantir Mansion.
elseif(gsItemName == "Rilmani Language Guide") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Rilmani", "Language Guide")
		AdItem_SetProperty("Description", "Created by the Heavenly Doves researchers, this guide shows some Rilmani symbols and can be used to translate their language.\nKey Item.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/BookOrange")
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Decayed Bee Nectar") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Decayed", "Bee Nectar")
		AdItem_SetProperty("Description", "A jar of nectar collected by the bees that decayed because it went too long without processing. The bees don't seem to want it.\nKey Item.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarYellow")
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Hypnotic Flower Petals") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Hyptnotic Flower", "Petals")
		AdItem_SetProperty("Description", "Petals from a flower. When smelled, they put a person to sleep fairly quickly.\nKey Item.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/FlowerWhite")
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Gaardian Cave Moss") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Gaardian Cave", "Moss")
		AdItem_SetProperty("Description", "It'd take a leap of faith to believe this moss could make anything tasty...\nKey Item.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarGreen")
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Booped Paper") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Paper that has been booped against a dog's nose. No, it doesn't make any sense to me either. Roll with it.\nKey Item.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/BookBlue")
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Translucent Quantirian Salami") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Name Lines", "Translucent", "Quantirian Salami")
		AdItem_SetProperty("Description", "Despite it's name, not made out of Quantir residents. It's actually pork. You can see through it partially...\nKey Item.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarPurple")
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Kokayanee") then
	AdInv_CreateItem(gsItemName)
	
		local iRoll = LM_GetRandomNumber(0, 99)
		if(iRoll < 50) then
			AdItem_SetProperty("Description", "When you want to get down, get right down on the ground, Kokayanee.\nKey Item.")
		else
			AdItem_SetProperty("Description", "Kokayanee is a hell of a drug.\nKey Item.")
		end
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmPowder")
	DL_PopActiveObject()
	gbFoundItem = true

--Credits Chip. Provides 30 Work Credits when cashed in.
elseif(gsItemName == "Credits Chip") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "30 Work Credits, good only in Regulus City. Insert this into a Work Terminal and the credits will be added to your account.\nKey Item.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
        AdItem_SetProperty("Is Stackable")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ComputerChip")
	DL_PopActiveObject()
    AdInv_SetProperty("Stack Items")
	gbFoundItem = true

end