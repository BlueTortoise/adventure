--[Repeatable Quest Items]
--These items are turned in for various quests to get other things. They can be turned in multiple times.
	
--Recycleable junk. Tellurium Mines.
if(gsItemName == "Recycleable Junk") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Junk which could probably be melted down to recover some usable metal.\nKey Item (4 rep).")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Is Key Item", true)
        AdItem_SetProperty("Is Stackable")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JunkB")
	DL_PopActiveObject()
    AdInv_SetProperty("Stack Items")
	gbFoundItem = true
	
--Assorted parts. Tellurium Mines.
elseif(gsItemName == "Assorted Parts") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Various parts which could probably be re-used.\nKey Item (6 credits).")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Is Key Item", true)
        AdItem_SetProperty("Is Stackable")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JunkB")
	DL_PopActiveObject()
    AdInv_SetProperty("Stack Items")
	gbFoundItem = true
	
--Bent Tools. Tellurium Mines.
elseif(gsItemName == "Bent Tools") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Damaged tools. Could probably be given to someone who could repair them.\nKey Item (6 rep).")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Is Key Item", true)
        AdItem_SetProperty("Is Stackable")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JunkB")
	DL_PopActiveObject()
    AdInv_SetProperty("Stack Items")
	gbFoundItem = true

end