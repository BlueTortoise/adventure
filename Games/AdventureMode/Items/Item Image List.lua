--[Item Image List]
--Associates item names with images. Note that this is not necessarily the image that the item will
-- use when it's in the inventory, this is the image used when the item does not exist yet or can't
-- possibly exist. For example, Adamantite is not a proper item but it does have an icon.
--This list is used for cases like the Victory UI, where the item's image is needed but it is not
-- actually created yet.
--If the item name match is not found, then "Null" is used and no image should be displayed.

--Argument Listing:
-- 0: sItemName - The item's name.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(LM_GetCallStack(0) .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sItemName = LM_GetScriptArgument(0)

--[ ========================================== Builder ========================================== ]
--On the first run of this script, build the image listing.
if(gbHasBuiltImageRemaps == nil) then
	
	--[Setup]
	--Flag.
	gbHasBuiltImageRemaps = true
	
	--Table.
	giImageRemapsTotal = 0
	gzImageRemapTable = {}
	
	--Adder function.
	local fnAddImageRemap = function(sName, sPath)
		giImageRemapsTotal = giImageRemapsTotal + 1
		gzImageRemapTable[giImageRemapsTotal] = {sName, sPath}
	end
	
	--[Adamantite and Platina]
	fnAddImageRemap("Platina",           "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Adamantite Powder", "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Adamantite Flakes", "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Adamantite Shard",  "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Adamantite Piece",  "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Adamantite Chunk",  "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Adamantite Ore",    "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	
	--[Mei's Items]
	fnAddImageRemap("Silver Runestone",       "Root/Images/AdventureUI/ItemIcons/RunestoneMei")
	fnAddImageRemap("Rusty Katana",           "Root/Images/AdventureUI/ItemIcons/WeaponMei")
	fnAddImageRemap("Steel Katana",           "Root/Images/AdventureUI/ItemIcons/WeaponMei")
	fnAddImageRemap("Serrated Katana",        "Root/Images/AdventureUI/ItemIcons/WeaponMei")
	fnAddImageRemap("Rusty Jian",             "Root/Images/AdventureUI/ItemIcons/WeaponMei")
	fnAddImageRemap("Steel Jian",             "Root/Images/AdventureUI/ItemIcons/WeaponMei")
	fnAddImageRemap("Mei's Work Uniform",     "Root/Images/AdventureUI/ItemIcons/ArmorLight")
	fnAddImageRemap("Fencer's Raiment",       "Root/Images/AdventureUI/ItemIcons/ArmorLight")
	fnAddImageRemap("Arm Brace",              "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	fnAddImageRemap("Alacrity Bracer",        "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	fnAddImageRemap("Haemophiliac's Pendant", "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	
	--[Florentina's Items]
	fnAddImageRemap("Florentina's Pipe", "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	fnAddImageRemap("Hunting Knife",     "Root/Images/AdventureUI/ItemIcons/WeaponFlorentina")
	fnAddImageRemap("Butterfly Knife",   "Root/Images/AdventureUI/ItemIcons/WeaponFlorentina")
	fnAddImageRemap("Flowery Tunic",     "Root/Images/AdventureUI/ItemIcons/ArmorLight")
	fnAddImageRemap("Decorative Bracer", "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	fnAddImageRemap("Jade Eye Ring",     "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	
	--[Christine's Items]
	fnAddImageRemap("Violet Runestone",         "Root/Images/AdventureUI/ItemIcons/RunestoneChristine")
	fnAddImageRemap("Tazer",                    "Root/Images/AdventureUI/ItemIcons/WeaponChristine")
	fnAddImageRemap("Schoolmaster's Suit",      "Root/Images/AdventureUI/ItemIcons/ArmorLight")
	fnAddImageRemap("Carbonweave Electrospear", "Root/Images/AdventureUI/ItemIcons/WeaponChristine")
	fnAddImageRemap("Flowing Dress",            "Root/Images/AdventureUI/ItemIcons/ArmorMedium")
	fnAddImageRemap("Kinetic Capacitor",        "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	fnAddImageRemap("Viewfinder Module",        "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	fnAddImageRemap("Sure-Grip Gloves",         "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	
	--[55's Items]
	fnAddImageRemap("Pulse Diffractor",         "Root/Images/AdventureUI/ItemIcons/Weapon55")
	fnAddImageRemap("Command Unit Garb",        "Root/Images/AdventureUI/ItemIcons/ArmorLight")
	fnAddImageRemap("Wide-Spectrum Scanner",    "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	fnAddImageRemap("Pulse Radiation Dampener", "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
	
	--[Healing Items]
	fnAddImageRemap("Healing Tincture",  "Root/Images/AdventureUI/ItemIcons/ItemPotionRed")
	fnAddImageRemap("Palliative",        "Root/Images/AdventureUI/ItemIcons/ItemPotionRed")
	fnAddImageRemap("Pepper Pie",        "Root/Images/AdventureUI/ItemIcons/ItemPotionRed")
	fnAddImageRemap("Nanite Injection",  "Root/Images/AdventureUI/ItemIcons/ItemPotionRed")
	fnAddImageRemap("Explication Spike", "Root/Images/AdventureUI/ItemIcons/ItemPotionRed")
	fnAddImageRemap("Regeneration Mist", "Root/Images/AdventureUI/ItemIcons/ItemPotionRed")
	
	--[Light Armors]
	fnAddImageRemap("Dispersion Cloak",    "Root/Images/AdventureUI/ItemIcons/ArmorLight")
	fnAddImageRemap("Adaptive Cloth Vest", "Root/Images/AdventureUI/ItemIcons/ArmorLight")
	
	--[Medium Armors]
	fnAddImageRemap("Light Leather Vest", "Root/Images/AdventureUI/ItemIcons/ArmorMedium")
	fnAddImageRemap("Ceramic Weave Vest", "Root/Images/AdventureUI/ItemIcons/ArmorMedium")
	
	--[Heavy Armors]
	fnAddImageRemap("Neutronium Jacket", "Root/Images/AdventureUI/ItemIcons/ArmorHeavy")
	
	--[Accessories]
	fnAddImageRemap("Distribution Frame", "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
    
    --[Gems]
	fnAddImageRemap("Yemite Gem",     "Root/Images/AdventureUI/ItemIcons/GemYellow")
	fnAddImageRemap("Rubose Gem",     "Root/Images/AdventureUI/ItemIcons/GemPurple")
	fnAddImageRemap("Glintsteel Gem", "Root/Images/AdventureUI/ItemIcons/GemBlue")
	fnAddImageRemap("Ardrite Gem",    "Root/Images/AdventureUI/ItemIcons/GemPink")
	fnAddImageRemap("Blurleen Gem",   "Root/Images/AdventureUI/ItemIcons/GemYellow")
	fnAddImageRemap("Qederite Gem",   "Root/Images/AdventureUI/ItemIcons/GemPurple")
	
	--[Miscellaneous Items]
	fnAddImageRemap("Tattered Rags", "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Ruined Armor",  "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Broken Spear",  "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Emerald",       "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Aquamarine",    "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Amethyst",      "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	
	--[Key Items]
	fnAddImageRemap("Pry Bar",                       "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Hacksaw",                       "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Boat Oars",                     "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Quantir Mansion Key",           "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Quantir Sewer Key",             "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Rilmani Language Guide",        "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Decayed Bee Nectar",            "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Hypnotic Flower Petals",        "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Gaardian Cave Moss",            "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Booped Paper",                  "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Translucent Quantirian Salami", "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Kokayanee",                     "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Credits Chip",                  "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	
	--[Repeatable Quest Items]
	fnAddImageRemap("Recycleable Junk", "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Assorted Parts",   "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
	fnAddImageRemap("Bent Tools",       "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
end
	

--[ ========================================= Resolver ========================================== ]
--If the item name is deliberately "Null" just return Null without an error.
if(sItemName == "Null") then
	AdInv_SetProperty("Item Image Path", "Null")
	return
end

--Check if the item has a quantity set after it. If so, remove that.
local iLen = string.len(sItemName)
local tSlot = iLen-1
while(tSlot > 1) do
	if(string.sub(sItemName, tSlot, tSlot) == "x") then
		local iNumber = string.byte(sItemName, tSlot + 1)
		if(iNumber >= string.byte("0") and iNumber <= string.byte("9")) then
			sItemName = string.sub(sItemName, 1, tSlot-2)
			break
		end
	end
	tSlot = tSlot - 1
end

--Check if the item has a (+X) case. Remove that.
tSlot = iLen-1
while(tSlot > 1) do
	if(string.sub(sItemName, tSlot, tSlot+1) == "(+") then
		sItemName = string.sub(sItemName, 1, tSlot-2)
		break
	end
	tSlot = tSlot - 1
end

--Run across the list and resolve the appropriate path.
for i = 1, giImageRemapsTotal, 1 do
	if(gzImageRemapTable[i][1] == sItemName) then
		AdInv_SetProperty("Item Image Path", gzImageRemapTable[i][2])
		return
	end
end

--No matches.
AdInv_SetProperty("Item Image Path", "Null")
Debug_ForcePrint(LM_GetCallStack(0) .. ": Error, no item " .. sItemName .. " found.\n")
