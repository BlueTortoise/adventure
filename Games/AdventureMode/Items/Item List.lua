--[Item Listing]
--Contains a list of all the items in the game. Call this script with the name of the item you want and the
-- item will be added to the player's inventory.
--Subscripts use Lua globals for efficiency (there is no need to re-declare script arguments since they are shared).

--Argument Listing:
-- 0: sItemName - What item to add.
-- 1: bBypassAdamantite - Optional. If this argument is true, the Adamantite entry will be created. Used for shops.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
gsItemName = LM_GetScriptArgument(0)
gfBypassAdamantite = 0.0
if(iArgs >= 2) then gfBypassAdamantite = tonumber(LM_GetScriptArgument(1)) end

--[ ==================================== Subscript Iteration ==================================== ]
--Global flag, set to false. Stop execution as soon as one of the subscripts sets it to true.
local sBasePath = fnResolvePath()
gbFoundItem = false

--[Name Building]
local sStrippedString = fnStripRankBonus(gsItemName)
local sOriginalString = gsItemName

--[Call Subscripts]
--Variables.
local iTotal = 1
local saFileList = {}

--Adder function.
local fnAddFile = function(sName)
	saFileList[iTotal] = sName
	iTotal = iTotal + 1
end

--List of subscripts.
fnAddFile("100 Adamantite and Platina")
fnAddFile("101 Mei Items")
fnAddFile("102 Florentina Items")
--fnAddFile("103 Christine Items")
--fnAddFile("104 55 Items")
--fnAddFile("105 JX101 Items")
--fnAddFile("106 SX399 Items")
fnAddFile("200 Healing Items")
fnAddFile("300 Light Armors")
fnAddFile("301 Medium Armors")
fnAddFile("302 Heavy Armors")
fnAddFile("310 Accessories")
fnAddFile("320 Offense Items")
fnAddFile("900 Misc Items")
fnAddFile("901 Key Items")
fnAddFile("902 Repeatable Quest Items")

--Set the gsItemName to the stripped string.
gsItemName = sStrippedString

--Iterate across the file list.
for i = 1, iTotal-1, 1 do
	LM_ExecuteScript(sBasePath .. saFileList[i] .. ".lua")
	
	--End execution. Print debug if needed.
	if(gbFoundItem == true) then
		return
	end
end

--[Gem Case]
--Gems are the only items which use the (+1) upgrade cases. Therefore, we use the unstripped item name here.
gsItemName = sOriginalString
LM_ExecuteScript(sBasePath .. "400 Gems" .. ".lua")
if(gbFoundItem == true) then
	return
end

--[Error Case]
if(gbFoundItem == false) then
	Debug_ForcePrint("Item List: Error, no item " .. gsItemName .. "\n")
end
