--[Item Subroutines]
--Does a partial match check for equipment, ignoring the (+1) part of an equipment's name.
--Match: "Light Leather Vest (+1)" to "Light Leather Vest"
fnIsEquipmentMatch = function(psItemName, psMatchCheck)

	--Arg check.
	if(psItemName   == nil) then return false end
	if(psMatchCheck == nil) then return false end

	--Length of the check word.
	local iCheckLen = string.len(psMatchCheck)

	--Check for a match.
	if(string.sub(psItemName, 1, iCheckLen) == psMatchCheck) then
		return true
	end

	--Check failed.
	return false
end

--Returns the original string without the (+X) on the end.
fnStripRankBonus = function(psItemName)

	--Arg check.
	if(psItemName   == nil) then return "Null" end

	--Length of the string.
	local iCheckLen = string.len(psItemName)
	
	--Iterate backwards until we find the '(+' case.
	for i = iCheckLen - 1, 1, -1 do
		local sCheckString = string.sub(psItemName, i, i + 1)
		if(sCheckString == "(+") then
			return string.sub(psItemName, 1, i-2)
		end
	end
	
	--All checks passed, no boost on the end.
	return psItemName
end

--Returns the integer rank of an item from its name, or 0 if it didn't have one.
--"Light Leather Vest (+2)" returns "2" as a string. Lua will coerce to integer for you.
fnGetEquipmentRank = function(psItemName, psMatchCheck)

	--Arg check.
	if(psItemName   == nil) then return 0 end
	if(psMatchCheck == nil) then return 0 end

	--Length of the check word.
	local iCheckLen = string.len(psMatchCheck)

	--Exact match is +0.
	if(psItemName == psMatchCheck) then return 0 end

	--Get the (+x) value. +10 is the nominal maximum.
	local sBoostString = string.sub(psItemName, iCheckLen+4, -2)
	
	return sBoostString
end

--[fnCreateEquipmentPack]
--Creates and returns a set of tables that store all the values used for equipment items. All the slots
-- default to 0, except the description.
fnCreateEquipmentPack = function(piRanks)
	
	--If ranks is ommitted or invalid, set it to 1.
	if(piRanks == nil or piRanks < 1) then piRanks = 1 end
	
	--Storage object.
	local zEqpPrototype = {}
	zEqpPrototype.saDescriptions = {}
		
	--Value Tables
	zEqpPrototype.iaValues = {} --Cash Value
	zEqpPrototype.iaDamVls = {} --Damage
	zEqpPrototype.iaSpdVls = {} --Speed
	zEqpPrototype.iaPrtVls = {} --Protection
	zEqpPrototype.iaEvdVls = {} --Evade
	zEqpPrototype.iaAcyVls = {} --Accuracy
	zEqpPrototype.iaIniVls = {} --Initiative
	
	--Effects
	zEqpPrototype.iaStnDamVls = {} --Stun Damage
	zEqpPrototype.iaStnEffVls = {} --Stun Effect
	zEqpPrototype.iaBlnDamVls = {} --Blind Damage
	zEqpPrototype.iaBlnEffVls = {} --Blind Effect
	zEqpPrototype.iaBldDamVls = {} --Bleed Damage
	zEqpPrototype.iaPsnDamVls = {} --Poison Damage
	zEqpPrototype.iaCrdDamVls = {} --Corrosion Damage
	zEqpPrototype.iaTerDamVls = {} --Terrify Damage
	for i = 1, piRanks, 1 do
		zEqpPrototype.iaValues[i] = 0
		zEqpPrototype.iaDamVls[i] = 0
		zEqpPrototype.iaSpdVls[i] = 0
		zEqpPrototype.iaPrtVls[i] = 0
		zEqpPrototype.iaEvdVls[i] = 0
		zEqpPrototype.iaAcyVls[i] = 0
		zEqpPrototype.iaIniVls[i] = 0
		
		--Effects
		zEqpPrototype.iaStnDamVls[i] = 0
		zEqpPrototype.iaStnEffVls[i] = 0
		zEqpPrototype.iaBlnDamVls[i] = 0
		zEqpPrototype.iaBlnEffVls[i] = 0
		zEqpPrototype.iaBldDamVls[i] = 0
		zEqpPrototype.iaPsnDamVls[i] = 0
		zEqpPrototype.iaCrdDamVls[i] = 0
		zEqpPrototype.iaTerDamVls[i] = 0
	end
	
	--Return this table.
	return zEqpPrototype
end
