-- |[ ==================================== Resolve Gem Name ==================================== ]|
--Script called by the AdventureInventory with a gem in the activity stack. The script will figure
-- out which name it should have based on its color.
--Typically called after a merge action, or by the load handler.

--Get color.
local iColor = AdItem_GetProperty("Gem Colors")

--Split it up by binaries.
local sFinalName = AdItem_GetProperty("Base Gem Name")
local sStartName = sFinalName
local bIsGry = (bit32.band(iColor, gciGemColor_Grey) > 0)
local bIsRed = (bit32.band(iColor, gciGemColor_Red) > 0)
local bIsBlu = (bit32.band(iColor, gciGemColor_Blue) > 0)
local bIsOrn = (bit32.band(iColor, gciGemColor_Orange) > 0)
local bIsVlt = (bit32.band(iColor, gciGemColor_Violet) > 0)
local bIsYlw = (bit32.band(iColor, gciGemColor_Yellow) > 0)

-- |[ ==================================== Gem Descriptions ==================================== ]|
--A one-step gem does not change its name at all.

--A two-step gem always has a unique name.

--A three-step gem is Trinitite if it contains Red and Blu gems.
--A three-step gem is Klaymonite if it contains Orn and Vlt gems.
--A three-step gem is Fourmonite if it contains Ylw and Gry gems.
--Three-step gems not falling into an above category are Fosterite.
--If there is a conflict between these, they resolve in order Trinitite, Klaymonite, Fourmonite.

--A four-step gem is Quatemor if it contains Red, Blu, and Gry gems.
--A four-step gem is Repemor if it contains Orn, Vlt, and Ylw gems.
--Four-step gems not falling into an above category are Sapromor.
--If there is a conflict between these, they resolve in order Quatemor, Repemor.

--A five step gem is Niolern if it does not contain a Gry gem.
--A five step gem is Arulern if it does not contain a Red gem.
--A five step gem is Mizatern if it does not contain a Blu gem.
--A five step gem is Youngetern if it does not contain an Orn gem.
--A five step gem is Qeferine if it does not contain an Vlt gem.
--A five step gem is Pyrrotern if it does not contain an Ylw gem.

--The six-step gem is always called a Demona gem.

-- |[ ======================================== Grey Gems ======================================= ]|
if(bIsGry) then
    
    -- |[Red Containing]|
    if(bIsRed) then
    
        --[Blue Containing]
        if(bIsBlu) then
    
            --[Orange Containing]
            if(bIsOrn) then
            
                --Violet Containing
                if(bIsVlt) then
                
                    --Yellow Containing
                    if(bIsYlw) then
                        sFinalName = "Demona Gem"
                    else
                        sFinalName = "Pyrrotern Gem (GRBOV)"
                    end
                
                --Yellow Containing
                elseif(bIsYlw) then
                    sFinalName = "Qeferine Gem (GRBOY)"
                else
                    sFinalName = "Quatemor Gem (GRBO)"
                end
            
            --[Violet Containing]
            elseif(bIsVlt) then
                
                --Yellow Containing
                if(bIsYlw) then
                    sFinalName = "Youngetern (GRBVY)"
                else
                    sFinalName = "Quatemor Gem (GRBV)"
                end
            
            --[Yellow Containing]
            elseif(bIsYlw) then
                sFinalName = "Quatemor Gem (GRBY)"
            else
                sFinalName = "Trinitite Gem (GRB)"
            end
        
        --[Orange Containing]
        elseif(bIsOrn) then
    
            --Violet Containing
            if(bIsVlt) then
            
                --Yellow Containing
                if(bIsYlw) then
                    sFinalName = "Mizatern Gem (GROVY)"
                else
                    sFinalName = "Sapromor Gem (GROV)"
                end
            
            --Yellow Containing
            elseif(bIsYlw) then
                sFinalName = "Sapromor Gem (GROY)"
            else
                sFinalName = "Fosterite Gem (GRO)"
            end
        
        --[Violet Containing]
        elseif(bIsVlt) then
    
            --Yellow Containing
            if(bIsYlw) then
                sFinalName = "Sapromor Gem (GRVY)"
            else
                sFinalName = "Fosterite Gem (GRV)"
            end
        
        --[Yellow Containing]
        elseif(bIsYlw) then
            sFinalName = "Fourmonite Gem (GRY)"
        else
            sFinalName = "Sessatine Gem (GR)"
        end
    
    -- |[Blue Containing]|
    elseif(bIsBlu) then
    
        --[Orange Containing]
        if(bIsOrn) then
        
            --Violet Containing
            if(bIsVlt) then
            
                --Yellow Containing
                if(bIsYlw) then
                    sFinalName = "Arulern Gem (GBOVY)"
                else
                    sFinalName = "Sapromor Gem (GBOV)"
                end
            
            --Yellow Containing
            elseif(bIsYlw) then
                sFinalName = "Sapromor Gem (GBOY)"
            else
                sFinalName = "Fosterite Gem (GBO)"
            end
        
        --[Violet Containing]
        elseif(bIsVlt) then
            
            --Yellow Containing
            if(bIsYlw) then
                sFinalName = "Sapromor Gem (GBVY)"
            else
                sFinalName = "Fosterite Gem (GBV)"
            end
        
        --[Yellow Containing]
        elseif(bIsYlw) then
            sFinalName = "Fourmonite Gem (GBY)"
        else
            sFinalName = "Emorfen Gem (GB)"
        end
    
    -- |[Orange Containing]|
    elseif(bIsOrn) then
    
        --[Violet Containing]
        if(bIsVlt) then
        
            --Yellow Containing
            if(bIsYlw) then
                sFinalName = "Repemor Gem (GOVY)"
            else
                sFinalName = "Klaymonite Gem (GOV)"
            
            end
        
        --[Yellow Containing]
        elseif(bIsYlw) then
            sFinalName = "Fourmonite Gem (GOY)"
        else
            sFinalName = "Gresshite Gem (GO)"
        end
    
    -- |[Violet Containing]|
    elseif(bIsVlt) then
    
        --[Yellow Containing]
        if(bIsYlw) then
            sFinalName = "Fourmonite Gem (GVY)"
        else
            sFinalName = "Vivinite Gem (GV)"
        end
    
    -- |[Yellow Containing]|
    elseif(bIsYlw) then
        sFinalName = "Firatio Gem (GY)"
    end
    
-- |[ ======================================== Red Gems ======================================== ]|
elseif(bIsRed) then
    
    -- |[Blue Containing]|
    if(bIsBlu) then
    
        --[Orange Containing]
        if(bIsOrn) then
        
            --Violet Containing
            if(bIsVlt) then
            
                --Yellow Containing
                if(bIsYlw) then
                    sFinalName = "Niolern Gem (RBOVY)"
                else
                    sFinalName = "Sapromor Gem (RBOV)"
                end
            
            --Yellow Containing
            elseif(bIsYlw) then
                sFinalName = "Sapromor Gem (RBOY)"
            else
                sFinalName = "Trinitite Gem (RBO)"
            end
        
        --[Violet Containing]
        elseif(bIsVlt) then
            
            --Yellow Containing
            if(bIsYlw) then
                sFinalName = "Sapromor Gem (RBVY)"
            else
                sFinalName = "Trinitite Gem (RBV)"
            end
        
        --[Yellow Containing]
        elseif(bIsYlw) then
            sFinalName = "Trinitite Gem (RBY)"
        else
            sFinalName = "Cheomig Gem (RB)"
        end
    
    -- |[Orange Containing]|
    elseif(bIsOrn) then
    
        --[Violet Containing]
        if(bIsVlt) then
        
            --Yellow Containing
            if(bIsYlw) then
                sFinalName = "Repemor Gem (ROVY)"
            else
                sFinalName = "Klaymonite Gem (ROV)"
            end
        
        --[Yellow Containing]
        elseif(bIsYlw) then
            sFinalName = "Fosterite Gem (ROY)"
        else
            sFinalName = "Posrug Gem (RO)"
        end
    
    
    -- |[Violet Containing]|
    elseif(bIsVlt) then
        
        --[Yellow Containing]
        if(bIsYlw) then
            sFinalName = "Fosterite Gem (RVY)"
        else
            sFinalName = "Fillatem Gem (RV)"
        end
    
    -- |[Yellow Containing]|
    elseif(bIsYlw) then
        sFinalName = "Wotterite Gem (RY)"
    end
    
-- |[ ======================================= Blue Gems ======================================== ]|
elseif(bIsBlu) then
    
    -- |[Orange Containing]|
    if(bIsOrn) then
    
        --[Violet Containing]
        if(bIsVlt) then
        
            --Yellow Containing
            if(bIsYlw) then
                sFinalName = "Repemor Gem (BOVY)"
            else
                sFinalName = "Klaymonite Gem (BOV)"
            end
        
        --[Yellow Containing]
        elseif(bIsYlw) then
            sFinalName = "Fosterite Gem (BOY)"
        else
            sFinalName = "Arthirmite Gem (BO)"
        end
    
    -- |[Violet Containing]|
    elseif(bIsVlt) then
        
        --[Yellow Containing]
        if(bIsYlw) then
            sFinalName = "Fosterite Gem (BVY)"
        else
            sFinalName = "Rootite Gem (BV)"
        end
    
    -- |[Yellow Containing]|
    elseif(bIsYlw) then
        sFinalName = "Crifom Gem (BY)"
    end
    
-- |[ ====================================== Orange Gems ======================================= ]|
elseif(bIsOrn) then
    
    -- |[Violet Containing]|
    if(bIsVlt) then
    
        --[Yellow Containing]
        if(bIsYlw) then
            sFinalName = "Klaymonite Gem (OVY)"
        else
            sFinalName = "Tynagen Gem (OV)"
        end
    
    -- |[Yellow Containing]|
    elseif(bIsYlw) then
        sFinalName = "Jotarite Gem (OY)"
    end
    
-- |[ ======================================= Violet Gems ====================================== ]|
elseif(bIsVlt) then
    
    -- |[Yellow Containing]|
    if(bIsYlw) then
        sFinalName = "Xenimine Gem (VY)"
    end
end

-- |[ ======================================== Finish Up ======================================= ]|
AdItem_SetProperty("Name", sFinalName)
if(sFinalName ~= sStartName) then
    
    --Generate a gem description automatically.
    AdItem_SetProperty("Autogenerate Gem Description")
    
    --Figure out which gem icon to use.
    local iSubgems = 0
    local iBraceS = string.find(sFinalName, "(", 1, true)
    local iBraceE = string.find(sFinalName, ")", 1, true)
    
    --No braces means no subgems.
    if(iBraceS == nil or iBraceE == nil) then
        if(sFinalName == "Demona Gem") then
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem6")
        elseif(bIsGry) then
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|G")
        elseif(bIsRed) then
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|R")
        elseif(bIsBlu) then
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|B")
        elseif(bIsOrn) then
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|O")
        elseif(bIsVlt) then
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|V")
        elseif(bIsYlw) then
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|Y")
        end
    
    --If the parenthesis are found, then the difference between them is how many subgems there are.
    elseif(iBraceE - iBraceS == 3) then
        AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem2|" .. string.sub(sFinalName, iBraceS+1, iBraceE-1))
    
    --Three.
    elseif(iBraceE - iBraceS == 4) then
        AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem3|" .. string.sub(sFinalName, iBraceS+1, iBraceE-1))
    
    --Four.
    elseif(iBraceE - iBraceS == 5) then
        AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem4|" .. string.sub(sFinalName, iBraceS+1, iBraceE-1))
    
    --Five.
    elseif(iBraceE - iBraceS == 6) then
        AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem5|" .. string.sub(sFinalName, iBraceS+1, iBraceE-1))
    
    --Error case.
    else
        AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Gem1|G")
    end
end
