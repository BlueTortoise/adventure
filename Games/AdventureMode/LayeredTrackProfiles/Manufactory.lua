--[Layered Track: Manufactory]
--Basically the normal Regulus City track, with an extra factory sound track. Gets louder as the player gets closer to the machinery.
local iTracksTotal = 2
local saTrackNames = {"RegulusCity", "FactoryFloor"}

--Set this flag to indicate we handled setting layered tracks.
AL_SetProperty("Is Layering Music", true)

--Set the tracks and names.
AL_SetProperty("Total Layering Tracks", iTracksTotal)
for i = 0, iTracksTotal-1, 1 do
    AL_SetProperty("Layered Track Name", i, saTrackNames[i+1])
end

--[Track Volumes]
--Setup
local faVolumeList = {}
faVolumeList[0] = {}
faVolumeList[1] = {}

--[Track Zero]
--The volume is 100% for the 0th track at all times.
for iIntensity = 0, 100, 1 do
    faVolumeList[0][iIntensity] = 1.0
end

--[Track One]
--Volume increases by intensity.
for iIntensity = 0, 100, 1 do
    faVolumeList[1][iIntensity] = iIntensity
end


--[Volume Normalization and Input]
--Normalization does not occur in this case. Just input directly.
for iIntensity = 0, 100, 1 do
    for i = 0, iTracksTotal-1, 1 do
        AL_SetProperty("Layered Track Volume At Intensity", i, iIntensity, faVolumeList[i][iIntensity])
    end
end
