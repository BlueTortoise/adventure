--[Layered Track: Some Moths Do, Boating]
--Some Moths Do movie music, plays during the big finale.
local iTracksTotal = 3
local saTrackNames = {"Moth Speedboat", "Moth Intense", "DummyTrack"}

--Set this flag to indicate we handled setting layered tracks.
AL_SetProperty("Is Layering Music", true)

--Set the tracks and names.
AL_SetProperty("Total Layering Tracks", iTracksTotal)
for i = 0, iTracksTotal-1, 1 do
    AL_SetProperty("Layered Track Name", i, saTrackNames[i+1])
end

--[Description]
--Use the standard volume layering techniques.
LM_ExecuteScript(fnResolvePath() .. "ZStandardVolumeLayering.lua", 1, 1)
