--[Layered Track: Some Moths Do, Normal]
--Some Moths Do movie music. Upper track is silence, isn't reached normally.
local iTracksTotal = 3
local saTrackNames = {"Moth Passive", "Moth Intense", "DummyTrack"}

--Set this flag to indicate we handled setting layered tracks.
AL_SetProperty("Is Layering Music", true)

--Set the tracks and names.
AL_SetProperty("Total Layering Tracks", iTracksTotal)
for i = 0, iTracksTotal-1, 1 do
    AL_SetProperty("Layered Track Name", i, saTrackNames[i+1])
end

--[Description]
--Use the standard volume layering techniques.
LM_ExecuteScript(fnResolvePath() .. "ZStandardVolumeLayering.lua", 1, 1)
