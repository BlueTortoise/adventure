--[Layered Track Routing]
--Controls music profile names. When a layered track is being set for a level, the music name will be "LAYER|TrackName".
-- When the LAYER| tag is spotted, the TrackName is passed to this file which handles the rest of the setup.
--The location of this script is stored in the global gsLayeredTrackRouter.
if(fnArgCheck(1) == false) then return end

--Name of thr track profile
local sProfileName = LM_GetScriptArgument(0)

--Cultist track.
if(sProfileName == "CultistProfile") then
    LM_ExecuteScript(fnResolvePath() .. "CultistProfile.lua")

--The Sunrise Gala
elseif(sProfileName == "EmbassyFunction") then
    LM_ExecuteScript(fnResolvePath() .. "GalaProfile.lua")

--LRT Facility
elseif(sProfileName == "Telecomm") then
    LM_ExecuteScript(fnResolvePath() .. "TelecommProfile.lua")
    
--Manufactory.
elseif(sProfileName == "Manufactory") then
    LM_ExecuteScript(fnResolvePath() .. "Manufactory.lua")

--Some Moths Do, Normal
elseif(sProfileName == "SomeMothsDo") then
    LM_ExecuteScript(fnResolvePath() .. "SomeMothsDoNormal.lua")

--Some Moths Do, Boating Finale
elseif(sProfileName == "SomeMothsBoat") then
    LM_ExecuteScript(fnResolvePath() .. "SomeMothsDoBoat.lua")
    
--Sleep.
elseif(sProfileName == "Sleep") then
    LM_ExecuteScript(fnResolvePath() .. "Sleep.lua")

--Unresolved. Print an error.
else
    io.write("Error: Unable to identify layered track " .. sProfileName .. "\n")
end