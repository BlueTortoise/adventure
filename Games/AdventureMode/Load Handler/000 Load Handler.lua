-- |[ ====================================== Load Handler ====================================== ]|
--If the game is being loaded as opposed to booted into chapter 1, this script will be called after
-- loading has finished.
gbLoadDebug = false
Debug_PushPrint(gbLoadDebug, "Beginning Load Handler.\n")

--[Chapter Variable]
--This variable specifies which chapter the save was. It can be 0 or 1 for Chapter 1, otherwise
-- it's exactly equal to the chapter.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
Debug_Print("Chapter-specific loaders: " .. iCurrentChapter .. ".\n")

-- |[ =================================== Chapter 1 Handler ==================================== ]|
if(iCurrentChapter == 0.0 or iCurrentChapter == 1.0) then
    
    -- |[Debug]|
    Debug_Print("Running chapter 1 load.\n")

    -- |[Maps]|
    --Re-run the map lookup builder.
    LM_ExecuteScript(gsRoot .. "Maps/Z Map Lookups/Chapter 1 Lookups.lua")
    local sCurrentLevel = AL_GetProperty("Name")
	fnResolveMapLocation(sCurrentLevel)
    
    --Remove the map if Mei hasn't found it yet.
	local iHasNoMap = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasNoMap", "N")
	if(iHasNoMap == 1.0) then
		AM_SetMapInfo("Null", "Null", 0, 0)
	end

    -- |[Other Options]|
    --Place them here.

    -- |[Party Members]|
    --Clear the active party.
    Debug_Print("Booting party members.\n")
    AdvCombat_SetProperty("Clear Party")
    
    --Load new variables for all party members.
    local iRosterSize = VM_GetVar("Root/Saving/Combat/Base/iRosterSize", "N")
    for i = 0, iRosterSize-1, 1 do
        
        --Get the character.
        Debug_Print(" Character: " .. i .. "\n")
        local sCharName = VM_GetVar("Root/Saving/Combat/Party/sCharacter"..i.."Name",  "S")
        Debug_Print("  Name: " .. sCharName .. "\n")
    
        --If the character exists...
        if(AdvCombat_GetProperty("Does Party Member Exist", sCharName) == true) then
    
            --Get variables.
            Debug_Print("  Exists.\n")
            local fHPPercent  = VM_GetVar("Root/Saving/Combat/Party/fCharacter"..i.."HpPct", "N")
            local iExperience = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Exp",   "N")
            local iGlobalJP   = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."JP",    "N")
            local sCurrentJob = VM_GetVar("Root/Saving/Combat/Party/sCharacter"..i.."JobCur","S")
            local iTotalJobs  = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."JobsTotal", "N")
    
            --Push the entity.
            Debug_Print("  Pushing.\n")
            AdvCombat_SetProperty("Push Party Member", sCharName)
            
                --Base.
                Debug_Print("  Setting base statistics.\n")
                AdvCombatEntity_SetProperty("Current Exp", iExperience)
                AdvCombatEntity_SetProperty("Current JP", iGlobalJP)
                AdvCombatEntity_SetProperty("Active Job", sCurrentJob)
                AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
                
                --Drop all equipment.
                Debug_Print("  Dropping previous equipment.\n")
                AdvCombatEntity_SetProperty("Unequip Slot", "All")
                
                --Jobs.
                Debug_Print("  Beginning job iteration.\n")
                for p = 0, iTotalJobs-1, 1 do
                    
                    --Name of the job.
                    local sJobName = VM_GetVar("Root/Saving/Combat/Party/sCharacter"..i.."Job"..p.."Name", "S")
                    Debug_Print("  Job " .. p .. " is named " .. sJobName .. ".\n")
                    AdvCombatEntity_SetProperty("Push Job S", sJobName)
                    
                        --Base.
                        Debug_Print("   Setting job baseline.\n")
                        local iAvailableJP = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."JP",        "N")
                        local iAbilities   = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Abilities", "N")
            
                        --Set.
                        AdvCombatJob_SetProperty("JP Available", iAvailableJP)
                        
                        --For each ability...
                        Debug_Print("   Iterating " .. iAbilities .. " abilities.\n")
                        for o = 0, iAbilities, 1 do
                            
                            --Get variables.
                            local sAbilityName = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Ability"..o.."Name", "S")
                            local iIsUnlocked  = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Ability"..o.."Unlocked", "N")
                            Debug_Print("    Ability " .. o .. " is " .. sAbilityName .. ".\n")
                            
                            --Switch to boolean.
                            local bIsUnlocked = false
                            if(iIsUnlocked == 1.0) then bIsUnlocked = true end
                            
                            --Set.
                            AdvCombatJob_SetProperty("Ability Unlocked", sAbilityName, bIsUnlocked)
                            Debug_Print("    Done.\n")
                            
                        end
                    DL_PopActiveObject()
                    Debug_Print("  Done.\n")
                end
                Debug_Print("  Done job iteration.\n")
        
                --Abilities. Store abilities in the optional slots. First, block A:
                Debug_Print("  Setting abilities in blocks.\n")
                for x = gciAbility_SaveblockA_X1, gciAbility_SaveblockA_X2, 1 do
                    for y = gciAbility_SaveblockA_Y1, gciAbility_SaveblockA_Y2, 1 do
                        local sAbilityName = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Ability"..x..y.."Name", "S")
                        AdvCombatEntity_SetProperty("Set Ability Slot", x, y, sAbilityName)
                    end
                end
                for x = gciAbility_SaveblockB_X1, gciAbility_SaveblockB_X2, 1 do
                    for y = gciAbility_SaveblockB_Y1, gciAbility_SaveblockB_Y2, 1 do
                        local sAbilityName = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Ability"..x..y.."Name", "S")
                        AdvCombatEntity_SetProperty("Set Ability Slot", x, y, sAbilityName)
                    end
                end
                Debug_Print("  Done Setting abilities in blocks.\n")
                
            DL_PopActiveObject()
            Debug_Print("  Done with party member.\n")
    
        --Error.
        else
            io.write("Error, party member " .. sCharName .. " does not exist.\n")
        end
        Debug_Print(" Completed.\n")
    end
    
    --Reconstruct the party.
    Debug_Print("Beginning party reconstruction.\n")
    for i = 0, gciCombat_MaxActivePartySize-1, 1 do
        
        --Get the named entity.
        local sCharacterName = VM_GetVar("Root/Saving/Combat/Base/sActiveChar"..i, "S")
        
        --Name is "Null", empty slot.
        if(sCharacterName == "Null") then
            AdvCombat_SetProperty("Party Slot", i, "Null")
        
        --Not "Null", place the character.
        else
            AdvCombat_SetProperty("Party Slot", i, sCharacterName)
        end
    end
    Debug_Print("Party reconstructed.\n")

    -- |[Followers]|
    --Dynamically tracks who is following the main character. This will add any characters who were not
    -- in the combat lineup. Characters who already were will get their field sprites created.
    Debug_Print("Placing followers.\n")
    gsPartyLeaderName = VM_GetVar("Root/Saving/Followers/Base/sLeader", "S")

    --For each follower, get their name.
    local iFollowersToRead = VM_GetVar("Root/Saving/Followers/Base/iFollowers", "N")
    for i = 0, iFollowersToRead-1, 1 do
        
        --Get variables.
        local sFollowerName   = VM_GetVar("Root/Saving/Followers/Base/sFollower"..i.."Name", "S")
        local sFollowVariable = VM_GetVar("Root/Saving/Followers/Base/sFollower"..i.."Variable", "S")
        
        --Run a function to create them.
        fnAddPartyMember(sFollowerName)
    end
    
    -- |[Dump All Items]|
    --Characters may have dumped their starting equipment into the inventory. Clear those off here.
    Debug_Print("Clearing inventory.\n")
    AdInv_SetProperty("Clear")
    
    -- |[Catalysts]|
    --Must be tracked before we call the form setter, since that's where stats are computed.
    local iHealthCatalyst     = VM_GetVar("Root/Saving/Inventory/Base/iCatalystHealth", "N")
    local iAttackCatalyst     = VM_GetVar("Root/Saving/Inventory/Base/iCatalystAttack", "N")
    local iInitiativeCatalyst = VM_GetVar("Root/Saving/Inventory/Base/iCatalystInitiative", "N")
    local iDodgeCatalyst      = VM_GetVar("Root/Saving/Inventory/Base/iCatalystEvade", "N")
    local iAccuracyCatalyst   = VM_GetVar("Root/Saving/Inventory/Base/iCatalystAccuracy", "N")
    local iSkillCatalyst      = VM_GetVar("Root/Saving/Inventory/Base/iCatalystSkill", "N")
    AdInv_SetProperty("Catalyst Count", gciCatalyst_Health,     iHealthCatalyst)
    AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack,     iAttackCatalyst)
    AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, iInitiativeCatalyst)
    AdInv_SetProperty("Catalyst Count", gciCatalyst_Dodge,      iDodgeCatalyst)
    AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy,   iAccuracyCatalyst)
    AdInv_SetProperty("Catalyst Count", gciCatalyst_Skill,      iSkillCatalyst)
    
    --Set extra slots from skill catalysts.
    local iSlotCount = math.floor(iSkillCatalyst / gciCatalyst_Skill_Needed)
    AdvCombat_SetProperty("Set Skill Catalyst Slots", iSlotCount)

    -- |[Money]|
    local iPlatina = VM_GetVar("Root/Saving/Inventory/Base/iPlatina", "N")
    AdInv_SetProperty("Add Platina", iPlatina)

    -- |[Adamantite]|
    for i = 0, 5, 1 do
        local iAdamantite = VM_GetVar("Root/Saving/Inventory/Base/iAdamantite"..i, "N")
        AdInv_SetProperty("Crafting Material", i, iAdamantite)
    end
    
    -- |[Inventory Items]|
    --Items that are not equipped. This includes unused equipment, key items, or consumables.
    local iSocketCount = 0
    local iTotalItems = VM_GetVar("Root/Saving/Inventory/Items/iItemsTotal", "N")
    for i = 0, iTotalItems-1, 1 do
        
        --Variables.
        local sName         = VM_GetVar("Root/Saving/Inventory/Items/sItem" .. i .. "Name",         "S")
        local iQuantity     = VM_GetVar("Root/Saving/Inventory/Items/iItem" .. i .. "Quantity",     "N")
        local sInstructions = VM_GetVar("Root/Saving/Inventory/Items/sItem" .. i .. "Instructions", "S")
        local iIsGem        = VM_GetVar("Root/Saving/Inventory/Items/iItem" .. i .. "IsGem",        "N")
        
        --If the item is a gem, we may need to merge it with subgems. It may also have a different
        -- name than the stored one. Gems never have quantities over 1.
        if(iIsGem == 1.0) then
            local sOrigGemName = VM_GetVar("Root/Saving/Inventory/Items/sItem" .. i .. "GemName", "S")
            LM_ExecuteScript(gsItemListing, sOrigGemName)
            
            --Mark as the master gem. Future merge actions will merge with the master gem.
            AdInv_SetProperty("Mark Last Item As Master Gem")
        
            --Check the subgems, created by merging gems together.
            for p = 0, gciSubgemMax-1, 1 do
                local sSubgemName = VM_GetVar("Root/Saving/Inventory/Items/sItem" .. i .. "Subgem"..p.."Name", "S")
                if(sSubgemName ~= "Null" and sSubgemName ~= "NULL") then
                    LM_ExecuteScript(gsItemListing, sSubgemName)
                    AdInv_SetProperty("Merge Last Gem With Master")
                end
            end
        
            --If ordered to socket, do so here.
            if(iSocketCount > 0) then
                AdInv_SetProperty("Socket Last Item In Socket Item")
                iSocketCount = iSocketCount - 1
                if(iSocketCount == 0) then
                    AdInv_SetProperty("Clear Socket Item")
                end
            end
        
            --Clean up the master gem pointer.
            AdInv_SetProperty("Clear Master Gem")
        
        --Item is not a gem, so construct it normally.
        else
            for p = 1, iQuantity, 1 do
                LM_ExecuteScript(gsItemListing, sName)
            end
        
            --Always mark the item as the last equipped item. This is in case it actually is equipment.
            AdInv_SetProperty("Mark Last Item As Equipment")
        end
        
        --If there is an instruction, handle that here.
        local saStrings = fnSubdivide(sInstructions, "|")
        for p = 1, #saStrings, 1 do
            
            --Further subdivide.
            local saSubstrings = fnSubdivide(saStrings[p], ":")
            local iSubstringsTotal = #saSubstrings
            
            --If the 1st substring is "Equip", we're equipping this item.
            if(saSubstrings[1] == "Equip" and iSubstringsTotal >= 3) then
                AdvCombat_SetProperty("Push Party Member", saSubstrings[2]) 
                    AdvCombatEntity_SetProperty("Equip Marked Item To Slot", saSubstrings[3])
                DL_PopActiveObject()
                AdInv_SetProperty("Clear Equipment Marker")
            
            --Socket instruction. Increment the socketing stack by 1.
            elseif(saSubstrings[1] == "SocketNext") then
                AdInv_SetProperty("Mark Last Item As Socket Item")
                iSocketCount = iSocketCount + 1
            end
        end
        AdInv_SetProperty("Clear Equipment Marker")
    end

    --Clean.
    AdInv_SetProperty("Clear Equipment Marker")

    -- |[Doctor Bag]|
    --Load the charges and update the inventory.
    local iDoctorBagCharges    = VM_GetVar("Root/Variables/System/Special/iDoctorBagCharges", "N")
    local iDoctorBagChargesMax = VM_GetVar("Root/Variables/System/Special/iDoctorBagChargesMax", "N")
    AdInv_SetProperty("Doctor Bag Charges Max", iDoctorBagChargesMax)
    AdInv_SetProperty("Doctor Bag Charges", iDoctorBagCharges)
    
    -- |[Topics]|
    --Refresh topic information for each listed character.
    local iTotalTopics = VM_GetVar("Root/Saving/Topics/Base/iTotalTopics", "N")

    --For each topic:
    for i = 0, iTotalTopics - 1, 1 do
        
        --Variables.
        local sTopicName  = VM_GetVar("Root/Saving/Topics/Base/sTopic"..i.."Name",  "S")
        local iTopicLevel = VM_GetVar("Root/Saving/Topics/Base/sTopic"..i.."Level", "N")
        WD_SetProperty("Unlock Topic", sTopicName, iTopicLevel)

        --For each NPC...
        local iTopicNPCs  = VM_GetVar("Root/Saving/Topics/Base/sTopic"..i.."NPCs", "N")
        for p = 0, iTopicNPCs-1, 1 do
        
            --Variables.
            local sSubNPCName  = VM_GetVar("Root/Saving/Topics/Base/sTopic"..i.."Sub"..p.."Name",  "S")
            local iSubNPCLevel = VM_GetVar("Root/Saving/Topics/Base/iTopic"..i.."Sub"..p.."Level", "N")
        
            --Set.
            WD_SetProperty("Topic NPC Level", sTopicName, sSubNPCName, sSubNPCLevel)
        
        end
    end
    
--[ ===================================== Chapter 5 Handler ===================================== ]
else

    --Load all graphics.
    Debug_Print("Beginning load sequence.\n")
    fnIssueLoadReset("AdventureModeCH5")
    Debug_Print("Portraits.\n")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/501 Chapter 5 Portraits.lua")
    Debug_Print("Sprites.\n")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/500 Chapter 5 Sprites.lua")
    Debug_Print("Actors.\n")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/502 Chapter 5 Actors.lua")
    Debug_Print("Scenes.\n")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/503 Chapter 5 Scenes.lua")
    Debug_Print("Overlays.\n")
    LM_ExecuteScript(gsRoot .. "System/Loading Scripts/504 Chapter 5 Overlays.lua")
    Debug_Print("Closing.\n")
    SLF_Close()
    fnCompleteLoadSequence()
    Debug_Print("Completed load sequence.\n")
    
	--[Debug]
	--io.write("Chapter variable was " .. iCurrentChapter .. " indicating this is Chapter 5.\n")
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Build Scene List.lua")
	LM_ExecuteScript(gsRoot .. "Maps/Build Warp List.lua", "Chapter 5")
    
    --[Map Special]
    --Used to give the player the map in the biolabs when loading an older save.
    local iSawMusicComments = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMusicComments", "N")
    if(iSawMusicComments == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N", 1.0)
    end

	--[System Var Boot]
	--We need to run 001 Variables again, but not overwrite the variables from the Scenario. This
	-- flag ensures that won't happen.
	gbOnlyBootSystemVars = true
	LM_ExecuteScript(gsRoot .. "Chapter5Init/001 Variables.lua")
	gbOnlyBootSystemVars = false

	--[Lua Globals]
	gsPartyLeaderName = "Christine"
	AC_SetProperty("Cutscene Path", "Chapter5Scenes")
	gsStandardRetreat = gsRoot .. "Chapter5Scenes/Retreat/Scene_Begin.lua"
	AC_SetProperty("Default Retreat Path", gsStandardRetreat)
	
	--[Party and Variable Builders]
	AC_SetProperty("Cutscene Path", "Chapter5Scenes")
	gsStandardGameOver = gsRoot .. "Chapter5Scenes/Defeat_BackToSave/Scene_Begin.lua"
    
    --Re-run the map lookup builder.
    LM_ExecuteScript(gsRoot .. "Maps/Z Map Lookups/Chapter 5 Lookups.lua")
    local sCurrentLevel = AL_GetProperty("Name")
	fnResolveMapLocation(sCurrentLevel)

	--[Party Forms]
	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    
    --Leader voice.
    WD_SetProperty("Set Leader Voice", "ChrisMaleVoice")
	
	--Get Christine's form. Transform her if it's not "Human".
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(sChristineForm == "Male" or iHasGolemForm == 0.0) then
		LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Male.lua")
	elseif(sChristineForm == "Golem") then
		LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
	elseif(sChristineForm == "LatexDrone") then
		LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua")
	elseif(sChristineForm == "Electrosprite") then
		LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Electrosprite.lua")
	elseif(sChristineForm == "Darkmatter") then
		LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Darkmatter.lua")
	elseif(sChristineForm == "SteamDroid") then
		LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_SteamDroid.lua")
        
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        if(iSXUpgradeQuest == 2.0) then
            LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_SX399.lua")
        end
       
	elseif(sChristineForm == "Eldritch") then
		LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua")
	elseif(sChristineForm == "Raiju") then
        LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua")
	elseif(sChristineForm == "Doll") then
		LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Doll.lua")
	else
		LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua")
	end
    
	--[Violet Runetone]
	--Change the description of Christine's runestone, if it's not equipped, and she has the golem form.
	if(iHasGolemForm == 1.0) then
		if(AdInv_GetProperty("Item Count", "Violet Runestone") == 1) then
			AdInv_PushItem("Violet Runestone")
				AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
			DL_PopActiveObject()
		
		--Runestone was equipped so get it from Christine's inventory.
		else
			AC_PushPartyMember("Christine")
            
                --Item names.
                local sItemNameA = ACE_GetProperty("Item In Slot A")
                local sItemNameB = ACE_GetProperty("Item In Slot A")
                
                --If the runestone is in slot A:
                if(sItemNameA == "Violet Runestone") then
                    ACE_SetProperty("Equip", "Item A", "Nothing")
                elseif(sItemNameB == "Violet Runestone") then
                    ACE_SetProperty("Equip", "Item B", "Nothing")
                end
			DL_PopActiveObject()
			
            --Push and rename.
			AdInv_PushItem("Violet Runestone")
				AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
			DL_PopActiveObject()
			
            --Re-equip.
			AC_PushPartyMember("Christine")
                if(sItemNameA == "Violet Runestone") then
                    ACE_SetProperty("Equip", "Item A", "Violet Runestone")
                elseif(sItemNameB == "Violet Runestone") then
                    ACE_SetProperty("Equip", "Item B", "Violet Runestone")
                end
			DL_PopActiveObject()
		end
	end

	--Unit 2855 is a Doll. This also recomputes her stats with Catalysts.
	LM_ExecuteScript(gsRoot .. "FormHandlers/55/Form_Doll.lua")

	--[Reset]
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}

	--[55]
	--If she's in the combat party, add her variables as a following entity. Note that she does NOT spawn if she isn't following, but
	-- she can still be in the party.
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
	if(AC_GetProperty("Is Character In Party", "55") == true and iIs55Following == 1.0) then
		
		--Create her world character.
		fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)

		--Get her uniqueID. 
		EM_PushEntity("55")
			local iFollowerID = RE_GetID()
		DL_PopActiveObject()
		
		--Lua globals.
		gsFollowersTotal = gsFollowersTotal + 1
		gsaFollowerNames[gsFollowersTotal] = "55"
		giaFollowerIDs[gsFollowersTotal] = iFollowerID
		
		--Tell her to follow.
		AL_SetProperty("Follow Actor ID", iFollowerID)
	end
	
	--[JX-101]
	--Joins the party for a brief period.
	local iIsJX101Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsJX101Following", "N")
	if(AC_GetProperty("Is Character In Party", "JX-101") == true and iIsJX101Following == 1.0) then
		
		--Create her world character.
        LM_ExecuteScript(gsRoot .. "FormHandlers/JX101/Form_SteamDroid.lua")
		fnSpecialCharacter("JX101", "SteamDroid", -100, -100, gci_Face_South, false, nil)

		--Get her uniqueID. 
		EM_PushEntity("JX101")
			local iFollowerID = RE_GetID()
		DL_PopActiveObject()

		--Store it.
		gsFollowersTotal = gsFollowersTotal + 1
		gsaFollowerNames[gsFollowersTotal] = "JX-101"
		giaFollowerIDs[gsFollowersTotal] = iFollowerID
		
		--Tell her to follow.
		AL_SetProperty("Follow Actor ID", iFollowerID)
	end
    
    --[SX-399]
    --Joins the party post-gala if Tellurium Mines are completed.
    LM_ExecuteScript(gsRoot .. "FormHandlers/SX399/Form_SteamDroid.lua")
	local iSX399IsFollowing = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N")
	if(AC_GetProperty("Is Character In Party", "SX-399") == true and iSX399IsFollowing == 1.0) then
		
		--Create her world character.
		fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)

		--Get her uniqueID. 
		EM_PushEntity("SX399")
			local iFollowerID = RE_GetID()
		DL_PopActiveObject()

		--Store it.
		gsFollowersTotal = gsFollowersTotal + 1
		gsaFollowerNames[gsFollowersTotal] = "SX399"
		giaFollowerIDs[gsFollowersTotal] = iFollowerID
		
		--Tell her to follow.
		AL_SetProperty("Follow Actor ID", iFollowerID)
	end
	
	--[Sophie]
	--If on a date with Sophie, spawn her.
	local iIsOnDate            = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
    local iIsGalaTime          = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStarted55Sequence   = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted55Sequence", "N")
    local iReachedBiolabs      = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
    local iSophieLeftInBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieLeftInBiolabs", "N")
	if((iIsOnDate == 1.0 or iIsOnDate == 2.0) and iStarted55Sequence == 0.0) then
		
		--Create her world character. Frames get set during her costume resolver.
        TA_Create("Sophie")
            TA_SetProperty("Position", -10, -10)
            TA_SetProperty("Clipping Flag", false)
            TA_SetProperty("Facing", gci_Face_North)
        DL_PopActiveObject()

		--Get her uniqueID. 
		EM_PushEntity("Sophie")
			local iFollowerID = RE_GetID()
		DL_PopActiveObject()

		--Store it.
		gsFollowersTotal = gsFollowersTotal + 1
		gsaFollowerNames[gsFollowersTotal] = "Sophie"
		giaFollowerIDs[gsFollowersTotal] = iFollowerID
		
		--Tell her to follow.
		AL_SetProperty("Follow Actor ID", iFollowerID)
    
    --Sophie, in Gala Dress.
    elseif((iIsGalaTime >= 2.0 and iStarted55Sequence == 0.0) or (iReachedBiolabs == 1.0 and iSophieLeftInBiolabs == 0.0)) then
		
		--Create her world character. Frames get set during her costume resolver.
        TA_Create("Sophie")
            TA_SetProperty("Position", -10, -10)
            TA_SetProperty("Clipping Flag", false)
            TA_SetProperty("Facing", gci_Face_North)
        DL_PopActiveObject()

		--Get her uniqueID. 
		EM_PushEntity("Sophie")
			local iFollowerID = RE_GetID()
		DL_PopActiveObject()

		--Store it.
		gsFollowersTotal = gsFollowersTotal + 1
		gsaFollowerNames[gsFollowersTotal] = "Sophie"
		giaFollowerIDs[gsFollowersTotal] = iFollowerID
		
		--Tell her to follow.
		AL_SetProperty("Follow Actor ID", iFollowerID)
    
	end

	--[Lantern]
	--Activate it again if Christine has it.
	local iHasLantern = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N")
	if(iHasLantern == 1.0) then
		AL_SetProperty("Activate Player Light", 3600, 3600)
	end

	--[Take Point]
	--This ability changes properties if 55 is in the party.
	local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	if(iMet55InLowerRegulus == 1.0) then
		AC_PushPartyMember("Christine")
			ACE_PushAction("Take Point")
				ACAC_SetProperty("Visible In State", "Standard")
			DL_PopActiveObject()
		DL_PopActiveObject()
	end

	--[Leader Voice]
	--If Christine is Christine and not Chris, change the leader voice.
	if(iHasGolemForm == 1.0) then
		WD_SetProperty("Set Leader Voice", "Christine")
	end
    
    --[Portrait Changer]
    --If the player has reached the biolabs, change these portrait highlights.
    local iStarted56Sequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N")
    if(iStarted56Sequence == 1.0) then
        DialogueActor_Push("2856")
            DialogueActor_SetProperty("Remove Alias", "55")
            DialogueActor_SetProperty("Remove Alias", "2855")
        DL_PopActiveObject()
    end
    
    --[REMOVE AFTER BIG BALANCE PATCH]
    --[Costume Unlocker]
    --Because costume handling isn't done in chapters 1 and 5 yet, this will unlock costumes. First, you can change to the gala
    -- outfit after reaching the biolabs.
    if(iReachedBiolabs == 1.0) then
        VM_SetVar("Root/Variables/Costumes/Christine/iGolemGala", "N", 1.0)
        VM_SetVar("Root/Variables/Costumes/Sophie/iGolemGala", "N", 1.0)
    end
    
    --These are always unlocked even if you don't have the form yet.
    VM_SetVar("Root/Variables/Costumes/Christine/iRaijuNude", "N", 1.0)
    
    --[Costume Setter]
    --Set characters to be in their costumes when they are not party members.
    LM_ExecuteScript(gsCostumeAutoresolve, "Sophie_Golem")
end
        
--Modify 56's properties so she doesn't share dialogue with 55. This occurs after a certain scene.
local iStarted56Sequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N")
if(iStarted56Sequence >= 1.0) then
    DialogueActor_Push("2856")
        DialogueActor_SetProperty("Remove Alias", "55")
        DialogueActor_SetProperty("Remove Alias", "2855")
    DL_PopActiveObject()
end


--[ ======================================= Map Handling ======================================== ]
--Actor graphics reset.
AL_SetProperty("Reset Actor Graphics")

--[Position]
--Set this as the last save point.
local sCurrentLevel = AL_GetProperty("Name")
AL_SetProperty("Last Save Point", sCurrentLevel)

--[ ========================================== Options ========================================== ]
--Sound Settings.
local iHasSoundSettings = VM_GetVar("Root/Variables/System/Special/iHasSoundSettings", "N")
local fMusicVolume      = VM_GetVar("Root/Variables/System/Special/fMusicVolume", "N")
local fSoundVolume      = VM_GetVar("Root/Variables/System/Special/fSoundVolume", "N")
if(iHasSoundSettings ~= 0.0) then
	AudioManager_SetProperty("Music Volume", fMusicVolume)
    AudioManager_SetProperty("Sound Volume", fSoundVolume)
end

--Tourist Mode.
local iIsTouristMode = VM_GetVar("Root/Variables/System/Special/iIsTouristMode", "N")
if(iIsTouristMode == 1.0) then AC_SetProperty("Tourist Mode", true) end

--Ambient Light Boost
local fAmbientLightBoost = VM_GetVar("Root/Variables/System/Special/fAmbientLightBoost", "N")
AL_SetProperty("Ambient Light Boost", fAmbientLightBoost)

--Combat Options.
local iAutoUseDoctorBag = VM_GetVar("Root/Variables/System/Special/iAutoUseDoctorBag", "N")
local iMemoryCursor     = VM_GetVar("Root/Variables/System/Special/iMemoryCursor", "N")
if(iAutoUseDoctorBag == 0.0) then 
    AdvCombat_SetProperty("Auto Doctor Bag", false) 
else
    AdvCombat_SetProperty("Auto Doctor Bag", true) 
end
if(iMemoryCursor == 0.0) then
    AdvCombat_SetProperty("Memory Cursor", false)
else
    AdvCombat_SetProperty("Memory Cursor", true)
end
    
--Dialogue options.
local iAutoHastenDialogue = VM_GetVar("Root/Variables/System/Special/iAutoHastenDialogue", "N")
if(iAutoHastenDialogue == 1.0) then WD_SetProperty("Set AutoHasten", true) end

--Electrosprite quest is unlocked on the main menu.
local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
if(iFinished198 == 1.0) then
    OM_SetOption("Unlock Electrosprites", "1.000000")
    OM_WriteConfigFiles()
end

--[ ====================================== Map Reposition ======================================= ]
--Reposition. This will move us to the save point.
AL_BeginTransitionTo("LASTSAVEINSTANT", "Null")
AL_SetProperty("Fold Party")

--[Actor Images Reresolve]
--As above, any images that the TilemapActors were missing will get resolved here. This is set up in ZLaunch.lua.
TA_SetProperty("Reresolve All Images")
TA_SetProperty("Store Image Paths", false)

--[ ========================================== Costumes ========================================= ]
--Costumes. We need to make sure all spawned entities have their correct costumes on, which cannot be known until all the loading is done.
-- Rather than do some complex logic here, we just run the form autoresolve for all characters.
if(iCurrentChapter == 0.0 or iCurrentChapter == 1.0) then

elseif(iCurrentChapter == 5.0) then
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Human") then 
        LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Human") 
    elseif(sChristineForm == "Golem") then 
        LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Golem") 
    elseif(sChristineForm == "Raiju") then 
        LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Raiju") 
    end
    LM_ExecuteScript(gsCostumeAutoresolve, "Sophie_Golem") 
end

--[Debug]
Debug_PopPrint("Completed Load Handler.\n")
