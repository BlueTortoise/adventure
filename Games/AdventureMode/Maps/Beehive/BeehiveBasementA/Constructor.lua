--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "Apprehension")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/BeehiveBasementA/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "BeehiveBasementA")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("BeehiveBasementA")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then
	
	--[Variables]
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--[Functions]
	--Bee spawner function. NPC bees use the same properties.
	local fnSpawnBee = function(iX, iY)
		if(iX == nil or iY == nil) then return end
		
		TA_Create("BeeGirl")
			TA_SetProperty("Position", iX, iY)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Facing", LM_GetRandomNumber(gci_Face_North, gci_Face_NorthWest))
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/BeehiveBasementBee/Root.lua")
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
	end
	
	--[Cases]
	--Normal:
	if(sMeiForm ~= "Zombee") then
	
		--Add some bee NPCs. They hold position.
		fnSpawnBee(11, 20)
		fnSpawnBee(12, 22)
		fnSpawnBee(12, 23)
		fnSpawnBee(12, 19)
		
		--Variables.
		local iHasSeenBeePanicScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeePanicScene", "N")
		
		--Special rep. These beegirls move during the cutscenes.
		TA_Create("BeeGirlRepA")
			if(iHasSeenBeePanicScene == 0.0 or iHasSeenBeePanicScene == 2.0) then
				TA_SetProperty("Position", 15, 24)
				TA_SetProperty("Facing", gci_Face_North)
			elseif(iHasSeenBeePanicScene == 1.0) then
				TA_SetProperty("Position", 17, 24)
				TA_SetProperty("Facing", gci_Face_East)
			end
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/BeehiveBasementBee/Root.lua")
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
		TA_Create("BeeGirlRepB")
			if(iHasSeenBeePanicScene == 0.0 or iHasSeenBeePanicScene == 2.0) then
				TA_SetProperty("Position", 17, 22)
				TA_SetProperty("Facing", gci_Face_South)
			elseif(iHasSeenBeePanicScene == 1.0) then
				TA_SetProperty("Position", 17, 23)
				TA_SetProperty("Facing", gci_Face_East)
			end
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/BeehiveBasementBee/Root.lua")
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
		TA_Create("BeeGirlRepC")
			if(iHasSeenBeePanicScene == 0.0 or iHasSeenBeePanicScene == 2.0) then
				TA_SetProperty("Position", 16, 22)
				TA_SetProperty("Facing", gci_Face_North)
			elseif(iHasSeenBeePanicScene == 1.0) then
				TA_SetProperty("Position", 17, 22)
				TA_SetProperty("Facing", gci_Face_South)
			end
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/BeehiveBasementBee/Root.lua")
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()

	--If Mei is a Zombee, spawn some extra Zombee NPCs for the cutscene, and despawn the enemy zombees. This is used for a cutscene.
	-- Also, Mei is expected to enter from the north.
	else
		
		--NPCs.
		TA_Create("ZombeeNPCA")
			TA_SetProperty("Position", 13, 14)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
			TA_SetProperty("Facing", gci_Face_South)
		DL_PopActiveObject()
		TA_Create("ZombeeNPCB")
			TA_SetProperty("Position", 15, 14)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
			TA_SetProperty("Facing", gci_Face_South)
		DL_PopActiveObject()
		TA_Create("ZombeeNPCC")
			TA_SetProperty("Position", 13, 13)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
			TA_SetProperty("Facing", gci_Face_South)
		DL_PopActiveObject()
		TA_Create("ZombeeNPCD")
			TA_SetProperty("Position", 15, 13)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
			TA_SetProperty("Facing", gci_Face_South)
		DL_PopActiveObject()
		
		--Despawn the enemies.
		EM_PushEntity("ZombeeA")
			RE_SetDestruct(true)
		DL_PopActiveObject()
		EM_PushEntity("ZombeeB")
			RE_SetDestruct(true)
		DL_PopActiveObject()
		EM_PushEntity("ZombeeC")
			RE_SetDestruct(true)
		DL_PopActiveObject()
		EM_PushEntity("ZombeeD")
			RE_SetDestruct(true)
		DL_PopActiveObject()
		
		--Spawn bee NPCs. Florentina has been organizing them!
		TA_Create("BeeGirlTroopA")
			TA_SetProperty("Position", 12, 22)
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
		TA_Create("BeeGirlTroopB")
			TA_SetProperty("Position", 13, 22)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
		TA_Create("BeeGirlTroopC")
			TA_SetProperty("Position", 14, 23)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
		TA_Create("BeeGirlTroopD")
			TA_SetProperty("Position", 15, 21)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
		TA_Create("BeeGirlTroopE")
			TA_SetProperty("Position", 16, 24)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
		TA_Create("BeeGirlScout")
			TA_SetProperty("Position", 14, 19)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
		
		--Spawn Florentina.
		fnSpecialCharacter("Florentina", "Alraune", 14, 20, gci_Face_South, false, nil)
		
	end

end
