--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "Apprehension")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/BeehiveBasementD/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "BeehiveBasementD")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("BeehiveBasementD")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then


	--If Mei is a Zombee, spawn some extra Zombee NPCs she can talk to. Well, "Talk".
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sMeiForm == "Zombee") then
		
		--NPCs.
		TA_Create("ZombeeNPCA")
			TA_SetProperty("Position", 8, 13)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Zombee/Root.lua")
		DL_PopActiveObject()
		TA_Create("ZombeeNPCB")
			TA_SetProperty("Position", 13, 14)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Zombee/Root.lua")
		DL_PopActiveObject()
		
		--Victim Bee. If this flag is set, spawn as a zombee.
		local iConvertedVictimBee = VM_GetVar("Root/Variables/Chapter1/Scenes/iConvertedVictimBee", "N")
		if(iConvertedVictimBee == 1.0) then
			TA_Create("ZombeeNPCD")
				TA_SetProperty("Position", 14, 14)
				TA_SetProperty("Clipping Flag", true)
				fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
				TA_SetProperty("Facing", gci_Face_South)
				TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Zombee/Root.lua")
			DL_PopActiveObject()
		
		--Otherwise, spawn as the victim. For simplicity, the name is the same.
		else
			TA_Create("ZombeeNPCD")
				TA_SetProperty("Position", 14, 14)
				TA_SetProperty("Clipping Flag", true)
				fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
				TA_SetProperty("Facing", gci_Face_South)
				TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/ZombeeVictim/Root.lua")
				TA_SetProperty("Auto Animates", true)
				TA_SetProperty("Y Oscillates", true)
			DL_PopActiveObject()
		end
		
		--Despawn the enemies.
		EM_PushEntity("ZombeeA")
			RE_SetDestruct(true)
		DL_PopActiveObject()
		EM_PushEntity("ZombeeB")
			RE_SetDestruct(true)
		DL_PopActiveObject()
		
	end

end
