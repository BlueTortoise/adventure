--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Boss battle. Triggers a... boss battle... if the player hasn't done it yet and Mei is not a zombee.
if(sObjectName == "BossBattle") then
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iSavedBeehive = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N")
	
	--Mei cannot be a Zombee, and the cultist must actually be there:
	if(sMeiForm ~= "Zombee" and iSavedBeehive == 0.0) then
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/BeehiveBasement_FightBoss/Scene_Begin.lua")
	end
end