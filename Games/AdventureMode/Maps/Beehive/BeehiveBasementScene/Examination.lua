--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examinations]
--Corrupted honey pool.
if(sObjectName == "Honey") then
	
	--Get Mei's form.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Any form but Zombee:
	if(sMeiForm ~= "Zombee") then
		fnStandardDialogue("[VOICE|Mei](Looks like the honey has been corrupted by some malign influence...)")
	
	--Zombee:
	else
		fnStandardDialogue("Mei:[VOICE|Bee] (Drone obeys.[SOFTBLOCK] Capture bees.[SOFTBLOCK] Convert bees.[SOFTBLOCK] Drone obeys.[SOFTBLOCK] Drone obeys.)")
	end
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end