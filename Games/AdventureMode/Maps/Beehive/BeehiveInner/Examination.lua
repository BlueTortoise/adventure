--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Decision Handler]
--This is fired if Mei selects the decision to eat the honey when she has entered the hive as a human without the bee TF.
if(sObjectName == "EatHoney") then
		
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Mei is alone.
	if(bIsFlorentinaPresent == false) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 1.0)
		
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I *am* kind of hungry...[SOFTBLOCK] Just a little bit...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *slurp*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Oohhhh, so sweet.[SOFTBLOCK]*slurp*[SOFTBLOCK] So gooooood...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (It's so thick...[SOFTBLOCK] OH![SOFTBLOCK] I got some on my dress...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] *giggles*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] *slurp*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Mmmmmm.....)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] *slurp*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] It feels so good on my hands...[SOFTBLOCK] On my lips...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Maybe I...[SOFTBLOCK] should rub it on my skin...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (It's hardening?[SOFTBLOCK] Oh?[SOFTBLOCK] It's supposed to?[SOFTBLOCK] But I wanted to eat more...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I'm kind of tired, though...[SOFTBLOCK] I'll sleep now, instead...)") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei crouches and falls over.
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		
		--Fade to black.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneWait(240)
		fnCutsceneBlocker()
		
		--Execute the bee cutscene. It should do the rest, with the flag set so Mei reacts correctly.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Bee/Scene_Begin.lua")
	
	--Florentina is present.
	else
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 1.0)
	
		--Cutscene setup. Florentina is on the opposite side.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Just a little bit...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *slurp*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] M-[SOFTBLOCK]Mei!?[SOFTBLOCK] What in the wastes are you doing?![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] It's so good...[SOFTBLOCK] You should try some...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Mei![SOFTBLOCK] Mei![SOFTBLOCK] Look at me, focus on me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Ooh...[SOFTBLOCK] I got some on my dress...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] *giggles*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] This honey...[SOFTBLOCK] it's gotta be laced with a psychoactive![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Mmmmmmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Stop eating it, you dolt![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I want it...[SOFTBLOCK] so badly...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Don't you dare stop me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Cripes![SOFTBLOCK] Mei, you're not yourself![SOFTBLOCK] Try to focus![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ...[SOFTBLOCK] They call...") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei crouches and falls over.
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		
		--Florentina's dialogue.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneWait(120)
		fnCutsceneBlocker()
	
		--Cutscene setup. Florentina is on the opposite side.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] G-[SOFTBLOCK]get away![SOFTBLOCK] Stupid bees![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Mei![SOFTBLOCK] Mei![SOFTBLOCK] I'll come back with help![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] (Idiot![SOFTBLOCK] What did she think was going to happen?)") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(240)
		fnCutsceneBlocker()
		
		--Execute the bee cutscene. It should do the rest, with the flag set so Mei reacts correctly.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Bee/Scene_Begin.lua")
	end
	return

--This is fired if Mei decides not to eat the honey. It basically just closes the dialogue.
elseif(sObjectName == "DontEatHoney") then
	WD_SetProperty("Hide")
	return
end

--[Execution]
--Honey Pool. Different during bee examination.
if(sObjectName == "HoneyPool") then

	--Variables.
	local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasBeeForm           = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
	local iHasConsumedHoney     = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N")
	local iIsDuringBeeTransform = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")
	
	--During normal examination, but Mei can voluntarily begin the bee transformation.
	if(iHasBeeForm == 0.0 and sMeiForm == "Human" and iIsDuringBeeTransform == 0.0) then
		
		--Bit of dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](The honey looks delicious.[SOFTBLOCK] Try a little?)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		
		--Decision mode.
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"EatHoney\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"DontEatHoney\") ")
		fnCutsceneBlocker()
	
	--During bee transformation...
	elseif(iIsDuringBeeTransform == 1.0) then
		
		--Common.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		
		--First time.
		if(iHasConsumedHoney == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei dipped a finger into the honey and raised it to her lips. She hesitated for a moment, letting the sweet scent wash over her face before reaching a tentative tongue out to taste it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (...[SOFTBLOCK] Delicious![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "She stuck her finger in her mouth.[SOFTBLOCK] Her tongue wrapped around its length, searching to find every last sticky drop of honey as a quiet need began to grow deep in her abdomen. [BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (I...[SOFTBLOCK] I think I want a little more...)") ]])
		
		--Second time.
		elseif(iHasConsumedHoney == 1.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N", 2.0)
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei knelt before the open pool of honey and dipped her hand in.[SOFTBLOCK] The viscous fluid quivered and dripped from her cupped hand as she raised it to her lips.[SOFTBLOCK] It slid down her throat, where it settled heavily in her stomach.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (...[SOFTBLOCK] Oh it's so good![SOFTBLOCK] Maybe just a bit more...)") ]])
		
		--Third time.
		elseif(iHasConsumedHoney == 2.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N", 3.0)
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei took another handful of honey, her head dipping to meet her hand as it raised to her lips.[SOFTBLOCK] She sucked the fluid into her mouth and swallowed it down with deep gulps.[SOFTBLOCK] Her sticky hand dropped to the edge of the pool to support her as she gasped for air after the hasty meal.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (...[SOFTBLOCK] Soooo[SOFTBLOCK] *gasp* [SOFTBLOCK] tasty...)") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image Fast", "Root/Images/Portraits/Combat/Mei_Human") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/BeeTF0") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Bee](Process...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Yeah...[SOFTBLOCK] Process...[SOFTBLOCK] Make more honey...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Her forehead began to tickle, and she reached her honey-covered hand to scratch at it as she stood.[SOFTBLOCK] The honey hardened quickly on her head, and cracked as it dried.[SOFTBLOCK] A pair of insectile feelers began to push up through her skin beneath the honey.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei looked about, her thoughts oblivious to her new appendages.[SOFTBLOCK] Her attention had turned entirely to the instructions she had been given.") ]])
			fnCutsceneBlocker()
		
		--No more honey.
		elseif(iHasConsumedHoney == 3.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Bee](Process...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (I'll go...[SOFTBLOCK] Process...[SOFTBLOCK] Make more honey...)") ]])
		end
		
		--Common.
		fnCutsceneBlocker()
	
	--General examination.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](A pool full of golden honey...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]Thick, viscous honey...") ]])
		fnCutsceneBlocker()
	end

--Nectar Pool. Has a different examination during the bee transformation.
elseif(sObjectName == "NectarPool") then

	--During bee transformation...
	local iIsDuringBeeTransform = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")
	local iHasWorkedNectar      = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasWorkedNectar", "N")
	if(iIsDuringBeeTransform == 1.0 and iHasWorkedNectar == 0.0) then
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (I need to process this...[SOFTBLOCK] But how?[SOFTBLOCK] What does that mean?)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Fade out.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/BeeTF0") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "New ideas began to flow into Mei's mind, as if responding to her thoughts.[SOFTBLOCK] The ideas brought about a feeling of languid contentment, of fullness, that seemed to press on her abdomen.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "She understood what she needed to do.[SOFTBLOCK] She understood how she needed to process the nectar.[SOFTBLOCK] She knelt silently by the pool of nectar,[SOFTBLOCK] then scooped it into her hands and began spreading it on her sex...") ]])
		fnCutsceneBlocker()
		
		fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image Fast", "Root/Images/Scenes/Mei/BeeTF0") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/BeeTF1") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Her body tingled as she slid a nectar-covered finger between the lips of her vagina.[SOFTBLOCK] She could feel the thick fluid being absorbed into the tender depths of her core, then spreading throughout her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "She scooped another handful of nectar and poured it on herself, sliding a second finger beside the first.[SOFTBLOCK] A chill ran up her spine despite the warmth of the hive and the nectar.[SOFTBLOCK] She shivered at the pleasant sensation as it spread through her limbs and pebbled her nipples beneath her dress.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Her thrusts deepend as she dribbled another handful of honey onto herself.[SOFTBLOCK] She pressed her thumb against her sensitive clit as the fingers drove the honey deeper inside of her, massaging the bundle of nerves in slow circles as she felt her fullness growing.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The chill began to fade as her fullness grew.[SOFTBLOCK] It lingered on her back and her legs, and she scratched at it absentmindedly as her fingers pumped in and out of her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Her eyes slid closed as translucent wings pushed out from her back, and her feet grew hard and insectile.[SOFTBLOCK] A burning heat seemed to kindle in her core as the itching faded, and she knew that she, and the nectar within her, were nearly ready. [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Her thumb pressed more firmly against her clit,[SOFTBLOCK] light shocks of electric ecstasy pulsing through her body with each beat of her heart as her fingers quickened inside her. [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "It pushed her body over the edge, her orgasm coursing through her as she shouted her release into the air.[SOFTBLOCK] Dozens of voices,[SOFTBLOCK] perhaps echoes,[SOFTBLOCK] seemed to join her release in her mind as she began to slowly come down. [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "She slid her fingers out from within her, her own fluids mixing with the nectar and leaving a long, thin strand as she lifted the fingers to her lips.[SOFTBLOCK] Her tongue snaked out for a single lick as she stood.[SOFTBLOCK] It was already sweetening, the flavor distinct.[SOFTBLOCK] The flavor of herself mixed with the nectar. [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The nectar would process inside her, to be secreted later with as much pleasure as when she had begun to prepare it.[SOFTBLOCK] This she knew, an understanding based entirely on instinct.[SOFTBLOCK] Even so, she knew it would take some time,[SOFTBLOCK] and further preparation,[SOFTBLOCK] before it would be ready.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Switch Mei's sprite to the bee.
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua") ]])
		
		--Fade back in.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (I...[SOFTBLOCK] we...[SOFTBLOCK] clean...[SOFTBLOCK] clean...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		
		--Variables.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasWorkedNectar", "N", 1.0)
	
	--General examination.
	else
		--Normal dialogue:
		local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		if(sMeiForm ~= "Bee") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](A large pool containing the nectar that bees are so keen on collecting.)") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](A large pool containing the nectar that my hive collects.)") ]])
            fnCutsceneBlocker()
        end
	end
	
--Stuff left behind by previous victims. 
elseif(sObjectName == "Junk") then
	
	--Bee Transformation: This fixes Mei's brain and makes her conscious again.
	local iIsDuringBeeTransform = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")
	if(iIsDuringBeeTransform == 1.0) then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Clean...[SOFTBLOCK] Useless...[SOFTBLOCK] Clean...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image Fast", "Root/Images/Scenes/Mei/BeeTF2") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei began to thoughtlessly sort through the remnants of her old life.[SOFTBLOCK] Her clothes and weapons meant nothing to her anymore.[SOFTBLOCK] She was not even fully aware of what she was looking at.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Images of other parts of the world flowed into her head.[SOFTBLOCK] Ideas and whispers came and went of their own accord.[SOFTBLOCK] Mei sent out her own thoughts in return.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "But, as she picked up her old work uniform, a small stone rune fell out and landed on the ground near her.") ]])
		fnCutsceneBlocker()
		
		--Animation.
		local sAllocationString = "WD_SetProperty(\"Allocate Animation\", gciMeiRuneFramesTotal, 1, -90)"
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutsceneInstruction(sAllocationString)
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image Fast", "Root/Images/Scenes/Mei/BeeTF2") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Flash", 0) ]])
		for i = 0, gciMeiRuneFramesTotal-1, 1 do
			local iNumber = string.format("%02i", i)
			local sString = "WD_SetProperty(\"Set Animation Frame\", " .. i .. ", \"Root/Images/Scenes/Mei/RuneAnim" .. iNumber .. "\")"
			fnCutsceneInstruction(sString)
		end
		fnCutsceneInstruction([[ WD_SetProperty("Append", "A pale grey light began to surround Mei.[SOFTBLOCK] Somehow, her own thoughts rushed into her head.[SOFTBLOCK] Memories of her life, her childhood, her friends...[SOFTBLOCK] They had never left her, but somehow she had still forgotten them.") ]])
		fnCutsceneBlocker()
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] M-[SOFTBLOCK]m-[SOFTBLOCK]Mei?[SOFTBLOCK] My name is Mei?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] But I'm...[SOFTBLOCK] What happened to me?[SOFTBLOCK][EMOTION|Mei|Surprise] Is that a stinger?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No![SOFTBLOCK] Wait, please, be quiet![SOFTBLOCK] Be quiet![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] G-[SOFTBLOCK]get out of my head![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I'm sorry![SOFTBLOCK] I don't know what's going on, really![SOFTBLOCK] I was just doing what we said to do...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Oh, right, the runestone.[SOFTBLOCK] Yes, I came here with it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's not dangerous, sisters.[SOFTBLOCK] It will only work on me, I'm sure of it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yes.[SOFTBLOCK] Yes.[SOFTBLOCK] Yes, of course![SOFTBLOCK] I didn't mean to...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] H-[SOFTBLOCK]hey![SOFTBLOCK] Don't question my loyalty, I'm a drone![SOFTBLOCK] I obey without question, sisters![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I will honor the consensus.[SOFTBLOCK] I will...[SOFTBLOCK][EMOTION|Mei|Sad] find out why I'm different...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, it's okay.[SOFTBLOCK] This is for the best.[SOFTBLOCK] If I find any exotic nectars, I'll mark them right away.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thank you for understanding, sisters![SOFTBLOCK] I'll do my best![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Oh, certainly.[SOFTBLOCK] Yes, some honey will help greatly.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOUND|World|TakeItem](Received Everlasting Bee Honey!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (...[SOFTBLOCK] Okay then, the hive has tasked me with a crucial mission::[SOFTBLOCK] Solve the mystery of the runestone!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Which is...[SOFTBLOCK] what I was doing before all this happened, I guess...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (But now I'm doing it for my drone sisters![SOFTBLOCK] Onward!)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Variables.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N", 0.0)
		
		--If this is the relive case, return to the last save point and execute that scene.
		local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
		if(iIsRelivingScene == 1.0) then
	
			--Wait a bit.
			fnCutsceneWait(10)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(45)
			fnCutsceneBlocker()
			
			--Return to the last save point and execute the post-script..
			fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
			fnCutsceneBlocker()
	
		--Not reliving, gain the bee honey.
		else
			LM_ExecuteScript(gsItemListing, "Everlasting Honey")
	
		end
	
	--All other cases.
	else
	
		--Normal dialogue:
		local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		if(sMeiForm ~= "Bee") then
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Assorted junk left over from previous victims.[SOFTBLOCK] The bees don't seem to care about it.)") ]])
			fnCutsceneBlocker()
		
		--Bee-form dialogue.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](The belongings left over from the previous lives of my bee sisters.[SOFTBLOCK] They'll get rid of it eventually.)") ]])
			fnCutsceneBlocker()
		end
		
		--If the player has not searched this, they find some adamantite powder.
		local iBeehiveJunkSearched = VM_GetVar("Root/Variables/Chapter1/WorldState/iBeehiveJunkSearched", "N")
		if(iBeehiveJunkSearched == 0.0) then
			
			--Flags.
			iBeehiveJunkSearched = VM_SetVar("Root/Variables/Chapter1/WorldState/iBeehiveJunkSearched", "N", 1.0)
			LM_ExecuteScript(gsItemListing, "Adamantite Powder x1")
			
			--Dialogue.
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|TakeItem") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](There was some Adamantite Powder in the pile.[SOFTBLOCK] Might as well take it!)") ]])
			fnCutsceneBlocker()
		end
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end