--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Orders Mei back into the honey room to consume more honey.
if(sObjectName == "ConsumeZone" and iIsWholeCollision == 1.0) then

	--Variables.
	local iHasConsumedHoney       = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N")
	local iIsDuringBeeTransform   = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")
		
	--Must be during the bee transformation, but Mei has not consumed the honey yet.
	if(iIsDuringBeeTransform == 1.0 and iHasConsumedHoney < 3.0) then
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Dialogue setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

		--Actual talking.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "(Consume... Honey...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (I should...[SOFTBLOCK] Eat the honey...[SOFTBLOCK] I guess I *am* kinda hungry.") ]])
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Move Mei back.
		Cutscene_CreateEvent("Move Mei North", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move Amount", 0.0, gciSizePerTile * -1.0)
		DL_PopActiveObject()
		Cutscene_CreateEvent("Stop Moving", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Stop Moving")
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
	end

--Orders Mei back into the hive.
elseif(sObjectName == "WalkbackZone" and iIsWholeCollision == 1.0) then

	--Variables.
	local sMeiForm                = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasBeeForm             = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
	local iHasConsumedHoney       = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N")
	local iIsDuringBeeTransform   = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")
	local iHasSeenBeesIgnoreScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeesIgnoreScene", "N")

	--If not during the bee transformation, and Mei does not have bee form, and we haven't seen the bee-ignore scene, show that scene.
	if(iHasBeeForm == 0.0 and iHasSeenBeesIgnoreScene == 0.0 and iIsDuringBeeTransform == 0.0 and sMeiForm == "Human") then

		--[Setup]
		--Set flags.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeesIgnoreScene", "N", 1.0)
		
		--Variables.
		local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--[Movement]
		--Move Mei forward.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", 8.25 * gciSizePerTile, 16.50 * gciSizePerTile)
		DL_PopActiveObject()
		
		--Move Florentina forward.
		if(bIsFlorentinaPresent) then
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", 8.25 * gciSizePerTile, 17.50 * gciSizePerTile)
			DL_PopActiveObject()
		end
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--[Dialogue]
		fnStandardMajorDialogue()
		
		--Mei only version.
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Seems the bees aren't paying attention to me.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I guess these ones are so busy they don't even notice I'm here.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Mmmmmmmm.[SOFTBLOCK] It smells...[SOFTBLOCK] so...[SOFTBLOCK] good...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Honey...[SOFTBLOCK] The bees won't mind if I try a bit of their honey...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] (Huh?[SOFTBLOCK] Was I daydreaming?)") ]])
			fnCutsceneBlocker()
		
		--Mei and Florentina.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Seems the bees haven't noticed us yet.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Maybe they're too busy?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Or they don't see us a threat.[SOFTBLOCK] Either way, I don't like it.[SOFTBLOCK] Let's not linger.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *sniff*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It smells so sweet in here...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I don't smell anything.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And I'm so hungry...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Mei?[SOFTBLOCK] Hey, Mei![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Were you talking to me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You looked miles away.[SOFTBLOCK] Whatever we're looking for here, let's find it and leave.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Just what *am* I looking for in here?)") ]])
		end
		fnCutsceneBlocker()
		
		--[Movement]
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Move Florentina onto Mei if she's present.
		if(bIsFlorentinaPresent) then
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", 8.25 * gciSizePerTile, 16.50 * gciSizePerTile)
			DL_PopActiveObject()
			fnCutsceneWait(5)
			fnCutsceneBlocker()

			--Fold the party positions up.
			fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
			fnCutsceneBlocker()
		end
	
	--During the bee transformation, move Mei back into the hive.
	elseif(iIsDuringBeeTransform == 1.0) then
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Dialogue setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

		--Actual talking.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "(Return...[SOFTBLOCK] Complete...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (I need to go back...[SOFTBLOCK] I'm not done here yet...") ]])
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Move Mei back.
		Cutscene_CreateEvent("Move Mei East", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move Amount", gciSizePerTile * 2.0, 0.0)
		DL_PopActiveObject()
		Cutscene_CreateEvent("Stop Moving", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Stop Moving")
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
	end

--Orders Mei away from the junk pile until the time is right.
elseif(sObjectName == "NoJunkZone" and iIsWholeCollision == 1.0) then

	--Variables.
	local iIsDuringBeeTransform = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")
	local iHasWorkedNectar      = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasWorkedNectar", "N")
	
	--During the bee transformation, move Mei back into the hive.
	if(iIsDuringBeeTransform == 1.0 and iHasWorkedNectar == 0.0) then
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Dialogue setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

		--Actual talking.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "(Nectar...[SOFTBLOCK] Process...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (Process...[SOFTBLOCK] I will process...") ]])
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Move Mei back.
		Cutscene_CreateEvent("Move Mei North", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move Amount", 0.0, gciSizePerTile * -1.0)
		DL_PopActiveObject()
		Cutscene_CreateEvent("Stop Moving", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Stop Moving")
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
	end

end