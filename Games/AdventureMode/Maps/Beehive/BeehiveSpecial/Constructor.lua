--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "Island2")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/TiostaIslandG/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "TiostaIslandG")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("TiostaIslandG")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
  TA_CreateUsingPosition("BeeA", "BeeA")
        TA_SetProperty("Shadow", gsStandardShadow)
        TA_SetProperty("Auto Animates", true)
        TA_SetProperty("Y Oscillates", true)
    DL_PopActiveObject()

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then


	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.


end
