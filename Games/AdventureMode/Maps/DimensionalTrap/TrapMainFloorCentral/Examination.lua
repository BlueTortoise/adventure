--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examinations]
if(sObjectName == "StatueRow") then
	fnStandardDialogue([[ [VOICE|Mei](A row of three statues.[SOFTBLOCK] Their helmets all point to the south door.) ]])
	
elseif(sObjectName == "Statue") then
	fnStandardDialogue([[ [VOICE|Mei](A suit of armor.[SOFTBLOCK] It's tarnished and hasn't been maintained for a long time.) ]])
	
elseif(sObjectName == "Junk") then
	fnStandardDialogue([[ [VOICE|Mei](Household junk, like candles, utensils, and washcloths.) ]])
	
elseif(sObjectName == "SoapCrate") then
	fnStandardDialogue([[ [VOICE|Mei](The crate has a selection of soaps.) ]])
	
elseif(sObjectName == "Empty") then
	fnStandardDialogue([[ [VOICE|Mei](An empty barrel.[SOFTBLOCK] This seems to belong to the cultists...) ]])
	
elseif(sObjectName == "Liquor") then
	fnStandardDialogue([[ [VOICE|Mei](Smells like moonshine with something sweet in it.) ]])
	
elseif(sObjectName == "Cash") then
	local iGotCashCrate = VM_GetVar("Root/Variables/Chapter1/Scenes/iGotCashCrate", "N")
	if(iGotCashCrate == 0.0) then
		LM_ExecuteScript(gsItemListing, "Platina x15")
		AudioManager_PlaySound("World|TakeItem")
		fnStandardDialogue([[ [VOICE|Mei](Found 15 platina in the crate!) ]])
		VM_SetVar("Root/Variables/Chapter1/Scenes/iGotCashCrate", "N", 1.0)
	else
		fnStandardDialogue([[ [VOICE|Mei](Nothing else of interest in the crate.) ]])
	end

--[Exits]
--Exits to mansion northern edge.
elseif(sObjectName == "ToNorthDoor") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("TrapMainFloorExteriorN", "FORCEPOS:18.5x32.0")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end