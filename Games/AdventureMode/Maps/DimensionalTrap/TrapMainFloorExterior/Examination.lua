--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examinations]
--This door is locked unless Florentina is present. She can unlock it.
if(sObjectName == "LockedDoor") then
	
	--Variables
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	local iFlorentinaUnlockedTrapWest = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaUnlockedTrapWest", "N")
    VM_SetVar("Root/Variables/Chapter1/Scenes/iDepthReset", "N", 1.0)
	
	--Door is not unlocked:
	if(iFlorentinaUnlockedTrapWest == 0.0) then
	
		--If Florentina is not present:
		if(bIsFlorentinaPresent == false) then
	
			--Mei talks to herself.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Crud, this door is locked...)") ]])
	
	
		--Florentina is here, so unlock the door:
		else
	
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaUnlockedTrapWest", "N", 1.0)
	
			--Setup.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Tch, the door is locked.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] No it isn't.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Yes it is![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Let me see it...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy][SOUND|World|FlipSwitch] See?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] It's not locked.[SOFTBLOCK] Obviously.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] You know how to pick locks?[SOFTBLOCK] Cool![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Where'd you learn to do that?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] A caravan came through with a whole bunch of spare locks, once, but it turned out they got the paperwork wrong.[SOFTBLOCK] So they gave me some platina to dispose of them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Rather than just dump a hundred door locks, I started fiddling with them.[SOFTBLOCK] Turns out, picking them isn't hard if you practice.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Can you teach me how to do that?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I've always wanted to be a burglar![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] R-[SOFTBLOCK]really?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Ah, no, I was kidding.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Don't lead a girl on like that...") ]])
	
		end
	
	--Door is unlocked, go in!
    else
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("TrapDungeonEntry", "FORCEPOS:17.0x13.0x0")
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end