--[Cross Lake]
--When the player examines the boat, this script is fired. Depending on their answer, it either changes
-- maps or just does nothing.

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Cross the lake.
if(sTopicName == "Yes") then

	WD_SetProperty("Hide")
	AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 0.0, 0, 0, 0, 1)
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ AL_BeginTransitionTo("PlainsC", "FORCEPOS:49.5x48.0") ]])

--Don't do that.
else
	WD_SetProperty("Hide")
end