--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examinations]
if(sObjectName == "Junk") then
	fnStandardDialogue([[ [VOICE|Mei](Nails, rope, various repair supplies...) ]])
	
elseif(sObjectName == "Barrel") then
	fnStandardDialogue([[ [VOICE|Mei](The barrel smells of ammonia.) ]])
	
elseif(sObjectName == "Statue") then
	fnStandardDialogue([[ [VOICE|Mei](Presumably this statue is in storage.[SOFTBLOCK] It hasn't been cleaned in a very long time.) ]])
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end