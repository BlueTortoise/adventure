--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "ForestTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/AlrauneChamber/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "AlrauneChamber")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Map Setup
	fnResolveMapLocation("AlrauneChamber")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Variables.
	local iCompletedTrapDungeon        = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	local iSeenAlrauneBattleIntroScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N")

	--If the trap dungeon is complete, or the player has not seen the Alraune battle intro, spawn Rochea.
	if(iCompletedTrapDungeon == 1.0 or iSeenAlrauneBattleIntroScene == 0.0) then
		TA_Create("Rochea")
			TA_SetProperty("Position", 16, 8)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_Rochea/Root.lua")
			TA_SetProperty("Activate Wander Mode")
		DL_PopActiveObject()
	else
		VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 0.0)
	end
end
