--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "BreannesTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/BreannesPitStop/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "BreannesPitStop")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Map Setup
	fnResolveMapLocation("BreannesPitStop")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Variables.
	local fSpawnX = 0
	local fSpawnY = 0
	local iSpawnD = gci_Face_South
	local iHasMetMei = VM_GetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N")
	local iCanSpawnJoanie = VM_GetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N")

	--Case: Haven't met Breanne yet. Only she spawns. She spawns in the kitchen.
	if(iHasMetMei == 0.0) then
		fSpawnX = 25
		fSpawnY = 15
		iSpawnD = gci_Face_North

	--Mei has met Breanne.
	else

		--[Nadia]
		--Special: If Mei has not been to the Dimensional Trap Dungeon, is level 5, has Florentina, and has not seen it already, spawn Nadia:
		local bSpawnedNadia = false
		local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		local iInformedOfDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N")
		AdvCombat_SetProperty("Push Party Member", "Mei")
			local iMeiLevel = AdvCombatEntity_GetProperty("Level")
		DL_PopActiveObject()
		if(iInformedOfDungeon == 0.0 and bIsFlorentinaPresent == true and iMeiLevel >= 5) then
			
			--Flags:
			bSpawnedNadia = true
			iCanSpawnJoanie = 0.0
			
			--Spawn Nadia:
			TA_Create("Nadia")
				TA_SetProperty("Position", 8, 14)
				TA_SetProperty("Facing", gci_Face_East)
				TA_SetProperty("Clipping Flag", true)
				TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Nadia/Root.lua")
				fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
			DL_PopActiveObject()

			--[Walking]
			--Mei/Florentina walks in a bit.
			fnCutsceneMove("Mei", 6.25, 14.00)
			fnCutsceneMove("Florentina", 6.25, 15.00)
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneInstruction([[ fnPartyStopMovement() ]])
			fnCutsceneWait(25)
			fnCutsceneBlocker()

			--Nadia faces left.
			fnCutsceneFace("Nadia", -1, 0)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Play the cutscene.
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Nadia_InformOfDungeon/Scene_Begin.lua")
			
			--Move Florentina onto Mei, fold the party.
			fnCutsceneMove("Florentina", 6.25, 14.00)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
			fnCutsceneBlocker()
			
		end
	
		--[Random NPCs]
		--There is a 25% chance that a werecat will spawn on the south end of the map.
		local iRoll = LM_GetRandomNumber(0, 99)
		if(iRoll < 25 or true) then
			TA_Create("Werecat")
				TA_SetProperty("Position", 11, 22)
				TA_SetProperty("Facing", gci_Face_East)
				TA_SetProperty("Clipping Flag", true)
				TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/PitStopNPCs/Werecat.lua")
				fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			DL_PopActiveObject()
		end
		
		--50% chance of an NPC spawning here.
		iRoll = LM_GetRandomNumber(0, 99)
		if(iRoll < 50 or true) then
			TA_Create("Man")
				TA_SetProperty("Position", 12, 16)
				TA_SetProperty("Facing", gci_Face_East)
				TA_SetProperty("Clipping Flag", true)
				TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/PitStopNPCs/ChairMan.lua")
				fnSetCharacterGraphics("Root/Images/Sprites/GenericM0/", false)
			DL_PopActiveObject()
		end
				
		--A human NPC spawns here.
		TA_Create("Man")
			TA_SetProperty("Position", 12, 11)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/PitStopNPCs/Suitor.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/GenericM1/", false)
		DL_PopActiveObject()
		
		--Alraune in one of the bedrooms.
		iRoll = LM_GetRandomNumber(0, 99)
		if(iRoll < 70 or true) then
			TA_Create("Alraune")
				TA_SetProperty("Position", 22, 9)
				TA_SetProperty("Facing", gci_Face_South)
				TA_SetProperty("Clipping Flag", true)
				TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/PitStopNPCs/Alraune.lua")
				fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			DL_PopActiveObject()
		end
		
		--[Specific NPC]
		--Mei can convince this fellow to become an Alraune.
		local iMeiJoinedSuitor = VM_GetVar("Root/Variables/Chapter1/Breanne/iMeiJoinedSuitor", "N")
		iRoll = LM_GetRandomNumber(0, 99)
		if((iRoll < 70 or true) and iMeiJoinedSuitor == 0.0) then
			TA_Create("PliableMan")
				TA_SetProperty("Position", 20, 12)
				TA_SetProperty("Facing", gci_Face_East)
				TA_SetProperty("Clipping Flag", true)
				TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/PitStopNPCs/PliableMan.lua")
				fnSetCharacterGraphics("Root/Images/Sprites/MercM/", false)
			DL_PopActiveObject()
		end

		--[Breanne]
		--Breanne's spawn position will be in one of several locations.
		iRoll = LM_GetRandomNumber(0, 99)
		
		--Nadia case:
		if(bSpawnedNadia == true) then
			fSpawnX = 9
			fSpawnY = 14
			iSpawnD = gci_Face_West
		
		--Normal case:
		elseif(iCanSpawnJoanie == 0.0) then
		
			--Outside the shed.
			if(iRoll < 10) then
				fSpawnX = 11
				fSpawnY = 11
				iSpawnD = gci_Face_South
			
			--On the chair outside of the building.
			elseif(iRoll < 20) then
				fSpawnX = 13
				fSpawnY = 17
				iSpawnD = gci_Face_West
			
			--Near the table inside the building.
			elseif(iRoll < 30) then
				fSpawnX = 19
				fSpawnY = 15
				iSpawnD = gci_Face_South
			
			--Kitchen, near the counter.
			elseif(iRoll < 40) then
				fSpawnX = 27
				fSpawnY = 17
				iSpawnD = gci_Face_East
			
			--Kitchen, shelves.
			elseif(iRoll < 50) then
				fSpawnX = 25
				fSpawnY = 15
				iSpawnD = gci_Face_North
			
			--Breanne's room.
			elseif(iRoll < 60) then
				fSpawnX = 26
				fSpawnY = 9
				iSpawnD = gci_Face_North
			
			--Main room.
			elseif(iRoll < 80) then
				fSpawnX = 21
				fSpawnY = 12
				iSpawnD = gci_Face_South
			
			--Near the entrance.
			else
				fSpawnX = 5
				fSpawnY = 11
				iSpawnD = gci_Face_North
			end
		
		--If Joanie can be spawned, Breanne always spawns near Joanie.
		else
			--Position.
			fSpawnX = 14
			fSpawnY = 8
			iSpawnD = gci_Face_East
			
			--Spawn the bee.
			TA_Create("Joanie")
				TA_SetProperty("Position", 15, 8)
				TA_SetProperty("Facing", gci_Face_West)
				TA_SetProperty("Clipping Flag", true)
				TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Joanie/Root.lua")
				fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
				TA_SetProperty("Auto Animates", true)
				TA_SetProperty("Y Oscillates", true)
			DL_PopActiveObject()
		
		end
		
	end

	--Spawn Breanne.
	TA_Create("Breanne")
		TA_SetProperty("Position", fSpawnX, fSpawnY)
		TA_SetProperty("Facing", iSpawnD)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Breanne/Root.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/Breanne/", true)
	DL_PopActiveObject()

end
