--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Sign next to Breanne's shed.
if(sObjectName == "Shed Sign") then
	fnStandardDialogue([[ [VOICE|Mei](It reads "Supply shed.[SOFTBLOCK] Keep out.") ]])

--Attempting to open the door.
elseif(sObjectName == "Shed Door") then

	--Variables.
	local iHasPryBar = AdInv_GetProperty("Item Count", "Pry Bar")
	
	--Player does not have anything to pry the door with.
	if(iHasPryBar == 0.0) then
		fnStandardDialogue([[[VOICE|Mei] (It's locked.) ]])
	
	--Pry it open.
	else
		fnStandardDialogue([[[VOICE|Mei] (It's locked.[SOFTBLOCK] Probably not a good idea to pry it open...) ]])
	end
	
--Sign next to the main Inn building.
elseif(sObjectName == "Inn Sign") then
	fnStandardDialogue("[VOICE|Mei](It reads \"Breanne's Pit Stop. Come on in!\".)\n[BLOCK][CLEAR][VOICE|Mei](Written below that, in smaller letters, is \"Unless my parents sent you.[SOFTBLOCK] In which case, taff off!\")")
	WD_SetProperty("Unlock Topic", "BreanneSign", 1)
	
--Sign next to the watering hole.
elseif(sObjectName == "Water Sign") then
	fnStandardDialogue("[VOICE|Mei](It reads \"This is the watering hole.[SOFTBLOCK] No fishing, please.\")[BLOCK][CLEAR][VOICE|Mei](\"To whoever keeps eating my pet fish -[SOFTBLOCK] Knock it off!\")")

--Bookshelves.
elseif(sObjectName == "Bookshelf0") then
	fnStandardDialogue([[[VOICE|Mei] (Books that seem to have been left here by previous guests.[SOFTBLOCK] There's a lot of romance novels, but some on swordplay, astronomy, exploration, and marine life.) ]])
	
elseif(sObjectName == "Bookshelf1") then
	fnStandardDialogue([[[VOICE|Mei] (Books that seem to have been left here by previous guests.[SOFTBLOCK] All romance novels, every single one!) ]])

elseif(sObjectName == "Bookshelf2") then
	fnStandardDialogue([[[VOICE|Mei] (This is Breanne's bookshelf.[SOFTBLOCK] Lots of books on woodworking, cooking, and negotiating.)[BLOCK][CLEAR][VOICE|Mei](Oh, what's this?[SOFTBLOCK] Lesbian erotica?[SOFTBLOCK] Better not touch that, she might notice...) ]])

elseif(sObjectName == "KitchenShelves") then
	fnStandardDialogue([[[VOICE|Mei] (Vegetables, breads, and a few dried meats.[SOFTBLOCK] Despite not having any refrigeration, the food is in very good condition.) ]])

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end