--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Launches the first cutscene with Breanne. She should be in the kitchen for this, though the cutscene will move her if necessary.
if(sObjectName == "FirstSceneTrigger") then
	
	--Make sure we're not double-launching the scene.
	local iHasMetMei = VM_GetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N")
	if(iHasMetMei == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N", 1.0)
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Breanne_FirstScene/Scene_Begin.lua")
	end
end