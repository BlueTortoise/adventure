--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Triggers]
--Battle at the campsite.
if(sObjectName == "CampFight") then
	
	--In all cases, mark the combat intro dialogues as complete.
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")

	--Nothing happens if this encounter has already played out.
	local iEncounteredCatCampsite = VM_GetVar("Root/Variables/Chapter1/Scenes/iEncounteredCatCampsite", "N")
	if(iEncounteredCatCampsite == 2.0) then return end

	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--[Movement]
	--Move the party to the east.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (19.50 * gciSizePerTile))
	DL_PopActiveObject()
	if(bIsFlorentinaPresent) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (20.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Cats turn to face the party.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "WerecatA")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "WerecatB")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "WerecatC")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--[Dialogue]
	--If this is 0.0, then Mei hasn't seen these cats before:
	if(iEncounteredCatCampsite == 0.0) then
		
		--Set variable:
		VM_SetVar("Root/Variables/Chapter1/Scenes/iEncounteredCatCampsite", "N", 1.0)
		
		--Setup.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])

		--Mei is a human:
		if(sMeiForm == "Human") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrrr,[SOFTBLOCK] this human walks with confidence...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You wouldn't happen to be friendly cats, would you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ha ha![SOFTBLOCK] The human knows nothing![SOFTBLOCK] Stupid![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] A no would suffice...[BLOCK][CLEAR]") ]])
			
			--Mei is alone:
			if(bIsFlorentinaPresent == false) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Have you the strength to challenge us, weak human?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I've half a mind to teach you some manners![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: A fight![SOFTBLOCK] That is more like it![SOFTBLOCK] Show us your fury!") ]])
			
			--Florentina is present:
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Shall we knock some manners into them?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'd be delighted![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: A fight![SOFTBLOCK] That is more like it![SOFTBLOCK] Show us your fury!") ]])
			end
		
		--Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ah, this kinfang wishes to join our pride?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *sniff*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I smell weakness.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Pah, you smell your own stink![BLOCK][CLEAR]") ]])
		
			--Mei is alone:
			if(bIsFlorentinaPresent == false) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Shall we see who is stronger?[SOFTBLOCK] Come, face me, kinfang![SOFTBLOCK] Prove yourself!") ]])
			
			--Florentina is present:
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Shall we see who is stronger?[SOFTBLOCK] Come, face me, kinfang![SOFTBLOCK] Prove yourself![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I wonder exactly how many ways there are to skin a cat...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Let's find out!") ]])
			end
			
		--Mei is something else:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrrr...[SOFTBLOCK] wait...[SOFTBLOCK] you smell like a human, but are not?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: A disguise, perhaps?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Err...[SOFTBLOCK] you wouldn't happen to be friendly cats, would you?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ha ha![SOFTBLOCK] The hidden human knows nothing![SOFTBLOCK] Stupid![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] A no would suffice...[BLOCK][CLEAR]") ]])
			
			--Mei is alone:
			if(bIsFlorentinaPresent == false) then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Have you the strength to challenge us, weak hidden human?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I've half a mind to teach you some manners![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: A fight![SOFTBLOCK] That is more like it![SOFTBLOCK] Show us your fury!") ]])
			
			--Florentina is present:
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Shall we knock some manners into them?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'd be delighted![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: A fight![SOFTBLOCK] That is more like it![SOFTBLOCK] Show us your fury!") ]])
			end
		end
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Start a battle.
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Reinitialize") ]])
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Activate") ]])
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_DefeatCampCats.lua") ]])
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
		fnCutsceneBlocker()
		
	--If this is 1.0, Mei fought them and lost.
	elseif(iEncounteredCatCampsite == 1.0) then
		
		--Setup.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
		
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ah![SOFTBLOCK] You challenge us again![SOFTBLOCK] We shall become strong together![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You got lucky last time.[SOFTBLOCK] This time will be different![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: We shall sharpen each other's claws![SOFTBLOCK] Come![SOFTBLOCK] To battle!") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Start a battle.
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Reinitialize") ]])
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Activate") ]])
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_DefeatCampCats.lua") ]])
		fnCutsceneInstruction([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/013 Werecat Difficulty 0.lua", 0) ]])
		fnCutsceneBlocker()

    end

elseif(sObjectName == "GoBack") then

    --Does nothing if the Cassandra events are not active.
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
    if(iStartedCassandraEvent ~= 1.0) then return end
    
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    
    --Florentina is not here.
    if(bIsFlorentinaPresent == false) then
		fnStandardMajorDialogue()
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I can't go back without at least trying to find that girl...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (But I have my own problems to worry about.[SOFTBLOCK] I can't save every random person I meet.[SOFTBLOCK] Maybe I should just let her go...)[BLOCK]") ]])

    --Florentina is here:
    else
		fnStandardMajorDialogue()
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Had a change of heart, Mei?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] We should try to save her, but we can't save every random person in the world by ourselves.[SOFTBLOCK] Maybe we should just let it go...[BLOCK]") ]])
    end

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Save Her\", " .. sDecisionScript .. ", \"Save\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"Leave\") ")
    fnCutsceneBlocker()
    
elseif(sObjectName == "Save") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    
    --Florentina is not here.
    if(bIsFlorentinaPresent == false) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (No way![SOFTBLOCK] Whoever you are, hold on![SOFTBLOCK] Mei is coming!)") ]])
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Mei", 10.25, 19.50)

    --Florentina is here:
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No way![SOFTBLOCK] Come on Florentina, let's go rough up some cats and save someone![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well when you put it that way...") ]])
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Mei", 10.25, 19.50)
        fnCutsceneMove("Florentina", 10.25, 19.50)
        fnCutsceneBlocker()
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Leave") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Deactivate the music.
    fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
    
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N", 100)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraTooLate", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraWayTooLate", "N", 1.0)
    
    --Florentina is not here.
    if(bIsFlorentinaPresent == false) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (It can't be helped.[SOFTBLOCK] I'm sure she'll enjoy her life as a werecat.[SOFTBLOCK] I need to worry about getting back home...)") ]])
        fnCutsceneBlocker()

    --Florentina is here:
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Sorry for getting worked up, Florentina, but we'll have to just leave her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Not a problem.[SOFTBLOCK] In fact, it's very mature to admit you can't do everything by yourself.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] The werecats won't kill her, they'll just turn her into a werecat.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Becoming a monstergirl is...[SOFTBLOCK] maybe not the best, but certainly not the worst.[SOFTBLOCK] The world will get on fine without us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thanks, Florentina.[SOFTBLOCK] Let's get going.") ]])
        fnCutsceneBlocker()
    end
    
    --Forest music.
    fnCutsceneInstruction([[ AL_SetProperty("Music", "ForestTheme") ]])
end
