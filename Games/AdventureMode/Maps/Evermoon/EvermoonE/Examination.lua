--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examination]
--Silver fort door.
if(sObjectName == "Fort Door S") then
	fnStandardDialogue("[VOICE|Mei](Locked, seems like it needs a key of the same color as the door.)")
	
--Silver fort door.
elseif(sObjectName == "Fort Door C") then
	fnStandardDialogue("[VOICE|Mei](Locked, seems like it needs a key of the same color as the door.)")
	
--Silver fort door.
elseif(sObjectName == "Fort Door E") then
	fnStandardDialogue("[VOICE|Mei](Locked, seems like it needs a key of the same color as the door.)")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end