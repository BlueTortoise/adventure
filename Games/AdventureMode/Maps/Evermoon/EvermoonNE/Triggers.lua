--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Trying to open the door from the north fails.
if(sObjectName == "IsDoorNorth") then
	VM_SetVar("Root/Variables/Chapter1/WorldState/iEvermoonNEDoorFromSouth", "N", 0.0)

--Opening the door from the south succeeds.
elseif(sObjectName == "IsDoorSouth") then
	VM_SetVar("Root/Variables/Chapter1/WorldState/iEvermoonNEDoorFromSouth", "N", 1.0)
	
--Disables the forest overlay.
elseif(sObjectName == "NoOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 15)
	
--Disables the forest overlay instantly.
elseif(sObjectName == "NoOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 0)
	
--Enables the forest overlay.
elseif(sObjectName == "YesOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.50, 15)
end