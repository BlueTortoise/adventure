--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Note, can't be decoded until later.
if(sObjectName == "CodedNote") then
	fnStandardDialogue([[ [VOICE|Mei](It reads "QXCCE OHTT 1NN".[SOFTBLOCK] Is this some sort of code?) ]])

--Barrels. Nothing of major interest.
elseif(sObjectName == "Barrels") then
	fnStandardDialogue([[ [VOICE|Mei](The barrels contain salted fish.[SOFTBLOCK] Not very appetizing...) ]])

--Bookshelves. Redirects to a topic set.
elseif(sObjectName == "Bookshelf") then
	fnStandardDialogue([[ [VOICE|Mei](There's quite a collection of books here.[SOFTBLOCK] Read one?)[BLOCK] ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "EvermoonNWBookshelf", "Leave") ]])

--Bed.
elseif(sObjectName == "Bed") then
	fnStandardDialogue([[ [VOICE|Mei](The bed hasn't been used in a while, probably at least a week, judging by the dust buildup.[SOFTBLOCK] It was very neatly made, though.) ]])

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end