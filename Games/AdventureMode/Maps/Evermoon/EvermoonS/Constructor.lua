--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "ForestTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/EvermoonS/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "EvermoonS")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("EvermoonS")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Special: If this flag is tripped, spawn Florentina in the NW corner for the slime TF scene.
	local iSlimeMeetFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N")
	if(iSlimeMeetFlorentina == 1.0) then
		
		--Unset the flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N", 1.0)

		--Spawn and position Florentina.
		fnSpecialCharacter("Florentina", "Alraune", 16, 9, gci_Face_NorthEast, false, nil)
	end
	
	--Overlays.
    AL_SetProperty("Allocate Foregrounds", 3)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/ForestA")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 2.7, 2.7)
    AL_SetProperty("Foreground Alpha",          0, 0.40, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 6.0)
end
