--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--This trigger is only fired after the slime TF scene, and only if Florentina was in the party.
if(sObjectName == "SlimeTrigger") then

	--[Flags]
	--Check the variable.
	local iSlimeMeetFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N")
	local iFlorentinaSawSmartSlimes = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSawSmartSlimes", "N")
	if(iSlimeMeetFlorentina ~= 1.0) then return end

	--Unset the flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N", 0.0)
	
	--[Dialogue]
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Mei?[SOFTBLOCK] Meeeiii![SOFTBLOCK] Where are you, you damn idiot!") ]])
	fnCutsceneBlocker()
	
	--[Movement]
	--Move Mei over to Florentina.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", 15.25 * gciSizePerTile, 9.5 * gciSizePerTile)
	DL_PopActiveObject()
	fnCutsceneWait(2)
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Florentina turns to see Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, 0.0)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--[Dialogue]
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	--Talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Are you *freaking* kidding me?[SOFTBLOCK] This is the last thing I need![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Err, is this a bad time?[BLOCK][CLEAR]") ]])
	
	--Next bit of dialogue varies based on whether or not Florentina knows about Mei's runestone's powers.
	local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
	if(iFlorentinaKnowsAboutRune == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
		
		--Florentina knows about the smarty-slimes:
		if(iFlorentinaSawSmartSlimes == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You -[SOFTBLOCK] you can talk like those other slimes?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I guess so.[SOFTBLOCK] My runestone did this.[SOFTBLOCK] It just started glowing, and now I'm smart again.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh really?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Your smile is starting to unnerve me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] The gears in my head are turning, is all.[SOFTBLOCK] This opens up so many options...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Be specific.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Slimes can slide under doors, you see.[SOFTBLOCK] No lock could stop you...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] For heaven's sake, Florentina![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're the one who asked.[SOFTBLOCK] Blame yourself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I assume you still want to find a way home, even now?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yes, of course.[SOFTBLOCK] My runestone -[SOFTBLOCK] it will let me change back if I so choose.[SOFTBLOCK] I'm sure of it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] The surprises just keep coming, don't they?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happpy] Then let's not dally.[SOFTBLOCK] If the amazements keep coming, maybe the next one will finally get me rich.[SOFTBLOCK] Onward![BLOCK][CLEAR]") ]])
		
		--Florentina doesn't know about smarty-slimes:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] By the four holies, you can talk?[SOFTBLOCK] How!?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Search me.[SOFTBLOCK] My runestone just started glowing...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Listen, Mei.[SOFTBLOCK] You may not realize this, but you're literally the only talking slime in...[SOFTBLOCK] ever.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Do you have any idea what this means?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] We can keep looking for a way back to Earth?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] What?[SOFTBLOCK] No![SOFTBLOCK][E|Happy] People will pay big to see you![SOFTBLOCK] Fortune and fame await![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Florentina!![SOFTBLOCK] I'm not a circus freak show![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] See this is why I'm never going to get rich...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So if you're a slime, does that mean you can change form at will?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I suppose so.[SOFTBLOCK] I quite like how I look right now...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Can you do something about that stupid look on your face?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Very funny...[SOFTBLOCK] Shall we go, then?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, yeah.[SOFTBLOCK] I'm fine, by the way.") ]])
		end
		fnCutsceneBlocker()
	
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] At least you're in one piece...[SOFTBLOCK] sort of...[SOFTBLOCK] Are you okay?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Your concern is touching.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Just keeping my good eye on my investment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Naturally.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I'm guessing this is the result of your little runestone, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yep.[SOFTBLOCK] It let out a glow...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I thought I saw something in the thicket.[SOFTBLOCK] That thing gets more intriguing by the minute...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Shall we be off, then?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Hold on a moment...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What are you looking at?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] If I don't miss my guess, you got [SOFTBLOCK]*bigger*[SOFTBLOCK] didn't you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei[E|Blush]: Well, I can look however I want.[SOFTBLOCK] Do you like what you see?[SOFTBLOCK] Hehe...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Can you do anything about the dumb look on your face?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] *Sigh*") ]])
		fnCutsceneBlocker()
	end
	
	--[Movement]
	--Wait a moment.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Move Florentina onto Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", 15.25 * gciSizePerTile, 9.5 * gciSizePerTile)
	DL_PopActiveObject()
	fnCutsceneWait(2)
	fnCutsceneBlocker()
	
	--[Florentina Rejoins]
	--Compose the execution string.
	local sString = [[fnAddPartyMember("Florentina")]]

	--Execute
	fnCutsceneInstruction(sString)
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end