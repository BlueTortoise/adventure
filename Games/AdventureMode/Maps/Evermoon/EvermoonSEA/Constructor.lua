--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "EvermoonSEA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "ForestTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/EvermoonSEA/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("EvermoonSEA")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	TA_Create("Alicia")
		TA_SetProperty("Position", 43, 23)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Trannadar/Alicia.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
	DL_PopActiveObject()
	
	--Overlays.
    AL_SetProperty("Allocate Foregrounds", 3)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/ForestA")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 2.7, 2.7)
    AL_SetProperty("Foreground Alpha",          0, 0.40, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 2.0)
	
    AL_SetProperty("Foreground Image",          1, "Root/Images/AdventureUI/MapOverlays/ForestC")
    AL_SetProperty("Foreground Render Offsets", 1, 0.0, 0.0, 2.3, 2.3)
    AL_SetProperty("Foreground Alpha",          1, 0.40, 0)
    AL_SetProperty("Foreground Autoscroll",     1, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          1, 2.0)
	
    AL_SetProperty("Foreground Image",          2, "Root/Images/AdventureUI/MapOverlays/ForestB")
    AL_SetProperty("Foreground Render Offsets", 2, 0.0, 0.0, 2.0, 2.0)
    AL_SetProperty("Foreground Alpha",          2, 0.40, 0)
    AL_SetProperty("Foreground Autoscroll",     2, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          2, 2.0)

end
