--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Examinables]
if(sObjectName == "Foodshelf") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Breads, fish, veggies...[SOFTBLOCK] Now I'm getting hungry!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Bags") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Various unpolished and uncut gems.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Barrels") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This barrel has fish in it.[SOFTBLOCK] It seems pretty fresh.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Bookshelf") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Care Fur Your Coat'.[SOFTBLOCK] Cat grooming tips for the feline enthusiast.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Crops") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (These crops are fairly well taken care of.[SOFTBLOCK] There are lots more budding nearby.)") ]])
	fnCutsceneBlocker()

--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end