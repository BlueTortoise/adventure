--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Examinables]
if(sObjectName == "Sign") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (\"South:: Trafal Glacier.[SOFTBLOCK] Be wary of rock falls, or I might get fired.\"[SOFTBLOCK] -Nadia)") ]])
	fnCutsceneBlocker()
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)

--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end