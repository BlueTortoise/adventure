--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examination]
--Sign next to the rockslide.
if(sObjectName == "Rockslide Sign") then
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (\"Caution:: Rockslide.[SOFTBLOCK] Also, now-hiring:: People to clear rockslides.[SOFTBLOCK] Apply at Trannadar Trading Post.\" -Nadia.)") ]])
	fnCutsceneBlocker()
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)

--Sign on the west side.
elseif(sObjectName == "Sign West") then
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (\"South:: Trafal Glacier Pass.[SOFTBLOCK] North and West:: Trannadar Trading Post.\" -Nadia.)") ]])
	fnCutsceneBlocker()
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)

--Barrel in the northwest.
elseif(sObjectName == "BarrelNW") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like a barrel that smells vaguely of gunpowder.[SOFTBLOCK] It's mostly empty, though.)") ]])
	fnCutsceneBlocker()

--Plants.
elseif(sObjectName == "Plants") then

	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

	--Normal dialogue:
	if(sMeiForm ~= "Alraune") then
		fnStandardDialogue([[ [VOICE|Mei](These plants are very well kept.) ]])
	
	--Alraune.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Clamoring for attention, are you?[SOFTBLOCK] Yes yes, I'm excited to meet you too.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Your vines are so well pruned![SOFTBLOCK] The leaf-sisters here are true experts.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Can't stay too long, though.[SOFTBLOCK] I have adventuring to do!)") ]])
	end

--Plants that slimes really seem to like.
elseif(sObjectName == "SlimePlant") then

	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiTriedFruit = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiTriedFruit", "N")

	--Normal dialogue:
	if(sMeiForm ~= "Alraune" and sMeiForm ~= "Slime") then
		fnStandardDialogue([[[VOICE|Mei](The fruit from these plants is very pungent smelling!)]])
	
	--Slime:
	elseif(sMeiForm == "Slime") then
	
		--If Florentina is not present:
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Hmm, these smell delicious!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Probably not a good idea to eat strange fruit, though.)") ]])
	
		--If Florentina is present:
		else
			fnStandardMajorDialogue()
			
			if(iMeiTriedFruit == 0.0) then
				VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiTriedFruit", "N", 1.0)
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Mmmmm, these fruits smell really, really good![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You can smell stuff with no nose?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I smell with my whole body.[SOFTBLOCK] The other slimes here must smell it too.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well that's because these are -[SOFTBLOCK][EMOTION|Florentina|Happy] oh, you should try one of the fruits.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hmm, eating strange fruit might be unsafe.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I can talk to plants, dummy.[SOFTBLOCK] These are safe.[SOFTBLOCK] Besides, the other slimes have them in their bodies, see?[SOFTBLOCK] They're okay.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If you say so.[SOFTBLOCK][EMOTION|Mei|Offended] But if I get sick, I'll slime-smack you good![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Just eat one.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] I guess I can just shove this inside myself.[SOFTBLOCK] No need for a mouth.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *shlorp*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oooh, that was so sweet![SOFTBLOCK] It tastes - [BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Uuuuhhhh...[SOFTBLOCK] Florentina?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I'm feeling a little -[SOFTBLOCK] hey, were you always blue?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Haaaa ha ha ha![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I'ma tell you a shecret right now.[SOFTBLOCK] Florentina -[SOFTBLOCK] I'm a shlime right now.[SOFTBLOCK] Shhhhh...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I'ma shlime and I'm totally washted...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Why send away for dancers or performers when we have drunk slime Mei for entertainment?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Thish fruit got me drunk?[SOFTBLOCK] Oooohh...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] *shlorp*[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Heh.[SOFTBLOCK] The effects dissipate pretty quickly when the fruit is gone.[SOFTBLOCK] Guess that's what happens when your whole body acts like a liver.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Aw come on, put it back in![SOFTBLOCK] I was having fun![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] We have a job to do![SOFTBLOCK] But -[SOFTBLOCK][EMOTION|Mei|Neutral] remember where these plants are so I can come back later...") ]])
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I don't have time to get hammered right now...[SOFTBLOCK] but...)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (As my alcoholic uncle Liang once said::[SOFTBLOCK] 'Any day not spent wasted, is'.)") ]])
			end
		end
	
	--Alraune.
	elseif(sMeiForm == "Alraune") then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			
		if(iMeiTriedFruit == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (You're very popular with the slimes, it seems.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Oh come on, tell me![SOFTBLOCK] Why!?)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (...[SOFTBLOCK] Always with the pranks![SOFTBLOCK] One of these days I'm going to play a prank on you right back!)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (I don't actually mean that, little one.[SOFTBLOCK] Really![SOFTBLOCK] I'll be nice!)") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (So the slimes eat your fruit to get drunk, and then spread the seeds around as they wander?)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Well aren't you clever little ones!)") ]])
		end
	end

--[Doors]
--Western cabin door. Requires a pry bar to open.
elseif(sObjectName == "StuckCabinDoorW") then

	--Variables.
	local iShowedDoorPry = VM_GetVar("Root/Variables/Chapter1/Scenes/iShowedDoorPry", "N")
	local iHasPryBar = AdInv_GetProperty("Item Count", "Pry Bar")
	
	--Has the pry bar.
	if(iHasPryBar >= 1) then
		
		--Hasn't shown the dialogue, so do that.
		if(iShowedDoorPry == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iShowedDoorPry", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The door is stuck...)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(15)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Got it open with the pry bar!)") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Open Door", "CabinDoorW") ]])
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
			fnCutsceneBlocker()
		
		--Don't repeat the dialogue.
		else
			AL_SetProperty("Open Door", "CabinDoorW")
			AudioManager_PlaySound("World|FlipSwitch")
		end
	
	--Door is stuck.
	else
		AudioManager_PlaySound("World|RemoteDoor")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The door is stuck.[SOFTBLOCK] Maybe I can find something to open it with?)") ]])
		fnCutsceneBlocker()
	end

--Eastern cabin door. Requires a pry bar to open.
elseif(sObjectName == "StuckCabinDoorE") then

	--Variables.
	local iShowedDoorPry = VM_GetVar("Root/Variables/Chapter1/Scenes/iShowedDoorPry", "N")
	local iHasPryBar = AdInv_GetProperty("Item Count", "Pry Bar")
	
	--Has the pry bar.
	if(iHasPryBar >= 1) then
		
		--Hasn't shown the dialogue, so do that.
		if(iShowedDoorPry == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iShowedDoorPry", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The door is stuck...)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(15)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Got it open with the pry bar!)") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Open Door", "CabinDoorE") ]])
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
			fnCutsceneBlocker()
		
		--Don't repeat the dialogue.
		else
			AL_SetProperty("Open Door", "CabinDoorE")
			AudioManager_PlaySound("World|FlipSwitch")
		end
	
	--Door is stuck.
	else
		AudioManager_PlaySound("World|RemoteDoor")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The door is stuck.[SOFTBLOCK] Maybe I can find something to open it with?)") ]])
		fnCutsceneBlocker()
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end