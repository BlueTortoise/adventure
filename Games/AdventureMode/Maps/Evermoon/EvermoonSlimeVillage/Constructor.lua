--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "TownTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("EvermoonSlimeVillage")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
	fnStandardNPCLocation("Guide",            "Guide",            gci_Face_South,  "SlimeB", "Slimeville/Guide")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	fnStandardNPCLocation("GallerySlimeA",    "GallerySlimeA",    gci_Face_North,  "SlimeB", "Slimeville/GallerySlimeA")
	fnStandardNPCLocation("GallerySlimeB",    "GallerySlimeB",    gci_Face_East,   "Slime",  "Slimeville/GallerySlimeB")
	fnStandardNPCLocation("GallerySlimeC",    "GallerySlimeC",    gci_Face_East,   "SlimeG", "Slimeville/GallerySlimeC")
	fnStandardNPCLocation("GallerySlimeD",    "GallerySlimeD",    gci_Face_South,  "SlimeG", "Slimeville/GallerySlimeD")
	fnStandardNPCLocation("GallerySlimeE",    "GallerySlimeE",    gci_Face_South,  "Slime",  "Slimeville/GallerySlimeE")
	fnStandardNPCLocation("GalleryAttendant", "GalleryAttendant", gci_Face_South,  "Slime",  "Slimeville/GalleryAttendant")
	fnStandardNPCLocation("HouseSlimeA",      "HouseSlimeA",      gci_Face_East,   "SlimeG", "Slimeville/HouseSlimeA")
	fnStandardNPCLocation("HouseSlimeB",      "HouseSlimeB",      gci_Face_North,  "SlimeB", "Slimeville/HouseSlimeB")
	fnStandardNPCLocation("HouseSlimeC",      "HouseSlimeC",      gci_Face_West,   "SlimeG", "Slimeville/HouseSlimeC")
	
	--Extended directions.
	EM_PushEntity("GalleryAttendant")
		TA_SetProperty("Extended Activation Direction", gci_Face_South)
	DL_PopActiveObject()
	
	--Crowbar-Chan. Has her own special sprite set.
	TA_Create("CrowbarChan")
		TA_SetProperty("Position By Entity", "CrowbarChan")
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/CrowbarChan/Root.lua")
		for i = 1, 8, 1 do
			TA_SetProperty("Move Frame", i-1, 0, "Root/Images/Sprites/CrowbarChan/0")
			TA_SetProperty("Move Frame", i-1, 1, "Root/Images/Sprites/CrowbarChan/1")
			TA_SetProperty("Move Frame", i-1, 2, "Root/Images/Sprites/CrowbarChan/2")
			TA_SetProperty("Move Frame", i-1, 3, "Root/Images/Sprites/CrowbarChan/1")
		end
		TA_SetProperty("Auto Animates", true)
	DL_PopActiveObject()
	
	--Overlays.
    AL_SetProperty("Allocate Foregrounds", 1)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/ForestA")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 2.5, 2.5)
    AL_SetProperty("Foreground Alpha",          0, 0.50, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 2.0)

end
