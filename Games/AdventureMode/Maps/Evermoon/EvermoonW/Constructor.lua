--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "ForestTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/EvermoonW/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "EvermoonW")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("EvermoonW")
	
	--If this flag is set, spawn Nadia. Also, Florentina if flagged to.
	local iSpawnNadiaEvermoonW = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iSpawnNadiaEvermoonW", "N")
	local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	if(iSpawnNadiaEvermoonW == 1.0) then
		VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iSpawnNadiaEvermoonW", "N", 0.0)
		TA_Create("Nadia")
			TA_SetProperty("Position", 1, 1)
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
		DL_PopActiveObject()
		
		if(iHasFlorentina == 1.0) then
			fnSpecialCharacter("Florentina", "Alraune", 1, 1, gci_Face_West, false, nil)
		end
	end

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--If Mei is any species other than Alraune, remove the staircase that leads to the TF chamber.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sMeiForm ~= "Alraune") then
		AL_RemoveObject("Exit", "AlrauneStairs")
	end
	
	--If this flag is set, spawn Florentina.
	local iAlrauneMeetFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N")
	if(iAlrauneMeetFlorentina == 1.0 or iAlrauneMeetFlorentina == 100.0) then
		fnSpecialCharacter("Florentina", "Alraune", 47, 38, gci_Face_West, false, nil)
	end
	
	--Overlays.
    AL_SetProperty("Allocate Foregrounds", 1)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/ForestA")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 2.5, 2.5)
    AL_SetProperty("Foreground Alpha",          0, 0.50, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 2.0)
end
