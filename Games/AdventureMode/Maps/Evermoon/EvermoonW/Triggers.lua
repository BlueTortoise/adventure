--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Mei must be in Alraune form for this to work.
if(sObjectName == "SetRetreatToStairs") then
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
	--Variables.
	local iMeiVolunteeredToAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N")
	
	--If this flag is set, trigger the Florentina cutscene.
	local iAlrauneMeetFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N")
	if(iAlrauneMeetFlorentina == 1.0) then
		
		--[Flag]
		VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 0.0)
	
		--[Movement]
		--Wait.
		fnCutsceneWait(120)
		fnCutsceneBlocker()
	
		--Florentina looks around.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", -1.0, 1.0)
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", 0.0, 1.0)
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", 1.0, 1.0)
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", 1.0, 0.0)
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Florentina moves to the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (38.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		
		--Face north to open the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", 0.0, -1.0)
		DL_PopActiveObject()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Open the door.
		fnCutsceneInstruction([[AL_SetProperty("Open Door", "CabinDoor")]])
		fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei walks to greet her.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (36.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--[Dialogue]
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--If Mei did not volunteer:
		if(iMeiVolunteeredToAlraune == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] So.[SOFTBLOCK] Are they done with you, then?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Florentina...[SOFTBLOCK] This is...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hrmph.[SOFTBLOCK] They wouldn't even let me be there.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I tried to stop them...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No need to apologize.[SOFTBLOCK] I feel wonderful, leaf-sister.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Don't start with that leaf-sister crap, okay?[SOFTBLOCK] Bad enough they used force like that, worse that you'd enjoy it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not angry.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] No?[SOFTBLOCK] Beaten up, dragged off, and turned into a plant against your will, and you're not angry?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Then allow me to be angry on your behalf, yeah?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Did I do something wrong?[SOFTBLOCK] I'm sorry...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You just don't understand.[SOFTBLOCK] Can we just go?[SOFTBLOCK] Get out of here, away from here and all these smiling freaks...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, all right.[SOFTBLOCK] Focus on the task at hand, right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah.[SOFTBLOCK] That's it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] ...[SOFTBLOCK] Just standing here is bringing back bad memories...") ]])
		
		--If Mei volunteered:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] So.[SOFTBLOCK] Are they done with you, then?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Florentina...[SOFTBLOCK] This is...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Hrmph.[SOFTBLOCK] They wouldn't even let me be there.[SOFTBLOCK] I should have...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I wish you could have been there, leaf-sister.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Knock that off.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Don't call you - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] No.[SOFTBLOCK] I'm not like them.[SOFTBLOCK] I don't want to be like them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I...[SOFTBLOCK] I'm sorry I put you through this.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's nothing.[SOFTBLOCK] If you're happy, we can get going.") ]])
			
		end
		fnCutsceneBlocker()
		
		--[Movement]
		--Move Mei onto Florentina.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (38.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Fold the party up.
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
		--[System]
		--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
		fnAddPartyMember("Florentina")
	
	--If the flag is 100, then teleport Florentina to Mei and have her join the party.
	elseif(iAlrauneMeetFlorentina == 100.0 and gsFollowersTotal < 1) then
	
		--[Movement]
		--Move Mei onto Florentina.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Teleport To", (50.25 * gciSizePerTile), (33.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Fold the party up.
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
		--[System]
		--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
		fnAddPartyMember("Florentina")
        VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 0.0)
	
    end

--Remove the stairs at the secret alraune HQ if Mei is not an alraune:
elseif(sObjectName == "FormRemoveStairs") then

    --Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    --Remove stairs.
	if(sMeiForm ~= "Alraune") then
		AL_RemoveObject("Exit", "AlrauneStairs")
    
    --Spawn the stairs.
    else
        AL_SetProperty("Create Staircase", "AlrauneStairs", 800, 528, 16, 16, "DR", "ExitStairs", "AlrauneChamber")
	end
end