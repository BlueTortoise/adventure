--[Warp Handler]
--If the player warps here using the campfire, this script is called to reposition them as necessary.

--Constants. This is the important part that changes between each room.
local fTargetX = (43.25 * gciSizePerTile)
local fTargetY = (31.50 * gciSizePerTile)

--Execute.
LM_ExecuteScript(gsStandardWarpHandler, fTargetX, fTargetY)

--Overlay flag.
gbNoOverlay = true