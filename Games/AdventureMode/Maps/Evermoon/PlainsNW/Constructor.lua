--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "HighlandsTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/PlainsNW/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "PlainsNW")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("PlainsNW")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Claudia's Acolyte, Karina. Studying bees.
	TA_Create("Karina")
		TA_SetProperty("Position", 31, 13)
		TA_SetProperty("Depth", 1)
		TA_SetProperty("Facing", gci_Face_North)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Karina/Root.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
	DL_PopActiveObject()
	
	--If Mei rescued Claudia, spawn her here:
	local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
	if(iSavedClaudia == 1.0) then
		TA_Create("Claudia")
			TA_SetProperty("Position", 32, 13)
			TA_SetProperty("Depth", 1)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Karina/Root.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/Claudia/", false)
		DL_PopActiveObject()
	end
	
	--Gate Guards.
	fnStandardNPC("GuardE", 40, 26, gci_Face_South, "MercF", "Outland_General/Guard E")
	fnStandardNPC("GuardS", 16, 41, gci_Face_South, "MercM", "Outland_General/Guard S")
	fnStandardNPC("GuardN", 13, 19, gci_Face_North, "MercF", "Outland_General/Guard N")
	
	--Farmers. 
	fnStandardNPC("NPC A", 21, 27, gci_Face_East,  "GenericF1", "Outland_General/NPC A")
	fnStandardNPC("NPC B",  6, 32, gci_Face_South, "GenericM0", "Outland_General/NPC B")
	fnStandardNPC("NPC C",  6, 18, gci_Face_West,  "MercM",     "Outland_General/NPC C")
	fnStandardNPC("NPC D", 21, 20, gci_Face_North, "GenericF1", "Outland_General/NPC D")
	fnStandardNPC("NPC E", 28, 36, gci_Face_North, "MercF",     "Outland_General/NPC E")
	
	--If Mei just got turned into a Bee and came down south, move NPCs around. Also spawn Florentina if that happens.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
	local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	if(iHasSeenOutlandBeeScene == 0.0 and sMeiForm == "Bee") then
		
		--Karina changes position.
		EM_PushEntity("Karina")
			TA_SetProperty("Position", 15, 19)
			TA_SetProperty("Depth", 0)
			if(iHasSeenTrannadarFlorentinaScene == 1.0) then
				TA_SetProperty("Facing", gci_Face_East)
			else
				TA_SetProperty("Facing", gci_Face_North)
			end
		DL_PopActiveObject()
		
		--Claudia changes position as well.
		if(iSavedClaudia == 1.0) then
			EM_PushEntity("Claudia")
				TA_SetProperty("Position", 14, 19)
				TA_SetProperty("Depth", 0)
				TA_SetProperty("Facing", gci_Face_North)
			DL_PopActiveObject()
		end
		
		--Spawn Florentina.
		if(iHasSeenTrannadarFlorentinaScene == 1.0) then
			fnSpecialCharacter("Florentina", "Alraune", 16, 19, gci_Face_West, false, nil)
		end
	end
	
	--Overlays.
    AL_SetProperty("Allocate Foregrounds", 1)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/ForestA")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 2.5, 2.5)
    AL_SetProperty("Foreground Alpha",          0, 0.50, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 2.0)
end
