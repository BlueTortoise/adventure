--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Bed with book on bees.
if(sObjectName == "Bed") then
	fnStandardDialogue([[ [VOICE|Mei](There's a book tucked under the pillow.[SOFTBLOCK] It's called "Bees and You: Getting to Know Our Winged Friends".)[BLOCK][CLEAR] (It has some very detailed pictures of bee reproductive anatomy...) ]])

--Shelf stuffed with goodies.
elseif(sObjectName == "Shelf") then
	fnStandardDialogue([[ [VOICE|Mei](Dried food and medicine.[SOFTBLOCK] There is a lot of bee-sting cream.[SOFTBLOCK] Go figure.) ]])

--Assorted crap lying around.
elseif(sObjectName == "Junk") then
	fnStandardDialogue([[ [VOICE|Mei](Assorted junk in varying states of disrepair.[SOFTBLOCK] Nothing useful.) ]])

--Nothing of interest.
elseif(sObjectName == "Nothing") then
	fnStandardDialogue([[ [VOICE|Mei](Nothing of interest here.) ]])
	
--Barrel.
elseif(sObjectName == "Barrel") then
	fnStandardDialogue([[ [VOICE|Mei](This barrel contains enough apple cider to drown someone in.[SOFTBLOCK] Guess they really like cider here.) ]])
	
--Crops.
elseif(sObjectName == "Crops") then

	--Normal dialogue:
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sMeiForm ~= "Alraune") then
		fnStandardDialogue([[ [VOICE|Mei](Despite the dry climate, these crops are doing fairly well.) ]])
	else
		fnStandardDialogue([[ [VOICE|Mei](The little ones say they're very well taken care of.[SOFTBLOCK] Always nice to see a human farmer who cares about their crops!) ]])
	end
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end