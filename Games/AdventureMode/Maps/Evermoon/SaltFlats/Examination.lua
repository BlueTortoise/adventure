--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Water. Does nothing if Mei doesn't need water at the moment.
if(sObjectName == "Water") then

	--Variables.
	local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
	local iHasWater        = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasWater", "N")
	
	--Normal.
	if(iIsMeiControlled == 0.0) then
	
	--Under control, but no watering can.
	elseif(iIsMeiControlled == 1.0 and iHasWater == 0.0) then
		fnStandardDialogue([[ [VOICE|Mei](Thrall requires a watering can to fill with water.) ]])
	
	--Has a watering can that needs filling.
	elseif(iIsMeiControlled == 1.0 and iHasWater == 1.0) then
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasWater", "N", 2.0)
		fnStandardDialogue([[ [VOICE|Mei](...[SOFTBLOCK] Thrall has filled the watering can.[SOFTBLOCK] Thrall will proceed with task.) ]])
	end

--Special grass, used for farming. Does nothing if Mei doesn't need it.
elseif(sObjectName == "SpecialGrass") then

	--Variables.
	local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
	local iHasGrass        = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasGrass", "N")
	
	--Normal.
	if(iIsMeiControlled == 0.0) then
		fnStandardDialogue([[ [VOICE|Mei](The grass here is a different color.[SOFTBLOCK] Is it a different species?) ]])
	
	--Under control, needs grass.
	elseif(iIsMeiControlled == 1.0 and iHasGrass == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasGrass", "N", 1.0)
		fnStandardDialogue([[ [VOICE|Mei](This thrall has collected the required grasses.[SOFTBLOCK] Proceeding with application.) ]])
	end

--Fertilizer barrel, used for farming.
elseif(sObjectName == "FertilizerBarrel") then

	--Variables.
	local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
	local iPutThingsAway   = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iPutThingsAway", "N")
	local iHasFertilizer   = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasFertilizer", "N")
	
	--Normal.
	if(iIsMeiControlled == 0.0) then
		fnStandardDialogue([[ [VOICE|Mei](This barrel is filled with compost.[SOFTBLOCK] It smells potent!) ]])
	
	--Under control, needs fertilizer.
	elseif(iIsMeiControlled == 1.0 and iHasFertilizer == 0.0 and iPutThingsAway == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasFertilizer", "N", 1.0)
		fnStandardDialogue([[ [VOICE|Mei](This thrall has collected the needed fertilizer.[SOFTBLOCK] This thrall will take care of the plants.) ]])
	
	--Putting it away.
	elseif(iIsMeiControlled == 1.0 and iHasFertilizer > 0.0 and iPutThingsAway == 1.0) then
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasFertilizer", "N", 0.0)
		fnStandardDialogue([[ [VOICE|Mei](Thrall has put fertilizer in the barrel.[SOFTBLOCK] Thrall lives to serve mistress.) ]])
	end

--Watering can case.
elseif(sObjectName == "WateringCanCase") then

	--Variables.
	local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
	local iPutThingsAway   = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iPutThingsAway", "N")
	local iHasWater        = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasWater", "N")
	
	--Normal.
	if(iIsMeiControlled == 0.0) then
		fnStandardDialogue([[ [VOICE|Mei](There's a watering can in this box.) ]])
	
	--Under control, but no watering can.
	elseif(iIsMeiControlled == 1.0 and iHasWater == 0.0 and iPutThingsAway == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasWater", "N", 1.0)
		fnStandardDialogue([[ [VOICE|Mei](Thrall will take the watering can.[SOFTBLOCK] Thrall will fill it with water.[SOFTBLOCK] Thrall obeys.) ]])
	
	--Putting it away.
	elseif(iIsMeiControlled == 1.0 and iHasWater > 0.0 and iPutThingsAway == 1.0) then
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasWater", "N", 0.0)
		fnStandardDialogue([[ [VOICE|Mei](Thrall returns watering can.[SOFTBLOCK] Thrall obeys.) ]])
	end

--Pollen barrel.
elseif(sObjectName == "PollenBarrel") then

--Hypnotic Flower
elseif(sObjectName == "HypnoFlower") then

	--Mei needs to take some of the flower petals:
	local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
	local iFlowerCount = AdInv_GetProperty("Item Count", "Hypnotic Flower Petals")
	local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
	if(iTakenPieJob == 1.0 and iFlowerCount < 1) then

		--Short scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hey, this guy will do.[SOFTBLOCK] He won't mind if we borrow a few petals.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do you think Adina will mind?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] It's just a few petals.[SOFTBLOCK] Quit being a wimp.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Got Hypnotic Flower Petals*") ]])
		
		--Add the item.
		LM_ExecuteScript(gsItemListing, "Hypnotic Flower Petals")
		
		--Reset flag.
		WD_SetProperty("Clear Topic Read", "Pepper Pie")

	elseif(iIsMeiControlled == 0.0) then
		fnStandardDialogue([[[VOICE|Mei](An oddly alluring flower...)]])
	else
		fnStandardDialogue([[[VOICE|Mei](Thrall ignores flower.[SOFTBLOCK] Thrall will find a new task.)]])
	end

--Plots. These are numerically handled.
elseif(string.sub(sObjectName, 1, 4) == "Plot")  then

	--Variables.
	local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")

	--If Mei is free-willed, normal examination.
	if(iIsMeiControlled == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](The plants here are struggling to survive.)") ]])
		fnCutsceneBlocker()

	--Thrall examination.
	else

		--Variables.
		local iPlotNumber = string.sub(sObjectName, 5, 5)
		local iPlotStatus = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iPlotState" .. iPlotNumber, "N")

		--If no work needs to be done:
		if(iPlotStatus == gci_SFF_NoTending) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](These plants do not need tending.[SOFTBLOCK] Thrall will find another task.)") ]])
			fnCutsceneBlocker()
		
		--Something needs to be done, so handle it.
		else
		
			--Handle case.
			local bHandledEvent = false
			
			--If the plot needs watering:
			if(iPlotStatus == gci_SFF_Water) then
				
				--Variables.
				local iHasWater = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasWater", "N")
				
				--Needs to get the watering can.
				if(iHasWater == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Thrall does not have a watering can.[SOFTBLOCK] Thrall must acquire watering can.)") ]])
					fnCutsceneBlocker()
				
				--Needs to fill the watering can.
				elseif(iHasWater == 1.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Thrall's watering can is empty.[SOFTBLOCK] Thrall must fill the can.)") ]])
					fnCutsceneBlocker()
				
				--Water.
				else
					bHandledEvent = true
					fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This thrall has watered the plants.[SOFTBLOCK] Thrall will find another task.)") ]])
					fnCutsceneBlocker()	
				end
			
			--If the plot needs fertilizer:
			elseif(iPlotStatus == gci_SFF_Fertilizer) then
				
				--Variables.
				local iHasFertilizer = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasFertilizer", "N")
				
				--Needs to collect the fertilizer.
				if(iHasFertilizer == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Thrall lacks fertilizer.[SOFTBLOCK] Thrall must collect fertilizer.)") ]])
					fnCutsceneBlocker()
				
				--Apply.
				else
					bHandledEvent = true
					fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Thrall spreads fertilizer.[SOFTBLOCK] Thrall completes task.[SOFTBLOCK] Thrall finds new task.)") ]])
					fnCutsceneBlocker()
				end
			
			--If the plot needs special grass:
			elseif(iPlotStatus == gci_SFF_SpecialGrass) then
				
				--Variables.
				local iHasGrass = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasGrass", "N")
				
				--Needs to collect the grass.
				if(iHasGrass == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This thrall does not have the special grass.[SOFTBLOCK] This thrall needs to collect prepared grass.)") ]])
					fnCutsceneBlocker()
				
				--Apply.
				else
					bHandledEvent = true
					fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Plants need specially prepared grass.[SOFTBLOCK] Thrall places specially prepared grass.[SOFTBLOCK] Thrall finds new task.)") ]])
					fnCutsceneBlocker()
				end
			
			--If the plot needs pollen:
			elseif(iPlotStatus == gci_SFF_SpecialPollen) then
				
				--Variables.
				local iHasPollen = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasPollen", "N")
				
				--Needs to collect the pollen.
				if(iHasPollen == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This thrall requires pollen from the mistress to complete its task.)") ]])
					fnCutsceneBlocker()
				
				--Apply.
				else
					bHandledEvent = true
					fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Thrall spreads pollen from the mistress.[SOFTBLOCK] Thrall inhales pollen from the mistress.[SOFTBLOCK] Thrall will find new task.)") ]])
					fnCutsceneBlocker()
				end
			end
			
			--If the event was handled...
			if(bHandledEvent == true) then
				
				--Set variables and overlay as needed.
				VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState" .. iPlotNumber, "N", gci_SFF_NoTending)
				
				--Get time values.
				local iTimeOfDay      = VM_GetVar("Root/Variables/Global/Time/iTimeOfDay", "N")
				local iTasksDone      = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N")
				local iTasksDoneTotal = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iTasksDoneTotal", "N")
				VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", iTasksDone + 1)
				VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDoneTotal", "N", iTasksDoneTotal + 1)
				
				--Modify the time of day.
				fnModifyTimeOfDay(iTimeOfDay, iTasksDone + 1)
				
				--Move the NPC.
				EM_PushEntity("NPCPLOT" .. iPlotNumber)
					TA_SetProperty("Position", -100, -100)
				DL_PopActiveObject()
				
			end
			
		end
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end