--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Triggers the meeting with Adina if Mei needs it.
if(sObjectName == "MeetAdina") then

	--Variables.
	local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasMetAdina         = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N")
	local iHasAlrauneForm      = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Has not met Adina, fire a cutscene.
	if(iHasMetAdina == 0.0) then
		
		--[Flags]
		--Prevent scene from firing twice.
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 1.0)
		
		--Store which form Mei first met Adina in.
		VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", sMeiForm)
		
		--[Music Change]
		AL_SetProperty("Music", "AdinasTheme")
		
		--[Movement]
		--Move Mei forward.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (15.50 * gciSizePerTile))
		DL_PopActiveObject()
		
		--If Florentina is here, move her as well.
		if(bIsFlorentinaPresent == true) then
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (14.50 * gciSizePerTile))
			DL_PopActiveObject()
		end
		
		--Adina faces west.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Adina")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneBlocker()

		--Wait a bit.
		fnCutsceneInstruction([[ fnPartyStopMovement() ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Adina faces Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Adina")
			ActorEvent_SetProperty("Face", 0, -1)
		DL_PopActiveObject()
		fnCutsceneBlocker()

		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--[Dialogue]
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

		--Baseline: Mei is Human, Florentina is not present, and Mei has never been an Alraune.
		if(sMeiForm == "Human" and bIsFlorentinaPresent == false and iHasAlrauneForm == 0.0) then

			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Greetings, human.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] You're not going to attack me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: I am rather tired. If it's all the same to you, I'd rather not.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] What a relief![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: You look tired as well. Would you like to rest for a moment?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Well that'd -[SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Oh, no![SOFTBLOCK] That's the oldest trick in the book![SOFTBLOCK] No way am I falling for that one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Interesting.[SOFTBLOCK] You do not trust me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Did I make it obvious?[SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: What reason do you have to not trust me, human?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You've got it backwards.[SOFTBLOCK] I need a pretty good reason to trust you, otherwise -[SOFTBLOCK] no way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Ah, I see.[SOFTBLOCK] Humans are paranoid by nature.[SOFTBLOCK] I apologize if I seemed too forward.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You -[SOFTBLOCK] didn't know?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: We Alraunes trust one another, you see.[SOFTBLOCK] Perhaps if I introduced myself?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: My name is Adina. I tend to these salted earths.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: A sweet name.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Maybe if we got to know one another, I could trust you.[SOFTBLOCK] Your cohorts haven't given me a reason to.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: They are merely assuming that you are a danger.[SOFTBLOCK] You *are* openly brandishing a weapon.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll have to give you that one...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Stay as long as you like, Mei.[SOFTBLOCK] I have made sure the creatures of the forest are kept away, they will not threaten you here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (She seems nice, but I bet something is up.[SOFTBLOCK] Better be careful.)") ]])
		
		--Mei is an Alraune, but Florentina is not present.
		elseif(sMeiForm == "Alraune" and bIsFlorentinaPresent == false) then

			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Greetings, leaf-sister.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You are...[SOFTBLOCK] leaf-sister Adina?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Correct.[SOFTBLOCK] I see the little ones cannot help but gossip.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Has it ever been otherwise?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Have they told you the nature of my work here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Some, but not the whole story.[SOFTBLOCK] Please.[SOFTBLOCK] Enlighten me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Certainly.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Long ago, before the river wound this way, this was the site of a battle.[SOFTBLOCK] Two armies of humans clashed here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Like all human conflicts, it was pointless and soon forgotten.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Of course.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: However, to turn the tide of war, a mage brought forth molten stone from the sky.[SOFTBLOCK] It showered his enemies and scarred them horribly.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The battle was won for him, but the stone was rich in salt.[SOFTBLOCK] It scoured the ground, and no life grows here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Even with periodic rains and the river there, it will be centuries before the salt has washed out.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] So you take it upon yourself to remove the salt?[SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I have found several species of little ones that can survive in saline conditions.[SOFTBLOCK] They have agreed to help me, so I transplanted them here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Their descendants are here, reclaiming the earth, but the work is hard.[SOFTBLOCK] They need constant attention.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] But with your dedication, the soil is slowly recovering.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Do you consider me a fool for this errand?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Me?[SOFTBLOCK] Of course not, leaf-sister.[SOFTBLOCK] You're changing the world for the better![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: But the other leaf-sisters...[SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're likely to succeed if you try![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Don't let them get you down.[SOFTBLOCK] They'll be jealous when you have reclaimed the earth here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Well, not jealous.[SOFTBLOCK] That's such a human emotion.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You are a recently joined leaf-sister, are you not?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I am.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Your support is appreciated.[SOFTBLOCK] You are welcome to stay here as long as you like.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm currently on a journey of my own, but perhaps a moment or two here would not be wasted.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: If there's anything I can do to help, please ask.") ]])
	
		--Mei is a bee or slime, Florentina is not present:
		elseif((sMeiForm == "Bee" or sMeiForm == "Slime" or sMeiForm == "Werecat" or sMeiForm == "Ghost") and bIsFlorentinaPresent == false) then

			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Ah, it seems you have wandered across the wards.[SOFTBLOCK] You are a determined creature.[BLOCK][CLEAR]") ]])
			
			if(sMeiForm == "Bee") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Wards?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: And it speaks![SOFTBLOCK] Most intriguing![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Of course I can speak![SOFTBLOCK] Oh, oops.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I sometimes forget I'm a bee.[SOFTBLOCK] It's so natural, I feel like I've been one my whole life![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: What is your name, forest-creature?[BLOCK][CLEAR]") ]])
			elseif(sMeiForm == "Slime") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Wards?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: And it speaks![SOFTBLOCK] Most intriguing![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Of course I can speak![SOFTBLOCK] Oh, oops.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I guess the other slimes don't talk.[SOFTBLOCK] I forgot.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: What is your name, forest-creature?[BLOCK][CLEAR]") ]])
			elseif(sMeiForm == "Werecat") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] You mean the minor tingling?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Those were meant to keep me out?[SOFTBLOCK] Ha ha![SOFTBLOCK] Hilarious![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: What is your name, forest-creature?[BLOCK][CLEAR]") ]])
			elseif(sMeiForm == "Ghost") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I didn't notice anything.[SOFTBLOCK] Are you sure they're working?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I would like to assure you I come in peace.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: What is your name, wilted-one?[BLOCK][CLEAR]") ]])
			end
			
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei.[SOFTBLOCK] You?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Adina.[SOFTBLOCK] I tend these salted earths.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What was that about wards?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I have placed some minor magical wards to keep the forest creatures away.[SOFTBLOCK] My work is difficult, so I cannot entertain visitors easily.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I felt a bit of a stinging feeling, but it wasn't much.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Your willpower, it seems, exceeds that of your kind.[SOFTBLOCK] Since you are not aggressive, I see no reason to repulse you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Thanks for that, I guess.[SOFTBLOCK] So what are you doing here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The soil here was salted centuries ago in a battle between two human armies.[SOFTBLOCK] I am caring for these plants as they bravely extract the salt.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: It is difficult work, but I know this land will be reclaimed in the end.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You've got passion for your work.[SOFTBLOCK] I can respect that.[SOFTBLOCK] I hope you succeed.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Thank you.[SOFTBLOCK] Whatever your errands, I hope they meet with success.") ]])
	
		--Mei is not an Alraune, and Florentina is not present, but Mei has access to Alraune form.
		elseif(sMeiForm ~= "Alraune" and bIsFlorentinaPresent == false and iHasAlrauneForm == 1.0) then
	
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
			VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Completed")

			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Greetings, leaf-sister Mei.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Do not look so surprised![SOFTBLOCK] The little ones mentioned you were coming this way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: A leaf-sister who can change her form.[SOFTBLOCK] Most intriguing.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Seems they can't keep a secret.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Not even for a moment![SOFTBLOCK] Nothing is hidden from them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well that's all right.[SOFTBLOCK] Honestly, the other Alraunes don't seem to recognize me.[SOFTBLOCK] We often...[SOFTBLOCK] come to blows...[BLOCK][CLEAR]") ]])
			
			--Form-sensisitive. First, Human.
			if(sMeiForm == "Human") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: I admit, it is difficult to tell humans apart.[SOFTBLOCK] Perhaps they assume you are a mercenary?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: That's very possible.[BLOCK][CLEAR]") ]])
			
			--Bee.
			elseif(sMeiForm == "Bee") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Perhaps they assume you are after their nectar?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: ...[SOFTBLOCK] Who says I'm not?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Heh.[SOFTBLOCK] Amusing.[BLOCK][CLEAR]") ]])
			
			--Slime.
			elseif(sMeiForm == "Slime") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: I would not believe the little ones, given your appearance.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: The slime is probably a turn-off.[BLOCK][CLEAR]") ]])
			
			--Werecat.
			elseif(sMeiForm == "Werecat") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Admittedly, the werecats often attack us for their amusement.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll not contest the point.[SOFTBLOCK] You look like you'd be a good bit of sport.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Not that I would even think of harming you![BLOCK][CLEAR]") ]])
			
			--Ghost.
			elseif(sMeiForm == "Ghost") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Surely you know that the undead are abhorrent to the daughters of the wild?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I do, but this form has advantages.[SOFTBLOCK] I need it, for the time being.[BLOCK][CLEAR]") ]])
				
			end
			
			--Continue.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: So.[SOFTBLOCK] What brings you here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I was just exploring.[SOFTBLOCK] I'm trying to find my place in the great scheme of things.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: One who walks easily between bodies probably has a higher calling than tilling the earth.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] That's one way of looking at it, but your work here has merit, too.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You were the one who was reclaiming the salt flats.[SOFTBLOCK] Adina, right?[SOFTBLOCK] The little ones spoke of you when I could hear them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Yes, I am her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The work is hard, but I know I will be triumphant in the end.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: The earth here is so coated in salt that it would take centuries to wash away normally.[SOFTBLOCK] The little ones require constant care to survive.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I admit, your task seems daunting.[SOFTBLOCK] Nature will thank you in the end.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I appreciate your encouragement.[SOFTBLOCK] If you need a place to rest on your journey, look no further.") ]])
	
		--If Florentina is present, and Mei is not in Alraune form, this occurs:
		elseif(bIsFlorentinaPresent == true and sMeiForm ~= "Alraune") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iNeedsToSeeSaltFlatsScene", "N", 1.0)
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
			VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaMetAdina", "N", 1.0)
		
			--Add Florentina to the lineup.
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Null") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Hello, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Adina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You have brought company, I see.[BLOCK][CLEAR]") ]])
			
			--If Mei is a bee, this extra bit plays:
			if(sMeiForm == "Bee") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nice to meet you![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: This bee speaks?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] It's getting her to shut up that's hard.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: That is not the attitude I would have in the company of such a bee.[BLOCK][CLEAR]") ]])
			
			--If Mei is a slime, this extra bit plays:
			elseif(sMeiForm == "Slime") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nice to meet you![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: And you bring a slime that has learned to speak?[SOFTBLOCK] Truly, the world is filled with wonders.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Yeah, wonders.[SOFTBLOCK] Sure.[BLOCK][CLEAR]") ]])
			end
			
			--Dialogue. If Mei is a human, this segues naturally from the previous.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do you two know each other?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] No, actually.[SOFTBLOCK] Never met.[SOFTBLOCK] But, the little ones talk about Adina all the time.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Likewise, they whisper of you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm just glad she's not trying to attack us.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Personally, I would rather not.[SOFTBLOCK] I am far too worn from my labours here.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: If you intend to attack me, now is not a convenient time.[SOFTBLOCK] Later, perhaps?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I think you can work us into your schedule - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Florentina, no![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Hello, Adina.[SOFTBLOCK] My name is Mei.[SOFTBLOCK] Pleased to meet you.[BLOCK][CLEAR]") ]])
			
			--Human:
			if(sMeiForm == "Human") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You are most cordial, for a human.[BLOCK][CLEAR]") ]])
			
			--Bee:
			elseif(sMeiForm == "Bee") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You are most cordial.[SOFTBLOCK] Would that all bees were like you.[BLOCK][CLEAR]") ]])
			
			--Slime:
			elseif(sMeiForm == "Slime") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You are most cordial.[SOFTBLOCK] Would that all slimes were like you.[BLOCK][CLEAR]") ]])
				
			--Werecat:
			elseif(sMeiForm == "Werecat") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: You are most cordial, unlike your kin.[BLOCK][CLEAR]") ]])
			
			--Ghost:
			elseif(sMeiForm == "Ghost") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: My dealings with the unliving are...[SOFTBLOCK] uncommon.[SOFTBLOCK] You are certainly the most polite.[BLOCK][CLEAR]") ]])
			end
			
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I just like making friends, not enemies.[SOFTBLOCK] Right, Florentina?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're way too trusting, kid.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Most surprising.[SOFTBLOCK] Thank you for your tact.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] So...[SOFTBLOCK] what are you doing out here by yourself?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I am not alone, I am surrounded by the voices of the forest.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] Ugh.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh][EMOTION|Florentina|Neutral] Well, what are you doing out here, surrounded by voices?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina:[EMOTION|Mei|Neutral] This earth has been scarred, and covered in salt, by some ancient conflict.[SOFTBLOCK] Nothing has grown here for centuries.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: On my travels, I met a few species of little ones who agreed to help me cleanse it.[SOFTBLOCK] So, I care for them as they work the salt from the soil.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] What a waste of time.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Pray tell, Florentina, what would you do that would be less wasteful?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's not my problem.[SOFTBLOCK] I'd leave this place to stew in its salty juices.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Who cares if there's a few meters of earth not covered in grass?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Your attitude is common, even amongst Alraunes.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well I think it's a great idea![SOFTBLOCK] Good for you![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, thank you.[SOFTBLOCK] Truly.[SOFTBLOCK] I am often called a fool even by my own kin, though not for the reasons that Florentina espouses.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're likely to succeed if you try.[SOFTBLOCK] Try not to give up![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Words to live by.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, and yes, Florentina.[SOFTBLOCK] You are welcome here if you need a moment to rest.[SOFTBLOCK] Stay if you like.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Nice to meet you, Adina!") ]])
		
		--If Florentina is present, and Mei is in Alraune form:
		elseif(bIsFlorentinaPresent == true and sMeiForm == "Alraune") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
			VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaMetAdina", "N", 1.0)
		
			--Add Florentina to the lineup.
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Null") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Hello, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Adina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Adina?[SOFTBLOCK] You're the one that tends the salt flats?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: My reputation precedes me.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I heard the little ones whispering of you.[SOFTBLOCK] I can see how monumental your task is.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: It is.[SOFTBLOCK] Legend has it that a mage blasted his enemies with molten rock from the sky.[SOFTBLOCK] The rock was salt, and it stays here to this day.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I have found little ones who will aid me in cleansing it.[SOFTBLOCK] They are resistant to salt, but the work is hard.[SOFTBLOCK] I care for them as best I can.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You should give up.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: And why would I do that?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Because not every millimeter of the world has to be covered in grass?[SOFTBLOCK] Because it's okay to just let things lie?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: It goes deeper than that...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's not part of the natural cycle, Florentina.[SOFTBLOCK] This is one of humanity's mistakes, and we correct for it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Ah, cripes![SOFTBLOCK] Not you too, Mei![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Mei, was it?[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Most of the other Alraunes do not agree as you do.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're likely to succeed if you try.[SOFTBLOCK] Try not to give up![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Wise words.[SOFTBLOCK] Where did you hear them?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's...[SOFTBLOCK] uh...[SOFTBLOCK] not important...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] If you want to while away your time here, I'm not going to stop you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: I would not wish to impose on you, either.[SOFTBLOCK] Do what you will with yours, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I intend to.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: However, if you wish to stay here for a moment, you are welcome to.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Very nice to meet you, leaf-sister Adina.") ]])
		
		end
		
		--Common.
		fnCutsceneBlocker()
		
		--If Florentina is here, move her onto Mei and fold the party.
		if(bIsFlorentinaPresent == true) then
			
			--Move.
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneBlocker()

			--Fold the party positions up.
			fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
			fnCutsceneBlocker()
		end

	end

--If Mei is currently mind-controlled, this zone keeps her from wandering off duty.
elseif(sObjectName == "Walkback") then

	--Variables.
	local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
	if(iIsMeiControlled == 1.0) then
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thrall:[VOICE|Mei] (Mistress has not dismissed me.)") ]])
		fnCutsceneBlocker()
		
		--Move Mei south.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (35.75 * gciSizePerTile), (13.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
	end

--A short scene plays here after meeting Adina with Florentina present.
elseif(sObjectName == "PostAdinaFlorentinaScene") then

	--Variables. If flagged, play the scene.
	local iNeedsToSeeSaltFlatsScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iNeedsToSeeSaltFlatsScene", "N")
	if(iNeedsToSeeSaltFlatsScene == 1.0) then
		
		--Unflag so it doesn't play twice.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iNeedsToSeeSaltFlatsScene", "N", 0.0)

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hey, Mei?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yes?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Just what the hell is wrong with you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Be more specific.[SOFTBLOCK] We could start with my childhood and go from there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Why are you all lovey-dovey with everyone you meet?[SOFTBLOCK] That attitude is going to get you killed someday.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You mean Adina?[SOFTBLOCK] But she's not a bad person![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Someone's going to take advantage of you if you're too trusting.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Hm.[SOFTBLOCK] I'm not sure I want to be paranoid all the time.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If you assume everyone is out to get you, you'll never have any friends.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Friends are better termed 'liabilities'.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Is that how you see me?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[SOFTBLOCK] No.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Because you're a goodie-[SOFTBLOCK]freaking-[SOFTBLOCK]two-shoes.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, how's this?[SOFTBLOCK] As long as you're around, I'll be the nice one.[SOFTBLOCK] If you're not with me, I'll be a jerk.[SOFTBLOCK] Deal?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Deal.[SOFTBLOCK] Pleasure doing business with you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Likewise.") ]])
	end
	
--Disables the forest overlay.
elseif(sObjectName == "NoOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 15)
	
--Disables the forest overlay instantly.
elseif(sObjectName == "NoOverlayFast") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 0)
	
--Enables the forest overlay.
elseif(sObjectName == "YesOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.50, 15)
	
end
