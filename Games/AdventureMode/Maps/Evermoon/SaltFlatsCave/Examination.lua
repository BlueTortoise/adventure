--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Variables]
--All examinations use the control variable in some way.
local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")

--[Execution]
--Bookshelf.
if(sObjectName == "Bookshelf") then

	--Normal case:
	if(iIsMeiControlled == 0.0) then
		fnStandardDialogue("[VOICE|Mei](Various books on farming in extreme conditions.[SOFTBLOCK] Extreme heat, humidity, pest control...)")
	
	--Thrall:
	else
		fnStandardDialogue("[VOICE|Mei](Thrall has not been ordered to read. Thrall does not know what it has not been told to know.)")
	end
	
--Bed.
elseif(sObjectName == "Bed") then

	--Normal case:
	if(iIsMeiControlled == 0.0) then
		fnStandardDialogue("[VOICE|Mei](A bed.[SOFTBLOCK] I guess Adina doesn't want to sleep on the salty earth here...)")
	
	--Thrall:
	else
		fnStandardDialogue("[VOICE|Mei](The mistress' bed requires no maintenance.[SOFTBLOCK] Thrall will find a new task.)")
	end
	
--Bed.
elseif(sObjectName == "Barrel") then

	--Normal case:
	if(iIsMeiControlled == 0.0) then
		fnStandardDialogue("[VOICE|Mei](This barrel is full of some sort of sealant.[SOFTBLOCK] The cave must be waterproofed somehow.)")
	
	--Thrall:
	else
		fnStandardDialogue("[VOICE|Mei](Thrall does not require barrel of sealant for its tasks.[SOFTBLOCK] Thrall will find a new task.)")
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end