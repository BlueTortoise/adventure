--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "TownTheme")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "TrannadarTradingPost")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/TrannadarTradingPost/")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Map Setup
	fnResolveMapLocation("TrannadarTradingPost")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Determine which side the player entered from.
	local bIsPlayerAtBottom = false
    local bIsPlayerAtTop = false
	EM_PushEntity("Adventure Party", 0)
		local iPlayerX, iPlayerY = TA_GetProperty("Position")
	DL_PopActiveObject()
	if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
	if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end
	
	--Variables for cutscenes:
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasSeenTrannadarFirstScene  = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
	local iHasSeenTrannadarSecondScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N")
	local iHasSeenTrannadarThirdScene  = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local sTrannadarFirstSceneForm     = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S")
	local sTrannadarSecondSceneForm    = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S")
	
	--Trap Dungeon Informer
	local iInformedOfDungeon   = VM_GetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N")
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	AdvCombat_SetProperty("Push Party Member", "Mei")
		local iMeiLevel = AdvCombatEntity_GetProperty("Level")
	DL_PopActiveObject()
	
	--In all cases, mark the combat intro dialogues as complete.
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")
	
	--Nadia always spawns on the side the player is on.
	TA_Create("Nadia")
		if(bIsPlayerAtBottom) then
			TA_SetProperty("Position", 30, 37)
			TA_SetProperty("Facing", gci_Face_South)
        elseif(bIsPlayerAtTop) then
			TA_SetProperty("Position", 21, 7)
			TA_SetProperty("Facing", gci_Face_North)
		else
			TA_SetProperty("Position", 41, 26)
			TA_SetProperty("Facing", gci_Face_East)
		end
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Nadia/Root.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
	DL_PopActiveObject()
		
	--Blythe spawns in his office 50% of the time, and 50% of the time will be near the watering hole.
	local iRoll = LM_GetRandomNumber(0, 99)
	TA_Create("Blythe")
		if(iRoll < 50) then
			TA_SetProperty("Position", 14, 27 - 0.10)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Extended Activation Direction", gci_Face_East)
		else
			TA_SetProperty("Position", 20, 11)
			TA_SetProperty("Facing", gci_Face_South)
		end
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Blythe/Root.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/Blythe/", false)
	DL_PopActiveObject()
	
	--If Florentina is not in the party, she's in the back room of her shop.
	local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
	if(bIsFlorentinaInParty == false) then
		fnSpecialCharacter("Florentina", "Alraune", 32, 15, gci_Face_North, false, nil)
	end
	
	--Hypatia, Florentina's assistant. She's in the shop behind the counter.
	TA_Create("Hypatia")
		TA_SetProperty("Position", 31, 20)
		TA_SetProperty("Facing", gci_Face_East)
		TA_SetProperty("Extended Activation Direction", gci_Face_East)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Hypatia/Root.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/GenericF1/", false)
	DL_PopActiveObject()
	
	--Equipment vendor.
	TA_Create("EquipmentVendor")
		TA_SetProperty("Position", 27, 19)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Extended Activation Direction", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/TranEquipVendor/Root.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/MercM/", false)
	DL_PopActiveObject()
	
	--Goat. Baa.
	TA_Create("Goat")
		TA_SetProperty("Position", 30, 30)
		TA_SetProperty("Facing", gci_Face_West)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Goat/Root.lua")
		fnSetCharacterGraphics("Goat", false)
		TA_SetProperty("Auto Animates", true)
	DL_PopActiveObject()
	
	--Cassandra. Spawns here if the party saved her.
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
	local iSavedCassandra = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N")
	if(iStartedCassandraEvent == 2.0 and iSavedCassandra == 1.0) then
		TA_Create("Cassandra")
			TA_SetProperty("Position", 25, 23)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Cassandra/Root_TradingPost.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/CassandraH/", false)
		DL_PopActiveObject()
	end
	
	--Amulet vendor.
	fnStandardNPC("AmuletVendor", 34, 32, gci_Face_North, "GenericF0", "AmuletVendor/Root")
	
	--General NPCs who say one line of dialogue.
	fnStandardNPC("NPC A", 21, 33, gci_Face_West,  "MercF",     "Trannadar_General/NPC A")
	fnStandardNPC("NPC B", 24, 17, gci_Face_West,  "GenericF1", "Trannadar_General/NPC B")
	fnStandardNPC("NPC C", 14, 10, gci_Face_South, "MercM",     "Trannadar_General/NPC C")
	fnStandardNPC("NPC D", 37, 23, gci_Face_South, "MercF",     "Trannadar_General/NPC D")
	fnStandardNPC("NPC E", 16, 17, gci_Face_West,  "MercM",     "Trannadar_General/NPC E")
	fnStandardNPC("NPC F", 39, 32, gci_Face_West,  "GenericF1", "Trannadar_General/NPC F")
	fnStandardNPC("NPC G", 44, 18, gci_Face_East,  "Alraune",   "Trannadar_General/NPC G")
	fnStandardNPC("NPC I", 38, 15, gci_Face_South, "GenericF0", "Trannadar_General/NPC I")
	
	--This guard takes whatever the opposite position is to Nadia.
	if(bIsPlayerAtBottom) then
		fnStandardNPC("Gate Guard", 41, 26, gci_Face_East, "MercM", "Trannadar_General/NPC H")
	else
		fnStandardNPC("Gate Guard", 30, 37, gci_Face_South, "MercM", "Trannadar_General/NPC H")
	end
		
	--First entrance cutscene:
	if(iHasSeenTrannadarFirstScene == 0.0 and gbBypassTrannadarCutscene == false) then
	
		--Change the music.
		AL_SetProperty("Music", "NadiasTheme")
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S", sMeiForm)
	
		--Blythe will reposition right next to Nadia during this scene.
		EM_PushEntity("Blythe")
			if(bIsPlayerAtBottom) then
				TA_SetProperty("Position", 29, 37)
				TA_SetProperty("Facing", gci_Face_East)
            elseif(bIsPlayerAtTop) then
				TA_SetProperty("Position", 22, 7)
				TA_SetProperty("Facing", gci_Face_West)
			else
				TA_SetProperty("Position", 41, 27)
				TA_SetProperty("Facing", gci_Face_North)
			end
		DL_PopActiveObject()
		
		--Alraune
		if(sMeiForm == "Alraune") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneAlraune/Scene_Begin.lua")
		
		--Bee
		elseif(sMeiForm == "Bee") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneBee/Scene_Begin.lua")
		
		--Slime
		elseif(sMeiForm == "Slime") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneSlime/Scene_Begin.lua")
		
		--Ghost
		elseif(sMeiForm == "Ghost") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneGhost/Scene_Begin.lua")
		
		--Werecat
		elseif(sMeiForm == "Werecat") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneWerecat/Scene_Begin.lua")
			
		--Catch-all case defualts to human.
		else
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneHuman/Scene_Begin.lua")
		end
	
	--Cutscene where species is different than first entrance species, and the first species was human.
	elseif(sTrannadarFirstSceneForm == "Human" and sMeiForm ~= "Human" and iHasSeenTrannadarSecondScene == 0.0 and iHasSeenTrannadarThirdScene == 0.0 and gbBypassTrannadarCutscene == false) then
	
		--Change the music.
		AL_SetProperty("Music", "NadiasTheme")
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S", sMeiForm)
		
		--Alraune
		if(sMeiForm == "Alraune") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneAlraune/Scene_Begin.lua")
		
		--Bee
		elseif(sMeiForm == "Bee") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneBee/Scene_Begin.lua")
		
		--Slime
		elseif(sMeiForm == "Slime") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneSlime/Scene_Begin.lua")
		
		--Ghost.
		elseif(sMeiForm == "Ghost") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneGhost/Scene_Begin.lua")
		
		--Werecat.
		elseif(sMeiForm == "Werecat") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneWerecat/Scene_Begin.lua")
		end

	--Cutscene where species is different than the first entrance species, and the first species was not human.
	elseif(sTrannadarFirstSceneForm ~= "Human" and sMeiForm ~= sTrannadarFirstSceneForm and iHasSeenTrannadarSecondScene == 0.0 and iHasSeenTrannadarThirdScene == 0.0 and gbBypassTrannadarCutscene == false) then
	
		--Change the music.
		AL_SetProperty("Music", "NadiasTheme")
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		--Execute.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_ThirdScene/Scene_Begin.lua")

	--Cutscene where species is different from the second scene species.
	elseif(sMeiForm ~= sTrannadarSecondSceneForm and iHasSeenTrannadarSecondScene == 1.0 and iHasSeenTrannadarThirdScene == 0.0 and gbBypassTrannadarCutscene == false) then
	
		--Change the music.
		AL_SetProperty("Music", "NadiasTheme")
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		--Execute.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_ThirdScene/Scene_Begin.lua")

	--Nadia informs Mei of the Trap Dungeon events.
	elseif(iInformedOfDungeon == 0.0 and bIsFlorentinaPresent == true and iMeiLevel >= 5) then
		
		--Right-side spawn:
		if(bIsPlayerAtBottom == false and bIsPlayerAtTop == false) then
			fnCutsceneMove("Mei", 43.25, 28.50)
			fnCutsceneMove("Florentina", 44.25, 28.50)
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneInstruction([[ fnPartyStopMovement() ]])
			fnCutsceneWait(25)
			fnCutsceneBlocker()

			--Nadia faces right.
			fnCutsceneFace("Nadia", 1, 0)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Top spawn.
		elseif(bIsPlayerAtTop == true) then
			fnCutsceneMove("Mei", 22.25, 6.50)
			fnCutsceneMove("Florentina", 23.25, 6.50)
			fnCutsceneBlocker()
		
		--Bottom spawn:
		else
			fnCutsceneMove("Mei", 28.25, 39.50)
			fnCutsceneMove("Florentina", 29.25, 39.50)
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneInstruction([[ fnPartyStopMovement() ]])
			fnCutsceneWait(25)
			fnCutsceneBlocker()

			--Nadia faces north.
			fnCutsceneFace("Nadia", 0, 1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
		end
		
		--Run the common dialogue:
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Nadia_InformOfDungeon/Scene_Begin.lua")
			
		--Right-side spawn:
		if(bIsPlayerAtBottom == false and bIsPlayerAtTop == false) then
			fnCutsceneMove("Florentina", 43.25, 28.50)
			fnCutsceneBlocker()
        
        --Top.
		elseif(bIsPlayerAtTop == true) then
			fnCutsceneMove("Florentina", 22.25, 6.50)
			fnCutsceneBlocker()
		
		--Bottom spawn:
		else
			fnCutsceneMove("Florentina", 28.25, 39.50)
			fnCutsceneBlocker()
		end
		
		--Fold the party.
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
			
	--Normal, no cutscenes:
	else
	
	end
	
	--Overlays.
    AL_SetProperty("Allocate Foregrounds", 1)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/ForestA")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 2.5, 2.5)
    AL_SetProperty("Foreground Alpha",          0, 0.50, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 2.0)
end
