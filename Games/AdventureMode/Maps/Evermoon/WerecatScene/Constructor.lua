--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "Null")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "WerecatScene")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Map Setup
	fnResolveMapLocation("WerecatScene")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Nadia. Place her off the map edge. She needs the special sleep frame.
	TA_Create("Nadia")
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Facing", gci_Face_East)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Nadia/WerecatScene.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
        TA_SetProperty("Add Special Frame", "Sleep", "Root/Images/Sprites/Special/Nadia|Sleep")
	DL_PopActiveObject()
	
	--Spawner function.
	local fnSpawnWerecat = function(sName, iX, iY, iFacing)
		TA_Create(sName)
			TA_SetProperty("Position", iX, iY)
			TA_SetProperty("Facing", iFacing)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Wounded")
		DL_PopActiveObject()
	end
	
	--Werecats.
	fnSpawnWerecat("WerecatA", 15, 17, gci_Face_East)
	fnSpawnWerecat("WerecatB", 19, 17, gci_Face_West)
	fnSpawnWerecat("WerecatC", 16, 14, gci_Face_South)
	fnSpawnWerecat("WerecatD", 18, 14, gci_Face_South)
	fnSpawnWerecat("WerecatE", 19, 12, gci_Face_South)
	fnSpawnWerecat("WerecatF", 15, 12, gci_Face_South)
	
	--Overlays.
    AL_SetProperty("Allocate Foregrounds", 1)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/ForestA")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 2.5, 2.5)
    AL_SetProperty("Foreground Alpha",          0, 0.50, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 2.0)

end
