--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examinations]
--Western bookshelf.
if(sObjectName == "BookshelfA") then
	
	fnStandardDialogue("[VOICE|Mei](A few moldy old books.[SOFTBLOCK] Nothing interesting.)")
	
--Northern bookshelf.
elseif(sObjectName == "BookshelfB") then
	
	fnStandardDialogue("[VOICE|Mei](Most of the books are so faded as to be unreadable.)")
	
--Plants out front.
elseif(sObjectName == "WildPlant") then
	
	--Variables.
	local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
	
	--Normal case:
	if(iHasAlrauneForm == 0.0) then
		fnStandardDialogue("[VOICE|Mei](This plant has grown wild for several seasons by the look of it.)")
	
	--Has Alraune form:
	else
		fnStandardDialogue("[VOICE|Mei](Unfortunately I can't ask the little one about this cabin's owner right now...)")
	end
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end