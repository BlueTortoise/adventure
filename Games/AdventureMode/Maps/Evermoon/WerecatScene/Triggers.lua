--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Changes time of day.
if(sObjectName == "Time0") then

	--Get the time of day:
	local iTimeOfDay = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iTimeOfDay", "N")
	
	--Fade if we're before that time of day.
	if(iTimeOfDay < 1) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iTimeOfDay", "N", 1.0)
		
		--This is the 1/3rd mark.
		local fPercentComplete = 0.33
		
		--Compute, set.
		local fRS = gcf_Noon_R
		local fGS = gcf_Noon_G
		local fBS = gcf_Noon_B
		local fAS = gcf_Noon_A
		local fRE = gcf_Noon_R + ((gcf_Evening_R - gcf_Noon_R) * fPercentComplete)
		local fGE = gcf_Noon_G + ((gcf_Evening_G - gcf_Noon_G) * fPercentComplete)
		local fBE = gcf_Noon_B + ((gcf_Evening_B - gcf_Noon_B) * fPercentComplete)
		local fAE = gcf_Noon_A + ((gcf_Evening_A - gcf_Noon_A) * fPercentComplete)
		AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, fRS, fGS, fBS, fAS, fRE, fGE, fBE, fAE, true)
	end
	
--Changes time of day.
elseif(sObjectName == "Time1") then

	--Get the time of day:
	local iTimeOfDay = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iTimeOfDay", "N")
	
	--Fade if we're before that time of day.
	if(iTimeOfDay < 2) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iTimeOfDay", "N", 2.0)
		
		--This is the 2/3rds mark.
		local fPercentOld = 0.33
		local fPercentComplete = 0.66
		
		--Compute, set.
		local fRS = gcf_Noon_R + ((gcf_Evening_R - gcf_Noon_R) * fPercentOld)
		local fGS = gcf_Noon_G + ((gcf_Evening_G - gcf_Noon_G) * fPercentOld)
		local fBS = gcf_Noon_B + ((gcf_Evening_B - gcf_Noon_B) * fPercentOld)
		local fAS = gcf_Noon_A + ((gcf_Evening_A - gcf_Noon_A) * fPercentOld)
		local fRE = gcf_Noon_R + ((gcf_Evening_R - gcf_Noon_R) * fPercentComplete)
		local fGE = gcf_Noon_G + ((gcf_Evening_G - gcf_Noon_G) * fPercentComplete)
		local fBE = gcf_Noon_B + ((gcf_Evening_B - gcf_Noon_B) * fPercentComplete)
		local fAE = gcf_Noon_A + ((gcf_Evening_A - gcf_Noon_A) * fPercentComplete)
		AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, fRS, fGS, fBS, fAS, fRE, fGE, fBE, fAE, true)
	end
	
--Changes time of day.
elseif(sObjectName == "Time2") then

	--Get the time of day:
	local iTimeOfDay = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iTimeOfDay", "N")
	
	--Fade if we're before that time of day.
	if(iTimeOfDay < 3) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iTimeOfDay", "N", 3.0)
		
		--Finish.
		local fPercentOld = 0.66
		local fPercentComplete = 1.00
		
		--Compute, set.
		local fRS = gcf_Noon_R + ((gcf_Evening_R - gcf_Noon_R) * fPercentOld)
		local fGS = gcf_Noon_G + ((gcf_Evening_G - gcf_Noon_G) * fPercentOld)
		local fBS = gcf_Noon_B + ((gcf_Evening_B - gcf_Noon_B) * fPercentOld)
		local fAS = gcf_Noon_A + ((gcf_Evening_A - gcf_Noon_A) * fPercentOld)
		local fRE = gcf_Noon_R + ((gcf_Evening_R - gcf_Noon_R) * fPercentComplete)
		local fGE = gcf_Noon_G + ((gcf_Evening_G - gcf_Noon_G) * fPercentComplete)
		local fBE = gcf_Noon_B + ((gcf_Evening_B - gcf_Noon_B) * fPercentComplete)
		local fAE = gcf_Noon_A + ((gcf_Evening_A - gcf_Noon_A) * fPercentComplete)
		AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, fRS, fGS, fBS, fAS, fRE, fGE, fBE, fAE, true)
	end

--Walkback trigger.
elseif(sObjectName == "Walkback") then

	--If it's night time, ignore this trigger.
	local iIsNight = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iIsNight", "N")
	if(iIsNight == 0.0) then

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Maybe I should check out that cabin over there...)") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Walk.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move Amount", 32, 0)
		DL_PopActiveObject()
		fnCutsceneBlocker()

	end

--Nadia appears.
elseif(sObjectName == "NadiaTrigger") then

	--Don't play this scene twice.
	local iIsNight = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iIsNight", "N")
	if(iIsNight == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iIsNight", "N", 1.0)
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Defeat_Werecat/Scene_Nadia.lua")
	end
	
--Kinfang trigger.
elseif(sObjectName == "Kinfang") then

	--Don't play this scene twice.
	local iIsMeiWerecat = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iIsMeiWerecat", "N")
	if(iIsMeiWerecat == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iSpawnNadiaEvermoonW", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iIsMeiWerecat", "N", 1.0)
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Defeat_Werecat/Scene_Kinfang.lua")
	end

--Catfight trigger.
elseif(sObjectName == "Catfight") then

	--Don't play this scene unless Mei is a werecat.
	local iIsMeiWerecat = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iIsMeiWerecat", "N")
	if(iIsMeiWerecat == 1.0) then
		VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iSpawnNadiaEvermoonW", "N", 1.0)
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Defeat_Werecat/Scene_Catfight.lua")
	end

end