--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "NixNedar")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/NixNedarMain/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "NixNedarMain")
	
	--Background
	AL_SetProperty("Background", "Root/Images/AdvMaps/Backgrounds/NixNedar", -0, -0, -0.1, -0.1)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Map Setup
	fnResolveMapLocation("NixNedar")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--NPC Construction Functions
	local function fnSpawnRilmani(sName, iX, iY, iFacing, sDialoguePath)
		TA_Create(sName)
			TA_SetProperty("Position", iX, iY)
			TA_SetProperty("Facing", iFacing)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", sDialoguePath)
			fnSetCharacterGraphics("Root/Images/Sprites/Rilmani/", false)
		DL_PopActiveObject()
	end

	--Standard Rilmani NPCs.
	fnSpawnRilmani("RilmaniA", 18, 17, gci_Face_South, gsRoot .. "CharacterDialogue/NixNedar/RilmaniA.lua")
	fnSpawnRilmani("RilmaniB", 11, 16, gci_Face_South, gsRoot .. "CharacterDialogue/NixNedar/RilmaniB.lua")
	fnSpawnRilmani("RilmaniC",  4, 19, gci_Face_West,  gsRoot .. "CharacterDialogue/NixNedar/RilmaniC.lua")
	fnSpawnRilmani("RilmaniD",  4, 20, gci_Face_West,  gsRoot .. "CharacterDialogue/NixNedar/RilmaniD.lua")
	fnSpawnRilmani("RilmaniE", 28, 16, gci_Face_East,  gsRoot .. "CharacterDialogue/NixNedar/RilmaniE.lua")
	fnSpawnRilmani("RilmaniF", 30, 18, gci_Face_East,  gsRoot .. "CharacterDialogue/NixNedar/RilmaniF.lua")
	fnSpawnRilmani("RilmaniG", 21, 16, gci_Face_West,  gsRoot .. "CharacterDialogue/NixNedar/RilmaniG.lua")

	--If Mei has not spoken to Septima yet:
	local iMetSeptima = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSeptima", "N")
	if(iMetSeptima == 0.0) then
		TA_Create("Septima")
			TA_SetProperty("Position", 20, 9)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Septima/Root.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/Septima/", false)
		DL_PopActiveObject()

	--If Mei has spoken to Septima:
	else
		fnSpawnRilmani("RilmaniZ", 19, 25, gci_Face_East,  gsRoot .. "CharacterDialogue/NixNedar/RilmaniZ.lua")


	end
end
