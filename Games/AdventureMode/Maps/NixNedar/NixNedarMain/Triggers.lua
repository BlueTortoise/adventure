--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Meeting Septima.
if(sObjectName == "SeptimaScene" and iIsWholeCollision == 1.0) then

	--Variables.
	local iMetSeptima = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSeptima", "N")
	if(iMetSeptima == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSeptima", "N", 1.0)
		
		--This script does the rest.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/NixNedar/Scene_MeetSeptima.lua")
	end
	
--Sample
elseif(sObjectName == "CorgiScene" and iIsWholeCollision == 1.0) then

	--Variables
	local iSawCorgis = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawCorgis", "N")
	if(iSawCorgis == 0.0) then
	
		--[Flag]
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawCorgis", "N", 1.0)
	
		--[Movement]
		--Move Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (7.25 * gciSizePerTile), (20.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--[North Rilmani Speaks]
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniC")
			ActorEvent_SetProperty("Face",  0, 1)
		DL_PopActiveObject()
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] So I left my cane in your house the other day.[SOFTBLOCK] When I went to get it back...") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--[South Rilmani Speaks]
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniD")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] Oh no![SOFTBLOCK] You saw the statue!") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--[North Rilmani Speaks]
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniC")
			ActorEvent_SetProperty("Face",  -1, 0)
		DL_PopActiveObject()
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] Yes![SOFTBLOCK] You have a tiny little corgi statue on your end table![SOFTBLOCK] That's so not fair![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] I want one![SOFTBLOCK] I know exactly where I'd put it, and I'd get a little stone food bowl and a stone ball for him to play with!") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--[South Rilmani Speaks]
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniD")
			ActorEvent_SetProperty("Face",  -1, 0)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniD")
			ActorEvent_SetProperty("Face", 0, -1)
		DL_PopActiveObject()
		fnCutsceneBlocker()
	
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] I didn't want you to see it, it was supposed to be a surprise.[SOFTBLOCK] I'm getting a second one made but it's not ready yet.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] I was going to give it to you and then we could have twin corgis![SOFTBLOCK] They'd be so cute!") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--[North Rilmani Speaks]
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniC")
			ActorEvent_SetProperty("Face", 0, 1)
		DL_PopActiveObject()
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] Really?[SOFTBLOCK] You mean it?[SOFTBLOCK] Ooohhh![SOFTBLOCK] You're the best![SOFTBLOCK] We can get them matching collars and take them for little statue walks!") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--[Movement]
		--Move Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (6.25 * gciSizePerTile), (20.50 * gciSizePerTile), 0.50)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei speaks.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] Excuse me...") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--[North Rilmani Speaks]
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniC")
			ActorEvent_SetProperty("Face", 1, 0)
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniD")
			ActorEvent_SetProperty("Face", 1, 0)
		DL_PopActiveObject()
		
		--Wait.
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] V-[SOFTBLOCK]voidwalker![SOFTBLOCK] W-[SOFTBLOCK]we -[SOFTBLOCK] we were - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] We are honored to meet you![SOFTBLOCK] Welcome to our home![SOFTBLOCK] P-please stay as long as you like...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani:[VOICE|Septima] *Oh I think she overheard us![SOFTBLOCK] We must look so stupid!*") ]])
		fnCutsceneBlocker()
	
		--They turn away.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniC")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "RilmaniD")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneBlocker()
	
	end
end