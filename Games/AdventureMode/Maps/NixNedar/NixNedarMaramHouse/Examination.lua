--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Objects]
--Clothes.
if(sObjectName == "Clothes") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Maram's outfits.[SOFTBLOCK] Seems we're the same size!)") ]])
	
--Bookshelf.
elseif(sObjectName == "Bookshelf") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('A Brief History of Earth'.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('... Guess who's here?[SOFTBLOCK] Khmer![SOFTBLOCK] Where?[SOFTBLOCK] Here![SOFTBLOCK] And Pagan is there.[SOFTBLOCK] Vietnam just unconquered itself,[SOFTBLOCK] Korea just became itself,[SOFTBLOCK] and Japan is so addicted to art that the military might have to take over the government.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('China just invented bombs, and typing![SOFTBLOCK] And the mongols just invaded most of the universe!')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](It goes on like this...)") ]])
	
--[Exits]
--Back to the main town.
elseif(sObjectName == "Exit") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("NixNedarMain", "FORCEPOS:13x16x0")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end