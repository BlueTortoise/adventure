--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Objects]
--Clothes.
if(sObjectName == "Clothes") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Septima's clothes.[SOFTBLOCK] She dresses sharply, I'll give her that.)") ]])
	
--Cabinet.
elseif(sObjectName == "Cabinet") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Glassware.[SOFTBLOCK] There's also chocolate bars and cakes in here, but they're untouched.[SOFTBLOCK] Maybe they're for me?)") ]])
	
--Bookshelf.
elseif(sObjectName == "Bookshelf") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](It's a bunch of books from Earth![SOFTBLOCK] Looks like Septima transcribed them and put them here, because there are notes written in Rilmani in the margins.)") ]])
	
--[Exits]
--Back to Nix Nedar.
elseif(sObjectName == "Exit") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("NixNedarMain", "FORCEPOS:20x7")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end