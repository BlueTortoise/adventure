--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "Nowhere"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "Null")
	
	--Chest path.
	--DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)
	
	--Create the default character.
	TA_Create("Septima")
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Position", 10, 4)
		TA_SetProperty("Facing", gci_Face_South)
		fnSetCharacterGraphics("Root/Images/Sprites/Septima/", false)
	DL_PopActiveObject()
	
	--Order the player to take control of the party leader.
	AL_SetProperty("Player Actor ID", giPartyLeaderID)
	
	--Map Setup
	fnResolveMapLocation("Nowhere")
	
	--[Spawn NPCS Here]
	--Mei, represents Chapter 1.
    local iChapter1 = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N")
    if(iChapter1 < 1.0) then
        TA_Create("Mei")
            TA_SetProperty("Position", 10, 6)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/Mei_Human/", true)
            TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter1.lua")
            
            --Special frames.
            TA_SetProperty("Wipe Special Frames")
            TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Mei|Wounded")
            TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Mei|Crouch")
        DL_PopActiveObject()
    end
	
	--Sanya, represents Chapter 2.
	TA_Create("Sanya")
		TA_SetProperty("Position", 4, 8)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Sanya_Human/", true)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter2.lua")
	DL_PopActiveObject()
	
	--Jeanne, represents Chapter 3.
	TA_Create("Jeanne")
		TA_SetProperty("Position", 6, 10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Jeanne_Human/", true)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter3.lua")
	DL_PopActiveObject()
	
	--Lotta, represents Chapter 4.
	TA_Create("Lotta")
		TA_SetProperty("Position", 14, 10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Lotta_Human/", true)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter4.lua")
	DL_PopActiveObject()
	
	--Christine, represents Chapter 5.
    local iChapter5 = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter5", "N")
    if(iChapter5 < 1.0) then
        TA_Create("Christine")
            TA_SetProperty("Position", 16, 8)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/Christine_Human/", true)
            TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter5.lua")
        DL_PopActiveObject()
    end
	
	--Maram. Helps set some debug stuff.
	TA_Create("Maram")
		TA_SetProperty("Position", 2, 6)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Rilmani/", false)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Debug.lua")
	DL_PopActiveObject()
    
    --Chapter 5 Credits.
	TA_Create("Credits5")
		TA_SetProperty("Position", 16, 10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Rilmani/", false)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Credits5.lua")
	DL_PopActiveObject()

	--Variables.
	local iHasSeenControlsDialogue = VM_GetVar("Root/Variables/Nowhere/Scenes/iHasSeenControlsDialogue", "N")
	if(iHasSeenControlsDialogue == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Nowhere/Scenes/iHasSeenControlsDialogue", "N", 1.0)
	
	--[Controls]
	--Show the dialogue and set it to scenes mode. This scene takes up the whole screen.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI_Blackout_World, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/System/Controls") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Ignore Scene Offsets", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Press Examine (Z) to continue...") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.

    --[Clear Field Abilities]
    AdvCombat_SetProperty("Set Field Ability", 0, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 1, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 2, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 3, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 4, "NULL")

end
