--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Bag with a hacksaw in it.
if(sObjectName == "HacksawBag") then
	
	--If Mei hasn't taken the hacksaw yet:
	local iGotHacksaw = VM_GetVar("Root/Variables/Chapter1/Scenes/iGotHacksaw", "N")
	if(iGotHacksaw == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iGotHacksaw", "N", 1.0)
		LM_ExecuteScript(gsItemListing, "Hacksaw")
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (...[SOFTBLOCK] This pile contains a hacksaw, metal files, all the things you'd need to break out of prison...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (And it was put here in full view of anyone behind these fused doors...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Guess I'll take the hacksaw...)") ]])
		fnCutsceneBlocker()
		
		--Teleport away the NPC that shows the hacksaw.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "HacksawNPC")
			ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Got a hacksaw*") ]])
		fnCutsceneBlocker()
		fnCutsceneBlocker()
	
	--Mei has taken the hacksaw:
	else
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Leaving this stuff where a prisoner could see it...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (It must have been intentional, right?[SOFTBLOCK] But who would do that?)") ]])
		fnCutsceneBlocker()
	
	end
	
--This door is impassable. But a Slime could just ooze through it.
elseif(sObjectName == "FusedDoorS") then
	
	--Variables.
	local sMeiForm                  = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiKnowsRilmani          = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
	local iHasSlimeForm             = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N")
	local iHasGhostForm             = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
	local bIsFlorentinaPresent      = fnIsCharacterPresent("Florentina")
	local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
	
	--Get Mei's position.
	EM_PushEntity("Mei")
		local fMeiX, fMeiY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--South side of the cell door:
	if(fMeiY >= 29.0 * gciSizePerTile) then
	
		--If Florentina is not present:
		if(bIsFlorentinaPresent == false) then
			
			--If Mei is a slime:
			if(sMeiForm == "Slime") then
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] (... as if that matters![SOFTBLOCK] I'll just squeeze through it!)") ]])
				fnCutsceneBlocker()
				
				--Position Mei in front of the door.
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				fnCutsceneBlocker()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Face",  0, -1)
				DL_PopActiveObject()
				fnCutsceneBlocker()
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Mei squeezes through.
				for i = 1, 20, 1 do
					Cutscene_CreateEvent("ActorEvent", "Actor")
						ActorEvent_SetProperty("Subject Name", "Mei")
						ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
					DL_PopActiveObject()
					fnCutsceneWait(7)
					fnCutsceneBlocker()
				end
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Dialogue.
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOFTBLOCK]*huff* [SOFTBLOCK]*huff*[SOFTBLOCK] Phew![SOFTBLOCK][EMOTION|Mei|Happy] That actually felt kinda good!") ]])
				fnCutsceneBlocker()
				
			--If Mei is a ghost:
			elseif(sMeiForm == "Ghost") then
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] (... as if that matters![SOFTBLOCK] I'll just walk through it!)") ]])
				fnCutsceneBlocker()
				
				--Position Mei in front of the door.
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				fnCutsceneBlocker()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Face",  0, -1)
				DL_PopActiveObject()
				fnCutsceneBlocker()
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Mei squeezes through.
				for i = 1, 20, 1 do
					Cutscene_CreateEvent("ActorEvent", "Actor")
						ActorEvent_SetProperty("Subject Name", "Mei")
						ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
					DL_PopActiveObject()
					fnCutsceneWait(7)
					fnCutsceneBlocker()
				end
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Dialogue.
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (...[SOFTBLOCK] Phasing through stuff feels really weird.[SOFTBLOCK] I don't think ghosts are supposed to walk through walls...)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] (So I guess all those ghost videos were full of it!)") ]])
				fnCutsceneBlocker()
			
			--If Mei is not a slime, but has access to slime form:
			elseif(sMeiForm ~= "Slime" and iHasSlimeForm == 1.0) then
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (This sucks, there's no way through...)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] (Which is what I'd be thinking if I couldn't become a slime at will!)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I'll just come back once I've found a safe spot to transform...)") ]])
				fnCutsceneBlocker()
			
			--If Mei is not a slime, but has access to slime form:
			elseif(sMeiForm ~= "Ghost" and iHasGhostForm == 1.0) then
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (This sucks, there's no way through...)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] (Which is what I'd be thinking if I couldn't become a ghost at will!)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I'll just come back once I've found a safe spot to transform...)") ]])
				fnCutsceneBlocker()
			
			--Mei is not a slime/ghost and does not have slime/ghost form:
			else
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.)[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (There doesn't seem to be any other way through.[SOFTBLOCK] Guess I'll come back once I figure something out...)") ]])
				fnCutsceneBlocker()
			
			end

		--Florentina is present:
		else

			--If Mei is a slime:
			if(sMeiForm == "Slime") then
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] ... as if that matters![SOFTBLOCK] I'll just squeeze through it![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So I'll just wait here then?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll try to find a way around for you.[SOFTBLOCK] I'll come right back, promise.") ]])
				fnCutsceneBlocker()
				
				--Position Mei in front of the door.
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Florentina")
					ActorEvent_SetProperty("Move To", (68.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				fnCutsceneBlocker()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Florentina")
					ActorEvent_SetProperty("Face",  1, 0)
				DL_PopActiveObject()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Face",  0, -1)
				DL_PopActiveObject()
				fnCutsceneBlocker()
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Mei squeezes through.
				for i = 1, 20, 1 do
					Cutscene_CreateEvent("ActorEvent", "Actor")
						ActorEvent_SetProperty("Subject Name", "Mei")
						ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
					DL_PopActiveObject()
					fnCutsceneWait(7)
					fnCutsceneBlocker()
				end
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Dialogue.
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOFTBLOCK]*huff* [SOFTBLOCK]*huff*[SOFTBLOCK] Phew![SOFTBLOCK][EMOTION|Mei|Happy] That actually felt kinda good![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Take your time, slimey.") ]])
				fnCutsceneBlocker()
				
				--Remove Florentina from the follow list.
				AL_SetProperty("Unfollow Actor Name", "Florentina")

			--If Mei is a ghost:
			elseif(sMeiForm == "Ghost") then
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] ... as if that matters![SOFTBLOCK] I'll just walk through it![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So I'll just wait here then?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll try to find a way around for you.[SOFTBLOCK] I'll come right back, promise.") ]])
				fnCutsceneBlocker()
				
				--Position Mei in front of the door.
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Florentina")
					ActorEvent_SetProperty("Move To", (68.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				fnCutsceneBlocker()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Florentina")
					ActorEvent_SetProperty("Face",  1, 0)
				DL_PopActiveObject()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Face",  0, -1)
				DL_PopActiveObject()
				fnCutsceneBlocker()
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Mei squeezes through.
				for i = 1, 20, 1 do
					Cutscene_CreateEvent("ActorEvent", "Actor")
						ActorEvent_SetProperty("Subject Name", "Mei")
						ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
					DL_PopActiveObject()
					fnCutsceneWait(7)
					fnCutsceneBlocker()
				end
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Dialogue.
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] That felt really weird.[SOFTBLOCK] Youku lied, ghosts aren't supposed to walk through walls![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Take your time, spooks.") ]])
				fnCutsceneBlocker()
				
				--Remove Florentina from the follow list.
				AL_SetProperty("Unfollow Actor Name", "Florentina")
			
			--If Mei is not a slime, but has access to slime form:
			elseif(sMeiForm ~= "Slime" and iHasSlimeForm == 1.0) then
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] No biggy, I'll just turn into a slime and squeeze through![BLOCK][CLEAR]") ]])
				
				if(iFlorentinaKnowsAboutRune == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Care to repeat that bit of blithering idiocy?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh, have I not shown you yet?[SOFTBLOCK] Okay![SOFTBLOCK] But, not here.[SOFTBLOCK] We need to find a safe spot.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] This is gonna be good, isn't it?") ]])
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So what's stopping you?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I can only do it in certain places.[SOFTBLOCK] I don't know why, I can just feel when it's right.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We'll need to go to a campsite.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Lead the way, then.") ]])
				end
				fnCutsceneBlocker()
			
			--If Mei is not a ghost, but has access to slime form:
			elseif(sMeiForm ~= "Ghost" and iHasGhostForm == 1.0) then
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] No biggy, I'll just turn into a ghost and walk through![BLOCK][CLEAR]") ]])
				
				if(iFlorentinaKnowsAboutRune == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Care to repeat that bit of blithering idiocy?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh, have I not shown you yet?[SOFTBLOCK] Okay![SOFTBLOCK] But, not here.[SOFTBLOCK] We need to find a safe spot.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] This is gonna be good, isn't it?") ]])
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So what's stopping you?[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I can only do it in certain places.[SOFTBLOCK] I don't know why, I can just feel when it's right.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We'll need to go to a campsite.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Lead the way, then.") ]])
				end
				fnCutsceneBlocker()
			
			--Mei is not a slime/ghost and does not have slime/ghost form:
			else
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm, seems this door was welded shut by something.[SOFTBLOCK] It won't open.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hm, I can't think of a way to get through, and I don't see a way around.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Guess we'll come back once we figure something out...") ]])
				fnCutsceneBlocker()
			
			end
		end
	
	--North side. Since it's impossible to get here if Mei is not a slime, we don't do that check. This means debug/glitch cases are handled!
	else
	
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Here we go...)") ]])
		fnCutsceneBlocker()
		
		--Position Mei in front of the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, 1)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei squeezes through.
		for i = 1, 20, 1 do
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (28.50 * gciSizePerTile) + (i * 0.10 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneWait(7)
			fnCutsceneBlocker()
		end
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Variables.
		local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		
		--If Florentina is not present:
		if(bIsFlorentinaPresent == false) then
			
			fnStandardMajorDialogue()
			
			--Slime:
			if(sMeiForm == "Slime") then
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOFTBLOCK]*huff*[SOFTBLOCK] ooh, that feels incredible...[SOFTBLOCK] I love feeling so full...[SOFTBLOCK] so squishy...") ]])
			
			--Ghost:
			else
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This feels so weird...") ]])
			
			end
			fnCutsceneBlocker()
		
		--If Florentina is present:
		else
			--Variables.
			local iMetClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
			local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
			local iFlorentinaKnowsMeiSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMeiSavedClaudia", "N")
			
			--If Mei came back after finding Claudia and giving her the hacksaw:
			if(iFlorentinaKnowsMeiSavedClaudia == 0.0 and iMetClaudia == 1.0 and iSavedClaudia == 1.0) then
				VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMeiSavedClaudia", "N", 1.0)
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I heard something going on in there.[SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I found Claudia![SOFTBLOCK] She's stuck in there![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I gave her a hacksaw I found.[SOFTBLOCK] She said she's going to saw the bars off after she performed some rites, or something.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] And yet you come out here without any cash to speak of...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh...[SOFTBLOCK] Yeah, I showed her my runestone, but she's not exactly in a position to pay us.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, understandable.[SOFTBLOCK] Did she know what it means?[BLOCK][CLEAR]") ]])
				
				if(iMeiKnowsRilmani == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, but she did say she had written about the symbol.[SOFTBLOCK] She left her research notes in the northeast wing of the Quantir Estate![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Feh, guess we're going up there.[SOFTBLOCK] Shame about the payoff...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'm sure we'll find some loot for you to tote around.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Aww, you know how to lead a girl on don't you?") ]])
				
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nothing we didn't already find in her research, unfortunately.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Guess it's back to the Dimensional Trap, then.[SOFTBLOCK] Shame about the payoff...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'm sure we'll find some loot for you to tote around.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Aww, you know how to lead a girl on don't you?") ]])
				end
				fnCutsceneBlocker()
			
			--If Mei didn't save Claudia.
			elseif(iFlorentinaKnowsMeiSavedClaudia == 0.0 and iMetClaudia == 1.0 and iSavedClaudia == 0.0) then
				VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMeiSavedClaudia", "N", 1.0)
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I heard something going on in there.[SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I found Claudia![SOFTBLOCK] She's stuck in there![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I -[SOFTBLOCK] couldn't get her out, unfortunately.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Guess that means the cash reward is out.[SOFTBLOCK] Maybe I'll hire some workers to come get her.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I showed her my runestone![BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Did she know what it means?[BLOCK][CLEAR]") ]])
				
				if(iMeiKnowsRilmani == 0.0) then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, but she did say she had written about the symbol.[SOFTBLOCK] She left her research notes in the northeast wing of the Quantir Estate![BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Feh, guess we're going up there.[SOFTBLOCK] Shame about the payoff...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'm sure we'll find some loot for you to tote around.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Aww, you know how to lead a girl on don't you?") ]])
				
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nothing we didn't already find in her research, unfortunately.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Guess it's back to the Dimensional Trap, then.[SOFTBLOCK] Shame about the payoff...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'm sure we'll find some loot for you to tote around.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Aww, you know how to lead a girl on don't you?") ]])
				end
				fnCutsceneBlocker()
			
			--Otherwise, Florentina joins up without discussing Claudia.
			else
			
				fnStandardMajorDialogue()
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
				
				--Slime version.
				if(sMeiForm == "Slime") then
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOFTBLOCK]*huff*[SOFTBLOCK] ooh, that feels so incredible...[SOFTBLOCK] I love feeling so full...[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] I am so glad I had to sit here and watch you do that![SOFTBLOCK] Let's get going before I throw up!") ]])
					fnCutsceneBlocker()
				
				--Ghost version:
				else
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It feels so weird, walking through stuff.[SOFTBLOCK] I don't think I like it.[BLOCK][CLEAR]") ]])
					fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Well then don't do that.[SOFTBLOCK] Duh.") ]])
					fnCutsceneBlocker()
				end
			end
				
			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Rejoin Florentina to follow Mei.
			EM_PushEntity("Florentina")
				local iFlorentinaID = RE_GetID()
			DL_PopActiveObject()

			--Store it and tell her to follow.
			giaFollowerIDs = {iFlorentinaID}
			AL_SetProperty("Follow Actor ID", iFlorentinaID)
			
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
			fnCutsceneBlocker()
		end
	end
	
--Ooze again!
elseif(sObjectName == "FusedDoorN") then
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Get Mei's position.
	EM_PushEntity("Mei")
		local fMeiX, fMeiY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--South position, ooze north.
	if(fMeiY >= 23.5 * gciSizePerTile) then
	
		--Dialogue.
		fnStandardMajorDialogue()
		if(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Okay.[SOFTBLOCK] Try not to cum this time...)") ]])
			fnCutsceneBlocker()
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Okay.[SOFTBLOCK] Here goes nothing...)") ]])
			fnCutsceneBlocker()
		end
		
		--Position Mei in front of the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (24.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei squeezes through.
		for i = 1, 20, 1 do
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (24.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneWait(7)
			fnCutsceneBlocker()
		end
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--Dialogue.
		fnStandardMajorDialogue()
		if(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Ungh... I...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei] *squish*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Still got a job to do...)") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] (Maybe I'm getting used to being a ghost...)") ]])
		end
		fnCutsceneBlocker()
	
	--North position, ooze north.
	else
	
		--Dialogue.
		fnStandardMajorDialogue()
		if(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Unnnghhhh...)") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] (Here we go...)[BLOCK][CLEAR]") ]])
		end
		fnCutsceneBlocker()
		
		--Position Mei in front of the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (22.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, 1)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei squeezes through.
		for i = 1, 20, 1 do
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (22.50 * gciSizePerTile) + (i * 0.10 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneWait(7)
			fnCutsceneBlocker()
		end
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--Dialogue.
		if(sMeiForm == "Slime") then
			fnStandardMajorDialogue()
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Sooooo sliiiimy....)") ]])
			fnCutsceneBlocker()
		end
	
	end
	
--Gaardian Cave Moss.
elseif(sObjectName == "GaardianMoss") then
	
	--Pie Job:
	local iMossCount = AdInv_GetProperty("Item Count", "Gaardian Cave Moss")
	local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
	if(iTakenPieJob == 1.0 and iMossCount < 1) then
		
		--Short scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Wait up a second, Mei.[SOFTBLOCK] This guy is the kind of moss we need.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Really?[SOFTBLOCK] That's going into the pie?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] It is...[SOFTBLOCK] less than appetizing...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You're going to eat it and you're going to like it.[SOFTBLOCK] Now shut up and grab a handful.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Got Gaardian Cave Moss*") ]])
		
		--Add the item.
		LM_ExecuteScript(gsItemListing, "Gaardian Cave Moss")
		
		--Reset flag.
		WD_SetProperty("Clear Topic Read", "Pepper Pie")
		
	--Normal:
	else
		fnStandardDialogue("[VOICE|Mei](Moss growing on the edge of the room.[SOFTBLOCK] Water is leaking through in places...)")
	end
	
--[Exits]
--To the Quantir Mansion Basement.
elseif(sObjectName == "NWExit") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseBasementW", "FORCEPOS:58.5x20")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end