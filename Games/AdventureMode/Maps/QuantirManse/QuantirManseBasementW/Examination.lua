--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Crates.
if(sObjectName == "Crates") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (These crates are full of bones...) ]])
	
--Coffins.
elseif(sObjectName == "Coffins") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Row upon row of coffins.[SOFTBLOCK] Some are partially open, and they're full to bursting with bones...) ]])
	
--Bed.
elseif(sObjectName == "Bed") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (A well-kept bed in this sewer...) ]])
	
--Left bookshelf.
elseif(sObjectName == "BookshelfA") then
	LM_ExecuteScript(gsFlorentinaSkillbook, 3)
	
--Right bookshelf.
elseif(sObjectName == "BookshelfB") then
	LM_ExecuteScript(gsFlorentinaSkillbook, 3)

--Secret room locked door.
elseif(sObjectName == "DoorW") then
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_SetProperty("Open Door", "DoorW")
	else
		fnStandardDialogue([[ [VOICE|Mei](Locked tight.) ]])	
	end

--Secret room locked door.
elseif(sObjectName == "DoorE") then
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_SetProperty("Open Door", "DoorE")
	else
		fnStandardDialogue([[ [VOICE|Mei](Locked tight.) ]])	
	end

--Locked door, western side.
elseif(sObjectName == "LockedDoor") then

	--Variables.
	local iSewerKeycount = AdInv_GetProperty("Item Count", "Quantir Sewer Key")
	local iOpenedSewerDoor = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedSewerDoor", "N")
	
	--Open the door.
	if(iSewerKeycount > 0) then
		AL_SetProperty("Open Door", "LockedDoor")
		AudioManager_PlaySound("World|FlipSwitch")
		
		--Show dialogue on the first pass.
		if(iOpenedSewerDoor == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedSewerDoor", "N", 1.0)
			fnStandardDialogue([[ [VOICE|Mei](The sewer key fit the lock.) ]])
		end
	
	--Locked!
	else
		local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
		if(iMansionKeyCount > 0) then
			fnStandardDialogue([[ [VOICE|Mei](Locked.[SOFTBLOCK] The mansion key doesn't fit.) ]])	
		else
			
			--Mei is a slime, but she can't squeeze through here.
			local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
			if(sMeiForm == "Slime") then
				fnStandardDialogue([[ [VOICE|Mei](Locked.[SOFTBLOCK] The bars are freezing cold![SOFTBLOCK] I'd get frozen solid if I squeezed through!) ]])
			else
				fnStandardDialogue([[ [VOICE|Mei](Locked.) ]])
			end
		end
	end
	
--Journal with its pages torn out.
elseif(sObjectName == "TornJournal") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[SOFTBLOCK] This must have been the journal that the other pages were from.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](The last entry is still in here...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('There is no hope.[SOFTBLOCK] All turns to ash.[SOFTBLOCK] The body is merely a contagion that is to be snuffed out.[SOFTBLOCK] Impurities are the norm, not the exception, and the degenerates indulge these sins.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](That seems to be it.[SOFTBLOCK] It's the same handwriting, but not the same style at all.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Oh, there's something written on the back cover...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('Rot forever, you hateful, wicked monster.[SOFTBLOCK] I'll dedicate every fibre of my being to guarantee your suffering.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](...[SOFTBLOCK] There's nothing else here.)") ]])
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiReadLastJournal", "N", 1.0)

--[Exits]
--To the Quantir Mansion Basement East.
elseif(sObjectName == "SEExit") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseBasementE", "FORCEPOS:10.5x7")

--Secret exit!
elseif(sObjectName == "ExitSecret") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseSecretExit", "Null")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end