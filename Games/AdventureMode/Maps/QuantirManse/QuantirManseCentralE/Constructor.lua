--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "QuantirManseCentralE"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "QuantirManseThemeLow")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("QuantirManseCentralE")
	
	--If Mei has access to ghost form, and this isn't the ghost TF, spawn Lydie.
	local iIsGhostTF      = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
	local iHasGhostForm   = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
	local iLydieLeftParty = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
	if((iIsGhostTF == 0.0 and iHasGhostForm == 1.0) or (iIsGhostTF == 1.0 and iLydieLeftParty == 1.0)) then
		TA_Create("Lydie")
			TA_SetProperty("Position", 10, 6)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Lydie/Root.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/MaidGhost/", false)
		DL_PopActiveObject()
	end

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

end
