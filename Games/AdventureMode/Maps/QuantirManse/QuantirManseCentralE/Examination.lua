--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Water.
if(sObjectName == "Water") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Looks like this is where the workers do the laundry.) ]])
	
--Crates.
elseif(sObjectName == "Crates") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Detergents.[SOFTBLOCK] Likely used for the laundry.) ]])
	
--Big Crate.
elseif(sObjectName == "BigCrate") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (There's a lot of clothing in this crate.[SOFTBLOCK] Most of it has decayed, and I certainly wouldn't wear it.) ]])

--[Exits]
elseif(sObjectName == "ExitDoor") then
	AudioManager_PlaySound("World|FlipSwitch")
	
	--If currently reliving the ghost TF:
	local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
	if(iIsRelivingScene == 1.0) then
		AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 0.0)
	
	--Normal case:
	else
		AL_BeginTransitionTo("QuantirManseEntrance", "FORCEPOS:14x15")
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end