--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Cutscene Triggers]
--Lydie leaves the party.
if(sObjectName == "GhostTrigger") then
	
	--Variables.
	local iIsGhostTF      = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
	local iLydieLeftParty = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
	local iHasFlorentina  = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	
	--Trigger scene:
	if(iLydieLeftParty == 0.0 and iIsGhostTF == 1.0) then
		
		--Move Natalie:
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (9.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (7.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (7.50 * gciSizePerTile))
		DL_PopActiveObject()
		
		--Move Lydie:
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Lydie")
			ActorEvent_SetProperty("Move To", (9.25 * gciSizePerTile), (9.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Lydie")
			ActorEvent_SetProperty("Move To", (9.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Lydie")
			ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
		
		--Dialogue:
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Ugh, what a day![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Sad] I'm sorry...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Oh, it's not you.[SOFTBLOCK] More sick came in today.[SOFTBLOCK] This one had a sword and was swinging it all over the place![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Surprise] A sword?[SOFTBLOCK] Yikes![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Yeah, the plague is making people desperate.[SOFTBLOCK] I don't know what we're gonna do...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Smirk] We'll be fine.[SOFTBLOCK] The Countess will think of something.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: I don't want to get sick...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Blush] Keep clean, bathe regularly.[SOFTBLOCK] We'll make it through this together, Lydie.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Yeah.[SOFTBLOCK] Thanks for cheering me up.[SOFTBLOCK] I know we'll be okay.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Offended] Hey, the sword wasn't curved, was it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Hmm, maybe.[SOFTBLOCK] The girl was oddly dressed, too.[SOFTBLOCK] Maybe she wasn't from Quantir?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Offended] It's just like my dream...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] I'm being silly.[SOFTBLOCK] Just a dream.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Get washing, you goof![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Smirk] Yeah, sure...") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie picked up the odd clothes she had been wearing.[SOFTBLOCK] She checked over them again.[SOFTBLOCK] Gold and grey, with a big blue symbol on the front. They were unlike anything she'd ever worn.[SOFTBLOCK] Yet, she had been wearing them.[SOFTBLOCK] Why?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "She absentmindedly washed the clothes while pondering her life.[SOFTBLOCK] She couldn't really recall what she had been doing before she got dressed that day.[SOFTBLOCK] In fact, she could only vaguely recall a lot of things.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie didn't seem any different than usual.[SOFTBLOCK] She was her cheerful, prissy self.[SOFTBLOCK] Maybe, Natalie worried, she'd gotten sick?[SOFTBLOCK] Had any of the other patients shown memory loss?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "As she scrubbed, she realized there was something concealed in a hidden pocket inside the dress.[SOFTBLOCK] Without really thinking about it, her hand touched the object...") ]])
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])

		--Animation.
		local sAllocationString = "WD_SetProperty(\"Allocate Animation\", gciMeiRuneFramesTotal, 1, -90)"
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutsceneInstruction(sAllocationString)
		for i = 0, gciMeiRuneFramesTotal-1, 1 do
			local iNumber = string.format("%02i", i)
			local sString = "WD_SetProperty(\"Set Animation Frame\", " .. i .. ", \"Root/Images/Scenes/Mei/RuneAnim" .. iNumber .. "\")"
			fnCutsceneInstruction(sString)
		end
		
		fnCutsceneInstruction([[ WD_SetProperty("Activate Flash", 0) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "All at once, Mei remembered who she was.[SOFTBLOCK] The runestone sent a shock through her ghostly form, awakening her true self,[SOFTBLOCK] formerly lost within Natalie.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "It took all of Mei's concentration not to react in pure horror at what she had become.[SOFTBLOCK] Lydie, her best friend, hadn't noticed...[SOFTBLOCK] No...[SOFTBLOCK] She was Mei...[SOFTBLOCK] not Natalie...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie let despair wash over her as the realization became inescapable.[SOFTBLOCK] She was dead.[SOFTBLOCK] She had died a long time ago, cold, scared, and alone.[SOFTBLOCK] This poor girl, Mei, had been forced to share her memories with her, to donate her body involuntarily.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie remained oblivious to the revelation, still content in the delusion of undeath.[SOFTBLOCK] In a perverted way, the curse gave a second chance to the dead.[SOFTBLOCK] But it was wrong.[SOFTBLOCK] It was a lie, and it needed to end.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei began to sort herself from Natalie.[SOFTBLOCK] The two had become so inseperable, so interwoven...[SOFTBLOCK] but, the runestone had split them again.[SOFTBLOCK] Memories that weren't hers...[SOFTBLOCK] No.[SOFTBLOCK] They were hers now.[SOFTBLOCK] She would cherish them always.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie deserved to be remembered, to have the second chance that undeath had teased to her.[SOFTBLOCK] Mei would give her that chance, but not because of a curse.[SOFTBLOCK] From her own free will.[SOFTBLOCK] Natalie and Mei's spirits embraced and became one...") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Get the feather duster.
		local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
		if(iIsRelivingScene == 0.0) then
			LM_ExecuteScript(gsItemListing, "Translucent Feather Duster")
		end
		
		--Dialogue:
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Lydie?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Hmm, Natalie?[SOFTBLOCK] You sound awful![SOFTBLOCK] Are you sick?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] No, I'm fine.[SOFTBLOCK] Really.[SOFTBLOCK] Lydie...[SOFTBLOCK] I -[SOFTBLOCK] I love you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] You're...[SOFTBLOCK] like my sister.[SOFTBLOCK] You're the closest thing I have to family.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Where's this coming from?[SOFTBLOCK] Are you sure you're okay?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (I don't think she realizes what's going on...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (What terrible curse would do this to someone?)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I need to go.[SOFTBLOCK] I have some things I need to do.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Oh, okay![SOFTBLOCK] I have to finish washing up.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Don't let any of the others catch you or they'll think you're slacking.[SOFTBLOCK] Apparently, the Countess authorized corporal punishment for slackers.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I'll -[SOFTBLOCK] I'll set you free if I can...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Huh?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nothing...[SOFTBLOCK] see you later...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Hey, wait![SOFTBLOCK] You forgot your feather duster![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: [SOUND|World|TakeItem](Received Translucent Feather Duster)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Sheesh, you really are a scatterbrain![SOFTBLOCK] What would you do if someone saw you without this?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Uh,[SOFTBLOCK] panic?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Yeah, that's your answer for everything.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: ...[SOFTBLOCK] Sorry.[SOFTBLOCK] I think never having any time off is getting to me.[SOFTBLOCK] I didn't mean to snap.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's okay.[SOFTBLOCK] See you later.") ]])
		fnCutsceneBlocker()
	
		--Mei remembers her real name.
		AdvCombat_SetProperty("Push Party Member", "Mei")
			AdvCombatEntity_SetProperty("Display Name", "DEFAULT")
		DL_PopActiveObject()
		
		--Flags
		VM_SetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N", 1.0)
		
		--Remove Lydie as a follower.
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "Lydie")
		
		--Re-add Lydie's collision flag and set her dialogue.
		fnCutsceneInstruction([[ 
		EM_PushEntity("Lydie")
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Lydie/Root.lua")
		DL_PopActiveObject()
		]])
		
		--If Florentina was not in the party, unset the ghost TF flag:
		if(iHasFlorentina == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 0.0)
		end
		
	end
end