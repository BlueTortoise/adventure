--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Variables.
local iIsGhostTF        = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
local iPutMaidOutfitOn  = VM_GetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N")
local iCleaningProgress = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N")

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Western Bookshelves.
if(sObjectName == "BookshelvesWA") then
	
	--Variables
	local iCleanedBookshelvesW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
		LM_ExecuteScript(gsMeiSkillbook, 1)
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesW == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (So much dust on these shelves!) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (All right, that's done!) ]])
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (These shelves are already dusted.) ]])
	end
--Western Bookshelves.
elseif(sObjectName == "BookshelvesWB") then
	
	--Variables
	local iCleanedBookshelvesW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
		LM_ExecuteScript(gsMeiSkillbook, 2)
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesW == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (So much dust on these shelves!) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (All right, that's done!) ]])
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (These shelves are already dusted.) ]])
	end

--Eastern Bookshelves.
elseif(sObjectName == "BookshelvesEA") then
	
	--Variables
	local iCleanedBookshelvesE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
		LM_ExecuteScript(gsFlorentinaSkillbook, 2)
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (So much dust on these shelves and books!) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (All right, that's done!) ]])
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (These shelves are already dusted.) ]])
	end

--Eastern Bookshelves.
elseif(sObjectName == "BookshelvesEB") then
	
	--Variables
	local iCleanedBookshelvesE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
		LM_ExecuteScript(gsFlorentinaSkillbook, 3)
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (So much dust on these shelves and books!) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (All right, that's done!) ]])
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (These shelves are already dusted.) ]])
	end

--Eastern Bookshelves.
elseif(sObjectName == "BookshelvesEC") then
	
	--Variables
	local iCleanedBookshelvesE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] ('Modern Diagnosis and Treatment of Humourous Deficiencies'.[SOFTBLOCK] Not a treatise on mirth-through-suffering, but a discussion of precious bodily fluids.)]])
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (So much dust on these shelves and books!) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (All right, that's done!) ]])
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (These shelves are already dusted.) ]])
	end

--Eastern Bookshelves.
elseif(sObjectName == "BookshelvesED") then
	
	--Variables
	local iCleanedBookshelvesE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] ('Drain The Lizard'.[SOFTBLOCK] This seems to be a guide on using iguana blood to cure sick people...)]])
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (So much dust on these shelves and books!) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (All right, that's done!) ]])
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (These shelves are already dusted.) ]])
	end

--Crates.
elseif(sObjectName == "Crates") then
	
	--In Ghost TF but haven't put on the uniform yet:
	if(iIsGhostTF == 1.0 and iPutMaidOutfitOn == 0.0) then

		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N", 1.0)

		--Fade to black quickly.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneBlocker()

		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie sorted through the contents of the crate, looking for the spare uniform.[SOFTBLOCK] In her panic, she didn't notice that it had materialized from nothing underneath an old bedsheet as she lifted it.") ]])
		fnCutsceneBlocker()
		
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "She picked it up with haste and checked it over.[SOFTBLOCK] There were no stains, nor wear.[SOFTBLOCK] It was immaculate, like it had never been used.[SOFTBLOCK] She undressed and quickly began putting the uniform on.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "It fit poorly, but Natalie didn't have time to complain.[SOFTBLOCK] If the Countess caught her neglecting her duty, she'd be disciplined.[SOFTBLOCK] She needed to think of how to clean the room up, not how she looked.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Beneath her notice, her body slowly conformed to the size of the uniform.[SOFTBLOCK] Her hair grew in length as her skin grew translucent...") ]])
		fnCutsceneBlocker()
		
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "She felt a mild discomfort, but shrugged it off.[SOFTBLOCK] The uniform was tighter than she would have liked.[SOFTBLOCK] It felt like it was constricting her neck and legs, but this was her fault -[SOFTBLOCK] she should have worn her uniform this morning![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "She felt a cold wind blow through her.[SOFTBLOCK] She felt odd, like she had lost something important. But, she didn't have time to ponder it.[SOFTBLOCK] The countess could be there at any moment!") ]])
		fnCutsceneBlocker()
		
		fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Ghost") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "As Natalie did one last check over the uniform, looking for anything out of place, her thoughts wandered yet again.[SOFTBLOCK] She thought back to what she had been daydreaming about.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "That very pretty girl, Mei, fighting to find her way home.[SOFTBLOCK] It was so fantastical, but so vivid.[SOFTBLOCK] She decided to tell Lydie all about her dream later...[BLOCK][CLEAR]") ]])
		fnCutsceneBlocker()
		
		--Switch Natalie to ghost form.
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Ghost.lua") ]])
		fnCutsceneBlocker()
	
		--Mei temporarily changes name to Natalie.
		fnCutsceneInstruction([[
		AdvCombat_SetProperty("Push Party Member", "Mei")
			AdvCombatEntity_SetProperty("Display Name", "Natalie")
		DL_PopActiveObject() ]])

		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Fade to black quickly.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[EMOTION|Mei|Neutral][VOICE|Mei] (A little tight, but it'll have to do!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[EMOTION|Mei|Sad][VOICE|Mei] (Why is it so cold in here all of the sudden?)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[EMOTION|Mei|Neutral][VOICE|Mei] (No time, gotta clean or the Countess will be furious!)") ]])

	elseif(iIsGhostTF == 1.0 and iPutMaidOutfitOn == 1.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (I'm already wearing my uniform!) ]])
	end

--Bed.
elseif(sObjectName == "Bed") then
	
	--Variables
	local iMadeBed = VM_GetVar("Root/Variables/Chapter1/Scenes/iMadeBed", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (A very nice bed, fit for two.)]])
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iMadeBed == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMadeBed", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (This bed could be made better...) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (Nice and tidy![SOFTBLOCK] On to the next job!) ]])
	
	--Maid TF, uniform is on, and the job is done:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (The bed has already been made.) ]])
	end

--Table West.
elseif(sObjectName == "TableW") then
	
	--Variables
	local iCleanedTableW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedTableW", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedTableW == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableW", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (There's a lot of dust on this table...) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (That should do it!) ]])
	
	--Maid TF, uniform is on, and the job is done:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (The table has already been dusted.) ]])
	end

--Table Center.
elseif(sObjectName == "TableC") then
	
	--Variables
	local iCleanedTableC = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedTableC", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedTableC == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableC", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (There's a lot of dust on this table...) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (That should do it!) ]])
	
	--Maid TF, uniform is on, and the job is done:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (The table has already been dusted.) ]])
	end

--Table East.
elseif(sObjectName == "TableE") then
	
	--Variables
	local iCleanedTableE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedTableE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedTableE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableE", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (There's a lot of dust on this table...) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (That should do it!) ]])
	
	--Maid TF, uniform is on, and the job is done:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (The table has already been dusted.) ]])
	end

--Chair West.
elseif(sObjectName == "ChairW") then
	
	--Variables
	local iCleanedChairW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedChairW", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedChairW == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairW", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (This chair is so dusty!) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (All cleaned!) ]])
	
	--Maid TF, uniform is on, and the job is done:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (The chair has already been cleaned up.) ]])
	end

--Chair Center.
elseif(sObjectName == "ChairC") then
	
	--Variables
	local iCleanedChairC = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedChairC", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedChairC == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairC", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (This chair is so dusty!) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (All cleaned!) ]])
	
	--Maid TF, uniform is on, and the job is done:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (The chair has already been cleaned up.) ]])
	end

--Chair East.
elseif(sObjectName == "ChairE") then
	
	--Variables
	local iCleanedChairE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedChairE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Better put the uniform on before I start cleaning!)]])
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedChairE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairE", "N", 1.0)
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (This chair is so dusty!) [BLOCK][CLEAR][VOICE|Mei] (...) [BLOCK][CLEAR][VOICE|Mei] (All cleaned!) ]])
	
	--Maid TF, uniform is on, and the job is done:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (The chair has already been cleaned up.) ]])
	end

--[Exits]
elseif(sObjectName == "ExitDoor") then

	--Not ghost TF, or the cleaning job is done:
	local iCanLeaveRoomW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCanLeaveRoomW", "N")
	if(iIsGhostTF == 0.0 or iCanLeaveRoomW == 1.0) then 
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseEntrance", "FORCEPOS:5.0x15.0x0")
	
	--Otherwise, stop Natalie from leaving, she needs to do cleaning:
	elseif(iCleaningProgress < 9.0) then
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (No slacking![SOFTBLOCK] I still need to clean the room up!) ]])

	--Stop Natalie from leaving, she needs to talk to Lydie:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (I should talk to Lydie before I go wandering off...) ]])
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end