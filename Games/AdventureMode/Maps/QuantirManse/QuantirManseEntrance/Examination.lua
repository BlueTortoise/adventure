--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Tent.
if(sObjectName == "Tent") then
	fnStandardDialogue([[ [VOICE|Mei](A single-person tent.[SOFTBLOCK] Looks pretty beaten up...) ]])
	
--Cooking pots.
elseif(sObjectName == "CookingPot") then
	fnStandardDialogue([[ [VOICE|Mei](Cooking utensils.[SOFTBLOCK] Spotless.[SOFTBLOCK] Whoever used them last cleaned them really well.) ]])
	
--Sleeping bags.
elseif(sObjectName == "SleepingBag") then
	fnStandardDialogue([[ [VOICE|Mei](Portable cloth sleeping bags.[SOFTBLOCK] They feel damp.[SOFTBLOCK] Probably haven't been used for a while.) ]])
	
--Journal A.
elseif(sObjectName == "JournalA") then

	--Setup.
	fnStandardMajorDialogue()
		
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei] (A sketchbook, with a drawing of a slime girl on it.[SOFTBLOCK] Read it?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalRead\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()

--Reading the southern journal.
elseif(sTopicName == "JournalReadA") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Heavenly Doves Research Log:: [SOFTBLOCK]Slime Partirhuman, Trannadar Region'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] There are several sketches of various slime girls, some as humanoids and some as puddles on the ground.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The common slime girl, these creatures can be found in many biomes across Pandemonium. They are a carrior feeder which periodically hunts live game.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Slimes are never found in dry regions, as they appear to require frequent moisture refilling. Their locomotion appears to consume water at a great rate.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'In addition, they are not found in freezing regions for presumably a similar reason. Otherwise, they have a great range. Their properties do not change across their habitat.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Slimes usually stay in an amorphous form, hiding from sunlight during the day and emerging at night to consume insects, rodents, or unaware birds. Slimes are not choosy in what they eat, but rarely eat much leafy material.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Their life duration is considered to be indefinite at this point in time. Their body structure is highly resistant to parasites and diseases, and they do not appear to age like humans do.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Slimes tend to be killed by drying out or being unable to find food. Most predators do not regard them as worth the effort, as their bodies provide very little nutrition.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'To reproduce, slimes will behave erratically for several weeks during the summer. Instead of hiding during the day, they will instead take on human form, always female.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'While in human form, their locomotive rate is increased, as is their water consumption. The slime will then attempt to locate humans to infect.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'An infected human will slowly become covered in slime. The infection progresses over a period of about thirty minutes. Once the entirety of the human has been overcome, they are permanently a slime.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'A number of cures exist for this state, but all are worthless once the human succumbs. Folk remedies and modern medicine can revert an infected human from approximately an eighty percent infection rate.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] After that is a series of diagrams of infected humans, and a discussion of cures.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I guess I know all about slimes now!") ]])
	
--Journal B.
elseif(sObjectName == "JournalB") then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiReadClaudiasJournal", "N", 1.0)

	--Setup.
	fnStandardMajorDialogue()
		
	--If Mei has not met Florentina, she doesn't know who Claudia is...
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (This journal belongs to Claudia Romanus.[SOFTBLOCK] Seems she wrote a lot, as this book is full and the last entry is over a year ago.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Should I read it?)[BLOCK][CLEAR]") ]])
	
	--If Florentina is present, Mei knows who Claudia is.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Look, Florentina![SOFTBLOCK] The journal says 'Claudia Romanus'![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Don't get too excited.[SOFTBLOCK] This is one of her older journals.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yep, the last entry is over a year ago, before they came to Trannadar.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We should probably read it anyway, right?[BLOCK]") ]])
	end

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadB\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()

--Reading the southern journal.
elseif(sObjectName == "JournalRead") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Heavenly Doves Research Log:: [SOFTBLOCK]Slime Partirhuman, Trannadar Region'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] There are several sketches of various slime girls, some as humanoids and some as puddles on the ground.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The common slime girl, these creatures can be found in many biomes across Pandemonium.[SOFTBLOCK] They are a carrion feeder which periodically hunts live game.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Slimes are never found in dry regions, as they appear to require frequent moisture refilling.[SOFTBLOCK] Their locomotion appears to consume water at a great rate.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'In addition, they are not found in freezing regions for presumably a similar reason.[SOFTBLOCK] Otherwise, they have a great range.[SOFTBLOCK] Their properties do not change across their habitat.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Slimes usually stay in an amorphous form, hiding from sunlight during the day and emerging at night to consume insects, rodents, or unaware birds.[SOFTBLOCK] Slimes are not choosy in what they eat, but rarely eat much leafy material.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Their life duration is considered to be indefinite at this point in time.[SOFTBLOCK] Their body structure is highly resistant to parasites and diseases, and they do not appear to age like humans do.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Slimes tend to be killed by drying out or being unable to find food.[SOFTBLOCK] Most predators do not regard them as worth the effort, as their bodies provide very little nutrition.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'To reproduce, slimes will behave erratically for several weeks during the summer.[SOFTBLOCK] Instead of hiding during the day, they will take on human form, always female.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'While in human form, their locomotive rate is increased, as is their water consumption.[SOFTBLOCK] The slime will then attempt to locate humans to infect.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'An infected human will slowly become covered in slime.[SOFTBLOCK] The infection progresses over a period of about thirty minutes.[SOFTBLOCK] Once the entirety of the human has been overcome, they are permanently a slime.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'A number of cures exist for this state, but all are worthless once the human succumbs.[SOFTBLOCK] Folk remedies and modern medicine can revert an infected human from approximately an eighty percent infection rate.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] After that is a series of diagrams of infected humans, and a discussion of cures.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I guess I know all about slimes now!") ]])

--Reading the northern journal.
elseif(sObjectName == "JournalReadB") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Heavenly Doves Research Log::[SOFTBLOCK] Quantir Expedition'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'We met up with the faithful located in Dry Well today.[SOFTBLOCK] Despite the climate, they were hospitable and cheerful.[SOFTBLOCK] It's always nice to see fellow adherents on Pandemonium!'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Sadly, none of the convent elected to stay and study with them.[SOFTBLOCK] It seems they wanted to follow me and aid in my research all the more.[SOFTBLOCK] No matter, I will act as their shepard.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'We must first study the demiwasps in the Quantir High Wastes region west of here, and possibly the harpies if we have time.[SOFTBLOCK] After that, we must head to the Trannadar region.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'I'm looking forward to spreading the word amongst the people of Trannadar, but our research does take priority.[SOFTBLOCK] It is so exhausting being on the road, but our purpose is clear and our will is strong.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] There's a lot of research notes about Quantir, but the book is filled before they cross to Trannadar.[SOFTBLOCK] There must be another journal around here...") ]])
	
--Ignore the journal.
elseif(sObjectName == "Ignore") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--[Exits]
elseif(sObjectName == "ToCenterW") then
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseCentralW", "Null")
	else
		fnStandardDialogue([[ [VOICE|Mei](Locked tight.[SOFTBLOCK] There must be a key around here.) ]])	
	end
	
elseif(sObjectName == "ToCenterE") then
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseCentralE", "Null")
	else
		fnStandardDialogue([[ [VOICE|Mei](Locked tight.[SOFTBLOCK] There must be a key around here.) ]])	
	end
	
elseif(sObjectName == "ToNorthE") then
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseNEHall", "FORCEPOS:25.0x42.0x0")
	else
		fnStandardDialogue([[ [VOICE|Mei](Locked tight.[SOFTBLOCK] There must be a key around here.) ]])	
	end
	
elseif(sObjectName == "ToNorthW") then
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseNWHall", "Null")
	else
		fnStandardDialogue([[ [VOICE|Mei](Locked tight.[SOFTBLOCK] There must be a key around here.) ]])	
	end
	
elseif(sObjectName == "ToOutside") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("SpookyExterior", "FORCEPOS:13.5x6.0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end