--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "QuantirManseNEHall"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "QuantirManseTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("QuantirManseNEHall")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Overlay.
	AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0.50, 0.50, 1.0, 1, true)
	
	--NPC Spawner.
	TA_Create("Laura")
		TA_SetProperty("Position", 27, 9)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Extended Activation Direction", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Laura/Root.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/Mei_Ghost/", false)
	DL_PopActiveObject()
	
	--Door opening.
	local iOpenedStatueA = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueA", "N")
	local iOpenedStatueB = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueB", "N")
	local iOpenedStatueC = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueC", "N")
	local iOpenedStatueD = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueD", "N")
	if(iOpenedStatueA == 1.0) then
		AL_SetProperty("Open Door", "LockDoorA")
		AL_SetProperty("Open Door", "LockDoorB")
	end
	if(iOpenedStatueB == 1.0) then
		AL_SetProperty("Open Door", "LockDoorC")
	end
	if(iOpenedStatueC == 1.0) then
		AL_SetProperty("Open Door", "LockDoorD")
		AL_SetProperty("Open Door", "LockDoorE")
		AL_SetProperty("Open Door", "LockDoorF")
		AL_SetProperty("Open Door", "LockDoorG")
		AL_SetProperty("Open Door", "LockDoorH")
		AL_SetProperty("Open Door", "LockDoorI")
	end
	if(iOpenedStatueD == 1.0) then
		AL_SetProperty("Open Door", "LockDoorJ")
	end
	
	--Overlays.
    AL_SetProperty("Allocate Foregrounds", 1)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/Fog")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 2.5, 2.5)
    AL_SetProperty("Foreground Alpha",          0, 0.50, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.30, -0.30)
    AL_SetProperty("Foreground Scale",          0, 2.0)
end
