--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Crate.
if(sObjectName == "Crate") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Cleaning supplies.[SOFTBLOCK] Scrap rags, dusters, extra aprons, that sort of thing.)]])
	
--Foodshelves.
elseif(sObjectName == "Foodshelves") then

	--Pie Job:
	local iSalamiCount = AdInv_GetProperty("Item Count", "Translucent Quantirian Salami")
	local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
	if(iTakenPieJob == 1.0 and iSalamiCount < 1) then
		
		--Short scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hmmmmmmmm...[SOFTBLOCK] hey look![SOFTBLOCK] Quantirian Salami![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Yes, the rare see-through kind.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Are you sure this is a good idea?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] It's ghost-salami, but it's still salami![SOFTBLOCK] This counts![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Okay, I'll let you taste-test the final product first...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Got Translucent Quantirian Salami*") ]])
		
		--Add the item.
		LM_ExecuteScript(gsItemListing, "Translucent Quantirian Salami")
		
		--Reset flag.
		WD_SetProperty("Clear Topic Read", "Pepper Pie")
		
	--Normal case:
	else
		fnStandardDialogue([[ [CLEAR][VOICE|Mei] (The food doesn't seem to be rotted or decaying, but it's still probably ghost food.[SOFTBLOCK] No thanks.)]])
	end

--Coffins.
elseif(sObjectName == "Coffins") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (So many coffins...)]])

--Barrels
elseif(sObjectName == "Barrels") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (The barrel is full of a viscous, black tar...)]])

--[Journals]
elseif(sObjectName == "JournalA") then

	--Setup.
	fnStandardMajorDialogue()
		
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei] (A sketchbook, with a drawing of an Alraune on it.[SOFTBLOCK] Read it?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadA\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalB") then

	--Setup.
	fnStandardMajorDialogue()
		
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei] (A sketchbook, with a drawing of a werecat on it.[SOFTBLOCK] Read it?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadB\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalC") then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiReadClaudiasJournal", "N", 1.0)

	--Setup.
	fnStandardMajorDialogue()
		
	--If Mei has not met Florentina, she doesn't know who Claudia is...
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (This journal belongs to Claudia Romanus.[SOFTBLOCK] Looks like this journal is the last one she wrote in.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Should I read it?)[BLOCK][CLEAR]") ]])
	
	--If Florentina is present, Mei knows who Claudia is.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Florentina![SOFTBLOCK] This is Claudia's journal![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] ...[SOFTBLOCK] Yep, this is hers.[SOFTBLOCK] The last entry is dated a few weeks back.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] But she's not around.[SOFTBLOCK] Just creepy ghosts.[SOFTBLOCK] You don't think...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Better read the journal to find out, right?[BLOCK]") ]])
	end

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadC\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalD") then

	--Setup.
	fnStandardMajorDialogue()
		
	--If Mei has not met Florentina, she doesn't know who Claudia is...
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Hey![SOFTBLOCK] There's some images on this book that look like my runestone!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Should I read it?)[BLOCK][CLEAR]") ]])
	
	--If Florentina is present, Mei knows who Claudia is.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] F-[SOFTBLOCK]Florentina, look![SOFTBLOCK] That symbol![SOFTBLOCK] It's just like my runestone's![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Hey, calm down, kid.[SOFTBLOCK] You'll wake the dead[SOFTBLOCK]. Literally.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I can't wait to read it![BLOCK]") ]])
	end

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadD\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalE") then

	--Setup.
	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei] (A sketchbook, with a drawing of a bee girl on it.[SOFTBLOCK] There's only a small amount on them.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei] ('Bee girls are a species of partirhuman common in many parts of the world. They can survive in any non-freezing non-aquatic biome.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei] (That seems to be it.[SOFTBLOCK] Seems they haven't finished their research.") ]])

--Reading journal A.
elseif(sObjectName == "JournalReadA") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Heavenly Doves Research Log::[SOFTBLOCK] Alraune Partirhuman, Trannadar Region'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] In between the text are sketches of Alraunes.[SOFTBLOCK] Some of the notes are on which species of plant they take after.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'A very peculiar partirhuman, the Alraune is one of several species capable of coexisting with humans, though they often prefer not to.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Most partirhumans have extremely potent reproductive drives, yet Alraunes seem to be able to control theirs.[SOFTBLOCK] Indeed, they have been known to even take jobs in human settlements.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The Alraune reproductive process is fairly straightforward.[SOFTBLOCK] The Alraune passively collects pollen from plant species and returns to a communal pool to deposit the collected pollen.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The pollen is then mixed with water and enchanted.[SOFTBLOCK] When a human is placed in this mixture, they soon become an Alraune.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Alraunes are intelligent, can communicate with humans, and also seem to be able to speak with plant species using some unknown mechanism.[SOFTBLOCK] It is unknown if they can communicate with fungi or animals.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Attempts to interview Alraunes usually end in conflict, as many of them are 'wild'.[SOFTBLOCK] These ones are uninterested in humans except for reproduction, and often attack or flee.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Alraunes seem to be able to recover from nearly any injury.[SOFTBLOCK] They will regrow a lost limb over a long period of time.[SOFTBLOCK] They obviously cannot regrow a lost head, though.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'However, they are extremely frail.[SOFTBLOCK] Alraunes do not bleed the way humans do.[SOFTBLOCK] A cut that might take a human a few hours to heal from may scar an Alraune for weeks or months.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Because they take after plants, Alraunes appear to eat rotted material, like mulch, for sustenance, and need sunlight to survive.[SOFTBLOCK] They can go long periods without sunlight, though, before they wither.[SOFTBLOCK] None have ever been in captivity long enough to observe this.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Each Alraune is unique in that it takes affinity after a certain plant species.[SOFTBLOCK] Some have extra thorns on their body like roses, while others appear to be tulips, or even potatoes.[SOFTBLOCK] It seems the species may be related to the Alraune's personality.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Lastly, Alraunes often refer to one another as 'leaf-sister' and to plants as 'little ones'.[SOFTBLOCK] Their society does not seem to have a hierarchy the way human society does.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I feel smarter already!") ]])
	VM_SetVar("Root/Variables/Chapter1/Scenes/iReadAlraunes", "N", 1.0)

--Reading journal B.
elseif(sObjectName == "JournalReadB") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Heavenly Doves Research Log::[SOFTBLOCK] Werecat Partirhuman, Trannadar Region'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] There are several sketches of werecats in various poses.[SOFTBLOCK] It seems the artist was a real cat lover, as there are regular cats here, too.[SOFTBLOCK] So cute![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The werecat is a partirhuman species known for their superb hunting skills and extreme agility.[SOFTBLOCK] Werecats have been reported to be able to leap up cliffs in a single bound.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Their bone structure and muscle density configure like a spring, allowing them to move extremely quickly in short bursts.[SOFTBLOCK] They are like common housecats in many respects besides these.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Werecat society is highly fragmented.[SOFTBLOCK] Werecats usually move in small groups, between one and four cats.[SOFTBLOCK] The membership of these prides changes periodically, usually after a major hunt.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The werecats hunt for food, but also sport.[SOFTBLOCK] They frequently kill animals simply because they can, and may attack travellers but leave them alive.[SOFTBLOCK] They are merely in it for the challenge.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'There may be some sort of honor code associated with werecat hunts, but it is inconsistently applied.[SOFTBLOCK] The few werecats that agreed to an interview expressed disdain for attacking children, for example.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The reproductive process is difficult to discern.[SOFTBLOCK] It appears to be connected to the phase of the moon, and a large collection of cats will group together when transforming a human.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Werecats refer to one another as 'kinfang', though they do not appear to have status or hierarchy.[SOFTBLOCK] They freely break association with one another, depending on whether they prefer to hunt alone or in a group.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Hopefully, we will be able to observe a werecat transformation ritual in progress, though the odds of doing this undetected are very low.[SOFTBLOCK] More research is required.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I'm going to be an expert on monster girls soon enough!") ]])

--Reading journal C.
elseif(sObjectName == "JournalReadC") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Topic.
	WD_SetProperty("Unlock Topic", "Rilmani", 1)
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Heavenly Doves Research Log::[SOFTBLOCK] Quantir Expedition'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Let's see...[SOFTBLOCK] this part -[SOFTBLOCK] Rilmani...?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hmm, a species that -[SOFTBLOCK] communicates with symbols![SOFTBLOCK] Some of these look like the one on my runestone![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'We believe that the large manor near the lake in eastern Trannadar may be an artifact known as the 'Dimensional Trap'.[SOFTBLOCK] Local maps refer to it as such, and its position is consistent with legend.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'It is probably a misnomer to call it a manor, as it supposedly changes shape periodically to match modern styles.[SOFTBLOCK] In a few centuries it may have completely changed its layout.[SOFTBLOCK] Some older stories refer to the structure as a tower, for example.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The Rilmani species is likely involved with the creation of the building.[SOFTBLOCK] Most scholars do not believe the Rilmani exist, but I, Claudia Romanus, can personally attest to their existence.[SOFTBLOCK] I have encountered several on my journeys.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The building appears to be some sort of low point between dimensions.[SOFTBLOCK] Objects lost between dimensions tend to accrue there, sometimes doing so years or decades after the fact.[SOFTBLOCK] In a few cases, explorers claim to have found objects from the future, as well.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Unfortunately, most of this is uncorroborated legend. [SOFTBLOCK]There is little in the Dimensional Trap at present, save for a religious cult who has taken up residence there.[SOFTBLOCK] They have halted all research progress.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Crud![SOFTBLOCK] She doesn't say anything more about the Rilmani in here...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] But she does make a few references to another document.[SOFTBLOCK] If I can find that, it might have a translation in it![BLOCK][CLEAR]") ]])
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well then let's go find it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Have you ever heard of a Rilmani, Florentina?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] There's local legends around here, yes.[SOFTBLOCK] I didn't think they exist.[SOFTBLOCK] Maybe they don't, but your runestone...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If they have something to do with dimensional travel, maybe they're the reason I'm here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Maybe.[SOFTBLOCK] Let's look for that document.") ]])
	end
	
--Reading journal D.
elseif(sObjectName == "JournalReadD") then
	
	--Flags.
	local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N", 1.0)
	
	--Topic.
	WD_SetProperty("Unlock Topic", "Rilmani", 1)
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Clear the flag on this topic if this is the first time reading the book.
	if(iMeiKnowsRilmani == 0.0) then
		WD_SetProperty("Clear Topic Read", "NextMove")
	end
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Heavenly Doves Research Log::[SOFTBLOCK] Rilmani Partirhuman, Trannadar Region'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Diagrams, diagrams...[SOFTBLOCK] Seems the doves have no idea what these things look like.[SOFTBLOCK] This one...[SOFTBLOCK] looks kind of like an olive?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The Rilmani are believed to be one of the oldest partirhuman species in existence.[SOFTBLOCK] What little we know of them is based on legend and rumour.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Artifacts of Rilmani origin are rare and usually prized.[SOFTBLOCK] They often contain potent magics which cannot be replicated by human sorcery, and thus are sought after by the wise and the greedy alike.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Most curiously, the Rilmani language does not evolve the way human languages do.[SOFTBLOCK] Instead, the Rilmani are said to already have every word they will ever need, and instead simply discover the new word at the right time.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'What follows is an incomplete study of known Rilmani words...'[BLOCK][CLEAR]") ]])
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Courage![SOFTBLOCK] Valor![SOFTBLOCK] Bravery![SOFTBLOCK] Look, Florentina, that's what my runestone means![SOFTBLOCK] It's a Rilmani word![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It means all three?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'These are the six words that are considered first-order in the Rilmani language.[SOFTBLOCK] They are only used in specific contexts, though the nature of the context is not known.'[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] So my runestone is one of these first-order words![SOFTBLOCK] I don't recognize the other five...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Proper nouns are third-order, and are allowed to be created.[SOFTBLOCK] Most other words are second-order and supposedly have existed as long as the Rilmani have.[SOFTBLOCK] The first-order words are therefore meant to be older than the Rilmani language.'[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So just what does that mean?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I haven't got any idea![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But, I bet if we poke around the Dimensional Trap for a bit, we can find some hints.[SOFTBLOCK] With this, we can try to translate anything we find![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So...[SOFTBLOCK] no payoff?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Oh for crying - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Relax, kid.[SOFTBLOCK] If we can find a real-live Rilmani, I can probably mug her and get something worth a fortune.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Are you joking right now?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] If they look like this sketch of a cloudy pig, yes.[SOFTBLOCK] If they look like this one that has all the spikes...[SOFTBLOCK] not so much...") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Courage![SOFTBLOCK] Valor![SOFTBLOCK] Bravery![SOFTBLOCK] It's a Rilmani word![SOFTBLOCK] My runestone is definitely a Rilmani artifact![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, with this translation guide, I bet I can find something at that old mansion to help me get home![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I hope the Rilmani can help me, if it comes to that...") ]])
	end

--Ignore the journal.
elseif(sObjectName == "Ignore") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--Bookshelf A.
elseif(sObjectName == "BookshelfA") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[SOFTBLOCK] Seems there's a few torn pages jammed in various books here...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Seems this is the first in the series...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('Countess Quantir keeps on disappearing for days at a time.[SOFTBLOCK] The head maid doesn't know any more than anyone else where she is.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I asked her about it, but she blew me off.[SOFTBLOCK] She looks so tired all the time, but then again, everyone is.[SOFTBLOCK] The sick keep coming and all we can do is ease their pain.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I think she's working harder than anyone else.[SOFTBLOCK] She was a great mage, once, and I bet she's trying to find a magical cure.[SOFTBLOCK] The failure must be eating at her.')") ]])

--Bookshelf B.
elseif(sObjectName == "BookshelfB") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[SOFTBLOCK] Seems there's a few torn pages jammed in various books here...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Seems this is the second in the series...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('While the Countess was out, I decided to take a look at some of her old magic books.[SOFTBLOCK] It's all way above my level, but I tried a few of the incantations and - [SOFTBLOCK]they worked![SOFTBLOCK] I was so excited!')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('As soon as the Countess got back, I showed her what I learned.[SOFTBLOCK] I wasn't expecting her to yell at me like that.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I guess I should have known better.[SOFTBLOCK] Those were *her* magic books, after all, but it's not like she was using them.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('She looked really, really scared when I told her.[SOFTBLOCK] I guess she's under a lot of stress.[SOFTBLOCK] Everyone is counting on her to cure the sickness, because nothing else has worked.')") ]])

--Bookshelf C.
elseif(sObjectName == "BookshelfC") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[SOFTBLOCK] Seems there's a few torn pages jammed in various books here...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Seems this is the third in the series...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('Ernie came down sick.[SOFTBLOCK] Doctor said it was the plague.[SOFTBLOCK] Now the serving staff are getting sick, even though we've been keeping clean and bathing just like the Countess said.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I've handled a lot of the dead bodies -[SOFTBLOCK] am I going to get sick too?[SOFTBLOCK] I feel great, but so did Ernie.[SOFTBLOCK] Now, he's pretty much done for.[SOFTBLOCK] There's no cure...')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('...[SOFTBLOCK] Countess came back after one of her absences today.[SOFTBLOCK] Ernie had gone in the middle of the night, as had a bunch of the sick patients![SOFTBLOCK] Countess said she managed to cure them!')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('She said she had been working on a healing spell, and it worked![SOFTBLOCK] But, only on some people, and not others.[SOFTBLOCK] She's not sure why.[SOFTBLOCK] But, there's hope![SOFTBLOCK] She can do it!')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I decided to borrow her books again, and try my best to learn magic, too.[SOFTBLOCK] Of course I won't be telling her I'm practicing, at least not until I'm sure I can help.[SOFTBLOCK] But maybe we can cure everyone!')") ]])

--Bookshelf D.
elseif(sObjectName == "BookshelfD") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[SOFTBLOCK] Seems there's a few torn pages jammed in various books here...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Seems this is the fourth in the series...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I asked the head maid where Ernie was, and she said the Countess had to send all the cured patients away to Trannadar.[SOFTBLOCK] She said they needed to be quarantined so they don't get sick again.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I didn't know Ernie too well, but May has to be really broken up.[SOFTBLOCK] She and him were all sweet and such, and I don't think she knew.[SOFTBLOCK] I bet she wants to head out to Trannadar, but she has to keep helping the sick here.[SOFTBLOCK] Must be tough.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I decided to talk to the Countess again, but I didn't tell her about my magic practice.[SOFTBLOCK] She's really nice on the outside, but I can tell she doesn't like me.[SOFTBLOCK] She probably holds a grudge about the books.[SOFTBLOCK] It's okay, she'll come around.')") ]])

--Bookshelf E.
elseif(sObjectName == "BookshelfE") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[SOFTBLOCK] Seems there's a few torn pages jammed in various books here...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Seems this is the fifth in the series...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('More of the other maids got sick this week.[SOFTBLOCK] The Countess said she's trying, but she can't cure them.[SOFTBLOCK] We had to bury Laura last week.[SOFTBLOCK] I've never cried so hard.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('Every minute I'm not on duty, I'm reading those books.[SOFTBLOCK] I still don't understand half of it, but I'm getting better.[SOFTBLOCK] The Countess mentioned I have a lot of talent, last we talked.[SOFTBLOCK] I think she knows I'm reading them.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('...[SOFTBLOCK] Nina's dead.[SOFTBLOCK] So is Emmeline.[SOFTBLOCK] There's only five maids left.[SOFTBLOCK] All the butlers are either dead or really sick.[SOFTBLOCK] I'm fine, but I think everyone else is showing the first signs.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I don't want to be alone...')") ]])

--Bookshelf F.
elseif(sObjectName == "BookshelfF") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[SOFTBLOCK] Seems there's a few torn pages jammed in various books here...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Seems this is the last in the series...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('Well, I found out where the Countess has been disappearing to.[SOFTBLOCK] She's been using an invisibility spell and going down into the sewers![SOFTBLOCK] There's a secret room and everything![SOFTBLOCK] That must be where she's been working on her healing spell.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I have to bury Penrose, and...[SOFTBLOCK] then I'm going to show her what I've learned.[SOFTBLOCK] No more patients are showing up, I think everyone has either died or fled Quantir.[SOFTBLOCK] I can't blame them.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I'm still feeling fine, though.[SOFTBLOCK] Maybe I'm immune, or maybe one of my spells helped.[SOFTBLOCK] I don't know.[SOFTBLOCK] I've never told her, but the Countess is my hero.[SOFTBLOCK] I wanted to tell her because, if she dies, I won't know what to do.')[BLOCK][CLEAR]") ]])

	--If Mei doesn't have the sewer key:
	local iSewerKeyCount = AdInv_GetProperty("Item Count", "Quantir Sewer Key")
	if(iSewerKeyCount == 0.0) then
		LM_ExecuteScript(gsItemListing, "Quantir Sewer Key")
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I hope she's not sick and hiding it, down there in the sewers.[SOFTBLOCK] She's given so much for us.[SOFTBLOCK] We failed, but at least we tried.')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Hey, looks like the author left their keyring here.[SOFTBLOCK] There are several spare keys.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei][SOUND|World|TakeItem]*Got Quantir Sewer Key*") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I hope she's not sick and hiding it, down there in the sewers.[SOFTBLOCK] She's given so much for us.[SOFTBLOCK] We failed, but at least we tried.')") ]])
	end

--Bookshelf X.
elseif(sObjectName == "BookshelfX") then
	LM_ExecuteScript(gsMeiSkillbook, 4)

--Bookshelf Z.
elseif(sObjectName == "BookshelfZ") then
	LM_ExecuteScript(gsFlorentinaSkillbook, 4)

--[Statues]
elseif(sObjectName == "OddStatueA") then

	local iOpenedStatueA = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueA", "N")
	
	if(iOpenedStatueA == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueA", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This statue looks different from the others...[SOFTBLOCK] hey!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's a switch here...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Something was opened...)") ]])
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorA") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorB") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I've already flipped the switch on this statue.)") ]])
	end
	
elseif(sObjectName == "OddStatueB") then

	local iOpenedStatueB = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueB", "N")
	
	if(iOpenedStatueB == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueB", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This statue looks different from the others...[SOFTBLOCK] hey!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's a switch here...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Something was opened...)") ]])
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorC") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I've already flipped the switch on this statue.)") ]])
	end
	
elseif(sObjectName == "OddStatueC") then

	local iOpenedStatueC = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueC", "N")
	
	if(iOpenedStatueC == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueC", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This statue looks different from the others...[SOFTBLOCK] hey!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's a switch here...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Something was opened...)") ]])
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorD") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorE") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorF") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorG") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorH") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorI") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I've already flipped the switch on this statue.)") ]])
	end
	
elseif(sObjectName == "OddStatueD") then

	local iOpenedStatueD = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueD", "N")
	
	if(iOpenedStatueD == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueD", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This statue looks different from the others...[SOFTBLOCK] hey!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's a switch here...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Something was opened...)") ]])
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockDoorJ") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I've already flipped the switch on this statue.)") ]])
	end
		
--[Doors]
elseif(sObjectName == "LockedDoor") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Locked.[SOFTBLOCK] There's a mechanism attached to the door.[SOFTBLOCK] Maybe something nearby opens it?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BrokenDoor") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]The lock is broken.[SOFTBLOCK] No way I can get through this door.)") ]])
	fnCutsceneBlocker()

--[Exits]
elseif(sObjectName == "ToEntrance") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseEntrance", "FORCEPOS:14x7")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end