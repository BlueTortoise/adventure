--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
if(sObjectName == "Crates") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Cleaning supplies and old clothes.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Firepit") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A firepit, long since burned down.[SOFTBLOCK] I can see book covers in the ash pile.[SOFTBLOCK] There is no wind here, so the ash hasn't blown away...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "MagicBookA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The covers promise books on sorcery, but the pages have all been ripped out.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "MagicBookB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Books on the history of Quantir.[SOFTBLOCK] All the pages are ripped out.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "MagicBookC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Storybooks about knights and dragons.[SOFTBLOCK] The pages are all ripped out.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Corpse") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I suppose this is whoever burned the books.[SOFTBLOCK] It seems to be the only unburied skeleton in the mansion.[SOFTBLOCK] Does that mean this was the final survivor?)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There was a rusty knife lodged in the skeleton's ribcage.[SOFTBLOCK] I'll leave it where it is.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalD") then

	--Setup.
	fnStandardMajorDialogue()
		
	--If Mei has not met Florentina, she doesn't know who Claudia is...
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Hey![SOFTBLOCK] There's some images on this book that look like my runestone!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Should I read it?)[BLOCK][CLEAR]") ]])
	
	--If Florentina is present, Mei knows who Claudia is.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] F-[SOFTBLOCK]Florentina, look![SOFTBLOCK] That symbol![SOFTBLOCK] It's just like my runestone's![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Hey, calm down, kid.[SOFTBLOCK] You'll wake the dead[SOFTBLOCK]. Literally.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I can't wait to read it![BLOCK]") ]])
	end

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadD\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
--Reading journal D.
elseif(sObjectName == "JournalReadD") then
	
	--Flags.
	local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N", 1.0)
	
	--Topic.
	WD_SetProperty("Unlock Topic", "Rilmani", 1)
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Clear the flag on this topic if this is the first time reading the book.
	if(iMeiKnowsRilmani == 0.0) then
		WD_SetProperty("Clear Topic Read", "NextMove")
	end
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Heavenly Doves Research Log::[SOFTBLOCK] Rilmani Partirhuman, Trannadar Region'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Diagrams, diagrams...[SOFTBLOCK] Seems the doves have no idea what these things look like.[SOFTBLOCK] This one...[SOFTBLOCK] looks kind of like an olive?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'The Rilmani are believed to be one of the oldest partirhuman species in existence.[SOFTBLOCK] What little we know of them is based on legend and rumour.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Artifacts of Rilmani origin are rare and usually prized.[SOFTBLOCK] They often contain potent magics which cannot be replicated by human sorcery, and thus are sought after by the wise and the greedy alike.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Most curiously, the Rilmani language does not evolve the way human languages do.[SOFTBLOCK] Instead, the Rilmani are said to already have every word they will ever need, and instead simply discover the new word at the right time.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'What follows is an incomplete study of known Rilmani words...'[BLOCK][CLEAR]") ]])
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Courage![SOFTBLOCK] Valor![SOFTBLOCK] Bravery![SOFTBLOCK] Look, Florentina, that's what my runestone means![SOFTBLOCK] It's a Rilmani word![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It means all three?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'These are the six words that are considered first-order in the Rilmani language.[SOFTBLOCK] They are only used in specific contexts, though the nature of the context is not known.'[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] So my runestone is one of these first-order words![SOFTBLOCK] I don't recognize the other five...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] 'Proper nouns are third-order, and are allowed to be created.[SOFTBLOCK] Most other words are second-order and supposedly have existed as long as the Rilmani have.[SOFTBLOCK] The first-order words are therefore meant to be older than the Rilmani language.'[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So just what does that mean?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I haven't got any idea![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But, I bet if we poke around the Dimensional Trap for a bit, we can find some hints.[SOFTBLOCK] With this, we can try to translate anything we find![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So...[SOFTBLOCK] no payoff?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Oh for crying - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Relax, kid.[SOFTBLOCK] If we can find a real-live Rilmani, I can probably mug her and get something worth a fortune.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Are you joking right now?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] If they look like this sketch of a cloudy pig, yes.[SOFTBLOCK] If they look like this one that has all the spikes...[SOFTBLOCK] not so much...") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Courage![SOFTBLOCK] Valor![SOFTBLOCK] Bravery![SOFTBLOCK] It's a Rilmani word![SOFTBLOCK] My runestone is definitely a Rilmani artifact![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, with this translation guide, I bet I can find something at that old mansion to help me get home![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I hope the Rilmani can help me, if it comes to that...") ]])
	end

--Ignore the journal.
elseif(sObjectName == "Ignore") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--Bookshelf F.
elseif(sObjectName == "BookshelfF") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[SOFTBLOCK] Seems there's a few torn pages jammed in various books here...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Seems this is the last in the series...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('Well, I found out where the Countess has been disappearing to.[SOFTBLOCK] She's been using an invisibility spell and going down into the sewers![SOFTBLOCK] There's a secret room and everything![SOFTBLOCK] That must be where she's been working on her healing spell.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I have to bury Penrose, and...[SOFTBLOCK] then I'm going to show her what I've learned.[SOFTBLOCK] No more patients are showing up, I think everyone has either died or fled Quantir.[SOFTBLOCK] I can't blame them.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I'm still feeling fine, though.[SOFTBLOCK] Maybe I'm immune, or maybe one of my spells helped.[SOFTBLOCK] I don't know.[SOFTBLOCK] I've never told her, but the Countess is my hero.[SOFTBLOCK] I wanted to tell her because, if she dies, I won't know what to do.')[BLOCK][CLEAR]") ]])

	--If Mei doesn't have the sewer key:
	local iSewerKeyCount = AdInv_GetProperty("Item Count", "Quantir Sewer Key")
	if(iSewerKeyCount == 0.0) then
		LM_ExecuteScript(gsItemListing, "Quantir Sewer Key")
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I hope she's not sick and hiding it, down there in the sewers.[SOFTBLOCK] She's given so much for us.[SOFTBLOCK] We failed, but at least we tried.')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Hey, looks like the author left their keyring here.[SOFTBLOCK] There are several spare keys.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei][SOUND|World|TakeItem]*Got Quantir Sewer Key*") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I hope she's not sick and hiding it, down there in the sewers.[SOFTBLOCK] She's given so much for us.[SOFTBLOCK] We failed, but at least we tried.')") ]])
	end
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end