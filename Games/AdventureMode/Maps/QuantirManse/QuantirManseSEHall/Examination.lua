--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Filthy Bed.
if(sObjectName == "Bed") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Looks like a bed used by the serving staff.[SOFTBLOCK] It's filthy, and it looks like there are traces of old vomit on it...)]])
	
--Crates
elseif(sObjectName == "Crates") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (There was food in these crates once, but it seems to have rotted to nothing.)]])
	
--Men's Lavatory
elseif(sObjectName == "MensLavatory") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Men's lavatory.[SOFTBLOCK] No thanks.)]])
	
--Women's Lavatory
elseif(sObjectName == "WomensLavatory") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Women's lavatory.[SOFTBLOCK] Not tempted...)]])
	
--Baths
elseif(sObjectName == "Baths") then
	fnStandardDialogue([[ [CLEAR][VOICE|Mei] (Looks like a public bath, maybe for the serving staff.[SOFTBLOCK] Seems they were okay with the sexes mixing, as this area is pretty open.)]])

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end