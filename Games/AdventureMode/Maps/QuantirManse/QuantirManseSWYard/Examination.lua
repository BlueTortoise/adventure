--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Barrels.
if(sObjectName == "Barrels") then
	fnStandardDialogue([[[VOICE|Mei](A terrible smell is coming from these barrels.[SOFTBLOCK] No way am I opening them!) ]])

--Crates.
elseif(sObjectName == "Crates") then
	fnStandardDialogue([[[VOICE|Mei](There's something oppressive about the crates here.[SOFTBLOCK] I'm not opening them!) ]])

--Coffin.
elseif(sObjectName == "Coffin") then
	fnStandardDialogue([[[VOICE|Mei](A coffin.[SOFTBLOCK] It seems to be occupied.[SOFTBLOCK] I'm not a graverobber, so it's staying closed.) ]])
	
--Gaardian Cave Moss.
elseif(sObjectName == "GaardianMoss") then
	
	--Pie Job:
	local iMossCount = AdInv_GetProperty("Item Count", "Gaardian Cave Moss")
	local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
	if(iTakenPieJob == 1.0 and iMossCount < 1) then
		
		--Short scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Wait up a second, Mei.[SOFTBLOCK] This guy is the kind of moss we need.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Really?[SOFTBLOCK] That's going into the pie?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] It is...[SOFTBLOCK] less than appetizing...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You're going to eat it and you're going to like it.[SOFTBLOCK] Now shut up and grab a handful.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Got Gaardian Cave Moss*") ]])
		
		--Add the item.
		LM_ExecuteScript(gsItemListing, "Gaardian Cave Moss")
		
		--Reset flag.
		WD_SetProperty("Clear Topic Read", "Pepper Pie")
		
	--Normal:
	else
		fnStandardDialogue("[VOICE|Mei](Moss growing on the edge of the room.[SOFTBLOCK] It is...[SOFTBLOCK] oddly appropriate, for some reason.)")
	end

--Exit door:
elseif(sObjectName == "ExitDoor") then
	AL_BeginTransitionTo("QuantirManseSWYardInterior", "FORCEPOS:5.0x16.0x0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end