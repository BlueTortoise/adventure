--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Duties") then
	
	--Player has not seen this scene before.
	local iSawDutiesScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawDutiesScene", "N")
	local iDisturbedCoffin = VM_GetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N")
	if(iSawDutiesScene == 0.0 and iDisturbedCoffin == 1.0) then
		
		--Variables.
		local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawDutiesScene", "N", 1.0)
	
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Ghost][SOFTBLOCK]*Return to your duties...*[BLOCK][CLEAR]") ]])
		
		--If Mei is alone:
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (..?[SOFTBLOCK] I thought I heard something...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (.[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK] Guess it was nothing.)[SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Ghost][SOFTBLOCK]*You will be punished...*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Is someone there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] It's -[SOFTBLOCK] really cold all of a sudden.[SOFTBLOCK] Something is different...") ]])
			fnCutsceneBlocker()

		--Florentina is here:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Did you say something?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I thought you did.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Ghost][SOFTBLOCK]*You will be punished...*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Okay, that wasn't you.[SOFTBLOCK] Who said that?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] It's -[SOFTBLOCK] really cold all of a sudden.[SOFTBLOCK] Something is different...") ]])
			fnCutsceneBlocker()
		end
	
		--Start the music.
		AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 1, 1, 1, 1, 0.50, 0.50, 1.0, 1, true)
		fnCutsceneInstruction([[ AL_SetProperty("Music", "QuantirManseTheme") ]])
		fnCutsceneBlocker()
		
		--Re-enable the enemies.
		fnCutsceneInstruction([[ 
		EM_PushEntity("GhostA0")
			TA_SetProperty("Disabled", false)
		DL_PopActiveObject()
		EM_PushEntity("GhostB0")
			TA_SetProperty("Disabled", false)
		DL_PopActiveObject()
		]])
	
	end
end