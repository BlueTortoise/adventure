--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Barrels.
if(sObjectName == "Barrels") then
	fnStandardDialogue([[[VOICE|Mei](A terrible smell is coming from these barrels.[SOFTBLOCK] No way am I opening them!) ]])

--Crates.
elseif(sObjectName == "Crates") then
	fnStandardDialogue([[[VOICE|Mei](There's something oppressive about the crates here.[SOFTBLOCK] I'm not opening them!) ]])

--Coffin.
elseif(sObjectName == "Coffin") then
	fnStandardDialogue([[[VOICE|Mei](A coffin.[SOFTBLOCK] It seems to be occupied.[SOFTBLOCK] I'm not a graverobber, so it's staying closed.) ]])

--Coffin Special.
elseif(sObjectName == "CoffinSpecial") then

	--First time disturbing the coffin:
	local iDisturbedCoffin = VM_GetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N")
	if(iDisturbedCoffin == 0.0) then
		LM_ExecuteScript(gsItemListing, "Quantir Mansion Key")
		VM_SetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N", 1.0)
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](This coffin is open a crack...[SOFTBLOCK] and I think I can see something in there!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](Looks like it's a journal page left by the person who was doing the burial.[SOFTBLOCK] Let's see...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('We've been working so hard lately, but the ranks of the sick and dying continue to swell.[SOFTBLOCK] Countess Quantir says she will find a cure, but I am not so certain.')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('Rumour has it that Countess Quantir was a great magician in her younger days, but fat lot of good that seems to be doing against the epidemic!')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I shouldn't be so harsh.[SOFTBLOCK] She's trying to find a cure.[SOFTBLOCK] She says that the disease spreads in filth and among rodents, so we've been bathing the sick and keeping the manor spotless.')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I've been charged with the burials, but I'd be lying if I said it wasn't getting to me.[SOFTBLOCK] And yet, I don't want to let the Countess down after all she's done for me.')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](..!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](There's a key in here too!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Got Quantir Mansion Key!*") ]])

	--Further cases.
	else
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](The journal page left by the person doing the burial...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('We've been working so hard lately, but the ranks of the sick and dying continue to swell.[SOFTBLOCK] Countess Quantir says she will find a cure, but I am not so certain.')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('Rumour has it that Countess Quantir was a great magician in her younger days, but fat lot of good that seems to be doing against the epidemic!')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I shouldn't be so harsh.[SOFTBLOCK] She's trying to find a cure.[SOFTBLOCK] She says that the disease spreads in filth and among rodents, so we've been bathing the sick and keeping the manor spotless.')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei]('I've been charged with the burials, but I'd be lying if I said it wasn't getting to me.[SOFTBLOCK] And yet, I don't want to let the Countess down after all she's done for me.')") ]])
	end
	
--Gaardian Cave Moss.
elseif(sObjectName == "GaardianMoss") then
	
	--Pie Job:
	local iMossCount = AdInv_GetProperty("Item Count", "Gaardian Cave Moss")
	local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
	if(iTakenPieJob == 1.0 and iMossCount < 1) then
		
		--Short scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Wait up a second, Mei.[SOFTBLOCK] This guy is the kind of moss we need.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Really?[SOFTBLOCK] That's going into the pie?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] It is...[SOFTBLOCK] less than appetizing...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You're going to eat it and you're going to like it.[SOFTBLOCK] Now shut up and grab a handful.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Got Gaardian Cave Moss*") ]])
		
		--Add the item.
		LM_ExecuteScript(gsItemListing, "Gaardian Cave Moss")
		
		--Reset flag.
		WD_SetProperty("Clear Topic Read", "Pepper Pie")
		
	--Normal:
	else
		fnStandardDialogue("[VOICE|Mei](Moss growing on the edge of the room.[SOFTBLOCK] It is...[SOFTBLOCK] oddly appropriate, for some reason.)")
	end

--Exit door:
elseif(sObjectName == "ExitDoor") then
	AL_BeginTransitionTo("QuantirManseSWYard", "FORCEPOS:5.0x8.0x0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end