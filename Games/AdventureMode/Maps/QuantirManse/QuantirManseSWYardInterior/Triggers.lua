--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Maid Ghost cutscene.
if(sObjectName == "Duties") then
	
	--Player has not seen this scene before.
	local iSawDutiesScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawDutiesScene", "N")
	local iDisturbedCoffin = VM_GetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N")
	if(iSawDutiesScene == 0.0 and iDisturbedCoffin == 1.0) then
		
		--[Variables]
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawDutiesScene", "N", 1.0)
		
		--Variables.
		local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		
		--[Setup]
		--Sound effect.
		AudioManager_PlaySound("World|FlipSwitch")
		
		--Spawn a ghost.
		TA_Create("GhostMaid")
			TA_SetProperty("Position", 5, 16)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/MaidGhost/", false)
		DL_PopActiveObject()
		
		--[Movement]
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Ghost walks up to the party.
		fnCutsceneMove("GhostMaid", 5.25, 15.50)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--[Dialogue]
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Surprise") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Surprise") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Surprise") ]])
		end
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost][SOFTBLOCK] I smell...[SOFTBLOCK] sickness...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: S-[SOFTBLOCK]stay back!") ]])
		fnCutsceneBlocker()
	
		--[Movement]
		--Fade to blue.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 1, 1, 1, 1, 0.50, 0.50, 1.0, 1, true) ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Ghost walks up to the party.
		fnCutsceneMove("GhostMaid", 5.25, 12.50)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--[Dialogue]
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Surprise") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Surprise") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Surprise") ]])
		end
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] You are sick.[SOFTBLOCK] We can help you.[SOFTBLOCK] Please, come with me.[BLOCK][CLEAR]") ]])
		
		--Mei is alone:
		if(bIsFlorentinaPresent == false) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Sick?[SOFTBLOCK] I'm not sick...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] Look at me.[SOFTBLOCK] Breathe deeply.[SOFTBLOCK] You are sick.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Listen to her.\", " .. sDecisionScript .. ", \"Listen\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Fight her!\",  " .. sDecisionScript .. ", \"Fight\") ")
			fnCutsceneBlocker()
		
		--Florentina is here:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Sick?[SOFTBLOCK] I'm not sick...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] Look at me.[SOFTBLOCK] Breathe deeply.[SOFTBLOCK] You are sick.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Mei?[SOFTBLOCK] Mei![SOFTBLOCK] Get a hold of yourself![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] You're ill.[SOFTBLOCK] We can help.[SOFTBLOCK] Please, listen to me...[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Listen to her.\", " .. sDecisionScript .. ", \"Listen\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Fight her!\",  " .. sDecisionScript .. ", \"Fight\") ")
			fnCutsceneBlocker()
		end
	end

--Mei decides to listen to the ghost.
elseif(sObjectName == "Listen") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Darken the screen.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])
	end
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
	
	--Text.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] [SOFTBLOCK]Can...[SOFTBLOCK] you...[SOFTBLOCK] help me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] Yes.[SOFTBLOCK] Follow me.[BLOCK][CLEAR]") ]])
	
	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] My skin...[SOFTBLOCK] It looks wrong...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] It is an effect of the plague.[SOFTBLOCK] We must wash you immediately.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Please do...[SOFTBLOCK] I feel...[SOFTBLOCK] faint...[BLOCK][CLEAR]") ]])
		fnCutsceneBlocker()
		
	--Florentina is present:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] What are you doing, Mei?[SOFTBLOCK] Hey![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Can you hear me?[SOFTBLOCK] Mei![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Did you hear something...?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] No.[SOFTBLOCK] We are alone.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] You're running a fever.[SOFTBLOCK] You must be hearing things.[SOFTBLOCK] Come quickly.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] My skin...[SOFTBLOCK] It looks wrong...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] It is an effect of the plague.[SOFTBLOCK] We must wash you immediately.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Mei![SOFTBLOCK] Don't -[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK][SOUND|World|Thump][SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...") ]])
		fnCutsceneBlocker()
	end
		
	--Wait a bit.
	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--Scene.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The maid led Mei through the courtyard and back into the main building.[SOFTBLOCK] A chill ran up and down her body, and her teeth chattered as they walked.[SOFTBLOCK] Her head spun and the world became a blur.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She saw other maids as they went.[SOFTBLOCK] They gave her a passing glance or a curt nod, scarcely covering their disinterest in her.[SOFTBLOCK] Doubtless they had seen many more sick like her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The maid led her to the baths.[SOFTBLOCK] Without needing to be told, she disrobed and stepped into the waters.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her chill seemed to abate.[SOFTBLOCK] The water was warm and had a flowery aroma.[SOFTBLOCK] The maid poured some salts into the bath as Mei began to clean herself.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK]She floated for a time.[SOFTBLOCK] Her hostess said nary a word.[SOFTBLOCK] She closed her eyes and enjoyed the bath, and the feeling of cleanliness.[SOFTBLOCK] She hated being sick.[SOFTBLOCK] Why had fate done this to her?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "After a few moments, the maid gently prodded her.[SOFTBLOCK] She knew she needed to leave the bath, but her clothes had gone.[SOFTBLOCK] She dried herself with a nearby towel and wondered what had become of them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She gave an inquiring look to the maid.[SOFTBLOCK] [VOICE|Ghost]'Your clothes will need to be cleaned, I sent them to your room.[SOFTBLOCK] Please, wear these instead.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The maid produced a uniform much like her own.[SOFTBLOCK] [VOICE|Ghost]'I'm sorry, but we have no clothes in your size.[SOFTBLOCK] This is all that is available.'") ]])
	fnCutsceneBlocker()
	
	--Restart.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The maid uniform was the most beautiful dress she had ever seen.[SOFTBLOCK] She loved the frilly lace and ribbons that adorned it, and the fabric was finely made and soft on her skin.[SOFTBLOCK] She pulled it over her and felt a weight lift from her heart.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She felt a draft in the room.[SOFTBLOCK] Something seemed to be blowing over and around her, but the lovely dress absorbed her full attention.[SOFTBLOCK] She scarcely noted something hard circling around her feet, clanking as it went...") ]])
	fnCutsceneBlocker()
	
	--Restart.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Something slammed shut around her foot, as another cold, hard feeling began snaking up her leg.[SOFTBLOCK] She looked down in horror to see a chain climbing her leg -[SOFTBLOCK] no, her leg -[SOFTBLOCK] her leg had become a vapour![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "A horrible realization gripped Mei -[SOFTBLOCK] none of this was real![SOFTBLOCK] The bath, the maid -[SOFTBLOCK] the maid was a ghost![SOFTBLOCK] She had tricked her and now -[SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK]Calm.[SOFTBLOCK] No need to panic.[SOFTBLOCK] Everything was fine.") ]])
	fnCutsceneBlocker()
	
	--Restart.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Ghost") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The strange feeling, of something cold blowing, had come again.[SOFTBLOCK] Natalie was unperturbed.[SOFTBLOCK] Bath time was over, and she had to resume her duties.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She thought, perhaps, she had fallen asleep in the bath.[SOFTBLOCK] It happened fairly often.[SOFTBLOCK] The water was warm and comfortable, she certainly wouldn't have been the first to do so.[SOFTBLOCK] But her dream had been so vivid.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "A charming, pretty girl, from Hong Kong, fighting to find her way home.[SOFTBLOCK] It seemed so unreal, but then, dreams always did.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She had been assigned to clean the guest room.[SOFTBLOCK] She made her way there, thinking about Mei and her fantastical journey...") ]])
	fnCutsceneBlocker()
	
	--Transfer:
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_BeginTransitionTo("QuantirManseCentralW", gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Ghost/Scene_Alternate.lua") ]])
	fnCutsceneBlocker()

--Mei decides to fight the ghost:
elseif(sObjectName == "Fight") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])
	end
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
	
	--Text.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] [SOFTBLOCK][SOUND|World|Thump]Back off![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] Ngh![SOFTBLOCK] I'm trying to help you...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost:[VOICE|Ghost] The sickness must be eradicated...[SOFTBLOCK] We will help you...[SOFTBLOCK] You will be cured...") ]])
	fnCutsceneBlocker()
		
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
		
	--Ghost walks up to the party.
	fnCutsceneMove("GhostMaid", 5.25, 16.50)
	fnCutsceneBlocker()
	
	--Play a sound and teleport the ghost away.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
	fnCutsceneTeleport("GhostMaid", -100.25, -100.50)
	fnCutsceneBlocker()
		
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Confused") ]])
	end
	
	--Talking.
	if(bIsFlorentinaPresent == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] What on Earth was she talking about... I'm not sick... Am I?") ]])
		fnCutsceneBlocker()
	
	--With Florentina:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] What on Earth was she talking about...[SOFTBLOCK] I'm not sick...[SOFTBLOCK] Am I?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Of course this old place would be haunted.[SOFTBLOCK] Just my luck.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Stay strong, Mei.[SOFTBLOCK] They might not be keen on letting us leave.") ]])
		fnCutsceneBlocker()
	end
end