--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Point where the party can drop down.
if(sObjectName == "DropPoint") then
	
	--Variables
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Common.
	fnStandardMajorDialogue()
	
	--If Florentina is not present:
	if(bIsFlorentinaPresent == false) then
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I could probably drop down right here without hurting myself...)[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Drop\", " .. sDecisionScript .. ", \"DropSolo\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Stop\") ")
		fnCutsceneBlocker()
	
	--If Florentina is present:
	else
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We could probably drop down right here without hurting ourselves...[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Drop\", " .. sDecisionScript .. ", \"DropTeam\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Stop\") ")
		fnCutsceneBlocker()
	
	end

--Mei drops down by herself.
elseif(sObjectName == "DropSolo") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Move.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (39.25 * gciSizePerTile), (13.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Drop instructions.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Fall") ]])
	for i = 1, 30, 1 do
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Teleport To", (39.25 * gciSizePerTile), (13.50 * gciSizePerTile) + (0.10 * i * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneWait(2)
		fnCutsceneBlocker()
	end

--Mei and Florentina drop down.
elseif(sObjectName == "DropTeam") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Move.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (39.25 * gciSizePerTile), (13.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (39.25 * gciSizePerTile), (13.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Drop instructions.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Fall") ]])
	for i = 1, 30, 1 do
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Teleport To", (39.25 * gciSizePerTile), (13.50 * gciSizePerTile) + (0.10 * i * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Teleport To", (39.25 * gciSizePerTile), (13.50 * gciSizePerTile) + (0.10 * i * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneWait(2)
		fnCutsceneBlocker()
	end
	
	--Fold the party positions up.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Do nothing.
elseif(sObjectName == "Stop") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--[Exits]
elseif(sObjectName == "ExitSecret") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseBasementW", "FORCEPOS:26x8")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end