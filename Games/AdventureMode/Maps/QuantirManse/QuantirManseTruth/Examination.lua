--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Empty Journal.
if(sObjectName == "EmptyJournal") then
	fnStandardDialogue([[[VOICE|Mei](A journal.[SOFTBLOCK] No words appear on its pages.) ]])
	
--Empty Crate.
elseif(sObjectName == "EmptyCrate") then
	fnStandardDialogue([[[VOICE|Mei](There is nothing in this crate.[SOFTBLOCK] It is spotless.[SOFTBLOCK] It has never been used.) ]])
	
--Empty Books.
elseif(sObjectName == "EmptyBooks") then
	fnStandardDialogue([[[VOICE|Mei](All of the books are blank.) ]])

--[Big Sequence]
--Countess Quantir...
elseif(sObjectName == "Countess") then
				
	--Topic.
	WD_SetProperty("Unlock Topic", "Countess Quantir", 1)

	--Variables.
	local iSpokeToCountess = VM_GetVar("Root/Variables/Chapter1/Scenes/iSpokeToCountess", "N")
	local iCompletedQuantirMansion = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")
	
	--Event is complete:
	if(iCompletedQuantirMansion == 1.0) then
		
		--Base.
		fnStandardMajorDialogue(true)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] A pool of inert blood. There's nothing else here...") ]])
	
	--Mei has not spoken to the Countess before.
	elseif(iSpokeToCountess == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSpokeToCountess", "N", 1.0)

		--Base.
		fnStandardMajorDialogue(true)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] That's a lot of blood...[SOFTBLOCK] and it's very fresh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Ghost] So you have come.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Who's there?[SOFTBLOCK] Who is it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Ghost] A naive question.[SOFTBLOCK] More appropriately, who was it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Ghost] Once, I was Countess Josephina Quantir.[SOFTBLOCK] This land was mine by birth.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Are you one commanding the ghosts here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] No.[SOFTBLOCK] They are beyond my reach.[SOFTBLOCK] I am here, serving my sentence.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] I have not had a visitor before.[SOFTBLOCK] My jailor will no doubt be here, soon, to send you on your way.[SOFTBLOCK] This is not the business of the living.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Countess Quantir...[SOFTBLOCK] You were looking for a cure.[SOFTBLOCK] Did you find one?[SOFTBLOCK] Did you cure the sickness?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] You have read her diaries, I assume, as you certainly were not alive to see the events.[SOFTBLOCK] Your perception is mangled.[SOFTBLOCK] At no point was I looking for a cure.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] I created the disease.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] Such innocence![SOFTBLOCK] It reminds me of her.[SOFTBLOCK] Before she found out, she was like you.[SOFTBLOCK] Did you idolize me?[SOFTBLOCK] Did you think me a hero?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost][EMOTION|Mei|Neutral] Your answer doesn't matter.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] The people were unwashed, gullible, easily led.[SOFTBLOCK] Their deaths meant nothing to me.[SOFTBLOCK] Ruling them was a burden which kept me from my true passions.[SOFTBLOCK] They were my property, I ought to be able to kill them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] The disease was meant to wipe out the ones who were a burden.[SOFTBLOCK] But she survived.[SOFTBLOCK] She had as much talent as I, perhaps more.[SOFTBLOCK] She found my books and studied without my consent.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] She would have been one of the two left alive once the disease ran its course,[SOFTBLOCK] but when she found out,[SOFTBLOCK] she plunged a butcher's knife into my chest.[SOFTBLOCK] Then, her own.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] The fool had no idea the magical power she had been refining.[SOFTBLOCK] Her anger broke the levies and let it spill forth.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] Each day, she murders me, and herself, again.[SOFTBLOCK] I feel the terror, the lament, the release.[SOFTBLOCK] Doubtless she does.[SOFTBLOCK] It is only fair.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Did you really kill so many people?[SOFTBLOCK] Didn't it matter to you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] Yes, they died by my hand.[SOFTBLOCK] I hid some of the bodies and claimed I had a potential cure, so I could watch the hope glimmer and then fade in their eyes.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] Even now, I don't regret it.[SOFTBLOCK] They were my property, it was my right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] Do you think me a monster?[BLOCK]") ]])
		
		--Decision.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"You Sicken Me\",  " .. sDecisionScript .. ", \"Yes\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I Forgive You\",  " .. sDecisionScript .. ", \"No\") ")
		fnCutsceneBlocker()
	
	--Mei has spoken to the Countess before:
	else
		--Base.
		fnStandardMajorDialogue(true)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Countess...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] You return.[SOFTBLOCK] You linger.[SOFTBLOCK] Will you confront my jailor?[BLOCK]") ]])
		
		--Decision.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Help Her\",  " .. sDecisionScript .. ", \"Help\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"ForgetIt\") ")
		fnCutsceneBlocker()
	end

--You Sicken Me!
elseif(sObjectName == "Yes") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Lady, that is the sickest thing I've ever heard.[SOFTBLOCK] You deserve everything you get and more.[BLOCK][CLEAR]") ]])
	
	--Florentina chimes in.
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Gotta agree.[SOFTBLOCK] I'd join in the daily murder if I could.[BLOCK][CLEAR]") ]])
	end
	
	--Resume.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] I'll not argue the point.[SOFTBLOCK] It doesn't matter.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] So why have you come, if not just to taunt the dead?[SOFTBLOCK] Perhaps you will free this tormented soul?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] After all you've done?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] I have suffered as the wicked ought, but I could suffer more and still not pay my debt.[SOFTBLOCK] I leave it to you to decide.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] I warn you.[SOFTBLOCK] The warden will be most displeased.[BLOCK]") ]])
	
	--Decision.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Help Her\",  " .. sDecisionScript .. ", \"Help\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"ForgetIt\") ")
	fnCutsceneBlocker()
	
--You deserve forgiveness.
elseif(sObjectName == "No") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No matter what terrible crimes you have committed, nobody need suffer.[SOFTBLOCK] Even if you aren't sorry, you still don't deserve this.[BLOCK][CLEAR]") ]])
	
	--Florentina chimes in.
	if(bIsFlorentinaPresent == true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I'm not so sure.[SOFTBLOCK] I'd be giving her a daily perforation too, if I had the chance.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Then again, all things gotta end.[SOFTBLOCK] Maybe it's time for this to...[BLOCK][CLEAR]") ]])
	end
	
	--Resume.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] I'll not argue the point.[SOFTBLOCK] It doesn't matter.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] So why have you come, if not just to taunt the dead?[SOFTBLOCK] Perhaps you will free this tormented soul?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] After all you've done?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] I have suffered as the wicked ought, but I could suffer more and still not pay my debt.[SOFTBLOCK] I leave it to you to decide.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] I warn you.[SOFTBLOCK] The warden will be most displeased.[BLOCK]") ]])
	
	--Decision.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Help Her\",  " .. sDecisionScript .. ", \"Help\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"ForgetIt\") ")
	fnCutsceneBlocker()

--Help her against her jailor:
elseif(sObjectName == "Help") then
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We'll help.[SOFTBLOCK] We'll speak to your jailor.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] There can be no negotiation.[SOFTBLOCK] So lost is she in her rage that I doubt she even understands my pleas.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] You will have to fight her, I am afraid.[SOFTBLOCK] Even now I hear her draw close.[SOFTBLOCK] She comes to resume our ritual.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] Prepare yourselves, mortals.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Okay...[SOFTBLOCK] here we go...") ]])
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0.5, 0, 0, 0, 0.5, 0, 0, 1) ]])
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	--Boss battle!
	fnCutsceneInstruction([[
		AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
		AdvCombat_SetProperty("Reinitialize")
		AdvCombat_SetProperty("Activate")
		AdvCombat_SetProperty("Unretreatable", true)
		LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/900 Boss Arachnophelia.lua", 0)
		AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/QuantirManse_Ending/Combat_Victory.lua")
		AdvCombat_SetProperty("Defeat Script", gsStandardGameOver)
	]])
	fnCutsceneBlocker()

--Let her rot.
elseif(sObjectName == "ForgetIt") then
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You'll stay where you are, for now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] Understandable.[SOFTBLOCK] I'll not ask you to risk yourself on my behalf.[SOFTBLOCK] Nothing would be gained or lost in any case.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Quantir:[VOICE|Ghost] Please leave.[SOFTBLOCK] I would not want you to be caught in her mindless wrath.") ]])
	fnCutsceneBlocker()

--[Exits]
elseif(sObjectName == "ToNWHall") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseNWHall", "FORCEPOS:5x7")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end