--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusArcaneA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "LAYER|EmbassyFunction")
    AL_SetProperty("Mandated Music Intensity", 0.0)
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusArcaneA")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    
    --[Objectives Handler]
    local iReviewedObjectives = VM_GetVar("Root/Variables/Chapter5/Scenes/iReviewedObjectives", "N")
    local iGalaGoal0State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal0State", "N")
    local iGalaGoal1State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N")
    local iGalaGoal2State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal2State", "N")
    local iGalaGoal3State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal3State", "N")
    local iGalaGoal4State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal4State", "N")
    if(iReviewedObjectives == 1.0) then
        
        --Music intensity.
        AL_SetProperty("Mandated Music Intensity", 30.0)
        
        AL_SetProperty("Register Objective", "0: Meet Contact")
        AL_SetProperty("Register Objective", "1: Identify Prime Command Units")
        AL_SetProperty("Register Objective", "2: Identify Security Head")
        AL_SetProperty("Register Objective", "3: Dance with Sophie")
        AL_SetProperty("Register Objective", "4: Leave Party")
        if(iGalaGoal0State == 1.0) then
            AL_SetProperty("Flag Objective True", "0: Meet Contact")
        end
        if(iGalaGoal1State == 1.0) then
            AL_SetProperty("Flag Objective True", "1: Identify Prime Command Units")
        end
        if(iGalaGoal2State == 1.0) then
            AL_SetProperty("Flag Objective True", "2: Identify Security Head")
        end
        if(iGalaGoal3State == 1.0) then
            AL_SetProperty("Flag Objective True", "3: Dance with Sophie")
        end
        if(iGalaGoal4State == 1.0) then
            AL_SetProperty("Flag Objective True", "4: Leave Party")
        end
    end
end
