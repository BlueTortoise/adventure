--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "Terminal") then
    
    --Variables.
    local iReviewedObjectives = VM_GetVar("Root/Variables/Chapter5/Scenes/iReviewedObjectives", "N")
    if(iReviewedObjectives == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A list of arrival times.[SOFTBLOCK] Nothing interesting.)") ]])
        fnCutsceneBlocker()
    
    --Review objectives.
    else
    
        --Variables.
        local iGalaGoal0State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal0State", "N")
        local iGalaGoal1State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N")
        local iGalaGoal2State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal2State", "N")
        local iGalaGoal3State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal3State", "N")
        local iGalaGoal4State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal4State", "N")

        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Let's go over the plan...[BLOCK][CLEAR]") ]])
        if(iGalaGoal0State == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 0::[SOFTBLOCK] Link up with our mole.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Our mole is a Slave Unit assigned to the cleanup crew.[SOFTBLOCK] There can't be too many.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] How do we identify her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I've got a code phrase.[SOFTBLOCK] Don't worry.[BLOCK][CLEAR]") ]])
        end
        if(iGalaGoal1State == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 1::[SOFTBLOCK] Identify the three Prime Command Units.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Blue Leader specified that she knows there will be three of the Prime Command Units here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But I guess she didn't know exactly who they were.[SOFTBLOCK] Once you leave the party, you can relay that information to the other team leaders.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It shouldn't be hard to figure out who they are.[SOFTBLOCK] I'll just talk to them and introduce myself.[BLOCK][CLEAR]") ]])
        end
        if(iGalaGoal2State == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 2::[SOFTBLOCK] Identify the local head of security.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Someone, probably a Lord Unit, will be in charge of the security units posted in the building.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But she'll probably be undercover...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yeah.[SOFTBLOCK] We'll have to come up with some way to reveal her...[BLOCK][CLEAR]") ]])
        end
        if(iGalaGoal3State == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 3::[SOFTBLOCK] Dance with Sophie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] B-b-b-but![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It'll be fun![SOFTBLOCK] I promise![BLOCK][CLEAR]") ]])
        end
        if(iGalaGoal4State == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 4::[SOFTBLOCK] Leave the Party...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The security units won't want us wandering around.[SOFTBLOCK] We may need to make a distraction.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hopefully our mole will have an idea.[SOFTBLOCK] I'd really hate to have to fight well-dressed Lord Units.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Maybe we can request a place to go be alone together...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's a pretty good idea![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Yeah...[SOFTBLOCK] I'd make a great spy...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] As long as it involves making out with you...[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] After that, it's all down to me.[SOFTBLOCK] You'll have to leave the party via the trams.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Go hole up in the repair bay and wait for us to rendezvous.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh.[SOFTBLOCK] Simple.[SOFTBLOCK] Nothing will go wrong.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Of course.[SOFTBLOCK] Let's go.") ]])
        fnCutsceneBlocker()   
    end
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end