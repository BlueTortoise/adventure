--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Sample
if(sObjectName == "Silence") then
    AL_SetProperty("Mandated Music Intensity", 20.0)

elseif(sObjectName == "TriggerN") then
    
    --Silence.
    AL_SetProperty("Mandated Music Intensity", 20.0)
    
    --Don't double-play.
    local iReviewedObjectives = VM_GetVar("Root/Variables/Chapter5/Scenes/iReviewedObjectives", "N")
    if(iReviewedObjectives == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iReviewedObjectives", "N", 1.0)
    
    --Scene.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] All right, let's get down to business.[SOFTBLOCK] PDU, list objectives.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Objectives?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] There are some things that I need to do at the party before sunrise.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Rub elbows, confirm identities...[SOFTBLOCK] you know...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Spy stuff?*[SOFTBLOCK] Oh this is making me hot![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Is there any way I can help?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Follow my lead, play along, and act like nothing unusual is happening.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 0::[SOFTBLOCK] Link up with our mole.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We've got a sympathizer who was assigned to cleaning duty.[SOFTBLOCK] She can probably help us accomplish the rest of the objectives.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 1::[SOFTBLOCK] Identify the three Prime Command Units.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We know there are three of them here, but we don't know what they're in charge of.[SOFTBLOCK] This will be useful information to know.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 2::[SOFTBLOCK] Find out who is in charge of local security.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It will be a plainclothes operative, most likely.[SOFTBLOCK] We'll need to figure out who it is without asking directly.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'm sure someone will let it slip.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 3::[SOFTBLOCK] Dance with Sophie.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] I -[SOFTBLOCK] I don't know how to dance![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I said to follow my lead, didn't I?[SOFTBLOCK] It'll be easy, I promise.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Objective 4::[SOFTBLOCK] Find a reason to leave the party at the correct time.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] The security units probably won't want us wandering around.[SOFTBLOCK] We'll need a good reason.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hmmm....[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We need to complete those objectives.[SOFTBLOCK] If you forget one, just come down here and we can review them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Just check the terminal over there and I'll make it look like we're checking arrival times.") ]])
    
    --Display the objectives on screen.
    AL_SetProperty("Register Objective", "0: Meet Contact")
    AL_SetProperty("Register Objective", "1: Identify Prime Command Units")
    AL_SetProperty("Register Objective", "2: Identify Security Head")
    AL_SetProperty("Register Objective", "3: Dance with Sophie")
    AL_SetProperty("Register Objective", "4: Leave Party")
    
elseif(sObjectName == "TriggerS") then
    AL_SetProperty("Mandated Music Intensity", 30.0)
    
elseif(sObjectName == "NoExitTrigger") then
end
