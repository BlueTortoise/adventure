--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Music volumes.
if(sObjectName == "LouderPiano") then
    AL_SetProperty("Mandated Music Intensity", 70.0)
    
elseif(sObjectName == "SofterPiano") then
    AL_SetProperty("Mandated Music Intensity", 60.0)
    
elseif(sObjectName == "SneakTheme") then
    AL_SetProperty("Mandated Music Intensity", 40.0)

--Cutscenes.
elseif(sObjectName == "TriggerBump") then

    --Variables.
    local iBumpedIntoLatexDrone = VM_GetVar("Root/Variables/Chapter5/Scenes/iBumpedIntoLatexDrone", "N")
    local iGalaInstalledBug     = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaInstalledBug", "N")
    
    --Run the scene.
    if(iBumpedIntoLatexDrone == 0.0 and iGalaInstalledBug == 1.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iBumpedIntoLatexDrone", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Sophie, stand right here.*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Like this?*") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 21.25, 13.50)
        fnCutsceneMove("Sophie", 20.25, 13.50)
        fnCutsceneMove("Sophie", 20.25, 12.50)
        fnCutsceneBlocker()
        fnCutsceneFace("Sophie", 0, 1)  
        fnCutsceneFace("Christine", 0, -1) 
        fnCutsceneFace("GolemGalaF", 0, 1) 
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Security unit, are you due for a recharge?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] THIS UNIT'S POWER CORE READOUT IS STILL GREEN.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You stupid drone![SOFTBLOCK] You're reading it upside down![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] THE INDICATOR IS INTERNAL AND DOES NOT HAVE AN EXTERNAL READOUT.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Don't talk back to me, drone![SOFTBLOCK] Recharge immediately, before you black out![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] ...[SOFTBLOCK] AFFIRMATIVE, LORD UNIT.[SOFTBLOCK] THIS UNIT NEED A RECHARGE.[SOFTBLOCK] MOVING TO RECHARGE STATION.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Drone bumps into Sophie.
        fnCutsceneMoveFace("GolemGalaF", 20.75, 12.50, 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneMove("GolemGalaF", 19.75, 12.50)
        fnCutsceneMoveFace("Sophie", 18.75, 12.50, 0, 1)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] APOLOGIES, LORD UNIT.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, it's all right...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] DRONE UNIT, YOU HAVE JUST RUN INTO MY TANDEM UNIT![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] I'll -[SOFTBLOCK] I'll have you scrapped for this misbehavior![SOFTBLOCK] This is unthinkable.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Uhh, yeah![SOFTBLOCK] Bad drone![SOFTBLOCK] Bad![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] WOULD YOU LIKE TO LODGE A COMPLAINT WITH MY SUPERIOR OFFICER, LORD UNIT?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You're darn right I would![SOFTBLOCK] Wait here while I compose a hardcopy to show just how upset I am![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I feel really bad for this poor drone, but this is too good a plan to give up...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I just need to find some stationery supplies now...)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move the drone back.
        fnCutsceneMove("Christine", 20.25, 13.50)
        fnCutsceneMove("Sophie", 20.25, 13.50)
        fnCutsceneMove("GolemGalaF", 21.25, 12.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
        
    end

--Post-dance scene.
elseif(sObjectName == "PostDance") then

    --Variables.
    local iDancedWithSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iDancedWithSophie", "N")
    local iSawPostDance     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPostDance", "N")
    if(iDancedWithSophie == 1.0 and iSawPostDance == 0.0) then

        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal3State", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPostDance", "N", 1.0)
        AL_SetProperty("Flag Objective True", "3: Dance with Sophie")
        
        --Fade to black.
        AL_SetProperty("Music", "Null")
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --Position.
        fnCutsceneTeleport("Christine", 14.75, 23.50)
        fnCutsceneTeleport("Sophie", 15.75, 23.50)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneFace("Sophie", -1, 0)
        fnCutsceneBlocker()
        
        --Fade in.
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Ahaha, I've never danced before![SOFTBLOCK] That was so fun![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I told you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I never would have done it if you hadn't made me.[SOFTBLOCK] Thanks![SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *But I didn't see anyone in the crowd, and time is almost up.[SOFTBLOCK] I need to be leaving the party soon.*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *I didn't see anyone I'd call a Prime, either.[SOFTBLOCK] Perhaps we should talk to your contact.*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Good idea, tandem unit.*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *But if you want to do some more dancing...*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Time is of the essence...[SOFTBLOCK] but, don't tempt me![SOFTBLOCK] I might have to!*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Hee hee![SOFTBLOCK] Let's go talk to our friend.*") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Restart the music.
        fnCutsceneInstruction([[ AL_SetProperty("Music", "LAYER|EmbassyFunction") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 50.0) ]])
        fnCutsceneBlocker()
        
        --Fold the party.
        fnCutsceneMove("Christine", 14.75, 24.50)
        fnCutsceneMove("Sophie", 15.75, 24.50)
        fnCutsceneMove("Sophie", 14.75, 24.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    end
end
