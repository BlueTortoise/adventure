--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusArcaneB"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "LAYER|EmbassyFunction")
    AL_SetProperty("Mandated Music Intensity", 90.0)
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusArcaneB")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("GolemGala",   "A", "N")
    fnSpawnNPCPattern("CommandGala", "A", "C")
    
    --Spawn this entity if the flag is set.
    local iSentComplaint = VM_GetVar("Root/Variables/Chapter5/Scenes/iSentComplaint", "N")
    if(iSentComplaint == 1.0) then
        fnStandardNPCByPosition("DroneSpecial")
        fnCutsceneFace("GolemGalaH", -1, 0)
    end
    
    --[Randomized NPCs]
    --Spawn all the NPCs that have randomized outfits and no dialogue.
    for i = 0, 34, 1 do
        
        --Generate name.
        local sName = "GenGolem"
        if(i < 10) then sName = sName .. "0" end
        sName = sName .. i
        
        --Spawn NPC. Randomize the appearance slightly.
        TA_CreateUsingPosition(sName, sName)
        
            --80% chance to be a fancy golem:
            local iRoll = LM_GetRandomNumber(0, 99)
            if(iRoll < 80) then
                local sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 23))
                fnSetCharacterGraphics("Root/Images/Sprites/GolemFancy" .. sRandomLetter .. "/", false)
            
            --Remaining roll is for a normal lord outfit.
            else
                local sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 3))
                fnSetCharacterGraphics("Root/Images/Sprites/GolemLord" .. sRandomLetter .. "/", false)
                
            end
        
            TA_SetProperty("Shadow", gsStandardShadow)
        DL_PopActiveObject()
    end
    
    --[Dancer Script]
    --Spawn the dancers. They are deliberately unclipped. They also use randomized sprites.
    TA_CreateUsingPosition("DancerAA", "DancerAA")
        local sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 23))
        fnSetCharacterGraphics("Root/Images/Sprites/GolemFancy" .. sRandomLetter .. "/", false)
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    TA_CreateUsingPosition("DancerAB", "DancerAB")
        sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 23))
        fnSetCharacterGraphics("Root/Images/Sprites/GolemFancy" .. sRandomLetter .. "/", false)
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    TA_CreateUsingPosition("DancerBA", "DancerBA")
        sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 23))
        fnSetCharacterGraphics("Root/Images/Sprites/GolemFancy" .. sRandomLetter .. "/", false)
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    TA_CreateUsingPosition("DancerBB", "DancerBB")
        sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 23))
        fnSetCharacterGraphics("Root/Images/Sprites/GolemFancy" .. sRandomLetter .. "/", false)
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    TA_CreateUsingPosition("DancerCA", "DancerCA")
        sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 23))
        fnSetCharacterGraphics("Root/Images/Sprites/GolemFancy" .. sRandomLetter .. "/", false)
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    TA_CreateUsingPosition("DancerCB", "DancerCB")
        sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 23))
        fnSetCharacterGraphics("Root/Images/Sprites/GolemFancy" .. sRandomLetter .. "/", false)
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    
    --Create a parallel script to handle the dancers in the middle of the room.
    Cutscene_HandleParallel("Create", "DancerScript", gsRoot .. "Maps/RegulusArcane/RegulusArcaneC/DancerScript.lua")
    
    --[Objectives Handler]
    local iReviewedObjectives = VM_GetVar("Root/Variables/Chapter5/Scenes/iReviewedObjectives", "N")
    local iGalaGoal0State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal0State", "N")
    local iGalaGoal1State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N")
    local iGalaGoal2State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal2State", "N")
    local iGalaGoal3State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal3State", "N")
    local iGalaGoal4State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal4State", "N")
    if(iReviewedObjectives == 1.0) then
        
        AL_SetProperty("Register Objective", "0: Meet Contact")
        AL_SetProperty("Register Objective", "1: Identify Prime Command Units")
        AL_SetProperty("Register Objective", "2: Identify Security Head")
        AL_SetProperty("Register Objective", "3: Dance with Sophie")
        AL_SetProperty("Register Objective", "4: Leave Party")
        if(iGalaGoal0State == 1.0) then
            AL_SetProperty("Flag Objective True", "0: Meet Contact")
        end
        if(iGalaGoal1State == 1.0) then
            AL_SetProperty("Flag Objective True", "1: Identify Prime Command Units")
        end
        if(iGalaGoal2State == 1.0) then
            AL_SetProperty("Flag Objective True", "2: Identify Security Head")
        end
        if(iGalaGoal3State == 1.0) then
            AL_SetProperty("Flag Objective True", "3: Dance with Sophie")
        end
        if(iGalaGoal4State == 1.0) then
            AL_SetProperty("Flag Objective True", "4: Leave Party")
        end
    end
end
