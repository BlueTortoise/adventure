--[Dancer Script]
--Script that executes in parallel to the regular scripts. Does not block player input.

--Script Usage:
--This script controls the dancers dancing about in the middle of the room. It always re-executes.

--Variables.
local fDancerAX = 23.25
local fDancerAY = 18.50
local fDancerBX = 26.25
local fDancerBY = 18.50
local fDancerCX = 25.25
local fDancerCY = 16.50
local fDancerSpeed = 1.00
local iWaitTicks = 30
local iReducedWaitTicks = iWaitTicks - 10

--First move.
fnCutsceneMoveFace("DancerAA", fDancerAX + 1.00, fDancerAY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerAB", fDancerAX + 2.00, fDancerAY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBA", fDancerBX + 1.00, fDancerBY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBB", fDancerBX + 2.00, fDancerBY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCA", fDancerCX + 1.00, fDancerCY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCB", fDancerCX + 2.00, fDancerCY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneBlocker()
fnCutsceneWait(iWaitTicks)
fnCutsceneBlocker()

--Second move.
fnCutsceneMoveFace("DancerAA", fDancerAX + 0.00, fDancerAY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerAB", fDancerAX + 1.00, fDancerAY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBA", fDancerBX + 0.00, fDancerBY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBB", fDancerBX + 1.00, fDancerBY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCA", fDancerCX + 0.00, fDancerCY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCB", fDancerCX + 1.00, fDancerCY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneBlocker()
fnCutsceneWait(iWaitTicks)
fnCutsceneBlocker()

--Third move. Diagonals.
fnCutsceneFace("DancerAA", 0, 1)
fnCutsceneMoveFace("DancerAB", fDancerAX + 0.00, fDancerAY + 1.00, 0, -1, fDancerSpeed)
fnCutsceneFace("DancerBA", 0, 1)
fnCutsceneMoveFace("DancerBB", fDancerBX + 0.00, fDancerBY + 1.00, 0, -1, fDancerSpeed)
fnCutsceneFace("DancerCA", 0, 1)
fnCutsceneMoveFace("DancerCB", fDancerCX + 0.00, fDancerCY + 1.00, 0, -1, fDancerSpeed)
fnCutsceneBlocker()
fnCutsceneWait(iReducedWaitTicks)
fnCutsceneBlocker()

--Fourth move. Place swap.
fnCutsceneMoveFace("DancerAA", fDancerAX + 1.00, fDancerAY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerAB", fDancerAX + 0.00, fDancerAY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBA", fDancerBX + 1.00, fDancerBY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBB", fDancerBX + 0.00, fDancerBY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCA", fDancerCX + 1.00, fDancerCY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCB", fDancerCX + 0.00, fDancerCY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneBlocker()
fnCutsceneWait(iWaitTicks)
fnCutsceneBlocker()

--Fifth move.
fnCutsceneMoveFace("DancerAB", fDancerAX + 1.00, fDancerAY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerAA", fDancerAX + 2.00, fDancerAY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBB", fDancerBX + 1.00, fDancerBY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBA", fDancerBX + 2.00, fDancerBY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCB", fDancerCX + 1.00, fDancerCY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCA", fDancerCX + 2.00, fDancerCY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneBlocker()
fnCutsceneWait(iWaitTicks)
fnCutsceneBlocker()

--Sixth move.
fnCutsceneMoveFace("DancerAB", fDancerAX + 0.00, fDancerAY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerAA", fDancerAX + 1.00, fDancerAY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBB", fDancerBX + 0.00, fDancerBY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBA", fDancerBX + 1.00, fDancerBY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCB", fDancerCX + 0.00, fDancerCY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCA", fDancerCX + 1.00, fDancerCY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneBlocker()
fnCutsceneWait(iWaitTicks)
fnCutsceneBlocker()

--Seventh move. Diagonals.
fnCutsceneFace("DancerAB", 0, 1)
fnCutsceneMoveFace("DancerAA", fDancerAX + 0.00, fDancerAY + 1.00, 0, -1, fDancerSpeed)
fnCutsceneFace("DancerBB", 0, 1)
fnCutsceneMoveFace("DancerBA", fDancerBX + 0.00, fDancerBY + 1.00, 0, -1, fDancerSpeed)
fnCutsceneFace("DancerCB", 0, 1)
fnCutsceneMoveFace("DancerCA", fDancerCX + 0.00, fDancerCY + 1.00, 0, -1, fDancerSpeed)
fnCutsceneBlocker()
fnCutsceneWait(iReducedWaitTicks)
fnCutsceneBlocker()

--Eight move. Place swap. Returns to original configuration.
fnCutsceneMoveFace("DancerAB", fDancerAX + 1.00, fDancerAY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerAA", fDancerAX + 0.00, fDancerAY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBB", fDancerBX + 1.00, fDancerBY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerBA", fDancerBX + 0.00, fDancerBY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCB", fDancerCX + 1.00, fDancerCY + 0.00, -1, 0, fDancerSpeed)
fnCutsceneMoveFace("DancerCA", fDancerCX + 0.00, fDancerCY + 0.00,  1, 0, fDancerSpeed)
fnCutsceneBlocker()
fnCutsceneWait(iWaitTicks)
fnCutsceneBlocker()