--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "Piano") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A classically designed piano, set with motivators to play music according to preset sheet music.[SOFTBLOCK] Far less efficient than a simple speaker, but so much more elegant!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh my, organic shrimp?[SOFTBLOCK] I've never had one...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you sure about this?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Num*[SOFTBLOCK][SOFTBLOCK][EMOTION|Sophie|Surprised] S-s-s[SOFTBLOCK]-s-s-s[SOFTBLOCK]SPICY!!![SOFTBLOCK] HOT!!![SOFTBLOCK] HOT HOT HOTTTTT!!!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Are you okay?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Organic food is the best![SOFTBLOCK] Oh, I want another![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] But you just said it was really hot...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Oh my oral intake is on fire, but mmm -[SOFTBLOCK] spicy![SOFTBLOCK] I love it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Num*[SOFTBLOCK][SOFTBLOCK][EMOTION|Sophie|Surprised] YOWCH HOT HOT HOT![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Another![SOFTBLOCK] Another![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Num*[SOFTBLOCK][SOFTBLOCK][EMOTION|Sophie|Surprised] I'M GOING TO MELT MY POWER CORE!!!![SOFTBLOCK] AIIEEEE!!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Okay I better stop you there before you liquify![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Here, try one?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No thanks, I'm good![SOFTBLOCK] Let's go!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Organic spinach leaves from the habitat, and chipmunk meat rolls on a rubber bun! Yum!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Look, Christine![SOFTBLOCK] Human Milk Cheese![SOFTBLOCK] Aged two years![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] They make cheese from human milk?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh yes.[SOFTBLOCK] Human females produce milk, but it usually gets allocated to rations or fertilizer.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] But [SOFTBLOCK]*Num*[SOFTBLOCK] Ohh, it's so creamy![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Something about this seems a little off...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] You don't want it?[SOFTBLOCK] More for me![SOFTBLOCK] *Num*") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Synthoney drizzled on top of copper wire...[SOFTBLOCK] Mmmm...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Woah![SOFTBLOCK] Sweet![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] It [SOFTBLOCK]*is*[SOFTBLOCK] pure sugar.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I remember reading in a manual somewhere that sweetness is one of the few things our oral intakes measure the same way a human's does.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I don't mind![SOFTBLOCK] Mmm![SOFTBLOCK] *Crunch*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] This is the best Sunrise Gala ever![SOFTBLOCK] *Num*[SOFTBLOCK] *Num*!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Salad with vinegar?[SOFTBLOCK] And what's this?[SOFTBLOCK] *Num*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Cry] No, Sophie, you don't eat the pot plant![SOFTBLOCK] It's there for decoration![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] But it tastes so good![SOFTBLOCK] *Num*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Cry] (I'm going to shutdown out of sheer embarassment!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oooh, tripe![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Do you like it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] *Num*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] ...[SOFTBLOCK] No I do not...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Okay, on to the next table!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This table is empty.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetH") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This table is empty.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "KitchenDoor") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This leads to the kitchen, but it's off limits for the party.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Sky") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The unmistakable glow of the sun lurks just below the horizon.[SOFTBLOCK] It won't be long now.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end