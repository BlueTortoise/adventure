--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusArcaneCAlt"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "Null")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusArcaneCAlt")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("CommandGala", "A", "B")
    fnStandardNPCByPosition("Influenced")
    
    --[Randomized NPCs]
    --Spawn all the NPCs that have randomized outfits and no dialogue.
    for i = 0, 19, 1 do
        
        --Generate name.
        local sName = "GenGolem"
        if(i < 10) then sName = sName .. "0" end
        sName = sName .. i
        
        --Spawn NPC. Randomize the appearance slightly.
        TA_CreateUsingPosition(sName, sName)
        
            --80% chance to be a fancy golem:
            local iRoll = LM_GetRandomNumber(0, 99)
            if(iRoll < 80) then
                local sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 23))
                fnSetCharacterGraphics("Root/Images/Sprites/GolemFancy" .. sRandomLetter .. "/", false)
            
            --Remaining roll is for a normal lord outfit.
            else
                local sRandomLetter = string.char(string.byte("A") + LM_GetRandomNumber(0, 3))
                fnSetCharacterGraphics("Root/Images/Sprites/GolemLord" .. sRandomLetter .. "/", false)
                
            end
        
            TA_SetProperty("Shadow", gsStandardShadow)
        DL_PopActiveObject()
    end
    
    --[Layer Disable]
    AL_SetProperty("Set Layer Disabled", "FloorExpl0",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl1",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl2",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl3",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl4",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl5",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl6",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl7",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl8",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl9",  true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl10", true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl11", true)
    AL_SetProperty("Set Layer Disabled", "FloorExpl12", true)
end
