--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Volume handlers.
if(sObjectName == "SceneTrigger") then
    
    --Variables.
    local iStartInvasion = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartInvasion", "N")
    if(iStartInvasion == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iStartInvasion", "N", 1.0)
    
    --Call.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Scenes/FloorBurster.lua")

--Go back.
elseif(sObjectName == "NoLeave") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It goes the path.[SOFTBLOCK] Other is east.[SOFTBLOCK] It will go east.)") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 25.25, 23.50)

--Sophie's cutscene. Only plays if the party is defeated by Eldritch Christine.
elseif(sObjectName == "SophieScene") then

    --Variables.
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")

    --Movement.
    fnCutsceneMove("Christine", 45.25, 23.50)
    fnCutsceneMove("Christine", 45.25, 22.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Unset collision flag.
    EM_PushEntity("SX399")
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Sad") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (It has found the other.[SOFTBLOCK] It will take the other.[SOFTBLOCK] It will...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Please let her be safe, please let her be safe...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Christine![SOFTBLOCK] You're okay![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Oh, you don't have to explain anything.[SOFTBLOCK] I know you can transform yourself, 55 said - [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Well it doesn't matter.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] I don't like tentacles, though.[SOFTBLOCK] Didn't I mention that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But I guess you must have had a reason.[SOFTBLOCK] You look pretty darn intimidating like that![SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Christine?[SOFTBLOCK] Why aren't you saying anything?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oops![SOFTBLOCK] Hee hee![SOFTBLOCK] You know how I rant when I get nerv -[SOFTBLOCK] nerv...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] You're starting to freak me out, Christine...[SOFTBLOCK] Hee...[SOFTBLOCK] Hee...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm so...[SOFTBLOCK] Happy...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's only one side, and it twists and twists around us.[SOFTBLOCK] Do you see it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] See what?[SOFTBLOCK] Do your weird eyes let you see gamma rays or something?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It wraps around the center.[SOFTBLOCK] It emerges from the center.[SOFTBLOCK] One eye.[SOFTBLOCK] One brain.[SOFTBLOCK] A billion eyes![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Christine...[SOFTBLOCK] You're starting to scare me![SOFTBLOCK] Stop it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I will.[SOFTBLOCK] Show.[SOFTBLOCK] You.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] G-[SOFTBLOCK]get away from me![SOFTBLOCK] Get back![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Show.[SOFTBLOCK] You.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Show.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] D-[SOFTBLOCK]don't touch me![SOFTBLOCK] Help![SOFTBLOCK] Someone help![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (It is upset.[SOFTBLOCK] Other was upset by it.[SOFTBLOCK] It...[SOFTBLOCK] It...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Not?[SOFTBLOCK] Show?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Stop it Christine![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] *Slap*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Don't touch me![SOFTBLOCK] Don't![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Make you whole...[SOFTBLOCK] Make you empty...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I want my Christine back![SOFTBLOCK] Give her back you dumb ugly thing![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] *Sniff*[SOFTBLOCK] Give her back...[SOFTBLOCK] She was my everything...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] How could you take her away from me...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Empty...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] *Sniff*...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It is...[SOFTBLOCK] It is...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Soooo -[SOFTBLOCK] feeeeeeee.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] How could you...[SOFTBLOCK] I loved her...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] T-[SOFTBLOCK]touch it.[SOFTBLOCK] Touch it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] (Hmm?[SOFTBLOCK] What did she just say?)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Touch.[SOFTBLOCK] Touch.[SOFTBLOCK] Touchtouchtouchtouchtouch!!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] T-[SOFTBLOCK]taff it![SOFTBLOCK] Taff it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Smooch*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] R-[SOFTBLOCK]right on your gross slimy lips -[SOFTBLOCK] Taff me you're so nasty![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Smooch*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Again![SOFTBLOCK] Again![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Smooch*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Christine I don't know how much longer I can keep this up![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] [MUSIC|Null]How about,[SOFTBLOCK][SOFTBLOCK] for the rest of our synthetic lives?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Did it -[SOFTBLOCK] did it do something?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Christine?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] [MUSIC|SophiesThemeSlow]Sophie...[SOFTBLOCK] I am awake...[SOFTBLOCK] again...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] My mind is still cloudy, but, I think getting necked woke me up.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Christine![SOFTBLOCK] Oh thank good -[SOFTBLOCK][E|Surprised]nope![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Tentacles are gross![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sorry, I didn't mean to upset you.[SOFTBLOCK] But 201890 did this...[SOFTBLOCK] and...[SOFTBLOCK] I lost control...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, I gave 55 a thrashing...[SOFTBLOCK] Hopefully she's as forgiving as you are.[SOFTBLOCK] Come on, I better go apologize!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Entourage leaves.
    fnCutsceneFace("Christine", 1, 1)
    fnCutsceneFace("Sophie", 1, 1)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneFace("SX399", -1, 0)
    end
    fnCutsceneSetFrame("Sophie", "Null")
    fnCutsceneSetFrame("55", "Downed")
    fnCutsceneBlocker()
    fnCutsceneMove("EntourageA", 48.25, 25.50)
    fnCutsceneMove("EntourageA", 48.25, 16.50)
    fnCutsceneMove("EntourageB", 48.25, 25.50)
    fnCutsceneMove("EntourageB", 48.25, 16.50)
    fnCutsceneMove("EntourageC", 48.25, 16.50)
    fnCutsceneTeleport("EntourageA", -100.25, -100.50)
    fnCutsceneTeleport("EntourageB", -100.25, -100.50)
    fnCutsceneTeleport("EntourageC", -100.25, -100.50)
    fnCutsceneBlocker()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Sad") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] [MUSIC|Null]Did those things just... leave?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes.[SOFTBLOCK] Because she just called.[SOFTBLOCK] She's calling.[SOFTBLOCK] Follow me.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Run.
    fnCutsceneMove("Christine", 44.25, 23.50, 1.70)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 44.25, 25.50, 1.70)
    fnCutsceneMove("Christine", 48.25, 25.50, 1.70)
    fnCutsceneMove("Christine", 48.25, 17.50, 1.70)
    fnCutsceneMove("Christine", 34.25, 17.50, 1.70)
    fnCutsceneMove("Christine", 34.25,  7.50, 1.70)
    fnCutsceneMove("Christine", 28.25,  7.50, 1.70)
    fnCutsceneMove("Sophie", 44.25, 25.50, 1.70)
    fnCutsceneMove("Sophie", 48.25, 25.50, 1.70)
    fnCutsceneMove("Sophie", 48.25, 17.50, 1.70)
    fnCutsceneMove("Sophie", 34.25, 17.50, 1.70)
    fnCutsceneMove("Sophie", 34.25,  7.50, 1.70)
    fnCutsceneMove("Sophie", 29.25,  7.50, 1.70)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Null")
    fnCutsceneFace("SX399", 1, 0)
    fnCutsceneFace("55", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --SX-399 is present.
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Sophie...[SOFTBLOCK] I'm hit...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] G-[SOFTBLOCK]Get away from Christine![SOFTBLOCK] Quickly...[SOFTBLOCK] ow...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, no, it's okay.[SOFTBLOCK] I'm fine now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Should I tell them that girl-on-girl lovin' saved me?[SOFTBLOCK] Would 55 believe it?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you two all right?[SOFTBLOCK] Sorry for that...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am injured...[SOFTBLOCK] Auto-repair is activated...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I'm all right, but I need to regenerate a bit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oh good, the ChocoBromine nanites worked![SOFTBLOCK] That was something of an experiment.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I should have followed up, but you never contacted me to say anything was wrong.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Chocolate Bromine?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It's common practice to bake potions into cakes or sweets on the surface of Pandemonium.[SOFTBLOCK] Nobody knows why it amplifies the effect, but it does.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] So I mixed SX-399's auto-repair nanites with chocolate and bromine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It's worked before...[SOFTBLOCK] But she's not a milking machine, so I wasn't sure if it would scale up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Thanks, love.[SOFTBLOCK] We should be all right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Are you going to explain what happened, you big palooka?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] When I got close to 201890, she showed me a vision.[SOFTBLOCK] A vision of the future.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And I don't know how I know, but it was real.[SOFTBLOCK] It was true.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What...[SOFTBLOCK] In the future, what has happened?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All of Regulus is...[SOFTBLOCK] part of some giant...[SOFTBLOCK] thing.[SOFTBLOCK] It's organic, I think.[SOFTBLOCK] It's made of meat, forever bleeding.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's covered in eggs, but they're all stillborn.[SOFTBLOCK] Fleshy tendrils the size of a skyscraper lay limp, and dead.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dead like this body.[SOFTBLOCK] It's just like what we saw in the basement.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Then...[SOFTBLOCK] Do we fail?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't know if we can change it, but...[SOFTBLOCK] When I came back here, I was...[SOFTBLOCK] this.[SOFTBLOCK] I was listening to her song.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Vivify is in the biolabs somewhere, she's singing.[SOFTBLOCK] She's telling us all to come back to her.[SOFTBLOCK] All of her followers.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I can hear her voice echo off all the new ones she's making.[SOFTBLOCK] We -[SOFTBLOCK] we have to stop her...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will message my sister.[SOFTBLOCK] I will tell her to meet up with us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay.[SOFTBLOCK] Okay.[SOFTBLOCK] We can do this.[SOFTBLOCK] Follow me.") ]])
    
    --No SX-399.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] System...[SOFTBLOCK] reinitializing...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 499323, move away from the hostile entity...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, no, it's okay.[SOFTBLOCK] I'm fine now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Should I tell them that girl-on-girl lovin' saved me?[SOFTBLOCK] Would she believe it?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you all right?[SOFTBLOCK] I shouldn't have hurt you...[SOFTBLOCK] I'm sorry...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Auto-repair is activated.[SOFTBLOCK] My systems will be functional again momentarily.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852.[SOFTBLOCK] Explain.[SOFTBLOCK] Cause of behavior.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] When I got close to 201890, she showed me a vision.[SOFTBLOCK] A vision of the future.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And I don't know how I know, but it was real.[SOFTBLOCK] It was true.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The future?[SOFTBLOCK] What has happened in the future?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All of Regulus is...[SOFTBLOCK] part of some giant...[SOFTBLOCK] thing.[SOFTBLOCK] It's organic, I think.[SOFTBLOCK] It's made of meat, forever bleeding.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's covered in eggs, but they're all stillborn.[SOFTBLOCK] Fleshy tendrils the size of a skyscraper lay limp, and dead.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dead like this body.[SOFTBLOCK] It's just like what we saw in the basement.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Does that mean we fail?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't know if we can change it, but...[SOFTBLOCK] When I came back here, I was...[SOFTBLOCK] this.[SOFTBLOCK] I was listening to her song.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Vivify is in the biolabs somewhere, she's singing.[SOFTBLOCK] She's telling us all to come back to her.[SOFTBLOCK] All of her followers.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I can hear her voice echo off all the new ones she's making.[SOFTBLOCK] We -[SOFTBLOCK] we have to stop her...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will message my sister.[SOFTBLOCK] I will tell her to meet up with us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay.[SOFTBLOCK] Okay.[SOFTBLOCK] We can do this.[SOFTBLOCK] Follow me.") ]])
    
    
    end
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Modify party setup. Christine is back in, and Sophie is now following.
    if(iSX399JoinsParty == 1.0) then
        
        --Party lineup.
        AC_SetProperty("Set Party", 0, "Christine")
        AC_SetProperty("Set Party", 1, "55")
        AC_SetProperty("Set Party", 2, "SX-399")

        --IDs.
        EM_PushEntity("55")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        EM_PushEntity("SX399")
            local iSX399ID = RE_GetID()
        DL_PopActiveObject()
        EM_PushEntity("Sophie")
            local iSophieID = RE_GetID()
        DL_PopActiveObject()

        --Store names and IDs.
        gsFollowersTotal = 3
        gsaFollowerNames[1] = "55"
        gsaFollowerNames[2] = "SX399"
        gsaFollowerNames[3] = "Sophie"
        giaFollowerIDs[0] = i55ID
        giaFollowerIDs[1] = iSX399ID
        giaFollowerIDs[2] = iSophieID

        --Tell everyone to follow.
        AL_SetProperty("Unfollow Actor Name", "55")
        AL_SetProperty("Unfollow Actor Name", "SX399")
        AL_SetProperty("Unfollow Actor Name", "Sophie")
        AL_SetProperty("Follow Actor ID", i55ID)
        AL_SetProperty("Follow Actor ID", iSX399ID)
        AL_SetProperty("Follow Actor ID", iSophieID)
        
        --Remove SX-399's dialogue script.
        EM_PushEntity("SX399")
            TA_SetProperty("Activation Script", "NULL")
        DL_PopActiveObject()
    
    --SX-399 not joining.
    else
        
        --Party lineup.
        AC_SetProperty("Set Party", 0, "Christine")
        AC_SetProperty("Set Party", 1, "55")

        --IDs.
        EM_PushEntity("55")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        EM_PushEntity("Sophie")
            local iSophieID = RE_GetID()
        DL_PopActiveObject()

        --Store names and IDs.
        gsFollowersTotal = 2
        gsaFollowerNames[1] = "55"
        gsaFollowerNames[2] = "Sophie"
        giaFollowerIDs[0] = i55ID
        giaFollowerIDs[1] = iSophieID

        --Tell everyone to follow.
        AL_SetProperty("Unfollow Actor Name", "55")
        AL_SetProperty("Unfollow Actor Name", "Sophie")
        AL_SetProperty("Follow Actor ID", i55ID)
        AL_SetProperty("Follow Actor ID", iSophieID)
    
    end

    --Scene transition.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsA", "FORCEPOS:15.0x31.0x0") ]])
    fnCutsceneBlocker()

end
