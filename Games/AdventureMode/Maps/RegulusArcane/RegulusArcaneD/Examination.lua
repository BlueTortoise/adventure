--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "EastExitN") then
    AudioManager_PlaySound("World|FlipSwitch")
    AL_BeginTransitionTo("RegulusArcaneB", "FORCEPOS:4.5x12.0x0")
    
elseif(sObjectName == "EastExitS") then
    AudioManager_PlaySound("World|FlipSwitch")
    AL_BeginTransitionTo("RegulusArcaneB", "FORCEPOS:4.5x14.0x0")
    
--[Objects]
elseif(sObjectName == "Offline") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A mobile work computer used by the faculty.[SOFTBLOCK] It's offline right now.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A local control console, managing temperature, lighting, music volume...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An RVD in the faculty break room.[SOFTBLOCK] Nothing is playing right now.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The ever ubiquitous oil making machine.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "LockedDoor") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I think whoever is in there should be given some privacy...)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end