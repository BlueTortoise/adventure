--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "NoExitTrigger") then
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There should be a supply room around here somewhere...)") ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 7.25, 9.50)
    fnCutsceneMove("Sophie", 7.25, 9.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "MakeoutTrigger") then

    --Variables.
    local iGalaGoal0State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal0State", "N")
    local iGalaGoal1State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N")
    local iGalaGoal2State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal2State", "N")
    local iGalaGoal3State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal3State", "N")
    local iGalaGoal4State = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGoal4State", "N")
    if(iGalaGoal0State == 0.0 or iGalaGoal1State == 0.0 or iGalaGoal2State == 0.0 or iGalaGoal3State == 0.0 or iGalaGoal4State == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (We should look around and try to finish all our objectives before leaving the party...)") ]])
        fnCutsceneBlocker()
    
        --Movement.
        fnCutsceneMove("Christine", 16.25, 15.50)
        fnCutsceneMove("Sophie", 16.25, 15.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    --Move on to the next scene.
    else

        --Sophie moves in.
        fnCutsceneMove("Sophie", 16.25, 12.50)
        fnCutsceneMove("Sophie", 15.25, 12.50)
        fnCutsceneMove("Christine", 16.25, 12.50)
        fnCutsceneFace("Sophie", 1, 0)
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Christine..?") ]])
        fnCutsceneBlocker()

        --Movement.
        fnCutsceneMoveFace("Sophie", 11.25, 12.50, 1, 0)
        fnCutsceneMoveFace("Christine", 12.25, 12.50, -1, 0)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneMoveFace("Christine", 11.75, 12.50, -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I want you *smooch* so bad, mmmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Mmm, please... lower...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *smooch* yes, mmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Scan completed.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] W-why did you stop?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sorry to lead you on, but I was using you as cover while I scanned the room for cameras. We're not being observed.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] But you can keep going, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Sorry, Sophie, but this is where we split up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I hope I've shown you a lovely evening, or rather, morning.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I wish it didn't have to end, but I suppose it does.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] So you're going underground now...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You know what you need to do?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Leave this closet, pretend like we had a fight and make sure everyone knows you're in here, crying.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Mill around the party for a little while, then hop on the tram back to Sector 96 and...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55 and I will meet up with you as soon as we can.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's going to be a lot of scared and confused units in Sector 96. Make sure you keep smiling, so they know everything will be okay.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Even if I don't think it will. I'll keep smiling.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Just like that. When I see that smile, I know I can do anything![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Come back in one piece you sexy maverick. I'll be waiting...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 11.25, 13.50)
        fnCutsceneMove("Christine",  8.75, 13.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I love you, Sophie.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I love you, Christine.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Next scene.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusArcaneF", "FORCEPOS:37.0x14.0x0") ]])
        fnCutsceneBlocker()

    end
end
