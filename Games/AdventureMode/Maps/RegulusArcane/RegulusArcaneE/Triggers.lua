--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "EntryScene") then
    
    --Repeat check.
    local iPhysicsEntry = VM_GetVar("Root/Variables/Chapter5/Scenes/iPhysicsEntry", "N")
    if(iPhysicsEntry == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iPhysicsEntry", "N", 1.0)
    
    --Black the screen out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Lock camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (42.75 * gciSizePerTile), (6.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Spawn Christine
	TA_Create("Christine")
		local iChristineID = RE_GetID()
		TA_SetProperty("Position", 42, 6)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_GolemDress/", true)
	DL_PopActiveObject()
    
    --Reposition.
    fnCutsceneTeleport("Christine", 42.75, 6.50)
    fnCutsceneTeleport("55", -100.25, -100.50)
    fnCutsceneTeleport("SX399", -100.25, -100.50)
    fnCutsceneBlocker()
    
    --Adjust the party to be Christine and nobody else.
	gsPartyLeaderName = "Christine"
    AL_SetProperty("Unfollow Actor Name", "SX399")
    WD_SetProperty("Set Leader Voice", "Christine")
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AL_SetProperty("Player Actor ID", iChristineID)
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fade screen in.
    fnCutsceneWait(45)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I haven't seen a single security unit the whole way here...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (No alarms, not a lot of cameras to dodge...[SOFTBLOCK] Where is everyone?)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Doesn't matter, I'm almost at the elevator.[SOFTBLOCK] This should be a piece of cake...)") ]])
    fnCutsceneBlocker()
    
    --Disable overlays.
    AL_SetProperty("Set Layer Disabled", "OverlayA", true)
    AL_SetProperty("Set Layer Disabled", "OverlayB", true)
    
elseif(sObjectName == "TerminalTrigger") then
    
    --Variables.
    local iSawElevatorScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawElevatorScene", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    if(iSawElevatorScene == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawElevatorScene", "N", 1.0)
        
        --Spawn the drone.
        TA_Create("Drone")
            TA_SetProperty("Position", -100, -100)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
        DL_PopActiveObject()
        
        --Spawn 55 and SX399.
        fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
        if(iSX399JoinsParty == 1.0) then
            fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
        end
        
        --Movement.
        fnCutsceneMove("Christine", 8.25, 27.50)
        fnCutsceneMove("Christine", 5.25, 27.50)
        fnCutsceneMove("Christine", 5.25, 24.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This is definitely the terminal![SOFTBLOCK] All right!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Just need to hook this repeater in and...[SOFTBLOCK] done!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Drone spawns!
        fnCutsceneTeleport("Drone", 14.25, 24.50)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Turn to face.
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (CRAPTANKEROUS CRAP, NOT RIGHT NOW!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 6.25, 26.50, 1.50)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Golem] UNIT 771852.[SOFTBLOCK] A MESSAGE HAS BEEN RECORDED FOR YOU.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Golem] ...[SOFTBLOCK] UNABLE TO LOCATE UNIT 771852 IN DESIGNATED AREA.[SOFTBLOCK] THIS UNIT IS LIKELY AT FAULT.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Golem] ENGAGE SELF-FLAGELLATION ROUTINES...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Sodbiscuits![SOFTBLOCK] How did they know I was here?[SOFTBLOCK] Did I miss a camera?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Or what if they got 55?[SOFTBLOCK] Or they got someone else, and made them fess up![SOFTBLOCK] Damn it, think, think!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Enable these layers.
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "OverlayA", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "OverlayB", false) ]])
        
        --Elevator appears.
        fnCutsceneInstruction([[ AL_SetProperty("Add Dislocation", "DislocationA", "Floor1", 3, 1, 5, 4, 8 * gciSizePerTile, 29 * gciSizePerTile) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Add Dislocation", "DislocationB", "Floor2", 3, 1, 5, 4, 8 * gciSizePerTile, 29 * gciSizePerTile) ]])
        fnCutsceneBlocker()
        fnCutsceneTeleport("55", 9.25, 30.50)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneTeleport("SX399", 10.25, 30.50)
        end
        fnCutsceneFace("55", 0, 1)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneFace("SX399", 0, 1)
        end
        
        for y = 29.0, 24.0, -0.05 do
            fnCutsceneTeleport("55", 9.25, y + 1.50)
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneTeleport("SX399", 10.25, y + 1.50)
            end
            fnCutsceneInstruction([[ AL_SetProperty("Modify Dislocation", "DislocationA", 8.00 * gciSizePerTile, ]] .. y .. [[ * gciSizePerTile) ]])
            fnCutsceneInstruction([[ AL_SetProperty("Modify Dislocation", "DislocationB", 8.00 * gciSizePerTile, ]] .. y .. [[ * gciSizePerTile) ]])
            fnCutsceneWait(3)
            fnCutsceneBlocker()
        end
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|TramStop") ]])
        for y = 24.0, 23.0, -0.05 do
            fnCutsceneTeleport("55", 9.25, y + 1.50)
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneTeleport("SX399", 10.25, y + 1.50)
            end
            fnCutsceneInstruction([[ AL_SetProperty("Modify Dislocation", "DislocationA", 8.00 * gciSizePerTile, ]] .. y .. [[ * gciSizePerTile) ]])
            fnCutsceneInstruction([[ AL_SetProperty("Modify Dislocation", "DislocationB", 8.00 * gciSizePerTile, ]] .. y .. [[ * gciSizePerTile) ]])
            fnCutsceneWait(7)
            fnCutsceneBlocker()
        end
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|TramArrive") ]])
        
        --Movement.
        fnCutsceneMove("Drone", 14.25, 27.50)
        fnCutsceneFace("Drone", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] ACCEPTABLE TARGETS LOCATED.[SOFTBLOCK] BEGINNING AUDIO PLAYBACK.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral][VOICE|2855] 'Once you've finished wasting time with pointless espionage, report to the basement under the ballroom.'[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral][VOICE|2855] 'And don't even think of running off.[SOFTBLOCK] I need your help, right now.[SOFTBLOCK] Drone, end message.[SOFTBLOCK] Don't record this part.'[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] THANK YOU FOR YOUR COOPERATION, AND HAVE A SPIFFY DAY.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Disable these layers.
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "OverlayA", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "OverlayB", true) ]])
        
        --Drone walks off.
        fnCutsceneMove("Drone", 12.25, 27.50)
        fnCutsceneMove("Drone", 12.25, 30.50)
        fnCutsceneMove("Drone", 23.25, 30.50)
        fnCutsceneBlocker()
        fnCutsceneTeleport("Drone", -104.25, -204.50)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (...[SOFTBLOCK] What?)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Walk out.
        fnCutsceneMove("Christine", 6.25, 27.50)
        fnCutsceneMove("Christine", 8.25, 27.50)
        fnCutsceneMove("55", 9.25, 27.50)
        fnCutsceneFace("55", -1, 0)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 10.25, 27.50)
            fnCutsceneFace("SX399", -1, 0)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Ummmm...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Explain.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I don't know![SOFTBLOCK] As soon as I attached the repeater, that drone just popped out of nowhere![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You failed to secure the area.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Give the girl a break.[SOFTBLOCK] Besides, it didn't pull the alarm, right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There was no security presence in the underground lift, and now we have received a cordial invitation to the location where we already going.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There wasn't much security on my way, either.[SOFTBLOCK] What's going on?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So what do we do?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Proceed with the plan.[SOFTBLOCK] If this is an ambush, then we may get that 'action' you were asking for, SX-399.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Whoever sent that message, however, seemed familiar with our plans.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It sounded a lot like your voice...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Obviously I did not record that message.[SOFTBLOCK] We must proceed with caution.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Ummmm...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Explain.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I don't know![SOFTBLOCK] As soon as I attached the repeater, that drone just popped out of nowhere![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You failed to secure the area.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yet that drone did not pull the alarm or bring a combat escort... Nor were there any security units near the underground lift.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Now, we have received a cordial invitation to the location where we already going.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There wasn't much security on my way, either.[SOFTBLOCK] What's going on?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] We have no choice but to proceed with the plan, despite the possibility of mission compromise.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Whoever sent that message seemed familiar with our plans.[SOFTBLOCK] Very familiar.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It sounded a lot like your voice...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Obviously I did not record that message.[SOFTBLOCK] We must proceed with caution.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        end
        
        --Movement.
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneMove("Christine", 8.25, 30.50)
        fnCutsceneMove("Christine", 23.25, 30.50)
        fnCutsceneMove("55", 9.25, 30.50)
        fnCutsceneMove("55", 23.25, 30.50)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 10.25, 30.50)
            fnCutsceneMove("SX399", 23.25, 30.50)
        end
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusArcaneH", "FORCEPOS:14.0x8.0x0") ]])
        
        --Set the party to include 55 and SX399.
        if(iSX399JoinsParty == 1.0) then
            gsFollowersTotal = 2
            gsaFollowerNames = {"55", "SX399"}
            giaFollowerIDs = {0, 0}

            --Get their uniqueIDs. 
            EM_PushEntity("55")
                local i55CharacterID = RE_GetID()
            DL_PopActiveObject()
            EM_PushEntity("SX399")
                local iSXCharacterID = RE_GetID()
            DL_PopActiveObject()

            --Store it and tell her to follow.
            giaFollowerIDs = {i55CharacterID, iSXCharacterID}
            AL_SetProperty("Follow Actor ID", i55CharacterID)
            AL_SetProperty("Follow Actor ID", iSXCharacterID)
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        
        --Party includes just 55.
        else
            gsFollowersTotal = 1
            gsaFollowerNames = {"55"}
            giaFollowerIDs = {0}

            --Get their uniqueIDs. 
            EM_PushEntity("55")
                local i55CharacterID = RE_GetID()
            DL_PopActiveObject()

            --Store it and tell her to follow.
            giaFollowerIDs = {i55CharacterID}
            AL_SetProperty("Follow Actor ID", i55CharacterID)
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        end
    end
end
