--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "NorthExit") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusArcaneG", "FORCEPOS:18.5x11.0x0")

elseif(sObjectName == "Offline") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A portable computer used by the administrator of this receiving department.[SOFTBLOCK] It's locked and I don't have time to hack it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "PopMachine") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A Fizzy Pop! vending machine.[SOFTBLOCK] I have no use for it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (No mentions of the security team on this console, just a list of ice-cream flavours.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The terminal contains a list of heavy equipment shipped to the department.[SOFTBLOCK] No mention of the security team posted here.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end