--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "55Begin") then
    
    --Repeat check.
    local iStarted55Sequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted55Sequence", "N")
    if(iStarted55Sequence == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iStarted55Sequence", "N", 1.0)
    
    --Black the screen out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Lock camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (37.25 * gciSizePerTile), (14.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Spawn 55. If needed, spawn SX-399.
    fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    if(iSXUpgradeQuest >= 3.0) then
        fnSpecialCharacter("SX399Lord", "SteamLord", 10, 8, gci_Face_North, false, nil)
    end
    
    --Move Christine off the field. Sophie too, she might still be in the party.
    fnCutsceneTeleport("Christine", -100.25, -100.50)
    fnCutsceneTeleport("Sophie", -100.25, -100.50)
    fnCutsceneBlocker()
    
    --Adjust the party to be just 55 for now.
	gsPartyLeaderName = "55"
    AL_SetProperty("Unfollow Actor Name", "Sophie")
    WD_SetProperty("Set Leader Voice", "2855")
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
    EM_PushEntity("55")
        local i55sID = RO_GetID()
    DL_PopActiveObject()
	AL_SetProperty("Player Actor ID", i55sID)
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fade screen in.
    fnCutsceneWait(45)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --55 Appears.
    fnCutsceneTeleport("55", 37.25, 14.50)
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (No hostiles on motion tracker, scanning for security pulses...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (All checks still green.[SOFTBLOCK] No network traffic.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (Do not question good fortune.[SOFTBLOCK] Proceed with the mission.[SOFTBLOCK] Begin radio silence.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] (Here we go...)") ]])
    fnCutsceneBlocker()
    
    --Player light properties.
    fnCutsceneInstruction([[ AL_SetProperty("Deactivate Player Light") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Player Light", 3600, 3600) ]])
    
elseif(sObjectName == "WhereIsEveryone") then
    
    --Repeat check.
    local iSawWhereIsEveryone = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawWhereIsEveryone", "N")
    if(iSawWhereIsEveryone == 1.0) then return end
    
    --Variables.
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --SX-399 does not join the party:
    if(iSXUpgradeQuest < 3.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWhereIsEveryone", "N", 1.0)
        
        --Camera movement.
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (8.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(85)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Actor Name", "55")
        DL_PopActiveObject()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No movement...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No security detail, no barricades, no turrets, not even a remote sensing camera...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In this situation, Unit 771852 would say something dumb, like...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I am unable to construct a stupid enough sentence to do her justice.[SOFTBLOCK] Note to self, ask her for a list of stupid phrases later.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Proceeding with mission.") ]])
        fnCutsceneBlocker()
    
    --SX-399 joins the party:
    else
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWhereIsEveryone", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N", 1.0)
        
        --Camera movement.
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0) --Default is 5.0
            CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (8.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Gosh darn it, where the hay is everyone?[SOFTBLOCK] Don't tell me I'm too late...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --55 walks up.
        fnCutsceneMove("55", 14.25, 11.50)
        fnCutsceneMove("55", 11.25, 8.50)
        fnCutsceneFace("55", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] SX-399...[SOFTBLOCK] why are you here?[SOFTBLOCK] You are in the process of compromising the most important operation - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Oh there you are.[SOFTBLOCK] I thought maybe you'd already had some fun without me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But then I noticed the total absence of scorch marks and plasma scoring, so I figured maybe you hadn't had any fun at all.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Why.[SOFTBLOCK] Are you.[SOFTBLOCK] Here?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Beeeecause...[SOFTBLOCK] you asked me to be?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] I requested a Steam Droid heavy support detachment, in the event that I needed to destroy enemy vehicles or blockades.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Yep.[SOFTBLOCK] Reporting for duty.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] Don't tell me you took the assignment![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Well, of course I did![SOFTBLOCK] I've been keen on it ever since PW-12 started looking for volunteers![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Everyone in my squad said I was perfect![SOFTBLOCK] I even got my mother to sign off on it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] This is not some social call![SOFTBLOCK] This is life and death![SOFTBLOCK] One mistake on this operation will cost hundreds of units![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] And you're assuming that I've been spinning my gears since you left Sprocket City?[SOFTBLOCK] Hell no, sister![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I've spent months blasting creepy crawlies and wrestling with security units![SOFTBLOCK] I'm the best Sprocket City has to offer![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Yes, mother says I need more experience.[SOFTBLOCK] I'm not arguing that.[SOFTBLOCK] But when the chips are down, my shiny new body gives me faster reactions and prime accuracy.[SOFTBLOCK] I *am* your heavy support.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I will not allow you to be injured.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Well then.[SOFTBLOCK] You'd best cover me, hadn't you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Because I'm here and I ain't leaving your patootie until the job is done.[SOFTBLOCK] Get me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Arguing with you is unlikely to produce a favourable outcome.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It seems I cannot find any companions who listen to reason, can I?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] [SOFTBLOCK][SOFTBLOCK][SOFTBLOCK][SOFTBLOCK][SOFTBLOCK][EMOTION|SX-399|Flirt]Nope![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will take point.[SOFTBLOCK] If we encounter heavy resistance and are unable to proceed, we *will* retreat.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Uh, maybe your optical scanners are shot.[SOFTBLOCK] Haven't you seen that there is no resistance?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I even polished my fission carbine but there's nobody to melt.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is indeed a problem.[SOFTBLOCK] I was expecting at least a security division to be posted here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The schematics show the access elevator is in the back room there.[SOFTBLOCK] They may yet be waiting in ambush.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Stay behind me, and move out, soldier.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You got it, boss bot![SOFTBLOCK] (Because behind you is right where I want to be...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (SX-399 has joined the party!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --SX-399 joins the party.
        fnCutsceneMove("SX399", 11.25, 8.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
            
        --Lua globals.
        gsFollowersTotal = 1
        gsaFollowerNames = {"SX399"}
        giaFollowerIDs = {0}

        --Get her uniqueID. 
        EM_PushEntity("SX399")
            local iCharacterID = RE_GetID()
        DL_PopActiveObject()

        --Store it and tell her to follow.
        giaFollowerIDs = {iCharacterID}
        AL_SetProperty("Follow Actor ID", iCharacterID)

        --Add SX-399 to the party lineup.
        AC_SetProperty("Set Party", 2, "SX-399")
        LM_ExecuteScript(gsItemListing, "Fission Carbine")
        LM_ExecuteScript(gsItemListing, "Bronze Chestguard")
        AC_PushPartyMember("SX-399")
            --Equip.
            ACE_SetProperty("Equip", "Weapon", "Fission Carbine")
            ACE_SetProperty("Equip", "Armor", "Bronze Chestguard")
            
            --SX-399 starts the game at level 4, same as 55 and Christine.
            ACE_SetProperty("EXP", 17 + 66 + 115 + 1)
        DL_PopActiveObject()
    end
end
