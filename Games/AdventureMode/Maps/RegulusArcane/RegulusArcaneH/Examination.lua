--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "WhereWeCame") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (These passages will lead back to the main halls, but we're not done here yet.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "LockedDoor") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Not the way we need to go right now...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "MouthThing") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Some kind of mouth where a door should be...[SOFTBLOCK] I can't open it, maybe there's a way around...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SpareChairs") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Spare chairs cleared out from the staging area south of here...)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] You![SOFTBLOCK] 771852![SOFTBLOCK] What part of crisis did you not understand?[SOFTBLOCK] Stop snooping around my consoles, and seal those breaches!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Observations of Quantum Superfluids in a Nano-particle Bath'.[SOFTBLOCK] Looks like a fun read!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Research logs for the fluid dynamics team.[SOFTBLOCK] Looks like they got reassigned to work in weapons research...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Unit 688238 pushed her work terminal next to mine.[SOFTBLOCK] She's waaaay out of my league, is she trying to torment me with the sexual tension?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I moved our work terminals together, but Unit 703442 still hasn't noticed me.[SOFTBLOCK] I might just have to have my hand slip over hers soon!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like logs related to the development of pulse weaponry.[SOFTBLOCK] Seems the development team has been working overtime to get the new pulse rifles to factoring.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Supplies") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Assorted junk.[SOFTBLOCK] Nothing that is of use right now.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Attention::[SOFTBLOCK] There are no guard rails because we assume you units are smart enough not to wander onto the weapons testing range while it is in use.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('If not, well, that's what is called a self-correcting problem.[SOFTBLOCK] -Unit 89332, Head of Weapons Testing')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ChargeA") then
    
    --Variables.
    local iSetChargeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeA", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    if(iSetChargeA == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSetChargeA", "N", 1.0)
        
        --Scene.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] 55, get the charges out.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        fnCutsceneMove("Christine", 48.25, 10.50)
        fnCutsceneFace("Christine", 1, 0)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 48.25, 11.50)
            fnCutsceneFace("SX399", 1, 0)
        end
        fnCutsceneMove("55", 49.25, 10.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 50.25, 10.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Ending case.
        local iSetChargeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeA", "N")
        local iSetChargeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeB", "N")
        local iSetChargeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeC", "N")
        if(iSetChargeA == 0.0 or iSetChargeB == 0.0 or iSetChargeC == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Charges set.[SOFTBLOCK] Continue with the operation.") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] We've set all three charges.[SOFTBLOCK] We should head back to the entry point.") ]])
            fnCutsceneBlocker()
        end
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fold the party.
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 48.25, 10.50)
        end
        fnCutsceneMove("55", 48.25, 10.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (55 pasted a bomb onto this creepy mouth thing.[SOFTBLOCK] Probably best not to hang around it.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "ChargeB") then
    
    --Variables.
    local iSetChargeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeB", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    if(iSetChargeB == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSetChargeB", "N", 1.0)
        
        --Scene.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] 55, plant the bomb.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        fnCutsceneMove("Christine", 75.25, 40.50)
        fnCutsceneFace("Christine", 1, 0)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 75.25, 41.50)
            fnCutsceneFace("SX399", 1, 0)
        end
        fnCutsceneMove("55", 76.25, 40.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 77.25, 40.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Ending case.
        local iSetChargeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeA", "N")
        local iSetChargeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeB", "N")
        local iSetChargeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeC", "N")
        if(iSetChargeA == 0.0 or iSetChargeB == 0.0 or iSetChargeC == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Charges set.[SOFTBLOCK] Continue with the operation.") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] We've set all three charges.[SOFTBLOCK] We should head back to the entry point.") ]])
            fnCutsceneBlocker()
        end
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fold the party.
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 75.25, 40.50)
        end
        fnCutsceneMove("55", 75.25, 40.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (55 pasted a bomb onto this creepy mouth thing.[SOFTBLOCK] Probably best not to hang around it.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "ChargeC") then
    
    --Variables.
    local iSetChargeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeC", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    if(iSetChargeC == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSetChargeC", "N", 1.0)
        
        --Scene.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] You know what to do, 55.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        fnCutsceneMove("Christine", 6.25, 58.50)
        fnCutsceneFace("Christine", -1, 0)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 6.25, 59.50)
            fnCutsceneFace("SX399", -1, 0)
        end
        fnCutsceneMove("55", 5.25, 58.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 4.25, 58.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Ending case.
        local iSetChargeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeA", "N")
        local iSetChargeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeB", "N")
        local iSetChargeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeC", "N")
        if(iSetChargeA == 0.0 or iSetChargeB == 0.0 or iSetChargeC == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Charges set.[SOFTBLOCK] Continue with the operation.") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] We've set all three charges.[SOFTBLOCK] We should head back to the entry point.") ]])
            fnCutsceneBlocker()
        end
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fold the party.
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 6.25, 58.50)
        end
        fnCutsceneMove("55", 6.25, 58.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (55 pasted a bomb onto this creepy mouth thing.[SOFTBLOCK] Probably best not to hang around it.)") ]])
        fnCutsceneBlocker()
    end
    
    


--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end