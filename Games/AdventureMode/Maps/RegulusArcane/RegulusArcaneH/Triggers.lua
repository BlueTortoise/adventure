--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "56Trigger") then
    
    --[Variables]
    local iSetChargeA        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeA", "N")
    local iSetChargeB        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeB", "N")
    local iSetChargeC        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeC", "N")
    local iStarted56Sequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N")
    local iSX399JoinsParty   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --[First Scene]
    --2856 basically yells at the party. What a jerk.
    if(iStarted56Sequence == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N", 1.0)
        
        --Modify 56's properties so she doesn't share dialogue with 55.
        DialogueActor_Push("2856")
            DialogueActor_SetProperty("Remove Alias", "55")
            DialogueActor_SetProperty("Remove Alias", "2855")
        DL_PopActiveObject()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] About time you got here.[SOFTBLOCK] Just how long were you going to make me wait?") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 13.25, 21.50)
        fnCutsceneMove("Christine", 12.25, 21.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMove("55", 13.25, 21.50)
        fnCutsceneFace("55", 0, 1)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 13.25, 21.50)
            fnCutsceneMove("SX399", 14.25, 21.50)
            fnCutsceneFace("SX399", 0, 1)
        end
        fnCutsceneMove("2856", 11.25, 25.50)
        fnCutsceneMove("2856", 13.25, 25.50)
        fnCutsceneMove("2856", 13.25, 23.50)
        fnCutsceneFace("2856", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] So you managed to stumble your way here eventually.[SOFTBLOCK] What kept you?[SOFTBLOCK] The complete lack of obstacles and security units?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I have been managing a serious crisis in your stead.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Are you...[SOFTBLOCK] 2856?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] That's Prime Command Unit 2856, Head of Research.[SOFTBLOCK] Yes, I am.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] You already knew that, but asked anyway.[SOFTBLOCK] Are you, perhaps, an idiot?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Already I see the family resemblence.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Sister...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Yes, yes, a touching family reunion.[SOFTBLOCK] Now, are you going to yammer all day, or shall we get down to business?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Approximately 45 minutes before the Sunrise Gala was set to begin, my security units began noticing tectonic disturbances.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Something of unknown origin has tunneled...[SOFTBLOCK] well, perhaps it'd be best to say, morphed through, the rock and concrete beneath this facility.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] We are currently under assault by attackers disgorged from the unknown organism.[SOFTBLOCK] I have lost numerous security units.[SOFTBLOCK] If we do not seal the access to the lower levels, they will overrun this area and flood into the ballroom above us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I have called upon your assistance to prevent that from happening.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Thank you for barking orders at us.[SOFTBLOCK] Now, please, tell us why you think we will help you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You seem to know about our plan and objectives.[SOFTBLOCK] Why have you not stopped our operation?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] The one where you plant explosives on the support pillars?[SOFTBLOCK] The one where you start riots and attempt to take over the city?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Why yes, I am fully informed of your plans.[SOFTBLOCK] In fact, it is because of that that I have allowed them to proceed.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I had originally intended to arrest you.[SOFTBLOCK] After all, while I have allowed your revolutionaries a certain amount of leeway, I cannot allow so many high-ranking units to be retired.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] But the situation has changed.[SOFTBLOCK] You have the explosive charges, do you not?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] You will use those to seal the breaches.[SOFTBLOCK] There are three in this area.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I think allowing these beasts to overrun the ballroom will be doing our work for us.[SOFTBLOCK] We refuse.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Your objectives are confused because of limited information.[SOFTBLOCK] You are aware of Project Vivify, I know you are.[SOFTBLOCK] I was there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I have been chasing her since she exfiltrated the LRT facility.[SOFTBLOCK] I have tracked her to the caverns beneath this building.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Feel free to allow her minions to subsume all the high-ranking units.[SOFTBLOCK] You will doom the city with your inaction.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] What?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Project Vivify destroys units that oppose her, and...[SOFTBLOCK] influences...[SOFTBLOCK] other units.[SOFTBLOCK] For every five I have lost, one has joined her side.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I've taken to strapping my units with remote EM-charges.[SOFTBLOCK] But I have not taken such a precaution with the Lord Units above us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] While I cannot ascertain the true depth of Vivify's motives, her actions are not random.[SOFTBLOCK] I have concluded she intends to assimilate the Sunrise Gala.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] And that will doom the city...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Oh, good, you were listening.[SOFTBLOCK] Bravo![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Yes, dear sister, the city will be destroyed because we have yet to successfully [SOFTBLOCK]*stop*[SOFTBLOCK] Vivify.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] She attacked our forces at the LRT facility, and they were wiped out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] One month ago, she stormed the refueling station west of here.[SOFTBLOCK] All contact was lost.[SOFTBLOCK] One-hundred units wiped out, many of them to join in her next attack.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Two weeks ago, Sector 110 had to be quarantined.[SOFTBLOCK] We welded the doors shut and posted guards.[SOFTBLOCK] All my assault teams were lost.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] We have yet to score a single victory against Vivify and her forces.[SOFTBLOCK] The best we can do is corral her minions.[SOFTBLOCK] Direct assaults are always defeats.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] There are exactly two units to come into direct contact with Vivify and survive.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Us...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Correct.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] She is likely nearby.[SOFTBLOCK] If she is not neutralized, then she will take the city at her leisure.[SOFTBLOCK] We have equipped security teams with the latest hardware and advanced combat routines, to exactly zero effect.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Or did you think I had ordered all fabricators to produce combat equipment because I thought it would be a good laugh?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Okay, okay![SOFTBLOCK] We'll help![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Good, you see reason after all.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Not yet.[SOFTBLOCK] How did you know about our plans?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Oh, yes, by all means, continue asking questions.[SOFTBLOCK] We are currently facing obliteration at the hands of an unknown hostile force of unfathomable size and destructive power.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] That seems like the perfect time to fight one another![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Get your equipment, seal those breaches, and then get back here post-haste so we can determine our next move.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] STOP STARING AT ME AND GET MOVING ALREADY!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("2856", 13.25, 25.50)
        fnCutsceneMove("2856", 11.25, 25.50)
        fnCutsceneMove("2856", 11.25, 24.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 13.25, 21.50)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 13.25, 21.50)
        end
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    --[Second Sequence]
    --Set off the bombs. This is where things go very badly.
    elseif(iSetChargeA == 1.0 and iSetChargeB == 1.0 and iSetChargeC == 1.0) then
        
        --Movement.
        fnCutsceneMove("Christine", 13.25, 22.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMove("55", 14.25, 22.50)
        fnCutsceneFace("55", 0, 1)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 15.25, 22.50)
            fnCutsceneFace("SX399", 0, 1)
        end
        fnCutsceneMove("2856", 11.25, 25.50)
        fnCutsceneMove("2856", 14.25, 25.50)
        fnCutsceneMove("2856", 14.25, 24.50)
        fnCutsceneFace("2856", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Judging by the slack-jawed empty expressions, you've gotten your assignment done.[SOFTBLOCK] Are you waiting to pull the trigger?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Oh she's a right proper bitch, isn't she?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] You're damn right I am.[SOFTBLOCK] Set the charges off.[SOFTBLOCK] NOW.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You heard her, 55.[SOFTBLOCK] Hit the bu - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Christine, you're receiving an urgent call.[SOFTBLOCK] Patching you through.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: You gave root access to your tandem unit and maximum communication priority.[SOFTBLOCK] She is starting a video call...") ]])
        
        --No SX-399.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Judging by the slack-jawed empty expressions, you've gotten your assignment done.[SOFTBLOCK] Are you waiting to pull the trigger?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're a bit of a tosser, aren't you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] You're damn right I am.[SOFTBLOCK] Set the charges off.[SOFTBLOCK] NOW.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You heard her, 55.[SOFTBLOCK] Hit the bu - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Christine, you're receiving an urgent call.[SOFTBLOCK] Patching you through.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: You gave root access to your tandem unit and maximum communication priority.[SOFTBLOCK] She is starting a video call...") ]])
            
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusArcaneCAlt", "FORCEPOS:25.0x22.0x0") ]])
        fnCutsceneBlocker()
    end
    
end
