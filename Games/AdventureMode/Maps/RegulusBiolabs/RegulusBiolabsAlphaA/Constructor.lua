--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsAlphaA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iSawMusicComments = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMusicComments", "N")
    if(iSawMusicComments == 0.0) then
        AL_SetProperty("Music", "SerenityCrater")
    else
        AL_SetProperty("Music", "Biolabs")
    end
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
    local iReceivedMap = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N")
    if(iReceivedMap == 1.0) then
        fnResolveMapLocation("RegulusBiolabsAlphaA")
    end
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iSawBigRant = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawBigRant", "N")
    if(iSawBigRant == 0.0) then
        fnStandardNPCByPosition("2856")
        fnSpawnNPCPattern("Security", "A", "B")
    end
end
