--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "Terminal") then
    local iSawBigRant = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawBigRant", "N")
    if(iSawBigRant == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unit 2856 wiped her activity logs on the terminal.[SOFTBLOCK] She is nothing if not thorough.)") ]])
        fnCutsceneBlocker()
    end
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end