--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "EntryScene") then
    
    --Variables.
	local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
        
    --Topics for 55.
    WD_SetProperty("Unlock Topic", "Biolabs_2856", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Biolabs", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Biology", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Magic", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Runestones", 1)
    
    --Topics for SX-399.
    WD_SetProperty("Unlock Topic", "Biolabs_Latex", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Oxygen", 1)
    
    --Topics for Sophie
    local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
    WD_SetProperty("Unlock Topic", "Biolabs_Runestone", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Tourism", 1)
    if(iHasElectrospriteForm == 1.0) then
        WD_SetProperty("Unlock Topic", "Biolabs_Electrosprites", 1)
    end
    
    --Turn off everyone's collision flags, in case they are still on.
    EM_PushEntity("55")
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    EM_PushEntity("Sophie")
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    if(iSX399JoinsParty == 1.0) then
        EM_PushEntity("SX399")
            TA_SetProperty("Clipping Flag", false)
        DL_PopActiveObject()
    end
    
    --Set this flag to indicate we're in the biolabs.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/i55IsFollowing", "N", 1.0)
    if(iSX399JoinsParty == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
    end
    
    --Unlock the gala dresses for Sophie and Christine.
    VM_SetVar("Root/Variables/Costumes/Christine/iGolemGala", "N", 1.0)
    VM_SetVar("Root/Variables/Costumes/Sophie/iGolemGala", "N", 1.0)
    
    --Warps are still disallowed.
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
    
    --Black the screen out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Undo this collision.
    AL_SetProperty("Set Collision", 23, 29, 0, 0)
    
    --Position the party.
    fnCutsceneTeleport("Christine", 14.25, 31.50)
    fnCutsceneTeleport("55", 14.25, 31.50)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneTeleport("SX399", 14.25, 31.50)
    end
    fnCutsceneTeleport("Sophie", 14.25, 31.50)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    
    --Movement.
    local fSpeed = 2.50
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("Christine", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 16.25, 31.50, fSpeed)
        fnCutsceneMove("55", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 17.25, 31.50, fSpeed)
        fnCutsceneMove("55", 16.25, 31.50, fSpeed)
        fnCutsceneMove("SX399", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 18.25, 31.50, fSpeed)
        fnCutsceneMove("55", 17.25, 31.50, fSpeed)
        fnCutsceneMove("SX399", 16.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 22.25, 31.50, fSpeed)
        fnCutsceneFace("Christine", 1, -1)
        fnCutsceneMove("55", 21.25, 31.50, fSpeed)
        fnCutsceneMove("SX399", 20.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 19.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    else
        fnCutsceneMove("Christine", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 16.25, 31.50, fSpeed)
        fnCutsceneMove("55", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 17.25, 31.50, fSpeed)
        fnCutsceneMove("55", 16.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 18.25, 31.50, fSpeed)
        fnCutsceneMove("55", 17.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 16.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 22.25, 31.50, fSpeed)
        fnCutsceneFace("Christine", 1, -1)
        fnCutsceneMove("55", 21.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 20.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Gee, I wonder if she went this way...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Obviously, she did.[SOFTBLOCK] Security units do not use acid to destroy walls or leave corpses in their wake.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sarcasm, 55.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I'd tell her to lighten up, but considering the situation, maybe I shouldn't be so facetious.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Nah.[SOFTBLOCK] I gotta keep screwing with her...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Christine, do you have to stay so slimy and...[SOFTBLOCK] tentacled?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Does this help?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
			
    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "SteamLord") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Smashing![SOFTBLOCK] I still have my dress on and everything![SOFTBLOCK] It's not even scuffed![BLOCK][CLEAR]") ]])
    
    --Sophie knows about the runestone:
    if(iSophieKnowsAboutRunestone == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] So much better![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Wasting time, 771852...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yeah, yeah.[SOFTBLOCK] Let's go, team.[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Wow, that's so cool![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] 55 told me you could transform yourself but this is the first time I've seen it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] She is most uncreative in the use of this ability.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Now Sophie, my other forms may be more useful in here.[SOFTBLOCK] So don't get upset if I change again.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Promise me no tentacles.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I promise![BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, I can also use the Costumes menu at a save point to switch to my normal outfit if I want to![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Me too, now that nobody cares about me being a slave unit.[SOFTBLOCK] Let's go!") ]])
    
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 23.25, 30.50)
    fnCutsceneMove("Christine", 23.25, 27.50)
    fnCutsceneMove("55", 22.25, 31.50)
    fnCutsceneMove("55", 23.25, 30.50)
    fnCutsceneMove("55", 23.25, 27.50)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX399", 22.25, 31.50)
        fnCutsceneMove("SX399", 23.25, 30.50)
        fnCutsceneMove("SX399", 23.25, 27.50)
    end
    fnCutsceneMove("Sophie", 22.25, 31.50)
    fnCutsceneMove("Sophie", 23.25, 30.50)
    fnCutsceneMove("Sophie", 23.25, 27.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
    --Reset this collision.
    fnCutsceneInstruction([[ AL_SetProperty("Set Collision", 23, 29, 0, 1) ]])
    fnCutsceneBlocker()

--Get the map!
elseif(sObjectName == "DumbGuards") then

    --Repeat check.
    local iReceivedMap = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N")
    if(iReceivedMap == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "LatexDrone", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "LatexDroneB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] EXCUSE THE INTERRUPTION, SUPERIOR UNITS, BUT THIS UNIT REQUIRES YOUR ASSISTANCE.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh?[SOFTBLOCK] What do you need help with?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] THIS UNIT WAS ORDERED TO [SOFTBLOCK][VOICE|2856]'Get that worthless piece of trash away from me' [VOICE|LatexDrone][SOFTBLOCK]BY COMMAND UNIT 2856.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] THIS UNIT FOUND IT AND THOUGHT THE COLOURS WERE PRETTY.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hmm?[SOFTBLOCK] Let me take a look.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is a tourist brochure.[SOFTBLOCK] Its previous owner scribbled some notes on it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Look at the little raiju face![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] [EMOTION|Sophie|Smirk]It is also a map of the area we can refer to.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You didn't download the map before we came here?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Wireframe area schematics only.[SOFTBLOCK] This map contains additional details and connectivity paths.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Excellent work, drone![SOFTBLOCK] Well done![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] THIS UNIT IS PLEASED IT COULD DISPOSE OF ITS GARBAGE IN A WAY THAT PLEASED YOU.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] WOULD YOU LIKE IT TO PROVIDE ANY FURTHER WORTHLESS GARBAGE FOR YOU?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No thank you, drone.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] HAVE A SPIFFY DAY.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] [SOUND|Menu|SpecialItem](Received a map of the biolabs!)") ]])
    fnCutsceneBlocker()
    
    fnResolveMapLocation("RegulusBiolabsAlphaA")
    

end
