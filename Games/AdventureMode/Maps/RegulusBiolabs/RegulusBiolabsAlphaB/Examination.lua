--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "Sign") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hydroponics and Datacore:: West.[SOFTBLOCK]\nGamma Labs:: North.)") ]])
    
elseif(sObjectName == "WaterTerminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Oceanic Aquatic Habitat Alpha::[SOFTBLOCK] Status, green.[SOFTBLOCK] Chemical checks normal.[SOFTBLOCK] Life form balance green.[SOFTBLOCK] Aquatic oxygen levels normal, next monitor pulse in 13 minutes.)") ]])
    
elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] 'The Juni Swallow, so named for its regal character, is often portrayed in folklore as a fierce predatory bird.[SOFTBLOCK] In fact, it consumes primarily fruit which, which the exception of the durian, does not put up much of a fight.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like a nature documentary is playing, recorded right here in the Biolabs!)") ]])
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Plain water solution, no fertilizer.[SOFTBLOCK] No pesticides, manual removal of ambient pests.[SOFTBLOCK] Growth rate 'baseline', used as reference for other plant specimens.)") ]])
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Nitrate rich water solution, no solid fertilizer, no pesticides, manual pest removal.[SOFTBLOCK] Growth rate above baseline, recommend further research into nitrate watering solutions.)") ]])
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Plain water solution, no fertilizer.[SOFTBLOCK] Use of pesticides rather than manual removal of pests.[SOFTBLOCK] Growth rates well below baseline, vastly reduced labor requirements.[SOFTBLOCK] Possible for mass production or use in times of labour shortages?[SOFTBLOCK] Investigate.)") ]])
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Plain water solution, nitrogen-rich soil fertilizer derived from animal droppings.[SOFTBLOCK] No pesticides manual removal of ambient pests.[SOFTBLOCK] Growth rates well above baseline, but use of externally derived fertilizers unsustainable barring extensive mining of potash or other fertilizers.[SOFTBLOCK] Reserve usage for possible food shortages recommended.)") ]])
    
elseif(sObjectName == "Wall") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Someone tore a section of the wall off of the power station.[SOFTBLOCK] The wiring is exposed but doesn't seem to be damaged.[SOFTBLOCK] A hacking attempt, maybe?)") ]])
    
elseif(sObjectName == "Boat") then

    --Variables.
    local iBoatShortcut = VM_GetVar("Root/Variables/Chapter5/Scenes/iBoatShortcut", "N")
    
    --Not open yet:
    if(iBoatShortcut == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's a boat here, but the sluice gates to the east are closed.[SOFTBLOCK] I can probably use this boat to get to the Delta labs if I can open them...)") ]])
        fnCutsceneBlocker()

    --Opened:
    else
        WD_SetProperty("Show")
        WD_SetProperty("Append", "Thought:[VOICE|Leader] (Use the boat to cross to the Delta Laboratories?)[BLOCK]")
        
        --Activate and set.
        local sDestScript = LM_GetCallStack(0)
        WD_SetProperty("Activate Decisions")
        WD_SetProperty("Add Decision", "Yes", sDestScript, "Yes")
        WD_SetProperty("Add Decision", "No",  sDestScript, "No")

    end

--[Crossing the Lake]
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
    
    --Variables.
    local i55ComplainedBoat = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ComplainedBoat", "N")
    if(i55ComplainedBoat == 0.0) then
    
        --Flag
        VM_SetVar("Root/Variables/Chapter5/Scenes/i55ComplainedBoat", "N", 1.0)
    
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay everyone, grab an oar![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Is this absolutely necessary?[SOFTBLOCK] Why don't we simply use your teleporation ability?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This is faster.[SOFTBLOCK] C'mon.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Are you absolutely sure?[SOFTBLOCK] What if the boat capsizes?[SOFTBLOCK] That crude wooden device looks poorly maintained.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Are you joking?[SOFTBLOCK] What rich girl wasn't part of the rowing team in school?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay a lot of them, but not this one![SOFTBLOCK] I'm a seasoned mariner.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] So long as I have your assurances...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It'll be okay, I know what I'm doing.") ]])
        fnCutsceneBlocker()
    
        --Transition.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsDeltaH", "FORCEPOS:39.5x13.0x0") ]])
    
    else
        AL_BeginTransitionTo("RegulusBiolabsDeltaH", "FORCEPOS:39.5x13.0x0")
    
    end
    
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end