--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "ChangeMusic") then
    local iSawMusicComments = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMusicComments", "N")
    if(iSawMusicComments == 0.0) then
        
        --Topics for 55.
        WD_SetProperty("Unlock Topic", "Biolabs_2856", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Biolabs", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Biology", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Magic", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Runestones", 1)
        
        --Topics for SX-399.
        WD_SetProperty("Unlock Topic", "Biolabs_Latex", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Oxygen", 1)
        
        --Topics for Sophie
        local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
        WD_SetProperty("Unlock Topic", "Biolabs_Runestone", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Tourism", 1)
        if(iHasElectrospriteForm == 1.0) then
            WD_SetProperty("Unlock Topic", "Biolabs_Electrosprites", 1)
        end
        
        --Flag. Allows warps.
        VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 0.0)
        
        --Variables.
        local iSX399IsFollowing   = 0
        if(fnIsCharacterPresent("SX399")) then iSX399IsFollowing = 1.0 end
        local iSpokeTo2856Biolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N")
        local sChristineForm      = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMusicComments", "N", 1.0)
        
        --Dialogue
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        
        --Didn't talk to 2856? Dialogue changes.
        if(iSpokeTo2856Biolabs == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N", 1.0)
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Hmm?[SOFTBLOCK] I'm getting a call.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] Oh, hello? Is anyone there?[SOFTBLOCK] I'm terrible sorry to interrupt...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] BECAUSE YOU STUPID DONGLES DECIDED YOU DIDN'T NEED TO TALK TO ME BEFORE JUST WALTZING PAST ME YOU WORTHLESS - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] PDU, volume levels to five percent.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: I'm sorry, Christine, volume levels were already at five percent.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Well then, one percent?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] WHY DON'T YOU JUST TURN ME OFF INSTEAD OF DAMPENING ME, HMM?[SOFTBLOCK] NOTHING IMPORTANT I COULD SAY TO YOU?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] Since obstinacy is your one talent, I'll be as brief as I can.[BLOCK][CLEAR]") ]])
        
        --Otherwise:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Hmm?[SOFTBLOCK] I'm getting a call.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] Christine, there's something you should know before you go any further.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] You're sorry for all the terrible things you did?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] Don't interrupt me you stupid - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] A girl can dream...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] Since obstinacy is your one talent, I'll be as brief as I can.[BLOCK][CLEAR]") ]])
        end
        
        --Common dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] Network access in the Biolabs is spotty, for longstanding reasons.[SOFTBLOCK] Normally, we'd communicate with radio, but I'd prefer if you didn't.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] We can't know for certain who is listening in.[SOFTBLOCK] Only contact me via network stations.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Listening in?[SOFTBLOCK] Does she mean the rebels?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unlikely.[SOFTBLOCK] She means the invaders, I would assume.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55's right.[SOFTBLOCK] Those creatures can metabolize radio waves.[SOFTBLOCK] Don't ask me how they do that, but they can do that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'm more interested in how you know that...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It just...[SOFTBLOCK] I just know it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Because I am one...[SOFTBLOCK] I'm just still in denial about it...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856][EMOTION|Christine|PDU] Whatever the reason, don't use radio waves for communication except at short ranges.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] Your objective is the Raiju Ranch, north-east of your position.[SOFTBLOCK] You'll need to go through the Gamma habitat, then go east to the Delta habitat.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Understood.[SOFTBLOCK] Moving out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Actually...[SOFTBLOCK] PDU?[SOFTBLOCK] I don't want to listen to dour music all the time.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] Affirmative, Christine.[SOFTBLOCK] Will this do?") ]])
        fnCutsceneBlocker()
        
        --Change the music.
        fnCutsceneInstruction([[ AL_SetProperty("Music", "Biolabs") ]])
        fnCutsceneWait(425)
        fnCutsceneBlocker()
        
        --Dialogue
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "2855", "Neutral") ]])
        if(iSX399IsFollowing == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
        end
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thank you, PDU.[SOFTBLOCK] This will do nicely.[BLOCK][CLEAR]") ]])
        
        --Changes if SX-399 is in the party.
        if(iSX399IsFollowing == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Music?[SOFTBLOCK] You mean you listen to smooth beats while vaporizing critters?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] A fellow enthusiast?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Nope![SOFTBLOCK] RJ-45 said she'd box my ears if she caught me listening to music on a mission![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] RJ-45 is practical.[SOFTBLOCK] You should follow her advice.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Ha![SOFTBLOCK] I just never let her catch me![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] This girl's got digital recording and playback.[SOFTBLOCK] Nobody but me can tell.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Christine likes synthwave?[SOFTBLOCK] Oooh![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Classical, myself, but I can respect your tastes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] During a combat situation, we should not distract ourselves with pointless diversions.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You don't do music?[SOFTBLOCK] You're missing out, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Besides, I think it actually enhances my combat performance.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Kicking butt to a beat.[SOFTBLOCK] Seems like something you might want to research, cutie-five.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] Cutie...[SOFTBLOCK] Five?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Oh, a reaction?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] That name...[SOFTBLOCK] will do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Very well, Christine.[SOFTBLOCK] I will research the effects of music on combat efficiency at a later date.[SOFTBLOCK] For now, proceed with caution.[BLOCK][CLEAR]") ]])
        
        --Default.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Music?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I like to have music playing in my head while we're out in the field.[BLOCK][CLEAR]") ]])
            if(sChristineForm == "Human") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Organics cannot play music in their heads...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Of course we can, silly![SOFTBLOCK] I just have to hear it and then I can memorize it![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (I'm glad she didn't ask why I was thinking of spooky music this whole time...)[BLOCK][CLEAR]") ]])
            elseif(sChristineForm == "Darkmatter") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Can you do that with a head that's made of...[SOFTBLOCK] whatever you're made of?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Of course I can, silly![SOFTBLOCK] There aren't many things I *can't* do![BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I object to the usage of music for entertainment while in a dangerous situation.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It has worked out so far, hasn't it?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Stop being a worrywart.[SOFTBLOCK] I switch it to combat music when we get attacked.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You're not going to get far by arguing the point.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Evidently.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Synthwave...[SOFTBLOCK] She has good taste![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Is there any genre more appropriate?[SOFTBLOCK] I think not![BLOCK][CLEAR]") ]])
            end
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay then.[SOFTBLOCK] Let's move out!") ]])
        fnCutsceneBlocker()
    end
end
