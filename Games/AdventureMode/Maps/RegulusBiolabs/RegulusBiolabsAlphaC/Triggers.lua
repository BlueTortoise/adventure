--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "2856Call") then
    
    --Variables.
    local i2856YelledAboutWestward = VM_GetVar("Root/Variables/Chapter5/Scenes/i2856YelledAboutWestward", "N")
    local iSawGammaAirlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawGammaAirlockSequence", "N")
    
    --Dialogue.
    if(i2856YelledAboutWestward == 0.0 and iSawGammaAirlockSequence == 0.0) then
    
        --Flag
        VM_SetVar("Root/Variables/Chapter5/Scenes/i2856YelledAboutWestward", "N", 1.0)

        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Wow, we went five whole minutes without 2856 calling us.[SOFTBLOCK] Whoopee.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] *Click*[SOFTBLOCK] What do you want?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] Hello?[SOFTBLOCK] Christine?[SOFTBLOCK] Are you all right?[SOFTBLOCK] Is there anything I can get you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] Oh dear, I hope I'm not interrupting anything important.[SOFTBLOCK] I just saw you enter range of a network node and thought I'd check up on my dear sister.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] You know she means the world to me.[SOFTBLOCK] I hope your quest is going all right.[SOFTBLOCK] Are you having fun?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] What are you getting at?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] Well, just something I noticed...[SOFTBLOCK] You see, I told you to go north.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] And you're going west.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] BECAUSE YOU ARE SO STUPID YOU CAN'T TELL THE TWO APART, SO I'LL HELP YOU.[SOFTBLOCK] STOP GOING THAT WAY, GO BACK, GO NORTH.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] I tried being nice because it doesn't help to yell at stupid units, you see.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] BUT IT FEELS SO MUCH BETTER.[SOFTBLOCK] NOW GET BACK THERE AND GO NORTH, RIGHT NOW.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Shut it.[SOFTBLOCK] We go where we want, when we want.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] YOU STUPID WORTHLESS PIECE OF - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I know exactly where I'm going, and we're going this way.[SOFTBLOCK] Christine, out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] GET BACK THERE AND - [SOFTBLOCK]*click*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Take me, right now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Just a kiss for now, dearest.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *smooch*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Y-[SOFTBLOCK]yeah.[SOFTBLOCK] Dangerous.[SOFTBLOCK] Have a job to do.[SOFTBLOCK] *But it's so hot when you stand up to her like that...*") ]])
    
    end
end
