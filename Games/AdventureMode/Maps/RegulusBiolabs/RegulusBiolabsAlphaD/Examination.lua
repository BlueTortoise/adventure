--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "ConversionDoor") then
    
    --Variables.
    local iSawConvertedScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawConvertedScene", "N")
    
    --Hasn't seen the scene yet.
    if(iSawConvertedScene == 0.0) then
        
        --Variables.
        local iSX399IsFollowing  = 0
        local iSophieIsFollowing = 0
        local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        if(fnIsCharacterPresent("SX399"))  then iSX399IsFollowing  = 1.0 end
        if(fnIsCharacterPresent("Sophie")) then iSophieIsFollowing = 1.0 end
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawConvertedScene", "N", 1.0)
    
        --Cutscene.
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorConversion") ]])
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Scraprat moves over.
        fnCutsceneMove("Scraprat", 17.25, 20.50)
        fnCutsceneFace("Scraprat", -1, 0)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Scraprat:[VOICE|Narrator] Scraprat helped![SOFTBLOCK] Scraprat helped![SOFTBLOCK] Hee hee, hehehehehe!") ]])
        fnCutsceneBlocker()
        
        --Move everyone onto Christine.
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
    
        --Move the party in.
        fnCutsceneMove("Christine", 14.25, 20.50)
        fnCutsceneMove("Christine", 15.25, 20.50)
        fnCutsceneMove("55", 14.25, 20.50)
        fnCutsceneFace("55", 1, 0)
        if(iSX399IsFollowing == 1.0) then
            fnCutsceneMove("SX399", 14.25, 21.50)
            fnCutsceneMove("SX399", 15.25, 21.50)
            fnCutsceneFace("SX399", 1, 0)
        end
        if(iSophieIsFollowing == 1.0) then
            fnCutsceneMove("Sophie", 14.25, 21.50)
            fnCutsceneFace("Sophie", 1, 0)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Helped with what, little guy?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Scraprat:[VOICE|Narrator] Help help![SOFTBLOCK] Very good![SOFTBLOCK] Hehehehe![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Their linguistic algorithms are quite lacking.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Layer changes.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Floor1AltA", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Walls1AltA", false) ]])
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneFace("55", 0, -1)
        if(iSX399IsFollowing == 1.0) then
            fnCutsceneFace("SX399", 0, -1)
        end
        if(iSophieIsFollowing == 1.0) then
            fnCutsceneFace("Sophie", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Tube:[VOICE|Golem] Conversion completed.[SOFTBLOCK] Disengaging...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Walls1AltB", false) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Spatial estimation matrix online.[SOFTBLOCK] Physical calibrations complete.[SOFTBLOCK] Setting logical tables...[SOFTBLOCK] done.[SOFTBLOCK] Secondary boot complete.[SOFTBLOCK] Exiting conversion chamber.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        
        --SFX.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneTeleport("Converted", 15.25, 19.50)
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Floor1AltA", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Walls1AltA", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Walls1AltB", true) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Superior units located, disengaging core overrides...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I -[SOFTBLOCK] I...[SOFTBLOCK] What happened?[SOFTBLOCK] Did I...[SOFTBLOCK] oh...[SOFTBLOCK] I'm a robot now...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Was that not the expected outcome of stepping into a conversion chamber?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I didn't step into a -[SOFTBLOCK] oh.[SOFTBLOCK] I guess I did?[SOFTBLOCK] I don't remember.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What do you remember?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oops, um.[SOFTBLOCK] Command Unit...[SOFTBLOCK] I am reporting for function assignment.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Your function assignment is to tell me what happened to you, without delay.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Y-[SOFTBLOCK]yes, Command Unit![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I heard the evacuation order over the network, and I started heading to the ranch as ordered.[SOFTBLOCK] But then I went into the transfer tube to the Gamma Labs and...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I'm not sure.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Impressive.[SOFTBLOCK] I didn't remember much when I was converted.[BLOCK][CLEAR]") ]])
        if(sChristineForm ~= "Golem" and sChristineForm ~= "Latex") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] You were...[SOFTBLOCK] converted?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It's a long story.[SOFTBLOCK] Continue, Unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Affirmative, Command Unit.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I am Unit 732115, of the Breeding Program.[SOFTBLOCK] I guess I won't be attending the graduation ceremony...[BLOCK][CLEAR]") ]])
        if(iSophieIsFollowing == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Breeding Program humans are already trained on proper conversion, manufacturing, and inspection procedures.[SOFTBLOCK] Reprogramming is fairly light for them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] There's no reason to rewrite their memories, is there?[SOFTBLOCK] They're practically already golems.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Unit...[SOFTBLOCK] Uh, woah, I can read authenticator chips now...[SOFTBLOCK] Unit 499323 is right.[SOFTBLOCK] But, I don't remember what happened right before I was converted...[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Breeding Program humans are already trained on proper conversion, manufacturing, and inspection procedures.[SOFTBLOCK] Reprogramming is fairly light for them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Only a few routines need to be updated.[SOFTBLOCK] They tend to remember the majority of their organic lives.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Unit...[SOFTBLOCK] Uh, woah, I can read authenticator chips now...[SOFTBLOCK] Command Unit 2855 is right.[SOFTBLOCK] But, I don't remember what happened right before I was converted...[BLOCK][CLEAR]") ]])
        end
    
        if(iSX399IsFollowing == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] The scraprat seems to.[SOFTBLOCK] He's been giggling this whole time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Scraprat: Search and rescue![SOFTBLOCK] Scraprat did good![SOFTBLOCK] Hee hee hee![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] This is probably the first time I've seen them do something useful...[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Scraprat, report.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Scraprat: Search and rescue![SOFTBLOCK] Scraprat did good![SOFTBLOCK] Hee hee hee![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Search and rescue?[SOFTBLOCK] You mean you're useful for something other than self-destructing and banging your head on fabricator benches?[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator][EMOTION|Christine|PDU] If I may, Christine.[SOFTBLOCK] In the Biolabs, unexpected depressurization of an area triggers a search-and-rescue response by local security units.[SOFTBLOCK] Any humans found in the area are recovered.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh no, I must have blacked out when the tube depressurized.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator] If an organic subject is severely damaged, rescue units are authorized to convert them.[SOFTBLOCK] That is likely what happened here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hmm, I remember that a human can survive for a few minutes in a vacuum, though severe brain damage can result.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] So, welcome to the ranks of the mechanical![SOFTBLOCK] It's just great to have you![BLOCK][CLEAR]") ]])
        if(sChristineForm ~= "Golem" and sChristineForm ~= "Latex" and sChristineForm ~= "SteamDroid") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Uhhh...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] As I said, long story.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I suppose we'll have to figure out why the tube depressurized.[SOFTBLOCK] 55, is this area safe?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No safer than any other area of the Biolabs.[SOFTBLOCK] Unit 732115, your assignment is to report to the Raiju Ranch as soon as the passage is safe.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The area is filled with hostiles.[SOFTBLOCK] If necessary, it may be best to move over the surface.[SOFTBLOCK] You are immune to the effects of vacuum now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Affirmative, Command Unit.[SOFTBLOCK] I -[SOFTBLOCK] I guess I'll remain here and make a run for it when I can.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're trying to get the security situation under control.[SOFTBLOCK] If you see something unexpected, run.[SOFTBLOCK] Do not engage.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Of course...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I was really hoping I'd make Lord Golem after I graduated...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move to Christine, fold party.
        fnCutsceneMove("55", 15.25, 20.50)
        if(iSX399IsFollowing == 1.0) then
            fnCutsceneMove("SX399", 15.25, 20.50)
        end
        if(iSophieLeftInBiolabs == 0.0) then
            fnCutsceneMove("Sophie", 15.25, 20.50)
        end
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    --Just open the door.
    else
        AL_SetProperty("Open Door", "DoorConversion")
        AudioManager_PlaySound("World|AutoDoorOpen")
    
    end

--Airlock.
elseif(sObjectName == "AirlockS") then

    --SFX, open door.
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_SetProperty("Open Door", "DoorS")
    AL_SetProperty("Close Door", "DoorN")

elseif(sObjectName == "AirlockN") then

    --If Christine is an organic, we need a warning.
    local sChristineForm  = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iKnowsTubeHoles = VM_GetVar("Root/Variables/Chapter5/Scenes/iKnowsTubeHoles", "N")
    if(sChristineForm == "Human" or sChristineForm == "Raiju") then
    
        --Doesn't know about the holes.
        if(iKnowsTubeHoles == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowsTubeHoles", "N", 1.0)

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[SOFTBLOCK] Secure organic subjects for transport before exiting.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Organ- [EMOTION|Christine|Neutral]oh, right.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] PDU?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator] Accessing local network...[SOFTBLOCK] error... [BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Try to get the airlocks's opcodes.[SOFTBLOCK] I can figure it out from there.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator] Success![SOFTBLOCK] Displaying...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Seems the passage was depressurized about ten minutes after we left the gala, give or take.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The timing is too convenient.[SOFTBLOCK] She's been through here...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Vivify?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No...[SOFTBLOCK] Her disciple.[SOFTBLOCK] Vivify doesn't saw holes in anything.[SOFTBLOCK] She smashes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I agree with that assessment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I can change forms and we can come back this way.[SOFTBLOCK] We won't be going anywhere if I'm squishy like this.") ]])

        --Repeats.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[SOFTBLOCK] Secure organic subjects for transport before exiting.[BLOCK][CLEAR]") ]])
            
            if(fnIsCharacterPresent("Sophie") == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Better transform before going this way...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] What if I kissed you and pinched your nose shut?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And shut my eyes, and formed a seal over my ears?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] A-[SOFTBLOCK]and your...[SOFTBLOCK] uhh...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sounds absurdly risky for little gain.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Y-yeah...[SOFTBLOCK] So let's, uh, not do that, right?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] As if I need an excuse to kiss you...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Risking 771852's life for cheap sexual thrill is wildly inefficient.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] She's far more useful absorbing damage or distracting an enemy gunner.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Things I guess she can't do while kissing me...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yeah, so, I'll transform before we go this way.") ]])
            
            --Sophie isn't here:
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Better change forms before stepping into hard vacuum.[SOFTBLOCK] I don't have much luck breathing vacuum.)") ]])
            end
        end

    --Open it.
    else
    
        --Doesn't know about the holes.
        if(iKnowsTubeHoles == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowsTubeHoles", "N", 1.0)

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[SOFTBLOCK] Exercise caution.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Depressurized? PDU, check the opcodes on the door, please.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator] [EMOTION|Christine|PDU]Downloaded local opcodes. Displaying...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Seems the passage was depressurized about ten minutes after we left the gala, give or take.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The timing is too convenient.[SOFTBLOCK] She's been through here...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Vivify?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No...[SOFTBLOCK] Her disciple.[SOFTBLOCK] Vivify doesn't saw holes in anything.[SOFTBLOCK] She smashes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I agree with that assessment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Stay alert, everyone...") ]])
            
        end
            
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorN") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorS") ]])
    end

--Door.
elseif(sObjectName == "ToGammaC") then

    --Variables.
    local iRepoweredGamma          = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    local iSawGammaAirlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawGammaAirlockSequence", "N")
    local i2856YelledAboutWestward = VM_GetVar("Root/Variables/Chapter5/Scenes/i2856YelledAboutWestward", "N")

    --Door is not repowered.
    if(iRepoweredGamma == 0.0) then

        --First time.
        if(iSawGammaAirlockSequence == 0.0) then

            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSawGammaAirlockSequence", "N", 1.0)

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Hmmm...[SOFTBLOCK] the door's deadbolts were thrown...^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] ^PDU, can you unset the bolts?^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator] ^Negative, Christine.[SOFTBLOCK] The door is receiving power intermittantly, and cannot build capacity to move the bolts.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] ^Huh?[SOFTBLOCK] What do you mean, intermittantly?^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator] ^It appears the load on the power circuit for the Gamma Labs is too high and the distribution computer cannot compensate.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator] ^Shall I contact a repair crew?^[BLOCK][CLEAR]") ]])
            if(fnIsCharacterPresent("Sophie") == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ^Uh, hi.[SOFTBLOCK] Repair crew, reporting.[SOFTBLOCK] What do we need to do?^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^These doors have been sabotaged, Sophie.[SOFTBLOCK] They bolted the door, depressurized the connection tube...^[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] ^PDU, we [SOFTBLOCK]*are*[SOFTBLOCK] the repair crew.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^This is obviously an act of sabotage.[SOFTBLOCK] Bolt the door, depressurize the connection tube...^[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] ^2856, come in please.[SOFTBLOCK] We've got a problem.^[BLOCK][CLEAR]") ]])
            
            --Variation:
            if(i2856YelledAboutWestward == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^YOU STUPID UNIT YOU'VE GOT SOME GALL!^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] ^The door to the Gamma Laboratories is unusable.[SOFTBLOCK] We have to find another way around.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^WHAT?^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^ARE YOU TELLING ME YOU WENT THE WRONG WAY ON PURPOSE AND THEN DOUBLED BACK JUST TO ANGER ME?^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (Lie to her lie to her lie to her lie to her)[SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Yes that is exactly what we did.[SOFTBLOCK] You seem surprised that I'd do it on purpose.^[SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^If I were a betting woman, I'd bet I don't like you.^[SOFTBLOCK][CLEAR]") ]])
                if(fnIsCharacterPresent("SX399") == true) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] ^Gee, I wonder what the odds would be on that...^[SOFTBLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] [EMOTION|Christine|PDU]^YOU UNGRATEFUL AGGRAVATINGLY STUPID INSOLENT LITTLE - ^[SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^The deadbolts are down and there's not enough power to raise them.[SOFTBLOCK] I don't have the tools needed to safely remove the airlock door.^[SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] [EMOTION|Christine|PDU]^Grrrr...^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^My sister has leftover blasting charges from her failed Gala Operation.[SOFTBLOCK] Use those.^[BLOCK][CLEAR]") ]])
            
            --Didn't go west first:
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^Yes?[SOFTBLOCK] Are you at the Gamma Labs yet?^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^The door is unusable.[SOFTBLOCK] We'll find another entrance.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^No, you'll go that way, it's much shorter.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^You have leftover blasting charges, sister.[SOFTBLOCK] Use them.^[BLOCK][CLEAR]") ]])
            end
            
            if(fnIsCharacterPresent("SX399") == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] ^Are you kidding me?^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] ^You blow the airlock into this corridor, and that'll suck all the air right out of the labs!^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] ^Everything in there will die!^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^Very observant.[SOFTBLOCK] Plant the charges.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] ^Don't you dare!^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^I have no intention of using a blasting charge in this manner.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] ^Uh, oops, my bad.[SOFTBLOCK] Sorry, cutie.[SOFTBLOCK] You and your sister's radio signatures are identical.[SOFTBLOCK] I got you mixed up.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] ^And apparently, only one of you has a heart!^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^Stopping Project Vivify takes priority over everything else.[SOFTBLOCK] Organic life in the Gamma Labs can be replaced.[SOFTBLOCK] SO STOP MEWLING AND BLOW THE DOOR.^[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Are you serious right now?^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^I DO NOT HAVE TIME FOR JOKES, MUCH LESS YOUR IDIOTIC WHINING.[SOFTBLOCK] BLOW THE DOOR.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Absolutely not!^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^We refuse.[SOFTBLOCK] Detonating a charge has a higher than thirty percent change of destroying the inner airlock door.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^If destroyed, that will expose this chamber, which you must know is depressurized.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^Every organic in the Gamma Labs will die in a few minutes.^[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ^Stopping Project Vivify takes priority over everything else.[SOFTBLOCK] Organic life in the Gamma Labs can be replaced.[SOFTBLOCK] SO, BLOW THE DOOR.^[BLOCK][CLEAR]") ]])
            end
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Oh, oh no![SOFTBLOCK] Unit 2856?[SOFTBLOCK] You're breaking up!^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Qrrrtkk![SOFTBLOCK] Qrrrt can't krrrk[SOFTBLOCK] hear kkkrrkkr you!^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855][EMOTION|Christine|PDU] ^Are you making static sounds on a digital transmission through a network router?^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] ^Qqrrrrr signal[SOFTBLOCK] kktktktkk jam![SOFTBLOCK] Call ykkrkrt ou later!^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^I don't care if she bought it, let's find a way around.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^The map suggests a passage through the Datacore and Beta labs on the western end.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Then let's go that way.[SOFTBLOCK] And, PDU?[SOFTBLOCK] If Unit 2856 calls, send a bunch of static at her.[SOFTBLOCK] Digital static.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator][EMOTION|Christine|PDU] ^Affirmative.[SOFTBLOCK] I will randomize the static for maximum irritation.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Good work, PDU.[SOFTBLOCK] Let's move out.^") ]])
        
        --Still locked.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The deadbolts on the door are down, and it's not receiving power.[SOFTBLOCK] We can't even pry it open...)") ]])
        end
    
    --Door is repowered.
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsGammaC", "FORCEPOS:37.0x50.0x0")
    
    end

--Object
elseif(sObjectName == "Racks") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The plants in here are all dead from exposure to a vacuum.[SOFTBLOCK] There's no saving them...)") ]])

--Object
elseif(sObjectName == "ConversionChamber") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A conversion chamber, used for lifesaving if an individual is exposed to hard vacuum.[SOFTBLOCK] That scraprat is a real hero!)") ]])

--Object
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like a log of the experiment going on in the connection tube.[SOFTBLOCK] Seems the researchers were testing various species of plant and how they cope with exposure to radiation from space.[SOFTBLOCK] Amazingly, soybeans are the most resilient by an order of magnitude!)") ]])

--Object
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (AUTOMATED NOTICE::[SOFTBLOCK] All monitored subjects are compromised.[SOFTBLOCK] Contact the research lead immediately!)") ]])

--Object
elseif(sObjectName == "Boxes") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Seeds, watering cans, and other gardening equipment.[SOFTBLOCK] The seeds are sorted by 'phenotype' but the labels are just random letters, I think.)") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end