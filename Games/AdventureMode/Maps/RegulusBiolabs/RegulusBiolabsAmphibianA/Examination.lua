--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Variables]
local iListCommands = 0

--[Exits]
if(sObjectName == "ToBetaD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    local iAmphibianPipesReleased = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPipesReleased", "N")
    if(iAmphibianPipesReleased == 0.0) then
        AL_BeginTransitionTo("RegulusBiolabsBetaD", "FORCEPOS:42.5x15.0x0")
    else
        AL_BeginTransitionTo("RegulusBiolabsBetaMeltedD", "FORCEPOS:42.5x15.0x0")
    end
    
elseif(sObjectName == "ToAmphibianBLft") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianB", "FORCEPOS:26.5x17.0x0")
    
elseif(sObjectName == "ToAmphibianBRgt") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianB", "FORCEPOS:33.0x15.0x0")
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's a list of visitor hours along with upcoming events.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then

    --Variables.
    local iAmphibianSaw55Joke   = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianSaw55Joke", "N")
    local iAmphibianGotAuthcode = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N")
    local iSX399JoinsParty      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --First time starting the console:
    if(iAmphibianSaw55Joke == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianSaw55Joke", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Someone left this terminal logged in, 55![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The greatest vulnerability in any system is between the keyboard and the chair.[BLOCK][CLEAR]") ]])
        if(iAmphibianGotAuthcode == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It was not an administrator account, but with some work we can figure out the access codes to the building covertly.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Is this what you do when you're hacking?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] Identify flaws in the architecture and exploit them.[SOFTBLOCK] A logged-in terminal that is unsupervised is one such flaw.[BLOCK][CLEAR]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Messing with computers isn't really my thing.[SOFTBLOCK] Just ask if you need something.[BLOCK][CLEAR]") ]])
            end
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] While there is likely some information on this terminal, we already have the building's authcodes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Right, but this is hacking isn't it?[SOFTBLOCK] Wheee![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Very few units consider hacking to be enjoyable.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I like to think of myself as one of a kind.[BLOCK][CLEAR]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Messing with computers isn't really my thing.[SOFTBLOCK] You two enjoy it.[BLOCK][CLEAR]") ]])
            end
        end

    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Let's see if there's anything useful on the local hard drive...)[BLOCK][CLEAR]") ]])
    end
    
    --Decision setup.
    iListCommands = 1

elseif(sObjectName == "Emails") then
	WD_SetProperty("Hide")
	
    --Variables.
    local iAmphibianGotAuthcode = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N")
    local iAmphibianDecryptAttachment = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianDecryptAttachment", "N")
    
    --Flags.
    if(iAmphibianDecryptAttachment < 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianDecryptAttachment", "N", 1.0)
    end
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Let's see what's in her outbox...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This email is encrypted, which is unusual.[SOFTBLOCK] Fortunately, it is a common pattern.[SOFTBLOCK] I will decrypt it for you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If the correct password is input, the email will appear as though it was entirely corrupted and lost.[SOFTBLOCK] This deflects suspicion.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She probably didn't want her Lord Golem reading her emails.[SOFTBLOCK] Which means it's probably useful for us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Hey Elmira, sorry about missing our get-together the other day.[SOFTBLOCK] My Lord Golem had me clean out the frog habitats.[SOFTBLOCK] You know, again.[SOFTBLOCK] Because she hates me.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Let me make it up to you, okay?[SOFTBLOCK] We can have our next -[SOFTBLOCK] meeting -[SOFTBLOCK] in her office, ideally on her desk.'[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'I put the authcode to the labs in the attachment to this email.[SOFTBLOCK] The attachment password is the common name of your favourite amphibian species.[SOFTBLOCK] See you tomorrow after work!'[BLOCK][CLEAR]") ]])
    if(iAmphibianGotAuthcode == 0.0 and iAmphibianDecryptAttachment == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So the authcodes are in that attachment.[SOFTBLOCK] Can you decode it, 55?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not without additional information.[SOFTBLOCK] Though we could input the password.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hmmm...[BLOCK][CLEAR]") ]])
    end
    
    --Decision setup.
    iListCommands = 1

elseif(sObjectName == "Photos") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Two units posing in front of the lake in the Alpha Labs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Adorable, but not relevant.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] At least we agree they're adorable![BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 1

--Input password.
elseif(sObjectName == "Password") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay, let's give the password a shot...") ]])
    local sScript = "\"" .. fnResolvePath() .. "/PasswordInput.lua" .. "\""
    fnCutsceneInstruction(" WD_SetProperty(\"Activate String Entry\", " .. sScript .. ") ")
    fnCutsceneBlocker()

elseif(sObjectName == "SpeciesList") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A list of the species in the public viewing area, and some information.[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 2

elseif(sObjectName == "Salamanders") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ooh, so many pretty pictures![SOFTBLOCK] 'Salamanders are lizard-like amphibians with distinct legs and a low body.'[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 3

elseif(sObjectName == "Frogs") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Frogs and toads are so cute![SOFTBLOCK] 'Frogs and Toads are amphibians known for hopping, long tongues, and their ribbit calls.'[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Caecilians") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Caecilians...[SOFTBLOCK] exotic and squirmy![SOFTBLOCK] 'The oft-forgotten third amphibian, caecilians are burrowers or swimmers that resemble snakes or worms.'[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 5

--[Species Descriptions]
elseif(sObjectName == "Axolotl") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The axolotl (Code AXOLOTL) is a species of salamander that is predominantly aquatic.[SOFTBLOCK] They do not have lungs, and retain their gills throughout life.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sometimes called 'Walking Fish', axolotls are vital for our research here in Amphibian Genetics for their ability to regenerate lost limbs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] When a limb or other non-essential body part is removed, the axolotl does not scar.[SOFTBLOCK] Instead, the lost part heals entirely.[SOFTBLOCK] It may even produce additional copies, leaving extra limbs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This allows researchers to hold genetic properties constant when performing invasive experiments.[SOFTBLOCK] With time, we hope to allow healing of all organic creatures using insights gained from the axolotls.[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 3
    
elseif(sObjectName == "Olm") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The olm (Code OLM) is a species of salamander that live exclusively in underground caverns.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Living in caverns leaves the olm totally blind, but possessing an enhanced sensory suite in terms of smell and hearing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Their bodies are very long with short limbs, and their skin is fleshy in appearance.[SOFTBLOCK] They have gained the affectionate nickname 'danger noodle' from our staff due to how similar they look to snakes.[SOFTBLOCK] Despite the name, they are totally harmless![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If you wish to pet one of our olms, please be extremely gentle.[SOFTBLOCK] Like all organics, they are very fragile and sensitive.[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 3
    
elseif(sObjectName == "Blackbelly") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The blackbelly salamander (Code BLACKBELLY) is a mostly aquatic salamander native to many terrestrial parts of Pandemonium.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Despite being an amphibian, the blackbelly rarely travels over land more than a few centimeters.[SOFTBLOCK] In fact, over 99pct of their lives are spent in the water, only leaving to forage for food.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Blackbelly salamander eggs require water to incubate correctly.[SOFTBLOCK] Attempts to create artificial incubators without water have failed, though the eggs enter a sort of stasis briefly until water returns, likely an adaptation for dry seasons.[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 3
    
elseif(sObjectName == "Redbelly") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The red-bellied newt (Code REDBELLY) is a highly toxic amphibian noted for its black back scales and bright red tummy scales.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If you wish to handle one, please wear gloves and wash your chassis -[SOFTBLOCK] they are toxic to organics![SOFTBLOCK] Please don't spread the toxins beyond the habitat.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] When threatened, the red-bellied newt deliberately exposes its belly to warn off predators.[SOFTBLOCK] Bright colors in the animal kingdom are an almost sure sign of poison -[SOFTBLOCK] predators, beware![BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 3

elseif(sObjectName == "Bufo") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The common Pandemonium toad (Code BUFO) are very common all across boreal biomes in eastern Pandemonium, specifically Argolis and Entreana.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unlike other toxic species, the bufo toads are not brightly colored.[SOFTBLOCK] In fact, they are generally muddy browns.[SOFTBLOCK] Brief contact is not lethal, but extensive contact causes heart paralysis and death.[SOFTBLOCK] Handle with care, organic guests![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Even today, toads are often associate with witchcraft and magic.[SOFTBLOCK] The Arcane University would like to inform all visitors that toads possess no special arcane properties.[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Lemur") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The lemur frog (Code LEMUR) is a small terrestrial amphibian found in tropical parts of Pandemonium.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Notably, their skin color is green during the day, but changes to brown at night -[SOFTBLOCK] the better to hide from predators.[SOFTBLOCK] They often spend the days hiding beneath leaves and emerge at night to hunt prey.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Lemur frogs are also extremely resistant to high temperatures.[SOFTBLOCK] Most amphibians lose moisture quickly, but the lemur frog's unique skin chemistry protects moisture loss and allows them to sun themselves for long periods.[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Spadefoot") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The spadefoot toad (Code SPADEFOOT) is a small terrestrial amphibian of brownish coloration, native to boreal habitats.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This is one of our 'burrowing' species, which prefers to dig into the soil in moist areas near water sources.[SOFTBLOCK] When the soil is sufficiently wet, they mate and lay eggs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Interestingly, their tadpoles are known for cannibalism when their environments are stressed.[SOFTBLOCK] If the water threatens to dry or food is not sufficient, the tadpoles eat one another.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This causes them to mature rapidly, allowing at least some individuals to survive despite the danger.[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Moor") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The moor frog (Code MOOR) is a small brownish frog with an unspotted belly and typical frog characteristics, such as webbed feet and horizontal pupils.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Moor frogs have an enormous range, inhabiting dry, wet, warm, and cool areas.[SOFTBLOCK] They can even survive in rough tundra.[SOFTBLOCK] Their relatively high range but low locomotion means there are many subspecies which have adapted to local conditions extensively.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] They are also the only species we have here in the public area which like to form breeding choruses.[SOFTBLOCK] Many frogs will get together and sing to attract mates simultaneously.[SOFTBLOCK] We will provide a notification if you would like to see one in action![BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Banded") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The banded bullfrog (Code BANDED) is often associated with gluttony due to its large body but its tiny head and narrow mouth.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Their appetites are voracious -[SOFTBLOCK] they can devour hundreds of ants per day.[SOFTBLOCK] Like many predators they are opportunistic, though we cultivate ants for feeding here in the Biolabs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] When threatened, they puff their bodies up and secrete a mild toxin.[SOFTBLOCK] Not dangerous to larger organics, the usual rules of protection apply. Wash your chassis![BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Iwokramae") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The iwokramae caecilia (Code IWOKRAMAE) is a caecilia species of amphibian which notably has no lungs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Caecilians are often mistaken for worms or snakes, depending on their size, but are in fact amphibians.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Native to tropical biomes, they are unfamiliar to most local humans because they burrow into the soil and live their lives there.[SOFTBLOCK] Many humans have no interactions with caecilia despite living very close to them every day![BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 5

elseif(sObjectName == "Annalatus") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The annulatus caecilia (Code ANNULATUS) is a caecilia species of amphibian which is rare among its genus -[SOFTBLOCK] it secretes toxins![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Toxins are secreted from its rectum as it burrows.[SOFTBLOCK] They are currently a subject of research in order to determine how they use these tocins.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Please use care when handling them, and remember to wash your chassis to prevent cross-contamination with other species.[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 5

elseif(sObjectName == "Rubber") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The rubber eel (Code RUBBER) is a caecilia species of amphibian that is entirely aquatic in habitat.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Despite being aquatic, they still must surface to breathe.[SOFTBLOCK] They are often mistaken for eels, hence the nickname 'rubber eel'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We would like to assure our visitors that the Drone Units at work here have no genetic similarity with these amphibians despite a similar skin sheen and dearth of intelligence.[SOFTBLOCK] That is purely coincidental.[BLOCK][CLEAR]") ]])
    
    --Decision setup.
    iListCommands = 5

elseif(sObjectName == "Reset") then
	WD_SetProperty("Hide")
    --Decision setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    iListCommands = 1
    
elseif(sObjectName == "Exit") then
	WD_SetProperty("Hide")
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end

--[List Commands]
--Base list.
if(iListCommands == 1) then
    
    --Variables.
    local iAmphibianDecryptAttachment = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianDecryptAttachment", "N")
    local iAmphibianGotAuthcode       = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N")
    
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Terminal: Listing commands...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sent Emails\", " .. sDecisionScript .. ", \"Emails\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Photos\",  " .. sDecisionScript .. ", \"Photos\") ")
    if(iAmphibianDecryptAttachment == 1.0 and iAmphibianGotAuthcode == 0.0) then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Password\",  " .. sDecisionScript .. ", \"Password\") ")
    end
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Specimen List\",  " .. sDecisionScript .. ", \"SpeciesList\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Exit\",  " .. sDecisionScript .. ", \"Exit\") ")
    fnCutsceneBlocker()

--Amphibians list.
elseif(iListCommands == 2) then
    
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Terminal: Listing categories...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Salamanders\", " .. sDecisionScript .. ", \"Salamanders\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Frogs and Toads\",  " .. sDecisionScript .. ", \"Frogs\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Caecilians\",  " .. sDecisionScript .. ", \"Caecilians\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Back\",  " .. sDecisionScript .. ", \"Reset\") ")
    fnCutsceneBlocker()

--Salamanders
elseif(iListCommands == 3) then
    
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Terminal: Listing salamanders...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Axolotl\", " .. sDecisionScript .. ", \"Axolotl\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Olm\",  " .. sDecisionScript .. ", \"Olm\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Blackbelly Salamander\",  " .. sDecisionScript .. ", \"Blackbelly\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Red-bellied Bewt\",  " .. sDecisionScript .. ", \"Redbelly\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Back\",  " .. sDecisionScript .. ", \"SpeciesList\") ")
    fnCutsceneBlocker()

--Frogs and Toads
elseif(iListCommands == 4) then
    
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Terminal: Listing frogs and toads...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Bufo Bufo\", " .. sDecisionScript .. ", \"Bufo\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Lemur Frog\", " .. sDecisionScript .. ", \"Lemur\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Spadefoot Toad\", " .. sDecisionScript .. ", \"Spadefoot\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Moor Frog\", " .. sDecisionScript .. ", \"Moor\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Banded Bullfrog\", " .. sDecisionScript .. ", \"Banded\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Back\",  " .. sDecisionScript .. ", \"SpeciesList\") ")
    fnCutsceneBlocker()

--Caecilians
elseif(iListCommands == 5) then
    
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Terminal: Listing caecilians...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Iwokramae Caecilia\", " .. sDecisionScript .. ", \"Iwokramae\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Annulatus Caecilia\", " .. sDecisionScript .. ", \"Annalatus\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Rubber Eel\", " .. sDecisionScript .. ", \"Rubber\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Back\",  " .. sDecisionScript .. ", \"SpeciesList\") ")
    fnCutsceneBlocker()
end
