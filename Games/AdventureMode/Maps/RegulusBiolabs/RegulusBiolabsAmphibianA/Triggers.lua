--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Footprints") then
    
    --Variables.
    local iAmphibianEntranceScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianEntranceScene", "N")
    if(iAmphibianEntranceScene == 1.0) then return end
    
    --Variables.
    local iAmphibianMetDafoda = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianEntranceScene", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N", 0.0)
    
    --Roll the correct answer to the puzzle.
    local iRoll = LM_GetRandomNumber(1, 12)
    
    --If the answer is a frog/toad or higher, add 5 to move it to the next 10's digit.
    if(iRoll >=  5) then iRoll = iRoll + 5 end
    
    --Caecilians move to the 20's digits.
    if(iRoll >= 15) then iRoll = iRoll + 5 end
    
    --Set it.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianCorrectSpecies", "N", iRoll)
    
    --Scene.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well well well...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Footprints.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Someone came this way.[BLOCK][CLEAR]") ]])
    if(iAmphibianMetDafoda == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Might be worth investigating.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The decision is yours.") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Reika must have come this way.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We may be able to follow the tracks to find her.") ]])
    end
    fnCutsceneBlocker()
    
end
