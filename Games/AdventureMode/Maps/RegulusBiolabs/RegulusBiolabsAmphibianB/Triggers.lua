--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--[Secret Room Unlocking]
if(sObjectName == "PoolRoom") then

    local iAmphibianUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N")
    if(iAmphibianUnlockSequence == 0.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N", 1.0)
    elseif(iAmphibianUnlockSequence == 4.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N", 5.0)
        
        --Three sounds.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Chirp") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Chirp") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Chirp") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
        --Open the door.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Position", (4.75 * gciSizePerTile), (11.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "SecretWalls2", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "SecretWallsHi2", false) ]])
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|RemoteDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
    end


elseif(sObjectName == "CoatRoom") then

    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N")
    if(iMovieUnlockSequence == 1.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N", 2.0)
    end
    
elseif(sObjectName == "Office") then
    
    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N")
    if(iMovieUnlockSequence == 3.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N", 4.0)
    end

end
