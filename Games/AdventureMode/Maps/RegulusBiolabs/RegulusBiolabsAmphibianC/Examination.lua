--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToAmphibianB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianB", "FORCEPOS:10.5x8.0x0")
    
elseif(sObjectName == "ToAmphibianD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianD", "FORCEPOS:38.5x29.0x0")
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Lists of shipping and receiving orders.[SOFTBLOCK] This laboratory imports a lot of feedstock from the other parts of the lab, probably to feed its specimens.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The water here is used for tadpole breeding, but it's not in use.[SOFTBLOCK] Good thing, too, because it's far too cold right now.[SOFTBLOCK] Current temperature readout is 1C.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('An algorithm is valid even if it is not executed.[SOFTBLOCK] So then does validity actually mean anything?[SOFTBLOCK] What exactly would constitute an invalid algorithm?[SOFTBLOCK] Seems like a tautology.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An evacuation notice is flashing.[SOFTBLOCK] All of the domiciles above are marked as unoccupied.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Did you notice anything out of place in your office this morning?[SOFTBLOCK] Some of the things on my desk were shifted around, but no cleaners were scheduled to be in here.[SOFTBLOCK] Odd.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Ha ha, Unit 9203 sent me the same thing.[SOFTBLOCK] Axioms, derivations.[SOFTBLOCK] Mostly pseudointellectual nonsense that is basically untestable.[SOFTBLOCK] You're a lot more charitable than I am, it seems.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Automated Notification::[SOFTBLOCK] Unable to contact Beta Laboratories, possible network outage.[SOFTBLOCK] Please contact a repair team.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Oh yeah, I totally read that story![SOFTBLOCK] Was there more than one chapter?[SOFTBLOCK] I loved the part where Alice tries to turn Erica into a doll!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Tandem reading a Professor Killsalot story?[SOFTBLOCK] Wouldn't you rather feed the ducks with me, my sweet?[SOFTBLOCK] And, uh, make out on the park bench?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Proper organic handling and decontamination protocols.[SOFTBLOCK] A videograph is playing showing a Slave Unit washing her chassis, step by step.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Steps 12 through 34 are about making sure the mammary chassis is sparkling clean.[SOFTBLOCK] Very educational.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This oilmaker has been sweetened by Fizzy Pop! which is probably a lethal amount of stimulant.[SOFTBLOCK] Yum!)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end