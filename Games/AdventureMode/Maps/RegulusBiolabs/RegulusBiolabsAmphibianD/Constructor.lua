--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsAmphibianD"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "Biolabs")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusBiolabsAmphibianD")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
    if(iAmphibianRescuedReika < 1.0) then
        fnStandardNPCByPosition("Reika")
        
        --Give Reika the downed emote, and activate it.
        EM_PushEntity("Reika")
            TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Alraune|Wounded")
            TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Alraune|Crouch")
        DL_PopActiveObject()
        
        --Set her to use the wounded frame.
        fnCutsceneSetFrame("Reika", "Wounded")
    end
    
    --[Layer Controllers]
    local iAmphibianCrackedIce = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianCrackedIce", "N")
    if(iAmphibianCrackedIce == 0.0) then
        AL_SetProperty("Set Layer Disabled", "FreezeDoorWallsB", true)
        AL_SetProperty("Set Layer Disabled", "FreezeDoorWallsHiB", true)
        AL_SetProperty("Set Collision", 39, 26, 0, 1)
    else
        AL_SetProperty("Set Layer Disabled", "FreezeDoorWallsA", true)
        AL_SetProperty("Set Layer Disabled", "FreezeDoorWallsHiA", true)
        AL_SetProperty("Set Collision", 39, 26, 0, 0)
    end
end
