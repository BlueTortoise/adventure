--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Rescue Reika!
    if(sActorName == "Reika") then
        
        --Variables.
        local iSawRaijuIntro         = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
        local iSX399JoinsParty       = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        local iAmphibianMetDafoda    = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N")
        local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
        
        --First dialogue.
        if(iAmphibianRescuedReika == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N", 1.0)
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Unnnnggghh....[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hello, can you hear me?[BLOCK][CLEAR]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Time to shine, 399![SOFTBLOCK] I'll warm her right up![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Absolutely do not use your weapon![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Nope![SOFTBLOCK] Steam Lord skills![SOFTBLOCK] C'mere, plant girl![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] There you go![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Ah, you merely increased your power core's clock to heat your chassis.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] In retrospect, I may have been able to do that to the ice wall.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Being clever is really hard.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Fantastic work, SX-399![SOFTBLOCK] She's coming around!") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                
                --Change.
                fnCutsceneSetFrame("Reika", "Crouch")
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutsceneSetFrame("Reika", "Null")
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                
                --Dialogue.
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] C-[SOFTBLOCK]cold...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Cuddle with me.[SOFTBLOCK] I'll warm you up.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Jealous?[SOFTBLOCK] You know, you could probably pull the same trick, cutie-5.[SOFTBLOCK] C'mon...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Who...[SOFTBLOCK] who are you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Units who are rescuing you.[SOFTBLOCK] You do not need to know more.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] In fact, we have some questions for you.[SOFTBLOCK] Did you sabotage the heat transfer system?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] No![SOFTBLOCK] No![SOFTBLOCK] I came here to fix it![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's okay, it's what we figured.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is an authenticator chip in her leafy skirt.[SOFTBLOCK] This is Unit 765532, 'Reika'.[BLOCK][CLEAR]") ]])
                if(iAmphibianMetDafoda == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Dafoda sent us to rescue you, Reika.[SOFTBLOCK] Do you have any medical problems besides the hypothermia?[BLOCK][CLEAR]") ]])
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Nice to meet you, Reika.[SOFTBLOCK] Do you have any medical problems besides the hypothermia?[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] 'Besides' is doing a lot of work in that sentence.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] We're not doctors, unfortunately.[SOFTBLOCK] You'll probably need proper treatment that we can't give.[BLOCK][CLEAR]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But at least you're not frozen![BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Yeah, yeah...[SOFTBLOCK] I'm fine.[SOFTBLOCK] I'll be all right.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So you were trying to fix the heat transfer pipes?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Affirmative.[SOFTBLOCK] There were some explosions and I was in the gamma laboratories at the time.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Then I heard the little ones calling out for help, so I went to investigate.[SOFTBLOCK] The whole beta habitat was rapidly freezing over.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] The heat transfer pipes here pump excess heat out of the city and radiate it into space, then pump the cooled water back in.[SOFTBLOCK] If there's a major leak, it could freeze the entire biolabs out.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] I was trying to get the backup transfer pipe in here open, but when I turned around, a steam leak had frosted over the door.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] I thought I was done for...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Not today![BLOCK][CLEAR]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Yeah![SOFTBLOCK] Look at these heroic units I follow around![BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm sure somebody would have found you...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Of course they would have...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Ungh, Dafoda is going to kill me when I get back.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] I can make my way on my own.[SOFTBLOCK] We Alraunes can talk to plants, I won't even be seen on the way to Biological Services.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are certain you do not require an escort?[BLOCK][CLEAR]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Positive.[SOFTBLOCK] You four would just slow me down.[SOFTBLOCK] I'm fine, really.[BLOCK][CLEAR]") ]])
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Positive.[SOFTBLOCK] You three would just slow me down.[SOFTBLOCK] I'm fine, really.[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If you insist.[SOFTBLOCK] We'll see you back at Biological Services HQ then.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Yeah, thanks.[SOFTBLOCK] Thanks a lot...") ]])
                fnCutsceneBlocker()
            
            --No SX-399.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] I believe I can warm her up with selective discharges...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Wait - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] [SOUND|World|SparksC]YEEEEEOOWWWWWWWWWW!!!!!") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Change.
                fnCutsceneSetFrame("Reika", "Crouch")
                fnCutsceneWait(15)
                fnCutsceneBlocker()
                fnCutsceneSetFrame("Reika", "Null")
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                
                --Dialogue.
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] I'm up![SOFTBLOCK] I'm up![SOFTBLOCK] Yowch![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] We're trying to warm her up, not kill her![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Her skin is covered in a thin layer of water.[SOFTBLOCK] At a low voltage, the electricity discharges evenly across it, warming the skin.[BLOCK][CLEAR]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] You could at least have started with a lower voltage and worked your way up![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I did.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Honestly I prefer my vegetables steamed, *not fried*.[BLOCK][CLEAR]") ]])
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh yes, you like your vegetables nice and crispy.[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] It -[SOFTBLOCK] it worked.[SOFTBLOCK] Owwww...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The efficacy of this approach was not in question.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] I'm not trying to be rude, Command Unit, but who exactly are you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Units who are rescuing you.[SOFTBLOCK] You do not need to know more.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] In fact, we have some questions for you.[SOFTBLOCK] Did you sabotage the heat transfer system?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] No![SOFTBLOCK] No![SOFTBLOCK] I came here to fix it![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's okay, it's what we figured.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is an authenticator chip in her leafy skirt.[SOFTBLOCK] This is Unit 765532, 'Reika'.[BLOCK][CLEAR]") ]])
                if(iAmphibianMetDafoda == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Dafoda sent us to rescue you, Reika.[SOFTBLOCK] Do you have any medical problems besides the hypothermia?[BLOCK][CLEAR]") ]])
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Nice to meet you, Reika.[SOFTBLOCK] Do you have any medical problems besides the hypothermia?[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] 'Besides' is doing a lot of work in that sentence.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You'll have to see a doctor as soon as possible, we're not in position to properly diagnose an organic such as yourself.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Yeah, yeah...[SOFTBLOCK] I'm fine.[SOFTBLOCK] I'll be all right.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So you were trying to fix the heat transfer pipes?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Affirmative.[SOFTBLOCK] There were some explosions and I was in the gamma laboratories at the time.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Then I heard the little ones calling out for help, so I went to investigate.[SOFTBLOCK] The whole beta habitat was rapidly freezing over.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] The heat transfer pipes here pump excess heat out of the city and radiate it into space, then pump the cooled water back in.[SOFTBLOCK] If there's a major leak, it could freeze the entire Biolabs out.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] I was trying to get the backup transfer pipe in here open, but when I turned around, a steam leak had frosted over the door.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] I thought I was done for...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm sure somebody would have found you...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Of course they would have...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Ungh, Dafoda is going to kill me when I get back.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] I can make my way on my own.[SOFTBLOCK] We Alraunes can talk to plants, I won't even be seen on the way to Biological Services.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are certain you do not require an escort?[BLOCK][CLEAR]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Positive.[SOFTBLOCK] You four would just slow me down.[SOFTBLOCK] I'm fine, really.[BLOCK][CLEAR]") ]])
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Positive.[SOFTBLOCK] You two would just slow me down.[SOFTBLOCK] I'm fine, really.[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If you insist.[SOFTBLOCK] We'll see you back at Biological Services HQ then.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Yeah, thanks.[SOFTBLOCK] Thanks a lot...") ]])
                fnCutsceneBlocker()
                
            end
        
        --Repeats.
        else
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Dafoda is probably going to give me a thrashing when I get back...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why?[SOFTBLOCK] You were trying to help.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] I ignored the evacuation order and went alone.[SOFTBLOCK] She hates it when I do that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] But there wasn't anyone else around.[SOFTBLOCK] If I hadn't done what I did, the entire Biolabs would be a freezer by now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We'll vouch for you, then![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[E|Neutral] Thank you for that, and saving my life.[SOFTBLOCK] You're a pretty good bunch.") ]])
        
        end
    end
end