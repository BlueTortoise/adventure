--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToAmphibianC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianC", "FORCEPOS:32.5x4.0x0")
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Yes, it's a game where you collect small animals, train them, and have them fight other small animals.[SOFTBLOCK] It's really fun, not horrible![SOFTBLOCK] They're cartoons, you're not supposed to think about it!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The caecilians don't react to this substace-E crap at all.[SOFTBLOCK] Maybe we got a bad batch?[SOFTBLOCK] I tried it like they said but nothing happened, at all.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Oh yeah, they're filming the next one right now.[SOFTBLOCK] I heard it's being shot on Pandemonium, and the Head of Abductions was absolutely furious.[SOFTBLOCK] There was a big row between her and Head of Recreation and Culture.[SOFTBLOCK] They're still not on speaking terms.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Hey, I'm just a data-entry clerk.[SOFTBLOCK] I don't know any more about server architecture than you do.[SOFTBLOCK] Say, what's your designation?[SOFTBLOCK] It's not coming up on the chat client.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Hello?[SOFTBLOCK] Still there?[SOFTBLOCK] Oh, I guess you went away-from-terminal.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Warning::[SOFTBLOCK] Thermal circulation compromised.[SOFTBLOCK] Please contact a repair crew immediately.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A broken work terminal.[SOFTBLOCK] Guess it's waiting to be shipped for recycling.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Yeah, we've been getting conflicting orders.[SOFTBLOCK] Head of Research said to build more waterproof containers for this substance-E stuff.[SOFTBLOCK] Two days later, build pulse rifle parts and magazines.[SOFTBLOCK] So which is it?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalH") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('One of my units slipped and almost let that substance-E crap into the aquatic breeding habitat.[SOFTBLOCK] What a mess that would have been![SOFTBLOCK] Still, I'm curious to see what would have happened.[SOFTBLOCK] I talked to a researcher from Aquatic Genetics, they've been getting some big results.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalI") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('So we're just not fixing stuff now?[SOFTBLOCK] Head of Research said to fabricate guns, guns, and more guns.[SOFTBLOCK] We can't fix a busted bridge but we can probably make new doors -[SOFTBLOCK] by just shooting the walls out.[SOFTBLOCK] We have that many rounds!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Fabricators, mostly used to make spare pipes and containers for amphibian research.[SOFTBLOCK] There are bits of unfinished items all over the bench.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BluePipeCheck") then
    local iAmphibianBlueRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianBlueRelease", "N")
    if(iAmphibianBlueRelease == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The blue pipes are frozen solid...)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The blue pipes have warm water flowing through them.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "YellowPipeCheck") then
    local iAmphibianYellowRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianYellowRelease", "N")
    if(iAmphibianYellowRelease == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The yellow pipes are frozen solid...)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The yellow pipes have warm water flowing through them.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "PurplePipeCheck") then
    local iAmphibianPurpleRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPurpleRelease", "N")
    if(iAmphibianPurpleRelease == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The purple pipes are frozen solid...)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The purple pipes have warm water flowing through them.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "BluePipeActivate") then
    local iAmphibianBlueRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianBlueRelease", "N")
    if(iAmphibianBlueRelease == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianBlueRelease", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|ValveSqueak][SOFTBLOCK]There, got the reserve pipe opened.)") ]])
        fnCutsceneBlocker()
        
        --Check if all pipes were released. If so, trip this flag.
        local iAmphibianYellowRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianYellowRelease", "N")
        local iAmphibianPurpleRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPurpleRelease", "N")
        local iAmphibianBlueRelease   = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianBlueRelease", "N")
        local iAmphibianRedRelease    = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRedRelease", "N")
        if(iAmphibianBlueRelease == 1.0 and iAmphibianPurpleRelease == 1.0 and iAmphibianRedRelease == 1.0 and iAmphibianYellowRelease == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianPipesReleased", "N", 1.0)
        end
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The blue pipes have warm water flowing through them already.)") ]])
        fnCutsceneBlocker()

    end
    
elseif(sObjectName == "RedPipeActivate") then
    local iAmphibianRedRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRedRelease", "N")
    if(iAmphibianRedRelease == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianRedRelease", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|ValveSqueak][SOFTBLOCK]There, got the reserve pipe opened.)") ]])
        fnCutsceneBlocker()
        
        --Check if all pipes were released. If so, trip this flag.
        local iAmphibianYellowRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianYellowRelease", "N")
        local iAmphibianPurpleRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPurpleRelease", "N")
        local iAmphibianBlueRelease   = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianBlueRelease", "N")
        local iAmphibianRedRelease    = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRedRelease", "N")
        if(iAmphibianBlueRelease == 1.0 and iAmphibianPurpleRelease == 1.0 and iAmphibianRedRelease == 1.0 and iAmphibianYellowRelease == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianPipesReleased", "N", 1.0)
        end
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The red pipes have warm water flowing through them already.)") ]])
        fnCutsceneBlocker()
    end
    
    
elseif(sObjectName == "PurplePipeActivate") then
    local iAmphibianPurpleRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPurpleRelease", "N")
    if(iAmphibianPurpleRelease == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianPurpleRelease", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|ValveSqueak][SOFTBLOCK]There, got the reserve pipe opened.)") ]])
        fnCutsceneBlocker()
        
        --Check if all pipes were released. If so, trip this flag.
        local iAmphibianYellowRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianYellowRelease", "N")
        local iAmphibianPurpleRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPurpleRelease", "N")
        local iAmphibianBlueRelease   = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianBlueRelease", "N")
        local iAmphibianRedRelease    = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRedRelease", "N")
        if(iAmphibianBlueRelease == 1.0 and iAmphibianPurpleRelease == 1.0 and iAmphibianRedRelease == 1.0 and iAmphibianYellowRelease == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianPipesReleased", "N", 1.0)
        end
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The purple pipes have warm water flowing through them already.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "YellowPipeActivate") then
    local iAmphibianYellowRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianYellowRelease", "N")
    if(iAmphibianYellowRelease == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianYellowRelease", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|ValveSqueak][SOFTBLOCK]There, got the reserve pipe opened.)") ]])
        fnCutsceneBlocker()
        
        --Check if all pipes were released. If so, trip this flag.
        local iAmphibianYellowRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianYellowRelease", "N")
        local iAmphibianPurpleRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPurpleRelease", "N")
        local iAmphibianBlueRelease   = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianBlueRelease", "N")
        local iAmphibianRedRelease    = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRedRelease", "N")
        if(iAmphibianBlueRelease == 1.0 and iAmphibianPurpleRelease == 1.0 and iAmphibianRedRelease == 1.0 and iAmphibianYellowRelease == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianPipesReleased", "N", 1.0)
        end
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The yellow pipes have warm water flowing through them already.)") ]])
        fnCutsceneBlocker()
    end

--Frozen door, plays a scene.
elseif(sObjectName == "FrozenDoor") then
    local iAmphibianCrackedIce = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianCrackedIce", "N")
    if(iAmphibianCrackedIce == 1.0) then return end
    
    --Get variables.
    local iAmphibianYellowRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianYellowRelease", "N")
    local iAmphibianPurpleRelease = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPurpleRelease", "N")
    local iAmphibianBlueRelease   = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianBlueRelease", "N")
    local iAmphibianRedRelease    = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRedRelease", "N")
    
    --If any one of these is a zero, we can't open the door:
    if(iAmphibianBlueRelease == 0.0 or iAmphibianPurpleRelease == 0.0 or iAmphibianRedRelease == 0.0 or iAmphibianYellowRelease == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The ice is too firm to break.[SOFTBLOCK] Maybe releasing the reserve pipes will weaken it?)") ]])
        fnCutsceneBlocker()
        
    --Break it.
    else
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianCrackedIce", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] All right, let's give this a crack.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        --Crack that ice!
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Firework0") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Firework1") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Firework2") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Firework1") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Ice") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FreezeDoorWallsA", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FreezeDoorWallsHiA", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FreezeDoorWallsB", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FreezeDoorWallsHiB", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Collision", 39, 26, 0, 0) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] That did it!") ]])
        fnCutsceneBlocker()
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end