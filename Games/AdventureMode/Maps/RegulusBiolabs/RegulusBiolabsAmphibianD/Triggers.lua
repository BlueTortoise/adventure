--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Alraune") then
    
    --Repeat set.
    local iAmphibianSawReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianSawReika", "N")
    if(iAmphibianSawReika == 1.0) then return end
    
    --Variables.
    local iSawRaijuIntro      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
    local iSX399JoinsParty    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iAmphibianMetDafoda = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianSawReika", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 39.75, 28.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 40.75, 28.50)
    fnCutsceneMove("55", 39.75, 28.50)
    fnCutsceneBlocker()
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("Christine", 41.75, 28.50)
        fnCutsceneMove("55", 40.75, 28.50)
        fnCutsceneMove("SX399", 39.75, 28.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 43.25, 27.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneMove("55", 42.25, 27.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneMove("SX399", 41.25, 27.50)
        fnCutsceneFace("SX399", 0, -1)
        if(iSawRaijuIntro == 0.0) then
            fnCutsceneMove("Sophie", 42.25, 28.50)
            fnCutsceneFace("Sophie", 0, -1)
        end
    else
        fnCutsceneMove("Christine", 43.25, 27.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneMove("55", 42.25, 27.50)
        fnCutsceneFace("55", 0, -1)
        if(iSawRaijuIntro == 0.0) then
            fnCutsceneMove("Sophie", 42.25, 28.50)
            fnCutsceneFace("Sophie", 0, -1)
        end
    end
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Oh no![SOFTBLOCK] There's someone in there![BLOCK][CLEAR]") ]])
    if(iAmphibianMetDafoda == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] That's got to be Reika.[SOFTBLOCK] But how did she get in there?[SOFTBLOCK] Why is she in there?[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Any idea who that popsicle is, 55?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Negative.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am detecting life signs.[SOFTBLOCK] She is unconscious and likely suffering hypothermia.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We better move quick, then.[BLOCK][CLEAR]") ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Time to shine, 399![SOFTBLOCK] I have a melting fission gun right here![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hold your fire, that is an order.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Right-o, boss![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Wait, what?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] These pipes are already at risk of burst due to freezing.[SOFTBLOCK] Any unregulated increase in temperature may vaporize the water within and trigger a burst.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A burst could very well kill the girl we are trying to save.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] But -[SOFTBLOCK] I can -[SOFTBLOCK] what if I...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is my evaluation.[SOFTBLOCK] I estimate a 30pct chance of a rupture.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Yeah, nuclear explosions are hard to predict.[SOFTBLOCK] Makes for good weapons, not so great for cutting tools.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I said I'd follow your orders, didn't I?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's got to be a way in there, but the door is frozen over.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If we open the release valves, we may be able to reroute warm water through these pipes.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Look for the valves in this area.[SOFTBLOCK] Melting the ice safely will require opening all of them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative, find the release valves.[SOFTBLOCK] Let's get going!") ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
