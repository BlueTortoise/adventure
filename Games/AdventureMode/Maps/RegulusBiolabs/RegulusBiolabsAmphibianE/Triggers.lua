--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Discussion") then
    
    --Repeat check.
    local iAmphibianUnlockDialogue = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockDialogue", "N")
    if(iAmphibianUnlockDialogue == 1.0) then return end
        
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockDialogue", "N", 1.0)
    
    --Variables.
    local iSawMovieIntro   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMovieIntro", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Interesting.[SOFTBLOCK] A secret room?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unlikely.[SOFTBLOCK] It is more likely that the public area was repurposed and this entrance was merely blocked off.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As to why such an exotic trigger method was selected, rather than the simple use of an administrator's console, is not obvious.[BLOCK][CLEAR]") ]])
    if(iSawMovieIntro == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I have no idea.[SOFTBLOCK] I just did random things when I heard a chirping noise.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Strange.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Regardless, it seems there is equipment here for us to purloin.") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Uh yeah...[SOFTBLOCK] about that...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Your body language suggests you are about to make a mistake.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Well...[SOFTBLOCK] Sophie and I may have watched a silly movie about a moth girl...[BLOCK][CLEAR]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This sounds very relevant.[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This sounds very relevant.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Christine has good taste in videographs.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] It was shot in the Biolabs.[SOFTBLOCK] In fact, some of it was recorded in this room.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I see.[SOFTBLOCK] Excellent reconnaissance, Unit 771852.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You're not mad?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] As I said, your body language suggested you were about to make a mistake.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] That mistake is always assuming I will be upset by your...[SOFTBLOCK] lapses in judgement.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, okay?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am not an authority figure, I am your partner.[SOFTBLOCK] I do not always agree with your choices, but you have a good instinct.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please do not avoid telling me things because you are worried I will judge you harshly.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Wow![SOFTBLOCK] Okay, 55![SOFTBLOCK] Great![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Wait a minute...[SOFTBLOCK] That sounds vaguely familiar...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] I borrowed it from episode 802 of 'All My Processors'.[BLOCK][CLEAR]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] All my whatnow?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's a show they record in Regulus City.[SOFTBLOCK] It has...[SOFTBLOCK] a lot of seasons...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] And you watch it, 55?[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] You're a fan?[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I sometimes watch the episodes while waiting for replies during correspondence, yes.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do not read any further into that than necessary.[BLOCK][CLEAR]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Too late.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Reading way too far into it![BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Suit yourself, 55.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] At any rate, there is equipment for us to purloin.[SOFTBLOCK] Let us proceed.") ]])
        
    end
    fnCutsceneBlocker()
end
