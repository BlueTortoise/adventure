--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToBiolabsDatacoreH") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsDatacoreH", "FORCEPOS:11.0x4.0x0")
    
--[Objects]
elseif(sObjectName == "Box") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Spare organic rations, possibly for a human interning here.[SOFTBLOCK] They're frozen solid.)") ]])
    
elseif(sObjectName == "Shelf") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A broken handheld gaming console.[SOFTBLOCK] Wait -[SOFTBLOCK] the battery is just dead![SOFTBLOCK] Sheesh, assign a repair unit already!)") ]])
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('All Units::[SOFTBLOCK] Evacuate to Transit Station Omicron.[SOFTBLOCK] Defragmentation permissions suspended.')") ]])
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The screen is frozen over, but I can make out some of the text underneath.[SOFTBLOCK] Looks like an apology email to someone.)") ]])
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I can't see much of the screen underneath the ice, but it's clearly an evacuation order.)") ]])
    
elseif(sObjectName == "Glass") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The glass was shattered with an incredible amount of force...)") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end