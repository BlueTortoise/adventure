--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "Wine") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Barrels of fermenting wine.[SOFTBLOCK] I'm sure these will be delicious in a few months!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bag") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The bag is full of plastic cards with descriptions of wine and beer flavours on them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like someone left a chat session open.[SOFTBLOCK] PJen27:: 'Got a note from Daisy today.[SOFTBLOCK] She said she wanted to talk about something really important, and that she'd be over at the Raiju Ranch, waiting for me to get off work.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Satch:: 'Woah, you think she's going to ask you go steady?')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (PJen27:: 'Dunno![SOFTBLOCK] Dunno!')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (PJen27:: 'Ah crud, my Lord Golem is telling me we need to go.[SOFTBLOCK] I'll try to slip away and go visit Daisy when she's not looking.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Satch:: 'Why are you going?[SOFTBLOCK] I just heard an explosion, are you okay?')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Satch:: 'Hello?')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Satch:: 'Aw geez.[SOFTBLOCK] Nobody is messaging me back all of the sudden.[SOFTBLOCK] Hope you're okay.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Attention all winery staff::[SOFTBLOCK] Evacuate immediately to transit station Omicron.[SOFTBLOCK] This is not a drill.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Shelf") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An assortment of cheeses, for wine tastings.[SOFTBLOCK] Goat, sheep, cow...[SOFTBLOCK] human...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Wine Fermentation Tour Scheduling...[SOFTBLOCK] List of available hours...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Glass") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The glass here was shattered by an immense force...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Barrel") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Barrels of alcohol, presumably for guests.[SOFTBLOCK] Alcohol doesn't have an inebriating effect on golems, but it can taste good with the right cheese!)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end