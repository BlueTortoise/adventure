--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Lord Golem, I dropped off the parts you wanted dumped.[SOFTBLOCK] I saw a whole bunch of units going towards the transit station.[SOFTBLOCK] Is something going on?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Storage area::[SOFTBLOCK] 10pct capacity used.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ShelfA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Broken construction tools, awaiting recycling probably.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ShelfB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A busted circuit board.[SOFTBLOCK] Someone covered it in solder and then snapped it in half...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ShelfC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Wiring and parts of a terminal frame.[SOFTBLOCK] Useless.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ShelfD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A keyboard, except half of the keys have been removed and none of the springs work correctly.[SOFTBLOCK] Junk to be recycled.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end