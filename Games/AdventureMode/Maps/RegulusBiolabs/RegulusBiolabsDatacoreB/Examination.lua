--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToDatacoreCRgt") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsDatacoreC", "FORCEPOS:37.0x6.0x0")
    
elseif(sObjectName == "Elevator") then
    
    --Variables.
    local iFixedDatacoreElevator  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedDatacoreElevator", "N")
    local iBiolabsFoundRedKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N")
    
    --No keycard:
    if(iBiolabsFoundRedKeycard == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: [SOUND|World|AutoDoorFail] Elevator out of service.[SOFTBLOCK] Please contact a repair crew.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I can probably fix this elevator as a shortcut, but it won't go anywhere without a red keycard.)") ]])
        fnCutsceneBlocker()
    
    --Keycard. Fix the elevator.
    elseif(iBiolabsFoundRedKeycard == 1.0 and iFixedDatacoreElevator == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedDatacoreElevator", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: [SOUND|World|AutoDoorFail] Elevator out of service.[SOFTBLOCK] Please contact a repair crew.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] PDU, can you reset the message buffer for me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Affirmative, Unit 771852.[SOFTBLOCK] The elevator's message buffer has been wiped.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[SOFTBLOCK] Yep, that did it.[SOFTBLOCK] Software error.[SOFTBLOCK] Since we have the keycard we can take this upstairs now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I'll leave a note for that Lord Unit over there, but it's up to her if she's going to make a run for it.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreF", "FORCEPOS:20.5x14.0x0") ]])
        fnCutsceneBlocker()
    
    --Repeats.
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsDatacoreF", "FORCEPOS:20.5x14.0x0")
    end
    
--[Objects]
elseif(sObjectName == "Boxes") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Assorted office supplies.[SOFTBLOCK] Spare batteries, replacement power cables, nothing useful.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Laptop") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's an email on the laptop here ordering an immediate evacuation.[SOFTBLOCK] Seems it came too late...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "DoorN") then
    
    --Form checker. Don't allow organics past this point.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Human" or sChristineForm == "Raiju") then
        
        --Variables:
        local iHasLatexForm         = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
        local iHasDarkmatterForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
        local iHasEldritchForm      = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
        local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
        local iHasSteamDroidForm    = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
        local iSawRaijuIntro        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[SOFTBLOCK] Secure organic subjects for transport before exiting.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Better change to an inorganic form before I go outside...)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        if(iHasDarkmatterForm == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"TFToDarkmatter\") ")
        end
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"TFToGolem\") ")
        if(iHasLatexForm == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"TFToLatex\") ")
        end
        if(iHasEldritchForm == 1.0 and iSawRaijuIntro == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Dreamer\",  " .. sDecisionScript .. ", \"TFToEldritch\") ")
        end
        if(iHasElectrospriteForm == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Electrosprite\",  " .. sDecisionScript .. ", \"TFToElectrosprite\") ")
        end
        if(iHasSteamDroidForm == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Steam Droid\",  " .. sDecisionScript .. ", \"TFToSteam\") ")
        end
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"NoCancel\") ")
        fnCutsceneBlocker()
    
    --Normal case:
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_SetProperty("Close Door", "DoorS")
        AL_SetProperty("Open Door", "DoorN")
    end
    
    
elseif(sObjectName == "DoorS") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_SetProperty("Close Door", "DoorN")
    AL_SetProperty("Open Door", "DoorS")
    
--[Form Changers]
elseif(sObjectName == "TFToGolem") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToGolem/Scene_Begin.lua")
    
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorS") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorN") ]])
    
elseif(sObjectName == "TFToLatex") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToLatexDrone/Scene_Begin.lua")
    
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorS") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorN") ]])
    
elseif(sObjectName == "TFToDarkmatter") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToDarkmatter/Scene_Begin.lua")
    
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorS") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorN") ]])
    
elseif(sObjectName == "TFToEldritch") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToEldritch/Scene_Begin.lua")
    
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorS") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorN") ]])
    
elseif(sObjectName == "TFToElectrosprite") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToElectrosprite/Scene_Begin.lua")
    
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorS") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorN") ]])
    
elseif(sObjectName == "TFToSteam") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToSteamDroid/Scene_Begin.lua")
    
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorS") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorN") ]])
    
elseif(sObjectName == "NoCancel") then
	WD_SetProperty("Hide")
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end