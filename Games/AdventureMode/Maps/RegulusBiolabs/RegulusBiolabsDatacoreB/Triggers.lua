--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "SceneTriggerA") then
    
    --Variables
    local iDatacoreSawScaredGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iDatacoreSawScaredGolem", "N")
    if(iDatacoreSawScaredGolem == 0.0) then
        
        --Variables.
        local bIsSX399Present = fnIsCharacterPresent("SX399")
        local sChristineForm  = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iDatacoreSawScaredGolem", "N", 1.0)
        
        --Scene.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] ^Who's there?[SOFTBLOCK] Who are you?[SOFTBLOCK] Please don't hurt me!^") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Space out.
        fnCutsceneMove("Christine", 30.25, 24.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneMove("55", 29.25, 24.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneMove("Sophie", 31.25, 24.50)
        fnCutsceneFace("Sophie", 0, -1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 32.25, 24.50)
            fnCutsceneFace("SX399", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue setup.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        
        --Varies by form.
        if(sChristineForm == "Golem") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Are you security units?[SOFTBLOCK] You must help!^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^We are under no obligations to help you.[SOFTBLOCK] We need to pass through the datacore's offices.[SOFTBLOCK] Give us your access codes.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Ease off a tick, 55.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^I -[SOFTBLOCK] I can't give you my access codes![SOFTBLOCK] I don't have the keycard!^[BLOCK][CLEAR]") ]])
            
        elseif(sChristineForm == "LatexDrone") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Are you security units?[SOFTBLOCK] You must help!^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^We are under no obligations to help you.[SOFTBLOCK] We need to pass through the datacore's offices.[SOFTBLOCK] Give us your access codes.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Ease off a tick, 55.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Did that Drone Unit -[SOFTBLOCK] nevermind.[SOFTBLOCK] Not important.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^I can't give you my access codes![SOFTBLOCK] I don't have the keycard.^[BLOCK][CLEAR]") ]])
            
        elseif(sChristineForm == "Electrosprite") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^A -[SOFTBLOCK] what are you?^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^Security units.[SOFTBLOCK] That is all you need to know.[SOFTBLOCK] We require datacore access codes.[SOFTBLOCK] Give them to us.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Slow down there, 55.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Are you -[SOFTBLOCK] no, no.[SOFTBLOCK] Trust my Command Unit's judgement.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^I'm sorry, I can't give you my access codes.[SOFTBLOCK] I don't have the keycard.^[BLOCK][CLEAR]") ]])
            
        elseif(sChristineForm == "SteamDroid") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Help -[SOFTBLOCK] you must help![SOFTBLOCK] Please.[SOFTBLOCK] I know our peoples are in conflict, but we have mutual enemies now![SOFTBLOCK] I can see to it you are rewarded!^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^Straight to buying me off, eh?^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^These are my security squad.[SOFTBLOCK] Unorthodox, but effective.[SOFTBLOCK] That is all you need to know.[SOFTBLOCK] We require datacore access codes.[SOFTBLOCK] Give them to us.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Are you -[SOFTBLOCK] no, no.[SOFTBLOCK] Trust my Command Unit's judgement.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^I'm sorry, I can't give you my access codes.[SOFTBLOCK] I don't have the keycard.^[BLOCK][CLEAR]") ]])
            
        elseif(sChristineForm == "Darkmatter") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Command Unit, please help![SOFTBLOCK] I -[SOFTBLOCK] is that Darkmatter following you?^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^Do not become so easily distracted.[SOFTBLOCK] We require datacore access codes.[SOFTBLOCK] Give them to us.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^I am a member of the security entourage of this Command Unit, for the time being, hardmatter.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Is this -[SOFTBLOCK] I'm sorry, but the scientific leaps -[SOFTBLOCK] no no.[SOFTBLOCK] I must contain myself, and deal with the task at hand.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^I'm sorry, I can't give you my access codes.[SOFTBLOCK] I don't have the keycard.^[BLOCK][CLEAR]") ]])
        
        end

        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ^Then you are useless.[SOFTBLOCK] Stand aside.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Ma'am, we have an important objective, more important than rescuing you.[SOFTBLOCK] The fate of the city rests on us.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ^(Christine, why are you lying?[SOFTBLOCK] She's one of the bad robots, isn't she?)^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^(She's a potential ally, or a prisoner.[SOFTBLOCK] Even if you hate them, you can't mistreat them.)^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ^(But I don't hate her...[SOFTBLOCK] Even though I should, I just can't...)^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^(We'll need to talk more about this later.[SOFTBLOCK] For now, this place is dangerous.[SOFTBLOCK] We need to keep moving.)^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^The elevator broke when we were doing a routine upgrade on the server.[SOFTBLOCK] I checked the comms and there's an evacuation notice.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Geneva -[SOFTBLOCK] I mean, Unit 702339, went to check if she could find an access ladder.[SOFTBLOCK] I heard a distress call on the radio and...[SOFTBLOCK] she didn't come back...^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^I presume, then, that this Unit 702339 has the access card we need.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Yes, we always keep a spare if we get locked out.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^But what should I do?[SOFTBLOCK] There are[SOFTBLOCK] - things -[SOFTBLOCK] all around![SOFTBLOCK] They could get me!^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Your offices are over there, right?[SOFTBLOCK] Go in, drop the door's deadbolts, and lock yourself off.[SOFTBLOCK] Wait for rescue.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^I'm sure you can find a copy of solitaire to entertain yourself with.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^C-[SOFTBLOCK]can't I come with you?^[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] ^Sorry, toutse.[SOFTBLOCK] Can't have a civilian in my firing line.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^What my mercenary friend here means is, you are in greater danger with us than without.^[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ^I think it's best if you...[SOFTBLOCK] uh...[SOFTBLOCK] don't.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^I was not speaking to you.^[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^My [SOFTBLOCK]- lieutenant? -[SOFTBLOCK] is correct.[SOFTBLOCK] You are in greater danger with us than without.^[BLOCK][CLEAR]") ]])
        end
        
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^It is unlikely you will be able to evade capture.[SOFTBLOCK] Barricade yourself in place.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^If you do need to leave in a hurry, everyone is rallying at the Raiju Ranch.[SOFTBLOCK] Go there.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^And...[SOFTBLOCK] Geneva?^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Likely retired.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ^55!^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^No, no.[SOFTBLOCK] I must accept that. I will be delighted if she is not, but...[SOFTBLOCK] this is a crisis.[SOFTBLOCK] I cannot allow my feelings to make me another victim.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ^Thank you, security units.[SOFTBLOCK] I'll lock myself in my office and wait for rescue.^") ]])
        fnCutsceneBlocker()
        
        --Fold party.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end
end
