--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('You are wrong about everything.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It's not an email, it's just what was on the terminal.[SOFTBLOCK] Pressing any key just reprints the message.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('It's cold where we are.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('It went inside her, and I watched it.[SOFTBLOCK] It writhed and squirmed and it came back out, and she wasn't there any more.[SOFTBLOCK] Now, it's inside me.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Body") then
    
    --Variables.
    local bIsSophiePresent = fnIsCharacterPresent("Sophie")
    local bIsSX399Present  = fnIsCharacterPresent("SX399")
    local iBiolabsFoundRedKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N")
    local iHasDarkmatterForm      = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N", 1.0)
    
    --First time.
    if(iBiolabsFoundRedKeycard == 0.0) then
        
        --Switch the last save point to this room. Otherwise if the player passes the savepoint and surrenders, they go to the Alpha Labs instead.
        AL_SetProperty("Last Save Point", "RegulusBiolabsDatacoreD")
        
        --Topics.
        WD_SetProperty("Unlock Topic", "Biolabs_Darkmatters", 1)
        
        --Spawn a Darkmatter.
        TA_Create("Darkmatter")
            TA_SetProperty("Position", -100, -100)
            TA_SetProperty("Facing", gci_Face_North)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
        DL_PopActiveObject()
        
        --Spawn enemies.
        AL_SetProperty("Wipe Destroyed Enemies")
        fnStandardEnemyPulse()
        
        --Party moves over.
        fnCutsceneMove("Christine", 4.25, 6.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneMove("55", 3.25, 5.50)
        fnCutsceneFace("55", 1, 0)
        if(bIsSophiePresent == true) then
            fnCutsceneMove("Sophie", 5.25, 5.50)
            fnCutsceneFace("Sophie", -1, 0)
        end
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 4.25, 4.50)
            fnCutsceneFace("SX399", 0, 1)
        end
        
        --Sophie and SX-399 are here.
        if(bIsSophiePresent == true and bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess that accounts for the missing unit...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Shame they didn't do more damage.[SOFTBLOCK] She's barely scratched and not melted at all.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Please don't glorify violence.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Just an observation, boss.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[SOFTBLOCK] No, no, no good.[SOFTBLOCK] She's long gone.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] There's nothing I can do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I know.[SOFTBLOCK] Maybe if we had gotten here sooner...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Did you really intend to save her?[SOFTBLOCK] The enemy?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] She's not the enemy until she picks up a gun.[SOFTBLOCK] Until then, she's a civilian.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Are you kidding?[SOFTBLOCK] Do you have any idea what the Lords have done?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] *Ahem*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Please inform me as to what sort of things are to be forgiven, and what are not.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Whatever standard you set, I assure you, I do not meet it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] But...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] But you're different...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] How.[SOFTBLOCK] State how.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] I...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] [EMOTION|2855|Neutral]If 55 can change, then this Lord Unit could, too.[SOFTBLOCK] You can't treat her differently.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Uhhh...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I know, I know.[SOFTBLOCK] We have to be evenhanded.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Just, sometimes the things I saw out in the mines, or the things mother told me...[SOFTBLOCK] they stuck with me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But how do I know the difference between a good golem, and a bad one?[SOFTBLOCK] How many are Christines?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] There isn't a difference.[SOFTBLOCK] The good ones [SOFTBLOCK]*are*[SOFTBLOCK] the bad ones.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] The Lord Units don't see us any differently than you see a discarded pulse-rifle cell.[SOFTBLOCK] To be thrown away when it is not useful.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] The truly cruel ones will get theirs in the end...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ...[SOFTBLOCK] but to paint them all with the same brush is to make the same mistake they made about you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hate feeds on hate.[SOFTBLOCK] To end it, we must erase its source.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] All this because of a simple statement...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I get it.[SOFTBLOCK] Say, didn't we come here for a keycard?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] [SOUND|World|Keycard]Like this one?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: (Received Red Keycard!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Great, back to the mission!") ]])
            fnCutsceneBlocker()
        end
        
        --Wait.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Everyone looks south.
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneFace("55", 0, 1)
        if(bIsSophiePresent == true) then
            fnCutsceneFace("Sophie", 0, 1)
        end
        if(bIsSX399Present == true) then
            fnCutsceneFace("SX399", 0, 1)
        end
        
        --Teleport the Darkmatter in place.
        fnCutsceneTeleport("Darkmatter", 5.25, 10.50)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Darkmatter moves up.
        fnCutsceneMove("Darkmatter", 5.25, 8.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --If Christine has Darkmatter form:
        if(iHasDarkmatterForm == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
            
            --SX-399 is present, Sophie is present:
            if(bIsSX399Present == true and bIsSophiePresent == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Far-out![SOFTBLOCK] It's a Darkmatter Girl![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Does that mean you're here to help?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I can talk to her.[SOFTBLOCK] Don't worry, I'll translate.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You will stop here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She says - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are able to understand her.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You are?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You cannot leave this place.[SOFTBLOCK] We are sorry for this.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey, hold on.[SOFTBLOCK] What's going on?[SOFTBLOCK] I'm one of you -[SOFTBLOCK] just, ask - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You must die.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Now there's no need for this.[SOFTBLOCK] Don't you recognize me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Christine.[SOFTBLOCK] Bearer of the runestone.[SOFTBLOCK] You have helped us cleanse the taint of Serenity Crater.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] That taint nucleates within you.[SOFTBLOCK] It has followed you across the divide to this place.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You have seen the future.[SOFTBLOCK] You have seen what you become.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Is she talking about...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] B-[SOFTBLOCK]but she can control herself...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The future isn't set.[SOFTBLOCK] We can avoid that fate.[SOFTBLOCK] Give me a chance.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Please do not resist.[SOFTBLOCK] We owe you much.[SOFTBLOCK] It will not hurt.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not happening.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We know.[SOFTBLOCK] We know that we fail.[SOFTBLOCK] You leave this place, and continue to where you will be.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] But we must try.[SOFTBLOCK] We are sorry.") ]])
            
            --Just SX-399.
            elseif(bIsSX399Present == true and bIsSophiePresent == false) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Far-out![SOFTBLOCK] It's a Darkmatter Girl![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Does that mean you're here to help?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I can talk to her.[SOFTBLOCK] Don't worry, I'll translate.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You will stop here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She says - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are able to understand her.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You are?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You cannot leave this place.[SOFTBLOCK] We are sorry for this.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey, hold on.[SOFTBLOCK] What's going on?[SOFTBLOCK] I'm one of you -[SOFTBLOCK] just, ask - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You must die.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Now there's no need for this.[SOFTBLOCK] Don't you recognize me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Christine.[SOFTBLOCK] Bearer of the runestone.[SOFTBLOCK] You have helped us cleanse the taint of Serenity Crater.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] That taint nucleates within you.[SOFTBLOCK] It has followed you across the divide to this place.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You have seen the future.[SOFTBLOCK] You have seen what you become.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Is she talking about...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] We should have known better than to trust these creatures.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Please 55.[SOFTBLOCK] Not now.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The future isn't set.[SOFTBLOCK] We can avoid that fate.[SOFTBLOCK] Give me a chance.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Please do not resist.[SOFTBLOCK] We owe you much.[SOFTBLOCK] It will not hurt.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not happening.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We know.[SOFTBLOCK] We know that we fail.[SOFTBLOCK] You leave this place, and continue to where you will be.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] But we must try.[SOFTBLOCK] We are sorry.") ]])
            
            --Just Sophie.
            elseif(bIsSX399Present == false and bIsSophiePresent == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hello night lady![SOFTBLOCK] Are you one of Christine's friends?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Of course![SOFTBLOCK] Don't worry, I'll translate, I can understand them.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You will stop here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] [EMOTION|Sophie|Neutral]She says - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are able to understand her.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You are?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You cannot leave this place.[SOFTBLOCK] We are sorry for this.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey, hold on.[SOFTBLOCK] What's going on?[SOFTBLOCK] I'm one of you -[SOFTBLOCK] just, ask - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You must die.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Now there's no need for this.[SOFTBLOCK] Don't you recognize me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Christine.[SOFTBLOCK] Bearer of the runestone.[SOFTBLOCK] You have helped us cleanse the taint of Serenity Crater.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] That taint nucleates within you.[SOFTBLOCK] It has followed you across the divide to this place.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You have seen the future.[SOFTBLOCK] You have seen what you become.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] B-[SOFTBLOCK]but she can control herself...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The future isn't set.[SOFTBLOCK] We can avoid that fate.[SOFTBLOCK] Give me a chance.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Please do not resist.[SOFTBLOCK] We owe you much.[SOFTBLOCK] It will not hurt.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not happening.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We know.[SOFTBLOCK] We know that we fail.[SOFTBLOCK] You leave this place, and continue to where you will be.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] But we must try.[SOFTBLOCK] We are sorry.") ]])
            
            --Neither.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] About time the darkmatters made themselves useful.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] See? This is why you don't shoot them, 55![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You will stop here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She says - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I was able to understand her.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You were?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You cannot leave this place.[SOFTBLOCK] We are sorry for this.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey, hold on.[SOFTBLOCK] What's going on?[SOFTBLOCK] I'm one of you -[SOFTBLOCK] just, ask - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You must die.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Now there's no need for this.[SOFTBLOCK] Don't you recognize me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Christine.[SOFTBLOCK] Bearer of the runestone.[SOFTBLOCK] You have helped us cleanse the taint of Serenity Crater.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] That taint nucleates within you.[SOFTBLOCK] It has followed you across the divide to this place.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You have seen the future.[SOFTBLOCK] You have seen what you become.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] This course of action is idiotic.[SOFTBLOCK] Christine is your greatest ally.[SOFTBLOCK] You are making a mistake.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55's right.[SOFTBLOCK] We can avoid that fate.[SOFTBLOCK] Give me a chance.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Please do not resist.[SOFTBLOCK] We owe you much.[SOFTBLOCK] It will not hurt.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not happening.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We know.[SOFTBLOCK] We know that we fail.[SOFTBLOCK] You leave this place, and continue to where you will be.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] But we must try.[SOFTBLOCK] We are sorry.") ]])
                
            end
            
            fnCutsceneBlocker()
        
        --Doesn't have Darkmatter form.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
            
            --SX-399 is present, Sophie is present:
            if(bIsSX399Present == true and bIsSophiePresent == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Far-out![SOFTBLOCK] It's a Darkmatter Girl![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I've seen them in the mines.[SOFTBLOCK] They aren't friends with the creepies, that's for sure.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Are you here to help?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Uh, you want my runestone?[SOFTBLOCK] Here, touch it...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] stop...[SOFTBLOCK] now...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Me, stop, now?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I heard what she said.[SOFTBLOCK] It appears your runestone can translate.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Well that's dead handy![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't worry about us, darkmatter.[SOFTBLOCK] We can take care of ourselves.[SOFTBLOCK] In fact, could you - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We...[SOFTBLOCK] stop...[SOFTBLOCK] you...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Excuse me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] are...[SOFTBLOCK] them...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Seen...[SOFTBLOCK] future...[SOFTBLOCK] you...[SOFTBLOCK] are...[SOFTBLOCK] them...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I can control it.[SOFTBLOCK] The future is not set.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] What's she talking about?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] They've already turned Christine against us once...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I know, and I'm sorry.[SOFTBLOCK] I won't let it happen.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But that means I need your help, darkmatter![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] die...[SOFTBLOCK] here...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] You won't stop us![SOFTBLOCK] If we don't stop Vivify, what chance will you have?[SOFTBLOCK] You know I'm your best hope![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Talking...[SOFTBLOCK] over...[SOFTBLOCK] goodbye...") ]])
            
            --Just SX-399.
            elseif(bIsSX399Present == true and bIsSophiePresent == false) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Far-out![SOFTBLOCK] It's a Darkmatter Girl![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I've seen them in the mines.[SOFTBLOCK] They aren't friends with the creepies, that's for sure.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Are you here to help?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Uh, you want my runestone?[SOFTBLOCK] Here, touch it...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] stop...[SOFTBLOCK] now...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Me, stop, now?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I heard what she said.[SOFTBLOCK] It appears your runestone can translate.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Well that's dead handy![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't worry about us, darkmatter.[SOFTBLOCK] We can take care of ourselves.[SOFTBLOCK] In fact, could you - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We...[SOFTBLOCK] stop...[SOFTBLOCK] you...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Excuse me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] are...[SOFTBLOCK] them...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Seen...[SOFTBLOCK] future...[SOFTBLOCK] you...[SOFTBLOCK] are...[SOFTBLOCK] them...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I can control it.[SOFTBLOCK] The future is not set.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] What's she talking about?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What happened at the Gala.[SOFTBLOCK] Christine, despite her best efforts, is well-and-truly one of them.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The darkmatter's assessment is incorrect, but understandable.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm sorry.[SOFTBLOCK] I won't let it happen if I can avoid it.[SOFTBLOCK] I didn't like being...[SOFTBLOCK] them...[SOFTBLOCK] either![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But that means I need your help, darkmatter![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] die...[SOFTBLOCK] here...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] You won't stop us![SOFTBLOCK] If we don't stop Vivify, what chance will you have?[SOFTBLOCK] You know I'm your best hope![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Talking...[SOFTBLOCK] over...[SOFTBLOCK] goodbye...") ]])
            
            --Just Sophie.
            elseif(bIsSX399Present == false and bIsSophiePresent == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Christine, look![SOFTBLOCK] A darkmatter![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Are they your friends?[SOFTBLOCK] Have you seen them on your adventures?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Friends, maybe.[SOFTBLOCK] They certainly aren't friends of Vivify or her ilk.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, you want my runestone?[SOFTBLOCK] Here, touch it...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] stop...[SOFTBLOCK] now...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Me, stop, now?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I heard what she said.[SOFTBLOCK] It appears your runestone can translate.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Well that's dead handy![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't worry about us, darkmatter.[SOFTBLOCK] We can take care of ourselves.[SOFTBLOCK] In fact, could you - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We...[SOFTBLOCK] stop...[SOFTBLOCK] you...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Excuse me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] are...[SOFTBLOCK] them...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Seen...[SOFTBLOCK] future...[SOFTBLOCK] you...[SOFTBLOCK] are...[SOFTBLOCK] them...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I can control it.[SOFTBLOCK] The future is not set.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Christine won't let them take her over...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, despite her best efforts, is well-and-truly one of them.[SOFTBLOCK] The enemy.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The darkmatter's assessment is incorrect, but understandable.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm sorry.[SOFTBLOCK] I won't let it happen if I can avoid it.[SOFTBLOCK] I didn't like being...[SOFTBLOCK] them...[SOFTBLOCK] either![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But that means I need your help, darkmatter![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] die...[SOFTBLOCK] here...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] You won't stop us![SOFTBLOCK] If we don't stop Vivify, what chance will you have?[SOFTBLOCK] You know I'm your best hope![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Talking...[SOFTBLOCK] over...[SOFTBLOCK] goodbye...") ]])
            
            --Neither.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A darkmatter.[SOFTBLOCK] This is unusual.[SOFTBLOCK] They do not often come this far into the city.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Maybe she wants to be friends?[SOFTBLOCK] They certainly aren't friends of Vivify or her ilk.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] They were there at the LRT facility.[SOFTBLOCK] Do you think they want to help us against Vivify?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, you want my runestone?[SOFTBLOCK] Here, touch it...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] stop...[SOFTBLOCK] now...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Me, stop, now?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I heard what she said.[SOFTBLOCK] It appears your runestone can translate.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Well that's dead handy![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't worry about us, darkmatter.[SOFTBLOCK] We can take care of ourselves.[SOFTBLOCK] In fact, could you - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We...[SOFTBLOCK] stop...[SOFTBLOCK] you...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Excuse me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] are...[SOFTBLOCK] them...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Seen...[SOFTBLOCK] future...[SOFTBLOCK] you...[SOFTBLOCK] are...[SOFTBLOCK] them...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I can control it.[SOFTBLOCK] The future is not set.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Christine won't let them take her over...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, despite her best efforts, is well-and-truly one of them.[SOFTBLOCK] The enemy.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The darkmatter's assessment is incorrect, but understandable.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm sorry.[SOFTBLOCK] I won't let it happen if I can avoid it.[SOFTBLOCK] I didn't like being...[SOFTBLOCK] them...[SOFTBLOCK] either![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But that means I need your help, darkmatter![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] You...[SOFTBLOCK] die...[SOFTBLOCK] here...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] You won't stop us![SOFTBLOCK] If we don't stop Vivify, what chance will you have?[SOFTBLOCK] You know I'm your best hope![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Talking...[SOFTBLOCK] over...[SOFTBLOCK] goodbye...") ]])
                
            end
            
            fnCutsceneBlocker()
        
        end
        
        --Wait a bit.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Darkmatter walks away.
        fnCutsceneMoveFace("Darkmatter", 5.25, 10.50, 0, -1, 1.20)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneTeleport("Darkmatter", -100.25, -100.50)
        fnCutsceneBlocker()
        
        --Cutscene.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Exactly what we needed![SOFTBLOCK] More infighting![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We have acquired the Datacore's keycard.[SOFTBLOCK] We should continue.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55, don't you get it?[SOFTBLOCK] This wasn't an accident.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It's an ambush.[SOFTBLOCK] They stuck the keycard here knowing we'd need it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It wasn't Vivify's goons that attacked this unit, it was the darkmatters.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This does not change our objectives.[SOFTBLOCK] We will need to fight our way out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But don't -[SOFTBLOCK] just, give them a bonk.[SOFTBLOCK] You know?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sure we can talk some sense into them later.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Darkmatters are difficult to injure with physical weaponry.[SOFTBLOCK] I'm sure a pulse round to the face will be your 'bonk'.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] *sigh*[SOFTBLOCK] Why did it come to this?") ]])
        fnCutsceneBlocker()
        
        --Fold the party.
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
        --Move camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Position", (11.25 * gciSizePerTile), (5.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] We could rest at that bench, just like a heating coil.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] It won't allow us to warp, but at least we'll be at full health...") ]])
        fnCutsceneBlocker()
        
        --Move camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The remains of Unit 689344.[SOFTBLOCK] She's beyond repair.[SOFTBLOCK] She was clutching a red keycard.)") ]])
        fnCutsceneBlocker()
    
    
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end