--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Despawner") then
    local iBiolabsFoundRedKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N")
    if(iBiolabsFoundRedKeycard == 0.0) then
        --Despawn all enemies.
        local sList = {"EnemyAA", "EnemyAB", "EnemyBA", "EnemyBB", "EnemyCA", "EnemyCB", "EnemyCC", "EnemyDA", "EnemyEA", "EnemyEB", "EnemyWanderAA", "EnemyWanderBA", "EnemyWanderCA"}
        local i = 1
        while(sList[i] ~= nil) do
            if(EM_Exists(sList[i]) == true) then
                EM_PushEntity(sList[i])
                    RE_SetDestruct(true)
                DL_PopActiveObject()
            end
            i = i + 1
        end
    end
end
