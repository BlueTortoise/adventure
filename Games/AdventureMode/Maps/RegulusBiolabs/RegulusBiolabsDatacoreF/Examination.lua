--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToDatacoreA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsD", "FORCEPOS:21.0x5.0x0")
    
elseif(sObjectName == "ToDatacoreG") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsDatacoreG", "FORCEPOS:14.0x14.0x0")
    
elseif(sObjectName == "ToDatacoreB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsDatacoreB", "FORCEPOS:11.5x23.0x0")
    
--[Objects]
elseif(sObjectName == "Boxes") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Broken laptops and computer parts to be shipped off for recycling.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ShelfA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A whole pile of SCSI-R adaptors.[SOFTBLOCK] A note on the pile says 'BURNT'.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ShelfB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Assorted junk.[SOFTBLOCK] Unfortunately, not the useful kind of junk.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Alert Status::[SOFTBLOCK] Red.[SOFTBLOCK] All units please proceed to your nearest security checkpoint for further instructions.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I'm getting out while the getting is good.[SOFTBLOCK] I just heard the elevator crash and glass breaking.[SOFTBLOCK] Security is nowhere to be found.[SOFTBLOCK] Run.[SOFTBLOCK] Don't stop. Head for the Raiju Ranch.[SOFTBLOCK] I heard everyone was going there.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Pod") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An empty research pod.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end