--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "ToTransitB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsTransitB", "FORCEPOS:8.0x26.0x0")
    
elseif(sObjectName == "SignA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Duck Pond)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Please help yourselves![SOFTBLOCK] The ducks love our fruit![SOFTBLOCK] -Biological Services')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Boxes") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Gardening equipment.[SOFTBLOCK] Watering cans, fertilizer, hand troughs.[SOFTBLOCK] Doesn't look made for Slave Units...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Crops") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Crops being grown without the usual indicators of Golem activity.[SOFTBLOCK] No automated sprinklers, no fertilizer pellets...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Ducks") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The ducks don't seem to notice the monsters, and the monsters don't seem to notice the ducks.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end