--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsDeltaB"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "Biolabs")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusBiolabsDeltaB")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    gsPartyLeaderName = "Christine"
    
    --Always reset this flag to indicate that the party has merged together.
    local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 3.0)
    
    --If we have seen the Raiju Ranch cutscene, we need to make sure Sophie isn't in the party.
    local iSawRaijuIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
    if(iSawRaijuIntro == 1.0) then
        
        --If the party had not split, don't do anything.
        if(iChristineLeadingParty == 3.0) then
        
        --If Christine was leading the party, and SX-399 was present, reform the party. We need to spawn 55 and SX-399.
        elseif(iChristineLeadingParty == 2.0) then
            local i55ID = fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
            local iSX399ID = fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
            
            --Remove Sophie.
            fnCutsceneTeleport("Sophie", -100, -100)
            AL_SetProperty("Unfollow Actor Name", "Sophie")
            
            --Add everyone else.
            AL_SetProperty("Follow Actor ID", i55ID)
            AL_SetProperty("Follow Actor ID", iSX399ID)
            gsFollowersTotal = 2
            gsaFollowerNames = {"55", "SX399"}
            giaFollowerIDs   = {i55ID, iSX399ID}
            AL_SetProperty("Fold Party")
        
        --Christine is leading the party, but SX-399 is not present. Spawn 55.
        elseif(iChristineLeadingParty == 1.0) then
            local i55ID = fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
            
            --Remove Sophie.
            fnCutsceneTeleport("Sophie", -100, -100)
            AL_SetProperty("Unfollow Actor Name", "Sophie")
            
            --Add everyone else.
            AL_SetProperty("Follow Actor ID", i55ID)
            gsFollowersTotal = 1
            gsaFollowerNames = {"55"}
            giaFollowerIDs   = {i55ID}
            AL_SetProperty("Fold Party")
        
        --55 was leading the party with SX-399. Christine needs to spawn.
        else
            EM_PushEntity("55")
                local i55ID = RE_GetID()
            DL_PopActiveObject()
            EM_PushEntity("SX399")
                local iSX399ID = RE_GetID()
            DL_PopActiveObject()
            
            --Figure out what outfit to give Christine.
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
            
            --Create Christine.
            TA_Create("Christine")
                TA_SetProperty("Position", 33, 2)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", false)
                fnSetCharacterGraphics(sOutfit, true)
                local iChristineID = RE_GetID()
            DL_PopActiveObject()
            
            --Run Christine's form script so she gets her special frames. This will also give her the appropriate costume.
            if(sChristineForm == "Human") then
                LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua")
            elseif(sChristineForm == "Raiju") then
                LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua")
            elseif(sChristineForm == "Golem") then
                LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
            end
            
            --Add everyone else.
            AL_SetProperty("Unfollow Actor Name", "SX399")
            AL_SetProperty("Player Actor ID", iChristineID)
            AL_SetProperty("Follow Actor ID", i55ID)
            AL_SetProperty("Follow Actor ID", iSX399ID)
            gsFollowersTotal = 2
            gsaFollowerNames = {"55", "SX399"}
            giaFollowerIDs   = {i55ID, iSX399ID}
            AL_SetProperty("Fold Party")
    
            --Set leader voice.
            WD_SetProperty("Set Leader Voice", "Christine")
        
        end
    end
end
