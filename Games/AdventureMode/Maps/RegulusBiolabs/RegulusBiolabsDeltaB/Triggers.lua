--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "NopeSouth") then
    
    --Repeat Check.
    local iSawRaijuIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
    if(iSawRaijuIntro == 1.0) then return end
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The Raiju Ranch is just north of here.[SOFTBLOCK] I should really get Sophie to safety before we go wandering off...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 34.25, 28.50)
    fnCutsceneMove("55", 34.25, 28.50)
    fnCutsceneMove("Sophie", 34.25, 28.50)
    if(fnIsCharacterPresent("SX399") == true) then
        fnCutsceneMove("SX399", 34.25, 28.50)
    end
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NopeEast") then
    
    --Repeat Check.
    local iSawRaijuIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
    if(iSawRaijuIntro == 1.0) then return end
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The Raiju Ranch is just north of here.[SOFTBLOCK] I should really get Sophie to safety before we go wandering off...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 39.25, 17.50)
    fnCutsceneMove("55", 39.25, 17.50)
    fnCutsceneMove("Sophie", 39.25, 17.50)
    if(fnIsCharacterPresent("SX399") == true) then
        fnCutsceneMove("SX399", 39.25, 17.50)
    end
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
end
