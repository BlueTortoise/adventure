--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('This seems like an important question, yet I can't get a straight answer from any higher-ups.[SOFTBLOCK] Why is my testing range being used to test infiltration of specific chemical compounds?')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I've been modelling water infiltration through various soil types for a decade now.[SOFTBLOCK] We've made some major improvements in agricultural technology.[SOFTBLOCK] Revolutionary, even.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Now, I'm being told to study dispersal patterns of lead acetate.[SOFTBLOCK] Why?[SOFTBLOCK] It's crucial that a scientist knows the details.[SOFTBLOCK] We can't do science in the dark!')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I get the feeling this isn't simply to study pollution cleanup.[SOFTBLOCK] I have no allegiance to the organics planetside.[SOFTBLOCK] Just tell me already!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The soil itself acts as a recepticle for the noted materials, effectively filtering the water.[SOFTBLOCK] Given enough time, most pollutants would diffuse throughout an ecosystem, absorbed and used as catalysts by plant and bacterial species.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('This is to say nothing of the fixing tendency of mineral formation, which we do not model here.[SOFTBLOCK] Presumably, lead acetate would decay and form minerals like galena in a sedimentary deposit eventually, fully denaturing it.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Though if the purpose was to cause neurological damage, it would be enough to pour an excessive amount of lead acetate upstream and allow it to infiltrate into a river body.[SOFTBLOCK] The sugar dissolves well, even tasting sweet.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Organic citizens are not to drink water in the infiltration testing range under any circumstances.[SOFTBLOCK] Please use approved filtered taps only.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('South:: Biological Services barracks.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('East:: Aviary.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('West:: Raiju Ranch, Breeding Program facilities, Public Campsites.')") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end