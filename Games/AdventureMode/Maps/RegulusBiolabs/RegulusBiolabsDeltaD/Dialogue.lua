--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)


--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--[Topics]
--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --[Alraunes]
    if(sActorName == "AlrauneA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] Keep your eyes open out there.[SOFTBLOCK] Those things could be hiding behind any branch.") ]])
        
    elseif(sActorName == "AlrauneB") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] Dafoda called me a couch potato, can you believe that?[SOFTBLOCK] I can't help my starchy heritage!") ]])
        
    elseif(sActorName == "AlrauneC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
        if(iAmphibianRescuedReika < 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] Reika's not back yet...[SOFTBLOCK] And knowing her, she did something stupid but also really brave.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] I don't care what Dafoda says, you are heroes to me.[SOFTBLOCK] Thanks for saving Reika.") ]])
        end
        
    elseif(sActorName == "AlrauneD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] We're keeping watch here.[SOFTBLOCK] We'll take in any civilians that come by, too.") ]])
        
    elseif(sActorName == "AlrauneE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] If this was any other day, I'd be sunning myself in the orchards.[SOFTBLOCK] I hope war doesn't come to the Biolabs...") ]])
        
    elseif(sActorName == "AlrauneF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] I shot one of those things.[SOFTBLOCK] Why won't my hands stop shaking?[SOFTBLOCK] Didn't I do the right thing?") ]])
        
    elseif(sActorName == "AlrauneG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
        if(iAmphibianRescuedReika < 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] Please find our wayward leaf-sister.[SOFTBLOCK] She can be a handful, but she's our handful.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] Trying to fix the pipes all by herself?[SOFTBLOCK] Reika is either brave or stupid.[SOFTBLOCK] Probably stupid.") ]])
        end
        
    elseif(sActorName == "Reika") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Reika:[VOICE|Alraune] Out of the freezer and into the frying pan.[SOFTBLOCK] Dafoda put me on probation for three weeks, *and* gave me a commendation![SOFTBLOCK] I don't know what to think!") ]])
        
    elseif(sActorName == "Dafoda") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
        if(iAmphibianRescuedReika == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[VOICE|Alraune] We will owe you a debt of gratitude if you rescue our missing unit.") ]])
        
        --Scene.
        elseif(iAmphibianRescuedReika == 1.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N", 2.0)
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] You return.[SOFTBLOCK] Reika arrived a few moments ago.[SOFTBLOCK] Alive.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It'd be quite odd if she had arrived any other way.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] The little ones have already told me what I need to know.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So...[SOFTBLOCK] will you support our cause?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] I told you, I cannot state that.[SOFTBLOCK] We must confer.[SOFTBLOCK] The Alraunes, and the little ones, and our grandfathers, make decisions together.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] At the very least, we will adopt a position of neutrality in the coming conflict.[SOFTBLOCK] You would not selflessly rescue our wayward sister if you did not have some purity of motive.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] I will testify on your behalf, but our decision will take time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's about the best we can hope for.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will inform our forces to respect your neutrality as soon as I am able.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As neutrals, perhaps you will aid in negotiations?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] The fighting has only just started, and already you're thinking of that?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] My plans go far in advance.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The longer the fighting goes on, the fewer units who will live to see its end.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The more lives will be scarred, the more resources wasted on violence.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We want a swift, decisive victory, but are prepared for the long haul.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] The Alraune way is not one of violence.[SOFTBLOCK] We pray you, or the Administration, win swiftly and bloodlessly.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Though I doubt the Administration will show you much mercy.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] All the more reason to back the rebels.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] A factor we will consider when we confer.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Until then, be safe, rebels.[SOFTBLOCK] Good luck out there.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Same to you, Dafoda.[SOFTBLOCK] And don't punish Reika too hard, she's trying her best.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] She has been punished appropriately to her actions.[SOFTBLOCK] Threats of punishment do not stop her, but they set an example for others.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I can say I have the same problem with Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Hey!!![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Ha ha![SOFTBLOCK] I see a resemblence![SOFTBLOCK] Good luck, rebels!") ]])
            fnCutsceneBlocker()
        
        elseif(iAmphibianRescuedReika == 2.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[VOICE|Alraune] I promise only that Reika will not be a problem for you in the future.[SOFTBLOCK] Come what may, you have our gratitude.") ]])
        
        
        end
        
    --[Civilians]
    elseif(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I hope my little chickadees are all right.[SOFTBLOCK] These Alraunes keep telling me they are, but I worry...") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My Lord Golem hasn't said a thing since the explosions.[SOFTBLOCK] I want to help her, but...") ]])
        
    elseif(sActorName == "GolemC") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] ........") ]])

    end
end