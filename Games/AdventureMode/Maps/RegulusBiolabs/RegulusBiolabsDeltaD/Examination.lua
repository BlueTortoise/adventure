--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "Sign") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Biological Services barracks.[SOFTBLOCK] No visitors after 8pm -[SOFTBLOCK] organics are easily awoken by loud noises!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Recall all your team members, Unit 443992.[SOFTBLOCK] That's a direct order, you are not authorized to engage the invading force.[SOFTBLOCK] Hunker down and await the security team response.[SOFTBLOCK] -2856')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Notices and emails for the Alraunes here.[SOFTBLOCK] Mostly just job orders and chit-chat.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Motilvac") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A motilvac, currently inactive.[SOFTBLOCK] This room is spotless, good job little buddy!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A documentary on Pandemonium is playing.[SOFTBLOCK] Continents, terrain features, species, history -[SOFTBLOCK] it's a broad overview.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computer") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('You're the only ones I can raise on the network.[SOFTBLOCK] I think something is wrong with the modems, I can't get anyone outside the Biolabs consistently.[SOFTBLOCK] I heard explosions, and the little ones are telling me about these strange creatures -[SOFTBLOCK] of course everyone is panicking.[SOFTBLOCK] I'm trying to keep them calm.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Erotus and the Golden Mirror'.[SOFTBLOCK] Most of the books on this shelf are high-fantasy stuff.[SOFTBLOCK] Swords, sorcery, beautiful ladies on horses.[SOFTBLOCK] The usual.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Everything You Ever Wanted to Know About Plumbing (but were afraid to ask)'.[SOFTBLOCK] Exactly what it sounds like.[SOFTBLOCK] Pipes, pressure, how to clear plugged pipes...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There are some organic snacks on this shelf.[SOFTBLOCK] They smell like rotted flesh -[SOFTBLOCK] but I guess that's a snack for a plant girl.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The books on this shelf are covered in dust.[SOFTBLOCK] There's a handheld video game console on the top shelf, which is less dusty.[SOFTBLOCK] Hmmm...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Dating Advice by Unit 40010'.[SOFTBLOCK] Someone is lonely![SOFTBLOCK] Don't worry, you'll find the right unit for you someday.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Professor Killsalot and the Very Bad No Good Chemical Spill'.[SOFTBLOCK] Killsalot gets covered in goop and looks like a slime girl -[SOFTBLOCK] and has to avoid getting shot by security robots![SOFTBLOCK] Oh no!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Philosophy of Partirhuman Species'.[SOFTBLOCK] The chapter on Alraune philosophies has a bookmark in it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfH") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Pics of Dicks'.[SOFTBLOCK] Oh my, is the Alraune quartered here one of the rumoured heterosexual partirhumans?[SOFTBLOCK] Or maybe she just likes dicks.[SOFTBLOCK] Dicks are great in their own right!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "PollenWater") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Water mixed with many types of pollens, sugars, and plant fibers.[SOFTBLOCK] Biological Services uses this to convert humans.[SOFTBLOCK] Not as efficient as a golem tube, but you can't argue with biology.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end