--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Meeting") then
    
    --Variables.
    local iAmphibianMetDafoda = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N")
    if(iAmphibianMetDafoda > 0.0) then return end
    
    --Variables.
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N", 1.0)
    
    --Topics.
    WD_SetProperty("Unlock Topic", "Biolabs_Alraune", 1)
    
    --Merge party.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 15.25, 21.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneMove("55", 15.25, 22.50)
    fnCutsceneFace("55", -1, 0)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX399", 15.25, 23.50)
        fnCutsceneFace("SX399", -1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[VOICE|Alraune] Yes, I know they're here.[SOFTBLOCK] Which one was the leader again?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Dafoda", 11.25, 21.50)
    fnCutsceneFace("Dafoda", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Dafoda", 14.25, 21.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] So I'm hearing that you're the leader of your group.[SOFTBLOCK] Is that correct?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] I wasn't expecting someone so...[BLOCK][CLEAR]") ]])
    if(sChristineForm == "Human") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Squishy?[BLOCK][CLEAR]") ]])
    elseif(sChristineForm == "Golem") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Fabulous?[BLOCK][CLEAR]") ]])
    elseif(sChristineForm == "LatexDrone") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Firm and shiny?[BLOCK][CLEAR]") ]])
    elseif(sChristineForm == "Electrosprite") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Electromental?[BLOCK][CLEAR]") ]])
    elseif(sChristineForm == "Darkmatter") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Exotic?[BLOCK][CLEAR]") ]])
    elseif(sChristineForm == "Eldritch") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Cold and clammy?[BLOCK][CLEAR]") ]])
    elseif(sChristineForm == "SteamDroid") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Retro?[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] Naive looking.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Naive![SOFTBLOCK] How can you [SOFTBLOCK]*look*[SOFTBLOCK] naive?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] [EMOTION|Christine|Neutral]Doesn't matter.[SOFTBLOCK] You're with the rebels, aren't you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] Don't bother denying it, we know.[BLOCK][CLEAR]") ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Is it true?[SOFTBLOCK] Can you Alraunes speak to plants?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] It is.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Keen![SOFTBLOCK] Can you teach me how to do that?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] Funny, we get that question from many Lord Golems.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] The fact that our services have been retained this long indicates they have still failed to learn the skill themselves.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Bah, humbug.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] [EMOTION|SX-399|Neutral]I didn't catch your name.[SOFTBLOCK] Miss?[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My sources indicate that Alraunes are capable of conversing with plants.[SOFTBLOCK] It is likely they have been following our movements.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune:[E|Neutral] Correct.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I didn't catch your name.[SOFTBLOCK] Miss?[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] I am Unit 588641.[SOFTBLOCK] Secondary designation, Dafoda.[SOFTBLOCK] I am the Lord Unit assigned to Biological Services.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're a Lord Unit?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] I have the same access priviliges as one, and the same responsibilities.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Though I will say that, despite this, I am not regarded as a peer by the other lord units.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] They're quite catty, aren't they?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] You're Christine, or Unit 771852.[SOFTBLOCK] One of the rebel leaders, and also someone who can apparently transform herself.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] I've been doing my homework.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] So you know who we are.[SOFTBLOCK] You know why we are here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] If you're looking for allies or traitors, keep looking.[SOFTBLOCK] We are loyal to the Administration.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Why?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] We believe in the Cause of Science.[SOFTBLOCK] All of us are from the breeding program, and we chose to become alraunes.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Of my own free will, I went to Pandemonium and was joined there.[SOFTBLOCK] Then, I came back, to aid the Cause.[SOFTBLOCK] All of my leaf-sisters here were transformed by me, to help the Cause.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] And what makes you think the rebels also don't serve the Cause?[SOFTBLOCK] I believe in it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The rebels are fighting for decent lives and respect.[SOFTBLOCK] If they can't have even that then what good is Science?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] True.[SOFTBLOCK] But many of the units you are trying to protect will be retired.[SOFTBLOCK] Violence is not the alraune way.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Violence?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Violence![SOFTBLOCK] The Administration are the violent ones![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Every day they arrest, torture, and execute units who stepped one centimeter out of line.[SOFTBLOCK] We just don't see it because it happens in prisons or in back rooms.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The entire system is predicated on passive violence.[SOFTBLOCK] If our new system is less violent, then we are right to overthrow it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] A big promise.[SOFTBLOCK] You think you can be less destructive?[SOFTBLOCK] Why won't you fall into the same patterns as the Administration has?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We believe in something fundamentally different.[SOFTBLOCK] It's easy to sit there and not pick sides by assuming everyone is the same, but we are not.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Hmm...[SOFTBLOCK] I'm not convinced.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] But...[SOFTBLOCK] It is not only I who makes decisions here.[SOFTBLOCK] All are equal in nature.[BLOCK][CLEAR]") ]])
    
    --Has not rescued Reika:
    local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
    if(iAmphibianRescuedReika == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] One of our number, Unit 765532, is missing.[SOFTBLOCK] Her secondary designation is Reika.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] If you help us find her, then perhaps the others will be more charitable in their assessment of you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Can't you locate her with the help of your plant-speech?[BLOCK][CLEAR]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Maybe if she does it I can watch her and mimic it...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] I have done it several times during this conversation.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Nuts...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] [EMOTION|SX-399|Neutral]There are many areas of the biolabs where the little ones do not grow.[SOFTBLOCK] The last place she was seen was heading north towards the Beta Laboratories.[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] There are many areas of the biolabs where the little ones do not grow.[SOFTBLOCK] The last place she was seen was heading towards the Beta Laboratories.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You mean, the frozen Beta Laboratories?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Frozen?[SOFTBLOCK] Interesting.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Reika mentioned a curious silence from there, and she was going to investigate.[SOFTBLOCK] Do you know what has happened?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Likely the heat-exchange pipes were sabotaged.[SOFTBLOCK] That would sap the heat from the system and trigger a freeze-over.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] And Reika doubtless would try to fix it.[SOFTBLOCK] She is the most mechanically-oriented of us, and also the most foolhardy.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We'll find her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] I did not promise a reward.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Don't need one.[SOFTBLOCK] We'll find your friend.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] I would send assistance, but we are few and we must care for the civilians here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You can count on us, Dafoda.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Good luck, rebels.[SOFTBLOCK] Be safe.") ]])
        fnCutsceneBlocker()
    
    --Has already rescued Reika:
    elseif(iAmphibianRescuedReika == 1.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N", 2.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] One of our number, Unit 765532, went missing.[SOFTBLOCK] You know her as Reika, and you have returned her to us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] This is already more than the officials of the city have done.[SOFTBLOCK] For that, I thank you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We found someone in need, and helped them.[SOFTBLOCK] Simple as that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Indeed.[SOFTBLOCK] We will take this into consideration.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] We must confer.[SOFTBLOCK] The Alraunes, and the little ones, and our grandfathers, make decisions together.[SOFTBLOCK] I will testify to your intentions.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] At the very least, we will adopt a position of neutrality in the coming conflict.[SOFTBLOCK] You would not selflessly rescue our wayward sister if you did not have some purity of motive.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Our decision will take time.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's about the best we can hope for.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will inform our forces to respect your neutrality as soon as I am able.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As neutrals, perhaps you will aid in negotiations?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] The fighting has only just started, and already you're thinking of that?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] My plans go far in advance.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The longer the fighting goes on, the fewer units who will live to see its end.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The more lives will be scarred, the more resources wasted on violence.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We want a swift, decisive victory, but are prepared for the long haul.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] The Alraune way is not one of violence.[SOFTBLOCK] We pray you, or the Administration, win swiftly and bloodlessly.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Though I doubt the Administration will show you much mercy.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] All the more reason to back the rebels.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] A factor we will consider when we confer.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Until then, be safe, rebels.[SOFTBLOCK] Good luck out there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Same to you, Dafoda.[SOFTBLOCK] And don't punish Reika too hard, she's trying her best.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] She has been punished appropriately to her actions.[SOFTBLOCK] Threats of punishment do not stop her, but they set an example for others.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I can say I have the same problem with Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Hey!!![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Dafoda:[E|Neutral] Ha ha![SOFTBLOCK] They share a spirit![SOFTBLOCK] Good luck, rebels!") ]])
        fnCutsceneBlocker()
    
    end
end
