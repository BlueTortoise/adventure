--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I'm formally contesting a reprimand I received for washing my hands in the fluid infiltration testing range.[SOFTBLOCK] What was I supposed to do?')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Yes, we do have water for washing filth off in the aviary.[SOFTBLOCK] Do you know where the only tap is located?[SOFTBLOCK] At the back of the chicken coop -[SOFTBLOCK] you know, the filthy place?[SOFTBLOCK] I get gunk on my hands just making my way out!')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I'll install the extra piping myself if it means I don't have to walk around with chicken crap all over my chassis.[SOFTBLOCK] Just let me do something about it, the stuff is just nasty!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I got the evacuation notice, too.[SOFTBLOCK] I'm on my way to the Raiju Ranch.[SOFTBLOCK] It said to leave everything where it is.[SOFTBLOCK] Hope the alert is lifted before feeding time, because those chickens get ornery if they aren't fed.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Some idiot sent me an article asking about second-order transformations.[SOFTBLOCK] You know, making a partirhuman out of a partirhuman?[SOFTBLOCK] I think it's total nonsense.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Every single thing we know about the universe says it's a one-time, one-way thing.[SOFTBLOCK] Frankly I'd be a little creeped out if I, a noble machine, could be transformed into some quivering mass of slime.[SOFTBLOCK] Pathetic.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Sorry I had to knock off early today.[SOFTBLOCK] Noma wanted to get together with some of the golems from Sector 12.[SOFTBLOCK] Don't you know?[SOFTBLOCK] The sun's coming up in a few hours!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An inventory of the stocks in the aviary's storage building.[SOFTBLOCK] Diatomaceous earth is on back order.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The golems here have been flavouring the oil with eggs, because the chickens lay far too many to keep up with.[SOFTBLOCK] It smells...[SOFTBLOCK] like an acquired taste.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Care and Feeding of Our Feathered Friends' is playing.[SOFTBLOCK] It's meant for visitors to the aviary, and shows how to not snap their spines.[SOFTBLOCK]  Organics sure are fragile!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Door") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The door is locked, and I can hear far more clucking than I'd like in there.[SOFTBLOCK] I've never done well around chickens.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('My dearest Unit 399838::[SOFTBLOCK] If the smell of chicken feces disturbs you so, disable your olfactory sensors.[SOFTBLOCK] I'm not reassigning you.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Cage") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An enclosed terrarium containing many types of grasshopper.[SOFTBLOCK] It seems the golems are experimenting using insect protein as animal feed.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end