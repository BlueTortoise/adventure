--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I caught one of the humans eating carrots in the experimental area.[SOFTBLOCK] She was just standing there, idly observing us.[SOFTBLOCK] I don't know how long she had been standing there.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('At first I wanted to dismiss her, but she actually asked what we were doing and seemed to understand my explanations.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I sometimes forget the breeding program humans aren't as stupid as the one's planetside.[SOFTBLOCK] The way she took to the material, she might be my Lord Golem someday!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Experimental Log::[SOFTBLOCK] We've found statistical data indicating that high saline levels retard seedlings, but are generally tolerated later in the plant's lifecycle.[SOFTBLOCK] Oddly, the seeds themselves germinate normally at higher salinity, suggesting the seeds do not test for saline content chemically.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('As for which species are most resistant, Barley and Rye are the standouts, with Trafal White Wheat coming in third before a major gulf.[SOFTBLOCK] The long-stalk plants generally do well, while shorter-stalk plants perform poorly and most species of rice have poor saline tolerance.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('This is likely due to a cellular-level balancing and removal of excess salt.[SOFTBLOCK] With genetic modification, it may be possible to customize a crop for salt tolerance.[SOFTBLOCK] We may be able to do this with conventional artificial selection, though I'm sure those retroviral crazies in the Arcane University have other ideas.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I'll be categorizing my findings after the next set of experiments conclude, and publishing a paper on it.[SOFTBLOCK] I'm hoping Central Administration lets me keep my current staff for the next experiment, they proved quite capable and will be receiving commendations for their work.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Racks") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Plants of various species, with notes specifying filtration properties and saline tolerances.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Intercom") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (No response on the intercom.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end