--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Traps") then
    
    --Variables.
    local iSawTrapDialogue = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawTrapDialogue", "N")
    if(iSawTrapDialogue == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawTrapDialogue", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, what are those on the floor?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Given the nature of the room and the context of the area, I would say these are part of the defenses here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The material on the surface is exposed conductive wire, but there is no pressure-activation mechanism.[SOFTBLOCK] Personnel walk over them on the way to the labs.[SOFTBLOCK] Therefore, they are likely manually activated.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Electrical discharge defenses?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes, but the wires are not receiving power.[SOFTBLOCK] We should be all right near them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] They had ample cover, range, traps, and they couldn't stop the attack...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unfortunate.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yeah...") ]])
        fnCutsceneBlocker()
        
    end
    
elseif(sObjectName == "MusicChange") then
    AC_SetProperty("Next Music Override", "Null", 0.0000)

--Final cutscene before the flashback sequence.
elseif(sObjectName == "Betrayal") then

    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local iMet20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet20", "N")
    local iBiolabsFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFinale", "N")
    if(iMet20 >= 1.0 and iBiolabsFinale == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsFinale", "N", 1.0)
        
        --Spawn NPCs.
        TA_Create("2856")
            TA_SetProperty("Position", 7, 38)
            TA_SetProperty("Facing", gci_Face_North)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/56/", true)
        DL_PopActiveObject()
        
        --Movement.
        fnCutsceneMove("Christine", 7.25, 18.50)
        fnCutsceneMove("55", 7.25, 16.50)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 8.25, 18.50)
        end
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, -1)
        if(bIsSX399Present == true) then
            fnCutsceneFace("SX399", 0, -1)
        end
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, have you thought about what our next move will be?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Aren't you usually the one who thinks eight steps ahead?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I was wholly unable to predict what would happen in these circumstances.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Considering what we have seen in the Epsilon habitat...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's real.[SOFTBLOCK] I don't know how it works precisely, but it was and is real.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] It wasn't like anything I've seen in the mines, that's for sure.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Still.[SOFTBLOCK] Our next objective.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Neutralize Unit 2856.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 201890's goons seem to have backed off, for the moment, but she might not realize that.[SOFTBLOCK] We still have the advantage of surprise.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] Once again, you are incorrect.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("55", 7.25, 17.50)
        fnCutsceneMove("2856", 7.75, 21.50)
        fnCutsceneFace("Christine", 0, 1)
        if(bIsSX399Present == true) then
            fnCutsceneFace("SX399", 0, 1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, there you are.[SOFTBLOCK] On the plus side, you've saved us the trouble of hunting you down.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] On the minus side, I have to put up with talking to you again.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I do so appreciate our repartee.[SOFTBLOCK] My company is usually Drone Units, who are poor conversationalists.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So can I melt her now?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Ugh.[SOFTBLOCK] Much as I hate to say it...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We'll be taking you alive.[SOFTBLOCK] Since you neglected to bring your bodyguards, and are unarmed.[SOFTBLOCK] Please come quietly.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She will not, I presume.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Correct.[SOFTBLOCK] If you would just take one step forward?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Right onto the electricity trap?[SOFTBLOCK] Which you have probably repaired and reactivated?[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] She must think we're idiots.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Have you given me any reason to doubt that so far?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You're the one who tried to approach three heavily armed combat units with no support and make threats.[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not happening.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Fine.[SOFTBLOCK] I tire of this.[SOFTBLOCK] Let us conclude it.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement. Slight variation if SX-399 is present.
        if(bIsSX399Present == false) then
            fnCutsceneMoveFace("55", 7.25, 16.50, 0, 1, 0.50)
            fnCutsceneBlocker()
            fnCutsceneMoveFace("55", 7.25, 18.50, 0, 1, 2.00)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
            fnCutsceneMoveFace("Christine", 7.25, 19.50, 0, 1, 2.00)
            fnCutsceneBlocker()
        
        --With SX-399:
        else
            fnCutsceneMoveFace("55", 7.75, 16.50, 0, 1, 0.50)
            fnCutsceneBlocker()
            fnCutsceneMoveFace("55", 7.75, 18.50, 0, 1, 2.00)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
            fnCutsceneMoveFace("Christine", 7.25, 19.50, 0, 1, 2.00)
            fnCutsceneMoveFace("SX399", 8.25, 19.50, 0, 1, 2.00)
            fnCutsceneBlocker()
        
        end
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Ouch![SOFTBLOCK] 55, did you - ") ]])
        fnCutsceneBlocker()
        
        --Shocking!
        fnCutsceneFace("Christine", 0, -1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX399", -1, 0) end
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock0", false) ]])
        fnCutsceneWait(15)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|SparksA") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX399", -1, 0) end
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock0", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock1", false) ]])
        fnCutsceneWait(17)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|SparksB") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 0)
        if(bIsSX399Present == true) then fnCutsceneFace("SX399", 1, -1) end
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock1", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock2", false) ]])
        fnCutsceneWait(15)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|SparksC") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, -1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX399", 1, 0) end
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock2", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock3", false) ]])
        fnCutsceneWait(15)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|SparksA") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, 1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX399", -1, 0) end
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock3", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock1", false) ]])
        fnCutsceneWait(15)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|SparksA") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, -1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX399", 1, 1) end
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock1", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock2", false) ]])
        fnCutsceneWait(15)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|SparksB") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Electroshock2", true) ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMoveFace("Christine", 6.25, 20.50, 1, -1, 0.20)
        if(bIsSX399Present == true) then fnCutsceneMoveFace("SX399", 9.25, 20.50, 1, 1, 0.20) end
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Christine", "Wounded")
        if(bIsSX399Present == true) then fnCutsceneSetFrame("SX399", "Wounded") end
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        if(bIsSX399Present == false) then
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Sad") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Sad") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "2856", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Ahh...[SOFTBLOCK] I can't move...[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] SX?[SOFTBLOCK] Can you...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] All my systems are...[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The last-minute calibrations were my doing.[SOFTBLOCK] I had presumed you would be in your Lord Golem form, and had to reduce the voltage accordingly.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If I had wanted to terminate you, I would have.[SOFTBLOCK] So, be quiet and speak only when spoken to.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But...[SOFTBLOCK] you...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As expected, the properties of her golem body seem to be merging with her human body.[SOFTBLOCK] She absorbed electrical currents that would lead to total paralysis in a normal human.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Is it possible you miscalculated the voltage?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is always a possibility, but a remote one.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hm.[SOFTBLOCK] I think she is upset.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you upset that I have shocked you?[SOFTBLOCK] I recall the last time I did this yielded a similar result.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] T-[SOFTBLOCK]traitor...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Oh.[SOFTBLOCK] I see.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do you think that word will hurt me?[SOFTBLOCK] Is this the last, desperate attempt to regain control over the situation?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Pathetic.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Agreed.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] How...[SOFTBLOCK] could you...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Simple.[SOFTBLOCK] I am a perfect machine.[SOFTBLOCK] I do not feel the guilt you are convinced I was feeling.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But...[SOFTBLOCK] you were...[SOFTBLOCK] making such good progress...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55...[SOFTBLOCK] you were my best friend...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The 'progress' you believed I had been making was an act.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] I contacted her on your PDU, Christine.[SOFTBLOCK] I gave her a simple offer.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Bring down Vivify, and then give me Christine, and I would see to it she is reinstated as Head of Security.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A full pardon.[SOFTBLOCK] After all, I had merely been acting according to my own interests without my memories.[SOFTBLOCK] That is forgiveable, given the circumstances.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] With my memories lost, I moved to recover them.[SOFTBLOCK] I have reasserted my understanding of the situation, and made the correct choice.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] While the Administrator will likely be unhappy with me and my actions, I am prepared to face the consequences.[SOFTBLOCK] My sister, however, is innocent.[SOFTBLOCK] The Administrator will see the logic of the situation.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] With Project Vivify neutralized, we can now deal with the rebellion you have instigated.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is no but.[SOFTBLOCK] You have lost, Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] But it will not be the end for you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] S-[SOFTBLOCK]Sophie...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your tandem unit.[SOFTBLOCK] I will leave her fate up to you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Oh?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I am a machine, and will take no enjoyment whatsoever in watching you mete out just punishment for her rebellious behavior.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I -[SOFTBLOCK] can't -[SOFTBLOCK] WHAT -[SOFTBLOCK] are you...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Oh, it's all right to show a little emotion, sister.[SOFTBLOCK] You're clearly enjoying it, the same way I do.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Just don't let anyone else know you love it.[SOFTBLOCK] But it's fine if it's between us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Sister.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Sister.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] She would...[SOFTBLOCK] never help you...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] To think...[SOFTBLOCK] I loved you...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Yes, she will.[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I won't...[SOFTBLOCK] help you...[SOFTBLOCK] *cough*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Yes, you will.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] My sister informs me that the Loyalty Program on new Command Units is extremely potent.[SOFTBLOCK] Look at what it has done to us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Perfect, unflinching loyalty.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] As such, you shall become a new Command Unit, Christine.[SOFTBLOCK] This will make the process of experimenting on you and your runestone much more efficient.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Further, your infiltration and combat capabilities, when properly programmed, will be excellent.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] All that is necessary is removing that pesky independent streak you possess, and you will be -[SOFTBLOCK] perhaps even a better unit than I.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'd die first...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Let SX-399 go...[SOFTBLOCK] This is between us...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Nothing to say, SX-399?[SOFTBLOCK] It seems she has entered system standby.[SOFTBLOCK] Pity.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No, Christine, I will not let her go.[SOFTBLOCK] And no, I will not retire her here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] There are doubtless many engineering advancements in her chassis.[SOFTBLOCK] Even if not, there are many novel improvements to be researched.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] She will keep our engineers occupied for at least a few months as we take her apart and analyze each piece.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] Bitch...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your insults do nothing to me.[SOFTBLOCK] I am a machine.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I...[SOFTBLOCK] I...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] It seems she has lost consciousness.") ]])
        fnCutsceneBlocker()
        
        --Re-dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "2856", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not a great loss, then.[SOFTBLOCK] She tends to talk too much.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] How does it feel to be back?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Same as ever.[SOFTBLOCK] Empty and mechanical.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Not even a hint of regret?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] None.[SOFTBLOCK] What is our next task?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] I have sent word ahead.[SOFTBLOCK] There is a special chamber beneath Sector 5 where the Command Unit conversion facilities are located.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] It can only be opened by two Command Units, but you are lacking authorization.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I understand.[SOFTBLOCK] You will have to lead the way, then.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] We can make our way there through the transit station.[SOFTBLOCK] My units have cleared enough debris to move on foot.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] By the way, what if Christine fails to be swayed by the Loyalty Program?[SOFTBLOCK] What if she resists?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She has demonstrated impressive resolve thus far.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] It will be a shame, but she would have to be scrapped.[SOFTBLOCK] I am not sure myself.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] The program was written by the Administrator, and is perfect.[SOFTBLOCK] It has never failed.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Is it possible that it will remove components of her psyche in order to ensure loyalty?[SOFTBLOCK] What if she is of no use to us afterwards?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] We can still experiment on her and her runestone.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Ah, yes.[SOFTBLOCK] I am thinking from a security perspective, while you are using a research perspective.[SOFTBLOCK] I apologize.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Come now.[SOFTBLOCK] There is a rebellion raging.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Won't it be sweet to watch 771852 crush the very rebellion she is the figurehead of?[SOFTBLOCK] To watch the despair in the eyes of those Slave Units as they realize there is truly no hope?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] It will be a decade before any revolutionary thoughts even cross their minds![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It will be so sweet.[SOFTBLOCK] But you didn't hear me say that.[SOFTBLOCK] I am an unfeeling machine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] Sister.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK][SOFTBLOCK][SOFTBLOCK][EMOTION|2855|Smirk] Sister.") ]])
        fnCutsceneBlocker()
        
        --Remove 55 and SX-399 from the party.
        AC_SetProperty("Set Party", 0, "Christine")
        AC_SetProperty("Set Party", 1, "Null")
        AC_SetProperty("Set Party", 2, "Null")
        AC_SetProperty("Set Party", 3, "Null")
        
        --Remove the followers so it's just Christine.
        AL_SetProperty("Unfollow Actor Name", "55")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        if(bIsSX399Present == true) then
            AL_SetProperty("Unfollow Actor Name", "SX399")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
        end
        
        --Wait a bit, fade to black.
        fnCutsceneWait(120)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --Transition to this new level where the player can save in expectation of the next content.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFlashbackA", "FORCEPOS:7.0x6.0x0") ]])
        fnCutsceneBlocker()
    end
end
