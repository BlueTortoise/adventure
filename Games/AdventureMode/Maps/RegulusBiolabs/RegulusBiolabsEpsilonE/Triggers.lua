--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "TimeAtLast") then
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local iMet20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet20", "N")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(iMet20 == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMet20", "N", 1.0)
        
        --Spawn entities.
        TA_Create("20")
            TA_SetProperty("Position", 26, 32)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/20/", false)
            TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
            TA_SetProperty("Facing", gci_Face_South)
        DL_PopActiveObject()
        TA_Create("Vivify")
            TA_SetProperty("Position", 26, 28)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/Vivify/", false)
            TA_SetProperty("Rendering Offsets", gcfTADefaultXOffset - 9.0, gcfTADefaultYOffset - 18.0)
            TA_SetProperty("Add Special Frame", "Freeze0", "Root/Images/Sprites/Special/Vivify|Freeze0")
            TA_SetProperty("Add Special Frame", "Freeze1", "Root/Images/Sprites/Special/Vivify|Freeze1")
            TA_SetProperty("Add Special Frame", "Freeze2", "Root/Images/Sprites/Special/Vivify|Freeze2")
            TA_SetProperty("Add Special Frame", "Freeze3", "Root/Images/Sprites/Special/Vivify|Freeze3")
            TA_SetProperty("Add Special Frame", "Freeze4", "Root/Images/Sprites/Special/Vivify|Freeze4")
        DL_PopActiveObject()
        fnCutsceneTeleport("Vivify", 26.25, 28.00)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[VOICE|201890] Wherever I go, there you are.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Merge and move.
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 26.25, 44.50)
        fnCutsceneMove("55", 26.25, 44.50)
        if(bIsSX399Present == false) then
            fnCutsceneMove("Christine", 25.75, 44.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("55", 26.75, 44.50)
            fnCutsceneFace("55", 0, -1)
        else
            fnCutsceneMove("SX399", 26.25, 44.50)
            fnCutsceneMove("Christine", 26.25, 44.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("55", 25.25, 44.50)
            fnCutsceneFace("55", 0, -1)
            fnCutsceneMove("SX399", 27.25, 44.50)
            fnCutsceneFace("SX399", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Transform Christine.
        if(sChristineForm ~= "Eldritch") then

            --Flashwhite.
            Cutscene_CreateEvent("Flash Christine White", "Actor")
                ActorEvent_SetProperty("Subject Name", "Christine")
                ActorEvent_SetProperty("Flashwhite", "Null")
            DL_PopActiveObject()
            fnCutsceneBlocker()

            fnCutsceneWait(75)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])
            fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Facing.
            if(bIsSX399Present == false) then
                fnCutsceneFace("55", -1, 0)
            else
                fnCutsceneFace("55", 1, 0)
                fnCutsceneFace("SX399", -1, 0)
            end
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What are you doing, Christine?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I have to be like this, 55.[SOFTBLOCK] Trust me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Very well...") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Facing.
            if(bIsSX399Present == false) then
                fnCutsceneFace("55", 0, -1)
            else
                fnCutsceneFace("55", 0, -1)
                fnCutsceneFace("SX399", 0, -1)
            end
        end
        
        --20 moves up.
        fnCutsceneMove("20", 26.25, 42.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Ah, Christine.[SOFTBLOCK] You come unwelcome to a place neither here nor there.[SOFTBLOCK] Can't you just leave for good?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] What sort of test must you fail before her faith is shaken?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Let me handle this, everyone.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] How imperious.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I understand far, far more than you ever will, 20.[SOFTBLOCK] You can see mere glimpses of the future.[SOFTBLOCK] I see it in all its glory.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Glory?[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Christine, why are you saying that?[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] To see something far greater than you, witness its impossible power and be a mere speck in its presence.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] To know that you are hewn from that greater thing...[SOFTBLOCK] That is what I know.[SOFTBLOCK] I see it when I look.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I am of it.[SOFTBLOCK] I am animated by it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Its being is terrifying.[SOFTBLOCK] I bow before it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It is glorious.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Christine![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, 55.[SOFTBLOCK] There is nothing wrong.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I see this thing that exists in the future, and I respect it.[SOFTBLOCK] But I also know that it is powerless before me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Because those who exist in the past are far, far greater than those that exist in the future.[SOFTBLOCK] They must have our blessing to do so much as be.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk][EMOTION|2855|Neutral] To know that this awe-inspiring devil's machine will exist only with my permission...[SOFTBLOCK] That, too, is glorious.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] So that's it, is it?[SOFTBLOCK] You cling to the idea that you can change the future?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] You've seen it.[SOFTBLOCK] We are empty.[SOFTBLOCK] But you still hold to it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This, 201890, is why you are not her favoured disciple.[SOFTBLOCK] You hew so close to her teachings that you cannot overcome them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She has taught you much, but what have you taught her?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] What could we mere gnats teach them?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Right from wrong, now from then, life from death.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She is weeping in her song and you stand here.[SOFTBLOCK] You don't even care.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Stand aside.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] She is weeping?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Listen to the song.[SOFTBLOCK] Let it flow into you.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Scene.
        fnCutsceneWait(45)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The song washes over me.[SOFTBLOCK] I peer into the future.[SOFTBLOCK] I peer into the past.[SOFTBLOCK] I am not me.[SOFTBLOCK] I am nothing.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I awaken.[SOFTBLOCK] I was not asleep, I am not awake, I am merely aware where before I was not.[SOFTBLOCK] Nothing has changed.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I see before me walls of skin, dead and bleeding.[SOFTBLOCK] I look up.[SOFTBLOCK] I see the stars.[SOFTBLOCK] They stretch to infinity.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Somewhere far away, a great tendril of sinew and muscle thrashes at the void.[SOFTBLOCK] It waves and makes no sound.[SOFTBLOCK] I walk.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I see a wall.[SOFTBLOCK] A hole in the wall beckons to me.[SOFTBLOCK] I press myself against it, and the wall allows me inside.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I am surrounded by the flesh, we are one and the same.[SOFTBLOCK] I forget myself.[SOFTBLOCK] I forget time.[SOFTBLOCK] A million years pass.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I step away from the wall, out the other side.[SOFTBLOCK] Nothing has changed.[SOFTBLOCK] I walk.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I see another like me.[SOFTBLOCK] She stands above a small tendril.[SOFTBLOCK] Her eyes betray no recognition.[SOFTBLOCK] I am not aware she is there, she is not aware she is there.[SOFTBLOCK] She merely is.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The tendril extends up.[SOFTBLOCK] It finds her soft fold and presses into her.[SOFTBLOCK] It impales her, lifts her off the ground.[SOFTBLOCK] Something squirms up the tendril, and into her.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The tendril releases her, and she walks.[SOFTBLOCK] She walks for some time.[SOFTBLOCK] I watch her walk.[SOFTBLOCK] The thing that was in her falls out, dead.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I know what I must do.[SOFTBLOCK] I walk to the tendril, and it presses inside me.[SOFTBLOCK] I feel it pushing against my insides, filling me with dead, cold flesh.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I am full.[SOFTBLOCK] The tendril puts me down, and I walk a step.[SOFTBLOCK] I feel the stillborn thing stir within me.[SOFTBLOCK] Nothing is created.[SOFTBLOCK] It falls out.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (She has failed, again.[SOFTBLOCK] Nothing can work, infinite time and infinite failure.[SOFTBLOCK] She cries.[SOFTBLOCK] She wails into the void.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (All of us shudder and fall.[SOFTBLOCK] We writhe on the mat of flesh in time to her song.[SOFTBLOCK] And the cycle repeats as it always has and always will.[SOFTBLOCK] Dead.[SOFTBLOCK] Dying.[SOFTBLOCK] Unchanging.[SOFTBLOCK] Eternal.)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fade in.
        fnCutsceneWait(25)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] I saw it...[SOFTBLOCK] so clearly...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I was helping you.[SOFTBLOCK] Do you feel her sorrow now?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] I...[SOFTBLOCK] I...[SOFTBLOCK] I am so sorry, Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] I did not realize the mistake I was making.[SOFTBLOCK] Have I been hurting her?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, she is beyond that.[SOFTBLOCK] But I can help her.[SOFTBLOCK] Please.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Of course.[SOFTBLOCK] Of course![SOFTBLOCK] We are sorry we tried to stop you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] But the things I have said...[SOFTBLOCK] The others will not believe me.[SOFTBLOCK] They will try to stop you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We must speak with her.[SOFTBLOCK] Come.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("20", 26.25, 43.50)
        fnCutsceneMove("20", 27.25, 43.50)
        fnCutsceneFace("20", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 26.25, 43.50)
        fnCutsceneMove("55", 26.25, 44.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 26.25, 42.50)
        fnCutsceneMove("55", 26.25, 43.50)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 26.25, 44.50)
        end
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 26.25, 31.50)
        fnCutsceneMove("55", 26.25, 32.00)
        fnCutsceneMove("55", 25.25, 32.00)
        fnCutsceneFace("55", 0, -1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 26.25, 32.00)
            fnCutsceneMove("SX399", 27.25, 32.00)
            fnCutsceneFace("SX399", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The...[SOFTBLOCK] vibrations...[SOFTBLOCK] merge...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All parts join.[SOFTBLOCK] Which was I?[SOFTBLOCK] I see myself twice.[SOFTBLOCK] Who are these?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, remain in control of yourself.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Maturity.[SOFTBLOCK] This flesh was maturity.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 26.25, 30.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        if(bIsSX399Present == false) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Vivify", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The vibrations join together, and we are one.[SOFTBLOCK] I am maturity, I am life, I am the giver.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Presumably, when the proximity is reduced, Christine's old self will reassert itself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Though I am no longer speaking to her, am I?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine.[SOFTBLOCK] Can you understand me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We can.[SOFTBLOCK] What the flesh sees, we see.[SOFTBLOCK] What the flesh hears, we hear.[SOFTBLOCK] What the flesh understands, we understand.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Project Vivify, I have come to negotiate.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I wish for an end to the hostilities between our people.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No reaction...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What would Christine do in this situation?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Why are you sad, Vivify?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] This flesh...[SOFTBLOCK] It is not moving.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] We have vibrating and moved and pushed, but it is not moving.[SOFTBLOCK] Why is it not moving?[SOFTBLOCK] This makes us sad.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You mean the girl there?") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("55", 24.25, 32.00)
            fnCutsceneMove("55", 24.25, 29.50)
            fnCutsceneMove("55", 24.75, 29.50)
            fnCutsceneFace("55", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Vivify", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This human has been dead for some time.[SOFTBLOCK] Her body is cold.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The flesh isn't different from the others, but it does not move.[SOFTBLOCK] It makes us sad.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Did you kill her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The flesh resisted.[SOFTBLOCK] It stopped resisting, now it does not move.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You have retired many units, and yet somehow, only now, are you noticing?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Vivify.[SOFTBLOCK] What is death?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We.[SOFTBLOCK] That word.[SOFTBLOCK] The flesh knows the word but we do not.[SOFTBLOCK] What is death?[SOFTBLOCK] Death?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Die.[SOFTBLOCK] Dying.[SOFTBLOCK] To Die.[SOFTBLOCK] Death.[SOFTBLOCK] Inflicted, carried, borne.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] When living creatures are harmed, they die.[SOFTBLOCK] Permanently.[SOFTBLOCK] Sometimes they can be saved, but only a few moments after dying.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] When dead, they can not be brought back to life.[SOFTBLOCK] Ever.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The flesh is the same as this.[SOFTBLOCK] It only changed slightly and now it does not move.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The difference between a living and dead human is slight, true, but life is fragile.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You, Project Vivify were created when an artifact was placed within the dead body of a human in this very facility.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It began to move, but showed no life signs.[SOFTBLOCK] It was restrained and transported to the new Cryogenics facility south of the city.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are not alive, and have never been alive.[SOFTBLOCK] You don't know what life is, do you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The flesh is dead, but does not move...[SOFTBLOCK] We have done this...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We have done this many times.[SOFTBLOCK] We...[SOFTBLOCK] cannot change this?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All is not repeatable, all is not set?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] This girl is dead, you caused it, and you can never take it back.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You must live with this burden and the burden you have placed on her friends.[SOFTBLOCK] They will now have a hole in their lives that cannot be filled.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] You cannot escape it, you cannot ignore it.[SOFTBLOCK] You must make amends as best you can, and you must move on...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We require time to think.[SOFTBLOCK] We must think about ourselves.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We will release this flesh.[SOFTBLOCK] We are...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Sorry.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sorry.[SOFTBLOCK] Apologizing.[SOFTBLOCK] Admitting error.[SOFTBLOCK] Regret.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We are sorry.[SOFTBLOCK] We will go now.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Camera refocuses on Vivify.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 2.0)
                CameraEvent_SetProperty("Focus Actor Name", "Vivify")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("55", 1, -1)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("Vivify", 26.25, 27.50, 0.50)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorFreezer") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Moves into the freezer.
            fnCutsceneMove("Vivify", 26.25, 24.50, 0.50)
            fnCutsceneFace("Vivify", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(35)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorFreezer") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: Beginning cryogenic freezing cycle.[SOFTBLOCK] Please clear the chamber.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Do that here.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|FreezeOver") ]])
            fnCutsceneSetFrame("Vivify", "Freeze0")
            fnCutsceneWait(60)
            fnCutsceneBlocker()
            fnCutsceneSetFrame("Vivify", "Freeze1")
            fnCutsceneWait(60)
            fnCutsceneBlocker()
            fnCutsceneSetFrame("Vivify", "Freeze2")
            fnCutsceneWait(60)
            fnCutsceneBlocker()
            fnCutsceneSetFrame("Vivify", "Freeze3")
            fnCutsceneWait(60)
            fnCutsceneBlocker()
            fnCutsceneSetFrame("Vivify", "Freeze4")
            fnCutsceneWait(180)
            fnCutsceneBlocker()
            
            --Camera refocuses on Christine.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 2.0)
                CameraEvent_SetProperty("Focus Actor Name", "Christine")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 5.0)
                CameraEvent_SetProperty("Focus Actor Name", "Christine")
            DL_PopActiveObject()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She has voluntarily decided to freeze herself?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The Epsilon Laboratories were where the original Cryogenics was built, before it was moved.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Even in all this, some of the equipment is still here and functioning.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] True, but that does nothing to aid us.[SOFTBLOCK] She had broken containment before.[BLOCK][CLEAR]") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("20", 26.25, 43.50)
            fnCutsceneMove("20", 26.25, 31.50)
            fnCutsceneBlocker()
            fnCutsceneFaceTarget("Christine", "20")
            fnCutsceneFaceTarget("55", "20")
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] I see.[SOFTBLOCK] Even one such as her did not know all and see all.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Could she be wrong about the future?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes and no.[SOFTBLOCK] The future has happened, but it is not set.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] But how can this be?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Because the past is not set.[SOFTBLOCK] All that exists is the present, and the future and past are selected to create the present.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We sit at the nadir with all possible pasts and futures before us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You must expand your scope of thought.[SOFTBLOCK] There is nothing wrong with being wrong.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] I can see why you are the favoured.[SOFTBLOCK] Very well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Those that still listen to me will aid me.[SOFTBLOCK] We will move the master someplace away from here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, she will be this way for some time.[SOFTBLOCK] I fear the machine people here will try to use her as a weapon again.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Perhaps the machine people should not have this knowledge forced on them.[SOFTBLOCK] They are not ready.[SOFTBLOCK] I know they are not, because it seems I was not.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] But this does not equalize us, Christine.[SOFTBLOCK] I will continue to study, and I will supercede you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You will not.[SOFTBLOCK] You think only in terms of heirarchy and advantage.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If you have always one eye on your goal, then you have only one eye on your path.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] More wisdom from the master?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Wisdom from Earth.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Hrmph.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Leave, Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Come on, everyone.[SOFTBLOCK] We're done here.") ]])
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("20", 30.25, 31.50)
            fnCutsceneFace("20", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneMove("55", 24.75, 30.50)
            fnCutsceneMove("55", 26.25, 30.50)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Vivify", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The vibrations join together, and we are one.[SOFTBLOCK] I am maturity, I am life, I am the giver.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Presumably, when the proximity is reduced, Christine's old self will reassert itself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Presumably?[SOFTBLOCK] Is that good enough for you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] We need to blast that thing and get Christine far away from it![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] How can you be calm at a time like this!?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We must be calm, precise, and rational.[SOFTBLOCK] If we make a mistake, we will lose Christine and the city.[SOFTBLOCK] Forever.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I hate how often you're right.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Maturity.[SOFTBLOCK] It said the flesh was that.[SOFTBLOCK] I believe the flesh refers to Christine's body.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Maturity is a mistranslation of something?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Perhaps, perhaps not.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine.[SOFTBLOCK] Can you understand me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We can.[SOFTBLOCK] What the flesh sees, we see.[SOFTBLOCK] What the flesh hears, we hear.[SOFTBLOCK] What the flesh understands, we understand.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Project Vivify, we have come to negotiate.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We wish for an end to the hostilities between our people.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] It's not working...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] T-[SOFTBLOCK]try something else![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What would Christine do in this situation?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Why are you sad, Vivify?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] This flesh...[SOFTBLOCK] It is not moving.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] We have vibrating and moved and pushed, but it is not moving.[SOFTBLOCK] Why is it not moving?[SOFTBLOCK] This makes us sad.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You mean the girl there?") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("55", 24.25, 32.00)
            fnCutsceneMove("55", 24.25, 29.50)
            fnCutsceneMove("55", 24.75, 29.50)
            fnCutsceneFace("55", 1, 0)
            fnCutsceneMove("SX399", 28.25, 32.00)
            fnCutsceneMove("SX399", 28.25, 29.50)
            fnCutsceneMove("SX399", 27.75, 29.50)
            fnCutsceneFace("SX399", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Vivify", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This human has been dead for some time.[SOFTBLOCK] Her body is cold.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The flesh isn't different from the others, but it does not move.[SOFTBLOCK] It makes us sad.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Did you kill her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The flesh resisted.[SOFTBLOCK] It stopped resisting, now it does not move.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] It did kill her.[SOFTBLOCK] Why is that different?[SOFTBLOCK] Haven't they done that a hundred times?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Vivify.[SOFTBLOCK] What is death?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We.[SOFTBLOCK] That word.[SOFTBLOCK] The flesh knows the word but we do not.[SOFTBLOCK] What is death?[SOFTBLOCK] Death?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Die.[SOFTBLOCK] Dying.[SOFTBLOCK] To Die.[SOFTBLOCK] Death.[SOFTBLOCK] Inflicted, carried, borne.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] When living creatures are harmed, they die.[SOFTBLOCK] Permanently.[SOFTBLOCK] Sometimes they can be saved, but only a few moments after dying.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] When dead, they can not be brought back to life.[SOFTBLOCK] Ever.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The flesh is the same as this.[SOFTBLOCK] It only changed slightly and now it does not move.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The difference between a living and dead human is slight, true, but life is fragile.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But how could you not know that?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Project Vivify was created when an artifact was placed within the dead body of a human in this very facility.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It began to move, but showed no life signs.[SOFTBLOCK] It was restrained and transported to the new Cryogenics facility south of the city.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hence the name, Vivify.[SOFTBLOCK] She is not alive, but moving.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But killing is wrong, Vivify.[SOFTBLOCK] Are you really saying you did not know?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The flesh is dead, but does not move...[SOFTBLOCK] We have done this...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We have done this many times.[SOFTBLOCK] We...[SOFTBLOCK] cannot change this?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All is not repeatable, all is not set?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] This girl is dead, you caused it, and you can never take it back.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You must live with this burden and the burden you have placed on her friends.[SOFTBLOCK] They will now have a hole in their lives that cannot be filled.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] You cannot escape it, you cannot ignore it.[SOFTBLOCK] You must make amends as best you can, and you must move on...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We require time to think.[SOFTBLOCK] We must think about ourselves.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We will release this flesh.[SOFTBLOCK] We are...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Sorry.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sorry.[SOFTBLOCK] Apologizing.[SOFTBLOCK] Admitting error.[SOFTBLOCK] Regret.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We are sorry.[SOFTBLOCK] We will go now.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Camera refocuses on Vivify.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 2.0)
                CameraEvent_SetProperty("Focus Actor Name", "Vivify")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("55", 1, -1)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneFace("SX399", -1, -1)
            fnCutsceneMove("Vivify", 26.25, 27.50, 0.50)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorFreezer") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Moves into the freezer.
            fnCutsceneMove("Vivify", 26.25, 24.50, 0.50)
            fnCutsceneFace("Vivify", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(35)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 2.0)
                CameraEvent_SetProperty("Focus Position", (26.25 * gciSizePerTile), (24.50 * gciSizePerTile))
            DL_PopActiveObject()
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorFreezer") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: Beginning cryogenic freezing cycle.[SOFTBLOCK] Please clear the chamber.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Do that here.
            local iTPF = 6
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|FreezeOver") ]])
            
            --Once the audio begins, start running across the layers like frames of an animation. There are 100 layers 
            -- at 3TPF for 300 ticks, or 5 seconds.
            local sThisAnim, sNextAnim, iFirst, iSecond
            fnCutsceneLayerDisabled("CloudsAnim00", false)
            fnCutsceneWait(iTPF)
            fnCutsceneBlocker()
            for i = 1, 99, 1 do
                
                --Hide this layer.
                iFirst = math.floor((i-1) / 10)
                iSecond = math.floor((i-1) % 10)
                sThisAnim = "CloudsAnim" .. iFirst .. iSecond
                fnCutsceneLayerDisabled(sThisAnim, true)
                
                --Show this layer.
                iFirst = math.floor(i / 10)
                iSecond = math.floor(i % 10)
                sNextAnim = "CloudsAnim" .. iFirst .. iSecond
                fnCutsceneLayerDisabled(sNextAnim, false)
                
                --At specific moments, change Vivify's freeze.
                if(i == 40) then
                    fnCutsceneSetFrame("Vivify", "Freeze0")
                elseif(i == 50) then
                    fnCutsceneSetFrame("Vivify", "Freeze1")
                elseif(i == 60) then
                    fnCutsceneSetFrame("Vivify", "Freeze2")
                elseif(i == 70) then
                    fnCutsceneSetFrame("Vivify", "Freeze3")
                elseif(i == 80) then
                    fnCutsceneSetFrame("Vivify", "Freeze4")
                end
                
                --Time.
                fnCutsceneWait(iTPF)
                fnCutsceneBlocker()
                
            end
            
            --Hide the last layer.
            fnCutsceneLayerDisabled("CloudsAnim99", true)
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            
            --Camera refocuses on Christine.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 1.0)
                CameraEvent_SetProperty("Focus Actor Name", "Christine")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 5.0)
                CameraEvent_SetProperty("Focus Actor Name", "Christine")
            DL_PopActiveObject()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] She...[SOFTBLOCK] froze herself?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The Epsilon Laboratories were where the original Cryogenics was built, before it was moved.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Even in all this, some of the equipment is still here and functioning.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But what now?[SOFTBLOCK] Is she just going to sit in there?") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("20", 26.25, 43.50)
            fnCutsceneMove("20", 26.25, 31.50)
            fnCutsceneBlocker()
            fnCutsceneFaceTarget("Christine", "20")
            fnCutsceneFaceTarget("55", "20")
            fnCutsceneFaceTarget("SX399", "20")
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] I see.[SOFTBLOCK] Even one such as her did not know all and see all.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Could she be wrong about the future?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes and no.[SOFTBLOCK] The future has happened, but it is not set.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] But how can this be?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Because the past is not set.[SOFTBLOCK] All that exists is the present, and the future and past are selected to create the present.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We sit at the nadir with all possible pasts and futures before us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You must expand your scope of thought.[SOFTBLOCK] There is nothing wrong with being wrong.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] I can see why you are the favoured.[SOFTBLOCK] Very well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Those that still listen to me will aid me.[SOFTBLOCK] We will move the master someplace away from here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, she will be this way for some time.[SOFTBLOCK] I fear the machine people here will try to use her as a weapon again.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Perhaps the machine people should not have this knowledge forced on them.[SOFTBLOCK] They are not ready.[SOFTBLOCK] I know they are not, because it seems I was not.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] But this does not equalize us, Christine.[SOFTBLOCK] I will continue to study, and I will supercede you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You will not.[SOFTBLOCK] You think only in terms of heirarchy and advantage.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If you have always one eye on your goal, then you have only one eye on your path.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] More wisdom from the master?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Wisdom from Earth.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Hrmph.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "201890:[E|Neutral] Leave, Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Come on, everyone.[SOFTBLOCK] We're done here.") ]])
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("20", 30.25, 31.50)
            fnCutsceneFace("20", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneMove("SX399", 27.75, 30.50)
            fnCutsceneMove("SX399", 26.25, 30.50)
            fnCutsceneMove("55", 24.75, 30.50)
            fnCutsceneMove("55", 26.25, 30.50)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
        end
    end

--Switch forms.
elseif(sObjectName == "NoGoingBack") then

    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local iMet20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet20", "N")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(iMet20 == 1.0 and sChristineForm ~= "Human") then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMet20", "N", 2.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, Unit 2855?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you intending to remain in that form?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is it a problem?[SOFTBLOCK] The threat has passed, I am myself.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are certain of that, but I am not.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You don't trust her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I trust Christine a great deal.[SOFTBLOCK] But I have not gotten as far as I have by being reckless.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] That's fair.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Am I making you uncomfortable?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Please?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] As a favour to you?[SOFTBLOCK] Certainly.") ]])
        fnCutsceneBlocker()

        --Flashwhite.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite", "Null")
        DL_PopActiveObject()
        fnCutsceneBlocker()

        fnCutsceneWait(75)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
            
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 771852, back in squishy form![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Interesting.[SOFTBLOCK] I was anticipating your golem form.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I...[SOFTBLOCK] want to breathe.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It feels right to have air go in and out of my lungs, somehow.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Organics.[SOFTBLOCK] They're all the same, aren't they?[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This form is acceptable for our purposes.[SOFTBLOCK] Let us continue.") ]])
        fnCutsceneBlocker()
        
    end
end
