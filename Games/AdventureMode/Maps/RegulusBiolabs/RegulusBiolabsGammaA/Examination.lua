--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToGammaB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGammaB", "FORCEPOS:44.0x4.0x0")
    
--[Objects]
elseif(sObjectName == "OilMaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An oilmaker.[SOFTBLOCK] The only flavour packets available are steel shavings.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Instructions for the team here to seal the door and proceed to the Raiju Ranch.[SOFTBLOCK] A direct order from Unit 2856.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An inventory of equipment and units stationed here.[SOFTBLOCK] All but two are listed as relocated.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Sorry, I won't be able to attend our sunrise watching, Lauralie.[SOFTBLOCK] Just got an important order from the Head of Research to relocate my whole team to the Raiju Ranch.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I tried to get confirmation but now the radios are out.[SOFTBLOCK] This message will send as soon as a network connection is established.[SOFTBLOCK] Hope you haven't left already![SOFTBLOCK] Love you.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An unencrypted list of Slave Unit designations, marked for...[SOFTBLOCK] retirement.[SOFTBLOCK] Fortunately, only a few are listed as retired.[SOFTBLOCK] I hope the rest got away...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Five Habits of Highly Successful Lord Units'.[SOFTBLOCK] I get this on my terminal from Command Unit 105322 every morning.[SOFTBLOCK] Not even security units are immune to the power of chain letters.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A test pattern is playing, indicating the network connection is offline.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A videograph named 'Proper Decontamination Procedures' is playing on loop.[SOFTBLOCK] It shows a researcher washing off her chassis and clothes thoroughly.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A note on the oilmaker says 'LORD UNITS ONLY'.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Rack") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A weapon rack with a pulse rifle and spare charge packs. Why is it here and not taken like the others?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computer") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A computer left here by the Lord Golem in charge of this facility.[SOFTBLOCK] The hard drive has been wiped.)") ]])
    fnCutsceneBlocker()

--Exit, has a cutscene. Breaching tools are needed.
elseif(sObjectName == "BreachDoor") then
    local iBreachedDoor      = VM_GetVar("Root/Variables/Chapter5/Scenes/iBreachedDoor", "N")
    local iGotBreachingTools = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotBreachingTools", "N")
    
    --Door is already open, just move to the next room.
    if(iBreachedDoor == 1.0) then
        AL_BeginTransitionTo("RegulusBiolabsEpsilonA", "FORCEPOS:7.5x40.0x0")
        
    --Door isn't open and we don't have the tools.
    elseif(iBreachedDoor == 0.0 and iGotBreachingTools == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This door leads into the Epsilon labs, but the deadbolts are down and the edges were welded shut.[SOFTBLOCK] It's effectively a wall.)") ]])
        fnCutsceneBlocker()
        
    --Door isn't open and we have the tools.
    elseif(iBreachedDoor == 0.0 and iGotBreachingTools == 1.0) then
        
        --Vareiables.
        local bIsSX399Present = fnIsCharacterPresent("SX399")
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iBreachedDoor", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right loves, stand back and let a repair unit get to work.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will keep you covered.") ]])
        fnCutsceneBlocker()
        
        --Fade out.
        fnCutsceneWait(25)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --Sounds.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Come on you stupid -[SOFTBLOCK] woah![SOFTBLOCK] This welder has some kick to it![SOFTBLOCK] Woo![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] What happened?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Were you not watching?[SOFTBLOCK] I almost sawed my own arm off![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I have other things I am attending to.[SOFTBLOCK] Specifically, protecting you.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Change layers.
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Floor2Cut", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Floor2Uncut", true) ]])
        
        --Reposition.
        fnCutsceneTeleport("Christine", 45.75, 3.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneTeleport("55", 44.25, 6.50)
        fnCutsceneFace("55", 0, 1)
        if(bIsSX399Present == true) then
            fnCutsceneTeleport("SX399", 47.25, 6.50)
            fnCutsceneFace("SX399", -1, -1)
        end
        
        --Fade in.
        fnCutsceneWait(45)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Uhhhh, all done?") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Movement.
        fnCutsceneMoveFace("Christine", 45.75, 4.50, 0, -1)
        fnCutsceneMove("55", 44.75, 4.50)
        fnCutsceneFace("55", 0, -1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 46.75, 4.50)
            fnCutsceneFace("SX399", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        if(bIsSX399Present == false) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Excellent.[SOFTBLOCK] We can access the Epsilon Laboratories now.[SOFTBLOCK] Good work.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Good work?[SOFTBLOCK] I wound up sawing through it![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] This military-grade plasteel has absolutely no give.[SOFTBLOCK] If I had known that when I started...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The nature of the cut does not matter, Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Maybe not to you.[SOFTBLOCK] Any repair unit would laugh at this.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just please don't tell Sophie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If I agree not to tell your tandem unit, will you drop this pointless issue?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Deal.[SOFTBLOCK] Let's go.") ]])
            fnCutsceneBlocker()
            
            --Fold.
            fnCutsceneMove("55", 45.75, 4.50)
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Excellent.[SOFTBLOCK] We can access the Epsilon Laboratories now.[SOFTBLOCK] Good work.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Look at that![SOFTBLOCK] First try on military-grade plasteel![SOFTBLOCK] You're a pro![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Are you kidding?[SOFTBLOCK] I had to just saw through it![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Yes, that is the point.[SOFTBLOCK] That's why they made it the way they did.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Though if you get lucky sometimes recrystallization exposes a plane of weakness.[SOFTBLOCK] But that's probably not the case for the new stuff.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Have you dealt with this material before?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] All the time.[SOFTBLOCK] A lot of transport cars on the tram network are made out of repurposed plasteel.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Breaking and entering is a hobby.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess I did the best I could with the tools at hand.[SOFTBLOCK] That's all a repair unit can ask for.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I assure you, complaining about it will increase the quality of the cut.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Can we continue?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] All right, all right.[SOFTBLOCK] Let's go.") ]])
            fnCutsceneBlocker()
            
            --Fold.
            fnCutsceneMove("55", 45.75, 4.50)
            fnCutsceneMove("SX399", 45.75, 4.50)
            fnCutsceneBlocker()
        end
        
        --Fold.
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    end
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end