--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "TransitionToBetaE") then
    
    --The variable needed is the "pipes released" one.
    local iAmphibianPipesReleased = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPipesReleased", "N")
    if(iAmphibianPipesReleased == 0.0) then
        AL_BeginTransitionTo("RegulusBiolabsBetaE", "FORCEPOS:31.0x34.0x0")
    else
        AL_BeginTransitionTo("RegulusBiolabsBetaMeltedE", "FORCEPOS:31.0x34.0x0")
    end
end
