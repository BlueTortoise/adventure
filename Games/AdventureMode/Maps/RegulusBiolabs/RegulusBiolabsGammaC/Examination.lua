--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "ToAlphaD") then
    
    --Variables.
    local iRepoweredGamma          = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")

    --Door is not repowered.
    if(iRepoweredGamma == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawGammaAirlockSequence", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The deadbolts on the door are down, and it's not receiving power.[SOFTBLOCK] We can't even pry it open.[SOFTBLOCK] Maybe if we can repower this area?)") ]])
    
    --Door is repowered.
    else
    
        --Christine organic form checker:
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        if(sChristineForm == "Human" or sChristineForm == "Raiju") then
    
            --Variables:
            local iHasLatexForm         = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
            local iHasDarkmatterForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
            local iHasEldritchForm      = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
            local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
            local iHasSteamDroidForm    = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
            local iSawRaijuIntro        = VM_GetVar("Root/Variables/Global/Christine/iSawRaijuIntro", "N")
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (The area beyond this door is depressurized.[SOFTBLOCK] Better take on an inorganic form before I go out there.)[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            if(iHasDarkmatterForm == 1.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"TFToDarkmatter\") ")
            end
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"TFToGolem\") ")
            if(iHasLatexForm == 1.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"TFToLatex\") ")
            end
            if(iHasEldritchForm == 1.0 and iSawRaijuIntro == 1.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Dreamer\",  " .. sDecisionScript .. ", \"TFToEldritch\") ")
            end
            if(iHasElectrospriteForm == 1.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Electrosprite\",  " .. sDecisionScript .. ", \"TFToElectrosprite\") ")
            end
            if(iHasSteamDroidForm == 1.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Steam Droid\",  " .. sDecisionScript .. ", \"TFToSteam\") ")
            end
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"NoCancel\") ")
            fnCutsceneBlocker()
    
        --Inorganic:
        else
            AudioManager_PlaySound("World|AutoDoorOpen")
            AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0")
        end
    end
    
elseif(sObjectName == "TerminalA") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('WARNING:: POWER MODULATORS --'[SOFTBLOCK] The terminal keeps cutting out before it can print the rest of the statement.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Power modulation system reinitialized.[SOFTBLOCK] Contact repair crew to replace damaged components.'[SOFTBLOCK] It then displays a very, very long list of damaged electrical components...)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalB") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Modulator system che --'[SOFTBLOCK] The terminal is unable to boot the modulator before it surges.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Autogenerated Repair Unit Warning::[SOFTBLOCK] Unauthorized loopback tampering in Gamma Modulator 7-R.[SOFTBLOCK] Please contact a security unit.')") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalC") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The terminal is offline...)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('So we can hire partirhuman contractors from Pandemonium, but nobody has thought of hiring bee girls yet?[SOFTBLOCK] That's solve our pollination problems and then some!')[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I understand that it's not in our budget, but think of the benefits![SOFTBLOCK] Plus we can probably collect excess honey for sale to Lord Units.[SOFTBLOCK] Maybe that'd bring in some extra work credits.')") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalD") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The terminal is offline...)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I had thought this experiment was meant to establish nutrient density's relation to genetics, but I was told to increase productivity.[SOFTBLOCK] Productivity?[SOFTBLOCK] In an experiment?')[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('But I asked around and it turns out some genius in the Beta labs has been fermenting my berries with hops from Hydroponics, and distributing the booze.[SOFTBLOCK] Lovely.')[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I'm afraid to terminate the project now, so I guess we'll just be researching quality of alcohol flavour in relation to genetics.[SOFTBLOCK] At least I won't run short of budget allocation...')") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "RVD") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The RVD is offline...)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The diminuitive ant, so called in relation to its big-sister, the antgirl, is a eusocial insect species with a very wide range on Pandemonium.')[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('As of this recording, research habitats are being constructed that will allow the Biolabs to understand how these species construct their underground colonies.')[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Central Administration believes that we can learn more from the habits of ants, particularly transport and storage techniques.[SOFTBLOCK] It is believed their techniques could be of use in the Regulus Secundus project.[SOFTBLOCK] Truly, there is much we can learn from the biological world.')") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] The oilmaker...[SOFTBLOCK] is empty![SOFTBLOCK] There's no oil, no flavours![SOFTBLOCK] Nothing![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] An act of industrial sabotage, surely.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] What kind of monster would do that?[SOFTBLOCK] Surely one beyond redemption.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Wire") then
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N", 1.0)
        
        --Version where SX-399 is not in the party:
        if(bIsSX399Present == false) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well let's see here.[SOFTBLOCK] I know I'm not much of a repair unit, but I'd say that's your problem right there.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Someone sabotaged the power modulators and stuck a main wire into the water.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] For those of us who are not repair units, please explain.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Are you actually asking about my job?[SOFTBLOCK] Without already knowing the answer?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] And here I thought you knew everything, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not.[SOFTBLOCK] I research only what I deem to be of vital interest, and download secondary manuals when possible.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am not familiar with a power modulator unit.[SOFTBLOCK] Please elaborate.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] They're not that complex.[SOFTBLOCK] They're an adjustable analog computational unit that stops too much electricity going out to a given wire.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] They basically prevent power surges from frying equipment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If you hook several modulators in sequence, though, they can transmit back up the line to choke voltage at the source.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Normally, if you put a main wire into the water, the modulators would just stop the flow.[SOFTBLOCK] They were removed, so the power modulators didn't work and all the systems got fried.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A deliberate act of sabotage, then.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not Vivify's modus operandi.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] But the golem we saw at the gala...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are not dealing with a mindless destructive force anymore, Christine.[SOFTBLOCK] Regulus City's security forces have already been compromised and outmaneuvered.[SOFTBLOCK] We should assume ambushes and sabotage will become more commonplace, now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Her name was 20, right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Isn't she just like us?[SOFTBLOCK] A rebel?[SOFTBLOCK] Someone freed from their bondage, and taking revenge on their oppressors?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Why is what she's doing different from what we're doing?[SOFTBLOCK] Shouldn't we be friends with her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We have the same enemy, but not the same goal.[SOFTBLOCK] 20 doesn't ask, she takes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She's not like us at all.[SOFTBLOCK] We don't...[SOFTBLOCK] change...[SOFTBLOCK] others.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You seemed pretty excited to hear about Abductions from that Command Unit, Christine.[SOFTBLOCK] Were you, perhaps, not acting?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] W-[SOFTBLOCK]well, I guess you got me there![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's a shame, but I guess I can't compromise my principles.[SOFTBLOCK] We'll make Abductions an all-volunteer program when we've liberated the city.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] You sound almost disappointed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Maybe a little.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Anyway, I cut the wire and sent a reboot order.[SOFTBLOCK] The modulators should work now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The lights are blown out, but at least the doors should work now. [SOFTBLOCK][EMOTION|Christine|PDU]PDU, please notify Unit 2856 that she can now enter the Gamma Labs.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator] Message sent.[SOFTBLOCK] Reply received.[SOFTBLOCK] A video call is coming...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] PDU, cancel video call.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] You enjoy tormenting my sister far too much.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Just desserts.[SOFTBLOCK] It's a phrase from Earth.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] She says she's on her way to Transit Station Omicron.[SOFTBLOCK] That's east of here.[SOFTBLOCK] Let's get going.") ]])
            fnCutsceneBlocker()
        
        --SX-399 is in the party.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well let's see here.[SOFTBLOCK] I know I'm not much of a repair unit, but I'd say that's your problem right there.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Someone sabotaged the power modulators and stuck a main wire into the water.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] For those of us who are not repair units, please explain.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Are you actually asking about my job?[SOFTBLOCK] Without already knowing the answer?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] And here I thought you knew everything, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] It's her second best quality.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And the first?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] That little smirk she gets when she's figured something out but you haven't...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not 'Know Everything', Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I research only what I deem to be of vital interest, and download secondary manuals when possible.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] [EMOTION|SX-399|Neutral]I am not familiar with a power modulator unit.[SOFTBLOCK] Please elaborate.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] They're not that complex.[SOFTBLOCK] They're an adjustable analog computational unit that stops too much electricity going out to a given wire.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] They basically prevent power surges from frying equipment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If you hook several modulators in sequence, though, they can transmit back up the line to choke voltage at the source.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Normally, if you put a main wire into the water, the modulators would just stop the flow.[SOFTBLOCK] They were removed, so the power modulators didn't work and all the systems got fried.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A deliberate act of sabotage, then.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not Vivify's modus operandi.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] But the golem we saw at the gala...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] 20.[SOFTBLOCK] And she was probably less than half golem.[SOFTBLOCK] Probably a quarter doll, and the other quarter...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are not dealing with a mindless destructive force anymore, Christine.[SOFTBLOCK] Regulus City's security forces have already been compromised and outmaneuvered.[SOFTBLOCK] We should assume ambushes and sabotage will become more commonplace, now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Isn't she just like us?[SOFTBLOCK] A rebel?[SOFTBLOCK] Someone freed from their bondage, and taking revenge on their oppressors?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Why is what she's doing different from what we're doing?[SOFTBLOCK] Shouldn't we be friends with her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We have the same enemy, but not the same goal.[SOFTBLOCK] 20 doesn't ask, she takes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She's not like us at all.[SOFTBLOCK] We don't...[SOFTBLOCK] change...[SOFTBLOCK] others.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You seemed pretty excited to hear about Abductions from that Command Unit, Christine.[SOFTBLOCK] Were you, perhaps, not acting?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] W-[SOFTBLOCK]well, I guess you got me there![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's a shame, but I guess I can't compromise my principles.[SOFTBLOCK] We'll make Abductions an all-volunteer program when we've liberated the city.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Let me put it this way for you, Sophie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] We Steam Droids?[SOFTBLOCK] Nobody's better than someone else.[SOFTBLOCK] Truly, because if my eyes work, then my ears probably don't.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So I pair up with someone whose eyes don't work, but their ears do.[SOFTBLOCK] We gotta team up.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Prior to your upgrade, you mean?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] I still team up with units smarter and cuter than me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Meanwhile, the golems?[SOFTBLOCK] Someone's on top, someone's on bottom.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] It's a big club, and you ain't in it.[SOFTBLOCK] So what did that palooka at the gala say?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] She's mad, since Christine is on top, and she's not.[SOFTBLOCK] She's okay with there being a top and a bottom, she just wants to be on top.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Vivify and her gang of weirdos are a lot closer to the golems than the steam droids, if you get me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] When you put it like that...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] SX-399 understands intuitively what a rich girl like me had to be taught.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] We're all in it together.[SOFTBLOCK] You can't watch your own back.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Install optical receptors in the rear of your cranial chassis.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Not literally, babe.[SOFTBLOCK] How about::[SOFTBLOCK] We all gotta sleep sometime.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Traps, turrets, and mines around your defragmentation pod.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, 55.[SOFTBLOCK] You can be independent, strong, and forced to live in a fortress surrounded by turrets and bombs for fear of having even one friend who can betray you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ... [BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I see.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Anyway, I cut the wire and sent a reboot order.[SOFTBLOCK] The modulators should work now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The lights are blown out, but at least the doors should work now. [SOFTBLOCK][EMOTION|Christine|PDU]PDU, please notify Unit 2856 that she can now enter the Gamma Labs.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|Narrator] Message sent.[SOFTBLOCK] Reply received.[SOFTBLOCK] A video call is coming...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] PDU, cancel video call.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] You enjoy tormenting my sister far too much.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Just desserts.[SOFTBLOCK] It's a phrase from Earth.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] She says she's on her way to Transit Station Omicron.[SOFTBLOCK] That's east of here.[SOFTBLOCK] Let's get going.") ]])
            fnCutsceneBlocker()
        
        end
    end
    
--[Transformation]
--Causes Christine to change forms before heading out an airlock.
elseif(sObjectName == "TFToDarkmatter") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToDarkmatter/Scene_Begin.lua")
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])
    
elseif(sObjectName == "TFToGolem") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToGolem/Scene_Begin.lua")
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])
    
elseif(sObjectName == "TFToLatex") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToLatexDrone/Scene_Begin.lua")
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])
    
elseif(sObjectName == "TFToEldritch") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToEldritch/Scene_Begin.lua")
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])
    
elseif(sObjectName == "TFToElectrosprite") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToElectrosprite/Scene_Begin.lua")
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])

elseif(sObjectName == "TFToSteam") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToSteamDroid/Scene_Begin.lua")
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])

elseif(sObjectName == "NoCancel") then
	WD_SetProperty("Hide")
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end