--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Hydrophobe") then
    
    --Variables.
    local bIsSX399Present     = fnIsCharacterPresent("SX399")
    local iSawHydrophobe      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawHydrophobe", "N")
    local iGammaPowerRestored = VM_GetVar("Root/Variables/Chapter5/Scenes/iGammaPowerRestored", "N")
    
    --Haven't seen the cutscene and didn't take the shortcut,
    if(iSawHydrophobe == 0.0 and iGammaPowerRestored == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawHydrophobe", "N", 1.0)
        
        --Scene.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Christine, there's a power station.") ]])
        fnCutsceneBlocker()
        
        --Move the party.
        fnCutsceneMove("Christine", 33.25, 3.50)
        fnCutsceneFace("Christine", 1, 1)
        fnCutsceneMove("55", 34.25, 3.50)
        fnCutsceneFace("55", 1, 1)
        fnCutsceneMove("Sophie", 35.25, 3.50)
        fnCutsceneFace("Sophie", 1, 1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 36.25, 3.50)
            fnCutsceneFace("SX399", 1, 1)
        end
        fnCutsceneBlocker()
        
        --Move the camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Position", (54.25 * gciSizePerTile), (12.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move the camera back.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sabotage would explain why the lights are blown out.[SOFTBLOCK] Come on, we can just jump over this gap.[SOFTBLOCK] Follow me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Wait...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 34.25, 3.50)
        fnCutsceneMove("Christine", 34.25, 3.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMoveFace("55", 34.25, 2.50, 0, 1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You might need to back up a bit...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] My predictions estimate you'll need a two-meter running start.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Mmmm...[SOFTBLOCK] Oh, don't splash, I might get my dress wet.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] This course of action is incorrect.[SOFTBLOCK] The probability of failure is too high.[SOFTBLOCK] Reconsider.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Facing.
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneFace("Sophie", -1, -1)
        if(bIsSX399Present == true) then
            fnCutsceneFace("SX399", -1, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --SX-399 is not present:
        if(bIsSX399Present == false) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's not that far, 55.[SOFTBLOCK] Besides, the water's pretty shallow.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Unit 499323 risks soiling her garments.[SOFTBLOCK] You should not risk that by leaping this gap.[SOFTBLOCK] Find another path.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It's just a little water, 55.[SOFTBLOCK] Besides, this dress is synthweave.[SOFTBLOCK] It washes out very easily.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You are not taking into account the penalties for failure.[SOFTBLOCK] You could sink.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Then I'll be the first to dive in and save her.[SOFTBLOCK] Besides, your own estimation matrices must point out that there's almost no chance for failure.[SOFTBLOCK] And, the water is barely waist-deep.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] 99 percent is not 100 percent.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I'm willing to take a 99 percent leap here, 55.[SOFTBLOCK] For crying out loud.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I am not.[SOFTBLOCK] Find another path.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Maybe we should just go around, Christine.[SOFTBLOCK] It's not very far, right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] She's just trying to protect us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right.[SOFTBLOCK] Follow me.[SOFTBLOCK] There's a bridge to the south there.") ]])
            fnCutsceneBlocker()
        
        --SX-399 is present.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's not that far, 55.[SOFTBLOCK] Besides, the water's pretty shallow.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Unit 499323 risks soiling her garments.[SOFTBLOCK] You should not risk that by leaping this gap.[SOFTBLOCK] Find another path.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] She's right.[SOFTBLOCK] Never risk a gorgeous outfit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] I would never![SOFTBLOCK] Please, come to your senses, SX![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But it is a little bit of an overreaction.[SOFTBLOCK] Isn't that dress made of synthweave?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Machine-wash, warm![SOFTBLOCK] Easy to launder.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] So a little puddle isn't a problem.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] It is.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Huh.[SOFTBLOCK] Are you sure?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Why are you making such a big issue of this?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Find another path, 771852.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Maybe we should just go around, Christine.[SOFTBLOCK] It's not very far, right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] She's just trying to protect us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right.[SOFTBLOCK] Follow me.[SOFTBLOCK] There's a bridge to the south there.") ]])
            fnCutsceneBlocker()
        
        end
    
        --Fold the party.
        fnCutsceneMove("Christine", 34.25, 3.50)
        fnCutsceneMove("55", 34.25, 3.50)
        fnCutsceneMove("Sophie", 34.25, 3.50)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 34.25, 3.50)
        end
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    end
    
elseif(sObjectName == "GoBack") then
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Oh joy.[SOFTBLOCK] It's you-know-who.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Can you maybe not yell at her?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I don't like it when people are fighting...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] To make you happy?[SOFTBLOCK] Anything.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] [EMOTION|Sophie|Neutral]What is it, 2856?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] You just connected to the network node in the Gamma labs.[SOFTBLOCK] So I have to ask, why haven't you fixed the door yet?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] I need that door open to relocate, and your CURRENT SOLE USEFUL PURPOSE TO ME IS DOING THAT.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] We're on it.[SOFTBLOCK] Stand by.[SOFTBLOCK] You should receive a notification when it's fixed.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2855] ...[SOFTBLOCK] Thank you, Unit 771852.[SOFTBLOCK] Continue.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well that worked![SOFTBLOCK] Let's go fix the power issue.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] So I can tell her to get stuffed in person.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] (I don't like when they argue, but she's so hot when she does this...[SOFTBLOCK] Am I a bad unit?)") ]])
        fnCutsceneBlocker()
    
        --Fold the party.
        fnCutsceneMove("Christine", 60.25, 20.50)
        fnCutsceneMove("55", 60.25, 20.50)
        fnCutsceneMove("Sophie", 60.25, 20.50)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 60.25, 20.50)
        end
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
        
    end
end
