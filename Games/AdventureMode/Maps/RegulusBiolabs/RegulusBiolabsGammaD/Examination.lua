--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "ToTransitA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsTransitA", "FORCEPOS:4.0x13.0x0")
    
elseif(sObjectName == "Boxes") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Spare sports equipment and repair equipment.[SOFTBLOCK] Polymer for fixing a tennis racket, adhesive for fixing balls, and a welder for fixing a scraped chassis.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Shelf") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A shelf to hold extra sports equipment.[SOFTBLOCK] Someone left their tennis racket and some balls here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "DoorN") then
    AL_SetProperty("Close Door", "DoorS")
    
elseif(sObjectName == "DoorS") then
    AL_SetProperty("Close Door", "DoorN")
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end