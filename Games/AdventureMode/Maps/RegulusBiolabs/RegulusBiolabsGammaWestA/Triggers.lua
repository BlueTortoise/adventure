--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "RaibiesScene") then
    
    --Variables.
    local iRaibieDrank = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N")
    if(iRaibieDrank ~= 1.0) then return end
    
    --Set leader to 2855.
    WD_SetProperty("Set Leader Voice", "2855")
    
    --Variables.
    local iSX399JoinsParty  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N", 2.0)
    
    --Dialogue.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Position", (40.25 * gciSizePerTile), (41.50 * gciSizePerTile))
        CameraEvent_SetProperty("Max Move Speed", 1.0)
    DL_PopActiveObject()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (???)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Merge.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 42.25, 41.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneMove("55", 41.25, 41.50)
    fnCutsceneFace("55", 1, 0)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX399", 41.25, 42.50)
        fnCutsceneFace("SX399", 1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Jeez, 55![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is probably not the time for - [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Shut up![SOFTBLOCK] Why are you so mean to me all the time?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I did not - [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] And your voice - ohhh it makes me so mad![SOFTBLOCK] You're always harping on me and insulting me -[SOFTBLOCK] ARRRGGHH!!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] W-[SOFTBLOCK]wait, 55. I'm sorry![SOFTBLOCK] I would never![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] But you need to stop insulting me![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I did not insult you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] J-[SOFTBLOCK]just...[SOFTBLOCK] need to cool down...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMoveFace("Christine", 45.25, 41.50, 1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As expected, the PDU indicates a reaction...[BLOCK][CLEAR]") ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Antibody production increasing threefold every eight seconds?[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, are you feeling anything unusual?[SOFTBLOCK] Was this outburst related to your dosing?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMoveFace("Christine", 42.25, 41.50, -1, 0, 3.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Strike_Crit") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
    
    --If SX-399 is present:
    if(iSX399JoinsParty == 1.0) then
        
        --Reposition.
        fnCutsceneTeleport("Christine", -100, -100)
        fnCutsceneTeleport("55", 33.25, 30.50)
        fnCutsceneSetFrame("55", "Downed")
        fnCutsceneTeleport("SX399", 34.25, 30.50)
        fnCutsceneSetFrame("SX399", "Wounded")
        
        --Glass layers.
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "BrokenGlass", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "NormalGlass", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "BrokenGlassHi", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "NormalGlassHi", true) ]])
    
        --Wait a bit.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Focus Actor Name", "SX399")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(245)
        fnCutsceneBlocker()
        
        --Fade in.
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] (Gyroscope auto-stabilizier recalibrated.[SOFTBLOCK] Rebooting cognitive procedures.)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneSetFrame("SX399", "Crouch")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("SX399", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] (My cranial chassis got a few dents I'll need to buff out...[SOFTBLOCK] What happened?)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneFace("SX399", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] (55 is in system-standby...[SOFTBLOCK] But I know how to reset her...[SOFTBLOCK] Wait hold on, there's a lockout?)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Lockout bypassed.[SOFTBLOCK] Reactiviting consciousness protocols...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneSetFrame("55", "Crouch")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("55", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("55", 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Wakey-wakey, 55.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My -[SOFTBLOCK] my chrono -[SOFTBLOCK] internal...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Let your boot finish before you talk.[SOFTBLOCK] Don't overdo it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My internal chronometer reports thirteen minutes of unaccounted for time.[SOFTBLOCK] My CPU also reports a maintenance lockout.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Can you confirm the lost time?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Yep.[SOFTBLOCK] I just got up and removed the lockout.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Last thing I remember was Christine coming at you faster than a bullet.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is what my memory files indicate, as well.[SOFTBLOCK] I was unprepared and did not move in time.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] And considering the military-grade glass which is shattered next to us, I'm thinking that stuff may have given Christine quite a kick.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But why didn't she say anything?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I cannot say, other than that she clearly had a reason or a plan.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She put me into maintenance lockout, presumably to have you remove it, and left us here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Hold on, your PDU is flashing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] 'Am inside.[SOFTBLOCK] Not myself.[SOFTBLOCK] Stop me.[SOFTBLOCK] -C'[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Maybe she couldn't concentrate enough to transform herself with the runestone?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] But she still did her best.[SOFTBLOCK] There won't be anyone to harm in the Tissue Testing Laboratories.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] True, but considering her newfound strength, can we stop her?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You don't need to stop her, you goof.[SOFTBLOCK] You just need to neutralize her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will need to be adaptable, now that our flimsy plans have fallen apart.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will enter the facility and attempt to stop Christine.[SOFTBLOCK] You remain here and see to it she doesn't get out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] You got it.[SOFTBLOCK] Try not to get taken down again.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] You did not fare any better than I.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Oh, I let her win.[SOFTBLOCK] I'm a good sport like that.[SOFTBLOCK] Now get to it!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --SX-399 is not present:
    else
        
        --Reposition.
        fnCutsceneTeleport("Christine", -100, -100)
        fnCutsceneTeleport("55", 33.25, 30.50)
        fnCutsceneSetFrame("55", "Downed")
        
        --Glass layers.
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "BrokenGlass", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "NormalGlass", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "BrokenGlassHi", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "NormalGlassHi", true) ]])
    
        --Wait a bit.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Focus Actor Name", "55")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(245)
        fnCutsceneBlocker()
        
        --Fade in.
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Maintenance lockout reached timeout, rebooting...") ]])
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneSetFrame("55", "Crouch")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("55", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("55", -1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My -[SOFTBLOCK] recalibrating...[SOFTBLOCK] Finishing secondary table setup...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Internal chronometer reports thirteen minutes in maintenance lockout.[SOFTBLOCK] Christine?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] She is not here, and the broken military-grade glass suggests her exit path.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine must have been unable to use her runestone, and disabled me...[SOFTBLOCK] Why?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I'll need to recover her.[SOFTBLOCK] Luckily, she won't find any civilians inside the Tissue Testing Facility.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Perhaps that was her plan.[SOFTBLOCK] Perhaps I have misjudged her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No matter.[SOFTBLOCK] With luck, I will be able to find some way of neutralizing Christine within the facility.[SOFTBLOCK] I will fix this.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    
    end
    
    --Remove party members so it's just 55.
    AL_SetProperty("Unfollow Actor Name", "55")
    AL_SetProperty("Unfollow Actor Name", "SX399")
    EM_PushEntity("55")
        local i55ID = RO_GetID()
    DL_PopActiveObject()
    AL_SetProperty("Player Actor ID", i55ID)
    AC_SetProperty("Set Party", 0, "55")
    AC_SetProperty("Set Party", 1, "Null")
    AC_SetProperty("Set Party", 2, "Null")
    
    --55 enters the building.
    fnCutsceneMove("55", 30.25, 30.50)
    fnCutsceneFace("55", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsGammaWestC", "FORCEPOS:12.0x20.0x0") ]])
    fnCutsceneBlocker()

--[Defeated by Raibie Christine]
elseif(sObjectName == "RaibieDefeat") then

    --Black the screen out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
	AL_SetProperty("Music", "Null")
    
    --Variables.
    local iSX399JoinsParty       = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iRaibieFoundNoisemaker = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundNoisemaker", "N")
    local iRaibieFoundKeycode    = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundKeycode", "N")
    local iRaibieFoundTranqGun   = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundTranqGun", "N")
    
    --Reposition.
    fnCutsceneTeleport("55", 30.25, 31.50)
    fnCutsceneSetFrame("55", "Wounded")
    fnCutsceneBlocker()
    
    --If SX-399 is present, spawn her.
    if(iSX399JoinsParty == 1.0) then
        fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
        fnCutsceneTeleport("SX399", 31.25, 31.50)
        fnCutsceneFace("SX399", -1, 0)
    end

    --Fade in.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Crouch")
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Dialogue.
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Damage... registered...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] You're fine. Looks like Christine went easy on you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Unfortunately the pavement didn't when you hit it. But a reinforced chassis can absorb that kind of it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What happened?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] You came sailing through the window is what happened. I fixed you up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And Christine?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Still in there. She growled at me and ran off.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] I think she's trying her best to fight it...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I have failed her this time...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Find any useful clues in there? There's got to be a way to stop her.[BLOCK][CLEAR]") ]])
        if(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 0.0) then
            
            --Not found keycode.
            if(iRaibieFoundKeycode == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have been investigating but not found any useful clues yet.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Did you check all the terminals and crates? Must be something useful.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not yet.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Sounds like a plan. Good luck.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I will not give up.") ]])
            
            --Found the keycode.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have found a keycode that opens the airlock on the west side.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Have you checked the storage shed out there?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not yet, but doubtless something I will need is there.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Sounds like a plan. Good luck.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I will not give up.") ]])
            end
        
        --Found noisemaker but not the tranq gun:
        elseif(iRaibieFoundNoisemaker == 1.0 and iRaibieFoundTranqGun == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I located a noisemaking device in a crate.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You think Christine will respond to a loud bang?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Possibly. There is a shooting range in the basement with realistic dummies.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] However, if I lured her there, I do not have a way of subduing her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Yet.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Correct. One must exist, and I will find it.") ]])
        
        --Found tranq gun but not noisemaker:
        elseif(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I located a tranquiliser gun in a storage shed. If it can break her skin, that will at least slow Christine down.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But you didn't use it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is faster than I am in her enraged form. I would need a way to distract her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There are some dummies downstairs used for target practice. But I would need to lure her to them first.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] So find a way to lure her to the dummies, then tranq her? Sounds like a plan![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I will make another attempt. Resume your position here.") ]])
        
        --Found tranq and noisemaker:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I located a tranquiliser gun in a storage shed. If it can break her skin, that will at least slow Christine down.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have also located a noisemaking device, often used for Lord Golem parties.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So... You're going to distract Christine and then shoot her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes. However, I would need her to remain stationary.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There are some dummies downstairs used for target practice. Perhaps if I detonate the noisemaker near them...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Sounds like a plan![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I will make another attempt. Resume your position here.") ]])
        
        end
        fnCutsceneBlocker()
        
        --Stand and walk.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("55", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("55", 0, -1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 30.25, 30.50)
        fnCutsceneFace("SX399", -1, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Transition.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsGammaWestC", "FORCEPOS:12.0x20.0x0") ]])
        fnCutsceneBlocker()
        
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Damage...[SOFTBLOCK] registered...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My sensors indicate a cranial chassis impact...[SOFTBLOCK] Christine must have thrown me through the window...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] But she has not followed. She must have enough control over herself for that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What can I do to improve my performance?[BLOCK][CLEAR]") ]])
        if(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 0.0) then
            
            --Not found keycode.
            if(iRaibieFoundKeycode == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have been investigating but not found any useful clues yet.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I must continue to search the terminals and crates in the basement. There must be some useful clue...") ]])
            
            --Found the keycode.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have found a keycode that opens the airlock on the west side.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] With luck, I can find something useful in the storage shed out there.") ]])
            end
        
        --Found noisemaker but not the tranq gun:
        elseif(iRaibieFoundNoisemaker == 1.0 and iRaibieFoundTranqGun == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I located a noisemaking device in a crate.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] However, lacking a way of neutralizing Christine, I will only succeed in distracting her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If I do find a way to neutralize her, the target dummies will do nicely.") ]])
        
        --Found tranq gun but not noisemaker:
        elseif(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I located a tranquiliser gun in a storage shed. If it can break her skin, that will at least slow Christine down.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unfortunately, she is considerably faster than I am in her enraged form. I would need a way to distract her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There are some dummies downstairs used for target practice. But I would need to lure her to them first.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] If I can find a device to lure her there...") ]])
        
        --Found tranq and noisemaker:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I located a tranquiliser gun in a storage shed. If it can break her skin, that will at least slow Christine down.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have also located a noisemaking device, often used for Lord Golem parties.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Perhaps the realistic dummies near the target range will distract her if I can lure her to them. I must at least try that.") ]])
        
        end
        fnCutsceneBlocker()
        
        --Stand and walk.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("55", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("55", 0, -1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 30.25, 30.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Transition.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsGammaWestC", "FORCEPOS:12.0x20.0x0") ]])
        fnCutsceneBlocker()
        
    end

--[No Music]
--Disables the music in the outdoor area.
elseif(sObjectName == "NoMusic") then
	AL_SetProperty("Music", "Null")

end
