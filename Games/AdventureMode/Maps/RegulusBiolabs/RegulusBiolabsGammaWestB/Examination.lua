--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "SampleExit") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGammaA", "FORCEPOS:29.0x45.0x0")
    
--[Objects]
elseif(sObjectName == "Barrels") then

    --Variables
    local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
    local iRaibieQuest      = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
    local iSX399JoinsParty  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Haven't found these yet:
    if(iRaibieFoundDrums == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N", 1.0)
        
        --Player is not far enough along the quest to know what this is:
        if(iRaibieQuest < 3.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well hello, what do we have here?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Six barrels arranged in a rectangular formation on the western half of the Gamma Laboratories.[BLOCK][CLEAR]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] At some point, Christine is going to figure out she shouldn't ask dumb questions.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Honestly?[SOFTBLOCK] I'm too stubborn to not be an idiot.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No, what I mean is::[SOFTBLOCK] Why are these barrels here and why are they leaking?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] See the dead grass leading right into the water supply?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Organic contaminants allowed to leak into the groundwater may not be an accident.[SOFTBLOCK] In fact, there is an infiltration testing range in the Delta Laboratories.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] [SOUND|World|TakeItem]I'm going to collect a sample anyway.[SOFTBLOCK] It might come in handy later.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Hmmm...[SOFTBLOCK] My PDU doesn't have the best sensor suite, but the material looks carcinogenic.[BLOCK][CLEAR]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] It's also colourless, odorless, and dissolves in water.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Screams 'bioweapon' to me.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A bioweapon?[SOFTBLOCK] Perhaps one of Unit 2856's countermeasures to the invaders?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Maybe, maybe not.[SOFTBLOCK] We'll have to look into it later.[SOFTBLOCK] Let's get going.") ]])
            fnCutsceneBlocker()
        
        --Far enough along the quest:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Chemical storage barrels far away from a storage depot...[SOFTBLOCK] This is suspicious.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, did Dr. Maisie brief you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes, she informed me via mail about your plans.[SOFTBLOCK] Do you think these barrels are related to your investigation?[BLOCK][CLEAR]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] They do seem out of place.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] They're clearly leaking into the water supply.[SOFTBLOCK] Look, you can see a line of dead grass.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Organic contaminants allowed to leak into the groundwater may not be an accident.[SOFTBLOCK] In fact, there is an infiltration testing range in the Delta Laboratories.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] True, but this is active in an area where the Raibies are present and would serve as a vector.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] My PDU says they're a candidate...[SOFTBLOCK] but they don't match perfectly...[BLOCK][CLEAR]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] It's also colourless, odorless, and dissolves in water.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Screams 'bioweapon' to me.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Perhaps one of Unit 2856's countermeasures to the invaders?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't think so.[SOFTBLOCK] Let's get this back to Dr. Maisie and see what she has to say.") ]])
            fnCutsceneBlocker()
        
        
        end
    
    --Repeat:
    else
    
        --Not far enough along the quest:
        if(iRaibieQuest < 6.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A colourless, odorless chemical that dissolves readily in water...[SOFTBLOCK] Obvious bioweapon candidate.[SOFTBLOCK] I've already got a sample of it.)") ]])
            fnCutsceneBlocker()
        
        --Time to take a hit!
        elseif(iRaibieQuest == 6.0) then
        
            --Christine has to be a Raiju:
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
            --Not in Raiju form:
            if(sChristineForm ~= "Raiju") then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This is the right place, but I need to be a Raiju in order to dose myself with it.)") ]])
                fnCutsceneBlocker()
        
            --In Raiju form:
            else
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay team, are we ready?[BLOCK][CLEAR]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] My gun is ready.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Please do not melt Christine.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] You kidding?[SOFTBLOCK] Her head's so thick, I probably wouldn't get past the first layer![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I will not attempt a counterargument.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You two are my very best friends.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are ready.[SOFTBLOCK] The rest is up to you.[SOFTBLOCK] Are you ready?[BLOCK][CLEAR]") ]])
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am as prepared as is possible.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The rest is up to you.[SOFTBLOCK] Are you ready?[BLOCK][CLEAR]") ]])
                end
                
                --Dialogue.
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Shall I take a drink of E-v89-r?[SOFTBLOCK] Better make sure I'm absolutely ready.)[BLOCK]") ]])

                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
                fnCutsceneBlocker()
            end
        
        --No dice!
        elseif(iRaibieQuest == 7.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (What a bust![SOFTBLOCK] This must not have been the vector.[SOFTBLOCK] We had best go back to Dr. Maisie.)") ]])
            fnCutsceneBlocker()
        
        --Post-sequence.
        elseif(iRaibieQuest == 8.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This nasty stuff is the cause of the Raibies outbreak.[SOFTBLOCK] Hopefully Dr. Maisie can get it under control now.)") ]])
            fnCutsceneBlocker()
        end
    end

--[Decisions]
elseif(sObjectName == "Yes") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 7.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right... *slurp*") ]])
    fnCutsceneBlocker()
    
    --Fade.
    fnCutsceneWait(25)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Scene.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine took a tiny bit of the substance in her hands and slurped it down.[SOFTBLOCK] It burned like whisky as it went, and she tensed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She stopped, and listened.[SOFTBLOCK] The area was still.[SOFTBLOCK] 55 put her hand on Christine's shoulder.[SOFTBLOCK] She was shaking.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "After a few moments, the shaking subsided.[SOFTBLOCK] Christine burped.[SOFTBLOCK] She smiled at 55, who frowned in response.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Unfortunately, it seemed this was not the cause of the Raibies outbreak.[SOFTBLOCK] 55's PDU, connected to the monitoring nanites, reported no effects.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine, while slightly upset, was more relieved.[SOFTBLOCK] She wasn't going to give up just yet, but the prospect of risking becoming a raging monster still was not her ideal outcome.[SOFTBLOCK] She grinned.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Unfade.
    fnCutsceneWait(25)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well that wasn't so bad, was it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Odd.[SOFTBLOCK] Dr. Maisie's mail seemed certain this was the vector, but I am reading no effects.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Let's go pay her a visit and figure out what do next.[SOFTBLOCK] And to get me something to wash the awful taste out of my mouth!") ]])
    fnCutsceneBlocker()
    
    --Despawn enemies.
    if(EM_Exists("EnemyAA") == true) then
        EM_PushEntity("EnemyAA")
            RE_SetDestruct(true)
        DL_PopActiveObject()
    end
    if(EM_Exists("EnemyAB") == true) then
        EM_PushEntity("EnemyAB")
            RE_SetDestruct(true)
        DL_PopActiveObject()
    end
    if(EM_Exists("EnemyBA") == true) then
        EM_PushEntity("EnemyBA")
            RE_SetDestruct(true)
        DL_PopActiveObject()
    end
    
elseif(sObjectName == "No") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Actually, not quite yet.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As you wish.") ]])
    fnCutsceneBlocker()

--[Errors]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end