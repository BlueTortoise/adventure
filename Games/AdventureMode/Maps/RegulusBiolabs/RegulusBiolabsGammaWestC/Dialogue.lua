--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Special: No need to go further if just backing out of dialogue.

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --[Lord Golems]
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] I'm a debug NPC. Would you like to complete this event instantly?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
    
    end

--Complete the Raibie event.
elseif(sTopicName == "Yes") then
	WD_SetProperty("Hide")

    --Boot out.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 8.0)
    
    --Go to the Raiju Ranch.
    AL_BeginTransitionTo("RegulusBiolabsRaijuRanchA", "FORCEPOS:46.0x12.0x0")


--Backing out of dialogue.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")


end