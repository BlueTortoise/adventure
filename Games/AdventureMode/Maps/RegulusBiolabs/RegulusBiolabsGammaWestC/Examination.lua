--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "Ladder") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusBiolabsGammaWestD", "FORCEPOS:4.0x9.0x0")
    
elseif(sObjectName == "Exterior") then
    
    --Variables.
    local iRaibieFoundKeycode = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundKeycode", "N")
    if(iRaibieFoundKeycode == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Access denied.[SOFTBLOCK] Quarantine protocols in effect.[SOFTBLOCK] Please specify override code...") ]])
        fnCutsceneBlocker()
    
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsGammaWestA", "FORCEPOS:22.0x23.0x0")
    end
    
--[Objects]
elseif(sObjectName == "Exit") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The glass was shattered, but there's no blood.[SOFTBLOCK] Christine probably didn't even injure herself from the blow.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "OilMaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (When the units evacuated, they took all the oil with them.[SOFTBLOCK] They also took the flavour packets.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The terminal is showing an evacuation notice.[SOFTBLOCK] Quarantine protocols were put in place after the units left.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Everything is to be disposed of on-site.[SOFTBLOCK] Which of course means the incinerator is broken and nobody is able to fix it because all the spare parts are replaced with guns and body armor.[SOFTBLOCK] What am I supposed to do with a tranquilizer gun if I can't take it offsite?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Boxes") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There is nothing useful in the boxes concerning Christine's infection.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Door") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The door is shut and quarantine protocols are in place.[SOFTBLOCK] Christine has not gone past this point.)") ]])
    fnCutsceneBlocker()

--[Errors]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end