--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Special: No need to go further if just backing out of dialogue.

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = "None"
if(sTopicName ~= "YesChristine" and sTopicName ~= "Yes55" and sTopicName ~= "NoCancel") then
    sActorName = TA_GetProperty("Name")
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --[Rebel Golems]
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[VOICE|RebelGolem] You know Captain Jo used to be just a welding unit?[SOFTBLOCK] She's got the courage to stare down a bullet train!") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[VOICE|RebelGolem] I joined up so I could be just like 771852 one day.[SOFTBLOCK] And now, here we are, face to face![SOFTBLOCK] It's an honour, ma'am.") ]])
        
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[VOICE|RebelGolem] I set up a monitoring application.[SOFTBLOCK] Everyone who got hit is in one of the domiciles above, and they can send a signal if they need something.") ]])
        
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[VOICE|RebelGolem] We all decided to drop our designations and give ourselves proper names.[SOFTBLOCK] I'm Leela, and the captain over there is named Jo.[SOFTBLOCK] I think she's a good Jo, don't you?") ]])
        
    elseif(sActorName == "GolemF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[VOICE|RebelGolem] I'll keep watch on the door.[SOFTBLOCK] Do what you gotta do.") ]])
        
    elseif(sActorName == "PolishRebel") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[VOICE|RebelGolem] The only way to get the prisoner to shut up is to order her to polish my chassis.[SOFTBLOCK] It was her idea to use her tongue, don't judge me!") ]])
        
    elseif(sActorName == "GolemLord") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Affirmative, Rebel Unit.[SOFTBLOCK] Whatever you ask, Rebel Unit.[SOFTBLOCK] Mmmmmmmm...") ]])
        
    elseif(sActorName == "Jo") then
        
        --Variables.
        local iAquaticsCompleted = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsCompleted", "N")
        local iSX399JoinsParty   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        
        --Player has not completed the subquest:
        if(iAquaticsCompleted == 0.0) then
    
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Anything I can do for you lot?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Give me a quick refresher on the situation east of here.[SOFTBLOCK] What do we need to do?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] While I'm against this...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] The security units locked off the eastern access with a blue seal.[SOFTBLOCK] Not only does it lock the doors, it prevents network access.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] You'll need to get a blue keycard somewhere.[SOFTBLOCK] I'm sure you can find one if you look.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] The security team also blew up an access bridge, presumably to cover their retreat.[SOFTBLOCK] You'll need to either fix it, or find a way around.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And then?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Assuming you get past the hostiles, locked doors, and blown bridge?[SOFTBLOCK] There's a gun-nest overlooking a bridge with no cover.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Our units are on the other side of that bridge.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm a repair unit.[SOFTBLOCK] Even if I can't haul them back, I can at least stabilize them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] That'd be more than enough.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] But absolutely don't do any hero crap.[SOFTBLOCK] This war has been on a few hours and I've already lost a lot of friends.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll make sure you don't lose any here.[SOFTBLOCK] We're fighting for peace.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] A lasting one, I hope.[SOFTBLOCK] Good hunting out there.") ]])
        
        --Subquest completed:
        elseif(iAquaticsCompleted == 1.0) then
        
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsCompleted", "N", 2.0)
    
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Well well well, if it isn't the hero of the units.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Rescued three units by selflessly almost getting yourself killed?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hello, Jo![SOFTBLOCK] We have a successful mission to report![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] We saw the whole thing on the security cameras.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You said you were unable to access the systems on the east side of the facility.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] That's right, boss.[SOFTBLOCK] But then, someone, and I won't say who, opened the blue lockdown door.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] So apparently, as long as the keycard is in that door, we could access the network by piggybacking the door controls.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] A security vulnerability I was unaware of.[SOFTBLOCK] Excellent work, captain.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Presumably you set up a tunneler for when the card is removed?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Probably, you'd have to ask my network unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Point is, we now have a videograph of Christine doing idiot-heroics to rescue downed units.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Terrific, right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Does she have a death wish?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I believe so.[BLOCK][CLEAR]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Zest for life means risking death.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So have you sent anyone to pick up the units?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] They made contact a moment ago.[SOFTBLOCK] We'll be dredging them up in a few minutes and getting them the repairs they need.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Now, obviously I can't go around condoning such madness.[SOFTBLOCK] There was a much larger chance that hit you took would be fatal.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Regardless of your transformative abilities, you're not invincible.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] But...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But I'm more important to the revolution than mere battlefield activities?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] But...[SOFTBLOCK] Thank you for saving my friends.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] When this is all over, I'll buy you a synthtwister and we'll laugh about how dumb we were.[BLOCK][CLEAR]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Are.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I've known Christine for a little while now and I'll tell you one thing.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Becoming a robot has made her braver...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] But it sure hasn't made her any smarter![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Hey![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] We had to do something while covering your escape route.[SOFTBLOCK] So we came up with quips.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Among other things.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Right, that's all I can ask of you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] We'll get ourselves ship-shape and move out to the transit station as soon as possible.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] It might be some time before we see each other again, so I'll say it now.[SOFTBLOCK] On behalf of everyone here, it's been an honour.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Likewise.[SOFTBLOCK] Take care, captain.") ]])
        
        --Repeats.
        else
    
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] We'll be sending out a videograph of a crazed hero saving downed units and taking a bullet while doing it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It makes a great morale booster![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Oh, no.[SOFTBLOCK] It's to terrify the administration.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Look at how absolutely bonkers our units are![SOFTBLOCK] The loyalists don't have a chance!") ]])
        
        end
        
    elseif(sActorName == "Isabel") then
        
        --Variables.
        local bIsSX399Present = fnIsCharacterPresent("SX399")
        local iAquaticsSpokeToIsabel = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsSpokeToIsabel", "N")
        
        --First encounter:
        if(iAquaticsSpokeToIsabel == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsSpokeToIsabel", "N", 1.0)
    
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Isabel", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] (Oh criminy, it's Christine!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Isabel, was it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] (And she knows my name!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hello.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] (Aaaaack!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Are you all right?[SOFTBLOCK] You're not saying anything...[BLOCK][CLEAR]") ]])
            
            --SX-399 variation.
            if(bIsSX399Present == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] She's awestruck.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] ...............[SOFTBLOCK] oof.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's kind of your catch phrase, isn't it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] I'm not trying to be rude, Christine.[SOFTBLOCK] It's just -[SOFTBLOCK] you're the reason I'm here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Really?[SOFTBLOCK] I'm flattered![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] It was my idea to record the recruitment videos.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral][EMOTION|Christine|Smirk] Yes, but...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] I had heard some mutterings among my staff.[SOFTBLOCK] I -[SOFTBLOCK] I'm not much of a leader.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] I worked in janitorial services at the Arcane University.[SOFTBLOCK] You know, making sure the rooms were clean and the oil makers were full.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] And a couple days ago, my second in command comes up to me and says she thinks the Slave Units are up to something.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] I went and asked, and...[SOFTBLOCK] they showed me a video.[SOFTBLOCK] Of you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] The same one playing all over the city?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Yeah, that one...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] And I realized that I didn't have to be the bad guy.[SOFTBLOCK] So I got in contact with the big boss...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Isabel is one of a few Lord Units who have decided to back our movement.[SOFTBLOCK] Her access codes have been very useful.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] ...[SOFTBLOCK] Too late to go back now, right?[SOFTBLOCK] Now I'm carrying around a pulse rifle and fighting the good fight.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] And it's all thanks to you.[SOFTBLOCK] You showed me that it's possible for a Lord Golem to be one of the good guys.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Odd, though.[SOFTBLOCK] I thought most Lord Units would want leadership roles.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] ...[SOFTBLOCK] oof.[SOFTBLOCK] No thanks.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Jo is the best leader I've seen bar none.[SOFTBLOCK] I would probably just panic and screw something up.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Not all of us got picked to be Lord Units because we're good leaders.[SOFTBLOCK] Some of us got picked because we didn't question orders.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What about the rest of your staff?[SOFTBLOCK] Some of them might not forgive you for the things you've done to them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] ...[SOFTBLOCK] Yeah.[SOFTBLOCK] My record isn't exactly clean.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] But if I provide covering fire for someone like Jo, well, that's as good a redemption as I'll get.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] I'm going to try to improve as much as I can.[SOFTBLOCK] Maybe someday I'll be as good a leader as she is.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] But for now, I'm just glad I got to meet you in person.[SOFTBLOCK] It's been a real pleasure.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey, it's not over yet.[SOFTBLOCK] There's going to be some dark times ahead.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] ...[SOFTBLOCK] oof.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But if you switched sides, maybe some other Lord Units will.[SOFTBLOCK] Maybe the fighting will be over before you know it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] I sure hope so.[SOFTBLOCK] Good luck, Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Same to you, Isabel.") ]])
        
        --Repeats.
        else
    
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Isabel", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] You know, I've heard the phrase 'You should never meet your heroes'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Probably because they're not what you think they are in reality.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Sometimes they are.[SOFTBLOCK] That's all.") ]])
        
        
        end
    end
end