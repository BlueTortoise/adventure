--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToDeltaI") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsDeltaI", "FORCEPOS:51.5x9.0x0")
    
elseif(sObjectName == "ToGeneticsB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsB", "FORCEPOS:4.5x27.0x0")
    
--[Objects]
elseif(sObjectName == "SignA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Welcome to the Aquatic Genetics research lab![SOFTBLOCK] Please check your coat with the attendant -[SOFTBLOCK] things get wet here![SOFTBLOCK] Enjoy your stay!')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "SignB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('These tanks house the large aquatic mammals under study.[SOFTBLOCK] There is an underwater passage they can swim through that connects to the next room.[SOFTBLOCK] Watch out -[SOFTBLOCK] the seals love to boop guests!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Water") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An aquatic habitat that connects to the next room underwater.[SOFTBLOCK] The connection hatch is closed, so there's nothing in here right now.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Some small tropical fish are darting here and there, nibbling on bits of food.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There are some small crabs walking to and fro on the bottom of the habitat.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A sign on the aquarium says 'Currently being cleaned'.[SOFTBLOCK] There is a lobster in the tank, nibbling on the muck within.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Some colorful tropical fish are swimming in circles.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I think I can see the eyes of an eel hidden in the shadowy recesses of the habitat.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This aquarium houses some corals.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There is a robotic fish in here.[SOFTBLOCK] It flits about with a camera on it, recording the other fish without drawing attention to itself.[SOFTBLOCK] Stealthy!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A guest list, indicating whose coat belong to who.[SOFTBLOCK] An evacuation notice blinks on the top, and only one administrator is currently listed as present.[SOFTBLOCK] Looks like she's in the southern transit office.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The administrator's console.[SOFTBLOCK] The rebels have a program scanning it for tactical information.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A list of appointments for the administrator here.[SOFTBLOCK] They are all marked as cancelled.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like the staff here all evacuated.[SOFTBLOCK] The rebels are using their quarters for repair work.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hey, it's a recording I made a month ago![SOFTBLOCK] 55 and I made it to encourage everyone.[SOFTBLOCK] I'm telling them that they need to keep calm and stay strong, and work together for the future.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The RVD is powered off.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The rebels drank all the oil.[SOFTBLOCK] They used every flavour and even drank some unflavoured oil -[SOFTBLOCK] desperate times, and all that.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computer") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Jo has repurposed the administrator's portable computer and has the building's schematics displayed along with targets and objectives.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Banner") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A banner depicting the rebel symbol -[SOFTBLOCK] a killphrase plate, broken in half.[SOFTBLOCK] Never shall we again be slaves.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyPop") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Fizzy Pop! is a fantastic way to deal with the stress of being shot at![SOFTBLOCK] Is there nothing it can't do?)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end