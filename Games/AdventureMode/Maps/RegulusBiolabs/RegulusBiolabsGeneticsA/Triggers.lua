--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Meeting") then
    
    --Variables.
    local iAquaticMeeting = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticMeeting", "N")
    if(iAquaticMeeting == 1.0) then return end
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    
    --Disable Isabel's collision flag.
    TA_ChangeCollisionFlag("Isabel", false)
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticMeeting", "N", 1.0)
    
    --Spread the party.
    fnCutsceneMove("Christine", 5.75, 12.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("55", 6.75, 12.50)
    fnCutsceneFace("55", 0, 1)
    if(bIsSX399Present == true) then
        fnCutsceneMove("SX399", 4.75, 12.50)
        fnCutsceneFace("SX399", 0, 1)
    end
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|RebelGolem] Hold it right there!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMoveFace("GolemF", 5.25, 17.50, 0, -1, 2.50)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    if(bIsSX399Present == false) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] Unit 771852?[SOFTBLOCK] Unit 2855?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Present.[SOFTBLOCK] What squad are you?[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] Unit 771852?[SOFTBLOCK] Unit 2855?[SOFTBLOCK] And -[SOFTBLOCK] uhhh...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] SX-399.[SOFTBLOCK] Steam Droid heavy support.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] H-[SOFTBLOCK]hold on...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] Isabel, get up here!") ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Isabel", 12.75, 25.50, 2.00)
    fnCutsceneFace("Isabel", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorC") ]])
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Isabel", 12.75, 21.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorB") ]])
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Isabel", 12.75, 18.50, 2.00)
    fnCutsceneMove("Isabel", 6.25, 18.50, 2.00)
    fnCutsceneMove("Isabel", 6.25, 17.50, 2.00)
    fnCutsceneFace("Isabel", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneFace("Isabel", 0, -1)
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Isabel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Are we under attack?[SOFTBLOCK] I'm here![SOFTBLOCK] Ack![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] Calm down.[SOFTBLOCK] I just need you to escort these units to Jo.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Is that it?[SOFTBLOCK] Oof...[SOFTBLOCK] Sorry...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] You don't need to apologize.[SOFTBLOCK] Just take them to the back.[SOFTBLOCK] I'll keep watch up here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Wait...[SOFTBLOCK] Is that...?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] Right now, Isabel![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Sorry![SOFTBLOCK] I'm on it!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Isabel", 9.25, 17.50)
    fnCutsceneFace("Isabel", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorA") ]])
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[VOICE|Isabel] This way, please.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 9.25, 15.50)
    fnCutsceneMove("Christine", 9.25, 16.50)
    fnCutsceneMove("55", 9.25, 14.50)
    fnCutsceneMove("55", 9.25, 15.50)
    if(bIsSX399Present == true) then
        fnCutsceneMove("SX399", 9.25, 13.50)
        fnCutsceneMove("SX399", 9.25, 14.50)
    end
    fnCutsceneBlocker()
    fnCutsceneMove("Isabel", 9.25, 18.50)
    fnCutsceneMove("Isabel", 12.75, 18.50)
    fnCutsceneMove("Isabel", 12.75, 34.50)
    fnCutsceneMove("Isabel", 11.25, 34.50)
    fnCutsceneFace("Isabel", 1, 0)
    fnCutsceneMove("Christine", 9.25, 18.50)
    fnCutsceneMove("Christine", 12.75, 18.50)
    fnCutsceneMove("Christine", 12.75, 32.50)
    fnCutsceneMove("Christine", 13.25, 32.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("55", 9.25, 18.50)
    fnCutsceneMove("55", 12.75, 18.50)
    fnCutsceneMove("55", 12.75, 32.50)
    fnCutsceneMove("55", 12.25, 32.50)
    fnCutsceneFace("55", 0, 1)
    if(bIsSX399Present == true) then
        fnCutsceneMove("SX399", 9.25, 18.50)
        fnCutsceneMove("SX399", 12.75, 18.50)
        fnCutsceneMove("SX399", 12.75, 31.50)
        fnCutsceneFace("SX399", 0, 1)
    end
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[VOICE|Isabel] She'll be right out.[SOFTBLOCK] Hey, boss![SOFTBLOCK] Visitors!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Jo", 17.25, 31.50)
    fnCutsceneMove("Jo", 17.25, 32.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorD") ]])
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneMove("Jo", 17.25, 34.50)
    fnCutsceneMove("Jo", 14.25, 34.50)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[VOICE|RebelGolem] Yes?[SOFTBLOCK] What is it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[VOICE|Isabel] We have some distinguished guests.") ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Jo", 13.25, 34.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", 0, -1)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", -1, 0)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", 0, -1)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Isabel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] We are really up a creek now...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Really?[SOFTBLOCK] Why is that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Oh, no reason.[SOFTBLOCK] Just the big cheeses showing up out of nowhere for an inspection when everything has gone wrong.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, this is Captain Jo.[SOFTBLOCK] Former welding unit from Sector 34.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Nice to meet you.[SOFTBLOCK] I am - [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Unit 771852.[SOFTBLOCK] We've seen you on the videographs.[SOFTBLOCK] Unit 2855 told me all about your -[SOFTBLOCK] abilities.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Great![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So let's get to the brass tacks.[SOFTBLOCK] What's gone wrong?[BLOCK][CLEAR]") ]])
    if(bIsSX399Present == true) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] We're here to help, no matter the risk.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Huh?[SOFTBLOCK] Oh.[SOFTBLOCK] Uh, no.[SOFTBLOCK] You can't help.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please state your reasons, Captain.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Simple.[SOFTBLOCK] We lose Christine, and it'll be a massive morale hit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Like it or not, you're very important.[SOFTBLOCK] When units see you on the RVDs, they have hope.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you will recall the videographs we filmed, Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yeah, the ones we did a month ago?[SOFTBLOCK] Where I tell everyone it'll be okay and to rise up and join the rebels?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] If things are on schedule, those videographs are playing all across Regulus City right now.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] They are.[SOFTBLOCK] Some of the golems here joined our company because of those.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Which is why we absolutely can't have you getting retired.[SOFTBLOCK] So, you'll need to find another exfiltration route.[SOFTBLOCK] This one is blocked.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please fill us in on the situation, Captain.[SOFTBLOCK] Specifically, why your company is so far out of position.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Right, these are the facts as I understand them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] My company was assigned to capture and hold Transit Station Upsilon-C.[SOFTBLOCK] We did capture the station without resistance, but suffered a counterattack from security forces.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] They were disorganized but outnumbered us.[SOFTBLOCK] We prioritized evacuating the civilians out along the track.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] We made our way between stations, but a number of them had been collapsed.[SOFTBLOCK] I think the demolition crews were off-target.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hmm.[SOFTBLOCK] I think the 6th Shock Company may have overcharged their explosives.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Either way, we got pretty far off course before we finally found a surface entrance, which led us to the loading bay on the far east side of this facility.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] We moved in as an evacuation order went off.[SOFTBLOCK] Security presence was non-existent.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Then, we came under heavy fire from a security team turret emplacement.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Oh no...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] We took a lot of hits, but they did more damage to the building than to us.[SOFTBLOCK] Still, about half of my company is in need of repairs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But we can fix them, right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] No, because not all of them are with us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What happened?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] I was on the B-team when the shooting started.[SOFTBLOCK] We were on a bridge and badly exposed, no cover whatsoever.[SOFTBLOCK] We got split up.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Some of us made it here and linked up with the rest of the company, and some us are still back there...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] And we're way too beat up to make a move back there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] It's...[SOFTBLOCK] my fault, Captain.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] And beating yourself up about it is going to help how?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] ...[SOFTBLOCK] oof...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] All right.[SOFTBLOCK] So that's the situation.[SOFTBLOCK] We're on it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Excuse me, did you not hear the part about the turret emplacement?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Maybe that wasn't enough.[SOFTBLOCK] They also blew up the bridge leading to the east side of the facility, and locked it down.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] So you'd need an access card, and you'd need to jump a fifty-meter gap, [SOFTBLOCK]*and*[SOFTBLOCK] find the trapped units, assuming any of them are operational.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] As I said, you're too valuable to do that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] And the alternative is leaving them behind?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] I'm not trading twenty units for five.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Both of you are correct.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
    if(bIsSX399Present == true) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Well, obviously.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] If we rescue these units, we're heroes.[SOFTBLOCK] That's a story that gets around.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] And if you get shot?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Martyrs.[SOFTBLOCK] Double-or-nothing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Tch.[SOFTBLOCK] You're far too optimistic about this.[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Rescuing downed units is a propaganda victory.[SOFTBLOCK] It is a story that will catch the attention of the masses.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It will be particularly effective if it happens to be true.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] And if you get shot and scrapped?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] A heroic death is often just as potent.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] There's excellent logic behind every great folly.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] This is war.[SOFTBLOCK] We're going to take losses.[SOFTBLOCK] You have to accept that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I am prepared to accept it.[SOFTBLOCK] I know we can't save everyone.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But I wouldn't be doing this if I didn't believe it was the right thing to do.[SOFTBLOCK] Those units are damaged because of me, because of us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] This isn't [SOFTBLOCK]*just*[SOFTBLOCK] your revolution, Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It isn't, and you're right.[SOFTBLOCK] It belongs to everyone willing to take up arms.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Isabel:[E|Neutral] Boss, I think we should let them try.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] As if we could stop them.[SOFTBLOCK] Some idiots are hell-bent on getting themselves retired.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Leading a rebellion is the task of idealists and lunatics.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] ...[SOFTBLOCK] I suppose that's the kind of crazy that changes the world.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Commander 2855, make sure she survives the mission.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] We've been through worse.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Since you're here, what are our orders?[SOFTBLOCK] We're obviously not capturing the transit station while stuck here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Finish repairs and escort the civilians in your group to the Raiju Ranch northwest of here.[SOFTBLOCK] The security presence there is light.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] From there, move to Transit Station Omicron.[SOFTBLOCK] There are a group of civilian Lord Units there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Take them prisoner?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] More importantly, keep them safe from other hostiles.[SOFTBLOCK] There are unidentified hostile creatures in the Biolabs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Affirmative.[SOFTBLOCK] It will be some time before we're ready to move out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] Your company is doing well so far.[SOFTBLOCK] Keep it up.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And show them Sector 34's best, Captain![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jo:[E|Neutral] Of course.[SOFTBLOCK] As you were, everyone.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Jo moves back to the console.
    fnCutsceneMove("Jo", 17.25, 34.50)
    fnCutsceneMove("Jo", 17.25, 31.50)
    fnCutsceneMove("Jo", 15.25, 31.50)
    fnCutsceneMove("Jo", 15.25, 31.00)
    
    --Party fold.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Re-enable Isabel's collision flag.
    fnCutsceneInstruction([[ TA_ChangeCollisionFlag("Isabel", true) ]])
    
end
