--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToGeneticsA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsA", "FORCEPOS:26.5x15.0x0")
    
elseif(sObjectName == "ToGeneticsC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsC", "FORCEPOS:15.5x26.0x0")
    
elseif(sObjectName == "ToGeneticsD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsD", "FORCEPOS:5.5x4.0x0")
    
elseif(sObjectName == "ToGeneticsE") then
    
    --Variables.
    local iAquaticsKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N")
    
    --No keycard.
    if(iAquaticsKeycard == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Access denied.[SOFTBLOCK] Blue authorization required.[SOFTBLOCK] Contact your administrator for access permissions.") ]])
        fnCutsceneBlocker()
    
    --First time opening the door.
    elseif(iAquaticsKeycard == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N", 2.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Access granted.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsGeneticsE", "FORCEPOS:3.0x12.0x0") ]])
        fnCutsceneBlocker()
    
    --Repeats.
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsGeneticsE", "FORCEPOS:3.0x12.0x0")
    end
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Analysis of Aquatic Mammal Mating Habits'.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (...[SOFTBLOCK] 'Dolphins are one of a small group of mammals known to have sex for recreational purposes.[SOFTBLOCK] This is likely correlated with emotional intelligence and social structures, which have similarities to humans.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Since intelligence is correlated to sex drive, this explains why my tandem unit hasn't made a move on me in months.[SOFTBLOCK] I'm thirsty in the damn desert here, people.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (...[SOFTBLOCK] Geez...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Yesterday, I ordered the production of several latex toys for use in my experiments.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('To my total dearth of surprise, I found some of them had not been delivered to my laboratory.[SOFTBLOCK] So, I did a quick search of the workers' quarters.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I caught several units in the act.[SOFTBLOCK] I was unsure as to what punishment they deserved for misappropriation of research materials.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I can, fortunately, report that no further discipline will be needed.[SOFTBLOCK] After what I did to their rear receiver ports, I doubt they will be doing that again.[SOFTBLOCK] At least not until a repair unit has looked at them first.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('In other news, the fabrication staff indicated they are eager to work on the next shipment of toys for my experiments.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Experimentation logs with the new 'Substance E', which they received from the Epsilon Labs.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Substance E, at first glance, is a non-reactive neutral fluid with a violet color, no odor, and a low boiling point.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('When applied to organic material, however, the fluid begins to self-assemble.[SOFTBLOCK] What is odd is that no material is pulled from the organic subject.[SOFTBLOCK] It seems to derive from the air itself.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('After resolving several technical issues, my next round of experiments will be attempting to determine the location where extra matter is derived. I expect a test in a suspended vacuum chamber will yield insight.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Experimentation logs with the new 'Substance E', which they received from the Epsilon Labs.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The process, which slave units derisively call 'dunking', is simple.[SOFTBLOCK] The subject's body is immersed in Substance E dissolved in water.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Most interesting is the results.[SOFTBLOCK] There is no particular pattern by species, mass, or personality.[SOFTBLOCK] Some subjects are wholly unaffected, while others show an immediate increase in aggression.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I would like to begin a genetically-controlled test next, using cloned aquatic species raised in isolation.[SOFTBLOCK] That should help reduce confounding variables.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Evacuation order is in effect?[SOFTBLOCK] Is this a drill?[SOFTBLOCK] In any case, I'll meet you at Transit Station Omicron.[SOFTBLOCK] See you soon!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I was defragmenting when my Lord Golem came running in screaming about results or somesuch.[SOFTBLOCK] She dragged half the domicile block out of the chambers and lined us all up.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('She pointed at each of us and marked us as 'not' and 'are'.[SOFTBLOCK] Then she went back down to the main floor and jumped into one of the aquariums.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('They've got her in the repair bay in Sector 112 trying to figure out what's wrong with her.[SOFTBLOCK] I don't usually say this, but I feel bad for my Lord Golem.[SOFTBLOCK] Maybe she got some of that Substance E stuff on her?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The seals are just too darn playful to use as experimentation subjects.[SOFTBLOCK] I'll put a motion in to see if we can't get them dedicated to entertainment purposes.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('After every show I get at least one Lord Unit asking about them.[SOFTBLOCK] Having to hack them up and pull their organs out when we're done with them...[SOFTBLOCK] I always lie about their fate...')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalH") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('If you want a good example of biological services being good at their jobs, have I got one for you.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I noticed that some of the grass near the inlet was turning yellow, so I asked them about it.[SOFTBLOCK] Two minutes later, they say someone was dumping waste in.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('A quick check of the security cameras and the offending unit was identified and punished.[SOFTBLOCK] The grass began growing normally afterwards.[SOFTBLOCK] How do they do it?')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I swear they can talk to plants.[SOFTBLOCK] Amazing!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's an aquatics show that takes place in the next room.[SOFTBLOCK] There's a list of show times, but they've obviously been cancelled.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "NoteB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Authorized scientific personnel only beyond this point.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Please clean up any liquid spills, or contact a Slave Unit to do so.[SOFTBLOCK] We've had far too many slips this month!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Samples from the Epsilon Habitat must be cryogenically sealed before transport.[SOFTBLOCK] NO EXCEPTIONS.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A fabrication bench with the tools still left out.[SOFTBLOCK] The staff here left in a hurry, as expected.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end