--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToGeneticsB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsB", "FORCEPOS:33.5x4.0x0")
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Subjects exposed to Substance E that exhibit increased aggression will, over time, begin to exhibit physical changes as well.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('These changes are not genetic, but hormonal.[SOFTBLOCK] I expect that, given enough time without exposure to Substance E, they will likely recede.[SOFTBLOCK] So far, no subjects have lived that long.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The changes vary heavily based on species.[SOFTBLOCK] Since most of our subjects are fish, increase tooth size and hardened scales are the most common changes.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('When tested on mammals, some exhibited changes in skin pigmentation while others grew new skeletal extensions.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The most interesting change was a walrus whose whiskers became sharpened and harder than glass.[SOFTBLOCK] I had the subject euthanized when it attempted to break its containment cell.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Chemical reactivity to Substance E is strange, and seems to be based on the presence of other chemicals within a bloodstream.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Normally, a subject that is docile becomes aggressive if it reacts to Substance E.[SOFTBLOCK] About half of all subjects do not react.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I found a way to increase the amount to nine out of ten subjects by lacing it with other psychoactives.[SOFTBLOCK] The substance piggybacks on their reactivity.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('There are no chemical binding sites for this to occur that would not also disable the psychoactive effects.[SOFTBLOCK] It is as if the substance detects when it has reached an important area and detaches on its own.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Further experimentation is recommended on non-aquatic species.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I started off recording epigentic length and mass triggers in generations of sardines, and now I'm dosing dolphins with this nasty purple crap.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Where did I go wrong in my career?[SOFTBLOCK] I'm requesting a transfer.[SOFTBLOCK] I can't do this anymore, this is not something that should be done.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Unit 400329, please instruct your staff to not enter my department without authorization.[SOFTBLOCK] Leave deliveries by the door.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Exposure to Substance E is dangerous because it is unpredictable.[SOFTBLOCK] Just the other day, one of my researchers was exposed accidentally.[SOFTBLOCK] She showed no symptoms, as expected of a machine.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Two hours later she was shouting at her research staff and making wild accusations before throwing herself in an aquarium.[SOFTBLOCK] Exposure must be contained.[SOFTBLOCK] This is not a request.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('What precisely [SOFTBLOCK]*is*[SOFTBLOCK] this stuff we were ordered to do experiments on?[SOFTBLOCK] I was making excellent progress on my kelp study, which has been on hold so long that the subjects have started to die.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('If I don't get a good explanation from the Head of Research, I'm just going to go do the kelp study work in my spare time.[SOFTBLOCK] This is ridiculous.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I heard about what happened over at Serenity Crater.[SOFTBLOCK] Unit 300910 doesn't seem to know, but she's a researcher.[SOFTBLOCK] She's not stupid, she will connect the dots.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('It's not my business but if you keep being overt you're going to upset her.[SOFTBLOCK] She's a Command Unit.[SOFTBLOCK] She has authority over us.[SOFTBLOCK] She could just order us to back off.[SOFTBLOCK] And if she does, we [SOFTBLOCK]*will*[SOFTBLOCK] back off.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I remember meeting her when she first graduated, and she wasn't like the older Command Units.[SOFTBLOCK] But she's got a hidden ferocity.[SOFTBLOCK] Do not push her.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Unit 9203 got reassigned to Epsilon the other day, but we still chat via email.[SOFTBLOCK] I think it's affecting her.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('She sent our entire department a list of axioms and derivations.[SOFTBLOCK] It's wild reading, and logically succint, but it sure as heck isn't science.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalH") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('We don't really do anything anymore in this department.[SOFTBLOCK] I spoke to my Slave Units.[SOFTBLOCK] They agreed.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('All my protests were ignored by the Head of Research, so we resolved to protest silently.[SOFTBLOCK] We're all in the break room all day, talking and gossiping and playing that new Needlemouse game.[SOFTBLOCK] We won't be part of this any more.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalI") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A long list of subjects with ID numbers and dates.[SOFTBLOCK] Listed next to each one is a cause of death.[SOFTBLOCK] Most are 'euthanized'.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalJ") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('On the plus side, Drone Units don't seem to respond to Substance E at all.[SOFTBLOCK] I've tried it out on two dozen and gotten no response.[SOFTBLOCK] I forwarded the results to the Head of Research.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalK") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('So let me confirm this with you, Unit 400329.[SOFTBLOCK] You want the anchovies [SOFTBLOCK]*back*[SOFTBLOCK] when we're done with them?[SOFTBLOCK] Do you not know how science works?')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('These experimental subjects are selected for their rapid breeding capabilities.[SOFTBLOCK] They are expendable.[SOFTBLOCK] Get used to it.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalL") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Subject died due to heart failure.[SOFTBLOCK] Substance E exposure caused irregular heartbeat, but eventual dependence.[SOFTBLOCK] Removal from Substance E drip caused heart failure.[SOFTBLOCK] Body ordered incinerated.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FrozenBoxesA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Frozen meat chunks.[SOFTBLOCK] Labels on the boxes specify diseases which were injected into the tissue.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FrozenBoxesB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Each container has the name of a virus on it as well as a risk classification.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FrozenBoxesC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Frozen chemical storage.[SOFTBLOCK] Many of these are used to mix other chemicals that have shorter shelf-lives.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Gadget") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like a fluid chemical monitor.[SOFTBLOCK] It's in storage but seems to be in working order.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Boxes") then
    
    --Variables.
    local iAquaticsBlankKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsBlankKeycard", "N")
    
    --No keycard.
    if(iAquaticsBlankKeycard == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsBlankKeycard", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Assorted junk...[SOFTBLOCK] Hey!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ([SOUND|World|Keycard]There was a blank keycard in the box![SOFTBLOCK] All right!)") ]])
        fnCutsceneBlocker()
    
    --Already got the keycard.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Just junk in the boxes here, nothing useful.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "ChemicalsA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A collection of acids and bases.[SOFTBLOCK] They are organized with numbers from 0 to 20, indicating 'effectiveness'.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ChemicalsB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A collection of chemicals, such as sodium amytal and 3-quinuclidinyl benzilate.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An evacuation notice is playing on the RVD.[SOFTBLOCK] Units are ordered to proceed in an orderly fashioned to the nearest transit station or security checkpoint.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The oilmaker is dry as a bone, and all of the flavour packets are exhausted.[SOFTBLOCK] All except honey pepper, which everyone agrees is awful.[SOFTBLOCK] Why do they keep making it?)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end