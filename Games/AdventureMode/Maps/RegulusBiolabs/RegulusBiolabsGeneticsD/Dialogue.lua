--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Special: No need to go further if just backing out of dialogue.

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = "None"
if(sTopicName ~= "YesChristine" and sTopicName ~= "Yes55" and sTopicName ~= "NoCancel") then
    sActorName = TA_GetProperty("Name")
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local iAquaticsMetLord = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsMetLord", "N")
    local iAquaticsBlankKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsBlankKeycard", "N")
    
    --[Wait What?]
    if(iAquaticsMetLord == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsMetLord", "N", 1.0)
        
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh no, the rebels are upon me![SOFTBLOCK] I surrender![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I am unarmed and in no position to resist you.[SOFTBLOCK] There is no need to get violent.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] Don't make any sudden moves.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We accept your surrender.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Well, I suppose you can do whatever you want with me, then.[SOFTBLOCK] No need to make it quick, either.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] We don't scrap prisoners.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] That is not what I meant.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] You could do [SOFTBLOCK]*absolutely anything*[SOFTBLOCK] to me, and I would be unable to stop you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] All three of you at once, if necessary, or two at once while the other watches.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] And we don't torture either.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] In addition to it being ineffective for any purpose but yielding forced confessions, it is morally abhorrent.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So you have nothing to fear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Not what -[SOFTBLOCK] Hmm.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Listen, I'd do absolutely anything you demanded of me.[SOFTBLOCK] *Anything*.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Like quietly be escorted to a secured room?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (These two just do not get it do they?)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Perhaps you would answer some questions voluntarily?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Questions?[SOFTBLOCK] While bent over and begging for release?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Oh no, we're not letting you go.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You two are so naive...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not follow your logic.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Look, miss.[SOFTBLOCK] Whatever you think is going to go on here, you're wrong.[SOFTBLOCK] We're all spoken for.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] All of you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All of us.[SOFTBLOCK] Though perhaps some of the other rebels aren't, I wouldn't know.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] There are more of you?[SOFTBLOCK] How many?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Enough.[SOFTBLOCK] We will not divulge sensitive information.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Perhaps a dozen or more, all seeking to take revenge on me for years of mistreatment?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] And maybe they'd record and broadcast it...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] As a warning to others...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Getting off track![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ma'am, we need to access the eastern section of the facility for our nefarious rebel purposes.[SOFTBLOCK] If you cooperate...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] That area is under code-blue security lockdown.[SOFTBLOCK] I cannot bypass it myself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] She's lying.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I said [SOFTBLOCK]*myself*[SOFTBLOCK], my dear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I would require a keycard which I can imprint with my authcode.[SOFTBLOCK] That will allow you to bypass the lockdown.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I do not, however, have a blank keycard.[SOFTBLOCK] The lockdown was implemented without distributing keycards to the staff as per procedure.[BLOCK][CLEAR]") ]])
            
            --Don't have the blank card yet.
            if(iAquaticsBlankKeycard == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So we need to find a blank keycard?[SOFTBLOCK] Shouldn't be a problem.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I understand they are kept in storage, but with everything being so chaotic as of late, I can't say where.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] You might find something in the restricted labs north of here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Stay here.[SOFTBLOCK] This place is dangerous.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Of course.[SOFTBLOCK] Oh how awful it'd be if someone were to happen upon poor, vulnerable me while trapped in this office...") ]])
            
            --Already have the keycard.
            else
                VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N", 1.0)
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A keycard like this one?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Precisely.[SOFTBLOCK] One moment...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] [SOUND|World|Keycard]And there you have it.[SOFTBLOCK] One code-blue keycard.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I didn't think this would be so easy...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I have kept my end of the...[SOFTBLOCK] shall we say, agreement.[SOFTBLOCK] Therefore, it is time for you to keep yours.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] By taking you prisoner?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh yes, and forcing me to do whatever it is you desire.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We'll contact a rebel team to pick you up.[SOFTBLOCK] This area is still dangerous.[SOFTBLOCK] Stay in your office.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] That's what I've been doing so far, and I am still functional.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Will your fellow rebels be far less charitable than you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Likely.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh my...[SOFTBLOCK] the things I will be made to do...[SOFTBLOCK] so degrading for one of my stature...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] They might make you lick their boots, or clean the floors where they walk...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Or lash me to a post, nude, and allow passersby to gawk at my exposed metallic skin...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (We had better get going before I ask to trade places with her!)") ]])
            end
        
        --SX-399 is not present.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Prisoners are not to be harmed so long as they cooperate with instructions.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] That is not what I meant.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] You could do [SOFTBLOCK]*absolutely anything*[SOFTBLOCK] to me, and I would be unable to stop you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Both of you at once, if necessary, though if one merely wanted to watch...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Torture is inefficient and morally abominable.[SOFTBLOCK] We will not tolerate it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Not what -[SOFTBLOCK] Hmm.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Listen, I'd do absolutely anything you demanded of me.[SOFTBLOCK] *Anything*.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You will be taken to a secure room and interrogated.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Oh, 55...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you answer questions voluntarily, we can provide certain amenities.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Questions?[SOFTBLOCK] While bent over and begging for release?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You will not be released for the duration of the conflict.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh my goodness, yes...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You are so naive, 55...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ..?[SOFTBLOCK] I do not follow your logic.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Look, miss.[SOFTBLOCK] Whatever you think is going to go on here, you're wrong.[SOFTBLOCK] I'm spoken for, and my friend...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Yes?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There are other rebels who may be more...[SOFTBLOCK] accomodating.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] There are more of you?[SOFTBLOCK] How many?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Enough.[SOFTBLOCK] We will not divulge sensitive information.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Perhaps a dozen or more, all seeking to take revenge on me for years of mistreatment?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] And maybe they'd record and broadcast it...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] As a warning to others...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Getting off track![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ma'am, we need to access the eastern section of the facility for our nefarious rebel purposes.[SOFTBLOCK] If you cooperate...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] That area is under code-blue security lockdown.[SOFTBLOCK] I cannot bypass it myself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You are an administrator.[SOFTBLOCK] You have access.[SOFTBLOCK] Do not lie to us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I said [SOFTBLOCK]*myself*[SOFTBLOCK], my dear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I would require a keycard which I can imprint with my authcode.[SOFTBLOCK] That will allow you to bypass the lockdown.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I do not, however, have a blank keycard.[SOFTBLOCK] The lockdown was implemented without distributing keycards to the staff as per procedure.[BLOCK][CLEAR]") ]])
            
            --Don't have the blank card yet.
            if(iAquaticsBlankKeycard == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So we need to find a blank keycard?[SOFTBLOCK] Shouldn't be a problem.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I understand they are kept in storage, but with everything being so chaotic as of late, I can't say where.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] You might find something in the restricted labs north of here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Stay here.[SOFTBLOCK] This place is dangerous.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Of course.[SOFTBLOCK] Oh how awful it'd be if someone were to happen upon poor, vulnerable me while trapped in this office...") ]])
            
            --Already have the keycard.
            else
                VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N", 1.0)
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A keycard like this one?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Precisely.[SOFTBLOCK] One moment...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] [SOUND|World|Keycard]And there you have it.[SOFTBLOCK] One code-blue keycard.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I didn't think this would be so easy...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I have kept my end of the...[SOFTBLOCK] shall we say, agreement.[SOFTBLOCK] Therefore, it is time for you to keep yours.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You wish to become a prisoner voluntarily?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Maybe 55 and I need to have 'the talk'...)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh yes, and forcing me to do whatever it is you desire.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We'll contact a rebel team to pick you up.[SOFTBLOCK] This area is still dangerous.[SOFTBLOCK] Stay in your office.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] That's what I've been doing so far, and I am still functional.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Will your fellow rebels be far less charitable than you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Likely.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh my...[SOFTBLOCK] the things I will be made to do...[SOFTBLOCK] so degrading for one of my stature...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] They might make you lick their boots, or clean the floors where they walk...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Or lash me to a post, nude, and allow passersby to gawk at my exposed metallic skin...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (We had better get going before I ask to trade places with her!)") ]])
            end
            
            
            
        end
        fnCutsceneBlocker()
    
    --Repeat, no keycard.
    elseif(iAquaticsMetLord == 1.0 and iAquaticsBlankKeycard == 0.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Do let me known when you have located a blanked keycard.[SOFTBLOCK] I will get it authorized for you post-haste.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] You may be able to locate one on the northern end of the facility, in a storage container.") ]])
    
    --Repeat, got keycard.
    else
    
        --Don't have the blank card yet.
        local iAquaticsKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N")
        if(iAquaticsKeycard == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N", 1.0)
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We've found a keycard.[SOFTBLOCK] Can you authorize it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] One moment...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] [SOUND|World|Keycard]And there you have it.[SOFTBLOCK] One code-blue keycard.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I didn't think this would be so easy...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I have kept my end of the...[SOFTBLOCK] shall we say, agreement.[SOFTBLOCK] Therefore, it is time for you to keep yours.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You wish to become a prisoner voluntarily?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Maybe 55 and I need to have 'the talk'...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh yes, and forcing me to do whatever it is you desire.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We'll contact a rebel team to pick you up.[SOFTBLOCK] This area is still dangerous.[SOFTBLOCK] Stay in your office.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] That's what I've been doing so far, and I am still functional.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Will your fellow rebels be far less charitable than you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Likely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh my...[SOFTBLOCK] the things I will be made to do...[SOFTBLOCK] so degrading for one of my stature...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] They might make you lick their boots, or clean the floors where they walk...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Or lash me to a post, nude, and allow passersby to gawk at my exposed metallic skin...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (We had better get going before I ask to trade places with her!)") ]])
            
        --Repeats.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I hope your rebel friends get here soon.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] They will provide protection in exchange for cooperation.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh, I intend to cooperate.[SOFTBLOCK] As hard as I can.") ]])
        
        
        end
    end
end