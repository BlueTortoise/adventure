--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToGeneticsB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsB", "FORCEPOS:43.5x30.0x0")
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('WHY YES I GREATLY APPRECIATE YOU COMPLAINING THAT YOUR PACKAGES ARE NOT ARRIVING ON TIME.[SOFTBLOCK] PLEASE DO NOT ACKNOWLEDGE THE SECURITY TEAM SETTING UP A TAFFING BARRICADE IN MY DEPARTMENT AND FRISKING EVERYONE WHO GOES THROUGH.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (...[SOFTBLOCK] I would read the rest but it's mostly swearing...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This console records the security screenings.[SOFTBLOCK] The units are explicitly ordered to prevent 'Substance E' from being shipped anywhere, accidentally or otherwise.[SOFTBLOCK] All packages and containers are extensively searched.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An evacuation order is flashing on this terminal.[SOFTBLOCK] Units are advised that this security station is not safe and to proceed to Transit Station Omicron.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Ha ha ha![SOFTBLOCK] They took half your department and then got mad when your productivity dropped?[SOFTBLOCK] Too bad!')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Meanwhile we ran out of spare parts for our fabricator, and when I ordered my units to make more, Unit 2856 countermanded me and ordered them to keep making those incediary grenades.[SOFTBLOCK] So now we're down a fabricator but at least we can burn down a few dozen forests if we have to.[SOFTBLOCK] What is wrong with us?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Several unsent emails calling out superior Lord Units for incompetence, backbiting, poor sportsmanship, and more.[SOFTBLOCK] All of them written since the evacuation order was sent.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I guess you have to make those final moments count.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Device") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A radioscopic receiver, with a note on it saying 'DAMN IT' on it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The oilmaker has tea leaves around it.[SOFTBLOCK] Goodness, how long has it been since I had proper Earl Grey?)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end