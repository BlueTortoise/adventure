--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsGeneticsE"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "RegulusTense")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusBiolabsGeneticsD")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iSX399JoinsParty          = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iAquaticsChristineIsAlone = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N")
    local iAquaticsActionScene      = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N")
    if(iAquaticsChristineIsAlone == 1.0) then
        
        --Normal:
        if(iAquaticsActionScene ~= 2.0) then
            TA_Create("55")
                TA_SetProperty("Position", 23, 25)
                TA_SetProperty("Facing", gci_Face_East)
                TA_SetProperty("Clipping Flag", false)
                fnSetCharacterGraphics("Root/Images/Sprites/55/", true)
            DL_PopActiveObject()
            if(iSX399JoinsParty == 1.0) then
                TA_Create("SX399")
                    TA_SetProperty("Position", 23, 26)
                    TA_SetProperty("Facing", gci_Face_East)
                    TA_SetProperty("Clipping Flag", false)
                    fnSetCharacterGraphics("Root/Images/Sprites/SX399Lord/", true)
                DL_PopActiveObject()
            end
        
        --Last rejoin:
        else
            TA_Create("55")
                TA_SetProperty("Position", 22, 24)
                TA_SetProperty("Facing", gci_Face_North)
                TA_SetProperty("Clipping Flag", false)
                fnSetCharacterGraphics("Root/Images/Sprites/55/", true)
            DL_PopActiveObject()
            if(iSX399JoinsParty == 1.0) then
                TA_Create("SX399")
                    TA_SetProperty("Position", 21, 24)
                    TA_SetProperty("Facing", gci_Face_North)
                    TA_SetProperty("Clipping Flag", false)
                    fnSetCharacterGraphics("Root/Images/Sprites/SX399Lord/", true)
                DL_PopActiveObject()
            end
        end
    end
end
