--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToGeneticsB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsB", "FORCEPOS:47.0x16.0x0")
    
elseif(sObjectName == "ToGeneticsF") then
    
    --Plot Variables
    local iSX399JoinsParty          = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iAquaticsChristineIsAlone = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N")
    
    --Form Variables.
    local bCauseFormDecision = false
    local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iHasLatexForm      = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local iAquaticsActionScene      = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N")
    
    --Subquest is finished.
    if(iAquaticsActionScene >= 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (No need to go down there again.)") ]])
        fnCutsceneBlocker()
    
    --If Christine is not alone:
    elseif(iAquaticsChristineIsAlone == 0.0) then
        
        --Remove 55 and SX-399.
        gsFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "55")
        AL_SetProperty("Unfollow Actor Name", "SX399")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
        local iSlot = AC_GetProperty("Character Party Slot", "55")
        if(iSlot > -1) then
            AC_SetProperty("Set Party", iSlot, "Null")
        end
        iSlot = AC_GetProperty("Character Party Slot", "SX-399")
        if(iSlot > -1) then
            AC_SetProperty("Set Party", iSlot, "Null")
        end
            
        --If Christine is in an acceptable underwater form:
        if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "Darkmatter") then
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I will remain here.[SOFTBLOCK] Keep me posted.") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] We will remain here.[SOFTBLOCK] Keep us posted.") ]])
            end
            fnCutsceneBlocker()
            
            --Move to the next room.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsGeneticsF", "FORCEPOS:25.0x28.0x0") ]])
        
        elseif(sChristineForm == "Eldritch") then
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I will remain here.[SOFTBLOCK] Keep me posted.[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] We will remain here.[SOFTBLOCK] Keep us posted.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (This body's eyes might not see well underwater.[SOFTBLOCK] I should change forms.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Electrosprite" or sChristineForm == "Raiju") then
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I will remain here.[SOFTBLOCK] Keep me posted.[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] We will remain here.[SOFTBLOCK] Keep us posted.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (This body...[SOFTBLOCK] Well, I really have no idea what will happen if I take a dip.[SOFTBLOCK] Better not risk it.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Human") then
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I will remain here.[SOFTBLOCK] Keep me posted.[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] We will remain here.[SOFTBLOCK] Keep us posted.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I don't know if my organic endurance will hold out long enough to swim this.[SOFTBLOCK] Better pick something more durable.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "SteamDroid") then
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I will remain here.[SOFTBLOCK] Keep me posted.[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] We will remain here.[SOFTBLOCK] Keep us posted.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (SX-399 said this body wouldn't work well underwater.[SOFTBLOCK] Better pick something a little more modern.)[BLOCK]") ]])
            bCauseFormDecision = true
        end
        
        --If a form decision is required:
        if(bCauseFormDecision == true) then
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"Golem\") ")
            if(iHasLatexForm == 1.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"Drone\") ")
            end
            if(iHasDarkmatterForm == 1.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"Darkmatter\") ")
            end
            fnCutsceneBlocker()
        end
    
    --Christine is alone:
    else
        --If Christine is in an acceptable underwater form:
        if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "Darkmatter") then
            
            --Move to the next room.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsGeneticsF", "FORCEPOS:25.0x28.0x0") ]])
        
        elseif(sChristineForm == "Eldritch") then
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (This body's eyes might not see well underwater.[SOFTBLOCK] I should change forms.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Electrosprite" or sChristineForm == "Raiju") then
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (This body...[SOFTBLOCK] Well, I really have no idea what will happen if I take a dip.[SOFTBLOCK] Better not risk it.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Human") then
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I don't know if my organic endurance will hold out long enough to swim this.[SOFTBLOCK] Better pick something more durable.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "SteamDroid") then
        
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (SX-399 said this body wouldn't work well underwater.[SOFTBLOCK] Better pick something a little more modern.)[BLOCK]") ]])
            bCauseFormDecision = true
        end
        
        --If a form decision is required:
        if(bCauseFormDecision == true) then
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"Golem\") ")
            if(iHasLatexForm == 1.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"Drone\") ")
            end
            if(iHasDarkmatterForm == 1.0) then
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"Darkmatter\") ")
            end
            fnCutsceneBlocker()
        end
    
    end

--[Transformations]
elseif(sObjectName == "Golem" or sObjectName == "Drone" or sObjectName == "Darkmatter") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

    --Wait a bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    --Flash the active character to white. Immediately after, execute the transformation.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite")
    DL_PopActiveObject()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    if(sObjectName == "Golem") then
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    elseif(sObjectName == "Drone") then
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua") ]])
    elseif(sObjectName == "Darkmatter") then
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Darkmatter.lua") ]])
    end
    fnCutsceneWait(gci_Flashwhite_Ticks_Total)
    fnCutsceneBlocker()

    --Now wait a little bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()
    
    --Transfer to the next room.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsGeneticsF", "FORCEPOS:25.0x28.0x0") ]])
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I was running late after a meeting at the manufactory, so I decided to take a shortcut and get off the tram at the cargo bay.[SOFTBLOCK] Big mistake.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The drones there gave me a frisk, searched my pack, and then searched my quarters afterwards.[SOFTBLOCK] I didn't get an answer when I asked why, but they're drones so they probably don't know either.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Whatever they're searching for, it sure is annoying.[SOFTBLOCK] Won't be taking that shortcut again.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Yeah, I wrote that in a paper actually.[SOFTBLOCK] Free will is a required component of scientific endeavour, but Slave Units?[SOFTBLOCK] We could reprogram them for total obedience and not miss a beat.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Now, the counterargument is that Slave Units having free will allows them to optimize their own workspaces and suggest improvements.[SOFTBLOCK] But let's face it, Slave Units are terribly stupid and lazy.[SOFTBLOCK] What could they optimize?[SOFTBLOCK] What have they truly ever designed?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This computer's background is a Darkmatter playing with a ball of string.[SOFTBLOCK] The image has probably been altered, but it's still cute.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A portable computer that is locked.[SOFTBLOCK] Probably not worth hacking.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Device") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Water pH and chemical composition are within expected parameters, according to this sensor.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A fabrication bench in fairly good working order.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bridge") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Just like they said, the bridge is blown up.[SOFTBLOCK] We'll have to find a way around.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end