--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Hydrophobe") then
    
    --Variables.
    local iAquaticsSawHydrophobe = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsSawHydrophobe", "N")
    if(iAquaticsSawHydrophobe == 1.0) then return end
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Topics.
    WD_SetProperty("Unlock Topic", "Biolabs_Hydrophobia", 1)
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsSawHydrophobe", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 37.25, 27.50)
    fnCutsceneFace("Christine", 1, -1)
    fnCutsceneMove("55", 36.25, 27.50)
    fnCutsceneFace("55", 1, 0)
    if(bIsSX399Present == true) then
        fnCutsceneMove("SX399", 35.25, 27.50)
        fnCutsceneFace("SX399", 1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well well well, what's this?[SOFTBLOCK] An elevator that goes down into the water?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 55 this is the part where you tell me what this is, because you already know.[BLOCK][CLEAR]") ]])
    if(bIsSX399Present == true) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Because you know everything.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Soooo...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This elevator goes into the water because research staff have underwater labs set up.[SOFTBLOCK] After all, a golem isn't afraid of getting wet.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Lack of oxygen and high pressure are nothing to a robot.[SOFTBLOCK] Right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55 I'm guessing here.[SOFTBLOCK] I don't know if that's true.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We need to find another way.[BLOCK][CLEAR]") ]])
    if(bIsSX399Present == true) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] This [SOFTBLOCK]*is*[SOFTBLOCK] the other way, unless you think you can jump that gap over there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We cannot jump that gap.[SOFTBLOCK] We must find another way.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Okay, something is up...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55, what do you think is at the bottom?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Look down there, Christine.[SOFTBLOCK] What if you fell in, and damaged yourself.[SOFTBLOCK] Imagine being trapped at the bottom of a pool of water, unable to climb out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Your power core would be able to keep running at reduced efficiency, almost indefinitely, by using the water as a power source.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] You would be partially conscious, trapped in the dark, and never escape.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Uh huh.[SOFTBLOCK] But someone would find you, or you could go into system standby and build up power charge for improved but intermittent consciousness.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] So even if something went wrong, you could escape.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] But -[SOFTBLOCK] no, there is a chance even that would not work.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's all right, 55.[SOFTBLOCK] I understand.[BLOCK][CLEAR]") ]])
    
    --SX-399 is present.
    if(bIsSX399Present == true) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I don't.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, okay,[SOFTBLOCK] one second.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 39.25, 27.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMove("SX399", 39.25, 28.50)
        fnCutsceneFace("SX399", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 35.25, 27.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Now this is just a guess, but I think 55 might be hydrophobic.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Fear of water?[SOFTBLOCK] Hmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But why?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Fears aren't rational, she just has it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But she sees herself as totally rational, which means it's going to be even harder to help her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] ...[SOFTBLOCK] Fiddlesticks.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] No matter what we do, she's not coming with us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Can you go ahead without us?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I suppose I must.[SOFTBLOCK] Keep her company?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] As if you had to ask.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 37.25, 27.50)
        fnCutsceneMove("SX399", 37.25, 28.50)
        fnCutsceneBlocker()
        fnCutsceneFace("55", 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Right, strategic decision.[SOFTBLOCK] I will proceed ahead, alone, while you two cover the rear.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] But - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I will remain in contact via the PDU.[SOFTBLOCK] If I get in trouble, you can send SX-399 to support.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Remember, Christine can transform.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I'm sure her golem body can withstand the pressure just fine.[SOFTBLOCK] She'll be all right.[BLOCK][CLEAR]") ]])
        if(sChristineForm ~= "Golem") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Don't forget about my Steam Droid form![BLOCK][CLEAR]") ]])
        elseif(sChristineForm == "SteamDroid") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right, Steam Droid Christine, going deep![BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What if I transformed into a Steam Droid?[SOFTBLOCK] Would that work better?[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Ah -[SOFTBLOCK] no.[SOFTBLOCK] Don't do that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I'm considerably better insulated than your average Steam Droid, but you won't be.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] The water will suck the heat out of your frame and greatly reduce your energy.[SOFTBLOCK] Steam Droids were not made for underwater work.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, scratch that, then.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Meanwhile, I'll make sure Cutie-5 -[SOFTBLOCK][SOFTBLOCK] keeps you covered.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are absolutely sure of this, 771852?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes.[SOFTBLOCK] I'm prepared to risk my life.[SOFTBLOCK] This is war.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] That is -[SOFTBLOCK] good.[SOFTBLOCK] We will remain here.[SOFTBLOCK] Make sure to keep us updated.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Count on it.[SOFTBLOCK] I'll be back before you know it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] But that means the glory will be all mine![SOFTBLOCK] Ha ha!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("55", 23.25, 27.50)
        fnCutsceneMove("55", 23.25, 25.50)
        fnCutsceneFace("55", 1, 0)
        fnCutsceneMove("SX399", 23.25, 28.50)
        fnCutsceneMove("SX399", 23.25, 26.50)
        fnCutsceneFace("SX399", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Remove everyone else from the party.
        gsFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "55")
        AL_SetProperty("Unfollow Actor Name", "SX399")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
		local iSlot = AC_GetProperty("Character Party Slot", "55")
		if(iSlot > -1) then
			AC_SetProperty("Set Party", iSlot, "Null")
		end
		iSlot = AC_GetProperty("Character Party Slot", "SX-399")
		if(iSlot > -1) then
			AC_SetProperty("Set Party", iSlot, "Null")
		end
    
    --Just 55.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (55 must be scared, but this really isn't the time or the place for a therapy session...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55, I think it's best if we split up.[SOFTBLOCK] I'll proceed alone, you cover the rear and make sure my exit is clear.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And if something goes wrong, and you require rescue?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll remain in contact via my PDU.[SOFTBLOCK] You'll be able to track me if you need to.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And besides -[SOFTBLOCK] I'm the tough one, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are absolutely sure of this, 771852?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes.[SOFTBLOCK] I'm prepared to risk my life.[SOFTBLOCK] This is war.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] That is -[SOFTBLOCK] good.[SOFTBLOCK] I will remain here.[SOFTBLOCK] Keep me updated.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Count on it.[SOFTBLOCK] I'll be back before you know it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] But that means the glory will be all mine![SOFTBLOCK] Ha ha!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("55", 23.25, 27.50)
        fnCutsceneMove("55", 23.25, 25.50)
        fnCutsceneFace("55", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Remove everyone else from the party.
        gsFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "55")
        AL_SetProperty("Unfollow Actor Name", "SX399")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
		local iSlot = AC_GetProperty("Character Party Slot", "55")
		if(iSlot > -1) then
			AC_SetProperty("Set Party", iSlot, "Null")
		end
		iSlot = AC_GetProperty("Character Party Slot", "SX-399")
		if(iSlot > -1) then
			AC_SetProperty("Set Party", iSlot, "Null")
		end

    end
    fnCutsceneBlocker()
    
--55 and SX-399 rejoin the party.
elseif(sObjectName == "Rejoin") then

    --Variables.
    local iSX399JoinsParty          = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iAquaticsSawHydrophobe    = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsSawHydrophobe", "N")
    local iAquaticsChristineIsAlone = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N")
    local iAquaticsActionScene      = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N")
    if(iAquaticsChristineIsAlone == 0.0 or iAquaticsSawHydrophobe == 0.0) then return end
    
    --[Normal]
    if(iAquaticsActionScene ~= 2.0) then

        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N", 0.0)

        --Characters rejoin the party.
        fnCutsceneMove("55", 23.25, 27.50)
        fnCutsceneFace("55", 1, 0)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 23.25, 28.50)
            fnCutsceneFace("SX399", 1, 0)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        if(iSX399JoinsParty == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Anything to report?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Not yet. Let's join up for a moment.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Affirmative.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Party merging.
        fnCutsceneMove("Christine", 23.25, 27.50)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX399", 23.25, 27.50)
        end
        fnCutsceneBlocker()
        
        --Rejoin 55.
        EM_PushEntity("55")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        gsFollowersTotal = 1
        gsaFollowerNames = {"55"}
        giaFollowerIDs = {i55ID}
        AL_SetProperty("Follow Actor ID", i55ID)
        AC_SetProperty("Set Party", 1, "55")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        
        --SX-399 also rejoins.
        if(iSX399JoinsParty == 1.0) then
        
            EM_PushEntity("SX399")
                local iSX399ID = RE_GetID()
            DL_PopActiveObject()
            gsFollowersTotal = 2
            gsaFollowerNames = {"55", "SX399"}
            giaFollowerIDs = {i55ID, iSX399ID}
            AL_SetProperty("Follow Actor ID", iSX399ID)
            AC_SetProperty("Set Party", 2, "SX-399")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        end
        
        --Fold.
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    --[Rejoin Scene]
    else

        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N", 3.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsCompleted", "N", 1.0)
        
        --Movement.
        fnCutsceneMove("Christine", 22.25, 27.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --If SX-399 is not in the party, a much shorter scene plays.
        if(iSX399JoinsParty == 0.0) then
            
            --Movement.
            fnCutsceneFace("55", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneMove("55", 22.25, 26.50)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your smile suggests a smug self-satisfaction that often accompanies a successful mission.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Wow, 55.[SOFTBLOCK] People who are happy often have good news to report.[SOFTBLOCK] What sort of insight will you present next?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Extra sarcasm in a response comment?[SOFTBLOCK] No casualties, then?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Other than yourself, evidently.[BLOCK][CLEAR]") ]])
            if(sChristineForm == "Darkmatter") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What's a little starlight here and there?[SOFTBLOCK] You said yourself that Darkmatters are hard to influence with conventional weapons.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Doesn't stop it from hurting a ton, though...)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you say you are well, I am in no position to override your assessment.[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This dent in my cranial chassis?[SOFTBLOCK] Oh it'll buff right out.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] Don't tell Sophie or she'll worry...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] At your insistence.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If that is all, our next assignment should be letting the captain know.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, in person.[SOFTBLOCK] I don't want to risk anyone overhearing us on the radio.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] Secrecy is important.[SOFTBLOCK] Let's move, then.") ]])
            fnCutsceneBlocker()
        
            --Rejoin 55.
            EM_PushEntity("55")
                local i55ID = RE_GetID()
            DL_PopActiveObject()
            gsFollowersTotal = 1
            gsaFollowerNames = {"55"}
            giaFollowerIDs = {i55ID}
            AL_SetProperty("Follow Actor ID", i55ID)
            AC_SetProperty("Set Party", 1, "55")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
            fnCutsceneMove("Christine", 22.25, 26.50)
            fnCutsceneBlocker()
        
            --Fold.
            fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
        --A slightly longer scene plays out if SX-399 is present.
        else
        
            --Camera movement.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 5.0)
                CameraEvent_SetProperty("Focus Actor Name", "55")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[SOFTBLOCK] But that is the concern I have.[SOFTBLOCK] I have done research on similar situations.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You looked this sort of thing up?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I have no experience with relationships.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] According to the camera footage I reviewed, even my previous self never had a tandem unit.[SOFTBLOCK] I - [BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Have I ever?[SOFTBLOCK] Before I became this?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] But you do now, isn't that enough?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No, it is not.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Units who find love during dangerous or stressful situations tend to form unstable connections.[SOFTBLOCK] My research suggests this.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So you think it won't work out because of the circumstances.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] But as long as it's not all relationships, we can make it work.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is also not my concern.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] My concern is that, while this may be genuine, I do not want it to be temporary.[SOFTBLOCK] I do not want to hurt you emotionally if I am inadequate.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] You aren't helping your case one bit, no ma'am![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please explain.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] It's one thing to be attracted to one another.[SOFTBLOCK] I grant you that, and it certainly helps...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] But attraction is about wanting and understanding.[SOFTBLOCK] And the effort you put into understanding me means that your mistakes are just that, mistakes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As opposed to?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I don't know.[SOFTBLOCK] I can't say an error is deliberate, but a refusal to learn from an error is something like it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You won't abuse me.[SOFTBLOCK] You won't manipulate me.[SOFTBLOCK] You won't betray me.[SOFTBLOCK] Because if you do, I know you'll learn from it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Sorry, those seem like pretty strong words.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I'm just saying I trust you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] What?[SOFTBLOCK] What is it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Christine has been listening to us...") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("55", 0, 1)
            fnCutsceneFace("SX399", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneMove("55", 22.25, 25.50, 0.50)
            fnCutsceneMove("SX399", 21.25, 25.50, 0.50)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Before you lay into me, I just want to say that I didn't want to interrupt you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Well you were still eavesdropping.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] F-[SOFTBLOCK]forgive me![SOFTBLOCK] I'm sorry![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, mission status report.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Mission accomplished![SOFTBLOCK] Three units were rescued and will be making their way here underwater.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We should let captain Jo know so she can have someone pick them up.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Great work.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I guess you get to live another day.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I said I'm sorry![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] I'm just toying with you, pal.[SOFTBLOCK] It's all cool.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Though eavesdropping is not appreciated.[SOFTBLOCK] Please exercise discretion in the future.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[SOFTBLOCK] Shall we be off?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Lead the way.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("55", 22.25, 27.50)
            fnCutsceneMove("SX399", 22.25, 27.50)
            fnCutsceneBlocker()
        
            --Rejoin 55.
            EM_PushEntity("55")
                local i55ID = RE_GetID()
            DL_PopActiveObject()
            gsFollowersTotal = 1
            gsaFollowerNames = {"55"}
            giaFollowerIDs = {i55ID}
            AL_SetProperty("Follow Actor ID", i55ID)
            AC_SetProperty("Set Party", 1, "55")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        
            --Rejoin SX-399.
            EM_PushEntity("SX399")
                local iSX399ID = RE_GetID()
            DL_PopActiveObject()
            gsFollowersTotal = 2
            gsaFollowerNames = {"55", "SX399"}
            giaFollowerIDs = {i55ID, iSX399ID}
            AL_SetProperty("Follow Actor ID", iSX399ID)
            AC_SetProperty("Set Party", 2, "SX-399")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        
            --Fold.
            fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
        end
        
        
    
    
    end
    
end
