--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToGeneticsH") then
    local iAquaticsActionScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N")
    if(iAquaticsActionScene == 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I probably don't need to go back to that gun nest...)") ]])
        fnCutsceneBlocker()
        
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsGeneticsH", "FORCEPOS:9.5x18.0x0")
    end
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The Diving Bell Spider creates a large 'diving bell' out of web.[SOFTBLOCK] This contains the air the spider uses to survive, and gradually gas-exchanges oxygen in and carbon-dioxide out.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The Partirhuman version of the same spider performs many of the same life processes, though notably with human intelligence.[SOFTBLOCK] They often use sealed objects, such as clay vessels, to truck more air to their diving bell as it slowly deflates due to nitrogen diffusion.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Our research into the Diving Bell Spidergirl indicates they are often considerably more social than their diminuitive counterpart.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('These spidergirls visit the surface far more often, often have beach enclaves where they relax socially and exchange information.[SOFTBLOCK] These rituals occur once or twice per month.[SOFTBLOCK] We have provided the necessary facilities, and, with the permission of the Head of Research, even allow the public to mingle with them.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('After some work, it is this unit's belief that the 'diving bell' the spidergirl creates is effectively an inorganic form of gills.[SOFTBLOCK] This allows the spidergirl to function equally well underwater and on land, though an obvious aquatic preference emerges from their morphology.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Creation of underwater cities using this gas-diffusion technique would vastly reduce the costs of maintaining seals.[SOFTBLOCK] Industrial areas would still require proper sealing, but residences could be isolated to simplify repairs and reduce chain-collapse problems.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('There are several other partirhuman species that are primarily aquatic, but these so far have proved far less accomodating to our research methods.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Hopefully, some of the elusive mermaids or nixies will agree to be transported here for study.[SOFTBLOCK] Capture is out of the question, as our abductions teams are simply not equipped for aquatic operations.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('With the help of the spidergirl research subjects, we have constructed some simple fabrication technologies that allow underwater welding through so-called 'gravity' magic that reduces pressure interactions.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Wet-welding is still of lower quality than dry-welding, but enables on-site repairs and construction.[SOFTBLOCK] It is expected that many other underwater partirhumans will have some mastery of this 'gravity' magic.[SOFTBLOCK] Finding a non-arcane technological solution should be an objective for less magically-inclined repair units.')") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end