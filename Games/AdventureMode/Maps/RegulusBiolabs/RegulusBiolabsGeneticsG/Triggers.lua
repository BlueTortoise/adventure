--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "PostQuest") then
    
    --Value must be exactly 1.0 to run this scene.
    local iAquaticsActionScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N")
    if(iAquaticsActionScene ~= 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N", 2.0)
    
    --Black the screen out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn characters.
    fnSpawnNPCPattern("Golem", "A", "C")
    
    --Give special frames to these NPCs.
    EM_PushEntity("GolemA")
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlaveR|Wounded")
        TA_SetProperty("Add Special Frame", "Sitting", "Root/Images/Sprites/Special/GolemSlaveR|Sitting")
    DL_PopActiveObject()
    EM_PushEntity("GolemB")
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlaveR|Wounded")
        TA_SetProperty("Add Special Frame", "Sitting", "Root/Images/Sprites/Special/GolemSlaveR|Sitting")
    DL_PopActiveObject()
    EM_PushEntity("GolemC")
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlaveR|Wounded")
        TA_SetProperty("Add Special Frame", "Sitting", "Root/Images/Sprites/Special/GolemSlaveR|Sitting")
    DL_PopActiveObject()
    
    --Disable this layer so the layering looks correct as Christine climbs.
    AL_SetProperty("Set Layer Disabled", "RailingFront", true)
    
    --Remove collisions so Christine can move.
    AL_SetProperty("Set Collision", 58, 25, 0, 0)
    AL_SetProperty("Set Collision", 58, 26, 0, 0)
    AL_SetProperty("Set Collision", 58, 27, 0, 0)
    AL_SetProperty("Set Collision", 58, 28, 0, 0)
    AL_SetProperty("Set Collision", 58, 29, 0, 0)
    
    --Reposition Christine and other characters.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 10.0)
        CameraEvent_SetProperty("Focus Position", (50.25 * gciSizePerTile), (23.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneTeleport("Christine", 58.25, 28.50)
    fnCutsceneTeleport("GolemC", 58.25, 29.00)
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneFace("GolemB", 1, 0)
    fnCutsceneSetFrame("GolemC", "Wounded")
    fnCutsceneBlocker()
    
    --Fade back in.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 58.25, 26.50, 0.25)
    fnCutsceneMove("GolemC", 58.25, 27.00, 0.25)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemA", 1, 0)
    fnCutsceneMove("Christine", 58.25, 24.50, 0.25)
    fnCutsceneMove("GolemC", 58.25, 24.50, 0.25)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "RailingFront", false) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Christine", 57.25, 24.50, 1, 0, 0.25)
    fnCutsceneBlocker()
    fnCutsceneMove("GolemA", 58.25, 23.50)
    fnCutsceneFace("GolemA", 0, 1)
    fnCutsceneMove("GolemB", 57.25, 23.50)
    fnCutsceneFace("GolemB", 0, 1)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneFace("Christine", 1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|RebelGolem] You took a hit![SOFTBLOCK] Are you - [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I'll be fine.[SOFTBLOCK] I seem to recall telling you not to move your limbs and to enter standby.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|RebelGolem] The bouyancy helps me walk, if I do it slowly.[SOFTBLOCK] But what about Tam?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I'll boot her up and see what her diagnostics say.") ]])
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("GolemC", "Sitting")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] System reinitializing...[SOFTBLOCK] Error, core output at 25pct...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's okay, you can survive at 25.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Any other damage I need to know about?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] You...[SOFTBLOCK] saved me?[SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You lost pretty much the entirety of your coolant store and were about to melt down.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So I opened the intake port and lowered your clock, and then tossed you into the water.[SOFTBLOCK] So now water is your coolant.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You'll need to keep your core clocked down because you're going to basically boil the water on each cycle, but you should be able to walk.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] As a repair unit, I can only imagine what sorts of havoc unfiltered water is going to play on your interiors...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] I'm not going to die?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Not today.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebel:[E|Neutral] Thank you, oh thank you...[SOFTBLOCK] But...[SOFTBLOCK] I'm really tired...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Take it easy, now.[SOFTBLOCK] Feeling sluggish is a result of your core speed being lowered.[SOFTBLOCK] Don't overdo it.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Christine", 1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Can I count on you two to get back to safety on your own?[SOFTBLOCK] Just follow this walkway west and take the elevator up.[SOFTBLOCK] I'll make sure someone is waiting for you.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneFace("GolemB", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("GolemA", 0, 1)
    fnCutsceneFace("GolemB", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Rebels:[VOICE|RebelGolem] *THANK YOU FOR SAVING US![SOFTBLOCK] THANK YOU SO MUCH!*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Heh, it was nothing.[SOFTBLOCK] Maybe you can return the favour some day.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I'll see you back at the field headquarters.[SOFTBLOCK] Don't overstress yourselves, and -[SOFTBLOCK] don't tell your repair unit half of what I did or I'll get a scolding!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Move Christine over a bit.
    fnCutsceneMove("Christine", 51.25, 24.50)
    fnCutsceneBlocker()
    
    --World repairs. This script cleans up for the cutscene.
    local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SceneCleanup.lua\")"
    fnCutsceneInstruction(sString)
    
end
