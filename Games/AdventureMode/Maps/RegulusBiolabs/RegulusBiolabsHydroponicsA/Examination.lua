--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "ToAlphaC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAlphaC", "FORCEPOS:5.5x24.0x0")
elseif(sObjectName == "ToHydroponicsB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsB", "FORCEPOS:21.5x26.0x0")
    
elseif(sObjectName == "ConsoleA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A list of personnel in the hydroponics division.[SOFTBLOCK] All of the names are listed as 'evacuated'.[SOFTBLOCK] That's a relief!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ConsoleB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A list of biological specimens, arrival dates, special instructions, and the like.[SOFTBLOCK] Not much of a discernable pattern.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like an experiment record, by an anonymous author.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hypothesis::[SOFTBLOCK] Drone Units will receive an increase in motivator efficiency if specific colours are applied to their chassis.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Experiment::[SOFTBLOCK] Apply red dye to a Drone Unit, and order it to race the other Drone Units assigned to Hydroponics Research.[SOFTBLOCK] Do this when the Lord Units are otherwise engaged.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Results::[SOFTBLOCK] Unit dyed red came fifth out of six racers.[SOFTBLOCK] Curiously, all six Drone Units were convinced that the red racer had won the race, despite the obvious evidence otherwise.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Repeated trials yielded the same result.[SOFTBLOCK] The unit was not faster before or after being dyed.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Secondary Experiment Proposal::[SOFTBLOCK] Which chemical cleaner can most efficiently remove dye from a unit's chassis?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like a log left by a researcher.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Now, I've looked at the personnel records.[SOFTBLOCK] Amazingly, nutrient requirements for the Raijus throughout Regulus City have tripled over the past three months.[SOFTBLOCK] Yet, Raiju Unit populations have increased by about ten percent.[SOFTBLOCK] Why the discrepency?')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I'm not trying to be subversive here, though I was verbally reprimanded by Command Unit 2856 when I asked if increased power production accounted for the difference.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('It's quite strange.[SOFTBLOCK] My hypothesis was that an increase in nutrient density might lead to higher voltages.[SOFTBLOCK] I was shouted at for even proposing it, and told not to waste time with the records.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I'm also not stupid.[SOFTBLOCK] I know when I'm being told not to stick my olfactory sensors where they don't belong.[SOFTBLOCK] I dropped the issue.[SOFTBLOCK] I haven't heard anything since, which is the best possible news.')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ConsoleE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (ATTENTION ALL UNITS.[SOFTBLOCK] EVACUATE HYDROPONICS LABS IMMEDIATELY.[SOFTBLOCK] PROCEED TO TRANSIT STATION OMICRON.[SOFTBLOCK] LEAVE PERSONAL POSSESSIONS BEHIND.[SOFTBLOCK] THANK YOU.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I heard something big is going down at the new Epsilon Labs.[SOFTBLOCK] I want to ask my Lord Golem about it, maybe get some volunteer work done, but she was...[SOFTBLOCK] unusually candid.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('She looked straight at me and said not to ask about the Epsilon Labs.[SOFTBLOCK] She's never that serious, ever.[SOFTBLOCK] She told me to ignore all the rumours and just keep my head down.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I know my Lord Golem cares about me very deeply.[SOFTBLOCK] It is good that she shows it by maintaining discipline over me.[SOFTBLOCK] But this was so different...')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ConsoleG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Marcie got busted for her stupid drone experiment![SOFTBLOCK] What a socket-head!')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('She's lucky she got away with just a reprimand.[SOFTBLOCK] Honestly, why would putting red dye in the showers for the Drone Units do anything?[SOFTBLOCK] They're probably too dumb to even know what red is!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleH") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Work orders for the unit assigned to this room.[SOFTBLOCK] Cleaning, watering plants, nothing interesting.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleI") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE UNIT LOG.[SOFTBLOCK] I AM WRITING A LOG.[SOFTBLOCK] MY LORD GOLEM TOLD ME I WROTE A GOOD LOG.[SOFTBLOCK] YAY!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ConsoleJ") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Finally got clearance from my Lord Unit to do my gravitation experiment, and then we get an evacuation order?[SOFTBLOCK] Why does fate hate me?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleK") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A research paper attempting to tie the age of a dragon to their hoard size.[SOFTBLOCK] Interesting stuff, but statistically insignificant since not enough dragons can be located.[SOFTBLOCK] The researcher's requests for abductions to locate more dragons were all denied...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleL") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The slave unit used her defragmentation terminal to record experimental results...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Experiment EFE18::[SOFTBLOCK] Results indicate that playing music to the plants has not influenced growth patterns within statistical significance.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Experiment EFE19::[SOFTBLOCK] Recently asked to reconsider previous positions on playing music to plants.[SOFTBLOCK] Reorganized apparatus to use fewer automated inputs, 60pct of labour performed by slave units.[SOFTBLOCK] Growth rates now correlate with musical input.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Experiment EFE20::[SOFTBLOCK] Reorganized apparatus again.[SOFTBLOCK] Genre of music split between habitat subsets.[SOFTBLOCK] Classical and Synthwave produced notable growth increases.[SOFTBLOCK] Death Metal inhibits growth rates.[SOFTBLOCK] Investigate.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Experiment EFE21::[SOFTBLOCK] Reorganized staffing.[SOFTBLOCK] Determined Death Metal enthusiast slave unit was simply ill-equipped for caring for plants.[SOFTBLOCK] Reassigned unit to fabrication, growth rates levelled off.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleM") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I managed to get my Lord Golem to approve the usage of Drone Units for trucking water through the hydroponics area in buckets, since the hoses are leaking.[SOFTBLOCK] Took long enough![SOFTBLOCK] Guess she was busy with something else.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Now I have no shortage of excuses to rub her and clean her off, every few minutes.[SOFTBLOCK] I just love how she feels...[SOFTBLOCK] So firm...[SOFTBLOCK] So taut...')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleO") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A log led by the Lord Golem assigned to hydroponics research...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Our limited experimental evidence indicates that hydroponic cultivation, by itself, could feed the entire population of Pandemonium three times over, if we were to install a fusion reactor under the ice caps and convert the energy into light for photosynthesis.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I suspect the harsh arctic environment will deter any sort of military assault by a hostile power, and the ice will provide hydrogen for fusion power.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I recommended to the Head of Abductions this course of action, and she agreed.[SOFTBLOCK] She believes providing food to the mass of lower-class humans will win them to the Cause of Science quite handily.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The challenge will be in maintaining soil fertility, as we are effectively exporting it around the planet.[SOFTBLOCK] I have requested resources to experiment with conversion of regolith into soil at an accelerated rate, but have yet to hear back from the Head of Research.[SOFTBLOCK] The Cause of Science will benefit greatly from my research, I am certain of it!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleP") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Some emails on the Head of Hydroponics' terminal...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I'm very sorry to have to requisition your crop, but we need it for the latest cut.[SOFTBLOCK] We finally got it growing in the Epsilon Habitat![SOFTBLOCK] It's incredible!')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('You can come take a look whenever you're not busy, though I'm pretty sure you'll need to get clearance from the Head of Research.[SOFTBLOCK] She can be so stuffy sometimes.[SOFTBLOCK] I swear she's gotten angrier and angrier over the past few months.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Just be careful where you step, the stuff tends to self-organize around you.[SOFTBLOCK] I'm not sure if it has any practical applications, but who cares?[SOFTBLOCK] This stuff is just incredible!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "OilmakerA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Some cheeky unit has written 'Crap' on all of the settings...[SOFTBLOCK] It doesn't smell that bad, does it?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "OilmakerB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The Lord Golem here was putting organic plants in her oil for flavor?[SOFTBLOCK] Not a proper use of scientific resources, but it tastes like Earl Grey tea.[SOFTBLOCK] Can't fault her there!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Whoever keeps vandalizing the oil maker::[SOFTBLOCK] We get the joke.[SOFTBLOCK] Stop it.[SOFTBLOCK] Our budget is already stretched as it is, we can't afford more flavours.[SOFTBLOCK] You aren't helping.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('ALL PERSONNEL MUST CLEAN ORGANIC CONTAMINANTS BEFORE RETURNING TO THEIR QUARTERS.[SOFTBLOCK] PLEASE DO NOT CROSS-CONTAMINATE SAMPLES IN HYDROPONICS WITH POLLEN FROM THE ALPHA LABS.[SOFTBLOCK] THANK YOU.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Window") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Ah, the surface of Regulus.[SOFTBLOCK] I can see the Datacore building in the distance...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's an evacuation order playing on the RVD.[SOFTBLOCK] Fortunately, it seems everyone got out okay.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Doll") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A Mini Ms. Seyton doll.[SOFTBLOCK] This Lord Golem has a refined taste!)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end