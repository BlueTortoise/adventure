--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "ToHydroponicsA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsA", "FORCEPOS:4.5x27.0x0")
    
elseif(sObjectName == "ToHydroponicsCLft") then
    gi_Force_Facing = gci_Face_North
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsC", "FORCEPOS:4.5x8.0x0")
    
elseif(sObjectName == "ToHydroponicsCRgt") then
    gi_Force_Facing = gci_Face_North
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsC", "FORCEPOS:38.5x8.0x0")
    
elseif(sObjectName == "ConsoleA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain 7-C.[SOFTBLOCK] High nutrient density, slow growth rate.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain 14-RR-2.[SOFTBLOCK] Generates excessive fruit relative to seed density.[SOFTBLOCK] Good crop candidate.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain SCCS-4.[SOFTBLOCK] Abnormally popular with avians in the Alpha labs, particularly crows.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain SCCS-5.[SOFTBLOCK] Kernals pop when heated.[SOFTBLOCK] Apply salt and butter, they're delicious!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain NORMA-R12.[SOFTBLOCK] We got the first 11 tries wrong, we'll get it this time.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain 44-ISAARI.[SOFTBLOCK] Photosynthesis rates 20pct higher than species average.[SOFTBLOCK] Further investigation recommended.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain MALVOL-104-L.[SOFTBLOCK] Does not react to fertilizers, but fares well in poor soil quality.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleH") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain JMM-19.[SOFTBLOCK] No special properties.[SOFTBLOCK] Recommend dumping seed stock.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleI") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain FFF-99.[SOFTBLOCK] Dubbed 'Naty' by the Slave Units, shows excellent resistance to exposure to cosmic radiation.[SOFTBLOCK] Recommended candidate for orbital hydroponics.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleJ") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain VORO-MNIS.[SOFTBLOCK] Tends to concentrate alkaline elements in its root system.[SOFTBLOCK] Investigate usages for soil repleneshment.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleK") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain 89-MIR-05.[SOFTBLOCK] Functions well at low moisture.[SOFTBLOCK] Recommend research into strains 99-MIR-05 and 117-MIR-05.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleL") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Genetic Strain NaM-AP0.[SOFTBLOCK] Annelids thrive in soils with this strain.[SOFTBLOCK] Useful as a first-order reclaimer.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ScreenLft") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('LAST SANITATION CYCLE -[SOFTBLOCK] SIXTEEN HOURS.[SOFTBLOCK] NEW CYCLE -[SOFTBLOCK] NEGATIVE FOUR HOURS.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ScreenRgt") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('ALL UNITS EVACUATE HYDROPONICS.[SOFTBLOCK] PROCEED TO TRANSIT STATION OMICRON.[SOFTBLOCK] LEAVE PERSONAL POSSESSIONS.[SOFTBLOCK] THANK YOU.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Racks") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hundreds of plant species, tagged with barcodes.[SOFTBLOCK] The tags also show their genetic strain and special instructions.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A terrarium, this one contains several small arthropod species.[SOFTBLOCK] There are sensors set up to collect data on them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumABig") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A terrarium, with amphibians.[SOFTBLOCK] Cameras and air filters monitor them and send data to the datacore north of here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A terrarium, with little fields tilled and segregated.[SOFTBLOCK] A list of worm species is written on the side of the glass.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumBBig") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A terrarium, with various fertilizer formulas written on the glass.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A terrarium, with snakes slithering around in it.[SOFTBLOCK] Looks like an experiment is being conducted on their breeding habits in various soils.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumCBig") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A terrarium, with a lot of webs on the leaves.[SOFTBLOCK] There are notes on spider web construction written on the glass.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A terrarium, with no animal species.[SOFTBLOCK] The experiment is documenting how pollination occurs with no intermediaries.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumDBig") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A list of personnel in the hydroponics division is written on the side of the glass, indicating who is responsible for which terrarium.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end