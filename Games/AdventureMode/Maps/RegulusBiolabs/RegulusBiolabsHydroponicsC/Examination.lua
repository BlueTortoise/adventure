--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToHydroponicsBLft") then
    gi_Force_Facing = gci_Face_North
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsB", "FORCEPOS:4.5x7.0x0")
    
elseif(sObjectName == "ToHydroponicsBRgt") then
    gi_Force_Facing = gci_Face_North
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsB", "FORCEPOS:38.5x7.0x0")
    
elseif(sObjectName == "ToHydroponicsD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsD", "FORCEPOS:9.0x27.0x0")
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Why yes, Unit 591302, those [SOFTBLOCK]*are*[SOFTBLOCK] the bones of animals from the beta labs![SOFTBLOCK] However did you come to that conclusion?')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Grind them into powder for fertilizer and stop complaining or I'll put an official reprimand on your record!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('I just got done fixing the light-amp rig and now you want me to go to Omicron Transit?[SOFTBLOCK] We're not even going to test the lights?[SOFTBLOCK] Fine.[SOFTBLOCK] Don't complain if they blow out next time you use them.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The Lord Golem of Hydroponics wrote in a note file that she likes standing on the catwalks, watching the Slave Units work...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The room is a mess because cleaning it is always at the bottom of my priority list.[SOFTBLOCK] Every time you add something new, you want it done first.[SOFTBLOCK] This is what happens![SOFTBLOCK] I recommend you adjust the algorithm so that a task's priority is summed with how many days since it was issued.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Or you can just yell louder.[SOFTBLOCK] That will fix it!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Cargo deliveries to datacore server room::[SOFTBLOCK] Right.[SOFTBLOCK] Datacore Offices::[SOFTBLOCK] Left.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Attention Idiots::[SOFTBLOCK] Blue is water, yellow is alcohol.[SOFTBLOCK] Do NOT water the plants with alcohol again![SOFTBLOCK] With the exception of the guava, no plant species can properly metabolize alcohol.')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Water") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Barrels of water, presumably for hand-watering of delicate samples that sprinklers can't be used for.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Booze") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Barrels of alcohol.[SOFTBLOCK] Smells like whiskey.[SOFTBLOCK] I wonder where they brew it?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Junk") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Tools and broken junk that nobody has gotten around to recycling.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Furniture") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like spare furniture for picnics.[SOFTBLOCK] I guess they have nowhere better to store it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bones") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Assorted animal bones.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A Model-D fabricator bench.[SOFTBLOCK] The tools are left out, so whoever was using it last left in a hurry.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end