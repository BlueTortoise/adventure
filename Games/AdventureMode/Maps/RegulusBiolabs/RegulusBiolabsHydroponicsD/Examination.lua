--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "ToHydroponicsC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsC", "FORCEPOS:22.0x4.0x0")
    
elseif(sObjectName == "ToDatacoreF") then
    
    --Variables.
    local iBiolabsFoundRedKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N")
    if(iBiolabsFoundRedKeycard == 0.0) then
        AudioManager_PlaySound("World|AutoDoorFail")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Locked.[SOFTBLOCK] Any idea where we can find a red keycard, 55?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We may want to check the lower access tunnels under the datacore.[SOFTBLOCK] Some of the offices have unsecured surface access points we can use.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] These facilities are quite old and have only been partially retrofitted.[SOFTBLOCK] Security is poor.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Suitable for our purposes.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] There's an airlock over there.[SOFTBLOCK] Let's take a stroll on the surface.[SOFTBLOCK] Keep your ocular receivers open.") ]])
        fnCutsceneBlocker()
    
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsDatacoreF", "FORCEPOS:8.0x14.0x0")
    end
    
elseif(sObjectName == "ToDatacoreA") then

    --Organic checker:
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Human" or sChristineForm == "Raiju") then
        
        --Variables:
        local iHasLatexForm         = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
        local iHasDarkmatterForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
        local iHasEldritchForm      = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
        local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
        local iHasSteamDroidForm    = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
        local iSawRaijuIntro        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[SOFTBLOCK] Secure organic subjects for transport before exiting.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Better change to an inorganic form before I go outside...)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        if(iHasDarkmatterForm == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"TFToDarkmatter\") ")
        end
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"TFToGolem\") ")
        if(iHasLatexForm == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"TFToLatex\") ")
        end
        if(iHasEldritchForm == 1.0 and iSawRaijuIntro == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Dreamer\",  " .. sDecisionScript .. ", \"TFToEldritch\") ")
        end
        if(iHasElectrospriteForm == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Electrosprite\",  " .. sDecisionScript .. ", \"TFToElectrosprite\") ")
        end
        if(iHasSteamDroidForm == 1.0) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Steam Droid\",  " .. sDecisionScript .. ", \"TFToSteam\") ")
        end
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"NoCancel\") ")
        fnCutsceneBlocker()
    
    --Normal case:
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsDatacoreA", "FORCEPOS:16.0x23.0x1")
    end
    
--[Form Changers]
elseif(sObjectName == "TFToGolem") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToGolem/Scene_Begin.lua")
    
    --Post-exec.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreA", "FORCEPOS:16.0x23.0x1") ]])
    
elseif(sObjectName == "TFToLatex") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToLatexDrone/Scene_Begin.lua")
    
    --Post-exec.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreA", "FORCEPOS:16.0x23.0x1") ]])
    
elseif(sObjectName == "TFToDarkmatter") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToDarkmatter/Scene_Begin.lua")
    
    --Post-exec.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreA", "FORCEPOS:16.0x23.0x1") ]])
    
elseif(sObjectName == "TFToEldritch") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToEldritch/Scene_Begin.lua")
    
    --Post-exec.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreA", "FORCEPOS:16.0x23.0x1") ]])
    
elseif(sObjectName == "TFToElectrosprite") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToElectrosprite/Scene_Begin.lua")
    
    --Post-exec.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreA", "FORCEPOS:16.0x23.0x1") ]])
    
elseif(sObjectName == "TFToSteam") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ChristineToSteamDroid/Scene_Begin.lua")
    
    --Post-exec.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreA", "FORCEPOS:16.0x23.0x1") ]])
    
elseif(sObjectName == "NoCancel") then
	WD_SetProperty("Hide")
    
--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Seventeen tips for watering your plants -[SOFTBLOCK] Get greenies today!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The article contains a list of tricks like 'Watering the plant regularly' and 'Setting a alarm to water the plants'.[SOFTBLOCK] Seventeen times...)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] 'What is with this fertilizer we're testing?[SOFTBLOCK] They sent us these big unmarked barrels and said to use it.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] 'So I started watering the G-12 batch with it.[SOFTBLOCK] The plants all died within a day.[SOFTBLOCK] A day!'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] 'I asked if they had made a shipping mistake and they told me they had not.[SOFTBLOCK] I asked them what it was and they said it was classified.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] 'This nonsense probably has something to do with the Epsilon Labs.[SOFTBLOCK] Well, I'm not using it.[SOFTBLOCK] It'll sit here until the Head of Research gets her Research Head out of her Research Ass.'") ]])
    fnCutsceneBlocker()

    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end