--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemA") then
        
        --Variables.
        local iEncounterA = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N")
        
        --Action:
        if(iEncounterA == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oof...[SOFTBLOCK] I am thoroughly defeated...") ]])
        
        --Wit:
        elseif(iEncounterA == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'm going to work very hard and not complain![SOFTBLOCK] Being a good guy is great!") ]])
        
        --Dance:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] HQ![SOFTBLOCK] No, this is real![SOFTBLOCK] Stop laughing and send backup![SOFTBLOCK] Arrrgghh!") ]])
        end
    
    elseif(sActorName == "GolemB") then
        
        --Variables.
        local iEncounterA = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N")
        
        --Action:
        if(iEncounterA == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My neck was snapped![SOFTBLOCK] How can this be![SOFTBLOCK] Also I'm still alive because this is a family-friendly videograph.") ]])
        
        --Wit:
        elseif(iEncounterA == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You know what?[SOFTBLOCK] I'm going to tell my tandem unit I love her!") ]])
        
        --Dance:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Hey, don't take this the wrong way, but when we say your dancing is unbelievable, well, it's a compliment.[SOFTBLOCK] Okay?") ]])
        
        end
    
    elseif(sActorName == "GolemC") then
        
        --Variables.
        local iEncounterB = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N")
        
        --Action:
        if(iEncounterB == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oof...[SOFTBLOCK] I hope the justice system goes easy on a first-time evil offender...") ]])
        
        --Wit:
        elseif(iEncounterB == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I was just looking at some evil stuff, don't mind me.[SOFTBLOCK] You go ahead.") ]])
        
        --Dance:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I shall heretofore dedicate my synthetic life to the art of dance.[SOFTBLOCK] Dance![SOFTBLOCK] I love it!") ]])
        
        end
    
    elseif(sActorName == "GolemD") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I am just so caught up in these barrels that I wouldn't even notice if a spy was right behind me.[SOFTBLOCK] Nope, not at all!") ]])
    
    
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You came all the way over here just to talk to me?[SOFTBLOCK] Wow.[SOFTBLOCK] We may be on opposite sides, but that really matters to me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You -[SOFTBLOCK] you care?[SOFTBLOCK] About little old evil me?[SOFTBLOCK] I...[SOFTBLOCK] I don't know what to say...") ]])
    
    elseif(sActorName == "GolemI") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (The director said to stare at the fish, 'but evilly'.[SOFTBLOCK] How the hell do you do that?[SOFTBLOCK] Scowl?[SOFTBLOCK] Grrr![SOFTBLOCK] I am evil, fear me, fish!)") ]])
    
    elseif(sActorName == "GolemJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You can just go right in...[SOFTBLOCK] Amphibian Research is happy to serve all visitors!") ]])
    end
end