--[Encounter A]
--Taking down the two no-good-nicks near the entrance!

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sResultName = LM_GetScriptArgument(0)

--Always clean up dialogue.
WD_SetProperty("Hide")

--[Action]
if(sResultName == "Action") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (*Action* is my middle name![SOFTBLOCK] Agent Action Almond![SOFTBLOCK] It's not confusing at all!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 35.25, 33.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneFace("GolemB", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Hey, you look knowledgeable.[SOFTBLOCK] I was looking for my friend.[SOFTBLOCK] Have you seen her?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Happy to help -[SOFTBLOCK] but, uh, evilly![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] What does your friend look like?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Let's see...[SOFTBLOCK] She's blue, has five fingers, is shaped like a ball, and is coming at your face at 100kph.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Did you just describe a fist?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] HIIYYAAAHHHH!!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --ACTION MOVIE!
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneMoveFace("Sammy", 34.25, 33.50, 1, 0, 1.50)
    fnCutsceneMove("Sammy", 35.75, 33.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemA", 36.25, 32.50, 0, 1, 2.50)
    fnCutsceneMove("Sammy", 36.25, 33.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("GolemA", "Wounded")
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 36.25, 34.50, 2.00)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 37.25, 34.50, 2.00)
    fnCutsceneMoveFace("GolemB", 39.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemB", 37.75, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneMoveFace("Sammy", 35.25, 34.50, 1, 0, 2.00)
    fnCutsceneMoveFace("GolemB", 37.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemB", 38.25, 34.50, -1, 0, 1.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemB", 36.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 35.25, 35.50, 0, -1, 2.00)
    fnCutsceneMoveFace("GolemB", 34.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 35.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 34.75, 34.50, -1, 0, 0.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Ack![SOFTBLOCK] She's got me![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Knock knock![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Who's there?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Neck snap![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] You can't neck snap a golem, lug nut.[SOFTBLOCK] You'll just - [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] ACTION NECK SNAP!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Action.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("GolemB", "Wounded")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 40.25, 35.50)
    fnCutsceneFace("Sammy", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (OW OW OW I THINK I BROKE MY HAND PUNCHING THAT METAL GIRL!!![SOFTBLOCK] THE BONE IS PRACTICALLY POWDER!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Ha ha![SOFTBLOCK] Your martial arts are weak even if your superior metal bodies are much stronger![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] I barely even broke a sweat![SOFTBLOCK] Better luck next time, evildoers![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (There had better not be a next time or I'm going to strangle the director!)") ]])
    fnCutsceneBlocker()

--[Wit]
elseif(sResultName == "Wit") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 2.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (The old Almond trademark wit.[SOFTBLOCK] This will give them something to think about -[SOFTBLOCK] literally and figuratively!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 35.25, 33.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneFace("GolemB", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Hey, you're evil robots, right?[SOFTBLOCK] Decided to work for Evil Corp and all that stuff?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Sure are![SOFTBLOCK] Hey, are you looking to join?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Benefits are great, and the pay isn't half bad despite being part-time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] I just wanted to ask what's so evil about following the rules and doing what you're told in exchange for money.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] If anything, that seems like what good guys do.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Well...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] You see...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] We may not be evil at each individual second of the day, but we're working for an evil cause.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Yeah![SOFTBLOCK] If we didn't move these barrels and punch in on time, our boss couldn't do really evil stuff![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Have you considered maybe punching in but not working?[SOFTBLOCK] That'd be pretty evil.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Or complaining unnecessarily to your Lord Golem.[SOFTBLOCK] Or not converting rogue humans when you detect them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (...[SOFTBLOCK] This script bites...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] No way...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] What I'm saying is, you two aren't evil.[SOFTBLOCK] Your boss is, but you aren't.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] She's right![SOFTBLOCK] Oh no, she's totally right![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] We're not evil![SOFTBLOCK] We're just accessories to evil![SOFTBLOCK] Our job is a sham![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Don't say that![SOFTBLOCK] We're super-evil![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] You pet bunnies in the Biolabs and hum showtunes when you're decontaminiating your chassis![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] I -[SOFTBLOCK] I love my tandem unit, always work hard and show up on time, and give my Lord Golem useful feedback on work conditions![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] We're not evil![SOFTBLOCK] We're good guys![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] But -[SOFTBLOCK] but...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] You're right.[SOFTBLOCK] We've been good guys this whole time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] You were just in denial about evil.[SOFTBLOCK] You wanted the sexy fun time evil...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Well take one look at this bod and tell me that good guys don't have sexy fun times too.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] She's right.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Good guys wear swimsuits and have a pleasing colour scheme![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] That's it![SOFTBLOCK] We're renouncing our bad guy status as of this moment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] From now on, we're all about petting bunnies, following the rules, and working hard with little expectation of reward.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Welcome aboard.[SOFTBLOCK] And you know what the best part is?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] ...[SOFTBLOCK] The good guys always win![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Yeah![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] We believe in you, Agent Almond![SOFTBLOCK] Go get em!") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 35.25, 35.50)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemA", 1, 0)
    fnCutsceneFace("GolemB", 0, -1)
    fnCutsceneMove("Sammy", 40.25, 35.50)
    fnCutsceneFace("Sammy", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Agent Almond does it again.[SOFTBLOCK] It's a shame too, because this is getting too easy.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] In this crazy-mixed up world we live in, there's just less and less of a place for wild action and explosions.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Talking things out and listening to one another is the way that us good guys solve our problems.[SOFTBLOCK] Violence never really solves anything.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] ...[SOFTBLOCK] But listen to this old dinosaur rambling about the changing world.[SOFTBLOCK] I better find that evidence and prove my innocence!") ]])
    fnCutsceneBlocker()
    
--[Dance]
elseif(sResultName == "Dance") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 3.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (All right![SOFTBLOCK] Let's show these metal girls how to boogie!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 34.25, 33.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Hey, evil goons!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneMove("GolemB", 36.25, 34.50)
    fnCutsceneFace("GolemB", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] You think she's talking to us?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Excuse me, but we are not goons.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] I have an advanced degree in hench-woman-ry, and wrote my thesis on cackling in time to your boss' evil laughter.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Wow, really?[SOFTBLOCK] I just showed up the day they opened the secret facility.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Our on-the-job evil-training program is top notch, so you are anything but some regular goon.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Just shut up and watch this!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dancing!
    local ciTicksPerFrame = 8
    local ciMoveIncrement37 = 1.00 / (3.00 * 7.00)
    local p = 0.0
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    for i = 1, 5, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneTeleport("Sammy", 34.25 + p, 33.50)
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            p = p + ciMoveIncrement37
        end
    end
    fnCutsceneFace("GolemA", 0, 1)
    fnCutsceneFace("GolemB", 0, -1)
    
    for i = 1, 2, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceB" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    p = 0
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneTeleport("Sammy", 35.25 - p, 33.50)
            fnCutsceneSetFrame("Sammy", "DanceB" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            p = p + ciMoveIncrement37
        end
    end
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneFace("GolemB", -1, 0)
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    p = 0
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneTeleport("Sammy", 34.25 - p, 33.50)
            fnCutsceneSetFrame("Sammy", "DanceB" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            p = p + ciMoveIncrement37
        end
    end
    fnCutsceneSetFrame("Sammy", "Null")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Ooh aah ooh![SOFTBLOCK] What do you think?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] HQ?[SOFTBLOCK] Yeah -[SOFTBLOCK] yeah it's me.[SOFTBLOCK] We're out moving the barrels -[SOFTBLOCK] yeah, listen.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] There's a blue moth girl in a bikini dancing out here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Well yes, that's what I said.[SOFTBLOCK] Why did you say it back to me?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Stop laughing![SOFTBLOCK] This is serious![SOFTBLOCK] She could be a spy![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] HQ?[SOFTBLOCK] HQ![SOFTBLOCK] Hey, don't put me on speaker phone![SOFTBLOCK] Just -[SOFTBLOCK] damn it![SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Uh, this might take a while.[SOFTBLOCK] You might just want to keep going.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Once again, the power of dance wins the day![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] HQ?[SOFTBLOCK] HQ!?[SOFTBLOCK] AAARRRGGHH!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])


--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end