--[Encounter B]
--Evil with a computer!

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sResultName = LM_GetScriptArgument(0)

--Always clean up dialogue.
WD_SetProperty("Hide")

--[Action]
if(sResultName == "Action") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N", 1.0)
    
    --Disable collision.
    TA_ChangeCollisionFlag("GolemC", false)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Let's see...[SOFTBLOCK] Stealth is on the menu today!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 65.25, 30.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 65.25, 30.00, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Freeze, creep![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh dear, it seems I have been captured.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Or have I?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] You have![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Or have I![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] The answer is still yes![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] (Okay someone missed her cue.[SOFTBLOCK] Blast it![SOFTBLOCK] Improvisation is part of the craft!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneFace("GolemC", 0, 1)
    fnCutsceneMoveFace("Sammy", 65.25, 31.50, 0, -1, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Ouch![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Come, dear child, you face the way of the iron fist![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Which is a real martial art![SOFTBLOCK] Really![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Yeah -[SOFTBLOCK] well -[SOFTBLOCK] *Action* is a martial art, too![SOFTBLOCK] Hyah![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] En garde!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMoveFace("Sammy", 63.25, 30.50, 1, 0, 2.00)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 63.75, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, -1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 63.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, -1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 63.75, 30.50, 1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 64.25, 30.50, 1, 0, 2.00)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 65.25, 29.50, 1, 1, 2.00)
    fnCutsceneFace("GolemC", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 65.25, 29.00, 0, 1, 2.00)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 64.25, 28.00, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 0, -1, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 63.25, 28.00, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 63.25, 30.50, 0, -1, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 64.25, 28.00, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 0, -1, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemC", 64.25, 28.70, 0, -1, 2.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Arrrghh![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Now it is I that have you, do-gooder![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Well, it was a good fight...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] But you made one mistake![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Huh?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Action!
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneMoveFace("GolemC", 64.25, 29.00, 0, -1, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 64.25, 29.50, 0, 1, 2.00)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 0, -1, 2.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Some moths give up -[SOFTBLOCK] and some moths never do![SOFTBLOCK] Rawwwrrr![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh I get it, that's like a play on the title of the videogr -[SOFTBLOCK] Waaahhh!!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Throw her!
    fnCutsceneFace("Sammy", 1, 1)
    fnCutsceneMoveFace("GolemC", 65.25, 29.50, -1, 0, 1.00)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, -1)
    fnCutsceneMoveFace("GolemC", 64.25, 28.50, 0, 1, 1.00)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, -1)
    fnCutsceneMoveFace("GolemC", 63.25, 29.50, 1, 0, 1.00)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, 1)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 1, 0, 1.00)
    fnCutsceneBlocker()
    
    --Faster!
    fnCutsceneFace("Sammy", 1, 1)
    fnCutsceneMoveFace("GolemC", 65.25, 29.50, -1, 0, 1.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, -1)
    fnCutsceneMoveFace("GolemC", 64.25, 28.50, 0, 1, 1.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, -1)
    fnCutsceneMoveFace("GolemC", 63.25, 29.50, 1, 0, 1.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, 1)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 1, 0, 1.50)
    fnCutsceneBlocker()
    
    --Faster!!!
    fnCutsceneFace("Sammy", 1, 1)
    fnCutsceneMoveFace("GolemC", 65.25, 29.50, -1, 0, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, -1)
    fnCutsceneMoveFace("GolemC", 64.25, 28.50, 0, 1, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, -1)
    fnCutsceneMoveFace("GolemC", 63.25, 29.50, 1, 0, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, 1)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 1, 0, 2.50)
    fnCutsceneBlocker()
    
    --THROW!
    fnCutsceneSetFrame("GolemC", "Wounded")
    fnCutsceneMoveFace("GolemC", 64.25, 34.50, 1, 0, 2.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneMoveFace("GolemC", 64.25, 36.50, 1, 0, 0.30)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sammy walks over.
    fnCutsceneMove("Sammy", 64.25, 35.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] *pant*[SOFTBLOCK] *pant*[SOFTBLOCK] Couldn't make it easy, could you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Owww...[SOFTBLOCK] I am vanquished...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Where's the evidence that will exonerate me!?[SOFTBLOCK] Talk![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I don't know, I swear![SOFTBLOCK] It's probably in the secret room in our secret base![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Secret room, huh?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I don't know how to get in, but I know it's in there.[SOFTBLOCK] Please, have mercy![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] You're lucky I'm a good guy, and good guys don't finish off beaten enemies.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] You're going to be arrested and stand trial for all your evil crimes.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh no![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Now, I had better go see about getting into that secret base!") ]])
    fnCutsceneBlocker()
    
    --Enable collision.
    fnCutsceneInstruction([[ TA_ChangeCollisionFlag("GolemC", true) ]])
    fnCutsceneBlocker()

--[Wit]
elseif(sResultName == "Wit") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N", 2.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Maybe it's time to use Agent Almond's Agent Smarts!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 64.25, 29.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneFace("GolemC", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Hey buddy.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Hey b -[SOFTBLOCK] hey![SOFTBLOCK] You look like that agent that everyone is always talking about![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Yeah![SOFTBLOCK] Agent Almond![SOFTBLOCK] She's a moth girl![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Moth girl?[SOFTBLOCK] Do I look like a moth to you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Yes?[SOFTBLOCK] You do?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Sheesh, shows what you know about biology.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Moths are human-sized bipedal hopping mammals with digitigrade legs that carry their young in pouches on their tummies.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Really?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] And do I look like I have digitigrade legs or carry my young in a pouch?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Erm, no.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] But wait, if a moth is that -[SOFTBLOCK] then what are you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Well obviously I'm a kangaroo girl, stupid.[SOFTBLOCK] Do they teach you nothing in evil school?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh no![SOFTBLOCK] I'm sorry![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Sheesh, you have no idea how offensive it is to be mislabelled like that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] No, no![SOFTBLOCK] What can I do to make it up to you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Well...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I really didn't mean to offend you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] I was looking for some information, about Agent Almond, coincidentally.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Apparently us Evil Corp baddies had some but I really wanted to see it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Huh.[SOFTBLOCK] I didn't see it myself, but it'd probably be in the secret room in our secret base over there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] There's a secret room?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Yep.[SOFTBLOCK] I don't know how you get in it, but I'm sure if you ask nicely someone will help you out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Sorry for getting off on the wrong foot like that.[SOFTBLOCK] You seem like a nice and evil robot.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Yeah, you're a pretty cool and evil kangaroo, too.[SOFTBLOCK] See you around!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 64.25, 34.50)
    fnCutsceneFace("GolemC", 0, -1)
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (That had no right working as well as it did... What an idiot!)") ]])
    fnCutsceneBlocker()
    
--[Dance]
elseif(sResultName == "Dance") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N", 3.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] Cue the music![SOFTBLOCK] It's time for that famous Almond Dance!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneMove("Sammy", 63.25, 29.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dance.
    local ciTicksPerFrame = 8
    local ciMoveIncrement37 = 1.00 / (3.00 * 7.00)
    local p = 0.0
    for i = 1, 4, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    fnCutsceneFace("GolemC", -1, 0)
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    fnCutsceneSetFrame("Sammy", "Null")
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Excuse me, but what the hell are you doing?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] Dancing![SOFTBLOCK] Care to join me?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemC", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (What is this feeling within me?)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] (I -[SOFTBLOCK] I must boogie![SOFTBLOCK] I must boogie down!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("GolemC", 65.25, 30.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemC", 65.75, 30.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, 0, -1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, -1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 65.75, 30.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, 0, -1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, -1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 65.75, 30.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, 0, -1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, -1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 65.75, 30.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, 0, -1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, -1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I am dancing![SOFTBLOCK] I am dancing![SOFTBLOCK] I feel the power of dance![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] Pretty good -[SOFTBLOCK] but try this move![SOFTBLOCK] You thrust your pelvis -[SOFTBLOCK] hwah!") ]])
    fnCutsceneBlocker()
    
    local h = 0
    local r = 0.20
    local o = 1
    local iDirX = 0
    local iDirY = 1
    for i = 1, 5, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceB" .. i)
            fnCutsceneMoveFace("GolemC", 65.25 + h, 30.50, iDirX, iDirY, 0.50)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            
            --Direction changer.
            o = o + 1
            if(o < 4) then
                iDirX = 0
                iDirY = 1
            elseif(o < 8) then
                iDirX = 1
                iDirY = 0
            elseif(o < 12) then
                iDirX = 0
                iDirY = -1
            elseif(o < 15) then
                iDirX = 1
                iDirY = 0
            else
                o = 0
            end
            
            --Position changer.
            h = h + r
            if(h >= 1.0) then
                r = r * -1.0
            elseif(h <= -1.0) then
                r = r * -1.0
            end
        end
    end
    for i = 1, 4, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneMoveFace("GolemC", 65.25 + h, 30.50, iDirX, iDirY, 0.50)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            
            --Direction changer.
            o = o + 1
            if(o < 4) then
                iDirX = 0
                iDirY = 1
            elseif(o < 8) then
                iDirX = 1
                iDirY = 0
            elseif(o < 12) then
                iDirX = 0
                iDirY = -1
            elseif(o < 15) then
                iDirX = 1
                iDirY = 0
            else
                o = 0
            end
            
            --Position changer.
            h = h + r
            if(h >= 1.0) then
                r = r * -1.0
            elseif(h <= -1.0) then
                r = r * -1.0
            end
        end
    end
    fnCutsceneSetFrame("Sammy", "Null")
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneFace("GolemC", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Phew![SOFTBLOCK] Nothing like a sexy dance to get rid of excess energy![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Wow![SOFTBLOCK] My life has been so empty![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I dedicated myself to evil deeds and maniacal laughter, but really I needed the thrusting of hips and jiggling of boobs![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Yeah, everyone says that.[SOFTBLOCK] I have that effect on people.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] How can I repay you for this epiphany?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] I happen to be looking for evidence on Agent Almond.[SOFTBLOCK] You know where it might be?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Hmm, probably in the secret base over there.[SOFTBLOCK] Evil Corp likes to keep secret stuff in secret places.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] In fact, it's probably in the secret room in the secret base.[SOFTBLOCK] Secret secret, I've got a secret.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Do you know how I get into that secret room?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] No, but I'm sure someone inside will.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Thank you very much-o missus robot-o,[SOFTBLOCK] for helping me escape to where I need to go.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] No problem!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 64.25, 34.50)
    fnCutsceneMove("GolemC", 65.25, 28.50)
    fnCutsceneFace("GolemC", 0, -1)
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (All right![SOFTBLOCK] Time to get in that secret base!)") ]])
    fnCutsceneBlocker()


--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end