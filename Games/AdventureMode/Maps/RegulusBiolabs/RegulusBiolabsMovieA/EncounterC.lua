--[Encounter C]
--TRIPLE EVIL

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sResultName = LM_GetScriptArgument(0)

--Always clean up dialogue.
WD_SetProperty("Hide")

--[Action]
if(sResultName == "Action") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterC", "N", 1.0)
    
    --Has the door code.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasDoorCode", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] I've got all of five minutes without using my guns -[SOFTBLOCK] let's fix that!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Spawn bullets. 10 for each golem.
    local iCurrentImpact = 1
    local iImpactsTotal = 30
    for q = 1, iImpactsTotal, 1 do
        TA_Create("Impact" .. q)
            TA_SetProperty("Position", -100, -100)
            TA_SetProperty("Rendering Depth", 0.000000)
            TA_SetProperty("Walk Ticks Per Frame", 3)
            TA_SetProperty("Auto Animates Fast", true)
            for i = 1, 8, 1 do
                for p = 1, 4, 1 do
                    TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/Impacts/Bullet" .. (p-1))
                end
            end
        DL_PopActiveObject()
    end
    
    --Setup.
    local fXPos = 94.25
    local fYPos = 22.50
    
    --Sound plays, Sammy faces the target.
    fnCutsceneFace("Sammy", 1, -1)
    Cutscene_CreateEvent("AudioEvent", "Audio")
        AudioEvent_SetProperty("Delay", 1)
        AudioEvent_SetProperty("Sound", "World|MachineGun")
    DL_PopActiveObject()
    
    --Bullet impacts.
    local iDelayTimer = 5
    for i = 1, 10, 1 do
        
        --Spawn position.
        local fSpawnX = LM_GetRandomNumber(-1, 1) + fXPos
        local fSpawnY = LM_GetRandomNumber(-1, 1) + fYPos
        
        --Order the delay.
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Negative Move Timer", iDelayTimer)
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
            ActorEvent_SetProperty("Reset Move Timer")
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
            ActorEvent_SetProperty("Auto Despawn")
        DL_PopActiveObject()
        
        --Move the impact.
        fnCutsceneTeleport("Impact" .. i, fSpawnX, fSpawnY)
        
        --Sound effect.
        Cutscene_CreateEvent("AudioEvent", "Audio")
            AudioEvent_SetProperty("Delay", iDelayTimer)
            AudioEvent_SetProperty("Sound", "World|BulletImpact" .. LM_GetRandomNumber(0, 3))
        DL_PopActiveObject()
        
        --Timing for the next impact.
        iDelayTimer = iDelayTimer + 2
        
    end
    
    --Wait for all the bullets to hit.
    fnCutsceneWait(iDelayTimer)
    fnCutsceneBlocker()
    
    --Switch targets.
    fXPos = 97.25
    fYPos = 22.50
    
    --Sound plays, Sammy faces the target.
    fnCutsceneFace("Sammy", 1, 0)
    
    --Bullet impacts.
    iDelayTimer = 5
    for i = 11, 20, 1 do
        
        --Spawn position.
        local fSpawnX = LM_GetRandomNumber(-1, 1) + fXPos
        local fSpawnY = LM_GetRandomNumber(-1, 1) + fYPos
        
        --Order the delay.
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Negative Move Timer", iDelayTimer)
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
            ActorEvent_SetProperty("Reset Move Timer")
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
            ActorEvent_SetProperty("Auto Despawn")
        DL_PopActiveObject()
        
        --Move the impact.
        fnCutsceneTeleport("Impact" .. i, fSpawnX, fSpawnY)
        
        --Sound effect.
        Cutscene_CreateEvent("AudioEvent", "Audio")
            AudioEvent_SetProperty("Delay", iDelayTimer)
            AudioEvent_SetProperty("Sound", "World|BulletImpact" .. LM_GetRandomNumber(0, 3))
        DL_PopActiveObject()
        
        --Timing for the next impact.
        iDelayTimer = iDelayTimer + 2
        
    end
    
    --Wait for all the bullets to hit.
    fnCutsceneWait(iDelayTimer)
    fnCutsceneBlocker()
    
    --Switch targets.
    fXPos = 97.25
    fYPos = 26.50
    
    --Sound plays, Sammy faces the target.
    fnCutsceneFace("Sammy", 1, 1)
    
    --Bullet impacts.
    iDelayTimer = 5
    for i = 21, 30, 1 do
        
        --Spawn position.
        local fSpawnX = LM_GetRandomNumber(-1, 1) + fXPos
        local fSpawnY = LM_GetRandomNumber(-1, 1) + fYPos
        
        --Order the delay.
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Negative Move Timer", iDelayTimer)
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
            ActorEvent_SetProperty("Reset Move Timer")
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
            ActorEvent_SetProperty("Auto Despawn")
        DL_PopActiveObject()
        
        --Move the impact.
        fnCutsceneTeleport("Impact" .. i, fSpawnX, fSpawnY)
        
        --Sound effect.
        Cutscene_CreateEvent("AudioEvent", "Audio")
            AudioEvent_SetProperty("Delay", iDelayTimer)
            AudioEvent_SetProperty("Sound", "World|BulletImpact" .. LM_GetRandomNumber(0, 3))
        DL_PopActiveObject()
        
        --Timing for the next impact.
        iDelayTimer = iDelayTimer + 2
        
    end
    
    --Wait for all the bullets to hit.
    fnCutsceneWait(iDelayTimer)
    fnCutsceneBlocker()
    
    --Long silence.
    fnCutsceneFace("GolemF", -1, 0)
    fnCutsceneFace("GolemG", -1, 0)
    fnCutsceneFace("GolemH", -1, 0)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We are vanquished![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *But in the future, don't use copperhead slugs.[SOFTBLOCK] They just bounce off our chassis.*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] *It's a movie, stupid![SOFTBLOCK] They're supposed to!*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *Oh, yeah, whoops*.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --They fall over.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("GolemF", "Wounded")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("GolemG", "Wounded")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("GolemH", "Wounded")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneMove("Sammy", 94.25, 23.50)
    fnCutsceneFace("Sammy", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] So since I so deftly defeated you, could you maybe give me the access codes for your secret base?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh, sure.[SOFTBLOCK] The code is 'SlimPickins'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Awesome![SOFTBLOCK] Now to go get the secret evidence!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 90.25, 23.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] The cleanup crew will deal with those thugs.[SOFTBLOCK] Now I have to go into the secret base and find the secret room!") ]])
    fnCutsceneBlocker()
    

--[Wit]
elseif(sResultName == "Wit") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterC", "N", 2.0)
    
    --Has the door code.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasDoorCode", "N", 1.0)
    
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] You have a predicament, and I can help!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 95.25, 24.50)
    fnCutsceneMove("GolemF", 96.25, 22.50)
    fnCutsceneFace("GolemF", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "GolemB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "GolemC", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] You three look unhappy.[SOFTBLOCK] What's wrong?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem A:[E|Neutral] Well, we were just having an evil picnic, see...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem B:[E|Neutral] And Golem C over there was supposed to pack the tinfoil sandwiches![SOFTBLOCK] But she forgot to![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] I just don't like tinfoil sandwiches, okay?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] What a difficult impasse...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Have you considered apologizing, Golem C?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] Apologizing?[SOFTBLOCK] That's not what us bad guys do![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] But aren't you friends with Golem A and Golem B?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (Why does the script call them A and B?[SOFTBLOCK] Did nobody read this before they sent it to me?)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem A:[E|Neutral] Of course we're friends![SOFTBLOCK] Evil friends for evil coporations![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem B:[E|Neutral] Yeah![SOFTBLOCK] Evil all the way![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] No matter how evil you are, you still need to be kind to your friends.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Consider their opinions and care about what they have to say.[SOFTBLOCK] We're all in this together.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] ...[SOFTBLOCK] But I became an evil robot because I hate apologizing![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Oh, so did you become an evil robot because you didn't want to have any friends, too?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Because that's what will happen![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] You're right,[SOFTBLOCK] aren't you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] It's a bad habit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] My fellow evil robots...[SOFTBLOCK] I am sorry I forgot to pack the tinfoil sandwiches.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] I will make it up to you later.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem A:[E|Neutral] We forgive you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem B:[E|Neutral] Yeah![SOFTBLOCK] We've been through so much evil stuff together, it'd be silly to throw it all away over some sandwiches.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] *sniff*[SOFTBLOCK] This is the best evil picnic ever...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Well, looks like my work here is done.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem A:[E|Neutral] Wait![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] Thanks so much for helping us.[SOFTBLOCK] If ever you need something...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] The access codes for the secret base?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem B:[E|Neutral] You mean 'SugarSweetKiss'?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem B:[E|Neutral] Whoops![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem A:[E|Neutral] Ha ha ha![SOFTBLOCK] Oh Golem B, you're such a goofball![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (I better leg it before they figure out I'm a good guy!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 90.25, 23.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (How do I keep getting away with this?)") ]])
    fnCutsceneBlocker()
    
--[Dance]
elseif(sResultName == "Dance") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterC", "N", 3.0)
    
    --Has the door code.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasDoorCode", "N", 1.0)
    
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Move to the beat, gearheads!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 96.25, 24.50, 2.00)
    fnCutsceneBlocker()
    
    --Dancing!
    local ciTicksPerFrame = 6
    for p = 1, 10, 1 do
        
        local sDanceName = "DanceC"
        if(p % 4 < 2) then sDanceName = "DanceD" end
        
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", sDanceName .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    fnCutsceneSetFrame("Sammy", "DanceC0")
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] Come on, do the monkey with me!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("GolemF", 95.25, 24.50, 2.00)
    fnCutsceneMove("GolemG", 97.25, 24.50, 2.00)
    fnCutsceneMove("GolemH", 98.25, 26.50, 2.00)
    fnCutsceneMove("GolemH", 98.25, 24.50, 2.00)
    fnCutsceneBlocker()
    
    --Dance sequence.
    for p = 1, 20, 1 do
        
        local sDanceName = "DanceC"
        local sDanceNameG = "DanceG"
        local sDanceNameGRev = "DanceH"
        if(p % 4 < 2) then 
            sDanceName = "DanceD"
            sDanceNameG = "DanceH"
            sDanceNameGRev = "DanceG"
        end
        
        for i = 0, 7, 1 do
            
            --Sammy's dance:
            fnCutsceneSetFrame("Sammy", sDanceName .. i)
            fnCutsceneSetFrame("GolemF", sDanceNameGRev .. i)
            fnCutsceneSetFrame("GolemG", sDanceNameGRev .. i)
            fnCutsceneSetFrame("GolemH", sDanceNameG .. i)
            
            --Next tick.
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    fnCutsceneFace("GolemF", 1, 0)
    fnCutsceneFace("GolemG", -1, 0)
    fnCutsceneFace("GolemH", -1, 0)
    fnCutsceneSetFrame("Sammy", "Null")
    fnCutsceneSetFrame("GolemF", "Null")
    fnCutsceneSetFrame("GolemG", "Null")
    fnCutsceneSetFrame("GolemH", "Null")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --This is such a dumb joke.
    local iEncounterB = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "GolemC", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] That was fun, wasn't it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem B:[E|Neutral] Amazing![SOFTBLOCK] I love dancing![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] Dancing can bring us together no matter the conflict![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem A:[E|Neutral] The access code for our secret base is [SOFTBLOCK]'FloatingMeatball'![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem B:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem A:[E|Neutral] You guys were all saying stuff and I wanted to contribute...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] It's all okay.[SOFTBLOCK] We're all bad guys here, right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] ..........[SOFTBLOCK] Absolutely![BLOCK][CLEAR]") ]])
    if(iEncounterB ~= 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] Okay, back to our evil picnic.[SOFTBLOCK] But thanks for the dancing lessons, moth girl![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem B:[E|Neutral] Blue moth in a bikini who looks *exactly* like Agent Almond, the very famous do-gooder.[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem C:[E|Neutral] Okay, back to our evil picnic.[SOFTBLOCK] But thanks for the dancing lessons, kangaroo girl![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem B:[E|Neutral] Blue kangaroo in a bikini who looks *exactly* like Agent Almond, the very famous do-gooder.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem A:[E|Neutral] Probably just a coincidence.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Gotta go!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 96.25, 25.50, 2.00)
    fnCutsceneMove("Sammy", 92.25, 25.50, 2.00)
    fnCutsceneMove("Sammy", 91.25, 24.50, 2.00)
    fnCutsceneMove("Sammy", 90.25, 24.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (Just how do I keep getting away with this?[SOFTBLOCK] Well you know what they say...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] (Dancing is good for the body and the soul -[SOFTBLOCK] but it sure doesn't make you any smarter!)") ]])
    fnCutsceneBlocker()
    
    

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end