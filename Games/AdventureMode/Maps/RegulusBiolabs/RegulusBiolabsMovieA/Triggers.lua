--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Introduction") then
    
    --First time.
    local iSawMovieIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMovieIntro", "N")
    if(iSawMovieIntro == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMovieIntro", "N", 1.0)
    
    --Remove followers.
    AL_SetProperty("Unfollow Actor Name", "Sophie")
    
    --Clear music.
	AL_SetProperty("Music", "Null")
    
    --Spawn characters.
    fnSpecialCharacter("Sammy", "Moth", 7, 30, gci_Face_North, false, nil)
    TA_Create("Coconut")
        TA_SetProperty("Position", 8, 30)
        TA_SetProperty("Facing", gci_Face_North)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
    DL_PopActiveObject()
    
    --Add Sammy's dancing ability.
    EM_PushEntity("Sammy")
        for i = 0, 7, 1 do
            TA_SetProperty("Add Special Frame", "DanceA" .. i, "Root/Images/Sprites/Special/SammyDanceA|" .. i)
            TA_SetProperty("Add Special Frame", "DanceB" .. i, "Root/Images/Sprites/Special/SammyDanceB|" .. i)
            TA_SetProperty("Add Special Frame", "DanceC" .. i, "Root/Images/Sprites/Special/SammyDanceC|" .. i)
            TA_SetProperty("Add Special Frame", "DanceD" .. i, "Root/Images/Sprites/Special/SammyDanceD|" .. i)
            TA_SetProperty("Add Special Frame", "DanceE" .. i, "Root/Images/Sprites/Special/SammyDanceE|" .. i)
            TA_SetProperty("Add Special Frame", "DanceF" .. i, "Root/Images/Sprites/Special/SammyDanceF|" .. i)
        end
        TA_SetProperty("Add Special Frame", "SwimS", "Root/Images/Sprites/Special/SammySwimS")
        TA_SetProperty("Add Special Frame", "SwimW", "Root/Images/Sprites/Special/SammySwimW")
    DL_PopActiveObject()
    
    --Switch control to Sammy. Teleport Christine and Sophie out.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Actor Name", "Sammy")
    DL_PopActiveObject()
    fnCutsceneTeleport("Christine", -1.25, -1.50)
    fnCutsceneTeleport("Sophie", -1.25, -1.50)
    
    --Set Sammy as the leading character.
    EM_PushEntity("Sammy")
        local iSammyID = RE_GetID()
    DL_PopActiveObject()
    AL_SetProperty("Player Actor ID", iSammyID)
    
    --Fade to black immediately.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Fade back in.
    fnCutsceneWait(25)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 65, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Begin movement.
    fnCutsceneMove("Sammy", 7.25, 28.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneMove("Coconut", 8.25, 28.50)
    fnCutsceneFace("Coconut", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Coconut", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] All right Coconut, you know your part of the plan?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[E|Neutral] Like the back of my hand.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[E|Neutral] Are you sure you don't need my help with the *Action*?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] I know you want to help, but if we don't have an escape plan, no amount of *Explosions* are going to help us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Make sure you're ready, and I'll deliver the goods.[SOFTBLOCK] Oh, and we'll probably be getting chased by murder robots, so make sure your shoes are tied.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[E|Neutral] I know, I know.[SOFTBLOCK] 'A good agent always has her shoes tied and her guns loaded.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[E|Neutral] So then why are you wearing a bikini, again?[SOFTBLOCK] Was that part of the plan?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] I know you're just a rookie, but you really should know the key to success is the three S's.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[E|Neutral] Wait, I know those![SOFTBLOCK][SOFTBLOCK] Uhhh, one S is...[SOFTBLOCK] shooting![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Shooting,[SOFTBLOCK] seducing,[SOFTBLOCK] and swimming.[SOFTBLOCK] The bikini takes care of two of those...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] And the guns do the rest.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[E|Neutral] Geez I have so much to learn![SOFTBLOCK] Good luck, Almond.[SOFTBLOCK] Give those Evil Corp meanies what for!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Coconut", 8.25, 31.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Coconut", -1.25, -1.50)
    fnCutsceneBlocker()
    
    --Music starts up.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "LAYER|SomeMothsDo") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])

--First ACTION encounter!
elseif(sObjectName == "EncounterA") then

    --Repeat handler.
    local iEncounterA = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N")
    if(iEncounterA > 0.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 1000.0)
    
    --Movement.
    fnCutsceneMove("Sammy", 34.25, 34.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Uh oh, evil goons.[SOFTBLOCK] Looks like they haven't noticed me yet...)") ]])
    fnCutsceneBlocker()
    
    --Camera movement.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (37.25 * gciSizePerTile), (34.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Action.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Phew![SOFTBLOCK] All this evil in such a short span of time.[SOFTBLOCK] Our evil boss must have big plans![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Oh yeah.[SOFTBLOCK] Evil boss said that Agent Almond is 'taken care of'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] You mean like, dead?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Heavens no![SOFTBLOCK] We're evil, not *murderers*.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] I don't think I could bring myself to kill someone...[SOFTBLOCK] even if our evil boss ordered us to...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Oh that's a relief.[SOFTBLOCK] I signed up because the brochures promised sexy evil.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Heh, but we spend all our time moving barrels around and standing with our backs to shadowed areas.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Speaking of, remember the super secret passage that our evil boss said to make sure nobody goes near?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Yeah?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] I -[SOFTBLOCK] completely forgot what I was going to say.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Wedge:[E|Neutral] Evil evil evil![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Vicks:[E|Neutral] Yeah![SOFTBLOCK] Go team evil!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera movement.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Actor Name", "Sammy")
    DL_PopActiveObject()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (All right, how should I take care of these no-good-nicks?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. fnResolvePath() .. "EncounterA.lua" .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Action\", " .. sDecisionScript .. ", \"Action\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Wit\",  " .. sDecisionScript .. ", \"Wit\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Dance\",  " .. sDecisionScript .. ", \"Dance\") ")
    fnCutsceneBlocker()

--Go back south
elseif(sObjectName == "GoSouth") then

    --Repeat handler.
    local iEncounterB = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N")
    if(iEncounterB > 0.0) then return end
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (My secret agent senses are tingling -[SOFTBLOCK] there is *Action* to be had!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0)
        CameraEvent_SetProperty("Focus Position", (64.25 * gciSizePerTile), (30.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0)
        CameraEvent_SetProperty("Focus Actor Name", "Sammy")
    DL_PopActiveObject()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Definitely go that way first...)") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 47.75, 33.50)

--Second ACTION encounter!
elseif(sObjectName == "EncounterB") then

    --Repeat handler.
    local iEncounterB = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N")
    if(iEncounterB > 0.0) then return end
    
    --Reset.
    --fnCutsceneTeleport("GolemC", 65.25, 29.50)
    --fnCutsceneFace("GolemC", 0, -1)
    --fnCutsceneSetFrame("GolemC", "Null")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (That baddie must know something, I had best interrogate her!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 64.25, 31.50)
    fnCutsceneFace("Sammy", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Action.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] La la la, oh what a wonderful, evil world~[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Just doing evil things on this computer, la la la la![BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. fnResolvePath() .. "EncounterB.lua" .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Action\", " .. sDecisionScript .. ", \"Action\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Wit\",  " .. sDecisionScript .. ", \"Wit\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Dance\",  " .. sDecisionScript .. ", \"Dance\") ")
    fnCutsceneBlocker()

--ACTION ACTION ACTION!!!
elseif(sObjectName == "EncounterC") then

    --Repeat handler.
    local iEncounterC = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterC", "N")
    if(iEncounterC > 0.0) then return end
    
    --Reset.
    --[=[fnCutsceneTeleport("GolemF", 94.25, 22.50)
    fnCutsceneFace("GolemF", 0, 1)
    fnCutsceneSetFrame("GolemF", "Null")
    fnCutsceneTeleport("GolemG", 97.25, 22.50)
    fnCutsceneFace("GolemG", 0, 1)
    fnCutsceneSetFrame("GolemG", "Null")
    fnCutsceneTeleport("GolemH", 97.25, 26.50)
    fnCutsceneFace("GolemH", 0, -1)
    fnCutsceneSetFrame("GolemH", "Null")]=]
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Uh oh, three of them![SOFTBLOCK] Those are bad odds -[SOFTBLOCK][SOFTBLOCK][SOFTBLOCK] for them!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 93.25, 24.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Action.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Hey you, evil robots![BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. fnResolvePath() .. "EncounterC.lua" .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Action\", " .. sDecisionScript .. ", \"Action\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Wit\",  " .. sDecisionScript .. ", \"Wit\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Dance\",  " .. sDecisionScript .. ", \"Dance\") ")
    fnCutsceneBlocker()

--Door Cutscene
elseif(sObjectName == "TheDoor") then

    --Repeat handler.
    local iMovieHasSeenDoorScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieHasSeenDoorScene", "N")
    if(iMovieHasSeenDoorScene > 0.0) then return end
    
    --Variables.
    local iMovieHasDoorCode = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieHasDoorCode", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasSeenDoorScene", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Sammy", 42.25, 18.50)
    fnCutsceneFace("Sammy", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Action.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] So this must be Evil Corp's secret base.[SOFTBLOCK] Not very secret, if you ask me, but what would I know about Evil Corporatism?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Hello![SOFTBLOCK] Welcome to the Amphibian Research lab![SOFTBLOCK] Are you a guest or here on business?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] *Psst![SOFTBLOCK] Did you forget your lines?*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Oh, I see.[SOFTBLOCK] Evil Corp disguised their secret base as a research lab![SOFTBLOCK] Clever![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] What are you talking about?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Obviously I won't be able to get inside without the secret access codes, though.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ...[SOFTBLOCK] No?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We're open during guest hours.[SOFTBLOCK] Remember to wash up and wear protective gloves if you wish to handle the toxic species![BLOCK][CLEAR]") ]])
    if(iMovieHasDoorCode == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Luckily, my secret agent senses made sure I already got the secret access codes![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Actually it's an automatic door.[SOFTBLOCK] There's a motion sensor and you just step in front of it, and it opens.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Here I come, secret base![BLOCK][CLEAR]") ]])
        
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Yep, I had better go get the secret access code.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] (...[SOFTBLOCK] I think organics are all crazy...)") ]])
    fnCutsceneBlocker()

end
