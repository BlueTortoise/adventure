--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemA") then
        
        
        local iMovieTalkedToReceptionist = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieTalkedToReceptionist", "N")
        
        --First time.
        if(iMovieTalkedToReceptionist == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieTalkedToReceptionist", "N", 1.0)
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Welcome to the Amphibian Research Laboratories![SOFTBLOCK] Would you like me to take your coat?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ...[SOFTBLOCK] Sorry I have to say that even if you're not wearing a coat.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Nice name for a research lab -[SOFTBLOCK] or secret evil base for Evil Corp![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I don't care what you want to call it.[SOFTBLOCK] Just please stand here and talk to me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] It gets so lonely in here...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Mistreating their employees![SOFTBLOCK] Evil Corp is really evil![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *We're being recorded![SOFTBLOCK] Watch what you say!*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] *I'm starring in a movie, dummy![SOFTBLOCK] Of course we're being recorded!*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *WHAT!?*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] I'm sorry you hapless evil minion, but you won't be stopping me.[SOFTBLOCK] Stand aside![SOFTBLOCK] I must clear my name![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Uhhhh.[SOFTBLOCK] Well, the exhibits are to your left.[SOFTBLOCK] Please enjoy your stay.") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *So are you a videograph star?[SOFTBLOCK] Can I have your autograph?[SOFTBLOCK] You're really cute...*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] *Come visit me after the shoot.[SOFTBLOCK] I love signing autographs for my fans!*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] *Actually you're my first fan.[SOFTBLOCK] But I expect to have more once this videograph is actually released.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Thank you for saving me from this evil corporation, great hero![SOFTBLOCK] I love you![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] *Try to keep a lid on the overacting, rookie...*") ]])
            fnCutsceneBlocker()
        
        end
    end
end