--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToMovieBLft") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsMovieB", "FORCEPOS:5.5x6.0x0")
    
elseif(sObjectName == "ToMovieBRgt") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsMovieB", "FORCEPOS:12.0x4.0x0")
    
elseif(sObjectName == "SecretDoor") then
    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N")
    if(iMovieUnlockSequence >= 5.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsMovieD", "FORCEPOS:26.5x6.0x0")
    end
    
--[Objects]
elseif(sObjectName == "LockedDoor") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Pretty sure the secret room isn't this way...)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "SignA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (For everyone wishing to visit the secret knife-and-fabricated-evidence storage room, please follow the instructions below::)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Enter the pool room, then the office.[SOFTBLOCK] Each time you do, you will hear a chirp.[SOFTBLOCK] Then, check the Fizzy Pop! machine.[SOFTBLOCK] (Sponsored by Fizzy Pop!))[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Then, enter the coat room, and you should hear a chirp.[SOFTBLOCK] Then, enter the pool room again, and the secret room will open.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Remember to maintain an evil mindset at all times.[SOFTBLOCK] How can I make my day-to-day life more evil?[SOFTBLOCK] Consult your Evil Lord Golem if you need help!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Remember to *not* touch the glass.[SOFTBLOCK] It's not evil, it's just annoying!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Wear protective gloves and clean your chassis thoroughly after handling a toxic specimen.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Please do not lick the specimens.[SOFTBLOCK] You know who you are.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] JRO56:: I'm so clever.[SOFTBLOCK] I was asked to write down the instructions for entering the secret room, but I wrote it -[SOFTBLOCK] backwards.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] PinPop:: As in, wrote all the letters backwards?[SOFTBLOCK] Because that's not clever at all.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] JRO56:: No no, the instructions are in the opposite order.[SOFTBLOCK] All we have to do is hope nobody finds that out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] PinPop:: Okay, very clever.[SOFTBLOCK] Great work.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (EVIL NOTICE TO ALL EVIL PERSONNEL::[SOFTBLOCK] Remember to smile!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] ('I hope they're done shooting that videograph soon.[SOFTBLOCK] It's impossible to get any work done around here.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] ('Evil never sleeps, but you do![SOFTBLOCK] Remember to get six hours of defragmentation every night!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Coats") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (A bunch of coats used by the evil Lord Golems who work here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Soda") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Fizzy Pop! is the only drink for a hardworking Agent like me![SOFTBLOCK] I love it!)") ]])
    fnCutsceneBlocker()
    
    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N")
    if(iMovieUnlockSequence == 2.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 3.0)
    end
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end