--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Overhearing") then
    
    --Repeat check.
    local iMovieOverheard = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieOverheard", "N")
    if(iMovieOverheard == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieOverheard", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Sammy", 24.25, 12.50)
    fnCutsceneFace("Sammy", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Open the door.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "MovieDoor") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 23.25, 12.50, 2.00)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sammy] (Yikes![SOFTBLOCK] They almost saw me!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (19.25 * gciSizePerTile), (9.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] Hey Righty, did you hear something?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Righty:[E|Neutral] Something like a door opening and someone running in a panic, and the door still being open?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] Yeah, like that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Righty:[E|Neutral] Nope.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Righty:[E|Neutral] So anyway, you were telling me about the secret room?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Righty:[E|Neutral] Sorry I keep forgetting how to access it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] Oh it's no problem.[SOFTBLOCK] In fact, everyone keeps forgetting how to access it, so I wrote it on one of the signs here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Righty:[E|Neutral] Gee, that seems like a security flaw.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] Tch, as if![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] An intruder would have to even know about our secret base to begin with...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] Slip past all our crack defense teams...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] Get the door access code without triggering a lockdown...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] Know about the secret room to begin with...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] *And* deciper my very tricky secret code.[SOFTBLOCK] Not likely![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Righty:[E|Neutral] Yeah, I get you.[SOFTBLOCK] Only some kind of super agent could pull that off.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] Exactly![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Righty:[E|Neutral] Well, we better get back to our evil tasks.[SOFTBLOCK] What did you get?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] I have to go clean the evil freezers.[SOFTBLOCK] You?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Righty:[E|Neutral] Evil mopping.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lefty:[E|Neutral] Ha ha![SOFTBLOCK] Always evil mopping![SOFTBLOCK] Your Lord Golem must think you're good at it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Righty:[E|Neutral] Come on, let's go.") ]])
    fnCutsceneBlocker()
    
    --Movement.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 1.0)
        CameraEvent_SetProperty("Focus Actor Name", "Sammy")
    DL_PopActiveObject()
    fnCutsceneMove("GolemA", 10.25, 9.50)
    fnCutsceneMove("GolemA", 10.25, 8.50)
    fnCutsceneMove("GolemB", 11.25, 9.50)
    fnCutsceneMove("GolemB", 11.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneTeleport("GolemA", -1.25, -1.50)
    fnCutsceneTeleport("GolemB", -1.25, -1.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] Guess those evil goons didn't count on me![SOFTBLOCK] All I have to do is decipher a secret code using my agent wits, and I'll be a free moth!") ]])
    fnCutsceneBlocker()

--[Secret Room Unlocking]
elseif(sObjectName == "PoolRoom") then

    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N")
    if(iMovieUnlockSequence == 0.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 1.0)
    elseif(iMovieUnlockSequence == 4.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 5.0)
        
        --Three sounds.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Chirp") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Chirp") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Chirp") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
        --Open the door.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Position", (4.75 * gciSizePerTile), (11.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "SecretWalls2", false) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "SecretWallsHi2", false) ]])
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|RemoteDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Actor Name", "Sammy")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
    end


elseif(sObjectName == "CoatRoom") then

    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N")
    if(iMovieUnlockSequence == 1.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 2.0)
    end
    
elseif(sObjectName == "Office") then
    
    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N")
    if(iMovieUnlockSequence == 3.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 4.0)
    end

end
