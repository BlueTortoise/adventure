--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "MovieMovieMovie") then
    
    local iEncounterA = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N")
    if(iEncounterA == 0.0) then
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterC", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasDoorCode", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasSeenDoorScene", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieTalkedToReceptionist", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieOverheard", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 5.0)
    
        --Remove followers.
        AL_SetProperty("Unfollow Actor Name", "Sophie")
        AL_SetProperty("Unfollow Actor Name", "55")
        AL_SetProperty("Unfollow Actor Name", "SX399")
        
        --Spawn Sammy, give her her properties.
        fnSpecialCharacter("Sammy", "Moth", 7, 6, gci_Face_South, false, nil)
        EM_PushEntity("Sammy")
            for i = 0, 7, 1 do
                TA_SetProperty("Add Special Frame", "DanceA" .. i, "Root/Images/Sprites/Special/SammyDanceA|" .. i)
                TA_SetProperty("Add Special Frame", "DanceB" .. i, "Root/Images/Sprites/Special/SammyDanceB|" .. i)
                TA_SetProperty("Add Special Frame", "DanceC" .. i, "Root/Images/Sprites/Special/SammyDanceC|" .. i)
                TA_SetProperty("Add Special Frame", "DanceD" .. i, "Root/Images/Sprites/Special/SammyDanceD|" .. i)
                TA_SetProperty("Add Special Frame", "DanceE" .. i, "Root/Images/Sprites/Special/SammyDanceE|" .. i)
                TA_SetProperty("Add Special Frame", "DanceF" .. i, "Root/Images/Sprites/Special/SammyDanceF|" .. i)
            end
            TA_SetProperty("Add Special Frame", "SwimS", "Root/Images/Sprites/Special/SammySwimS")
            TA_SetProperty("Add Special Frame", "SwimW", "Root/Images/Sprites/Special/SammySwimW")
        DL_PopActiveObject()
    
        --Switch control to Sammy. Teleport Christine and Sophie out.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 5.0)
            CameraEvent_SetProperty("Focus Actor Name", "Sammy")
        DL_PopActiveObject()
        fnCutsceneTeleport("Christine", -1.25, -1.50)
        fnCutsceneTeleport("Sophie", -1.25, -1.50)
        fnCutsceneTeleport("55", -1.25, -1.50)
        fnCutsceneTeleport("SX399", -1.25, -1.50)
        EM_PushEntity("Sammy")
            local iSammyID = RE_GetID()
        DL_PopActiveObject()
        AL_SetProperty("Player Actor ID", iSammyID)
    
    
    end
end
