--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsMovieF"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "NULL")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusBiolabsMovieF")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

	--[Lights]
    --AL_SetProperty("Activate Lights")
    
    --[Graphics]
    SLF_Open(gsDatafilesPath .. "Sprites.slf")

    --Modify the distance filters to keep everything pixellated. This is only for sprites.
    local iNearest = DM_GetEnumeration("GL_NEAREST")
    ALB_SetTextureProperty("MagFilter", iNearest)
    ALB_SetTextureProperty("MinFilter", iNearest)
    ALB_SetTextureProperty("Special Sprite Padding", true)

    DL_AddPath("Root/Images/Sprites/Delete/")
    for i = 0, 9, 1 do
        DL_ExtractBitmap("SammyBoatNormal|" .. i, "Root/Images/Sprites/Delete/SammyBoatNormal" .. i)
        DL_ExtractBitmap("SammyBoatGuns|" .. i,   "Root/Images/Sprites/Delete/SammyBoatGuns" .. i)
        DL_ExtractBitmap("SammyBoatShoot|" .. i,  "Root/Images/Sprites/Delete/SammyBoatShoot" .. i)
        DL_ExtractBitmap("GolemBoat|" .. i,       "Root/Images/Sprites/Delete/GolemBoat" .. i)
    end
    for i = 0, 8, 1 do
        DL_ExtractBitmap("GolemBoatDestroyed|" .. i, "Root/Images/Sprites/Delete/GolemBoatDestroyed" .. i)
    end
    for i = 0, 24, 1 do
        
        DL_ExtractBitmap("GolemBoatDestroying|" .. i, "Root/Images/Sprites/Delete/GolemBoatDestroying" .. i)
    end
    DL_ExtractBitmap("SammyBoatRock", "Root/Images/Sprites/Delete/SammyBoatRock")
    ALB_SetTextureProperty("Restore Defaults")
    ALB_SetTextureProperty("Special Sprite Padding", false)
    SLF_Close()

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    TA_Create("PlayerBoat")
        TA_SetProperty("Position", 10, 10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Golem/", false)
        TA_SetProperty("No Automatic Shadow", true)
        
        --Add all the special frames needed.
        for i = 0, 9, 1 do
            TA_SetProperty("Add Special Frame", "Normal" .. i, "Root/Images/Sprites/Delete/SammyBoatNormal" .. i)
            TA_SetProperty("Add Special Frame", "Guns"   .. i, "Root/Images/Sprites/Delete/SammyBoatGuns"   .. i)
            TA_SetProperty("Add Special Frame", "Shoot"  .. i, "Root/Images/Sprites/Delete/SammyBoatShoot"  .. i)
        end
    DL_PopActiveObject()
    
    --Enemy boat!
    TA_Create("EnemyBoat")
        TA_SetProperty("Position", 10, 10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Golem/", false)
        TA_SetProperty("No Automatic Shadow", true)
        
        --Add all the special frames needed.
        for i = 0, 9, 1 do
            TA_SetProperty("Add Special Frame", "Normal" .. i, "Root/Images/Sprites/Delete/GolemBoat" .. i)
        end
        for i = 0, 8, 1 do
            TA_SetProperty("Add Special Frame", "Destroyed" .. i, "Root/Images/Sprites/Delete/GolemBoatDestroyed" .. i)
        end
        for i = 0, 24, 1 do
            TA_SetProperty("Add Special Frame", "Destroying" .. i, "Root/Images/Sprites/Delete/GolemBoatDestroying" .. i)
        end
    DL_PopActiveObject()
    
    --A big rock!
    TA_Create("Rock")
        TA_SetProperty("Position", 10, 10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Golem/", false)
        TA_SetProperty("No Automatic Shadow", true)
        
        --Add all the special frames needed.
        TA_SetProperty("Add Special Frame", "Rock", "Root/Images/Sprites/Delete/SammyBoatRock")
        TA_SetProperty("Set Special Frame", "Rock")
    DL_PopActiveObject()
    
    --[Variables]
    --These variables are used only for this scene. They get purged when it is over.
	DL_AddPath("Root/Variables/Temporary/Movie/")
    VM_SetVar("Root/Variables/Temporary/Movie/iSammyGlobalTimer", "N", 0.0)
    VM_SetVar("Root/Variables/Temporary/Movie/iSammyBoatState", "N", 0.0)
    VM_SetVar("Root/Variables/Temporary/Movie/iGolemGlobalTimer", "N", 0.0)
    VM_SetVar("Root/Variables/Temporary/Movie/iGolemBoatState", "N", 0.0)
    VM_SetVar("Root/Variables/Temporary/Movie/iGolemPlayedSplash", "N", 0.0)
    
    --[Boat Handlers]
    --These will auto-update the boat animations.
    Cutscene_HandleParallel("Create", "GolemBoatScript",  gsRoot .. "Maps/RegulusBiolabs/RegulusBiolabsMovieF/GolemBoatScript.lua")
    Cutscene_HandleParallel("Create", "PlayerBoatScript", gsRoot .. "Maps/RegulusBiolabs/RegulusBiolabsMovieF/PlayerBoatScript.lua")
    
    --Flags. These will make the parallel scripts keep running even during cutscenes/dialogue.
    AL_SetProperty("Set Parallel Cutscenes During Cutscenes Flag", true)
    AL_SetProperty("Set Parallel Cutscenes During Dialogue Flag", true)
end
