--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Action") then
    
    --Black the screen out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Disable rendering of the under-water.
    AL_SetProperty("Set Layer Disabled", "Floor0", true)
    
    --Move Sammy offscreen, focus the camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Position", (19.25 * gciSizePerTile), (8.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneTeleport("Sammy", -1.25, -1.50)
    fnCutsceneTeleport("PlayerBoat", 38.25, 8.50)
    fnCutsceneTeleport("EnemyBoat", 38.25, 6.50)
    fnCutsceneTeleport("Rock", -10.25, 6.50)
    fnCutsceneBlocker()
    
    --Spawn the bullets we'll need.
    local iCurrentImpact = 1
    local iImpactsTotal = 45
    for q = 1, iImpactsTotal, 1 do
        TA_Create("Impact" .. q)
            TA_SetProperty("Position", -100, -100)
            TA_SetProperty("Rendering Depth", 0.000000)
            TA_SetProperty("Walk Ticks Per Frame", 3)
            TA_SetProperty("Auto Animates Fast", true)
            for i = 1, 8, 1 do
                for p = 1, 4, 1 do
                    TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/Impacts/Bullet" .. (p-1))
                end
            end
        DL_PopActiveObject()
    end
    
    --Music starts.
    fnCutsceneWait(5)
    fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Music", "LAYER|SomeMothsBoat") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("PlayerBoat", 15.25, 8.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Coconut", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] I can't believe we got away with no complications whatsoever![SOFTBLOCK] What an obvious anticlimax![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[E|Neutral] Funny you should say that, because she seemed pretty upset when I stole this boat.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[E|Neutral] She?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[E|Neutral] That evil robot who's chasing us.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Oh no! Enemy robot!
    fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 50.0) ]])
    fnCutsceneMove("EnemyBoat", 23.25, 6.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Switch Sammy's boat position.
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Stop the boat, fugitivies![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] No way, then you'd catch us and we wouldn't be fugitives anymore![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Well yes that is the premise here.[SOFTBLOCK] So stop the boat![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] You stop your boat, jerk![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'm not the jerk, you're the jerk![SOFTBLOCK] Jerk![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] Meanie![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Bolthead![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[VOICE|Coconut] Almond, this is thoroughly unproductive![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] I know![SOFTBLOCK] She's not giving me any ground here![SOFTBLOCK] How can we have an enlightened discussion when she resorts to namecalling?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You literally started the namecalling![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[VOICE|Coconut] I can't lose her![SOFTBLOCK] Her boat is just as fast as ours![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] I have an idea!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Switch Sammy's boat position.
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Temporary/Movie/iSammyBoatState", "N", 1.0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sammy:[VOICE|Sammy] Eat lead, lead head!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Move the bullets over the target with a delay and blast her!
    Cutscene_CreateEvent("AudioEvent", "Audio")
        AudioEvent_SetProperty("Delay", 1)
        AudioEvent_SetProperty("Sound", "World|MachineGun")
    DL_PopActiveObject()
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Temporary/Movie/iSammyBoatState", "N", 2.0) ]])
    
    --Bullet impacts.
    local fXPos = 23.25 + 3.0
    local fYPos =  6.50
    local iDelayTimer = 5
    for i = 1, iImpactsTotal, 1 do
        
        --Spawn position.
        local fSpawnX = (LM_GetRandomNumber(-3, 3) / 2.0) + fXPos
        local fSpawnY = (LM_GetRandomNumber(-3, 3) / 2.0) + fYPos
        
        --Order the delay.
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Negative Move Timer", iDelayTimer)
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
            ActorEvent_SetProperty("Reset Move Timer")
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. i)
            ActorEvent_SetProperty("Auto Despawn")
        DL_PopActiveObject()
        
        --Move the impact.
        fnCutsceneTeleport("Impact" .. i, fSpawnX, fSpawnY)
        
        --Sound effect.
        Cutscene_CreateEvent("AudioEvent", "Audio")
            AudioEvent_SetProperty("Delay", iDelayTimer)
            AudioEvent_SetProperty("Sound", "World|BulletImpact" .. LM_GetRandomNumber(0, 3))
        DL_PopActiveObject()
        
        --Timing for the next impact.
        iDelayTimer = iDelayTimer + 2
        
    end
    
    --Wait for all the bullets to hit.
    fnCutsceneWait(iDelayTimer)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Temporary/Movie/iSammyBoatState", "N", 0.0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(185)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[VOICE|Coconut] Almond,[SOFTBLOCK] I want to be supportive,[SOFTBLOCK] but I don't think that did anything.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] Are you sure?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[VOICE|Coconut] She didn't blow up or anything,[SOFTBLOCK] so yeah I'm pretty sure.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] I wasn't trying to blow her up.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Thank you for that![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] I was trying to hit her steering controls![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ...[SOFTBLOCK] Oh scoots mcgee!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --ROCK!
    fnCutsceneTeleport("Rock", 0.25, 6.50)
    fnCutsceneMove("Rock", 23.25 - 1.0, 6.50, 6.50)
    fnCutsceneMove("PlayerBoat", 0.25, 8.50, 5.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("PlayerBoat", -100.25, 8.50, 3.50)
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Temporary/Movie/iGolemGlobalTimer", "N", 0.0) ]])
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Temporary/Movie/iGolemBoatState", "N", 1.0) ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] [SOUND|Combat|Impact_Strike_Crit]WAAAUUUGHHH!!!") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Floor0", false) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Floor1", true) ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ...[SOFTBLOCK] This is going to look abysmal on my performance report...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Fade out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] We've got the evidence, Coconut, but we need to stop the evil clone of me![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Coconut:[VOICE|Coconut] Evil clone?[SOFTBLOCK] Holy crow![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] Ease off the profanity, Coconut.[SOFTBLOCK] A real agent only swears when it's necessary or just really funny.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Almond:[VOICE|Sammy] Will we succeed?[SOFTBLOCK] Will we fail?[SOFTBLOCK] Please come back after the intermission!") ]])
    fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Music", "NULL") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Transition.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsRaijuRanchC", "FORCEPOS:36.0x23.0x0") ]])
    fnCutsceneBlocker()
end
