--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsRaijuRanchA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "BreannesTheme")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusBiolabsRaijuRanchA")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieLeftInBiolabs", "N", 1.0)

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Golem", "A", "F")
    fnSpawnNPCPattern("Human", "A", "C")
    fnSpawnNPCPattern("Raiju", "A", "I")
    fnSpawnNPCPattern("GolemSlave", "A", "H")
    fnStandardNPCByPosition("DoctorMaisie")
    fnStandardNPCByPosition("Assistant")
    
    --Depending on who the party leader is, spawn the other characters.
    local iSawRaijuIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N") 
    if(iSawRaijuIntro == 1.0) then

        --Variables.
        local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
        local iSawRaijuIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
        
        --Nobody is leading the party. This happens when entering the ranch.
        if(iChristineLeadingParty == 3.0) then
            
            --If we saw the intro already, Sophie needs to spawn.
            if(iSawRaijuIntro == 1.0) then
                TA_Create("Sophie")
                    TA_SetProperty("Position", 52, 34)
                    TA_SetProperty("Facing", gci_Face_South)
                    TA_SetProperty("Clipping Flag", false)
                DL_PopActiveObject()
                LM_ExecuteScript(gsCostumeAutoresolve, "Sophie_Golem")
            end
        
        --Christine is leading the party, and SX-399 is present. Spawn 55 and SX-399.
        elseif(iChristineLeadingParty == 2.0) then
            TA_Create("55")
                TA_SetProperty("Position", 48, 36)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                fnSetCharacterGraphics("Root/Images/Sprites/55/", true)
            DL_PopActiveObject()
            TA_Create("SX399")
                TA_SetProperty("Position", 47, 36)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                fnSetCharacterGraphics("Root/Images/Sprites/SX399Lord/", true)
            DL_PopActiveObject()

        --Christine is leading the party, but SX-399 is not present. Spawn 55.
        elseif(iChristineLeadingParty == 1.0) then
            TA_Create("55")
                TA_SetProperty("Position", 50, 32)
                TA_SetProperty("Facing", gci_Face_West)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                fnSetCharacterGraphics("Root/Images/Sprites/55/", true)
            DL_PopActiveObject()
            fnCutsceneFace("GolemA", 1, 0)
        
        --55 is leading the party with SX-399.
        else
        
            --Get Christine's form.
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
            --Spawn Christine and Sophie. Sprites get set during costume resolving.
            TA_Create("Christine")
                TA_SetProperty("Position", 48, 36)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
            DL_PopActiveObject()
            TA_Create("Sophie")
                TA_SetProperty("Position", 47, 36)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
            DL_PopActiveObject()
            
            --Run Christine's form handler.
            if(sChristineForm == "Human") then
                LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua")
            elseif(sChristineForm == "Golem") then
                LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
            elseif(sChristineForm == "Raiju") then
                LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua")
            else
                LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua")
            end
            
            --Run Sophie's costume resolver.
            LM_ExecuteScript(gsCostumeAutoresolve, "Sophie_Golem")
        end
    end
end
