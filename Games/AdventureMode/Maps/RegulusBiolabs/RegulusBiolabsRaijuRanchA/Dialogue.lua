--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Variables]
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = "None"
if(sTopicName ~= "YesChristine" and sTopicName ~= "Yes55" and sTopicName ~= "NoCancel") then
    sActorName = TA_GetProperty("Name")
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --[Lord Golems]
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        if(iChristineLeadingParty >= 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[VOICE|GolemLord] Don't cause any problems for me or my staff, and we'll get along just fine.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[VOICE|GolemLord] I've got my optical receptors trained on you, Unit 2855.") ]])
        end
    
    elseif(sActorName == "GolemB") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] Did you see the creatures at the gala?[SOFTBLOCK] I've heard many rumours, but no facts...") ]])
    
    elseif(sActorName == "GolemC") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] There was an explosion, and I just -[SOFTBLOCK] I just ran.[SOFTBLOCK] Oh my goodness was I scared!") ]])
    
    elseif(sActorName == "GolemD") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] I haven't had occassion to visit the biolabs before.[SOFTBLOCK] I must say I appreciate the color scheme.") ]])
    
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] Entertainment passes to the labs?[SOFTBLOCK] Oh yes, they cost quite a few work credits.[SOFTBLOCK] Only the high ranking or frugal lords visit often.") ]])
    
    elseif(sActorName == "GolemF") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] What are we going to do?[SOFTBLOCK] Oh oh oh, it's all so hopeless...") ]])
    
    --[Humans]
    elseif(sActorName == "HumanA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Welcome to the Raiju Ranch, I suppose.[SOFTBLOCK] Our accomodations are limited, but we make the best of it.") ]])
        
    elseif(sActorName == "HumanB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF1] I'm keeping an eye on this area of the perimeter.[SOFTBLOCK] We don't have the numbers for any sort of scouting.") ]])
        
    elseif(sActorName == "HumanC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF1] Do you have any idea what happened at the gala?[SOFTBLOCK] A lot of frightened units have been showing up, and I don't know if we have space for all of them.") ]])
        
    elseif(sActorName == "Assistant") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        if(iChristineLeadingParty >= 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] I am doctor Maisie's assistant.[SOFTBLOCK] If you have any injuries, please let me know.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] She has an organic assistant?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Of course![SOFTBLOCK] I hope to have her job someday![SOFTBLOCK] Best way to learn is by experience, you know.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] I am doctor Maisie's assistant.[SOFTBLOCK] If you have any injuries, please let me know.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] It is unlikely that a medical worker could do much for an inorganic unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Hey I've dabbled in repairs, too.[SOFTBLOCK] A lot is quite similar if you just be generous with the auto-repair nanites.") ]])
        end
    
    --[Raijus]
    elseif(sActorName == "RaijuA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] Argh, my bed is a total mess![SOFTBLOCK] Every time I straighten it out, the static electricity causes it to curl!") ]])
        
    elseif(sActorName == "RaijuB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF1] Everyone is up and the lights are on despite the late hour.[SOFTBLOCK] I'm so tired, I just want to go to bed.[SOFTBLOCK] What's going on?") ]])
        
    elseif(sActorName == "RaijuC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] *pant*[SOFTBLOCK] *pant*[SOFTBLOCK] I'm just -[SOFTBLOCK] charging up for our next session.[SOFTBLOCK] Phew!") ]])
        
    elseif(sActorName == "RaijuD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF1] Hey there, cuties![SOFTBLOCK] Want to join us?[SOFTBLOCK] We're just warming up for round six!") ]])
        
    elseif(sActorName == "RaijuE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] They provide us reading material in case we need inspiration.[SOFTBLOCK] Honestly?[SOFTBLOCK] All I need is a hug and I'm good to go!") ]])
        
    elseif(sActorName == "RaijuF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF1] I kinda set fire to a power conduit because I got too excited, so now I've assigned to cleaning up dishes after chow time.") ]])
        
    elseif(sActorName == "RaijuG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        WD_SetProperty("Unlock Topic", "Biolabs_Games", 1)
        if(iChristineLeadingParty >= 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] *tap tap*[SOFTBLOCK] Oh, sorry.[SOFTBLOCK] I'm playing a video game -[SOFTBLOCK] I just need special gloves or the console will get fried.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] What game are you playing?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] It's called 'Unit 2209:: Silent Infiltrator'.[SOFTBLOCK] You have to sneak into places, find high-ranking people, and convert them into golems without getting caught.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] It's way too easy, though.[SOFTBLOCK] They give you the spaghetti sauce right at the start of the game.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] The what now?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] *Spaghetti Punch!*[SOFTBLOCK] *Spaghetti Throw!*[SOFTBLOCK] See, so easy![SOFTBLOCK] Mission complete.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] *tap tap*[SOFTBLOCK] Oh, sorry.[SOFTBLOCK] I'm playing a video game -[SOFTBLOCK] I just need special gloves or the console will get fried.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] Oh, what game?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] It's called 'Unit 2209:: Silent Infiltrator'.[SOFTBLOCK] You have to sneak into places, find high-ranking people, and convert them into golems without getting caught.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] A training program, likely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] It's way too easy, though.[SOFTBLOCK] They give you the spaghetti sauce right at the start of the game.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] *Spaghetti Punch!*[SOFTBLOCK] *Spaghetti Throw!*[SOFTBLOCK] See, so easy![SOFTBLOCK] Mission complete.") ]])
        end
        
    elseif(sActorName == "RaijuH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF1] A lot of really well dressed golems have been coming through here.[SOFTBLOCK] You think any of them want to snuggle?") ]])
        
    elseif(sActorName == "RaijuI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] I heard something was wrong with the other Raijus.[SOFTBLOCK] Maybe they didn't get enough cuddles?[SOFTBLOCK] Can I help?[SOFTBLOCK] I'm good at cuddling!") ]])
    
    --[Slave Golems]
    elseif(sActorName == "GolemSlaveA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We normally allow units to watch the sunrise if they want to, but otherwise they'd be in bed by now.[SOFTBLOCK] What's going on out there?") ]])
        
    elseif(sActorName == "GolemSlaveB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I've been working in this sector for a few years now, but I still don't understand 'injections'.[SOFTBLOCK] Why don't you just use the existing infiltration ports?") ]])
        
    elseif(sActorName == "GolemSlaveC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        if(iChristineLeadingParty >= 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We haven't had any serious injuries yet, but we haven't heard from Biological Services.[SOFTBLOCK] I hope they're okay.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Are all the organics accounted for?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The humans are, but there's a lot of Raijus missing, and we can't get a lock on their tracking collars...") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We haven't had any serious injuries yet, but we haven't heard from Biological Services.[SOFTBLOCK] I hope they're okay.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Can you re-establish communications with their barracks?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We'd have to run a line out there, and we don't have any spare security units.[SOFTBLOCK] So, until the network is fixed, no.[SOFTBLOCK] I hope they're okay...") ]])
        end
    elseif(sActorName == "GolemSlaveD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        if(iChristineLeadingParty >= 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You know, 'breakfast' actually tastes pretty good, even by organic standards.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We get fresh eggs, fruits, wheatflour -[SOFTBLOCK] and even ice cream![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Ice cream?[SOFTBLOCK] What do you milk?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] There are dromedaries in the dry habitats, and sheep in the gamma labs.[SOFTBLOCK] Plus, sometimes we have enough human milk if they're exceptionally productive. It's all great![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] But not Raiju milk?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh, oh my goodness no.[SOFTBLOCK] The only thing that comes out when you squeeze them is lightning bolts!") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You know, 'breakfast' actually tastes pretty good, even by organic standards.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We get fresh eggs, fruits, wheatflour -[SOFTBLOCK] and even ice cream![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Does your Lord Golem know you consume organic rations?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] What she doesn't know can't hurt her, right?") ]])
        end
        
    elseif(sActorName == "GolemSlaveE") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Steel?[SOFTBLOCK] Can't we make carbonweave cookware?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I have to lubricate the pans or everything sticks![SOFTBLOCK] It's like being in the stone age!") ]])
        
    elseif(sActorName == "GolemSlaveF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I'm sorry, the kitchen isn't currently serving anything.[SOFTBLOCK] We can provide some pre-made snacks if you like.") ]])
        
    elseif(sActorName == "GolemSlaveG") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] They just go through this stuff like candy...") ]])
        
    elseif(sActorName == "GolemSlaveH") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] It'll be all right.[SOFTBLOCK] Everything we'll be okay.[SOFTBLOCK] We'll make it through this.") ]])
        
    
    --[Doctor Maisie]
    elseif(sActorName == "DoctorMaisie") then
        LM_ExecuteScript(fnResolvePath() .. "DialogueMaisie.lua", "Hello")
    
    --[Party Members]
    elseif(sActorName == "55") then
    
        --SX-399 is not a party member.
        local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
        if(iChristineLeadingParty == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will accompany you when you exit the ranch.[SOFTBLOCK] Otherwise, enjoy yourselves.") ]])
            fnCutsceneBlocker()
            
        --SX-399 is a party member. Allow swapping.
        elseif(iChristineLeadingParty == 2.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Want to switch investigators?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesChristine\") ")
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoCancel\") ")
            fnCutsceneBlocker()
        end

    elseif(sActorName == "SX399") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I'll keep an eye on her, don't you worry.[SOFTBLOCK] I won't let her scamper off!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Christine") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Want to switch investigators?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes55\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoCancel\") ")
        fnCutsceneBlocker()
        
    elseif(sActorName == "Sophie") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Do behave, you two.[SOFTBLOCK] I'd hate to get kicked out of such a nice place because you melted something.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] What do you mean, melted something?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What do you mean, nice place?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Aahhh![SOFTBLOCK] Good one![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (I must have accidentally made a joke...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Thank you.") ]])
        fnCutsceneBlocker()
    
    end
    
--[Decision Handlers]
--Switch to 55/SX-399 as investigators.
elseif(sTopicName == "YesChristine") then
	WD_SetProperty("Hide")
    
    --Disable collision flags for 55 and SX-399.
    EM_PushEntity("55")
        local i55ID = RE_GetID()
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
    DL_PopActiveObject()
    EM_PushEntity("SX399")
        local iSX399ID = RE_GetID()
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
    DL_PopActiveObject()
    
    --Set follower values.
    AL_SetProperty("Unfollow Actor Name", "Sophie")
    AL_SetProperty("Player Actor ID", i55ID)
    AL_SetProperty("Follow Actor ID", iSX399ID)
    gsFollowersTotal = 1
    gsaFollowerNames = {"SX399"}
    giaFollowerIDs   = {iSX399ID}
    
    --Order Christine and Sophie to move over to the fence.
    fnCutsceneMove("Christine", 46.25, 35.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Sophie", 47.25, 35.50)
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneMove("55", 51.25, 35.50)
    fnCutsceneFace("55", 0, 1)
    fnCutsceneMove("SX399", 51.25, 35.50)
    fnCutsceneFace("SX399", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold.
    fnAutoFoldParty()
    fnCutsceneBlocker()
            
    --Set collision flags and dialogue flags for the departing characters.
    local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SetChristineProperties.lua\")"
    fnCutsceneInstruction(sString)
    sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SetSophieProperties.lua\")"
    fnCutsceneInstruction(sString)
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 0.0)
    
    --Set leader voice.
    gsPartyLeaderName = "55"
    WD_SetProperty("Set Leader Voice", "2855")

--Switch to Christine/Sophie as investigators.
elseif(sTopicName == "Yes55") then
	WD_SetProperty("Hide")
    
    --Disable collision flags for Christine and Sophie.
    EM_PushEntity("Christine")
        local iChristineID = RE_GetID()
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
    DL_PopActiveObject()
    EM_PushEntity("Sophie")
        local iSophieID = RE_GetID()
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
    DL_PopActiveObject()
    
    --Set follower values.
    AL_SetProperty("Unfollow Actor Name", "SX399")
    AL_SetProperty("Player Actor ID", iChristineID)
    AL_SetProperty("Follow Actor ID", iSophieID)
    gsFollowersTotal = 1
    gsaFollowerNames = {"Sophie"}
    giaFollowerIDs   = {iSophieID}
    
    --Order 55 and SX399 to move over to the fence.
    fnCutsceneMove("55", 46.25, 35.50)
    fnCutsceneFace("55", 0, 1)
    fnCutsceneMove("SX399", 47.25, 35.50)
    fnCutsceneFace("SX399", 0, 1)
    fnCutsceneMove("Christine", 51.25, 35.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Sophie", 51.25, 35.50)
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold.
    fnAutoFoldParty()
    fnCutsceneBlocker()
            
    --Set collision flags and dialogue flags for the departing characters.
    local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "Set55Properties.lua\")"
    fnCutsceneInstruction(sString)
    sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SetSX399Properties.lua\")"
    fnCutsceneInstruction(sString)
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 2.0)
    
    --Set leader voice.
    gsPartyLeaderName = "Christine"
    WD_SetProperty("Set Leader Voice", "Christine")

--Backing out of dialogue.
elseif(sTopicName == "NoCancel") then
	WD_SetProperty("Hide")


end