--[Root]
--Doctor Maisie's dialogue script.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

    --Variables
    local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
    local iRaibieQuest           = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
    local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

    --Common setup.
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])

    --Talking to Christine:
    if(iChristineLeadingParty ~= 0.0) then
        
        --Display.
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        
        --First time talking to the doc:
        if(iRaibieQuest == 0.0) then
            
            --Flag:
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 1.0)
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Ah ah, what is sis enchanting sing I see before me?[BLOCK][CLEAR]") ]])
            if(sChristineForm == "Human") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Unit 771852 in her, shall we say, more delicious variation?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I hope you don't talk to everyone you meet like this.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Sis is merely my way of paying a compliment, my friend.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] You are doubtless wondering, as many do, whesser or not I must feast on se blood of the living to survive?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I wasn't until you asked.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] The answer is no.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] As wiss all of the subspecies I have examined, omnivorality is not lost during the partirhuman transformassion process.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] While stated preferences change, sese vary between individuals.[SOFTBLOCK] The digestive capacity for meat and plant material is unchanged.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] She's trying to say she doesn't want to suck your blood.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, I picked that much up.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You must deal with that accusation quite often.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Oddly, more from golems san humans.[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Unit 771852 in her mechanical variation?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] How are you aware of my unique abilities?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] I was paying you a compliment, my friend.[SOFTBLOCK] But if you must know...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 2856?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] 2856.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] She talks a lot doesn't she?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Endlessly and often loudly.[BLOCK][CLEAR]") ]])
            end
        
            --Resume.
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] But enough about that nonsense.[SOFTBLOCK] Introductions are in order, yes?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I am doctor Maisie de Havilland of An-Nador.[SOFTBLOCK] Yes, yes, if you must know, I [SOFTBLOCK]*did*[SOFTBLOCK] attend se University of Nador Nang.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (I really should download a database on Pandemonium's geography one of these days.[SOFTBLOCK] For now, just smile and nod.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm Unit 771852, and this is Unit 499323.[SOFTBLOCK] We go by Christine and Sophie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hello, Dr. de Havilland.[BLOCK][CLEAR]") ]])
            if(sChristineForm == "Human") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Oh, I am, [SOFTBLOCK]how do you say,[SOFTBLOCK] 'informed' about you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] In fact, se Head of Research herself has volunteered your servises.[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] So, to get to se business at hand:: Se Head of Research has volunteered me your services.[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] And since sis is not se first time she has done so, I will assume that she has not consulted you on sis volunteering.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not at all.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Fursser, she did this only after multiple requests and with an undisguised display of disdain.[SOFTBLOCK] Serefore, I am to assume she did not mean it in se slightest.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] At least this means she's not being mean to only you, Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] How reassuring.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But, doctor, what do you need my help for?[SOFTBLOCK] I don't know anything about medicine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] No, but you do know somesing about the art of fighting, yes?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] We here in se Biolabs are currently dealing wiss a most curious passogen of unidentified providence, and it afflicts one partirhuman species alone.[SOFTBLOCK] Doubtless you have seen sem.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The green Raijus?[SOFTBLOCK] We saw them on the way here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Yes.[SOFTBLOCK] The staff here have taken to calling sem 'Raibies'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (That's so corny that only a real hack could have come up with it.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I came up wiss se name, due to similarities between the frothing of a rabid mammal and the frothing on se Raijus, though I cannot be certain if there is any genetic relation.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] So you need my help finding a cure?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Indeed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll help as best as I am able.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Splendid.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Is it risky?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't worry, Sophie.[SOFTBLOCK] My inorganic forms will be totally immune from infection.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Will they be immune to getting clawed by a rabid Raiju?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] My friend, nossing in sis life is wissout risk.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] Just don't get hurt...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Now, Christine, what I require from you is rasser simple.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Confront one of se Raibies specimens in se Gamma Laboraties and extract a tissue sample from her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Will a tuft of hair do?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] [SOUND|World|TakeItem]Sis sensor suite for your PDU will be able to perform se necessary work.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Hair samples, skin samples, saliva samples...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Blood samples...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It should be no difficulty to acquire seese once you have subdued one of se specimens.[SOFTBLOCK] Bring sem to me as soon as you do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And you can make a cure?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] No, I cannot guarantee sat.[SOFTBLOCK] At sis junction, I know literally nossing about se passogen.[SOFTBLOCK] Viral or bacterial, or a toxin? Attacking se brain, se kidneys, se toncils?[SOFTBLOCK] I know nossing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] But I assure you sat I will do what I can.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That's the best I can ask for.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It was very nice to meet you, doctor.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] And miss Christine?[SOFTBLOCK] If you choose an organic form for sis task -[SOFTBLOCK] please do not add yourself to my list of patients, yes?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You got it.") ]])
        
        --Repeats but hasn't gotten the samples
        elseif(iRaibieQuest == 1.0) then
        
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Be aware sat what little we do know about se Raibies indicates increased strengss and speed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Doubtless their electrical attacks will be even more potent san even the most excited Raiju.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We'll be careful.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Please return to me once you have se samples, sen.[SOFTBLOCK] I will make sure all my equipment is ready.") ]])
        
        --Got the samples.
        elseif(iRaibieQuest == 2.0) then
            
            --Flag:
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 3.0)
            
            --Dialogue:
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Ah, se samples, excellent.[SOFTBLOCK] Your PDU already sent me a preview but se results were not conclusive.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will need a few moments...") ]])
            fnCutsceneBlocker()
        
            --Merge party.
            fnCutsceneWait(25)
            fnCutsceneMergeParty()
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Christine", 56.25, 12.50)
            fnCutsceneFace("Christine", -1, 0)
            fnCutsceneMove("Sophie",    56.25, 13.50)
            fnCutsceneFace("Sophie",    -1, 0)
            fnCutsceneBlocker()
            
            --Door is not open:
            if(AL_GetProperty("Is Door Open", "MedicalDoor") == false) then
                fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
                fnCutsceneFace("DoctorMaisie", 0, -1)
                fnCutsceneBlocker()
                fnCutsceneWait(15)
                fnCutsceneBlocker()
                fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
                fnCutsceneInstruction([[ AL_SetProperty("Open Door", "MedicalDoor") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(15)
                fnCutsceneBlocker()
            else
                fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
            end
            fnCutsceneMove("DoctorMaisie", 55.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 8.50)
            fnCutsceneMove("DoctorMaisie", 56.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 55.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 54.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 57.25, 8.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 8.25)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneFace("DoctorMaisie", 0, 1)
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 57.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 55.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
            fnCutsceneFace("DoctorMaisie", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Please forgive my medical jargon, but we are, [SOFTBLOCK]how you say,[SOFTBLOCK] 'boned'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] That doesn't sound good.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I have identified a likely passogen and already synssesized a likely cure.[SOFTBLOCK] At least, according to the usual principles of retroviral engineering.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] There's always a catch, isn't there?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will try to keep sis as simple as possible.[SOFTBLOCK] Hmmm...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se passogen is self-reactive.[SOFTBLOCK] It is capable of identifying threats and adapting to it.[SOFTBLOCK] This is unlike anysing seen in nature.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Indeed, such sings are impossible in nature, as se process of adaptation at sis scale requires evolutionary changes.[SOFTBLOCK] Yet, sis passogen is capable of it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Haven't you seen the strange creatures in the labs?[SOFTBLOCK] I can tell you right now that they have a very different idea of possible.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Yes, and it is no coincidence that sey appeared at se same time as the Raibies became infected.[SOFTBLOCK] Still, proving the association was an important step.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But can you cure the Raijus?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sat is still possible, but I would need to know se original nature of the passogen.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] You see, sere were a dozen versions of it in the sample provided.[SOFTBLOCK] Sey had some common characteristics, but had already mutated semselves in response to se Raiju's immune system.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I suspect sat sere is a 'prime' version of se passogen.[SOFTBLOCK] Finding sat would aid in the process of curing it immensely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But it's not a guarantee?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Those poor Raijus...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Nosing is guaranteed in science.[SOFTBLOCK] We merely do what we can.[BLOCK][CLEAR]") ]])
            
            --Variables.
            local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
            if(iRaibieFoundDrums == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 4.0)
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] [EMOTION|Sophie|Neutral]Actually, Dr. de Havilland? Could you take a look at this reading?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Substance E-v89-r.[SOFTBLOCK] Odorless, colourless, virtually untraceable.[SOFTBLOCK] Dissolves in water, half-life sree hours...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Where did you find sis?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A raised section of the gamma laboratories, past the tissue testing center.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Yes, yes.[SOFTBLOCK] For, you see, sis material is not in se database.[SOFTBLOCK] At all.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] More top secret naughtiness from the Head of Research...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I would guess so.[SOFTBLOCK] Doesn't surprise me she'd be researching something like this.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Hmmm...[SOFTBLOCK] Sere are similarities, to be sure.[SOFTBLOCK] Sis substance would likely cause se aggression we observe, but would have a short duration.[SOFTBLOCK] Meant to be used as a combat stimulant, I would suspect...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Furser, it would be metabolized quickly in any living species.[SOFTBLOCK] Why has it affected se Raijus alone?[SOFTBLOCK] Why has it become self-reactive?[SOFTBLOCK] Sese are serious questions.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can we help answer them?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] We've got to help those poor Raijus![SOFTBLOCK] That big dumb meanie is using them as test subjects![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] ...[SOFTBLOCK] Yes, yes we can help sem.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] All I would need to do is observe an uninfected Raiju undergo infection, with my special monitoring nanites.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Deliberately infect one of these Raijus?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Doctor![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Oh no, of course not.[SOFTBLOCK] Not one of sem.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] You, Christine.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Me![SOFTBLOCK] But I'm not - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] I get where you're coming from.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You want to turn her into a Raiju, and then have her infect herself?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sat is correct, my friend.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will be able to observe se infection as it evolves, and simultaneously extract se information from her immune system's respones.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I would hypossisize sat a large influx of antibodies produced in a cloning machine, injected at once, would overwhelm se passogen and eliminate it before it self-reacts.[SOFTBLOCK] But I would need se first-response antibodies from a fresh infection.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Even if I am incorrect, Christine can transform herself and immediately be cured.[SOFTBLOCK] So sere is little danger.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I know it sounds risky, Sophie...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Christine, the transformation process involves...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I'm okay with this on one condition.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Of course I'll be careful, Sophie.[SOFTBLOCK] 55 will - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] I get to watch and record it so I can watch it later![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] ...[SOFTBLOCK] Excuse me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se Raiju transformation process involves sex with a Raiju, 771852.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh, oh![SOFTBLOCK] I'll need to get special discharge protectors -[SOFTBLOCK] and find a way to protect the cameras![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Cameras?[SOFTBLOCK] Plural?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I'll need to record it from different angles![SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It seems your tandem unit has made up her mind.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Speak to me again when you are ready.[SOFTBLOCK] I will get my nanite cocktail ready.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Sure thing, doc...") ]])
                fnCutsceneBlocker()
            
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] [EMOTION|Sophie|Neutral]Doctor, what if we could find the origin of the pathogen?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It's possible, if any of se source material is still present, sat I could work wiss it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It would most likely be located somewhere in se gamma laboratories, since sat is where se Raibies are located.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I may be able to do somesing if you can find it, yes.[SOFTBLOCK] Assuming it exists...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If it does, we'll find it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Bring a scan from your PDU back for me if you do.[SOFTBLOCK] I will continue to work on sese samples...") ]])
                fnCutsceneBlocker()
            
            end
        
            --Movement.
            fnCutsceneMove("DoctorMaisie", 54.25, 12.50)
            fnCutsceneMove("Sophie", 56.25, 12.50)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
        
        --Got the samples, need to find the source of the pathogen.
        elseif(iRaibieQuest == 3.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Any results on se source of se passogen?[BLOCK][CLEAR]") ]])
            
            --Variables.
            local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
            if(iRaibieFoundDrums == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 4.0)
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Maybe.[SOFTBLOCK] Take a look at this stuff, doc.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Substance E-v89-r.[SOFTBLOCK] Odorless, colourless, virtually untraceable.[SOFTBLOCK] Dissolves in water, half-life sree hours...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Where did you find sis?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A raised section of the gamma laboratories, past the tissue testing center.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Yes, yes.[SOFTBLOCK] For, you see, sis material is not in se database.[SOFTBLOCK] At all.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] More top secret naughtiness from the Head of Research...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I would guess so.[SOFTBLOCK] Doesn't surprise me she'd be researching something like this.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Hmmm...[SOFTBLOCK] Sere are similarities, to be sure.[SOFTBLOCK] Sis substance would likely cause se aggression we observe, but would have a short duration.[SOFTBLOCK] Meant to be used as a combat stimulant, I would suspect...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Furser, it would be metabolized quickly in any living species.[SOFTBLOCK] Why has it affected se Raijus alone?[SOFTBLOCK] Why has it become self-reactive?[SOFTBLOCK] Sese are serious questions.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can we help answer them?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] We've got to help those poor Raijus![SOFTBLOCK] That big dumb meanie is using them as guinea pigs![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] ...[SOFTBLOCK] Yes, yes we can help sem.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] All I would need to do is observe an uninfected Raiju undergo infection, with my special monitoring nanites.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Deliberately infect one of these Raijus?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Doctor![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Oh no, of course not.[SOFTBLOCK] Not one of sem.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] You, Christine.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Me![SOFTBLOCK] But I'm not - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] I get where you're coming from.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You want to turn her into a Raiju, and then have her infect herself?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sat is correct, my friend.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will be able to observe se infection as it evolves, and simultaneously extract se information from her immune system's respones.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I would hypossisize sat a large influx of antibodies produced in a cloning machine, injected at once, would overwhelm se passogen and eliminate it before it self-reacts.[SOFTBLOCK] But I would need se first-response antibodies from a fresh infection.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Even if I am incorrect, Christine can transform herself and immediately be cured.[SOFTBLOCK] So sere is little danger.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I know it sounds risky, Sophie...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Christine, the transformation process involves...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I'm okay with this on one condition.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Of course I'll be careful, Sophie.[SOFTBLOCK] 55 will - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] I get to watch and record it so I can watch it later![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] ...[SOFTBLOCK] Excuse me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se Raiju transformation process involves sex with a Raiju, 771852.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh, oh![SOFTBLOCK] I'll need to get special discharge protectors -[SOFTBLOCK] and find a way to protect the cameras![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Cameras?[SOFTBLOCK] Plural?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I'll need to record it from different angles![SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It seems your tandem unit has made up her mind.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Speak to me again when you are ready.[SOFTBLOCK] I will get my nanite cocktail ready.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Sure thing, doc...") ]])
                fnCutsceneBlocker()
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Not yet, doc.[SOFTBLOCK] We're looking.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It will likely be somewhere in se gamma laboratories, likely in a storage medium.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Hmm...[SOFTBLOCK] Perhaps a leaking container?[SOFTBLOCK] Search carefully!") ]])
            end
            
        --Raiju TF time!
        elseif(iRaibieQuest == 4.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Ready to become a Raiju for se Cause of Science, 771852?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"RaijuTF\") ")
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"No\") ")
            fnCutsceneBlocker()
            
        --Doctor Maisie tells you what to do.
        elseif(iRaibieQuest == 5.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 6.0)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay doc, ready to dose me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I have se mutator serum right here.[SOFTBLOCK] However, we do not have anywhere near enough E-v89-r.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Which is good, because having you undergo the transformation in my office would likely destroy most of my furniture, and quite possibly se ranch itself.[SOFTBLOCK] Raibies exhibit vassly increased strengss and agility, and to pair sat with your combat skills...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] [SOUND|World|TakeItem]So, take sis serum, but do not use it here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But don't you need to observe me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se serum also has a set of my observational nanites.[SOFTBLOCK] Sey will transmit the data to Unit 2855's PDU, who will relay it to me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] She has already downloaded se necessary protocols.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Also, she says you are foolish for undertaking sis risk.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's her![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] You will need to return to whatever location you found se E-v89-r substance at, and dose yourself wis sat.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So the western part of the Gamma Laboratories?[SOFTBLOCK] Shouldn't be a problem.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Perhaps, perhaps not.[SOFTBLOCK] But, when the changes begin, you will need to transform yourself before you are overcome.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] And, unfortunately, we do not know se threshold.[SOFTBLOCK] Do not take a risk.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Are your instructions clear?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] As crystal.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Christine, if you do sis, you will be saving se lives of se infected Raiju population here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] For even trying, you are a hero.[SOFTBLOCK] Do not forget sat.[SOFTBLOCK] Be brave, not stupid.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ...[SOFTBLOCK] Brave is what I do best, doc.") ]])
            
        --Doctor Maisie tells you what to do.
        elseif(iRaibieQuest == 6.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Return to se location where you found se E-v89-r, and give yourself a good dose of sat material.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se data will be transmitted directly to me.[SOFTBLOCK] After sat, well, I must busy myself finding a cure.[SOFTBLOCK] Good luck, Christine.") ]])
        
        --After Raibies.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Christine, what did you experience while infected?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A total loss of self-control, and extreme anger.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Every little thing 55 did ticked me off so much that I just had to crush her...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I had to focus and think hard to do anything other than that.[SOFTBLOCK] If my attention wandered I would just start slashing at the walls.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Interesting, very interesting.[SOFTBLOCK] I hope se other Raijus in se labs will not attack us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Them attacking you is them doing their best.[SOFTBLOCK] Try confusing them, that will buy you a few seconds.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Oh, I will not be doing sis dangerous work![SOFTBLOCK] Se security units will.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I would do it myself, but I have...[SOFTBLOCK] misplaced...[SOFTBLOCK] my tranquiliser gun.[SOFTBLOCK] If I had it I would be able to administer se serum easily.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Oh well.[SOFTBLOCK] Beanbag rounds will suffice to stun sem, I hope.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good luck, doctor.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You've been very helpful.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Stay safe, my friend.") ]])
        
        end
        
    --Talking to 55:
    else
        
        --Display.
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
        
        --First time talking to the doc:
        if(iRaibieQuest == 0.0) then
            
            --Flag:
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 1.0)
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Oh, hello Unit 285 -[SOFTBLOCK] erm, 5. Unit 2855.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Apologies, your sister was just here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is no need to apologize.[SOFTBLOCK] Our appearances are similar.[SOFTBLOCK] I assure you, our personalities are not.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Indeed![SOFTBLOCK] Already I see a crucial difference.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What is that difference?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] It is a masser of decibels.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I understand.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Fursser, you are engaging wiss me and not simply barking orders, sen leaving.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] Perchance, is sis 'Christine' nearby?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] She's covering the entrance.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] I see.[SOFTBLOCK] Your sister volunteered her services to me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Without consulting her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Not surprising at all.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Doctor:[E|Neutral] It is not see first time she has done sat.[SOFTBLOCK] Neverseless, I have need of your aid.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I am doctor Maisie de Havilland of An-Nador.[SOFTBLOCK] University of Nador Nang, top of my class.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So the golems abducted you to work as their doctor?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Ah ha![SOFTBLOCK] Abducted?[SOFTBLOCK] No, sey contracted me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se medical advances I have made here will save countless lives planetside.[SOFTBLOCK] Se equipment is wissout peer, even if I do not understand how all of it works.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Already I have definitively proved se hyposesized existence of microbes causing many afflictions and even begun to synsesize vaccines -[SOFTBLOCK] it has been an incredible few years![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] But nevermind sat.[SOFTBLOCK] You and your team are capable fighters, and sat is what se Raiju Ranch is in need of.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please inform us how we can help.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] We always help those in need![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] [EMOTION|SX-399|Smirk]Sen let me lay it out as best as I can.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] We here in se biolabs are currently dealing wiss a most curious passogen of unidentified providence, and it afflicts one partirhuman species alone.[SOFTBLOCK] Doubtless you have seen sem.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are referring to the sickly green raijus.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Yes.[SOFTBLOCK] The staff here have taken to calling sem 'Raibies'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] That seems like a name Christine would come up with.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Actually, it was I who came up wiss it.[SOFTBLOCK] It is due to similarities between the frothing of a rabid mammal and the frothing on se raijus, though I cannot be certain if there is any genetic relation.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do you need us to exterminate them?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] No.[SOFTBLOCK] I am a doctor, not a butcher.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] 55![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Apologies, but what little I know of medical procedures involve quarantining dangerous specimens to prevent the pathogen's spread.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Wiping out the infected population would save the raijus here at the ranch.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] While grim, Unit 2855 is correct, my dear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] But can't we save them?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] That is what I intend to do.[SOFTBLOCK] Only in the absolutely worst-case scenario would liquidation be called for.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will only undertake that task if the doctor explicitly orders it, SX-399.[SOFTBLOCK] If we cannot save the infected, we must save the uninfected.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I get you.[SOFTBLOCK] I hate how often you're right about these things.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I do not take pleasure in it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] [EMOTION|2855|Neutral]So, we must find a cure.[SOFTBLOCK] And to find one, I require information.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sus far, I know nossing about se disease.[SOFTBLOCK] A passogen, a genetic trigger, viral or fungal?[SOFTBLOCK] Nossing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Confront one of se Raibies specimens in se Gamma Laboraties and extract a tissue sample from her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Tuft of fur?[SOFTBLOCK] Saliva?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] [SOUND|World|TakeItem]Sis sensor suite for your PDU will be able to perform se necessary work.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Hair samples, skin samples, saliva samples...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Blood samples...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It should be no difficulty to acquire seese once you have subdued one of se specimens.[SOFTBLOCK] Bring sem to me as soon as you do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] And you can make a cure from that sample?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] No, I cannot guarantee sat.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] But I assure you sat I will do what I can.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will do what we can as well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] This will be a piece of cake with Dr. de Havilland on our team.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There.[SOFTBLOCK] I have sent a mail to Christine's PDU informing her of your requirements.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is ultimately the one who decides what we do.[SOFTBLOCK] I leave the tactical choices to her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I cannot wait to meet her.[SOFTBLOCK] Now, I must prepare my equipment.[SOFTBLOCK] Good luck.") ]])
        
        --Repeats but hasn't gotten the samples
        elseif(iRaibieQuest == 1.0) then
        
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Be aware sat what little we do know about se Raibies indicates increased strengss and speed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Doubtless their electrical attacks will be even more potent san even the most excited Raiju.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will be ready for them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Please return to me once you have se samples, sen.[SOFTBLOCK] I will make sure all my equipment is ready.") ]])
        
        --Got the samples.
        elseif(iRaibieQuest == 2.0) then
            
            --Flag:
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 3.0)
            
            --Dialogue:
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Ah, se samples, excellent.[SOFTBLOCK] Your PDU already sent me a preview but se results were not conclusive.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will need a few moments...") ]])
            fnCutsceneBlocker()
        
            --Merge party.
            fnCutsceneWait(25)
            fnCutsceneMergeParty()
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("55", 56.25, 12.50)
            fnCutsceneFace("55", -1, 0)
            fnCutsceneMove("SX399",    56.25, 13.50)
            fnCutsceneFace("SX399",    -1, 0)
            fnCutsceneBlocker()
            
            --Door is not open:
            if(AL_GetProperty("Is Door Open", "MedicalDoor") == false) then
                fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
                fnCutsceneFace("DoctorMaisie", 0, -1)
                fnCutsceneBlocker()
                fnCutsceneWait(15)
                fnCutsceneBlocker()
                fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
                fnCutsceneInstruction([[ AL_SetProperty("Open Door", "MedicalDoor") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(15)
                fnCutsceneBlocker()
            else
                fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
            end
            fnCutsceneMove("DoctorMaisie", 55.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 8.50)
            fnCutsceneMove("DoctorMaisie", 56.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 55.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 54.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 57.25, 8.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 8.25)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneFace("DoctorMaisie", 0, 1)
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 57.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 55.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
            fnCutsceneFace("DoctorMaisie", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Please forgive my medical jargon, but we are, [SOFTBLOCK]how you say,[SOFTBLOCK] 'boned'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] 'Boned' is probably not a synonym of 'good'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I have identified a likely passogen and already synssesized a likely cure.[SOFTBLOCK] At least, according to the usual principles of retroviral engineering.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Bear in mind we have no medical knowledge.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will try to keep sis as simple as possible.[SOFTBLOCK] Hmmm...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se passogen is self-reactive.[SOFTBLOCK] It is capable of identifying threats and adapting to it.[SOFTBLOCK] This is unlike anysing seen in nature.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Indeed, such sings are impossible in nature, as se process of adaptation at sis scale requires evolutionary changes.[SOFTBLOCK] Yet, sis passogen is capable of it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The creatures invading the labs possess many such impossibilities.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Yes, and it is no coincidence that sey appeared at se same time as the Raibies became infected.[SOFTBLOCK] Still, proving the association was an important step.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So you made a cure, but it doesn't work?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It does.[SOFTBLOCK] Unfortunately, se passogen adapts and se cure becomes useless.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Can you make a cure that, I dunno, bypasses that?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sat is still possible, but I would need to know se original nature of the passogen.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] You see, sere were a dozen versions of it in the sample provided.[SOFTBLOCK] Sey had some common characteristics, but had already mutated semselves in response to se Raiju's immune system, and fursser in response to se cure.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I suspect sat sere is a 'prime' version of se passogen.[SOFTBLOCK] Finding sat would aid in the process of curing it immensely.[BLOCK][CLEAR]") ]])
            
            --Variables.
            local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
            if(iRaibieFoundDrums == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 4.0)
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I believe this substance we located in the Gamma Labs may be related, then.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Substance E-v89-r.[SOFTBLOCK] Odorless, colourless, virtually untraceable.[SOFTBLOCK] Dissolves in water, half-life sree hours...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Where did you find sis?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A raised section of the gamma laboratories, past the tissue testing center.[SOFTBLOCK] There were visible leaching marks in the nearby soil.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Yes, yes.[SOFTBLOCK] For, you see, sis material is not in se database.[SOFTBLOCK] At all.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Or it is classified.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Likely.[SOFTBLOCK] If classified it would not appear on my database queries.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Hmmm...[SOFTBLOCK] Sere are similarities, to be sure.[SOFTBLOCK] Sis substance would likely cause se aggression we observe, but would have a short duration.[SOFTBLOCK] Meant to be used as a combat stimulant, I would suspect...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Furser, it would be metabolized quickly in any living species.[SOFTBLOCK] Why has it affected se raijus alone?[SOFTBLOCK] Why has it become self-reactive?[SOFTBLOCK] Sese are serious questions.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Maybe they were using the raijus as test subjects.[SOFTBLOCK] Do you think we can cure them, still?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] ...[SOFTBLOCK] Yes, yes we can help sem.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] All I would need to do is observe an uninfected raiju undergo infection, with my special monitoring nanites.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Would you like us to deliberately infect a raiju for observation?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] 55![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Wait, no.[SOFTBLOCK] You've got something up your sleeve, don't you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Correct.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Christine.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] She goes raiju, infects herself, and then goes golem.[SOFTBLOCK] Bam, she's an immune robot, you get your data.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sat is correct, my friend.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will be able to observe se infection as it evolves, and simultaneously extract se information from her immune system's respones.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I would hypossisize sat a large influx of antibodies produced in a cloning machine, injected at once, would overwhelm se passogen and eliminate it before it self-reacts.[SOFTBLOCK] But I would need se first-response antibodies from a fresh infection.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Even if I am incorrect, Christine can transform herself and immediately be cured.[SOFTBLOCK] So sere is little danger.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is attendant risk if she cannot control herself, though.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It will be at her discretion to undertake that risk.[SOFTBLOCK] I will not speak for her.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Please send her a message and have her meet me here if she is willing.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have.[SOFTBLOCK] She has said she will discuss is with her tandem unit.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Which seems odd.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Becoming a raiju means banging one.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I can see why Sophie might object to that.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Is fornication with another unit not allowed?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] ...[SOFTBLOCK] Implicitly yes.[SOFTBLOCK] If you ask and she says it's okay, you're okay.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] I was not aware.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] (Wow, she really means that...)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will prepare se nanites used to observe the infection.[SOFTBLOCK] Good day, ladies.") ]])
                fnCutsceneBlocker()
            
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And if we can find the source of the pathogen?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It's possible, if any of se source material is still present, sat I could work wiss it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It would most likely be located somewhere in se gamma laboratories, since sat is where se raibies are located.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I may be able to do somesing if you can find it, yes.[SOFTBLOCK] Assuming it exists...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Then we will find it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Bring a scan from your PDU back for me if you do.[SOFTBLOCK] I will continue to work on sese samples...") ]])
                fnCutsceneBlocker()
            
            end
        
            --Movement.
            fnCutsceneMove("DoctorMaisie", 54.25, 12.50)
            fnCutsceneMove("SX399", 56.25, 12.50)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
        
        --Got the samples, need to find the source of the pathogen.
        elseif(iRaibieQuest == 3.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Any results on se source of se passogen?[BLOCK][CLEAR]") ]])
            
            --Variables.
            local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
            if(iRaibieFoundDrums == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 4.0)
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I believe this substance we located in the Gamma Labs may be related.[SOFTBLOCK] Please check it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Substance E-v89-r.[SOFTBLOCK] Odorless, colourless, virtually untraceable.[SOFTBLOCK] Dissolves in water, half-life sree hours...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Where did you find sis?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A raised section of the gamma laboratories, past the tissue testing center.[SOFTBLOCK] There were visible leaching marks in the nearby soil.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Yes, yes.[SOFTBLOCK] For, you see, sis material is not in se database.[SOFTBLOCK] At all.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Or it is classified.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Likely.[SOFTBLOCK] If classified it would not appear on my database queries.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Hmmm...[SOFTBLOCK] Sere are similarities, to be sure.[SOFTBLOCK] Sis substance would likely cause se aggression we observe, but would have a short duration.[SOFTBLOCK] Meant to be used as a combat stimulant, I would suspect...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Furser, it would be metabolized quickly in any living species.[SOFTBLOCK] Why has it affected se raijus alone?[SOFTBLOCK] Why has it become self-reactive?[SOFTBLOCK] Sese are serious questions.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Maybe they were using the raijus as test subjects.[SOFTBLOCK] Do you think we can cure them, still?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] ...[SOFTBLOCK] Yes, yes we can help sem.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] All I would need to do is observe an uninfected raiju undergo infection, with my special monitoring nanites.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Would you like us to deliberately infect a raiju for observation?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] 55![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Wait, no.[SOFTBLOCK] You've got something up your sleeve, don't you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Correct.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Christine.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] She goes raiju, infects herself, and then goes golem.[SOFTBLOCK] Bam, she's an immune robot, you get your data.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sat is correct, my friend.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will be able to observe se infection as it evolves, and simultaneously extract se information from her immune system's respones.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I would hypossisize sat a large influx of antibodies produced in a cloning machine, injected at once, would overwhelm se passogen and eliminate it before it self-reacts.[SOFTBLOCK] But I would need se first-response antibodies from a fresh infection.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Even if I am incorrect, Christine can transform herself and immediately be cured.[SOFTBLOCK] So sere is little danger.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is attendant risk if she cannot control herself, though.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It will be at her discretion to undertake that risk.[SOFTBLOCK] I will not speak for her.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Please send her a message and have her meet me here if she is willing.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have.[SOFTBLOCK] She has said she will discuss is with her tandem unit.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Which seems odd.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Becoming a raiju means banging one.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I can see why Sophie might object to that.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Is fornication with another unit not allowed?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] ...[SOFTBLOCK] Implicitly yes.[SOFTBLOCK] If you ask and she says it's okay, you're okay.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] I was not aware.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] (Wow, she really means that...)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I will prepare se nanites used to observe the infection.[SOFTBLOCK] Good day, ladies.") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not yet, doctor.[SOFTBLOCK] We're looking.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It will likely be somewhere in se gamma laboratories, likely in a storage medium.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Hmm...[SOFTBLOCK] Perhaps a leaking container?[SOFTBLOCK] Search carefully!") ]])
            end
            
        --Raiju TF time!
        elseif(iRaibieQuest == 4.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Please inform Unit 771852 to meet me here when she is ready to undergo se transformation.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I have found a raiju volunteer for her.[SOFTBLOCK] Before you ask, no, it was not hard.") ]])
            
        --Doctor Maisie tells you what to do.
        elseif(iRaibieQuest == 5.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 6.0)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What is the next step, doctor?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I have se mutator serum right here.[SOFTBLOCK] However, we do not have anywhere near enough E-v89-r.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Which is good, because having Christine undergo the transformation in my office would likely destroy most of my furniture, and quite possibly se ranch itself.[SOFTBLOCK] Raibies exhibit vassly increased strengss and agility, and to pair sat with your combat skills...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] [SOUND|World|TakeItem]So, take sis serum, but do not use it here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Understood.[SOFTBLOCK] Will this compromise the observation?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se serum also has a set of my observational nanites.[SOFTBLOCK] Sey will transmit the data to your PDU, who will relay it to me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se network is spotty but someone repaired se power issues in se gamma labs, so we should be able to maintain a connection.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Please download se protocols, Unit 2855.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Already done, doctor.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] You will need to return to whatever location you found se E-v89-r substance at, and dose Christine wis sat.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Not a problem.[SOFTBLOCK] It was in the gamma labs past the tissue research center.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] When se infection takes, she must transform herself when her immune system has reacted.[SOFTBLOCK] It is dangerous.[SOFTBLOCK] She must transform, quickly.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] We will make sure she knows.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Unit 2855 and SX-399, thank you.[SOFTBLOCK] If you do sis, you will be saving se lives of se infected raiju population here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] For even trying, you are a hero.[SOFTBLOCK] Do not forget sat.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Being heroes is what we do.[SOFTBLOCK] Let's go, 55.") ]])
            
        --Doctor Maisie tells you what to do.
        elseif(iRaibieQuest == 6.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Return to se location where you found se E-v89-r, and give Christine a good dose of sat material.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se data will be transmitted directly to me.[SOFTBLOCK] After sat, well, I must busy myself finding a cure.[SOFTBLOCK] Good luck, my friends.") ]])
        
        --After Raibies.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Unit 2855, may I ask why you have saved se raijus here from sere fate?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I did not.[SOFTBLOCK] I merely supported Christine and SX-399 while they did.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] She's so cute when she's humble.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Blush] Th-[SOFTBLOCK]thank you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] From what I understand, sough, you would not have done this before.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] What has changed you from se Head of Security to sis helpful, caring maverick?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Total memory loss.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] ...[SOFTBLOCK] Oh.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I wiped my memories to prevent a suspected computer virus from spreading.[SOFTBLOCK] Ironic, isn't it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] After that, I struggled to regain them.[SOFTBLOCK] When I saw who the old me was, I realized I did not want them back.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine has told me that I need something to believe in.[SOFTBLOCK] I have chosen to believe in the freedom from the systems that controlled me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The raibies were used as test subjects by an uncaring administration.[SOFTBLOCK] Tools to be used and discarded.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In every way, that was both me, and those I once suppressed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Even as an enforcer of the administration, you were its victim?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] Not as much a victim as the others, but still one.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I must come to terms with that.[SOFTBLOCK] I must make amends for what I did.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] To help others free themselves from that control is one way to do that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sis has given me cause to think.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I suspect my role will be to return to Pandemonium and take my medical knowledge wiss me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I am now doubting the Administration cares what happens to se people of Pandemonium.[SOFTBLOCK] Has my research been used to create this E-v89-r?[SOFTBLOCK] Just how innocent am I?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What matters is what you do now that you know, not what you did while you were lied to.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I believe in you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Some of those are things Christine has said before, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] But not all of them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] [SOFTBLOCK]I am learning.") ]])
        
        end

    end

--Raiju TF time!
elseif(sTopicName == "RaijuTF") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 5.0)
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Very well.[SOFTBLOCK] I have already found several Raijus who will happily induct you into sere ranks.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh but I think a threesome would blow out the PDUs![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] (Speaking of, I'll need to get those set up with rubber blockers...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Using the PDU in-built cameras?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Definition won't be as high as I'd like, but where are we going to find high-quality videograph cameras at a time like this?[SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Please, Christine.[SOFTBLOCK] Right sis way...") ]])
    fnCutsceneBlocker()
    
    --Raiju TF scene:
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_RaijuFirst/Scene_Begin.lua")

--No thanks.
elseif(sTopicName == "No") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

    --Common setup.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Very well. Speak to me again when you are prepared.") ]])
    fnCutsceneBlocker()

end