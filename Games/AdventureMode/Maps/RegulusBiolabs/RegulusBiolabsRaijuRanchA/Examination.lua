--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[55 Investigation]
--If 55 is investigating, then run a different script.
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
if(iChristineLeadingParty == 0.0) then
    LM_ExecuteScript(fnResolvePath() .. "Examination55.lua", sObjectName)
    return
end

--[Exits]
--[Objects]
if(sObjectName == "Sign") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Swimming is prohibited for non-Raijus while the Raijus are being bathed.[SOFTBLOCK] No exceptions.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] When you see a warning like that, you just know some lugnut was stupid enough to try it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Swimming while a Raiju is being bathed?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Oh no...[SOFTBLOCK] what kind of voltage would they be capable of?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] All I will note is that there are no fish or amphibians assigned to this body of water.[SOFTBLOCK] That's all the answer you need.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignDirections") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'North - Outdoor Videograph Theater'.[SOFTBLOCK] 'Some Moths Do!' is scribbled next to the message in red marker.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'East - Clinic, Human Dormitories, Athletics Tracks, Pools, Classrooms'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'West - Raiju Dormitories and Voltaic Capture Chambers'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'South - Chow Hall, Exit'.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's a log of all the Raijus here, as well as a voltage number next to their name.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] They must be tracking how much power they produce.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Can you imagine being under pressure to...[SOFTBLOCK] discharge...[SOFTBLOCK] as much as you can?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Wouldn't that take the fun out of it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I guess you just try not to think about it.[SOFTBLOCK] You'll do better if it comes naturally.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The console is covered in rubber insulation.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I wonder if someone was dumb enough to hook it into the voltaic capturing line.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The what now?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] There's a line that goes under this whole area that absorbs electromagnetic fields and converts them into electricity.[SOFTBLOCK] I don't know how it works, but I know it's there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] The line comes closest to the surface under these buildings since, well, you can guess why.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] And someone tried hooking the terminal's power supply to that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not only would that blow the capacitor out, it'd probably blow a hole in the wall.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] But it'd be a lot of fun to watch -[SOFTBLOCK] from a safe distance![SOFTBLOCK] Hee hee!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Today's special is tomato bisque, spinach dip and crackers, and berry-blast milkshakes.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Gotta get that nutriment to keep productive, right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] As long as an organic is well-fed, they won't complain.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Is that, uhh, a saying from Earth?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] It's lacking a certain something, I'd say![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Oooh![SOFTBLOCK] 'An empty stomach is filled with complaints'![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Top shelf, my dear![SOFTBLOCK] Well done!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Should we be peeking on the good doctor's computer?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I don't see why not.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Look, it's mostly patient check-ups.[SOFTBLOCK] A surgery here and there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You know, I always thought of us as being part of the city's industrial base.[SOFTBLOCK] But when you think about it, we're a lot closer to doctors than engineers.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] There's not as sharp a distinction on Regulus City.[SOFTBLOCK] But she's also a research doctor, isn't she?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, I don't think I have head for the life of a researcher.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] You're really smart, Christine![SOFTBLOCK] Don't sell yourself short![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks, Sophie, but when all this is over, we'll just go back to the repair bay.[SOFTBLOCK] That's the best place for us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It's best to find joy in the little things, isn't it?") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess this is the break room.[SOFTBLOCK] What's on the terminal?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] A videograph of a darkmatter playing with a string.[BLOCK][CLEAR]") ]])
    
    --Variables
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local iDarkmattersBad    = VM_GetVar("Root/Variables/Chapter5/Scenes/iDarkmattersBad", "N")
    
    --Skipped the Darkmatter confrontation.
    if(iDarkmattersBad == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] The Darkmatters are friends, right?[BLOCK][CLEAR]") ]])
        
        --Christine does not have Darkmatter form.
        if(iHasDarkmatterForm == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I've seen them up close.[SOFTBLOCK] They are not friends with Vivify and her ilk.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Maybe they just don't know how to help, or maybe they are just too busy.[SOFTBLOCK] Either way, I think we can trust them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I hope so, but nobody really knows.[SOFTBLOCK] They're everywhere, but we know nothing about them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] They were at the LRT facility, watching.[SOFTBLOCK] Maybe they're just observing for now...") ]])
    
        --Has Darkmatter form.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] They are.[SOFTBLOCK] You can trust them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's hard for them to communicate normally, but they're doing what they can to help.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Are they risking their lives?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Absolutely.[SOFTBLOCK] They look cute on the videographs, but they are capable warriors.[SOFTBLOCK] I have no fear if they are covering my back.") ]])
        end
    
    --Darkmatter confrontation happened.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I hope the Darkmatters can be reasoned with...[BLOCK][CLEAR]") ]])
        
        --Christine does not have Darkmatter form.
        if(iHasDarkmatterForm == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] They may not be our direct allies, but they still aren't friends of Vivify.[SOFTBLOCK] They're just being cautious.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] In the same circumstances, maybe I'd even agree with them...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Don't say that.[SOFTBLOCK] You're -[SOFTBLOCK] you had better win![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] You go give that meanie a good whooping or you'll have me to deal with![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Okay, okay![SOFTBLOCK] I'll go save the universe![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But if anyone asks, it's only because you said so.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] (She jokes, but I'm still worried...)") ]])
    
        --Has Darkmatter form.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I know I'll be able to reason with them.[SOFTBLOCK] They're not so different from us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] They just see it differently.[SOFTBLOCK] I'm a current ally, but a potential enemy.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Don't say that.[SOFTBLOCK] You don't get to lose un -[SOFTBLOCK] unless I say so![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] So if that big meanie does that singing thing again, you -[SOFTBLOCK] slap -[SOFTBLOCK] slap her![SOFTBLOCK] Yeah![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] (Slap her?[SOFTBLOCK] I am the biggest wuss in the galaxy...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Woah, Sophie![SOFTBLOCK] Slapping her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But I get what you're saying.[SOFTBLOCK] I'll talk to the Darkmatters.[SOFTBLOCK] I know we can work together again.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] (But...[SOFTBLOCK] I'm still worried...)") ]])
        end
    
    
    end
    
elseif(sObjectName == "TerminalF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'I'm stuck in her because I scraped my knee.[SOFTBLOCK] Sorry, I can't go play tennis with you until Doc discharges me.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] A scraped knee?[SOFTBLOCK] Humans are so delicate.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[SOFTBLOCK] Is that all there is to it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's another file here.[SOFTBLOCK] It's treatment recommendations.[SOFTBLOCK] Including blood sample extraction.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Sixteen blood sample extractions...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Sixteen?[SOFTBLOCK] Are you implying that our good doctor is anything but the most professional medical expert?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I'm not implying anything at all.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Though now that I think about it, I was once afraid of needles.[SOFTBLOCK] You know, those little things they use to inject fluids into organics?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I think something in my conversion changed that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Can you reprogram someone not to be afraid of something?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I...[SOFTBLOCK] think so...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You don't look too happy about that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] If you can remove my fears with reprogramming, could you add them?[SOFTBLOCK] How else could you change me?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I don't think it's something that would be permanent.[SOFTBLOCK] If a fear is due to a memory, removing that memory might help, but the fear remains.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I think fears are part of who we are.[SOFTBLOCK] If you were scared of heights, it would re-emerge later even if it was reprogrammed out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I think you're just not scared of needles anymore because you're a golem.[SOFTBLOCK] Golems aren't scared of needles![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yeah, that must be it.[SOFTBLOCK] We've already seen how some silly program couldn't change who I really am.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks, Sophie.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This computer was set to play music as the patient slept.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] What kind?[SOFTBLOCK] Industrial?[SOFTBLOCK] Synthwave?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Classical.[SOFTBLOCK] Synthesized sheet music from Pandemonium.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not familiar with any of the composers planetside, though.[SOFTBLOCK] Where I'm from, we had Vivaldi and Beethoven and - [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] and I suddenly feel just a little homesick...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I'm sorry...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's all right.[SOFTBLOCK] I've heard The Four Seasons so many times, I had it memorized.[SOFTBLOCK] I could easily recreate some of the sheet music.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Such a travesty that the people of this world haven't heard Earth's finest composers.[SOFTBLOCK] This must be rectified at once![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Please tell me the sole purpose of our struggle for liberty is your desire to spread the arts.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] If all else fails, yes.[SOFTBLOCK] Music belongs to everyone.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] That is how we know we are in the right.") ]])
    
elseif(sObjectName == "TerminalH") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Patient records...[SOFTBLOCK] Nothing too interesting here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Nothing too interesting?[SOFTBLOCK] Excuse me, are you all right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Look at who the most common patient in this room is.[SOFTBLOCK] Tell me if you notice anything.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 749332 has been in this room repeatedly.[SOFTBLOCK] She's three-quarters of the patient data.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Looks like she spends a day at a time here.[SOFTBLOCK] Scraps and scuffs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I bet she gets herself hurt doing athletics.[SOFTBLOCK] Humans need to exercise their muscles or they break down.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Ahhh...[SOFTBLOCK] No, Sophie.[SOFTBLOCK] I don't think so.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The girl who is always going to the medical room?[SOFTBLOCK] She's either sick constantly, which is probably not the case here...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Or?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She's the one who's always getting into fights.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] There's always one.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Goodness, what would humans have to fight over?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] They have everything they want here.[SOFTBLOCK] Why fight?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I think we can rule out bad homelife or physical abuse.[SOFTBLOCK] Which means, dominance.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Some people just have to be on top.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Gee, I think I know the type.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I'm not trying to patronize you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It starts early.[SOFTBLOCK] I just hope she grows out of it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] No, she'll get picked for Command Unit.[SOFTBLOCK] Obviously.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] That is how it always goes, isn't it?") ]])
    
elseif(sObjectName == "TerminalI") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm, looks like a log the good doctor left, with audio transcription.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Sis is yet anosser instance of my contract being knowingly violated by se administrators.[SOFTBLOCK] You are machines, yes?[SOFTBLOCK] Are you not aware of se stipulations?'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Se first violasion is the request to deliberately infect sis boy vith a flu virus, and sen to transfuse his blood to his twin brosser.[SOFTBLOCK] Are you insane?'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'I was also asked to amputate se limbs from one individual, and to attach prosssetics.[SOFTBLOCK] While research into prosssetics is a worssy goal, it cannot be done in such a gross violation of medical essics.[SOFTBLOCK] Find an existing amputee, do not create sem!'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'If you vish to discuss sis wiss me, you may find me in my office.[SOFTBLOCK] Make an appointment wiss my assistant, I have real experiments to conduct.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I think Doctor de Havilland is a good doctor.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Whatever differences we have, I can't argue that her heart isn't in the right place.[SOFTBLOCK] Good for her.") ]])
    
elseif(sObjectName == "BoxesA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This box is full of children's toys.[SOFTBLOCK] Dolls, miniature fabrication benches, and a little PDU.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Huh.[SOFTBLOCK] But all the humans in the breeding program look to be teenagers.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] They probably store them here until the next wave of humans is born, then distribute them.[SOFTBLOCK] Who knows how many children have played with these dolls?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I hadn't even considered that.[SOFTBLOCK] There must be more than one class of breeding humans at a time, right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Probably several groups harbored in the other labs.[SOFTBLOCK] I guess it's not considered worth it to transfer toys between the habitat areas.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Looking at these dolls...[SOFTBLOCK] I get a feeling of lost familiarity...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Maybe you played with dolls as a child, too?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Maybe.[SOFTBLOCK] I hope so.[SOFTBLOCK] I like dolls, they're cute and I'd love to cuddle with one.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You might get your wish sooner than you think...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A discarded copy of Mr. Needlemouse.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Someone threw away a copy of Mr. Needlemouse?[SOFTBLOCK] Why?[SOFTBLOCK] I heard it was good.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No no, this is the sequel.[SOFTBLOCK] It just uses the same name as the first one.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Why would you make another game with the exact same name as the first one?[SOFTBLOCK] That just makes it confusing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh, the name is the least of its worries.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I didn't know you play video games, Sophie.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] I -[SOFTBLOCK] I don't, really![SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But a lot of my friends do.[SOFTBLOCK] Should I get into them?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Certainly![SOFTBLOCK] There's all kinds of great picks for a beginner![SOFTBLOCK] We should play some together when we have time![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Uh, after the...[SOFTBLOCK] civil war...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Life goes on ever when the bullets start flying... [EMOTION|Christine|Happy]Ooh, I should show you Pandemoniumbound![SOFTBLOCK] It's quirky and irreverent and funny and lighthearted![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It's a date!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Spare medicines and medical tools.[SOFTBLOCK] Disposable gloves, tubing, syringes.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Position Recommendations'...[SOFTBLOCK] Pretty simple title.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] ...[SOFTBLOCK] Oh my.[SOFTBLOCK] Very to-the-point, isn't it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oh?[SOFTBLOCK] Let me see![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] What!?[SOFTBLOCK] Can an organic's spine bend like that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Look at this one![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Wouldn't the blood rush to their head when they're upside-down like that?[SOFTBLOCK] I heard that causes an organic to pass out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I think that might be the point, Sophie.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Christine, I am totally okay with boring and conventional stuff.[SOFTBLOCK] Don't feel like you need to try these things just to impress me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (What if I just want your face in my crotch with the help of a high-tension wire?[SOFTBLOCK] I think this book is giving me new fetishes...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Doggos and Pupperinos'.[SOFTBLOCK] It's a book full of cute dog pictures![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Adorable![SOFTBLOCK] But what's it doing here?[SOFTBLOCK] I thought this is where Raijus...[SOFTBLOCK] you know...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The section here says 'overcharge aids'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Maybe they look at puppies to calm down if they're getting too frisky?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] If you wanted to get them out of the mood, just show them something gross.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Like tentacles?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'm sure somebody is into those, Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Just not me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Look![SOFTBLOCK] A husky![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Awwwwwwww![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Maybe these are just here to cheer them up if something goes wrong.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Oh no...[SOFTBLOCK] do Raijus have...[SOFTBLOCK] dysfunctions?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] And you can't just swap out the defective parts...[SOFTBLOCK] Being organic must be so hard...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] 'Arcane Supercondivity::[SOFTBLOCK] A Theory'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, this is a little above my pay grade.[SOFTBLOCK] I don't know much about superconductivity.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'm sure with the proper documentation we could fix...[SOFTBLOCK] whatever it is this book is describing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I think it's theorizing about how Raijus can generate so much electricity without exploding.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Of course![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Maybe one of the Raijus borrowed it to better understand herself?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] See![SOFTBLOCK] They're not all airheads![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Great![SOFTBLOCK] I hope she learns so much that she becomes a Raiju Researcher![SOFTBLOCK] The first of her kind![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But, maybe better if the Raijus don't become repair units.[SOFTBLOCK] I can't see that ending well...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] 'How To Get Her Right In The - '[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Is that the whole title?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Y-[SOFTBLOCK]yep![SOFTBLOCK] Whole title, right there.[SOFTBLOCK] Putting this book down!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "CoffeeMaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An oilmaker modified to grind coffee beans.[SOFTBLOCK] Has very little effect on golems, but probably gives organics quite a kick.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BarrelYellow") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This barrel smells strongly of alcohol.[SOFTBLOCK] It's been a while since I drank any, but I think this is a brandy of some sort.[SOFTBLOCK] Potent!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BarrelBlue") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This barrel is full of water.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The RVD is displaying a list of all the delicious things organics can order here.[SOFTBLOCK] There's over forty different vegetable platters alone![SOFTBLOCK] Meat, however, is absent from the menu.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Kitchen") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A full kitchen set, which is a real rarity on Regulus.[SOFTBLOCK] Everything is steel, fiber, or non-stick.[SOFTBLOCK] None of that cast-iron nonsense you find planetside!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Lunchbox") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A lunchbox with a thermos full of potato soup.[SOFTBLOCK] I guess someone's plans got cancelled.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computer") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Medical records.[SOFTBLOCK] The login is for the doctor's assistant.[SOFTBLOCK] Apparently, the doctor isn't very good with computers, and needs someone to keep her records in order.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Medicine") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (All kinds of organic medicines and disinfectants.[SOFTBLOCK] Common aspirin, pseudophedrine, penoxycyline, morphine, palimerfin...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Cooler") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A portable cooler to keep drinks cold.[SOFTBLOCK] There's several jars full of lemonade in here.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end