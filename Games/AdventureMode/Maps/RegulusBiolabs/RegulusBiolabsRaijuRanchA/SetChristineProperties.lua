--[Set Christine's Properties]
--A script that sets 55's properties for collision/activation.
EM_PushEntity("Christine")
    TA_SetProperty("Clipping Flag", true)
    TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
DL_PopActiveObject()