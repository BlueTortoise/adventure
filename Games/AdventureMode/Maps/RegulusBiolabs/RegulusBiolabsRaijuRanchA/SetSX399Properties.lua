--[Set SX399's Properties]
--A script that sets SX399's properties for collision/activation.
EM_PushEntity("SX399")
    TA_SetProperty("Clipping Flag", true)
    TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
DL_PopActiveObject()