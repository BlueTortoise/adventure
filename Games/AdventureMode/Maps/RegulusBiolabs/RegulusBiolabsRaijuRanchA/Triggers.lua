--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Triggers]
if(sObjectName == "Entrance") then
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local iSawRaijuIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
    local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
    if(iSawRaijuIntro == 0.0) then
        
        --Topics for 55.
        local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
        if(iResolvedCassandraQuest >= 0.0) then
            WD_SetProperty("Unlock Topic", "Biolabs_Cassandra", 1)
        end
        WD_SetProperty("Unlock Topic", "Biolabs_Monsters", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Processors", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Rebellion", 1)
        
        --Topics for SX-399.
        WD_SetProperty("Unlock Topic", "Biolabs_Latex", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Mercenaries", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Past", 1)
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieLeftInBiolabs", "N", 1.0)
        
        --Spawn these humans.
        TA_Create("SecurityHumieA")
            TA_SetProperty("Position", 51, 26)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/GenericBiolabsFRed/", false)
        DL_PopActiveObject()
        TA_Create("SecurityHumieB")
            TA_SetProperty("Position", 52, 26)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/GenericBiolabsMRed/", false)
        DL_PopActiveObject()
        
        --If SX-399 is present, she doesn't offer to explore the ranch with 55.
        if(bIsSX399Present == false) then
            
            --Movement.
            fnCutsceneMove("Christine", 51.25, 35.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("55", 50.25, 35.50)
            fnCutsceneFace("55", 0, -1)
            fnCutsceneMove("Sophie", 52.25, 35.50)
            fnCutsceneFace("Sophie", 0, -1)
            fnCutsceneBlocker()
            
            --Humies move down.
            fnCutsceneMove("SecurityHumieA", 51.25, 32.50)
            fnCutsceneFace("SecurityHumieA", -1, 0)
            fnCutsceneMove("SecurityHumieB", 52.25, 33.50)
            fnCutsceneMove("SecurityHumieB", 51.25, 33.50)
            fnCutsceneFace("SecurityHumieB", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLordB] Report.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanM0] No movement on the east side.[SOFTBLOCK] Continuing our patrols.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLordB] They're not making a move.[SOFTBLOCK] Yet.[SOFTBLOCK] Continue your patrols.[SOFTBLOCK] Dismissed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanM0] Yes, Lord Golem.") ]])
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneMove("SecurityHumieA", 51.25, 23.50)
            fnCutsceneMove("SecurityHumieB", 51.25, 24.50)
            fnCutsceneMove("GolemA", 51.25, 32.50)
            fnCutsceneMove("GolemA", 51.25, 34.50)
            fnCutsceneBlocker()
            fnCutsceneTeleport("SecurityHumieA", -100, -100)
            fnCutsceneTeleport("SecurityHumieB", -100, -100)
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] As expected.[SOFTBLOCK] Greetings.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] There is no need to explain your situation, Lord Unit 771852.[SOFTBLOCK] I have been informed by the Head of Research.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You may refer to me as Ms. Primrose.[SOFTBLOCK] I am the headmaster and administrator of this habitation area.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hello, Ms. Primrose.[SOFTBLOCK] I suppose introductions are not necessary?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Correct.[SOFTBLOCK] I know who your retinue are.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Before you get the wrong impression, I am obligated to make one thing clear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I run a tight operation, and I am aware of your affiliations.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] While I have been instructed to accomodate you, I will also not tolerate rebel activities.[SOFTBLOCK] You will not sabotage anything here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Because I am a fair-minded machine, I will allow you the full run of the facilities.[SOFTBLOCK] Betray my trust and I promise you, you will regret it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The fact that we can converse as equals, despite being on opposite sides, is...[SOFTBLOCK] refreshing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Apparently it's too much to ask that from...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] The Head of Research.[SOFTBLOCK] I am aware.[SOFTBLOCK] I do not care for her company either.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] She informed me of the situation, shall we say, rather curtly.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unsurprising.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I will get you up to speed, then.[SOFTBLOCK] I know this area better than you do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Shortly after we heard the reverberations from the explosions in the city, creatures began to appear.[SOFTBLOCK] We have not ascertained their origin point, but I can guess it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The Epsilon Laboratories.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] That was my deduction.[SOFTBLOCK] Neither I, nor any of my staff, were involved with the research there.[SOFTBLOCK] I'm sorry but we cannot provide any clues.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I have been informed that the garrison at the security station there was lost.[SOFTBLOCK] What is left is at this facility, guarding us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] However, despite having claimed most of the labs, they have stopped advancing.[SOFTBLOCK] We do not know why.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Care to enlighten me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Are you sure you know, Christine?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I am completely certain, Sophie.[SOFTBLOCK] I heard her calling.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ms. Primrose, their -[SOFTBLOCK] ah, leader, I suppose -[SOFTBLOCK] has summoned several of them back to her for a discussion.[SOFTBLOCK] They aren't supposed to be...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Attacking us?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sarru Lamadu...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Uh, yes.[SOFTBLOCK] They aren't.[SOFTBLOCK] Not until the meeting.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But I can't say how long that will last.[SOFTBLOCK] There is disagreement among -[SOFTBLOCK] us...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Us?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Those who can hear it, Sophie.[SOFTBLOCK] I'm not like them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You will need to guard the perimeter.[SOFTBLOCK] Keep everyone away from them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] So, do as you were before.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Lovely.[SOFTBLOCK] What a productive meeting.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You will need breaching tools, I have been informed.[SOFTBLOCK] There were some hydraulic pliers and a welder in the back.[SOFTBLOCK] That should be enough.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] To take apart a door?[SOFTBLOCK] Likely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ms. Primrose, I have a favour to ask of you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Hmm?[SOFTBLOCK] A favour despite having just met me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is my tandem unit, Sophie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hello, Ms. Primrose.[SOFTBLOCK] Some of my friends work at the ranch here, I've heard about you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You're going to ask me to look after her, aren't you.[SOFTBLOCK] She is not carrying a weapon.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is that too much?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I can guarantee she will be as safe as the rest of us, but no more than that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] If we come under a full assault, we will not hold.[SOFTBLOCK] The numbers are simply not on our side.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] True, but she won't be safer with me, either.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But I must have your word that you will not allow Unit 2856 to hold her as hostage.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Interesting.[SOFTBLOCK] Why would I do that?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Besides my own personal animosity towards Unit 2856, that is.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] When the rebellion comes through, I can promise - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I am not siding with you, nor are any of my personnel.[SOFTBLOCK] That I assure you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dash that, then...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What can we do for you to gain your cooperation?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] ...[SOFTBLOCK] I care deeply for those who live here.[SOFTBLOCK] They are my friends, my family, and more.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You have seen the Raijus in the Gamma Laboratories, yes?[SOFTBLOCK] The ones frothing at the mouth, attacking whatever they see?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] We have...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Save them.[SOFTBLOCK] If you can't, then at least try.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Doctor Maisie is at the clinic just north of here.[SOFTBLOCK] She's been doing what she can.[SOFTBLOCK] I'm sure you can help her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] There are doubtless many more things you can do to help us in the labs.[SOFTBLOCK] I suppose an unofficial cease-fire is a good enough time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] The Biological Services dorms are southeast of here.[SOFTBLOCK] We haven't had contact with them since the crisis.[SOFTBLOCK] See to them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] And, there may be stragglers in the Aquatic Genetics labs far to the southwest.[SOFTBLOCK] We can't get distress signals with the network down.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Promise to help, and I will keep your tandem unit from Unit 2856's clutches.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] An easy promise to make.[SOFTBLOCK] We'll help as much as we can.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Hmm.[SOFTBLOCK] At this juncture, that promise is already far more than the Administration has deigned to give.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You have my attention, 771852.[SOFTBLOCK] Do not waste it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] All right team.[SOFTBLOCK] Shall we?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If it is all right with you, Christine, I would like to remain here with Ms. Primrose.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I am a capable security unit.[SOFTBLOCK] I may be able to improve your protocols.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Why should I trust you to not sabotage my arrangements?[SOFTBLOCK] You have made promises and little else.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You may review all of my recommendations before implementing them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Acceptable.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Also, my two associates must be free to converse.[SOFTBLOCK] I do not wish to inconvenience them.[SOFTBLOCK] So I am finding an excuse.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] 55![SOFTBLOCK] You are our friend![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] We love having you with us![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] True, but you do behave differently when I am not present.[SOFTBLOCK] I merely wish to allow you to do that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I am simply not particularly skilled at the art of disguising this intent.[SOFTBLOCK] Better to state it bluntly.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I suppose honesty is appreciated...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk][EMOTION|Sophie|Neutral] She's just trying to be nice, Sophie.[SOFTBLOCK] I guess we shouldn't stop her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] When you leave, I will accompany you.[SOFTBLOCK] Unit 499323 will remain here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Please locate the breaching tools.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're on it.[SOFTBLOCK] Sophie, shall we?") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --If Christine is not a human or golem, transform her here.
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
            if(sChristineForm ~= "Golem" and sChristineForm ~= "Human") then

                --Flash the active character to white. Immediately after, execute the transformation.
                Cutscene_CreateEvent("Flash Christine White", "Actor")
                    ActorEvent_SetProperty("Subject Name", "Christine")
                    ActorEvent_SetProperty("Flashwhite")
                DL_PopActiveObject()
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
                fnCutsceneWait(gci_Flashwhite_Ticks_Total)
                fnCutsceneBlocker()

                --Now wait a little bit.
                fnCutsceneWait(25)
                fnCutsceneBlocker()
            end
            
            --Movement.
            fnCutsceneMove("GolemA", 49.25, 32.50)
            fnCutsceneBlocker()
            fnCutsceneMove("55", 50.25, 32.50)
            fnCutsceneFace("55", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 1, 0)
            fnCutsceneBlocker()
            
            --Move Sophie onto Christine. Remove 55 from lineup.
            AL_SetProperty("Unfollow Actor Name", "55")
            EM_PushEntity("Sophie")
                local iCharacterID = RE_GetID()
            DL_PopActiveObject()
            gsaFollowerNames = {"Sophie"}
            giaFollowerIDs   = {iCharacterID}
            fnCutsceneMove("Sophie", 51.25, 35.50)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
            --Set collision flags and dialogue flags for the departing characters.
            local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "Set55Properties.lua\")"
            fnCutsceneInstruction(sString)
            
        else
            
            --Movement.
            fnCutsceneMove("Christine", 51.25, 35.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("55", 50.25, 35.50)
            fnCutsceneFace("55", 0, -1)
            fnCutsceneMove("Sophie", 52.25, 35.50)
            fnCutsceneFace("Sophie", 0, -1)
            fnCutsceneMove("SX399", 50.25, 36.50)
            fnCutsceneFace("SX399", 0, -1)
            fnCutsceneBlocker()
            
            --Humies move down.
            fnCutsceneMove("SecurityHumieA", 51.25, 32.50)
            fnCutsceneFace("SecurityHumieA", -1, 0)
            fnCutsceneMove("SecurityHumieB", 52.25, 33.50)
            fnCutsceneMove("SecurityHumieB", 51.25, 33.50)
            fnCutsceneFace("SecurityHumieB", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLordB] Report.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanM0] No movement on the east side.[SOFTBLOCK] Continuing our patrols.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLordB] They're not making a move.[SOFTBLOCK] Yet.[SOFTBLOCK] Continue your patrols.[SOFTBLOCK] Dismissed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanM0] Yes, Lord Golem.") ]])
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneMove("SecurityHumieA", 51.25, 23.50)
            fnCutsceneMove("SecurityHumieB", 51.25, 24.50)
            fnCutsceneMove("GolemA", 51.25, 32.50)
            fnCutsceneMove("GolemA", 51.25, 34.50)
            fnCutsceneBlocker()
            fnCutsceneTeleport("SecurityHumieA", -100, -100)
            fnCutsceneTeleport("SecurityHumieB", -100, -100)
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] As expected.[SOFTBLOCK] Greetings.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] There is no need to explain your situation, Lord Unit 771852.[SOFTBLOCK] I have been informed by the Head of Research.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You may refer to me as Ms. Primrose.[SOFTBLOCK] I am the headmaster and administrator of this habitation area.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hello, Ms. Primrose.[SOFTBLOCK] I suppose introductions are not necessary?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Correct.[SOFTBLOCK] I know who your retinue are.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Retinue?[SOFTBLOCK] Fancy way of saying gang.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Quite right![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral][EMOTION|Christine|Smirk] Ahem.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Before you get the wrong impression, I am obligated to make one thing clear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I run a tight operation, and I am aware of your affiliations.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] While I have been instructed to accomodate you, I will also not tolerate rebel activities.[SOFTBLOCK] You will not sabotage anything here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Because I am a fair-minded machine, I will allow you the full run of the facilities.[SOFTBLOCK] Betray my trust and I promise you, you will regret it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The fact that we can converse as equals, despite being on opposite sides, is...[SOFTBLOCK] refreshing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Yeah I think she knows who we're talking about.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] The Head of Research.[SOFTBLOCK] I am aware.[SOFTBLOCK] I do not care for her company either.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I like you already.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Her description of the situation was, shall we say, curt.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] And she is something of a curt.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] You were off by one letter.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I will get you up to speed, then.[SOFTBLOCK] I know this area better than you do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Shortly after we heard the reverberations from the explosions in the city, creatures began to appear.[SOFTBLOCK] We have not ascertained their origin point, but I can guess it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The Epsilon Laboratories.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] That was my deduction.[SOFTBLOCK] Neither I, nor any of my staff, were involved with the research there.[SOFTBLOCK] I'm sorry but we cannot provide any clues.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I have been informed that the garrison at the security station there was lost.[SOFTBLOCK] What is left is at this facility, guarding us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] However, despite having claimed most of the labs, they have stopped advancing.[SOFTBLOCK] We do not know why.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Care to enlighten me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Are you sure you know, Christine?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I am completely certain, Sophie.[SOFTBLOCK] I heard her calling.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ms. Primrose, their -[SOFTBLOCK] ah, leader, I suppose -[SOFTBLOCK] has summoned several of them back to her for a discussion.[SOFTBLOCK] They aren't supposed to be...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Attacking us?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sarru Lamadu...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Uh, yes.[SOFTBLOCK] They aren't.[SOFTBLOCK] Not until the meeting.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But I can't say how long that will last.[SOFTBLOCK] There is disagreement among -[SOFTBLOCK] us...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Us?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Those who can hear it, Sophie.[SOFTBLOCK] I'm not like them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You will need to guard the perimeter.[SOFTBLOCK] Keep everyone away from them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] So, do as you were before.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Lovely.[SOFTBLOCK] What a productive meeting.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] But now the cavalry has arrived, so you can rest easy.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You will need breaching tools, I have been informed.[SOFTBLOCK] There were some hydraulic pliers and a welder in the back.[SOFTBLOCK] That should be enough.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] *I maintain that I could just melt the door...*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] To take apart a door?[SOFTBLOCK] *Without exposing anyone to a vacuum?* Yes, those tools will work..[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Bah.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ms. Primrose, I have a favour to ask of you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Hmm?[SOFTBLOCK] A favour despite having just met me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is my tandem unit, Sophie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hello, Ms. Primrose.[SOFTBLOCK] Some of my friends work at the ranch here, I've heard about you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You're going to ask me to look after her, aren't you.[SOFTBLOCK] She is not carrying a weapon.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is that too much?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I can guarantee she will be as safe as the rest of us, but no more than that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] If we come under a full assault, we will not hold.[SOFTBLOCK] The numbers are simply not on our side.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] True, but she won't be safer with me, either.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But I must have your word that you will not allow Unit 2856 to hold her as hostage.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Interesting.[SOFTBLOCK] Why would I do that?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Besides my own personal animosity towards Unit 2856, that is.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] When the rebellion comes through, I can promise - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I am not siding with you, nor are any of my personnel.[SOFTBLOCK] That I assure you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dash that, then...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What can we do for you to gain your cooperation?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] ...[SOFTBLOCK] I care deeply for those who live here.[SOFTBLOCK] They are my friends, my family, and more.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You have seen the Raijus in the Gamma Laboratories, yes?[SOFTBLOCK] The ones frothing at the mouth, attacking whatever they see?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] We have...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Save them.[SOFTBLOCK] If you can't, then at least try.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Doctor Maisie is at the clinic just north of here.[SOFTBLOCK] She's been doing what she can.[SOFTBLOCK] I'm sure you can help her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] There are doubtless many more things you can do to help us in the labs.[SOFTBLOCK] I suppose an unofficial cease-fire is a good enough time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] The Biological Services dorms are southeast of here.[SOFTBLOCK] We haven't had contact with them since the crisis.[SOFTBLOCK] See to them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] And, there may be stragglers in the Aquatic Genetics labs far to the southwest.[SOFTBLOCK] We can't get distress signals with the network down.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Promise to help, and I will keep your tandem unit from Unit 2856's clutches.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] An easy promise to make.[SOFTBLOCK] We'll help as much as we can.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Hmm.[SOFTBLOCK] At this juncture, that promise is already far more than the Administration has deigned to give.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] You have my attention, 771852.[SOFTBLOCK] Do not waste it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] All right team.[SOFTBLOCK] Shall we?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If it is all right with you, Christine, I would like to remain here with Ms. Primrose.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I am a capable security unit.[SOFTBLOCK] I may be able to improve your protocols.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Why should I trust you to not sabotage my arrangements?[SOFTBLOCK] You have made promises and little else.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You may review all of my recommendations before implementing them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Acceptable.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Also, my two associates must be free to converse.[SOFTBLOCK] I do not wish to inconvenience them.[SOFTBLOCK] So I am finding an excuse.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] 55![SOFTBLOCK] You are our friend![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] We love having you with us![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] True, but you do behave differently when I am not present.[SOFTBLOCK] I merely wish to allow you to do that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I am simply not particularly skilled at the art of disguising this intent.[SOFTBLOCK] Better to state it bluntly.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I suppose honesty is appreciated...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk][EMOTION|Sophie|Neutral] She's just trying to be nice, Sophie.[SOFTBLOCK] I guess we shouldn't stop her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] So that's how you're playing it?[SOFTBLOCK] All right.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] I'll be staying here as well.[SOFTBLOCK] You know, to watch the entrance.[SOFTBLOCK] That's a chokepoint, right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is no need to be coy.[SOFTBLOCK] I appreciate your company.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] And will be using the opportunity to work on your discipline.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] (Heavens, she has no idea what she just said!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Christine, when you leave, we will accompany you.[SOFTBLOCK] Unit 499323 will remain here at the ranch while we are out.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Please locate the breaching tools.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're on it.[SOFTBLOCK] Sophie, shall we?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I have one final request.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] I am aware of your capabilities.[SOFTBLOCK] However, the residents here are not.[SOFTBLOCK] Please do not frighten them unnecessarily with your...[SOFTBLOCK] forms.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I suppose that's fair.[SOFTBLOCK] Which of my forms should I take?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Primrose:[E|Neutral] Human or golem, those are acceptable.[SOFTBLOCK] Any others might frighten my staff.[SOFTBLOCK] Let's keep things quiet.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (There's a spot just outside that I can change forms at anyway.)") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --If Christine is not a human or golem, transform her here.
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
            if(sChristineForm ~= "Golem" and sChristineForm ~= "Human") then

                --Flash the active character to white. Immediately after, execute the transformation.
                Cutscene_CreateEvent("Flash Christine White", "Actor")
                    ActorEvent_SetProperty("Subject Name", "Christine")
                    ActorEvent_SetProperty("Flashwhite")
                DL_PopActiveObject()
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
                fnCutsceneWait(gci_Flashwhite_Ticks_Total)
                fnCutsceneBlocker()

                --Now wait a little bit.
                fnCutsceneWait(25)
                fnCutsceneBlocker()
            end
            
            --Movement.
            fnCutsceneMove("GolemA", 49.25, 32.50)
            fnCutsceneBlocker()
            fnCutsceneMove("55", 50.25, 32.50)
            fnCutsceneFace("55", -1, 0)
            fnCutsceneMove("SX399", 50.25, 33.50)
            fnCutsceneFace("SX399", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 1, 0)
            fnCutsceneBlocker()
            
            --Move Sophie onto Christine. Remove 55 and SX-399 from lineup.
            AL_SetProperty("Unfollow Actor Name", "55")
            AL_SetProperty("Unfollow Actor Name", "SX399")
            EM_PushEntity("Sophie")
                local iCharacterID = RE_GetID()
            DL_PopActiveObject()
            gsFollowersTotal = 1
            gsaFollowerNames = {"Sophie"}
            giaFollowerIDs   = {iCharacterID}
            fnCutsceneMove("Sophie", 51.25, 35.50)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
            --Set collision flags and dialogue flags for the departing characters.
            local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "Set55Properties.lua\")"
            fnCutsceneInstruction(sString)
            sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SetSX399Properties.lua\")"
            fnCutsceneInstruction(sString)
            
            --Indicate that 55 can swap leadership with Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 2.0)
            
        end
    
    --Already seen the cutscene. Split the party when re-entering the ranch.
    elseif(iSawRaijuIntro == 1.0 and iChristineLeadingParty == 3.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will resume our positions here.[SOFTBLOCK] Let us know if you want to switch.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will resume my positions here.[SOFTBLOCK] I will accompany you when you leave.") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Move 55 and SX-399 over.
        if(bIsSX399Present == true) then
            fnCutsceneMove("55", 51.25, 35.50)
            fnCutsceneMove("55", 47.25, 35.50)
            fnCutsceneMove("55", 47.25, 36.50)
            fnCutsceneMove("SX399", 51.25, 35.50)
            fnCutsceneMove("SX399", 46.25, 35.50)
            fnCutsceneMove("SX399", 46.25, 36.50)
            fnCutsceneBlocker()
            
            --Remove 55 and SX-399 from lineup.
            AL_SetProperty("Unfollow Actor Name", "55")
            AL_SetProperty("Unfollow Actor Name", "SX399")
            
            --Add Sophie to the follower list.
            EM_PushEntity("Sophie")
                local iCharacterID = RE_GetID()
            DL_PopActiveObject()
            gsFollowersTotal = 1
            gsaFollowerNames = {"Sophie"}
            giaFollowerIDs   = {iCharacterID}
            AL_SetProperty("Follow Actor ID", iCharacterID)
            
            --Move Christine to Sophie.
            fnCutsceneMove("Christine", 51.25, 34.50)
            fnCutsceneFace("Christine", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneFace("Sophie", -1, 0)
            fnCutsceneBlocker()
                
            --Set collision flags and dialogue flags for the departing characters.
            local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "Set55Properties.lua\")"
            fnCutsceneInstruction(sString)
            sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SetSX399Properties.lua\")"
            fnCutsceneInstruction(sString)
            
            --Indicate that 55 can swap leadership with Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 2.0)
            local iHasRaijuForm = VM_GetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N")
            
            --If Christine is not a human or golem, transform her here.
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
            if(sChristineForm ~= "Golem" and sChristineForm ~= "Human" and sChristineForm ~= "Raiju") then
                
                --Dialogue.
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Which form should I take?)[BLOCK]") ]])

                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"ToGolem\") ")
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Human\",  " .. sDecisionScript .. ", \"ToHuman\") ")
                if(iHasRaijuForm == 1.0) then
                    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Raiju\",  " .. sDecisionScript .. ", \"ToRaiju\") ")
                end
                fnCutsceneBlocker()
            
            --Already a valid form. Fold the party.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Shall we, dearest?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] After you, dearest.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
            
                --Move, fold.
                fnCutsceneMove("Sophie", 51.25, 34.50)
                fnCutsceneBlocker()
                fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
                fnCutsceneBlocker()
            
            end
            
        --Move just 55 over.
        else
            fnCutsceneMove("55", 51.25, 35.50)
            fnCutsceneMove("55", 47.25, 35.50)
            fnCutsceneMove("55", 47.25, 36.50)
            fnCutsceneBlocker()
            
            --Remove 55 from the lineup.
            AL_SetProperty("Unfollow Actor Name", "55")
            
            --Add Sophie to the follower list.
            EM_PushEntity("Sophie")
                local iCharacterID = RE_GetID()
            DL_PopActiveObject()
            gsFollowersTotal = 1
            gsaFollowerNames = {"Sophie"}
            giaFollowerIDs   = {iCharacterID}
            AL_SetProperty("Follow Actor ID", iCharacterID)
            
            --Move Christine to Sophie.
            fnCutsceneMove("Christine", 51.25, 34.50)
            fnCutsceneFace("Christine", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneFace("Sophie", -1, 0)
            fnCutsceneBlocker()
                
            --Set collision flags and dialogue flags for the departing characters.
            local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "Set55Properties.lua\")"
            fnCutsceneInstruction(sString)
            
            --Indicate that 55 can swap leadership with Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 1.0)
            local iHasRaijuForm = VM_GetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N")
            
            --If Christine is not a human or golem, transform her here.
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
            if(sChristineForm ~= "Golem" and sChristineForm ~= "Human" and sChristineForm ~= "Raiju") then
                
                --Dialogue.
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Which form should I take?)[BLOCK]") ]])

                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"ToGolem\") ")
                fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Human\", " .. sDecisionScript .. ", \"ToHuman\") ")
                if(iHasRaijuForm == 1.0) then
                    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Raiju\",  " .. sDecisionScript .. ", \"ToRaiju\") ")
                end
                fnCutsceneBlocker()
            
            --Already a valid form. Fold the party.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Shall we, dearest?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] After you, dearest.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
            
                --Move, fold.
                fnCutsceneMove("Sophie", 51.25, 34.50)
                fnCutsceneBlocker()
                fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
                fnCutsceneBlocker()
            
            end
            
        end
    end

--Change Christine's form to Human.
elseif(sObjectName == "ToHuman") then
	WD_SetProperty("Hide")

    --Flash the active character to white. Immediately after, execute the transformation.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite")
    DL_PopActiveObject()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total)
    fnCutsceneBlocker()

    --Now wait a little bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You are looking lovely as ever in your organic skin, dearest.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Do you like the color?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I do![SOFTBLOCK] A splendid shade.[SOFTBLOCK] Shall we?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Move, fold.
    fnCutsceneMove("Sophie", 51.25, 34.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

--Change Christine's form to Golem.
elseif(sObjectName == "ToGolem") then
	WD_SetProperty("Hide")

    --Flash the active character to white. Immediately after, execute the transformation.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite")
    DL_PopActiveObject()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total)
    fnCutsceneBlocker()

    --Now wait a little bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Simply a marvel.[SOFTBLOCK] Even claws and gunshots can't dent your metal skin.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If you see a scuff, you have my permission to kiss it better.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] An advanced repair unit technique we only teach to the most...[SOFTBLOCK] sensual...[SOFTBLOCK] of units.[SOFTBLOCK] Shall we?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Move, fold.
    fnCutsceneMove("Sophie", 51.25, 34.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

--Change Christine's form to Raiju.
elseif(sObjectName == "ToRaiju") then
	WD_SetProperty("Hide")

    --Flash the active character to white. Immediately after, execute the transformation.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite")
    DL_PopActiveObject()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total)
    fnCutsceneBlocker()

    --Now wait a little bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Beautiful as always...[SOFTBLOCK][EMOTION|Sophie|Surprised] Yikes![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Careful![SOFTBLOCK] I just might zap you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] So long as it is my privilege alone![SOFTBLOCK] Shall we?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Move, fold.
    fnCutsceneMove("Sophie", 51.25, 34.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

--Post-Raibie Scene
elseif(sObjectName == "Raibies") then

    --Variables.
    local iRaibieQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
    if(iRaibieQuest ~= 8.0) then return end
    
    --Variables.
    local iSX399JoinsParty  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Speaker is Christine.
    WD_SetProperty("Set Leader Voice", "Christine")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 9.0)
    
    --Black the screen.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Spawn characters.
    fnSpecialCharacter("Christine", "Golem", 57, 9, gci_Face_South, false, "Null")
    if(iSX399JoinsParty == 1.0) then
        fnSpecialCharacter("SX399Lord", "SteamLord", 53, 9, gci_Face_East, false, "Null")
    end
    
    --Move existing characters around.
    fnCutsceneTeleport("55", 57.25, 10.50)
    fnCutsceneFace("55", 0, -1)
    fnCutsceneTeleport("Sophie", 56.25, 10.50)
    fnCutsceneFace("Sophie", 1, -1)
    fnCutsceneTeleport("DoctorMaisie", 57.25, 8.50)
    fnCutsceneFace("DoctorMaisie", 0, 1)
    fnCutsceneTeleport("Assistant", 54.25, 8.50)
    fnCutsceneFace("Assistant", 0, -1)
    
    --Change everything so Christine is the party leader.
    EM_PushEntity("Christine")
        local iChristineID = RO_GetID()
    DL_PopActiveObject()
    AL_SetProperty("Player Actor ID", iChristineID)
    AC_SetProperty("Set Party", 0, "Christine")
    AC_SetProperty("Set Party", 1, "55")
    if(iSX399JoinsParty == 1.0) then
        AC_SetProperty("Set Party", 2, "SX-399")
    end
    
    --Sophie is a follower.
    EM_PushEntity("Sophie")
        local iCharacterID = RE_GetID()
    DL_PopActiveObject()
    AL_SetProperty("Follow Actor ID", iCharacterID) 
    
    --This flag indicates Christine is the party leader.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 2.0)
    
    --Christine changes to golem form.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
    
    --Christine is in the crouching pose.
    fnCutsceneSetFrame("Christine", "Crouch")
    AL_SetProperty("Open Door", "MedicalDoor")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] What happened?[SOFTBLOCK] Did it work?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Help me carry her, she's heavier than she looks.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Why didn't she transform?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Don't bump her head on the door.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Was that stuff the cause?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Hold her arms down, she could wake up at any second.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] Ah, sis is what I needed![SOFTBLOCK] A test subject![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Test subject?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] Yes -[SOFTBLOCK] I must test se cure before I deploy it on a large scale, yes?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Go on, do it, doc![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] Hmm, her skin is quite tough, se syringe is barely piercing it.[SOFTBLOCK] I should gasser some samples...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Doctor![SOFTBLOCK] Right now![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] Pah, you are no fun.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] Hold her down, she is reacting to se serum![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] [SOUND|World|Thump]Ack![SOFTBLOCK] Christine, stop this![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] ...[SOFTBLOCK] Interesting, she seems to have listened to you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Dearest, I need you to listen to me.[SOFTBLOCK] You have to concentrate on being a golem.[SOFTBLOCK] Try as hard as you can![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] ...[SOFTBLOCK] Nossing?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] She is not fully awake.[SOFTBLOCK] Try something else.[BLOCK][CLEAR]") ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] Something -[SOFTBLOCK] sweet![BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Christine, do you remember when we first met?[SOFTBLOCK] Imagine that day very clearly.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] I was nervous, you just wanted your function assignment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] I scanned you, and we immediately started breaking the rules -[SOFTBLOCK] hee hee![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] What did you look like then?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] Ah, she is glowing!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Maisie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Christine?[SOFTBLOCK] Hello?[SOFTBLOCK] Say something![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Amazing...[SOFTBLOCK] decentralized instructions...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But I can purge them now.[SOFTBLOCK] Cognitive systems working...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Christine?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Hello Sophie![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] How are you doing?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] You're okay![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's going to take more than a self-aware infection to stop me![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[VOICE|Maisie] All right, everyone out, I must gasser my data.[SOFTBLOCK] Out, out![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] We'll be right outside, Christine!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Christine", "Null")
    
    --Movement.
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("Sophie", 55.25, 10.50)
    fnCutsceneMove("Sophie", 55.25, 12.50)
    fnCutsceneMove("Sophie", 54.25, 12.50)
    fnCutsceneFace("Sophie", 1, 0)
    fnCutsceneMove("Assistant", 53.25, 8.50)
    fnCutsceneMove("Assistant", 53.25, 10.50)
    fnCutsceneMove("Assistant", 55.25, 10.50)
    fnCutsceneMove("Assistant", 55.25, 12.50)
    fnCutsceneMove("Assistant", 56.25, 12.50)
    fnCutsceneFace("Assistant", -1, 0)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX399", 53.25, 10.50)
        fnCutsceneMove("SX399", 55.25, 10.50)
        fnCutsceneMove("SX399", 55.25, 13.50)
        fnCutsceneMove("SX399", 54.25, 13.50)
        fnCutsceneFace("SX399", 1, -1)
    end
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Close Door", "MedicalDoor") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Now, listen to me, you two robots.[SOFTBLOCK] Sis does not leave sis room.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] It is important sat we do not cause a panic.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Announcing a cure to the Raibie epidemic would cause a panic?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Clearly she has come to another conclusion, 771852.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se cure is not se problem.[SOFTBLOCK] In fact, wissin a few hours I will have sis disease dealt wiss.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] My hypossesis was correct.[SOFTBLOCK] A surge of antibodies can overwhelm se passogen before it can adapt, killing it.[SOFTBLOCK] I will need some time to clone more antibodies but sis is not a challenge.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] No, it is what you said.[SOFTBLOCK] Se infection is self-aware.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please explain, doctor.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Christine can likely shed more light than I.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] When I transformed, my CPU registered millions of sub-instructions not originating from any source in my body.[SOFTBLOCK] They were likely left-over nerve impulses.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Which means the Raibies aren't in control of their own bodies, the pathogen is moving them against their will.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Interesting.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sat explains se adapative properties of se passogen.[SOFTBLOCK] Unfortunately, it does not explain where it came from.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I checked into se substance you provided, E-v89-r.[SOFTBLOCK] I happen to know a few passwords I am not aussorized to know.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Doctor![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] What?[SOFTBLOCK] Se password 'Corgis4Ever' is suspiciously common in se scientific fields.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] While se substance itself is inert in an organic body, electromagnetic waves can trigger reactions in it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Such waves would be common in the highly-electrified Raiju bodies.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Correct.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Se issue is...[SOFTBLOCK] sis is a designed behavior.[SOFTBLOCK] Sey took 'inspiration' from se Epsilon Labs, but I was not able to find out what sey took it from, specifically.[SOFTBLOCK] Sat is beyond my reach.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I would not have thought it possible to construct self-aware molecular clusters at se scale of bacteria, yet somehow sey have done it, and even made it have a trigger mechanism.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I think I know how, but I can't explain it.[SOFTBLOCK] Not scientifically.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But the real point is -[SOFTBLOCK] they [SOFTBLOCK]*knew*.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The Raibies are just their experiments getting out of hand.[SOFTBLOCK] This wasn't done by Vivify, this was done by 2856.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Sat is se conclusion I have come to, and why we cannot discuss sis in public.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are revolutionaries, doctor.[SOFTBLOCK] We are not here to protect 2856 and her power structure.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] I know sat, and frankly, I am now a supporter.[SOFTBLOCK] Sis...[SOFTBLOCK] sis is disgusting.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] But se security situation is fraught, we must not have chaos until we have at least dealt wiss sat.[SOFTBLOCK] Se populace here will likely become greatly upset, and we do not need sat.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So long as you'll let the people here know, eventually...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] You have my word.[SOFTBLOCK] Se Head of Research will pay for sis.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] We are here to cure and heal.[SOFTBLOCK] Medical Essics demand we stop her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We're working on it, doctor.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] For now, work on the cure and getting it distributed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We'll take care of the rest.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Very good.[SOFTBLOCK] Now, you must go.[SOFTBLOCK] My assistant and I must prepare more doses of se serum.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Maisie:[E|Neutral] Despite se dour nature of se proceedings, you have done very well, Christine.[SOFTBLOCK] Good work.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks, doc.[SOFTBLOCK] Be safe.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "MedicalDoor") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("55", 55.25, 10.50)
    fnCutsceneMove("55", 55.25, 13.50)
    fnCutsceneMove("55", 56.25, 13.50)
    fnCutsceneFace("55", -1, 0)
    fnCutsceneMove("Christine", 57.25, 10.50)
    fnCutsceneMove("Christine", 55.25, 10.50)
    fnCutsceneMove("Christine", 55.25, 14.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Assistant", 55.25, 12.50)
    fnCutsceneMove("Assistant", 55.25, 10.50)
    fnCutsceneMove("Assistant", 57.25, 10.50)
    fnCutsceneMove("Assistant", 57.25, 11.50)
    fnCutsceneFace("Assistant", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneFace("Sophie", 1, 1)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneFace("SX399", 1, 0)
    end
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] What was that about?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I had to debrief the doctor about the method of capture.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Sensitive information.[SOFTBLOCK] I'll tell you later when nobody might overhear.*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] *You got it, chief.*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] So did you do it?[SOFTBLOCK] Did you save the day?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ...[SOFTBLOCK] Nope![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The day won't be saved until every citizen of Regulus City is free.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But the good doctor does have a cure for the Raibies pathogen, so that's something.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You have my permission to be happy in the interim.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Amusing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 55...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks for saving me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] You are welcome.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Hey, I sorta helped, too.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] For example, I helped carry your heavy butt all the way here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] You all did great![SOFTBLOCK] I know the Raijus will thank you for it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And hopefully, they will join our revolution.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But, out of the frying pan and into the fire.[SOFTBLOCK] Our job isn't done yet.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, will you resume your position at the entrance?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are on the way.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Back to work.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Shall we, Sophie?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] We shall![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hey, SX-399, I'm going to need help editing the videographs I recorded.[SOFTBLOCK] If you're not busy...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Sophie![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] O-[SOFTBLOCK]okay![SOFTBLOCK] I'll have to do it all by myself![SOFTBLOCK] Hee hee!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --55 and SX-399 leave.
        fnCutsceneMove("55", 56.25, 14.50)
        fnCutsceneFace("55", 0, 1)
        fnCutsceneMove("SX399", 56.25, 13.50)
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "MedDoorL") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 56.25, 18.50)
        fnCutsceneMove("55", 57.25, 18.50)
        fnCutsceneFace("55", 0, 1)
        fnCutsceneMove("SX399", 56.25, 18.50)
        fnCutsceneFace("SX399", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "MedDoorC") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 57.25, 22.50)
        fnCutsceneMove("SX399", 57.25, 18.50)
        fnCutsceneMove("SX399", 57.25, 22.50)
        fnCutsceneBlocker()
        fnCutsceneTeleport("55", 48.25, 36.50)
        fnCutsceneFace("55", 0, 1)
        fnCutsceneTeleport("SX399", 47.25, 36.50)
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneBlocker()
        
        --Christine and Sophie form up.
        fnCutsceneMove("Sophie", 55.25, 14.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
            
        --Set collision flags and dialogue flags for the departing characters.
        local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "Set55Properties.lua\")"
        fnCutsceneInstruction(sString)
        sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SetSX399Properties.lua\")"
        fnCutsceneInstruction(sString)
        
        
    else
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] What was that about?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I had to debrief the doctor about the method of capture.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Sensitive information.[SOFTBLOCK] I'll tell you later when nobody might overhear.*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *I understand.[SOFTBLOCK] Tell me later.*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] So did you do it?[SOFTBLOCK] Did you save the day?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ...[SOFTBLOCK] Nope![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The day won't be saved until every citizen of Regulus City is free.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But the good doctor does have a cure for the Raibies pathogen, so that's something.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You have my permission to be happy in the interim.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Amusing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 55...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks for saving me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] You are welcome.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Yeah![SOFTBLOCK] Way to go, 55![SOFTBLOCK] I'm sure Christine will tell me all about your heroics![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And hopefully, the Raijus will join our revolution when they hear about who cured them...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] And who infected them...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But, out of the frying pan and into the fire.[SOFTBLOCK] Our job isn't done yet.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, will you resume your position at the entrance?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am on the way.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Shall we, Sophie?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] We shall!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --55 and SX-399 leave.
        fnCutsceneMove("55", 56.25, 14.50)
        fnCutsceneFace("55", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "MedDoorL") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 56.25, 18.50)
        fnCutsceneMove("55", 57.25, 18.50)
        fnCutsceneFace("55", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "MedDoorC") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 57.25, 22.50)
        fnCutsceneBlocker()
        fnCutsceneTeleport("55", 48.25, 36.50)
        fnCutsceneFace("55", 0, 1)
        fnCutsceneBlocker()
        
        --Christine and Sophie form up.
        fnCutsceneMove("Sophie", 55.25, 14.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
            
        --Set collision flags and dialogue flags for the departing characters.
        local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "Set55Properties.lua\")"
        fnCutsceneInstruction(sString)
        
    end

end
