--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Variables]
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --[Drone Units]
    if(sActorName == "SecurityA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] OUR COMMAND UNIT HAS APPROPRIATED THIS OFFICE AS HER HEADQUARTERS.[SOFTBLOCK] PLEASE WIPE YOUR FEET.") ]])
        
    elseif(sActorName == "SecurityB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] CURRENT SECURITY STATUS IS REGISTERED AS 'SPIFFY'.[SOFTBLOCK] AUTHORIZATION IS NEEDED TO UPGRADE IT TO 'SQUEAKY CLEAN'.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Squeaky clean is the theoretically highest security status.[SOFTBLOCK] It's also asymptotically unobtainable, but a drone wouldn't know that.)") ]])
        
    --[2856]
    elseif(sActorName == "2856") then
    
        --Variables
        local iGotBreachingTools = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotBreachingTools", "N")
        
        --Christine is leading:
        if(iChristineLeadingParty >= 1.0) then
    
            --Don't have the tools yet.
            if(iGotBreachingTools == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Have you obtained the breaching tools yet?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I checked the inventory records.[SOFTBLOCK] They're in the northeastern storage sheds.[SOFTBLOCK] Are you on your way there?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Yes.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Good.[SOFTBLOCK] Have we anything further to discuss?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No.[SOFTBLOCK] Goodbye.") ]])
                fnCutsceneBlocker()
        
            --Got the tools.
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Have you obtained the breaching tools yet?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Good.[SOFTBLOCK] Your objective is in the Epsilon Laboratories.[SOFTBLOCK] Exit the ranch, go west, then north when you are in the Gamma Laboratories.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] The connection to the Beta Laboratories has a security station.[SOFTBLOCK] The entrance to Epsilon is on the eastern side of that connection.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We're on our way.[SOFTBLOCK] Goodbye.") ]])
                fnCutsceneBlocker()
            end
    
        --55 is leading:
        else
    
            --Don't have the tools yet.
            if(iGotBreachingTools == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Have you obtained the breaching tools yet?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I checked the inventory records.[SOFTBLOCK] They're in the northeastern storage sheds.[SOFTBLOCK] Are you on your way there?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes we are, thank you for the information.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Good.[SOFTBLOCK] Have we anything further to discuss?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.") ]])
                fnCutsceneBlocker()
        
            --Got the tools.
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Have you obtained the breaching tools yet?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We have them.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Good.[SOFTBLOCK] Your objective is in the Epsilon Laboratories.[SOFTBLOCK] Exit the ranch, go west, then north when you are in the Gamma Laboratories.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] The connection to the Beta Laboratories has a security station.[SOFTBLOCK] The entrance to Epsilon is on the eastern side of that connection.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Moving out.") ]])
                fnCutsceneBlocker()
            end
        
        end
    
    --[Lord Golems]
    elseif(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] To think I am contemplating taking up quarters in this domicile.[SOFTBLOCK] There are no defragmentation pods anywhere else for one of my statue.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] Well, the principal's quarters.[SOFTBLOCK] But the Head of Research...[SOFTBLOCK] I'd rather not be in the same room with her.") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] The humans run here, in a circle![SOFTBLOCK] It keeps thier leg motivators strong, you know.[SOFTBLOCK] They have to use them constantly or they degrade.") ]])
        
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] There's something to be said for an organic system that maintains itself when it is used.[SOFTBLOCK] Perhaps we should integrate that into our systems?") ]])
        
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|GolemLord] I am...[SOFTBLOCK] supervising...[SOFTBLOCK] the humans at the swimming pools.[SOFTBLOCK] My optical receptors are plenty sensitive from this distance.") ]])
    
    --[Raijus]
    elseif(sActorName == "RaijuA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] I'm helping the security teams![SOFTBLOCK] Watch out for tangos, ha ha ha this is fun!") ]])
        
    elseif(sActorName == "RaijuB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF1] I wanna go swimming with everyone else![SOFTBLOCK] But they never let us because it'll probably kill them.[SOFTBLOCK] But I waaaannnaaaa!") ]])
        
    elseif(sActorName == "RaijuC") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] Don't worry sweetie.[SOFTBLOCK] If any of those nasties get close I can shock them for you![SOFTBLOCK] Zap zap!") ]])
    
    --[Slave Golems]
    elseif(sActorName == "GolemSlaveA") then
        if(iChristineLeadingParty >= 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Honestly, looking after the humans is one of the best assignments I've had.[SOFTBLOCK] Sure, my Lord Golem is a typical Lord Golem, but the humans are all kind and sweet.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] What about the humans who make Lord Golem?[SOFTBLOCK] Do you think they'll stay nice?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You know, I don't keep up with them afterwards.[SOFTBLOCK] Maybe the city will straighten itself out if we promote enough nice humans?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Something tells me the city will change them before they change the city...)") ]])
        
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Honestly, looking after the humans is one of the best assignments I've had.[SOFTBLOCK] Sure, my Lord Golem is a typical Lord Golem, but the humans are all kind and sweet.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] And after they are converted?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You know, I don't keep up with them afterwards.[SOFTBLOCK] Maybe the city will straighten itself out if we promote enough nice humans?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Structuralism suggests that is not the case.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Yeah, it does, doesn't it...") ]])
        end
        
    elseif(sActorName == "GolemSlaveB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Even with a freakin' crisis going on, oh we absolutely must trim the brush.[SOFTBLOCK] Of course, my Lord Golem![SOFTBLOCK] At once!") ]])
        
    elseif(sActorName == "GolemSlaveC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I am supervising this human in fabrication training.[SOFTBLOCK] Fabricating pulse weaponry is no different than fabricating a table leg!") ]])
        
    elseif(sActorName == "GolemSlaveD") then
        if(iChristineLeadingParty >= 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The humans, thus far, have managed to avoid accidents in the pool for 100 straight days.[SOFTBLOCK] Naturally my Lord Golem mandates that I watch them anyway, just in case.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] You don't sound like you mind.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Not really, it's easy work.[SOFTBLOCK] She also mandates I record the proceedings and upload them for her to view.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] She's extremely dedicated to the safety of these humans.[SOFTBLOCK] She dotes on them like they were her own children.[SOFTBLOCK] It's touching in a way.") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The humans, thus far, have managed to avoid accidents in the pool for 100 straight days.[SOFTBLOCK] Naturally my Lord Golem mandates that I watch them anyway, just in case.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] An impressive safety record for a human.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We watch them very closely, and we record their work for the local administrator to review.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] She's extremely dedicated to the safety of these humans.[SOFTBLOCK] She dotes on them like they were her own children.[SOFTBLOCK] It's touching in a way.") ]])
    
        end
    
    --[Humans]
    elseif(sActorName == "HumanA") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Hmm, where does the magrail fit onto the slide?[SOFTBLOCK] Oh, right there, of course!") ]])
        
    elseif(sActorName == "HumanB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF1] We were all going to have a pool party when everything went wild.[SOFTBLOCK] But until we're told otherwise, we're not letting them stop us![SOFTBLOCK] Dive in, girls!") ]])
        
    elseif(sActorName == "HumanC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Sometimes I like to just walk around in my swimwear and let all the male units gawk.[SOFTBLOCK] My Lord Golem even encourages me!") ]])
        
    elseif(sActorName == "HumanD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF1] Sometimes I wish I hadn't volunteered for security duty, but I'll be damned if I'm letting any of these things get my fellow units!") ]])
        
    elseif(sActorName == "HumanE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] If we work together, I know we can stop those bad guys.[SOFTBLOCK] I've got the power of lightning on my side!") ]])
        
    elseif(sActorName == "HumanF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF1] I was going to go to bed early, but of course we had to have an emergency drill.[SOFTBLOCK] This is a drill, right?") ]])
        
    elseif(sActorName == "HumanG") then
        
        --Variables.
        local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            if(iFinished198 == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Hmm?[SOFTBLOCK] Oh, I was just thinking about something.[SOFTBLOCK] Don't mind me.") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] I was told you might be coming this way, comrade.[SOFTBLOCK] I don't know about the others, but I'm with you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Better keep that to yourself, for the time being.[SOFTBLOCK] The Head of Research is in the building across from you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] I know.[SOFTBLOCK] I've been sending updates on her position out through the network.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] But how?[SOFTBLOCK] Network access is spotty at best.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Heh.[SOFTBLOCK] Unit 745110 sent a courier.[SOFTBLOCK] An...[SOFTBLOCK] electric...[SOFTBLOCK] courier.[SOFTBLOCK] You know what I mean.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Oh, I'm glad to hear that Amanda is doing all right.[SOFTBLOCK] Just be discreet.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Of course.[SOFTBLOCK] To freedom.") ]])
            end
        
        --55:
        else
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            if(iFinished198 == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Hmm?[SOFTBLOCK] Oh, I was just thinking about something.[SOFTBLOCK] Don't mind me.") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Blue Leader![SOFTBLOCK] Can I help you with anything?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Do not blow your cover.[SOFTBLOCK] I have the situation under control.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] I know.[SOFTBLOCK] I've been trying to send updates out but the network is spotty.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] But...[SOFTBLOCK] Unit 745110 sent a courier.[SOFTBLOCK] An...[SOFTBLOCK] electric...[SOFTBLOCK] courier.[SOFTBLOCK] You know what I mean.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Excellent work.[SOFTBLOCK] These couriers can operate despite network outages.[SOFTBLOCK] Well done.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanF0] Hey it was Unit 745110's idea.[SOFTBLOCK] Good luck out there, Blue Leader.") ]])
            end
        end
        
    elseif(sActorName == "HumanH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanM0] I can't wait to graduate so I can look as good as the girl units do...") ]])
        
    elseif(sActorName == "HumanI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanM1] If I get converted and assigned to a different department than my tandem unit, what then?[SOFTBLOCK] Will we never see each other again?[SOFTBLOCK] Oh my...") ]])
        
    elseif(sActorName == "HumanJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[VOICE|HumanM0] I can't tell if Unit 754334 likes me or not.[SOFTBLOCK] Look at her, she's so good at fabricating.[SOFTBLOCK] I bet she'll make a great golem...") ]])
        
    --[Boob Girls]
    --These have special reaction cases when spoken to by Christine. 55 does not notice.
    elseif(sActorName == "HumanK") then
        
        --Variables.
        local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
        local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "HumanF0B", "Neutral") ]])
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
            
            --Human
            if(sChristineForm == "Human") then
                
                --Hasn't spoken to the second boob girl yet:
                if(sSpokeToSecondBoobGirl == "Nobody") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, lord unit![SOFTBLOCK] Are you here for an inspection?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Lord unit?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *You, Sophie!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Eep![SOFTBLOCK] Yes, I am Lord Unit 49 -[SOFTBLOCK] uh, just call me Sophie![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, Sophie.[SOFTBLOCK] I've not seen your friend before.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Christine.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Are you a recent transfer?[SOFTBLOCK] I didn't know we were getting a transfer![SOFTBLOCK] Hello![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm just visiting, actually.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] ...[SOFTBLOCK] Oh, so that means you must be wondering what these machines are![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's it exactly![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] These are the milking machines.[SOFTBLOCK] All our human cheese are made from milk pumped from these![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] We're very proud of our work here.[SOFTBLOCK] We receive compliments from all across the city![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Human cheese?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Milking happens right here?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] It does![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] And your...[SOFTBLOCK] chest...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] A very common side effect of being milked is larger mammaries.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Ms. Primrose takes a great interest in making sure we are milked as often as possible.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Does it hurt?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] It feels wonderous, lord unit![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] In fact, permission is required to be milked.[SOFTBLOCK] It's a reward system to encourage good behavior.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Well, males selected for reproduction also get milked, but that has its own machine.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Males...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Getting milked...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Is there something upsetting you?[SOFTBLOCK] I wasn't aware this wasn't common knowledge.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is both of our first times here.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Say...[SOFTBLOCK] As a lord unit, would my friend here be able to give permission to a human to be milked?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Of course![SOFTBLOCK] I can show you how to operate the machines, or give you a datafile.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Christine?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *She says it feels really good, and gives you bigger boobs, Sophie!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *I would love it if you would play with my big jiggly boobs...*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Yes, but...[SOFTBLOCK] really?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *Really!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] *Oh my goodness, yes![SOFTBLOCK] Yes yes yes!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Yippee![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Very good![SOFTBLOCK] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                --Has spoken to two boob girls.
                elseif(sSpokeToSecondBoobGirl ~= "Nobody" and sSpokeToSecondBoobGirl ~= "Nomatter") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, lord unit![SOFTBLOCK] Are you here for an inspection?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Lord unit?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *You, Sophie!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Eep![SOFTBLOCK] Yes, I am Lord Unit 49 -[SOFTBLOCK] uh, just call me Sophie![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, Sophie.[SOFTBLOCK] I've not seen your friend before.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Christine.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Are you a recent transfer?[SOFTBLOCK] I didn't know we were getting a transfer![SOFTBLOCK] Hello![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm just visiting, actually.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] So I've been told that these are milking machines?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] That's right![SOFTBLOCK] All our human cheese are made from milk pumped from these![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] We're very proud of our work here.[SOFTBLOCK] We receive compliments from all across the city![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So if my lord unit friend were to authorize a certain human to use them...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Christine?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *They said it feels really good, and gives you bigger boobs, Sophie!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Yes, but...[SOFTBLOCK] really?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *Really!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] *Oh my goodness, yes![SOFTBLOCK] Yes yes yes!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Of course, my lord![SOFTBLOCK] I'll send your PDU the instructions.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Yippee![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Just inspect the machines and enjoy yourself![SOFTBLOCK] I was nervous my first time, but it feels *so good*...") ]])
                
                --Repeat cases.
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Just take a look at the machines whenever you are ready.[SOFTBLOCK] Unless you enjoy talking to me?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Yes...[SOFTBLOCK] We like being around...[SOFTBLOCK] you...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] They're just so big and bouncy...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Tee hee![SOFTBLOCK] Thank you![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] I hope to become a raiju after graduation![SOFTBLOCK] Are you aiming for raiju, or perhaps lord unit?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Something along those lines...") ]])
                end
                fnCutsceneBlocker()
            
            --Raiju.
            elseif(sChristineForm == "Raiju") then
                
                --Hasn't spoken to the second boob girl yet:
                if(sSpokeToSecondBoobGirl == "Nobody") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, lord unit![SOFTBLOCK] Are you here for an inspection?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Lord unit?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *You, Sophie!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Eep![SOFTBLOCK] Yes, I am Lord Unit 49 -[SOFTBLOCK] uh, just call me Sophie![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, Sophie.[SOFTBLOCK] I've not seen your friend before.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Christine.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Are you a recent convert?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Earlier today, actually.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] ...[SOFTBLOCK] Oh, so that means you must be wondering what these machines are![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's it exactly![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] These are the milking machines.[SOFTBLOCK] All our human cheese are made from milk pumped from these![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] We're very proud of our work here.[SOFTBLOCK] We receive compliments from all across the city![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Human cheese?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Milking happens right here?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] It does![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] And your...[SOFTBLOCK] chest...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] A very common side effect of being milked is larger mammaries.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Ms. Primrose takes a great interest in making sure we are milked as often as possible.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Does it hurt?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] It feels wonderous, lord unit![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] In fact, permission is required to be milked.[SOFTBLOCK] It's a reward system to encourage good behavior.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Well, males selected for reproduction also get milked, but that has its own machine.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Males...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Getting milked...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Is there something upsetting you?[SOFTBLOCK] I wasn't aware this wasn't common knowledge.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is both of our first times here.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Say...[SOFTBLOCK] As a lord unit, would my friend here be able to give permission to a human to be milked?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Of course![SOFTBLOCK] I can show you how to operate the machines, or give you a datafile.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Though as a raiju, it's best if you don't.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Electrical problems?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Yep.[SOFTBLOCK] Milking a raiju would probably blow a hole in the roof of the labs.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But if we brought a human friend of mine...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Christine?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *She says it feels really good, and gives you bigger boobs, Sophie!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Yes, but...[SOFTBLOCK] really?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *Really![SOFTBLOCK] Imagine sucking my big jiggly boobs...*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] *Oh my goodness, yes![SOFTBLOCK] Yes yes yes!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Are they nearby?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I'll be back with her![SOFTBLOCK] Just wait here![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Very good![SOFTBLOCK] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                --Has spoken to two boob girls.
                elseif(sSpokeToSecondBoobGirl ~= "Nobody" and sSpokeToSecondBoobGirl ~= "Nomatter") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, lord unit![SOFTBLOCK] Are you here for an inspection?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Not an inspection.[SOFTBLOCK] Instead, I was wondering if we could make use of your facilities?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Do you mean the milking machines?[SOFTBLOCK] Sorry, raijus can't be milked.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] I mean they can, but the resulting explosion of electricity would probably blow a hole in the roof.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No, I meant, if my lord unit friend were to authorize a certain human to use them?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Christine?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *They said it feels really good, and gives you bigger boobs, Sophie!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Yes, but...[SOFTBLOCK] really?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *Really!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] *Oh my goodness, yes![SOFTBLOCK] Yes yes yes!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Did you have someone in mind? Any lord unit may approve a milking session![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I'll be back with her![SOFTBLOCK] Just wait here![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Very good![SOFTBLOCK] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                --Repeat cases.
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Just take a look at the machines whenever your human arrives.[SOFTBLOCK] Unless you enjoy talking to me?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Yes...[SOFTBLOCK] We like being around...[SOFTBLOCK] you...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] They're just so big and bouncy...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Tee hee![SOFTBLOCK] Thank you![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] I hope to become a raiju after graduation![SOFTBLOCK] I'm sure we'll get to be great friends, then![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Of course...") ]])
                end
                fnCutsceneBlocker()
            
            --Golem.
            else
                
                --Hasn't spoken to the second boob girl yet:
                if(sSpokeToSecondBoobGirl == "Nobody") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, Lord Units![SOFTBLOCK] Are you here for an inspection?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Not an inspection, but, what is this place?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] These are the milking machines.[SOFTBLOCK] This is where we extract the milk used for the human cheeses you've no doubt had![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] We're very proud of our work here.[SOFTBLOCK] We receive compliments from all across the city about our milk and cheese![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Human cheese?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Milking happens right here?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] It does![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] And your...[SOFTBLOCK] chest...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] A very common side effect of being milked is larger mammaries.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Ms. Primrose takes a great interest in making sure we are milked as often as possible.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Does it hurt?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] It feels wonderous, lord unit![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] In fact, permission is required to be milked.[SOFTBLOCK] It's a reward system to encourage good behavior.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Well, males selected for reproduction also get milked, but that has its own machine.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Males...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Getting milked...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Is there something upsetting you, my lords?[SOFTBLOCK] I wasn't aware this wasn't common knowledge.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is my first year as a lord golem.[SOFTBLOCK] I've never been to the biolabs before.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Say...[SOFTBLOCK] As a lord unit, would I be able to give permission to a human to be milked?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Of course, lord unit.[SOFTBLOCK] I don't see why not.[SOFTBLOCK] I can show you how to operate the machines, or give you a datafile.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Christine?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *She says it feels really good, and gives you bigger boobs, Sophie!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Yes, but...[SOFTBLOCK] really?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *Really![SOFTBLOCK] Imagine suckling on my big jiggling boobies...*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] *Oh my goodness, yes![SOFTBLOCK] Yes yes yes!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Did you have someone in mind?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes.[SOFTBLOCK] I'll send her right along with my associate here.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I'll be back with her![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Very good![SOFTBLOCK] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                --Has spoken to two boob girls.
                elseif(sSpokeToSecondBoobGirl ~= "Nobody" and sSpokeToSecondBoobGirl ~= "Nomatter") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, Lord Units![SOFTBLOCK] Are you here for an inspection?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Not an inspection.[SOFTBLOCK] Instead, I was wondering if we could make use of your facilities?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Do you mean the milking machines?[SOFTBLOCK] I'm not sure they'd work on a golem.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No, I meant, if I were to authorize a human to use them?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Christine?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *They said it feels really good, and gives you bigger boobs, Sophie!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] *Yes, but...[SOFTBLOCK] really?*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *Really!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] *Oh my goodness, yes![SOFTBLOCK] Yes yes yes!*[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Did you have someone in mind?[SOFTBLOCK] Any lord unit may approve a milking session![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes.[SOFTBLOCK] I'll send her right along with my associate here.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I'll be back with her![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Very good![SOFTBLOCK] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                
                --Repeat cases.
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Just take a look at the machines whenever your human arrives.[SOFTBLOCK] Unless you enjoy talking to me?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Yes...[SOFTBLOCK] We like being around...[SOFTBLOCK] you...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] They're just so big and bouncy...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Tee hee![SOFTBLOCK] Thank you, lords![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] I hope to become a raiju after graduation![SOFTBLOCK] Put in a good word for me with Ms. Primrose if you can![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Of course...") ]])
                end
                fnCutsceneBlocker()

            end
            
        --55:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, Command Unit![SOFTBLOCK] And...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is SX-399.[SOFTBLOCK] She is an upgraded steam droid.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Hi![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Steam droid?[SOFTBLOCK] Wow, I remember seeing those in history class.[SOFTBLOCK] I didn't think there were any left.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] There are![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Does the Administration intend to upgrade them, too?[SOFTBLOCK] Because, pardon me, but you look fabulous![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Thanks![SOFTBLOCK] You too![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] I'm a little jealous...[SOFTBLOCK] You've got a very large...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Head?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Throat?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Eyes?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] Chest, 55.[SOFTBLOCK] She has big breasts.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not think so.[SOFTBLOCK] They are within the tenth percentile among most humans, but the milking process in the breeding program places her in the fiftieth percentile here in the biolabs.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you are jealous, I'm sure Unit 499393 could synthesize new mammary plating for you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Wow.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] Yeah, 55 is kind of an amazing unit, isn't she?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (I seem to be in error.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Thank you for the compliment.[SOFTBLOCK] Please explain this to me further at your leisure, SX-399.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Blush] Oh I absolutely will, hun...") ]])
        
        end
        
    elseif(sActorName == "HumanL") then
        
        --Variables.
        local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
        local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "HumanF1B", "Neutral") ]])
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
                
            --Speaking to a first boob girl:
            if(sSpokeToFirstBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "HumanL")
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Isn't it just a lovely day out?[SOFTBLOCK] I mean, it always is because this is climate-controlled habitat, but you know what I meant.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Erm, are you staring?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Oh...[SOFTBLOCK] no...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Not staring...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Oh.[SOFTBLOCK] All right.[SOFTBLOCK] Have a good one!") ]])
            
            --Second boob girl. Must not be the first one, and no second yet.
            elseif(sSpokeToFirstBoobGirl ~= "Nobody" and sSpokeToFirstBoobGirl ~= "HumanL" and sSpokeToSecondBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "HumanL")
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Isn't it just a lovely day out?[SOFTBLOCK] I mean, it always is because this is climate-controlled habitat, but you know what I meant.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] So, are you two enjoying the scenery?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] The...[SOFTBLOCK] scenery?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] So jiggly...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Do all breeding humans have those?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Only if you've been a good girl![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Repeated milking causes gland growth, and it feels so good, so you need to be rewarded for good behavior.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] I do volunteer cleaning so I can get milked.[SOFTBLOCK] I think it's worth it, but it's also addictive.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I think I'm addicted...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] How is it addictive?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Once you get used to getting milked, they start to get sore if they don't get milked.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] But I get my tandem unit to suck on them until they feel better...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Wow...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] You should go see the milking room in the northeast, there.[SOFTBLOCK] Maybe you'll even get to see someone being milked![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Have a good one!") ]])
            
            --All other repeat cases.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Isn't it just a lovely day out?[SOFTBLOCK] I mean, it always is because this is climate-controlled habitat, but you know what I meant.") ]])
            end
            fnCutsceneBlocker()

        --55:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, Command Unit![SOFTBLOCK] It's a lovely day, isn't it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In strictest terms, it is nearly midnight in your biological cycling.[SOFTBLOCK] The labs merely have their sky emulation set to daytime.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Further, the quality of the day is mandated by the climate control apparatus.[SOFTBLOCK] The delta laboratories do not emulate rainfall or cloudiness.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Affirmative, Command Unit![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Why did you make that observation to me, then?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] I just wanted to be friendly.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Oh.[SOFTBLOCK] Hello, friend.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Don't be a stranger, Command Unit![SOFTBLOCK] I love meeting new people!") ]])
        end
    
    elseif(sActorName == "HumanM") then
    
        --Variables.
        local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
        local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "HumanF0B", "Neutral") ]])
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
                
            --Speaking to a first boob girl:
            if(sSpokeToFirstBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "HumanM")
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Do you ever spend time just looking up research papers?[SOFTBLOCK] I don't like reading fiction.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Uh, my eyes are up here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] They... are?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Eyes...?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Heh. You like what you see?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] You must not come here often, then.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] What do you mean? Jiggle jiggle...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] You'll find out if you ask around.") ]])
            
            --Second boob girl. Must not be the first one, and no second yet.
            elseif(sSpokeToFirstBoobGirl ~= "Nobody" and sSpokeToFirstBoobGirl ~= "HumanM" and sSpokeToSecondBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "HumanM")
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Do you ever spend time just looking up research papers?[SOFTBLOCK] I don't like reading fiction.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Uh, my eyes are up here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] They... are?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Eyes...?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Do all breeding humans have...[SOFTBLOCK] those?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Only if you've been a good girl![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Repeated milking causes gland growth, and it feels so good, so you need to be rewarded for good behavior.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] My academic record is exemplary, so I'm both on track for lord unit, and I get milking permits often.[SOFTBLOCK] Bit of a double-edged sword, though.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Double-edged sword?[SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Once you get used to getting milked, they start to get sore if they don't get milked.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] They puff up and feel like they're going to pop, so you need to call your tandem unit to milk you with her mouth.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] And my tandem unit has the same problem, so we spend a lot of time together.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Wow...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] You should go see the milking room in the northeast, there.[SOFTBLOCK] Maybe you'll even get to see someone being milked![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Have a good one!") ]])
            
            --All other repeat cases.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Do you ever spend time just looking up research papers?[SOFTBLOCK] I don't like reading fiction.") ]])
            end
            fnCutsceneBlocker()

        --55:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, Command Unit![SOFTBLOCK] I'm just reading some physics research.[SOFTBLOCK] May I help you with anything?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] This is not an official visit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] But I do enjoy hearing the lark song.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] ...[SOFTBLOCK] Blue leader?[SOFTBLOCK] How...?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Nevermind.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Since I am here, may I have your report?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Most of the breeding population is fairly neutral on the state of the city.[SOFTBLOCK] We aren't as negatively affected by the change in industrial priority.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] We're well taken care of and our immediate superiors are kind by lord unit standards.[SOFTBLOCK] I'm afraid we won't find many recruits here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Except for those ideologically motivated, such as yourself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Freedom for some is freedom for none.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] I like you a lot.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] It's best if we're not seen together, so you should go.[SOFTBLOCK] To freedom, sisters.") ]])
        end
    
    elseif(sActorName == "HumanN") then
    
        --Variables.
        local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
        local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "HumanF1B", "Neutral") ]])
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
                
            --Speaking to a first boob girl:
            if(sSpokeToFirstBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "HumanN")
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] What is up with this level?[SOFTBLOCK] So many switchbacks -[SOFTBLOCK] argh![SOFTBLOCK] I crashed again![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Oh, sorry.[SOFTBLOCK] I didn't see you there.[SOFTBLOCK] I am authorized to play video games on the education computers during off hours.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Huh...[SOFTBLOCK] Video games...?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Video...[SOFTBLOCK] games..?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] You're looking at my -[SOFTBLOCK] oh.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] First time in the biolabs, ladies?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] ...[SOFTBLOCK] Jiggle jiggle...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Oh you two are going to have a grand time...") ]])
            
            --Second boob girl. Must not be the first one, and no second yet.
            elseif(sSpokeToFirstBoobGirl ~= "Nobody" and sSpokeToFirstBoobGirl ~= "HumanN" and sSpokeToSecondBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "HumanN")
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] What is up with this level?[SOFTBLOCK] So many switchbacks -[SOFTBLOCK] argh![SOFTBLOCK] I crashed again![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Oh, sorry.[SOFTBLOCK] I didn't see you there.[SOFTBLOCK] I am authorized to play video games on the education computers during off hours.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Huh...[SOFTBLOCK] Video games...?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Video...[SOFTBLOCK] games..?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] You're looking at my -[SOFTBLOCK] oh.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] First time in the biolabs, ladies?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] ...[SOFTBLOCK] Jiggle jiggle...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Do all breeding humans have...[SOFTBLOCK] those?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] My breasts?[SOFTBLOCK] Well...[SOFTBLOCK] only if you've been a good girl.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Repeated milking causes gland growth, and it feels so good, so you need to be rewarded for good behavior.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] I make video games in my spare time, and Ms. Primrose really likes them.[SOFTBLOCK] She gave me special milking permission.[SOFTBLOCK] I go once a day![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Because if I don't...[SOFTBLOCK] ouch.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Ouch?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Once you get used to getting milked, they start to get sore if they don't get milked.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] They puff up and feel like they're going to pop, so you need to call your tandem unit to milk you with her mouth.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] And since I don't have a tandem unit, I make damn sure not to miss a milking session.[SOFTBLOCK] Otherwise I'll have to use my hands and moan so loud I wake up the other units.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Wow...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] You should go see the milking room in the northeast, there.[SOFTBLOCK] Maybe you'll even get to see someone being milked![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Have a good one!") ]])
            
            --All other repeat cases.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] What is up with this level?[SOFTBLOCK] So many switchbacks -[SOFTBLOCK] argh![SOFTBLOCK] I crashed again![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] I let my friend design a track with the level editor I made and of course she made the most brutal course ever.") ]])
            end
            fnCutsceneBlocker()

        --55:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Hello, Command Unit![SOFTBLOCK] Would you like to play the racing game I'm working on?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is all right.[SOFTBLOCK] I do not partake in video games.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] You're missing out, hun.[SOFTBLOCK] Video games are the best thing since steamed bread.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Of course they are![SOFTBLOCK] And all the units who play video games are the coolest around![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I support your hobby.[SOFTBLOCK] I just do not partake myself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Please continue your creative development.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Affirmative, Command Unit!") ]])
        end
        
    end
end