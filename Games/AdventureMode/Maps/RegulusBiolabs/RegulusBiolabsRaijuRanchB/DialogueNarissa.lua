--[Narissa's Dialogue]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Variables]
local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iBiolabsMetNarissa     = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsMetNarissa", "N")
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Setup.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
    
    --Christine variation:
    if(iChristineLeadingParty >= 1.0) then
    
        --First time meeting Narissa.
        if(iBiolabsMetNarissa == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsMetNarissa", "N", 1.0)
    
            --Human:
            if(sChristineForm == "Human" or sChristineForm == "Golem") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[E|Neutral] You're unit 771852, aren't you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[E|Neutral] Don't bother denying it.[SOFTBLOCK] Everyone who knows what's up, knows what's up.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I don't believe I've had the pleasure.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Narissa.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hello Narissa![SOFTBLOCK] I'm Unit 499323, but you can call me Sophie if you want.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 771852, or Christine.[SOFTBLOCK] So, how do you know me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Relax, chuckles.[SOFTBLOCK] Your reputation gets around.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Specifically, your pretty metal face should be all over the RVDs.[SOFTBLOCK] Am I right?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Are you referring to the recruitment videograph we made?[SOFTBLOCK] Are you a sympathizer?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You made a videograph?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's just 55 and I standing in front of a banner depicting a smashed killphrase plate.[SOFTBLOCK] We encourage everyone to support the rebels when they come.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If our teams did their jobs, it'll be playing on every RVD in the city that has network access.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Yeah, but I knew about it well before tonight.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You didn't say you're a supporter.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] I'm not.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] You look on edge, Christine...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Administration stooge, then, Narissa?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, you don't look the type.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Is it the big dumb smile?[SOFTBLOCK] Sorry, I'm a Raiju.[SOFTBLOCK] Can't help it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] No, 771852, I'm what we call a 'fixer'.[SOFTBLOCK] I know people who know people.[SOFTBLOCK] I can get things done, or acquired.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Care to be specific?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] In my line of work?[SOFTBLOCK] Specific is called 'evidence'.[SOFTBLOCK] I don't do specific.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] There's a war brewing.[SOFTBLOCK] And when there's a war on, things become hard to find.[SOFTBLOCK] That's where I come in.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, okay.[SOFTBLOCK] So you just want to sell things.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Play both sides, get rich.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] And now you're smiling?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, why not?[SOFTBLOCK] We just found a smuggler![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But she's not a friend, Sophie.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] I don't pretend to be anything but what I am.[SOFTBLOCK] So, yes, I'm not your friend.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] I don't particularly care about your cause, or the Administration's.[SOFTBLOCK] But I do care about the color of your money.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] I don't get it?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Narissa here intends to sell her services to whichever side can afford to pay her.[SOFTBLOCK] Information, supplies, or maybe just bottles of booze.[SOFTBLOCK] Am I right?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Bottles of booze, eh?[SOFTBLOCK] Good thinking.[SOFTBLOCK] With transport interrupted, organic citizens are going to need their fix.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] So happy to help.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But we're trying to free you.[SOFTBLOCK] Won't you help us?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Girl, if I wanted off this rock, I could leave in an hour.[SOFTBLOCK] I know the right golems.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] I'm already free, and I prefer the administrators to not know that.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] So, during work hours, I'm just a big-titted dummy who screws everyone who gives her a tickle.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] But if you come in the off hours and say the right words, I can get you damn near anything.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] So, listen up.[SOFTBLOCK] You got spare work credits?[SOFTBLOCK] I can trade those for platina.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Got platina?[SOFTBLOCK] I can sell you credits chips, or goodies I've 'found'.[SOFTBLOCK] You know.[SOFTBLOCK] Fell off the back of a tram.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] And if you've got spare junk, well, one robot's trash is another raiju's treasure.[SOFTBLOCK] Get me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So what have you got for us?[BLOCK]") ]])
            
            --Raiju:
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[E|Neutral] You're unit 771852, aren't you.[SOFTBLOCK] Good choice, becoming one of us.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[E|Neutral] Though if half the stuff I've heard about you is true, that's a temporary feature.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You seem to know a lot.[SOFTBLOCK] I don't believe I've had the pleasure.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[E|Neutral] C'mon sweetie, act the part of the big-titted dumb raiju and smile![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] It is rather tempting, I admit...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Name's Narissa, by the way.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hello Narissa![SOFTBLOCK] I'm Unit 499323, but you can call me Sophie if you want.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I prefer Christine, if we're being informal.[SOFTBLOCK] So, how do you know me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Relax, chuckles.[SOFTBLOCK] Your reputation gets around.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Specifically, if you were metal right now, your face would be all over the RVDs.[SOFTBLOCK] Am I right?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Are you referring to the recruitment videograph we made?[SOFTBLOCK] Are you a sympathizer?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You made a videograph?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's just 55 and I standing in front of a banner depicting a smashed killphrase plate.[SOFTBLOCK] We encourage everyone to support the rebels when they come.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If our teams did their jobs, it'll be playing on every RVD in the city that has network access.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Yeah, but I knew about it well before tonight.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You didn't say you're a supporter.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] I'm not.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] You look on edge, Christine...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Administration stooge, then, Narissa?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, you don't look the type.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Is it the big dumb smile?[SOFTBLOCK] Sorry, I'm a Raiju.[SOFTBLOCK] Can't help it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] No, 771852, I'm what we call a 'fixer'.[SOFTBLOCK] I know people who know people.[SOFTBLOCK] I can get things done, or acquired.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Care to be specific?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] In my line of work? Specific is called 'evidence'.[SOFTBLOCK] I don't do specific.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] There's a war brewing.[SOFTBLOCK] And when there's a war on, things become hard to find.[SOFTBLOCK] That's where I come in.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, okay.[SOFTBLOCK] So you just want to sell things.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Play both sides, get rich.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] And now you're smiling?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We just me a smuggler who can help us![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Plus I kind of want to rub all over her...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But she's not a friend, Sophie.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] I don't pretend to be anything but what I am.[SOFTBLOCK] So, yes, I'm not your friend -[SOFTBLOCK] but if you want to cuddle...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] *ahem*[SOFTBLOCK] I don't particularly care about your cause, or the Administration's.[SOFTBLOCK] But I do care about the color of your money.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] I don't get it?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Narissa here intends to sell her services to whichever side can afford to pay her.[SOFTBLOCK] Information, supplies, or maybe just bottles of booze. Am I right?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Bottles of booze, eh?[SOFTBLOCK] Good thinking.[SOFTBLOCK] With transport interrupted, organic citizens are going to need their fix.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] So happy to help.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But we're trying to free you.[SOFTBLOCK] Won't you help us?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Girl, if I wanted off this rock, I could leave in an hour.[SOFTBLOCK] I know the right golems.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] I'm already free, and I prefer the administrators to not know that.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] So, during work hours, I'm just a big-titted dummy who screws everyone who gives her a tickle.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] But if you come in the off hours and say the right words, I can get you damn near anything.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] So, listen up.[SOFTBLOCK] You got spare work credits?[SOFTBLOCK] I can trade those for platina.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Got platina? I can sell you credits chips, or goodies I've 'found'.[SOFTBLOCK] You know.[SOFTBLOCK] Fell off the back of a tram.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] And if you've got spare junk, well, one robot's trash is another raiju's treasure.[SOFTBLOCK] Get me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So what have you got for us?[BLOCK]") ]])
            end
        --Repeats.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Need something?[BLOCK]") ]])
    
        end
    
    --55 in the lead.
    else
    
        --First time meeting Narissa.
        if(iBiolabsMetNarissa == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsMetNarissa", "N", 1.0)
            
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[E|Neutral] Former Head of Security, Unit 2855.[SOFTBLOCK] Criminy, never thought I'd be happy to see you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are almost as bad at greetings as I am.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] I haven't seen a burn like that since the last time I pulled the trigger![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (I must have said something funny.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] [EMOTION|SX-399|Smirk]Why are you happy to see me, raiju?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Call me Narissa, sweetie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] I'm happy to see you because you're a rebel leader.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Why did you not previously think you'd be happy to see me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] ...[SOFTBLOCK] Because you were a cop.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] So you are some sort of black marketeer.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] How'd you figure that out?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is a criminal of some description, hence fearing a 'cop' like me.[SOFTBLOCK] But she is not a rebel sympathizer, because I [SOFTBLOCK]*was*[SOFTBLOCK] a security unit before I was a rebel leader, so she was a criminal before the rebellion.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Therefore, of the list of criminal occupations, 'Black Marketeer' is the most encompassing.[SOFTBLOCK] She could be a mercenary, or a smuggler, but all fall under that umbrella.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Sweet saltwater taffy, she's a sharp one.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Isn't she?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Yes, Unit 2855, I am a 'fixer'.[SOFTBLOCK] I move product, and that includes information.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unfortunately, I am obligated not to execute you due to a promise made to Ms. Primrose.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Woah![SOFTBLOCK] That's a jump in tone![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This raiju intends to sell information about us to the administration, SX-399.[SOFTBLOCK] She intends to play both sides in the rebellion.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unlike most neutrals, she is a belligerant, but for both factions at the same time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And she is therefore a spy to be executed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Five sentences in and she's already threatening to kill me.[SOFTBLOCK] Maybe I was wrong about being happy to see you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] She does have a point.[SOFTBLOCK] Why should we trust you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] For your own benefit?[SOFTBLOCK] Don't.[SOFTBLOCK] Trust my goods, I trust your money.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] But I suppose I'll have to act through an intermediary if my head's on a chopping block.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] No.[SOFTBLOCK] I apologize.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Christine would not approve of executing a neutral.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] She would not approve of an execution, period![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[SOFTBLOCK] That is also correct as well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] [EMOTION|SX-399|Neutral]In the future, I'll be dealing with Christine, then.[SOFTBLOCK] But for now?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] [EMOTION|2855|Neutral]You got spare work credits?[SOFTBLOCK] I can trade those for platina.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Got platina? I can sell you credits chips, or goodies I've 'found'.[SOFTBLOCK] You know.[SOFTBLOCK] Fell off the back of a tram.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] And if you've got spare junk, well, one robot's trash is another raiju's treasure.[SOFTBLOCK] Get me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Very well.[BLOCK]") ]])
    
        --Repeats.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Need something?[BLOCK]") ]])
        end
    
    end
    
    --Decisions.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()

--Buying items.
elseif(sTopicName == "Buy") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Take a look, sweeties.") ]])
    
    --Setup.
    local sBuildPath = fnResolvePath() .. "Shop Setup.lua"
    local sTeardownPath = fnResolvePath() .. "Shop Teardown.lua"
	fnCutsceneInstruction(" AM_SetShopProperty(\"Show\", \"Narissa's Black Market\", \"" .. sBuildPath .. "\", \"" .. sTeardownPath .. "\") ")
	fnCutsceneBlocker()

--Buying work credits.
elseif(sTopicName == "CreditsBuy") then

    --Platina count.
    local iPlatinaTotal = AdInv_GetProperty("Platina")

    --Dialogue.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] So here's the deal, sweetie.[SOFTBLOCK] I'll sell you a credits chip worth 30 work credits for 600 platina a pop.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Just insert them in a work terminal and bam, instant wealth.[SOFTBLOCK] How many can I get you?[BLOCK]") ]])
    
    --Decisions.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    if(iPlatinaTotal >= 3000) then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"5 Chips for 3000\", " .. sDecisionScript .. ", \"Buy5Chips\") ")
    end
    if(iPlatinaTotal >= 1800) then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"3 Chips for 1800\", " .. sDecisionScript .. ", \"Buy3Chips\") ")
    end
    if(iPlatinaTotal >= 600) then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"1 Chip for 600\", " .. sDecisionScript .. ", \"Buy1Chip\") ")
    end
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No thanks\", " .. sDecisionScript .. ", \"NoThanks\") ")
    fnCutsceneBlocker()

--[Work Credits Purchasing]
elseif(sTopicName == "Buy5Chips") then
	WD_SetProperty("Hide")
    
    --Subtract cash.
    AdInv_SetProperty("Remove Platina", 3000)
    for i = 1, 5, 1 do
        LM_ExecuteScript(gsItemListing, "Credits Chip")
    end
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] High roller?[SOFTBLOCK] I like that. Here you go.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] (Received 5 Credits Chips)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
    
elseif(sTopicName == "Buy3Chips") then
	WD_SetProperty("Hide")
    
    --Subtract cash.
    AdInv_SetProperty("Remove Platina", 1800)
    for i = 1, 3, 1 do
        LM_ExecuteScript(gsItemListing, "Credits Chip")
    end
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Pleasure doing business with you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] (Received 3 Credits Chips)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Buy1Chip") then
	WD_SetProperty("Hide")
    
    --Subtract cash.
    AdInv_SetProperty("Remove Platina", 600)
    LM_ExecuteScript(gsItemListing, "Credits Chip")
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] One credits chip, coming up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] (Received Credits Chip)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()

--[Selling Work Credits]
elseif(sTopicName == "CreditsSell") then
	WD_SetProperty("Hide")

    --Credits count.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")

    --Dialogue.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] The current going rate for a work credit is 20 platina per credit.[SOFTBLOCK] I'll just need your sign off.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Append\", \"Narissa:[E|Neutral] How many would you like to sell? You have " .. iWorkCreditsTotal .. ".[BLOCK]\") ")
    
    --Decisions.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    if(iWorkCreditsTotal >= 200) then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"200 Credits for 4000\", " .. sDecisionScript .. ", \"Sell200Creds\") ")
    end
    if(iWorkCreditsTotal >= 100) then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"100 Credits for 2000\", " .. sDecisionScript .. ", \"Sell100Creds\") ")
    end
    if(iWorkCreditsTotal >= 50) then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"50 Credits for 1000\", " .. sDecisionScript .. ", \"Sell50Creds\") ")
    end
    if(iWorkCreditsTotal >= 10) then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"10 Credits for 200\", " .. sDecisionScript .. ", \"Sell10Creds\") ")
    end
    if(iWorkCreditsTotal >= 1) then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell All Credits\", " .. sDecisionScript .. ", \"SellAllCredits\") ")
    end
    
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No thanks\", " .. sDecisionScript .. ", \"NoThanks\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Sell200Creds") then
	WD_SetProperty("Hide")
    
    --Cash.
    AdInv_SetProperty("Add Platina", 4000)
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 200)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Here you go.[SOFTBLOCK] Trust me, we're both winning on this one.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] (Received 4000 Platina)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Sell100Creds") then
	WD_SetProperty("Hide")
    
    --Cash.
    AdInv_SetProperty("Add Platina", 2000)
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 100)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Another satisifed customer.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] (Received 2000 Platina)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Sell50Creds") then
	WD_SetProperty("Hide")
    
    --Cash.
    AdInv_SetProperty("Add Platina", 1000)
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 50)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Don't spend it all in one place, sweetie.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] (Received 1000 Platina)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Sell10Creds") then
	WD_SetProperty("Hide")
    
    --Cash.
    AdInv_SetProperty("Add Platina", 200)
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 10)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Here you go.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] (Received 200 Platina)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "SellAllCredits") then
	WD_SetProperty("Hide")
    
    --Cash.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    AdInv_SetProperty("Add Platina", 20 * iWorkCreditsTotal)
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", 0)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Closing the account, eh? Here you go.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Append\", \"Narissa:[E|Neutral] (Received " .. iWorkCreditsTotal * 20 .. " Platina)[BLOCK][CLEAR]\") ")
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()

--[Cancel Out]
elseif(sTopicName == "NoThanks") then
	WD_SetProperty("Hide")
    
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] Changed your mind?[SOFTBLOCK] No problem.[BLOCK]") ]])
    
    --Decisions.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()

--[Exit Conversation]
elseif(sTopicName == "Goodbye") then
	WD_SetProperty("Hide")
    
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Narissa:[E|Neutral] You know exactly where to find me, sweeties.[SOFTBLOCK] I'll be waiting.") ]])

end