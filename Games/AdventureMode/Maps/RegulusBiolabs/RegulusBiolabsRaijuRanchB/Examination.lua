--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[55 Investigation]
--If 55 is investigating, then run a different script.
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
if(iChristineLeadingParty == 0.0) then
    LM_ExecuteScript(fnResolvePath() .. "Examination55.lua", sObjectName)
    return
end

--[Exits]
--[Objects]
if(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Defrag -[SOFTBLOCK] er, sleeping logs for the human in this room.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I have a vague memory of having difficulty getting to sleep as a human.[SOFTBLOCK] That's about it, though.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Switching to defragmentation mode is much easier.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I don't miss it in the slightest!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Pog24:: Why is it absolutely necessary to do all this variable-manifold calculus?[SOFTBLOCK] It's not like we're going to use it ever!'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'LesKing:: Because if you don't, you'll get shearing duty.[SOFTBLOCK] I don't like it either.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Pog24:: All this stuff is useless.[SOFTBLOCK] I'm probably going to get assigned as a labour unit anyway.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'LesKing:: I know, babe.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Manifold calculus isn't useless, but I guess kids wouldn't know that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's a common complaint among students.[SOFTBLOCK] I heard it a lot, too.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] What was your counterargument?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 'You need to know enough about every topic to defend yourself, otherwise you'll fall for confidence jobs.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Did it work?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] No, never![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Kids who grow up rich aren't any smarter than anyone else.[SOFTBLOCK] A fool and her money are soon parted.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Earth is just loaded down with wise sayings, isn't it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Too many are pertaining to war or cheating, though...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Tolky:: Hey, you hear about that petroleum girl thing?[SOFTBLOCK] You think it's true?'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'TheBlat:: Of course not, that's so stupid.[SOFTBLOCK] You pour a slime into a drone unit's chassis and seal it up, then pretend you made something new?'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Tolky:: It can transform people.[SOFTBLOCK] That means it counts.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'TheBlat:: Into what?[SOFTBLOCK] More drone units?[SOFTBLOCK] They already do that.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Tolky:: I heard the slime turned all black.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'TheBlat:: It probably absorbed some of the rubber chassis.[SOFTBLOCK] Stop being stupid.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Tolky:: Come on, have some fun for once.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'TheBlat:: You keep this up, you'll become a drone unit come graduation.[SOFTBLOCK] Idiot.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Tolky:: Open your mind to new experiences!'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'TheBlat:: What?[SOFTBLOCK] I'm the one who's reading books and getting day passes![SOFTBLOCK] You spend all your time in the hot tubs!'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It goes on like this, Sophie.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] These humans get along so well.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The unit assigned to this room left her homework on the terminal.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ooh, made a mistake here.[SOFTBLOCK] I'll just leave a little correction...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] She made a mistake?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It wouldn't be a mistake if this was an R-9 chipset, but this is a Mu7-i/o chipset.[SOFTBLOCK] You know, the ones they stopped making when they started all those fires?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Then the homework assignment should be [SOFTBLOCK]'How to throw out a Mu7-i/o board',[SOFTBLOCK] not how to fix one![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The curriculum is laughably outdated?[SOFTBLOCK] Some things never change...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The class curriculum is here.[SOFTBLOCK] Seems they were going over...[SOFTBLOCK][EMOTION|Christine|Scared] Oh![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] What?[SOFTBLOCK] Something wrong?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I just realized -[SOFTBLOCK] you call it something else here![SOFTBLOCK] Silly me![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] On Earth, we called it Hawking Radiation, after the physicist who first theorized it.[SOFTBLOCK] What a brilliant fellow he was -[SOFTBLOCK] and English, I must add.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oh, you mean virtual pair asymmetry radiation?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I suppose it'd be a little ridiculous to name scientific concepts after the unit who discovers it.[SOFTBLOCK] Unit 392001 Radiation doesn't have the same ring to it.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Shall we make an appointment to see the principal, Sophie?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I don't think that's necessary.[SOFTBLOCK] Anything interesting?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm, seems the principal has lots of one-on-one time with each student.[SOFTBLOCK] There are regular schedules for each of them and frequent teacher appointments.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's hard to say if this is good without knowing the contents of the meetings.[SOFTBLOCK] I've found that the best results come from attention to the individual.[SOFTBLOCK] Class sizes over twenty mean the teacher just can't spend enough time with each student.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I think the largest classes here are twelve, Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Colour me pleasantly surprised, then.[SOFTBLOCK] Teachers need independence and resources to teach.[SOFTBLOCK] Give them those.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Are we snooping around the defragmentation logs of the principal for a reason, Christine?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I must make sure she is doing a good job.[SOFTBLOCK] The future of Regulus City depends on more than just its freedom -[SOFTBLOCK] it needs educated citizens to maintain that freedom.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Did you rehearse that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Politics does not happen on its own, it is a culture, like a garden.[SOFTBLOCK] And like any garden, without proper care, it will die.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It seems that the Lord Units here teach independent thought rather than blind obedience.[SOFTBLOCK] Look.[SOFTBLOCK] The principal set philosophical tracts to be downloaded during defragmentation.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Breeding program humans do generally become Lord Units.[SOFTBLOCK] But, what philosophies are being downloaded?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm...[SOFTBLOCK] I don't know these names, but they are predominantly concerned with metaphysics and not ethics.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Still, teaching how to think can only be good for the city.") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "TerminalH") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Huh, look at this.[SOFTBLOCK] 'Harpy Partirhuman Spread and Habits'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Learning about Pandemonium?[SOFTBLOCK] Let's see...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 'Harpies, which have many names depending on the region, are easily the most widespread of the partirhuman species of Pandemonium.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 'While only rough estimates of total population exist, they are expected to be in the top ten most populas species.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 'The likely candidate for most populous is the common slime girl, but harpies make up for their lack of population with sheer range.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 'Harpies can survive nearly anywhere, including deserts, ice caps, or frozen mountain ranges.[SOFTBLOCK] They typically have few competitors due to their ability to fly and locate food far from their nest colonies.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 'The harpy colonization pattern is common to all of the subvarieties.[SOFTBLOCK] Once a colony has enough members, the youngest members are compelled to leave and start their own colonies.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 'The colony is usually established in a location that can only be reached with flight, though plains harpies are known to construct simple wooden walls and obstacles to deter invaders.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 'From there, the colony locates isolated humans and attacks them, dragging them to their nest colony for transformation.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Oh![SOFTBLOCK] I understand![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] This is for abductions training![SOFTBLOCK] Harpies select humans based on their skillsets, look![SOFTBLOCK] Just like we do![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We learned from the best.[SOFTBLOCK] But harpies don't breed their own humans, so we've superceded them there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Maybe we should form an alliance![SOFTBLOCK] Ha ha![SOFTBLOCK] Imagine that!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalI") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Storage logs...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This place is a mess, but we're not much better in Sector 96 are we?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] It's not our fault![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] There's not much point in organizing our inventory if we're just shipping it back out tomorrow, anyway.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Meanwhile, I think the Raiju Ranch just doesn't have enough storage space.[SOFTBLOCK] You need space to categorize things.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] They could just build another storage shed though, right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Maybe the administrator is just really messy and doesn't care.[SOFTBLOCK] I know the type.[SOFTBLOCK] There's no getting through to them.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalJ") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Athletics records...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Look![SOFTBLOCK] They allow everyone to compete, even the Raijus and Biological Services![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But they don't have mixed competitions for swimming.[SOFTBLOCK] Not with the Raijus, anyway.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Gee.[SOFTBLOCK] I wonder.[SOFTBLOCK] Why.[SOFTBLOCK] Hee hee!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalK") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The dorms are located in the arcology above us.[SOFTBLOCK] This terminals lists which room each individual is housed in.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hmm, notice this?[SOFTBLOCK] Each year is housed on a given floor, and you move down a floor each year.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] That's because the creche is on the top floor, for the little baby humans.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Baby...[SOFTBLOCK] humans...[SOFTBLOCK] something I haven't thought about in a long time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Christine, when all this is over...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ...[SOFTBLOCK] Yes?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Uh, nevermind.[SOFTBLOCK] Forget I said anything.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Extra silverware.[SOFTBLOCK] Forks, knives, spoons.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Silverware?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, yes.[SOFTBLOCK] Did I say something wrong?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I just find it odd that you said silver.[SOFTBLOCK] They're not made of silver, they're made out of steel.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Must be my Earth dialect creeping in![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] On Earth, people made utensils out of silver because it is chemically inert, so the food didn't taste different if you ate with it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Then again, only rich people could afford that.[SOFTBLOCK] Poorer people had to eat with their hands, or wooden implements...[SOFTBLOCK] It varied based on where you were and what time you lived in...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The reason I know this is my family had silverware dating back six-hundred years, acquired by our ancestors.[SOFTBLOCK] Probably stolen from someone they killed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I didn't mean to upset you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Don't worry about it.[SOFTBLOCK] I shouldn't get upset about it, it doesn't mean anything.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Just a little way how our language shapes us, I suppose.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Art supplies?[SOFTBLOCK] Here?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Huh.[SOFTBLOCK] I figured you'd just draw with your PDU.[SOFTBLOCK] You can get a tablet extension.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] There's something to be said for putting a real pencil on a real page, though.[SOFTBLOCK] Besides, there's lots of wood products here in the biolabs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And I don't know about you, but I wouldn't want all the constant problems that those tablet extensions cause.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] What do you mean?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The constant driver malfunctions, stuck pixels, poor sensitivity, having to reinstall a terminal's OS because of a dumb update...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Did someone send you a broken tablet for repairs?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I spent two whole days futzing with it before I had to give up...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] Pencil and paper it is![SOFTBLOCK] Sometimes, simplest is best.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Sports equipment?[SOFTBLOCK] It smells pretty bad...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's the smell of sweat.[SOFTBLOCK] Humans that are overheating secrete water that evaporates, cooling the skin.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But isn't sweat just water, then?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Organics are also coated in micro-organisms that eat their dead skin cells or other waste.[SOFTBLOCK] It's their chemical processes that make sweat smell.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That, and some excess toxin is secreted in sweat.[SOFTBLOCK] A human who eats a lot of garlic might smell like garlic when they sweat.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Interesting.[SOFTBLOCK] Could you please repeat that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] To whichever lug nut put this sports equipment in here without washing the sweat off?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Ha ha![SOFTBLOCK] Of course![SOFTBLOCK] I'll have my PDU send a mail right away!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Huh, suction cups.[SOFTBLOCK] I wonder what they use these for...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (I think I know, but maybe I shouldn't tell Sophie just yet...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh look Christine, RJ-44 Pistons.[SOFTBLOCK] And they look broken down![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're not here to fix things, Sophie![SOFTBLOCK] Leave that to the repair units who work here![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] My goodness, do I get excited at the prospect of fixing things?[SOFTBLOCK] What's wrong with me?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] There's nothing wrong with loving your job, Sophie.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesF") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's a box full of portable computers here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hmm, maybe these are given out to visitors?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Crud, the hard drives were wiped.[SOFTBLOCK] No snooping for secrets.[SOFTBLOCK] Oh well.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Homework assignments to do before next class are printed on the screen.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Why would you require the students to do work after class is over?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] To accustom them to a life dominated by work, I'd say...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The last slides in use were documenting optical illusions.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hmm, isn't that interesting?[SOFTBLOCK] Despite our optical sensors being hundreds of times more sensitive than a human eye, golems still see the same optical illusions.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's because, even when you're metal, you're still a person under there.[SOFTBLOCK] It's in your brain and the way you think, not in your eye.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Or it's a software bug we could fix.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] But why would you want to?[SOFTBLOCK] Optical illusions are so much fun to play with!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm?[SOFTBLOCK] The RVD has photos of the students here displaying.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Along with a list of grades and recommendations from their teachers.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'd say the administrator here is doing due diligence.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, but what part of the anatomy are the photos focused on?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] The...[SOFTBLOCK] chest?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] That's the middle of the human, isn't it?[SOFTBLOCK] What are you getting at?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh, nothing.[SOFTBLOCK] Just pointing something out.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "PokerTable") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Looks like the staff here were playing cards.[SOFTBLOCK] I'm not familiar with what kind of game, though.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] This is Dog.[SOFTBLOCK] It's a simple card game of who has the best hand, but if you lose the hand, you have to bark like a dog.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Pretty high-stakes stuff.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And how do you know that?[SOFTBLOCK] There weren't any card games in my database when I last synchronized it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Sometimes I'd play in the basement of the repair bay when everyone else got off work.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] That stopped when we got a new Lord Golem, but that's because I was busy doing...[SOFTBLOCK] other things...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You should have asked me to play![SOFTBLOCK] I'd love to lose horribly and bark like a dog!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The human seems to have fallen asleep.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[VOICE|HumanF0] The water is so warm... mmmmm...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[VOICE|HumanF1] Nothing like a good soak in the hot tubs after a hard day of running around...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Those two girls are talking about something illicit.[SOFTBLOCK] Conveniently, there are no microphones in the pool.[SOFTBLOCK] Clever.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FabricatorA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A fabricator bench with a number of scuffs and cut marks on the top.[SOFTBLOCK] Fortunately, the humans in training have safety oversight and there's no blood.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FabricatorB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The fabricator has several non-standard parts that are newer than the rest.[SOFTBLOCK] Those parts are used to assemble pulse weaponry.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BackersTerminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A list of all the units who have made important contributions to the Raiju Ranch.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] None of this would have been possible without them! Great job![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Laying it on thick, Christine?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Gotta keep them happy, right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But they're not here, and they can't hear us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't be so sure of that...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "BackersRaiju", "Leave") ]])

--Work Terminal:
elseif(sObjectName == "WorkTerminal") then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/WorkTerminal/Execution.lua")

--Milking.
elseif(sObjectName == "Milkers") then

    --Variables.
    local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
    local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
    local iBiolabsGotMilked      = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsGotMilked", "N")
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])

    --No idea what these are:
    if(sSpokeToSecondBoobGirl == "Nobody") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What a curious machine.[SOFTBLOCK] Any idea what they are?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'm sure somebody around here can tell us.") ]])
    
    --Oh ho!
    else
    
        --Golem:
        if(sChristineForm == "Golem") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A milking machine...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] If you want to try it out, you'll need to come back in your squishy form.") ]])
            
        --Raiju:
        elseif(sChristineForm == "Raiju") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A milking machine...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But not if you are in your electrical form, Christine.[SOFTBLOCK] For you, it's just a comfy chair.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I don't think Ms. Primrose would like you blowing up her ranch.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] And damage such vital equipment?[SOFTBLOCK] Perish the thought!") ]])
        
        --Human:
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A milking machine...[BLOCK][CLEAR]") ]])
            if(iBiolabsGotMilked == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Want to try it out, dearest?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] W-[SOFTBLOCK]well...[BLOCK]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Think you've got it in you for another go?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Somehow, I think I could survive it.[SOFTBLOCK] Should I?[BLOCK]") ]])
    
            end

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
            fnCutsceneBlocker()
        end

    end

--[Milking Answers]
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
    
    VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsGotMilked", "N", 1.0)
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Fire it up, Sophie![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Yippee!") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Run the cutscene.
    LM_ExecuteScript(fnResolvePath() .. "Milking Scene.lua")
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] ...[SOFTBLOCK] Phew...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] So smooth and creamy...[SOFTBLOCK] I want more...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I would love to, but give me a few minutes to recover.[SOFTBLOCK] This is more tiring than fighting for my life![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Okay![SOFTBLOCK] Just come back if you want to go again!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Next time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I can't wait...") ]])

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end