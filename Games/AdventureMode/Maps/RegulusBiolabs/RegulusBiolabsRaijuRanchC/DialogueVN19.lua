--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
    local iMetJX101              = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N")
    local iMetGemcutter          = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N")
    local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
    
    --Common.
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	
	--Haven't met her yet.
	if(iMetGemcutter == 0.0) then
        
        --Set the flag to 2.0 to indicate we've met her in the biolabs.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N", 2.0)
        
        --Christine is leading the party.
        if(iChristineLeadingParty >= 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] What have we here but the battle-scarred leader of the resistance, perhaps on a reconnaisance mission?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] My name is VN-19, gemcutter extrordinaire.[SOFTBLOCK] You'll find no finer a cutter on this moon - or any other![BLOCK][CLEAR]") ]])
            if(iMetJX101 >= 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hello, VN-19.[SOFTBLOCK] Are you perchance an associate of JX-101?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Associate?[SOFTBLOCK] My friend, she is one of my biggest customers![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Unless you think an army can run without peerlessly cut gems?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] ...[SOFTBLOCK] Probably?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Yeah maybe.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] *cough*[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] You a friend of 'ers, love?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Of course![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Stupendous![SOFTBLOCK] As I said, I am a gemcutter.[SOFTBLOCK] If you need a gem cut, then that is my raison d'etre![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Great![SOFTBLOCK] Nice to meet you, VN-19![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I love meeting new robots.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Shall we get down to business then?[BLOCK]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hello, VN-19.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Pleased to meet you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Stupendously met![SOFTBLOCK] As I said, I am a gemcutter.[SOFTBLOCK] If you need a gem cut, then that is my raison d'etre![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] We're repair units.[SOFTBLOCK] That's our -[SOFTBLOCK] whatever you said![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] I 'unno love.[SOFTBLOCK] Just a thing I 'eard some lord unit say.[SOFTBLOCK] Dunno what it means, sounds fancy.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It means 'reason for existing'.[SOFTBLOCK] It's French.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] *cough*[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] So![SOFTBLOCK] Gems![SOFTBLOCK] Shall I cut one for you?[BLOCK]") ]])
            end
        
        --55 is leading the party.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Regulus is the only moon of Pandemonium and the only inhabited moon in the Belarus system.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your statement is needlessly elaborate.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] [SOFTBLOCK]*sniff*[SOFTBLOCK] Yeah that's called a flourish, love.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Hey, VN-19.[SOFTBLOCK] How you been?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] My my.[SOFTBLOCK] SX-399?[SOFTBLOCK] I barely recognize you![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] So the rumours were true![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] I got me a custom-made body all thanks to Cutie-5 here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Hello, friend of SX-399.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Cutie-5?[SOFTBLOCK] Huh.[SOFTBLOCK] Coulda swore 'er name was summat else.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] My designation is - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] So![SOFTBLOCK] My friends, are you in need of the services of a gemcutter?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Oh yeah![SOFTBLOCK] VN-19 here cuts all the gems for -[SOFTBLOCK] well, the Steam Droids.[SOFTBLOCK] I think she's the only gemcutter we have.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] It's a rare skill.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Are we in need of any of her services?[BLOCK]") ]])
        end
    
    --Met her before in the basement of Regulus City:
    elseif(iMetGemcutter == 1.0) then
        
        --Set the flag to 2.0 to indicate we've met her in the biolabs.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N", 2.0)
        
        --Christine is leading the party.
        if(iChristineLeadingParty >= 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Ha ha![SOFTBLOCK] Stupendous![SOFTBLOCK] The former maverick, now a revolutionary![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] VN-19![SOFTBLOCK] Great to see you again![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You know each other?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Ah.[SOFTBLOCK] Ah ah ah.[SOFTBLOCK] What a sight is bestowed upon me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Who is this enchanting machine?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is Unit 499323, Maintenance and Repair, Sector 96.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Call me Sophie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] A name as beautiful as her countenance.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh my![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't be nervous, dearest![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *smooch*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] H-[SOFTBLOCK]hee hee![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] So, Sophie.[SOFTBLOCK] Is she as fierce a lover as she is a warrior?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] .............[SOFTBLOCK] HEE HEE!!![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Okay, better dial it back, VN-19![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] As you wish.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I really didn't expect to see you here, though.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Wait, how did you get into the Raiju Ranch?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Well, I walked here.[SOFTBLOCK] And did a bit of climbing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] If you know where you're going in the undercity, you can get nearly anywhere without crossing a security unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Then all I had to do was crawl up a disposal pipe.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] [SOFTBLOCK]*sniff*[SOFTBLOCK] Why you lookin' at me like that?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Cry] No reason.[SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] No reason![SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Smell comes right out, loves.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Anyway, I did it all because I heard you were coming this way, and you need gems.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] So shall we get down to business?[BLOCK]") ]])
            
        --55 is leading the party.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Ha ha![SOFTBLOCK] Stupendous![SOFTBLOCK] The former maverick, now a revolutionary![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Greetings, VN-19.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] You've met?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] That voice...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Is that the sweet song of SX-399?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] The one and only![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Ha ha![SOFTBLOCK] Splendiferous![SOFTBLOCK] The rumours of your repair were true![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] I've known her since she was only up to JX-101's knee, you know.[SOFTBLOCK] I am so glad to see you fixed and healthy![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] It was all thanks to Cutie-5 here.[SOFTBLOCK] How did you meet each other?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] We have made use of her services in the past.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will note that I did not expect to meet you here, of all places.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] ...[SOFTBLOCK] Yeah how'd you get in here, anyway?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Well, I walked here.[SOFTBLOCK] And did a bit of climbing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] If you know where you're going in the undercity, you can get nearly anywhere without crossing a security unit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Then all I had to do was crawl up a disposal pipe.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] [SOFTBLOCK]*sniff*[SOFTBLOCK] Why you lookin' at me like that?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Oh.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Oh my.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That explains the smell.[SOFTBLOCK] You may wish to wash yourself before continuing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Smell comes right out, loves.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Anyway, I did it all because I heard you were coming this way, and you need gems.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] So shall we get down to business?[BLOCK]") ]])
        end
    
    --Repeat, go to dialogue decisions quickly.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] You return![SOFTBLOCK] Which of my talents are you in need of?[BLOCK]") ]])
    end
	
    --Decision setup.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's get to the gems!\", " .. sDecisionScript .. ", \"Gemcutting\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I'm good, thanks.\",  " .. sDecisionScript .. ", \"Bye\") ")
    fnCutsceneBlocker()

--Activate gemcutting mode.
elseif(sTopicName == "Gemcutting") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Marvellous![SOFTBLOCK] Let's get started!") ]])
    
	--Run the shop.
	fnCutsceneInstruction([[ AM_SetShopProperty("Show", "VN-19's Gem Shop", gsRoot .. "CharacterDialogue/SprocketCity/GemShopSetup.lua", gsRoot .. "CharacterDialogue/SprocketCity/GemShopTeardown.lua") ]])
	fnCutsceneBlocker()

--Talk.
elseif(sTopicName == "Talk") then

--Goodbye.
elseif(sTopicName == "Bye") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "VN-19:[E|Neutral] Until we meet again!") ]])
	
end