--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[55 Investigation]
--If 55 is investigating, then run a different script.
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
if(iChristineLeadingParty == 0.0) then
    LM_ExecuteScript(fnResolvePath() .. "Examination55.lua", sObjectName)
    return
end

--[Exits]
--[Objects]
if(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This terminal controls the screen outside.[SOFTBLOCK] It is currently in intermission mode.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This RVD plays the same videograph as the movie outside.[SOFTBLOCK] Is it so the staff can watch, too?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Fizzy Pop! flavour for oilmakers'.[SOFTBLOCK] Anyone who drinks that will probably be blown clear through the roof.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Sign") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's a lot of different snacks.[SOFTBLOCK] But what are these symbols next to their names?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] That one looks like a human, that one looks like a golem...[SOFTBLOCK] and that yellow spiky one is a Raiju?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, I get it![SOFTBLOCK] This is who the snacks are safe for![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Are you telling me, right now, that humans don't like eating shredded plastic?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] As hard as that is to believe...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Now that's just sad.[SOFTBLOCK] Shredded plastic on top of powdered gypsum is my idea of heaven...") ]])
    
elseif(sObjectName == "Snacks") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "HumanF1", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Can I get you any snacks?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] What is a chock-ho-lit?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] One chocolate chunk for my tandem unit, please![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] *Num*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Smooth, chewy, melty, delicious...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Chocolate is the perfect treat for organic and synthetic alike.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] We grow it here in the biolabs![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Christine, I hereby order you to overthrow the administration so we can all have chocolate.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'm now officially addicted.[SOFTBLOCK] I need more.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Yeah, you and everyone else.[SOFTBLOCK] I love the stuff.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (What monster have I unleashed?[SOFTBLOCK] The world trembles before steps of a chocoholic...)") ]])
    
elseif(sObjectName == "Speaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (These speakers play the videograph's audio with a detachable receiver.[SOFTBLOCK] You can also wire a golem's auditory receptors to receive it directly.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Screen") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An enormous digital display on a special platform.[SOFTBLOCK] The videograph is currently in intermission and will resume soon.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bathroom") then

    --Variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Wow, how long has it been since I've seen a bathroom.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Don't miss them, nope.[SOFTBLOCK] Not one bit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Bathroom...[SOFTBLOCK] Oh![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Organics dispose of waste in these![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You should not be smiling when you say that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oh don't tell me you still have the essence fallacy in your central processor.[BLOCK][CLEAR]") ]])
    if(sChristineForm == "Human") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I mean brain.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's a central processor, it's just wetware right now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's quite reassuring to know that, once reprogrammed, a human brain is just as efficient as a golem processor.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Except you still subscribe to the essence fallacy.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Which one is that again?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] The belief that a material imparts an invisible essence on contact.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] If biological waste disgusts you, just wash your chassis.[SOFTBLOCK] There's nothing dirty after you're washed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But you seemed excited about the very concept.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Because this is how the world will look once the Cause of Science remakes it.[SOFTBLOCK] Indoor plumbing![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Those poor humans on Pandemonium live in squalor and disease.[SOFTBLOCK] We invented pressurized pipes.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well as long as you're excited about the engineering...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] What else is there to be excited about?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uhhhh...[SOFTBLOCK] [EMOTION|Christine|Smirk]Converting all humans into perfect robots when we bring the Cause of Science to them?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, I see what you were getting at.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (Phew![SOFTBLOCK] Saved it!)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end