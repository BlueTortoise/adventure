--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This terminal controls the screen outside.[SOFTBLOCK] It is currently in intermission mode.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This RVD plays the same videograph as the movie outside.[SOFTBLOCK] Is it so the staff can watch, too?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Fizzy Pop! flavour for oilmakers'.[SOFTBLOCK] Anyone who drinks that will probably be blown clear through the roof.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Sign") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The sign lists snacks available for purchase.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Mostly stuff for organics.[SOFTBLOCK] What's the little yellow symbol?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Likely it indicates it is safe for Raijus to eat.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I guess giving a Raiju some caffeine will probably cause an explosion.[SOFTBLOCK] Gotta be careful.") ]])
    
elseif(sObjectName == "Snacks") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "HumanF1", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] May I get you anything, Command Unit?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No thank you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Oh, you should try some silcrete-toffee![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] You see it in the mines every now and then, caused by subsurface pipes breaking and building up dissolved silcretes.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Sprinkle some salt on it and it's the bee's knees![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] I can't eat it because it's literally harder than my teeth, but the inorganic units say it's great![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will pass.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Please?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Will it make you happy?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] You bet![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Very well...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] *chomp*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Do you like it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The interplay of hard and soft materials combined with the salt produces an enjoyable taste.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Human:[E|Neutral] Uhhh....[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Trust me, buddy.[SOFTBLOCK] That's her way of saying she loves it.") ]])
    
elseif(sObjectName == "Speaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (These speakers play the videograph's audio with a detachable receiver.[SOFTBLOCK] You can also wire a golem's auditory receptors to receive it directly.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Screen") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An enormous digital display on a special platform.[SOFTBLOCK] The videograph is currently in intermission and will resume soon.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bathroom") then

    --Variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Bathrooms, where organic units remove waste to be recycled.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You know Steam Droids still do that, too?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Because of flaws in our lubrication, grease tends to build up inside the chassis.[SOFTBLOCK] It has to be flushed and dumped.[SOFTBLOCK] Smells nasty.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You said 'our'.[SOFTBLOCK] Is that still the case for your new body?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I guess not, the auto-repair nanites do that work now.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I suppose I never noticed I haven't had to go in so long.[SOFTBLOCK] You just forget about it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] But I'm still a Steam Droid at heart, toutse.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Noted.") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end