--[Shop Setup]
--Script that builds what items this vendor is willing to sell you.

--Unlimited quantity items.
AM_SetShopProperty("Add Item", "Adamantite Powder", true)
AM_SetShopProperty("Add Item", "Yemite Gem", true)
AM_SetShopProperty("Add Item", "Rubose Gem", true)
AM_SetShopProperty("Add Item", "Glintsteel Gem", true)
AM_SetShopProperty("Add Item", "Ardrite Gem", true)
AM_SetShopProperty("Add Item", "Blurleen Gem", true)
AM_SetShopProperty("Add Item", "Qederite Gem", true)