--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "ToGammaD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGammaD", "FORCEPOS:59.0x17.0x0")
    
elseif(sObjectName == "ToTransitB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsTransitB", "FORCEPOS:8.0x4.0x0")
    
elseif(sObjectName == "SignA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Please enjoy the recreational facilities in the Gamma and Delta Laboratories![SOFTBLOCK] If you have any questions or concerns, let a staff member know.[SOFTBLOCK] We'd love to help!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Please leave your locker card with the attendant.[SOFTBLOCK] They are easy to lose in the labs.[SOFTBLOCK] Enjoy your stay!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Loose articles should be placed in a storage locker.[SOFTBLOCK] If you desire your clothes be cleaned and pressed, please inform an attendant.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Complimentary beverages include oil, ChemFuel, Fizzy Pop!, and our special beer and wines brewed right here in the Beta Laboratories!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computers") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Laptops, both powered off.[SOFTBLOCK] Probably belonging to the Lord Golems milling about, but not worth hacking.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Desk") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A service desk, but there are no Slave Units around right now.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Magazines") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hardcopy magazines advertising various sporting activities, including descriptions of the rules and scheduling protocols.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A big message says 'All outbound and inbound trams are cancelled. Please stand by.')") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end