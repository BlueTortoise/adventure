--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Encounter") then

    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local iSawTransitEncounter = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawTransitEncounter", "N")
    if(iSawTransitEncounter == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawTransitEncounter", "N",1.0)
        
        --Spawn NPCs.
        TA_Create("2856")
            TA_SetProperty("Position", 15, 14)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/56/", true)
        DL_PopActiveObject()
        TA_Create("DroneA")
            TA_SetProperty("Position", 14, 14)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
        DL_PopActiveObject()
        TA_Create("DroneB")
            TA_SetProperty("Position", 13, 14)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
        DL_PopActiveObject()
        
        --Reposition NPCs.
        fnCutsceneTeleport("GolemA", 16.25, 14.50)
        fnCutsceneFace("GolemA", -1, 0)
        fnCutsceneFace("GolemJ", 0, -1)
    
        --Party unfolds.
        fnCutsceneMove("Christine", 5.25, 13.50)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneMove("55", 5.25, 14.50)
        fnCutsceneFace("55", 1, 0)
        fnCutsceneMove("Sophie", 4.25, 13.50)
        fnCutsceneFace("Sophie", 1, 0)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 4.25, 14.50)
            fnCutsceneFace("SX399", 1, 0)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Camera moves.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0) --Default is 5.0
            CameraEvent_SetProperty("Focus Position", (16.25 * gciSizePerTile), (14.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2856", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "LatexDrone", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "LatexDroneB", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] But what will happen to us?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] None of my concern.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] The other Lord Golems are frightened and on the verge of panic.[SOFTBLOCK] Surely - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] None.[SOFTBLOCK] Of.[SOFTBLOCK] My.[SOFTBLOCK] Concern.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] My responsibilities are to the city, not a collection of its most worthless citizens.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Solve your own problems.[SOFTBLOCK] Stand aside, Lord Unit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Milady, please...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] STAND ASIDE.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMoveFace("GolemA", 16.25, 13.50, 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("2856", 26.25, 14.50)
        fnCutsceneMove("2856", 26.25, 15.50)
        fnCutsceneMove("DroneA", 26.25, 14.50)
        fnCutsceneMove("DroneB", 25.25, 14.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorC") ]])
        fnCutsceneBlocker()
        fnCutsceneMove("2856", 26.25, 19.50)
        fnCutsceneMove("DroneA", 26.25, 14.50)
        fnCutsceneMove("DroneA", 26.25, 18.50)
        fnCutsceneMove("DroneB", 26.25, 14.50)
        fnCutsceneMove("DroneB", 26.25, 17.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneTeleport("2856", -100, -100)
        fnCutsceneTeleport("DroneA", -100, -100)
        fnCutsceneTeleport("DroneB", -100, -100)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera moves back to the party.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0) --Default is 5.0
            CameraEvent_SetProperty("Focus Actor Name", "55")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] She's quite a...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] A bad thing.[SOFTBLOCK] Bad word.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We should do something.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Allow me.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] I don't think she meant 'shoot them'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not my intention.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Huh.[SOFTBLOCK] Okay.[SOFTBLOCK] Work your magic.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Go right ahead.") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("55", 15.25, 14.50)
        fnCutsceneMove("55", 15.25, 13.50)
        fnCutsceneFace("55", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneFace("GolemA", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What seems to be the problem here?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Command -[SOFTBLOCK] Unit?[SOFTBLOCK] Weren't you just here?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Incorrect.[SOFTBLOCK] That was my twin sister, Unit 2856.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Then that makes you...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Hello, Head of Security Unit 2855![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Former Head of Security.[SOFTBLOCK] I have resigned my post.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] You -[SOFTBLOCK] you can do that?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am here to assist you in whatever way I can.[SOFTBLOCK] What is the situation?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] But -[SOFTBLOCK] I - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are the Lord Golem in charge of this station.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] No, I'm in charge of manufacturing in Sector 82.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are currently facing an emergency that is wholly unprecedented.[SOFTBLOCK] You are the golem who stepped forward.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The others did not.[SOFTBLOCK] You are therefore the first choice for leader.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I'm no security unit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] None of you are.[SOFTBLOCK] Therefore, you will need to improvise.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I have important tasks elsewhere, but I can take a few minutes to give you some useful advice.[SOFTBLOCK] Will you listen?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Uh -[SOFTBLOCK] of course, Command Unit![SOFTBLOCK] Thank you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I'm not sure if I am up to the task...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your options are limited.[SOFTBLOCK] You will not be able to wait for rescue.[SOFTBLOCK] Take your fate into your own hands.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I believe in you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] All right.[SOFTBLOCK] What -[SOFTBLOCK] what should I do first?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] First, organize the other units and get a headcount.[SOFTBLOCK] Take stock of your personnel and skillsets.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Judging by your outfits, I am assuming you were diverted to this transit station after the gala?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Yes.[SOFTBLOCK] I was here with my tandem unit, when we heard explosions.[SOFTBLOCK] Then, these other golems came from below.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] They said the tunnels have collapsed and they were diverted here.[SOFTBLOCK] It seems we can't use the transit lines until they are cleared.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes, as I said, there is an emergency.[SOFTBLOCK] Invaders are in the facility, and security is busy elsewhere.[SOFTBLOCK] Do not allow your units to come into contact with them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Select those that are most physically capable.[SOFTBLOCK] Organize them into teams of two.[SOFTBLOCK] Arm them with whatever you have.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Blunt weapons will work, sharp weapons are preferred.[SOFTBLOCK] You can snap the sports equipment here for makeshift weapons.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] You want us to [SOFTBLOCK]*fight*[SOFTBLOCK] these creatures?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Only if absolutely necessary.[SOFTBLOCK] Rather, you will need to set scouts on each entrance.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you are attacked, fall back and drop the door's deadbolts.[SOFTBLOCK] Barricade yourselves in.[SOFTBLOCK] Use force only if the barricades are breached.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Have an escape plan.[SOFTBLOCK] The Raiju Ranches are safe, but there are likely hostiles between you and there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Meanwhile, anyone with geological or transit skills should assess the tunnels below us, and put together a crew to attempt to clear them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] We are Lord Units.[SOFTBLOCK] We don't do...[SOFTBLOCK] work.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You do now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Either you work or you wait here for the invaders to find you.[SOFTBLOCK] Make your choice.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] But can't you - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] No help is coming, and it is unknown when the situation will be resolved.[SOFTBLOCK] You cannot make excuses.[SOFTBLOCK] Reality will not wait for you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I see.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] So that's it, then.[SOFTBLOCK] No help is coming.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] At least I have an idea of what to do now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Lastly, you must not allow the golems here to panic, or give them time to spread rumours.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Keep them occupied or they will gossip.[SOFTBLOCK] Gossip leads to paranoia, and that is a problem.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If they will not work, find an activity for them.[SOFTBLOCK] Simple games or even presentations on research topics.[SOFTBLOCK] Anything so they will be diverted from negative feedback loops.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I'm sure we have some researchers here.[SOFTBLOCK] And we can playback videographs on those computers.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Make sure they know the security forces are doing what they can, and they must merely hold out.[SOFTBLOCK] Give them hope.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I know you can do this.[SOFTBLOCK] Don't let me down.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Yes, yes, this will be trivial.[SOFTBLOCK] I am certain we can weather this crisis.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Thank you, Command Unit.[SOFTBLOCK] I'll begin taking inventory and delegating tasks, just like I do in my department.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Very good.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Uh, one other thing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Are you the same Unit 2855 who I've heard so much about?[SOFTBLOCK] The -[SOFTBLOCK] you can't be the same one.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] No I am not.[SOFTBLOCK] Not the same one at all.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("GolemA", 22.25, 13.50)
        fnCutsceneBlocker()
        fnCutsceneFace("GolemI", -1, 0)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorA") ]])
        fnCutsceneBlocker()
        fnCutsceneMove("GolemA", 22.25, 11.50)
        fnCutsceneMove("GolemA", 25.25, 11.50)
        fnCutsceneFace("GolemA", 0, -1)
        fnCutsceneMove("GolemI", 23.25, 14.50)
        fnCutsceneMove("GolemI", 20.25, 14.50)
        fnCutsceneMove("GolemI", 20.25, 15.50)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorB") ]])
        fnCutsceneFace("GolemB", 0, 1)
        fnCutsceneFace("GolemC", 0, 1)
        fnCutsceneMove("GolemI", 20.25, 17.50)
        fnCutsceneMove("GolemI", 19.25, 17.50)
        fnCutsceneBlocker()
        fnCutsceneFace("GolemG", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --55 moves.
        fnCutsceneMove("55", 5.25, 14.50)
        fnCutsceneFace("55", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Christine, watch out.[SOFTBLOCK] Someone's gunning for your leadership spot.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Thank you, but I am not interested in leadership.[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Wow, 55![SOFTBLOCK] Things might just work out for these golems![BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But aren't these lords our enemies?[SOFTBLOCK] Will organizing them just make them harder to defeat later?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is a possibility, though a remote one.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It would, however, be irresponsible to leave them in this state and simply hope that our enemies do not claim them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We do not need to feed Vivify's ranks.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And who knows?[SOFTBLOCK] Maybe they'll think they owe 55 and even join us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Excessively optimistic, Christine.[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] After watching you do that, I'd say there's cause for optimism.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] You've come a long way since we first met.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Th-[SOFTBLOCK]thank you.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But how'd you know what to say?[SOFTBLOCK] And when to say it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] I merely asked myself what an overly emotional fool would say in the same situation.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] She means me...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Yes.[SOFTBLOCK] I do.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Now, proceed.[SOFTBLOCK] We cannot know how long until this position is compromised.[SOFTBLOCK] I suggest we do not allow this work to be for naught.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move everyone onto the leader, and call the fold automatically.
        fnAutoFoldParty()
        fnCutsceneBlocker()
    
    end
end
