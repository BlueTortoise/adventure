--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Stargazing") then
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX399")
    local iSawStargazing  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawStargazing", "N")
    if(iSawStargazing == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawStargazing", "N", 1.0)
    
        --Movement.
        fnCutsceneMove("Christine", 8.25, 13.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMove("55", 7.25, 13.50)
        fnCutsceneFace("55", 0, 1)
        fnCutsceneMove("Sophie", 9.25, 13.50)
        fnCutsceneFace("Sophie", 0, 1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX399", 8.25, 12.50)
            fnCutsceneFace("SX399", 0, 1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Christine![SOFTBLOCK] A glass-ceiling garden so you can watch the stars![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's just beautiful...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Spending my time in the repair bay, I forgot that such wonders exist right here in our own city.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Can we just spend a moment looking out?[BLOCK][CLEAR]") ]])
        if(bIsSX399Present == true) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] I just need to remind everyone that this whole operation, from start to finish, has been an excuse to arrange a double-date.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Has it, now?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Of course![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Wait what are you saying?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am sorry, I did not mean to upset you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am just unsure.[SOFTBLOCK] You need to take this situation seriously.[SOFTBLOCK] We are in extreme danger at all times.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I get you.[SOFTBLOCK] Really, I do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am glad you are here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Yeah.[SOFTBLOCK] I know.[SOFTBLOCK] But you're kind of a grump.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] She's working on it![SOFTBLOCK] Ha ha ha![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] That's what really counts.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] ...[SOFTBLOCK] ya grumpalump.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] And you are a -[SOFTBLOCK] cog.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Cog?[SOFTBLOCK] That's all you had?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will check the network for insults at a later time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] And I will compose some of my own.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Of course.[SOFTBLOCK] And from now on, I'll wear cog as a badge of honor.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] No.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sorry Sophie, but she's right.[SOFTBLOCK] People are counting on us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I know, I know.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But when I went to dance with you, and I got to just -[SOFTBLOCK] just dance and nothing else?[SOFTBLOCK] I lost myself in it.[SOFTBLOCK] For once I was just me, having a good time, without worrying.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I hope there will be many more nights like that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There will.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (She spoke up before I did!)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It will take a great deal of work.[SOFTBLOCK] You know quite well the requirement of delayed gratification.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I can still look forward to it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Me too![SOFTBLOCK] But for now, just save a recording and replay it later.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] The stars will be the same next you look, I promise.") ]])
            
        end
        fnCutsceneBlocker()
    
        --Autofold party.
        fnAutoFoldParty()
        fnCutsceneBlocker()
    
    end
end
