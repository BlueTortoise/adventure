--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ElevatorL" or sObjectName == "ElevatorR") then

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Where shall I take the elevators to?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    if(sObjectName == "ElevatorL") then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"MainFloorL\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Basement 2F\", " .. sDecisionScript .. ", \"Basement2L\") ")
    else
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"MainFloorR\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Basement 2F\", " .. sDecisionScript .. ", \"Basement2R\") ")
    end
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
    fnCutsceneBlocker()

--Broken airlock.
elseif(sObjectName == "BrokenAirlock") then

	--Variables.
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")

	--If 55 is not present:
	if(iIs55Following == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Airlock internal door is malfunctioning.[SOFTBLOCK] Please contact the maintenance crew.") ]])
		fnCutsceneBlocker()
	
	--Otherwise:
	else

		--Variables.
		local i55ExplainedRegulusBrokenAirlock = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ExplainedRegulusBrokenAirlock", "N")

		--If 55 has not explained the airlock yet, do that now.
		if(i55ExplainedRegulusBrokenAirlock == 0.0) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/i55ExplainedRegulusBrokenAirlock", "N", 1.0)

			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Airlock internal door is malfunctioning.[SOFTBLOCK] Please contact the maintenance crew.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Seems we'll have to go around.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The door isn't actually broken, I've just reprogrammed it to behave like it is.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Thus I have a point of ingress into the city without any suspicion.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Is this how you got in when you first converted me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Correct.[SOFTBLOCK] I merely crossed the door circuits with the airlock above, and waited for you to open it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] How clever of you.[SOFTBLOCK] I never suspected a thing.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Would you like the door opened to the exterior?[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesOpen\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoClose\") ")
			fnCutsceneBlocker()

		--Otherwise, skip right to her asking you to open the door.
		else
		
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Would you like the door opened to the exterior?[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesOpen\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoClose\") ")
			fnCutsceneBlocker()

		end
	end
	
--Damaged terminal.
elseif(sObjectName == "DamagedTerminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A wrecked terminal.[SOFTBLOCK] Looks like some unit was punching it.[SOFTBLOCK] Wonder why?)") ]])
	fnCutsceneBlocker()

--Assorted junk lying around.
elseif(sObjectName == "Junk") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Junk, just left lying around down here.)") ]])
	fnCutsceneBlocker()

--Box full of crap.
elseif(sObjectName == "Box") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This crate is full of miscellaneous bolts and parts.[SOFTBLOCK] It all looks to be in bad condition.)") ]])
	fnCutsceneBlocker()

--Big box full of crap.
elseif(sObjectName == "BoxBig") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Supply crates covered in dust.[SOFTBLOCK] Looks like they're set to be delivered somewhere but the delivery queue is really long...)") ]])
	fnCutsceneBlocker()

--Vents.
elseif(sObjectName == "Vent") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](These must be the vents that 2855 is using to get around the sector...)") ]])
	fnCutsceneBlocker()

--Intercom.
elseif(sObjectName == "Intercom") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](There's no reply from the intercom.[SOFTBLOCK] Looks like it's broken.)") ]])
	fnCutsceneBlocker()

--Fizzypop!
elseif(sObjectName == "FizzyDrinks") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Fizzy Pop![SOFTBLOCK] A great way to recharge![SOFTBLOCK] The machine is actually in good condition, but I'm not low on power right now.)") ]])
	fnCutsceneBlocker()

--Fabricator.
elseif(sObjectName == "Fabricator") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The fabrication machine is intact but not receiving power.)") ]])
	fnCutsceneBlocker()

--Terminal.
elseif(sObjectName == "Terminal") then

	--Variables.
	local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	
	--Already met 55.
	if(iMet55InLowerRegulus == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This terminal is active, but the PDU cut its network access.[SOFTBLOCK] I can't use it for anything fun.)") ]])
		fnCutsceneBlocker()
	
	--Scene.
	else
		VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N", 1.0)
		LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Meeting 55 In Lower Regulus City/Scene_Begin.lua")
	end

--[Response Cases]
--Open the door to lower Exterior SD.
elseif(sObjectName == "YesOpen") then
	WD_SetProperty("Hide")

    --Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Christine is not in a vacuum-safe format.
	if(sChristineForm == "Human") then

        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Give me a moment to put my face on, and we'll be off.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()

    end

	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusExteriorSD", "FORCEPOS:14.5x4.0x0") ]])
	fnCutsceneBlocker()

--Close the dialogue.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")
    
--[Main Floor]
elseif(sObjectName == "MainFloorL" or sObjectName == "MainFloorR") then
	
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    local iSaw55sMemories = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    local iSaw55PostLRT   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N")
	WD_SetProperty("Hide")
	
	--Christine needs to change form.
	if(sChristineForm ~= "Golem") then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Better switch back to Golem.[SOFTBLOCK] Wouldn't want to draw unnecessary attention.)") ]])
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
	end
	
	--If 55 is following, she mentions she's leaving here.
	if(iIs55Following == 1.0) then
		
        --Post LRT scene:
        if(iSaw55sMemories == 1.0 and iSaw55PostLRT == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, please report to your normal work assignments.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I downloaded what I could from the core and set up a back door into the network.[SOFTBLOCK] I will review what footage I can.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Are you...[SOFTBLOCK] okay?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Report to your normal work assignments.[SOFTBLOCK] I will contact you when I have determined our next move.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55, come on...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do not waste processor cycles worrying.[SOFTBLOCK] Focus on maintaining your cover.[SOFTBLOCK] Move out.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...") ]])
            fnCutsceneBlocker()
            
            --Remove 55 from the following group.
            gsFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AC_SetProperty("Set Party", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "55")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
            
            --Flag for the next part of the cutscene.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N", 1.0)
            
            --Change map to Christine's quarters. This will fire the next part of the scenario.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.0x10.0x0") ]])
        
        --Normal:
        else
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] It is best if I'm not seen on the main floor cameras.[SOFTBLOCK] I'll be in touch.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            
            --Remove 55's sprite. She remains in the combat party for equipment reasons.
            gsFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AC_SetProperty("Set Party", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "55")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        end
	
	end
	
	--Force-facing flag. This will cause Christine to spawn with the correct facing.
	gi_Force_Facing = gci_Face_South
	
	--Transition.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	if(sObjectName == "MainFloorL") then
		fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:6.5x14.0x0") ]])
	else
		fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:11.5x14.0x0") ]])
	end
	fnCutsceneBlocker()

--[Basement Floor 2]
elseif(sObjectName == "Basement2L" or sObjectName == "Basement2R") then
	
	--Force-facing flag. This will cause Christine to spawn with the correct facing.
	gi_Force_Facing = gci_Face_South
	WD_SetProperty("Hide")
	
	--Transition.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	if(sObjectName == "Basement2L") then
		fnCutsceneInstruction([[ AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:27.5x13.0x0") ]])
	else
		fnCutsceneInstruction([[ AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:31.5x13.0x0") ]])
	end
	fnCutsceneBlocker()

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end