--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ElevatorL" or sObjectName == "ElevatorR") then

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Where shall I take the elevators to?)") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    if(sObjectName == "ElevatorL") then
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"MainFloorL\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Basement 1F\", " .. sDecisionScript .. ", \"Basement1L\") ")
    else
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"MainFloorR\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Basement 1F\", " .. sDecisionScript .. ", \"Basement1R\") ")
    end
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
    fnCutsceneBlocker()
    
elseif(sObjectName == "LadderD") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("LowerRegulusCityD", "FORCEPOS:23.0x9.0x0")
    
elseif(sObjectName == "DoorEN") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("LowerRegulusCityC", "FORCEPOS:32.0x16.0x0")
    
elseif(sObjectName == "DoorES") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("LowerRegulusCityC", "FORCEPOS:32.0x18.0x0")
    
elseif(sObjectName == "DoorW") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("LowerRegulusCityC", "FORCEPOS:21.0x8.0x0")

--[Examination]
elseif(sObjectName == "PipeTerminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This terminal is currently monitoring fluid pressure in these pipes.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "RedTerminal") then

    --Variables.
    local iLowerRegulusRedLock = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusRedLock", "N")
    
    --Activate.
    if(iLowerRegulusRedLock == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusRedLock", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (...[SOFTBLOCK] Got it.[SOFTBLOCK] That should release the Security-Red lockdown.)") ]])
        fnCutsceneBlocker()
        
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I've already lifted the red lockdown.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "BlueTerminal") then

    --Variables.
    local iLowerRegulusBlueLock = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusBlueLock", "N")
    
    --Activate.
    if(iLowerRegulusBlueLock == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusBlueLock", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (...[SOFTBLOCK] Got it.[SOFTBLOCK] That should release the Security-Blue lockdown.)") ]])
        fnCutsceneBlocker()
        
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I've already lifted the blue lockdown.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "ShelfA") then

    --Variables.
    local iLowerRegulusPartsA = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusPartsA", "N")
    
    --Haven't taken the item yet.
    if(iLowerRegulusPartsA == 0.0) then
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hmm, I could borrow a few parts from this shelf...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|TakeItem](Received Assorted Parts x1)") ]])
        fnCutsceneBlocker()
        
        --Flags.
		LM_ExecuteScript(gsItemListing, "Assorted Parts")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusPartsA", "N", 1.0)
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's nothing useful left on the shelf.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "ShelfB") then

    --Variables.
    local iLowerRegulusPartsB = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusPartsB", "N")
    
    --Haven't taken the item yet.
    if(iLowerRegulusPartsB == 0.0) then
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hmm, I could borrow a few parts from this shelf...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|TakeItem](Received Bent Tools x1)") ]])
        fnCutsceneBlocker()
        
        --Flags.
		LM_ExecuteScript(gsItemListing, "Bent Tools")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusPartsB", "N", 1.0)
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's nothing useful left on the shelf.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "ShelfC") then

    --Variables.
    local iLowerRegulusPartsC = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusPartsC", "N")
    
    --Haven't taken the item yet.
    if(iLowerRegulusPartsC == 0.0) then
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hmm, I could borrow a few parts from this shelf...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|TakeItem](Received Assorted Parts x1)") ]])
        fnCutsceneBlocker()
        
        --Flags.
		LM_ExecuteScript(gsItemListing, "Assorted Parts")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusPartsC", "N", 1.0)
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's nothing useful left on the shelf.)") ]])
        fnCutsceneBlocker()
    end

--[Locked Doors]
elseif(sObjectName == "LockedDoorS") then
    local iLowerRegulusBlueLock = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusBlueLock", "N")
    if(iLowerRegulusBlueLock == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|AutoDoorFail](Locked.[SOFTBLOCK] Looks like there's a Security-Blue terminal locking this door down.)") ]])
        fnCutsceneBlocker()
    else
        AL_SetProperty("Open Door", "LockedDoorS")
        AudioManager_PlaySound("World|AutoDoorOpen")
    end
    
elseif(sObjectName == "LockedDoorW") then
    local iLowerRegulusRedLock = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusRedLock", "N")
    if(iLowerRegulusRedLock == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|AutoDoorFail](Locked.[SOFTBLOCK] Looks like there's a Security-Red terminal locking this door down.)") ]])
        fnCutsceneBlocker()
    else
        AL_SetProperty("Open Door", "LockedDoorW")
        AudioManager_PlaySound("World|AutoDoorOpen")
    end
    
elseif(sObjectName == "LockedDoorFarW") then
    local iLowerRegulusRedLock = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusRedLock", "N")
    if(iLowerRegulusRedLock == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|AutoDoorFail](Locked.[SOFTBLOCK] Looks like there's a Security-Red terminal locking this door down.)") ]])
        fnCutsceneBlocker()
    else
        AL_SetProperty("Open Door", "LockedDoorFarW")
        AudioManager_PlaySound("World|AutoDoorOpen")
    end
    
elseif(sObjectName == "LockedDoorNW") then
    local iLowerRegulusRedLock = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusRedLock", "N")
    if(iLowerRegulusRedLock == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|AutoDoorFail](Locked.[SOFTBLOCK] Looks like there's a Security-Red terminal locking this door down.)") ]])
        fnCutsceneBlocker()
    else
        AL_SetProperty("Open Door", "LockedDoorNW")
        AudioManager_PlaySound("World|AutoDoorOpen")
    end
    
--[Main Floor]
elseif(sObjectName == "MainFloorL" or sObjectName == "MainFloorR") then
	
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    local iSaw55sMemories = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    local iSaw55PostLRT   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N")
	WD_SetProperty("Hide")
	
	--Christine needs to change form.
	if(sChristineForm ~= "Golem") then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Better switch back to Golem.[SOFTBLOCK] Wouldn't want to draw unnecessary attention.)") ]])
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
	end
	
	--If 55 is following, she mentions she's leaving here.
	if(iIs55Following == 1.0) then
		
        --Post LRT scene:
        if(iSaw55sMemories == 1.0 and iSaw55PostLRT == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, please report to your normal work assignments.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I downloaded what I could from the core and set up a back door into the network.[SOFTBLOCK] I will review what footage I can.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Are you...[SOFTBLOCK] okay?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Report to your normal work assignments.[SOFTBLOCK] I will contact you when I have determined our next move.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55, come on...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do not waste processor cycles worrying.[SOFTBLOCK] Focus on maintaining your cover.[SOFTBLOCK] Move out.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...") ]])
            fnCutsceneBlocker()
            
            --Remove 55 from the following group.
            gsFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AC_SetProperty("Set Party", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "55")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
            
            --Flag for the next part of the cutscene.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N", 1.0)
            
            --Change map to Christine's quarters. This will fire the next part of the scenario.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.0x10.0x0") ]])
        
        --Normal:
        else
            
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] It is best if I'm not seen on the main floor cameras.[SOFTBLOCK] I'll be in touch.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            
            --Remove 55's sprite. She remains in the combat party for equipment reasons.
            gsFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AC_SetProperty("Set Party", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "55")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        end
	end
	
	--Force-facing flag. This will cause Christine to spawn with the correct facing.
	gi_Force_Facing = gci_Face_South
	
	--Transition.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	if(sObjectName == "MainFloorL") then
		fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:6.5x14.0x0") ]])
	else
		fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:11.5x14.0x0") ]])
	end
	fnCutsceneBlocker()

--[Basement Floor 2]
elseif(sObjectName == "Basement1L" or sObjectName == "Basement1R") then
	
	--Force-facing flag. This will cause Christine to spawn with the correct facing.
	gi_Force_Facing = gci_Face_South
	WD_SetProperty("Hide")
	
	--Transition.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	if(sObjectName == "Basement1L") then
		fnCutsceneInstruction([[ AL_BeginTransitionTo("LowerRegulusCityA", "FORCEPOS:22.5x15.0x0") ]])
	else
		fnCutsceneInstruction([[ AL_BeginTransitionTo("LowerRegulusCityA", "FORCEPOS:26.5x15.0x0") ]])
	end
	fnCutsceneBlocker()

--Close the dialogue.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end