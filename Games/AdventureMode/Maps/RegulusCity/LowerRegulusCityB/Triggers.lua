--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Nope.
--55 joins the party if she hasn't already.
if(sObjectName == "55Appears") then

    --Variables.
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    
	--If 55 is not following but should, cutscene time.
	if(iIs55Following == 0.0) then
		
		--Flag 55 as following Christine.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
		
		--Create if she doesn't exist. She probably won't, but you never know.
		if(EM_Exists("55") == false) then
			fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
		end
		
		--Christine moves a bit.
		fnCutsceneMove("Christine", 35.25, 13.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Teleport 55 in.
		fnCutsceneTeleport("55", 35.25, 9.50)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("55", 35.25, 12.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Let's go.") ]])
        fnCutsceneBlocker()
		
		--Walk 55 onto Christine, fold the party.
		fnCutsceneMove("55", 35.25, 13.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
		--Lua globals.
		gsFollowersTotal = 1
		gsaFollowerNames = {"55"}
		giaFollowerIDs = {0}

		--Get 55's uniqueID. 
		EM_PushEntity("55")
			local iCharacterID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs = {iCharacterID}
		AL_SetProperty("Follow Actor ID", iCharacterID)
	end
end
