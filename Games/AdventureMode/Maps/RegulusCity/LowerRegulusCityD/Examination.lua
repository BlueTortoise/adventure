--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "LadderE") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("LowerRegulusCityC", "FORCEPOS:27.0x10.0x0")
    
elseif(sObjectName == "LadderN") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:7.0x6.0x0")

--[Examination]
elseif(sObjectName == "Elevator") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An elevator, the shaft is long since disused and doesn't actually contain a lift unit.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WaterTerminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This terminal is covered in dust, but it's connected to the network and is monitoring the water composition here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "InsideTerminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hmm, it seems this terminal is still exposed to the network and has been used for hacking...[SOFTBLOCK] Probably by 55.)") ]])
    fnCutsceneBlocker()

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end