--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "LeftExit") then
		
    --Get Christine's position.
    EM_PushEntity("Christine")
        local fPlayerX, fPlayerY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This is the foot path connecting to Sector 104.[SOFTBLOCK] We don't need to go that way.)") ]])
    fnCutsceneBlocker()
    
    fnCutsceneMove("Christine", 14.25, fPlayerY / gciSizePerTile)
    fnCutsceneMove("Sophie", 14.25, fPlayerY / gciSizePerTile)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "RightExit") then
		
    --Get Christine's position.
    EM_PushEntity("Christine")
        local fPlayerX, fPlayerY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This is the foot path connecting to Sector 138.[SOFTBLOCK] We don't need to go that way.)") ]])
    fnCutsceneBlocker()
    
    fnCutsceneMove("Christine", 24.25, fPlayerY / gciSizePerTile)
    fnCutsceneMove("Sophie", 24.25, fPlayerY / gciSizePerTile)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
--[Cutscenes]
elseif(sObjectName == "FirstTimeShopping") then

    --Repeat check.
    local iShowedGetOffTram = VM_GetVar("Root/Variables/Chapter5/Scenes/iShowedGetOffTram", "N")
    if(iShowedGetOffTram == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iShowedGetOffTram", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 18.25, 16.50)
    fnCutsceneMove("Sophie", 19.25, 16.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] This place is really nice...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You've never been to Sector 119 before?[SOFTBLOCK] But you sounded like you had.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I have, actually.[SOFTBLOCK] I've delivered repaired appliances here before.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But, we Slave Units have back access paths.[SOFTBLOCK] The Lord Units don't want us to be seen if possible when we're making deliveries.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What about the employees?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] If you're assigned to a shop, you're expected to enter and exit via the back access paths.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I have a friend who works in the Chez Fuel shop.[SOFTBLOCK] She's always getting her chassis corroded...[SOFTBLOCK] and she tells me about her job while I fix her![SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] But I guess...[SOFTBLOCK] I'm out here now...[SOFTBLOCK] where everyone can see me...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Relax, silly![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If anyone asks, you're here to -[SOFTBLOCK] uh -[SOFTBLOCK][EMOTION|Christine|Laugh] carry my purchases![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But you don't actually have to...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Yeah, just, act synthetic.[SOFTBLOCK] I'm supposed to be here.[SOFTBLOCK] Nobody can punish me...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Just think of all the dresses and devices we'll get to see![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Yeah![SOFTBLOCK] Can we visit the pet shop?[SOFTBLOCK] Oh -[SOFTBLOCK] and see the shoes?[SOFTBLOCK] Yeah, this'll be great![SOFTBLOCK] Let's go!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold the party.
    fnCutsceneMove("Sophie", 18.25, 16.50)
    fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
    
end
