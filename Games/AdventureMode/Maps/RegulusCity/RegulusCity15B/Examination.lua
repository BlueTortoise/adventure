--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ToExteriorWA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorWA", "FORCEPOS:38.0x10.0x0")
	
--Exit
elseif(sObjectName == "ToCity15A") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCity15A", "FORCEPOS:19.0x12.0x0")
	
--Exit
elseif(sObjectName == "ToRegulus15CEast") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCity15C", "FORCEPOS:12.5x17.0x0")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N", 0.0)
	
	--Force-facing flag. This will cause Christine to spawn with the correct facing.
	gi_Force_Facing = gci_Face_South
	
--Exit
elseif(sObjectName == "ToRegulus15CWest") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCity15C", "FORCEPOS:7.5x17.0x0")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N", 0.0)
	
	--Force-facing flag. This will cause Christine to spawn with the correct facing.
	gi_Force_Facing = gci_Face_South

--[Other Examinables]
--Junk.
elseif(sObjectName == "Junk") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Junk, just left sitting out.[SOFTBLOCK] Looks like there aren't enough units assigned to trash removal here.)") ]])
	fnCutsceneBlocker()
	
--Work Terminal.
elseif(sObjectName == "WorkTerminal") then
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/WorkTerminal/Execution.lua")
	
--Fabrication Terminal
elseif(sObjectName == "FabTerminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This terminal has a long list of things to be fabricated by the golem on duty here.[SOFTBLOCK] It's mostly combat equipment.[SOFTBLOCK] No auto-vacuums on the list...)") ]])
	fnCutsceneBlocker()
	
--Waste Terminal
elseif(sObjectName == "WasteTerminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A unit left this terminal logged into a message board.[SOFTBLOCK] Seems she was complaining about inadequate garbage clearing.[SOFTBLOCK] Go figure.)") ]])
	fnCutsceneBlocker()
	
--Boxes
elseif(sObjectName == "Boxes") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](These boxes are full of junk for recycling.[SOFTBLOCK] Seems some unit filled them, then got disheartened by the monumental nature of her task...)") ]])
	fnCutsceneBlocker()
	
--Videograph Terminal
elseif(sObjectName == "VideographTerminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I don't really feel like watching a videograph right now.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end