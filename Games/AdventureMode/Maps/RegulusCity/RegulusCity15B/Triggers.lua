--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--A distress call from the Equinox facility.
if(sObjectName == "Equinox") then
	
	--Variables.
	local iSawEquinoxMessage = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N")
	
	--First time, play the cutscene.
	if(iSawEquinoxMessage == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N", 1.0)
		
		--Movement.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "NULL") ]])
		fnCutsceneMove("Christine", 23.25, 18.50)
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneMove("55", 24.25, 18.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "PDU") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Unit 771852, I am picking up an erratic signal.[SOFTBLOCK] Would you like it played?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Go ahead, PDU.") ]])
		fnCutsceneBlocker()
		
		--Scene, changes music.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "EquinoxTheme") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] 'This is Unit 618285, transmitting across all frequencies![SOFTBLOCK] Something has happened at the Equinox facility![SOFTBLOCK] Everything is crazy -[SOFTBLOCK] they're all dead![SOFTBLOCK] Some unit, please send help![SOFTBLOCK] Plea - '[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: ...[SOFTBLOCK] It seems the transmission has ended there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: A jamming signal is currently blocking my radio receivers.[SOFTBLOCK] Its activation coincides with the transmission cutting out.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Long-range radio usage is unauthorized at the moment.[SOFTBLOCK] The message boards have been complaining about it for several days.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So you think the administrators blocked the signal because it was unauthorized?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No, I do not.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Good, because it'd be insane to block a distress call![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is, then, why they blocked it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You don't think - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I think it's suspicious, but am unconcerned.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] If what that unit said is correct, though, then the Equinox Laboratories will have a reduced security footprint.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] We may be able to acquire equipment there without scrutiny.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I'm sure the security teams are already on their way.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Oh?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--55 Moves forward.
		fnCutsceneMove("55", 24.25, 19.50)
		fnCutsceneMove("55", 20.25, 19.50)
		fnCutsceneFace("55", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneFace("GolemAA", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit.[SOFTBLOCK] Report.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: (Eek![SOFTBLOCK] A command unit!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: All systems are nominal, Command Unit![SOFTBLOCK] Sector 15 is performing at peak efficiency![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: *You know, despite all the garbage laying around.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you sure about that?[SOFTBLOCK] You haven't heard any distress signals?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Erm, I don't mean to have overheard, but I believe that Lord Unit's PDU mentioned something about one.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: But my PDU is clear.[SOFTBLOCK] Are you experiencing a glitch, perhaps?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--55 Moves forward.
		fnCutsceneMove("55", 21.25, 19.50)
		fnCutsceneMove("55", 21.25, 17.50)
		fnCutsceneBlocker()
		fnCutsceneFace("GolemAA", 1, 0)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As expected, there is nothing in the security logs, either.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Erm, am I in trouble?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are dismissed.[SOFTBLOCK] Back to work, unit.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Right away, Command Unit![SOFTBLOCK] (Phew!)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneFace("GolemAA", -1, 0)
		fnCutsceneMove("55", 22.25, 18.50)
		fnCutsceneFace("55", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I don't understand.[SOFTBLOCK] Why wouldn't there be anything in the logs?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Because the administrators do not desire the signal to be heard or appear in the logs.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK][E|Offended] We have to stop them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I will not allow another Cryogenics...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] Let us proceed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[EMOTION|Christine|PDU] If you are travelling to the Equinox Laboratories, exit via the southern airlock in Sector 15 and travel southwest over the surface.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Oh, and have a nice day.[SOFTBLOCK] My algorithms keep pestering me to say that.") ]])
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fold party.
		fnCutsceneBlocker()
		fnCutsceneMove("55", 23.25, 18.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Music", "RegulusCity") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()

	end
end
