--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Triggers]
--Cassandra flees from the party.
if(sObjectName == "CassandraNTrigger") then
	
	--Variables.
	local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
	local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
	local iCassandraRanOff        = VM_GetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N")
	local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Play cutscene.
	if(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 0.0 and iCassandraRanOff == 0.0) then
		
		--If Christine is currently a non-human...
		if(sChristineForm ~= "Human") then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N", 1.0)
			
			--Close the door at the south.
			AL_SetProperty("Close Door", "DoorMiddleS")
			
			--Cassandra sees the party...
			fnCutsceneFace("Cassandra", 0, -1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
            
            --Golem, Latex Drone, Steam Droid:
            if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Eek![SOFTBLOCK] More robots![BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Eek![SOFTBLOCK] A monster![BLOCK][CLEAR]") ]])
            end
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Hey![SOFTBLOCK] Wait!") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
			--Cassandra runs off via the southern route.
			fnCutsceneFace("Christine", 0, 1)
			fnCutsceneFace("55", 0, 1)
			fnCutsceneMove("Cassandra", 17.25, 22.50, 2.50)
			fnCutsceneMove("Cassandra", 17.25, 23.50, 2.50)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorMiddleS") ]])
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			fnCutsceneMove("Cassandra", 17.25, 25.50, 2.50)
			fnCutsceneMove("Cassandra", 24.25, 25.50, 2.50)
			fnCutsceneMove("Cassandra", 24.25, 23.50, 2.50)
			fnCutsceneMove("Cassandra", 32.25, 23.50, 2.50)
			fnCutsceneBlocker()
			fnCutsceneTeleport("Cassandra", -100.25, -100.50)
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Wow, she's really fast![BLOCK][CLEAR]") ]])
            
            --Golem, Latex Drone, Steam Droid:
            if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What do you think she meant by 'more robots'?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We're robots, Christine.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, I mean, do you think some other units were sent down here to catch her?[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I suppose it's my fault for looking like this, isn't it...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She seemed pretty jumpy.[SOFTBLOCK] Do you think she's run into anyone else down here?[BLOCK][CLEAR]") ]])
            end
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Likely she encountered a maintainence unit, who probably put in the work order.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] How are we going to get close to her?[SOFTBLOCK] She didn't even wait to hear us out.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] I imagine you'll think of something.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
		--If Christine is currently a human...
		else
			
			--Cassandra sees the party...
			fnCutsceneFace("Cassandra", 0, -1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Eek![SOFTBLOCK] More robots![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm not a robot![SOFTBLOCK] Look![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] You...[SOFTBLOCK] aren't?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] May we come closer?[SOFTBLOCK] Please don't run away.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Movement.
			fnCutsceneMove("Christine", 16.25, 22.50)
			fnCutsceneMove("55", 17.25, 22.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Cassandra", 1, 0)
			fnCutsceneFace("Christine", -1, 0)
			fnCutsceneFace("55", -1, 0)
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Wait -[SOFTBLOCK] your friend...[SOFTBLOCK] Is she a robot?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes.[SOFTBLOCK] This is 2855.[SOFTBLOCK] She's a doll.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Command unit...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But it's okay![SOFTBLOCK] She's my friend.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Call it what you will...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] I didn't think I'd see another human down here...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] My name is Cassandra.[SOFTBLOCK] Pleased to meet you...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Christine Dormer![SOFTBLOCK] Charmed![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] I'm not really sure how I got here.[SOFTBLOCK] The last thing I remember, I was doing some reading at home, and then it all went black.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] And now I'm in this creepy place, and there are robots coming after me...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You know, I can say much the same thing.[SOFTBLOCK] We're not sure how I got here either.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (It seems like she trusts me now...[SOFTBLOCK] maybe I should convert her while her guard is down?)[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Convert Her\", " .. sDecisionScript .. ", \"Convert Her\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Lead Her to Safety\",  " .. sDecisionScript .. ", \"Lead Her to Safety\") ")
			fnCutsceneBlocker()
		
	
		end
	end
	
elseif(sObjectName == "CassandraSTrigger") then
	
	--Variables.
	local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
	local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
	local iCassandraRanOff        = VM_GetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N")
	local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Play cutscene.
	if(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 0.0 and iCassandraRanOff == 0.0) then
		
		--If Christine is currently a non-human...
		if(sChristineForm ~= "Human") then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N", 1.0)
			
			--Close the door at the north.
			AL_SetProperty("Close Door", "DoorMiddleN")
			
			--Cassandra sees the party...
			fnCutsceneFace("Cassandra", 0, 1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
            
            --Golem, Latex Drone, Steam Droid:
            if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Eek![SOFTBLOCK] More robots![BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Eek![SOFTBLOCK] A monster![BLOCK][CLEAR]") ]])
            end
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey![SOFTBLOCK] Wait!") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
			--Cassandra runs off via the southern route.
			fnCutsceneFace("Christine", 0, -1)
			fnCutsceneFace("55", 0, -1)
			fnCutsceneMove("Cassandra", 15.25, 21.50, 2.50)
			fnCutsceneMove("Cassandra", 17.25, 21.50, 2.50)
			fnCutsceneMove("Cassandra", 17.25, 20.50, 2.50)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorMiddleN") ]])
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			fnCutsceneMove("Cassandra", 17.25, 8.50, 2.50)
			fnCutsceneBlocker()
			fnCutsceneTeleport("Cassandra", -100.25, -100.50)
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Wow, she's really fast![BLOCK][CLEAR]") ]])
            
            --Golem, Latex Drone, Steam Droid:
            if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What do you think she meant by 'more robots'?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We're robots, Christine.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, I mean, do you think some other units were sent down here to catch her?[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I suppose it's my fault for looking like this, isn't it...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She seemed pretty jumpy.[SOFTBLOCK] Do you think she's run into anyone else down here?[BLOCK][CLEAR]") ]])
            end
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Likely she encountered a maintainence unit, who probably put in the work order.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How are we going to get close to her?[SOFTBLOCK] She didn't even wait to hear us out.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I imagine you'll think of something.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
		--If Christine is currently a human...
		else
			
			--Cassandra sees the party...
			fnCutsceneFace("Cassandra", 0, 1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Eek![SOFTBLOCK] More robots![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm not a robot![SOFTBLOCK] Look![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] You...[SOFTBLOCK] aren't?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] May we come closer?[SOFTBLOCK] Please don't run away.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Movement.
			fnCutsceneMove("Christine", 16.25, 22.50)
			fnCutsceneMove("55", 17.25, 22.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Cassandra", 1, 0)
			fnCutsceneFace("Christine", -1, 0)
			fnCutsceneFace("55", -1, 0)
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
            
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] Wait -[SOFTBLOCK] your friend...[SOFTBLOCK] Is she a robot?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes.[SOFTBLOCK] This is 2855.[SOFTBLOCK] She's a doll.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Command unit...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But it's okay![SOFTBLOCK] She's my friend.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Call it what you will...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady:[E|Neutral] I didn't think I'd see another human down here...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] My name is Cassandra.[SOFTBLOCK] Pleased to meet you...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Christine Dormer![SOFTBLOCK] Charmed![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] I'm not really sure how I got here.[SOFTBLOCK] The last thing I remember, I was doing some reading at home, and then it all went black.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] And now I'm in this creepy place, and there are robots coming after me...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You know, I can say much the same thing.[SOFTBLOCK] We're not sure how I got here either.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (It seems like she trusts me now...[SOFTBLOCK] maybe I should convert her while her guard is down?)[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Convert Her\", " .. sDecisionScript .. ", \"Convert Her\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Lead Her to Safety\",  " .. sDecisionScript .. ", \"Lead Her to Safety\") ")
			fnCutsceneBlocker()
		
	
		end
	end

--Convert Cassandra.
elseif(sObjectName == "Convert Her") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N", 1.0)
	
	--Spawn a converted Cassandra. This will take her place for ease-of-use purposes.
	TA_Create("CassandraGolem")
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/CassandraG/", false)
	DL_PopActiveObject()
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] Do you think you can help me find my way home?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, easily![SOFTBLOCK] We'll make this your new home![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] H-[SOFTBLOCK]huh?") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Christine transforms.
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	--Flash the active character to white. Immediately after, execute the transformation.
	Cutscene_CreateEvent("Flash Christine White", "Actor")
		ActorEvent_SetProperty("Subject Name", "Christine")
		ActorEvent_SetProperty("Flashwhite")
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
	fnCutsceneWait(gci_Flashwhite_Ticks_Total)
	fnCutsceneBlocker()

	--Now wait a little bit.
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] Y-[SOFTBLOCK]you were a robot in disguise![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes![SOFTBLOCK] And now, you will be too...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] Eeeek![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Stop fussing, you'll like it!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade to black.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Scene.
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Cassandra to Golem/Scene_Begin.lua")
	fnCutsceneBlocker()
	
	--Transport the party over to the golem tube.
	AL_SetProperty("Open Door", "ConversionDoor")
	fnCutsceneTeleport("Christine", 23.25, 13.50)
	fnCutsceneTeleport("55", 23.25, 14.50)
	fnCutsceneTeleport("CassandraGolem", 22.25, 13.50)
	fnCutsceneTeleport("Cassandra", -100.25, -100.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", -1, 0)
	fnCutsceneFace("55", -1, 0)
	fnCutsceneFace("CassandraGolem", 1, 0)
	fnCutsceneBlocker()
	
	--Unfade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(105)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Happy") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Golem] Proceeding to function assignment...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Eee![SOFTBLOCK] You're so pretty as a golem![SOFTBLOCK] I'm so proud of you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Golem] Proceeding to function assignment...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Cassandra leaves.
	fnCutsceneMove("CassandraGolem", 22.25, 17.50)
	fnCutsceneMove("CassandraGolem", 8.25, 17.50)
	fnCutsceneFace("Christine", -1, 1)
	fnCutsceneFace("55", -1, 1)
	fnCutsceneBlocker()
	fnCutsceneTeleport("CassandraGolem", -100.25, -100.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She wasn't all that talkative, was she?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Initial function assignment takes priority over everything else.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Unit 771852, I am legimately impressed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] You used the resources at hand to isolate and convert a rogue human.[SOFTBLOCK] You didn't allow emotion to cloud your judgement.[SOFTBLOCK] You did not hesitate.[SOFTBLOCK] Good work.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh 55![SOFTBLOCK] I allowed emotions to guide me, and they helped![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Can you just imagine it?[SOFTBLOCK] Thousands, millions of humans being converted...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] With their combined scientific output, we will plumb the mysterious depths of the universe...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What are you talking about?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Someday, we will return to Earth, where I am from, and we will bring mechanical perfection to everyone there.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I didn't - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Mmmmm, it's exciting to fantasize about...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But to bring about this beautiful future, we must focus on the present.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Let's get going.[SOFTBLOCK] I can't wait to mechanize more humans![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (She's actually kind of off-putting when she's like this...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fold the party.
	fnCutsceneMove("55", 23.25, 13.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Lead Cassandra to safety.
elseif(sObjectName == "Lead Her to Safety") then
	
    --Variables.
    local iMetJX101 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N")
    
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N", 2.0)
	
	--Close doors.
	AL_SetProperty("Close Door", "DoorMiddleN")
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] Do you think you can help me find my way home?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Mmm, do you even know where home is?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] ...[SOFTBLOCK] No...[SOFTBLOCK] I can't remember...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It's possible that she escaped from the biological research facility.[SOFTBLOCK] Or she was abducted and escaped from containment.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] No, that's not right...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, where can we take her that will be safe until she remembers where she's from?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Tch, nowhere in Regulus City.[SOFTBLOCK] She will be hunted down by security units so long as she remains here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I suppose the Steam Droids would probably take her in.[SOFTBLOCK] She is an agitator, and they like those.[BLOCK][CLEAR]") ]])
    if(iMetJX101 == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The Steam Droids?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Out-of-date models that live in the Tellurium Mines.[SOFTBLOCK] They could provide sanctuary for Cassandra.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Plus it would likely ingratiate us with them.[SOFTBLOCK] They -[SOFTBLOCK] are not keen on Command Units like me, but if I was seen aiding their allies...[BLOCK][CLEAR]") ]])
    
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Great idea![SOFTBLOCK] I'm sure JX-101 would love to have you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Though you'll need to work, but I'm sure there's something you can do in Sprocket City.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] Who's JX-101?[SOFTBLOCK] Another robot girl?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes, and another good friend of mine.[SOFTBLOCK] You can trust her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Given the nature of the Tellurium Mines, I would recommend against sending her to Sprocket City.[SOFTBLOCK] There are other, smaller settlements that will be easier to direct her to.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have uploaded the data to your PDU.[SOFTBLOCK] There will be some risks involved.[BLOCK][CLEAR]") ]])
    end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Yeah, not a safe path to Sprocket City...[SOFTBLOCK][EMOTION|Christine|Smirk] This settlement here is closer...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] How does that sound, Cassandra?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] I suppose it's better than fleeing whenever a door opens...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] PDU, please chart a course to the Tellurium Mines for Cassandra to follow.[SOFTBLOCK] Please avoid areas that are depressurized.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] Chart a course..?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will not be able to accompany you.[SOFTBLOCK] We will provide you a map to follow.[SOFTBLOCK] It should lead you to safety.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why can't we go with her?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The more of us there are, the more likely we will be detected.[SOFTBLOCK] It is best that she go alone.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] Alone...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] I'm sure I can make it.[SOFTBLOCK] I'll be fine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm sorry, Cassandra...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] It's all right.[SOFTBLOCK] Thank you for your help, Christine.[SOFTBLOCK] And yours, 55.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Map printout complete.[SOFTBLOCK] Have a nice day![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] So I just need to crawl through those vents, and run along that unused section of track?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra:[E|Neutral] Please come visit me if you can, Christine.[SOFTBLOCK] I don't know how long until my memory returns, but if there's anything I can do for you, don't hesistate to ask.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay![SOFTBLOCK] Good luck, Cassandra!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Cassandra walks off.
	fnCutsceneMove("Cassandra", 17.25, 22.50)
	fnCutsceneMove("Cassandra", 17.25, 20.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	AL_SetProperty("Open Door", "DoorMiddleN")
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneBlocker()
	fnCutsceneMove("Cassandra", 17.25, 8.50)
	fnCutsceneBlocker()
    fnCutsceneTeleport("Cassandra", -100.25, -100.50)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I hope she'll be okay...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Most likely.[BLOCK][CLEAR]") ]])
    if(iMetJX101 == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The Steam Droids will take her in, they detest Regulus City and its regime.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] This should make them more receptive to requests for assistance.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We did this to help Cassandra.[SOFTBLOCK] This was not a calculated strategic move![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] You may justify it any way you like.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You're justifying doing the right thing as something cold and logical.[SOFTBLOCK] That's no different than my way of justifying it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] That is true.[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Merely mentioning your name will likely guarantee her asylum.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I was more worried about the creatures in the mines.[SOFTBLOCK] There's a lot of them, and if she runs into one...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] For an organic, she runs quickly.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Guess there's no point in worrying, is there?[BLOCK][CLEAR]") ]])
        
    end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, come on.[SOFTBLOCK] Let's go lie on the work completion form.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneMove("55", 16.25, 22.50)
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
	
end
