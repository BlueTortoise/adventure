--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--55 leaves the party.
if(sObjectName == "PostTrigger") then
    
    --Variables.
    local iRunPostScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iRunPostScene", "N")
    if(iRunPostScene == 0.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRunPostScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    
    --Run the scene.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Fade in while walking.
    fnCutsceneTeleport("Christine", 19.25, 12.50)
    fnCutsceneTeleport("55", 19.25, 12.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 19.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 19.25, 10.50)
    fnCutsceneMove("55", 19.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 19.25, 8.50)
    fnCutsceneMove("55", 19.25, 9.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You know, 55...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Things turned out a lot more light-hearted than I was expecting.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you accustomed to otherwise?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] After what happened at Cryogenics, yes.[SOFTBLOCK] Yes I am.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Though I appreciate your concern.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am unconcerned with your emotional state.[SOFTBLOCK] What I was saying was a prelude to my real point.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Allow your emotions to overcome you at your peril.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Even the good ones?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] Joy is as offsetting as hate as is sorrow.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Maintain neutrality and even temperment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Good advice, I guess.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And if you ever feel lonely, you just tell me.[SOFTBLOCK] I always have time to chat.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Oh ho![SOFTBLOCK] Even temperment, is it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Should I feel...[SOFTBLOCK] lonely...[SOFTBLOCK] I will advise you.[SOFTBLOCK] In exchange, do not gloat over such things.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Deal.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Now let us proceed, Lord Unit.") ]])
    fnCutsceneBlocker()

    --Move 55 onto Christine, fold the part.
    fnCutsceneMove("55", 19.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

    --Get the characters's uniqueID. 
    EM_PushEntity("55")
        local iCharacterID = RE_GetID()
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
    DL_PopActiveObject()

    --[Lua Globals]
    --Create a new table with this character in it.
    local tFollowerNames = {sFieldName}
    local tFollowerIDs   = {iCharacterID}

    --Append the tables together.
    gsFollowersTotal = gsFollowersTotal + 1
    fnAppendTables(gsaFollowerNames, tFollowerNames)
    fnAppendTables(giaFollowerIDs,   tFollowerIDs)
    AL_SetProperty("Follow Actor ID", iCharacterID)

end
