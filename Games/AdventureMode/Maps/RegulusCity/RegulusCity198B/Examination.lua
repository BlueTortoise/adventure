--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ToCity198A") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCity198A", "FORCEPOS:19.0x12.0x0")
	
--Exit
elseif(sObjectName == "ToCity198C") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCity198C", "FORCEPOS:9.5x4.0x0")
	gi_Force_Facing = gci_Face_South

--[Other Examinables]
elseif(sObjectName == "ComputerA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A directory of all the units in residence in this sector.[SOFTBLOCK] Only a handful of golems are assigned here permanently, the rest are temporary accomodations for training.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] This unit seemed to be struggling with implanting golem cores.[SOFTBLOCK] The logs indicate she kept trying to apply them to the legs...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] This unit did a great job converting targets, but wasn't very choosy.[SOFTBLOCK] It's important to pick high-quality human specimens or you wind up with a lot of lazy units.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] This unit excelled in the combat part of the exercise.[SOFTBLOCK] She typed furiously!)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] There's no saved data on this one.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerF") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] The Lord Golem in charge of this room was using this console.[SOFTBLOCK] Her score was exemplary, as expected.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerG") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] This unit was multi-tasking, and playing three training simulations at once.[SOFTBLOCK] Nominally efficient, but her score was abysmal on all three...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerH") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] This unit didn't seem to understand the text prompt and was typing up a screed about poor-quality oil ingredients.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerI") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] The last unit did a great job and even got a commendation. Good work!)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerJ") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] Hmm, this unit really enjoyed watching humans get converted.[SOFTBLOCK] I suppose I would too, it's quite exciting.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerK") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](This is Unit 107822's console.[SOFTBLOCK] She...[SOFTBLOCK] doesn't seem to use it very often...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerL") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](This console is networked specially to spy on the other consoles.[SOFTBLOCK] The logs indicate the Lord Unit would sometimes spawn extra enemies in a golem's simulation...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerM") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](This console is networked specially to spy on the other consoles.[SOFTBLOCK] There are logs of the scores and evaluations of previous graduates.[SOFTBLOCK] Nothing really stands out.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerN") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Amanda's console.[SOFTBLOCK] It has a large number of requests and complaints, but she's been stuck on the strange entity behavior for a while now...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerO") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A training console not in use.[SOFTBLOCK] This unit somehow acquired a beam sword in the simulation...[SOFTBLOCK] but there's no evidence of cheating.[SOFTBLOCK] Hmm...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "Adaptors") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](These shelves have a wide range of adaptors for converting inputs between socket types.[SOFTBLOCK] They're fairly old and worn out.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "Converters") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](These shelves have burnt out power converters for frequencies that aren't used anymore.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end