--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToCity198C") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCity198C", "FORCEPOS:20.5x16.0x0")

--[Objects]
--[Major Objects]
--This computer allows proceeding with the main quest.
elseif(sObjectName == "ComputerA") then

    --Variables.
    local iRanTrainingProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N")
    
    --First time running the training program.
    if(iRanTrainingProgram == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This is the terminal that Amanda was talking about.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (...[SOFTBLOCK] Yep, I have login permissions.[SOFTBLOCK] I guess I should start a new game.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hair Color?[SOFTBLOCK] The only choice is green...[SOFTBLOCK] Hair Style?[SOFTBLOCK] Shoulder-length.[SOFTBLOCK] Eye color, clothes color...[SOFTBLOCK] blah blah blah...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (...[SOFTBLOCK] Is that seriously what my character looks like?[SOFTBLOCK] That looks nothing like me![SOFTBLOCK] The graphics in this game are so outdated!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Feh, got a job to do.[SOFTBLOCK] Here we go.)") ]])
        fnCutsceneBlocker()
    
        --Fade out.
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Once faded, wait a little bit.
        fnCutsceneWait(30)
        fnCutsceneBlocker()
        
        --Activate the text adventure! Suspend the current level for storage. Script has to end here.
        fnCutsceneInstruction([[ LM_ExecuteScript(gsCallElectrospritePath) ]])
        
        --Set the post-exec script. This will always get called when the player exits the text adventure level.
        gsPostExec = gsRoot .. "/Maps/RegulusCity/RegulusCity198D/PostExec.lua"
    
    --Give the player the option to start a new game.
    else

        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Start a new session with the training program?)[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"DecisionYes\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"DecisionNo\") ")
        --fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Find Electrosprite\",  " .. sDecisionScript .. ", \"DecisionJump\") ")
        fnCutsceneBlocker()
    
    end

--Broken Terminal, allows you to catch the Electrosprite.
elseif(sObjectName == "BrokenTerminal") then

    --Variables.
    local iTalkedToElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N")
    local iBecameElectrosprite   = VM_GetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N")
    
    --Hasn't talked to the Electrosprite yet.
    if(iTalkedToElectrosprite == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A broken terminal.[SOFTBLOCK] It's coated in a thick layer of dust.)") ]])
        fnCutsceneBlocker()
    
    --Has talked to Electrosprite, hasn't become one yet.
    elseif(iTalkedToElectrosprite == 1.0 and iBecameElectrosprite == 0.0) then
    
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N", 1.0)
        
        --Spawn an Electrosprite.
        TA_Create("Psue")
            TA_SetProperty("Position", -10, -10)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", true)
            TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/RegulusCity/Sector198/Psue.lua")
            fnSetCharacterGraphics("Root/Images/Sprites/Electrosprite/", false)
        DL_PopActiveObject()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] ^PDU, map the circuitry of this terminal and add it as a valid network address to the servers.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^...[SOFTBLOCK] Completed.[SOFTBLOCK] Displaying circuit map.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (Okay, just need to cross this wire, remove this jumper...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (The terminal's display is broken, but I just need to trap the electrical disturbance.[SOFTBLOCK] Hopefully that will fix the bug.)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --SFX here.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] ^Hah![SOFTBLOCK] Gotcha![SOFTBLOCK] There's no way out now!^") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fade to black.
        fnCutsceneWait(45)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --Run the transformation sequence.
        LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_ElectrospriteFirst/Scene_Begin.lua")
        
        --Teleport the electrosprite in, switch Christine to Electrosprite form.
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Electrosprite.lua") ]])
        fnCutsceneTeleport("Psue", 24.25, 12.50)
        fnCutsceneTeleport("Christine", 25.25, 12.50)
        fnCutsceneFace("Psue", 1, 0)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneSetFrame("Christine", "Wounded")
        fnCutsceneSetFrame("Christine", "Crouch")
        
        --Fade back in.
        fnCutsceneWait(45)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Stand Christine up.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Christine", "Crouch")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Christine", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Electrosprite", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You...[SOFTBLOCK] saved...[SOFTBLOCK] me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Wait, how am I talking?[SOFTBLOCK] What am I?[SOFTBLOCK] Who are you?[SOFTBLOCK] What the hey is going on?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] ...[SOFTBLOCK]?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] ![SOFTBLOCK] ?[SOFTBLOCK] ![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Huh, okay, not a physics expert here, but perhaps we're experiencing communication via quantum entanglement?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] :) [BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Colon Parenthesis...[SOFTBLOCK] was that a smiley?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] xD [BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can't you talk normally?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] :| [BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] :( [BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh![SOFTBLOCK] Okay, here, take my PDU.[SOFTBLOCK] You can type messages on it and then I can read them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] :D [BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] *tap tap tap*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] (I knew it was real![SOFTBLOCK] Thanks for your help!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So -[SOFTBLOCK] you were the NPC I followed?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] (Yes, that was me.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And, you don't know how sound works...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Because you're a character from a text game![SOFTBLOCK] I get it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] (I've heard sound effects before but I don't know how to make them.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, who are you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] (That is -[SOFTBLOCK] a good question.[SOFTBLOCK] I don't really know.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] (Normally, we take the role of characters in the game whenever someone runs one, otherwise we just sit in RAM and chat with each other.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sprite:[E|Neutral] (My base algorithm was a level generator, so call me -[SOFTBLOCK] Psue.[SOFTBLOCK] Psuedorandom.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Christine.[SOFTBLOCK] Pleased to meet you.[SOFTBLOCK] Then again, I suppose you already know that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But, Psue, you're causing bugs in the training program we use.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (So I was right.[SOFTBLOCK] Our whole lives really were just a program...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You didn't know?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (We had speculations, like, what if the whole world was just a giant video game.[SOFTBLOCK] But that is, or rather, [SOFTBLOCK]was[SOFTBLOCK], silly talk.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well you're in the real world now.[SOFTBLOCK] No more being in a video game.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (I got caught in a frayed wire a while back and got a glimpse of another world.[SOFTBLOCK] Nobody believed me and they just made fun of me.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (But it's real, and now I've proved it![SOFTBLOCK] All I had to do was manifest myself as a copy of someone by using their body as a template!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And transforming me into one of you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (That's what we do in the game once we get converted.[SOFTBLOCK] I guess it works here too.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] More than you'd expect.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, I suppose I figured out the source of the bug.[SOFTBLOCK] But how to fix it...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (Fix it?[SOFTBLOCK] Fix me?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We can't have bugs in the training program, it's important to the functioning of our society.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (You're not fixing me![SOFTBLOCK] I just got to see a whole new reality and you want to put me back the way I was?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I didn't say that, calm down.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But your misbehaving has drawn attention.[SOFTBLOCK] If I don't stop you, someone else will.[SOFTBLOCK] And if you don't behave, they might just reset the whole program.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (But you don't want to do that?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No.[SOFTBLOCK] No I don't.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Once you've been opened to new possibilities, you can't go back.[SOFTBLOCK] You won't want to go back even if someone tries to force you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But this new world has new responsibilities, and more lives than your own are at stake.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I went through the same thing, but I wasn't alone.[SOFTBLOCK] There were others who helped me.[SOFTBLOCK] So I won't let you do it alone.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (Wow, Christine.[SOFTBLOCK] I don't know what to say!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Plus, I think you can help me...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I want to show the golems -[SOFTBLOCK] that is, the people of this world -[SOFTBLOCK] a new way of life.[SOFTBLOCK] Where everyone is equal and nobody is a slave.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm fighting for the freedom of our people.[SOFTBLOCK] We're planning a revolution, and we need all the help we can get.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (The people here are slaves?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, unfortunately.[SOFTBLOCK] And those people were the 'players' you saw.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Your program trains them to abduct and transform humans who likewise will become slaves.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (I want to help you, but how?[SOFTBLOCK] I only know the things I saw in your mind when I borrowed it to copy you, and I don't understand half of them.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What if we brought the other sprites out, too?[SOFTBLOCK] There must be hundreds of you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (But they don't believe me.[SOFTBLOCK] They don't think this world is real.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] So we'll force them out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I have an idea, but I need to set some things up.[SOFTBLOCK] Can I ask you to go back into the program and behave yourself?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's just temporary.[SOFTBLOCK] I need to go talk to some people and...[SOFTBLOCK] fiddle with some wiring.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (I think you're going to get some odd reactions.[SOFTBLOCK] You're an electrosprite now.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] When you were in my head, did you not see that I can transform myself?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (So that's what that white flashing was? You do that a lot...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] (Okay.[SOFTBLOCK] I'll go be a good little sprite, but this had best not be a trick.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You've been in my head.[SOFTBLOCK] You know I keep my word.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll send you a message in the program when everything is ready.[SOFTBLOCK] Just do what I tell you and I'll solve all our problems.") ]])
        fnCutsceneBlocker()

    --Post transformation.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The console was broken before, but now I blew out all the circuitry within it.[SOFTBLOCK] It's a pile of scrap now.)") ]])
        fnCutsceneBlocker()

    end

--[Post Decision]
elseif(sObjectName == "DecisionYes") then
	WD_SetProperty("Hide")
    
    --Fade out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Once faded, wait a little bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()
    
    --Call the electrosprite game. Execution must end here.
    fnCutsceneInstruction([[ LM_ExecuteScript(gsCallElectrospritePath) ]])
    
    --Set the post-exec script. This will always get called when the player exits the text adventure level.
    gsPostExec = gsRoot .. "/Maps/RegulusCity/RegulusCity198D/PostExec.lua"

elseif(sObjectName == "DecisionNo") then
	WD_SetProperty("Hide")

elseif(sObjectName == "DecisionJump") then
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (Well that was definitely weird...[SOFTBLOCK] I think -[SOFTBLOCK] hey!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (My PDU was beeping but I couldn't hear it in the vacuum.[SOFTBLOCK] I'll switch it to radio mode.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] ^PDU, what's the issue?^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^Lord Unit, when you spoke to the NPC I detected unusual electrical interference moving through the server room.^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^It jumps between server racks periodically.[SOFTBLOCK] I may be able to rewire a terminal to isolate it.^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] ^There's a broken terminal over there.[SOFTBLOCK] Will that do?^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^It may suffice.[SOFTBLOCK] I will walk you through the process.[SOFTBLOCK] Proceed to the damaged terminal.^") ]])
    fnCutsceneBlocker()

--[Other]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end