--[Post-Exec]
--This is called after the game exits Text Adventure mode.
gsPostExec = nil

--Black out the screen.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()
    
--Fade back in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(15)
fnCutsceneBlocker()

--Dialogue execution is based on the variable states.
local iSawProblemWithProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N")
local iTalkedToElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N")
local iBecameElectrosprite   = VM_GetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N")
local iFinished198           = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")

--Nothing done yet:
if(iSawProblemWithProgram == 0.0 and iTalkedToElectrosprite == 0.0) then

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (I didn't really see anything unusual during that playthrough.[SOFTBLOCK] Maybe I should reset the program and try again?)") ]])
    fnCutsceneBlocker()

--Saw the problem but didn't fix it:
elseif(iSawProblemWithProgram == 1.0 and iTalkedToElectrosprite == 0.0) then

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (That NPC was definitely different from the others, but she warped or something.[SOFTBLOCK] I wonder what I should do?)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] ^PDU, activate radio mode. Did you notice anything unusual?^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^Yes, Lord Unit, but I was unable to track the odd behavior on the stack.[SOFTBLOCK] I will need you to interact with the NPC for a longer period.^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (Just like tracing a call...[SOFTBLOCK] but the NPC warped away.[SOFTBLOCK] I wonder if I can chase it somehow?)") ]])
    fnCutsceneBlocker()

--Talked to Susan but somehow *didn't* see the problem first, you hacker.
elseif(iSawProblemWithProgram == 0.0 and iTalkedToElectrosprite == 1.0 and iBecameElectrosprite == 0.0) then

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (Good thing I decided to randomly and for no reason at all type in that command.[SOFTBLOCK] Let's see what my PDU discovered.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] ^PDU, activate radio mode.[SOFTBLOCK] What have you discovered?^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^Lord Unit, when you spoke to the NPC I detected unusual electrical interference moving through the server room.^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^It jumps between server racks periodically.[SOFTBLOCK] I may be able to rewire a terminal to isolate it.^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] ^There's a broken terminal over there.[SOFTBLOCK] Will that do?^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^It may suffice.[SOFTBLOCK] I will look up circuit schematics for you.[SOFTBLOCK] Proceed to the damaged terminal.^") ]])
    fnCutsceneBlocker()

--Saw the problem and talked to the Electrosprite NPC:
elseif(iSawProblemWithProgram == 1.0 and iTalkedToElectrosprite == 1.0 and iBecameElectrosprite == 0.0) then

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (Well that was definitely weird...[SOFTBLOCK] Let's see what the PDU has to say.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] ^PDU, activate radio mode.[SOFTBLOCK] What have you discovered?^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^Lord Unit, when you spoke to the NPC I detected unusual electrical interference moving through the server room.^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^It jumps between server racks periodically.[SOFTBLOCK] I may be able to rewire a terminal to isolate it.^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] ^There's a broken terminal over there.[SOFTBLOCK] Will that do?^[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU] ^It may suffice.[SOFTBLOCK] I will look up circuit schematics for you.[SOFTBLOCK] Proceed to the damaged terminal.^") ]])
    fnCutsceneBlocker()

--Replays after becoming an electrosprite:
elseif(iSawProblemWithProgram == 1.0 and iTalkedToElectrosprite == 1.0 and iBecameElectrosprite == 1.0 and iFinished198 == 0.0) then

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (That was fun, but it doesn't look like Psue is in the program anymore.[SOFTBLOCK] I guess she's probably laying low to avoid attracting attention.)") ]])
    fnCutsceneBlocker()

--Replays after finishing the sector.
else

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (Converting humans is its own reward, and it's like I'm visiting old friends![SOFTBLOCK] Oh well, back to work.)") ]])
    fnCutsceneBlocker()

end