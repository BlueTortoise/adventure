--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCityA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        AL_SetProperty("Music", "RegulusCity")
    else
        AL_SetProperty("Music", "SophiesThemeSlow")
    end
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCityA")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnStandardNPCByPosition("GolemAA")
        fnStandardNPCByPosition("GolemAB")
        fnStandardNPCByPosition("GolemAC")
        fnStandardNPCByPosition("GolemAD")
        fnStandardNPCByPosition("GolemAE")
        fnStandardNPCByPosition("GolemAF")
        fnStandardNPCByPosition("GolemAG")
        fnStandardNPCByPosition("RaijuA")
        fnStandardNPCByPosition("RaijuB")
        fnStandardNPCByPosition("RaijuC")
        fnStandardNPCByPosition("RaijuD")
	
        EM_PushEntity("RaijuA")
            TA_SetProperty("Activate Wander Mode")
        DL_PopActiveObject()
        EM_PushEntity("RaijuB")
            TA_SetProperty("Activate Wander Mode")
        DL_PopActiveObject()

        --[Work Terminal]
        --Determine if the work terminal should be red or green. Green means an assignment is available, red means no assignments available.
        LM_ExecuteScript(gsRoot .. "Chapter5Scenes/WorkTerminal/QueryFunction.lua")

        --A work assignment is available, so show green work terminals.
        if(gbHasWorkAssignment == true) then
            AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", true)
            AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", true)
        end
	
    --Gala time! NPC listing changes.
    else
        fnStandardNPCByPosition("GolemGalaA")
        fnStandardNPCByPosition("GolemGalaB")
        fnStandardNPCByPosition("GolemGalaC")
        fnStandardNPCByPosition("GolemGalaD")
        EM_PushEntity("GolemGalaD")
            TA_SetProperty("Activate Wander Mode")
        DL_PopActiveObject()
    
    end
    
	--[Special]
	--Entering this area when the flags are right switches this variable. It changes Sophie's dialogue.
	if(true) then
		local iSophieImportantMeeting  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N")
		local iMet55InLowerRegulus     = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
		local iLeftRepairBay           = VM_GetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N")
		if(iSophieImportantMeeting == 2.0 and iMet55InLowerRegulus == 0.0 and iLeftRepairBay == 0.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N", 1.0)
		end
	end
end
