--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Plays a message when the player first enters Regulus City.
if(sObjectName == "WelcomeToRegulusCity") then

	--Variables.
	local iSawWelcomeToRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawWelcomeToRegulus", "N")
	
	--Play the scene.
	if(iSawWelcomeToRegulus == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWelcomeToRegulus", "N", 1.0)
		
		--Wait a bit for the fading.
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] Unidentified unit.[SOFTBLOCK] Please state designation.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Unit 771852 reporting for function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] You are not currently registered in the database.[SOFTBLOCK] Please proceed down the hallway to your left to the maintenance room.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] Airlock pressurized.[SOFTBLOCK] Welcome to Regulus City.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Affirmative.[SOFTBLOCK] Unit proceeding to maintenance room.") ]])
		fnCutsceneBlocker()
	end

--Forces the player to walk back a bit if they walk too far north without visiting the maintenance golem.
elseif(sObjectName == "NeedMaintenanceN") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--Play the scene.
	if(iTalkedToSophie == 0.0) then
		
		--Get Christine's position.
		EM_PushEntity("Christine")
			local fPlayerX, fPlayerY = TA_GetProperty("Position")
		DL_PopActiveObject()
		
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[VOICE|Christine] My programming indicates that I should go to the maintenance room before I do anything else.") ]])
		fnCutsceneBlocker()
		
		fnCutsceneMove("Christine", fPlayerX / gciSizePerTile, 25.50)
		fnCutsceneBlocker()
	end

--Forces the player to walk back a bit if they walk too far east without visiting the maintenance golem.
elseif(sObjectName == "NeedMaintenanceE") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--Play the scene.
	if(iTalkedToSophie == 0.0) then
		
		--Get Christine's position.
		EM_PushEntity("Christine")
			local fPlayerX, fPlayerY = TA_GetProperty("Position")
		DL_PopActiveObject()
		
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[VOICE|Christine] My programming indicates that I should go to the maintenance room before I do anything else.") ]])
		fnCutsceneBlocker()
		
		fnCutsceneMove("Christine", 21.25, fPlayerY / gciSizePerTile)
		fnCutsceneBlocker()
	end

end
