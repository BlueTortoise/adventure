--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCityB"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        AL_SetProperty("Music", "RegulusCity")
    else
        AL_SetProperty("Music", "SophiesThemeSlow")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCityB")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnStandardNPCByPosition("GolemBA")
        fnStandardNPCByPosition("GolemBB")
        fnStandardNPCByPosition("GolemBC")
        fnStandardNPCByPosition("GolemBD")
        fnStandardNPCByPosition("GolemBE")
        fnStandardNPCByPosition("GolemBF")
        fnStandardNPCByPosition("GolemBG")
        fnStandardNPCByPosition("GolemBH")
        fnStandardNPCByPosition("GolemBI")
        fnStandardNPCByPosition("GolemBJ")
        fnStandardNPCByPosition("GolemBK")

        --[Facing Override]
        --This value will be >= 0 if the player used the elevators in lower Regulus City. The force facing
        -- is always South, and can only ever affect Christine. 55 and Sophie are never present for this.
        if(gi_Force_Facing ~= -1) then
            fnCutsceneFace("Christine", 0, 1)
            gi_Force_Facing = -1
        end

        --[Work Terminal]
        --Determine if the work terminal should be red or green. Green means an assignment is available, red means no assignments available.
        LM_ExecuteScript(gsRoot .. "Chapter5Scenes/WorkTerminal/QueryFunction.lua")

        --A work assignment is available, so show green work terminals.
        if(gbHasWorkAssignment == true) then
            AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", true)
            AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", true)
        end
        
    --Gala time! NPC listing changes.
    else
        fnStandardNPCByPosition("GolemGalaA")
        fnStandardNPCByPosition("GolemGalaB")
        fnStandardNPCByPosition("GolemGalaC")
        fnStandardNPCByPosition("GolemGalaD")
        fnStandardNPCByPosition("GolemGalaE")
	end
end
