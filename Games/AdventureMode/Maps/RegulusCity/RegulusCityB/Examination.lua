--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Global temporary variable.
sLastElevator = "ElevatorsL"

--[Examinables]
--Vending machine.
if(sObjectName == "FizzyDrinkBlue") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Fizzy drinks, a perfect way to boost your productive output![SOFTBLOCK] This is the blue variety, meant for manual labour units.[SOFTBLOCK] The chemical composition is the same as the pink variety, though.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I really don't want to risk spilling any Fizzy Pop! on my dress...)") ]])
        fnCutsceneBlocker()
    end
    
--Vending machine.
elseif(sObjectName == "FizzyDrinkPink") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Fizzy drinks, a perfect way to boost your productive output![SOFTBLOCK] This is the pink variety, meant for Lord Units like me.[SOFTBLOCK] The chemical composition is the same as the blue variety, though.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I really don't want to risk spilling any Fizzy Pop! on my dress...)") ]])
        fnCutsceneBlocker()
    end
	
--Booze.
elseif(sObjectName == "Booze") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The smell of alcohol is unmistakable.[SOFTBLOCK] This would have no useful digestive properties for a Golem, but would probably intoxicate an organic.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I doubt there will be any organics at the gala, but if there were, they'd be drinking this.)") ]])
        fnCutsceneBlocker()
    end

--Broken cryogenics equipment.
elseif(sObjectName == "CryoEquipment") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A conversion cell and terminals.[SOFTBLOCK] Looks like it's here for servicing.)") ]])
	fnCutsceneBlocker()
	
--In-use console.
elseif(sObjectName == "Console") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This console is in use already.[SOFTBLOCK] Looks like a combat simulation program is running.)") ]])
        fnCutsceneBlocker()
	else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Oddly, whoever plays the armored heroine character seems to win at this simulation...)") ]])
        fnCutsceneBlocker()
    end
    
--Game Screen.
elseif(sObjectName == "GameScreen") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A combat simulation program, paused at the moment.[SOFTBLOCK] The fighter on the left seems to be winning.)") ]])
        fnCutsceneBlocker()
	else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The armored heroine character always seems to lose at first, but then she gets the missile launcher and it's all over.)") ]])
        fnCutsceneBlocker()
    end

--[Work Terminal]
--Offsite dialogue script.
elseif(sObjectName == "WorkTerminal") then

    --Variables.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    
    if(iIsGalaTime == 0.0) then
        LM_ExecuteScript(gsRoot .. "Chapter5Scenes/WorkTerminal/Execution.lua")
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](No need to access the work terminal right now.[SOFTBLOCK] I've got a gala to attend.)") ]])
        fnCutsceneBlocker()
    end

--Apartment Terminal
elseif(sObjectName == "ApartmentTerminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This terminal shows address listings for the various units in the residential block above.)") ]])

--[Elevators]
--Enter Christine's quarters.
elseif(sObjectName == "ElevatorsL" or sObjectName == "ElevatorsR") then

	--Variables.
	sLastElevator = sObjectName
	local iReceivedFunction        = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
	local iToldSophieAboutFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N")
	local iSophieImportantMeeting  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N")
	local iMet55InLowerRegulus     = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N") 
	local iSophieFirstDateState    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
    local iIsGalaTime              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
	local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")

    --Gala.
    if(iIsGalaTime > 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Going into the basement and roughing up some maverick robots might scuff my dress.[SOFTBLOCK] Inconceivable!)") ]])
		fnCutsceneBlocker()

	--Player doesn't have a function assignment. Don't use the elevator.
	elseif(iReceivedFunction == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This elevator can take me to my quarters, but my programming dictates I should get my function assignment first.)") ]])
		fnCutsceneBlocker()

	--Player has not told Sophie about her function yet. Don't use the elevator.
	elseif(iToldSophieAboutFunction == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This elevator can take me to my quarters, but I should really go tell Sophie about my function assignment first!)") ]])
		fnCutsceneBlocker()
        
    --Shopping!
    elseif(iStartedShoppingSequence == 1.0 and iIsOnDate == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I need to take Sophie to the tram, not my room.)") ]])
		fnCutsceneBlocker()
	
	--Player has not completed her first date with Sophie yet.
	elseif(iSophieFirstDateState < 2.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I should go find something fun to do with Sophie before I check out my quarters.)") ]])
		fnCutsceneBlocker()

	--Player has access to only their quarters:
	elseif(iSophieImportantMeeting == 0.0) then

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"My Quarters\", " .. sDecisionScript .. ", \"ElevatorToQuarters\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Important meeting with Sophie.
	elseif(iSophieImportantMeeting == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Sophie said it would safe to talk while in the repair bay...)") ]])
		fnCutsceneBlocker()
		
	--Player has access to the lower floors, and reminds herself about 55.
	elseif(iSophieImportantMeeting == 2.0 and iMet55InLowerRegulus == 0.0) then

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Which floor should I go to?[SOFTBLOCK] The PDU said 2855 was using terminals on the first basement floor...)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"My Quarters\", " .. sDecisionScript .. ", \"ElevatorToQuarters\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Basement 1F\", " .. sDecisionScript .. ", \"ElevatorToBasement1F\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	
	--Player has access to the lower floors. No reminder.
	else

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"My Quarters\", " .. sDecisionScript .. ", \"ElevatorToQuarters\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Basement 1F\", " .. sDecisionScript .. ", \"ElevatorToBasement1F\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Basement 2F\", " .. sDecisionScript .. ", \"ElevatorToBasement2F\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	
	end

--Elevator to Christine's Quarters.
elseif(sObjectName == "ElevatorToQuarters") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Change maps.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.5x10.0x0") ]])
	fnCutsceneBlocker()

--Elevator to Basement 1F.
elseif(sObjectName == "ElevatorToBasement1F") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--If Sophie is present:
	if(gsFollowersTotal > 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Erm, Christine?[SOFTBLOCK] I probably shouldn't go to the basement...[SOFTBLOCK] I'm not [SOFTBLOCK]*cleared*[SOFTBLOCK] like someone we know.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Cleared?[SOFTBLOCK] Oh![SOFTBLOCK] Of course, sorry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Did you finger slip?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yep, that's it.") ]])
	
	--Nope.
	else
	
		--Change maps.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		if(sLastElevator == "ElevatorsL") then
			fnCutsceneInstruction([[ AL_BeginTransitionTo("LowerRegulusCityA", "FORCEPOS:22.5x15.0x0") ]])
		else
			fnCutsceneInstruction([[ AL_BeginTransitionTo("LowerRegulusCityA", "FORCEPOS:27.5x15.0x0") ]])
		end
		fnCutsceneBlocker()
	end

--Elevator to Basement 2F.
elseif(sObjectName == "ElevatorToBasement2F") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--If Sophie is present:
	if(gsFollowersTotal > 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Erm, Christine?[SOFTBLOCK] I probably shouldn't go to the basement...[SOFTBLOCK] I'm not [SOFTBLOCK]*cleared*[SOFTBLOCK] like someone we know.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Cleared?[SOFTBLOCK] Oh![SOFTBLOCK] Of course, sorry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Did you finger slip?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yep, that's it.") ]])
	
	--Nope.
	else
	
		--Change maps.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		if(sLastElevator == "ElevatorsL") then
			fnCutsceneInstruction([[ AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:27.5x13.0x0") ]])
		else
			fnCutsceneInstruction([[ AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:31.5x13.0x0") ]])
		end
		fnCutsceneBlocker()
	end

--[Execution]
--Fabricator that is unused.
elseif(sObjectName == "FabricatorDate") then

	--Variables.
	local iIsOnDate            = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedFabricator = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedFabricator", "N")
    local iIsGalaTime          = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")

    --Gala case.
    if(iIsGalaTime > 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The fabricators are quiet for the first time since I was converted...)") ]])
		fnCutsceneBlocker()

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0 or iStartedShoppingSequence == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A fabrication machine.[SOFTBLOCK] This one is currently not in use.)") ]])
		fnCutsceneBlocker()

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedFabricator == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedFabricator", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This fabricator is currently not in use.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Would you like to spend an evening working one with me?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Just let your subroutines do the handwork.[SOFTBLOCK] We could chat with one another.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You'd find that to be a good time?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I want to know more about you.[SOFTBLOCK] Let's spend some time chatting.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Should we spend the evening working the fabricators, then?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WorkFabricators\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedFabricator == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Should we spend the evening working the fabricators?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WorkFabricators\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	end

--[Post Decision]
--Spend a date working the fabricators.
elseif(sObjectName == "WorkFabricators") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Fade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Sophie Conversation/Routing.lua")
	fnCutsceneBlocker()

	--Fade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")
	
	--First date.
	if(iSophieFirstDateState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Who would have thought manual labour could be so much fun with you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But, I really should go defragment my drives.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ummm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hee hee![SOFTBLOCK] You haven't even seen your quarters yet, have you?[SOFTBLOCK] Do you know where they are?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Vaguely.[SOFTBLOCK] I didn't download the directory when I got here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Well then allow me to show you.[SOFTBLOCK] We passed the elevators on the way here, they're to the west.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Thanks, Sophie.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] This does count as walking a lady back to her room.[SOFTBLOCK] We use the same elevators, of course.[SOFTBLOCK] All units in the sector use the same domicile block.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right.[SOFTBLOCK] Let's go.") ]])
		fnCutsceneBlocker()
	
	--On successive dates, Sophie requests to return to the maintenance bay unless she wants to synchronize.
	elseif(iSophieWillSynchronizeNow == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, look at that![SOFTBLOCK] I should be getting back to the maintenace bay.[SOFTBLOCK] I better get everything logged before I defragment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right, then.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I had a lot of fun today![SOFTBLOCK] We should do this again![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Me too, Sophie.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Would you care to escort your tandem unit?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] With pleasure.") ]])
		fnCutsceneBlocker()
	end
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
	
elseif(sObjectName == "NoClose") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end