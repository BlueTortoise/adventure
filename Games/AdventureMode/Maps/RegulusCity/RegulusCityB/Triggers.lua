--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Cutscene where Sophie shows Christine the elevators.
if(sObjectName == "ElevatorTriggerL" or sObjectName == "ElevatorTriggerR") then

	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	
	--Scene.
	if(iSophieFirstDateState == 1.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 2.0)
			
		--Remove Sophie from the party. She will have a different spawn in the next segment.
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "Sophie")
		
		--Movement for the left side.
		if(sObjectName == "ElevatorTriggerL") then
			fnCutsceneMove("Sophie", 7.25, 15.50)
			fnCutsceneMove("Christine", 6.25, 15.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Christine", 0, -1)
			fnCutsceneFace("Sophie", 0, -1)
			fnCutsceneBlocker()
			fnCutsceneWait(45)
			fnCutsceneBlocker()
		
		--Movement for the right side.
		else
			fnCutsceneMove("Sophie", 11.25, 15.50)
			fnCutsceneMove("Christine", 12.25, 15.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Christine", 0, -1)
			fnCutsceneFace("Sophie", 0, -1)
			fnCutsceneBlocker()
			fnCutsceneWait(45)
			fnCutsceneBlocker()
		end

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] These elevators can take you to the apartment blocks above us.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] They also go down to the basement levels, but we probably shouldn't go there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are there apartments in the basement as well?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hm?[SOFTBLOCK] Oh, that's not what I meant.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] When Regulus City was being built, there wasn't really a plan.[SOFTBLOCK] As a result, there's a lot of now-unused corridors and access points.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Sometimes things get in, so security doesn't like us going down there without a good reason.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] *Things*?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, the wildlife of Regulus.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] There are quite a few voidborne species of partirhuman on Regulus, and sometimes a broken robot will go maverick down there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But you look like you'd be able to handle them...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, thank you![SOFTBLOCK] I used to coach -[SOFTBLOCK] hmm...[SOFTBLOCK] my memory drives are a little fuzzy.[SOFTBLOCK] I think I used to teach sports...[SOFTBLOCK] maybe?[SOFTBLOCK] I think I taught literature, too.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Don't worry about it.[SOFTBLOCK] Your organic memories got repurposed when you were converted.[SOFTBLOCK] It causes much less distress than flat reprogramming.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Who you were before doesn't really matter that much.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No.[SOFTBLOCK] No it doesn't.[SOFTBLOCK] What matters is now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Well.[SOFTBLOCK] I'll get the other one to my quarters.[SOFTBLOCK] Good night.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Should we -[SOFTBLOCK] will you -[SOFTBLOCK] you'll be there tomorrow, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Of course I will, you silly spanner![SOFTBLOCK] Where else would I be?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Of course.[SOFTBLOCK] Of course.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Sophie walks to the opposite elevator, left side:
		if(sObjectName == "ElevatorTriggerL") then
			fnCutsceneMove("Christine", 6.75, 15.50)
			fnCutsceneMove("Sophie", 11.25, 15.50)
			fnCutsceneMove("Sophie", 11.25, 14.50)
			fnCutsceneBlocker()
			fnCutsceneWait(45)
			fnCutsceneBlocker()
		
		--Right side.
		else
			fnCutsceneMove("Christine", 11.75, 15.50)
			fnCutsceneMove("Sophie", 7.25, 15.50)
			fnCutsceneMove("Sophie", 7.25, 14.50)
			fnCutsceneBlocker()
			fnCutsceneWait(45)
			fnCutsceneBlocker()
		end
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Goodnight...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Sophie turns to face Christine, left side:
		if(sObjectName == "ElevatorTriggerL") then
			fnCutsceneFace("Sophie", -1, 0)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
		--Right side.
		else
			fnCutsceneFace("Sophie", 1, 0)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		end
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] See you tomorrow!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Common.
		fnCutsceneFace("Sophie", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneTeleport("Sophie", -100.25, -100.50)
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneWait(85)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneWait(75)
		fnCutsceneBlocker()
		
		--Fade to black, stop the music.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneWait(120)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Music", "NullSlow") ]])
		fnCutsceneWait(120)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AM_SetProperty("Execute Rest") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Please don't be a wonderful dream...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(85)
		fnCutsceneBlocker()
		
		--Begin the date montage scene.
		fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityWholeCutscene", gsRoot .. "Chapter5Scenes/DateMontage/Scene_Begin_PostTransition.lua") ]])
		fnCutsceneBlocker()

	end
end
