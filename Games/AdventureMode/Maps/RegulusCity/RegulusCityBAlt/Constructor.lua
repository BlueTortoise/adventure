--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCityBAlt"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    AL_SetProperty("Music", "Null")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCityB")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
    for i = string.byte("A"), string.byte("C"), 1 do
        fnStandardNPCByPosition("GolemLord" .. string.char(i))
    end
    for i = string.byte("A"), string.byte("K"), 1 do
        fnStandardNPCByPosition("Golem" .. string.char(i))
    end
    
    --[Animation]
    --This is used in the second part of the cutscene.
    SLF_Open(gsDatafilesPath .. "MapAnimations.slf")

    --Modify the distance filters to keep everything pixellated.
    local iNearest = DM_GetEnumeration("GL_NEAREST")
    ALB_SetTextureProperty("MagFilter", iNearest)
    ALB_SetTextureProperty("MinFilter", iNearest)
    ALB_SetTextureProperty("Special Sprite Padding", true)
    Bitmap_ActivateAtlasing()

    --Load. 12 animations, 20 frames per animation. Last one has 40.
    DL_AddPath("Root/Images/TempImg/ChristineGolem/")
    local iExpected = 11 * 20
    iExpected = iExpected + 40
    for i = 1, iExpected, 1 do
        local sNum = string.format("%03i", i-1)
        DL_ExtractBitmap("ChristineGolem|" .. sNum, "Root/Images/TempImg/ChristineGolem/" .. sNum)
    end

    --Build it.
    AL_SetProperty("Add Animation", "ChristineGolem", 21.90 * gciSizePerTile, 12.50 * gciSizePerTile, -0.000002)
    AL_SetProperty("Set Animation From Pattern", "ChristineGolem", "ChristineGolem", iExpected)
    
    --[Clean]
    ALB_SetTextureProperty("Special Sprite Padding", false)
    Bitmap_DeactivateAtlasing()
    ALB_SetTextureProperty("Restore Defaults")
    SLF_Close()
end
