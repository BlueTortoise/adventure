--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Cutscene where Sophie shows Christine the elevators.
if(sObjectName == "SceneTrigger") then
    
    --Flags.
    local iHumanFirstScene = VM_GetVar("Root/Variables/Chapter5/Sophie/iHumanFirstScene", "N")
    if(iHumanFirstScene == 1.0) then return end
    
    --Set.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanFirstScene", "N", 1.0)
    
    --Black the screen out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn Sophie. She's offscreen and will appear later.
    fnSpecialCharacter("Sophie", "Golem", -100, -100, gci_Face_South, false, nil)
    
    --Focus the camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (31.25 * gciSizePerTile), (23.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    
    --Move Christine offscreen.
    fnCutsceneTeleport("Christine", -1.25, -1.50)
    
    --Fade in.
    fnCutsceneWait(25)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Trigger a dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "GolemLord", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] So anyway, that's why you need to learn all the ki skills before you move past level six.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] After all, if you don't have them you may as well restart the game.[SOFTBLOCK] Unless you're on a no-fire run, but only speedrunners do that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave:[E|Neutral] Yes, Lord Unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave:[E|Neutral] (Someone shoot me.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] And then we get to the genius that is level twelve, when the redbirds come back with new pockfire skills.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] [SOFTBLOCK]Hmm?[SOFTBLOCK] Did you hear that?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Focus on the elevator.
    fnCutsceneInstruction([[ AL_SetProperty("Music", "EquinoxTheme") ]])
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (27.75 * gciSizePerTile), (24.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneTeleport("Christine", 27.75, 24.50)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemLordA", 0, 1)
    fnCutsceneFace("GolemA", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Oh no!
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Human![SOFTBLOCK] Rogue human![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] That's right![SOFTBLOCK] Fear me, metal girls![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Someone help![SOFTBLOCK] Rogue human![SOFTBLOCK] Stop her!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("GolemH", 33.25, 23.50, 3.0)
    fnCutsceneMove("Christine", 32.25, 24.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Trigger a dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "GolemLord", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] C-[SOFTBLOCK]cease and desist, human![SOFTBLOCK] You will be converted![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, is that so, robot girl?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *burp*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] You belched on me?[SOFTBLOCK] Disgusting![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] There's more where that came from![SOFTBLOCK] We organics are constantly ingesting air and it can easily come back up in bursts like this![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Intolerable![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Not only that, but my disgestive tract produce noxious methane gasses![SOFTBLOCK] Would you like to smell them?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Please no![SOFTBLOCK] Someone, stop this degenerate!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --More movement.
    fnCutsceneMoveFace("GolemLordA", 31.25, 22.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemA", 32.25, 22.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemH", 33.25, 22.50, 0, 1, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Slave Units![SOFTBLOCK] Get her![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave:[VOICE|Golem] Uhh, after you, Lord Unit![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave:[VOICE|Golem] Show us how it's done![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Just contain her![SOFTBLOCK] Don't let her get away!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ThatsEnough") then
    
    --Flags.
    local iHumanSecondScene = VM_GetVar("Root/Variables/Chapter5/Sophie/iHumanSecondScene", "N")
    if(iHumanSecondScene == 1.0) then return end
    
    --Set.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanSecondScene", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 17.25, 16.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("GolemLordB", 17.25, 15.50)
    fnCutsceneFace("GolemLordB", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Trigger a dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] That's far enough, human.[SOFTBLOCK] You've had your fun.[SOFTBLOCK] Come along quietly and be converted.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Oh, okay?[SOFTBLOCK] Would you mind if I just shed my hair and skin all over you now?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] What?[SOFTBLOCK] What did you say?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Unlike your synthetic chassis and hair, mine constantly grows and falls out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Everywhere I go, I leave skin, nails, hair -[SOFTBLOCK] and it coats every surface![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Clean it all you want, it just comes back![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I -[SOFTBLOCK] I need to get myself cleaned![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And then what, Lord Unit?[SOFTBLOCK] I wonder how many loose strands of my hair will find their way into the small cracks of this city?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It could be years before you find them all![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh my![SOFTBLOCK] I might have a system lock![SOFTBLOCK] Will no one save us from this terrible monster?") ]])
    
elseif(sObjectName == "Flee") then
    
    --Flags.
    local iHumanThirdScene = VM_GetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N")
    if(iHumanThirdScene == 1.0) then return end
    
    --Set.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 36.25, 15.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Trigger a dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] You have nowhere to run, metal girl![SOFTBLOCK] Kneel before me![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Heeeelp![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Sophie] Did someone call me?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Music starts.
    fnCutsceneTeleport("Sophie", 20.25, 15.50)
    fnCutsceneInstruction([[ AL_SetProperty("Music", "SophieToTheRescue") ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sophie", 33.25, 15.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Your reign of terror ends here, rogue human![SOFTBLOCK] Maintenance and Repair is here to save the day![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh yeah?[SOFTBLOCK] What makes you different than these other cowering metal girls, huh?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Because repair units -[SOFTBLOCK] fix -[SOFTBLOCK] problems!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Line up.
    fnCutsceneMoveFace("Sophie", 33.75, 15.50, 1, 0, 0.50)
    fnCutsceneMoveFace("Christine", 36.75, 15.50, -1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] En-garde![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Have at you!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sophie", 34.75, 15.50, 2.50)
    fnCutsceneMove("Christine", 35.25, 15.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("Christine", "Wounded")
    fnCutsceneMove("Christine", 36.25, 15.50, 0.20)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Kyaaa![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] *Uh oh, did I get you?[SOFTBLOCK] You all right?*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] *Oh you're not looking so good...[SOFTBLOCK] But you're not bleeding.[SOFTBLOCK] Are you okay?*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] *Cough*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I am vanquished by this beautiful, confident, strong repair unit![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Y-[SOFTBLOCK]yeah![SOFTBLOCK] Take notes, machine sisters![SOFTBLOCK] This is how you do it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] And then you just put this golem core on them, and everything is all right!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("Christine", "Crouch")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneSetFrame("Christine", "Null")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Motor control of subject established.[SOFTBLOCK] Resistance at zero.[SOFTBLOCK] Ready for commands.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Follow me to the conversion chamber, human.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sophie", 37.25, 15.50)
    fnCutsceneMove("GolemJ", 38.25, 16.50)
    fnCutsceneFace("GolemJ", 0, -1)
    fnCutsceneMove("GolemLordC", 39.25, 14.50)
    fnCutsceneMove("GolemLordC", 39.25, 16.50)
    fnCutsceneFace("GolemLordC", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneMove("Sophie", 38.25, 15.50)
    fnCutsceneMove("Christine", 37.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Wow, you did it, Unit 499323![SOFTBLOCK] Great work![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] You really mean that, Lord Unit?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Of course![SOFTBLOCK] Who would have thought clumsily ramming into the human would incapacitate it so thoroughly?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] It was nothing special.[SOFTBLOCK] I was just fulfilling my function assignment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Leave the rest to me![SOFTBLOCK] I'll get her converted into a productive golem in two shakes of a linked-list's tail-element!") ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("GolemJ", 22.25, 16.50)
    fnCutsceneMove("GolemLordC", 22.25, 16.50)
    fnCutsceneMove("Sophie", 41.25, 15.50)
    fnCutsceneMove("Sophie", 41.25, 10.50)
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneMove("Christine", 41.25, 15.50)
    fnCutsceneMove("Christine", 41.25, 11.50)
    fnCutsceneMove("Christine", 42.25, 10.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneBlocker()
    
    --Fade out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] You all set, Christine?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Subject is standing by.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Isn't this so much fun?[SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Subject is standing by.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Ooops, I guess I'm talking to myself again.[SOFTBLOCK] Okay, get in the tube.[SOFTBLOCK] I'll monitor you from here.") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
    
    --Major animation sequence.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Rendering", "ChristineGolem", true) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Major Animation", "ChristineGolem") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Frame", "ChristineGolem", 0.0) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "ChristineGolem", 0.5, 19.5, "Loop", 0.10, 20.0 * 12.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The core had taken total control of Christine's body by tapping into her spinal column and intercepting her nervous sytem's signals.[SOFTBLOCK] She was a mere puppet now.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She had put up no resistance to it.[SOFTBLOCK] In fact, her brain was already prepared and feeding the core's override useful information.[SOFTBLOCK] The core needed to regulate her heart rate, learn her muscle impulse signals, and many more things.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Now she floated in a familiar breathing fluid.[SOFTBLOCK] It coated her lungs.[SOFTBLOCK] She allows the tubes to connect to her, to fill her insides with conversion fluid.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sequence.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "ChristineGolem", 40.5, 59.5, "Loop", 0.10, 20.0 * 12.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She closed her eyes and let the feeling of perfection work up her body.[SOFTBLOCK] The fluid began seeping into her skin, changing her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The core on her chest began its work, organizing the fluid with electrical impulses.[SOFTBLOCK] It began building substructures in her skin and organs, turning her into carbon-nanofiber and metal.[SOFTBLOCK] It tingled, exciting her wherever the feeling spread.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sequence.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "ChristineGolem", 80.5, 109.5, "Loop", 0.10, 20.0 * 12.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She floated in the warm fluid, relaxing and allowing the machines to do their work on her.[SOFTBLOCK] This, she realized, was what pure bliss feels like.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] No panic, no pain.[SOFTBLOCK] No fear, no stress.[SOFTBLOCK] She was being improved, and all she had to do was allow herself to.[SOFTBLOCK] The core continued to send her brain positive reinforcement.[SOFTBLOCK] It told her she was a good girl.[SOFTBLOCK] She agreed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She felt her breasts become coated with metal, then, infused.[SOFTBLOCK] They became solid, forever pert and firm.[SOFTBLOCK] She imagined Sophie sliding her metal fingers across them delicately, and excitement built within her as the fluid continued to fill her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Her limbs, further from the core, began to change.[SOFTBLOCK] The bones became conduit, the flesh became glass and metal.[SOFTBLOCK] The fluid continued to pump inside her and fill her.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sequence.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "ChristineGolem", 120.5, 139.5, "Loop", 0.10, 20.0 * 12.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Now the fluid began to finalize her limbs.[SOFTBLOCK] She held still as it told her to, breathed when it told her to, thought what it told her to.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Moving on from her limbs, it focused on her head.[SOFTBLOCK] It organized her neck into a powerful conduit that would transmit energy and instructions from her core to her brain.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She began to feel her thoughts become distant.[SOFTBLOCK] They slipped from her.[SOFTBLOCK] She was losing herself.[SOFTBLOCK] A surge built in her loins at the very idea of it.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sequence.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "ChristineGolem", 160.5, 179.5, "Loop", 0.10, 20.0 * 12.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] As her thoughts flooded from her, a black reprogramming headpiece lowered from above her.[SOFTBLOCK] She felt it close around her face.[SOFTBLOCK] Her face...[SOFTBLOCK] her skin had become fluid.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Needles inserted from the helmet through her softened skin and bones, piercing into her brain.[SOFTBLOCK] They began to knead at her.[SOFTBLOCK] Change her.[SOFTBLOCK] Improve her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Her thoughts roared back as digital perfection replaced the organic chaos.[SOFTBLOCK] They became clear, sharp, synthetic.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sequence.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Loop", "ChristineGolem", 200.5, 219.5, "Loop", 0.10, 20.0 * 12.0) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She knew the next step was to rewrite her organic memories to better suit her new function.[SOFTBLOCK] She was eager.[SOFTBLOCK] She could not wait to serve.[SOFTBLOCK] She could not wait to be assigned.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She saw the moments of her life before her.[SOFTBLOCK] She knew they were being changed.[SOFTBLOCK] She did not care.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She saw herself as a finished unit.[SOFTBLOCK] She was a Lord Unit, watching units exit a tram car.[SOFTBLOCK] She checked them against her PDU's database.[SOFTBLOCK] The units walked past her in lockstep.[SOFTBLOCK] She confirmed compliance.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] As the last unit moved past her, she took her place at the end of the line.[SOFTBLOCK] She synchronized her steps with the units in front of her.[SOFTBLOCK] As a block, they marched together.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The group began to split and break off into corridors as they received local area assignments.[SOFTBLOCK] The first six units were line runners, the next three were delivery, the next five were assembly, and so on.[SOFTBLOCK] She marched and awaited her local assignment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] At the end, she walked through the corridor alone.[SOFTBLOCK] Units passed around her, delivering parts, conducting maintenance.[SOFTBLOCK] Her assignment was more important.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She reached the end of the corridor and entered the small closet there.[SOFTBLOCK] Inside was Unit 499323, a green-haired repair unit.[SOFTBLOCK] The unit smiled bashfully at her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Her programming commanded her to state out loud[SOFTBLOCK] 'This Unit is here to love, obey, and protect you.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Unit 499323 looked to the ground, brushed at her blue skirt, and mumbled out a reply.[SOFTBLOCK] 'Unit 499323 returns your love.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Having received her function assignment, the core relinquished direct control.[SOFTBLOCK] Her new task of loving, obeying, and protecting Unit 499323 would be her function assignment for the remainder of her existence.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sequence.
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Destination", "ChristineGolem", 259.5, 0.10) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Its task completed, the helmet began to detach from her head.[SOFTBLOCK] The hoses disconnected.[SOFTBLOCK] During her programming, she had already finished charging her new power core.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] As the fluid drained from the tube, she stared ahead.[SOFTBLOCK] Her systems began their secondary boot checks.[SOFTBLOCK] Following her programming, she stated aloud 'Reprogramming complete'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The special core that Sophie had prepared for her had left her memories intact.[SOFTBLOCK] Most of them.[SOFTBLOCK] There was one new memory, but Unit 771852 could not tell which one it was.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Unit 499323 stood nearby, waiting for her.[SOFTBLOCK] Her smile radiated brighter than any star as she examined the new golem.[SOFTBLOCK] Unit 771852 confirmed her objectives.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She was to love, obey, and protect Unit 499323 with her whole being.[SOFTBLOCK] Unit 499323 would do the same with her.[SOFTBLOCK] This was her true function assignment.[SOFTBLOCK] All other priorities were secondary to this one.[SOFTBLOCK] She smiled.[SOFTBLOCK] All was well.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Come on, Christine![SOFTBLOCK] I've got your things in the repair bay, let's go get you dressed!") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Set Animation Rendering", "ChristineGolem", false) ]])
    fnCutsceneInstruction([[ AL_SetProperty("Major Animation", "Null") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Transition.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityC", "FORCEPOS:23.0x13.0x0") ]])
    fnCutsceneBlocker()
end
