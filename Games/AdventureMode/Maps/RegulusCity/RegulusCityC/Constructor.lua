--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCityC"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        AL_SetProperty("Music", "RegulusCity")
    else
        AL_SetProperty("Music", "SophiesThemeSlow")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCityC")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iSophieImportantMeeting = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N")
	local iSophieWentToQuarters = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWentToQuarters", "N")
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")

	--Sophie. Doesn't spawn if currently following Christine.
	if((iIsOnDate == 0.0 or iIsOnDate == 3.0) and (iSophieImportantMeeting == 0.0 or iSophieImportantMeeting == 2.0) and iIsGalaTime == 0.0) then
		
		--If a date finished but Sophie went to her quarters, don't spawn her.
		if(iIsOnDate == 3.0 and iSophieWentToQuarters == 1.0) then
			
		--All checks passed. Spawn her.
		else
			fnSpecialCharacter("Sophie", "Golem", 22, 12, gci_Face_North, true, gsRoot .. "CharacterDialogue/RegulusCity/Sophie/Root.lua")
		end
	end
    
    --[Animation]
    --This is used during the "inspection" sequence. The animation has its own SLF file.
    SLF_Open(gsDatafilesPath .. "MapAnimations.slf")

    --Modify the distance filters to keep everything pixellated.
    local iNearest = DM_GetEnumeration("GL_NEAREST")
    ALB_SetTextureProperty("MagFilter", iNearest)
    ALB_SetTextureProperty("MinFilter", iNearest)
    ALB_SetTextureProperty("Special Sprite Padding", true)
    Bitmap_ActivateAtlasing()

    --Load. 11 animations, 20 frames per animation.
    DL_AddPath("Root/Images/TempImg/Inspection/")
    local iExpected = 11 * 20
    for i = 1, iExpected, 1 do
        local sNum = string.format("%03i", i-1)
        DL_ExtractBitmap("Inspection|" .. sNum, "Root/Images/TempImg/Inspection/" .. sNum)
    end

    --Build it.
    AL_SetProperty("Add Animation", "Inspection", 21.90 * gciSizePerTile, 12.50 * gciSizePerTile, -0.000002)
    AL_SetProperty("Set Animation From Pattern", "Inspection", "Inspection", iExpected)
    
    --[Clean]
    ALB_SetTextureProperty("Special Sprite Padding", false)
    Bitmap_DeactivateAtlasing()
    ALB_SetTextureProperty("Restore Defaults")
    SLF_Close()
    
    --[Friend Golem Linda]
    --Has a birb!
    local iCanSpawnBirdcage = VM_GetVar("Root/Variables/Chapter5/Scenes/iCanSpawnBirdcage", "N")
    if(iCanSpawnBirdcage == 1.0 and iIsGalaTime == 0.0) then
        TA_Create("Linda")
            TA_SetProperty("Position", 18, 5)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", true)
            TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
            fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
        DL_PopActiveObject()
        AL_SetProperty("Set Collision", 19, 5, 0, 1)
    
    --Remove the birdcage.
    else
        AL_SetProperty("Set Layer Disabled", "BirdcageLo", true)
        AL_SetProperty("Set Layer Disabled", "BirdcageHi", true)
    end
    
    --This check runs whenever we enter to recompute if Linda and Herbert show up.
    local iMetFriendGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetFriendGolem", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    if(iMetFriendGolem == 1.0 and iStartedShoppingSequence >= 4.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iCanSpawnBirdcage", "N", 1.0)
    end
    
end
