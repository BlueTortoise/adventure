--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Other Examinables]
--Maintenance scanning terminal.
if(sObjectName == "ScanTerminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A scanning terminal.[SOFTBLOCK] This is where units upload their physical and mental data when being maintained.[SOFTBLOCK] Better not touch it, Sophie's the expert on these.)") ]])
	fnCutsceneBlocker()
	
--Crates.
elseif(sObjectName == "Crates") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Crates of various sizes.[SOFTBLOCK] Some of the labels are spare parts, while others are machines here to be repaired.)") ]])
	fnCutsceneBlocker()

--[Date Objects]
--Objects the player can maintain.
elseif(sObjectName == "WorkObjects") then

	--Variables.
	local iIsOnDate             = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedMaintenance = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedMaintenance", "N")
    local iIsGalaTime           = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")

    --Gala.
    if(iIsGalaTime > 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Christine...[SOFTBLOCK] after all this...[SOFTBLOCK] will we come back here?[SOFTBLOCK] Will we work with these tools together again?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We will.[SOFTBLOCK] We will, as free units.[SOFTBLOCK] I promise you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'll hold you to that...") ]])

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0 or iStartedShoppingSequence == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Equipment in various states of disrepair, and tools to fix them with.)") ]])
		fnCutsceneBlocker()

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedMaintenance == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedMaintenance", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So this is what you work on?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Most of the time.[SOFTBLOCK] I get equipment brought in here and fix it.[SOFTBLOCK] Sometimes I work on site if the machines are too heavy.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Do you want to spend some time working on them?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You'd consider that a good time?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I really like my job, actually.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Plus we can just let our subroutines do the work while we talk.[SOFTBLOCK] Get to know each other better.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Splendid![SOFTBLOCK] Shall we spend our evening here, then?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Work\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoWork\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedMaintenance == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Shall we spend our evening performing our functions?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Work\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoWork\") ")
		fnCutsceneBlocker()
	end

--Post-decision.
elseif(sObjectName == "Work") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")

	--Fade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Sophie Conversation/Routing.lua")
	fnCutsceneBlocker()
	
	--Position characters if Sophie is not interested in synchronizing.
	if(iSophieWillSynchronizeNow == 0.0 or iSophieFirstDateState == 3.0) then
		fnCutsceneTeleport("Christine", 23.25, 10.10)
		fnCutsceneTeleport("Sophie", 23.25, 12.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneFace("Sophie", 0, -1)
	end

	--Fade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--First date, changes dialogue and events.
	if(iSophieFirstDateState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Wow, the day really flew by![SOFTBLOCK] That was a lot of fun![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I really should go defragment my drives and let my autorepair kick in.[SOFTBLOCK] I'm beat.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ummm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hee hee![SOFTBLOCK] You haven't even seen your quarters yet, have you?[SOFTBLOCK] Do you know where they are?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Vaguely.[SOFTBLOCK] I didn't download the directory when I got here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Well then allow me to show you.[SOFTBLOCK] We passed the elevators on the way here, they're on the east side of the sector.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Thanks, Sophie.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] This does count as walking a lady back to her room.[SOFTBLOCK] We use the same elevators, of course.[SOFTBLOCK] All units in the sector use the same domicile block.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right.[SOFTBLOCK] Let's go.") ]])
		fnCutsceneBlocker()
	
	--Successive dates: Sophie immediately returns to the maintenance bay if she doesn't want to synchronize.
	elseif(iSophieWillSynchronizeNow == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I never thought I'd have such a good time working with you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What's that supposed to mean?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hee hee![SOFTBLOCK] I was really nervous when you were assigned as my Lord Golem.[SOFTBLOCK] I guess I was just being silly.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Everything is going to be all right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Yeah, it is, isn't it?[SOFTBLOCK] Maybe things are going to work out.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You go ahead and defragment, I'll clean up here.[SOFTBLOCK] Good night, Christine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good night, Sophie.") ]])
		fnCutsceneBlocker()
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
		
		--Remove Sophie from the party.
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "Sophie")
		
		--Set Sophie's properties.
		EM_PushEntity("Sophie")
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/RegulusCity/Sophie/Root.lua")
		DL_PopActiveObject()
	
	--If she wants to synchronize, keep the flag at 2.0.
	else
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
	
    end

elseif(sObjectName == "Birb") then
    local iCanSpawnBirdcage = VM_GetVar("Root/Variables/Chapter5/Scenes/iCanSpawnBirdcage", "N")
    if(iCanSpawnBirdcage == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Herbert's living quarters.[SOFTBLOCK] He's fumbling around, pecking at things, and testing his wing.[SOFTBLOCK] He seems happy, for a bird.)") ]])
        fnCutsceneBlocker()
    end

--[Common Clearing]
elseif(sObjectName == "NoWork") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
--[Exit Ladder]
elseif(sObjectName == "Ladder") then
    
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCityG", "FORCEPOS:17.0x16.0x0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end