--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Triggers a cutscene the first time Christine enters the maintenance bay.
if(sObjectName == "EnterBay") then

	--Variables.
	local iIsOnDate                 = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iTalkedToSophie           = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")
	local iSophieImportantMeeting   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N")
	local iSophieFirstDateState     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
    local iStartedShoppingSequence  = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
	
	--Important plot cutscene.
	if(iSophieImportantMeeting == 1.0) then
	
		LM_ExecuteScript(gsRoot .. "Chapter5Scenes/After 55 Encounter/Scene_RepairBay.lua")
    
    --Post shopping sequence.
    elseif(iStartedShoppingSequence == 3.0) then
		
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 4.0)
        VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 0.0)
        
        --Set this flag so Herbert and Linda will spawn next time you enter.
        local iMetFriendGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetFriendGolem", "N")
        if(iMetFriendGolem == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iCanSpawnBirdcage", "N", 1.0)
        end
        
        --Movement.
        fnCutsceneMove("Christine", 25.25, 13.50)
        fnCutsceneMove("Sophie", 25.25, 12.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneFace("Sophie", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ah, home sweet repair bay.[SOFTBLOCK] How I missed the scent of grease and burnt metal.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] So, with your permission, Lord Unit, I'll be getting to work on those new dresses as soon as the materials are delivered![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Dresses.[SOFTBLOCK] Plural.[SOFTBLOCK] You've decided to go?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I had a good idea.[SOFTBLOCK] You'll see.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'm not going to let some stuffy Lord Unit rattle me.[SOFTBLOCK] I'm going to march right into that gala like I own the place.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] That's the spirit![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] B-[SOFTBLOCK]b-[SOFTBLOCK]b-[SOFTBLOCK]but I'll need you to hold my hand...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] As if I needed a reason.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Oh, 55 sent me a message.[SOFTBLOCK] She's in the basement.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Best not keep her waiting.[SOFTBLOCK] She'll be short with me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Tell her I said hi![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hmmm, going to need to borrow a binding gun...[SOFTBLOCK] Maybe Unit 763328 will let me borrow the one from her fabrication bench...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Sophie", 22.25, 12.50)
        fnCutsceneFace("Sophie", 0, -1)
        fnCutsceneBlocker()
        
        --Activate Sophie's collision flag and whatnot.
        gsFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "Sophie")
        
        --Set Sophie's properties.
        EM_PushEntity("Sophie")
            TA_SetProperty("Clipping Flag", true)
            TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/RegulusCity/Sophie/Root.lua")
        DL_PopActiveObject()
    
    --Go talk to 55 first.
    elseif(iStartedShoppingSequence == 4.0) then
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I really shouldn't keep 55 waiting...)") ]])
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 26.25, 13.50)
	
	--Play the scene.
	elseif(iTalkedToSophie == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N", 1.0)
		
		--Run the rest of the scene.
		LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Meeting Sophie/Execution.lua")
	
	--On a date with Sophie that is over.
	elseif(iIsOnDate == 2.0 and iSophieWillSynchronizeNow == 0.0) then
	
		--If this is the first date, don't play this scene. The player needs to go to Christine's quarters instead.
		if(iSophieFirstDateState == 3.0) then
	
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
		
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] I had a lot of fun today, Christine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I have a few things to finish up in here before I head back to my quarters.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right.[SOFTBLOCK] Don't stay up too late![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Good night, 771852.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good night, 499323.") ]])
			fnCutsceneBlocker()
		
			--Move Sophie to the repair area.
			fnCutsceneMove("Sophie", 22.25, 12.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Sophie", 0, -1)
			fnCutsceneBlocker()
			
			--Remove Sophie from the party.
			gsFollowersTotal = 0
			gsaFollowerNames = {}
			giaFollowerIDs = {0}
			AL_SetProperty("Unfollow Actor Name", "Sophie")
			
			--Set Sophie's properties.
			EM_PushEntity("Sophie")
				TA_SetProperty("Clipping Flag", true)
				TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/RegulusCity/Sophie/Root.lua")
			DL_PopActiveObject()
		end
	end

--55 leaves the group if you warped in.
elseif(sObjectName == "55Leaves") then

	--Variables.
	local iIs55Following  = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    local iSaw55sMemories = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    local iSaw55PostLRT   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N")
    
    --If Christine is in Eldritch form, Sophie looks north if she's in the area.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Eldritch" and EM_Exists("Sophie") == true) then
        EM_PushEntity("Sophie")
            TA_SetProperty("Facing", gci_Face_North)
        DL_PopActiveObject()
    end
	
	--If 55 is following, she mentions she's leaving here.
	if(iIs55Following == 1.0) then
        
        --Post LRT scene:
        if(iSaw55sMemories == 1.0 and iSaw55PostLRT == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, please report to your normal work assignments.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I downloaded what I could from the core and set up a back door into the network.[SOFTBLOCK] I will review what footage I can.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Are you...[SOFTBLOCK] okay?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Report to your normal work assignments.[SOFTBLOCK] I will contact you when I have determined our next move.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55, come on...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do not waste processor cycles worrying.[SOFTBLOCK] Focus on maintaining your cover.[SOFTBLOCK] Move out.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...") ]])
            fnCutsceneBlocker()
            
            --55 moves to the ladder and vanishes.
            fnCutsceneMove("55", 11.25, 14.50)
            fnCutsceneBlocker()
            fnCutsceneFace("55", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            fnCutsceneTeleport("55", -100.25, -100.50)
            fnCutsceneBlocker()
            
            --Remove 55 from the following group.
            gsFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AC_SetProperty("Set Party", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "55")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
            
            --Flag for the next part of the cutscene.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N", 1.0)
            
            --Change map to Christine's quarters. This will fire the next part of the scenario.
            fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.0x10.0x0") ]])
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 0.0)
        
        --Normal:
        else
		
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] If you're going in to the city, it's best we're not seen together.[SOFTBLOCK] I'll be in touch.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            
            --55 moves to the ladder and vanishes.
            fnCutsceneMove("55", 11.25, 14.50)
            fnCutsceneBlocker()
            fnCutsceneFace("55", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            fnCutsceneTeleport("55", -100.25, -100.50)
            fnCutsceneBlocker()
            
            --Remove 55's sprite. She remains in the combat party for equipment reasons.
            gsFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AC_SetProperty("Set Party", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "55")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        end
	end

--Christine must maintain her cover as a lord golem, and/or 55 leaves the group.
elseif(sObjectName == "MaintainCover") then

	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Christine needs to change form.
	if(sChristineForm ~= "Golem") then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Better transform to a Lord Unit.[SOFTBLOCK] Wouldn't want to draw unnecessary attention.)") ]])
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
	end

--Cutscene, part of the mines finale.
elseif(sObjectName == "SXCutsceneTrigger") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	if(iSXUpgradeQuest ~= 3.0) then return end

	--Execute.
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/SprocketFinale/Scene_A_RegulusCityC.lua")

--Cutscene, plays after the Human Special Date.
elseif(sObjectName == "PostHumanScene") then

    --Flags.
    local iHumanThirdScene = VM_GetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N")
    local iHumanRepeat = VM_GetVar("Root/Variables/Chapter5/Sophie/iHumanRepeat", "N")
    if(iHumanThirdScene ~= 1.0) then return end
    
    --Set.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N", 2.0)
    
    --Christine switches to golem.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
    
    --Black the screen out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Reposition.
    fnCutsceneTeleport("Christine", 19.25, 15.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneTeleport("Sophie", 18.26, 16.50)
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()

    
    --[First Time]
    if(iHumanRepeat == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] All right, all the checksums are set.[SOFTBLOCK] You are back to normal.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Except for one thing, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] The new memory you requested?[SOFTBLOCK] I most surely uploaded it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Which one was it, out of curiosity?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] It's not obvious?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] The programming makes it feel natural, but you have to search the memory for logical contradictions.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] And I can't find a single contradiction in my love for you...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You have a way with words, Christine.[SOFTBLOCK] Were you a poet before you became a repair unit?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] An English teacher.[SOFTBLOCK][EMOTION|Christine|Laugh] Which is practically the same thing when you get right down to it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Now I'm a little more worried about how we're going to make this little romp go away.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's a common practice in the security services to recycle unit designations.[SOFTBLOCK] When a unit gets 'disappeared', their designation gets a flag on it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Our mutual friend has been using them to access things without rousing too much suspicion.[SOFTBLOCK] There are thousands.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] The unit you just converted?[SOFTBLOCK] My PDU has already specified she was assigned to the security services and sent to work on the South-Field Station.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] South-Field Station?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's the Regulus Secundus project being built on the other side of the moon.[SOFTBLOCK] They finished laying the track a month ago.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Units assigned to that project will be out of communication for at least two years, and record keeping is terrible.[SOFTBLOCK] Nobody will suspect a thing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Doing all this just to get off on it...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I know it's selfish of me...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] N-[SOFTBLOCK]no![SOFTBLOCK] I was talking about me![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Forging documents, lying to units, pretending to be someone I'm not -[SOFTBLOCK] I'm like a secret agent![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] I don't know if you're bad for me, or if I'm bad for you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] The first one, absolutely.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Well that is just more than enough excitement for one day, Christine.[SOFTBLOCK] So, I'll bid you good night.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Of course.[SOFTBLOCK] Until tomorrow, Sophie.") ]])
        fnCutsceneBlocker()
    
    --[Repeats]
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] And then we stood here, and talked about how we were going to get away with it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh yeah, this part I remember.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] My favourite part was when you dressed me...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Hee hee![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] We've been going over our memory files for hours now! That was a lot of fun![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Shall we consider this a night out, then?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] We shall! I'll just get the files from earlier today sorted out, and then it's off to defrag for me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] All right. Good night, Sophie.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Good night, Christine!") ]])
    
    end
	
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)

end
