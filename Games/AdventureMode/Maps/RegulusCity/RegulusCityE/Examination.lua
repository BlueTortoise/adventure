--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ToCityD") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityD", "FORCEPOS:12.0x4.0x0")
	
--Exit
elseif(sObjectName == "ToCityF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityF", "FORCEPOS:19.0x12.0x0")

--[Examinables]
elseif(sObjectName == "Explosion") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A videograph of things exploding.[SOFTBLOCK] There may or may not be characters involved, it's hard to say.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Nothing is playing at the moment.)") ]])
        fnCutsceneBlocker()
	end
    
elseif(sObjectName == "Arcee") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](An action videograph is playing.[SOFTBLOCK] Apparently this robot can turn into a motorcycle at will.[SOFTBLOCK] What a useful function!)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Nothing is playing at the moment.)") ]])
        fnCutsceneBlocker()
	end
	
elseif(sObjectName == "Miku") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A musical videograph is playing.[SOFTBLOCK] While she looks fairly human, this is actually a robot who sings and dances.[SOFTBLOCK] Is there nothing we don't do better than organics?)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Nothing is playing at the moment.)") ]])
        fnCutsceneBlocker()
	end

--[Watching Videographs]
--Movies! On a date!
elseif(sObjectName == "VideographL" or sObjectName == "VideographR") then

	--Variables.
	local iIsOnDate            = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedVideograph = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedVideograph", "N")
    local iIsGalaTime          = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
	
	--Convenience Flag.
	gsTemporaryStore = sObjectName

    --Gala time.
    if(iIsGalaTime > 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](We don't have time to watch a videograph at the moment.)") ]])
        fnCutsceneBlocker()

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0 or iStartedShoppingSequence == 1.0) then
		
		--Christine doesn't know what videographs are.
		if(iExplainedBlueSphere == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This computer is hooked up to the display here.[SOFTBLOCK] Must be used for display videographs.)") ]])
			fnCutsceneBlocker()
		
		--Christine knows what videographs are.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This computer is hooked up the display here.[SOFTBLOCK] Maybe I should watch a videograph with Sophie later?)") ]])
			fnCutsceneBlocker()
		end

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedVideograph == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedVideograph", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Looks like this viewing room is open.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Did you want to watch a videograph with me?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What sort of videograph?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] Heh.[SOFTBLOCK] Obviously, a documentary or instructional videograph.[SOFTBLOCK] Of course.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Now, if you know what commands to enter, there are other ones on the system.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmmm?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] If you take my meaning, 771852.[SOFTBLOCK] Not that I wouldn't classify them as 'instructional', of course.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, I get what you're saying.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Does the central administration support such videographs?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Central doesn't know about them.[SOFTBLOCK] And, I suspect any Lord Golem who finds out about them wouldn't want them removed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right.[SOFTBLOCK] Shall we spend an evening watching some videographs?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"VideoChoose\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedVideograph == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Shall we spend an evening watching some videographs?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"VideoChoose\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	end
	
--Spend a date watching a videograph.
elseif(sObjectName == "VideoChoose") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Which videograph should we watch?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'll let you pick.[SOFTBLOCK] There's a lot of good ones.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Here.[SOFTBLOCK] Now you can unlock some of the -[SOFTBLOCK] hidden videographs.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hee hee![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right...[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Roberta and Joliet\", " .. sDecisionScript .. ", \"VideoRoberta\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"A Talking Dark Matter Girl?!?\", " .. sDecisionScript .. ", \"VideoDMG\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Slime Girls Are Easy\", " .. sDecisionScript .. ", \"VideoSlime\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Dr. Strangelover\", " .. sDecisionScript .. ", \"VideoBomb\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Forget It\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--All videgraphs use this for routing.
elseif(sObjectName == "VideoRoberta" or sObjectName == "VideoDMG" or sObjectName == "VideoSlime" or sObjectName == "VideoBomb") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Left screen.
	if(gsTemporaryStore == "VideographL") then
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorA") ]])
		fnCutsceneMove("Christine", 4.25, 8.50)
		fnCutsceneMove("Sophie",    4.25, 8.50)
		fnCutsceneMove("Christine", 4.25, 5.50)
		fnCutsceneMove("Sophie",    4.25, 5.50)
		fnCutsceneMove("Christine", 3.75, 5.50)
		fnCutsceneMove("Sophie",    4.75, 5.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneFace("Sophie",    0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
	--Right screen.
	else
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorC") ]])
		fnCutsceneMove("Christine", 12.25, 8.50)
		fnCutsceneMove("Sophie",    12.25, 8.50)
		fnCutsceneMove("Christine", 12.25, 5.50)
		fnCutsceneMove("Sophie",    12.25, 5.50)
		fnCutsceneMove("Christine", 11.75, 5.50)
		fnCutsceneMove("Sophie",    12.75, 5.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneFace("Sophie",    0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
	end

	--Fade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Sophie Conversation/Video Routing.lua", sObjectName)
	fnCutsceneBlocker()

	--Fade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	
	--First date.
	if(iSophieFirstDateState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Ah, videographs.[SOFTBLOCK] A classic first date.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is that -[SOFTBLOCK] a good thing?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I love a good videograph![SOFTBLOCK] We should do this again![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Phew!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, what would you like to do now?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I hate to cut this short, but it's late and I had quite a morning.[SOFTBLOCK] I should really go to my quarters.[SOFTBLOCK] As should you![SOFTBLOCK] Don't stress your motivators![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ummm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hee hee![SOFTBLOCK] You haven't even seen your quarters yet, have you?[SOFTBLOCK] Do you know where they are?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Vaguely.[SOFTBLOCK] I didn't download the directory when I got here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Well then allow me to show you.[SOFTBLOCK] We need to take the elevators east of the airlock.[SOFTBLOCK] I'll point them out when we get close.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Thanks, Sophie.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] This does count as walking a lady back to her room.[SOFTBLOCK] We use the same elevators, of course.[SOFTBLOCK] All units in the sector use the same domicile block.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right.[SOFTBLOCK] Let's go.") ]])
		fnCutsceneBlocker()
	
	--After the cutscene is over, Sophie will request to go back to the maintenance bay.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Well, I suppose it's back to the maintenance bay for me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I had a lot of fun today, Christine![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No matter what, it's fun if you're there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Would you care to escort your tandem unit?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] With pleasure.") ]])
		fnCutsceneBlocker()
	end
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
	
	--Left screen, merge.
	if(gsTemporaryStore == "VideographL") then
		fnCutsceneMove("Christine", 4.25, 5.50)
		fnCutsceneMove("Sophie",    4.25, 5.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneFace("Sophie",    0, 1)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
	--Right screen, erge.
	else
		fnCutsceneMove("Christine", 12.25, 5.50)
		fnCutsceneMove("Sophie",    12.25, 5.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneFace("Sophie",    0, 1)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	end

--[Multi-Purpose Close]
elseif(sObjectName == "NoClose") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Clean up temporary flags.
	gsTemporaryStore = nil

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end