--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCityG"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        AL_SetProperty("Music", "RegulusCity")
    else
        AL_SetProperty("Music", "SophiesThemeSlow")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCityG")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	local iMet55InLowerRegulus     = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    
    --No special entities spawn during gala prep.
    if(iIsGalaTime > 0.0) then return end
    
    if(iMet55InLowerRegulus == 0.0) then
        fnSpecialCharacter("55", "Doll", 17, 13, gci_Face_North, false, gsRoot .. "CharacterDialogue/RegulusCity/55/Root.lua")
    elseif((iStartedShoppingSequence == 4.0 or iStartedShoppingSequence == 5.0) and iIs55Following == 0.0) then
        fnSpecialCharacter("55", "Doll", 17, 8, gci_Face_South, false, gsRoot .. "CharacterDialogue/RegulusCity/55/Root.lua")
    end
	
	--[Special Flags]
	--Entering this area when the flags are right switches this variable. It changes Sophie's dialogue.
	local iSophieImportantMeeting  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N")
	local iLeftRepairBay           = VM_GetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N")
	if(iSophieImportantMeeting == 2.0 and iMet55InLowerRegulus == 0.0 and iLeftRepairBay == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N", 1.0)
	end

end
