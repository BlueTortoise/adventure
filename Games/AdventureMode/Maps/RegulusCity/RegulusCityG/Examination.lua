--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exit Ladder]
if(sObjectName == "Ladder") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCityC", "FORCEPOS:11.0x14.0x0")

--Terminal with backers information.
elseif(sObjectName == "Terminal") then

	fnStandardMajorDialogue()
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ah, this terminal has a list of all the commendations that have been awarded for the past few months.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If it weren't for them, this city wouldn't be the way it is today.[SOFTBLOCK] We owe them quite a debt![BLOCK][CLEAR]") ]])
	if(fnIsCharacterPresent("55")) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you talking to yourself?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I was talking to you.[SOFTBLOCK] Weren't you listening?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Ah.[SOFTBLOCK] It seemed you were talking to someone who isn't here but is still somehow hearing us speak.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Don't be silly.[BLOCK][CLEAR]") ]])
	end
		
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Backers5", "Leave") ]])
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end