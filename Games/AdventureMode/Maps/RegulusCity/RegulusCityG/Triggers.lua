--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
--if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--55 Meeting Scene.
if(sObjectName == "Meet55") then

	--Variables.
	local iSawSpecialAnnouncement  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    local iSawBasementScene        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawBasementScene", "N")
	
	--If the value is 2.0, the scene is primed but hasn't played.
	if(iSawSpecialAnnouncement == 2.0) then
		
		--Spawn Sophie.
		TA_Create("Sophie")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/RegulusCity/Sophie/Root.lua")
		DL_PopActiveObject()
		
		--Variables.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N", 3.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 3.0)
		
		--Music fades out.
		AL_SetProperty("Music", "Null")
		
		--Move Christine.
		fnCutsceneMove("Christine", 14.25, 7.50)
		fnCutsceneBlocker()
	
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Christine looks around.
		fnCutsceneFace("Christine", -1, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's nobody here...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Maybe the work terminal had a glitch?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[VOICE|2855] It was no glitch.") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move 55 in.
		fnCutsceneMove("55", 17.25, 11.50)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--55 Walks up. Christine turns around.
		fnCutsceneMove("55", 14.25, 8.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneFace("55", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Command Unit 2855![SOFTBLOCK] I'm glad you're all right![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK][E|Sad] Do I know you?[SOFTBLOCK] Something's wrong with my memory drives...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Of course, your drives are out of phase.[SOFTBLOCK] Let me recalibrate them for you.") ]])
		fnCutsceneBlocker()
		
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Christine backs off.
		fnCutsceneMoveFace("Christine", 14.25, 6.50, 0, 1, 0.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Something's wrong...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] Stop mewling and come here.[SOFTBLOCK] We don't have time for this.") ]])
		fnCutsceneBlocker()
		
		--Advance.
		fnCutsceneMove("55", 14.25, 7.50)
		fnCutsceneBlocker()
		
		--Christine moves away again.
		fnCutsceneMoveFace("Christine", 15.25, 6.50, -1, 1, 2.50)
		fnCutsceneBlocker()
		fnCutsceneMove("55", 14.25, 6.50)
		fnCutsceneMoveFace("Christine", 16.25, 6.50, -1, 1, 2.50)
		fnCutsceneBlocker()
		fnCutsceneFace("55", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneMove("55", 15.25, 6.50)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Stay away from me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Cease your struggles, this will be over in just a moment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "*ZAP*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Aahhhhhh!!!") ]])
		fnCutsceneBlocker()
		
		--Christine spazzes out.
		fnCutsceneMoveFace("Christine", 16.25, 8.50,  1,  1, 2.00)
		fnCutsceneMoveFace("Christine", 16.25, 9.50,  1, -1, 2.00)
		fnCutsceneMoveFace("Christine", 16.25, 7.50, -1,  0, 2.00)
		fnCutsceneMoveFace("Christine", 16.25, 9.50,  1,  1, 2.00)
		fnCutsceneMoveFace("Christine", 16.25, 8.50,  0,  1, 2.00)
		fnCutsceneMoveFace("Christine", 15.25, 8.50,  1, -1, 2.00)
		fnCutsceneMoveFace("Christine", 17.25, 8.50,  1,  1, 2.00)
		fnCutsceneMoveFace("Christine", 15.25, 8.50,  0,  0, 2.00)
		fnCutsceneMoveFace("Christine", 17.25, 8.50,  1,  1, 2.00)
		fnCutsceneMoveFace("Christine", 17.25, 8.20,  0,  1, 2.00)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		fnCutsceneMove("55", 16.25, 8.50)
		fnCutsceneBlocker()
		fnCutsceneFace("55", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Perhaps the voltage was too high?[SOFTBLOCK] No matter, your chassis could absorb that impact.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Cry] Get...[SOFTBLOCK] away from...[SOFTBLOCK] me...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Stop complaining and let your drives format correctly.") ]])
		fnCutsceneBlocker()
		
		--Christine stands up.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Null")
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 17.25, 8.50, 0.20)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I remember...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Cryogenics...[SOFTBLOCK] the airlock...[SOFTBLOCK] the bodies...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You -[SOFTBLOCK] you stuck me with a golem core![SOFTBLOCK] You did this to me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] It was the only way to proceed.[SOFTBLOCK] I needed your authenticator chip to access the Regulus City airlock.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] Your permission was not necessary.[SOFTBLOCK] Besides, you can - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Thank you so much, 2855![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "*Hug*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Offended] ...[SOFTBLOCK] What are you doing?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] I have you to thank for this![SOFTBLOCK] I've never been so happy in my life![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Truly?[SOFTBLOCK] You prefer being a golem?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Were it not for you, I would never have come to Regulus City.[SOFTBLOCK] I would never have gone to get scanned...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] (Oh dear...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I didn't tell you what my life was like before we met, did I?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] I didn't ask, and I don't care.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You see, I've always felt -[SOFTBLOCK] I suppose the word would be uncomfortable.[SOFTBLOCK] I was never happy with who I was.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] We have business to attend to.[SOFTBLOCK] Can this wait?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But now...[SOFTBLOCK] I have a place I belong.[SOFTBLOCK] Oh![SOFTBLOCK][E|Happy] Sophie![SOFTBLOCK] I must introduce you to Unit 499323![SOFTBLOCK] She'll be so excited to meet you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You made all of this possible.[SOFTBLOCK] From the bottom of my power core, thank you, 2855...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Yes yes yes.[SOFTBLOCK] I've already fabricated a new authenticator chip.[SOFTBLOCK] You can transform back to a human now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] .............................[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Assessing likelihood of deliberately false statements.[SOFTBLOCK] Likelihood is low.[SOFTBLOCK] Very low.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I was intrigued by that runestone you were carrying.[SOFTBLOCK] I did some database searches on it.[SOFTBLOCK] Do you know what I found?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] No no no no...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] It's a Rilmani artifact, and if the surface scans are accurate, it's a seventh-dimensional object.[SOFTBLOCK] Witness reports from the surface also suggest it allows the user to change their form.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smug] But you already knew that, didn't you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I discovered numerous reports of others bearing runes similar to yours.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] These were highly classified reports, mind.[SOFTBLOCK] Not everyone knows this.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Now stop wasting time and transform.[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Go Along With Her\", " .. sDecisionScript .. ", \"OptionA\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Refuse\",  " .. sDecisionScript .. ", \"OptionB\") ")
		fnCutsceneBlocker()
    
    --Basement cutscene leading to the shopping sequence.
    elseif(iStartedShoppingSequence == 1.0 and iSawBasementScene == 0.0) then    
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBasementScene", "N", 1.0)
        
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Black out the screen.
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --Position characters.
		TA_Create("Sophie")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Wipe Special Frames")
		DL_PopActiveObject()
        fnSpecialCharacter("55", "Doll", 13, 6, gci_Face_North, false, nil)
        
        --Position Christine.
        fnCutsceneTeleport("Christine", 17.25, 16.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move Christine spawn Sophie behind her.
        fnCutsceneMove("Christine", 16.25, 16.50)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneTeleport("Sophie", 17.25, 16.50)
        fnCutsceneFace("Sophie", 0, 1)
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneFace("Sophie", -1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move.
        fnCutsceneMove("Christine", 13.25, 16.50)
        fnCutsceneMove("Christine", 13.25, 8.50)
        fnCutsceneMove("Sophie", 13.25, 16.50)
        fnCutsceneMove("Sophie", 13.25, 9.50)
        fnCutsceneMove("Sophie", 14.25, 9.50)
        fnCutsceneMove("Sophie", 14.25, 8.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("55", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And just how long did you intend to make me wait?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Oh hush, you can find ways to entertain yourself![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Speaking of, how have you been lately?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] My time has value which you cannot appreciate.[SOFTBLOCK] Do not waste it carelessly.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So as good as usual then.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, we have some good news for you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Christine is going to the Sunrise Gala![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Oh I bet it's because of all those efficiency gains we've been making -[SOFTBLOCK] most first-year Lord Units don't get invites, you know![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Hm.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Uh oh.[SOFTBLOCK] Hope I didn't tick her off.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55, come on.[SOFTBLOCK] We're just trying to cheer you up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] It is to be expected that you did not realize the momentous nature of what you just told me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Errr...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Are you going, too?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] No, wait.[SOFTBLOCK] That'd be dumb...[BLOCK][CLEAR]") ]])
        if(iSXUpgradeQuest >= 3.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do not denigrate yourself, Unit 499323.[SOFTBLOCK] Your skillset is admirable.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I did not have time to thank you for your commendable performance with SX-399.[SOFTBLOCK] I would like to do that now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You're welcome![SOFTBLOCK] Give her my best the next time you see her![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please know that any and all scorn is exclusively reserved for Unit 771852.[SOFTBLOCK] Any directed at you is accidental.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (The hay?)[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] There is not, in fact, anything dumb about it, other than Christine's as-per-usual failure to think strategically.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] The needless put-downs are a firm indicator of a positive mood.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Is that the dynamic?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I have been given cause to be in good spirits.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Christine, please use all of your processor cycles for the next question I posit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Why would I be happy about a very large collection of high-ranking enemy officials gathering in the same place?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Wait, what?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55, no![SOFTBLOCK] We want to attend the Sunrise Gala, not blow it up![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unfortunate.[SOFTBLOCK] If that is the case, then I would suggest concluding your night before I set the charges off.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] You know, she's right, Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] It's a big opportunity.[SOFTBLOCK] There will be Lord Units and Command Units there in big numbers.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] A-[SOFTBLOCK]and security![SOFTBLOCK] It'll be packed to the gills with security units![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Correct.[SOFTBLOCK] You possess analytical skill after all.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Fortunately, I have a unit with access codes right in front of me, and I know the time and location of the event.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I figured you'd find that out with all your hacking...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And thus we come to the oddity.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have not seen any traffic related to this event on the network.[SOFTBLOCK] None.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Maybe you're just slipping![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Incorrect.[SOFTBLOCK] Such an event and its organization would trigger a great deal of planning amongst security forces, which I observe carefully.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yet, I have seen nothing of unusual volume, not even encrypted communications.[SOFTBLOCK] This suggests non-networked communication protocols.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Then they know their network is compromised.[SOFTBLOCK] Consider what we've been up to...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have reached the same conclusion.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Wait...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] What if this is a trap for you, Christine?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] What if they know what you did in the LRT Facility?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] She is not to know about our operations, Unit 771852.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It kinda slipped out...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] But while her assessment is not impossible, it is also unlikely.[SOFTBLOCK] At no point in the history of the city has arresting a Unit been anything other than a routine matter.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Were you targeted for interrogation, there would be dozens of security units swarming this area.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Though the possibility of a trap is definite, I would attribute the communications protocols to caution.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This will be an operation to be undertaken with utmost secrecy and efficiency.[SOFTBLOCK] The risk is enormous.[SOFTBLOCK] At the same time, the benefits are obvious.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sophie...[SOFTBLOCK] This could be...[SOFTBLOCK] it...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] It?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The Administration's command structure would be dealt a massive blow by the loss of a large number of key personnel.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Further, the Arcane Academy is a historical bastion of strength.[SOFTBLOCK] To attack it directly would expose the weakness of the Administration.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Even if we didn't intend it to be, other Golems might take this as a sign.[SOFTBLOCK] They might rise up on their own.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] We have to be prepared for that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You don't look too happy about it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's going to involve a lot of retired units...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Even if I know they're part of the problem...[SOFTBLOCK] I know there will be some innocents among them...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Hrmpf.[SOFTBLOCK] I see you have sympathy for the enemy.[SOFTBLOCK] Where is the firebrand maverick ready to risk her life?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] One of us has to be our conscience.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Innocent units will be destroyed.[SOFTBLOCK] Historical artifacts reduced to rubble.[SOFTBLOCK] The Administration will scapegoat some poor Slave Unit who had nothing to do with it, and they'll suffer.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I'm prepared to go to war, don't you dare think otherwise.[SOFTBLOCK] But I know I'm going to be damned for it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] That is all I require from you at the moment.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] You have preparations to see to, do you not?[SOFTBLOCK] Unless you intend to attend the gala in [SOFTBLOCK]that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] W-[SOFTBLOCK]what's that supposed to mean?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] C-[SOFTBLOCK]Christine![SOFTBLOCK] I know exactly what you should wear![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Let me make it for you![SOFTBLOCK] Please?[SOFTBLOCK] Please![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Contact me when you are ready.[SOFTBLOCK] I will attempt to pull up schematics and prepare our plan.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Hey![SOFTBLOCK] My dress looks great, 55!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("55", 14.25, 6.50)
        fnCutsceneBlocker()
        fnCutsceneFace("55", 0, -1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneTeleport("55", -100.0, -100.0)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneFace("Sophie", -1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] That no good[SOFTBLOCK] driggle-draggle[SOFTBLOCK] crooked-nosed[SOFTBLOCK] fopdoodled[SOFTBLOCK] muckspouted[SOFTBLOCK] fusterlug![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You might be the world's easiest robot to tease.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] But forget her![SOFTBLOCK] Let's make you a pretty dress to wear![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] *Oh maybe something low cut...*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Low cut?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] S-[SOFTBLOCK]said it too loud![SOFTBLOCK] H-h-h-hee hee![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] What about you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You have your heart set on this, I see.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] My feelings aren't hurt.[SOFTBLOCK] Really.[SOFTBLOCK] This isn't my first time not going to the Sunrise Gala, you know.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Can you imagine a Slave Unit walking in that door like she didn't care what anyone thought?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And what if...[SOFTBLOCK] nobody knew you were a Slave Unit?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] ..![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Long dress reaching the ground, thick sleeves to cover the arms...[SOFTBLOCK] maybe a big hat?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Do you not see the killphrase bolted to my forehead?[SOFTBLOCK] Or the headphones?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh I bet you look wonderful without your headphones on...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Besides, can't we just remove those?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] B-[SOFTBLOCK]but..![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I think two prominent repair units can come up with a way to get those off, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] We'd be breaking every single rule...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, well,[SOFTBLOCK] I think we're well past that point.[SOFTBLOCK] Don't you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Y-y-y-yeah...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We can worry about that when the time comes, of course.[SOFTBLOCK] I'll run a few searches.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm sure some unit somewhere has needed maintenance on their killphrase's hysterisis-switch, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] W-[SOFTBLOCK]well, be that as it may, I still need some materials...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Then![SOFTBLOCK] Let's go shopping![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Oh my gosh, Sector 119 will be abuzz![SOFTBLOCK] All the Lord Units will be out buying dresses![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] And all the shops will have their best on display and everyone will be gossiping and oh my gooooodness![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] I'm sure I can give some reason why we need to...[SOFTBLOCK] requisition fabric...[SOFTBLOCK] right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] And we can get some ChemFuel and just chat about everything and see all the new videograph posters...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Do you have anything to do first, or should we get going right now?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Do I have any other tasks to take care of before going shopping with Sophie?)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's go shopping!\", " .. sDecisionScript .. ", \"ShoppingTime\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Give me a few minutes...\",  " .. sDecisionScript .. ", \"NotThisSecond\") ")
		fnCutsceneBlocker()
    
	end
	
elseif(sObjectName == "OptionA" or sObjectName == "OptionB") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		
	--Acceptace.
	if(sObjectName == "OptionA") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (It's not worth the effort to argue with her...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Wait...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (I've said that before.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (No.[SOFTBLOCK] No!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Who does she think she is?[SOFTBLOCK] She can't just barge in here and tell me what to do!)[BLOCK][CLEAR]") ]])
	end
		
	--Common.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Your obstinancy serves no purpose.[SOFTBLOCK] We must finish the work we started at the Cryogenics Facility.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Now, transform.[SOFTBLOCK] Central administration can track your authenticator chip as it stands, but I've disabled that feature on mine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] If you become a human again, we'll be able to move undetected.[SOFTBLOCK] Which is for the best.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] It can't be that hard, if the other rune bearers could do it, you can.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] No!!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] You just focus - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] I know how to do it, okay?[SOFTBLOCK] I've known since the day I was born.[SOFTBLOCK] It's always been there, but I didn't realize it until now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] I'm not going back.[SOFTBLOCK] Ever.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] This is not optional, Unit 771852.[SOFTBLOCK] You need to transform so we can proceed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Don't -[SOFTBLOCK] don't you dare touch me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Security Alert![SOFTBLOCK] Unit 771852 has encountered a maverick in the basement of the maintenance bay![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] You idiot, what are you doing?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You're a rogue unit![SOFTBLOCK] Leave me alone![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] Damn it...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Teleport Sophie in.
	fnCutsceneTeleport("Sophie", 17.25, 16.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("Sophie", 13.25, 16.50, 3.0)
	fnCutsceneMove("55", 14.25, 8.50)
	fnCutsceneBlocker()
	fnCutsceneFace("55", 0, 1)
	fnCutsceneFace("Sophie", 0, -1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--55 Runs away.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Upset") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] 771852?[SOFTBLOCK] What's going on?[SOFTBLOCK] I heard a distress call, are you all right?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] Not good...") ]])
	fnCutsceneBlocker()
	
	--Zoop!
	fnCutsceneMove("55", 14.25, 6.50, 2.50)
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	fnCutsceneTeleport("55", -100.0, -100.0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--55 Runs away.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Over here, Sophie.[SOFTBLOCK] Over here...") ]])
	fnCutsceneBlocker()
	
	--Move up.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("Christine", 16.25, 8.50, 0.30)
	fnCutsceneMove("Sophie", 13.25, 8.50, 3.0)
	fnCutsceneMove("Sophie", 15.25, 8.50, 3.0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Christine![SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] A rogue unit ambushed me.[SOFTBLOCK] I think she escaped into the vents.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Are you all right?[SOFTBLOCK] You don't look damaged...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'll do a full system scan![SOFTBLOCK] You'll be all right, I promise![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] No, I'm fine.[SOFTBLOCK] I'll be all right.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Are you sure?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yeah.[SOFTBLOCK] I just need to go rest my drives for a bit.[SOFTBLOCK] I took a bit of a shock.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Did you get a clear look at the rogue unit?[SOFTBLOCK] Or get her designation?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] No.[SOFTBLOCK] Sorry.[SOFTBLOCK] It was dark and I didn't raise my ocular unit sensitivity in time.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] It's all right.[SOFTBLOCK] You're safe now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'll walk you back to your quarters.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, yeah.[SOFTBLOCK] I'll just go recharge and be good as new tomorrow, okay?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] All right, all right.[SOFTBLOCK] Sorry for fussing.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks, Sophie.[SOFTBLOCK] Thanks...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Fade to black.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Level transition.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityZ", gsRoot .. "Chapter5Scenes/After 55 Encounter/Scene_Begin.lua") ]])
	fnCutsceneBlocker()

--Let's go SHOPPING!
elseif(sObjectName == "ShoppingTime") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] No time like the present![SOFTBLOCK] Come on![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Fantastic![SOFTBLOCK] Let's head to the tram and take it to Sector 119![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Lead the way, Lord Unit!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Move.
    fnCutsceneMove("Sophie", 13.25, 8.50)
	fnCutsceneBlocker()
    
    --Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
	EM_PushEntity("Sophie")
		local iSophieID = RE_GetID()
	DL_PopActiveObject()
    
    --Fold the party.
	gsFollowersTotal = 1
	gsaFollowerNames = {"Sophie"}
	giaFollowerIDs = {0}
	giaFollowerIDs = {iSophieID}
	AL_SetProperty("Follow Actor ID", iSophieID)

	--Fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
    
--Not right now, sorry.
elseif(sObjectName == "NotThisSecond") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, I want to very much, but I do have a few things I need to take care of.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can you wait a little bit?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Certainly![SOFTBLOCK] I'll try to get some work done in the meantime.[SOFTBLOCK] Just come upstairs when you're ready to go.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Move.
    fnCutsceneMove("Sophie", 14.25, 11.50)
    fnCutsceneMove("Sophie", 13.25, 11.50)
    fnCutsceneMove("Sophie", 13.25, 16.50)
    fnCutsceneMove("Sophie", 17.25, 16.50)
	fnCutsceneFace("Sophie", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneTeleport("Sophie", -100.0, -100.0)
	fnCutsceneBlocker()
    
--Post Latex-Drone TF scene.
elseif(sObjectName == "PostLatex") then

    --Repeat check.
    local iManuPostLatexScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuPostLatexScene", "N")
    if(iManuPostLatexScene == 0.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuPostLatexScene", "N", 0.0)
    
    --If 55 is present, move her away.
    if(EM_Exists("55") == true) then
        fnCutsceneTeleport("55", -1.25, -1.50)
    end
    
    --Make sure Christine is a drone.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua")
    
    --Black the screen out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn Sophie.
    TA_Create("Sophie")
        TA_SetProperty("Position", 22, 11)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
        TA_SetProperty("Facing", gci_Face_East)
    DL_PopActiveObject()
    
    --Position actors.
    fnCutsceneTeleport("Christine", 24.25, 10.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Sophie", 22.25, 11.50)
    fnCutsceneFace("Sophie", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
	fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 24.25, 11.50, 0.50)
    fnCutsceneMove("Christine", 23.25, 11.50, 0.50)
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] CONVERSION COMPLETED.[SOFTBLOCK] INPUT FUNCTION ASSIGNMENT.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] You turned out wonderfully![SOFTBLOCK] You turn a standard unit template into a work of art![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] THANK YOU, SUPERIOR UNIT.[SOFTBLOCK] INPUT FUNCTION ASSIGNMENT.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Hee hee![SOFTBLOCK] Function assignment?[SOFTBLOCK] Oooh, Sophie![SOFTBLOCK] Dirty thoughts, control yourself![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] INSTRUCTIONS UNCLEAR.[SOFTBLOCK] RE-INPUT FUNCTION ASSIGNMENT.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Christine![SOFTBLOCK] You might be carrying this a bit too far![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] CARRY OBJECT.[SOFTBLOCK] DESTINATION UNCLEAR, OBJECT UNCLEAR.[SOFTBLOCK] RE-INPUT FUNCTION ASSIGNMENT.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Wh-[SOFTBLOCK] Oh no![SOFTBLOCK] I left the inhibitor settings at normal![SOFTBLOCK] Turn around, drone unit!") ]])
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneFace("Christine", 0, 1)
	fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
	fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("Sophie", 22.75, 11.50, 0.10)
    fnCutsceneBlocker()
	fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sophie", 22.25, 11.50, 1, 0, 1.00)
    fnCutsceneBlocker()
	fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] That should do it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] SOPHIE![SOFTBLOCK] YOU FORGOT TO DISABLE THE INHIBITOR SETTINGS![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Hee hee![SOFTBLOCK] Maybe I forgot about them![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] (Maybe I didn't![SOFTBLOCK] Hee hee hee hee!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] IT'S ALL RIGHT.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] MAYBE YOU CAN TURN IT BACK ON WHEN I DON'T HAVE A SECRET MISSION![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I would love that![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] NOW, LET'S SEE HERE.[SOFTBLOCK] DO YOU KNOW HOW I RESET MY VOICE MODULATOR?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Software setting.[SOFTBLOCK] Check tools, voice, settings.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] IT'S NOT IN THERE.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] It isn't?[SOFTBLOCK] But the manual says...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] NO, WAIT.[SOFTBLOCK] MY DRONE SOFTWARE IS SLIGHTLY OUT OF DATE, BECAUSE THIS CONVERSION TUBE IS A FEW MONTHS OLD.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There we go![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Ah, good.[SOFTBLOCK] Be sure to update your software over the network, it'd be suspicious if you had outdated code.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Of course![SOFTBLOCK] Thanks for your help, Sophie![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] D-[SOFTBLOCK]do you need an inspection or anything before you go?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] My goodness, yes...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] N-[SOFTBLOCK]no![SOFTBLOCK] Important mission![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] I'll be upstairs at my workstation if you think your assets need to be inspected...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] M-[SOFTBLOCK]maybe it can wait...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hee hee![SOFTBLOCK] Come see me if you like![SOFTBLOCK] But you've got spy sneaking to do![SOFTBLOCK] Go to it!") ]])
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sophie", 21.25, 11.50)
    fnCutsceneMove("Sophie", 21.25, 16.50)
    fnCutsceneMove("Sophie", 17.25, 16.50)
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Sophie", -1.25, -1.50)
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Blush") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (What did I do to deserve a girl like her?)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Well, the next move is for 55 and I to return to Sector 99 and get me into the sector undercover.)") ]])
    fnCutsceneBlocker()

end
