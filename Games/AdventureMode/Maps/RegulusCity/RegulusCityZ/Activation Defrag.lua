--[Activation Defrag]
--Used to activate objects in Christine's living quarters. Same as a dialogue script. This is for the Defragmentation Chamber.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Store the location of this TilemapActor. It should be on the activity stack.
	local fTubeX, fTubeY = TA_GetProperty("Position")
	VM_SetVar("Root/Variables/Chapter5/Quarters/iTubeDefragX", "N", fTubeX)
	VM_SetVar("Root/Variables/Chapter5/Quarters/iTubeDefragY", "N", fTubeY)
	
	--Store Christine's position.
	EM_PushEntity("Christine")
		local fPlayerX, fPlayerY = TA_GetProperty("Position")
		VM_SetVar("Root/Variables/Chapter5/Quarters/iChristineDefragX", "N", fPlayerX)
		VM_SetVar("Root/Variables/Chapter5/Quarters/iChristineDefragY", "N", fPlayerY)
	DL_PopActiveObject()
	
	--Variables.
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")
	local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	
	--Normal case:
	if(iIsOnDate == 0.0 or iIsOnDate == 3.0) then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My defragmentation chamber.[SOFTBLOCK] Should I spend some time organizing my hard drive?)[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\", " .. sDecisionScript .. ", \"No\") ")
		fnCutsceneBlocker()
	
	--If Sophie is present:
	elseif(iIsOnDate == 1.0) then
	
		--Variables.
		local iSophieWillSynchronize = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronize", "N")
	
		--If Sophie has not synchronized with Christine before:
		if(iSophieWillSynchronize == 0.0) then
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Are you tired, 771852?[SOFTBLOCK] Does your drive need defragmenting?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Maybe a little...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] It's okay if you want to rest.[SOFTBLOCK] I don't mind.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you sure?[SOFTBLOCK] I know it wasn't much of a date...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You're so cute when you apologize to me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And you're so cute when you're being so understanding![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Should I cut the date short and defragment my drive?)[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesSophie\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\", " .. sDecisionScript .. ", \"No\") ")
			fnCutsceneBlocker()
	
		--If Sophie has syncronized before, the dialogue is different.
		else
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Your defragmentation pod...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Are you thinking - [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What I'm thinking?[SOFTBLOCK] Yes.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] We're in each other's heads...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Should we resynchronize our timers?[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			--fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's synchronize...\", " .. sDecisionScript .. ", \"SynchronizeNow\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I just want to defrag\", " .. sDecisionScript .. ", \"YesDefragSync\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Nevermind\", " .. sDecisionScript .. ", \"No\") ")
			fnCutsceneBlocker()
	
		end
	
	--Date value of 2.0, Date is over and Sophie wants to synchronize.
	elseif(iIsOnDate == 2.0 and iSophieWillSynchronizeNow == 1.0) then
	
		--Variables.
		VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronize", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N", 0.0)
	
		--Fade out.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		
		--Call the routing script.
		LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Sophie H Scenes/ZRouting.lua")
	
		--Remove Sophie.
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 0.0)
		fnCutsceneTeleport("Sophie", -100, -100)
		fnCutsceneTeleport("Christine", 14.25, 4.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneBlocker()
			
		--Remove Sophie from the party.
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "Sophie")
		
		--Fade in.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneWait(85)
		fnCutsceneBlocker()
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Unit 771852 resuming solitary cognitive functions.[SOFTBLOCK] Time to be productive again.)") ]])
		fnCutsceneBlocker()
		
	end
	
--Defrag the ol' hard drive.
elseif(sTopicName == "Yes") then

	--Clean.
	WD_SetProperty("Hide")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I suppose my drive could use some sorting.[SOFTBLOCK] Plus I could always use a recharge.)") ]])
	fnCutsceneBlocker()
	
	--Call the defrag script, which is this script.
	LM_ExecuteScript(LM_GetCallStack(0), "StandardDefrag")
	
--Defrag the ol' hard drive. Sophie says goodbye.
elseif(sTopicName == "YesSophie") then

	--Clean.
	WD_SetProperty("Hide")
	
	--Get the name of the current object.
	local sTubeName = "DefragPod"
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You get some rest, Christine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What about you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I was thinking I'd get some defragging, too.[SOFTBLOCK] I'll see you tomorrow.[SOFTBLOCK] Good night![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good night, Sophie!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Sophie takes her leave.
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneMove("Sophie", 14.25, 9.50)
	fnCutsceneMove("Sophie", 9.25, 10.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Sophie", 0, 1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneTeleport("Sophie", -100.25, -100.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	if(LM_GetRandomNumber(0, 99) < 10 or true) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (A true lady wouldn't have stared at her rear junction...[SOFTBLOCK] Oh well.)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	end
	
	--Remove Sophie from the party.
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AL_SetProperty("Unfollow Actor Name", "Sophie")
	
	--Call the defrag script, which is this script.
	LM_ExecuteScript(LM_GetCallStack(0), "StandardDefrag")

--Defrag instead of synchronizing.
elseif(sTopicName == "YesDefragSync") then

	--Clean.
	WD_SetProperty("Hide")
	
	--Get the name of the current object.
	local sTubeName = "DefragPod"
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'm sorry Sophie, I'm actually really tired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] H-[SOFTBLOCK]hey![SOFTBLOCK] I was going to say that![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hee hee![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's no problem, you get some rest.[SOFTBLOCK] I'll be waiting for you tomorrow.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Now you're doing it to me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hee hee![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] Is that really how my laugh sounds?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I can't capture it perfectly.[SOFTBLOCK] Nobody can match you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] My sweet tandem unit...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Okay.[SOFTBLOCK] Good night.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good night.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Sophie takes her leave.
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneMove("Sophie", 14.25, 9.50)
	fnCutsceneMove("Sophie", 9.25, 10.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Sophie", 0, 1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneTeleport("Sophie", -100.25, -100.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	if(LM_GetRandomNumber(0, 99) < 10) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (A true lady wouldn't have stared at her rear junction...[SOFTBLOCK] Oh well.)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	end
	
	--Remove Sophie from the party.
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AL_SetProperty("Unfollow Actor Name", "Sophie")
	
	--Call the defrag script, which is this script.
	LM_ExecuteScript(LM_GetCallStack(0), "StandardDefrag")

--Standard defragmentation sequence.
elseif(sTopicName == "StandardDefrag") then
	
	--Variable reset.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 0.0)
	VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWentToQuarters", "N", 0.0)
	
	--Get the name of the current object.
	local sTubeName = "DefragPod"
	
	--Position.
	local iTubeDefragX = 14.0
	local iTubeDefragY = 4.0
	fnCutsceneMoveFace("Christine", 14.30, 4.5, 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneFace(sTubeName, 0, -1)
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Move Christine onto the tube. This is done by teleporting her offscreen, and changing the tube's display properties.
	fnCutsceneTeleport("Christine", -100, -100)
	fnCutsceneFace(sTubeName, 1, 0)
	fnCutsceneBlocker()
	
	--If Sophie exists, move her offscreen.
	if(EM_Exists("Sophie")) then
		fnCutsceneTeleport("Sophie", -100, -100)
	end

	--Wait a bit.
	fnCutsceneWait(75)
	fnCutsceneBlocker()
	
	--Christine closes her eyes.
	fnCutsceneFace(sTubeName, -1, 0)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Unit 771852 logging off for the night...)") ]])
	fnCutsceneBlocker()
	
	--Close the pod.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
	fnCutsceneFace(sTubeName, 0, 1)
	fnCutsceneWait(75)
	fnCutsceneBlocker()
	
	--Fade to black.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Wait a bit.
    fnCutsceneInstruction([[ AM_SetProperty("Execute Rest") ]])
	fnCutsceneWait(300)
	fnCutsceneBlocker()
	
	--Fade back in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--Open the pod.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "RegulusCity") ]])
	fnCutsceneWait(125)
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneFace(sTubeName, -1, 0)
	fnCutsceneBlocker()
	
	--Open Christine's eyes.
	fnCutsceneWait(45)
	fnCutsceneFace(sTubeName, 1, 0)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Unit 771852 resuming cognitive functions.)") ]])
	fnCutsceneBlocker()
	
	--Move Christine to 1 spot south of the pod.
	fnCutsceneFace(sTubeName, 0, -1)
	fnCutsceneTeleport("Christine", 14.30, 4.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Let's have a very productive day!)") ]])
	fnCutsceneBlocker()
	
	--Variables.
	local iSawSpecialAnnouncement = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N")
	if(iSawSpecialAnnouncement == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: (Hmm?[SOFTBLOCK] I seem to have downloaded a notice while I was defragmenting.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: ('A special work order has been placed on your account.[SOFTBLOCK] Please consult a work terminal at your earliest convenience.')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: (I guess I should go see what that's about.)") ]])
		fnCutsceneBlocker()
	
	--At 1.0, replay it with a slightly different dialogue.
	elseif(iSawSpecialAnnouncement == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: (Looks like administration sent me a reminder notice while I was defragmenting.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: ('A reminder:: There is currently a special work order on your account.[SOFTBLOCK] Please consult a work terminal at your earliest convenience.')") ]])
		fnCutsceneBlocker()
	end
	
	--Close the pod.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneFace(sTubeName, 0, 1)
	fnCutsceneBlocker()

--Close the dialogue.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")

end