--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCityZ"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	if(gbDontCancelMusic == false) then
        local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
        if(iIsGalaTime == 0.0) then
            AL_SetProperty("Music", "RegulusCity")
        elseif(iIsGalaTime == 1.0) then
            AL_SetProperty("Music", "Null")
        else
            AL_SetProperty("Music", "SophiesThemeSlow")
        end
	end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCityZ")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--Christine's defragmentation pod is an NPC.
	TA_Create("DefragPod")
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Activation Script", "Null")
		fnSetApartmentObjectGraphics(gci_Quarters_DefragPod)
		
		--Special activation script and directions.
		TA_SetProperty("Activation Script", gsRoot .. "Maps/RegulusCity/RegulusCityZ/Activation Defrag.lua")
		for q = 1, 4, 1 do
			TA_SetProperty("Move Frame", 0, q-1, "Root/Images/Sprites/Quarters/Tube1")
			TA_SetProperty("Move Frame", 2, q-1, "Root/Images/Sprites/Quarters/Tube2")
			TA_SetProperty("Move Frame", 4, q-1, "Root/Images/Sprites/Quarters/Tube0")
			TA_SetProperty("Move Frame", 6, q-1, "Root/Images/Sprites/Quarters/Tube3")
		end
	DL_PopActiveObject()
	
	--Move the defragmentation pod. It needs to be offset with decimals.
	fnCutsceneTeleport("DefragPod", 14.30, 3.00 + 0.50)
			
	--Set extra options. Rendering depth must be set AFTER the object is moved.
	EM_PushEntity("DefragPod")
		TA_SetProperty("Rendering Depth", -0.500000 + (4.00 * 0.000100))
	DL_PopActiveObject()

end
