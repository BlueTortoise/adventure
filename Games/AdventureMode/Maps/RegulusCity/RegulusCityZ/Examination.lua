--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "Elevator") then
	
	--If Sophie wants to synchronize, you better believe Christine is going for it!
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")
	if(iSophieWillSynchronizeNow == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A hot girl wants to synchronize with me.[SOFTBLOCK] No way am I leaving!)") ]])
		fnCutsceneBlocker()
	
	--Leave.
	else
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:6.5x14.0x0") ]])
	end

--The coffee maker, except it makes oil.
elseif(sObjectName == "Oilmaker") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](An oilmaker.[SOFTBLOCK] Nothing gets your circuits buzzing like a shot of ultra-high energy-density oil directly into your power core.)") ]])
	fnCutsceneBlocker()

--Leftmost computer terminal.
elseif(sObjectName == "LeftTerminal") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: (This terminal controls some settings in my quarters, like lighting and temperature.[SOFTBLOCK] I also seems to have some useful files about Regulus City.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: (I've already been programmed with this information, but it never hurts to review.)[BLOCK]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal", "Leave") ]])
	
	--Topics handler.

--The couch.
elseif(sObjectName == "Couch") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A very finely made chesterfield, or couch in some places.[SOFTBLOCK] It appears to be made of an artificial polymer that emulates leather very closely.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](If I want to watch TV on it, I should go check the TV first.)") ]])
	fnCutsceneBlocker()

--Window showing Regulus City.
elseif(sObjectName == "Window") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I can see the whole of Regulus City through this window.[SOFTBLOCK] The city pulses with energy, shining defiantly against the darkness of space.)") ]])
	fnCutsceneBlocker()

--Rightmost computer terminal.
elseif(sObjectName == "RightTerminal") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My personal workstation.[SOFTBLOCK] It's also hooked into my defragmentation pod, and stores my daily maintenance logs.)") ]])
	fnCutsceneBlocker()

--Nightstand next to the defrag pod.
elseif(sObjectName == "Nightstand") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A nightstand to keep things on when I'm defragmenting my drives.)") ]])
	fnCutsceneBlocker()

--A simple table. Dates can happen here.
elseif(sObjectName == "Table") then

	--Variables.
	local iIsOnDate       = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedTable = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedTable", "N")

	--Not on a date. Examine the stuff.
	if(iIsOnDate == 0.0 or iIsOnDate >= 3.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A simple table. I bet Sophie would love to have a nice chat here.[SOFTBLOCK] The chairs look very comfortable.)") ]])
		fnCutsceneBlocker()

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedTable == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedTable", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Do you want to perhaps just have a nice chat here?[SOFTBLOCK] Admire the view?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, that'd be lovely![SOFTBLOCK] You're such a romantic![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I am?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Turn the lighting down a bit, maybe put on some music...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Get to know each other better...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Shall we spend the evening chatting at the table?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"ChatAtTable\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedTable == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Shall we spend the evening chatting at the table?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"ChatAtTable\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	end
	
--Television. Can trigger dates.
elseif(sObjectName == "Television") then

	--Variables.
	local iIsOnDate            = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedTelevision = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedTelevision", "N")

	--Not on a date. Examine the stuff.
	if(iIsOnDate == 0.0 or iIsOnDate >= 3.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A television, technically called a Remote Videograph Display.[SOFTBLOCK] I could watch videographs with Sophie if she were here.)") ]])
		fnCutsceneBlocker()

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedTelevision == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedTable", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Wow, you have a really big RVD...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You don't have one?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'd never be able to fit one in my quarters.[SOFTBLOCK] Plus I can always use the public theaters.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But then you wouldn't get to curl up on the chesterfield with me while we watch something...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, we'd be watching something?[SOFTBLOCK] I don't know if I'd be able to pay attention to anything but you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, shall we curl up and watch something on the telly?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchTV\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedTelevision == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Shall we curl up and watch something on the telly?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchTV\") ")
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	end

--Spend a date just talking in a romantic setting. Note that this is not a valid candidate for Sophie's first date.
elseif(sObjectName == "ChatAtTable") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Fade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Sophie Conversation/Routing.lua")
	fnCutsceneBlocker()
	
	--Variables.
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")
	
	--Reposition the party.
	if(iSophieWillSynchronizeNow == 0.0) then
		
		--Reposition.
		fnCutsceneTeleport("Christine", 8.25, 9.50)
		fnCutsceneTeleport("Sophie", 9.25, 9.50)
		fnCutsceneBlocker()
	
		--Facings.
		fnCutsceneFace("Christine", 1, 0)
		fnCutsceneFace("Sophie", -1, 0)
		fnCutsceneBlocker()

		--Fade.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneWait(85)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Thank you for the lovely evening, Christine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] It's always just so much fun talking to you.[SOFTBLOCK] You've got a gift.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, thank you, but it takes two to tango.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Tango?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's an expression.[SOFTBLOCK] I can't have a conversation by myself, can I?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Hee hee![SOFTBLOCK] You're right, but that never seems to stop me from talking to myself.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Now I'll excuse myself.[SOFTBLOCK] Good night, Unit 771852.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good night.") ]])
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Sophie", 9.25, 10.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneTeleport("Sophie", -100.25, -100.50)
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneBlocker()
		
		--End scene. Remove Sophie from the party.
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "Sophie")
	
		--Variables.
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
		VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWentToQuarters", "N", 1.0)
	
	--During Synchronization, just reposition the party. Also fold them.
	else
		
		--Reposition.
		fnCutsceneTeleport("Christine", 6.25, 5.50)
		fnCutsceneTeleport("Sophie", 6.25, 5.50)
		fnCutsceneBlocker()

		--Fade.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneWait(85)
		fnCutsceneBlocker()
		
		--Fold.
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
		--This variable is set to 2.0 during synchronization.
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
	
	end
	
--Spend a date watching TV. Chooses which one to watch.
elseif(sObjectName == "WatchTV") then

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What videograph do you want to watch?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Up to you.[SOFTBLOCK] I like all genres and stories.[SOFTBLOCK] Just pick whatever takes your interest.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Let's see here...[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Roberta and Joliet\", " .. sDecisionScript .. ", \"VideoRoberta\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"A Talking Dark Matter Girl?!?\", " .. sDecisionScript .. ", \"VideoDMG\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Slime Girls Are Easy\", " .. sDecisionScript .. ", \"VideoSlime\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Dr. Strangelover\", " .. sDecisionScript .. ", \"VideoBomb\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Forget It\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--All videographs route the same way.
elseif(sObjectName == "VideoRoberta" or sObjectName == "VideoDMG" or sObjectName == "VideoSlime" or sObjectName == "VideoBomb") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Fade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Sophie Conversation/Video Routing.lua", sObjectName)
	fnCutsceneBlocker()
	
	--Reposition.
	fnCutsceneTeleport("Christine", 8.25, 9.50)
	fnCutsceneTeleport("Sophie", 9.25, 9.50)
	fnCutsceneBlocker()

	--Facings.
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneFace("Sophie", -1, 0)
	fnCutsceneBlocker()

	--Fade.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Mmmm, that was nice.[SOFTBLOCK] Your couch is soft and your chassis is warm...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're a good cuddler.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] We should do this more often.[SOFTBLOCK] It's nice to just watch something and forget about all my problems.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Now I'll be heading back to my quarters.[SOFTBLOCK] Good night, Unit 771852.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good night.") ]])
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Sophie", 9.25, 10.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneTeleport("Sophie", -100.25, -100.50)
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneBlocker()
	
	--End scene. Remove Sophie from the party.
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AL_SetProperty("Unfollow Actor Name", "Sophie")
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
	VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWentToQuarters", "N", 1.0)
	
--[Multi-purpose Close]
elseif(sObjectName == "NoClose") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end