--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Plays a message when the player first enters Regulus City.
if(sObjectName == "Entrance") then

	--Variables.
	local iSophieFirstDateState    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
    local iSaw55PostLRT            = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    local iIsGalaTime              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
	
	--Play the scene.
	if(iSophieFirstDateState < 3.0) then
		
		--Reset this flag.
		gbDontCancelMusic = false
		
		--Spawn Sophie.
		TA_Create("Sophie")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Wipe Special Frames")
			for i = 0, 7, 1 do
				TA_SetProperty("Add Special Frame", "Kiss" .. i, "Root/Images/Sprites/Special/ChristineSophie|Kiss" .. i)
			end
		DL_PopActiveObject()
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 3.0)
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 0.0)
		VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWentToQuarters", "N", 0.0)
		
		--Fade to black immediately.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneTeleport("Sophie", 9.25, 10.50)
		fnCutsceneTeleport("Christine", 9.25, 10.50)
		fnCutsceneBlocker()
		
		--Stop the music, fade in.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneWait(45)
		fnCutsceneBlocker()

		--Movement.
		fnCutsceneMove("Sophie", 9.25, 8.50)
		fnCutsceneMove("Christine", 9.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Sophie", 9.25, 8.50, 0.50)
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", -1, -1)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", 0, -1)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", 1, -1)
		fnCutsceneWait(105)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", 0, 1)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Wooooooooow.[SOFTBLOCK] Lord Units have very spacious quarters...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, I haven't shown you my quarters yet, have I?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I've been meaning to change the colors around...[SOFTBLOCK][E|Laugh] maybe I could do with something violet?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] In case my favourite color wasn't obvious!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneFace("Sophie", 0, 1)
		fnCutsceneMove("Christine", 8.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", -1, 1)
		fnCutsceneMove("Christine", 4.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Ohmygosh![SOFTBLOCK] We'll get some potted plants to match your green hair, and they can go right here next to the oil machine![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Don't you think that'd tie the room together?[SOFTBLOCK] It'd be lovely![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Look at you, the interior decorator![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I've a very refined taste, my dear.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 6.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 6.25, 8.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", -1, 0)
		fnCutsceneMove("Christine", 6.25, 4.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", -1, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie...[SOFTBLOCK] isn't the city beautiful?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Th-[SOFTBLOCK] the city?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Sophie", 7.25, 6.50)
		fnCutsceneMove("Sophie", 7.25, 4.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You have a window in your quarters...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is this non-standard?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] My quarters is a 1x2 block.[SOFTBLOCK] I have a filing cabinet and my defragmentation pod.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Of course I don't really [SOFTBLOCK]*need*[SOFTBLOCK] any more space than that...[SOFTBLOCK] I spend most of my time in the maintenance bay...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] But I just haven't seen the outside in a long time.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How long?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] A few years.[SOFTBLOCK] I used to work on the survey team.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Once I got reassigned to Maintenance and Repair...[SOFTBLOCK] I just never had a reason to go outside.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I mean it's dangerous out there for a unit without a security escort...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But you'd never forget the stars.[SOFTBLOCK] They're magnificent...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] The night sky is breathtaking...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] ...[SOFTBLOCK] is what I'd say if we needed to breathe.[SOFTBLOCK] Hee hee!") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(185)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Well, I suppose I need to be getting back to my quarters...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Cancel the music.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "NullSlow") ]])
		
		--Movement.
		fnCutsceneMove("Sophie", 7.25, 7.50, 0.70)
		fnCutsceneMove("Sophie", 9.25, 7.50, 0.70)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Wait!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Facing.
		fnCutsceneFace("Christine", 1, 1)
		fnCutsceneFace("Sophie", -1, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh -[SOFTBLOCK] Are you sure you don't want to stay with me a little longer?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oh, hun, I'm easy, but I'm not [SOFTBLOCK]*that*[SOFTBLOCK] easy![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] That is only twenty-five percent of what I meant.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee hee![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I just -[SOFTBLOCK] I'm not very good at this, but I don't want you to go.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] S-[SOFTBLOCK]Sophie...[SOFTBLOCK] I - [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Is something wrong?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] No![SOFTBLOCK] A-all parameters are within expected ranges![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Then what is it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm not sure how to put this, but...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I -[SOFTBLOCK] I uh -[SOFTBLOCK] I'm -[SOFTBLOCK] trying...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oh my goodness.[SOFTBLOCK] Christine.[SOFTBLOCK] Are you no good with girls?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] !!![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] Oh, I see it written all over your face![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Unit 771852, am I your first girlfriend?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] N -[SOFTBLOCK] n -[SOFTBLOCK] !![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Well now I'm just so flattered![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You're so pretty, and smart, and strong, and you have the cutest little accent...[SOFTBLOCK] I don't know...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] If you hadn't asked me out, I might have asked you...[SOFTBLOCK] maybe...[SOFTBLOCK] but I don't think I'd have the courage to do it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Even if you're a Lord Unit...[SOFTBLOCK] You only live once, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Oh thank goodness![SOFTBLOCK] I was so scared![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] When I first talked to you, I was smitten![SOFTBLOCK] But I thought you wouldn't like me as I was.[SOFTBLOCK] Nobody ever does.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But then, your smile made my fear melt away.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I was thinking the same thing the whole time, but I guess I didn't know how to tell you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I've had so much fun the past few weeks.[SOFTBLOCK] Every day just flew by...[SOFTBLOCK] The time before I met you,[SOFTBLOCK] doesn't even seem real...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I really like you, Christine.[SOFTBLOCK] I really do.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I -[SOFTBLOCK] I -[SOFTBLOCK] I don't know what to say![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] You don't need to say anything.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] You're right...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Start the music. Kiss happens 20.274 seconds after this.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "SophiesThemeSlow") ]])
		
		--Christine walks to Sophie.
		fnCutsceneMove("Christine", 6.25, 7.50, 0.50)
		fnCutsceneMove("Christine", 8.25, 7.50, 0.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Kiss. Teleport Christine offscreen and use Sophie's special frames.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Focus Position", (8.25 * gciSizePerTile), (7.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneSetFrame("Sophie", "Kiss0")
		fnCutsceneTeleport("Sophie", 8.25, 7.50)
		fnCutsceneTeleport("Christine", -100.25, -100.50)
		fnCutsceneWait(250)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss1")
		fnCutsceneWait(250)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss2")
		fnCutsceneWait(250)
		fnCutsceneBlocker()
		
		--Wait. Total time elapsed is 90 ticks. Total is 1216. We need to wait while the music builds.
		-- There is a fude rate for the walking, which is not strictly timed.
		fnCutsceneSetFrame("Sophie", "Kiss3")
		fnCutsceneWait(1216 - 750 - 175)
		fnCutsceneBlocker()
		
		--Start the kiss right as the music hits the start of its roll. Kiss lasts 10 seconds total.
		fnCutsceneSetFrame("Sophie", "Kiss4")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss5")
		fnCutsceneWait(125)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss6")
		fnCutsceneWait(600 - 140)
		fnCutsceneBlocker()
		
		--Switch to Kiss7 for the conversation.
		fnCutsceneSetFrame("Sophie", "Kiss7")
		fnCutsceneWait(200)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss6")
		fnCutsceneWait(200)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Christine...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I love you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I love you too...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		
		--Back to Kiss6, then 7.
		fnCutsceneSetFrame("Sophie", "Kiss6")
		fnCutsceneWait(400)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But I still don't know what to say...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] [SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] 'See you tomorrow'?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] And then the day after that, and the day after that, and the one after that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] We'll spend every day together.[SOFTBLOCK] Always.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Yes...[SOFTBLOCK] Every day like this...[SOFTBLOCK] together...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] We'll be together for the rest of our synthetic lives...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I'll look forward to tomorrow.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I can't wait for it to come, so I can spend it with you...") ]])
		fnCutsceneBlocker()
		
		--Fade to black.
		fnCutsceneWait(180)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneWait(360)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This is what it feels like to have something to look forward to...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(180)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Music", "NullSlow") ]])
		fnCutsceneWait(300)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AM_SetProperty("Execute Rest") ]])
		
		--Teleport Sophie offscreen.
		fnCutsceneTeleport("Sophie", -100.25, -100.50)
		fnCutsceneTeleport("Christine", 14.30, 4.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Stop the music. Fade in.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Show Christine the special message.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: (Unit 771852 resuming cognitive functions.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: (Hmm?[SOFTBLOCK] I seem to have downloaded a notice while I was defragmenting.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: ('A special work order has been placed on your account.[SOFTBLOCK] Please consult a work terminal at your earliest convenience.')[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: (I guess I should go see what that's about.)") ]])
		fnCutsceneBlocker()
    
    --Second quarters scene with Christine and Sophie.
    elseif(iSaw55PostLRT == 1.0 and iStartedShoppingSequence == 0.0) then
		
		--Spawn Sophie.
		TA_Create("Sophie")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Wipe Special Frames")
		DL_PopActiveObject()
		
		--Messenger Golem.
		TA_Create("Golem")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/GolemSlaveP/", true)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Wipe Special Frames")
		DL_PopActiveObject()
        
        --Christine needs to be a golem.
        LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
		
		--Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBasementScene", "N", 0.0)
        
        --Variables.
        local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
		
		--Fade to black immediately.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneTeleport("Christine", 6.25, 4.50)
        fnCutsceneFace("Christine", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(165)
		fnCutsceneBlocker()
        
        --Narration:
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: Three weeks later...") ]])
        fnCutsceneBlocker()
		
		--Stop the music, fade in.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneWait(165)
		fnCutsceneBlocker()
        
        --Sophie enters.
		fnCutsceneTeleport("Sophie", 8.75, 10.50)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
        
        --Sophie moves north a bit.
        fnCutsceneMove("Sophie", 7.25, 9.50)
        fnCutsceneFace("Sophie", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] U-[SOFTBLOCK]Unit 771852?") ]])
        fnCutsceneBlocker()
        
        --Christine turns around.
        fnCutsceneFace("Christine", 0, 1)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hello, Sophie.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] L-[SOFTBLOCK]Lord Unit, d-[SOFTBLOCK]did I do something?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] If you're mad at me and don't want to be my tandem unit anymore, it's okay.[SOFTBLOCK] Really.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] But I want to say I'm sorry so you don't hold it against me...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] What?[SOFTBLOCK] No![SOFTBLOCK] Sophie![SOFTBLOCK] I love you, what are you talking about?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] But you've been so distant at work.[SOFTBLOCK] You barely speak to me, and we haven't gone on a date since...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The LRT Facility...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Is that where you went three weeks ago?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Sorry, you weren't supposed to know.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Though I guess there's no harm in it, now.[SOFTBLOCK] It seems nobody knows.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The administration must be keeping a lid on it, because 55 would have notified me if they were publically looking for us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] So it's -[SOFTBLOCK][E|Happy] not something I did?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] No dearest, it wasn't.[SOFTBLOCK] This is my fault.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I'll love you until my core fuses its last.[SOFTBLOCK] Nothing could change that.[BLOCK][CLEAR]") ]])
        if(iCompletedSerenity == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I'll love you until my core fuses its last.[SOFTBLOCK] Nothing could change that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Dearest...[SOFTBLOCK] I like the sound of that.[SOFTBLOCK] I think I'll be borrowing it, dearest.") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I'll love you until my core fuses its last.[SOFTBLOCK] Nothing could change that.") ]])
        end
		fnCutsceneBlocker()
        
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/WindowReflection/Alone") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] It's my fault that you feel this way, and I'll take responsibility.[SOFTBLOCK] I've been preoccupied at work.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I -[SOFTBLOCK] I'm still not sure what to do.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Do you want to talk about it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] I know there are some things you can't tell me, to protect me, but...[SOFTBLOCK] Well, try to avoid those things.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Please?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] You're suffering, and it pains me not to be able to help you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] I wish I could be out there, on the front...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
            
        --Christine turns around.
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneWait(85)
        fnCutsceneBlocker()
        
        --Sophie walks up to the window.
		fnCutsceneMove("Sophie", 7.25, 4.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Show the dawn shot.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/WindowReflection/Together") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] The sun will show itself soon.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] The city is beautiful at night, but during the day, will it be even more breathtaking?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] It's been so long since I saw the sun.[SOFTBLOCK] I'm sure the city will glitter as the rays strike it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Half a year of dark, then half a year of light.[SOFTBLOCK] That happened in some places on Earth, too.[SOFTBLOCK] Far to the north and south, on the poles.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] The people there had to adapt each time it changed.[SOFTBLOCK] Living in total darkness, I'm sure they looked forward to the day.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] But after the day would come the night again.[SOFTBLOCK] It was inevitable.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Will our day too be followed by the night?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Sophie, I know it looks bad now, but I will liberate this city.[SOFTBLOCK] I will make Regulus City a paradise for all, I swear it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] I believe you, and I'll be right there with you when you do.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] But the night isn't over yet.[SOFTBLOCK] A lot of golems won't see the dawn...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] 55...[SOFTBLOCK] We went to the LRT facility because they keep long-term storage logs of all signals on Regulus there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] So 55 thought they might have logs of who she was, since she had otherwise been scrubbed from the network.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] And...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] And...[SOFTBLOCK] we found those logs...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] She -[SOFTBLOCK] was not the person who we had thought she was.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I watched as she...[SOFTBLOCK] retired them.[SOFTBLOCK] All of them.[SOFTBLOCK] Every single golem I found in the Cryogenics facility...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] And the worst part is that I wasn't surprised.[SOFTBLOCK] Not really.[SOFTBLOCK] Not deep down.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Oh, sure, I was surprised.[SOFTBLOCK] Shocked by the sheer brutality of it.[SOFTBLOCK] Shocked at the cold efficiency.[SOFTBLOCK] Shocked at the strewn casings and fragments and exposed wires.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] But if you asked me if I thought 55 was capable of such a massacre, even before seeing it, I would not have said no.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] But that's not her![SOFTBLOCK] That's not 55![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] It's not the same person, she's better![SOFTBLOCK] You said she wanted to change![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] And was that me saying it because I believed it, or because I wanted it to be true so badly that I believed it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I admit, I have a tendency to fool myself.[SOFTBLOCK] I told myself everything was okay.[SOFTBLOCK] I know how to trick myself into being happy when I'm broken inside.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] But then I became a robot.[SOFTBLOCK] I met you.[SOFTBLOCK] And I told myself that was all over.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] And it wasn't![SOFTBLOCK] I was lying to myself and I lied to 55 along with me![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Can you imagine how betrayed she would feel -[SOFTBLOCK] if she even feels anything![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Christine, stop this right now![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ..![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Aren't you just doing it again, but in the other direction?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] You have so much to be hopeful for, and here you are beating yourself up![SOFTBLOCK] Well, I won't let you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] 55 probably looks at you as her only true friend.[SOFTBLOCK] You stuck with her and believed in her, and that counts![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] She hasn't returned any of my messages for the last three weeks...[SOFTBLOCK] She's ignoring me...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] She's going through a lot, too.[SOFTBLOCK] You don't have to be there for her right now, but you do when she is ready for you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Yeah...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I suppose she's learning what her challenges are.[SOFTBLOCK] She's finding out what she has to do to avoid becoming the same person as before.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] And -[SOFTBLOCK] even if I'm not sure, I know she'll pull it off.[SOFTBLOCK] When she sets her mind to something, she doesn't give up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Christine, you are my light.[SOFTBLOCK] My bright spot.[SOFTBLOCK] You're why I drag myself out of the defragmentation pod every day.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Be that for her.[SOFTBLOCK] She'll get tired or scared or demotivated, and want to stop.[SOFTBLOCK] Keep her going.[SOFTBLOCK] That's what you have to do.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] She keeps insisting she doesn't feel anything.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] And you believed her?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] You should know, you claim to be the expert at lying to yourself.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Even if she thinks she doesn't, she does.[SOFTBLOCK] I bet her heart broke when she saw herself in those videographs.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Yeah...[SOFTBLOCK] Because that's what I would do.[SOFTBLOCK] I'd tell myself I felt nothing rather than feel pain.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Do you know what it's like being...[SOFTBLOCK] different?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Being shouted at on the street because you walk differently than everyone else?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Being reminded that you're bad and wrong and don't fit someone else's expectations and that's all that matters?[SOFTBLOCK] That random other people can't make your day, but they can ruin it, so they do?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It's unbearable, so you don't bear it.[SOFTBLOCK] You bury it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She must be doing that right now.[SOFTBLOCK] I -[SOFTBLOCK] PDU, send Unit 2855 an encrypted message please.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tell her to meet me in the repair bay basement, and that it's extremely important.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Message sent.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Unit 771852, someone is at the door.[SOFTBLOCK] Would you like me to allow them in?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Your door didn't stop me...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Unit 771852 has given Unit 499323 root access permissions to her quarters, and this PDU.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: She has also put maximum priority on messages from Unit 499323, including authorization to wake her from defragmentation if necessary.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh really?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Erm...[SOFTBLOCK] That's actually kind of a big step...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, I know![SOFTBLOCK] PDU, let the visitor in.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Hey![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hee hee![SOFTBLOCK] You've made a big mistake, dearest![SOFTBLOCK] Now I'm never going to stop messaging you while you're sleeping![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I love you so much right now...[SOFTBLOCK] Tease me more...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Golem enters.
		fnCutsceneTeleport("Golem", 8.25, 10.50)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --They turn to face her.
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneFace("Sophie", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Show the dawn shot.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] My apologies, Lord Unit, but I have a message for you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A message?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] That can't be sent to my PDU via the network?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Special order, Lord Unit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No problem![SOFTBLOCK] Sorry if I gave you the wrong idea there.[SOFTBLOCK] We're just in the middle of something.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you all right?[SOFTBLOCK] You look pretty down.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] No Lord Unit.[SOFTBLOCK] Everything is optimal.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm a repair unit.[SOFTBLOCK] Your eyes are in low-power mode, and I can see micro-fractures building up on the outside of your joints.[SOFTBLOCK] A full scan would likely show overstressing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, are you sure you're all right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] So the rumours were true about Lord Unit Christine...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Rumours?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ..![SOFTBLOCK] I-I-I mean, Lord Unit 771852![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Ha ha![SOFTBLOCK] No need to be so formal![SOFTBLOCK] Christine is fine![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Some of the Slave Units seem to be taking a liking to you.[SOFTBLOCK] Maybe it's because you don't shout at them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I've been putting in a good word here and there too, of course.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I was talking to some of the other Slave Units on the tram here.[SOFTBLOCK] They say you're...[SOFTBLOCK] nice.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks![SOFTBLOCK] I try.[SOFTBLOCK] Make sure to see a repair unit about that stress fractures and get a good defragmentation in as soon as you can.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ...[SOFTBLOCK] Thank you, Lord Unit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh, the message![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Here you go, it's a special note.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I'm not privy to the contents, of course, but it seems that the other Lord Units were happy to get theirs.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Aren't you curious what's in it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] It's -[SOFTBLOCK] not my place to know.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Balderdash.[SOFTBLOCK] Come, let's read it together.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Christine moves to the couch.
		fnCutsceneMove("Golem", 8.25, 6.50)
		fnCutsceneMove("Sophie", 7.25, 7.50)
		fnCutsceneMove("Christine", 7.25, 6.50)
        fnCutsceneFace("Golem", -1, 0)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneFace("Sophie", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Show the dawn shot.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Now look at this, a handwritten note on embroidered plastic paper...[SOFTBLOCK] luxurious![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'Dear Lord Unit 771852.[SOFTBLOCK] You are cordially invited to the annual Sunrise Gala, to be hosted in the Arcane Academy's ballroom hall.'[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Blah blah blah, date, time...[SOFTBLOCK] And I can bring a plus one![SOFTBLOCK] Splendid![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] What's the Sunrise Gala?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] OH.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] MY.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] GOOOOOSH.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] IT'S TIME FOR THE GALA I ALMOST FORGOT![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] SQUEEEE!!![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Yikes!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So what's this gala, then?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Oh oh oh, yes, you're a new unit, this is your first year here.[SOFTBLOCK] Sorry.[SOFTBLOCK] But -[SOFTBLOCK] ohmygoshyougettogotothegala![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] So that's why I'm delivering handwritten letters![SOFTBLOCK] It makes sense now![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Gala.[SOFTBLOCK] What is it already?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Only the biggest and fanciest event of the year![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Ah, well, one of the two.[SOFTBLOCK] The Sunset Gala is equally as big, as you might imagine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] See -[SOFTBLOCK] and correct me if I misremember something, PDU -[SOFTBLOCK] this is an event that goes back to the founding of the Arcane Academy.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Because it's six months of dark and six of light, the mages would celebrate the sunrise and sunset each year with a big party -[SOFTBLOCK] and probably get really drunk.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Humans do love their alcohol.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] It's one of the few traditions that has carried over, and now, you get to go![SOFTBLOCK] Gah I'm so excited![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Sounds fun![SOFTBLOCK] Have you ever been?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[SOFTBLOCK] No.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Obviously no.[SOFTBLOCK] No![SOFTBLOCK] Only the Lord Golems and Command Units get to go.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh.[SOFTBLOCK] Yes.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] For a moment I forgot what kind of place we lived in.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Some of my friends have gotten clean-up duty afterwards, but we've never actually seen it in action.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] But now, all the Lord Units are going to be getting the flashiest, shiniest, and prettiest dresses and parading them around![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] And more importantly, most of the Lord Units go to the gala so they're not supervising us.[SOFTBLOCK] So we usually have our own little parties, too.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Am I invited to those, too?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh Christine, please.[SOFTBLOCK] I know what you're trying to do, but you really can't let me hold you from this.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Ever since I was first converted I've wanted to go.[SOFTBLOCK] I couldn't stop you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well then why don't you come with me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Ha ha ha![SOFTBLOCK] I can see why the other units like you so much![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Ahhhh -[SOFTBLOCK] no, she's serious, I think.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, Sophie.[SOFTBLOCK] I'm not going without my tandem unit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's got to be a way.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh, I'm sorry![SOFTBLOCK] I'd best be delivering the rest of these.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] But, Lord Unit Christine, if you want to come to one of our parties I'm sure I can convince everyone.[SOFTBLOCK] Just send me a message.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Certainly![SOFTBLOCK] Have a good day, and stop by the Sector 96 repair bay if you're ever in the area!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Golem leaves.
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneFace("Sophie", 0, 1)
		fnCutsceneMove("Golem", 8.25, 10.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Golem enters.
		fnCutsceneTeleport("Golem", -100, -100)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
        fnCutsceneFace("Sophie", 0, -1)
		fnCutsceneBlocker()
        
        --Show the dawn shot.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] ...[SOFTBLOCK] Hey, look.[SOFTBLOCK] 55 messaged me back.[SOFTBLOCK] She's on her way to the repair bay.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But yes, Sophie.[SOFTBLOCK] If there's a way for you to attend the gala, I'm going to find it.[SOFTBLOCK] Don't you worry.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Attending the gala is what's making me worry, silly.[SOFTBLOCK] At the very least I'd get kicked out and reprimanded, and worse, they'd probably discipline you, too.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, I'll think of something.[SOFTBLOCK] Hey, have you met 55 yet?[BLOCK][CLEAR]") ]])
        if(iSXUpgradeQuest < 3.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Heh -[SOFTBLOCK] when I chased her off after she shocked you.[SOFTBLOCK] Remember?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Not the best circumstances.[SOFTBLOCK] Let's give you a proper introduction, shall we?[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] Of course![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] She carried SX-399 all the way to me so I could fix her, remember?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, of course![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] By the way, how is that poor dear doing with the shiny upgrades I gave her?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She's never been better, and I hope someday soon we can all hang out together without fear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Now let's go tell 55 all about the gala, and cheer her up a bit, shall we?[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Lead the way, dearest.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Transition to the next part of the scene.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityG", "FORCEPOS:14.0x8.0x0") ]])
        fnCutsceneBlocker()
    
    --Gala Dress Scene.
    elseif(iIsGalaTime == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N", 2.0)
        LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/DressTime.lua")
    
	end
end
