--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit door.
if(sObjectName == "ToCryoB") then

	--Variables.
	local iUnlockAirlockDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N")
	if(iUnlockAirlockDoor == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Access denied.") ]])
		fnCutsceneBlocker()
	
	--Allow.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusCryoB", "FORCEPOS:9.5x15.0x0")
	end

--Intercom.
elseif(sObjectName == "Intercom") then

	LM_ExecuteScript(gsRoot .. "CharacterDialogue/Intercom/Root.lua", "Hello")
    
elseif(sObjectName == "ToCryoSouthA") then
    
    --Variables.
    local sChristineForm    = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iReceivedFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
	local iMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55", "N")
    
    --Christine is a Human:
	if(iMet55 == 0.0 or sChristineForm == "Human") then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Subject would not survive in a vacuum.[SOFTBLOCK] Please secure the subject before transport.") ]])
		fnCutsceneBlocker()
    
    --Christine is a golem and has not met Sophie yet:
    elseif(iReceivedFunction == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](My programming indicates I must move to Regulus City for function assignment.[SOFTBLOCK] This is the incorrect airlock.[SOFTBLOCK] The correct airlock is on the western end of the command deck.)") ]])
		fnCutsceneBlocker()
    
    --Move outside.
    else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusCryoSouthA", "FORCEPOS:45.5x22.0x0")
    
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end