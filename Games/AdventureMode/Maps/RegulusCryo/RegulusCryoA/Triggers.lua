--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Begins the intro sequence for Chapter 5.
if(sObjectName == "Intro") then

	--[Checks]
	--Don't show this twice.
	local iShowIntroScene = VM_GetVar("Root/Variables/Chapter5/Scenes/Intro|iShowIntroScene", "N")
	if(iShowIntroScene == 0) then return end
	
	--No music.
	AL_SetProperty("Music", "Null")
	
	--[Flags]
	--Set the flag to prevent playing this twice.
	VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iShowIntroScene", "N", 0.0)
	
	--Set Christine to be a man.
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Male.lua")
		
	--[Setup]
	--Immediately fade to fullblack. This prevents the player from seeing anything.
	Debug_DropEvents()
	AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
	
	--[Intro]
	if(gbBypassIntro == false) then
	
		--Set Christine to wounded.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--[Dialogue]
		--Now, show the opening scene.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The chatter of the crowd fell silent as all eyes watched Brighton Boarding School's top tennis player prepare her serve.[SOFTBLOCK] The resounding *thwack* echoed in the near-silence, and the ball arced across the court.[SOFTBLOCK] It seemed almost to hover above the net before its spin cast it downward, out of the reach of the charging opponent.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The crowd cheered, and the referee called the point, match, and game for Brighton.[SOFTBLOCK] On the sidelines, Chris Dormer, an English teacher who served as the school's tennis instructor, beamed with pride.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "His student had come far, winning the regional qualifiers in a stunning upset despite the challenges she had faced only a year prior.[SOFTBLOCK] In just a few weeks, she would be playing in the nationals.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "A party would be held the next day, unanimously voted by the girls to be a pizza party.[SOFTBLOCK] Schedules would need to be shifted to cover classes, and awards made for display in the school.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "More immediately, he needed to herd the excited girls back to the school.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The matches had gone on longer than expected, and it was already late in the afternoon when the awards finished.[SOFTBLOCK] Dusk was already creeping along the roads as the rumbling engine of the bus began to ferry its excited occupants back to the school and dorms that awaited them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Awaited all but one.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "A faint light began to seep from Chris' pocket as his family's heirloom began to glow.[SOFTBLOCK] The small, inscribed stone grew brighter as the bus entered a dark tunnel, its light lost in the glow of the girls' cell phones and the headlights of passing cars.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "When the bus emerged into the last light of the setting sun, Chris was gone.") ]])

		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(180)
		fnCutsceneBlocker()
		
		--Activate music.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
		
		--Next part.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The quiet but incessant hum of some distant machine roused Chris from his stupor.[SOFTBLOCK] He had but a single heartbeat to take in his new surroundings before he began to choke.[SOFTBLOCK] He was standing in a metal room, the red light of the setting sun replaced by a single red light on the wall.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "How he had come to be here he could not guess, and his mind afforded him no time to think as the realization of suffocation enveloped him.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The room was devoid of any air save for that which the vacuum had ripped from his lungs.[SOFTBLOCK] He sought to cry out, to call for help, but no voice could be heard.[SOFTBLOCK] All that greeted his attempts was the peculiar sensation of the saliva on his tongue boiling away from his opened mouth.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "As the light faded from his eyes and his strength failed him, he lurched towards the red light.[SOFTBLOCK] His vision grew black and his grasping hands fell limp, and he crumpled to the floor.[SOFTBLOCK] The final sensation to reach his slipping mind was a soft hiss and a curious warmth.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK].") ]])
		fnCutsceneBlocker()
		
		--Deactivate the overlay.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris awoke slowly, his hands grasping at the wall as his mind still reeled in panic.[SOFTBLOCK] How much time had passed he could not guess, as the only light to reach him was the same red light on the wall that had greeted his unplanned arrival.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Had it been minutes or hours?[SOFTBLOCK] From the aching of his muscles and the burning in his lungs, his mind, slowly collecting itself, even entertained the thought of days.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "He knew only two things for certain:[SOFTBLOCK] wherever he was, he was able to breath once more, and that an angry-sounding voice was demanding he answer it.") ]])
		fnCutsceneBlocker()

		--[Finish Up]
		--Have Christine crouch.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
			
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Have Christine stand up.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Null")
		DL_PopActiveObject()
	
	--Skipping intro.
	else
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()
	end

	--Start the music.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "RegulusTense") ]])
	fnCutsceneBlocker()
    
    --Party setup.
	AC_SetProperty("Set Party", 0, "Christine")
	AC_SetProperty("Set Party", 1, "Null")
	AC_SetProperty("Set Party", 2, "Null")
	AC_SetProperty("Set Party", 3, "Null")

end
