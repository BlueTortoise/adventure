--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCryoB"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "RegulusTense")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
    AM_SetMapInfo("No Special Map", "Null", 1209, 1158)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain6")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain0")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain1",   0,   10)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain2",  11,   20)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain3",  21,   30)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain5",  31,   50)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain4",  51, 1000)
    AM_SetProperty("Advanced Map Properties", 1143.0 - 16, 1020.0 - 16, 70.0 / 96.0, 70.0 / 96.0)

	--[Lights]
    AL_SetProperty("Activate Lights")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 1.0) then
	
	--Spawn an entity to represent the PDU.
	else
		TA_Create("PDUNPC")
			TA_SetProperty("Position", 3, 11)
			TA_SetProperty("Clipping Flag", false)
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", 0, p-1, "Root/Images/Sprites/Objects/PDU")
				TA_SetProperty("Move Frame", 1, p-1, "Root/Images/Sprites/Objects/PDU")
				TA_SetProperty("Move Frame", 2, p-1, "Root/Images/Sprites/Objects/PDU")
				TA_SetProperty("Move Frame", 3, p-1, "Root/Images/Sprites/Objects/PDU")
			end
			TA_SetProperty("Facing", 0)
			TA_SetProperty("Set Ignore Special Lights", true)
		DL_PopActiveObject()
	end
	

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		
		--Position.
		local fXPos =  4.20
		local fYPos = 11.70
		fnCutsceneTeleport("PDUNPC", fXPos, fYPos)
		
		--Put a radial light on the PDU.
		AL_SetProperty("Register Radial Light", "PDULight", (fXPos * 16.0) - 8.0, (fYPos * 16.0) - 12.0, 256.0)
	end

end
