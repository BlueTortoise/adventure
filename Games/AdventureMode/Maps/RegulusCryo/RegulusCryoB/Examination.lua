--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit door.
if(sObjectName == "ToCryoA") then

	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoA", "FORCEPOS:4.5x2.0x0")

--PDU.
elseif(sObjectName == "PDU") then

	--If we already have the PDU, do nothing.
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 1.0) then return end
	
	--Otherwise, get the PDU.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 2.0)
	
	--Give Chris a Tazer, and equip it.
	LM_ExecuteScript(gsItemListing, "Tazer")
	AC_PushPartyMember("Christine")
		ACE_SetProperty("Equip", "Weapon", "Tazer")
	DL_PopActiveObject()

	--Dialogue stuff.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (This must be what that lady was talking about.[SOFTBLOCK] She didn't mention all the bodies...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (Looks kind of like an old cell phone.[SOFTBLOCK] Let's see...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*click*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Hello![SOFTBLOCK] I am the Portable Database Unit, revision 7.0![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Aren't you going to say hello?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Hello?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Thank you![SOFTBLOCK] Voice imprint acknowledged.[SOFTBLOCK] Setting facial recognition now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "*snap*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: You are not in the existing database.[SOFTBLOCK] I will register a new user.[SOFTBLOCK] What is your name?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Umm...[SOFTBLOCK] Chris.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Hello, Um... Chris![SOFTBLOCK] May I call you Chris for short?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] *sigh*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Oh, I'm just kidding![SOFTBLOCK] Would you like me to disable humour protocols?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] No, it's fine.[SOFTBLOCK] I was just -[SOFTBLOCK] PDU, what happened here?[SOFTBLOCK] Who killed all these -[SOFTBLOCK] robot -[SOFTBLOCK] women?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Error [SOFTBLOCK]-[SOFTBLOCK] unable to access network.[SOFTBLOCK] Also, it is termed 'retired', and the units in this room are 'golems'.[SOFTBLOCK] Please update your dictionary software.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Considering the plasma scoring and damage to the room, I would venture that they retired one another.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Why?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Because -[SOFTBLOCK] error[SOFTBLOCK], unable to access network.[SOFTBLOCK] Query cannot be completed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Please contact your network administrator.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Can you at least tell me where I am?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: You are currently standing in the Extra-Vehicular Activity (EVA) room in the Cryogenics Research Facility on Regulus.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Regulus?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Regulus is the moon of Pandemonium.[SOFTBLOCK] It completes one orbit every 45.15 days and is tidally locked to Pandemonium.[SOFTBLOCK] Its mass is - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Thank you, that's enough.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] PDU, do you know how I got here?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: I am sorry, Chris.[SOFTBLOCK] That information is not in my database.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Since I am unable to access the network, I would recommend moving to Regulus City and querying the central database.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Is there anything else I can do for you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] That will be enough for now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (Hmm, there's something else on the table here.[SOFTBLOCK] Looks like a tazer, and a first-aid kit.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem](*Received Tazer and Doctor Bag.*)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (I hope I don't have to use it...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Chris, I don't mean to interrupt, but it seems the area around us is rather dark.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: You can adjust the brightness via the options menu if you are finding it too difficult to navigate.[SOFTBLOCK] Thank you!") ]])
	fnCutsceneBlocker()
	
	--Set the Doctor Bag charges.
    gbAutoSetDoctorBagCurrentValues = true
    LM_ExecuteScript(gsComputeDoctorBagTotalPath)
	
	--Remove the PDU NPC.
	fnCutsceneTeleport("PDUNPC", -100.0, -100.0)
	fnCutsceneInstruction([[ AL_SetProperty("Disable Light", "PDULight") ]])
	fnCutsceneBlocker()

--Damaged Golem.
elseif(sObjectName == "GolemA") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[SOFTBLOCK] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #48922.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Major plasma damage to central processing unit.[SOFTBLOCK] Subject absorbed multiple major hits at close range.[SOFTBLOCK] System offline was instantaneous.") ]])
		fnCutsceneBlocker()
	end

--Damaged Golem.
elseif(sObjectName == "GolemB") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[SOFTBLOCK] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #51641. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Blunt trauma to processing unit.[SOFTBLOCK] Subject suffered a heavy strike from behind, crushing the CPU's thermal regulator.[SOFTBLOCK] System offline was instantaneous.") ]])
		fnCutsceneBlocker()
	end

--Damaged Golem.
elseif(sObjectName == "GolemC") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[SOFTBLOCK] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #10911. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Plasma damage to power storage battery.[SOFTBLOCK] Subject absorbed a plasma difractor blast at close range, then fell to the ground.[SOFTBLOCK] A second blast struck the storage battery, offlining the unit.") ]])
		fnCutsceneBlocker()
	end

--Damaged Golem.
elseif(sObjectName == "GolemD") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[SOFTBLOCK] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #10912. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Plasma damage to processing unit.[SOFTBLOCK] Subject was attempting to carry another, damaged, unit when it received a plasma difractor blast to the cranial region.[SOFTBLOCK] System offline was instantaneous.") ]])
		fnCutsceneBlocker()
	end

--Damaged Golem.
elseif(sObjectName == "GolemE") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[SOFTBLOCK] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #711820. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Fragmentation explosive detonated on impact with central chassis.[SOFTBLOCK] All systems suffered catastrophic damage, system offline was instantaneous.") ]])
		fnCutsceneBlocker()
	end
    
--Window
elseif(sObjectName == "Window") then

    --Variables.
    local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    if(iHasGolemForm == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] (I can see grey rock outside, and I was in an airlock...[SOFTBLOCK] Am I on -[SOFTBLOCK] the moon?[SOFTBLOCK] How!?)") ]])
		fnCutsceneBlocker()
    else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Hard to imagine I used to be so scared of being on Regulus.[SOFTBLOCK] Even harder to imagine that became the best day of my life.)") ]])
		fnCutsceneBlocker()
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end