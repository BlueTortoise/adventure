--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Exit door. Requires the red keycard.
if(sObjectName == "ToCryoG") then

	--Variables.
	local iHasPDUCryoRedCard = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N")
	local iSeenRedDoorWarning = VM_GetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N")
	if(iHasPDUCryoRedCard == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Access denied.[SOFTBLOCK] Please present security-red keycard.") ]])
		fnCutsceneBlocker()
	else
		
		--Warning text. Only plays once.
		if(iSeenRedDoorWarning == 0.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 1.0)
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice: Red access granted.[SOFTBLOCK] Warning:[SOFTBLOCK] A power failure has been detected in this sector.[SOFTBLOCK] Proceed with caution.") ]])
			fnCutsceneBlocker()
		end
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCryoG", "FORCEPOS:18.0x14.0x0") ]])
		fnCutsceneBlocker()
	end

--Exit ladder.
elseif(sObjectName == "ToLowerCryoA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoLowerA", "FORCEPOS:3.0x19.0x0")

--[Terminals]
elseif(sObjectName == "ComputerA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 3.[SOFTBLOCK] The subject has not become any calmer since it was relocated to this facility.[SOFTBLOCK] It seems to retain a degree of sentience, but refuses to speak to any of our units.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Earlier, one of the restraints broke and the subject grabbed and throttled the nearest slave unit.[SOFTBLOCK] Six units were damaged in the ensuing chaos.[SOFTBLOCK] The subject was eventually subdued through physical force.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Our first priority will be researching improved containment procedures.[SOFTBLOCK] Personnel damage is expensive and it will be better to have a failsafe rather than send units to maintenance.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The restraint had been of premium quality.[SOFTBLOCK] It seems it broke due to the sheer strength of the subject, which would be at least fifteen times that of a normal human.[SOFTBLOCK] Such strength is incredible.[SOFTBLOCK] If we can harness it, we could greatly improve slave unit efficiency.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The subject is asleep now.[SOFTBLOCK] It is unsettling, to say the least, that the subject sleeps with its eyes open.[SOFTBLOCK] Neurological scans show it is undergoing REM sleep despite not moving its eyes.[SOFTBLOCK] More research is required.[SOFTBLOCK] Unit 609144 signing out.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerB") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 4.[SOFTBLOCK] Subject has developed a series of orbs under her skin, at various locations without any discernable pattern.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Extraction of one such orb did not appear to cause any biological harm to the subject.[SOFTBLOCK] The orb did not grow back, and I am unable to determine if it is a biological organ or perhaps a lymph node.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The orb was placed in the containment area until we determine if it has any unusual properties.[SOFTBLOCK] If necessary, we can extract more orbs and use them to experiment on other subjects.[SOFTBLOCK] Signing out.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 12.[SOFTBLOCK] Central administration either has a great deal of faith in my abilities, or none.[SOFTBLOCK] I cannot say.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Strange radio signals have been reported from the surface near the facility.[SOFTBLOCK] Recently, some emanations were discovered inside the facility.[SOFTBLOCK] Why they require me to find the source is beyond me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: It's probably a slave unit with an unauthorized radio making short-wave transmissions for fun.[SOFTBLOCK] Increasing our security detail is thus not necessary.[SOFTBLOCK] I have instead reassigned the security units to aiding my research.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The power exchange system is due to come online tomorrow, meaning we will no longer require a line from Regulus City.[SOFTBLOCK] I will thus also be forced to compute the power budget.[SOFTBLOCK] Why is all of this being dumped on a research unit?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: No matter.[SOFTBLOCK] I will press on.[SOFTBLOCK] I've put in a request for more research units and will be looking for ways to improve my own efficiency.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 15.[SOFTBLOCK] A breakthrough![SOFTBLOCK] The subject has not awoken at all since it was transferred here![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Despite the eyes being open and 'aware', the subject is in a state of REM sleep at all times.[SOFTBLOCK] Even when it is observing units in the room with it, interacting with objects, or receiving stimulus, the subject is still asleep.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: While this causes me to ask a great deal of questions, answers are not forthcoming.[SOFTBLOCK] If this is what the subject is like when asleep, what would awakening it do?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I admit in my enthusiasm, I attempted several times to wake the subject up, with no success.[SOFTBLOCK] Incisions, burns, and high-frequency sound waves cause physical harm but have no impact on its sleeping patterns.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: In retrospect, we would need to quadruple our staff and provide combat routines to all of them just to contain it, should it escape.[SOFTBLOCK] I have reconsidered, and now I think it shall remain asleep for the time being.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 18.[SOFTBLOCK] Once again I am surrounded by incompetence.[SOFTBLOCK] Just where does central administration find these slave units?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The sample I extracted from the subject and placed in the containment area is gone.[SOFTBLOCK] I've pored over the cameras in that area but was unable to find a culprit.[SOFTBLOCK] Conveniently, there was a power outage for about thirty-five seconds two days ago.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I do not have time to narrow down suspects and cross-reference alibis.[SOFTBLOCK] I am a research unit.[SOFTBLOCK] And yet, there is no trace of the lost sample.[SOFTBLOCK] Why would a unit steal it?[SOFTBLOCK] To what end?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: No matter.[SOFTBLOCK] We will extract another sample and begin experiments on it.[SOFTBLOCK] I have ordered riot equipment for the slave units doing the extraction.[SOFTBLOCK] A repeat of last time would be undesirable.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "ComputerF") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 19. The mystery has been solved.[SOFTBLOCK] Punishments have been rescinded for all suspected slave units.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: After extraction of an orb sample from the subject's flesh, the very first experiment I subjected it to was simple thermal conductivity.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: When exposed to a sudden change in temperature, the material burst into a vapour.[SOFTBLOCK] Cold or hot, the outcome was the same.[SOFTBLOCK] This explains where the sample went.[SOFTBLOCK] Likely, the temperature of the containment unit spiked during the power outage.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I was unable to detect any lingering vapour, so the experiment will need to proceed with better sensors in place.[SOFTBLOCK] Of interesting note is the fact that the nearby intercom unit emitted static when the orb burst.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: One of the slave units also complained that a great deal of interference was being experienced when we were performing the experiment.[SOFTBLOCK] I disciplined the unit, but it seems the orbs are capable of emitting and/or reflecting radio waves.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Why a biological sample would produce or reflect radio waves is its own question, but I am glad we are making headway.[SOFTBLOCK] More experiments will follow.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "ComputerG") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 22.[SOFTBLOCK] Once again I am too clever for my own good.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: According to some recent findings by the units at Equinox, it is possible to map a living subject's REM cycle dreams into videographs.[SOFTBLOCK] Naturally I procured the mapping program they were using.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: And no sooner had I plugged the electrodes in and began recording data did a large amount of noise result.[SOFTBLOCK] I calibrated the software on a human subject, and the software is not malfunctioning.[SOFTBLOCK] This is, indeed, what the subject dreams of.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The audio channels are largely unused.[SOFTBLOCK] The video channels are chaotic garbles.[SOFTBLOCK] Periodically, geometric shapes of single-digit sides can be seen flowing across the background.[SOFTBLOCK] The dreams do not change in response to external stimulus.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I have set the program to record 72 hours of footage for later review while we consolidate our findings.[SOFTBLOCK] The slave units clearly need some time away from the subject, as they are behaving erratically.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I have ordered a few other subjects removed from the containment area for routine cryogenics data gathering.[SOFTBLOCK] After a few days, we will resume work on the subject.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()

--[Golem Bodies]
elseif(sObjectName == "GolemA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #990. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Major damage to power core.[SOFTBLOCK] Subject chassis absorbed a compression wave, possibly due to an explosive or directed sonic weapon.[SOFTBLOCK] The power core then ruptured, leading to a system offline within a few minutes.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #200817. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Self-inflicted plasma bolt.[SOFTBLOCK] Subject appears to have fired her plasma difractor into her own CPU from close range.[SOFTBLOCK] System offline was instantaneous.") ]])
	fnCutsceneBlocker()
		
elseif(sObjectName == "GolemC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #155981. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] CPU destruction.[SOFTBLOCK] Subject received two kinetic strikes to the rear chassis, then several pulse rounds into her cranial chassis.[SOFTBLOCK] The CPU and all related architecture is completely destroyed.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #208179. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Kill phrase.[SOFTBLOCK] Subject was severely damaged, including the loss of multiple limbs.[SOFTBLOCK] Unable to move, another unit stated its kill phrase twice.[SOFTBLOCK] System is permanently offline.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK]  Unit #208179. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] CPU destruction.[SOFTBLOCK] The unit received several pulse impacts to its joints.[SOFTBLOCK] While immobilized, it received a pulse hit to its cranial chassis, destroying its CPU.") ]])
	fnCutsceneBlocker()

--[Other Objects]
--Triggers dialogue with 55.
elseif(sObjectName == "Intercom") then
	LM_ExecuteScript(gsRoot .. "CharacterDialogue/Intercom/Root.lua", "Hello")

--Several cryogenics tubes. All are offline.
elseif(sObjectName == "CryoTube") then

	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
	
	--Doesn't have golem form:
	if(iHasGolemForm == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "(Looks like some sort of research tube.[SOFTBLOCK] It's offline.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] PDU, what is this thing?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: This is a Cryogenic Containment Tube Mk VII.[SOFTBLOCK] This model has been outfitted with extra sensors, tubes, and drainage clamps.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Unlike a standard tube, this one can mix fluids, administer electricity or sound waves, and inject the subject with a wide array of chemicals.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: These are very commonly used for scientific research on restrained organic subjects, as opposed to the long-term storage models.") ]])
		fnCutsceneBlocker()

	--Has golem form:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A Cryogenic Containment Tube Mk VII.[SOFTBLOCK] These are models used for research as opposed to long-term cryogenic storage of organic subjects.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](They are outfitted with extra sensors and can administer shocks, sound waves, and various fluid injections.[SOFTBLOCK] They serve the Cause of Science well.)") ]])
		fnCutsceneBlocker()

	end

--Defragmentation tube, closed or open.
elseif(sObjectName == "DefragTube") then

	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
	
	--Doesn't have golem form:
	if(iHasGolemForm == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "(A glass tube with some computer hookups in it...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] PDU, what is this thing?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: An Autonomous Defragmentation Module, Mk IV. This model has been outfitted with a number of additional sensors.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: These tubes are usually used for units to recharge their power cores and defragment their hard drives.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: This one is used for performing cybernetic experiments on subjects, such as outfitting an organic's neurological systems with interface ports.[SOFTBLOCK] Most experiments thus far have been unsuccessful.") ]])
		fnCutsceneBlocker()

	--Has golem form:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](An Autonomous Defragmentation Module, Mk IV.[SOFTBLOCK] These aren't used to defragment a unit's drives, though they could with a software update.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Instead, these are used for cybernetics experiments, such as installing neuro-interfaces on organics.[SOFTBLOCK] They serve the Cause of Science well.)") ]])
		fnCutsceneBlocker()

	end

--Rebel Banners
elseif(sObjectName == "Banner") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    
    --Male Chris:
    if(iHasGolemForm == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a robot, broken in half.[SOFTBLOCK] The banner has been torn up by projectiles.)") ]])
    
    --Golem Christine, in Programmed Mode
    elseif(iHasGolemForm == 1.0 and iTalkedToSophie == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a golem, broken in half.[SOFTBLOCK] The banner has been torn up by projectiles.[SOFTBLOCK] It is not relevant to my programming.)") ]])
        
    --After Regulus City:
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a golem, broken in half.[SOFTBLOCK] The banner has been torn up by projectiles.[SOFTBLOCK] Your retirement will not be in vain, rebel sister...)") ]])
    
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end