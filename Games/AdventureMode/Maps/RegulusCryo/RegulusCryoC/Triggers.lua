--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Gotta be clean!
if(sObjectName == "ScrubUpNotice") then

	--Variables
	local iHasSeenScrubNotice = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasSeenScrubNotice", "N")
	local iHasGolemForm       = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
	
	--Play the scene.
	if(iHasSeenScrubNotice == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasSeenScrubNotice", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Attention.[SOFTBLOCK] If you are entering a surgery theater, please be sure to remove all organic contaminants from your chassis.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
		
		--Male:
		if(iHasGolemForm == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] PDU, I don't think that matters right now...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: It was listed as a high-priority notice in my databanks.[SOFTBLOCK] Thank you for your cooperation.") ]])
		
		--Female:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] PDU, I don't think that matters right now...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: It was listed as a high-priority notice in my databanks.[SOFTBLOCK] Thank you for your cooperation.") ]])
		end
		fnCutsceneBlocker()
	end
end
