--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit ladder.
if(sObjectName == "ToCryoG") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoG", "FORCEPOS:12.0x12.0x0")

--Exit door.
elseif(sObjectName == "ToCryoCommandC") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoCommandC", "FORCEPOS:5.5x8.0x0")
	
--Exit door.
elseif(sObjectName == "ToCryoCommandB") then

	--Variables.
	local iIntercomState = VM_GetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N")

	--No access.
	if(iIntercomState < 7.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Only command staff are authorized in the command center.") ]])

	--Access granted.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusCryoCommandB", "FORCEPOS:10.0x20.0x0")
	end
	
--Exit ladder.
elseif(sObjectName == "ToCryoFabB") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoToFabricationB", "FORCEPOS:4.0x21.0x0")
	
--Door, toggles the floor light behind it on.
elseif(sObjectName == "DoorToggleLight") then
	AL_SetProperty("Enable Light", "ToggleLight")

--Intercom.
elseif(sObjectName == "Intercom") then

	LM_ExecuteScript(gsRoot .. "CharacterDialogue/Intercom/Root.lua", "Hello")
	
--Conversion Tube. Has different examination post-golem TF.
elseif(sObjectName == "ConversionTube") then

	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")

	--Don't have it yet.
	if(iHasGolemForm == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|ChrisMaleVoice](A pod of some sort.[SOFTBLOCK] Unlike most of the other ones like it, this one still hums with power.)") ]])

	--Have Golem form.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The device which brought me to mechanical perfection...[SOFTBLOCK] Seems it's out of nanofluid, though.[SOFTBLOCK] I was lucky to get the last of it.)") ]])
	end

--Damage door.
elseif(sObjectName == "DoorC") then

	--Door is already fixed.
	local iFixedCryoDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N")
	if(iFixedCryoDoor == 1.0) then
		AL_SetProperty("Open Door", "DoorC")
		AudioManager_PlaySound("World|AutoDoorOpen")
	
	--Door needs repair.
	else

		--Get Christine's position.
		EM_PushEntity("Christine")
			local fXPos, fYPos = TA_GetProperty("Position")
		DL_PopActiveObject()
        
        --Variables.
        local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")

		--Christine is to the south of the door.
		if(fYPos >= 12.5 * 16.0) then
            
            --Male:
            if(iHasGolemForm == 0.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Error.[SOFTBLOCK] Door motivator damaged. Please contact a repair crew.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Um, PDU?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Yes, Chris?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Can you do anything about this door?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: One moment...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: The door appears to be unmotivated.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Hey, you're the one who didn't want to disable humour protocols.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: I can bypass the circuit for you, but I will need to access the panel on the other side of the door.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Lovely.") ]])
                fnCutsceneBlocker()
                
            --All others:
            else
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Error.[SOFTBLOCK] Door motivator damaged.[SOFTBLOCK] Please contact a repair crew.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: PDU, diagnotistics.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: One moment...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: I can bypass the circuit, but only from the other side of the door.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Affirmative.[SOFTBLOCK] Thank you, PDU.") ]])
            
            end
		
		else
            
            --Male:
            if(iHasGolemForm == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Error.[SOFTBLOCK] Door motivator damaged.[SOFTBLOCK] Please contact a repair crew.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] PDU?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Please open the panel next to the door and hold me up to it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: There.[SOFTBLOCK] The damaged circuit has been bypassed.[SOFTBLOCK] The motivator should work now.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Thank you, PDU.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Please redirect your praise to Unit 7018, who programmed my repair routines.[SOFTBLOCK] Have an exceedingly nice day!") ]])

            --All others:
            else
                VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Error.[SOFTBLOCK] Door motivator damaged.[SOFTBLOCK] Please contact a repair crew.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: PDU, activate circuit bridge tool.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Affirmative.[SOFTBLOCK] Please open the panel next to the door and hold me up to it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: There.[SOFTBLOCK] The damaged circuit has been bypassed.[SOFTBLOCK] The motivator should work now.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Good work, PDU.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Please redirect your praise to Unit 7018, who programmed my repair routines.[SOFTBLOCK] Have an exceedingly nice day!") ]])
            end
		end
	end
	
--Left computer.
elseif(sObjectName == "ComputerL") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 1.[SOFTBLOCK] I have been reassigned to the cryogenics research facility, and am unsure why.[SOFTBLOCK] Supposedly a very rare subject was acquired by the abductions department, but every request I have put in for details was denied.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: How am I supposed to do research on a subject when I don't even know what it is?[SOFTBLOCK] What sort of chicanery is this?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I have put in my staffing requests with central and settled in.[SOFTBLOCK] The subject is due to arrive tomorrow and I intend to be at the transit bay to greet it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I am very excited to be granted such a rare subject, despite the secrecy.[SOFTBLOCK] I hope I can learn a great deal from it.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()
	
--Right computer.
elseif(sObjectName == "ComputerR") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 2.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: A series of statistically improbably events has occurred concerning the sample.[SOFTBLOCK] The tracks bent as it was due to arrive, power fluctuations and interference in the radio network alarmed the slave units...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: My PDU started behaving erratically and needed a restart, and worst of all, the oil machine in my quarters gave me decaff.[SOFTBLOCK] Why would you ever drink decaff?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Still, despite the numerous setbacks, the exhiliration was palpable.[SOFTBLOCK] I've already organized a dozen initial experiments to begin as soon as possible.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The future is bright indeed.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()
	
--Retired golem.
elseif(sObjectName == "GolemA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #40219. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Concussive grenade caused rapid compression of the CPU casing, causing severe damage.[SOFTBLOCK] Unit went offline instantaneously.") ]])
	fnCutsceneBlocker()
	
--Retired golem.
elseif(sObjectName == "GolemB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #752119. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Multiple pulse impacts to main chassis.[SOFTBLOCK] This unit was caught by a hail of fire and did not seek cover.") ]])
	fnCutsceneBlocker()
	
--Retired golem.
elseif(sObjectName == "GolemC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #602314. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Multiple pulse impacts to main chassis.[SOFTBLOCK] Unit was attempting to drag another unit to cover when she was struck repeatedly by pulse impacts.") ]])
	fnCutsceneBlocker()
	
--Retired golem.
elseif(sObjectName == "GolemD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #729804. Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit was hit by a pulse diffractor in the ocular unit at close range.[SOFTBLOCK] System offline was instantaneous.") ]])
	fnCutsceneBlocker()

--Rebel Banners
elseif(sObjectName == "Banner") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    
    --Male Chris:
    if(iHasGolemForm == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a robot, broken in half.[SOFTBLOCK] The banner has been torn up by projectiles.)") ]])
    
    --Golem Christine, in Programmed Mode
    elseif(iHasGolemForm == 1.0 and iTalkedToSophie == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a golem, broken in half.[SOFTBLOCK] The banner has been torn up by projectiles.[SOFTBLOCK] It is not relevant to my programming.)") ]])
        
    --After Regulus City:
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a golem, broken in half.[SOFTBLOCK] The banner has been torn up by projectiles.[SOFTBLOCK] Your retirement will not be in vain, rebel sister...)") ]])
    
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end