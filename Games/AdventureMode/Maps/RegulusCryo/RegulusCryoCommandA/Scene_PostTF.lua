--[Post TF Scene]
--Brief scene that plays post-Golem-TF. Repositions Christine. Also replaces her equipment.

--Black the screen out.
AL_SetProperty("Music", "Null")
AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1)

--Reposition Christine.
fnCutsceneTeleport("Christine", 16.25, 4.50)
fnCutsceneFace("Christine", 0, 1)
fnCutsceneBlocker()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Christine takes her first true steps.
fnCutsceneMove("Christine", 16.25, 5.50, 0.30)
fnCutsceneBlocker()

--Set the leader voice to Christine's.
WD_SetProperty("Set Leader Voice", "Christine")

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Initialize cognition routines...[SOFTBLOCK] done.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Physical calibrations complete.[SOFTBLOCK] Spatial estimation matrix online.[SOFTBLOCK] Setting logical tables.[SOFTBLOCK] Secondary boot complete.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Assignment::[SOFTBLOCK] Move to Regulus City.[SOFTBLOCK] Receive further programming.[SOFTBLOCK] Carry assignment out::[SOFTBLOCK] Priority Zero.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (The airlock on the western edge of this floor should lead me there.[SOFTBLOCK] After that, I must follow the light posts to Regulus City.)") ]])
fnCutsceneBlocker()
	
--Music resumes.
fnCutsceneInstruction([[ AL_SetProperty("Music", "RegulusTense") ]])

--Give Christine her Lord Unit equipment.
local iGotGolemEquipment = VM_GetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N")
if(iGotGolemEquipment == 0.0) then
	
	--Flag.
	VM_SetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N", 1.0)
	
	--Add the equipment.
	LM_ExecuteScript(gsItemListing, "Carbonweave Electrospear")
	LM_ExecuteScript(gsItemListing, "Flowing Dress")

	--Equip it.
	AC_PushPartyMember("Christine")
		ACE_SetProperty("Equip", "Weapon", "Carbonweave Electrospear")
		ACE_SetProperty("Equip", "Armor", "Flowing Dress")
	DL_PopActiveObject()

	--Delete Christine's old equipment.
	AdInv_SetProperty("Remove Item", "Tazer")
	AdInv_SetProperty("Remove Item", "Schoolmaster's Suit")
	
	--Change the description of Christine's runestone, if it's not equipped.
	if(AdInv_GetProperty("Item Count", "Violet Runestone") == 1) then
		AdInv_PushItem("Violet Runestone")
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
		DL_PopActiveObject()
	
	--Runestone was equipped so get it from Christine's inventory.
	else
		AC_PushPartyMember("Christine")
			ACE_SetProperty("Equip", "Item A", "Nothing")
			ACE_SetProperty("Equip", "Item B", "Nothing")
		DL_PopActiveObject()
		
		AdInv_PushItem("Violet Runestone")
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
		DL_PopActiveObject()
		
		AC_PushPartyMember("Christine")
			ACE_SetProperty("Equip", "Item A", "Violet Runestone")
		DL_PopActiveObject()
	end
	
end