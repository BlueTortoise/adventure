--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--55's cutscene.
if(sObjectName == "Graveyard") then
	
	--Variables.
	local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	local i55ReturnCryoScene = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ReturnCryoScene", "N")
	
	--If the player hasn't seen this scene before:
	if(iMet55InLowerRegulus == 1.0 and i55ReturnCryoScene == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/i55ReturnCryoScene", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] So you've led us back here...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Listen, I don't like wandering around this graveyard any more than you do, but...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There may be supplies here we can use.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Indeed.[SOFTBLOCK] Supplies removed from Regulus City may be noticed by the administration, but those procured from Cryogenics Research will not.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good thinking, 771852.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I guess she doesn't find this place unsettling...)") ]])
		fnCutsceneBlocker()


	end
end
