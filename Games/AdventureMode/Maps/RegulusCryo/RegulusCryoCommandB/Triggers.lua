--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Meeting 55 cutscene.
if(sObjectName == "Cutscene") then
	
	--[Setup]
	--Variables.
	local iMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55", "N")
	if(iMet55 == 1.0) then return end
	
	--Set.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 8.0)
	
	--[Movement]
	--Move Christine forward.
	fnCutsceneMove("Christine", 10.25, 10.50)
	fnCutsceneBlocker()
	
	--Camera focuses on 55.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "2855sTheme") ]])
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 1.0)
		CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] Excuse me...[SOFTBLOCK] are you the lady who was on the intercom?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[VOICE|2855] Yes.[SOFTBLOCK] I recognize your voice.[SOFTBLOCK] It is deeper than the interference made it out to be.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--[Movement]
	--55 turns partway around.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair1")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair2")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] Y-[SOFTBLOCK]your legs...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[VOICE|2855] I needed the parts you were collecting.[SOFTBLOCK] That should have been obvious.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--[Movement]
	--55 turns the rest of the way.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair3")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair4")
	DL_PopActiveObject()
	fnCutsceneWait(305)
	fnCutsceneBlocker()
	
	--[Fade]
	--Start the fade so it takes a long time. Most players won't notice.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1000, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	
	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Broken") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: I am unit 2855.[SOFTBLOCK] I - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: You -[SOFTBLOCK] (partial data retrieval failure) [SOFTBLOCK] you are a [SOFTBLOCK]human...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] And you're one of those robot things![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Correct.[SOFTBLOCK] Your thermal output -[SOFTBLOCK] I see.[SOFTBLOCK] But the airlock -[SOFTBLOCK] Where did you achieve on the second airlock!?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] ...E- Excuse me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Where did you achieve on the second airlock!?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: My query matrix must need recalibration.[SOFTBLOCK] I apologize.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: ...[SOFTBLOCK] There.[SOFTBLOCK] Now.[SOFTBLOCK] How did you get in the airlock when it was depressurized?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] I don't know![SOFTBLOCK] Aren't you the one who brought me here? You should already know.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Incorrect, but not surprising.[SOFTBLOCK] No, I only know what I've managed to recover from the logs in this facility.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] What? What's not surprising?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Presumably due to whatever calamity befell this installation, my memory of everything before the last three days has been purged.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: If you were collected before that happened, well, perhaps one of the servant units let you out.[SOFTBLOCK] For what purpose I cannot guess.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: What is your name,[SOFTBLOCK] human?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] C-[SOFTBLOCK]...[SOFTBLOCK]Chris...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Is there something about me that is upsetting to you, Chris?[SOFTBLOCK] Your heart rate is elevated.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] You're...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Disfigured, yes I know.[SOFTBLOCK] I will need to get more complete repairs at Regulus City, but the motivators and ocular unit you have collected will allow me to walk again and balance myself.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: You will not need to install them.[SOFTBLOCK] I will be able to do it myself.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] You...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Not that it matters to any degree.[SOFTBLOCK] Without the authenticator chip, we will be unable to enter the city.[SOFTBLOCK] We are stranded here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] Um... [SOFTBLOCK]What is this chip you keep talking about?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: All units are provided with an authenticator chip when they are created.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: The codes on the chip produce a four-dimensional formula that calculates a pre-determined primordial-class prime number to a pre-determined length.[SOFTBLOCK] When access is requested, random variables are provided by the requester.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: The chip computes the result and returns a four-kilobyte number block. The result is a unique code every time, guaranteeing the chip is the original one and the unit is who she says she is.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] I don't think I follow what you mean...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: ...[SOFTBLOCK]Regardless, without such a chip, we will not able to enter Regulus City.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] And you don't have one?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Mine was...[SOFTBLOCK] removed.[SOFTBLOCK] As it was, it seems, with all the units you scanned.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] So someone was removing them?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: The motion trackers have not indicated any additional intelligent presence in the building save us.[SOFTBLOCK] The scraprats are not programmed to remove the chips.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Not that I could accomodate you.[SOFTBLOCK] As a human, you would not survive the journey to Regulus City.[SOFTBLOCK] The tram system tracks were destroyed and you will not be able to survive in a vacuum.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: There is no fate for you but to starve to death here, or die of dehydration.[SOFTBLOCK] I will make the process as comfortable as possible.[SOFTBLOCK] I owe it to you for helping repair me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] ...[SOFTBLOCK] Is this some kind of joke?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: No, it is not.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] Is there no other -[SOFTBLOCK] well...[SOFTBLOCK] No.[SOFTBLOCK] Maybe I...[SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: If you prefer it, I can terminate you quickly and painlessly.[SOFTBLOCK] A pulse-diffractor round to the temple will suffice.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] W-[SOFTBLOCK] Wha[SOFTBLOCK][SOFTBLOCK]t[SOFTBLOCK][SOFTBLOCK][SOFTBLOCK]?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Would this not be a preferrable alternative to death by means of dehydration?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] ...[SOFTBLOCK]that isn't, [SOFTBLOCK]... but[SOFTBLOCK]...[SOFTBLOCK] I-[SOFTBLOCK] I can't...[SOFTBLOCK][SOFTBLOCK] what[SOFTBLOCK]?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Do you prefer self-termination?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] ...[SOFTBLOCK]No! ...[SOFTBLOCK]i [SOFTBLOCK]...[SOFTBLOCK]WHAT is...[SOFTBLOCK] no[SOFTBLOCK]... you[SOFTBLOCK] ... [SOFTBLOCK][SOFTBLOCK]what[SOFTBLOCK]?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Come, I will need to repair myself before we can do anything.[SOFTBLOCK] Give the parts to me...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral][VOICE|ChrisMaleVoice] But you... [SOFTBLOCK]Uh...[SOFTBLOCK]...OK...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: Excellent.[SOFTBLOCK] *Click*[SOFTBLOCK] Installing drivers...[SOFTBLOCK] These parts will do well.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	
	--[Dialogue Resumes]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|Thump]*WHACK*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] Ugh, you...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[VOICE|2855] Perhaps you will prove useful to me again after all.[SOFTBLOCK] Now, I just place the core here...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Golem Transformation.
	fnCutsceneBlocker()
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Transform_GolemFirst/Scene_Begin.lua")
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
	fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCryoCommandA", gsRoot .. "Maps/RegulusCryo/RegulusCryoCommandA/Scene_PostTF.lua") ]])
	fnCutsceneBlocker()
	
end
