--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit door.
if(sObjectName == "ToCryoG") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoG", "FORCEPOS:3.5x12.0x0")
	
--Exit door.
elseif(sObjectName == "ToContainmentBR") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoContainmentB", "FORCEPOS:42.0x49.0x0")
	
--Exit door.
elseif(sObjectName == "ToContainmentBL") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoContainmentB", "FORCEPOS:24.0x49.0x0")

--Retired doll.
elseif(sObjectName == "GolemA") then

	--Variables.
	local iHasDollMotivatorA = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N")

	--Already has the motivator.
	if(iHasDollMotivatorA == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #67111.[SOFTBLOCK] Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Massive blunt trauma.[SOFTBLOCK] Unit was slammed into the floor at high velocity.[SOFTBLOCK] Resulting shock caused extreme damage to all vital systems.[SOFTBLOCK] System offline was instantaneous.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: All remaining subsystems are severely damaged.[SOFTBLOCK] The authenticator chip has been removed.[SOFTBLOCK] An incision in the back of the head is the likely removal vector.[SOFTBLOCK] The incision was made an unknown amount after system offline took place.") ]])
		fnCutsceneBlocker()
	
	--Pick up the motivator.
	else
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoBlueCard", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #67111.[SOFTBLOCK] Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Massive blunt trauma.[SOFTBLOCK] Unit was slammed into the floor at high velocity.[SOFTBLOCK] Resulting shock caused extreme damage to all vital systems.[SOFTBLOCK] System offline was instantaneous.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Subsystem scan completed.[SOFTBLOCK] Right primary leg motivator is of acceptable condition.[SOFTBLOCK] The motivator was on the opposite side of the impact, and did not suffer the same damage as the left motivator.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: [SOUND|World|TakeItem]The right primary motivator has been extracted and added to your inventory.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: [SOUND|World|Keycard]The retired unit was also in possession of a security-blue keycard.[SOFTBLOCK] It has been added to this PDU's database.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: All remaining subsystems are severely damaged.[SOFTBLOCK] The authenticator chip has been removed.[SOFTBLOCK] An incision in the back of the head is the likely removal vector.[SOFTBLOCK] The incision was made an unknown amount of time after system offline took place.") ]])
		fnCutsceneBlocker()
	end

--Retired doll.
elseif(sObjectName == "BigBoxFunClub") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Something big and extremely strong was in this storage container...)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end