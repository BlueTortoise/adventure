--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit door.
if(sObjectName == "ToContainmentAR") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoContainmentA", "FORCEPOS:18.0x3.0x0")
	
--Exit door.
elseif(sObjectName == "ToContainmentAL") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoContainmentA", "FORCEPOS:4.0x4.0x0")

--Door locked due to a breach.
elseif(sObjectName == "LockedDoor") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Access denied.[SOFTBLOCK] Breach detected in chamber.[SOFTBLOCK] Radiation levels exceeding safe levels by 3000 percent.[SOFTBLOCK] Please contact a cleanup crew.") ]])
	fnCutsceneBlocker()

--Retired doll.
elseif(sObjectName == "GolemA") then

	--Variables.
	local iHasDollMotivatorB = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N")

	--Already has the motivator.
	if(iHasDollMotivatorB == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #3144.[SOFTBLOCK] Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Massive blunt trauma.[SOFTBLOCK] Unit was thrown into the nearby wall at high velocity.[SOFTBLOCK] All systems suffered severe damage from the shock.[SOFTBLOCK] System offline was instantaneous.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: All remaining subsystems are severely damaged.[SOFTBLOCK] The authenticator chip has been removed.[SOFTBLOCK] An incision in the back of the head is the likely removal vector.[SOFTBLOCK] The incision was made some time after system offline took place.") ]])
		fnCutsceneBlocker()
	
	--Pick up the motivator.
	else
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #3144.[SOFTBLOCK] Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Massive blunt trauma.[SOFTBLOCK] Unit was thrown into the nearby wall at high velocity.[SOFTBLOCK] All systems suffered severe damage from the shock.[SOFTBLOCK] System offline was instantaneous.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Subsystem scan completed.[SOFTBLOCK] Left primary leg motivator is of acceptable condition.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: [SOUND|World|TakeItem]The left primary motivator has been extracted and added to your inventory.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: All remaining subsystems are severely damaged.[SOFTBLOCK] The authenticator chip has been removed.[SOFTBLOCK] An incision in the back of the head is the likely removal vector.[SOFTBLOCK] The incision was made an unknown amount of time after system offline took place.") ]])
		fnCutsceneBlocker()
	end

--Retired doll.
elseif(sObjectName == "GolemB") then

	--Variables.
	local iHasDollOcular = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N")

	--Already has the motivator.
	if(iHasDollOcular == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #500932.[SOFTBLOCK] Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Severe sonic damage.[SOFTBLOCK] Unit was struck by an extremely high-frequency sound wave and went into system standby as a defense mechanism as subsystems began to vibrate apart.[SOFTBLOCK] The sound wave continued to be applied until the CPU shattered.[SOFTBLOCK] The unit is permanently offline.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: The authenticator chip has been removed, likely by an incision in the back of the head.[SOFTBLOCK] Estimates indicate the incision was made shortly after the high-frequency waves damaged the other systems.") ]])
		fnCutsceneBlocker()
	
	--Pick up the motivator.
	else
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #500932.[SOFTBLOCK] Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Severe sonic damage.[SOFTBLOCK] Unit was struck by an extremely high-frequency sound wave and went into system standby as a defense mechanism as subsystems began to vibrate apart.[SOFTBLOCK] The sound wave continued to be applied until the CPU shattered.[SOFTBLOCK] The unit is permanently offline.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Subsystem scan completed.[SOFTBLOCK] Right ocular unit did not suffer damage from the high-frequency wave, and is of acceptable condition.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: [SOUND|World|TakeItem]The right ocular unit has been extracted and added to your inventory.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: The authenticator chip has been removed, likely by an incision in the back of the head.[SOFTBLOCK] Estimates indicate the incision was made shortly after the high-frequency waves damaged the other systems.") ]])
		fnCutsceneBlocker()
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end