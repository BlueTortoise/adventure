--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCryoG"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "RegulusTense")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
    AM_SetMapInfo("No Special Map", "Null", 1000, 850)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabLower6")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabLower0")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabLower1",   0,   10)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabLower2",  11,   20)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabLower3",  21,   30)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabLower5",  31,   50)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabLower4",  51, 1000)
    AM_SetProperty("Advanced Map Properties", 840.0, 725.0, 70.0 / 96.0, 70.0 / 96.0)
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

	--[Lights]
    AL_SetProperty("Activate Lights")
	
	--If the player already has the lantern, disable it.
	local iHasLantern = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N")
	if(iHasLantern == 1.0) then
		AL_SetProperty("Disable Light", "LanternLight")
	
	--Spawn an entity to represent the lantern.
	else
		TA_Create("LanternNPC")
			TA_SetProperty("Position", 15, 8)
			TA_SetProperty("Clipping Flag", false)
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", 0, p-1, "Root/Images/Sprites/Objects/Lantern")
				TA_SetProperty("Move Frame", 1, p-1, "Root/Images/Sprites/Objects/Lantern")
				TA_SetProperty("Move Frame", 2, p-1, "Root/Images/Sprites/Objects/Lantern")
				TA_SetProperty("Move Frame", 3, p-1, "Root/Images/Sprites/Objects/Lantern")
			end
			TA_SetProperty("Facing", 0)
			TA_SetProperty("Set Ignore Special Lights", true)
		DL_PopActiveObject()
	end

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	local iHasLantern = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N")
	if(iHasLantern == 0.0) then
		fnCutsceneTeleport("LanternNPC", 15.00, 8.4)
	end

end
