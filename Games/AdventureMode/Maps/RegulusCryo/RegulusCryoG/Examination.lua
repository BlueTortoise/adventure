--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit door.
if(sObjectName == "ToCryoC") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoC", "FORCEPOS:16.0x3.0x0")
	
--Exit door.
elseif(sObjectName == "ToContainmentA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoContainmentA", "FORCEPOS:20.5x21.0x0")

--Ladder.
elseif(sObjectName == "ToFabricationA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoToFabricationA", "FORCEPOS:8.0x30.0x0")

--Ladder.
elseif(sObjectName == "ToCommandA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoCommandA", "FORCEPOS:10.0x14.0x0")

--Light source. Pick it up if we don't already have a light.
elseif(sObjectName == "Lantern") then

	--Variables.
	local iHasLantern = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N")
	
	--Player does not have the lantern yet.
	if(iHasLantern == 0.0) then

		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 1.0)

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "*Picked up an electric lantern.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The lantern provides a mobile light source in dark areas.[SOFTBLOCK] Be wary, as enemies have no problem seeing you in the dark.") ]])
		fnCutsceneBlocker()
		
		--Activate the light. Remove the world object.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Player Light", 3600, 3600) ]])
		fnCutsceneInstruction([[ AL_SetProperty("Disable Light", "LanternLight") ]])
		fnCutsceneTeleport("LanternNPC", -100,-100)
		fnCutsceneBlocker()
	end

--Intercom.
elseif(sObjectName == "Intercom") then
	LM_ExecuteScript(gsRoot .. "CharacterDialogue/Intercom/Root.lua", "Hello")

--EmptyTube.
elseif(sObjectName == "EmptyTube") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: This research containment unit is currently empty.[SOFTBLOCK] The hard drive has been wiped of any useful data.") ]])
	fnCutsceneBlocker()

--ComputerRB.
elseif(sObjectName == "ComputerRB") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 29.[SOFTBLOCK] I find myself at a loss.[SOFTBLOCK] I am not sure how to continue my research down this avenue at the present time.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I have reviewed several days of videographs from the subject.[SOFTBLOCK] I wrote several programs to help myself make sense of the chaos I was seeing, and found that the programs were holding me back.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: There is a sort-of coherency to the presence of the shapes, but when compared using the program I wrote, it suggests a quantum randomness.[SOFTBLOCK] This cannot be correct.[SOFTBLOCK] I can personally predict the next shape with over seventy percent accuracy.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Unless it is my own CPU that is the cause, or perhaps an error in my program, I am left to assume that this chaotic garbage is random but not random.[SOFTBLOCK] This is not possible.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I have asked a slave unit to review the footage.[SOFTBLOCK] She refused.[SOFTBLOCK] I then ordered her to review the footage.[SOFTBLOCK] She refused no matter what level of punishment I doled out.[SOFTBLOCK] It seems I am alone.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: There is no course of action other than to continue to record videographs and hope I can discern a pattern.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()
	
--ComputerM.
elseif(sObjectName == "ComputerM") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 27.[SOFTBLOCK] Effectively immediately, all radio equipment is hereby contraband.[SOFTBLOCK] I am sick of these radio signals![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: At first I thought the radio signals may be related to the subject, but that was incorrect.[SOFTBLOCK] The signals predate the extraction of the orbs, so a delinquint unit must be the cause.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The sheer amount of random interference on the intercoms and slave headsets has proven aggravating.[SOFTBLOCK] All slave units have been ordered to disable their headsets.[SOFTBLOCK] They will have to receive verbal commands from now on.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I have sent my recommendations to central administration in the hopes that they will dispatch personnel to help find the cause.[SOFTBLOCK] I am a research unit, damn it![SOFTBLOCK] My function is to research phenomena, not police the slave units![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: There has been no response from central administration yet.[SOFTBLOCK] It is entirely possible they will ignore this request as they have the others.[SOFTBLOCK] There is nothing more I can do.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()
	
--ComputerLR.
elseif(sObjectName == "ComputerLR") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 36.[SOFTBLOCK] Three slave golems are unaccounted for.[SOFTBLOCK] They are not reporting in when summoned and have not been seen by the other units.[SOFTBLOCK] The security cameras are no help, as usual.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: We've been experiencing more power outages, as well.[SOFTBLOCK] Our maintenance unit is strained enough with the demands we put on our equipment, and now she has to fix the power grid.[SOFTBLOCK] Without more personnel, we will never catch up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: On a positive note, I believe I am near a breakthrough in the videographs of the subject's dreams.[SOFTBLOCK] I found myself staring at a live stream of the dreams for a few hours and suddenly realized I had guessed fifteen shapes correctly in a row.[SOFTBLOCK] Astounding![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: In spite of our mechanical failures, the cause of science marches on.[SOFTBLOCK] I am excited about the coming ramifications of my research.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()

--ComputerLL.
elseif(sObjectName == "ComputerLL") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 39.[SOFTBLOCK] It is not often I am humbled by something, but the day has come.[SOFTBLOCK] At no point was the subject dreaming of geometric shapes. This I am now certain of.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: What the subject was dreaming of was sixteen-dimensional math of a vibrating membrane.[SOFTBLOCK] Without proper calibrations, these would look like random quantum fluctuations on an electron, but after I reconfigured my CPU it all became clear.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: If this is correct, the subject's mental capability far outstrip that of even central administration.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Even more interesting was that, as I have now reconfigured my CPU, the pace of the shapes appearing has increased by about three times its original rate.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Due to the limited bandwidth on communications with central administation, I have withheld sending the videographs.[SOFTBLOCK] I am eager to compile my findings, but want to follow this trail to its end.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()

--ComputerBUR.
elseif(sObjectName == "ComputerBUR") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 41.[SOFTBLOCK] Han droemmer om sagn-omspundne [REDACTED] hvor de doede guder slumrer.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Hun er hans taleroer, og hun er en gave til de droemmende masser.[SOFTBLOCK] Hun vil vaagne, han vil vaagne naar den trettende time ringer, og dukkerne vil blive hans marionetter, da de allerede spiller ind i hans mangfoldige haender.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()

--ComputerBLR.
elseif(sObjectName == "ComputerBLR") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 42.[SOFTBLOCK] While I have no recollection of recording the previous log entry, it has my authorization stamp on it.[SOFTBLOCK] Perhaps it is a slave unit playing a prank?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: This garbage is infuriating.[SOFTBLOCK] It seems so close to language but is not any of the spoken languages on Pandemonium.[SOFTBLOCK] Yet, somehow, I can vaguely understand what it is saying.[SOFTBLOCK] My CPU has been working hard trying to decipher it.[SOFTBLOCK] I need a rest.[SOFTBLOCK] Signing off.") ]])
	fnCutsceneBlocker()
	
--ComputerBUL.
elseif(sObjectName == "ComputerBUL") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] done.[SOFTBLOCK] Playing most recent log entry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Research Log of Unit 609144, day 44.[SOFTBLOCK] There is certainly some aesthetic value to the videographs.[SOFTBLOCK] While pseudo-random, they are quite beautiful to observe.[SOFTBLOCK] I have also realized I have not seen a slave unit for the past three days.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: I called out but received no response.[SOFTBLOCK] The subject was moved to the containment area without my authorization, and as I went there I thought I saw a slave unit moving in the shadows.[SOFTBLOCK] There was no one there.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: What the slave units do is none of my concern.[SOFTBLOCK] The facility is quiet.[SOFTBLOCK] I am now standing near the containment unit that holds the subject.[SOFTBLOCK] She is cryogenically frozen within the unit, yet I can tell she is still dreaming of shapes.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The vitals monitor just spiked.[SOFTBLOCK] A sudden increase in brain activity.[SOFTBLOCK] The intercom spewed static so loud I could hear it from here.[SOFTBLOCK] The subject -[SOFTBLOCK][SOFTBLOCK][SOFTBLOCK] is waking up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: ...[SOFTBLOCK] Apologies, that appears to be the end of the entry.[SOFTBLOCK] Unit 609144 did not sign off on it.") ]])
	fnCutsceneBlocker()

--ComputerBLL.
elseif(sObjectName == "ComputerBLL") then

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scanning hard drive...[SOFTBLOCK] error.[SOFTBLOCK] The hard drive has been deliberately wiped of data.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Scans suggest that the drive received a short-range electromagnetic pulse directed at it.[SOFTBLOCK] The rest of the terminal was not affected.[SOFTBLOCK] This was a carefully directed blast.") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end