--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit door.
if(sObjectName == "ToCryoF") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoC", "FORCEPOS:16.0x18.0x0")

--Ladder.
elseif(sObjectName == "ToLowerCryoD") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoLowerD", "FORCEPOS:49.0x41.0x0")

--Computer suspended above the darkness.
elseif(sObjectName == "ComputerMid") then

	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    local iSCryoPowerCoreShutOff = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreShutOff", "N")

    --Core shut off.
    if(iSCryoPowerCoreShutOff == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Thermal power exchange units shut down.[SOFTBLOCK] Emergency power projected to last 70 hours at current usage rates.") ]])
		fnCutsceneBlocker()

	--Chris.
	elseif(iHasGolemForm == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Thermal power exchange units are operating at 15 percent capacity.[SOFTBLOCK] I recommend reporting this to a maintenace team as soon as possible.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] How do the power exchange units work?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Geological activity related to tidal action in the rock below the facility generates heat.[SOFTBLOCK] The thermal power exchange units harness this heat and use it to maintain the facility's temperature.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Excess heat is then extracted via picofilters to produce electricity that powers this facility.[SOFTBLOCK] That accounts for half of the thumping you are hearing now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] ...[SOFTBLOCK] half?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The source of the other half is unknown.[SOFTBLOCK] Please notify a maintenance unit as soon as possible.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[VOICE|ChrisMaleVoice] ...") ]])
		fnCutsceneBlocker()
	
	--Christine.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Thermal power exchange units are operating at 15 percent capacity.[SOFTBLOCK] I recommend reporting this to a maintenace team as soon as possible.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] How do the power exchange units work?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Geological activity related to tidal action in the rock below the facility generates heat.[SOFTBLOCK] The thermal power exchange units harness this heat and use it to maintain the facility's temperature.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Excess heat is then extracted via picofilters to produce electricity that powers this facility.[SOFTBLOCK] That accounts for half of the thumping you are hearing now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[SOFTBLOCK] half?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: The source of the other half is unknown.[SOFTBLOCK] Please notify a maintenance unit as soon as possible.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] PDU, I *am* a maintenance unit![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Please notify yourself as soon as possible.") ]])
		fnCutsceneBlocker()
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end