--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit door.
if(sObjectName == "Nope") then
	--AL_BeginTransitionTo("RegulusCryoF", "FORCEPOS:13.0x14.0x0")
	
--Computer suspended above the darkness.
elseif(sObjectName == "ComputerMid") then

	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")

	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: A notice has been left on this terminal.[SOFTBLOCK] Would you like me to read it?[BLOCK][CLEAR]") ]])
	if(iHasGolemForm == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]Christine:[VOICE|Christine] Go ahead.[BLOCK][CLEAR]") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|ChrisMaleVoice]Chris:[VOICE|ChrisMaleVoice] Go ahead.[BLOCK][CLEAR]") ]])
	end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Security Alert to all units::[SOFTBLOCK] A large number of unauthorized radio signals have been recorded in the area around the cryogenics facility.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: If you see a unit in possession of illegal radio equipment, report them to your supervisor immediately.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|PDU]PDU: Remember::[SOFTBLOCK] Safety is everyone's job.[SOFTBLOCK] This means you.") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end