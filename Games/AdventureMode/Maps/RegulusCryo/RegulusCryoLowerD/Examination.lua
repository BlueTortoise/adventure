--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit ladder.
if(sObjectName == "ToLowerCryoA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoLowerA", "FORCEPOS:77.0x41.0x0")
	
--Unpowered door.
elseif(sObjectName == "ToLowerCryoE") then

	--Variables.
	local iHasGolemForm     = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
	local iFixedLowerDDoor  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedLowerDDoor", "N")
	local iReceivedFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
	
	--If the door is already fixed:
	if(iFixedLowerDDoor == 1.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusCryoPowerCoreE", "FORCEPOS:14.0x25.0x0")

	--Door isn't fixed yet.
	else
		
		--Doesn't have golem form:
		if(iHasGolemForm == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris: [VOICE|Leader](The door doesn't open or even respond...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: This door is not receiving power.[SOFTBLOCK] The transmission circuit was likely cut.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris: [VOICE|Leader]Can you fix it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Unfortunately that would require more than a simple circuit bridging.[SOFTBLOCK] A repair unit would need to attend to the issue.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris: [VOICE|Leader](Guess I'm not going this way, then...)") ]])
		
		--Has golem form, has not been to Regulus City yet:
		elseif(iHasGolemForm == 1.0 and iReceivedFunction == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "771852: [VOICE|Leader](This door doesn't appear on the local area map, and is not receiving power.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "771852: [VOICE|Leader](It is therefore not related to my programming.[SOFTBLOCK] I should proceed to Regulus City to receive a function assignment.)") ]])
		
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: [VOICE|Leader](This door isn't on the local area map, and isn't receiving power.)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: [VOICE|Leader]PDU, short-range ultraviolet scan.[SOFTBLOCK] What's wrong with this door?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: This door is not receiving power.[SOFTBLOCK] The transmission circuit was likely cut.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: [VOICE|Leader]Show me a schematic of the circuits.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: [VOICE|Leader]...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: [VOICE|Leader]Crud, looks like I can't fix it from this side.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55: [VOICE|2855]You don't have to repair everything you come across.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: [VOICE|Leader]Yes I do, I'm a repair unit![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: [VOICE|Leader]But more importantly, there might be something in there we can use.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55: [VOICE|2855]The access way isn't on the schematics.[SOFTBLOCK] It's probably disused and unimportant.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: [VOICE|Leader]Telling me I can't have it just makes me want it more![SOFTBLOCK] Argh![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: [VOICE|Leader]We'll have to find a way around, and it likely won't be in the Cryogenics Facility...") ]])
		
		end
	end

--Retired doll.
elseif(sObjectName == "DollA") then

	--Variable.
	local iHasPDUCryoRedCard = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N")

	--Common.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Command Unit #383099.[SOFTBLOCK] Status::[SOFTBLOCK] Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Power cycling unit ruptured leading to a slow loss of power.[SOFTBLOCK] Subject gradually lost function, presumably over the course of several hours, after receiving a penetrating blow in the torso.[BLOCK][CLEAR]") ]])

	--Player already has the keycard:
	if(iHasPDUCryoRedCard == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Subject's components have suffered acidic damage due to the cycler rupturing.[SOFTBLOCK] Ocular and motivator units are degraded severely.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Subject's authenticator chip appears to have been forcibly removed after retirement.[SOFTBLOCK] An incision in the back of the head post-dates the cycling unit's rupture by several days.") ]])
	else
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 4.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Subject's components have suffered acidic damage due to the cycler rupturing.[SOFTBLOCK] Ocular and motivator units are degraded severely.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Subject's authenticator chip appears to have been forcibly removed after retirement.[SOFTBLOCK] An incision in the back of the head post-dates the cycling unit's rupture by several days.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: [SOUND|World|Keycard]The command unit was clutching a security-red keycard.[SOFTBLOCK] It has been added to this PDU's database.") ]])
	end
	fnCutsceneBlocker()
		
		
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end