--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToRegulusCryoSouthG") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusCryoSouthG", "FORCEPOS:32.5x10.0x0")
    
--[Examinables]
elseif(sObjectName == "Tube") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A defragmentation chamber, it is currently not receiving power.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Terminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The terminal was wired up but isn't connected to the defragmentation chamber and has no logs on it.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "RVD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The RVD is playing its test pattern.[SOFTBLOCK] It is not receiving any broadcast data.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Scanner") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A radio-relay node to be attached to the lab's structures to boost network access.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Computer") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A portable computer.[SOFTBLOCK] There are no entries on it discussing the strange fleshy growth here, it must have been left here before that happened.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Boxes") then

    local iSCryoPowerABoxes = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerABoxes", "N")
    if(iSCryoPowerABoxes == 0.0) then
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerABoxes", "N", 1.0)
        LM_ExecuteScript(gsItemListing, "Assorted Parts")
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Mechanical parts to be used in constructing this laboratory room.[SOFTBLOCK] Mostly electrical wiring and socket connectors.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There are some parts we could use here...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|TakeItem](Got Assorted Parts x 1)") ]])
        fnCutsceneBlocker()
    
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Mechanical parts to be used in constructing this laboratory room.[SOFTBLOCK] Mostly electrical wiring and socket connectors.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "Furniture") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Furniture to be used in this area when it was finished construction.)") ]])
    fnCutsceneBlocker()
    
--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end