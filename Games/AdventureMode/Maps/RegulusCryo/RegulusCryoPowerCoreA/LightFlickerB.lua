--[Light Flickering Script - Light B]
--Parallel script that runs in this room to periodically flicker the lights on and off.
-- The light that flickers is named in the script path.

--First, roll how many ticks until the light flickers.
local iRollTicks = 160 + LM_GetRandomNumber(0, 300)

--Next, roll how long it flickers for. Each flicker is 4 ticks and several flickers occur.
local iFlickersTotal = LM_GetRandomNumber(2, 4)

--Now set events.
fnCutsceneWait(iRollTicks)
fnCutsceneBlocker()
for i = 1, iFlickersTotal, 1 do
    fnCutsceneInstruction([[ AL_SetProperty("Disable Light", "LampLightB") ]])
    fnCutsceneWait(3)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LampLightB") ]])
    fnCutsceneWait(3)
    fnCutsceneBlocker()
end

--The script then loops for another random flicker.