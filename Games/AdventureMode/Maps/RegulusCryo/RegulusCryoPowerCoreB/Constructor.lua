--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCryoPowerCoreB"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iSCryoPowerCoreShutOff = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreShutOff", "N")
    if(iSCryoPowerCoreShutOff == 0.0) then
        AL_SetProperty("Music", "PowerCore")
    else
        AL_SetProperty("Music", "Null")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCryoSouth")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

	--[Lights]
    AL_SetProperty("Activate Lights")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    
    --[Dislocations]
    --Move these blocks of tiles around for the parallel scripts.
    AL_SetProperty("Add Dislocation", "North", "SpecialFloorA", 9,  2, 3, 4, 28*gciSizePerTile,  2*gciSizePerTile)
    AL_SetProperty("Add Dislocation", "South", "SpecialFloorB", 9, 13, 3, 4, 28*gciSizePerTile, 13*gciSizePerTile)
    
    --[Parallel Scripts]
    Cutscene_HandleParallel("Remove", "Light Flicker A")
    Cutscene_HandleParallel("Remove", "Light Flicker B")
    Cutscene_HandleParallel("Remove", "Light Flicker D")
    local iSCryoPowerCoreShutOff = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreShutOff", "N")
    if(iSCryoPowerCoreShutOff == 0.0) then
        Cutscene_HandleParallel("Create", "Surge North", gsRoot .. "Maps/RegulusCryo/RegulusCryoPowerCoreB/SurgeNorth.lua")
        Cutscene_HandleParallel("Create", "Surge South", gsRoot .. "Maps/RegulusCryo/RegulusCryoPowerCoreB/SurgeSouth.lua")
    end
end
