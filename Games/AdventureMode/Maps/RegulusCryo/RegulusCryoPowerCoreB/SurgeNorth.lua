--[Surge - North]
--Parallel script that runs in this room to cause the... meat... to periodically surge as something
-- is pumped through it.

--The pump occurs every 180 ticks.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Surge the tiles.
local iStartPosX = 27
local iEndPostX = 12
for i = iStartPosX, iEndPostX, -1 do
    local sString = "AL_SetProperty(\"Modify Dislocation\", \"North\", " .. i*gciSizePerTile .. ", 31)"
    fnCutsceneInstruction(sString)
    fnCutsceneWait(4)
    fnCutsceneBlocker()
end
