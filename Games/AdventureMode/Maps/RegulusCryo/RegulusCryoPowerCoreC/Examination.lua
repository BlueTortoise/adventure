--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToRegulusCryoPowerCoreE") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoPowerCoreE", "FORCEPOS:5.0x7.0x0")
    
elseif(sObjectName == "ToRegulusCryoPowerCoreD") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoPowerCoreD", "FORCEPOS:17.0x11.0x0")

--[Examinables]
elseif(sObjectName == "BigCrate") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The crate's label indicates it contains replacement filters for the core, as they slowly corrode over the course of several years.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Crates") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Spare circuity for the computer equipment here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Furniture") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Furniture storage, presumably destined for the adminstrator's quarters.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Shelf") then
    local iSCryoPowerCShelf = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCShelf", "N")
    if(iSCryoPowerCShelf == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCShelf", "N", 1.0)
        LM_ExecuteScript(gsItemListing, "Bent Tools")
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There are some bent tools on the shelf we could salvage...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|TakeItem](Got Bent Tools x 1)") ]])
        fnCutsceneBlocker()
    
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's nothing else of use on the shelf.)") ]])
        fnCutsceneBlocker()
    
    end
elseif(sObjectName == "Pod") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A defragmentation pod meant to be used by the administrator here.[SOFTBLOCK] Construction wasn't completed and it was never used.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Terminals") then

    --Variables.
    local iSCryoPowerCoreShutOff = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreShutOff", "N")
    if(iSCryoPowerCoreShutOff == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] (Disable the power core permanently?)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"DisableCore\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
    
    --Core already disabled.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] (The power core is disabled and the thing is dead.)") ]])
        fnCutsceneBlocker()
    end
    

--[Other]
elseif(sObjectName == "DisableCore") then
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreShutOff", "N", 1.0)
    
    --Move the camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (8.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Turn the core off.
    fnCutsceneLayerDisabled("ReactorA", true)
    fnCutsceneLayerDisabled("ReactorB", false)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("ReactorB", true)
    fnCutsceneLayerDisabled("ReactorC", false)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
    fnCutsceneLayerDisabled("ReactorC", true)
    fnCutsceneLayerDisabled("ReactorD", false)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("ReactorD", true)
    fnCutsceneLayerDisabled("ReactorE", false)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Change the light colors.
    local fChangePerTick = 1.0 / 20.0
    for i = 1, 20, 1 do
        local fMod = 1.0 - (fChangePerTick * (i-1))
        local sString = "AL_SetProperty(\"Modify Light Color\", \"LampLightA\", 1, " .. fMod .. ", " .. fMod .. ", 1)"
        fnCutsceneInstruction(sString)
        sString = "AL_SetProperty(\"Modify Light Color\", \"LampLightB\", 1, " .. fMod .. ", " .. fMod .. ", 1)"
        fnCutsceneInstruction(sString)
        sString = "AL_SetProperty(\"Modify Light Color\", \"LampLightC\", 1, " .. fMod .. ", " .. fMod .. ", 1)"
        fnCutsceneInstruction(sString)
        sString = "AL_SetProperty(\"Modify Light Color\", \"LampLightD\", 1, " .. fMod .. ", " .. fMod .. ", 1)"
        fnCutsceneInstruction(sString)
        fnCutsceneWait(6)
        fnCutsceneBlocker()
        
    end
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] Attention all personnel.[SOFTBLOCK] Core deactivated, emergency power only.") ]])
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Variable.
    local iSCryoBatteryPickups = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This was the correct decision.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I know, but it still hurts.[BLOCK][CLEAR]") ]])
    if(iSCryoBatteryPickups < 5.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Come on, that battery we need must be around here somewhere.") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's nothing left to do here, 55.[SOFTBLOCK] Come on.") ]])
    end
    fnCutsceneBlocker()
    
elseif(sObjectName == "Cancel") then
	WD_SetProperty("Hide")
    
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end