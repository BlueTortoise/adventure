--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Sample object.
if(sObjectName == "Trigger") then

    --Repeat check.
    local iSCryoPowerCoreScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreScene", "N")
    if(iSCryoPowerCoreScene == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreScene", "N", 1.0)
    
    --Variables.
	local iCutscene50       = VM_GetVar("Root/Variables/Chapter5/Scenes/iCutscene50", "N")
	local iSawFirstEDGScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N")
    
    --Merge party for the scene.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 17.25, 13.50)
    fnCutsceneMove("55", 16.35, 13.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 25.25, 13.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("55", 24.25, 13.50)
    fnCutsceneFace("55", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera moves.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (8.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(185)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55...[SOFTBLOCK] What is this?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The geothermal exchange core recently activated to power the Cryogenics facility.[SOFTBLOCK] The core extracts heat from the subsurface of Regulus and conducts it here with a series of superconducting rods.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Heated air with an eighty-percent nitrogen content then cycles through a series of pico-filters which convert the heat energy into electrical charge.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] An unidentified organic material seems to be using the core's waste heat.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] No, it's more than that.[SOFTBLOCK] The core is...[SOFTBLOCK] part of it, now.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] The whole facility is now slowly becoming its blood vessels.[SOFTBLOCK] It's alive.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's -[SOFTBLOCK] alive![SOFTBLOCK] Wonderful![SOFTBLOCK] Spectacular![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Organic material of the type we have seen is not native to Regulus, Christine.[SOFTBLOCK] This should not be.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But it is![SOFTBLOCK] Do you not understand the wonder of what you are seeing with your material eyes?[SOFTBLOCK] Peek beyond the veil![BLOCK][CLEAR]") ]])
    
    --Christine has not encountered Vivify or reached the end of the mines:
    if(iCutscene50 == 0.0 and iSawFirstEDGScene == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Explain your statement, right now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] S-[SOFTBLOCK]sorry![SOFTBLOCK] I'm not really sure what it means, either.[SOFTBLOCK] I look at that thing and thoughts pop into my head...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Memories, memories of things that have yet to happen but do happen.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Why do I have these memories?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Like a section of a memory drive with unidentified encrypted data?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yeah...[SOFTBLOCK] Why?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It was mentioned on the logs stored in this facility that I found when I woke up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Most of the drives had been purged but several audio files recorded this phenomena shortly before the battle broke out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I derived my plan to smuggle your memory files from that pattern.[SOFTBLOCK] I now have a better idea of its origin.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But...[SOFTBLOCK] this is different, so different.[SOFTBLOCK] This is unlike what came before it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We -[SOFTBLOCK] we need to shut this core down and kill this thing![SOFTBLOCK] Right now![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] N-[SOFTBLOCK]no, wait![SOFTBLOCK] We can't do that![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But it's dangerous![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you arguing with me, or yourself?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] 55, I don't think I can trust myself on this.[SOFTBLOCK] I simply can't.[SOFTBLOCK] I don't even know which thoughts are mine right now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Listen, whatever you decide to do, I'll respect it unconditionally.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I appreciate the sentiment.[SOFTBLOCK] What if I decide to shut down the core?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You have no right to murder this creature, whatever it is.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It has a right to exist, same as we do.[SOFTBLOCK] Even if it's dangerous, you don't have that right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And if I leave the core as it is?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] It will consume us all...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This does not seem like a difficult decision.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Because you don't view killing as an abominable crime![SOFTBLOCK] But you should, you god damn monster![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Cry] N-[SOFTBLOCK]no![SOFTBLOCK] I'm sorry, 55, I didn't mean it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] It is all right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will make my decision in a moment.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (I can shut down the core by checking the terminals just south of here.[SOFTBLOCK] It should be easy to override the security.)") ]])
        
    --Christine met Vivify, putting this into context:
    elseif(iSawFirstEDGScene == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Explain your statement, right now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] S-[SOFTBLOCK]sorry![SOFTBLOCK] I'm not really sure what it means, either.[SOFTBLOCK] I look at that thing and thoughts pop into my head...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Memories, memories of things that have yet to happen but do happen.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Vivify...[SOFTBLOCK] It all has to do with Project Vivify...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Did she put these here?[SOFTBLOCK] Did I absorb them?[SOFTBLOCK] Were they always there?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Like a section of a memory drive with unidentified encrypted data?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yeah, it kind of is like that. Why?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It was mentioned on the logs stored in this facility that I found when I woke up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Most of the drives had been purged but several audio files recorded this phenomena shortly before the battle broke out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I derived my plan to smuggle your memory files from that pattern.[SOFTBLOCK] I now have a better idea of its origin.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But...[SOFTBLOCK] this is different.[SOFTBLOCK] I don't think Vivify even knows about this...[SOFTBLOCK] thing...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We -[SOFTBLOCK] we need to shut this core down and kill this thing![SOFTBLOCK] Right now![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] N-[SOFTBLOCK]no, wait![SOFTBLOCK] We can't do that![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But it's dangerous![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you arguing with me, or yourself?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] 55, I don't think I can trust myself on this.[SOFTBLOCK] I simply can't.[SOFTBLOCK] I don't even know which thoughts are mine right now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Listen, whatever you decide to do, I'll respect it unconditionally.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I appreciate the sentiment.[SOFTBLOCK] What if I decide to shut down the core?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You have no right to murder this creature, whatever it is.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It has a right to exist, same as we do.[SOFTBLOCK] Even if it's dangerous, you don't have that right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And if I leave the core as it is?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] It will consume us all...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] And she will cry...[SOFTBLOCK] Does it even matter what we do?[SOFTBLOCK] Can I even change anything for her?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Who?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] My master, the one we call Project Vivify.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This does not seem like a difficult decision.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Because you don't view killing as an abominable crime![SOFTBLOCK] But you should, you god damn monster![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Cry] N-[SOFTBLOCK]no![SOFTBLOCK] I'm sorry, 55, I didn't mean it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] It is all right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will make my decision in a moment.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (I can shut down the core by checking the terminals just south of here.[SOFTBLOCK] It should be easy to override the security.)") ]])
    
    --Christine reached the base of the mines but has not met Vivify:
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Explain your statement, right now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] S-[SOFTBLOCK]sorry![SOFTBLOCK] I'm not really sure what it means, either.[SOFTBLOCK] I look at that thing and thoughts pop into my head...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Memories, memories of things that have yet to happen but do happen.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just like in the Tellurium Mines...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Did 609144 put these here?[SOFTBLOCK] Did I absorb them?[SOFTBLOCK] Were they always there?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Like a section of a memory drive with unidentified encrypted data?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yeah, it kind of is like that. Why?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It was mentioned on the logs stored in this facility that I found when I woke up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Most of the drives had been purged but several audio files recorded this phenomena shortly before the battle broke out.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I derived my plan to smuggle your memory files from that pattern.[SOFTBLOCK] I now have a better idea of its origin.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But...[SOFTBLOCK] it was dead before.[SOFTBLOCK] This -[SOFTBLOCK] this is alive...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We -[SOFTBLOCK] we need to shut this core down and kill this thing![SOFTBLOCK] Right now![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] N-[SOFTBLOCK]no, wait![SOFTBLOCK] We can't do that![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But it's dangerous![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you arguing with me, or yourself?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] 55, I don't think I can trust myself on this.[SOFTBLOCK] I simply can't.[SOFTBLOCK] I don't even know which thoughts are mine right now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Listen, whatever you decide to do, I'll respect it unconditionally.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I appreciate the sentiment.[SOFTBLOCK] What if I decide to shut down the core?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You have no right to murder this creature, whatever it is.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It has a right to exist, same as we do.[SOFTBLOCK] Even if it's dangerous, you don't have that right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And if I leave the core as it is?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] It will consume us all...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This does not seem like a difficult decision.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Because you don't view killing as an abominable crime![SOFTBLOCK] But you should, you god damn monster![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Cry] N-[SOFTBLOCK]no![SOFTBLOCK] I'm sorry, 55, I didn't mean it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] It is all right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will make my decision in a moment.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (I can shut down the core by checking the terminals just south of here.[SOFTBLOCK] It should be easy to override the security.)") ]])
    end
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
