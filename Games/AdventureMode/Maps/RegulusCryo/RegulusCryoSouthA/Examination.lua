--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToRegulusCryoA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusCryoA", "FORCEPOS:4.5x4s.0x0")
    
--[Examinables]
elseif(sObjectName == "FakeDoor") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This isn't a door, it's actually just a door frame.[SOFTBLOCK] This area is still under construction.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Battery") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A mobile power supply casing.[SOFTBLOCK] There's no actual power storage capability, this is just a spare part.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A terminal that was never installed.[SOFTBLOCK] The hard drive is completely blank.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BrownCrates") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Assorted miscellaneous electrical parts.[SOFTBLOCK] Spare chipsets, wiring, and a circuit board or two.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WhiteCrates") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The white crates contain waste rock pending disposal offsite.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Temporary construction storage.[SOFTBLOCK] Please do not store irradiated material here!')") ]])
    fnCutsceneBlocker()
    
--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end