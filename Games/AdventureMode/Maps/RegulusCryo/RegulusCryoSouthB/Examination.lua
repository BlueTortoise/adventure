--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToRegulusCryoSouthCE") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoSouthC", "FORCEPOS:28.0x13.0x0")
    
elseif(sObjectName == "ToRegulusCryoSouthCW") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoSouthC", "FORCEPOS:10.0x28.0x0")
    
elseif(sObjectName == "ToRegulusCryoSouthD") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoSouthD", "FORCEPOS:25.0x9.0x0")

--[Examinables]
elseif(sObjectName == "Sensor") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A radio-data recorder.[SOFTBLOCK] It does not have a power source and presumably was meant to be installed in the nearby construction site.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('ATTENTION::[SOFTBLOCK] ACTIVE CONSTRUCTION SITE.[SOFTBLOCK] PLEASE CONSULT THE LORD GOLEM FOR SAFETY INSTRUCTIONS IF YOU CANNOT ACCESS THE LOCAL NETWORK.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Probably a notice for anyone with damaged radio receivers.[SOFTBLOCK] Christine's safety tip?[SOFTBLOCK] Get those repaired ASAP!)") ]])
    fnCutsceneBlocker()
    
--[Other]
elseif(sObjectName == "UnpoweredDoor") then

    --Variables.
    local iSCryoRepoweredDoor  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoRepoweredDoor", "N")
    local iSCryoBatteryPickups = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N")
    
    --Fixed the door:
    if(iSCryoRepoweredDoor == 1.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_SetProperty("Open Door", "Door")

    --Door is unpowered.
    else
    
        --Varying battery states.
        if(iSCryoBatteryPickups == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[SOFTBLOCK] I'll need to find five partially-charged batteries to do that.)") ]])
            fnCutsceneBlocker()
        elseif(iSCryoBatteryPickups == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[SOFTBLOCK] I'll need to find four more partially-charged batteries to do that.)") ]])
            fnCutsceneBlocker()
        elseif(iSCryoBatteryPickups == 2.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[SOFTBLOCK] I'll need to find three more partially-charged batteries to do that.)") ]])
            fnCutsceneBlocker()
        elseif(iSCryoBatteryPickups == 3.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[SOFTBLOCK] I'll need to find two more partially-charged batteries to do that.)") ]])
            fnCutsceneBlocker()
        elseif(iSCryoBatteryPickups == 4.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[SOFTBLOCK] I'll need to find one more partially-charged batteries to do that.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There weren't enough batteries at the construction site, so another one must be somewhere nearby.") ]])
            fnCutsceneBlocker()
        
        --Fix the door scene:
        else
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoRepoweredDoor", "N", 1.0)
            
            --Variables.
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
            
            --Merge.
            fnCutsceneMergeParty()
            fnCutsceneBlocker()
            
            --Spawn NPCs.
            TA_Create("Ravital")
                TA_SetProperty("Position", -10, -10)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
            DL_PopActiveObject()
            TA_Create("CrowbarChan")
                TA_SetProperty("Position", 33, 10)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                for i = 1, 8, 1 do
                    TA_SetProperty("Move Frame", i-1, 0, "Root/Images/Sprites/CrowbarChan/0")
                    TA_SetProperty("Move Frame", i-1, 1, "Root/Images/Sprites/CrowbarChan/1")
                    TA_SetProperty("Move Frame", i-1, 2, "Root/Images/Sprites/CrowbarChan/2")
                    TA_SetProperty("Move Frame", i-1, 3, "Root/Images/Sprites/CrowbarChan/1")
                end
                TA_SetProperty("Auto Animates", true)
            DL_PopActiveObject()
            
            --Movement.
            fnCutsceneMove("Christine", 35.25, 15.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("55", 36.25, 15.50)
            fnCutsceneFace("55", -1, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Open the door.
            fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door") ]])
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneLayerDisabled("SpecialBlackout", true)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Got it![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Well done, Unit 771852.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thank you for your praise, Command Unit.[SOFTBLOCK] Now let's -") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("CrowbarChan", 33.25, 11.50)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChan", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Go get our eyes checked...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My diagnostics are not detecting any errors.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Ravital![SOFTBLOCK] Ravital, exit system standby, we're saved!") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneLayerDisabled("GolemLower", true)
            fnCutsceneLayerDisabled("GolemUpper", true)
            fnCutsceneTeleport("Ravital", 35.25, 10.50)
            fnCutsceneFace("Ravital", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            fnCutsceneFace("Ravital", 1, 0)
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            fnCutsceneFace("Ravital", -1, 0)
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            fnCutsceneFace("Ravital", 0, 1)
            fnCutsceneWait(45)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChan", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Diagnostics...[SOFTBLOCK] Power core at forty percent...[SOFTBLOCK] CC, what happened?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Some very heroic friends are here to help![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That much is definitely true.[SOFTBLOCK] We were searching the area when I noticed your core cycle on a local area scan.[SOFTBLOCK] Are you all right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] ...[SOFTBLOCK] Who are you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 771852, Maintenance and Repair, Sector 96.[BLOCK][CLEAR]") ]])
            if(sChristineForm == "Human") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] But we're standing in a vacuum.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That'd be the result of a bug.[SOFTBLOCK] Report it to Salty and don't worry about it.[BLOCK][CLEAR]") ]])
            elseif(sChristineForm == "Golem") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] 771852?[SOFTBLOCK] You must be a new unit...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Not that it matters.[BLOCK][CLEAR]") ]])
            elseif(sChristineForm == "LatexDrone") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] (Did something happen to her inhibitor chip?[SOFTBLOCK] Maybe Command Unit 2855 likes them that way?)[BLOCK][CLEAR]") ]])
            elseif(sChristineForm == "SteamDroid") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Uhhhh...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's a long story.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] I'll bet it is.[BLOCK][CLEAR]") ]])
            elseif(sChristineForm == "Electrosprite") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] So what species are you, if I may ask?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're called electrosprites![SOFTBLOCK] We're -[SOFTBLOCK] electricity![SOFTBLOCK] I think![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Wow, you'd be pretty useful around a construction site...[BLOCK][CLEAR]") ]])
            elseif(sChristineForm == "Eldritch") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] So what species are you, if I may ask?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A dreamer, is what I will say.[SOFTBLOCK] It's rather complicated.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] It looks that way, but given what I heard they were researching...[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And I am - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] I already know who you are.[SOFTBLOCK] So...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Do I get any last words?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] After we went through the trouble of rescuing you?[SOFTBLOCK] We're not going to retire you![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is correct.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] You have a change of heart, 2855?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I wiped all of my memory drives, to be exact.[SOFTBLOCK] I do not have a heart.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] You can say that again -[SOFTBLOCK] wait, what?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] You wiped your memory drives?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] If we have met in the past, I do not remember it.[SOFTBLOCK] I harbor no grudges against you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We've gone maverick, actually.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] She's not gonna retire us, nyuuuu![SOFTBLOCK] Yaaay![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And who are you, exactly?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Oh, you've never heard of autonomous toolsets?[SOFTBLOCK] This is crowbar-chan.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Hi Christine![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I didn't tell you my secondary designation yet...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] ...............[SOFTBLOCK] *Whoops!*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] So you're a self-motile automated construction tool?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] .[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK] Yes?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] And therefore a robot?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] .[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK] Yes, absolutely![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Capital![SOFTBLOCK] Sorry about the misunderstanding, but I'm a repair unit and your schematics aren't on file.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] She's a prototype.[SOFTBLOCK] We're testing her out.[SOFTBLOCK] Records tend to get lost, you know how it is.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Oh, absolutely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So, Ravital, what exactly happened here?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] I'm not going to hide it.[SOFTBLOCK] There was a revolt.[SOFTBLOCK] The other workers made guns and took over the main building, demanding better treatment and a say in their working conditions.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] I'm not stupid, so I just kept doing my job out here.[SOFTBLOCK] I felt the breaching charges go off as they vibrated through the rock.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Then the power got cut and I couldn't get the door open again.[SOFTBLOCK] I was trapped.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] She went into system standby to conserve power until someone showed up to let us out.[SOFTBLOCK] We've been in here since then.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] How did you pass the time, Crowbar-chan?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] I wrote some stories on my hard drive.[SOFTBLOCK] Want to read them?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Later.[SOFTBLOCK] I'm just glad there were other survivors of the...[SOFTBLOCK] Cryogenics Incident...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In a way, we too are survivors like you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Hrmpf.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] So you say, but I don't trust you.[SOFTBLOCK] Command Unit 2855...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] They just got done saving us![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] It could be a trick.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Listen, 771852, I appreciate it.[SOFTBLOCK] But from what you just said...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Everyone else is retired...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] Yeah.[SOFTBLOCK] Yeah.[SOFTBLOCK] I'm going to head back to Regulus City.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] I know places we can hide out.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I can help you.[SOFTBLOCK] I know of gaps in the security web.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] I don't need your help.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] But...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] If you're the real deal, 771852, maybe I'll contact you later.[SOFTBLOCK] If we're all mavericks now, we have to stick together.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Ravital:[E|Neutral] But I have a lot of thinking to do.[SOFTBLOCK] So please, leave us to it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "CrowbarChan:[E|Neutral] Nyuuuu...[SOFTBLOCK] Thanks, Christine...") ]])
            fnCutsceneBlocker()
            
            --Fold the party.
            fnAutoFoldParty()
            fnCutsceneBlocker()
            
        end
    end

--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end