--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToRegulusCryoSouthB") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoSouthB", "FORCEPOS:14.0x12.0x0")
    
--[Examinables]
elseif(sObjectName == "BrownCrates") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A crate full of spare drilling equipment, mostly drill bits and replacement circuits.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WhiteCrates") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks to be waste rock for disposal.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Remember, brown is spare parts, white is waste rock for disposal.[SOFTBLOCK] The drone units *will not check for you*.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Temporary drill recharge station.')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The cable here suggests a battery was installed, but it was moved.[SOFTBLOCK] It can't be far.)") ]])
    fnCutsceneBlocker()
    
--[Other]

--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end