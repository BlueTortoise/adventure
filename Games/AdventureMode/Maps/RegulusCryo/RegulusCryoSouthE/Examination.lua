--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Examinables]
if(sObjectName == "Ladder") then
    local iSCryoDeployedLadder = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoDeployedLadder", "N")
    if(iSCryoDeployedLadder == 1.0) then return end
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like there's a safety access ladder up there.[SOFTBLOCK] I could deploy it if I could press the release switch.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "LadderDeploy") then
    local iSCryoDeployedLadder = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoDeployedLadder", "N")
    if(iSCryoDeployedLadder == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoDeployedLadder", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Let's see here...)") ]])
    fnCutsceneBlocker()
    
    --Sound and timing.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Change layers and collisions.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|RemoteDoor") ]])
    fnCutsceneLayerDisabled("LadderUp", true)
    fnCutsceneLayerDisabled("LadderDown", false)
    AL_SetProperty("Set Collision", 41, 21, 0, 15)
    AL_SetProperty("Set Collision", 41, 22, 0, 15)
    fnCutsceneBlocker()

    
--[Other]

--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end