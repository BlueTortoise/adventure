--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Examinables]
if(sObjectName == "Fabricator") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This fabrication bench is in pristine condition...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] 55![SOFTBLOCK] What if we had found this when I was still an organic?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] These benches are not connected to a power source and this area is still exposed to a vacuum.[SOFTBLOCK] They did not appear on the area schematics.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Probably because the area is under construction?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Likely.[SOFTBLOCK] It seems we could have used them if we knew of them and could connect a power source.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Oh what a shame that you had to upgrade me...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] It was necessary, and I would likely have done so at a later date.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I was joking, 55.[SOFTBLOCK] I love being a robot.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Ah, facetiousness.[SOFTBLOCK] I understand, let us proceed.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Crate") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Spare drill bits in bad condition.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Shelf") then
    
    local iSCryoEShelf = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoEShelf", "N")
    if(iSCryoEShelf == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoEShelf", "N", 1.0)
        LM_ExecuteScript(gsItemListing, "Recycleable Junk")
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's a bit of scrap here we could probably use...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|TakeItem](Got Recycleable Junk x 1)") ]])
        fnCutsceneBlocker()
    
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's nothing else of value here.)") ]])
        fnCutsceneBlocker()
    
    end
    
elseif(sObjectName == "Terminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('BIOS v7.55.30')[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like this terminal was installed but hasn't been programmed to do anything yet.[SOFTBLOCK] There's nothing useful on the hard drive.)") ]])
    fnCutsceneBlocker()

--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end