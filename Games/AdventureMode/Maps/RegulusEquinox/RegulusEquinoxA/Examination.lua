--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ToExteriorWC") then
	
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Christine's form must be vac-safe or this message plays. Forward-proofed for New Chapter +!
	if(sChristineForm == "Human" or sChristineForm == "Raiju") then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](I wouldn't last more than seven seconds before passing out if I went onto the surface as an organic...)") ]])
		fnCutsceneBlocker()
		
	--Valid. Exit.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusExteriorWC", "FORCEPOS:11.0x10.0x0")
	end
	
--Exit
elseif(sObjectName == "ToEquinoxB") then

	--Can't use this door unless the area boss is defeated.
	local iFightGolemA = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemA", "N")
	if(iFightGolemA == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Door:[SOUND|World|AutoDoorFail] Access denied.[SOFTBLOCK] Unauthorized user.[SOFTBLOCK] Please update passcodes at reception terminal.") ]])
		fnCutsceneBlocker()

	--Open the door.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusEquinoxB", "FORCEPOS:18.5x12.0x0")
	end
	
--Exit
elseif(sObjectName == "ToEquinoxD") then

	--Can't use this door unless the area boss is defeated.
	local iFightGolemA = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemA", "N")
	if(iFightGolemA == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Door:[SOUND|World|AutoDoorFail] Access denied.[SOFTBLOCK] Unauthorized user.[SOFTBLOCK] Please update passcodes at reception terminal.") ]])
		fnCutsceneBlocker()

	--Open the door.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusEquinoxD", "FORCEPOS:19.5x5.0x0")
	end

--[Objects]
elseif(sObjectName == "GolemA") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #186330.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit was electrocuted using a loose wire from the walls.[SOFTBLOCK] Massive internal damage resulted, forcing system offline.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemB") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #188924.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit received numerous close range blunt blows until its power core ruptured.[SOFTBLOCK] Unit went offline shortly thereafter.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DollA") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #9310.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit was shot in the ocular unit at close range and went offline moments later.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemBoss") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Unit autopsy unnecessary.[SOFTBLOCK] Unit retired by Command Unit 2855 and Lord Unit 771852.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Cause of retirement was the usage of an Electrospear and Pulse Diffractor.[SOFTBLOCK] Chassis damage is as expected.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Terminal") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](All units, First Drummer protocol is now in effect. All units to adjust, effective immediately.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end