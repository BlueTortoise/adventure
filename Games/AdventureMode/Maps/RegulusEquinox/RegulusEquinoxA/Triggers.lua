--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Opening Cutscene.
if(sObjectName == "WelcomeToEquinox") then

	--Variables.
	local iSawEquinoxOpening = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxOpening", "N")
	
	--Play the scene.
	if(iSawEquinoxOpening == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxOpening", "N", 1.0)
		
		--Move Christine and 55 up.
		fnCutsceneMove("Christine", 12.25, 10.50)
		fnCutsceneMove("55", 13.25, 10.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] N-[SOFTBLOCK]no. P-[SOFTBLOCK]please no.[SOFTBLOCK] No![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Not again![SOFTBLOCK] They're all dead, it's happening again![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Get hold of yourself, Christine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's just like it was at the Cryogenics Facility![SOFTBLOCK] They retired each other, look![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We don't know that for certain.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There's a unit standing over there.[SOFTBLOCK] We should ask her what happened before we jump to conclusions.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] A survivor?[SOFTBLOCK] Perhaps she's the one who sent the distress signal?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We won't find out unless we ask her.[SOFTBLOCK] Let's go.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fold party.
		fnCutsceneMove("55", 12.25, 10.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end

--Battle unit A.
elseif(sObjectName == "BattleUnitA") then

	--Variables.
	local iFightGolemA = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemA", "N")
	if(iFightGolemA == 0.0) then
		
		--Move Christine and 55 up.
		fnCutsceneMove("Christine", 11.25, 5.50)
		fnCutsceneMove("55", 12.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Um, excuse me?[SOFTBLOCK] Unit?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Error -[SOFTBLOCK] unit identity unknown.[SOFTBLOCK] Authenticator chip error.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Can you tell us what happened here?[SOFTBLOCK] Did you send the distress signal?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Engage First Drummer Protocol.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] What?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Never heard of it before.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Engage First Drummer Protocol.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Listen, we got a distress signal and we're here to help.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Terminate non-compliant units.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Battle!
		fnCutsceneInstruction([[ AC_SetProperty("Next Music Override", "RottenTheme", 0.0000) ]])
		fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Maps/RegulusEquinox/RegulusEquinoxA/Combat_Victory.lua") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Defeat Script",  gsRoot .. "Chapter5Scenes/Defeat_BackToSave/Scene_Begin.lua") ]])
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter5/Golem_EquinoxA.lua", 0) ]])
		fnCutsceneBlocker()
	end
end
