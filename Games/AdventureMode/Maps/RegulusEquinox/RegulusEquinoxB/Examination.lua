--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ToEquinoxA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxA", "FORCEPOS:4.5x6.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxC") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxC", "FORCEPOS:26.5x7.0x0")

--[Objects]
elseif(sObjectName == "DollA") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #4992.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit was beaten from multiple sources and thrown through reinforced glass at extreme speed.[SOFTBLOCK] Massive system damage resulted in system shutdown.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemA") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit unidentified.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit's head was hooked to two cables and electrocuted, wiping out all system functionality.[SOFTBLOCK] Authenticator chip unrecoverable, unit identity unknown.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemB") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit unidentified.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit was beaten into system standby.[SOFTBLOCK] Some time later, the unit was hurled into exposed wall wiring and electrocuted.[SOFTBLOCK] Authenticator chip unrecoverable, unit identity unknown.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemC") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #205777.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] High number of conventional tungsten slugs were launched into this unit's body at close range.[SOFTBLOCK] Massive system damage led to system offline.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemD") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #312728.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit launched a tungsten slug directly into its CPU from close range.[SOFTBLOCK] It is still clutching the makeshift firearm it used.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemBoss") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Unit autopsy unnecessary.[SOFTBLOCK] Unit retired by Command Unit 2855 and Lord Unit 771852.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Cause of retirement was the usage of an Electrospear and Pulse Diffractor.[SOFTBLOCK] Chassis damage is as expected.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalS") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This terminal contains a series of descriptions of plant subjects used for an experiment.[SOFTBLOCK] The last entry trails off mid-sentence.[SOFTBLOCK] It seems that First Drummer was enacted with no warning.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalSShot") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This terminal has been shot to pieces with conventional tungsten slugs.[SOFTBLOCK] It is unusable.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalN") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The terminal contains a list of...[SOFTBLOCK] retirements.[SOFTBLOCK] The unit was recording each victim and details of offlining them...)") ]])
	fnCutsceneBlocker()
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end