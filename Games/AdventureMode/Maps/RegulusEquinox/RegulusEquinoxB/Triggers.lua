--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Sample.
if(sObjectName == "BattleUnitB") then

	--Variables.
	local iFightGolemB = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemB", "N")
	if(iFightGolemB == 0.0) then
		
		--Move Christine and 55 up.
		fnCutsceneMove("Christine", 7.25, 5.50)
		fnCutsceneMove("55", 7.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] Hello?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem: Non-compliant units.[SOFTBLOCK] Enact First Drummer Protocol.[SOFTBLOCK] Terminate.[SOFTBLOCK] Terminate.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Battle!
		fnCutsceneInstruction([[ AC_SetProperty("Next Music Override", "RottenTheme", 0.0000) ]])
		fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Maps/RegulusEquinox/RegulusEquinoxB/Combat_Victory.lua") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Defeat Script",  gsRoot .. "Chapter5Scenes/Defeat_BackToSave/Scene_Begin.lua") ]])
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter5/Golem_EquinoxB.lua", 0) ]])
		fnCutsceneBlocker()
	end
end
