--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ToEquinoxB") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxB", "FORCEPOS:4.5x10.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxD") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxD", "FORCEPOS:8.5x4.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxE") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxE", "FORCEPOS:5.0x6.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxF", "FORCEPOS:5.0x6.0x0")

--[Objects]
elseif(sObjectName == "GolemA") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit unidentified.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit received a massive electrical shock and went offline due to power core rupture.[SOFTBLOCK] Authenticator chip is unrecoverable, identity unknown.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemB") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit unidentified.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit received a massive electrical shock and went offline due to power core rupture.[SOFTBLOCK] Authenticator chip is unrecoverable, identity unknown.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: It seems likely, given the state of this room, that the unit was defragmenting when it was assaulted.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemBoss") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Unit autopsy unnecessary.[SOFTBLOCK] Unit retired by Command Unit 2855 and Lord Unit 771852.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Cause of retirement was the usage of an Electrospear and Pulse Diffractor.[SOFTBLOCK] Chassis damage is as expected.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalW") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Lift contols.[SOFTBLOCK] They're in lockdown at the moment.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalE") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Lift contols.[SOFTBLOCK] They're in lockdown at the moment.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ConsoleNoTarget") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Error.[SOFTBLOCK] Unable to locate authenticator chip signal for unit assigned to this domicile.[SOFTBLOCK] Please notify the Chief of Equinox Security.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DefragTube") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "(A defragmentation tube.[SOFTBLOCK] No way would I ever deactivate cognitive routines in a place like this!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DefragTubeOpen") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "(A defragmentation tube.[SOFTBLOCK] Looks like its occupant is on the floor there...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Oilmaker") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "(At least the oilmaker is intact.[SOFTBLOCK] Even admidst a senseless slaughter, one can always go for a cuppa.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Elevator") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "(The elevator's repair grid is blinking...[SOFTBLOCK] seems a cable was cut and the lift fell into the shaft.[SOFTBLOCK] With -[SOFTBLOCK] a unit inside...)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end