--[Combat Victory]
--The party won!

--[Movement]
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Golem hits the floor.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "GolemA")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sigh...") ]])
fnCutsceneBlocker()

--Flags.
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemD", "N", 1.0)

--Fold the party.
fnCutsceneMove("55", 15.25, 21.50)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()