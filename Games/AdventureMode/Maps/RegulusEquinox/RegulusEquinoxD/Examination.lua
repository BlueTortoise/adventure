--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ToEquinoxA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxA", "FORCEPOS:4.5x14.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxC") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxC", "FORCEPOS:26.5x26.0x0")

--[Objects]
elseif(sObjectName == "GolemA") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit unidentified.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit was placed in a conversion chamber and converted to a golem.[SOFTBLOCK] It was then beaten to offline status for unknown reasons.[SOFTBLOCK] It seems likely that the unit was in conversion before First Drummer was enacted.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemB") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #301192.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Unit heard its killphrase twice and went into permanent shutdown.[SOFTBLOCK] Unit is permanently offline.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemC") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit #89112.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Cause of retirement is unknown.[SOFTBLOCK] The unit's body, save its cranial chassis, is unaccounted for.[SOFTBLOCK] The cranial chassis has had some of its recent ocular recordings uploaded to a nearby terminal.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: The cranial chassis did not appear to suffer enough damage to cause the system offline, and was likely removed after retirement.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemD") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit authenticator chip destroyed, unit cannot be identified.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Severe corrosive damage to many locations on the chassis.[SOFTBLOCK] Power core ruptured from one of the dozens of serious chemical burns.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemE") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning::[SOFTBLOCK] Unit authenticator chip destroyed, unit cannot be identified.[SOFTBLOCK] Status:: Retired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Likely cause of retirement::[SOFTBLOCK] Corrosive destruction of entire chassis.[SOFTBLOCK] Exact cause of system offline is impossible to determine given the level of sustained damage.[SOFTBLOCK] The unit was essentially melted.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemBoss") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Unit autopsy unnecessary.[SOFTBLOCK] Unit retired by Command Unit 2855 and Lord Unit 771852.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Cause of retirement was the usage of an Electrospear and Pulse Diffractor.[SOFTBLOCK] Chassis damage is as expected.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Traces of corrosive fluid are evident on the unit's chassis but were not the cause of retirement.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ConversionChamber") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A conversion chamber much like the one that perfected me.[SOFTBLOCK] Nanofluid is at 85 percent, suggesting some human was recently converted here.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "VideographTerminal") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](It looks like a videograph of the events at the facility has been uploaded.[SOFTBLOCK] It shows golems destroying each other, then goes dark when the recording unit is struck with a pipe...)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end