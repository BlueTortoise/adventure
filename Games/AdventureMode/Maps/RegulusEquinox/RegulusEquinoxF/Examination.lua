--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ToEquinoxC") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxC", "FORCEPOS:31.0x25.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxG") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusEquinoxG", "FORCEPOS:14.0x21.0x0")
	
--[Objects]	
elseif(sObjectName == "Terminals") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](These terminals appear to have been damaged a long time ago, and were placed here in storage.[SOFTBLOCK] There is nothing to recover from them.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end