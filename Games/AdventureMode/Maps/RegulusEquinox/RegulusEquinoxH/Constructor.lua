--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusEquinoxH"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "EquinoxTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
    AM_SetMapInfo("No Special Map", "Null", 0, 0)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox6")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox0")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox1",   0,   10)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox2",  11,   20)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox3",  21,   30)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox5",  31,   50)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox4",  51, 1000)
    AM_SetProperty("Advanced Map Properties", 1229.0 - (7*16), 837.0 - (9*16), 70.0 / 96.0, 70.0 / 96.0)
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
	
	--[Lighting]
	AL_SetProperty("Activate Lights")
	AL_SetProperty("Set Player Light No Drain", true)

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	TA_CreateUsingPosition("DollOverlord", "DollOverlord")
		TA_SetProperty("Wipe Special Frames")
		TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Doll|Wounded")
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()
	
	--Unit has already been defeated.
	local iCompletedEquinox = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
	if(iCompletedEquinox == 1.0) then
		EM_PushEntity("DollOverlord")
			TA_SetProperty("Set Special Frame", "Wounded")
		DL_PopActiveObject()
	end

end
