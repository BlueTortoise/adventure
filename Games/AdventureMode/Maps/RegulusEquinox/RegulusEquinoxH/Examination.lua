--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit
if(sObjectName == "ToEquinoxG") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxG", "FORCEPOS:9.0x4.0x0")
	
--[Objects]	
elseif(sObjectName == "Terminal") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Looks like that command unit left her thoughts on First Drummer here...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]('First Drummer protocol has been a resounding success, providing a veritable trove of combat data which can be used for further simulations.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]('As expected, when the protocol was first executed, most of the units were unsure of what to do.[SOFTBLOCK] The units who were most aggressive earliest were rewarded for their ruthlessness.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]('After the initial wave of retirings, the units then proceeded to isolate themselves from the other survivors.[SOFTBLOCK] It seems something must be done to galvanize further conflict.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]('Most of the units were unarmed initially and forced to improvise means of retiring their targets.[SOFTBLOCK] One clever unit used the fabricators to create a crude firearm.[SOFTBLOCK] This firearm should be collected for study.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]('This unit recommends further uses of First Drummer in more artificial circumstances, such as randomly providing weapons or armor to units before the protocol is enacted.[SOFTBLOCK] Units are clever and frequently can improvise weaponry.')[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](...[SOFTBLOCK] That seems to be all that's here...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "BigScreen") then
	
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Readouts on the units who did best during the chaos.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The last line indicates the data was uploaded to Regulus City's servers.[SOFTBLOCK] There's nothing more we can do here.)") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end