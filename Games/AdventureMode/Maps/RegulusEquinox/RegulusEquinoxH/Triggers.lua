--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Boss fight!
if(sObjectName == "BattleBoss") then

	--Variables.
	local iCompletedEquinox = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
	
	--Play the scene.
	if(iCompletedEquinox == 0.0) then
		
		--Party moves up.
		fnCutsceneMove("Christine", 6.75, 7.50)
		fnCutsceneMove("55", 7.75, 7.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: Yes, yes, I'll be right with you.[SOFTBLOCK] I'm sorting through all the data you've provided.[SOFTBLOCK] There's so much of it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Turn around.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Turn to face.
		fnCutsceneFace("DollOverlord", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: So that explains the discrepancy![SOFTBLOCK] How clever of you to suppress your own authenticator chips.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: Still, thermal scopes indicate you have eliminated the remaining opposition.[SOFTBLOCK] Good work.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Good work?[SOFTBLOCK] Dozens of units are dead![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: Expendable, all of them.[SOFTBLOCK] In fact, we are expendable.[SOFTBLOCK] You, me, this whole facility, all in the name of science.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: Is that Head of Security 2855?[SOFTBLOCK] Of all units I would expect you to agree with me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] Drop dead.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: Such a pity.[SOFTBLOCK] Well, no matter.[SOFTBLOCK] I've already sent off my report to the administrators.[SOFTBLOCK] I expect they'll be repeating this experiment at a later date to gather more data.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Data?[SOFTBLOCK] All of this to gather data?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Just what is First Drummer?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: An experimental protocol.[SOFTBLOCK] All units wish to be the First Drummer in the marching band, and they do whatever is necessary to achieve that goal.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Including murder...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: It brings out the most powerful motivating force.[SOFTBLOCK] We remove all inhibitions, and the units do what comes naturally.[SOFTBLOCK] We will use this combat data to train the next wave of security units, and we can use versions of First Drummer to improve efficiency in sectors that lack it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: I'm so glad I was chosen to oversee this project![SOFTBLOCK] It truly has been an honor.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: Now, if there's nothing else you require of me, I should return to Regulus City for reassignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] What makes you think you're walking away from this?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: ...[SOFTBLOCK] Come again, Unit 2855?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] I asked why you think you're above all of this.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: Tch, if this is about quaint ethical boundaries, those have never been a consideration.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] They are now![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: We are high-ranking units, and we use our logical processors.[SOFTBLOCK] Unit -[SOFTBLOCK] 771852?[SOFTBLOCK] Ah, you must be new.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: If you retire me it will not undo all of the destruction wrought here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: I have violated no statutes.[SOFTBLOCK] I did exactly as the central administrators requested.[SOFTBLOCK] I fulfilled my purpose.[SOFTBLOCK] If you attempt to detain me, I will have to resist.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] What are you doing?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She's right.[SOFTBLOCK] Fighting won't help anything, and it won't bring back all these fallen units...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You can stand aside if you want, but I'm not going to.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Command Unit 8711, I am retiring you.[SOFTBLOCK] Please resist.[SOFTBLOCK] I wouldn't want my pulse diffractor to wear from disuse.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll: Unit 2855, this is highly irregular![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I'm not who I used to be.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] *sigh* and so the cycle continues...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Battle!
		fnCutsceneInstruction([[ AC_SetProperty("Next Music Override", "RottenTheme", 0.0000) ]])
		fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Maps/RegulusEquinox/RegulusEquinoxH/Combat_Victory.lua") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Defeat Script",  gsRoot .. "Chapter5Scenes/Defeat_BackToSave/Scene_Begin.lua") ]])
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter5/Doll_EquinoxH.lua", 0) ]])
		fnCutsceneBlocker()
		
		
	end
end
