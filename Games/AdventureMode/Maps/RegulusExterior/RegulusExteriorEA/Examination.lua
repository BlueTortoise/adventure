--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Exit
if(sObjectName == "ToExteriorStationE") then

	--If Christine is not a Golem:
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(sChristineForm ~= "Golem") then

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](If I'm going back to Regulus City, I should maintain my cover as a Lord Golem...)") ]])
		fnCutsceneBlocker()
		
	--Other cases.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusExteriorStationE", "FORCEPOS:19.0x12.0x0")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 0.0)
	end
	
--[Objects]
elseif(sObjectName == "Fabricator") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A fabrication table.[SOFTBLOCK] Must be used to make equipment here instead of having to ship it from Regulus City.[SOFTBLOCK] Looks to be an old model.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DefragTube") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A defragmentation tube used by the unit on duty here.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "OilMaker") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (An oil-making machine.[SOFTBLOCK] Nothing realigns your sensors after a defragmentation cycle quite like thick, black, high energy-density oil!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FizzyDrink") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A fizzy drink machine![SOFTBLOCK] But...[SOFTBLOCK] all the buttons are pink-lemonade, and it's out of stock...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Television") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This television is playing videographs from Season 7 of 'All My Processors'.[SOFTBLOCK] In this episode, Unit 107822 gets caught trying to be a tandem unit to two other units![SOFTBLOCK] Hilarity and drama ensue.)") ]])
	fnCutsceneBlocker()

--[Airlock Door]
--Close each other to maintain pressurization.
elseif(sObjectName == "AirlockDoorS") then

	--If Christine is an organic:
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(sChristineForm == "Human" or sChristineForm == "Raiju") then

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] System, lock airlock external door.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] Airlock external door in lockdown.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Er, what are you doing?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Yes?[SOFTBLOCK] That's me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not according to my optical receptors.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Wh -[SOFTBLOCK] oh![SOFTBLOCK] Yes, I am rather squishy right now aren't I?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You will not survive exposure to a vacuum.[SOFTBLOCK] You will need to change form before we proceed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Right.[SOFTBLOCK] Sorry.[SOFTBLOCK] Sometimes I forget myself...") ]])
		fnCutsceneBlocker()

	--Normal case:
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_SetProperty("Open Door", "AirlockDoorS")
		AL_SetProperty("Close Door", "AirlockDoorN")
	end
	
elseif(sObjectName == "AirlockDoorN") then
	AL_SetProperty("Close Door", "AirlockDoorS")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end