--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Speaking to the station attendant.
if(sObjectName == "AttendantScene") then

	--Variables.
	local iSawAttendantStationE = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawAttendantStationE", "N")
	if(iSawAttendantStationE == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAttendantStationE", "N", 1.0)
		
		--Move the party south through the door.
		fnCutsceneMove("Christine", 14.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 14.25, 6.50)
		fnCutsceneMove("55", 14.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorA") ]])
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 14.25, 9.50)
		fnCutsceneMove("55", 14.25, 8.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 15.25, 9.50)
		fnCutsceneMove("55", 14.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 19.25, 9.50)
		fnCutsceneMove("55", 18.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Attendant notices the party.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Excuse me, are you the attendant here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh my, visitors?") ]])
		fnCutsceneBlocker()
		
		--Turns to speak to the party.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("GolemA", -1, 0)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] A lord unit and a command unit?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] ...[SOFTBLOCK] I'm not in trouble am I?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Of course not![SOFTBLOCK] We're just checking in on our way to the Telemetry Facility.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh.[SOFTBLOCK] All right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Wait.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] No you're not.[SOFTBLOCK] I haven't received any notice of any units being transferred.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Of course you haven't, as we are not being transferred.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's a -[SOFTBLOCK] surprise inspection.[SOFTBLOCK] Of the Telemetry Facility.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Obviously if we gave notice, it'd not be a surprise inspection.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Ah, very well.[SOFTBLOCK] You may proceed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Security clearance is very high for the Telemetry facility, but I doubt a command unit would lack clearance.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] *sigh*[SOFTBLOCK] And for a moment I thought I might have someone to talk to.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Oh, before you go.[SOFTBLOCK] There's been a great deal of tectonic activity as of late.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] A survey team went by recently and installed walkways, so you should be able to make your way to the Telemtry Facility.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Just be careful.[SOFTBLOCK] This area is not patrolled by security units.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will use extra caution.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And what if we run out of caution?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Then we'll use our weapons.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Ha ha![SOFTBLOCK] Good one![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It was not a joke.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Lord Unit, let us proceed.") ]])
		fnCutsceneBlocker()
		
		--Golem goes back to watching TV.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("GolemA", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutsceneMove("55", 19.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end

--These triggers share the same name, and cause the Darkmatter girl to watch the player's party.
elseif(sObjectName == "DarkmatterAWatch") then
	EM_PushEntity("DarkmatterA")
		TA_SetProperty("Face Character", "PlayerEntity")
	DL_PopActiveObject()

end
