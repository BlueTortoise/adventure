--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Note to indicate exit directions.
if(sObjectName == "Note") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (West:: Tram station.[SOFTBLOCK] South:: Serenity Crater Observatory.[SOFTBLOCK] East:: Long-range Telemetry Facility.)") ]])
	fnCutsceneBlocker()

--Battery. Just sitting on the ground.
elseif(sObjectName == "Battery") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A portable battery. It's being used to power the nearby equipment. Looks to be a Model IX, which almost never break down!)") ]])
	fnCutsceneBlocker()

--Survey Terminal.
elseif(sObjectName == "Terminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This portable computer is currently recording various readings from the spatial sensor unit.)") ]])
	fnCutsceneBlocker()

--Survey Terminal.
elseif(sObjectName == "VolumeRadar") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A Multi-wave Spatial Sensor, often erroneously called a 3D Radar.[SOFTBLOCK] It maps the nearby area using a wide range of wavelengths to create a 3D map accurate to 1cm.[SOFTBLOCK] Takes a while to run though.") ]])
	fnCutsceneBlocker()
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end