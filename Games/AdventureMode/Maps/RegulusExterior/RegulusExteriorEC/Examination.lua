--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit.
if(sObjectName == "ToExteriorED") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorED", "FORCEPOS:8.0x21.0x0")

--A lamp, standing next to a guilty-looking Darkmatter girl.
elseif(sObjectName == "Lamp") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](This lamp was knocked over by the extremely guilty-looking Darkmatter right next to it.[SOFTBLOCK] She is feigning innocence.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end