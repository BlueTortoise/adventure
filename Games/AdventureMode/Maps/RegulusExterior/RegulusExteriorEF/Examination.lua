--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToSerenityA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("SerenityObservatoryA", "FORCEPOS:22.5x15.0x0")
	
--[Examinables]
elseif(sObjectName == "VolumeRadar") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Nominally a volumetric radar scanning unit, this one has had its transmitter shut off.[SOFTBLOCK] It is instead collecting radio data from space.)") ]])
	fnCutsceneBlocker()

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end