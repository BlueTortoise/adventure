--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit.
if(sObjectName == "ToExteriorSE") then

	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorSE", "FORCEPOS:11.0x4.0x0")

--Airlock doors.
elseif(sObjectName == "DoorS") then

	--If Christine is an organic, don't open the door.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(sChristineForm == "Human") then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](This frail, fleshy body probably wouldn't do well in a vacuum...)") ]])
		fnCutsceneBlocker()
		
	--Open the door and close the other one.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_SetProperty("Close Door", "DoorN")
		AL_SetProperty("Open Door", "DoorS")
	end
	
--Airlock doors.
elseif(sObjectName == "DoorN") then
	AL_SetProperty("Close Door", "DoorS")

--Computer console.
elseif(sObjectName == "Terminal") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment, the terminal has different examination text.
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My programming says I need to go to Regulus City to receive my function assignment.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The terminal's directory indicates I should exit this building and move north to the city.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The terminal has a series of readouts about power production and fluid motion.[SOFTBLOCK] It's not interesting enough to download.)") ]])
		fnCutsceneBlocker()

	end

--Oil Maker.
elseif(sObjectName == "OilMaker") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment, the terminal has different examination text.
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My programming says I need to go to Regulus City to receive my function assignment.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I do not currently need any additional energy.[SOFTBLOCK] I should ignore this oil maker and follow my programming.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](An oil maker.[SOFTBLOCK] Nearly every unit on Regulus has one close by for those high-density energy injections.)") ]])
		fnCutsceneBlocker()

	end

--East Transit Door.
elseif(sObjectName == "EastTransit") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment, the terminal has different examination text.
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My programming says I need to go to Regulus City to receive my function assignment.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This door leads to Transit Repair Node 445B, not Regulus City.[SOFTBLOCK] I should ignore it.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusExteriorTRNA", "FORCEPOS:21.5x39.0x0")
	end

--East Transit Ladder.
elseif(sObjectName == "EastTransitLadder") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment, the terminal has different examination text.
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My programming says I need to go to Regulus City to receive my function assignment.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This ladder leads to Transit Repair Node 445B's lower area, not Regulus City.[SOFTBLOCK] I should ignore it.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		AudioManager_PlaySound("World|ClimbLadder")
		AL_BeginTransitionTo("RegulusExteriorTRNB", "FORCEPOS:25.0x45.0x0")
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end