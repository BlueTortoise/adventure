--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit.
if(sObjectName == "ToRegulusCityAirlockA") then
	
	--Variables.
	local iIs55Following  = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    local iSaw55sMemories = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    local iSaw55PostLRT   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N")

	--Form checker.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm ~= "Golem") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Better maintain my cover as a Golem...)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        --Flashwhite.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite", "Null")
        DL_PopActiveObject()
        fnCutsceneBlocker()

        fnCutsceneWait(75)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
    end
	
    --Post LRT scene:
    if(iSaw55sMemories == 1.0 and iSaw55PostLRT == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Christine, please report to your normal work assignments.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^I downloaded what I could from the core and set up a back door into the network.[SOFTBLOCK] I will review what footage I can.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ^Are you...[SOFTBLOCK] okay?^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^...^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Report to your normal work assignments.[SOFTBLOCK] I will contact you when I have determined our next move.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ^55, come on...^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Do not waste processor cycles worrying.[SOFTBLOCK] Focus on maintaining your cover.[SOFTBLOCK] Move out.^[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ^...^") ]])
        fnCutsceneBlocker()
        
        --Remove 55 from the following group.
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		--AC_SetProperty("Set Party", 1, "Null")
		AL_SetProperty("Unfollow Actor Name", "55")

		--Flag to indicate she is not following Christine.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        
        --Flag for the next part of the cutscene.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N", 1.0)
        
        --Change map to Christine's quarters. This will fire the next part of the scenario.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.0x10.0x0") ]])
    
	--If 55 is following, she mentions she's leaving here.
	elseif(iIs55Following == 1.0) then
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] If you're going inside, it's best we're not seen together.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I'll be in touch.") ]])
		fnCutsceneBlocker()
		
		--Remove 55's sprite. She remains in the combat party for equipment reasons.
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		--AC_SetProperty("Set Party", 1, "Null")
		AL_SetProperty("Unfollow Actor Name", "55")

		--Flag to indicate she is not following Christine.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        
        --Transition.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityA", "FORCEPOS:17.5x31.0x0") ]])
	
    --None of the above.
    else
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityA", "FORCEPOS:17.5x31.0x0") ]])
    
	end
	
--Exit.
elseif(sObjectName == "ToExteriorSD") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorSD", "FORCEPOS:9.0x5.0x0")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end