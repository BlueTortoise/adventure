--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit.
if(sObjectName == "ToExteriorSC") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorSC", "FORCEPOS:28.0x4.0x0")

--Exposed Wiring.
elseif(sObjectName == "ExposedWiring") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment, the terminal has different examination text.
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My programming says I need to go to Regulus City to receive my function assignment.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This exposed wiring is not part of my programming.[SOFTBLOCK] I should ignore it.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A section of the wall here was torn out, exposing the power transfer wiring beneath.)") ]])
		fnCutsceneBlocker()

	end

--Hydraulic lift terminal.
elseif(sObjectName == "Terminal") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment, the terminal has different examination text.
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My programming says I need to go to Regulus City to receive my function assignment.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This terminal is responsible for the hydraulic lift, which is not related to my programming.[SOFTBLOCK] I should ignore it.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This terminal controls the nearby hydraulic lift.[SOFTBLOCK] Units must use it for cargo transfers.)") ]])
		fnCutsceneBlocker()

	end

--Locked Door.
elseif(sObjectName == "LockedDoor") then

	--Variables.
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")

	--If 55 is not present:
	if(iIs55Following == 0.0) then
		
		--Christine has not received a function yet.
		if(iTalkedToSophie == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail](A message comes over your short-wave radio receivers...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Door: ^Access denied.[SOFTBLOCK] New units should proceed to the upper airlock for function assignments.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ^Affirmative.[SOFTBLOCK] Proceeding to upper airlock.^") ]])
			fnCutsceneBlocker()
		
		--Has received a function. The door is now broken.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail](A message comes over your short-wave radio receivers...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Door: ^Access denied.[SOFTBLOCK] Interior airlock door is malfunctioning.[SOFTBLOCK] Please use upper airlock.^") ]])
			fnCutsceneBlocker()
		end
	
	--If 55 is following, you can get through this door.
	else

		--Variables.
		local i55ExplainedRegulusBrokenAirlock = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ExplainedRegulusBrokenAirlock", "N")

		--If 55 has not explained the airlock yet, do that now.
		if(i55ExplainedRegulusBrokenAirlock == 0.0) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/i55ExplainedRegulusBrokenAirlock", "N", 1.0)

			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail](A message comes over your short-wave radio receivers...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Door: ^[SOFTBLOCK]Access denied.[SOFTBLOCK] Interior airlock door is malfunctioning.[SOFTBLOCK] Please use upper airlock.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ^Seems we'll have to go around.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^The door isn't actually broken, I've just reprogrammed it to behave like it is.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^Thus I have a point of ingress into the city without any suspicion.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^Is this how you got in when you first converted me?^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ^Correct.[SOFTBLOCK] I merely crossed the door circuits with the airlock above, and waited for you to open it.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ^How clever of you.[SOFTBLOCK] I never suspected a thing.^[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ^Would you like the door opened to Regulus City?^[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesOpen\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoClose\") ")
			fnCutsceneBlocker()

		--Otherwise, skip right to her asking you to open the door.
		else
		
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55: ^Would you like the door opened to Regulus City?^[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesOpen\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoClose\") ")
			fnCutsceneBlocker()

		end
	end

--Open the door to lower Regulus City.
elseif(sObjectName == "YesOpen") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LowerRegulusCityA", "FORCEPOS:31.5x25.0x0") ]])
	fnCutsceneBlocker()

--Close the dialogue.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end