--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
--if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit.
if(sObjectName == "ToExteriorSB") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorSB", "FORCEPOS:26.0x8.0x0")

--Terminal next to the defrag pod.
elseif(sObjectName == "DefragTerminal") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment...
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](This terminal controls environmental settings for this defragmentation pod.[SOFTBLOCK] My drives do not need to defragment.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I should proceed to Regulus City for further assignment.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A terminal hooked into the defragmentation pod.[SOFTBLOCK] It also controls lighting and temperature for this area.)") ]])
		fnCutsceneBlocker()

	end

--Terminal next to the defrag pod.
elseif(sObjectName == "DefragPod") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment...
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A defragmentation pod, used to recharge and support units while they organize their hard-drives.[SOFTBLOCK] My drives do not need to defragment.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I should proceed to Regulus City for further assignment.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The defragmentation pod used by the unit on duty here.[SOFTBLOCK] It has some stickers of turtles on the glass.[SOFTBLOCK] I guess she likes turtles.[SOFTBLOCK] Go figure.)") ]])
		fnCutsceneBlocker()

	end

--The only entertainment in this pit.
elseif(sObjectName == "Television") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--If on your way for a function assignment...
	if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A Remote Videograph Display.[SOFTBLOCK] These provide units with information and status updates, as well as entertainment during downtime.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My programming states I should ignore this RVD.[SOFTBLOCK] I should proceed to Regulus City for further assignment.)") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A television, or Remote Videograph Display.[SOFTBLOCK] An episode of 'All My Processors' is currently playing.)") ]])
		fnCutsceneBlocker()

	end

--[Pipe Nightmare Elements]
elseif(sObjectName == "RedCheck") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Nothing is currently out of standard with this detector suite.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iRedAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedAValue", "N")
    local iRedBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedBValue", "N")
    local iRedCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedCValue", "N")
    local iRedDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedDValue", "N")
    local iRedFinal = iRedAValue + iRedBValue + iRedCValue + iRedDValue
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (The detector on the red valve says " .. iRedFinal .. "psi.)")

elseif(sObjectName == "BlueCheck") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Nothing is currently out of standard with this detector suite.)") ]])
		fnCutsceneBlocker()
        return
    end
    local iBlueAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueAValue", "N")
    local iBlueBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueBValue", "N")
    local iBlueCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueCValue", "N")
    local iBlueDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueDValue", "N")
    local iBlueFinal = (iBlueAValue + iBlueBValue) - ((iBlueDValue) - (iBlueCValue))
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (The detector on the blue valve says " .. iBlueFinal .. "psi.)")
    
elseif(sObjectName == "VioletCheck") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Nothing is currently out of standard with this detector suite.)") ]])
		fnCutsceneBlocker()
        return
    end
    local iVioletAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletAValue", "N")
    local iVioletBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletBValue", "N")
    local iVioletCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletCValue", "N")
    local iVioletFinal = (iVioletAValue) * (iVioletBValue + iVioletCValue)
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (The detector on the violet valve says " .. iVioletFinal .. "psi.)")
    
elseif(sObjectName == "YellowCheck") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Nothing is currently out of standard with this detector suite.)") ]])
		fnCutsceneBlocker()
        return
    end
    local iYellowAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowAValue", "N")
    local iYellowBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowBValue", "N")
    local iYellowCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowCValue", "N")
    local iYellowDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowDValue", "N")
    local iYellowFinal = (iYellowAValue) - ((iYellowBValue) - (iYellowCValue + iYellowDValue))
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (The detector on the yellow valve says " .. iYellowFinal .. "psi.)")
    
elseif(sObjectName == "PressureTerminal") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Nothing is currently out of standard with this terminal.[SOFTBLOCK] I should proceed to Regulus City for my function assignment.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iRedAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedAValue", "N")
    local iRedBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedBValue", "N")
    local iRedCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedCValue", "N")
    local iRedDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedDValue", "N")
    local iBlueAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueAValue", "N")
    local iBlueBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueBValue", "N")
    local iBlueCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueCValue", "N")
    local iBlueDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueDValue", "N")
    local iVioletAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletAValue", "N")
    local iVioletBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletBValue", "N")
    local iVioletCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletCValue", "N")
    local iYellowAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowAValue", "N")
    local iYellowBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowBValue", "N")
    local iYellowCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowCValue", "N")
    local iYellowDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowDValue", "N")

    --Run formulas.
    local iRedFinal = iRedAValue + iRedBValue + iRedCValue + iRedDValue
    local iBlueFinal = (iBlueAValue + iBlueBValue) - ((iBlueDValue) - (iBlueCValue))
    local iVioletFinal = (iVioletAValue) * (iVioletBValue + iVioletCValue)
    local iYellowFinal = (iYellowAValue) - ((iYellowBValue) - (iYellowCValue + iYellowDValue))
    
    --Dialogue.
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (Red:: " .. iRedFinal .. "psi. Blue:: " .. iBlueFinal .. "psi. Violet:: " .. iVioletFinal .. "psi. Yellow:: " .. iYellowFinal .. "psi.)[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (Targets are - Red:: 27psi. Blue:: 16psi. Violet:: 39psi. Yellow:: 25psi.)")
    
elseif(sObjectName == "RedA") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iRedAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedAValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iRedAValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "3 psi", sDecisionScript, "RedA|3")
    WD_SetProperty("Add Decision", "4 psi", sDecisionScript, "RedA|4")
    WD_SetProperty("Add Decision", "9 psi", sDecisionScript, "RedA|9")
    
elseif(sObjectName == "RedB") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iRedBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedBValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iRedBValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "-2 psi", sDecisionScript, "RedB|-2")
    WD_SetProperty("Add Decision", "1 psi", sDecisionScript, "RedB|1")
    WD_SetProperty("Add Decision", "0 psi", sDecisionScript, "RedB|0")
    WD_SetProperty("Add Decision", "19 psi", sDecisionScript, "RedB|19")
    
elseif(sObjectName == "RedC") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iRedCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedCValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iRedCValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "7 psi", sDecisionScript, "RedC|7")
    WD_SetProperty("Add Decision", "12 psi", sDecisionScript, "RedC|12")
    WD_SetProperty("Add Decision", "15 psi", sDecisionScript, "RedC|15")
    
elseif(sObjectName == "RedD") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iRedDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedDValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iRedDValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "-5 psi", sDecisionScript, "RedD|-5")
    WD_SetProperty("Add Decision", "1 psi", sDecisionScript, "RedD|1")
    WD_SetProperty("Add Decision", "8 psi", sDecisionScript, "RedD|8")
    
elseif(sObjectName == "BlueA") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iBlueAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueAValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iBlueAValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "5 psi", sDecisionScript, "BlueA|5")
    WD_SetProperty("Add Decision", "12 psi", sDecisionScript, "BlueA|12")
    WD_SetProperty("Add Decision", "15 psi", sDecisionScript, "BlueA|15")
    
elseif(sObjectName == "BlueB") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueBValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "-2 psi", sDecisionScript, "BlueB|-2")
    WD_SetProperty("Add Decision", "3 psi", sDecisionScript, "BlueB|3")
    WD_SetProperty("Add Decision", "4 psi", sDecisionScript, "BlueB|4")
    
elseif(sObjectName == "BlueC") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueCValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "4 psi",  sDecisionScript, "BlueC|4")
    WD_SetProperty("Add Decision", "6 psi",  sDecisionScript, "BlueC|6")
    WD_SetProperty("Add Decision", "12 psi", sDecisionScript, "BlueC|12")
    WD_SetProperty("Add Decision", "18 psi", sDecisionScript, "BlueC|18")
    
elseif(sObjectName == "BlueD") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueDValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "-8 psi", sDecisionScript, "BlueD|-8")
    WD_SetProperty("Add Decision", "1 psi",  sDecisionScript, "BlueD|1")
    WD_SetProperty("Add Decision", "9 psi",  sDecisionScript, "BlueD|9")
    WD_SetProperty("Add Decision", "17 psi", sDecisionScript, "BlueD|17")
    
elseif(sObjectName == "VioletA") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletAValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "1 psi",  sDecisionScript, "VioletA|1")
    WD_SetProperty("Add Decision", "3 psi",  sDecisionScript, "VioletA|3")
    WD_SetProperty("Add Decision", "6 psi",  sDecisionScript, "VioletA|6")
    WD_SetProperty("Add Decision", "13 psi", sDecisionScript, "VioletA|13")
    
elseif(sObjectName == "VioletB") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletBValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "-2 psi", sDecisionScript, "VioletB|-2")
    WD_SetProperty("Add Decision", "4 psi",  sDecisionScript, "VioletB|4")
    WD_SetProperty("Add Decision", "7 psi",  sDecisionScript, "VioletB|7")
    WD_SetProperty("Add Decision", "9 psi",  sDecisionScript, "VioletB|9")
    
elseif(sObjectName == "VioletC") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletCValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "1 psi",  sDecisionScript, "VioletC|1")
    WD_SetProperty("Add Decision", "5 psi",  sDecisionScript, "VioletC|5")
    WD_SetProperty("Add Decision", "11 psi", sDecisionScript, "VioletC|11")
    
elseif(sObjectName == "YellowA") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowAValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "-10 psi", sDecisionScript, "YellowA|-10")
    WD_SetProperty("Add Decision", "4 psi",   sDecisionScript, "YellowA|4")
    WD_SetProperty("Add Decision", "8 psi",   sDecisionScript, "YellowA|8")
    WD_SetProperty("Add Decision", "12 psi",  sDecisionScript, "YellowA|12")
    WD_SetProperty("Add Decision", "16 psi",  sDecisionScript, "YellowA|16")
    
elseif(sObjectName == "YellowB") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowBValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "-9 psi",  sDecisionScript, "YellowB|-9")
    WD_SetProperty("Add Decision", "1 psi",  sDecisionScript, "YellowB|1")
    WD_SetProperty("Add Decision", "4 psi",  sDecisionScript, "YellowB|4")
    
elseif(sObjectName == "YellowC") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowCValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "2 psi",  sDecisionScript, "YellowC|2")
    WD_SetProperty("Add Decision", "8 psi",  sDecisionScript, "YellowC|8")
    WD_SetProperty("Add Decision", "17 psi",  sDecisionScript, "YellowC|17")
    
elseif(sObjectName == "YellowD") then
	
	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    if(iTalkedToSophie == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](My function assignment does not require interacting with this valve.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    local iValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowDValue", "N")
    WD_SetProperty("Show")
    WD_SetProperty("Append", "Thought:[VOICE|Leader] (This valve's current pressure is " .. iValue .. "psi. Change it?)[BLOCK]")

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = LM_GetCallStack(0)
    WD_SetProperty("Activate Decisions")
    WD_SetProperty("Add Decision", "-8 psi", sDecisionScript, "YellowD|-8")
    WD_SetProperty("Add Decision", "-3 psi", sDecisionScript, "YellowD|-3")
    WD_SetProperty("Add Decision", "0 psi",  sDecisionScript, "YellowD|0")

elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")

--Unhandled case.
else
	WD_SetProperty("Hide")

    --These actually change named variables auto-magically! Wow! Coding! Wow!
    if(string.sub(sObjectName, 1, 3) == "Red") then
        
        --Get the letter indicating which variable is in question.
        local sVarName = "Root/Variables/Chapter5/Scenes/iRed" .. string.sub(sObjectName, 4, 4) .. "Value"
        
        --Now get the value.
        local iValue = tonumber(string.sub(sObjectName, 6))
        
        --Set it.
        VM_SetVar(sVarName, "N", iValue)
        
    elseif(string.sub(sObjectName, 1, 4) == "Blue") then
        
        --Get the letter indicating which variable is in question.
        local sVarName = "Root/Variables/Chapter5/Scenes/iBlue" .. string.sub(sObjectName, 5, 5) .. "Value"
        
        --Now get the value.
        local iValue = tonumber(string.sub(sObjectName, 7))
        
        --Set it.
        VM_SetVar(sVarName, "N", iValue)
        
    elseif(string.sub(sObjectName, 1, 6) == "Violet") then
        
        --Get the letter indicating which variable is in question.
        local sVarName = "Root/Variables/Chapter5/Scenes/iViolet" .. string.sub(sObjectName, 7, 7) .. "Value"
        
        --Now get the value.
        local iValue = tonumber(string.sub(sObjectName, 9))
        
        --Set it.
        VM_SetVar(sVarName, "N", iValue)
        
    elseif(string.sub(sObjectName, 1, 6) == "Yellow") then
        
        --Get the letter indicating which variable is in question.
        local sVarName = "Root/Variables/Chapter5/Scenes/iYellow" .. string.sub(sObjectName, 7, 7) .. "Value"
        
        --Now get the value.
        local iValue = tonumber(string.sub(sObjectName, 9))
        
        --Set it.
        VM_SetVar(sVarName, "N", iValue)
        
        
    end


end