--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToExteriorSB") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorSB", "FORCEPOS:95.5x42.0x0")
	
elseif(sObjectName == "LadderN") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorTRNB", "FORCEPOS:7.0x7.0x0")
	
elseif(sObjectName == "LadderS") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorTRNB", "FORCEPOS:7.0x13.0x0")

--[Examinables]
elseif(sObjectName == "FizzyPopBlue") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Fizzy Pop![SOFTBLOCK] The blue variety.[SOFTBLOCK] I don't need a recharge right now.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FizzyPopPink") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Fizzy Pop![SOFTBLOCK] The pink variety.[SOFTBLOCK] I don't need a recharge right now.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "WorkTerminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A work terminal.[SOFTBLOCK] It's been disconnected from the Regulus City network.[SOFTBLOCK] The security authorization driver has been pulled out...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The defragmentation logs for this unit show nothing of interest.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This unit had music play while she defragmented.[SOFTBLOCK] It seems she was a fan of industrial metal -[SOFTBLOCK] as all good robots should be!)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The last defragmentation logs on this terminal indicate this facility was vacated shortly before the Cryogenics facility was 'restricted'.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: ('Attention all units:: This facility is to be vacated immediately.[SOFTBLOCK] Please acquire high priority equipment as per protocol SSCR-7 and return to Regulus City for reassignment.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The work logs show a steady increase in work assignments until the facility was ordered abandoned.[SOFTBLOCK] No reason for the abandonment can be found in the logs...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalF") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The only thing on the hard drive is a long list of transit network orders and repair requests.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalG") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The hard drive has two folders.[SOFTBLOCK] One labeled 'Work' and the other 'Porn'.[SOFTBLOCK] They both contain images of scantily-clad tram diagnostics and repair manuals.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "Oilmaker") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The oilmaker here is flavoured with sulfur and shaved iron crystals.[SOFTBLOCK] Yum!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Couch") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A synthleather chesterfield, facing the RVD on the south wall.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FabricationBench") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A fabrication bench.[SOFTBLOCK] The tools have been completely stripped, so it's useless at the moment.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "SpareParts") then
	local iServicePartsA = VM_GetVar("Root/Variables/Chapter5/Scenes/iServicePartsA", "N")
	if(iServicePartsA == 0.0) then
		LM_ExecuteScript(gsItemListing, "Assorted Parts")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iServicePartsA", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Spare parts.[SOFTBLOCK] I don't think anyone will mind if I borrow some...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [SOUND|World|TakeItem](Received Assorted Parts x1)") ]])
		fnCutsceneBlocker()
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (There's nothing useful left on the shelf.)") ]])
		fnCutsceneBlocker()
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end