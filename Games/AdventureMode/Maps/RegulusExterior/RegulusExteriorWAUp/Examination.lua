--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Objects]
if(sObjectName == "TerminalL") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Work orders, work orders, work orders...[SOFTBLOCK] Latex-Drone-on-Raiju videographs?[SOFTBLOCK] I'll just take a peek...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](...[SOFTBLOCK] I'll have to remember that position for later with Sophie...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalR") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Work orders, work orders...[SOFTBLOCK] Wait, this is the entire transcript for season 85 of All My Processors.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Ugh, this is the season where the writing staff replaced Unit 505900 with Unit 178112.[SOFTBLOCK] The dialogue is awful![SOFTBLOCK] Zero out of ten, would not read again!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Couch") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A very nice chesterfield, fit for a Lord Unit.[SOFTBLOCK] I have a similar furnishing in my quarters.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Window") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](You can see Sector 15's habitation complex from here, and beyond it, Regulus City's skyline.[SOFTBLOCK] It's entrancing...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TV") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A Remote Videograph Display.[SOFTBLOCK] It's currently showing a documentary on RX-1882 Vehicular Transmission Units and how they're manufactured.[SOFTBLOCK] It's fascinating, but I don't have time to watch it right now.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DefragTubes") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A set of defragmentation tubes, for the units on duty here.[SOFTBLOCK] One of them is in considerably better condition than the other two...)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end