--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Terminal.
if(sObjectName == "Terminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](A work terminal.[SOFTBLOCK] Seems the unit on duty here was doing math problems in her spare time.[SOFTBLOCK] How productive!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Let's see...[SOFTBLOCK] 80085.[SOFTBLOCK] Maybe that's the designation of her tandem unit?)") ]])
	fnCutsceneBlocker()

--Defragmentation Tube.
elseif(sObjectName == "DefragTube") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](The defragmentation tube used by the unit on duty here.)") ]])
	fnCutsceneBlocker()

--Note.
elseif(sObjectName == "Note") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Please leave the trash bins where they are when collecting refuse, thank you.)") ]])
	fnCutsceneBlocker()

--Northern Door.
elseif(sObjectName == "DoorN") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_SetProperty("Close Door", "DoorExterior")
	AL_SetProperty("Open Door", "DoorInterior")

--Southern Door.
elseif(sObjectName == "DoorS") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_SetProperty("Close Door", "DoorInterior")
	AL_SetProperty("Open Door", "DoorExterior")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end