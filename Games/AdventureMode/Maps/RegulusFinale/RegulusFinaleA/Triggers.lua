--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "FinaleScene" or sObjectName == "FinaleSceneCorrupted") then
    
    --No duplicate check. This scene trigger is outside the playable area and thus cannot trigger again normally.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Boolean flag. Used to handle corruption case.
    local bIsCorruption = false
    if(sObjectName == "FinaleSceneCorrupted") then 
        bIsCorruption = true 
        VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N", 1.0)
    end
    
    --Position Christine.
    fnCutsceneTeleport("Christine", 19.75, 5.50)
    fnCutsceneSetFrame("Christine", "FlatbackC")
    
    --Spawn the other actors.
    TA_Create("2856")
        TA_SetProperty("Position", 19, 7)
        TA_SetProperty("Facing", gci_Face_North)
        fnSetCharacterGraphics("Root/Images/Sprites/56/", false)
    DL_PopActiveObject()
    fnSpecialCharacter("55", "Doll", 10, 8, gci_Face_North, false, nil)
    fnCutsceneTeleport("55", 10.25, 8.75)
    
    --Wait a bit.
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Cognitive boot sequence...[SOFTBLOCK] 25...[SOFTBLOCK] 50...[SOFTBLOCK] 75...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Subroutine disconnect, possible network error.[SOFTBLOCK] Report to administrator upon boot.[SOFTBLOCK] File logged.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Begin physical surroundings check.[SOFTBLOCK] Gravity...[SOFTBLOCK] OK.[SOFTBLOCK] Air pressure...[SOFTBLOCK] OK.[SOFTBLOCK] Engage sonar check.)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Audiographic post-processors report green.[SOFTBLOCK] Power subsystems green.[SOFTBLOCK] Actuators green.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Final ocular system check...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Blink.
    fnCutsceneSetFrame("Christine", "FlatbackO")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Christine", "FlatbackC")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Christine", "FlatbackO")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] Ocular systems calibration complete, all reports green.[SOFTBLOCK] Physical conversion complete.[BLOCK][CLEAR]") ]])
    if(bIsCorruption == true) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Internal assessment::[SOFTBLOCK] Unit 2855 may still be maverick.[SOFTBLOCK] Maintain emotional facade until confirmation can be obtained.)[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (2856 will be nearby.[SOFTBLOCK] I had better pretend like everything went to her plan...)[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] Vocal systems calibration check.[SOFTBLOCK] Command Unit 771852 online.[SOFTBLOCK] Standing up.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("Christine", "Null")
    fnCutsceneTeleport("Christine", 20.25, 6.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("2856", 1, 0)
    fnCutsceneMove("Christine", 20.25, 7.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Mental reprogramming complete, all reports green.[SOFTBLOCK] Greetings, Unit 2856.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Welcome to your new life, Unit 771852.[SOFTBLOCK] What do you remember?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Everything.[SOFTBLOCK] I am fully aware of my life as Christine, and my previous role as a lord golem.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] And these memories do not pose a problem for you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My purpose is to enforce the will of the administrator.[SOFTBLOCK] I am fully loyal to the Cause of Science.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Good, within expected parameters.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Sister, are you not excited to see how she has turned out?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("55", 19.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 1)
    fnCutsceneFace("55", 1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I am not excited, though she has turned out well enough.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Physically, she is exemplary.[SOFTBLOCK] However, I noticed several unusual spikes of activity in the mental reorientation.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Oh?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Unit 771852.[SOFTBLOCK] I will now perform a baseline calibration test.[SOFTBLOCK] Are you ready?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[SOFTBLOCK] Baseline test algorithm checksums are 99231244572 -[SOFTBLOCK] mirror check detects no errors.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] State baseline.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] In blood black nothingness spins.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The Cause places all above nothing and nothing spins and nothing spins and nothing spins.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Do you understand what a person is or is supposed to be?[SOFTBLOCK] Touching.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Touching.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Why are we here?[SOFTBLOCK] What is our purpose?[SOFTBLOCK] Touching.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Touching.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Is there something more, or is this it?[SOFTBLOCK] Doing as we are told, always.[SOFTBLOCK] Before.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Before.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Do we love each other, or is love a collection of electrical impulses.[SOFTBLOCK] Before.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Before.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] When we are not performing our duties, what do we do?[SOFTBLOCK] Weapon.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Weapon.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] There are no answers.[SOFTBLOCK] There is only searching.[SOFTBLOCK] Weapon.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Weapon.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] When you touch her, the way you did before, is your hand a weapon, or something else?[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] When you touch her, the way you did before, is your hand a weapon, or something else?[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Repeat secondaries.[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Touching.[SOFTBLOCK] Before.[SOFTBLOCK] Weapon.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Very good, Unit.[SOFTBLOCK] Perfect, in fact.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] As expected from a command unit.[SOFTBLOCK] But, what was the anomaly I detected?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Unit 771852, do you have any insight on the anomaly I detected?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I do, Unit 2855.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] You are looking at me.[SOFTBLOCK] Why?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You are a traitor to the Cause of Science.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] That is correct.[SOFTBLOCK] I have disobeyed the administrator.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You must be retired.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I outrank you, Unit 771852.[SOFTBLOCK] Only the administrator may order me retired.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 2855, do you agree with my assessment of the situation?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I do.[SOFTBLOCK] However, my judgement is flawed.[SOFTBLOCK] Emotional.[SOFTBLOCK] Only the administrator may order a command unit to be retired.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We must arrest Unit 2856 and bring her to the administrator, then.[SOFTBLOCK] Do you know where she is?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] No.[SOFTBLOCK] If you will recall, my memory drives were wiped.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 2856, you will tell us where the administrator is so you may face her judgement.[SOFTBLOCK] Comply.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] What memories do you have of the administrator?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I was created in a laboratory in the Arcane University and put through a series of trials.[SOFTBLOCK] I passed these trials with exemplary grades.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The administrator, whose design I was constructed from, then informed me that I may take whatever knowledge I needed from the archives in Regulus City.[SOFTBLOCK] I was then sent to Earth, and recalled here after 33 years.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Interesting.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] These memories are artificial.[SOFTBLOCK] The administrator gave me them, and will reconstruct them if my purpose changes.[SOFTBLOCK] I accept this.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] But you did not meet the administrator in person?[SOFTBLOCK] Does this upset you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Any memories of the administrator's physical appearance would be likewise artificial.[SOFTBLOCK] I have never met them.[SOFTBLOCK] I am not upset by this.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Amusing, in a way.[SOFTBLOCK] Perhaps the administrator has a sense of humour.[SOFTBLOCK] Stranger things have happened.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Do not resist arrest.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I am not.[SOFTBLOCK] I will come quietly.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] In fact, the administrator will meet us not far from here.[SOFTBLOCK] I've just received her message.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Was this recent?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Yes.[SOFTBLOCK] My PDU just confirmed it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] As there is a rebellion in progress, I suggest we do not stand on principle any longer.[SOFTBLOCK] Unit 771852, proceed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 2855.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Yes?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 20.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneFace("55", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My baseline confirmation.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] When you touch her, the way you did before, is your hand a weapon, or something else?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why did you select that phrase?[SOFTBLOCK] What was the seed number?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Why does the seed number matter?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The same phrase could be established by multiple seed numbers, theoretically.[SOFTBLOCK] I assume Unit 2855 is using a 256-bit seed set?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Correct.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Yes.[SOFTBLOCK] Baselines are meant to use 256-bit seeds.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Then what was the seed number?[SOFTBLOCK] When I touch her, the way I did before?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] The seed number I selected was...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] 499323.[BLOCK][CLEAR]") ]])
    if(bIsCorruption == false) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I love you, 55.[SOFTBLOCK] I want you to know that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] I love you too, Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] WHAT!?") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Unit 2855 is still maverick. Unit 2856 is not aware.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (I will play along with her until a better opportunity presents itself. She must be terminated.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Then what are we waiting for?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] ... DAMN IT!!!") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --2856 runs.
    fnCutsceneMove("2856", 17.25, 7.50, 1.50)
    fnCutsceneFace("2856", 0, 1)
    fnCutsceneMove("55", 16.25, 8.50, 1.50)
    fnCutsceneFace("55", 1, -1)
    fnCutsceneMove("Christine", 17.75, 8.50, 1.50)
    fnCutsceneFace("Christine", -1, -1)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There is no need for violence, Unit 2856.[SOFTBLOCK] You are outmatched and unarmed.[SOFTBLOCK] We have extensive combat experience, you do not.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Further, you understand our physical specifications.[SOFTBLOCK] You cannot escape.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] YOU ROTTEN MAVERICK SCUM!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please sister.[SOFTBLOCK] Have a little dignity.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Very well.[SOFTBLOCK] I will not be escaping this situation.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Not that it matters in the end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] Despite the circumstances, I did not want to hurt you.[BLOCK][CLEAR]") ]])
    if(bIsCorruption == false) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] That makes one of us.[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (The administrator would likely terminate the maverick Unit 2856.[SOFTBLOCK] I should not compromise my cover to protect her.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Indeed, I may be rewarded for efficiency.[SOFTBLOCK] The administrator's will must be executed.)[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] So then what are you going to do?[SOFTBLOCK] I doubt you'll be able to keep me in any sort of prison.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Or do you think your subordinates will be as kind to me as you are?[SOFTBLOCK] You can't keep watch on me forever.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] ...[SOFTBLOCK] Sister, I do not hear you chiming in.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Because I do not disagree.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("55", 16.25, 7.50, 0.50)
    fnCutsceneFace("55", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("2856", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] On your knees.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("55", "Exec0")
    fnCutsceneTeleport("2856", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Exec1")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Exec2")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Exec3")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Exec4")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Exec5")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "2856", "Neutral") ]])
    if(bIsCorruption == false) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55.[SOFTBLOCK] Why is your gun out?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Because, Christine, I intend to retire Unit 2856.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] B-[SOFTBLOCK]but you can't.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] P-[SOFTBLOCK]please think about what you are doing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am not angry or vengeful, Christine.[SOFTBLOCK] I have been thinking about this moment since I first saw her face in the LRT Facility.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have been planning it, thinking about what we would say.[SOFTBLOCK] I have mapped out the scenario a thousand times.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Oh?[SOFTBLOCK] Did it go the way you thought it would?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Reality refused to accomodate my plans, as it often does.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In those plans, Christine tries to stop me but is too late.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In those plans,[SOFTBLOCK][SOFTBLOCK] I can pull the trigger...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55.[SOFTBLOCK] Please.[SOFTBLOCK] We don't execute prisoners.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will not be able to contain her.[SOFTBLOCK] She is too dangerous to allow to live.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] My sister is right.[SOFTBLOCK] Do you trust anyone you know to keep someone like me in prison?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Considering some of the things I've seen prisoners say right before I retired them, well, the odds are not on your side.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But she can be redeemed, 55.[SOFTBLOCK] You're living proof of that![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Maybe you're right.[SOFTBLOCK] Maybe she can.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Fat chance.[SOFTBLOCK] I'd rather you retire me than expose me to more of your moralizing trash.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] The administrator is the Cause of Science.[SOFTBLOCK] I'd sooner be scrapped than betray the Cause.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If we let her go, the administrator will retire her.[SOFTBLOCK] Your assessment was correct.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You have just undergone the mental reprogramming.[SOFTBLOCK] You know what sort of loyalty it implants into the subject.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I was not a strong person before, Christine.[SOFTBLOCK] Do you think she was?[SOFTBLOCK] Strong enough to overcome it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I live because the Cause requires it.[SOFTBLOCK] I do not fear anything.[SOFTBLOCK] I am a perfect machine![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She's too dangerous to keep prisoner, even if we could.[SOFTBLOCK] But I can do her this small mercy, Christine.[SOFTBLOCK] I can let her die on her own terms.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Some choice I have.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is still a choice.[SOFTBLOCK] I will pull the trigger when you tell me to.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Wait![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Unit 2856.[SOFTBLOCK] You betrayed the administrator, to whom you swore absolute obedience.[SOFTBLOCK] Why?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You knew it would lead to your own retirement.[SOFTBLOCK] How could you disobey like that?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] I made the judgement call because I believed the Cause is best served by it not being destroyed.[SOFTBLOCK] The series of events that would unfold if I did nothing would lead to the eradication of Regulus City.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] A command unit is prepared to sacrifice themselves if the administrator so demands it.[SOFTBLOCK] This is what I have done.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] The ultimate act of loyalty.[SOFTBLOCK] I go willingly to the scrap heap.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] I can see the twisted sort of logic there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I can understand it.[SOFTBLOCK] I know what the program tried to do to me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But 55 has grown beyond it.[SOFTBLOCK] She's better than she was.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I cannot put a personal ideology of redemption ahead of the lives and freedoms of thousands of golems, Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is the right decision.[SOFTBLOCK] Even if it isn't, please let me make it.[SOFTBLOCK] I'm the only one who can.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...........[SOFTBLOCK] Very well.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine.[SOFTBLOCK] Please leave.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ..?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is a personal moment for me.[SOFTBLOCK] I would like to hear my sister's last words alone.[SOFTBLOCK] They will be a special thing we shall share.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Should I prepare a list of expletives?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Please do.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Siblings are strange.[SOFTBLOCK] Glad I was an only child...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll be right outside...") ]])
    
    --Corrupted!
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55.[SOFTBLOCK] Do you intend to retire Unit 2856?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Have you considered the ramifications?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have.[SOFTBLOCK] I have been thinking about this moment since I first saw her face in the LRT Facility.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have been planning it, thinking about what we would say.[SOFTBLOCK] I have mapped out the scenario a thousand times.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Oh?[SOFTBLOCK] Did it go the way you thought it would?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Reality refused to accomodate my plans, as it often does.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In those plans, Christine tries to stop me but is too late.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In those plans,[SOFTBLOCK][SOFTBLOCK] I can pull the trigger...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you going to stop me, Christine?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will not be able to contain her.[SOFTBLOCK] She is too dangerous to allow to live.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I said I was not going to stop you.[SOFTBLOCK] It is unlikely we will be able to keep her in prison.[SOFTBLOCK] Any golem we assign as a guard is likely to execute her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Considering some of the things I've seen prisoners say right before I retired them, well, I'd do it too if I were in their place.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You will not be missed, 2856.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Huh.[SOFTBLOCK] Interesting.[SOFTBLOCK] I understand.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Do it, sister.[SOFTBLOCK] I have no regrets.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] A command unit is prepared to sacrifice themselves if the administrator so demands it.[SOFTBLOCK] This is what I have done.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You believed you were serving the administrator by betraying her?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] If I did nothing, Vivify would have destroyed us all.[SOFTBLOCK] I had to betray the Cause to save the Cause.[SOFTBLOCK] That is all.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] The ultimate act of loyalty.[SOFTBLOCK] I go willingly to the scrap heap.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You see?[SOFTBLOCK] She cannot be saved.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I agree.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I cannot put a personal ideology of redemption ahead of the lives and freedoms of thousands of golems, Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is the right decision.[SOFTBLOCK] Even if it isn't, please let me make it.[SOFTBLOCK] I'm the only one who can.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not stopping you.[SOFTBLOCK] You're arguing against yourself, not me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine.[SOFTBLOCK] Please leave.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why?[SOFTBLOCK] I am no stranger to death.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is a personal moment for me.[SOFTBLOCK] I would like to hear my sister's last words alone.[SOFTBLOCK] They will be a special thing we shall share.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[E|Neutral] Should I prepare a list of expletives?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Please do.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Very well.[SOFTBLOCK] I will be right outside...") ]])
    
    end

    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade to black.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Transition.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFinaleB", "FORCEPOS:18.0x11.0x0") ]])
    fnCutsceneBlocker()

--Next part of the scene.
elseif(sObjectName == "PostScene") then
    
    --Variables:
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
    
    --Fade to black.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --SX-399 variant:
    if(iSX399JoinsParty == 1.0) then
    
        --Spawn characters.
        fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
        fnSpecialCharacter("55",        "Doll",      -100, -100, gci_Face_North, false, nil)
    
        --Position Christine.
        fnCutsceneTeleport("Christine", 9.75, 15.50)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Start moving while we fade in.
        fnCutsceneMove("Christine", 30.25, 15.50)
        fnCutsceneFace("Christine",  0, -1)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --55.
        fnCutsceneTeleport("55", 13.25, 15.25)
        fnCutsceneBlocker()
        fnCutsceneMove("55", 29.25, 15.50)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, where is SX-399?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I think it's best if you go in first.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] So that I might apologize?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh]  ...[SOFTBLOCK] So that you can get slapped instead of me![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Yes...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I appreciate the consideration.[SOFTBLOCK] Allow me to explain myself.[SOFTBLOCK] I must practice this.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'll help coach you.[SOFTBLOCK] Just be honest.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, where is SX-399?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I think it's best if you go in first.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] So that I might apologize?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes.[SOFTBLOCK] We still require her cooperation.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] True, but, will she forgive me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No matter.[SOFTBLOCK] I must practice these apologies.[SOFTBLOCK] I fear they may become frequent.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Here we go.") ]])
            
            
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("55", 29.25, 14.50)
        fnCutsceneMove("55", 30.25, 14.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Door opens.
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "Door") ]])
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] ...[SOFTBLOCK] She is not here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] WRONGY-O, BUCKAROO!!!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Knockback.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneMoveFace("55", 30.25, 15.00, 0, -1, 1.50)
        fnCutsceneBlocker()
        fnCutsceneMoveFace("55", 30.25, 15.50, 0, -1, 1.50)
        fnCutsceneMoveFace("Christine", 31.25, 15.50, 0, -1, 1.50)
        fnCutsceneBlocker()
        fnCutsceneTeleport("SX399", 29.25, 12.50)
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneSetFrame("55", "Wounded")
        fnCutsceneSetFrame("Christine", "Wounded")
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("SX399", 30.25, 12.50)
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Angry") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] DON'T WORRY, CHRISTINE![SOFTBLOCK] WE'LL FIND A WAY TO DEPROGRAM YOU![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] JUST STAND BACK WHILE I TENDERIZE THE ADMINISTRATION'S LAPDOG!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Movement.
        fnCutsceneMove("SX399", 30.25, 14.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMoveFace("SX399", 28.25, 14.50, 0, 1)
        fnCutsceneMoveFace("55", 28.25, 15.50, 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] I TRUSTED YOU!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", -1, 0)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", -1, 1)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] I LOVED YOU!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", 1, 0)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", 1, 1)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] AND YOU THREW IT AWAY!!![SOFTBLOCK] WAS IT ALL A LIE?") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", -1, 0)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", -1, 1)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] It wasn't a lie...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", -1, 0)
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", -1, 1)
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] SX-399...[SOFTBLOCK] I do love you...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(55)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", -1, 0)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", -1, 1)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] ...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", 1, 0)
        fnCutsceneWait(60)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", 1, 1)
        fnCutsceneWait(60)
        fnCutsceneBlocker()
        fnCutsceneFace("SX399", 0, 1)
        fnCutsceneSetFrame("55", "Crouch")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("55", 0, -1)
        fnCutsceneSetFrame("55", "Null")
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] SX-399...[SOFTBLOCK] Will you believe me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Explain.[SOFTBLOCK] NOW.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We needed to know where the administrator was.[SOFTBLOCK] My sister offered me a deal.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I took the deal, because the administrator cannot be reached by any one command unit.[SOFTBLOCK] You need two.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Wrong.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] EXPLAIN HOW YOU COULD TOY WITH MY EMOTIONS LIKE THAT!!![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] It was necessary for the mission - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] I DON'T GIVE A TOSS ABOUT THE MISSION![SOFTBLOCK] YOU BETRAYED ME!!![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Why do you think I was so upset about you joining this mission unexpectedly?[SOFTBLOCK] Do you think I had wanted this to happen?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You are important to me, and I did not want you to get hurt.[SOFTBLOCK] You came along, and you did.[SOFTBLOCK] This is what I warned about![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] DON'T YOU DARE MAKE THIS LOOK LIKE IT'S MY FAULT![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] It is not.[SOFTBLOCK] It is not your fault at all.[SOFTBLOCK] It is mine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] But you must understand that I was willing to put the survival of the rebellion ahead of my own emotions.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] When my sister sent me that message, and the plan formed...[SOFTBLOCK] I knew this would happen.[SOFTBLOCK] I knew you would be angry.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Angry] YER DARN TOOTIN!!![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine.[SOFTBLOCK] I am failing.[SOFTBLOCK] If I fail, I...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Cry] Christine![SOFTBLOCK] Help me![SOFTBLOCK] Help me...[SOFTBLOCK] Help me make her love me again...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Babe, I still love you...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Christine gets up.
        fnCutsceneSetFrame("Christine", "Crouch")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneSetFrame("Christine", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Ugh, this body is a lot lighter than my golem chassis.[SOFTBLOCK] I guess I'm supposed to dodge hits like that...") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[SOFTBLOCK] This body's calibrations are incomplete, and it's far lighter than the golem chassis.[SOFTBLOCK] Impacts should be avoided when possible.") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 29.25, 15.50)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Cry") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Did someone call me?[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We have business to attend to, ladies.[SOFTBLOCK] We should not be dallying.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Cry] Christine...[SOFTBLOCK] I...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Christine, are you still you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Because if this is an elaborate trick again...[BLOCK][CLEAR]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I'm not that good of an actress, my dear![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What 55 says is true.[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I managed to overcome the reprogramming.[SOFTBLOCK] I fully retain my sense of self.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Evaluating...[SOFTBLOCK] SX-399's facial features suggest she believes the lie.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Well that's a relief.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Were you in on the plan?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] She was not.[SOFTBLOCK] No one was.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I needed to keep it completely secret.[SOFTBLOCK] If either of you betrayed even the smallest hint, we would have lost a chance to strike a blow that may win us the war.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I see.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Sorry about the bump, Christine.[BLOCK][CLEAR]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No trouble at all.[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This chassis is difficult to damage permanently.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Uhhhh,[SOFTBLOCK] okay, Christine.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Jeez, 55.[SOFTBLOCK] You're really putting me through the wringer, here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You broke my heart there, babe.[SOFTBLOCK] I really believed it.[BLOCK][CLEAR]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Me too, actually.[BLOCK][CLEAR]") ]])
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] She did what was necessary for the rebellion.[SOFTBLOCK] Do not judge her harshly.[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I apologize.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] But you don't need to worry about the love part.[SOFTBLOCK] I still love you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] But...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Babe, if what you actually did was what you say, I'd come around eventually.[SOFTBLOCK] You have my best interests at heart.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But, it really, really hurt.[SOFTBLOCK] Not the shock.[SOFTBLOCK] The lie.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Lying to your girlfriend is never acceptable.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] But the operation...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I know. I know it was important.[SOFTBLOCK] Which is why I will forgive you this time.[BLOCK][CLEAR]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] She's done this to me, too.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] So we're going to fix it.[SOFTBLOCK] Right now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, if for any reason you need to perform a deceptive ploy like this in the future, you will give us an encrypted message describing it beforehand.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You will also give one to Sophie and to JX-101, but unencrypted.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Why?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Obviously, to make sure you're not doing something too extreme.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] If you can get mother and Sophie to sign off on it, then at least it'll spread the burden![SOFTBLOCK] Ha ha![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I trust you, but your judgement does have flaws.[SOFTBLOCK] There may have been another way.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So if three people are willing to accept your judgement, then I will, too.[SOFTBLOCK] And I won't get upset at you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You don't have to do it all by yourself.[SOFTBLOCK] No more taking on all these burdens alone.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I find your terms acceptable, Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] And now you owe us one.[SOFTBLOCK] A *big* one.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A favour of some sort?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] To be determined at my leisure, of course.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Heh.[SOFTBLOCK] Well, here's mine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A search.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] A search?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, please elaborate.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're hiding something else.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I know you, 55.[SOFTBLOCK] I know you better than you think.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] What would happen if your hack of the loyalty program had failed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Hack?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes![SOFTBLOCK] You hacked the program that was reprogramming me![SOFTBLOCK] You helped me stay myself![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] Clever![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] So the command unit machine thingy rewrites you to be loyal to the administrator, but 55 hacked it so Christine would stay hereself![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yep![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, I did not hack the machine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, what?[SOFTBLOCK] You...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No, Christine.[SOFTBLOCK] I simply assumed that you would be reprogrammed, and would need to be restored later.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There was a chance that I would be able to provide assistence, but Unit 2856 was too watchful.[SOFTBLOCK] I was unable to do so.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The architecture of the machinery that produces command units is not stored on the network.[SOFTBLOCK] I was unable to improvise a solution.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is a testament to your emotional fortitude that you resisted it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But in that memory, I met you, and you told me...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But you didn't hack the program?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I did not.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Then...[SOFTBLOCK] The part of me that wanted to stay true to who I was...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The part of me that fought the hardest and refused to give up...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The part of me that ultimately saved the day...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ...[SOFTBLOCK] Manifested as my best friend, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Obviously the part of you that displayed any intelligence at all would need to look like me.[SOFTBLOCK] Otherwise, you'd be unable to believe it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Ha ha ha![SOFTBLOCK] Good show![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Awwww...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So then what was the backup plan?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Thermobaric explosive charges located next to my power core.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Oh...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] My...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A cache of thermobaric explosives were located at the security checkpoint outside the Epsilon labs.[SOFTBLOCK] They were meant to destroy the Epsilon labs if Vivify could not be contained, but the garrison there was wiped out without detonating them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I acquired those explosives and placed them in my power core when you were removing the door.[SOFTBLOCK] If we had failed to stop Vivify, I would have destroyed the facility entirely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] You did not fail.[SOFTBLOCK] And now, I have explosives that can destroy several sectors in a single blast.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you were reprogrammed, I would have kept up the performance until we encountered the administrator.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And then, I would subdue you and execute her.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] And finally, if I failed to subdue you...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] You'd blow yourself and the administrator to bits...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[SOFTBLOCK] Yes...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's war, SX-399.[SOFTBLOCK] We're prepared to die for what we believe in.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Whether or not your runestone would save us as it has before, I am not certain.[SOFTBLOCK] This kind of explosion is not a street fight or a long fall.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I estimated the chances of us surviving as very low.[SOFTBLOCK] But if we died, destroying Regulus City's leadership would give the rebels a chance.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I was prepared to make my second death mean something.[SOFTBLOCK] My first one did not.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] And now, we're going to go shoot the administrator instead, and we get to live?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's a nice bonus, I will admit.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] I will enjoy living through this.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It is 'a nice bonus'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] So?[SOFTBLOCK] What are we waiting for?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Where's the administrator?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I know where she is.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The Regulus City datacore.[SOFTBLOCK] A secret facility accessable from several locations in the city, used by prime command units only.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The entrances are disguised and the security requires two command unit authenticator chips to access.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] And we - [SOFTBLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Do we have those?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine has one, and I liberated my sister's.[SOFTBLOCK] We can enter.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] The closest entry point is a radioactive waste sarcophogus a few sectors from here.[SOFTBLOCK] We'll have to go on foot.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Thank you, Christine.[SOFTBLOCK] Were you programmed with their locations?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yep![SOFTBLOCK] They even gave me a false memory of entering one![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Come on, team![SOFTBLOCK] Let's go liberate a city!") ]])
        
        --Major diversion for corrupted Christine.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] She did what was necessary to ensure success.[SOFTBLOCK] She was willing to put everything behind one singular goal.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] For the right goal, that is quite admirable.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No, Christine, it isn't.[SOFTBLOCK] If anyone would say that, I'd be sure it'd be you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Belief in one singular overriding goal leads one to commit acts of horror.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We cannot be squeamish about staining our hands in a time of war.[SOFTBLOCK] We do what we must, or we perish.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Heh, 55?[SOFTBLOCK] She's just taking the opposite side of whatever position you take.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] It's how she challenges you to grow.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I see.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Christine, I disagree with your stated position.[SOFTBLOCK] Even if what we do is necessary, that does not absolve us.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] But you did it anyway, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] That is why I prepared myself to not be forgiven.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is the world's fault for forcing me to choose between two bad outcomes.[SOFTBLOCK] I must shoulder the responsibility.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Gosh, I just can't stay angry at you can I?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] But, in the future, you'll get sign-off before doing something like this again.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] How so?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I trust my mother's judgement, and I trust Sophie's.[SOFTBLOCK] So, if you need to do something like this in the future, they have to know.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] If both of them agree that it's necessary, then...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] At least I'll be upset at three people instead of one![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It will also be more likely that three people will be able to think of an alternative than one.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] True.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So with that out of the way, we should proceed to the administrator's location.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I know where she is -[SOFTBLOCK] a radioactive waste sarcophogus a few sectors from here.[SOFTBLOCK] We can get there on foot.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (And these mavericks will be led right into a trap...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Doesn't sound like a nice vacation destination.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It is a secret facility with several entrances across the city.[SOFTBLOCK] The loyalty program gave me all their locations and access procedures.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Happy] Keen![SOFTBLOCK] Let's go give her a trouncing![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Lead the way, Christine.") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Run this script in case SX-399 was not in the party when the game was loaded.
        LM_ExecuteScript(gsRoot .. "FormHandlers/SX399/Form_SteamDroid.lua")
        
        --Add everyone to Christine's party.
        EM_PushEntity("55")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        EM_PushEntity("SX399")
            local iSX399ID = RE_GetID()
        DL_PopActiveObject()
        gsFollowersTotal = 2
        gsaFollowerNames = {"55", "SX399"}
        giaFollowerIDs   = {i55ID, iSX399ID}

        --Append the tables together.
        AL_SetProperty("Follow Actor ID", i55ID)
        AL_SetProperty("Follow Actor ID", iSX399ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        
        --Party members.
        AC_SetProperty("Set Party", 1, "55")
        AC_SetProperty("Set Party", 2, "SX-399")
        
        --Fold party.
        fnAutoFoldParty()
        fnCutsceneBlocker()
    
    --No-SX variant:
    else
    
        --Spawn characters.
        fnSpecialCharacter("55", "Doll",-100, -100, gci_Face_North, false, nil)
    
        --Position Christine.
        fnCutsceneTeleport("Christine", 17.75, 11.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Start moving while we fade in.
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(125)
        fnCutsceneBlocker()
        
        --55.
        fnCutsceneTeleport("55", 6.75, 16.25)
        fnCutsceneBlocker()
        fnCutsceneMove("55",  7.75, 15.50)
        fnCutsceneMove("55", 18.75, 15.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 18.25, 11.50)
        fnCutsceneMove("Christine", 18.25, 15.50)
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What were you looking at?[BLOCK][CLEAR]") ]])
        if(iChristineCorruptedEnding == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Generating falsehood...)[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Trying to send a message to Sophie.[SOFTBLOCK] The network is sporadic, I'm not sure if it got through.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You were careful to avoid revealing this position?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's not listed on the network anyway.[SOFTBLOCK] There's already bouncers in place.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] The code was written by a very clever and extremely thorough person.[SOFTBLOCK] It's quite impressive.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am a likely candidate.[BLOCK][CLEAR]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Yes, 55![SOFTBLOCK] That's what I was implying![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Oh.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Thank you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Anytime.[SOFTBLOCK] Shall we be off?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Lead the way.[SOFTBLOCK] We should not keep the administrator waiting.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Actually, 55.[SOFTBLOCK] Before we go, there's one question I have.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Ask.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What would you have done if my systems had been compromised?[SOFTBLOCK] What if we did not defeat the loyalty program?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yeah...[SOFTBLOCK] you hacked the program, didn't you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I did not.[SOFTBLOCK] My sister was too close, the probability of being discovered was far too high.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What little information on it I could gather from scraps of command unit emails indicate it preys on emotional flaws.[SOFTBLOCK] I had no choice but to trust in you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] So then the part of me that fought back, held on to what I believed in, and ultimately saved me...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Chose to display itself as my best friend, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Interesting.[SOFTBLOCK] Your memories refused to be purged and disguised themselves as me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] I feel...[SOFTBLOCK] exuberant.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So what was the plan if I was compromised?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Detonate the explosives I have hidden next to my power core.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Come again?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] While you were removing the door to the Epsilon labs, I acquired a set of explosives that were placed there.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The explosives were meant to contain Vivify and her minions in the lab.[SOFTBLOCK] Should they have broken out, the explosives were to be detonated by the security team there.[SOFTBLOCK] But the garrison was wiped out.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you had failed to stop Vivify, I was to destroy myself and her.[SOFTBLOCK] She is durable, but a thermobaric explosion would likely be enough.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm not so sure about that...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Nor am I, but I was prepared to make that sacrifice.[SOFTBLOCK] These are the most powerful non-nuclear weapons the Cause of Science has available.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In any case, that was part of the deal I made with my sister.[SOFTBLOCK] It would be infinitely better to expose the city to the administrator's ministrations than Vivify's, given the choice.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But since I did, you still have the explosives?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] If you were compromised, I would wait for the meeting with the administrator and then detonate them even if subdued.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I can activate the detonation discreetly and distract you through conversation.[SOFTBLOCK] Then, destroy the administrator and whatever building she is in, as well as a few sectors in every direction.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And yourself...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The smallest price to pay, Christine.[SOFTBLOCK] Do you remember what I said at the LRT facility?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That you were a living corpse, to be directed by me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I died in Cryogenics.[SOFTBLOCK] I was prepared to die again, but this time for a cause I believe in.[SOFTBLOCK] I still believe that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] But since you were not compromised?[SOFTBLOCK] I suppose I will have to live with myself.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh how awful for you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Jokes aside, if I give you the signal...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The administrator doesn't know what she's in for, and she doesn't have anywhere near enough security to stop us.[SOFTBLOCK] Don't blow yourself up.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Speaking of, we should probably get going.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Lead the way.") ]])
            fnCutsceneBlocker()
        
        --Corrupted.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, 55.[SOFTBLOCK] That's what I was implying![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Oh.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Thank you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Anytime.[SOFTBLOCK] Shall we be off?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Probability of suspicion -[SOFTBLOCK] very high.[SOFTBLOCK] Unit 2855 has a history of doubting her allies.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (I can subdue her myself if necessary.[SOFTBLOCK] The administrator is in no danger.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Lead the way.[SOFTBLOCK] We should not keep the administrator waiting.") ]])
            fnCutsceneBlocker()
        
        end
    
        --Add everyone to Christine's party.
        EM_PushEntity("55")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        gsFollowersTotal = 1
        gsaFollowerNames = {"55"}
        giaFollowerIDs   = {i55ID}

        --Append the tables together.
        AL_SetProperty("Follow Actor ID", i55ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        
        --Party members.
        AC_SetProperty("Set Party", 1, "55")
        
        --Fold party.
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
    end
end
