--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToFinaleA") then
    AL_BeginTransitionTo("RegulusFinaleA", "FORCEPOS:6.5x16.0x0")
    
elseif(sObjectName == "ToFinaleC") then
    AL_BeginTransitionTo("RegulusFinaleC", "FORCEPOS:9.5x4.0x0")
    
--[Objects]
elseif(sObjectName == "Windows") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The servers that handle the reprogramming algorithm...[SOFTBLOCK] I'm torn between capturing them for study, and blowing them up.)") ]])
    fnCutsceneBlocker()

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end