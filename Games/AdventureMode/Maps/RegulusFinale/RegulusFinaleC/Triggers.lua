--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Debug catch script. Positions Christine, sets variables, adds party members.
if(sObjectName == "Debug") then
    
    --Variables:
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --55:
    local i55ID = fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_North, false, nil)
    gsFollowersTotal = 1
    gsaFollowerNames = {"55"}
    giaFollowerIDs   = {i55ID}
    AL_SetProperty("Follow Actor ID", i55ID)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    AC_SetProperty("Set Party", 1, "55")
    
    --SX-399:
    if(iSX399JoinsParty == 1.0) then 
        LM_ExecuteScript(gsRoot .. "FormHandlers/SX399/Form_SteamDroid.lua")
        local iSX399ID = fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
        gsFollowersTotal = 2
        gsaFollowerNames = {"55", "SX399"}
        giaFollowerIDs   = {i55ID, iSX399ID}
        AL_SetProperty("Follow Actor ID", iSX399ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        AC_SetProperty("Set Party", 2, "SX-399")
    end
    
    --Teleport all three characters.
    fnCutsceneTeleport("Christine", 9.25, 12.50)
    fnCutsceneTeleport("55", 9.25, 12.50)
    fnCutsceneTeleport("SX399", 9.25, 12.50)
    
    --Fold party.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Set variables.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N", 2.0)

end