--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "AirlockDoorEN") then
    AL_SetProperty("Close Door", "DoorS")
    
elseif(sObjectName == "AirlockDoorES") then
    AL_SetProperty("Close Door", "DoorN")
    
elseif(sObjectName == "AirlockDoorWN") then
    AL_SetProperty("Close Door", "DoorSW")
    
elseif(sObjectName == "AirlockDoorWS") then
    AL_SetProperty("Close Door", "DoorNW")

elseif(sObjectName == "MagazineA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Seventeen Things YOU can do to help the Rebel Cause'.[SOFTBLOCK] A pamphlet we had made.[SOFTBLOCK] They're being distributed all across the city right now.)") ]])

elseif(sObjectName == "MagazineB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Butt Monthly'.[SOFTBLOCK] Well, gotta look at something while you wait for the tram.)") ]])

elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Listings of which units use this domicile block, and who is currently defragmenting.[SOFTBLOCK] It seems the entire sector is awake right now.)") ]])

elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Emergency conversion pod.[SOFTBLOCK] Organics caught in a vacuum are taken here and transformed into golems if the alternative is death.)") ]])

elseif(sObjectName == "SignA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('On sale now, all prices slashed![SOFTBLOCK] Keep your work credits, just please buy my inventory before I literally explode![SOFTBLOCK] Psycho Sally's Insane Deals Emporium -[SOFTBLOCK] Just east in sector 45 at address 771-6.xv8!')") ]])
    
elseif(sObjectName == "SignB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Floor level -[SOFTBLOCK] Sector 165 Light Manufacturing, Clothes, Appliances.')") ]])
    
elseif(sObjectName == "SignC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('First level -[SOFTBLOCK] Sector 165 eatery and public recharging stations.')") ]])
    
elseif(sObjectName == "SignD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Organic citizens::[SOFTBLOCK] No access to sector 165 without vac-suit.[SOFTBLOCK] Inquire at information kiosk below.')") ]])
    
elseif(sObjectName == "Window") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Just behind the window are clothes and mannequins sporting them from sector 165's fashion boutiques.)") ]])

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end