--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
	
    --Characters.
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] The rebels have an excellent propaganda arm, I will admit.[SOFTBLOCK] This is the first time I've seen them take over the videograph displays.") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] These rebels seem much better armed than the usual mavericks.[SOFTBLOCK] They must be collaborating with someone...") ]])
        
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We'll get this area cleared as soon as possible, leave it to us!") ]])
        
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Stupid rebels![SOFTBLOCK] Slaves should know their place![SOFTBLOCK] The administration has given us so much, and they're going to ruin it for the rest of us!") ]])
        
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Ooh, this is bad.[SOFTBLOCK] I don't want to get hurt...[SOFTBLOCK] Why is this happening?") ]])
        
    elseif(sActorName == "GolemF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Are those the new DB-16 Mk2 Pulse Repeaters?[SOFTBLOCK] Where did the rebels get those?[SOFTBLOCK] Uh, you know, I'm kind of a hobbyist...") ]])
        
    elseif(sActorName == "GolemG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You can't stop us, bootlicker![SOFTBLOCK] This is our time![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] We're maverick sympathizers, actually.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] You are?[SOFTBLOCK] Command units are joining the rebels?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] We shall all be equal in the new society.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Yeah![SOFTBLOCK] We can do it if we work together!") ]])
        
    elseif(sActorName == "GolemH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] That command unit took a hit and her squad just ditched her in here.[SOFTBLOCK] We'll take her prisoner and get her to safety as soon as sarge gets back.") ]])
        
    elseif(sActorName == "GolemI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Commander 2855![SOFTBLOCK] So far, we've been received very well by the populace, and we even have a few volunteers![SOFTBLOCK] We've got a real shot at this!") ]])
        
    elseif(sActorName == "DroneA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] HELLO, COMMANDER 2855.[SOFTBLOCK] I HAVE JOINED THE REBELLION, AND TURNED MY INHIBITOR CHIP OFF, BUT I CAN'T FIGURE OUT HOW TO RESET THE VOCAL SYNTHESIZER.[SOFTBLOCK] THE STREET AHEAD IS CLEAR, YOU MAY PROCEED.") ]])
        
    elseif(sActorName == "DroneB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS DRONE HAS LOST ITS LORD UNIT.[SOFTBLOCK] PROCEEDING TO NEAREST SECURITY STATION FOR REASSIGNMENT.") ]])
    
    --[Cassandra!]
    elseif(sActorName == "Cassandra") then
    
        --Variables.
        local iResolvedCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
        local iSX399JoinsParty            = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        local iCassandraSpokenToPostPanic = VM_GetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenToPostPanic", "N")
        
        --Cassandra is a golem:
        if(iResolvedCassandraQuest == 1.0 or iResolvedCassandraQuest == 3.0) then
            
            --First pass:
            if(iCassandraSpokenToPostPanic == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenToPostPanic", "N", 1.0)
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Golem") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Greetings, Units 2855 and 771852.[BLOCK][CLEAR]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: And...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] SX-399.[SOFTBLOCK] Steam Droid heavy support.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] In training.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I see.[SOFTBLOCK] Christine was responsible for converting me.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Me too![SOFTBLOCK] Kinda![SOFTBLOCK] It's a long story.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] [EMOTION|SX-399|Neutral]Great to see you again, 771853![BLOCK][CLEAR]") ]])
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Great to see you again, 771853![BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Have you decided to join our cause?[SOFTBLOCK] I sent you several covert requests but you did not reply.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: No, but do not be offended.[SOFTBLOCK] I have no allegiance to the administration.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am not offended.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What she means is, she was worried you might rat on us.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Unfounded.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] One must be cautious when outnumbered.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So then what are you doing here?[SOFTBLOCK] We're across the city from sector 15.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I am making my way to the teleportarium complex two sectors over.[SOFTBLOCK] I intend to leave the city.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Well the city is kind of blowing up right now...[BLOCK][CLEAR]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] That's the fun part![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not when the explosions are directed at oneself.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Yeah...[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: It is not for that reason.[SOFTBLOCK] If I could, I would stay and join the revolution.[SOFTBLOCK] You did me a great favour by converting me, Christine.[SOFTBLOCK] I would follow you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: But I have had visions while performing my function assignment.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Visions?[SOFTBLOCK] Like, daydreams?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I have spoken to repair units and there is nothing wrong with my hardware.[SOFTBLOCK] Yet these visions persist.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: They last between seven and twelve seconds and depict a building with red crystals growing from the rock.[SOFTBLOCK] I see myself, looking at me and smiling, and the vision ends.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I must find the place this occurred.[SOFTBLOCK] It is somewhere on Pandemonium, and I must find it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] So you're going to Pandemonium all alone?[BLOCK][CLEAR]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Won't your core run out of power eventually?[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I can recharge my core with organic foodstuffs and disguise myself to blend in with the inhabitants.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Repairs will be difficult to manage, but I must solve this conundrum.[SOFTBLOCK] I must.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] So much for talking you out of it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good luck, 771853.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Thank you.[SOFTBLOCK] I may return to the city when I have solved this mystery.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Please make it the utopia we both know it can be.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well I wasn't going to, but since you asked...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: May our ways cross again, Christine.") ]])
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Golem") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I will be moving out soon.[SOFTBLOCK] I was just listening to the maverick journalist report.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: You have good people on your side, Christine.[SOFTBLOCK] You will succeed.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks, Cassandra.[SOFTBLOCK] I'm sure you will, too.") ]])
            end
        
        --Cassandra is a human:
        else
            
            --First pass:
            if(iCassandraSpokenToPostPanic == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenToPostPanic", "N", 1.0)
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Christine![SOFTBLOCK] 55![SOFTBLOCK] And -[SOFTBLOCK] Wow, SX, looking good![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Flirt] You know it, Cassandra![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You've met?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Cassandra is a bit of a celebrity among the steam droids.[SOFTBLOCK] A rogue human who escaped from Regulus City, battled her way through the mines, dragging a damaged steam droid with her?[SOFTBLOCK] Classic.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Impressive.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I was only dragging her because her ankle broke off...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Doesn't matter![SOFTBLOCK] Great to see you're alive![BLOCK][CLEAR]") ]])
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Christine![SOFTBLOCK] 2855![SOFTBLOCK] Great to see you're alive![BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Apologies for not following up on you, but, well, we got a little busy.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I transformed into a doll...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Command Unit.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We fought some creepy monsters, blew up some stuff...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] [EMOTION|2855|Neutral]And all I can say is you should probably vacate this sector soon.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Why?[SOFTBLOCK] Actually, don't tell me.[SOFTBLOCK] I'm leaving soon anyway.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Just got to purloin a vac suit and I can head to the teleportarium complex a few sectors over.[BLOCK][CLEAR]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Are you going back to Pandemonium?[BLOCK][CLEAR]") ]])
                else
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Teleportarium?[SOFTBLOCK] To go to Pandemonium?[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I'd love to stay and help liberate the city, really, I would.[SOFTBLOCK] I've got a lot of steam droid friends who deserve better.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: But...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Your vulnerability to extreme environments is not useful to the cause?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your organic synapses are too slow to participate in combat?[BLOCK][CLEAR]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You might ruin that lovely top?[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: No![SOFTBLOCK] Well, sort of.[SOFTBLOCK] But...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Every night, just before I bed, I have a vision, Christine.[SOFTBLOCK] It changes but I always have it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Like a hallucination?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Yeah![SOFTBLOCK] But the doctor said there's nothing wrong with me.[BLOCK][CLEAR]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You know she's a veterinarian by training, right?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: ... Big oof, as my contact says... but still.[BLOCK][CLEAR]") ]])
                end
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I know the vision is connected to a place on Pandemonium.[SOFTBLOCK] I think I even know where it is.[SOFTBLOCK] So, I'm going to find it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: It's a room with red crystals growing out of the moldy brick walls.[SOFTBLOCK] I see myself, but I'm staring at me and smiling.[SOFTBLOCK] Sometimes I laugh.[SOFTBLOCK] And then it ends.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Besides, there's not much good I can do here, but maybe I can spread the word planetside.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Most of the inhabitants of Pandemonium are not even aware that Regulus City exists.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The most they can see of it is a small blinking light on the moon.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Yeah...[SOFTBLOCK] Well, that's all in the future.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I think it's great.[SOFTBLOCK] You do what you need to.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And someday, we'll meet again.[SOFTBLOCK] If Regulus City is freed, there will be a place here for you if you want it.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Thanks, Christine.[SOFTBLOCK] For everything.") ]])
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: That maverick journalism there...[SOFTBLOCK] That's bravery.[SOFTBLOCK] To go into a war armed with a camera and the truth.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What they do is just as important as the units with the guns.[SOFTBLOCK] This must be documented, it must be seen.") ]])
            end
        
        end
    end
end