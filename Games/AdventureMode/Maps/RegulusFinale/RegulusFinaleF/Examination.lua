--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToFinaleE") then
    AL_BeginTransitionTo("RegulusFinaleE", "FORCEPOS:33.0x10.0x0")
    AudioManager_PlaySound("World|AutoDoorOpen")
    
--[Objects]
elseif(sObjectName == "AirlockN") then
    AL_SetProperty("Close Door", "DoorS")
    
elseif(sObjectName == "AirlockS") then
    AL_SetProperty("Close Door", "DoorN")
    
elseif(sObjectName == "Intercom") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's no answer on the intercom.[SOFTBLOCK] Information services has probably fled their posts.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Junk") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The sector puts its junk in here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Doll") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A command unit prisoner in system standby.[SOFTBLOCK] Hopefully she doesn't put up a fight when they reactivate her.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Shoes") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unfortunately, these shoes are not compatible with the command unit chassis...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Please ask a slave unit if you need help installing new footwear modules.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Slave units must retain purchase receipts when purchasing items on behalf of their lord units.[SOFTBLOCK] Slave units are subject to searches at any time.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('FREEDOM IS COMING, RISE UP!!!' is written on the LED lights.[SOFTBLOCK] A vandal must have hacked it.[SOFTBLOCK] You go, robot girl!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignD") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Transit stations are experiencing unexpected delays.[SOFTBLOCK] Please use overland routes when possible.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignE") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Diary of a Swordrunner:: Unit 4992's Untold Story'.[SOFTBLOCK] Looks like an advertisment for someone's memoirs.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Cafe") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Unit 764952's Cafe'.[SOFTBLOCK] Not cleverly named, but apparently the superlube-soup is to die for.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "GameShop") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Video games get their own shops?[SOFTBLOCK] Ooh, there's a copy of Needlemouse Heroes!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ApplianceShop") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An appliances shop.[SOFTBLOCK] Now's probably not the time for a shopping spree.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ScreenL") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Some rebels, probably near the eastern edge of the city, taking cover.[SOFTBLOCK] There is a crawl on the bottom detailing how citizens can support them even if they choose not to fight.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ScreenR") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Rebels standing over a disabled lord unit, checking her for power core damage.[SOFTBLOCK] The crawl describes the philosophy of freedom and equality for all golems, and why ordinary citizens should join today.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyBlue") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Fizzy Pop! Blue, the taste of freedom!'[SOFTBLOCK] Seems someone vandalized the machine already.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyPink") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Fizzy Pop! Pink, FOR BOLTHEADS LOL REKT'...[SOFTBLOCK] Who wrote this?)") ]])
    fnCutsceneBlocker()
    
--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end