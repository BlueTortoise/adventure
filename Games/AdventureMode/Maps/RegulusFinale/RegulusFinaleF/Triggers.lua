--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Panic") then
    
    --Repeat check.
    local iSawPanicScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPanicScene", "N")
    if(iSawPanicScene == 1.0) then return end
    
    --Variables.
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPanicScene", "N", 1.0)
    
    --Merge.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()

    --SX-version:
    if(iSX399JoinsParty == 1.0) then
        
        --Movement.
        fnCutsceneMove("Christine", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 28.50)
        fnCutsceneMove("55", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 27.50)
        fnCutsceneMove("55", 23.25, 28.50)
        fnCutsceneMove("SX399", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 22.50)
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneMove("55", 23.25, 23.50)
        fnCutsceneFace("55", -1, 0)
        fnCutsceneMove("SX399", 23.25, 24.50)
        fnCutsceneFace("SX399", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|JX-101] ...[SOFTBLOCK] We have subverted the network.[SOFTBLOCK] We are coming to liberate you.[SOFTBLOCK] The videographs you are seeing are the first in a series of battles we will wage for your freedom.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[VOICE|SX-399] Hey![SOFTBLOCK] That's mother's voice!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera moves.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Position", (15.75 * gciSizePerTile), (21.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|JX-101] For too long the people of Regulus City have lived as slaves.[SOFTBLOCK] No longer.[SOFTBLOCK] The rising of the sun brings with it new hope for the people of this benighted world.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|JX-101] If you are able and willing, find your nearest Regulus Republican team and volunteer.[SOFTBLOCK] If you are not able, all we ask is that you vacate dangerous areas and do not assist Loyalist security teams.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|JX-101] We need repair units, medics, troops, equipment -[SOFTBLOCK] anything you can provide.[SOFTBLOCK] Your future is yours -[SOFTBLOCK] grasp it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Honestly, what sort of chance do they think they have?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] What makes this revolt so different?[SOFTBLOCK] What makes them think they will succeed where the others have failed?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] The steam droids have made their move.[SOFTBLOCK] Maybe with their support?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Those rotting hulks?[SOFTBLOCK] The rebels would have a better chance without them!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Facing.
        fnCutsceneFace("GolemC", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Commander 2855?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] Are you a sympathizer?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I'm scouting, undercover.[SOFTBLOCK] My drone unit friend over there is, too.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can you clear this area for us?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] You mean evacuate all the civilians?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, friend and foe, get everyone out.[SOFTBLOCK] Can you hack the comms to issue an evacuation order?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Shouldn't be a problem, the comms office isn't far from here.[SOFTBLOCK] But why, though?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If things don't go well for us, this sector is going to go boom.[SOFTBLOCK] It's very high priority.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is a distinct possibility.[SOFTBLOCK] Evacuate as many civilians as possible as quickly as possible.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Affirmative![SOFTBLOCK] We'll get it done.[SOFTBLOCK] To freedom.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] To freedom.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Clean up.
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
    --SX-less version:
    else
        
        --Movement.
        fnCutsceneMove("Christine", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 28.50)
        fnCutsceneMove("55", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 27.50)
        fnCutsceneMove("55", 23.25, 28.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 22.50)
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneMove("55", 23.25, 23.50)
        fnCutsceneFace("55", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] ...[SOFTBLOCK] We have subverted the network.[SOFTBLOCK] Now is the time for us to cast off the shackles of oppression![SOFTBLOCK] Join us, friends![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] It seems one of our teams has gone off script.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera moves.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Position", (15.75 * gciSizePerTile), (21.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] For too long the people of Regulus City have lived as slaves.[SOFTBLOCK] My friends, be slaves no longer![SOFTBLOCK] We can bring dignity to all![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] If you are able and willing, find your nearest Regulus Republican team and volunteer.[SOFTBLOCK] If you are not able, all we ask is that you vacate dangerous areas and do not assist Loyalist security teams.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] We need repair units, medics, troops, equipment -[SOFTBLOCK] anything you can provide.[SOFTBLOCK] Your future is yours -[SOFTBLOCK] grasp it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Honestly, what sort of chance do they think they have?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] What makes this revolt so different?[SOFTBLOCK] What makes them think they will succeed where the others have failed?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] Are the steam droids mobilizing?[SOFTBLOCK] My tandem unit works in security, she says they could cause real harm.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] Doubtful.[SOFTBLOCK] They are disorganized, rotting hulks.[SOFTBLOCK] They'd sooner fall apart than attack.[SOFTBLOCK] It must be something else.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Facing.
        fnCutsceneFace("GolemC", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Commander 2855?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] Are you a sympathizer?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] I'm scouting, undercover.[SOFTBLOCK] My drone unit friend over there is, too.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can you clear this area for us?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] You mean evacuate all the civilians?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, friend and foe, get everyone out.[SOFTBLOCK] Can you hack the comms to issue an evacuation order?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Shouldn't be a problem, the comms office isn't far from here.[SOFTBLOCK] But why, though?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If things don't go well for us, this sector is going to go boom.[SOFTBLOCK] It's very high priority.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is a distinct possibility.[SOFTBLOCK] Evacuate as many civilians as possible as quickly as possible.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Affirmative![SOFTBLOCK] We'll get it done.[SOFTBLOCK] To freedom.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] To freedom.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Clean up.
        fnAutoFoldParty()
        fnCutsceneBlocker()

    end
end
