--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "SignA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Hazardous waste sarcophogus L-22-z.[SOFTBLOCK] No entry without protective equipment and permission from central administration.')") ]])
    
elseif(sObjectName == "Terminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Offsite waste disposal queue -[SOFTBLOCK] error, unable to load.[SOFTBLOCK] Estimated time to sarcophogus decommission -[SOFTBLOCK] error, unable to load.')") ]])

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end