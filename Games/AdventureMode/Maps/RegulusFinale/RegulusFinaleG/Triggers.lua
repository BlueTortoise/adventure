--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "TheEnd") then
    
    --Variables:
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Cannot trigger twice.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --SX-399 version:
    if(iSX399JoinsParty == 1.0) then
    
        --Movement.
        fnCutsceneMove("Christine", 4.75, 5.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneMove("55", 3.75, 5.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneMove("SX399", 5.75, 5.50)
        fnCutsceneFace("SX399", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This is it.[SOFTBLOCK] 55?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have the chip ready.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Make sure it's broadcasting when we enter the elevator.[SOFTBLOCK] There's also a series of passcodes, leave that to me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Administrator] That will not be necessary.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] It came from the access panel...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Who are you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The administrator...[SOFTBLOCK] She's here...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Administrator] Correct.[SOFTBLOCK] I have disabled the security for you.[SOFTBLOCK] I see no reason to drag this out any further.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Administrator] Please, come inside.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Well you heard the lady.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fade out.
        fnCutsceneWait(125)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Sound effect.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneBlocker()
        
        --Level transition.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFinaleH", "FORCEPOS:15.0x62.0x0") ]])
        fnCutsceneBlocker()
    
    --No SX-399 variant.
    else
    
        --Movement.
        fnCutsceneMove("Christine", 4.75, 5.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneMove("55", 3.75, 5.50)
        fnCutsceneFace("55", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This is it.[SOFTBLOCK] 55?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have the chip ready.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Make sure it's broadcasting when we enter the elevator.[SOFTBLOCK] There's also a series of passcodes, leave that to me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Administrator] That will not be necessary.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Who are you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The administrator...[SOFTBLOCK] She's here...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Administrator] Correct.[SOFTBLOCK] I have disabled the security for you.[SOFTBLOCK] I see no reason to drag this out any further.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Administrator] Please, come inside.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Come along, no dawdling.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fade out.
        fnCutsceneWait(125)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Sound effect.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneBlocker()
        
        --Level transition.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFinaleH", "FORCEPOS:15.0x62.0x0") ]])
        fnCutsceneBlocker()
    
    end
    
--Debug catch script. Positions Christine, sets variables, adds party members.
elseif(sObjectName == "Debug") then
    
    --Variables:
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --55:
    local i55ID = fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_North, false, nil)
    gsFollowersTotal = 1
    gsaFollowerNames = {"55"}
    giaFollowerIDs   = {i55ID}
    AL_SetProperty("Follow Actor ID", i55ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    
    --SX-399:
    if(iSX399JoinsParty == 1.0) then 
        local iSX399ID = fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
        gsFollowersTotal = 2
        gsaFollowerNames = {"55", "SX399"}
        giaFollowerIDs   = {i55ID, iSX399ID}
        AL_SetProperty("Follow Actor ID", iSX399ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
    end
    
    --Teleport all three characters.
    fnCutsceneTeleport("Christine", 25.25, 16.50)
    fnCutsceneTeleport("55", 25.25, 16.50)
    fnCutsceneTeleport("SX399", 25.25, 16.50)
    
    --Fold party.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Set variables.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N", 2.0)
    
--Debug catch script. Positions Christine, sets variables, adds party members. This variant has no SX-399.
elseif(sObjectName == "DebugNoSX") then
    
    --Variables:
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N", 0.0)
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --55:
    local i55ID = fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_North, false, nil)
    gsFollowersTotal = 1
    gsaFollowerNames = {"55"}
    giaFollowerIDs   = {i55ID}
    AL_SetProperty("Follow Actor ID", i55ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    
    --SX-399:
    if(iSX399JoinsParty == 1.0) then 
        local iSX399ID = fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
        gsFollowersTotal = 2
        gsaFollowerNames = {"55", "SX399"}
        giaFollowerIDs   = {i55ID, iSX399ID}
        AL_SetProperty("Follow Actor ID", iSX399ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
    end
    
    --Teleport all three characters.
    fnCutsceneTeleport("Christine", 25.25, 16.50)
    fnCutsceneTeleport("55", 25.25, 16.50)
    fnCutsceneTeleport("SX399", 25.25, 16.50)
    
    --Fold party.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Set variables.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N", 2.0)
    
--Debug catch for corrupted ending.
elseif(sObjectName == "DebugBad") then
    
    --Variables:
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --55:
    local i55ID = fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_North, false, nil)
    gsFollowersTotal = 1
    gsaFollowerNames = {"55"}
    giaFollowerIDs   = {i55ID}
    AL_SetProperty("Follow Actor ID", i55ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    
    --SX-399:
    if(iSX399JoinsParty == 1.0) then 
        local iSX399ID = fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
        gsFollowersTotal = 2
        gsaFollowerNames = {"55", "SX399"}
        giaFollowerIDs   = {i55ID, iSX399ID}
        AL_SetProperty("Follow Actor ID", iSX399ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
    end
    
    --Teleport all three characters.
    fnCutsceneTeleport("Christine", 25.25, 16.50)
    fnCutsceneTeleport("55", 25.25, 16.50)
    fnCutsceneTeleport("SX399", 25.25, 16.50)
    
    --Fold party.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Set variables.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N", 2.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCorruptionCount", "N", 4.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCorruptionFails", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N", 1.0)
    
--Debug catch for corrupted ending. No SX-399.
elseif(sObjectName == "DebugBadNoSX") then
    
    --Variables:
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N", 0.0)
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --55:
    local i55ID = fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_North, false, nil)
    gsFollowersTotal = 1
    gsaFollowerNames = {"55"}
    giaFollowerIDs   = {i55ID}
    AL_SetProperty("Follow Actor ID", i55ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    
    --SX-399:
    if(iSX399JoinsParty == 1.0) then 
        local iSX399ID = fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
        gsFollowersTotal = 2
        gsaFollowerNames = {"55", "SX399"}
        giaFollowerIDs   = {i55ID, iSX399ID}
        AL_SetProperty("Follow Actor ID", iSX399ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        fnCutsceneTeleport("SX399", 25.25, 16.50)
    end
    
    --Teleport all three characters.
    fnCutsceneTeleport("Christine", 25.25, 16.50)
    fnCutsceneTeleport("55", 25.25, 16.50)
    
    --Fold party.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Set variables.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N", 2.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCorruptionCount", "N", 4.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCorruptionFails", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N", 1.0)
end
