--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusFinaleH"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "Null")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusFinaleH")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

	--[Lights]
    AL_SetProperty("Activate Lights")
    
    --[Loading]
    --Loads if the images aren't already loaded.
    if(DL_Exists("Root/Images/AdventureUI/MapUnderlays/ServerBanks") == false) then
        
        --Underlays use linear filtering.
        local iLinear = DM_GetEnumeration("GL_LINEAR")
        ALB_SetTextureProperty("MagFilter", iLinear)
        ALB_SetTextureProperty("MinFilter", iLinear)

        --Underlays need to wrap.
        local iRepeatEnum = DM_GetEnumeration("GL_REPEAT")
        ALB_SetTextureProperty("Wrap S", iRepeatEnum)
        ALB_SetTextureProperty("Wrap T", iRepeatEnum)

        --Load.
        SLF_Open(gsDatafilesPath .. "UIAdventure.slf")
        DL_AddPath("Root/Images/AdventureUI/MapUnderlays/")
        DL_ExtractBitmap("Underlay|ServerBanks", "Root/Images/AdventureUI/MapUnderlays/ServerBanks")

        --Return to normal.
        ALB_SetTextureProperty("Restore Defaults")
        SLF_Close()
    end

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    
    --Backgrounds.
    local fMoveScale = -0.30
    local fMapScale = 0.50
    local fCloudScale = 0.50
    local fCloudScroll = 0.05
    AL_SetProperty("Allocate Backgrounds", 1)
    AL_SetProperty("Background Image",          0, "Root/Images/AdventureUI/MapUnderlays/ServerBanks")
    AL_SetProperty("Background Render Offsets", 0, 500.0, 500.0, fMoveScale, fMoveScale)
    AL_SetProperty("Background Alpha",          0, 1.0, 0)
    AL_SetProperty("Background Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Background Scale",          0, -fMapScale)
    
    --Layers.
    AL_SetProperty("Set Layer Disabled", "DisabledTerminal", true)
    AL_SetProperty("Set Layer Disabled", "Admin1", true)
    AL_SetProperty("Set Layer Disabled", "Admin2", true)
    AL_SetProperty("Set Layer Disabled", "Admin3", true)
    AL_SetProperty("Set Layer Disabled", "Admin4", true)
    AL_SetProperty("Set Layer Disabled", "Admin5", true)
    AL_SetProperty("Set Layer Disabled", "Admin6", true)
    AL_SetProperty("Set Layer Disabled", "Admin7", true)
end
