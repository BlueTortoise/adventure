--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Objects]
if(sObjectName == "Closet") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It's full of women's clothes.[SOFTBLOCK] A label on the outside says 'Goth Phase'.)") ]])
    
elseif(sObjectName == "Bed") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A bed.[SOFTBLOCK] It's too small to be mine.[SOFTBLOCK] Mine would be four times this size!)") ]])
    
elseif(sObjectName == "Bookshelf") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (All the titles are blank, and the book's pages are filled with nonsense about cleaning windows and bears eating porridge.)") ]])
    
elseif(sObjectName == "Plant") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It's a plant.[SOFTBLOCK] I don't know what species.)") ]])
    
elseif(sObjectName == "Chair") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It's a chair.[SOFTBLOCK] Often known as objects one can sit on, but never underestimate their potency as a projectile.)") ]])
    
elseif(sObjectName == "EndTable") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An end table, not placed next to a chesterfield or a bed.[SOFTBLOCK] This room's Feng Shui is terrible.)") ]])
    
elseif(sObjectName == "CoatRack") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A coat rack covered in a drape.[SOFTBLOCK] There's a thick brown leather trenchcoat under it.)") ]])

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end