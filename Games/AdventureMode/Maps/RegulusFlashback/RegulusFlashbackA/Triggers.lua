--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "FlashbackWakeup") then
    
    --Disable warps.
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
    
    --Repeat check.
    local iFlashbackTrueWakeup = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N")
    if(iFlashbackTrueWakeup == 1.0) then return end
    
    --Variables:
    local b55Exists = EM_Exists("55")
    local bSX399Exists = EM_Exists("SX399")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N", 1.0)
    
    --Position Christine. All other characters are offscreen.
    fnCutsceneTeleport("Christine", 6.75, 4.50)
    fnCutsceneFace("Christine", 0, -1)
    if(b55Exists == true) then fnCutsceneTeleport("55", -1.75, -1.50) end
    if(iSX399JoinsParty == 1.0 and bSX399Exists) then fnCutsceneTeleport("SX399", -1.75, -1.50) end
    
    --Remove characters from the party.
    gsFollowersTotal = 0
    gsaFollowerNames = {}
    giaFollowerIDs = {0}
	AL_SetProperty("Unfollow Actor Name", "55")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
    local iSlot = AC_GetProperty("Character Party Slot", "55")
    if(iSlot > -1) then AC_SetProperty("Set Party", iSlot, "Null") end
    if(iSX399JoinsParty == 1.0) then 
        AL_SetProperty("Unfollow Actor Name", "SX399") 
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
        iSlot = AC_GetProperty("Character Party Slot", "SX-399")
        if(iSlot > -1) then AC_SetProperty("Set Party", iSlot, "Null") end
    end
    
    --Black screen.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Remove Christine's current special frame.
    fnCutsceneSetFrame("Christine", "NULL")
    
    --Sounds.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(55)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] No, it's not in here either.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] If I knew what I was looking for, it'd be so much easier to find.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sounds.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(55)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Well, at least I found out my name is Christine.[SOFTBLOCK] Good thing that text box appeared as I was speaking to myself.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Wait, text box?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] There's a text box at the bottom of the screen?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] There's a screen?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Oh, yes, that makes sense.[SOFTBLOCK] I must be inside a program.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] There's also a door over there.[SOFTBLOCK] Maybe I'll find whatever it was I was looking for over there?") ]])
    fnCutsceneBlocker()
    
end
