--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = "Nobody"
if(sTopicName == "Hello") then
    sActorName = TA_GetProperty("Name")
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "Kernel") then
        
        --Variables
        local iFlashbackKernelBriefing = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N")
        local iFlashbackMet55          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N")
        
        --Kernel telling you to go become a doll:
        if(iFlashbackKernelBriefing == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] Head into the northwest door, there.[SOFTBLOCK] I'll be waiting in the western room.[SOFTBLOCK] Don't keep me waiting!") ]])
        
        --Kernel telling you what to do, broadly:
        elseif(iFlashbackMet55 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] The northeast door leads to your memories of Earth.[SOFTBLOCK] Most of those are ready, but there are a few repressed ones we're working on.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] The southest door leads to your memories of Regulus City.[SOFTBLOCK] The administrator will have great interest in those, and they largely survived intact.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] But the southwest door is where we've put all the memories we can't figure out.[SOFTBLOCK] That's where we'll need your help.") ]])
        
        --Having met 55:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] Is something the matter, 771852?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[VOICE|Christine] My repressed memories from Earth.[SOFTBLOCK] Where are they kept?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] Northeast door, head east and down the steps.[SOFTBLOCK] Or don't, because they are repressed due to emotional trauma.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] Activating a repressed memory requires activating an emotional trauma along with it, which means unlocking the containment area may cause an emotional destabilization.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] We can deal with those later.[SOFTBLOCK] Out analysis indicates we have no need of them, our priority is waking up to report to the administrator.") ]])
        
        end
        
    elseif(sActorName == "DollA") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Program:[VOICE|Doll] Sheesh, so many repressed memories of doctors visits.[SOFTBLOCK] We were such cowards when it came to needles!") ]])
        
    elseif(sActorName == "DollB") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Program:[VOICE|Doll] What is this game with a blue hedgehog we played so much?[SOFTBLOCK] Hmm, shouldn't that go with the Needlemouse memories?") ]])
        
    elseif(sActorName == "DollC") then
        
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Program:[VOICE|Doll] Memories are records, but the imagination is responsible for re-playing them.[SOFTBLOCK] I'm a program that synthesizes objects that appear in memories.") ]])
    
    --Debug Raiju:
    elseif(sActorName == "DebugRaiju") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|Narissa] Hello![SOFTBLOCK] I'm a debug NPC.[SOFTBLOCK] Please make a choice.[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Memory Meet 55\", " .. sDecisionScript .. ", \"Debug|Meet55\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Memory Begin Ending\", " .. sDecisionScript .. ", \"Debug|MemoryEnding\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Exterior A\", " .. sDecisionScript .. ", \"Debug|ExteriorA\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Exterior B\", " .. sDecisionScript .. ", \"Debug|ExteriorB\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Exterior C\", " .. sDecisionScript .. ", \"Debug|ExteriorC\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Skip To Run\", " .. sDecisionScript .. ", \"Debug|SkipRun\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Skip To Ending\", " .. sDecisionScript .. ", \"Debug|Ending\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Skip To Ending No SX\", " .. sDecisionScript .. ", \"Debug|EndingNoSX\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Skip To Ending Bad\", " .. sDecisionScript .. ", \"Debug|EndingBad\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Skip To Ending Bad No SX\", " .. sDecisionScript .. ", \"Debug|EndingBadNoSX\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Quick Finish Chapter\", " .. sDecisionScript .. ", \"Debug|FinishChapter\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
    end

--Debug: Skip to the room 55 is in.
elseif(sTopicName == "Debug|Meet55") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFlashbackI", "FORCEPOS:10.0x18.0x0")

--Debug: Skip to the ending of the memory sequence.
elseif(sTopicName == "Debug|MemoryEnding") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFlashbackJ", "FORCEPOS:7.0x10.0x0")

--Debug: Skip to the first part of Christine waking up.
elseif(sTopicName == "Debug|ExteriorA") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFinaleA", "FORCEPOS:24.0x4.0x0")

--Debug: Skip to the second part of Christine waking up.
elseif(sTopicName == "Debug|ExteriorB") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFinaleB", "FORCEPOS:18.0x10.0x0")

--Debug: Skip to the third part of Christine waking up.
elseif(sTopicName == "Debug|ExteriorC") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFinaleA", "FORCEPOS:13.0x19.0x0")

--Debug: Skip to after Christine has woken up, bypassing all cutscenes.
elseif(sTopicName == "Debug|SkipRun") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFinaleC", "FORCEPOS:15.0x8.0x0")

--Debug: Skip to the very end.
elseif(sTopicName == "Debug|Ending") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFinaleG", "FORCEPOS:9.0x10.0x0")
    
--Debug: Skip to the very end, no SX-399.
elseif(sTopicName == "Debug|EndingNoSX") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFinaleG", "FORCEPOS:13.0x10.0x0")

--Debug: Skip to the very end, bad ending.
elseif(sTopicName == "Debug|EndingBad") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFinaleG", "FORCEPOS:17.0x10.0x0")

--Debug: Skip to the very end, bad ending, no SX-399.
elseif(sTopicName == "Debug|EndingBadNoSX") then
	WD_SetProperty("Hide")
    AL_BeginTransitionTo("RegulusFinaleG", "FORCEPOS:21.0x10.0x0")

--Debug: Quickly finishes the chapter.
elseif(sTopicName == "Debug|FinishChapter") then
	WD_SetProperty("Hide")

    --Execute the cleaner script.
    fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
    fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Chapter5ZCleaner/000 Entry Point.lua") ]])
    fnCutsceneBlocker()

    --Return to the "Nowhere" map.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
    fnCutsceneBlocker()

--Debug: Skip to the very end.
elseif(sTopicName == "Cancel") then
	WD_SetProperty("Hide")

end