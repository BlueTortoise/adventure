--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToFlashbackC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusFlashbackC", "FORCEPOS:27.0x41.0x0")
    
elseif(sObjectName == "ToFlashbackD") then
    local iFlashbackSpokeToKernel = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N")
    if(iFlashbackSpokeToKernel == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I should find out what Kernel needs me to see first...)") ]])
        fnCutsceneBlocker()
    
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusFlashbackD", "FORCEPOS:6.0x25.0x0")
    end
    
elseif(sObjectName == "ToFlashbackE") then
    local iFlashbackSpokeToKernel = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N")
    if(iFlashbackSpokeToKernel == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I should find out what Kernel needs me to see first...)") ]])
        fnCutsceneBlocker()
    
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusFlashbackE", "FORCEPOS:4.0x15.0x0")
    end
    
elseif(sObjectName == "ToFlashbackF") then
    local iFlashbackSpokeToKernel = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N")
    if(iFlashbackSpokeToKernel == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I should find out what Kernel needs me to see first...)") ]])
        fnCutsceneBlocker()
    
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusFlashbackF", "FORCEPOS:33.0x4.0x0")
    end

--[Objects]
elseif(sObjectName == "NoteA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Descrambled original memories and interpersonal reference chamber.')") ]])
    
elseif(sObjectName == "NoteB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Childhood/Earth memories.')") ]])
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Can we get any confirmation on the goth phase?[SOFTBLOCK] Black lipstick or not?')") ]])
    
elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('For crying out loud, spell check your memories before you upload them![SOFTBLOCK] We were an English teacher![SOFTBLOCK] Come on!')") ]])

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end