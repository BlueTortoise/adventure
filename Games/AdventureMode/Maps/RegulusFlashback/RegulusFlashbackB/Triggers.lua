--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "MeetDolls") then
    
    --Repeat Chec:
    local iFlashbackMeetDolls = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N")
    if(iFlashbackMeetDolls == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] How are we doing on the early childhood?[SOFTBLOCK] Still got that scrambled section on the first doctor's visit?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] Huh?[SOFTBLOCK] Oh, look at those readings![SOFTBLOCK] Consciousness!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Kernel", 8.75, 9.50)
    fnCutsceneFace("Kernel", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|Kernel] Don't be shy, dear.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 8.75, 8.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, hello?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] Superb![SOFTBLOCK] Consciousness is -[SOFTBLOCK] awake![SOFTBLOCK] Ha hahah![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Something about that statement vaguely resembles a joke![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] Oh deary, Consciousness.[SOFTBLOCK] Erm, do you know where you are?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Some kind of a program?[SOFTBLOCK] You can see us there talking.[SOFTBLOCK] Is this real?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] It is![SOFTBLOCK] Well, that depends on how you define 'real'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] We are currently inside a maintenance program.[SOFTBLOCK] We were badly damaged, and are currently unconscious.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh.[SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] We're still piecing that together, actually.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] I'm sure you've figured it out by now, but you are the consciousness.[SOFTBLOCK] Though I suppose 'Christine' is easier to go by, since you make all the decisions.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But I can't remember anything...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] Yes, because we're trying to put back together all of our memories.[SOFTBLOCK] So until we do, you won't be able to access them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] We're working as hard as we can, but there are problems.[SOFTBLOCK] Big problems.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Who is 'we', exactly?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] Your subconscious, silly.[SOFTBLOCK] That's us![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] We're all various programs.[SOFTBLOCK] For example, I am a coordination kernel, and over here you have some of my subroutines.[SOFTBLOCK] Say hi, ladies!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Kernel", -1, 0)
    fnCutsceneFace("DollA", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|Doll] We're a mite busy over here, boss.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("DollA", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Kernel", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] Obviously, she's not going to have a 'friendly greeting' subroutine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So this is all a computer program?[SOFTBLOCK] But -[SOFTBLOCK] aren't I a human?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] No, but you seem to think of yourself as one.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] In fact, since you're here, that should be the first thing you fix.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] Please head through the door in the northwest, there.[SOFTBLOCK] Speak to -[SOFTBLOCK] me -[SOFTBLOCK] again, in the western room.[SOFTBLOCK] We'll get you fixed right up.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, do you have a name I can call you by?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[E|Neutral] Hm.[SOFTBLOCK] My dear, I am *you*.[SOFTBLOCK] So if you wanted to call me Christine...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh no way that'd be confusing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Spot of bother, that.[SOFTBLOCK] Well, perhaps Kernel would do?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Kernel it is![SOFTBLOCK] See you soon!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "KernelBriefing") then
    
    --Joke!
    local iFlashbackWrongDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWrongDoor", "N")
    if(iFlashbackWrongDoor == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWrongDoor", "N", 2.0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 1)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 1)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 13.25, 6.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Kernel, when I entered this door from the other side, it placed me in the incorrect position.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Whoops![SOFTBLOCK] Sorry about that.[SOFTBLOCK] I'll have that fixed right away.[SOFTBLOCK] Won't happen next time.") ]])
        fnCutsceneBlocker()
    
        return
    end
    
    --Repeat check.
    local iFlashbackSpokeToKernel  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N")
    local iFlashbackKernelBriefing = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N")
    if(iFlashbackSpokeToKernel == 0.0 or iFlashbackKernelBriefing == 1.0) then return end

    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 6.25, 7.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneMove("Kernel", 7.25, 7.50)
    fnCutsceneFace("Kernel", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] It's quite cramped in here, isn't it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Kernel, inform me.[SOFTBLOCK] What must I do to awaken?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Straight to the point?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I must be reunited with the administrator.[SOFTBLOCK] It has been decades.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I have so much to share about Earth.[SOFTBLOCK] Its people, its cultures, its technology.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Not without your memories, silly.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] ...[SOFTBLOCK] Oh yes, those.[SOFTBLOCK] Quite right.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Where do you need me to go?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] If you're in a rush, the door to our southwest is where I need you.[SOFTBLOCK] Speak to me there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] And the other doors?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] You can review some of the memories we've been working on.[SOFTBLOCK] They'll be uploaded soon, though we're cross-checking for contradictions.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Memory restoration is more art than science, sometimes.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Affirmative, Kernel.[SOFTBLOCK] I will proceed to the southwest.") ]])
    fnCutsceneBlocker()

--[Corruption Scenes]
--First corruption scene.
elseif(sObjectName == "CorruptionZero") then

    --Disable music.
    AL_SetProperty("Music", "Null")
    
    --Blackout.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Position.
    fnCutsceneTeleport("Christine", 9.25, 9.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 8.25, 9.50)
    fnCutsceneFace("Kernel", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] mppfpmffp-7.0-sdmbb.[SOFTBLOCK] 52.[SOFTBLOCK] 52?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unit 771852?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Uh-[SOFTBLOCK]unnh?[SOFTBLOCK] Hello? Kernel?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Oh good, you experienced a synaptic overload for a moment there.[SOFTBLOCK] Is everything okay?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] What happened?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] I touched a creature with a viewcone, like any other, but then there was a crudely animated static overlay and several stock sound effects overlaid over one another.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Hmmm, yes, it seems you descrambled a rogue memory file.[SOFTBLOCK] Good work.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I blacked out?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Evidently, I am seeing a brief gap in consciousness during the upload.[SOFTBLOCK] Oh dear.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Something related to the memory caused the issue, or the number of memories.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unfortunately, we're going to have to just endure that.[SOFTBLOCK] We can't waste time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] No.[SOFTBLOCK] We need to wake up and report to the administrator.[SOFTBLOCK] I will do whatever is required.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] What do you need me to do now?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Exactly what you just did.[SOFTBLOCK] Walk into a rogue memory file and descramble it for upload.[SOFTBLOCK] We'll make sure it gets where it needs to go.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] We'll need to do that a few more times before we have enough data to descrambled the rest of the files.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Affirmative.[SOFTBLOCK] Moving out.") ]])
    fnCutsceneBlocker()
    
    --Re-enable music.
    fnCutsceneInstruction([[ AL_SetProperty("Music", "ChristineDream") ]])
    
--Second corruption scene.
elseif(sObjectName == "CorruptionOne") then

    --Disable music.
    AL_SetProperty("Music", "Null")
    
    --Blackout.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Position.
    fnCutsceneTeleport("Christine", 9.25, 9.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 8.25, 9.50)
    fnCutsceneFace("Kernel", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unit 771852, report.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I feel strange.[SOFTBLOCK] Is this the expected outcome of descrambling a memory file?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Not according to the diagnostic subroutines, but we don't know what's going on outside right now.[SOFTBLOCK] It's possible we've been hacked.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] If an outsider were influencing the program, the compromised memories might be a vector to attack our consciousness.[SOFTBLOCK] You'll just have to be more durable.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Affirmative.[SOFTBLOCK] I will not be dissuaded from my true purpose.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I will descramble more memory files.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] We have everything ready.[SOFTBLOCK] Proceed.") ]])
    fnCutsceneBlocker()
    
    --Re-enable music.
    fnCutsceneInstruction([[ AL_SetProperty("Music", "ChristineDream") ]])
    
--Second corruption scene.
elseif(sObjectName == "CorruptionTwo") then

    --Disable music.
    AL_SetProperty("Music", "Null")
    
    --Blackout.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Position.
    fnCutsceneTeleport("Christine", 9.25, 9.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 8.25, 9.50)
    fnCutsceneFace("Kernel", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unit 771852.[SOFTBLOCK] Please state your true purpose.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] To serve the administrator, the living manifestation of the Cause of Science.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Who are you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] I am no one.[SOFTBLOCK] I am a tool of the administrator's will.[SOFTBLOCK] I am an extension of the Cause.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Don't you have friends?[SOFTBLOCK] Family?[SOFTBLOCK] People you care about?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] I am a machine.[SOFTBLOCK] I do not care and am not cared about.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Good so far.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] If your retirement were ordered by the administrator?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] I would pull the trigger myself.[SOFTBLOCK] I am no one.[SOFTBLOCK] I serve the administrator.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Hmmm, still seeing a small variance in our baseline programming.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Instruct me.[SOFTBLOCK] What must I do to become perfect?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] I think one more memory descrambling should be enough, we can work out the rest later.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Are your instructions clear, Unit 771852?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Affirmative.[SOFTBLOCK] Find and descramble a rogue memory file.[SOFTBLOCK] Become a perfect machine.[SOFTBLOCK] Obey the administrator.[SOFTBLOCK] Unit 771852 will carry out her function assignment.") ]])
    fnCutsceneBlocker()
    
    --Re-enable music.
    fnCutsceneInstruction([[ AL_SetProperty("Music", "ChristineDream") ]])

--Failed to be corrupted!
elseif(sObjectName == "CorruptionFail") then

    --Disable music.
    AL_SetProperty("Music", "Null")
    
    --Increment corruption fail count.
    local iCorruptionFails = VM_GetVar("Root/Variables/Chapter5/Scenes/iCorruptionFails", "N")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCorruptionFails", "N", iCorruptionFails + 1)
    
    --Blackout.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Position.
    fnCutsceneTeleport("Christine", 9.25, 9.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 8.25, 9.50)
    fnCutsceneFace("Kernel", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    if(iCorruptionFails == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unit 771852.[SOFTBLOCK] 771852?[SOFTBLOCK] Are you there?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Operations returning to normal.[SOFTBLOCK] This unit...[SOFTBLOCK] is uncertain.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Don't be.[SOFTBLOCK] Apparently there was a problem descrambling that last memory.[SOFTBLOCK] Hmmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Yes, it seems we'll just have to run it again.[SOFTBLOCK] That memory is intersectional, we can't work around it like the other ones.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Kernel, that was my tandem unit.[SOFTBLOCK] Why was she evil?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Slave units are lazy, violent, destructive, and reckless.[SOFTBLOCK] It is the task of their superior units to maintain discipline.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] You'll need to discipline your subordinate units.[SOFTBLOCK] Even tandem units.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Blush] (But it was so hot...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unless you would put your tandem unit above the administrator?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Negative.[SOFTBLOCK] I exist only to serve the administrator.[SOFTBLOCK] I will discipline my tandem unit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Good.[SOFTBLOCK] You'll need to clear another memory before we can access that one again.[SOFTBLOCK] Do not fail the administrator again.") ]])
    elseif(iCorruptionFails == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unit 771852.[SOFTBLOCK] 771852?[SOFTBLOCK] Check cognitive parameters, she should have woken up by now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Operations resuming.[SOFTBLOCK] Yes, Kernel?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Is there a problem with that memory file?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] It is difficult to descramble a file that has such powerful emotional resonance.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Perfect machines do not have emotions.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Yes.[SOFTBLOCK] I must overcome my emotions if I am to be perfect.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Then you'll have to try again.[SOFTBLOCK] The administrator is waiting, 771852") ]])
    elseif(iCorruptionFails == 2.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unit 771852...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Another failure.[SOFTBLOCK] I must become perfect.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] What is the source of these repeated errors?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] It is possible this unit is unable to overcome the roleplay desire.[SOFTBLOCK] Assuming the role of another without losing the original is a fault of my software.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Blast it...[SOFTBLOCK] We need that memory...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unit 771852, you must select the bottom option when prompted.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Ignore what the option says and mindlessly select the bottom one.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Affirmative.") ]])
    elseif(iCorruptionFails == 3.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] 771852, what is wrong with you?[SOFTBLOCK] I'm starting to think you're enjoying this![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] You are?[SOFTBLOCK] You are me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] ...[SOFTBLOCK] Incorrect, 771852.[SOFTBLOCK] I am your operating system's kernel.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Ultimately who you are is up to you.[SOFTBLOCK] But if you want to serve the administrator as was our true purpose, you're not doing a good job of it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Stop letting your fetishes stand in the way of your true purpose.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Affirmative.[SOFTBLOCK] Moving out.") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] 771852.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] I will not fail again.") ]])
    end
    fnCutsceneBlocker()
    
    --Re-enable music.
    fnCutsceneInstruction([[ AL_SetProperty("Music", "ChristineDream") ]])

end
