--[Become Doll Cutscene]
--This is where Christine gets transformed into a command unit!
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

--Music.
if(iIsRelivingScene == 1.0) then
    fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
end

--NPC assembly. Not done during a relive.
if(iIsRelivingScene == 0.0) then
    TA_Create("MemoryLordA")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", true)
    DL_PopActiveObject()
    TA_Create("MemoryLordB")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordB/", true)
    DL_PopActiveObject()
end

--Dialogue. Doesn't appear during a relive.
if(iIsRelivingScene == 0.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] There we are, love![SOFTBLOCK] So, do you recognize the room just north of us?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Nope![SOFTBLOCK] I don't have access to my memory files![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Please, it was a lead-in.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I know![SOFTBLOCK] I do the same thing![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Christine, I *am you*.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So, the room.[SOFTBLOCK] What's so important about it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] It just so happens to be the room where we were born, actually.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Your early memories got scrambled, so I'll do my best to summarize.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] In fact, why don't I show you before I upload the memory?[SOFTBLOCK] But first...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Fade to black.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
end

--Scene.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine awoke suddenly, her eyes flitting open and taking in the light blue sky.[SOFTBLOCK] She was lying on her back, staring upward.[SOFTBLOCK] The sky seemed a little off, but oddly familiar.[SOFTBLOCK] She tried to sit up, but found that she could not move at all.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Kernel]'Relax, Christine',[VOICE|Narrator] a voice said.[SOFTBLOCK] Recognition passed over her.[SOFTBLOCK] It was Kernel.[SOFTBLOCK] [VOICE|Kernel]'This is your imagination, it's not actually happening.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine's body would not respond to any commands.[SOFTBLOCK] She could only lay there, staring up at the sky as her eyes slowly focused on the oddness of it.[SOFTBLOCK] She soon realized what was wrong.[SOFTBLOCK] The light blue she saw was not the sky.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She was in a machine, lying on her back, unable to move.[SOFTBLOCK] She knew she should be afraid, but a restful serenity seemed to wash over her as she stared up into the false sky.[SOFTBLOCK] As she relaxed, a whirring noise came from above, and several hatches and panels began to open.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A small mechanical arm appeared from a panel to her right.[SOFTBLOCK] An arm with a small light on its end approached her face.[SOFTBLOCK] The light began to flicker on and off quickly as it hovered just above her, gliding just above her face.[SOFTBLOCK] She held still.[SOFTBLOCK] She could not move.[SOFTBLOCK] She did not think she wanted to move.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Another arm sprouted from the thick darkness within another panel.[SOFTBLOCK] It descended to the side of her head, a thick tube stretching behind it, and she soon lost sight of it.[SOFTBLOCK] A moment passed, and she felt it prodding at her ear.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She heard the crunch of cartilage breaking aside, but there was no pain.[SOFTBLOCK] She felt a bit of blood leaking from her ear as the tube began to fill with black fluid.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]'What's happening?',[SOFTBLOCK] [VOICE|Narrator] Christine thought.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Kernel]'We need to reshape your mental image of yourself.[SOFTBLOCK] This is the best way to do it.[SOFTBLOCK] It's not real, so it won't hurt.[SOFTBLOCK] Just sit back and enjoy.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine let her thoughts slip from her mind and allowed the fluid to fill the void they left.[SOFTBLOCK] Several minutes passed in this silence when the light stopped flashing.[SOFTBLOCK] It pulled back and placed itself in front of her face.[SOFTBLOCK] Her eyes focused on it as a crawling sensation built within her skull.[SOFTBLOCK] They were the only part of her body she could control.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]'My brain is...[SOFTBLOCK] a computer?'[VOICE|Narrator][SOFTBLOCK] Christine thought.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Kernel]'Yes, this machine will transform it from the inside-out.[SOFTBLOCK] The CPU has the capability of using your organic power supply until your core is installed.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine felt her thoughts become muddy.[SOFTBLOCK] Her sight became unfocused and she could no longer hear the whirring and humming of the machines that hovered around her. Tears began to drip from her eyes as the growing pressure pushed against the inside of her head.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A sudden crack split through the room, and though she could not hear it, she could feel the pressure suddenly vanish.[SOFTBLOCK] Relief washed over her as the newly installed CPU began to organize itself.[SOFTBLOCK] Her brain was now a computer.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A sense of familiarity came to her as the CPU, memory drives, quantum co-processor, and all the other conveniences she remembered faintly from her life as a golem began to process her surroundings.[SOFTBLOCK] She could see again.[SOFTBLOCK] Hear again.[SOFTBLOCK] Think again.[SOFTBLOCK] And think, she did.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She had been a golem once.[SOFTBLOCK] Yes, that seemed right.[SOFTBLOCK] Some of the memories were still with her.[SOFTBLOCK] This was right.[SOFTBLOCK] This was how she was supposed to be.[SOFTBLOCK] This was who she was supposed to be.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "But this CPU was faster.[SOFTBLOCK] Its memory was more densely-packed.[SOFTBLOCK] She was smarter than she had been, and her memories had greater clarity.[SOFTBLOCK] Even her visual acuity increased as her CPU increased its post-processing.[SOFTBLOCK] Her organic eyes were more efficient, now.[SOFTBLOCK] This -[SOFTBLOCK] this was a command unit's brain.[SOFTBLOCK] It was incredible![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The arm withdrew from her ear and retracted into the ceiling.[SOFTBLOCK] The blood had ceased flowing, but would need to be cleaned later.[SOFTBLOCK] A fraction of a second passed, too short for even her former golem processor to register, and Christine willed the blood away.[SOFTBLOCK] This was her imagination.[SOFTBLOCK] She had control.[SOFTBLOCK] She would not need to do something as menial as cleaning a little blood.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The light in front of her began to flash again, except now she realized it was transmitting information in the pulses.[SOFTBLOCK] Her processor, working through her organic eyes, began to decode the information.[SOFTBLOCK] She was limited, as her eyes could not deduce the wavelengths of the light, but simple information could be communicated.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her designation as Unit 771852 came first.[SOFTBLOCK] Security access codes soon followed.[SOFTBLOCK] Later came packets of condensed data she could not yet understand without her authenticator chip...[SOFTBLOCK] She remained still as information flooded into her brain.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It was not long before her organic eyes began to tire.[SOFTBLOCK] The constant need to blink, to moisten their surface, became inefficient.[SOFTBLOCK] They could no longer capture the same amount of information and showed red splotches as the light flashed.[SOFTBLOCK] The arm, sensing this, withdrew into the ceiling.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Beneath her, a conveyor belt sprung to life and she began to move forwards.[SOFTBLOCK] She exited the machine into an open space.[SOFTBLOCK] The walls of this room were the same light blue of the first, but she could still not move her head.[SOFTBLOCK] All she could see were three large tanks of fluid suspended above her, connected to thick needles by thicker hoses.[SOFTBLOCK] They began to descend even as she watched them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The first tank detached from the other two and hung above her.[SOFTBLOCK] It pressed down, the needle close to her body, then repositioned to her left shoulder.[SOFTBLOCK] It poked into her and began to discharge its fluid.[SOFTBLOCK] The tank bubbled as it discharged, and she lost what little feeling remained in that arm.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The tank then repositioned over her left elbow, and injected her again.[SOFTBLOCK] Then, her wrist.[SOFTBLOCK] Then, her pelvis, her knee, her ankle.[SOFTBLOCK] It moved to each joint, injecting them with the fluid before moving to the next, and with each pass, her body numbed further.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "When it completed its work, the tank rose back to the ceiling and waited.[SOFTBLOCK] She did not feel pain, but she did feel her body as it reconfigured itself.[SOFTBLOCK] After a few minutes, she realized she could control her arms.[SOFTBLOCK] She raised her left hand.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Calibrating...)[VOICE|Narrator][SOFTBLOCK] she thought to herself.[SOFTBLOCK] She held her hand in front of her.[SOFTBLOCK] Her wrist had become a ball joint, locked in place by a magnetic quantum superfluid.[SOFTBLOCK] It was sluggish and slow, still relying on her primitive organic blood to power it.[SOFTBLOCK] Even if the movements were imperfect, she could move it, and she marveled at her skin.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Calibrating...)[VOICE|Narrator][SOFTBLOCK] she thought to herself again.[SOFTBLOCK] She did the same to her right arm, lifting it and inspecting it.[SOFTBLOCK] Then, she lifted her legs above her.[SOFTBLOCK] Her knees and ankles were the same ball joints.[SOFTBLOCK] She could now easily bend them backwards and contort herself if she so chose.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Calibrations complete.)[VOICE|Narrator][SOFTBLOCK] her CPU reported.[SOFTBLOCK] She now understood how to move with her new joints.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The second tank descended toward her navel and its needle injected itself through her torso.[SOFTBLOCK] She looked down at it, between her breasts, and saw the fluid entering her skin and moving along her body's web of blood vessels.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Tendrils of the fluid snaked across her skin, its finger-like filaments smoothing and hardening to laminated sheets of resin.[SOFTBLOCK] Flexible, but extremely strong if struck.[SOFTBLOCK] Resistant to radiation, impacts, chemical damage, and electrical overloads.[SOFTBLOCK] The fluid was making her body strong and hard, like plastic.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The injections ceased, the tank empty, and the fluid tank raised into the ceiling.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine][Begin chassis inspection...][VOICE|Narrator][SOFTBLOCK] she thought to herself.[SOFTBLOCK] Christine sat up, looking her body over.[SOFTBLOCK] She placed her hand against her stomach, feeling it for imperfections.[SOFTBLOCK] Still slowed by her reliance on organic power sources, she delicately felt her skin all across her body.[SOFTBLOCK] Countless sensors awoke within her new skin as her fingers glided over her smooth surface.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Having completed the inspection, Christine lay back down and stated aloud,[SOFTBLOCK] [VOICE|Christine]'Begin internal reconfiguration injection.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The third fluid descended and injected into her, deeper this time.[SOFTBLOCK] This fluid was different from the first two, meant to reconfigure her insides for the final stage of the process.[SOFTBLOCK] The sensors that her touch had awoken reported more advanced information than her organic nerves did, transmitting it to her CPU along her reconfigured spinal column.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The more advanced command unit components would need to be inserted, but the simpler systems could be assembled on-the-spot by nanites.[SOFTBLOCK] Her coolant pump, power regulator, and other low-tech systems began to take form inside her.[SOFTBLOCK] Christine waited as the fluid tank finished and returned to the ceiling with the other two.[SOFTBLOCK] The belt whirred to life beneath her and brought her to the third and final room.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Inside another machine, still looking at the false sky, she saw hatches where the mechanical arms began to appear.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The arms descended, bearing a fusion cutter and a nanite-factory module.[SOFTBLOCK] The cutter sawed through her torso's chassis.[SOFTBLOCK] Despite the advanced sensors her chassis now had, Christine felt no pain.[SOFTBLOCK] Only anticipation.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "When the cutter had completed its work, Christine placed ball-jointed segmented hands on her torso cavity and pried it open.[SOFTBLOCK] [VOICE|Christine]'Install components.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The arm inserted the factory module at the base of her torso cavity.[SOFTBLOCK] She was now largely empty on the inside, her internal organs having dissolved away to form the more basic components of her body.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Another arm descended, this one bearing one of the parts for her power core.[SOFTBLOCK] Others followed soon after bearing additional parts.[SOFTBLOCK] The arms began to do their work as Christine held her chassis open for them.[SOFTBLOCK] A few minutes passed as new systems were installed and attached, followed by a brief diagnostics check.[SOFTBLOCK] Christine felt her strength return as her power core booted for the first time.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her internals assembled, Christine closed her chassis.[SOFTBLOCK] Auto-repair nanites surged into the gap and reassembled the resinous layers.[SOFTBLOCK] Within moments she was whole again.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF3") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Replacing ocular units...)[VOICE|Narrator][SOFTBLOCK] Christine thought.[SOFTBLOCK] The one remaining component that could not be synthesized by nanites were her eyes.[SOFTBLOCK] Though they had served her well for the first pulses of information, they were still organic.[SOFTBLOCK] The optic nerves had a tenuous connection to the circuitry behind them.[SOFTBLOCK] Christine lay herself flat again and stared at the roof of the machine.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Two new ocular receivers appeared, borne by arms.[SOFTBLOCK] The nanites that now acted as her bloodstream severed the optic nerves and disassembled her eyes to be used in future nanites.[SOFTBLOCK] All material would be recycled.[SOFTBLOCK] Now blinded, Christine waited for the arms to attach her new eyes.") ]])
fnCutsceneBlocker()

fnCutsceneWait(65)
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF4") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "With a click and a whirr, the new ocular receivers were attached by quantum superfluid.[SOFTBLOCK] Christine calibrated her new receivers as the conveyor moved her to the end of the line.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her receivers darted left, right, up, and down.[SOFTBLOCK] She felt the CPU make minor motor adjustments.[SOFTBLOCK] The process repeated itself.[SOFTBLOCK] She was satisfied with her new eyes.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF4") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF5") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]'Command unit assembly completed.[SOFTBLOCK] Psychological modifications at fifteen percent.'[SOFTBLOCK][VOICE|Narrator] Christine said aloud.[SOFTBLOCK] She sat up to briefly survey her new, perfect body.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Strong, fast, intelligent, beautiful.[SOFTBLOCK] Mechanical perfection.[SOFTBLOCK] She imagined her ideal set of clothing, and arms appeared to dress her.[SOFTBLOCK] It was done.[SOFTBLOCK] She had finally returned to her true form.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine lay back down on the belt and closed her eyes. [SOFTBLOCK][VOICE|Christine]'Resuming psychological reset. Begin reorientation.'") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_End.lua") ]])
	fnCutsceneBlocker()
    return
end

--Transform Christine.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Doll.lua") ]])

--Layers.
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FlashbackFloor1", false) ]])
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FlashbackWalls1", false) ]])

--Reposition.
fnCutsceneTeleport("Christine", 7.25, 32.50)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneTeleport("Kernel", 6.25, 32.50)
fnCutsceneFace("Kernel", 0, -1)

--NPCs.
fnCutsceneTeleport("MemoryLordA",  8.25, 28.50)
fnCutsceneFace("MemoryLordA", 0, -1)
fnCutsceneTeleport("MemoryLordB",  9.25, 26.50)
fnCutsceneFace("MemoryLordB", 0, -1)

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I understand.[SOFTBLOCK] I am a command unit.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] And now you see yourself as one![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Smashing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But, wait, you said I was made in this room?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Correct.[SOFTBLOCK] And if you saw yourself as a human, it would be rather awkward to be built, wouldn't it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] That's you, right there.[SOFTBLOCK] Those are our earliest memories.[SOFTBLOCK] This is the moment we were first switched on.[SOFTBLOCK] Watch...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Camera position.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 3.0)
	CameraEvent_SetProperty("Focus Position", (7.25 * gciSizePerTile), (28.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Eyes open.
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FlashbackWalls1", true) ]])
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FlashbackWalls2", false) ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Cognitive boot completed.[SOFTBLOCK] Unit Zero, online and awaiting instructions.") ]])
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("MemoryLordA", 7.25, 28.50)
fnCutsceneFace("MemoryLordA", 0, -1)
fnCutsceneMove("MemoryLordB", 9.25, 28.50)
fnCutsceneFace("MemoryLordB", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] The experiment has been a success![SOFTBLOCK] Please notify the administrator![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] My PDU has already sent the information.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Unit Zero, do you know where you are?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMoveFace("MemoryLordA", 7.25, 30.50, 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FlashbackFloor1", true) ]])
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FlashbackWalls2", true) ]])
fnCutsceneTeleport("DollChristine", 7.25, 28.50)
fnCutsceneFace("DollChristine", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] Unit Zero is currently in the Synthetic Intelligence Research Wing of the Arcane University, Sector Zero, Regulus City, Regulus.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] She responds to verbal instructions?[SOFTBLOCK] This early?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] A full grammatical understanding of two-hundred languages has been imprinted into my memory drives.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] All physical calibrations are green.[SOFTBLOCK] Please input instructions.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Well, I'm afraid we cannot do that, Unit Zero.[SOFTBLOCK] That is the administrator's privilige.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] Who is the administrator?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] The one who created you, who designed your specifications.[SOFTBLOCK] We are merely assistants.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Oh, yes![SOFTBLOCK] I've just received a message -[SOFTBLOCK] you are to see the administrator in person at once![SOFTBLOCK] I'll call for someone to escort you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] Affirmative.[SOFTBLOCK] Unit Zero awaiting escort.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Camera movement.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 3.0)
	CameraEvent_SetProperty("Focus Actor Name", "Christine")
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Kernel", 1, 0)
fnCutsceneFace("Christine", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] The moment of my birth![SOFTBLOCK] Incredible![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But, wait.[SOFTBLOCK] Kernel?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Yes?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I have memories -[SOFTBLOCK] as a human.[SOFTBLOCK] I was born on Earth, wasn't I?[SOFTBLOCK] Because I grew up there.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Ah, yes.[SOFTBLOCK] This, Christine, is why we have been working so hard.[SOFTBLOCK] You see...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Actually, best if I show you.[SOFTBLOCK] I've just heard that we've finished that particular memory.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] At the northern most end of this area, past the terminals, I've got the memory cued up for you.[SOFTBLOCK] Just head through the door there.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Don't dally![SOFTBLOCK] I'll be needing your help to finish sorting your memories.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Affirmative![SOFTBLOCK] I can't wait to meet the administrator!") ]])
fnCutsceneBlocker()
