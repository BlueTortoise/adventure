--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToFlashbackB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:4.0x6.0x0")
    
elseif(sObjectName == "ToFlashbackK") then
    
    --Variables.
    local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
    
    --Not a doll yet:
    if(iFlashbackBecameDoll == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Kernel told me to meet her by the westernmost room...)") ]])
        fnCutsceneBlocker()
    
    --Doll!
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusFlashbackK", "FORCEPOS:21.5x63.0x0")
    end
    
--[Objects]
elseif(sObjectName == "MemoryUpload") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An upload terminal with various memories on it, undergoing the final stages of crosschecking before being placed in my memory permanently.)") ]])
    
elseif(sObjectName == "Terminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The terminal which contained my schematics.[SOFTBLOCK] I never actually read it myself.)") ]])
    
elseif(sObjectName == "FabBench") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The fabrication bench and tools used to create my non-standard components.)") ]])
    
elseif(sObjectName == "BirthChamber") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This is where my final assembly took place.[SOFTBLOCK] This is where I was born.)") ]])
    
elseif(sObjectName == "Garbage") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Imagination objects being sorted before being sent to a memory to hold them.)") ]])
    
elseif(sObjectName == "FizzyPop") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Fizzy Pop![SOFTBLOCK] Do these represent an addiction?)") ]])
    
elseif(sObjectName == "55") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unit 2855.[SOFTBLOCK] Maverick command unit.[SOFTBLOCK] Memories pending descrambling.[SOFTBLOCK] Abrasive, intelligent, tactical.)") ]])
    
elseif(sObjectName == "SX399") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (SX-399.[SOFTBLOCK] Refurbished steam droid.[SOFTBLOCK] Memories pending descrambling.[SOFTBLOCK] Rash, young, courageous, naive.)") ]])
    end
elseif(sObjectName == "Vivify") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Project Vivify.[SOFTBLOCK] Former human, true origins unknown.[SOFTBLOCK] Memories pending descrambling.[SOFTBLOCK] Personality assessment unavailable.)") ]])
    
elseif(sObjectName == "20") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('20'.[SOFTBLOCK] Golem, true origins unknown.[SOFTBLOCK] Memories pending descrambling.[SOFTBLOCK] Arrogant, vain, intelligent, dangerous.)") ]])
    
elseif(sObjectName == "Sophie") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unit 499323 'Sophie'.[SOFTBLOCK] Golem.[SOFTBLOCK] Memories pending descrambling.[SOFTBLOCK] Beautiful, witty, kind, caring, smart, gorgeous, loving, cute, adorable, literally causes rooms to become brighter.)") ]])
    
elseif(sObjectName == "Golem") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unit 599239, 'Hadasa'.[SOFTBLOCK] Golem.[SOFTBLOCK] Met once, present at Equinox Labs during disaster.[SOFTBLOCK] Cautious.[SOFTBLOCK] Further personality assessment unavailable.)") ]])
    
elseif(sObjectName == "300910") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    if(iCompletedSerenity == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unit 300910.[SOFTBLOCK] Command Unit.[SOFTBLOCK] Memories pending descrambling.[SOFTBLOCK] Intelligent, organized, caring, curious, confident.)") ]])
    end
elseif(sObjectName == "Psue") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
    if(iHasElectrospriteForm == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Psue.[SOFTBLOCK] Electrosprite.[SOFTBLOCK] Electrical elemental?[SOFTBLOCK] Shares knowledge and most memories due to temporary physical possession.[SOFTBLOCK] Reckless, rash, exuberant, adorable.)") ]])
    end

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end