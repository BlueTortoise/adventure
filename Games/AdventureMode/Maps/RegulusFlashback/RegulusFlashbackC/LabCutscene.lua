--[Lab Cutscene]
--Unit Zero receives some... advice.

--Move the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Position", (50.25 * gciSizePerTile), (27.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneFace("MemoryLordC", 0, -1)
fnCutsceneTeleport("MemoryLordC", 53.25, 26.50)
fnCutsceneFace("MemoryLordD", 1, 0)
fnCutsceneTeleport("MemoryLordD", 52.25, 26.50)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Spawn the characters we need.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneFace("DollChristine", 0, -1)
fnCutsceneTeleport("DollChristine", 50.75, 30.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("MemoryLordC", 0, 1)
fnCutsceneFace("MemoryLordD", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("MemoryLordD", 52.25, 28.50)
fnCutsceneMove("MemoryLordD", 50.25, 28.50)
fnCutsceneFace("MemoryLordD", 0, 1)
fnCutsceneMove("MemoryLordC", 53.25, 28.50)
fnCutsceneMove("MemoryLordC", 51.25, 28.50)
fnCutsceneFace("MemoryLordC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Unit Zero![SOFTBLOCK] You're back![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] The administrator said this might be your last meeting.[SOFTBLOCK] I was so worried.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] It was to be our last meeting.[SOFTBLOCK] She has finally given me my function assignment.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Wonderful![SOFTBLOCK] No more converting humans or repetetive manual tasks?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] I will be transported to another dimension to gather information for the Cause of Science.[SOFTBLOCK] I will disguise myself as a human.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] So...[SOFTBLOCK] we will not see you again?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] No.[SOFTBLOCK] Not until I am recalled.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] I see...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] You are crying due to your emotions.[SOFTBLOCK] Suppress them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] It's hard...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] We're not [SOFTBLOCK]*sniff*[SOFTBLOCK] perfect like you.[SOFTBLOCK] Golems have flaws...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] Do not waste the lubricant tears on me.[SOFTBLOCK] I need your assistance.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] Certainly![SOFTBLOCK] With what?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] My encounters with humans have been extremely limited.[SOFTBLOCK] I need information on how they work.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] Oh, of course![SOFTBLOCK] I studied human societies on Pandemonium before my assignment to the AI Research wing![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Naturally, the Administrator planned for that.[SOFTBLOCK] She is so wise.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] You're going to need a name.[SOFTBLOCK] Humans will suspect someone named Unit Zero.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Zero:[VOICE|Christine] Suggest one.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Erm...[SOFTBLOCK] Jeff?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutsceneFace("MemoryLordC", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] Jeff is a male's name you drainer-brain![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Male?[SOFTBLOCK] Really?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] Don't listen to her, Zero.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutsceneFace("MemoryLordC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] Perhaps...[SOFTBLOCK] Christine?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Affirmative.[SOFTBLOCK] Adding alias 'Christine'.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Well, what else...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] You'll need to pick out some clothes, and acquire more once you arrive.[SOFTBLOCK] Try to blend in with the local style.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] You'll need to hide your joints...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] Oh, and don't eat metal or oil![SOFTBLOCK] Those are toxic, so humans will get suspicious.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] And humans respirate![SOFTBLOCK] Through their mouths![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] Yes, the expel gasses from both ends, but only inhale oxygen from one end.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] If you see a human inhaling oxygen from the other end, they may need medical attention.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] Oh![SOFTBLOCK] Oh![SOFTBLOCK] Many human societies consider sniffing another's hands as a sign of respect![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] And they're fragile.[SOFTBLOCK] You can barely throw them ten meters before they break bones.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] And electric shocks are right out.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "103220:[VOICE|GolemLord] But humans do value their own mortality.[SOFTBLOCK] They believe it drives them to accomplish things.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "499431:[VOICE|GolemLordB] You'll have to be very patient with them...") ]])
fnCutsceneBlocker()

--Camera movement.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 3.0)
	CameraEvent_SetProperty("Focus Actor Name", "Christine")
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[VOICE|Christine] The briefing went on for some time...") ]])
fnCutsceneBlocker()
fnCutsceneTeleport("DollChristine", -50.75, -30.50)










