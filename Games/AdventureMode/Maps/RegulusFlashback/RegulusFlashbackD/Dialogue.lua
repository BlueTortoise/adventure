--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "KernelA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] This area is dedicated to our life on Earth.[SOFTBLOCK] After about the age of seven, the memories are pretty clear.[SOFTBLOCK] We're working on getting the rest back.") ]])
        
    elseif(sActorName == "KernelB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] Not all of these are happy memories.[SOFTBLOCK] If you don't want to look, you don't have to.") ]])
            
    elseif(sActorName == "KernelC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] Department stores.[SOFTBLOCK] Those have been out of fashion for a while, haven't they?") ]])
            
    elseif(sActorName == "KernelD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] These are repressed memories.[SOFTBLOCK] We'll need to do something to unlock them, but it's not a priority right now.") ]])
        
    elseif(sActorName == "Tiffany") then
        
        --If you've already spoken to 55:
        local iFlashbackMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N")
        if(iFlashbackMet55 == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855Sundress", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] Repressed memories are located south of this position, down the steps.") ]])
            return
        end
        
        --First pass:
        local iSpokeToTiffany = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N")
        if(iSpokeToTiffany == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855Sundress", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[E|Neutral] Ah, there you are, Schatzelchen.[SOFTBLOCK] I'm so glad you could attend.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] (Hm, this memory seems to be aware of me?)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[E|Neutral] It is a splendid day for a gathering, is it not?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] It is just the right amount of overcast.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[E|Smirk] English weather.[SOFTBLOCK] These are the standards we go by, I suppose.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I'm sorry, have we met before?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[E|Neutral] We have.[SOFTBLOCK] Do you remember me?[SOFTBLOCK] My name, perhaps.[SOFTBLOCK] My -[SOFTBLOCK] designation, as it were?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] (Something about this memory seems off...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Fifty...[SOFTBLOCK] fifty something?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Tiffany-five?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Smirk] Tiffany von Marlow the fifth, heiress to the von Marlow phamaceuticals corporation.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Smirk] Your father and mine are good friends, so he has been helping us enter the English market in psychoactives.[SOFTBLOCK] The regulations are much different from the EU versions.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Ah, I apologize.[SOFTBLOCK] We must have met during a previous business meeting.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Smirk] That must be it.[SOFTBLOCK] You've grown since last I saw you![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] My company has been working on a drug to help restore memories in patients with severe trauma.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Smirk] Such as electrical damage.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I'm afraid that's not my subject of expertise.[SOFTBLOCK] Can you explain how it works?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] Well, theoretically, let us say we have a patient who was shocked and suffered brain damage.[SOFTBLOCK] They lose access to their memories, but over time the brain can recover those memories by piecing them back together.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] Our drug increases the neurotransmitter that is responsible for this.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] However, it is possible that false memories could be created by the damage.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] False memories?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] Blasted linguistic simulator...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] Attempting to interf-[SOFTBLOCK] intera-[SOFTBLOCK] touching a broken memory is bad, my dear.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] Do not listen to anyone who tells you to.[SOFTBLOCK] They're lying to you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] ...[SOFTBLOCK] Okay?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] It can cause serious brain damage.[SOFTBLOCK] Luckily, my company produces the drug, INTERCOM.[SOFTBLOCK] Use the INTERCOM or speak to me and I will repair the damage.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] That's a strange name for a drug.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Smirk] It is -[SOFTBLOCK] German![SOFTBLOCK] Yes, of course it is German![SOFTBLOCK] I am from Austria, am I not?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Smirk] Any and all linguistic anomalies in my speech patterns are due to my Germanic heritage, I assure you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Smirk] Oh, all right.[SOFTBLOCK] Splendid![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Smirk] You know, I speak a little German myself![SOFTBLOCK] Took it in primary school![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] That's lovely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] There is something very interesting that I need you to see in the damaged section of your memories.[SOFTBLOCK] Avoid interacting with the memories, ignore instructions to do so.[SOFTBLOCK] Use the intercoms or speak to me and I can undo any damage you take.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Smirk] German turn of phrase, my dear.[SOFTBLOCK] It's a Salzburg expression.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] ...[SOFTBLOCK] Yeah, I'm going to go, now. Nice seeing you again.") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855Sundress", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] Ah, there you are, Schatzelchen.[SOFTBLOCK] I'm so glad you could attend.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] (The memory is just replaying itself.[SOFTBLOCK] I'll skip ahead to the odd thing she said.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Tiffany:[E|Neutral] There is something very interesting that I need you to see in the damaged section of your memories.[SOFTBLOCK] Avoid interacting with the memories, ignore instructions to do so.[SOFTBLOCK] Use the intercoms or speak to me and I can undo any damage you take.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] (I wonder what that means in Austria?[SOFTBLOCK] What a strange country.)") ]])
        end
    end
end



