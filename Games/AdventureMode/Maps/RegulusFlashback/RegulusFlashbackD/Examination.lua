--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToFlashbackB") then

    --First time.
    local iFlashbackWrongDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWrongDoor", "N")
    if(iFlashbackWrongDoor > 0.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:13.0x6.0x0")
    
    else
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWrongDoor", "N", 1.0)
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:4.0x6.0x0")
    end

--Repressed memory:
elseif(sObjectName == "ToFlashbackJ") then

    --Repressed.
    local iFlashbackMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N")
    if(iFlashbackMet55 == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There is a section of repressed memories beyond this point.[SOFTBLOCK] Unable to access.)") ]])
        fnCutsceneBlocker()
    
    --First time.
    elseif(iFlashbackMet55 == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N", 2.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The memory...[SOFTBLOCK] it's still repressed, but I can enter it?)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFlashbackJ", "FORCEPOS:8.0x25.0x0") ]])
        fnCutsceneBlocker()
    
    --Repeats.
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusFlashbackJ", "FORCEPOS:8.0x25.0x0")
    end

--[Objects]
elseif(sObjectName == "Staring") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It didn't matter what I did, they were always staring at me.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Crying") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It hurts to look.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Hate") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The room always stunk of pipe smoke.[SOFTBLOCK] I hated this room.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Clothes") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Why did mother scold me when I touched these?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Dolls") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Father never found these down here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BackersTerminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Some partially repressed memories are on this terminal...[SOFTBLOCK] Well, maybe I should see if I can sort them out?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "BackersRepressed", "Leave") ]])

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end