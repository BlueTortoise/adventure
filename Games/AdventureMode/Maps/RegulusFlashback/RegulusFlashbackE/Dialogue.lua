--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)


--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    if(sActorName == "KernelA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] These are memories from your life on Regulus.[SOFTBLOCK] The administrator will doubtless be very interested in the rebel activities.[SOFTBLOCK] Fortunately, these memories weren't too badly damaged.") ]])
        
    elseif(sActorName == "KernelB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] Memories like this are incomprehensible.[SOFTBLOCK] They're from the future, but they're formatted so differently from any other memory as to be alien.") ]])
        
    elseif(sActorName == "KernelC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] We've collected some of your favourite fantasies here.[SOFTBLOCK] Would you like to review them?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[VOICE|Christine] Fantasies?[SOFTBLOCK] Why would a perfect machine need fantasies?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] You were organic for a long time, they formed naturally.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[VOICE|Kernel] They may prove entertaining if nothing else.[SOFTBLOCK] It will be the administrator's choice to let us keep them.") ]])
        
    elseif(sActorName == "DollA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|Doll] This performance review session occurred shortly before the Sunrise Gala.[SOFTBLOCK] Would you like to review the memory?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchPerformanceReview\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
        
    elseif(sActorName == "DollB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|Doll] We were invited to another lord unit's quarters to review work credit allocations.[SOFTBLOCK] Would you like to review the memory?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchMolestation\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
        
    elseif(sActorName == "DollC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|Doll] Keeping drone units out of trouble is an important part of every unit's function assignment.[SOFTBLOCK] Would you like to review the memory?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchDumbDrones\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
        
    elseif(sActorName == "DollD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|Doll] This memory took place while we were training mavericks for the revolt.[SOFTBLOCK] Would you like to review the memory?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchTargetPractice\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
        
    elseif(sActorName == "DollE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|Doll] I just finished this memory, though some of its precursors are still missing.[SOFTBLOCK] Would you like to review the memory?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchTableFlop\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
        
    elseif(sActorName == "DollF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|Doll] This memory details the electrosprite activities after our encounters with them.[SOFTBLOCK] Would you like to review the memory?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchFlameBanner\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
    end

elseif(sTopicName == "WatchPerformanceReview") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "PerformanceReview.lua")

elseif(sTopicName == "WatchMolestation") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "Molestation.lua")

elseif(sTopicName == "WatchDumbDrones") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "DumbDrones.lua")

elseif(sTopicName == "WatchTargetPractice") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "TargetPractice.lua")

elseif(sTopicName == "WatchTableFlop") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "TableFlop.lua")

elseif(sTopicName == "WatchFlameBanner") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "FlameBanner.lua")
    
elseif(sTopicName == "Cancel") then
	WD_SetProperty("Hide")
end