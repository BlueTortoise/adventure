--[Flame Banner]
--This isn't a parody. How could you suggest that?
DialogueActor_Push("Christine")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/Golem|Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/Golem|Happy", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/Golem|Blush", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/Golem|Smirk", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/Golem|Sad", true)
    DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/Golem|Scared", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/Golem|Offended", true)
    DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/Golem|Serious", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/Golem|Cry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/Golem|Laugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/Golem|Angry", true)
    DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/Golem|PDU", true)
DL_PopActiveObject()

--Position entities.
fnCutsceneFace("Psue", 0, 1)
fnCutsceneTeleport("Psue", 26.25, 28.50)
fnCutsceneTeleport("Algy", -26.25, -28.50)
fnCutsceneTeleport("Cmdy", -26.25, -28.50)
fnCutsceneTeleport("GolemChristine", -24.25, -19.50)
    
--Move the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Position", (26.75 * gciSizePerTile), (30.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine enters.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneFace("GolemChristine", 0, 1)
fnCutsceneTeleport("GolemChristine", 32.75, 32.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("GolemChristine", 26.75, 32.50)
fnCutsceneFace("GolemChristine", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Electrosprite", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay Psue, you made a big boast.[SOFTBLOCK] Let's see this secret project of yours.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] XD XD XD,[SOFTBLOCK] get out here girls!") ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Rotation Array
local iXArray = {1, 0, -1, 0}
local iYArray = {0, 1, 0, -1}

--Algy pops up.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|SparksD") ]])
fnCutsceneFace("Psue", 1, 0)
fnCutsceneTeleport("Algy", 28.25, 28.50)
fnCutsceneBlocker()
for i = 1, 5, 1 do
    for p = 1, 4, 1 do
        fnCutsceneFace("Algy", iXArray[p], iYArray[p])
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
end
fnCutsceneFace("Algy", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Cmdy pops up.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|SparksB") ]])
fnCutsceneFace("Psue", 0, -1)
fnCutsceneTeleport("Cmdy", 26.25, 26.50)
fnCutsceneBlocker()
for i = 1, 5, 1 do
    for p = 1, 4, 1 do
        fnCutsceneFace("Cmdy", iXArray[p], iYArray[p])
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
end
fnCutsceneFace("Cmdy", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] Algy, everything ready on your end?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Algy:[VOICE|Amanda] You bet![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] Cmdy, is the interface fixed?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cmdy:[VOICE|LatexDroneB] As if you have to ask![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] Let's get this show on the road![SOFTBLOCK] That's a phrase I borrowed from Christine's brain, I have no idea what it even means!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneBlocker()

for i = 1, 5, 1 do
    for p = 4, 1, -1 do
        fnCutsceneFace("Cmdy", iXArray[p], iYArray[p])
        fnCutsceneFace("Psue", iXArray[p], iYArray[p])
        fnCutsceneFace("Algy", iXArray[p], iYArray[p])
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
end
fnCutsceneInstruction([[ AudioManager_PlaySound("World|SparksA") ]])
fnCutsceneTeleport("Psue", -26.25, -26.50)
fnCutsceneTeleport("Cmdy", -26.25, -26.50)
fnCutsceneTeleport("Algy", -26.25, -26.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] Just use that console and start up Flame Banner:: Romance of the Three Lineages![SOFTBLOCK] We'll handle the rest![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Uhhh, okay.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("GolemChristine", 28.75, 30.50)
fnCutsceneFace("GolemChristine", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Flame Banner:: Romance of the Three Lineages.[SOFTBLOCK] Hm, name your character, pick an appearance.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Do the available appearances have to be sexy?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] Yes.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Obviously.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Okay, watch a couple cutscenes...[SOFTBLOCK] I'm the chosen hero with a magic emblem...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Gee, where did you take inspiration from, Psue?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] ::D[SOFTBLOCK] I mean I don't know what you're talking about![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Okay, we're on Pandemonium someplace, I'm a mercenary.[SOFTBLOCK] I have a...[SOFTBLOCK] Mark Five Pulse Carbine...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Command my troops...[SOFTBLOCK] Hmm, I have to aim the rifle myself to shoot the enemies...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] This is very realistic, and the dialogue...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Psue, are you three playing the role of the NPCs?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] XD[SOFTBLOCK] She figured it out, girls![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Algy:[VOICE|Amanda] Isn't it obvious?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cmdy:[VOICE|LatexDroneB] I wanted to be the girl with the axe but Psue called dibs.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] But -[SOFTBLOCK] But, Christine, this is a realistic combat training game![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] We can get slave units to play it and it will teach them small-group tactics, weapon handling, and how to date cute girls![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cmdy:[VOICE|LatexDroneB] And we can spy on them and identify sympathetic golems to join the rebellion.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] You made an entire video game for that?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Psue, you're a genius![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Okay, I guess I'm going with Psue's lineage...[SOFTBLOCK] I like a red girl with an axe, what can I say.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] And now my character is teaching you how to be a leader?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] XD inorite?[SOFTBLOCK] It was Algy's idea.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cmdy:[VOICE|LatexDroneB] Yeah she gets picked to be a teacher after exactly one battle.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Algy:[VOICE|Amanda] Players want to get right to the action![SOFTBLOCK] May as well get it over with quickly.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I hope you three are ready for a full night because I intend to test this program extensively.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[VOICE|Electrosprite] Counting on in it baybee!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Camera moves.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Actor Name", "Christine")
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Maps/RegulusFlashback/RegulusFlashbackE/ResetToDollEmotes.lua") ]])
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The game was a huge success and helped identifies hundreds of units across the city for recruitment.[SOFTBLOCK] Video games are clearly powerful tools of learning and should be respected as the most legitimate art form.)") ]])
fnCutsceneBlocker()

fnCutsceneTeleport("GolemChristine", -100, -100)


















