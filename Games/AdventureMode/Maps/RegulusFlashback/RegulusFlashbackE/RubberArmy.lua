--[Rubber Army]
--A highly reactive rubber accidentally repurposes Sophie - and many others!
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Oh I remember this one...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Why do you need my slave unit to test this?[SOFTBLOCK] I need her in the repair bay, repairing things!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine was frustrated by the bureaucratic response.[SOFTBLOCK] Her tandem unit was one of the few slave units who had visible hair.[SOFTBLOCK] For some reason that made her an ideal test candidate.[SOFTBLOCK] For what?[SOFTBLOCK] Christine didn't know.[SOFTBLOCK] It seemed convenient for some awful plot.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Still, Christine was overruled by a superior unit.[SOFTBLOCK] She sulked in the repair bay, fixing broken servos and scanning damaged golems.[SOFTBLOCK] It was boring without Sophie there.[SOFTBLOCK] At the end of the workday, Sophie did not return.[SOFTBLOCK] Christine drew her PDU.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Oh, good.[SOFTBLOCK] A message from my beloved command unit.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'WHAT!?[SOFTBLOCK] Unit reassigned?[SOFTBLOCK] Oh, she's going to get my boot so far up her access port, they'll - [SOFTBLOCK]well, it will be something droll I'll think of later!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine stomped to the tram.[SOFTBLOCK] Every golem, lord or slave, got out of her way.[SOFTBLOCK] They knew not to get between a lord golem and whatever her source of ire was.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The tram arrived and whisked her to sector 22.[SOFTBLOCK] When she stepped onto the platform, it zipped off down the track.[SOFTBLOCK] Immediately, a drone unit with a familiar set of green hair greeted her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Amanda] 'HELLO, LORD UNIT.[SOFTBLOCK] WELCOME TO SECTOR 22.[SOFTBLOCK] HOW MAY THIS DRONE ASSIST YOU?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Oh, thank goodness.[SOFTBLOCK] I'll just get a few things straightened out and I'll repurpose you back to a slave unit.[SOFTBLOCK] Won't take an hour, dear.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Amanda] 'THIS DRONE IS UNSURE OF YOUR REQUEST.[SOFTBLOCK] HOW MAY THIS DRONE ASSIST YOU?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine ordered the drone to lead her to whoever was in charge.[SOFTBLOCK] It was strange.[SOFTBLOCK] The drone's authenticator chip was not reading 499323, but that hair was unmistakable.[SOFTBLOCK] Then, she saw it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Hundreds of drone units, walking to and fro.[SOFTBLOCK] Moving crates, sorting packages, cleaning equipment.[SOFTBLOCK] And every single one had Sophie's green hair.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Gingerly, Christine entered the office of the superior unit.[SOFTBLOCK] A drone sat in her chair.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Erm, are you Unit 732284?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'AFFIRMATIVE.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'You're...[SOFTBLOCK] a drone unit?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'REPURPOSE HER.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The drone who had led Christine there placed something on her back.[SOFTBLOCK] Christine spun around but it was far too late to stop what came next.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The gel covered her body head to toe.[SOFTBLOCK] It was a goopy, rubbery material.[SOFTBLOCK] Nanitic gel, possibly, but it quickly cleared up.[SOFTBLOCK] Christine looked at her chassis.[SOFTBLOCK] She was ceramo-latex, her hands black and bound.[SOFTBLOCK] Her face had become a mask.[SOFTBLOCK] She was a drone.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She felt at her head.[SOFTBLOCK] Familiar, pretty green hair flopped in front of her optic sensors.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'WHAT HAVE YOU DONE TO ME?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'WE WERE EXPERIMENTING ON A PORTABLE REPURPOSING AGENT.[SOFTBLOCK] IT BOUND ITSELF TO OUR TESTING SLAVE AND QUICKLY OVERWHELMED THE SECTOR.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'THE GEL HAS A SIDE OF EFFECT OF COMPELLING US TO SPREAD IT FURTHER.[SOFTBLOCK] TAKE IT BACK TO YOUR SECTOR.[SOFTBLOCK] SPREAD IT.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The desire to spread it was powerful indeed, but nothing was more powerful than love.[SOFTBLOCK] Christine nodded at the director and stepped out of her office.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] '55, GOT A BIT OF A SITUATION HERE.[SOFTBLOCK] COME TO SECTOR 22.[SOFTBLOCK] AVOID CONTACT WITH ANYONE THERE, INCLUDING ME.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'So that's the case?[SOFTBLOCK] Well, the administration has already mobilized a counter-attack.[SOFTBLOCK] They intend to isolate the sector and find a way to stop the goop from spreading.[SOFTBLOCK] Can you transform?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'YES, BUT WE NEED TO FIND SOPHIE.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'And every unit in here is reporting scrambled authenticator readings.[SOFTBLOCK] I have an idea.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'A short-range virus, transmitted by touch.[SOFTBLOCK] If I run it, I'll be able to shut down a unit at will.[SOFTBLOCK] It will also be able to probe the unit for memories.[SOFTBLOCK] I'll be able to identify Unit 499323 when you apply it.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'SPIFFY!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'Don't talk like that.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'FORCE OF HABIT.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'Now, get up there and start transmitting the virus.[SOFTBLOCK] I'll stay here and run the scans.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine emerged from the coolant junction where she had met with 55.[SOFTBLOCK] Drones were wandering back and forth, oblivious.[SOFTBLOCK] They all looked like Sophie would if she were a latex-bound drone.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She picked one at random.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'DRONE UNIT, PREPARE FOR INSPECTION.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] 'AFFIRMATIVE.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine placed her shiny latex hand on the drone's firm breasts.[SOFTBLOCK] She prodded, ran her hand along them.[SOFTBLOCK] They weren't the right breasts...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'Virus uploaded.[SOFTBLOCK] Checking memories...'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'DON'T BOTHER, THIS ISN'T HER.[SOFTBLOCK] I'LL KNOW WHEN I FIND HER.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine found another drone, and ordered her to stop for inspection.[SOFTBLOCK] She touched the drone's breasts, ran her hands along them.[SOFTBLOCK] She admired the smooth, taut chassis.[SOFTBLOCK] They were firm but gave just enough jiggle.[SOFTBLOCK] These were exemplary drones.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She found another, and another.[SOFTBLOCK] 55 continued to upload the virus.[SOFTBLOCK] Christine got more creative with her inspections.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She clutched at a drone from behind, grasping her chest, squeezing the breasts together.[SOFTBLOCK] She ran her hands along the drone's torso chassis.[SOFTBLOCK] Massaged the pelvic motivator.[SOFTBLOCK] They weren't her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'Picking up an unusual signal from the drone near the corner, Christine.[SOFTBLOCK] Take a look.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine saw a single drone unit, standing in the corner.[SOFTBLOCK] This one was anxiously watching the other drones go back and forth.[SOFTBLOCK] She was tapping her fingers against her leg.[SOFTBLOCK] She was very nervous.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'DRONE UNIT, PREPARE FOR INSPECTION.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The drone immediately complied.[SOFTBLOCK] Christine prodded her.[SOFTBLOCK] This drone felt so right, so correct.[SOFTBLOCK] The shape, density, distribution, orientation.[SOFTBLOCK] Everything.[SOFTBLOCK] But she had to be sure.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She reached down and grasped between the drone's legs.[SOFTBLOCK] She rubbed.[SOFTBLOCK] The drone grasped at Christine's shoulders, moaning through her vocal synthesizer.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'Virus uploaded.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'IT'S HER.[SOFTBLOCK] THIS IS HER.[SOFTBLOCK] CAN YOU DISABLE HER?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'Yes.[SOFTBLOCK] I will also disable the repurposement gel.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Suddenly, the rubbery latex coating receded all over the drone's body.[SOFTBLOCK] Somehow, as if by magic, Sophie's familiar golem form returned.[SOFTBLOCK] She looked around, bewildered.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sophie] 'What -[SOFTBLOCK] what happened?[SOFTBLOCK] Where am I?[SOFTBLOCK] Why do you -[SOFTBLOCK] did you copy my hairstyle?[SOFTBLOCK] Hee hee!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'WE NEED TO GO, SOPHIE.[SOFTBLOCK] IT'S NOT SAFE HERE.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Sophie] 'Christine?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Unfortunately, some of the other drones had noticed.[SOFTBLOCK] They saw an unassimilated golem, and bore down on them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] '55?[SOFTBLOCK] Little help?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The drones stopped, shuddered for a few moments, then turned.[SOFTBLOCK] All of them simultaneously began groping and 'inspecting' one another.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|2855] 'That should help spread the virus to any unaffected drones.[SOFTBLOCK] Get out of there, you two!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine led Sophie by the hand to the nearest junction room.[SOFTBLOCK] They escaped into the vents just as the administration security forces blew their breaching charges.[SOFTBLOCK] They would easily subdue the sector with 55 having disabled the drones.[SOFTBLOCK] The rubbery latex would be contained.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] But Christine could not help but wonder what would have happened if some had escaped...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Mmmm, hundreds of Sophies...[SOFTBLOCK] my own personal slice of heaven...)") ]])














