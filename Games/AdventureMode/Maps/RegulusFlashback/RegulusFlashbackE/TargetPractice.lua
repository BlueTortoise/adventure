--[Target Practice]
--55 loses her temper when trying to train steam droids to shoot.
DialogueActor_Push("Christine")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/Golem|Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/Golem|Happy", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/Golem|Blush", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/Golem|Smirk", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/Golem|Sad", true)
    DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/Golem|Scared", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/Golem|Offended", true)
    DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/Golem|Serious", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/Golem|Cry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/Golem|Laugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/Golem|Angry", true)
    DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/Golem|PDU", true)
DL_PopActiveObject()

--Position entities.
fnCutsceneFace("GolemChristine", 0, 1)
fnCutsceneTeleport("GolemChristine", 45.25, 21.50)
fnCutsceneFace("55", 0, 1)
fnCutsceneTeleport("55", 46.25, 21.50)
    
--Move the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Position", (45.75 * gciSizePerTile), (22.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We were asked here to review your firing discipline and accuracy.[SOFTBLOCK] I am told your team leader is unable to attend due to a malfunction.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] That is correct, madam.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I suppose we should see what your current state is, then.[SOFTBLOCK] Prepare a firing drill.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("GolemChristine", 43.25, 21.50)
fnCutsceneMove("GolemChristine", 43.25, 24.50)
fnCutsceneMove("GolemChristine", 42.25, 24.50)
fnCutsceneFace("GolemChristine", 1, 0)
fnCutsceneMove("55", 43.25, 21.50)
fnCutsceneMove("55", 43.25, 24.50)
fnCutsceneFace("55", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("DroidA", -1, 0)
fnCutsceneFace("DroidB", -1, 0)

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Begin.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Sounds.
fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Laser") ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Laser") ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Cease fire.") ]])
fnCutsceneBlocker()
fnCutsceneFace("DroidC", -1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In my relatively brief time I have never seen such a miserable accuracy over two shots.[SOFTBLOCK] Are you intending to use that as a weapon or a breaching tool?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] I'm sorry, madam.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Embarassed is what you should be.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] Yes, madam...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do not allow my words to cause you emotional damage.[SOFTBLOCK] You must improve your accuracy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] Y-[SOFTBLOCK]yes madam.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Are you crying?[SOFTBLOCK] Are you a soldier?[SOFTBLOCK] Do you intend to fight and kill others?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] I just want to protect my friends![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Time out![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] Do not interrupt.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Wrong.[SOFTBLOCK] This is not how you teach people.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] In all of my interactions, I notice that negative reinforcement is followed by increases in performance and positive reinforcement is followed by decreases in performance.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] If you want this soldier to be able to fight, she will have to accept harsh criticism.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Very logical, 55.[SOFTBLOCK] Also incorrect.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What you just described is called 'reversion to the mean'.[SOFTBLOCK] If someone has a bad performance by bad luck, their next one will be better simply by statistical likelihood.[SOFTBLOCK] Chiding them only looks like it works, but in reality it doesn't.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Come now, I'm a teacher.[SOFTBLOCK] You think I don't know what I'm talking about?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Her accuracy is not an issue due to bad luck.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's an issue because this is the first time she's picked up a gun.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] That's true.[SOFTBLOCK] This is my first week.[SOFTBLOCK] I've only fired once before.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You need to be patient, 55.[SOFTBLOCK] Patient, assertive, calm, and supportive.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] All of the training materials I have observed on the Regulus City network show my method, and show it working.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Really?[SOFTBLOCK] Because I've seen the same training videographs and noticed an abysmal performance.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But those security units aren't volunteers.[SOFTBLOCK] They'd walk away if they had the chance.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] They can't.[SOFTBLOCK] These steam droids can, but they're not going to because they want to defend their homes.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I still believe you are incorrect.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Were you going to tell her that her rifle isn't sighted before or after the insults?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[SOFTBLOCK] Point taken.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] Sighted?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The little knob on the side adjusts where the reticule is.[SOFTBLOCK] Yours is set for 800m, so you're hitting well above the mark.[SOFTBLOCK] Set it down to its lowest setting for close combat.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] Okay...") ]])
fnCutsceneBlocker()
fnCutsceneFace("DroidC", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Begin.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Sounds.
fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Laser") ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Laser") ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneFace("DroidC", -1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good shooting, droid.[SOFTBLOCK] But your shots are still hitting high.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] Yes, madam.[SOFTBLOCK] I think it's because my eyes aren't aligned right.[SOFTBLOCK] I'll have to adjust for it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A quirk of steam droid design?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] Flawed receiver.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is...[SOFTBLOCK] fine.[SOFTBLOCK] You will need to practice with that offset.[SOFTBLOCK] You will also need to...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *You're doing great.*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I...[SOFTBLOCK] apologize for earlier.[SOFTBLOCK] I am as new to teaching as you are to shooting.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *I'm so proud!*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] No need to apologize, madam.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You will need to action the weapon faster.[SOFTBLOCK] Do not anticipate the recoil and brace for it, simply begin moving your hand to the action once you have evaluated where the shot hit.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In close combat, action the weapon without evaluating the shot.[SOFTBLOCK] It is more important to get more rounds on the target than to make sure every round hits.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gunner:[E|Neutral] Yes madam!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Camera moves.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Actor Name", "Christine")
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Maps/RegulusFlashback/RegulusFlashbackE/ResetToDollEmotes.lua") ]])
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It took several training sessions before I felt confident enough to let 55 do it on her own.[SOFTBLOCK] She got frustrated that others weren't as capable as she was. But she did learn.)") ]])
fnCutsceneBlocker()

fnCutsceneFace("DroidC", 0, -1)
fnCutsceneTeleport("GolemChristine", -100, -100)
fnCutsceneTeleport("55", -100, -100)










