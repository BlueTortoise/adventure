--[The Dollmaster]
--Never mess with a teenage girl who has psychic powers. That never seems to go well.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I don't have the memory files of what those girls said to me but...[SOFTBLOCK] They deserved this!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine sat in the middle of her class. She felt all their eyes on her.[SOFTBLOCK] She heard their snickering.[SOFTBLOCK] Their laughs.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She felt the sting of their pranks.[SOFTBLOCK] The teacher knew, and did not care.[SOFTBLOCK] Nobody ever helped her.[SOFTBLOCK] She was different and weird and wrong, and she paid for it every day of her life.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] But she knew she should not teach them a lesson.[SOFTBLOCK] It would be wrong to use her powers on them.[SOFTBLOCK] It would lead to chaos.[SOFTBLOCK] Pain.[SOFTBLOCK] Pain like the kind they inflicted on her.[SOFTBLOCK] That, too, was wrong.[SOFTBLOCK] An eye for an eye left them all blind.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Days like today tested her patience.[SOFTBLOCK] They were particularly mean.[SOFTBLOCK] Then, one of them produced something from her bag.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Hey, Sissy-Chrissy.[SOFTBLOCK] You know what this is?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine looked.[SOFTBLOCK] It was one of her custom-made dolls, with a hand-stitched dress and remolded torso![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Where did you get that?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Do you bring them to school to feel safe, or something?[SOFTBLOCK] What a sissy![SOFTBLOCK] Sissy-Chrissy![SOFTBLOCK] Ha ha ha ha!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine's blood boiled.[SOFTBLOCK] They would graduate in a few days.[SOFTBLOCK] Then she would leave for university and never see them again.[SOFTBLOCK] It wouldn't be a moment too soon.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] *snap*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine's jaw dropped as Mila snapped her custom-doll in half.[SOFTBLOCK] She dropped the pieces to the side.[SOFTBLOCK] The entire class laughed at her in unison.[SOFTBLOCK] And Christine felt a piece of her break with the doll.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'So Jess' parents are out of town, and we've got her estate to ourselves?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'That's what my invite said.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Electrosprite] 'What I'm asking, why didn't Jess mention it before now?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Duh.[SOFTBLOCK] Because Sissy-Chrissy would overhear.[SOFTBLOCK] Gotta keep it secret or that busy-body might crash our party.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'Pfft, fat chance.[SOFTBLOCK] Why would she ever want to come?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Because, girls, I brought the booze.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The three girls turns to see Christine right behind them.[SOFTBLOCK] She had a duffel bag in one hand which clanked as she raised it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] '...[SOFTBLOCK] Seriously?[SOFTBLOCK] You got liquor?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'I didn't know you were cool, Chrissy.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Electrosprite] 'Yeah.[SOFTBLOCK] What happened?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Hey, we graduate soon.[SOFTBLOCK] Never going to see each other again, right?[SOFTBLOCK] Let's not leave as enemies.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Aw yeah!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'Don't let anyone hear you![SOFTBLOCK] We're still technically underage!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Not me, I'm 18.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'Yeah but your dad would kill you if he knew you were drinking.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'You're not 18, Chrissy.[SOFTBLOCK] Where'd you get this stuff?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'I know how to pick the lock on my father's wine rack.[SOFTBLOCK] Did you know he drinks beer from a can?[SOFTBLOCK] Don't tell anyone or it'll be a scandal!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'Oh yeah, pipes and wine for the ol' colonel right?[SOFTBLOCK] Ha ha ha!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine and her new friends made their way to Jessica's house.[SOFTBLOCK] She had several painfully contrived dialogues with them about various subjects.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I was never good at writing dialogue, even in my own head...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] They rang the buzzer and Jess opened the gates for them without a word.[SOFTBLOCK] Jess didn't have the biggest house, but she did have the emptiest one this weekend, and that was what really mattered.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine knocked at the front door.[SOFTBLOCK] Jess answered.[SOFTBLOCK] She had a subdued look on her face, flat and uninterested.[SOFTBLOCK] Her eyes seemed hollow.[SOFTBLOCK] 'Hello'.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'You ready to get wild, Jess?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Golem] 'Please come in.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Jessica was wearing a frilly maid outfit that covered most of her body.[SOFTBLOCK] The house was spotless.[SOFTBLOCK] Had she been cleaning it herself?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'What's with the outfit?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Golem] 'I must keep the house clean.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Don't you have people for that?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Golem] 'Not yet.[SOFTBLOCK] I must keep the house clean.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Come on, I got some movies in here, too.[SOFTBLOCK] Fire up the projector, Jess, we're having a movie night!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The girls squealed with delight.[SOFTBLOCK] Jessica had prepared snacks, from sandwiches to chips to chicken wings.[SOFTBLOCK] The girls, surprisingly, held their liquor well.[SOFTBLOCK] Christine never liked the taste but drank just enough to fit in.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Maybe things were going to be all right.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The sun had gone down.[SOFTBLOCK] Mila excused herself to visit the restroom.[SOFTBLOCK] When she came back, Jess and Christine were gone.[SOFTBLOCK] Annie had fallen asleep, and Theresa looked half-conscious.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'You think Sissy-Chrissy is cool now, Mila?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Nah.[SOFTBLOCK] When the booze is done, we'll boot the freak.[SOFTBLOCK] Make her walk home in the dark by herself![SOFTBLOCK] Ha ha!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Theresa giggled.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Where is she, anyway?[SOFTBLOCK] Go find her!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'Aw come on...'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Mila pointed sternly, and Theresa knew better than to argue.[SOFTBLOCK] She sleepily wandered into the darkened house, in search of Jess and Christine.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She heard something from the upstairs bedrooms, and groggily made her way there.[SOFTBLOCK] She had had way more to drink than a teenage girl should, but had enough focus to not hurt herself on the stairs.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The master bedroom's door was ajar.[SOFTBLOCK][VOICE|Christine] 'Make it again,'[SOFTBLOCK][VOICE|Narrator] she head Christine say.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She stood at the door, peering within.[SOFTBLOCK] Christine was standing there, with her back to Theresa.[SOFTBLOCK] Jess, still in the maid outfit, still with her face flat and emotionless, was making the bed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'Oh, just what I needed.[SOFTBLOCK] May I sleep there tonight?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine turned to see the half-conscious Theresa.[SOFTBLOCK] She smiled.[SOFTBLOCK][VOICE|Christine] 'Certainly, love.[SOFTBLOCK] Jess was just making it perfect.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Golem] 'Perfect,'[SOFTBLOCK] Jess echoed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Without another thought, Theresa lurched to the bad and flopped herself down.[SOFTBLOCK] She heard Christine and Jessica doing something, but was too tired to care.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She rolled over to see them standing over her.[SOFTBLOCK] Jessica stared at her with those empty eyes.[SOFTBLOCK] Theresa began to feel nervous.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'What are you doing?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Golem] 'Making you perfect,'[SOFTBLOCK] Jess said emptily.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] It was then that Theresa saw Jessica's elbow.[SOFTBLOCK] It was a sphere, connected tenuously to her forearm.[SOFTBLOCK] It seemed to be made of plastic.[SOFTBLOCK] Was she just drunk and seeing things?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Jess touched her arm and she felt cold.[SOFTBLOCK] Extremely cold.[SOFTBLOCK] Jess had no body heat, and her skin was smooth plastic.[SOFTBLOCK] Theresa couldn't put together a coherent thought in her state, and merely watched as Jess began to massage her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Jess massaged her arms and then her legs.[SOFTBLOCK] She felt cold all over.[SOFTBLOCK] Christine was standing nearby, smiling.[SOFTBLOCK] Encouraging her.[SOFTBLOCK][VOICE|Christine] 'Yes, make her perfect, Jessica.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Golem] 'I am making her perfect.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Now Jess massaged Theresa's head, and Theresa understood.[SOFTBLOCK] Her body hardened all over, solidifying into plastic.[SOFTBLOCK] She went limp as her mind totally fogged over.[SOFTBLOCK] Christine told her to stand.[SOFTBLOCK] She stood.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Jess presented a maid uniform.[SOFTBLOCK] Theresa disrobed and dressed herself in the new uniform.[SOFTBLOCK] She didn't think anymore, she didn't need to.[SOFTBLOCK] She was perfect.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The three descended to the main floor to find Annie, still asleep in front of the projector screen.[SOFTBLOCK] She snored softly.[SOFTBLOCK] Christine motioned to her perfect dolls.[SOFTBLOCK] They began their work.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Mila had far too many spicy chicken wings and had needed some cold, clean water.[SOFTBLOCK] She returned to the projector room but found Annie missing.[SOFTBLOCK] Where was she?[SOFTBLOCK] And where had Theresa gotten to?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She shrugged and sat on the chesterfield.[SOFTBLOCK] It was soft and warm there, like Annie had just gotten up.[SOFTBLOCK] She would return soon.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She felt a chill as she realized someone was behind her.[SOFTBLOCK] She turned to see Annie staring at her, hands folded on her lap.[SOFTBLOCK] She was wearing a maid uniform like the one Jessica had been wearing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Why are you in that dumb getup?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Electrosprite] 'We must keep the house clean.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Mila turned to see Jess and Theresa, both dressed as maids, blocking the door.[SOFTBLOCK] She realized, in the dim light of the projector, why they looked so empty and distant.[SOFTBLOCK] Their skin was plastic, and their arms and legs were those of a ball-jointed doll.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Well, Mila, what do you think of my custom dolls now?[SOFTBLOCK] Going to snap them in half?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Mila tried to stand but Annie's hands grasped her shoulders and forced her to sit.[SOFTBLOCK] Jess and Theresa had surrounded her in an instant.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Christine?[SOFTBLOCK] What are you doing?[SOFTBLOCK] How are you doing this?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Well, you broke my doll.[SOFTBLOCK] I needed a replacement.[SOFTBLOCK] You know I spent a lot of time on it, the one you broke was one of my favourites.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'So I decided to make you take her place.[SOFTBLOCK] Not like the world will miss someone like you.[SOFTBLOCK] Someone who never said a nice thing in her life.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Hey, come on.[SOFTBLOCK] It was all jokes, right?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'You and I both know they weren't.[SOFTBLOCK] My dollies will attest to that, won't you dollies?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Golem] 'We hated you, mistress,'[SOFTBLOCK] the Jessica doll said.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isabel] 'We thought you were a freak,'[SOFTBLOCK] the Theresa doll said.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Electrosprite] 'We were going to kick you out and humiliate you,'[SOFTBLOCK] the Annie doll said.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] 'But now we are perfect and we will love you forever,'[SOFTBLOCK] the dolls said in unison.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Christine, come on![SOFTBLOCK] We can talk about this!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Dollies don't speak until they are spoken to.[SOFTBLOCK] Dollies, make her perfect.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Mila screamed as the dolls began to do their work.[SOFTBLOCK] The scream echoed off the empty walls.[SOFTBLOCK] Soon, the silence returned.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine presented Mila with her uniform.[SOFTBLOCK] Mila put it on and folded her hands patiently on her lap.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'You're so pretty, Mila.[SOFTBLOCK] Shame you had an attitude like the one you did.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|GolemLord] 'Yes, mistress.[SOFTBLOCK] But I am perfect now.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'Well, what am I going to do with my dollies?[SOFTBLOCK] Can't have anyone wondering where you went.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'No, you'll just have to wear clothes that hides your joints and go about your lives as though nothing happened.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] 'But you'll always serve me, and only me, with your whole being.[SOFTBLOCK] Won't you dollies?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] 'Yes, mistress,'[SOFTBLOCK] the dolls said in unison.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Bunch of meanies...[SOFTBLOCK] I'm glad I don't have to put up with them anymore...)") ]])












