--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "KernelBriefing") then
    
    --Repeat check.
    local iFlashbackMemoryBrief = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N")
    if(iFlashbackMemoryBrief == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 33.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 4.0)
        CameraEvent_SetProperty("Focus Position", (42.25 * gciSizePerTile), (18.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 4.0)
        CameraEvent_SetProperty("Focus Position", (29.25 * gciSizePerTile), (26.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMove("Kernel", 32.25, 6.50)
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 4.0)
        CameraEvent_SetProperty("Focus Position", (23.25 * gciSizePerTile), (15.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Refocus on Christine.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Quite a mess in here, isn't it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] The electrical damage was severe.[SOFTBLOCK] This is what happened to a lot of our memories.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Electrical damage?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] ...[SOFTBLOCK] Well that's my assumption of what happened to us.[SOFTBLOCK] Electric currents can do a lot of brain damage.[SOFTBLOCK] And we were in human form at the time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Though that could also be wrong.[SOFTBLOCK] We revert to human when injured, as part of the runestone's magic.[SOFTBLOCK] It's jumbled.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] But the answer is in this mess, isn't it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] That is correct, 771852.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] So what do you need me to do, then?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Put simply, walk into those memories you see wandering around.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] The creatures with the viewcones?[SOFTBLOCK] Doesn't that trigger a battle?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Not here, because inside your own mind, you are in total control.[SOFTBLOCK] Instead, you will straighten out the rogue memory.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] The sooner you do that, the sooner we can awaken and return to the administrator.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I understand.[SOFTBLOCK] I will get my memories sorted so the administrator can view them.[SOFTBLOCK] Then I will have fulfilled my true purpose.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] I'll be over here if you need anything.[SOFTBLOCK] Good luck.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Kernel", 32.25, 4.50)
    fnCutsceneMove("Kernel", 31.25, 4.50)
    fnCutsceneFace("Kernel", 0, 1)
    fnCutsceneBlocker()


end
