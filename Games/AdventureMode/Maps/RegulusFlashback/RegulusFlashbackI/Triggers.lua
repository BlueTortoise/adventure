--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "Cutscene") then
	
	--Repeat check.
	local iFlashbackMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N")
	if(iFlashbackMet55 == 1.0) then return end
	
    --Variables.
	local iSpokeToTiffany = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N")
    
	--Set.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N", 1.0)
	
	--[Movement]
	--Move Christine forward.
	fnCutsceneMove("Christine", 10.25, 10.50)
	fnCutsceneBlocker()
	
	--Camera focuses on 55.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "2855sTheme") ]])
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 1.0)
		CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Excuse me...[SOFTBLOCK] are you the lady who was on the intercom?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[VOICE|2855] Yes.[SOFTBLOCK] I recognize your voice.[SOFTBLOCK] It is deeper than the interference made it out to be.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--[Movement]
	--55 turns partway around.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair1")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair2")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Y-[SOFTBLOCK]your legs...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Wait, is this a memory right now, or is this actually happening?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Girl:[VOICE|2855] I needed the parts you were collecting.[SOFTBLOCK] That should have been obvious.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] But I can speak, and I can move myself...[SOFTBLOCK] And I can't do that in memories, right?") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--[Movement]
	--55 turns the rest of the way.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair3")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair4")
	DL_PopActiveObject()
	fnCutsceneWait(305)
	fnCutsceneBlocker()
	
	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Broken") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: I am unit 2855.[SOFTBLOCK] I - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I know exactly who you are.[SOFTBLOCK] This is a memory.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55: So you are the consciousness, then?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] W-[SOFTBLOCK]what? Hey![SOFTBLOCK] That's not what you say next![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55: Correct.[SOFTBLOCK] Because I am not a memory.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Everything else in here is, though.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55: Incorrect.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55: Christine, you are currently in a program.[SOFTBLOCK] This program is modifying you internally.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] ...[SOFTBLOCK] I know.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55: ...[SOFTBLOCK] You do?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] Yes.[SOFTBLOCK] That was the first thing Kernel told me, actually.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] And since this is my mind, I can just change things to suit me using my imagination.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] So while I appreciate the accuracy of your expression, I don't much like it.[SOFTBLOCK] So, please return to your repaired self.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Like this?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] That is preferable.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] Then I must take this appearance.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] H-[SOFTBLOCK]hey![SOFTBLOCK] You are my own creation and I command you to change back![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] ...[SOFTBLOCK] Dang.[SOFTBLOCK] Does that actually work, or was I mistaken?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] Christine, I need you to listen to me.[SOFTBLOCK] You are being manipulated.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] But if I can't modify you, then you're not a memory.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] And if you're not a memory...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Offended] Then you must be...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] The real 55...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] I have accessed this program, Christine, to liberate you from it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] So far I have evaded detection by disguising myself as the memory of our meeting.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] No, no, no.[SOFTBLOCK] I don't care what you say, you filthy low-down bog-nosed liar![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] Because at the end of this memory, you 'betrayed' me.[SOFTBLOCK] You used me, and turned me into a golem.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] If I recall correctly, you enjoyed the change.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] But that wasn't the only betrayal, now was it, 55?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] The Epsilon labs, we had -[SOFTBLOCK] Vivify -[SOFTBLOCK] and then you...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] Interesting.[SOFTBLOCK] You have been given most of your memory files back.[SOFTBLOCK] The upload must be nearly completed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] You...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Offended] You...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] What did I do, Christine?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] I am not Christine![SOFTBLOCK] I am Command Unit 771852![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] I was created by the administrator![SOFTBLOCK] I was to be her perfectly loyal unit![SOFTBLOCK] I was sent to Earth to bring back information for the Cause of Science![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] The administrator created me![SOFTBLOCK] Created the runestone![SOFTBLOCK] Gave me purpose![SOFTBLOCK] I am her perfect doll![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] Christine.[SOFTBLOCK] What did I do.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Angry] I love her with all my heart![SOFTBLOCK] If she ordered me to -[SOFTBLOCK] to...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] How did I betray you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Offended] You...[SOFTBLOCK] pushed me onto an electrical shock trap...[SOFTBLOCK] because you...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Offended] You sided with the Regulus City administration...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] So you should obviously be listening to me, yes?[SOFTBLOCK] We are on the same side.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Offended] But...[SOFTBLOCK] then why are you hiding in here?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] Oh.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] This is most amusing.[SOFTBLOCK] Yes, very amusing.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I don't understand.[SOFTBLOCK] 55?[SOFTBLOCK] What's going on?[SOFTBLOCK] Why are you here?[SOFTBLOCK] This - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] I cannot solve this contradiction logically.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] The solution is very obvious, but you are missing a key piece of information.[SOFTBLOCK] And because you are missing that, you will believe anything.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] But...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] I know this is hard, Christine, but you must listen to me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I shouldn't trust you, you -[SOFTBLOCK] but I should.[SOFTBLOCK] 55?[SOFTBLOCK] Aren't you my friend?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] Someone smarter than me once told me that the only way to gain someone's trust, is to trust them.[SOFTBLOCK] And the fastest way to lose someone's trust, is to wear that mistrust like a badge.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] I'm asking you, after all this, to trust me one last time.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] But people who abuse your trust...[SOFTBLOCK] You've done that.[SOFTBLOCK] You've done that and I was angry at you for it...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] Then make your decision once you have seen the evidence.[SOFTBLOCK] That is all I'm asking of you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] In your memories of Earth, there are repressed memories.[SOFTBLOCK] Go see them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] And if it's a trick?[SOFTBLOCK] And if I cannot view my memories?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] You can.[SOFTBLOCK] They are repressed by the program, not by you.[SOFTBLOCK] Go see them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] 55...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] Where did it all go wrong?[SOFTBLOCK] I was so happy there in the repair bay with Sophie.[SOFTBLOCK] Why did I bring this upon myself?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] Because you have an abundance of something I lack.[SOFTBLOCK] Courage.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] And if evil requires that good machines stand by and do nothing, then you did not stand by.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] Go to your repressed memories of Earth, Christine.[SOFTBLOCK] Please.[BLOCK][CLEAR]") ]])
    if(iSpokeToTiffany == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Broken] The intercoms will allow you to repair any mental damage you sustain.[SOFTBLOCK] My alter-ego in your memories will, as well.[BLOCK][CLEAR]") ]])
    end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Sad] A-[SOFTBLOCK]Affirmative...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --55 rotates back.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair3")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
    
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair2")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
    
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair1")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
    
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Chair0")
	DL_PopActiveObject()
end
