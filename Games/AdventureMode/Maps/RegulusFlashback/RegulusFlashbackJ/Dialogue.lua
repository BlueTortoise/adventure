--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "Chris") then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "FakeChris", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Guy:[E|Neutral] H-[SOFTBLOCK]hello?[SOFTBLOCK] Who are you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Me?[SOFTBLOCK] I'm... [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] LIVID.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Guy:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55, I'm sorry I ever doubted you.[SOFTBLOCK] I was such an idiot to be taken in by such trickery.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] And now I understand everything.[SOFTBLOCK] The entire veil falls away, and I will never be misled again.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Guy:[E|Neutral] Uh, am I missing something?[SOFTBLOCK] Who are you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I am Unit 771852, Lord Golem of Maintenance and Repair, Sector 96.[SOFTBLOCK] My secondary designation is Christine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Guy:[E|Neutral] H-[SOFTBLOCK]huh?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Just shut up and come with me.[SOFTBLOCK] I'm going to go tear Kernel a new co-routine.") ]])
        fnCutsceneBlocker()
        
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFlashbackD", "FORCEPOS:30.0x68.0x0") ]])
        fnCutsceneBlocker()
    end
end