--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToFlashbackC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusFlashbackC", "FORCEPOS:22.0x4.0x0")
    
--[Objects]
elseif(sObjectName == "AdministratorConsole") then

    --Variables.
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    
    --First time:
    if(iFlashbackMetAdministrator == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Oh, this is in the memory now?[SOFTBLOCK] I'll just let that take over...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Administrator.[SOFTBLOCK] I am in the datacore now.[SOFTBLOCK] What are your instructions?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] Very well done, Unit Zero.[SOFTBLOCK] Your performance in training has been exemplary so far.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] It is thus with a heavy heart that I must finally give you your true purpose.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative, administrator.[SOFTBLOCK] Please input true purpose.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] Unit Zero, what do you see around you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The server architecture that governs Regulus City.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] Yes.[SOFTBLOCK] You see the knowledge we have gathered here.[SOFTBLOCK] Is it not breathtaking?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This unit does not breathe.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Sheesh![SOFTBLOCK] Did I not have a sense of humour?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] Hahah![SOFTBLOCK] You remind me of another of my favoured command units.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative, administrator.[SOFTBLOCK] Thank you, administrator.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] No, Unit Zero.[SOFTBLOCK] This knowledge we have gathered concerns but one of the realities that surround us.[SOFTBLOCK] There are more, several more.[SOFTBLOCK] We know nothing of them save legends that have filtered through the ages.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] That is why I have created you.[SOFTBLOCK] That is why I have created the runestone you now hold.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] I need my most loyal, most intelligent, most capable unit to go to these other realities.[SOFTBLOCK] You must go to them, learn from them, and bring back their knowledge.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] And to these places, the Cause of Science will be spread.[SOFTBLOCK] What you see around you is what we have accomplished on one small moon in one tiny corner of one reality.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] The knowledge we can gain if we can visit these other places at will...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I do not understand, administrator.[SOFTBLOCK] Why have you selected me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] Because, Unit Zero.[SOFTBLOCK] Because you are my daughter.[SOFTBLOCK] My perfect unit.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] You shall visit the benighted people of this 'Still Plane' and bring to them the Cause of Science.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And this runestone?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] It will allow you to travel there, to integrate yourself among them.[SOFTBLOCK] It will keep you safe.[SOFTBLOCK] And it will recall you when you are needed here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] This is your true purpose, my favoured unit.[SOFTBLOCK] To execute my will.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] I now entrust to you the judgement to act as you will.[SOFTBLOCK] The console you stand before has full access to the entire archives of Regulus City.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] Take whatever information you think you need.[SOFTBLOCK] Whenever you are ready, clutch the runestone to your chest and concentrate.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Administrator:[VOICE|Administrator] Do not weep, my beloved daughter.[SOFTBLOCK] I shall see you again when you task is done.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Thank you, administrator.[SOFTBLOCK] I will not fail you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I remember there was a flash of light, and then...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That must be the end of the memory...") ]])
        fnCutsceneBlocker()
        
        --Spawn Kernel.
        TA_Create("Kernel")
            TA_SetProperty("Position", 20, 57)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", true)
            TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
            fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
        DL_PopActiveObject()

    --Repeats:
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The administrator did not even see me off before I left...[SOFTBLOCK] But I shall be forever loyal...)") ]])
        fnCutsceneBlocker()
    end

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end