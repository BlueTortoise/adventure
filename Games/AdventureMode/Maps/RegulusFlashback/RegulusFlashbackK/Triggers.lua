--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
if(sObjectName == "TalkToKernel") then
    
    --Repeat check.
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    local iFlashbackSpokeToKernel = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N")
    if(iFlashbackMetAdministrator == 0.0 or iFlashbackSpokeToKernel == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 21.75, 57.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Kernel, is this memory accurate?[SOFTBLOCK] Was I sent away from Regulus City to gather information about Earth for the administrator?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Yes.[SOFTBLOCK] That is what the memory files we have recovered say.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So the memories I have of growing up?[SOFTBLOCK] Of being a human?[SOFTBLOCK] My parents, my country?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] We have always been a command unit in the service of the Cause of Science.[SOFTBLOCK] We used the runestone to transform into a human, to blend among them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And now I have been recalled.[SOFTBLOCK] That is why I arrived on Regulus the way I did.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Was I ever truly a human?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] No.[SOFTBLOCK] We were always a machine, but there was an accident when we arrived on Earth.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Something malfunctioned, and we lost our memories.[SOFTBLOCK] We forgot who we were.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] We believed we were human.[SOFTBLOCK] Our true self was lost and the organic mind of our disguise was unsuited to its recovery.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I understand.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well then, Kernel, our task is to wake up and report to the administrator immediately, isn't it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Yes, it is.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] And we are working as hard as we can to recover our fragmented memories.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Then why am I, the consciousness, within this program?[SOFTBLOCK] Can't you wake me up?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] We need your help dealing with some of the more difficult memories.[SOFTBLOCK] We've gotten what we can done, but there is a large block we are unable to handle.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I see.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Meet me back in the central room and I will explain things there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[SOFTBLOCK] I must be reunited with the administrator as soon as possible...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Don't rush things, Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Neutral] I am Unit 771852, not Christine.[SOFTBLOCK] That is the name I was falsely given on Earth.[SOFTBLOCK] We should use the designation I was given here, on Regulus, where I belong.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] That is affirmative, 771852.[SOFTBLOCK] I will be waiting in the main access room.") ]])
end
