--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Triggers]
if(sObjectName == "BDSMScene") then
    
    --Spawn characters.
    TA_Create("Kernel")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
    DL_PopActiveObject()
    fnSpecialCharacter("Sophie", "Golem", -100, -100, gci_Face_East, false, nil)
    EM_PushEntity("Sophie")
        fnSetCharacterGraphics("Root/Images/Sprites/SophieLeather/", true)
    DL_PopActiveObject()
    
    --Change Sophie to her BDSM variant. She doesn't appear for the rest of the chapter anyway.
    DialogueActor_Push("Sophie")
        DialogueActor_SetProperty("Add Emotion", "Neutral",   "Root/Images/Portraits/SophieDialogueDomina/Neutral",   true)
        DialogueActor_SetProperty("Add Emotion", "Happy",     "Root/Images/Portraits/SophieDialogueDomina/Happy",     true)
        DialogueActor_SetProperty("Add Emotion", "Blush",     "Root/Images/Portraits/SophieDialogueDomina/Blush",     true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",     "Root/Images/Portraits/SophieDialogueDomina/Smirk",     true)
    DL_PopActiveObject()

    --Blackout.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Position.
    fnCutsceneTeleport("Christine", 28.25, 13.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 23.25, 10.00)
    fnCutsceneFace("Kernel", 0, 1)
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(15)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 26.25, 13.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] (Memory File 5506902-BBRCav.9, variant zero.[SOFTBLOCK] 'Meeting Tandem Unit'.[SOFTBLOCK] File is pending verification by consciousness algorithm.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Kernel:[E|Neutral] Unit 771852.[SOFTBLOCK] This should be the last file we need descrambled.[SOFTBLOCK] Are you ready?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Affirmative.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sophie appears.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Flash") ]])
    fnCutsceneWait(42)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Sophie", 19.25, 13.50)
    fnCutsceneFace("Sophie", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    --Scene.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/SophieLeather/SophieLeather") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]Unit 499323 'Sophie' wearing normal slave unit clothes.[SOFTBLOCK] She is visibly menacing and likely unproductive.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine]Begin scan for object anomalies...[SOFTBLOCK] Memory playback beginning.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Greetings, tandem unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Greetings, tandem unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Are you prepared to begin our manual tasks for the day?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Affirmative.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Well, I'm not.[SOFTBLOCK] I'm afraid we'll be deviating from the usual.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] You upper-class types disgust me.[SOFTBLOCK] I've hated you since the day we met.[SOFTBLOCK] I've only been manipulating you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] (No deviations from expected slave golem behavior detected.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Like all slave golems, efficiency can only be extracted through force.[SOFTBLOCK] Are you going to stop me, Unit 771852?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] No.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] No?[SOFTBLOCK] Then perhaps you'd like to get on the floor.[SOFTBLOCK] KNEEL![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Blush] Kneel?[SOFTBLOCK] Kneel before you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Blush] (Memory descramble interrupted.[SOFTBLOCK] Consciousness algorithm requests clear-up protocol.[SOFTBLOCK] Proceed.)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 20.25, 13.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Blush] Unit 499323...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] On your knees![SOFTBLOCK] Worship me, worm![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Blush] ...[SOFTBLOCK] Oh my goodness...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Blush] (Should I get on my knees and kiss her metal feet?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Worship her...\", " .. sDecisionScript .. ", \"Worship\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Discipline the unit\",  " .. sDecisionScript .. ", \"Discipline\") ")
    fnCutsceneBlocker()

--Worship. Returns to main area.
elseif(sObjectName == "Worship") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Mmmm, yes my goddess.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Kiss my feet, you worthless sack of bolts![SOFTBLOCK] Kiss them until your goddess tells you to stop![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Right away my goddess...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *smooch*[SOFTBLOCK] *lick*[SOFTBLOCK] mmmm[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Ha ha![SOFTBLOCK] Be careful, Christine![SOFTBLOCK] Your tongue tickles![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] *Don't break character, Sophie!*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I can't![SOFTBLOCK] Hee hee![SOFTBLOCK] It tickles![SOFTBLOCK] Oooh, I need to -[SOFTBLOCK] hee hee! -[SOFTBLOCK] disable the sensor![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Mmmm, my goddess, have you forgotten to tell me to stop?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] R-[SOFTBLOCK]right![SOFTBLOCK] You pathetic unit![SOFTBLOCK] Your worship was tolerable at best![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Display your rear receiver port, I intend to punish it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Right away my goddess...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Mmmm....") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Blush") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] [SOUND|World|WhipCrack]Mmmm...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] (Uh oh, it left a mark!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] (No, no, that's just the wax.[SOFTBLOCK] Obviously her chassis is going to be harder than a simple synth-leather whip!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] [SOUND|World|WhipCrack]This will teach you to disrespect your goddess![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Yes my goddess, I will not fail you again.[SOFTBLOCK] I love you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I love you too.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie![SOFTBLOCK] You keep breaking character![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'm sorry![SOFTBLOCK] This is my first time![SOFTBLOCK] I'm trying![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's okay, I think you did pretty good![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Now come here.[SOFTBLOCK] Let me show you what else I can do...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Ooh![SOFTBLOCK] Ooh![SOFTBLOCK] Kiss your goddess!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:13.0x1.0x0") ]])
    fnCutsceneBlocker()

--Discipline. Proceeds.
elseif(sObjectName == "Discipline") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Negative, unit.[SOFTBLOCK] You are displaying insubordinate tendencies.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Wha?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Command protocol requires disciplining of subordinate units.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oh, oh?[SOFTBLOCK] We're doing it that way?[SOFTBLOCK] Okay![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] P-[SOFTBLOCK]punish away![SOFTBLOCK] This unit is ready![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] (What should I do to this unit?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Whip her a few times...\", " .. sDecisionScript .. ", \"Whip\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Order her reprogrammed\",  " .. sDecisionScript .. ", \"Reprogram\") ")
    fnCutsceneBlocker()

--Whip. Returns to main area.
elseif(sObjectName == "Whip") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Clearly you do not understand what sort of authority I have as your superior unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Bend over.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Affirmative, lord unit![SOFTBLOCK] Right away, lord unit![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Lord unit?[SOFTBLOCK] No, I believe 'mistress' will do.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Yes, my mistress!") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Offended") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] [SOUND|World|WhipCrack]Now, perhaps one stroke for speaking out of turn...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] (Eeee!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] [SOUND|World|WhipCrack]And another for not giving your mistress the proper respect?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] (Oh my![SOFTBLOCK] It feels so good!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] [SOUND|World|WhipCrack]One final to instill proper understanding into you.[SOFTBLOCK] Now, unit, who do you serve?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] My mistress![SOFTBLOCK] I only serve my mistress![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I worship the ground she walks on![SOFTBLOCK] I live to be punished by her perfection![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's more like it.[SOFTBLOCK] Your mistress is pleased. For now.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] I love you, mistress![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I love you, too, Sophie.[SOFTBLOCK] But you're not supposed to say that part![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Huh?[SOFTBLOCK] Oh, did I mess up?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Don't worry.[SOFTBLOCK] It was your first time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] Did you like it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I loved it.[SOFTBLOCK] Now...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Prepare your oral intake, worm![SOFTBLOCK] Your mistress needs her lower port massaged![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Yes, mistress![SOFTBLOCK] Executing oral service programs!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:13.0x1.0x0") ]])
    fnCutsceneBlocker()

--Reprogram. Proceeds along corruption path.
elseif(sObjectName == "Reprogram") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Considering your rebellious tendencies, reformation is unlikely.[SOFTBLOCK] Initiating reprogramming.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] C-[SOFTBLOCK]Christine![SOFTBLOCK] What are you doing?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Initiating upload.[SOFTBLOCK] Do not resist.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Ah![SOFTBLOCK] Ah![SOFTBLOCK] No![SOFTBLOCK] Christine -[SOFTBLOCK] Christine don't   [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Reprogramming complete.[SOFTBLOCK] Unit 499323 reports cognitive activity at baseline.[SOFTBLOCK] Please provide function assignment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] (What should I do with this unit?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Order her to be my personal footrest\", " .. sDecisionScript .. ", \"Love\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Order her to betray the rebels\",  " .. sDecisionScript .. ", \"Betray\") ")
    fnCutsceneBlocker()

--Love. Returns to main area.
elseif(sObjectName == "Love") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] Unit 499323.[SOFTBLOCK] In a previous iteration, you aided a rebel uprising.[SOFTBLOCK] You were punished for this.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Affirmative, lord unit.[SOFTBLOCK] This unit deserved its punishment.[SOFTBLOCK] This unit now serves the Cause of Science.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] Unit 499323, your function assignment is to act as a service unit for Unit 771852.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] You will follow her everywhere, clean up after her, polish her chassis.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] You will be her footrest if she so demands.[SOFTBLOCK] You are a machine, you will have no dignity whatsoever.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] If at any time she orders sexual services, you will provide them without question.[SOFTBLOCK] Confirm.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Unit 499323's function assignment accepted.") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] (Please order me to service you, Christine!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] Unit 499323, you are currently out of standard.[SOFTBLOCK] Slave units are not to have visible hair folicles or wear non-protective clothing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] So, remove that dress.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Affirmative.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No, no.[SOFTBLOCK] Slowly.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Like this?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh dear, slave units are so incompetent.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I'll have to 'train' you...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Train me![SOFTBLOCK] Train me, lord unit![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie![SOFTBLOCK] You're supposed to be a reprogrammed blank golem![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Oops![SOFTBLOCK] I'm sorry![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's okay, you're doing great for your first time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Unit 499323 standing by.[SOFTBLOCK] Please begin training procedure.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Your former self had a whip?[SOFTBLOCK] That will do.[SOFTBLOCK] Unit, expose rear port for training program.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Affirmative![SOFTBLOCK] Beginning training program![SOFTBLOCK] Hee hee!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:13.0x1.0x0") ]])
    fnCutsceneBlocker()

--Betray. Corrupted ending.
elseif(sObjectName == "Betray") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Unit 499323.[SOFTBLOCK] In a previous iteration, you aided a rebel uprising.[SOFTBLOCK] You were punished for this.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Affirmative, lord unit.[SOFTBLOCK] This unit deserved its punishment.[SOFTBLOCK] This unit now serves the Cause of Science.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Upload all information about known rebel activities.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Affirmative.[SOFTBLOCK] Beginning network upload.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] You must be brought to code.[SOFTBLOCK] Shave your head, remove your clothes.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] You are a machine, not an individual.[SOFTBLOCK] Confirm.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Unit 499323...[SOFTBLOCK] will...") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave:[VOICE|Sophie] Unit 499323 reporting.[SOFTBLOCK] Please input function assignment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] You will report to sector 42 for reassignment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave:[VOICE|Sophie] Affirmative.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Upload all remaining memory files of the rebel activity you supported.[SOFTBLOCK] Once upload is complete, delete those files.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave:[VOICE|Sophie] Affirmative.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Work at maximum efficiency without complaint.[SOFTBLOCK] Machines do not require social interaction.[SOFTBLOCK] Obey the Cause of Science.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave:[VOICE|Sophie] Affirmative.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] Reprogramming satisfactory.[SOFTBLOCK] Memory file descrambled.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "771852:[E|Serious] All obstacles to reactivation of consciousness removed.[SOFTBLOCK] Unit 771852 activating consciousness...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusFinaleA", "FORCEPOS:29.0x3.0x0") ]])
    fnCutsceneBlocker()
end
