--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Exit.
if(sObjectName == "ToExteriorED") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorED", "FORCEPOS:58.0x7.0x0")

--Exit.
elseif(sObjectName == "ToLRTB") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTB", "FORCEPOS:12.0x15.0x0")
    
--[Vents]
elseif(sObjectName == "VentSW") then
	fnCrawlThroughVent(15.25, 11.50, 18.25, 5.50)
    
elseif(sObjectName == "VentNE") then
	fnCrawlThroughVent(18.25, 5.50, 15.25, 11.50)

--[Objects]
--Locked doors.
elseif(sObjectName == "LockedDoor") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] *tap tap tap*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] No good, the entire system is on lockdown.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I'll need a terminal closer to the core to access it with a dataspike.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There core is on the northwestern end of the facility.[SOFTBLOCK] With the doors locked down, we will need to use the ventilation systems to access it.") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The visitor login terminal.[SOFTBLOCK] It's currently locked down.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Please select a videograph to view while your security escort is prepared.'[SOFTBLOCK] But the terminal is locked down, so no watching naughty slimes...)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Junk") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A broken sensor node.[SOFTBLOCK] Not worth stealing.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end