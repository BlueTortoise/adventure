--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Opening cutscene.
if(sObjectName == "EntryScene") then

	--Variables.
	local iSawLRTOpening = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawLRTOpening", "N")
	
	--Hasn't seen the scene.
	if(iSawLRTOpening == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawLRTOpening", "N", 1.0)
	
		--Move the party.
		fnCutsceneMove("Christine", 12.25, 14.50)
		fnCutsceneMove("55", 11.25, 14.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is there nobody here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Seems they haven't caught on to us yet...") ]])
		fnCutsceneBlocker()
		
		--Lights go off.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Activate Lights") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Golem] Attention all units::[SOFTBLOCK] unidentified intruder detected in the facility.[SOFTBLOCK] All units to combat stations.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You were saying?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55: ...[SOFTBLOCK] This was expected.[SOFTBLOCK] I'm just surprised they didn't sound the alarm when they saw us on the external cameras.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So what do we do now?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is a remote-access terminal we can use in the northwestern corner of the facility.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The security alert will have locked out the other terminals, but we might be able to find something useful on them anyway.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|PDU][EMOTION|Christine|PDU] Correct.[SOFTBLOCK] I will be able to scan a terminal's cache from close range if required.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay, better get looking around then...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you discomforted?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Well, last time I was poking around a dark facility by myself...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Don't be ridiculous.[SOFTBLOCK] This is nothing like that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Maybe not to you it isn't.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You just stay close to me, okay?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Affirmative.[SOFTBLOCK] It would be dangerous to split up.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Not what I meant...") ]])
		fnCutsceneBlocker()
		
		--Move 55 onto Christine.
		fnCutsceneMove("55", 12.25, 14.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
        
        fnCutsceneInstruction([[ AL_SetProperty("Music", "LAYER|Telecomm") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 0.0) ]])
	
    end

--Falling out of the vents.
elseif(sObjectName == "FallScene") then
    if(gbFallIntoLRTA == nil) then return end
    gbFallIntoLRTA = nil

    --Black the screen out.
    fnCutsceneTeleport("Christine", -100.25, -100.50)
    fnCutsceneTeleport("55", -100.25, -100.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneFace("55", 0, 1)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 115.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (4.25 * gciSizePerTile), (11.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Fade back in.
    fnCutsceneWait(75)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()

    --Christine teleports in and falls.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Fall") ]])
    fnCutsceneBlocker()
    for i = 1, 20, 1 do
        fnCutsceneTeleport("Christine", 4.75, 11.50 + (i * 3.0 / 20.0) - 3.0)
        fnCutsceneTeleport("55",        4.75, 11.50 + (i * 3.0 / 20.0) - 3.0)
        fnCutsceneWait(1)
        fnCutsceneBlocker()
    end
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])

end
