--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusLRTC"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    AL_SetProperty("Music", "LAYER|Telecomm")
	local iSawSouthernCoreCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N")
    if(iSawSouthernCoreCutscene == 0.0) then
        AL_SetProperty("Mandated Music Intensity", 0.0)
    else
        AL_SetProperty("Mandated Music Intensity", 100.0)
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
    AM_SetMapInfo("No Special Map", "Null", 0, 0)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast6")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast0")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast1",   0,   10)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast2",  11,   20)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast3",  21,   30)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast5",  31,   50)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast4",  51, 1000)
    AM_SetProperty("Advanced Map Properties", 1624.0 - (13*16), 720.0 - (10*16), 70.0 / 96.0, 70.0 / 96.0)
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
	
	--[Lighting]
	AL_SetProperty("Activate Lights")
	AL_SetProperty("Set Player Light No Drain", true)

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	
	--[Lighting]
	--Activate and deactivate lights based on which variables are set.
	local iLRTCLightSectionA = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionA", "N")
	local iLRTCLightSectionB = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionB", "N")
	local iLRTCLightSectionC = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N")
	local iLRTCLightSectionD = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N")
	local iLRTHBLightSectionA = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTHBLightSectionA", "N")
	
	--Section A. Used to enter the room proper.
	if(iLRTCLightSectionA == 0.0) then
		AL_SetProperty("Disable Light", "LgtA00")
		AL_SetProperty("Disable Light", "LgtA01")
		AL_SetProperty("Disable Light", "LgtA02")
		AL_SetProperty("Disable Light", "LgtA03")
		AL_SetProperty("Disable Light", "LgtA04")
		AL_SetProperty("Disable Light", "LgtA05")
		AL_SetProperty("Disable Light", "LgtA06")
		AL_SetProperty("Disable Light", "LgtA07")
	end
	
	--Section B. Northmost door.
	if(iLRTCLightSectionB == 0.0) then
		AL_SetProperty("Disable Light", "LgtB00")
		AL_SetProperty("Disable Light", "LgtB01")
		AL_SetProperty("Disable Light", "LgtB02")
		AL_SetProperty("Disable Light", "LgtB03")
	end
	
	--Section C. Eastern doors.
	if(iLRTCLightSectionC == 0.0) then
		AL_SetProperty("Disable Light", "LgtC00")
		AL_SetProperty("Disable Light", "LgtC01")
		AL_SetProperty("Disable Light", "LgtC02")
		AL_SetProperty("Disable Light", "LgtC03")
		AL_SetProperty("Disable Light", "LgtC04")
	end
	
	--Section D. Southern
	if(iLRTCLightSectionD == 0.0) then
		AL_SetProperty("Disable Light", "LgtD00")
		AL_SetProperty("Disable Light", "LgtD01")
		AL_SetProperty("Disable Light", "LgtD02")
		AL_SetProperty("Disable Light", "LgtD03")
		AL_SetProperty("Disable Light", "LgtD04")
		AL_SetProperty("Disable Light", "LgtD05")
		AL_SetProperty("Disable Light", "LgtD06")
		AL_SetProperty("Disable Light", "LgtD07")
	end
	
	--Section E. Northernmost.
	if(iLRTHBLightSectionA == 0.0) then
		AL_SetProperty("Disable Light", "LgtE00")
		AL_SetProperty("Disable Light", "LgtE01")
		AL_SetProperty("Disable Light", "LgtE02")
		AL_SetProperty("Disable Light", "LgtE03")
	end
end
