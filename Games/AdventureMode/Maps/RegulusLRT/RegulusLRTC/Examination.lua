--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Exit.
if(sObjectName == "ToLRTBNorth") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTB", "FORCEPOS:21.0x10.0x0")
	
--Exit.
elseif(sObjectName == "ToLRTBSouth") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTB", "FORCEPOS:21.0x16.0x0")
	
--Exit.
elseif(sObjectName == "ToLRTHA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTHA", "FORCEPOS:11.0x21.0x0")

--Exit.
elseif(sObjectName == "ToLRTIA") then
	
	--Variables.
	local iLRTHBLightSectionA = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTHBLightSectionA", "N")
	if(iLRTHBLightSectionA == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusLRTIA", "FORCEPOS:13.5x26.0x0")
	end

--[Doors]
elseif(sObjectName == "DoorA") then
	
	--Variables.
	local iLRTCLightSectionB = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionB", "N")
	if(iLRTCLightSectionB == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorA")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorB") then
	
	--Variables.
	local iLRTCLightSectionA = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionA", "N")
	if(iLRTCLightSectionA == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorB")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorC") then
	
	--Variables.
	local iLRTCLightSectionC = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N")
	if(iLRTCLightSectionC == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorC")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DoorE") then
	
	--Variables.
	local iLRTCLightSectionC = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N")
	if(iLRTCLightSectionC == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorE")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorI") then
	
	--Variables.
	local iLRTCLightSectionD = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N")
	if(iLRTCLightSectionD == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorI")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorJ") then
	
	--Variables.
	local iLRTCLightSectionD = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N")
	if(iLRTCLightSectionD == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorJ")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorK") then
	
	--Variables.
	local iLRTCLightSectionD = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N")
	if(iLRTCLightSectionD == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorK")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end

--[Terminals]
--Flavour text.
elseif(sObjectName == "TerminalA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](This terminal was last set to a directory of all the drones working here.[SOFTBLOCK] But, they have no secondary designations, so it just a long list of numbers...)") ]])
	fnCutsceneBlocker()
	
--Terminal that unlocks the doors, IF the player has completed the west side of the facility.
elseif(sObjectName == "TerminalB") then

	--Variables.
	local iSawSouthernCoreCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N")
	local iSawLightCutscene        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawLightCutscene", "N")
	
	--Locked out.
	if(iSawSouthernCoreCutscene == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[SOFTBLOCK] It's no good, this terminal is locked.[SOFTBLOCK] Can you hack it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I could -[SOFTBLOCK] oh.[SOFTBLOCK] Great, a blue box.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[SOFTBLOCK] Help a simple repair robot out?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Quantum blue box encryption.[SOFTBLOCK] The passphrase changes each time it's accessed, and is quantum-entangled to a box somewhere else.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] So I'd have to hack it for every single action on it.[SOFTBLOCK] Every file access, every menu query.[SOFTBLOCK] It'd take weeks to do anything.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] So we need to find the blue box?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Or lift the security lockdown with an auth code.[SOFTBLOCK] In either case, we'd need access to the datacore.[SOFTBLOCK] Let's find another way.") ]])
		fnCutsceneBlocker()

	--Saw the datacore scene.
	elseif(iSawSouthernCoreCutscene == 1.0 and iSawLightCutscene == 0.0) then
	
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawLightCutscene", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionA", "N", 1.0)
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] This terminal is locked.[SOFTBLOCK] 55?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I downloaded the auth codes for most of these terminals from the datacore.[SOFTBLOCK] I can lift the lockdown...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Got it.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Lights turn on.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA00") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA01") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA02") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA03") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA04") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA05") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA06") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA07") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The floor lights indicate which door each console is responsible for.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Guess we have to go around.[SOFTBLOCK] Where's our objective?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Northern end of this sector...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Show the target.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Max Move Speed", 2.0) 
			CameraEvent_SetProperty("Focus Position", (19.75 * gciSizePerTile), (4.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneWait(125)
		fnCutsceneBlocker()
		
		--Show the target.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Max Move Speed", 2.0) 
			CameraEvent_SetProperty("Focus Actor Name", "Christine")
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Wait -[SOFTBLOCK] does this seem like a puzzle to you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It's called a security system, Christine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good.[SOFTBLOCK] [EMOTION|Christine|Angry]Because I HATE PUZZLES.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ...[SOFTBLOCK] Sorry, lost my composure.[SOFTBLOCK][EMOTION|Christine|Neutral] Won't happen again.[SOFTBLOCK] Let's be off.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] (Yikes.)") ]])
		fnCutsceneBlocker()

	--All other cases.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (The door that the lights lead to should be open now...)") ]])
		fnCutsceneBlocker()
	end
	
--Opens doors.
elseif(sObjectName == "TerminalC") then

	--Variables.
	local iLRTCLightSectionB = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionB", "N")
	
	--Activate it.
	if(iLRTCLightSectionB == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionB", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Disabling the security lockout...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Lights turn on.
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtB00") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtB01") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtB02") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtB03") ]])
		fnCutsceneBlocker()

	--Already activated.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (The door that the lights lead to should be open now...)") ]])
		fnCutsceneBlocker()
	end
	
--Opens doors.
elseif(sObjectName == "TerminalD") then

	--Variables.
	local iLRTCLightSectionC = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N")
	
	--Activate it.
	if(iLRTCLightSectionC == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Disabling the security lockout...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Lights turn on.
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtC00") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtC01") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtC02") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtC03") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtC04") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()

	--Already activated.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (The doors that the lights lead to should be open now...)") ]])
		fnCutsceneBlocker()
	end
	
--Opens doors.
elseif(sObjectName == "TerminalE") then

	--Variables.
	local iLRTCLightSectionD = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N")
	
	--Activate it.
	if(iLRTCLightSectionD == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Disabling the security lockout...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Lights turn on.
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtD00") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtD01") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtD02") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtD03") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtD04") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtD07") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtD06") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtD05") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()

	--Already activated.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (The doors that the lights lead to should be open now...)") ]])
		fnCutsceneBlocker()
	end

--[Objects]
--Locked doors.
elseif(sObjectName == "LockedDoor") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "GenTermA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Searching for logs on Unit 2855...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unit 2855 Reprimand Notice.[SOFTBLOCK] Apparently, she got into a fight with a bunch of Lord Units...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There is an implicit no-cutting-in-line rule at the cafeteria in the Geology Research Block of the Arcane University.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (2855 apparently saw a unit cut in line and proceeded to beat that unit, and other nearby units, into system standby until explicit orders from Central told her to stop.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Though it seems nobody has cut in line a single time since that day...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "GenTermB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Searching for logs on Unit 2855...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A few fragmented logs on Unit 2855 including service commendations.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This log file indicates that unauthorized appropriations decrease in whatever sector she is stationed in by at least 25 percent.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (No action is required, merely stationing her causes the drop.[SOFTBLOCK] The log recommends creating Unit 2855 body-doubles to get the most of this.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "GenTermC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Searching for logs on Unit 2855...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Part of a repair manual written by Unit 2855...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It logs proper care and maintenance of hyperweave clothing, as the unique magnetic refractor properties are less efficient when the clothing is dirty.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "GenTermD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A security log from Sector 388.[SOFTBLOCK] Apparently, that sector had several 'The Administration is Watching' motivational posters.[SOFTBLOCK] One of them got defaced by a Slave Unit.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The security cameras had been tampered with, but Unit 2855 tracked down the culprit and beat them to system standby in front of half of the sector's workers.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (From then on, 'Unit 2855 is Watching' posters were posted.[SOFTBLOCK] They were never defaced.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A fabrication bench.[SOFTBLOCK] It hasn't been used in a while.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end