--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusLRTD"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    AL_SetProperty("Music", "LAYER|Telecomm")
    AL_SetProperty("Mandated Music Intensity", 0.0)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
    AM_SetMapInfo("No Special Map", "Null", 0, 0)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast6")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast0")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast1",   0,   10)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast2",  11,   20)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast3",  21,   30)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast5",  31,   50)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast4",  51, 1000)
    AM_SetProperty("Advanced Map Properties", 1034.0 - (76*16), 440.0 - (25*16), 70.0 / 96.0, 70.0 / 96.0)
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
	
	--[Lighting]
	AL_SetProperty("Activate Lights")
	AL_SetProperty("Set Player Light No Drain", true)
	
	--[Right Conversion Tube]
	--This tube is a special tube since 2855 uses it during the Latex Drone TF.
	TA_Create("ConversionTube")
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Facing", gci_Face_East)
		TA_SetProperty("Activation Script", "Null")
		fnSetApartmentObjectGraphics(gci_Quarters_DefragPod)
		
		--Special activation script and directions.
		TA_SetProperty("Activation Script", gsRoot .. "Maps/RegulusLRT/RegulusLRTD/Activation Tube.lua")
		for q = 1, 4, 1 do
			TA_SetProperty("Move Frame", 0, q-1, "Root/Images/Sprites/Quarters/Tube1")
			TA_SetProperty("Move Frame", 2, q-1, "Root/Images/Sprites/Quarters/Tube0")
			TA_SetProperty("Move Frame", 4, q-1, "Root/Images/Sprites/Quarters/Tube55")
			TA_SetProperty("Move Frame", 6, q-1, "Root/Images/Sprites/Quarters/Tube55")
		end
	DL_PopActiveObject()
	
	--Move the defragmentation pod. It needs to be offset with decimals.
	fnCutsceneTeleport("ConversionTube", 65.25, 11.05 + 0.50)
			
	--Set extra options. Rendering depth must be set AFTER the object is moved.
	EM_PushEntity("ConversionTube")
		TA_SetProperty("Rendering Depth", -0.500000 + (12.00 * 0.000100))
	DL_PopActiveObject()

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.

end
