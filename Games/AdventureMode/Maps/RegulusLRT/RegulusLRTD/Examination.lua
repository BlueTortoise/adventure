--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Exit.
if(sObjectName == "ToLRTB") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTB", "FORCEPOS:4.0x5.0x0")

--Exit.
elseif(sObjectName == "ToLRTE") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTE", "FORCEPOS:32.0x32.0x0")

--[Objects]
--Locked doors.
elseif(sObjectName == "LockedDoor") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
	fnCutsceneBlocker()
	
--Locked elevator.
elseif(sObjectName == "LockedElevator") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Elevator access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
	fnCutsceneBlocker()

--[Vent Crawling Action]
elseif(sObjectName == "VentAEast") then
	
	--Subroutine does the work.
	fnCrawlThroughVent(68.25, 4.50, 50.25, 4.50)
	
--Western edge.
elseif(sObjectName == "VentAWest") then
	fnCrawlThroughVent(50.25, 4.50, 68.25, 4.50)
	
--Western edge.
elseif(sObjectName == "VentBWest") then
	fnCrawlThroughVent(3.25, 5.50, 31.25, 5.50)
	
--Eastern edge.
elseif(sObjectName == "VentBEast") then
	fnCrawlThroughVent(31.25, 5.50, 3.25, 5.50)
	
	
--[Elevator E]
--Allows the player to enter the main floor.
elseif(sObjectName == "ElevatorESouth") then

	--Open the elevator.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "ElevatorESouth") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move Christine and 55 inside.
	fnCutsceneMove("Christine", 10.25, 21.50)
	fnCutsceneMove("55", 11.25, 21.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneFace("55", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Close the elevator.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Close Door", "ElevatorESouth") ]])
	
	--Teleport both character offscreen. Move the camera north.
	fnCutsceneTeleport("Christine", -100.25, -100.50)
	fnCutsceneTeleport("55", -100.25, -100.50)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 0.5)
		CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (13.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Teleport the characters back and set the camera to follow Christine again.
	fnCutsceneTeleport("Christine", 10.25, 13.50)
	fnCutsceneTeleport("55", 11.25, 13.50)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 2.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "ElevatorENorth") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move the party out.
	fnCutsceneMove("Christine", 10.25, 11.50)
	fnCutsceneMove("55", 11.25, 11.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Close the door.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Close Door", "ElevatorENorth") ]])
	fnCutsceneBlocker()
	
	--Move 55 onto Christine.
	fnCutsceneMove("55", 10.25, 11.50)
	fnCutsceneBlocker()
	
	--Fold party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Northern edge.
elseif(sObjectName == "ElevatorENorth") then

	--Open the elevator.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "ElevatorENorth") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move Christine and 55 inside.
	fnCutsceneMove("Christine", 10.25, 13.50)
	fnCutsceneMove("55", 11.25, 13.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneFace("55", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Close the elevator.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Close Door", "ElevatorENorth") ]])
	
	--Teleport both character offscreen. Move the camera south.
	fnCutsceneTeleport("Christine", -100.25, -100.50)
	fnCutsceneTeleport("55", -100.25, -100.50)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 0.5)
		CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (21.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Teleport the characters back and set the camera to follow Christine again.
	fnCutsceneTeleport("Christine", 10.25, 21.50)
	fnCutsceneTeleport("55", 11.25, 21.50)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 2.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "ElevatorESouth") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move the party out.
	fnCutsceneMove("Christine", 10.25, 19.50)
	fnCutsceneMove("55", 11.25, 19.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Close the door.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Close Door", "ElevatorESouth") ]])
	fnCutsceneBlocker()
	
	--Move 55 onto Christine.
	fnCutsceneMove("55", 10.25, 19.50)
	fnCutsceneBlocker()
	
	--Fold party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--[Objects]
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Notice to All Units::[SOFTBLOCK] Stop reading security notices and resume your hallway patrols![SOFTBLOCK] This is an order!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We need to head north from here.[SOFTBLOCK] Is there a way past these doors?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Once again, the ventilation shafts.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not all of the shafts lead where we need.[SOFTBLOCK] We should try the air duct on the transit floor below us.") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Scraprat Wranglers...[SOFTBLOCK] Drone Units assigned to order packs of Scraprats around.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Better brush up on group combat, some of the packs can get pretty big.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Oh come on![SOFTBLOCK] Even the oilmaker is locked down?[SOFTBLOCK] But what if I need a pick-me-up during a crisis?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "LatexTerminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Repurposing fluid::[SOFTBLOCK] Nominal.[SOFTBLOCK] Tube status::[SOFTBLOCK] Spiffy.[SOFTBLOCK] Lights::[SOFTBLOCK] Green.[SOFTBLOCK] Conversion available.[SOFTBLOCK] Insert unit.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end