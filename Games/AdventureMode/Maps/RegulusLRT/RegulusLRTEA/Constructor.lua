--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusLRTEA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    AL_SetProperty("Music", "LAYER|Telecomm")
    AL_SetProperty("Mandated Music Intensity", 60.0)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
    AM_SetMapInfo("No Special Map", "Null", 0, 0)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest6")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest0")
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest1",   0,   10)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest2",  11,   20)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest3",  21,   30)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest5",  31,   50)
    AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest4",  51, 1000)
    AM_SetProperty("Advanced Map Properties", 1623.0 - (12*16), 1106.0 - (23*16), 70.0 / 96.0, 70.0 / 96.0)
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
	
	--[Lighting]
	AL_SetProperty("Activate Lights")
	AL_SetProperty("Set Player Light No Drain", true)

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.

end
