--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Darkmatter girls watch the player...
if(sObjectName == "DarkmatterWatch") then
	local iSawSouthernCoreCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N")
	if(iSawSouthernCoreCutscene == 0.0) then
		EM_PushEntity("DarkmatterA")
			TA_SetProperty("Face Character", "PlayerEntity")
		DL_PopActiveObject()
		EM_PushEntity("DarkmatterB")
			TA_SetProperty("Face Character", "PlayerEntity")
		DL_PopActiveObject()
		EM_PushEntity("DarkmatterC")
			TA_SetProperty("Face Character", "PlayerEntity")
		DL_PopActiveObject()
	end
	
--This Darkmatter is in the SW corner of the map.
elseif(sObjectName == "DarkmatterWatchSW") then
	local iSawSouthernCoreCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N")
	if(iSawSouthernCoreCutscene == 0.0) then
		EM_PushEntity("DarkmatterD")
			TA_SetProperty("Face Character", "PlayerEntity")
		DL_PopActiveObject()
	end
end
