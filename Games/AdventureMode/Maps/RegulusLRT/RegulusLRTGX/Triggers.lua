--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Cutscene on the southern end of the data center.
if(sObjectName == "SceneTrigger") then

	--Variables.
	local iSawSouthernCoreCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N")
	if(iSawSouthernCoreCutscene == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N", 1.0)
		
		--Execute.
		LM_ExecuteScript(gsRoot .. "Chapter5Scenes/LRT EDG Scene Part 4/Scene_Begin.lua")
	end
    
--Shortcut.
elseif(sObjectName == "SceneTriggerB") then

    --Variables.
    local iSawShortcutConversation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawShortcutConversation", "N")
    if(iSawShortcutConversation == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawShortcutConversation", "N", 1.0)

    --Pan the camera over.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 0.5)
        CameraEvent_SetProperty("Focus Position", (22.25 * gciSizePerTile), (14.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()

    --Pan the camera back to Christine.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 0.5)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It looks like our interlocutor has opened a path for us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I do not have schematics for that area.[SOFTBLOCK] We should search it.") ]])
    fnCutsceneBlocker()

end
