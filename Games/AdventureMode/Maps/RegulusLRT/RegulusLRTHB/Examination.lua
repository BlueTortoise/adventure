--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Exit.
if(sObjectName == "ToLRTHA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTHA", "FORCEPOS:19.0x21.0x0")
	
--Exit.
elseif(sObjectName == "ToLRTHC") then
	
	--Variables.
	local iLRTHBLightSectionA = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTHBLightSectionA", "N")
	if(iLRTHBLightSectionA == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusLRTHC", "FORCEPOS:4.5x11.0x0")
	end

--Exit.
elseif(sObjectName == "ToLRTHD") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusLRTHD", "FORCEPOS:4.5x21.0x0")

--[Vents]
--Southern Vent.
elseif(sObjectName == "VentWest") then
	fnCrawlThroughVent(17.25, 49.50, 32.25, 49.50)
	
--Southern Vent.
elseif(sObjectName == "VentEast") then
	fnCrawlThroughVent(32.25, 49.50, 17.25, 49.50)

--[Objects]
--Locked doors.
elseif(sObjectName == "LockedDoor") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
	fnCutsceneBlocker()

--Broken glass.
elseif(sObjectName == "BrokenGlass") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](The reinforced glass was shattered from the inside.[SOFTBLOCK] The force needed would be immense...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](The security access wire goes under the concrete and emerges on the northern end of the room.)") ]])
	fnCutsceneBlocker()
	
--Locked doors.
elseif(sObjectName == "BrokenAirlock") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Airlock exterior door is malfunctioning.[SOFTBLOCK] Please contact the maintenance crew.") ]])
	fnCutsceneBlocker()

--Security Terminal.
elseif(sObjectName == "SecurityTerminal") then

	--Variables.
	local iCheckedSecurityConsole = VM_GetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N")
	local iTalkedWithRehabGolem   = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N")
    local iCompletedBlackSite     = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	
	--Player doesn't have the code:
	if(iTalkedWithRehabGolem == 0.0) then
		
		--First time checking the console:
		if(iCheckedSecurityConsole == 0.0) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N", 1.0)
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] This console is locked out.[SOFTBLOCK] 55, do you have the access code?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I should...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] No I do not.[SOFTBLOCK] In fact, this console is set to an access code that is not even used in the Long-Range Telemetry facility.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm, do you think this console was taken from another facility and they didn't reset the code?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] The serial number...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] PDU, please confirm the schematics of the LRT facility.[SOFTBLOCK] Check the area just east of us.[SOFTBLOCK] What's there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[EMOTION|Christine|PDU] The door on the easten edge of the building leads to a mineshaft.[SOFTBLOCK] It has not been used in 16 years, well before the LRT facility was constructed.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] PDU, please check the power levels being sent to the eastern edge of the building.[SOFTBLOCK] Do they match an unused mineshaft?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Power transfer levels are 341,000 percent higher than expected for an unused facility.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just what are you getting at, 55?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It's a black site.[SOFTBLOCK] But -[SOFTBLOCK][EMOTION|2855|Down] I think I already knew that.[SOFTBLOCK] The layouts of these hallways are familiar to me.[BLOCK][CLEAR]") ]])
            
            --Christine doesn't know about the black sites.
            if(iCompletedBlackSite < 10.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] What do you mean by a 'black site'?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Sections of Regulus that do not officially exist.[SOFTBLOCK] When the administration wishes to do something that the Slave Units are best not knowing about, it is done in black sites.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Considering the staffing here and the high-security nature, the LRT facility is a good fit for one such site.[SOFTBLOCK] There are more beneath Regulus City, I believe.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[SOFTBLOCK] My memory drives were wiped, and yet I'm almost certain I know where most of them are...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You were Head of Security.[SOFTBLOCK] You would probably know...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I visited them.[SOFTBLOCK] Personally.[SOFTBLOCK] So many times that my motivators updated their flash chips for their layouts.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] 55, this may be inappropriate, but...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] My grandfather's journal contained references to something like what you'd call a black site.[SOFTBLOCK] They're one of the reasons I refused to join the army.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] He -[SOFTBLOCK] he -[SOFTBLOCK] he was not a good man.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just -[SOFTBLOCK] don't read into that.[SOFTBLOCK] We don't know for certain what's down there.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If we want the access codes, we'll need to find out.[SOFTBLOCK] I can't hack this terminal due to the blue box encryption.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Okay.[SOFTBLOCK] But please don't - [SOFTBLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Machines do not suffer emotional crises, Christine.[SOFTBLOCK] I am a machine.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Whatever we find out, I will continue to perform my function, as will you.[SOFTBLOCK] Is that clear?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ... as crystal...") ]])
			
            --She does.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Another black site, just like in the mines.[SOFTBLOCK] Do you think the same...[SOFTBLOCK] things...[SOFTBLOCK] will be in there?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unlikely.[SOFTBLOCK] Otherwise, the containment protocols would be more prepared for something like Vivify.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This does not discount the possibility, just reduces its likelihood.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] And, I am -[SOFTBLOCK] I am certain I have been to this particular site before.[SOFTBLOCK] My motivators have flash memory of them.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] 55, this may be inappropriate, but...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] My grandfather's journal contained references to something like what you'd call a black site.[SOFTBLOCK] They're one of the reasons I refused to join the army.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] He -[SOFTBLOCK] he -[SOFTBLOCK] he was not a good man.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just -[SOFTBLOCK] don't read into that.[SOFTBLOCK] We don't know for certain what's down there.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We must enter to acquire the access codes we need.[SOFTBLOCK] That much is certain.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you are worried about my emotional state, you are wasting your cycles.[SOFTBLOCK] Machines do not suffer emotional crises.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Whatever we find out, I will continue to perform my function, as will you.[SOFTBLOCK] Is that clear?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ... as crystal...") ]])
            end
            
		--All other attempts:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](We need the auth codes from the black site on the eastern edge of the facility...)") ]])
			fnCutsceneBlocker()
	
		end
	
	--Has the auth codes.
	else
		
		--First time checking the console:
		if(iCheckedSecurityConsole == 0.0) then
	
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N", 1.0)
			VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTHBLightSectionA", "N", 1.0)
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The warden's authcodes were accepted.") ]])
			fnCutsceneBlocker()

			--Lights turn on.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA00") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA01") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA03") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA02") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA11") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA09") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA04") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA05") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA12") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA13") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA10") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA06") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA07") ]])
			fnCutsceneInstruction([[ AL_SetProperty("Enable Light", "LgtA08") ]])
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The door to the prisoner exchange lifts should be open now.[SOFTBLOCK] We can access the transit tunnels from there.") ]])
			fnCutsceneBlocker()
			
		--Repeat.
		else	
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](We should be able to backtrack to the exchange lifts now that this terminal is unlocked...)") ]])
			fnCutsceneBlocker()
		end
	end
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end