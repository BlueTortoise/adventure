--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToLRTHB") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusLRTHB", "FORCEPOS:27.5x44.0x0")

--[Examinables]
elseif(sObjectName == "Terminal") then

    --Variables.
    local iSawArmoryList = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawArmoryList", "N")
    if(iSawArmoryList == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawArmoryList", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, check the armory logs.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No problem...[SOFTBLOCK] PDU?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Encryption bypassed.[SOFTBLOCK] I do so enjoy burglary.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you serious, PDU?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Humour protocols remain active.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Too bad, 55.[SOFTBLOCK] The security units took everything not nailed down.[SOFTBLOCK] Most of the armory is what we saw outside, melted.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I was not expecting an equipment upgrade.[SOFTBLOCK] Stealing these weapons could be useful for a later armed insurrection, but is not vital to our current operation.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] This does mean that the security teams were insufficient.[SOFTBLOCK] They had time to arm themselves, and were not caught out of position.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Why is that making you smile?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It means that they are similarly unprotected elsewhere.[SOFTBLOCK] The LRT facility has a number of elite units stationed at it for obvious reasons.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] If we were to unleash Project Vivify on a facility of our choosing...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No.[SOFTBLOCK] Absolutely not.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Merely a suggestion.[SOFTBLOCK] Controlling the creature is extremely dangerous, I would not advise it unless we were to learn a direct, safe method of command.[SOFTBLOCK] But, were such an opportunity to present itself...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Still no![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] She is not an attack dog, 55![SOFTBLOCK] What is wrong with you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am being practical.[SOFTBLOCK] If we intended to destroy her, sending her at our enemies before doing so would deal them crippling damage.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is very easy to win a three-way battle if you can choose when to engage.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I absolutely forbid it.[SOFTBLOCK] Don't mention such a thing again.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As you like.") ]])
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Leader] (The armory has been stripped of firearms, according to the logs.[SOFTBLOCK] There may be something useful if we search.)") ]])
        
    end
    
elseif(sObjectName == "EMPBox") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] EMP grenades, but the box is empty.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] EMP grenades wouldn't work against an organic that spits acid, would they?[SOFTBLOCK] Am I forgetting something?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your assessment is correct.[SOFTBLOCK] This indicates that the security units did not know they were dealing with an organic enemy.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Or, they were sufficiently desperate.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, there's none left.[SOFTBLOCK] Moving on.") ]])
        
elseif(sObjectName == "ImplosionBox") then

    --Variables.
    local iGotImplosionGrenades = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotImplosionGrenades", "N")
    if(iGotImplosionGrenades == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iGotImplosionGrenades", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Ah, the so called 'Seris Class Anti-Pressure Grenade'.[SOFTBLOCK] Not many of these were manufactured.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Is there a reason?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] When used carelessly, they are extremely dangerous and tend to destroy targets intended to be incapacitated.[SOFTBLOCK] They are not useful for riot control.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] When detonating, the grenade temporarily generates a microsingularity for 2-3 zeptoseconds.[SOFTBLOCK] Targets within range are pulled into the center, usually colliding with one another.[SOFTBLOCK] The more targets affected, the more catastrophic the impact.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yeah, throwing that in a crowded room would get pretty messy...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Failing their original purpose, not many were produced.[SOFTBLOCK] It seems the security units here decided not to equip them.[SOFTBLOCK] So much the better for us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You're gonna be careful with those, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Please pay extra attention when engaging in melee combat, Christine.[SOFTBLOCK] Your chassis serves a more useful purpose than mere battering ram.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] (55 has learned the 'Implosion Grenade' ability.)") ]])
        AC_PushPartyMember("55")
            LM_ExecuteScript(gsRoot .. "Abilities/55/000 Initializer.lua", "Implosion Grenade")
        DL_PopActiveObject()
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Microsingularities sound kind of absurdly dangerous...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The size of the singularity versus its mass means it persists for an extremely brief time.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Most of the grenade's casing it designed to contain the singularity and provide mass for it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Unfortunately, the laws of physics do not make it possible to modify the grenade to exhibit more power.[SOFTBLOCK] Keeping the singularity active for longer requires magnitudinal increases in mass.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] How reassuring...") ]])
        
    end

elseif(sObjectName == "FlashbombBox") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Flash Grenades, designed to shut down a unit's optical receptors.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Ineffective against properly armored units, designed for suppressing civilians.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Would they be of any use to us?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not when the security teams are using them.[SOFTBLOCK] The boxes are empty.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dash it.[SOFTBLOCK] Let's keep going.") ]])
    
elseif(sObjectName == "Gloves") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Only the special radiation-resistant gloves are here.[SOFTBLOCK] They're not suited for combat.[SOFTBLOCK] All the other armor was stripped.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ArmorRacks") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The flak armor racks are bare.)") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "Guns") then

    --Variables.
    local iScavengedArmoryGuns = VM_GetVar("Root/Variables/Chapter5/Scenes/iScavengedArmoryGuns", "N")
    
    --First time, get an item.
    if(iScavengedArmoryGuns == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iScavengedArmoryGuns", "N", 1.0)

        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Stripped.[SOFTBLOCK] The only remaining firearms are ones noted to be defective and in need of maintenance.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Let me take a look...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm, no,[SOFTBLOCK] hmm, maybe...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Can you perhaps use this Magrail Harmonizer?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It seems the quartermaster neglected to remove it.[SOFTBLOCK] These are carefully controlled, meant only for elite security teams.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Judging from what happened outside...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] They will have no further use for it.[SOFTBLOCK] Let us proceed.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] [SOUND|World|TakeItem](Received Magrail Harmonizer Module)") ]])
        LM_ExecuteScript(gsItemListing, "Magrail Harmonizer Module")
    
    --Repeats.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (These guns are defective and I've already removed the useful parts.)") ]])
        fnCutsceneBlocker()

    end

elseif(sObjectName == "LockedDoor") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail]Access denied.[SOFTBLOCK] Security lockdown in effect.") ]])
	fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end