--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Entry cutscene. Triggers once.
if(sObjectName == "EntryScene") then

	--Variables.
	local iSawPrisonEntryScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPrisonEntryScene", "N")
	if(iSawPrisonEntryScene == 0.0) then
		
		--Variables.
		local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPrisonEntryScene", "N", 1.0)
		
		--Move up.
		fnCutsceneMove("Christine", 4.75, 31.50)
		fnCutsceneMove("55",        5.75, 31.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneFace("55",        0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: GREETINGS, COMMAND UNIT.[SOFTBLOCK] HOW MAY THIS UNIT SERVE YOU?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *They're not attacking?*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] *They probably don't know about the base status.[SOFTBLOCK] Remember, they're isolated from the grid.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Drone, report on the security status of this site.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: EVERYTHING IS SPIFFY, COMMAND UNIT.[SOFTBLOCK] NO INTRUDERS HAVE BEEN DETECTED.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: IF YOU WOULD LIKE TO TRANSFER YOUR CARGO FOR REPROGRAMMING OR REPURPOSEMENT, WE CAN HAVE A COURIER FOR YOU IN A MOMENT.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Cargo?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] That'd be you.[BLOCK][CLEAR]") ]])
		
		--Human:
		if(sChristineForm == "Human") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Drone, this human is my property.[SOFTBLOCK] See that the security grid is updated to allow her through.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: AFFIRMATIVE, COMMAND UNIT.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] EXCUSE ME?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] By default, humans have no rights when not bearing a security badge.[SOFTBLOCK] The drones in this site will leave you alone, now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Mgrgmgr...[BLOCK][CLEAR]") ]])
		
		--Golem:
		elseif(sChristineForm == "Golem") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Drone, Unit 771852 is to be afforded full command privileges, effective immediately.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: AFFIRMATIVE, COMMAND UNIT.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: GREETINGS, LORD UNIT.[SOFTBLOCK] HOW MAY THIS UNIT SERVE YOU?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Could you curtsy?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: AFFIRMATIVE, LORD UNIT.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "*The drone does an awkward curtsy.*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Hee hee hee![SOFTBLOCK] Oh, I'm already letting the power go to my head.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Shall we continue...?[BLOCK][CLEAR]") ]])
		
		--Latex Drone:
		elseif(sChristineForm == "LatexDrone") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Drone, Unit 771852 is in my retinue.[SOFTBLOCK] Update her permissions.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: AFFIRMATIVE, COMMAND UNIT.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] *Play along, Christine*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] THANK YOU FOR UPDATING MY PERMISSIONS, COMMAND UNIT.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Let us continue, then.[BLOCK][CLEAR]") ]])
		
		--Darkmatter:
		elseif(sChristineForm == "Darkmatter") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Drone, this Darkmatter is my personal pet.[SOFTBLOCK] No harm is to come to her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: AFFIRMATIVE, COMMAND UNIT.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *The things I let you get away with...*[BLOCK][CLEAR]") ]])
		
		--Raiju. Not nominally possible, but here it is.
		elseif(sChristineForm == "Raiju") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Drone, this Raiju is my personal pet.[SOFTBLOCK] No harm is to come to her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: AFFIRMATIVE, COMMAND UNIT.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] *Pet?[SOFTBLOCK] Careful what you say, or I just might zap you.*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] *Maintain your cover.*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] *Grrr.*[BLOCK][CLEAR]") ]])
		
		--Steam Droid:
		elseif(sChristineForm == "SteamDroid") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Drone, this Steam Droid is an important ambassador.[SOFTBLOCK] Treat her with the respect accorded to Command Units.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: AFFIRMATIVE, COMMAND UNIT.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Very good.[SOFTBLOCK] I am glad these drones are hospitable.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: THANK YOU FOR THE COMPLIMENT, AMBASSADOR.[SOFTBLOCK] HAVE A PLEASANT STAY.[BLOCK][CLEAR]") ]])
		end
		
		--Resume.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Drone, we need to speak to the warden.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: THE WARDEN IS AT HER COMMAND TERMINAL IN THE PRISON BLOCK, DOWN THE LADDER.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone: PLEASE HAVE A FRUSTRATINGLY SAFE DAY.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (They really need to change the adjective generator on that...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Let's get going.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Merge party.
		fnCutsceneMove("55", 4.75, 31.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	end

--Diary cutscene. Triggers once.
elseif(sObjectName == "DiaryScene") then

	--Variables.
	local iSawDiaryScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawDiaryScene", "N")
	if(iSawDiaryScene == 0.0) then
		
		--Variables.
		local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawDiaryScene", "N", 1.0)
		
		--Move up.
		fnCutsceneMove("Christine", 26.75, 31.50)
		fnCutsceneMove("55",        22.75, 31.50)
		fnCutsceneFace("55",        1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you coming?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My motivators are indeed familiar with this building.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We shouldn't dawdle, or the drones might suspect something.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] Christine.[SOFTBLOCK] Tell me about your grandfather.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Which one?[SOFTBLOCK] My mother's side?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I never knew him, he stayed in India and never visited us.[SOFTBLOCK] Mother said her family did not approve of her marriage in the slightest.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The one who wrote the diary you mentioned.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ah, my father's father.[SOFTBLOCK] Conrad Dormer.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 28.75, 31.50, 0.25)
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] The old bastard died well before I was born.[SOFTBLOCK] Apparently, he was on deployment and was shot by a civilian.[SOFTBLOCK] I never found out why.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] My ancestors were all officers, and before that, knights and lords.[SOFTBLOCK] He served in the expeditionary force during World War 2. [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Which was?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] The largest conflict in the history of Earth, where I am from.[SOFTBLOCK] 60 million humans dead in six years.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] That -[SOFTBLOCK] completely dwarfs any conflict in Pandemonium's history...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] The world tore itself apart as ideologies of mass production retooled their factories to produce corpses.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] When I was a little boy I had looked up to my grandfather.[SOFTBLOCK] His portrait hangs in the hall outside the bedrooms.[SOFTBLOCK] Father said he was a great hero in the war.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I believed all of it.[SOFTBLOCK] Father said I had his eyes, and maybe I did.[SOFTBLOCK] I certainly had his intellect.[SOFTBLOCK] But I didn't have his force of will.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Father told me stories about what he did.[SOFTBLOCK] He had led a company of commandos on daring raids.[SOFTBLOCK] His elite squad was the terror of the Reich, striking at night and vanishing before the guards could rouse.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] He told me how he led his men with courage.[SOFTBLOCK] How he once had run onto a battlefield in sight of a machine gun to haul a wounded man back to the lines.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How he had walked calmly as shells burst around him, because an officer could never flinch, lest the men lose their nerve.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How they had resolved once to use only their knives on one mission, and gutted the Jerries like pigs.[SOFTBLOCK] The enemy fled rather than face them the next night.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I wanted to be just like him.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] And then I found his diary.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You see, I had been going through my father's study looking for something.[SOFTBLOCK] I don't remember what.[SOFTBLOCK] And I found grandfather Conrad's diary tucked behind a photo album.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I eagerly read it.[SOFTBLOCK] The stories were true, at least according to him.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But then I read the things he did after the war.[SOFTBLOCK] He was redeployed to West Germany.[SOFTBLOCK] He worked in places that didn't officially exist.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Captured spies were taken there.[SOFTBLOCK] He would beat them.[SOFTBLOCK] Torture them.[SOFTBLOCK] Starve them.[SOFTBLOCK] To find out what they knew, sometimes.[SOFTBLOCK] To break their spirits, always.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] He would write how the Soviet spies were barely human.[SOFTBLOCK] He loved to humiliate them.[SOFTBLOCK] He said the lower orders needed to know their place, and that he was happy to oblige.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] They were your enemies.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] His enemies.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Yes, they were his enemies.[SOFTBLOCK] The enemies he was told to hate.[SOFTBLOCK] When I was young, that's what I thought, too.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But then I realized that the men who he was killing, they -[SOFTBLOCK] well, they were boys.[SOFTBLOCK] Kids.[SOFTBLOCK] Most of them teenagers.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] They would never go to school.[SOFTBLOCK] They would never grow up.[SOFTBLOCK] Find love.[SOFTBLOCK] Start families.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] He was a killer, like any other.[SOFTBLOCK] If killing isn't glorious, then being courageous while doing it is no more glorious.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sophie thinks the work we do out here...[SOFTBLOCK] the sneaking, fighting, searching, hacking.[SOFTBLOCK] She thinks it's sexy.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It's not.[SOFTBLOCK] It's vile and disgusting.[SOFTBLOCK] I'm doing it because to do nothing would be worse.[SOFTBLOCK] But don't you think for a second we'll find redemption out here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Was your home much like Regulus?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] In some ways, no.[SOFTBLOCK] In many ways, yes.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] When I was converted, I didn't want to think about it.[SOFTBLOCK] But, what happened at the cryogenics facility would not be out of place where I come from.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I read his whole diary in one sitting.[SOFTBLOCK] Then I read it again the following night.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] My family's history is not one of glory and heroism.[SOFTBLOCK] That's just the part they like to tell you about.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Cry] The knights of old England did the same thing to peasants that my grandfather did to spies.[SOFTBLOCK] That's my family's legacy.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] When I came of age, I told my father I would never join the army.[SOFTBLOCK] I told him his connections were worthless and his friends were thugs.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What did he do?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] He told me I was young and unschooled in the ways of the world.[SOFTBLOCK] He acted like he was in the right.[SOFTBLOCK] It sickened me.[SOFTBLOCK] I could guess he did the same things when he was stationed in India.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I asked my mother, but she wouldn't hear anything of it.[SOFTBLOCK] She wants to believe he's a good man.[SOFTBLOCK] I know better.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We've not really spoken since.[SOFTBLOCK] I took my trust fund and got an apartment.[SOFTBLOCK] I went to school to learn something useful.[SOFTBLOCK] I became a teacher.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Teachers...[SOFTBLOCK] Teachers don't make corpses for a living...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, I know this place.[SOFTBLOCK] I've been here before.[SOFTBLOCK] Do you think I... [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I would love to believe you're innocent, but I don't think I can fool myself like my mother did.[SOFTBLOCK] And this coming from someone who knows how to fool herself.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But it's not who you were that matters, it's who you are.[SOFTBLOCK] Make your own future, 55.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Merge party.
		fnCutsceneMove("55", 28.75, 31.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	end
end
