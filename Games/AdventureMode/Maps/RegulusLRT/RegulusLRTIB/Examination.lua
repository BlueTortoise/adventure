--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit.
if(sObjectName == "ToLRTIA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTIA", "FORCEPOS:24.0x4.0x0")

--Exit.
elseif(sObjectName == "ToLRTIC") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTIC", "FORCEPOS:23.0x4.0x0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end