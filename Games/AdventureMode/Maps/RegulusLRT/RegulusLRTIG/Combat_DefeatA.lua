--[Combat Defear]
--The party was defeated.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

--Music change.
AudioManager_PlayMusic("Null")

--[Movement]
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--55 Collapses.
if(iIsRelivingScene == 0.0) then
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "55")
        ActorEvent_SetProperty("Special Frame", "Crouch")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "55")
        ActorEvent_SetProperty("Special Frame", "Wounded")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Christine moves up.
    fnCutsceneMoveFace("55", 12.75, 10.50, 0, 1, 0.30)
    fnCutsceneMove("Christine", 12.75, 12.50, 0.50)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sorry.[SOFTBLOCK] Please don't hurt 55.[SOFTBLOCK] She doesn't know you like I do.") ]])
    fnCutsceneBlocker()

    --Christine's form. Transform her to human if she isn't already.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm ~= "Human") then
        
        --Wait a bit.
        fnCutsceneWait(30)
        fnCutsceneBlocker()

        --Flash the active character to white. Immediately after, execute the transformation.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite")
        DL_PopActiveObject()
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total)
        fnCutsceneBlocker()

        --Now wait a little bit.
        fnCutsceneWait(30)
        fnCutsceneBlocker()
        
    end

    --Dialogue.
    fnCutsceneInstruction([[ AudioManager_PlayMusic("Vivify") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Yes Master.[SOFTBLOCK] Thank you...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    --Fade to black.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
end


--Transformation scene.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine lost consciousness...[SOFTBLOCK] No...[SOFTBLOCK] She was awake...[SOFTBLOCK] But the world had gone dark...[SOFTBLOCK] It smelled...[SOFTBLOCK] Wrong...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Urgh...[SOFTBLOCK] M...[SOFTBLOCK] my head...[SOFTBLOCK] Where...[SOFTBLOCK] Where am I?![SOFTBLOCK] 55?![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked about frantically as she called for her friend, but her voice caught in her throat as her eyes began to focus.[SOFTBLOCK] Her thoughts collided in her mind, fighting against the visions that surrounded her, refusing to believe her eyes were seeing correctly.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The sights before her eyes were unlike anything she had see on Regulus or Earth.[SOFTBLOCK] Writhing tendrils grew from walls of flesh as orb-like pustules shivered and pulsed with a familiar hue.[SOFTBLOCK] Wherever she was, she was no longer in the LRT facility, and she suspected she was no longer even in the same solar system.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Before her, shrouded in darkness, was an enormous figure.[SOFTBLOCK] It stepped forth with an inaudible chuckle that seemed to resonate within Christine's mind.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK][SOFTBLOCK]Vivify.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine opened her mouth to speak, to issue what challenge she could muster against Vivify, but found her captor vanished from sight.[SOFTBLOCK] She closed her mouth, confused, only for the abomination to appear directly in front of her as if she had blinked.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "With a drawn smile, the abomination returned its gaze to Christine.[SOFTBLOCK] It was grinning at her. Christine heard someone whisper, someone from far away in a familiar voice, but one she could not place.[SOFTBLOCK] There were no gods here.[SOFTBLOCK] If she had refused to learn before, she would now.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine's eyes widened and her mouth fell agape as one of the tendrils attached to Vivify reached out and dangled slowly in front of her face.[SOFTBLOCK] Her body convulsed again as her head twisted away from the tendril.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A forgotten instinct pushed a silent prayer to her mind that those [SOFTBLOCK][SOFTBLOCK]things[SOFTBLOCK][SOFTBLOCK] would not touch her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Vivify frowned, and the distant whisper returned to Christine's mind.[SOFTBLOCK] She was fighting.[SOFTBLOCK] She should not be fighting.[SOFTBLOCK] She could not refuse the lesson that was to come.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "With a lunge, the tentacle wrapped itself around her waist.[SOFTBLOCK] The grip on her arms and legs slackened as the tendrils from the wall dutifully released their grip, and Christine gasped as she was jerked forward, nearer to the abomination that now tormented her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A second tentacle reached out to her, snaking around her waist once before sliding up across her stomach and under her shirt.[SOFTBLOCK] It wrapped itself around a single breast and squeezed at it gently as the tip continued upward before dipping behind her neck, only to reappear on her other shoulder where it brushed lightly against her cheek.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] V-[SOFTBLOCK]vivify, please, don't![SOFTBLOCK] This -[SOFTBLOCK] what you're doing is wrong, you must be able to see that! [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The smile that had been drawn across Vivify's face fell briefly before twisting back into itself at odd angles.[SOFTBLOCK] The whisper returned, but slurred and with a hint of disinterest towards Christine.[SOFTBLOCK] She was exaggerating.[SOFTBLOCK] Over-reacting.[SOFTBLOCK] She should not do so towards her captor.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "With a jerk that threw her head back, Christine's body was pulled next to Vivify.[SOFTBLOCK] She opened her mouth to scream, but was silenced as the tentacle pushed her head forward, forcing her lips against her captor's all-too-eager lips.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A cloud seemed to pass over her mind even as it clouded her eyes, and her body fell limp.[SOFTBLOCK] Only the tentacles that held her waist and head kept her from crumpling to the floor.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The kiss drew her from her thoughts and held her captive.[SOFTBLOCK] Her mind numbed and her thoughts grew dull, leaving her consciousness soft and malleable.[SOFTBLOCK] She was nothing more than a bit of potter's clay that Vivify's skilled fingers twisted into a crude mockery of all she had once been.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Slowly Vivify began to pull away, her broken mouth twisting into a smirk as one of the tentacles wiped a thin strand of saliva from her lips.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (What's...[SOFTBLOCK] Going on... [SOFTBLOCK]I can't...[SOFTBLOCK] Move...[SOFTBLOCK] Can't...[SOFTBLOCK] think...[SOFTBLOCK] Master...) [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine's eyes drifted upward to the face of her captor, where her empty gaze was met by the unfathomable depths within Vivify's impossible eyes.[SOFTBLOCK] A third tentacle began to wrap itself about her body as the second continued to massage her breast and caress her face.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her strength failed her, and within the back of her thoughts, where the last hint of herself lingered, she knew her body would not have complied even if she were not hurt and tired.[SOFTBLOCK] Instead, her body pressed itself invitingly against the abomination's probing grasp.  [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She moaned softly as the slimy tentacles rolled along her body and across her sides in an unending caress.[SOFTBLOCK] Wherever they touched they left a thin film that grew hot on her skin and seemed to increase her body's sensitivity.[SOFTBLOCK] Each inch that the tentacles crept along seemed to go painfully slow.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Deep within her mind the last lingering point of sanity screamed at her, telling her to resist, to struggle, to fight.[SOFTBLOCK] The thought seared her mind as it clashed against the seductive calls that had infected her, and she cried out in mental anguish.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The groping undulations of the tentacles ceased as her silent cry became vocal.[SOFTBLOCK] They pulled her away from Vivify.[SOFTBLOCK] Christine stared up into the depths of her face, and Vivify smiled her broken smile.[SOFTBLOCK] She held up a single finger and placed it against her captive's lips.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A quiet thought pierced Christine's mind, seeking out this lingering ember of herself.[SOFTBLOCK] It penetrated deep within her thoughts to the very core of her psyche to where her vestigial consciousness yet lingered.[SOFTBLOCK] It chided her.[SOFTBLOCK] Scolded her.[SOFTBLOCK] Fighting would displease her master.[SOFTBLOCK] Her master required obedience.[SOFTBLOCK] She would be obedient.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Again the ember flared to life and screamed at her, struggling against the probing thought that was as alien as it was familiar.[SOFTBLOCK] It burned into her mind as it sought to rekindle what remained of her fighting spirit.[SOFTBLOCK] It burned until her very essence began to hurt as the two thoughts collided. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mind pleaded for mercy, begging to escape the burning thought even as it sought to rekindle itself.[SOFTBLOCK] An empty cry fell from her lips as her captor pulled her close again.[SOFTBLOCK] The finger was removed, and once more her lips were pressed against the lips of her captor.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The kiss was cool against the burning ember within her mind.[SOFTBLOCK] It soothed her, welcomed her, and as a laggard smile crept across Christine's face,[SOFTBLOCK] the flame that was the last anchor that held her mind was smothered, and she reached eagerly for her master's embracing lips.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her body convulsed and her eyes grew wide as another of the tendrils had begun to probe her, wrapping gently about the stiffening nipples her clothes had sought to hide away.[SOFTBLOCK] The slime-enhanced sensitivity sent a surge of pleasure throughout her body with each flick, each followed by a new convulsion.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] P-[SOFTBLOCK]please...[SOFTBLOCK] Don't stop![SOFTBLOCK] ... [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Vivify again chuckled within her mind...") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Before she could comprehend it, the tentacles and tendrils enveloped her body.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "They lashed out, grasping at her while rubbing every inch of her body.[SOFTBLOCK] They slid beneath her clothes, tearing the constricting shirt away.[SOFTBLOCK] They brushed over her breasts and flicked at her nipples, twisted around her legs and massaged her thighs as they slid playfully across the fabric that yet covered her dampening pussy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A pleading moan fell from her lips as the lustful passion began to well up within the deepest folds of her being.[SOFTBLOCK] Vivify looked on with a smile that was unseen by her captive's empty gaze, and was all too eager to comply.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her tentacles pulled her captive's legs apart, and in a single swift surge the soft material of her pants was torn from her legs.[SOFTBLOCK] A tentacle held her captive aloft by her raised arms as another probed the outer lips of her pleading pussy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Pulling the thin fabric of her underwear aside, a single tendril began pressing inside her.[SOFTBLOCK] It was thick and slick, and every inch within her wiggled, writhed, and throbbed with a greedy hunger.[SOFTBLOCK] A tingling sensation spread though the depths of her womanhood from the tentacle, and Christine's body tensed.[SOFTBLOCK] Her moans caught in her throat and her eyes shot wide.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine stared down with equal greed.[SOFTBLOCK] The tentacle buried itself deeper within her, pulling itself inch by writhing inch into the depths of her welcoming womb.[SOFTBLOCK] Christine trembled.[SOFTBLOCK] Vivify moaned.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "But even as Christine's head lolled back, the small ember took light in the back of her mind.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Perhaps it was renewed by the heat of the slime, or perhaps her captor's attention had been pulled away with her orgasm.[SOFTBLOCK] However it had occurred, the burning thought that would not let Christine be free returned, and she gasped as realization began to sweep over her.[SOFTBLOCK] Her breath hitched and she tried to squeeze her legs shut.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Vivify frowned and the orgasmic pleasure that had covered her face washed away to be replaced by a terrible frustration.[SOFTBLOCK] She licked her lips and growled quietly.[SOFTBLOCK] The whispering dug itself into Christine's mind.[SOFTBLOCK] Christine was strong.[SOFTBLOCK] A kiss was not enough.[SOFTBLOCK] It understood this.[SOFTBLOCK] The abomination was stronger.[SOFTBLOCK] Other solutions would be had. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The tentacle that had been within her began to writhe again, and with a single thrust, a cold fluid began to surge from it and into her body.[SOFTBLOCK] Christine cried out.[SOFTBLOCK] Her mind could scarcely comprehend the pleasure that was flooding her body as the squirming tentacle pressed itself deeper into her body and filled her with the cold fluid.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A shiver of ecstasy danced its way along Christine's spine, and Vivify smiled once more.[SOFTBLOCK] She reached out to her captive and cupped her cheek in one hand.[SOFTBLOCK] Immediately the whispering voice in Christine's mind grew soft.[SOFTBLOCK] Again, she was ready to learn.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The tentacle began pumping harder, and Christine's mind clouded over as more of the fluid filled her.[SOFTBLOCK] She could do nothing more than moan and squirm in bliss until a fresh orgasm coursed through her limbs and lingered deep within her muscles and bones.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her breath caught in her chest as the orgasm faded, and she coughed once, then gagged as her tongue seemed to fill her mouth as fully as the tentacle did her vagina, and she wondered if the tentacle might make its way through her enough to greet her tongue.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Even as the warmth of a new orgasm began to build up within her at the thought of such penetration, the tentacle retracted, leaving Christine gaping as the liquid dripped from her now-leaking pussy.[SOFTBLOCK] She looked around, confused and disappointed as her body took sorrowful notice of how empty she felt without it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A wistful frown crossed her face as she looked at her master, but it was short-lived as her gaze fell upon why Vivify had pulled out.[SOFTBLOCK] From between her legs slid a long, hard, chitinous appendage.[SOFTBLOCK] It was shiny, slick, and tube-like, and immediately Christine knew what it would be used for.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She spread her legs as much as her body would allow, her eyes never leaving her master's newest offering.[SOFTBLOCK] Vivify's cold eyes narrowed in glee as she watched her captive's awe as the alien protrusion grew longer and closer, until a small trickle of the slick, cold fluid leaked from its tip. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Vivify would not leave Christine waiting.[SOFTBLOCK] She took hold of Christine roughly with her hands, grabbing at her hips and pulling her onto the alien appendage.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine felt like she was being impaled.[SOFTBLOCK] Her body tensed as every muscle seemed to clench, and her mouth flew wide as if to scream, but before even a single sound could form within her a tentacle buried itself within her throat to fill the void.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Tears stung her eyes and she squirmed in the vice-like grip of the tentacles.[SOFTBLOCK] She struggled against them, but with each push and pull, the tentacle dove deeper within her mouth and down her throat, all the while the alien[SOFTBLOCK] thing [SOFTBLOCK]thrust into her over and over and over again.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The force of the thrusting rocked her entire body, and a searing light invaded her vision with each thrust.[SOFTBLOCK] She could feel her body filling with the strange fluid the tentacle in her mouth released.[SOFTBLOCK] Her stomach began to distend and her body shivered with the whimpering moans the tentacle kept buried within her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Even as her mind began to give way to the pleasures the thrusting provided her, a new tentacle reached up and plunged itself within her anus.[SOFTBLOCK] Her body convulsed with the whispers of another orgasm as brief hints of panic began to invade her addled mind.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The panic began to grow as her vision took in a large, faintly-glowing sphere making its way through Vivify's appendage, and she began to struggle against the tentacles that held her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "With each push and pull against her bonds, Vivify seemed to thrust harder and faster until the sphere reached the lips of her pussy.[SOFTBLOCK] It pressed against them, rolling through them and pressing, briefly, against her clitoris as it passed through and into her womb.[SOFTBLOCK] As it did, the whispers of the orgasm broke forth into a towering crescendo, and her entire body answered its call.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The orgasm surged through her, racing from the tips of her toes to the top of her scalp, stretching into her fingers and emanating from every point of her body with an electric joy.[SOFTBLOCK] A strangled scream worked its way from behind the tentacle in her mouth, but as the orgasm passed, her mind went quiet.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her body felt like it was made of air and she was drifting, floating through an endless universe of exploding stars and all-consuming black holes.[SOFTBLOCK] She watched eons pass, suns burn out and die, civilizations rise, grow fat, then fall under their own weight.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her stomach was warm and full, and a light glowed within it.[SOFTBLOCK] The life and power within this light sent shockwaves of knowledge through her and throughout the universe.[SOFTBLOCK] She watched herself disintegrate, be re-born, then shatter into an uncountable number of atoms, only to put herself back together in a thousand shapes that were as possible as they were impossible.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She saw the illogical geometry of the sacred cities, and the guard dogs walk between the lines to protect their masters.[SOFTBLOCK] She saw them squirm, grow, reach out and multiply, and she reached out to be with them. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The light in her stomach expanded as it pulsated, growing bigger by the second.[SOFTBLOCK] By the century.[SOFTBLOCK] By the millennia.[SOFTBLOCK] Christine could no longer tell.[SOFTBLOCK] Time had no meaning.[SOFTBLOCK] There was no sequence to it.[SOFTBLOCK] What was.[SOFTBLOCK] What is.[SOFTBLOCK] What will be.[SOFTBLOCK] She saw the universe expand, contract, explode, implode, be born, and die before her very eyes, and she understood. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked up into Vivify's impossible eyes, and saw they made sense.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF3") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her master smiled gently at her, tilted her head to the side, and chuckled again within her mind.[SOFTBLOCK] Christine's mind was so racked by new experience that she had not noticed 55's recovery.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She was ripped from her master and sent tumbling over the guard rail as 55 tackled her and threw her over the edge.[SOFTBLOCK] 55 cast one hate-filled glance back at Vivify before following.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Why was she doing this?[SOFTBLOCK] Did she not understand?[SOFTBLOCK] Of course she did not.[SOFTBLOCK] 55 was a machine.[SOFTBLOCK] It would take special work to teach her.[SOFTBLOCK] Her master would - [SOFTBLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine's thoughts were cut short as 55 struck her head with her pulse diffractor's stock.[SOFTBLOCK] It disoriented her, but she was well beyond such primitive feelings as 'pain'.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55 was dragging her back to the transit tunnels.[SOFTBLOCK] She did not resist.[SOFTBLOCK] 55 was unruly.[SOFTBLOCK] She would need to be tired before she would be pliant.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Now 55 threw her down and busied herself with a nearby supply crate.[SOFTBLOCK] She tore through its contents before finding two bottles of cleaning fluid.[SOFTBLOCK] 55 poured them onto the concrete before Christine, and an opaque gas emerged from the mixture.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The vapour smelled materialistic, and Christine would have been amused had 55 then not stuck her power discharge cable onto Christine's head and unleashed the full voltage.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "All at once the blur of emptiness cleared from her mind.[SOFTBLOCK] Christine began to scream and convulse as pain, loss, and lucidity returned to her...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Change Christine's form.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])
fnCutsceneBlocker()

--Unlock Latex Drone form.
VM_SetVar("Root/Variables/Global/Christine/iHasEldritch", "N", 1.0)

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] 55...[SOFTBLOCK] What...[SOFTBLOCK] What did you do?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Confirm unit status.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Unit 771852...[SOFTBLOCK] Status is operational...[SOFTBLOCK] Cognitive routines at normal levels...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55 -[SOFTBLOCK] please tell me what you did...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I downloaded every file related to Project Vivify I could find.[SOFTBLOCK] The experiments proved useful.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Vivify is constantly in a state of REM sleep, and I determined that your brain patterns were in a similar phase.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Attempts to wake Vivify failed, but certain stimuli generated stronger responses than others.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The most likely candidate was exposure to high concentrations of chlorine gas followed by a high-voltage shock administered to the cranium.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The research was not finished before the Cryogenics incident took place.[SOFTBLOCK] It seems they were on the right track.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] So everything I saw, everything I was, was a dream?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Or it was an alternate state of mind.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] All the things I learned are with me still, but I don't know them anymore.[SOFTBLOCK] When I sleep...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You will not.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Returning to REM sleep will likely return you to Vivify's influence.[SOFTBLOCK] Remain awake at all times.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] And if I defragment?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There is no data available as to the effects of defragmentation on an exposed golem.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] I can't stay awake forever, 55...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No, but you can until we run our queries on the datacore.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Yes.[SOFTBLOCK] Yes, we can return.[SOFTBLOCK] I will do it for you.[SOFTBLOCK] Vivify...[SOFTBLOCK] I know she'll let us run our queries.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Any 'knowledge' gained in your altered state is highly suspect.[SOFTBLOCK] Ignore it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You just don't understand...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Nor have I any intention to understand.[SOFTBLOCK] What you know, or think you know, is responsible for the retirement of hundreds of golems.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Consider that carefully before putting any weight in it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Now, follow my instruction without question.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, okay, 55...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I wonder if someday I can bring 55 around...)") ]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_End.lua") ]])
	fnCutsceneBlocker()
    return
end

fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Defeat_BackToSave/Scene_PostTransition.lua") ]])

--Restore the party's HP.
fnCutsceneInstruction([[ AdvCombat_SetProperty("Restore Party") ]])
