--[Combat Victory]
--The party won!
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N", 1.0)

--Music change.
AudioManager_PlayMusic("Null")

--[Movement]
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Vivify runs off.
fnCutsceneMoveFace("EDG", 12.25, 18.50, 0, -1, 0.30)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sorry![SOFTBLOCK] Please don't be angry!") ]])
fnCutsceneBlocker()

--Vivify runs off.
fnCutsceneTeleport("EDG", -100.25, -100.50)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("55", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Just what do you mean that you're sorry?[SOFTBLOCK] That thing was trying to retire us![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] No, she isn't.[SOFTBLOCK] She's asleep, dreaming.[SOFTBLOCK] She doesn't know what she's doing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] She -[SOFTBLOCK] there's so many of her.[SOFTBLOCK] Some of them are friendly.[SOFTBLOCK] Some of them want to help us.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And how do you know that?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Because they told me![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The notes on Project Vivify suggest it has internal emotional contradictions, assuming it has emotions like a normal organic.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We cannot, and should not, listen to it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do not show mercy if we encounter that creature again.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] She said she'd leave us alone until we're done here.[SOFTBLOCK] So, you can download whatever you need from the core.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Of course I'm going to trust a gibbering creature that speaks in a manner I can't even hear.[SOFTBLOCK] Sure.[SOFTBLOCK] Fine.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I trust her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Play the common scene.
LM_ExecuteScript(gsRoot .. "Chapter5Scenes/LRT After Boss/Scene_Begin.lua")
