--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Big boss cutscene.
if(sObjectName == "MainScene") then
	
	--Variables.
	local iSawFirstEDGScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N")
	
	--First time this scene fires.
	if(iSawFirstEDGScene == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N", 1.0)
		
		--Spawn the EDG. She is referred to as "Vivify" later.
		TA_Create("EDG")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Vivify/", false)
            TA_SetProperty("Rendering Offsets", gcfTADefaultXOffset - 9.0, gcfTADefaultYOffset - 18.0)
		DL_PopActiveObject()
		
		--Move the party up.
		fnCutsceneMove("Christine", 12.25, 7.50)
		fnCutsceneMove("55", 13.25, 7.50)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Access permissions check...[SOFTBLOCK] interface check...[SOFTBLOCK] I can plug directly into the core from here...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Superb![SOFTBLOCK] Do you need my help?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Actually the PDU would be more useful.[SOFTBLOCK] It can interface directly with the system.[SOFTBLOCK] Plug it in over there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The security isn't very good.[SOFTBLOCK] I don't think they expected a hacking attempt from a terminal right in the core.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Begin animating the text.
        local iWaitTime = 25
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|TerminalOn") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer0", false) ]])
		fnCutsceneWait(45)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer0", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer1", false) ]])
        fnCutsceneWait(iWaitTime)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer1", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer2", false) ]])
        fnCutsceneWait(iWaitTime)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer2", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer3", false) ]])
        fnCutsceneWait(iWaitTime)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer3", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer4", false) ]])
        fnCutsceneWait(iWaitTime)
        fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Interesting...[SOFTBLOCK] PDU, download that section...[SOFTBLOCK] Yes, as I suspected...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (...?[SOFTBLOCK] I hear something...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Kill the music.
        fnCutsceneWait(45)
        fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
		
		--Christine moves south.
		fnCutsceneMove("Christine", 12.25, 8.50, 0.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, 1)
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneWait(65)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Uhh, 55?[SOFTBLOCK] We may have a problem.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What is it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I hear -[SOFTBLOCK] singing...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Face south. Sound effect.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|ThingSmash") ]])
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneWait(105)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Music", "Vivify") ]])
		fnCutsceneWait(95)
		fnCutsceneBlocker()
        
        --Screen animates.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|TerminalOff") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer4", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", true) ]])
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|TerminalOn") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer4", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer7", false) ]])
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		
		--Screen shows up.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|2855] Unit 2855![SOFTBLOCK] Get Christine out of there, right now![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|2855] I was such an idiot.[SOFTBLOCK] I've lost control of the creature and it's coming towards you![SOFTBLOCK] You must not let the creature come in contact with Christine![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Doll:[VOICE|2855] I repeat::[SOFTBLOCK] Do not let the creature come in contact with Christine![SOFTBLOCK] We can't predict what will happen if it does!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|TerminalOff") ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer7", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", true) ]])
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", true) ]])
		fnCutsceneWait(65)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She's singing...[SOFTBLOCK] But it's not like last time.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's strained.[SOFTBLOCK] She's angry.[SOFTBLOCK] We upset her.[SOFTBLOCK] We did something we should not have.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What?[SOFTBLOCK] What did we do?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We fooled her.[SOFTBLOCK] Fooled her and now she's coming.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Who the hell sent that message...[SOFTBLOCK] Christine, we need to leave![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That would make her mad.[SOFTBLOCK] We don't want to make her mad.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Damn it, not now![SOFTBLOCK] PDU, cancel all queries.[SOFTBLOCK] Download every file related to Project Vivify, right now![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's coming...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Teleport the EDG in.
		fnCutsceneTeleport("EDG", 12.75, 18.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 12.75, 11.50, 0.40)
		fnCutsceneBlocker()
		fnCutsceneWait(125)
		fnCutsceneBlocker()
		fnCutsceneMove("EDG", 12.75, 14.50, 0.50)
		fnCutsceneBlocker()
		fnCutsceneWait(105)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Still downloading...[SOFTBLOCK] Christine, try to distract it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Distract...[SOFTBLOCK] my master?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move Christine south.
		fnCutsceneMove("Christine", 12.75, 12.50, 0.20)
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I can hear her...[SOFTBLOCK] she whispers...[SOFTBLOCK] garbled...[SOFTBLOCK] but it all makes sense...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It was inside all along.[SOFTBLOCK] Oh.[SOFTBLOCK] Yes, it was between the between.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But I'm not empty yet.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Please, teach me.[SOFTBLOCK] Teach me how to collect the gaps.[SOFTBLOCK] I'll be good.[SOFTBLOCK] I won't run away.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Move 55 south.
		fnCutsceneMove("55", 12.75, 12.50, 2.50)
		fnCutsceneBlocker()
		fnCutsceneMoveFace("Christine", 12.75, 11.50, 0, 1, 1.50)
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] What are you doing?[SOFTBLOCK] Get your weapon out![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] We can't fight her, she's already empty...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Christine, snap out of it![SOFTBLOCK] Right now![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] You'll never make it back to Sophie if you listen to this thing![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] S-[SOFTBLOCK]Sophie, my precious Sophie...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] If -[SOFTBLOCK] Sophie is waiting for me to come back to her.[SOFTBLOCK] 55?[SOFTBLOCK] 55 are you there?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Wake up![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We -[SOFTBLOCK] we have to destroy this thing, 55![SOFTBLOCK] We have to chop it into pieces so it can't be put back together![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] Welcome back to the real world.[SOFTBLOCK] It seems your little vacation has pissed it off.[SOFTBLOCK] Weapon up, right now!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Battle!
		fnCutsceneInstruction([[ AC_SetProperty("Next Music Override", "MotherTheme", 0.0000) ]])
		fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Maps/RegulusLRT/RegulusLRTIG/Combat_VictoryA.lua") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Defeat Script", gsRoot .. "Maps/RegulusLRT/RegulusLRTIG/Combat_DefeatA.lua") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Unretreatable", true) ]])
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter5/Vivify.lua", 0) ]])
		fnCutsceneBlocker()

	end
end
