--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Execution]
--Exit.
if(sObjectName == "ToLRTGX") then
	AL_BeginTransitionTo("RegulusLRTGX", "FORCEPOS:22.5x13.0x0")
    
elseif(sObjectName == "ToLRTZB") then
    AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTZB", "FORCEPOS:4.0x5.0x0")

--[Objects]
elseif(sObjectName == "LockedDoor") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] The door isn't powered...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Retired") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (What's left of the unit is melted slop...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "EntryVector") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Look at these marks...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Likely entry vector is the ceiling vent above us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It ripped through the plasteel ceiling?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Or melted through it.") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end