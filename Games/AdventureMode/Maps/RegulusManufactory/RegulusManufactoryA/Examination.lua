--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Variables.
local iManuFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--[Exits]
--[Examinables]
if(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Habitation logs for the domicile block above.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A list of the lord golems working in this sector.[SOFTBLOCK] Apparently this sector handles a lot of technical support queries.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Intercom") then
    if(iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Intercom:[VOICE|Golem] We are aware of the transit outage.[SOFTBLOCK] A repair team is already working on it.[SOFTBLOCK] Thank you for your report, lord unit.") ]])
        fnCutsceneBlocker()
    
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Intercom:[VOICE|Golem] Yes, lord unit, may I aid you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Any word on the transit situation?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Intercom:[VOICE|Golem] Work crews estimate it will be fixed within a few days.[SOFTBLOCK] Sector 99 has been issued a set of rovers to perform overland transport.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] A few days?[SOFTBLOCK] Really?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Intercom:[VOICE|Golem] I believe the tram hit a crucial support column and collapsed the entire tunnel block.[SOFTBLOCK] It was very unlikely to happen, but accidents are accidents.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Considering Project Mirabelle, it wasn't an accident...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Intercom:[VOICE|Golem] Our original estimates were off because we were provided older schematics.[SOFTBLOCK] Sector 99 has provided us updated schematics and greatly improved efficiency.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I see.[SOFTBLOCK] Congratulate the repair crews on their efficiency.") ]])
        fnCutsceneBlocker()
    
    end
    
elseif(sObjectName == "CrateA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Mannequins to be used in a shopping district, or maybe a firing range.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "CrateB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Empty pulse munitions catridges, probably on their way to be recharged.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "CrateC") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Compressed propellant containers.[SOFTBLOCK] Could be used for vehicle power, or as an explosive.)") ]])
    fnCutsceneBlocker()
    
--[Other]
elseif(sObjectName == "DoorAN") then
    AL_SetProperty("Open Door",  "DoorAN")
    AL_SetProperty("Close Door", "DoorAS")
    AudioManager_PlaySound("World|AutoDoorOpen")
    
elseif(sObjectName == "DoorAS") then
    AL_SetProperty("Open Door",  "DoorAS")
    AL_SetProperty("Close Door", "DoorAN")
    AudioManager_PlaySound("World|AutoDoorOpen")
    
elseif(sObjectName == "DoorBN") then
    AL_SetProperty("Open Door",  "DoorBN")
    AL_SetProperty("Close Door", "DoorBS")
    AudioManager_PlaySound("World|AutoDoorOpen")
    
elseif(sObjectName == "DoorBS") then
    AL_SetProperty("Open Door",  "DoorBS")
    AL_SetProperty("Close Door", "DoorBN")
    AudioManager_PlaySound("World|AutoDoorOpen")
    
elseif(sObjectName == "DoorCN") then
    AL_SetProperty("Open Door",  "DoorCN")
    AL_SetProperty("Close Door", "DoorCS")
    AudioManager_PlaySound("World|AutoDoorOpen")
    
elseif(sObjectName == "DoorCS") then
    AL_SetProperty("Open Door",  "DoorCS")
    AL_SetProperty("Close Door", "DoorCN")
    AudioManager_PlaySound("World|AutoDoorOpen")

--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end