--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Talk to 55 about the job.
if(sObjectName == "TalkTo55") then

    --Repeat check.
    local iManuTalkedTo55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTalkedTo55", "N")
    if(iManuTalkedTo55 == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuTalkedTo55", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So once again we find ourselves crawling around the undercity like a pair of desperate voyeurs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're also out of earshot and my sonar isn't picking up any recording devices.[SOFTBLOCK] So, why did you get me this assignment?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Sector 99 has had an unusual increase in productivity.[SOFTBLOCK] That is why the efficiency expert was looking for someone with social capability.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This productivity increase could be extremely useful if we could determine its origin.[SOFTBLOCK] A new technique, a new device, a new organization.[SOFTBLOCK] We must secure that knowledge for ourselves.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But you don't know anything beyond that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, just leave the talking to me![SOFTBLOCK] Me and my social skills![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I intend to.[SOFTBLOCK] Proceed.") ]])
    fnCutsceneBlocker()

end
