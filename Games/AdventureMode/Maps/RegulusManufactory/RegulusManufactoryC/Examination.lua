--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToManufactoryD") then
    
    --Form checker. Christine has to be a golem until a certain part of the story, latex drone after that.
    local iManuTold55Plan         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTold55Plan", "N")
    local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
    local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Factory is depopulated. Do nothing special.
    if(iManufactoryDepopulated == 1.0) then
    
    --Must be a golem.
    elseif(iManuTold55Plan == 0.0) then
        
        if(sChristineForm ~= "Golem") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Better switch to my golem form.[SOFTBLOCK] Wouldn't want to blow my cover...)") ]])
            fnCutsceneBlocker()
            return
        end
        
    --Must be a latex drone:
    else
        
        --Does Christine have the form?
        local iHasLatexForm = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
        if(sChristineForm ~= "LatexDrone") then
            
            --Has the form:
            if(iHasLatexForm == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I should switch to my drone form before going in there.)") ]])
                fnCutsceneBlocker()
                return
            
            --Needs to get it.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I need to come back as a drone unit.[SOFTBLOCK] I should speak to Sophie in the repair bay, she can get me repurposed.)") ]])
                fnCutsceneBlocker()
                return
            end
        end
    end
    
    --Special scene:
    local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    if(iManuTold55Plan == 1.0 and sChristineForm == "LatexDrone" and iIs55Following and iManufactoryDepopulated == 0.0) then
        
        --Set flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N", 1.0)
        
        --Remove 55 from the party.
        gsFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "55")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        
        --Variables.
        local iLRTBossResult = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N")
        if(iLRTBossResult == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, I will remain here and support you remotely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, probably best if I'm not seen with you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have adjusted your authenticator chip's signal.[SOFTBLOCK] You are now Unit 772890.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will see if I can locate an alternate route into the storage chamber.[SOFTBLOCK] Nothing appeared on the schematics, but the undercity's plans are often incomplete.[SOFTBLOCK] I will search.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll let you know if I find out anything.[SOFTBLOCK] Stay safe.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        
        --Has encountered Vivify.
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iTerminalHasBackers", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, I will remain here and support you remotely.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, probably best if I'm not seen with you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have adjusted your authenticator chip's signal.[SOFTBLOCK] You are now Unit 772890.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have a request.[SOFTBLOCK] I would like you to crossload your defragmentation logs to this terminal so I may observe them.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My defragmentation logs?[SOFTBLOCK] Why?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your encounters with Project Vivify have impacted your mental state, however, it only manifests itself when you are defragmenting.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Similar reports appeared in the logs at the Cryogenics Facility.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yeah, things appearing during defragmentation that don't have a genesis anywhere else.[SOFTBLOCK] It's why they ordered radio silence.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It may be nothing, but I would like to watch your log videos.[SOFTBLOCK] I may be able to learn more that may help us later.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right, but I warn you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Maybe I'm unaffected, maybe I'm not.[SOFTBLOCK] I can't guarantee you won't be.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am prepared to take that risk.[SOFTBLOCK] We must learn more about Project Vivify.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If you feel like you're losing focus, stop.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] PDU, forward the data to that terminal.[SOFTBLOCK] Encryption code, [SOFTBLOCK]'My Best Friend'.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Touching.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll let you know if I find anything out up top.[SOFTBLOCK] Stay safe.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        end
        
    end
    
    
    --If we got this far, proceed.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusManufactoryD", "ENTPOS:ToManufactoryC:S") ]])
    
--[Examinables]
elseif(sObjectName == "CrateA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A few assorted metal ingots, probably in transport and forgotten ages ago.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "CrateB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Spare pipe parts.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TransponderA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A network transponder, but the chips are all stripped out.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TransponderB") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A thermometer apparatus designed to wirelessly monitor changes in heat.[SOFTBLOCK] It has no power supply.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyPop") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A Fizzy Pop! machine, it has no inventory inside.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyPopR") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Fizzy Pop! is the right drink for hanging out in a dank undercity.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Golems") then
    local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
    local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")
    if(iManufactoryDepopulated ~= 1.0 or iManuFinale ~= 0.0) then
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I'll leave them in system standby a little longer, just in case that synaptic part wasn't causing their behavior.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Cargo elevator usage statistics and monitoring.[SOFTBLOCK] Nothing interesting.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    local iTerminalHasBackers = VM_GetVar("Root/Variables/Chapter5/Scenes/iTerminalHasBackers", "N")
    if(iTerminalHasBackers == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A network terminal with a card game on its screen.[SOFTBLOCK] Someone was using it on their break.)") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I uploaded my dreams to this terminal for 55 to review.[SOFTBLOCK] Looks like she wrote some notes...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "BackersManufactory", "Leave") ]])
    end
    
--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end