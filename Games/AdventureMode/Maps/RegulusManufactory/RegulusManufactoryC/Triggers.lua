--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Talking to 55 to hatch a plan.
if(sObjectName == "TalkTo55") then

    --Repeat check.
    local iManuMetForegolemAsGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetForegolemAsGolem", "N")
    local iManuMetAdminAsGolem     = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetAdminAsGolem", "N")
    local iManuTold55Plan          = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTold55Plan", "N")
    if(iManuTold55Plan == 1.0) then return end
    if(iManuMetAdminAsGolem == 0.0 or iManuMetForegolemAsGolem == 0.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuTold55Plan", "N", 1.0)
    
    --Variables.
    local iHasLatexForm = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, 55.[SOFTBLOCK] Did anything seem strange to you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] The units were highly productive, model citizens.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Did they seem a little...[SOFTBLOCK] robotic, to you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] A common trait among robots.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Certainly, but that's not what I was referring to.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You have made a determination I have not.[SOFTBLOCK] I would like you to share it with me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Oh, I think she might be a bit jealous, but not know how to deal with the feelings it gives her.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Don't worry, 55.[SOFTBLOCK] I understand.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's not that we saw them doing anything objectionable, but the fact that we saw absolutely nothing objectionable at all.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The units who were on break were treating like being on break is a job.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There was a lord unit in the break room with the slave units.[SOFTBLOCK] That's unheard of.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And, perhaps most telling of all?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] All of the lord units were working![SOFTBLOCK] All of them, at once![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] That is their job.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] True, but all the little things taken together suggest something suspicious.[SOFTBLOCK] No one event is hard evidence, but the trend is clear.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] When lacking a critical piece of evidence, we must act based on trends.[SOFTBLOCK] You are correct.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] But we have not learned the production secret, and none of the units were willing to divulge that information.[SOFTBLOCK] I take it you have an idea?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Is it possible they are revolutionary mavericks?[SOFTBLOCK] They would necessarily be unwilling to divulge secrets, and may be keeping produced equipment somewhere.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's my guess, actually.[SOFTBLOCK] I was thinking if we could get into their storage room...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Without them seeing?[BLOCK][CLEAR]") ]])
    
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] They're going to see me, they just won't be doing anything about it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I need you to fudge some records for me.[SOFTBLOCK] I'll go in disguised as a drone unit who was recently assigned to the sector, and see if I can find out anything when they don't think they're being overheard.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll get an assignment, poke around, figure out what clever secrets they're hiding.[BLOCK][CLEAR]") ]])
    if(iHasLatexForm == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] By disguised, I presume you mean you will transform yourself?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I can get Sophie to swap my parts out and coat me in the latex.[SOFTBLOCK] We'll just leave the inhibitor chip off.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I see.[SOFTBLOCK] Good. I am pleased to see you using your transformative abilities strategically.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We can use that warp point over there to head back to Sector 96.[SOFTBLOCK] Sophie can probably get me done in a few minutes.[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] By disguised, I presume you mean you will transform yourself?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes, but I'll need you to fake my authenticator signal.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] A trivial task.[SOFTBLOCK] Good.[SOFTBLOCK] I am pleased to see you using your transformative abilities strategically.[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Our task is clear.[SOFTBLOCK] Let us proceed.") ]])
    fnCutsceneBlocker()

--Underground encounter!
elseif(sObjectName == "Encounter") then

    --Variables.
    local iManuDroneTaskC         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N")
    local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
    if(iManuDroneTaskC ~= 2.0 or iManufactoryDepopulated ~= 0.0) then return end
    
    --Remove enemies so they don't get in the way.
    if(EM_Exists("EnemyCA") == true) then
        EM_PushEntity("EnemyCA")
            RE_SetDestruct(true)
        DL_PopActiveObject()
    end
    
    --Spawn characters.
    fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
    TA_Create("GolemA")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()
    TA_Create("GolemB")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()
    TA_Create("GolemC")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()

    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 47.25, 20.50)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] UNABLE TO LOCATE UNIT.[SOFTBLOCK] WAIT 300 SECONDS, IF UNIT IS NOT FOUND, RETURN FOR REASSIGNMENT.[SOFTBLOCK] EXECUTE.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneTeleport("GolemA", 46.25, 7.50)
    fnCutsceneTeleport("GolemB", 47.25, 7.50)
    fnCutsceneTeleport("GolemC", 48.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneMove("GolemA", 46.25, 17.50)
    fnCutsceneMove("GolemB", 47.25, 16.50)
    fnCutsceneMove("GolemC", 48.25, 16.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] GREETINGS, SUPERIOR UNITS.[SOFTBLOCK] DID YOU REQUEST THIS DRONE'S PRESENCE?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Found her.[SOFTBLOCK] She is a problem.[SOFTBLOCK] Assimilate.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --55 to the rescue!
    fnCutsceneTeleport("55", 48.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneMove("GolemA", 46.25, 20.50)
    fnCutsceneFace("GolemA", 1, 0)
    fnCutsceneMove("55", 48.25, 15.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("GolemC", "Wounded")
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("55", 48.25, 7.50, 0, 1, 1.50)
    fnCutsceneMoveFace("GolemC", 48.25, 8.50, 0, 1, 1.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] INSTRUCTION NOT FOUND, 'ASSIMILATE'.[SOFTBLOCK] PLEASE PROVIDE FURTHER INSTRUCTION.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Stand still, don't move.[SOFTBLOCK] This will only take a moment.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Get them!
    fnCutsceneTeleport("55", 47.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneMove("55", 47.25, 15.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("GolemB", "Wounded")
    fnCutsceneBlocker()
    fnCutsceneMove("55", 46.25, 19.50, 1.50)
    fnCutsceneMoveFace("GolemB", 48.25, 16.50, 0, 1, 1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] COMMAND UNIT 1111.[SOFTBLOCK] PLEASE RESOLVE INSTRUCTIONAL IMPASSE.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Command unit -[SOFTBLOCK] threat detected.[SOFTBLOCK] Maximum threat.[SOFTBLOCK] Terminate.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unlikely.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("GolemA", "Wounded")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] 771852, what is wrong with you?[SOFTBLOCK] Were you just going to let them adjust your hardware?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] THIS UNIT WAS OUT OF COMPLIANCE.[SOFTBLOCK] A SUPERIOR UNIT WAS PERFORMING MAINTENANCE.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Christine, this is absolutely not the time for your ridiculous jokes![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] THIS UNIT WAS NOT PERFORMING A COMEDY ROUTINE.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I disabled your inhibitor chip when I modified your authenticator's signal![SOFTBLOCK] Stop acting stupid.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] YOU...[SOFTBLOCK] what?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My inhibitor chip was never active?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] It was not.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Uhhhhhh...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] Christine![SOFTBLOCK] You put yourself in danger![SOFTBLOCK] I do not like it when you are in danger![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm sorry![SOFTBLOCK] But...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] But nothing![SOFTBLOCK] That was grossly irresponsible![SOFTBLOCK] Haven't you been paying attention?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] The lord unit drank a blue Fizzy Pop![SOFTBLOCK] Clearly their processors are being manipulated![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] That's the conclusion you reached?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have my suspicions based on what I have observed.[SOFTBLOCK] The fact that a golem just tried to 'assimilate' you and declared me a maximum threat may also be taken as 'further evidence'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, that's the subtle approach gone.[SOFTBLOCK] Force time?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] While I think these units are likely mavericks, I do not believe we can recruit them.[SOFTBLOCK] Still, we should continue our investigation.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If necessary, we will force our way into the rear storage area.[SOFTBLOCK] I was unable to penetrate its security, which is a rare occurrence for one of my skills.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Something important is back there.[SOFTBLOCK] Christine, let's go.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Meet me at the transit station above.[SOFTBLOCK] I'm going to make sure these three are in system standby.[SOFTBLOCK] Wouldn't want them coming up behind us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Affirmative.[SOFTBLOCK] Move out.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Transition to next scene.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuShowElevatorScene", "N", 1.0)
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusManufactoryD", "FORCEPOS:18.0x4.0x0") ]])
    fnCutsceneBlocker()

--Finale.
elseif(sObjectName == "Bizarre") then
    
    --Fade out.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Position.
    fnCutsceneTeleport("Christine", 46.75, 9.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneTeleport("55", 47.75, 9.50)
    fnCutsceneFace("55", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(305)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Work credits!
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 200)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Oh, Katarina sent me some work credits.[SOFTBLOCK] Guess we had to make sure the order forms are filled out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem](Received 200 Work Credits!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, are you all right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am fine.[SOFTBLOCK] Do not worry about me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Yeah I don't believe her.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So, off to our next misadventure?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will monitor this sector's activities and coordinate with Katarina as needed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I doubt the Administration will not notice the explosion but we do not have much of a choice.[SOFTBLOCK] We have done what we can do here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] We pick up the pieces from their experiments and then hide the innocent victims from them...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But we'll get them, 55.[SOFTBLOCK] We won't allow this to happen again.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] Lead the way, 771852.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
end
