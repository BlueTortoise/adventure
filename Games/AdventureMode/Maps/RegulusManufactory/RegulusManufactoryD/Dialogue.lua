--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--Variables.
local sForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Katarina, efficiency expert.
    if(sActorName == "Katarina") then
        
        --Variables.
        local iManuSetAuthChip = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N")
        
        --Initial meeting.
        if(iManuSetAuthChip == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Karatina:[VOICE|Karatina] Let me know if you need anything.[SOFTBLOCK] I dislike idleness, but I have no choice at the moment.") ]])
        
        --Drone. Beep boop.
        elseif(iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Karatina:[VOICE|Karatina] I wouldn't guess you're here to report the track is cleared, or Christine found something?[SOFTBLOCK] No?[SOFTBLOCK] Oh well.[SOFTBLOCK] Back to work, drone.") ]])
        
        --Manufactory depopulated...
        elseif(iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            
            if(sForm == "Human") then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] A human?[SOFTBLOCK] Here?[SOFTBLOCK] Command unit 1111, what is going on?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A breeding program unit.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] The clumsy organic lost her day pass badge, so I must make sure she is not converted until she returns to the biolabs.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (...)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm -[SOFTBLOCK] so sorry Command Unit![SOFTBLOCK] I swear it won't happen again![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Please go easy on her, command unit.[SOFTBLOCK] You know their brains tend to forget things.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I understand.[SOFTBLOCK] It is irresponsible to hold them to the same standards.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm doing my best.[SOFTBLOCK] When I am converted, I will not fail you again![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] At least she has the right attitude.") ]])
                fnCutsceneBlocker()
                
            elseif(sForm == "Golem") then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Oh, Christine.[SOFTBLOCK] You were gone for a while.[SOFTBLOCK] Find anything?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Nothing at all.[SOFTBLOCK] Stay here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I wasn't intending to leave.[SOFTBLOCK] The trams will be out for some time and I don't leave a function assignment incomplete.[SOFTBLOCK] That's how I got my current job.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ...[SOFTBLOCK] Excuse me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I wasn't always a lord unit.[SOFTBLOCK] I was repurposed when the lord I was working for was demoted.[SOFTBLOCK] I think she's a drone someplace in Sector 189.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're a promoted slave unit?[SOFTBLOCK] I don't think I've ever heard of that.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Former drone unit, actually.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Oh my...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Even with my inhibitor on, I was more capable than she was.[SOFTBLOCK] Evidently the Administration could see that.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Did you enjoy being a drone?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I don't miss it.[SOFTBLOCK] I like my hair, and my ability to run advanced scripts.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] If you could keep your hair, and your intellect?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] What are you implying, Christine?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] N-[SOFTBLOCK]nothing![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I suppose it is an experience not many lords would understand, so I suppose I should share.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] The chassis is certainly smoother and more environmentally protective.[SOFTBLOCK] They're designed for dangerous conditions.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] But the sensation of having someone rub their hand up and down your back will make your leg motivators give out.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Mmmmmm...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] There's a certain quality to simply doing as you're told and not thinking.[SOFTBLOCK] It's quite relaxing to not have to worry about the future.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Just be quiet, obedient, and empty.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Yeah...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Well, I've taken enough of your time.[SOFTBLOCK] Let me know if you find anything during your investigation.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Sure...[SOFTBLOCK] of course...") ]])
                fnCutsceneBlocker()
                
            elseif(sForm == "LatexDrone") then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] You're back, drone.[SOFTBLOCK] Any news about the transit network?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] REPAIR CREWS ARE WORKING TO CLEAR THE RUBBLE.[SOFTBLOCK] NO FURTHER NEWS AT THIS TIME.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Can't be helped, then.[SOFTBLOCK] As you were, drone.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] RETURNING TO FUNCTION ASSIGNMENT.") ]])
                
            elseif(sForm == "Darkmatter") then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Oh my![SOFTBLOCK] A darkmatter![SOFTBLOCK] You frightened me![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She appears to be allowing me to follow her.[SOFTBLOCK] I am trying to gather what data I can.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Oh![SOFTBLOCK] She looks happy![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] She's a cute one.[SOFTBLOCK] If you don't find anything out, you can at least upload photos of her to the network.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Nothing helps me relax after a long day quite like looking at pictures of darkmatter girls.") ]])
                
            elseif(sForm == "Eldritch") then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] !!![SOFTBLOCK] What are you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Good question.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is a biological research specimen.[SOFTBLOCK] While docile, I must accompany her at all times.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Oh, I see.[SOFTBLOCK] What have they gotten up to at the university?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Despite being organic, she has an order of mangitude more strength, durability, and agility compared to a human.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is impervious to the effects of vacuum, cold, heat, and radiation.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Amazing![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thank you, lord unit.[SOFTBLOCK] I hope my genetics help the Cause of Science.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please watch the network as we intend to begin publishing our initial trial results soon.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I certainly will![SOFTBLOCK] Good luck, command unit.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *She believed us?*[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] *The researchers at the university are in no way constrained by any sort of ethical boundaries.*") ]])
                
            elseif(sForm == "Electrosprite") then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] !!![SOFTBLOCK] What are you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hi![SOFTBLOCK] I'm an electrical girl![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We have discovered this partirhuman species on Pandemonium.[SOFTBLOCK] They possess an innate sense of electrical wiring.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I believe they may be useful in optimizing power grids.[SOFTBLOCK] I have asked her here to help with the task you charged 771852 with.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Bringing in contractors from Pandemonium?[SOFTBLOCK] That...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Well, you're a command unit.[SOFTBLOCK] You have the authority to do such things.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And all they need to pay me with is a nice live wire to bite into![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] When Unit 1111 is done with you, I may wish to acquire your services.[SOFTBLOCK] Until then, good luck.") ]])
                
            elseif(sForm == "SteamDroid") then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Unit 1111, I know it is not my place to question a command unit.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] But why is there a steam droid here?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hey, I'll take any job that pays.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The few remaining steam droids have made a number of novel engineering improvements to continue to function.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It is not common knowledge, but the Administration retains contact with some steam droids.[SOFTBLOCK] We contract out their services on occasion.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] And you are not punished for breaking the rules?[SOFTBLOCK] She could steal something![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am keeping track of all her activities.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This doll girl just wants me to see if any of the fabricator benches were upgraded illegally.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Ah, you're working with 771852.[SOFTBLOCK] Now I understand.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] She apparently has friends in high places.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And low places, too.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Unfortunately, we officially have not met, miss steam droid.[SOFTBLOCK] I wouldn't want the security services to ask a lot of questions.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Me neither.[SOFTBLOCK] Better not mention me.[SOFTBLOCK] Unit 1111, shall we?") ]])
            end
        
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Just in time for me to not leave the sector, the work crews are getting the transit situation under control.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Funny that it was originally going to take a month, and now it will be done in a few hours.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Mirabelle?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Likely.[SOFTBLOCK] Apparently the work crews had been provided 'outdated' schematics.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It was protecting itself.[SOFTBLOCK] Sector 99 is on a hill and has no cover on the overland approach.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The storage facility is built into a ridgeline and would be difficult to bombard.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Being discovered would be the real problem, 55.[SOFTBLOCK] Disabling the transit line would keep lords from snooping around.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I wonder what would have happened to me if you hadn't come, Christine.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Speaking of, how is everyone taking it?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] They remember vaguely what they have to keep secret.[SOFTBLOCK] I spoke to the other lords, and they're with you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Maybe it's a residual part of the hive mind, but the lords don't consider themselves too high and mighty to work with slave units as equals anymore.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Oh, we should have let it assimilate the city, then![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Seriously, that's a positive development.[SOFTBLOCK] Guess there's a silver lining here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do what you can to keep up appearances.[SOFTBLOCK] Sector 99's productive capacity will prove extremely useful in the future.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] For now, we're still shipping off pulse weapons.[SOFTBLOCK] We have to fulfill our requisition claims.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Good luck, Christine.[SOFTBLOCK] For all our sakes.") ]])
        
        end
    
    --Delivery golem.
    elseif(sActorName == "GolemA") then
        
        --Variables.
        local iManuSetAuthChip = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N")
        
        --Initial meeting.
        if(iManuSetAuthChip == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ...") ]])
        
        --Drone. Beep boop.
        elseif(iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I had to deliver some chips but the track was out, so I did it on foot.[SOFTBLOCK] Now I need to wait for the tram.[SOFTBLOCK] Hey, just following procedure, right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Besides, Lord Golem Katarina is actually nice to talk to.[SOFTBLOCK] I don't mind.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ...[SOFTBLOCK] Wow I'm so desperate, I'm talking to a drone.[SOFTBLOCK] I must be losing it...") ]])
        
        --Manufactory depopulated...
        elseif(iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (She's playing a game on her PDU.[SOFTBLOCK] Looks like it has something to do with stacking slime girls.[SOFTBLOCK] Better not interrupt her.)") ]])
            fnCutsceneBlocker()
        
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Pack") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chip:[E|Pack] By the way, Christine.[SOFTBLOCK] My name's Chip.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Charmed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chip:[E|Pack] Katarina is pretty nice.[SOFTBLOCK] She got me a transfer order to work in Sector 99 out of her own work credits.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chip:[E|Pack] I think she just likes talking to me.[SOFTBLOCK] We're both possession fetishists.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Woah![SOFTBLOCK] A bit too much information, Chip![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chip:[E|Pack] Oh, as if you don't have any kinks.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Now that you mention it...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chip:[E|Pack] Hey, did you know Katarina used to be a drone unit?[SOFTBLOCK] Imagine getting that promotion.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Okay?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Chip sure does pick weird topics...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chip:[E|Pack] Oh yeah, I'll let you go, Christine.[SOFTBLOCK] Just give me your PDU address.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] To keep in touch?[SOFTBLOCK] Sure![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You sent me a photo of a darkmatter?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chip:[E|Pack] I got tons of them.[SOFTBLOCK] Good luck out there.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (What an interesting unit...)") ]])
        end
    
    end
end