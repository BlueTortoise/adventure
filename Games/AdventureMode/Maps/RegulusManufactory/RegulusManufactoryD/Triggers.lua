--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Meeting Katarina.
if(sObjectName == "TalkToKatarina") then

    --Repeat check.
    local iManuMetKatarina = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetKatarina", "N")
    if(iManuMetKatarina == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuMetKatarina", "N", 1.0)
    
    --Movement.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 18.25, 8.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("55", 19.25, 8.50)
    fnCutsceneFace("55", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Katarina turns around.
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneFace("55", -1, 0)
    fnCutsceneFace("Katarina", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Intriguing.[SOFTBLOCK] You came from the underground passages?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, we did.[SOFTBLOCK] The trams were out, we got out at Sector 48.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I heard, my PDU let me know there was a transit interruption.[SOFTBLOCK] You are Unit 771852?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Lord Golem of Maintenance and Repair, Sector 96, at your service.[SOFTBLOCK] How many I serve the Cause of Science?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Unit 600484 speaking.[SOFTBLOCK] Though please, call me Katarina.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Christine.[SOFTBLOCK] Pleased to meet you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] And the command unit who is shadowing you...?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Hello.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Oh, where [SOFTBLOCK]*are*[SOFTBLOCK] my manners?[SOFTBLOCK] This Unit...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 1...[SOFTBLOCK] 1...[SOFTBLOCK] 1...[SOFTBLOCK] uuhhhh...[SOFTBLOCK] 1![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Command Unit 1111?[SOFTBLOCK] Head of Intelligence and AI research?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, you see, I'm currently being considered for a pay raise in my department.[SOFTBLOCK] My command unit wanted me to be evaluated during my next special assignment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Unit 1111 is under orders just to observe, not interact with anyone.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Ah, yes.[SOFTBLOCK] Standard procedure.[SOFTBLOCK] Still, you should let the staff know that before your inspection, Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Oh, of course![SOFTBLOCK] I'll have my PDU let them know right away![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So, Katarina.[SOFTBLOCK] You're an efficiency expert.[SOFTBLOCK] Why have you called me here today for a special assignment?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] You have a reputation among the workers in Sector 96 for being kindhearted and open.[SOFTBLOCK] Good.[SOFTBLOCK] I've been trying for years to get my superiors to listen to me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] An arrogant or dismissive lord does not inspire the slave units to work themselves until their parts are falling off.[SOFTBLOCK] An open palm is better than a closed fist.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I couldn't agree more.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Also 55 probably fudged the paperwork to give me a better 'reputation', but I guess I am kind of popular...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Mostly due to Sophie's kind heart...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Well, Sector 99 is a manufacturing sector.[SOFTBLOCK] Until a few months ago, light industry, mostly.[SOFTBLOCK] Consumer goods, electronics, that sort of thing.[SOFTBLOCK] If you've placed your metallic feet on a carpet in Regulus City, there's a 70 percent chance it came from Sector 99.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Recently, they were switched to weapons and ammunition manufacturing for the security forces.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Any idea why?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Oh, it's rather common for sectors to switch assignments.[SOFTBLOCK] Central Administration likely weighed the productivity loss of retooling against many other factors.[SOFTBLOCK] It's nothing unusual.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] But, something odd happened last month.[SOFTBLOCK] A sudden spike of productivity, an increase of fifty percent.[SOFTBLOCK] Fifty![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Capital![SOFTBLOCK] May I borrow their secret?[SOFTBLOCK] My sector could do with a productivity increase![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] And this is why I've called you here.[SOFTBLOCK] I don't know the secret.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] May I interrupt?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Of course, Command Unit 1111.[SOFTBLOCK] Am I doing something wrong?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] A detailed list of errors will be in my full report later.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Well, she's a good actress, isn't she?)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] However, I would like to ask Unit 600484 how she does not know the 'Secret'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Productivity is easy to measure, but the exact source is difficult to determine.[SOFTBLOCK] It's my job to determine what makes units produce more, and make sure other sectors replicate that success.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] An increase of personnel, a new work technique, better scheduling, incentives programs.[SOFTBLOCK] Sometimes simply moving the production line around increases output.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I've never seen a sector increase productivity by fifty percent and not do [SOFTBLOCK]*anything*.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Nothing?[SOFTBLOCK] At all?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Nothing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You've checked the shipping records?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Yes, the shipped parts arrived as declared.[SOFTBLOCK] There were more of them, it was not a miscount.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Have you tried asking the units what they did?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Absolutely nothing, so they say.[SOFTBLOCK] That's where you come in.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Unit 771852, I need you to investigate this sector for me and determine what it is they did to produce such an impressive increase in production.[SOFTBLOCK] You are doing this so that other sectors might share the benefits.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Now I recognize that you are basically doing my job.[SOFTBLOCK] I apologize for that.[SOFTBLOCK] However, I believe the issue may be that it is me asking.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But why would a sector not tell you immediately?[SOFTBLOCK] How does the Cause benefit from withholding information like that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I am not sure.[SOFTBLOCK] The lord units here are as tight-lipped as the slave units.[SOFTBLOCK] I simply cannot proceed as I am.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I am hoping your charm will be enough to win them over.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'll do what I can.[SOFTBLOCK] Leave it to me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Very good.[SOFTBLOCK] Now, I would help, but it's likely best if I'm not seen with you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I was going to go back to Sector 22 and at least get some work done, but I suppose with the trams out, it'd be hours before I got back here if you found something.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I hate being idle...[SOFTBLOCK] Perhaps I can help with Sector 15's problems with my PDU...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That is fine.[SOFTBLOCK] You have done well, Unit 600484.[SOFTBLOCK] Unit 771852, proceed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes, Command Unit.[SOFTBLOCK] Moving out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] (She's better at acting like someone else than acting like herself, isn't she?)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    fnAutoFoldParty()
    fnCutsceneBlocker()

--After disabling the golems.
elseif(sObjectName == "ReadyToGo") then

    --Repeat check.
    local iManuShowElevatorScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuShowElevatorScene", "N")
    if(iManuShowElevatorScene ~= 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuShowElevatorScene", "N", 0.0)
    
    --Black out the scene.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn 55.
    fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
    AL_SetProperty("Open Door", "DoorE")
    
    --Position actors.
    fnCutsceneTeleport("55", 19.25, 7.50)
    fnCutsceneFace("55", -1, 1)
    fnCutsceneTeleport("Christine", -1.25, -1.50)
    fnCutsceneFace("Christine", 0, 1)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (19.25 * gciSizePerTile), (7.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneTeleport("Christine", 18.25, 4.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 19.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("55", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Any movement up here?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No activity.[SOFTBLOCK] Local network traffic is suspended.[SOFTBLOCK] I cannot hear the hum of the factory machinery.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't like where this is going at all.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The golems?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] System standby.[SOFTBLOCK] I checked the cranial chassis of the one you bumped on the head to disable.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Were repairs necessary?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She'll be fine, but I found something unusual under her killphrase synaptic spike.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It looked like a receiver of some sort, but it's not in my repair database and I don't have any idea what purpose it serves.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's an entangelement send and receive unit, pretty crudely made, but it wasn't hooked to anything.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you certain?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I found the same one in all three golems, and removed them, just to be sure.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I...[SOFTBLOCK] also found another one on the ground.[SOFTBLOCK] I think they were going to put it in me...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Proceed with extreme caution, 771852.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Katarina and that other golem don't seem to have noticed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is not a threat to them.[SOFTBLOCK] We are.[SOFTBLOCK] She will become a threat if she speaks to us and attempts to leave the sector.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do not say anything to alarm her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Sure, okay.[SOFTBLOCK] That will help.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Are you intending to stay in drone unit form for the duration of this mission?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I kinda like being a rubber robot![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But I suppose it doesn't matter if I'm seen at this point.[SOFTBLOCK] I can go back downstairs and change forms if needed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Let us proceed, then.[SOFTBLOCK] Lead the way.") ]])
    fnCutsceneBlocker()
    
    --Fold the party.
    fnAddPartyMember("55")
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
