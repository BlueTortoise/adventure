--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--Variables.
local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManuDroneTaskB         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Mandated for story progression.
    if(sActorName == "Admin") then
        
        --Variables.
        local iManuMetForegolemAsGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetForegolemAsGolem", "N")
        local iManuMetAdminAsGolem     = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetAdminAsGolem", "N")
            
        --First meeting case:
        if(iManuMetAdminAsGolem == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iManuMetAdminAsGolem", "N", 1.0)
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Yes, Unit 771852.[SOFTBLOCK] How may I help you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you the unit in charge of this sector?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Affirmative.[SOFTBLOCK] I have been tasked with administration here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, all right.[SOFTBLOCK] My sector has a command unit in charge.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Ours was reassigned.[SOFTBLOCK] I have been tasked with administration here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, all right.[SOFTBLOCK] Do you know where she was reassigned to?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] *PDU, run a query for me.*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] She was reassigned.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Okay, then.[SOFTBLOCK] How long have you been in charge here?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Since our command unit was reassigned.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Interesting.[SOFTBLOCK] Well, it was nice meeting you.[SOFTBLOCK] I was just stopping in to say hi.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Hello.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] Goodbye?[BLOCK][CLEAR]") ]])
            
            --"Ending" case.
            if(iManuMetForegolemAsGolem == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Goodbye.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (We're not getting anywhere at all with this approach, but I think I have an idea.[SOFTBLOCK] I should head back to the undercity and speak with 55.)") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            
            --Normal case.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Goodbye.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            end
        
        --Repeat.
        elseif(iManuReassigned == 0.0) then
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Unit 771852.[SOFTBLOCK] Have you any further questions for me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Erm, no?[BLOCK][CLEAR]") ]])
            
            --"Ending" case.
            if(iManuMetForegolemAsGolem == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Very good. Enjoy your stay.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (We're not getting anywhere at all with this approach, but I think I have an idea.[SOFTBLOCK] I should head back to the undercity and speak with 55.)") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            
            --Normal case.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Very good.[SOFTBLOCK] Enjoy your stay.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            end
        
        --Christine is a drone.
        elseif(iManuReassigned == 1.0 and iManuFinale == 0.0) then
            
            --Not on assignment.
            if(iManuDroneTaskB == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I do not have an assignment for you, Drone 772890.[SOFTBLOCK] Finish your current assignment.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] DRONE WILL FINISH CURRENT ASSIGNMENT.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
                
            --Needs to go get the drink.
            elseif(iManuDroneTaskB == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Drone 772890, acquire a Fizzy Pop! canister and deliver it to me.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] DRONE WILL ACQUIRE FIZZY POP! CANISTER.[SOFTBLOCK] MOVING OUT.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
                
            --Deliver the drink.
            elseif(iManuDroneTaskB == 2.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N", 3.0)
                VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N", 1.0)
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] LORD UNIT, THIS UNIT IS DELIVERING A CANISTER OF FIZZY POP! DRINK.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Thank you, drone.[SOFTBLOCK] I notice you are not yet hooked into the network.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] DRONE NETWORK CONNECTION GREEN.[SOFTBLOCK] THIS DRONE IS MAINTAINING A LOCAL NETWORK CONNECTION.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Incorrect.[SOFTBLOCK] Drone, report to the long-term storage area immediately.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] MOVING TO LONG-TERM STORAGE FACILITY.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
                
                --Objective handler.
                fnCutsceneInstruction([[ AL_SetProperty("Clear Objectives") ]])
                fnCutsceneInstruction([[ AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:") ]])
                fnCutsceneInstruction([[ AL_SetProperty("Register Objective", "1: MOVE TO LONG-TERM STORAGE FACILITY FOR NETWORK INTEGRATION.") ]])
                
            --Repeat.
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Drone, report to the long-term storage area for network upgrade.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] REPORTING TO LONG-TERM STORAGE FACILITY.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            end
        
        --Post-sequence.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Ah, hello Christine.[SOFTBLOCK] What brings you to this sector?[BLOCK][CLEAR]") ]])
            if(sChristineForm ~= "Golem" and sChristineForm ~= "LatexDrone") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You recognize me?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] The entire sector does.[SOFTBLOCK] I saw you as a drone unit, an electrical girl, a steam droid, a human, a plant, a slime, a raiju...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Wait, what?[SOFTBLOCK] Slime, plant?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I'm afraid I don't know whose memory files they were, the whole thing is still rather fuzzy.[SOFTBLOCK] But we all shared them.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Were they mine from the future?[SOFTBLOCK] Did...[SOFTBLOCK] did I get assimilated?)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I don't understand, but...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Maybe we shouldn't dwell on it, then.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, how are things in the sector, then?[BLOCK][CLEAR]") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How are things in the sector?[BLOCK][CLEAR]") ]])
            end
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] We're all having group sessions to talk about what we remember when we're not keeping our production numbers up.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I've gotten several mails asking me about the explosion.[SOFTBLOCK] I said we're investigating.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Got any ideas on how to cover it up?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Something tells me the Administration already knows, so it'll be a formality either way.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] We can play dumb, but Mirabelle didn't get here on its own.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do you have any memories on the initial setup time for Project Mirabelle?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] No, in fact, the past few months are a haze.[SOFTBLOCK] Someone might, and I hope we find out more at our group sessions.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] If we manage to dig up the servers, I'll let you know if we find something.[SOFTBLOCK] We have to keep our production quota up, but what labour we can spare goes to the unofficial dig teams.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Is there any opposition to the resistance movement in the sector?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] None.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I speak for everyone.[SOFTBLOCK] I know that, somehow.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sharing minds isn't all bad, right?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Sector 99 will protect its own.[SOFTBLOCK] We're with you, Christine, 2855.") ]])
        end
        
    elseif(sActorName == "LordA") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] I am currently maintaining personnel files.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordA", 0, 1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] I do not require your assistance, drone.[SOFTBLOCK] Perform your function assignment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] MOVING TO PERFORM FUNCTION ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordA", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] I've got the work ethic of a slave unit, the fashion sense of a lord, and the blissful happiness of a drone.[SOFTBLOCK] Maybe we should revive Project Mirabelle?") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordA", 0, 1)
        end
        
    elseif(sActorName == "LordB") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] I am currently updating function assignments.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordB", 0, -1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] I do not require your assistance, drone.[SOFTBLOCK] Perform your function assignment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] MOVING TO PERFORM FUNCTION ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordB", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] We have to keep productive, both to keep from generating suspicion, and because our slave unit sisters need us to.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] To think of all the times we failed them because of our own vanity...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] What about switching tasks with them?[SOFTBLOCK] Can't they do administrative work?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] Oh, of course.[SOFTBLOCK] We just need to get the passwords all straightened out, and make sure we're ready in case of a surprise inspection.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] We'll have to maintain the division of labour a little longer.[SOFTBLOCK] Believe me, I'd love to be on the factory floor with my friends...") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordB", 0, -1)
        end
        
        
    elseif(sActorName == "LordC") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLordB] I am ordering additional materials and maintaining invoices.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordC", 0, -1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] I do not require your assistance, drone.[SOFTBLOCK] Perform your function assignment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] MOVING TO PERFORM FUNCTION ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordC", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] We have unofficially set our break room to be used by the slave units.[SOFTBLOCK] If an inspection comes by, we just pretend we were getting them to clean it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] I feel just awful for all the times I yelled at them.[SOFTBLOCK] What was wrong with me?[SOFTBLOCK] They're thinking, feeling machines just like us...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Are you saying you didn't know?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] I did, but...[SOFTBLOCK] I just... [BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] I'm so sorry.[SOFTBLOCK] I'll just keep working and maybe they'll forgive me.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordC", 0, -1)
        end
        
    elseif(sActorName == "GolemA") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Special instructions are received through work terminals.[SOFTBLOCK] I am making sure the members of my shift receive their instructions.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Function assignment for Drone 772890 -[SOFTBLOCK] checks are green.[SOFTBLOCK] Follow instructions on your HUD, drone.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] AFFIRMATIVE, DRONE WILL COMPLETE ASSIGNMENTS ON HUD.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The past few months are kind of a blur, all my memory files are fragmented.[SOFTBLOCK] But I know you helped us, Christine.[SOFTBLOCK] Thank you.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        end
        
        
    elseif(sActorName == "GolemB") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The transit system is experiencing an interruption.[SOFTBLOCK] We must organize these crates for fast deployment when transit resumes.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemB", 1, 0)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Good units maintain maximum productivity.[SOFTBLOCK] Resume your assignments, drone.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] AFFIRMATIVE, DRONE WILL MAXIMIZE PRODUCTIVITY.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemB", 1, 0)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The more things change, the more they stay the same.[SOFTBLOCK] I'm still packing shipping crates, but at least we have a sense of community now.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Also, there's no stigma to dating between model variants anymore.[SOFTBLOCK] Ooooh, I hope she says yes...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Who?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] As if I'd tell you![SOFTBLOCK] She's all mine!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        end
        
    elseif(sActorName == "GolemC") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] These parts must be sent as soon as the transit system is cleared.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemC", -1, 0)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Explosions, hive minds.[SOFTBLOCK] My job is still to keep everything marked and sorted!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemC", -1, 0)
        
        end
        
    elseif(sActorName == "GolemD") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Visitors should not cross the yellow lines unless appropriately hooked to the local network.[SOFTBLOCK] Please have your badge visible at all times.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Do not cross the yellow lines until your hair has been removed, drone.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] AFFIRMATIVE, DRONE WILL NOT CROSS YELLOW LINES.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Hey, you were a drone for a while, right?[SOFTBLOCK] If you want to be a drone again, we'd love to have you![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE, DRONE THANKS YOU FOR THE OFFER.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] But it'll have to wait.[SOFTBLOCK] To freedom!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        end
        
    elseif(sActorName == "Inspector") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Carry out your function assignment, drone.") ]])
        fnCutsceneBlocker()
    end
end