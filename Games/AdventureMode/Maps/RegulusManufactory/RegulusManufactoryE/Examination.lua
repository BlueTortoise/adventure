--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Variables.
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManuDroneTaskA         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--[Exits]
--[Examinables]
if(sObjectName == "BrownCrates") then
    
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Computer parts organized for shipping.[SOFTBLOCK] Each crate is destined for a different sector.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManuDroneTaskA == 0.0) then
        AudioManager_PlaySound("World|TakeItem")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N", 1.0)
        AL_SetProperty("Flag Objective True", "1: LOCATE MMF-RC-8 CHIPSET IN BROWN BOXES OF LOADING BAY")
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (PART MMF-RC-8 ACQUIRED.[SOFTBLOCK] DELIVER TO STORE ROOM C, NORTH END OF SECTOR.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManuDroneTaskA == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (PART ALREADY ACQUIRED.[SOFTBLOCK] DELIVER TO STORE ROOM C, NORTH END OF SECTOR.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManuDroneTaskA > 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (FUNCTION ASSIGNMENT DOES NOT REQUIRE THESE PARTS.)") ]])
        fnCutsceneBlocker()
    end
        
elseif(sObjectName == "TerminalA") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A list of inbound trams, all marked as canceled.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE DETECTS NO ERRORS WITH SHIPPING INVOICES.[SOFTBLOCK] PROCEEDING TO NEXT TASK.)") ]])
        fnCutsceneBlocker()
    end
        
elseif(sObjectName == "TerminalB") then
    if(iManuReassigned == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A work terminal with assignments for all the units in this sector.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (WORK TERMINAL CONNECTION HIGHLIGHTED ON DRONE HUD.[SOFTBLOCK] CONNECTION STATUS::[SOFTBLOCK] GREEN.)") ]])
        fnCutsceneBlocker()
    elseif(iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A work terminal with assignments for all the units in this sector.[SOFTBLOCK] Seems all the assignments were dropped a few minutes ago...)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A work terminal with assignments for all the units in this sector.[SOFTBLOCK] It's all lies to fool the Administration, though, since the units are informally sharing tasks now.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalC") then
    
    if(iManuReassigned == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There are twelve apartments in this block, all occupied by lord units.[SOFTBLOCK] Unfortunately, this terminal doesn't record which ones have fine tastes in interior design.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE FUNCTION ASSIGNMENT DOES NOT REQUIRE ACCESS TO LORD UNIT DOMICILES.)") ]])
        fnCutsceneBlocker()
    elseif(iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There are twelve apartments in this block, all occupied by lord units.[SOFTBLOCK] The records indicate they're all empty right now.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There are twelve apartments in this block, all occupied by lord units.[SOFTBLOCK] I hope the units are reallocating quarters communally, but leaving a record on this terminal would be a bad idea.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalD") then
    
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The terminal controlling the RVD.[SOFTBLOCK] It hasn't been ordered to play anything for three weeks.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE IS NOT ON BREAK.[SOFTBLOCK] DRONE DOES NOT NEED TO INSPECT RVD FOR FUNCTION ASSIGNMENT.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The terminal controlling the RVD.[SOFTBLOCK] It was last ordered to play some animal documentaries about Pandemonium.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Laptop") then
    
    if(iManuReassigned == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The lord unit is working on a portable computer.[SOFTBLOCK] I'm not going to snoop on it while she's looking at it.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE FUNCTION ASSIGNMENT DOES NOT CONCERN LORD'S PORTABLE COMPUTER.[SOFTBLOCK] RESUME FUNCTION ASSIGNMENT.)") ]])
        fnCutsceneBlocker()
    elseif(iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (...[SOFTBLOCK] There's nothing on the computer.[SOFTBLOCK] The lord unit must have wiped it when she left it here...)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Savior of the sector or not, I probably shouldn't snoop on someone's computer while they're looking at it.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "DoorA") then
    
    if(iManuReassigned == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Private quarters for a command unit.[SOFTBLOCK] The door is locked and I'd get in trouble if I tried to hack it open.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE FUNCTION ASSIGNMENT DOES NOT REQUIRE ACCESS TO LORD'S PRIVATE QUARTERS.[SOFTBLOCK] ACCESS DENIED.)") ]])
        fnCutsceneBlocker()
    elseif(iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unless we can't find any more clues, I'm not going to break into a fellow lord's quarters.[SOFTBLOCK] That's just rude!)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I wonder if they're going to use the larger quarters for something now.[SOFTBLOCK] Maybe they'll throw parties in there!)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Elevators") then
    
    if(iManuReassigned == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Elevators to the lord domicile block.[SOFTBLOCK] While I'm here on an investigation, I'm not going to go snooping through the other lords' quarters.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE FUNCTION ASSIGNMENT IS NOT TO CLEAN LORD QUARTERS.[SOFTBLOCK] ACCESS DENIED.)") ]])
        fnCutsceneBlocker()
    elseif(iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I'm not going to start breaking into lord unit quarters unless we can't find any more clues.[SOFTBLOCK] I'm a maverick, not a burglar.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Well, I wouldn't.[SOFTBLOCK] 55 probably would...)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Elevators to the lord domicile block.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "RVD") then
    
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's nothing playing on the RVD.[SOFTBLOCK] The break room looks abandoned.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE IS NOT REQUIRED TO INTERACT WITH RVD FOR FUNCTION ASSIGNMENT.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There's nothing playing on the RVD.[SOFTBLOCK] It was last used to play a nature documentary about Pandemonium.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "WhiteCrates") then
    
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Finished pulse munitions destined for security forces across the city.[SOFTBLOCK] Each crate is marked for a different security station.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (ANALYSIS::[SOFTBLOCK] CHARGED PULSE MUNITIONS.[SOFTBLOCK] NOT REQUIRED FOR FUNCTION ASSIGNMENT, IGNORING.)") ]])
        fnCutsceneBlocker()
    end
    
--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end