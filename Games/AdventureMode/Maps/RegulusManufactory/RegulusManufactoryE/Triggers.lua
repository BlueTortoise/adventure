--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Volume changers.
if(sObjectName == "ManuLoud") then
    AL_SetProperty("Mandated Music Intensity", 40.0)
    
elseif(sObjectName == "ManuQuiet") then
    AL_SetProperty("Mandated Music Intensity", 20.0)
    
--Cutscene.
elseif(sObjectName == "FirstVisit") then
    
    --Repeat check.
    local iManuGotBadge = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuGotBadge", "N")
    if(iManuGotBadge == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuGotBadge", "N", 1.0)
    
    --Scene.
    fnCutsceneTeleport("Christine", 29.25, 16.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneTeleport("55", 30.25, 16.50)
    fnCutsceneFace("55", 0, -1)
    fnCutsceneBlocker()
    
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Hello, lord unit.[SOFTBLOCK] Command unit.[SOFTBLOCK] Is your visit official or unofficial?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unofficial.[SOFTBLOCK] Behest of a fellow lord unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Is the lord unit employed in this sector?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Very good, here are your visitor security badges.[SOFTBLOCK] Please keep them on at all times.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Better play my role...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Slave unit, why do we require security badges?[SOFTBLOCK] Are you implying your superior units are possible security threats?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Oh I hope I don't get her in trouble...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] No, lord unit.[SOFTBLOCK] These security badges are for your protection.[SOFTBLOCK] They will emit a radio warning if you are in an area you are not cleared for.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] This sector has many automated moving parts that do not check for clearance.[SOFTBLOCK] We would not want you to be damaged if you entered an active area unknowingly.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ah, I see.[SOFTBLOCK] Excellent work, unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Please do not enter the automated long-term storage area at the rear of the facility, or cross any of the marked yellow lines.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] If you have any comments, please speak to the supply administrator or the factory floor administrator.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Hmm, those two sector administrators would be a good place to start our investigation...)") ]])
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
elseif(sObjectName == "Reassignment") then

	--Do the thing here.
    local iManuSetAuthChip = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N")
    local iManuReassigned = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
    if(iManuReassigned == 1.0 or iManuSetAuthChip ~= 1.0) then return end
    
    --Spawn NPC.
    fnStandardNPCByPosition("Inspector")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N", 1.0)
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Drone unit identified without appropriate passcodes.[SOFTBLOCK] Drone, state purpose.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] UNIT 772890 REPORTING FOR FUNCTION ASSIGNMENT.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Inspector", 23.25, 14.50)
    fnCutsceneMove("Inspector", 23.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] ...[SOFTBLOCK] Permission check green.[SOFTBLOCK] Network handshake successful.[SOFTBLOCK] Drone, follow me for function assignment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] FOLLOWING SUPERIOR UNIT.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Seems they bought it.[SOFTBLOCK] Great spoofing work, 55!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Inspector", 23.25, 14.50)
    fnCutsceneMove("Christine", 23.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorW") ]])
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("Inspector", 23.25, 6.50)
    fnCutsceneMove("Inspector", 18.25, 6.50)
    fnCutsceneMove("Inspector", 18.25, 4.50)
    fnCutsceneMove("Christine", 23.25, 7.50)
    fnCutsceneMove("Christine", 23.25, 6.50)
    fnCutsceneMove("Christine", 19.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Permission setup completed.[SOFTBLOCK] Begin inspection.[SOFTBLOCK] Drone, stand at attention.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] AFFIRMATIVE.") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Inspector", 18.25, 6.50)
    fnCutsceneFace("Inspector", 1, 0)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Minor drone anomaly detected.[SOFTBLOCK] Drone, turn around.[SOFTBLOCK] Expose CPU casing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Uh oh![SOFTBLOCK] I think they're on to me, but I can't blow my cover!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ...[SOFTBLOCK] AFFIRMATIVE.[SOFTBLOCK] TURNING AROUND.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Fix completed.[SOFTBLOCK] Drone, report status of inhibitor chip.[SOFTBLOCK] Run basic diagnostics.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (W-[SOFTBLOCK]wait![SOFTBLOCK] She did so -[SOFTBLOCK] something?[SOFTBLOCK] I can't...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (WHAT DID SHE DO? SUPERIOR UNIT DID SOMETHING.[SOFTBLOCK] DRONE WAS SUPPOSED TO DO SOMETHING.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I -[SOFTBLOCK] I...[SOFTBLOCK] PREVIOUS MISSION PARAMETERS EXCEED ALLOWED COMPLEXITY.[SOFTBLOCK] DISREGARD.[SOFTBLOCK] RESPOND TO REQUEST.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] INHIBITOR CHIP REPORTS GREEN.[SOFTBLOCK] MENTAL ACUITY WITHIN NORMAL PARAMETERS.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] IDENTITY CHECK.[SOFTBLOCK] UNIT 772890.[SOFTBLOCK] ASSIGNMENT, SECTOR 99, LABOUR AND SERVICE.[SOFTBLOCK] PLEASE INPUT FUNCTION ASSIGNMENT.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Response within expected parameters.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] An order was misfiled.[SOFTBLOCK] Move material from the brown crates at the loading dock to the storage rooms north of the factory floor.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Familiarize yourself with the layout of the area.[SOFTBLOCK] You will not be assigned to the factory floor until your hair has been removed as per standard.[SOFTBLOCK] It may get caught in the machinery, do not pass the yellow lines unless ordered.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] FLASH CHIPS IN STANDBY.[SOFTBLOCK] UNIT WILL NOT CROSS YELLOW LINES UNLESS ORDERED.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Execute function assignment, drone.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] UNIT 772890 CARRYING OUT FUNCTION ASSIGNMENT.") ]])
    fnCutsceneBlocker()
    
    --Objectives.
    fnCutsceneInstruction([[ AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Register Objective", "1: LOCATE MMF-RC-8 CHIPSET IN BROWN BOXES OF LOADING BAY") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Register Objective", "2: REPLACE MMF-RC-8 CHIPSET IN STORAGE ROOM ON NORTH END OF FACILITY.") ]])

--Phone call.
elseif(sObjectName == "PDUCall") then

    --Repeat check.
    local iManuDroneTaskC = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N")
    if(iManuDroneTaskC ~= 1.0) then return end

    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N", 2.0)

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "PDU") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "PDU", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[E|Alarmed] Christine, you are receiving a priority call.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|PDU] UNIDENTIFIED ALIAS, 'Christine'.[SOFTBLOCK] PRIORITY CHECK -[SOFTBLOCK] HIGH.[SOFTBLOCK] CONNECT CALL.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|PDU] UNIT 772890 REPORTING.[SOFTBLOCK] PLEASE IDENTIFY CALLER.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[E|Quiet] [VOICE|2855]Christine, the PDU is hooked into your audio receivers.[SOFTBLOCK] You don't need to keep the act up, nobody else can hear us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|PDU] UNIDENTIFIED ALIAS, 'Christine'.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[E|Quiet] [VOICE|2855]...[SOFTBLOCK] I was watching you on the cameras.[SOFTBLOCK] Did you just give a blue Fizzy Pop! to that lord unit?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|PDU] AFFIRMATIVE.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[E|Quiet] [VOICE|2855]And she drank it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|PDU] AFFIRMATIVE.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[E|Quiet] [VOICE|2855]That doesn't seem unusual to you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|PDU] REPEAT QUERY.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[E|Quiet] [VOICE|2855]...[SOFTBLOCK] Drone 772890, report to the underground of Sector 99.[SOFTBLOCK] Command Authorization QUIM77T.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|PDU] ASSIGNMENT ACCEPTED.[SOFTBLOCK] MOVING TO UNDERGROUND.") ]])
    fnCutsceneBlocker()

    --Objective handler.
    fnCutsceneInstruction([[ AL_SetProperty("Clear Objectives") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Register Objective", "1: REPORT TO SECTOR 99 UNDERGROUND - OVERRIDE QUIM77T.") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Register Objective", "2: (DELAYED) MOVE TO LONG-TERM STORAGE FACILITY FOR NETWORK INTEGRATION.") ]])

--Finale.
elseif(sObjectName == "Finale") then

    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N", 1.0)
    
    --Fade to black.
	AL_SetProperty("Music", "Null")
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Flag.
    local iManuFinale = VM_SetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N", 1.0)
    
    --Spawn entities.
    fnStandardNPCByPosition("Katarina")
    fnStandardNPCByPosition("DeliveryGolem")
    
    --Crowd.
    local iCounter = 0
    local iaPositions = {}
    local fnAddPosition = function(iX, iY)
        iaPositions[iCounter] = {iX, iY}
        iCounter = iCounter + 1
    end
    fnAddPosition( 9,  4)
    fnAddPosition( 9,  5)
    fnAddPosition( 9,  6)
    fnAddPosition( 9,  7)
    fnAddPosition( 9,  8)
    fnAddPosition( 9,  9)
    fnAddPosition( 9, 10)
    fnAddPosition( 9, 11)
    fnAddPosition( 9, 12)
    fnAddPosition(10,  4)
    fnAddPosition(10,  5)
    fnAddPosition(10,  6)
    fnAddPosition(10,  7)
    fnAddPosition(10,  8)
    fnAddPosition(10,  9)
    fnAddPosition(10, 10)
    fnAddPosition(10, 11)
    fnAddPosition(10, 12)
    fnAddPosition(11,  4)
    fnAddPosition(11,  5)
    fnAddPosition(11,  6)
    fnAddPosition(11,  7)
    fnAddPosition(12,  4)
    fnAddPosition(12,  5)
    fnAddPosition(12,  6)
    fnAddPosition(12,  7)
    for i = 0, iCounter-1, 1 do
        TA_Create("Golem"..i)
            TA_SetProperty("Position", iaPositions[i][1], iaPositions[i][2])
            TA_SetProperty("Facing", gci_Face_West)
            TA_SetProperty("Clipping Flag", false)
            local iRoll = LM_GetRandomNumber(1, 100)
            if(iRoll < 40) then
                fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
                TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlave|Wounded")
            elseif(iRoll < 70) then
                fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
                TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/LatexDrone|Wounded")
            else
                fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", false)
                TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemLord|Wounded")
            end
        DL_PopActiveObject()
    end
    
    --Position everyone.
    fnCutsceneTeleport("Christine", 4.25, 6.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneTeleport("55", 4.25, 7.50)
    fnCutsceneFace("55", 1, 0)
    fnCutsceneTeleport("Katarina", 4.25, 8.50)
    fnCutsceneFace("Katarina", 1, 0)
    fnCutsceneTeleport("DeliveryGolem", 3.25, 8.50)
    fnCutsceneFace("DeliveryGolem", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(365)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(365)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Katarina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Pack") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Christine, why do I have the niggling feeling I'm about to die?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, I'll try to be brief.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm a transforming maverick with a magical runestone, this is Unit 2855.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hello.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're pretty sure these golems are linked in some sort of hive mind called Project Mirabelle.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Transforming bit aside, I'm with you so far.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And they're not going to let us go because we're a threat to their collective.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] All of that sounds reasonable.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] So are they going to add us to the hive mind too, or just retire us?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] They can't add 55 because she's a Command Unit.[SOFTBLOCK] Might be because she's incompatible.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Command Units don't have killphrases so they don't have synaptic override spikes.[SOFTBLOCK] I think that's how they control the individual members.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] That is correct, I do not have a killphrase.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("55", 5.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Project Mirabelle?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mirabelle:[E|Neutral] I will speak for the collective.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You cannot add me to your collective, because I am incompatible.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mirabelle:[E|Neutral] We will develop the technology.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And if you cannot?[SOFTBLOCK] There is no guarantee that it is even possible.[SOFTBLOCK] Command Unit architecture is fundamentally different.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What if I can never be integrated into your society?[SOFTBLOCK] Will you retire me?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mirabelle:[E|Neutral] No.[SOFTBLOCK] You will be kept safe.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] I will be a burden.[SOFTBLOCK] I will consume resources but not contribute.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not want that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mirabelle:[E|Neutral] A place for you will be found in our collective.[SOFTBLOCK] We will find one for you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A place for me to belong...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] 'The Quiet Aristocrat Reads Shivering Ink'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mirabelle:[E|Neutral] Sh...[SOFTBLOCK] Shh...[SOFTBLOCK] Zzzz...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sound.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("55", -1, 0)
    
    --Everyone is knocked over.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("Golem5", "Wounded")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("Golem14", "Wounded")
    fnCutsceneSetFrame("Golem3", "Wounded")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("Golem20", "Wounded")
    fnCutsceneSetFrame("Golem9", "Wounded")
    fnCutsceneSetFrame("Golem3", "Wounded")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    for i = 0, iCounter, 1 do
        fnCutsceneSetFrame("Golem" .. i, "Wounded")
    end
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Cry") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Cry] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I apologize for the deception, Christine.[SOFTBLOCK] While we were running, I was scanning everything I could access for information on Project Mirabelle.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I managed to get a network signal for a few moments and found a mail waiting for me.[SOFTBLOCK] It contained Mirabelle's killphrase.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Who sent the mail?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No sending address.[SOFTBLOCK] Someone knew I was looking, I did not have time to hide it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] While that's really bad, at least we have some sort of benefactor.[SOFTBLOCK] Perhaps a researcher who worked on the project?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] If Mirabelle had a killphrase, whoever made it knew it might go rogue...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will see what I can find later.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So you talking to them like that was you waiting until all the golems were here with us and not in the server room.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Because they're networked together, the server heard the killphrase and blew up, but none of the golems were harmed![SOFTBLOCK] Clever work, 55![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] (A place to belong...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Thank you, Christine. I was unsure if the killphrase would cause a shutdown or an overheat.[SOFTBLOCK] It seems we found out which.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Katarina", 5.25, 8.50)
    fnCutsceneFace("Katarina", 0, -1)
    fnCutsceneMove("DeliveryGolem", 4.25, 8.50)
    fnCutsceneFace("DeliveryGolem", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneFace("55", 0, 1)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Katarina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Pack") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I'm just going to go ahead and assume I'm now privy to a large amount of things I don't have clearance for.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Yeah.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yep.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Affirmative.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Well, I don't particularly want to be reprogrammed, so.[SOFTBLOCK] Guess I won't be mentioning this on my report.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, Unit 771852 arrived and determined that a compound leaked into the oilmaker and boosted unit productivity.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Unfortunately, it also causes degredation of CPU synaptic integrity, leading to memory ambiguation.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Meaning the units become forgetful?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] I suspect Project Mirabelle stored unit memories on its servers.[SOFTBLOCK] They will not remember anything when they wake up.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] And if they do?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] They probably don't want to be reprogrammed, either.[SOFTBLOCK] Help them keep it quiet.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Guess we're all mavericks, now.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Unit 2855, you were the Head of Security.[SOFTBLOCK] Can't you do something?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am a maverick and have no influence.[SOFTBLOCK] Your best hope is to cooperate with us, as the Administration is likely to cover this up.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're fighting the Administration.[SOFTBLOCK] They use us as unwilling test subjects for their sick experiments.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Mirabelle could be used to rehabilitate the hurt, help machines find community.[SOFTBLOCK] But instead, it was used to make robots work harder.[SOFTBLOCK] How banal.[SOFTBLOCK] Such a waste.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Are we even sure this was an experiment?[SOFTBLOCK] What if the project went rogue?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unfortunately, the answers are on the servers that we just exploded.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Then...[SOFTBLOCK] We'll have to stay here, Katarina.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] That we will.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] So, Christine.[SOFTBLOCK] We don't have a choice, but, count us in.[SOFTBLOCK] We'll work with you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] We'll keep an eye on this sector.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] I'll get us transferred here officially.[SOFTBLOCK] I'm sure the lords will agree when they wake up.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] There's just so much wrong in Regulus City.[SOFTBLOCK] It looks like a paradise from outside...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We'll need to return to our other missions, Christine.[SOFTBLOCK] Katarina, I will contact you later with instructions on how to reach me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] All right.[SOFTBLOCK] Well, first thing to do is get this factory back up and running.[SOFTBLOCK] A drop in productivity will be even more suspicious than an increase!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Transit to last scene.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusManufactoryC", "FORCEPOS:35.0x2.0x0") ]])
    fnCutsceneBlocker()
    
end
