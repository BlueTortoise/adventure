--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusManufactoryF"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
    local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")
    if(iManufactoryDepopulated == 0.0 or iManuFinale == 1.0) then
        AL_SetProperty("Music", "LAYER|Manufactory")
        AL_SetProperty("Mandated Music Intensity", 60.0)
    else
        AL_SetProperty("Music", "RegulusContainment")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusManufactoryF")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
    local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")
    if(iManufactoryDepopulated == 0.0 or iManuFinale == 1.0) then
        fnStandardNPCByPosition("Foregolem")
        fnStandardNPCByPosition("LordA")
        fnSpawnNPCPattern("Golem", "A", "G")
        fnSpawnNPCPattern("Drone", "A", "D")
        fnSpawnNPCPattern("WorkDroneU", "A", "D")
        fnSpawnNPCPattern("WorkGolemU", "A", "E")
        fnSpawnNPCPattern("WorkDroneL", "A", "D")
        fnSpawnNPCPattern("WorkGolemL", "A", "E")
        fnSpawnNPCPattern("WorkGolemM", "D", "E")
    
        --Create a parallel script to handle the workers moving around.
        Cutscene_HandleParallel("Create", "FactoryScript", fnResolvePath() .. "FactoryScript.lua")
        
        --Finale: Move some units around.
        if(iManuFinale == 1.0) then
            fnCutsceneTeleport("Foregolem", 28.25, 4.50)
            fnCutsceneFace("Foregolem", 0, -1)
            fnCutsceneTeleport("DroneD", 16.25, 15.50)
            fnCutsceneFace("DroneD", 0, 1)
            fnCutsceneTeleport("GolemE", 10.25, 8.50)
            fnCutsceneFace("GolemE", 0, 1)
            fnCutsceneTeleport("DroneC", 5.25, 26.50)
            fnCutsceneFace("DroneC", 0, 1)
        end
    else
        AL_SetProperty("Set Animation Disabled", "FactoryAnim0", true)
        AL_SetProperty("Set Animation Disabled", "FactoryAnim1", true)
        AL_SetProperty("Set Animation Disabled", "FactoryAnim2", true)
        AL_SetProperty("Set Animation Disabled", "FactoryAnim3", true)
        AL_SetProperty("Set Animation Disabled", "FactoryAnim4", true)
    end
    
    --[Objective Handler]
    --Variables.
    local iManuReassigned = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
    local iManuDroneTaskA = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N")
    local iManuDroneTaskB = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N")
    local iManuDroneTaskC = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N")
    if(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        
        --First task:
        if(iManuDroneTaskA < 2.0) then
            AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:")
            AL_SetProperty("Register Objective", "1: LOCATE MMF-RC-8 CHIPSET IN BROWN BOXES OF LOADING BAY")
            AL_SetProperty("Register Objective", "2: REPLACE MMF-RC-8 CHIPSET IN STORAGE ROOM ON NORTH END OF FACILITY")
            if(iManuDroneTaskA == 1.0) then
                AL_SetProperty("Flag Objective True", "1: LOCATE MMF-RC-8 CHIPSET IN BROWN BOXES OF LOADING BAY")
            end
        
        --Reassignment:
        elseif(iManuDroneTaskA == 2.0 and iManuDroneTaskB == 0.0) then
            AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:")
            AL_SetProperty("Register Objective", "1: REPORT TO LORD UNIT IN FACTORY FLOOR OFFICE FOR FURTHER ASSIGNMENT")
        
        --Second task:
        elseif(iManuDroneTaskA == 2.0 and iManuDroneTaskB < 3.0) then
            AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:")
            AL_SetProperty("Register Objective", "1: ACQUIRE FIZZY POP! CANISTER")
            AL_SetProperty("Register Objective", "2: DELIVER FIZZY POP! CANISTER TO ADMINISTRATIVE LORD")
            if(iManuDroneTaskB == 2.0) then
                AL_SetProperty("Flag Objective True", "1: ACQUIRE FIZZY POP! CANISTER")
            end
        
        --Third task:
        elseif(iManuDroneTaskA == 2.0 and iManuDroneTaskB == 3.0 and iManuDroneTaskC == 1.0) then
            AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:")
            AL_SetProperty("Register Objective", "1: MOVE TO LONG-TERM STORAGE FACILITY FOR NETWORK INTEGRATION")
        
        --Override.
        elseif(iManuDroneTaskA == 2.0 and iManuDroneTaskB == 3.0 and iManuDroneTaskC == 2.0) then
            AL_SetProperty("Clear Objectives")
            AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:")
            AL_SetProperty("Register Objective", "1: REPORT TO SECTOR 99 UNDERGROUND - OVERRIDE QUIM77T")
            AL_SetProperty("Register Objective", "2: (DELAYED) MOVE TO LONG-TERM STORAGE FACILITY FOR NETWORK INTEGRATION")
        end
    end
end
