--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--Variables.
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Mandated for story progression.
    if(sActorName == "Foregolem") then
        
        --Variables.
        local iManuMetForegolemAsGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetForegolemAsGolem", "N")
        local iManuMetAdminAsGolem     = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetAdminAsGolem", "N")
            
        --First meeting case:
        if(iManuMetForegolemAsGolem == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iManuMetForegolemAsGolem", "N", 1.0)
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Unit 771852, Unit 1111.[SOFTBLOCK] How may I help you?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're the unit in charge of the factory floor?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Affirmative.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, I just came to commend you for your outstanding performance![SOFTBLOCK] Well done, the Cause needs more units like you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Thank you, however, I did not do anything outside normal parameters.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No need to be humble about it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Though I suppose you could say it's your talented units who did the impressive work, right?[SOFTBLOCK] How charitable of you![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] No.[SOFTBLOCK] They performed their function assignments within normal parameters.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, well.[SOFTBLOCK] No commendations to give out, then.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] No.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (She's like 55, but without the charm or charisma.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (No wait, that's mean, to both of them.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Actually, I'm a repair unit.[SOFTBLOCK] Maybe the new machinery you got has a repair manual that's not in my database?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] We do not have any machinery or manuals not in the standard database.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Well.[SOFTBLOCK] Suppose I ought to be off back to my sector.[SOFTBLOCK] It was very nice meeting you.[BLOCK][CLEAR]") ]])
            
            --"Ending" case.
            if(iManuMetAdminAsGolem == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Thank you.[SOFTBLOCK] Returning to work.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (We're not getting anywhere at all with this approach, but I think I have an idea.[SOFTBLOCK] I should head back to the undercity and speak with 55.)") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            
            --Normal case.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Thank you.[SOFTBLOCK] Returning to work.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            end
        
        --Repeat.
        elseif(iManuMetForegolemAsGolem == 1.0 and iManuReassigned == 0.0) then
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Unit 771852, Unit 1111.[SOFTBLOCK] Have you anything to discuss?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Erm, no?[BLOCK][CLEAR]") ]])
            
            --"Ending" case.
            if(iManuMetAdminAsGolem == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Thank you for your consideration.[SOFTBLOCK] I must return to my assignments.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (We're not getting anywhere at all with this approach, but I think I have an idea.[SOFTBLOCK] I should head back to the undercity and speak with 55.)") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            
            --Normal case.
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Thank you for your consideration.[SOFTBLOCK] I must return to my assignments.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            end
        
        --Christine is a latex drone.
        elseif(iManuReassigned == 1.0 and iManuFinale == 0.0) then
        
            --Variables.
            local iManuDroneTaskA = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N")
            local iManuDroneTaskB = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N")
        
            --First task to-be-completed.
            if(iManuDroneTaskA < 2.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Drone 772890, acquire the incorrectly assigned parts from the brown crates in the loading bay.[SOFTBLOCK] Then, deliver them to the store room and store them in the brown crates.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] CARRYING OUT ORDERS.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            
            --First task completed, receive second task.
            elseif(iManuDroneTaskA == 2.0 and iManuDroneTaskB == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N", 1.0)
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Drone 772890, acquire a canister of Fizzy Pop! and deliver it to the administrative lord unit in the office block.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] CARRYING OUT ORDERS.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
                
                AL_SetProperty("Clear Objectives")
                AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:")
                AL_SetProperty("Register Objective", "1: ACQUIRE FIZZY POP! CANISTER")
                AL_SetProperty("Register Objective", "2: DELIVER FIZZY POP! CANISTER TO ADMINISTRATIVE LORD")
            
            --First task completed, receive second task.
            elseif(iManuDroneTaskA == 2.0 and iManuDroneTaskB == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Drone 772890, your assignment is to acquire a canister of Fizzy Pop! and deliver it to the administrative lord unit in the office block.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] CARRYING OUT ORDERS.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Drone, carry out your current function assignment.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[E|Neutral] AFFIRMATIVE.[SOFTBLOCK] DRONE WILL COMPLETE CURRENT FUNCTION ASSIGNMENT.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
        
            end
        
        --Post-finale.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Oh, greetings, Christine, 2855.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you cleaning the walls?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Affirmative.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] First time for everything?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] My hands are just as capable as a drone's or a slave unit's.[SOFTBLOCK] We rotate tasks.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Don't worry, if there's a surprise inspection, I'll head back into the foregolem's office and watch the production line as I used to.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Great![SOFTBLOCK] This is how it ought to be![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Maybe it's my memories being fuzzy, but I recall something about you doing that in your sector?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] I'm not used to manual labour, but I find it oddly refreshing to just do something with your hands for a while.[SOFTBLOCK] It feels good.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] In short bursts, anyway.[SOFTBLOCK] A machine's life should be balanced.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] By the way, am I recalling right that you're dating a slave unit?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Sorry if I'm being too forward, my fuzzy memories seem to indicate 771852 having a reputation for that.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] Yes?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] How do you go about asking one out?[SOFTBLOCK] The...[SOFTBLOCK] the unit who works at the loading dock...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Be honest and upfront.[SOFTBLOCK] You have nothing to be ashamed of, love is love.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Yeah.[SOFTBLOCK] It's going to take a while to shake off years of conditioning.[SOFTBLOCK] I can't believe I put my feelings aside for such a petty reason as model variant.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[E|Neutral] Thanks, Christine.[SOFTBLOCK] For everything.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("Foregolem", 0, -1)
            
        end
        
    elseif(sActorName == "GolemA") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oil is a useful way to recharge a depleted core, though not as efficient as a defragmentation pod.[SOFTBLOCK] This unit is consuming oil.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (All the oil is unflavoured.[SOFTBLOCK] Don't the golems here like any of the flavours?)") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oil is a useful way to recharge a depleted core, though not as efficient as a defragmentation pod.[SOFTBLOCK] This unit is consuming oil.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] DRONE 772890 DOES NOT REQUIRE RECHARGING.[SOFTBLOCK] RETURNING TO ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oil flavour packets...[SOFTBLOCK] mmmm...[SOFTBLOCK] tungsten![SOFTBLOCK] Getting assimilated into a hive mind was the best thing to happen to me!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemB") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I am watching a popular show. It is called 'All My Processors'.[SOFTBLOCK] I enjoy it.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemB", 0, -1)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I am watching a popular show. It is called 'All My Processors'.[SOFTBLOCK] I enjoy it.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] ENJOYMENT NOT REQUIRED FOR DRONE EFFICIENCY.[SOFTBLOCK] RETURNING TO ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemB", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] 'All My Processors' is the one constant throughout the years.[SOFTBLOCK] No matter what happens, I still have no idea what the hell is going on in this show.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Though I had a great idea for an episode.[SOFTBLOCK] Unit 593490 and Unit 399421 get assimilated into a hive mind...") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemC") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Fizzy Pop! is a useful way to recharge a depleted core, though not as efficient as a defragmentation pod.[SOFTBLOCK] This unit is consuming Fizzy Pop!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemC", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] If there's one thing everyone::[SOFTBLOCK] Steam droid, golem, lord, drone, can bond over, it's our shared addiction to Fizzy Pop!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemC", 0, -1)
        
        end
        
    elseif(sActorName == "GolemD") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Materials are stored below and transferred for unloading here.[SOFTBLOCK] I am making sure nothing jams the line.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Materials are stored below and transferred for unloading here.[SOFTBLOCK] I am making sure nothing jams the line.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] DRONE INQUIRES IF ASSISTANCE IS REQUIRED.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Negative. Return to your existing assignment, drone.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] We rotate tasks now, but I like to get this job when I can.[SOFTBLOCK] Watching the crates go by is just so relaxing...") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemE") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] I am observing production statistics.[SOFTBLOCK] It is important to notice production trends and improve on them.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemE", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] The production statistics haven't been updated in months.[SOFTBLOCK] My guess is that's when this whole thing began.[SOFTBLOCK] Hopefully we'll find out more if there's anything left on the blown-out servers.") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemF") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Visitors should not cross the yellow lines unless they are connected to the network and properly programmed.[SOFTBLOCK] Please stand back.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemF", 0, 1)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Drone 772890's hair is out of standard.[SOFTBLOCK] Do not cross the yellow markers unless explicitly ordered.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] AFFIRMATIVE.[SOFTBLOCK] DRONE WILL REMAIN BEHIND YELLOW MARKERS.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemF", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Oh, Christine, 2855![SOFTBLOCK] Sorry, but you'd still best stay behind the lines.[SOFTBLOCK] Wouldn't want to get in the way of our production line.[SOFTBLOCK] We're even more efficient now that we work as a team!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemG") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My task is to report errors or defects in the production line.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemG", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] My task is to report errors or defects in the production line...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Ha![SOFTBLOCK] Gotcha![SOFTBLOCK] I remember saying that really clearly, but not much else.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemG", 0, -1)
        
        end
    elseif(sActorName == "DroneA") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT IS BEING INSPECTED FOR FLAWS.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneA", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] Sometimes I want to just turn my inhibitor chip back on and slip away, but then I'd be letting down all my fellow units.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneA", 0, 1)
        end
        
    elseif(sActorName == "DroneB") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT IS PERFORMING AN INSPECTION OF A FELLOW DRONE.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneB", 1, 0)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT IS PERFORMING AN INSPECTION OF A FELLOW DRONE.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] EVALUATION.[SOFTBLOCK] DRONE TECHNIQUE RATING::[SOFTBLOCK] SPIFFY.[SOFTBLOCK] LOGGING RESULTS WITH CENTRAL SERVER.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] AFFIRMATIVE.[SOFTBLOCK] THIS UNIT APPRECIATES EVALUATION.[SOFTBLOCK] RESUMING.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] (DRONE EMOTION::[SOFTBLOCK] ENJOYMENT.[SOFTBLOCK] CAUSE::[SOFTBLOCK] HIGH-QUALITY INSPECTION TECHNIQUE OBSERVED.[SOFTBLOCK] LOGGED WITH CENTRAL SERVER.[SOFTBLOCK] RESUMING TASK.)") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneB", 1, 0)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] Inspecting drones is the best job...[SOFTBLOCK] I'm so lucky!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneB", 1, 0)
        end
        
    elseif(sActorName == "DroneC") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT IS CLEANING DUST FROM THE WALLS.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneC", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT IS UNABLE TO FUNCTION WITHOUT ITS INHIBITOR CHIP ACTIVATED.[SOFTBLOCK] THIS UNIT WILL MOP THE FLOORS AS ORDERED.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I wonder if she was forced to become a drone after having her CPU badly damaged...)") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneC", 0, -1)
        end
    elseif(sActorName == "DroneD") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] THIS UNIT IS CLEANING DUST FROM THE WALLS.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneD", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] I was elected foregolem almost unanimously.[SOFTBLOCK] With my inhibitor chip turned off, I'm a capable organizer.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] That means you were likely a revolutionary who was repurposed as punishment.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] That's what I had guessed, but I can't remember anything before becoming a drone.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] When we win our freedom, I hope all the others like me can find out who we were.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] If the data exists, we will find it for you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|LatexDrone] Thank you, 2855, Christine.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneD", 0, 1)
        
        end
        
    elseif(sActorName == "LordA") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] I am relaxing with my fellow units.[SOFTBLOCK] Breaks improve productivity.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordA", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord:[VOICE|GolemLord] I never frequented the slave break room before, but now?[SOFTBLOCK] We simply *must* get this place redecorated!") ]])
            fnCutsceneBlocker()
        
        end
    end
end