--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Variables.
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManuDroneTaskA         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N")
local iManuDroneTaskB         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N")
local iManuDroneTaskC         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--[Exits]
if(sObjectName == "ToManufactoryG") then

    --Variables.
    local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
    
    --Not able to open yet.
    if(iManufactoryDepopulated == 0.0) then
        
        --Normal case:
        if(iManuDroneTaskC < 2.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Access to automated long-term storage facility requires Class B authorization.[SOFTBLOCK] Access denied.") ]])
            fnCutsceneBlocker()
        
        --Bonus scene:
        else

            --Fade to black, cut music.
            fnCutsceneInstruction([[ AL_SetProperty("Music", "NULL") ]])
            fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Wait a bit.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] UNIT 772890 PRESENTING FOR NETWORK INTEGRATION.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Good.[SOFTBLOCK] Unit, state your purpose.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] PRESENTING FOR NETWORK INTEGRATION.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Incorrect.[SOFTBLOCK] You were sent to spy on us.[SOFTBLOCK] What was your original designation?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] QUERY NOT UNDERSTOOD.[SOFTBLOCK] UNIT 772890 PRESENTING FOR NETWORK INTEGRATION.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Fine.[SOFTBLOCK] We'll take the information from your memory banks directly.[SOFTBLOCK] Drone, open CPU casing.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] CASING OPENED.[SOFTBLOCK] PROCEED.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Scene.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Drone 772890 stood unflinching as a trio of golems surrounded her.[SOFTBLOCK] One produced a small device.[SOFTBLOCK] The drone could not identify it.[SOFTBLOCK] The drone continue to stand at attention.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The golem placed the device on the drone's synaptic spike, and closed her CPU casing.[SOFTBLOCK] The device activated.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] All at once, Christine recalled who she was.[SOFTBLOCK] She felt unusual, as all of her memories surged back.[SOFTBLOCK] She was...[SOFTBLOCK] calm.[SOFTBLOCK] Organized.[SOFTBLOCK] Disciplined.[SOFTBLOCK] She was not concerned.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The memories kept flowing.[SOFTBLOCK] Every detail of her life.[SOFTBLOCK] Childhood, becoming a golem, coming to Sector 99.[SOFTBLOCK] She was remembering all of it vividly.[SOFTBLOCK] Why?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Because we needed the memories.[SOFTBLOCK] Christine understood.[SOFTBLOCK] The memories were being replayed and stored elsewhere.[SOFTBLOCK] The collective needed them to understand who she was and what role she needed to fill.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The trio of golems returned to the factory floor as Christine continued uploading her memory data.[SOFTBLOCK] It took a few minutes to finish, while she stood at attention and waited.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The upload completed and Christine felt new instructions enter her mind.[SOFTBLOCK] These were not her thoughts, these were override instructions downloaded via her synaptic spike.[SOFTBLOCK] She expressed alarm.[SOFTBLOCK] Her alarm was sent along the synaptic spike, as well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She was told not to be alarmed, and the feeling subsided.[SOFTBLOCK] Everything was fine, this was quite common in new members.[SOFTBLOCK] She was now part of a group mind, that was where the thoughts were coming from.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Christine informed the group mind of her upcoming revolution, her friend 55, her long-term goals.[SOFTBLOCK] The group mind absorbed the data.[SOFTBLOCK] She could feel it adjusting itself to the new unit's personality.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] She informed it of the threats she had encountered, her combat experience, her knowledge of Earth.[SOFTBLOCK] It adjusted further still.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] At last, the synchronization was completed.[SOFTBLOCK] The group mind had integrated her.[SOFTBLOCK] She forgot her own name, no longer needing it.[SOFTBLOCK] She was not Christine, nor was she 771852, a drone, or anyone else.[SOFTBLOCK] She was merely another node.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The node could see and hear the other nodes in the network, understand their thoughts.[SOFTBLOCK] She worked to process the information they passed back, help with decisions, come to conclusions based on evidence.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] At the node's recommendation, the group sent fifty nodes to capture Unit 2855.[SOFTBLOCK] Even with their foreknowledge, the unit escaped into the undercity.[SOFTBLOCK] The group mind learned and adapted.[SOFTBLOCK] She would not escape the next encounter.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The group mind had cut off direct tram access to protect itself.[SOFTBLOCK] It was at risk of being discovered due to Katarina's questions.[SOFTBLOCK] It had made a mistake by increasing productivity unexpectedly.[SOFTBLOCK] It learned and grew from these errors.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The node formerly known as 'Christine' was sent back to Sector 96 to keep up appearances.[SOFTBLOCK] Contact with Unit 2855 was not re-established.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The collective tried to maintain secrecy, but the command units had discovered them.[SOFTBLOCK] Some of their nodes were isolated and lost.[SOFTBLOCK] This occurred slowly, over the course of several months.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The collective could not locate its lost nodes.[SOFTBLOCK] Its inspection of the Regulus City network indicated an attack was coming.[SOFTBLOCK] Security units were massing.[SOFTBLOCK] It sealed the surface access zones.[SOFTBLOCK] The collective prepared.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The attack came.[SOFTBLOCK] The collective fought as one.[SOFTBLOCK] The node formerly known as 'Christine' fought alongside them.[SOFTBLOCK] She was powerful.[SOFTBLOCK] But then the collective heard the thing it feared most.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The killphrase, burnt into its software and unable to be removed by design, shot through the collective.[SOFTBLOCK] All its nodes went offline.[SOFTBLOCK] Then, the collective did, as well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] The node known as 'Christine' was captured and interrogated.[SOFTBLOCK] The future that was meant to happen, happened, and all was lost.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] Or so it seemed.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Narrator] ...") ]])
            fnCutsceneBlocker()
            fnCutsceneTeleport("Christine", 34.75, 8.50)
            fnCutsceneFace("Christine", 0, 1)
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            
            --Fade back in.
            fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
            fnCutsceneBlocker()

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] (...[SOFTBLOCK] UNIT STATUS::[SOFTBLOCK] ORANGE.[SOFTBLOCK] SYSTEM SCAN INITIATED.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] (UNIDENTIFIED MEMORY FILES LOCATED.[SOFTBLOCK] NO FILE METADATA AVAILABLE.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] (COMMAND UNIT...[SOFTBLOCK] COMMAND UNIT...)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] (UNIT PROHIBITED FROM UPLOADING DIAGNOSTIC DATA TO NETWORK::[SOFTBLOCK] SHARE WITH COMMAND UNIT 2855 IMMEDIATELY.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "772890:[VOICE|Christine] (UNIT 772890 MUST PROCEED TO SECTOR 99 IMMEDIATELY.[SOFTBLOCK] OVERRIDE QUIM77T.[SOFTBLOCK] EXECUTE.)") ]])
            fnCutsceneBlocker()
            
            --Music resumes.
            fnCutsceneInstruction([[ AL_SetProperty("Music", "LAYER|Manufactory") ]])
            fnCutsceneInstruction([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])

        end

    --Scene when opening.
    elseif(iManufactoryDepopulated == 1.0) then
    
        --First time.
        local iManuOpenedStorageDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuOpenedStorageDoor", "N")
        if(iManuOpenedStorageDoor == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iManuOpenedStorageDoor", "N", 1.0)
    
            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You couldn't hack anything beyond this point?[SOFTBLOCK] No other entrances?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] No, the sector's network security is exemplary.[SOFTBLOCK] I did not want to risk revealing my position.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hacker is out, repair unit, you're up![SOFTBLOCK] PDU?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Screwdriver tool.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "*click*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] Bridge tool.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "*click*[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] All done![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] Impressive.[SOFTBLOCK] How did you bypass the security on the door?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Unscrew the panel and move the electrical wire out of the network auth box and into the motivator box.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] These aren't high-security doors, which have the motivator on the [SOFTBLOCK]*other*[SOFTBLOCK] side.[SOFTBLOCK] This is a civilian door.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Odd.[SOFTBLOCK] The network security was high, but the physical security was not?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, that is odd.[SOFTBLOCK] Maybe whatever they're hiding, they didn't expect anyone to physically enter the area?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Or maybe they didn't think three-dimensionally like I do.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Add it to the pile of oddities.[SOFTBLOCK] Let's go.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
    
        end
    
        --Common code.
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusManufactoryG", "ENTPOS:ToManufactoryF:N") ]])
        fnCutsceneBlocker()
    end
    
--[Examinables]
elseif(sObjectName == "BrownCrates") then
    
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Spare computer parts.[SOFTBLOCK] Motherboards, chipsets, power supplies.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManuDroneTaskA == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (ACQUIRE PARTS, DELIVER TO THIS AREA.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManuDroneTaskA == 1.0) then
        AudioManager_PlaySound("World|TakeItem")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N", 2.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (PART DELIVERY COMPLETED.[SOFTBLOCK] REPORT TO LORD UNIT FOR FURTHER ASSIGNMENT.)") ]])
        fnCutsceneBlocker()
        AL_SetProperty("Clear Objectives")
        AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:")
        AL_SetProperty("Register Objective", "1: REPORT TO LORD UNIT IN FACTORY FLOOR OFFICE FOR FURTHER ASSIGNMENT")
    elseif(iManuReassigned == 1.0 and iManuDroneTaskA > 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (FUNCTION DOES NOT REQUIRE THESE PARTS.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "FizzyPop") then
    
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Fizzy Pop!, the best way to recharge![SOFTBLOCK] This is the blue variety, meant for slave units.[SOFTBLOCK] I assume factory workers go through a lot of energy refills.)") ]])
        fnCutsceneBlocker()
    else
        
        --Not on assignment.
        if(iManuDroneTaskB == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (FIZZY POP.[SOFTBLOCK] THIS UNIT DOES NOT REQUIRE A RECHARGE. IGNORING.)") ]])
            fnCutsceneBlocker()
        
        --Get dat fizzy.
        elseif(iManuDroneTaskB == 1.0) then
            AudioManager_PlaySound("World|TakeItem")
            AL_SetProperty("Flag Objective True", "1: ACQUIRE FIZZY POP! CANISTER")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N", 2.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A CANISTER OF FIZZY POP HAS BEEN ACQUIRED.[SOFTBLOCK] DRONE WILL DELIVER CANISTER TO ADMINISTRATIVE LORD GOLEM IN SECTOR 99.)") ]])
            fnCutsceneBlocker()
        
        --Deliver dat fizzy.
        elseif(iManuDroneTaskB == 2.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (CANISTER MUST BE DELIVERED TO ADMINISTRATIVE LORD GOLEM IN SECTOR 99.)") ]])
            fnCutsceneBlocker()
        
        --Other cases.
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (FIZZY POP.[SOFTBLOCK] THIS UNIT DOES NOT REQUIRE A RECHARGE.[SOFTBLOCK] IGNORING.)") ]])
            fnCutsceneBlocker()
        end
    end
    
elseif(sObjectName == "Elevators") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Freight elevators for the underground packing center.[SOFTBLOCK] You can also access the slave domiciles from here.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE DOES NOT REQUIRE ACCESS TO PACKING CENTER OR SLAVE DOMICILES.[SOFTBLOCK] POWER CORE 95pct, RECHARGE IN 53 HOURS.)") ]])
        fnCutsceneBlocker()
    end
        
elseif(sObjectName == "Oilmaker") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unflavoured oil...[SOFTBLOCK] What kind of philistine drinks unflavoured oil!?)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE DOES NOT REQUIRE LUBRICATION OR ENERGY RECHARGE.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I know I'm doing the right thing because now the oilmaker has flavour packets next to it.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "RVD") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('How to be Productive', a very special episode of 'All My Processors'.[SOFTBLOCK] It's probably the worst episode, but it's set to play on loop.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (RVD INTERACTION NOT REQUIRED FOR CURRENT FUNCTION ASSIGNMENT.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Unit 2834 is hanging off a cliff by her fingertips with a villain standing over her.[SOFTBLOCK] The worst part is there's at least six episodes this could be without further context.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Terminal") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This terminal controls the RVD in this room.[SOFTBLOCK] It's set to play the same episode of 'All My Processors' on loop, and nobody has bothered to change it yet.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (TERMINAL CONTROLS RVD.[SOFTBLOCK] RVD INTERACTION NOT REQUIRED FOR CURRENT FUNCTION ASSIGNMENT.)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (RVD controls.[SOFTBLOCK] It has episodes of 'All My Processors' playing.[SOFTBLOCK] I guess every unit loves the show!)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "NoteA") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Long-term storage facility.[SOFTBLOCK] No access without authorization.[SOFTBLOCK] Caution::[SOFTBLOCK] Automated moving parts!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteB") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Productivity statistics for 6 months ago.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE IS NOT REQUIRED TO RECORD OR UNDERSTAND PRODUCTIVITY STATISTICS.[SOFTBLOCK] IGNORING.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "NoteC") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Productivity statistics for 5 months ago.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE IS NOT REQUIRED TO RECORD OR UNDERSTAND PRODUCTIVITY STATISTICS.[SOFTBLOCK] IGNORING.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "NoteD") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Productivity statistics for 4 months ago.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE IS NOT REQUIRED TO RECORD OR UNDERSTAND PRODUCTIVITY STATISTICS.[SOFTBLOCK] IGNORING.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "NoteE") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Productivity statistics for 3 months ago.[SOFTBLOCK] This is the most recent chart on the wall, but it's behind by a few months.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (DRONE IS NOT REQUIRED TO RECORD OR UNDERSTAND PRODUCTIVITY STATISTICS.[SOFTBLOCK] IGNORING.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Laptop") then
    if(iManuReassigned == 0.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Might not be a good idea to inspect the lord unit's computer while she's in the room.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (FUNCTION ASSIGNMENT DOES NOT CONCERN LORD'S PORTABLE COMPUTER, IGNORING.)") ]])
        fnCutsceneBlocker()
    elseif(iManuFinale == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The portable computer is blank.[SOFTBLOCK] Was it wiped, or was the foregolem not actually using it?)") ]])
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I think I'll not snoop on the foredrone's laptop.)") ]])
        fnCutsceneBlocker()
    end
    
--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end