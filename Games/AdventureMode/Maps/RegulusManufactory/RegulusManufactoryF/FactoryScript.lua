--[Factory Script]
--Moves some of the factory workers around to improve dynamism.

--[Initial]
--Move the latex drones on the west end.
fnCutsceneMove("WorkDroneUA", 8.25, 17.50)
fnCutsceneMove("WorkDroneUA", 6.25, 17.50)
fnCutsceneMove("WorkDroneUA", 6.25, 18.50)
fnCutsceneFace("WorkDroneUA", -1, 0)
fnCutsceneMove("WorkDroneUB", 8.25, 18.50)
fnCutsceneFace("WorkDroneUB", 1, 0)

fnCutsceneMove("WorkDroneLA", 8.25, 21.50)
fnCutsceneMove("WorkDroneLA", 6.25, 21.50)
fnCutsceneMove("WorkDroneLA", 6.25, 22.50)
fnCutsceneFace("WorkDroneLA", -1, 0)
fnCutsceneMove("WorkDroneLB", 8.25, 22.50)
fnCutsceneFace("WorkDroneLB", 1, 0)

--Worker golems reposition.
fnCutsceneMove("WorkGolemUA", 15.25, 19.50)
fnCutsceneFace("WorkGolemUA", 0, -1)

fnCutsceneMove("WorkDroneUC", 34.25, 19.50)
fnCutsceneFace("WorkDroneUC", 0, -1)

fnCutsceneFace("WorkGolemUC", -1, 0)

fnCutsceneMove("WorkGolemLA", 15.25, 23.50)
fnCutsceneFace("WorkGolemLA", 0, -1)

fnCutsceneMove("WorkDroneLC", 34.25, 23.50)
fnCutsceneFace("WorkDroneLC", 0, -1)

fnCutsceneFace("WorkGolemLC", -1, 0)
fnCutsceneBlocker()

--Cycle.
fnCutsceneWait(125)
fnCutsceneBlocker()

--[Second Phase]
--Swap the drones back.
fnCutsceneMove("WorkDroneUB", 8.25, 17.50)
fnCutsceneMove("WorkDroneUB", 6.25, 17.50)
fnCutsceneMove("WorkDroneUB", 6.25, 18.50)
fnCutsceneFace("WorkDroneUB", -1, 0)
fnCutsceneMove("WorkDroneUA", 8.25, 18.50)
fnCutsceneFace("WorkDroneUA", 1, 0)

fnCutsceneMove("WorkDroneLB", 8.25, 21.50)
fnCutsceneMove("WorkDroneLB", 6.25, 21.50)
fnCutsceneMove("WorkDroneLB", 6.25, 22.50)
fnCutsceneFace("WorkDroneLB", -1, 0)
fnCutsceneMove("WorkDroneLA", 8.25, 22.50)
fnCutsceneFace("WorkDroneLA", 1, 0)

--Worker golems reposition.
fnCutsceneMove("WorkGolemUA", 11.75, 19.50)
fnCutsceneFace("WorkGolemUA", 0, -1)

fnCutsceneMove("WorkDroneUC", 37.75, 19.50)
fnCutsceneFace("WorkDroneUC", 0, -1)

fnCutsceneFace("WorkGolemUC", 0, 1)

fnCutsceneMove("WorkGolemLA", 11.75, 23.50)
fnCutsceneFace("WorkGolemLA", 0, -1)

fnCutsceneMove("WorkDroneLC", 37.75, 23.50)
fnCutsceneFace("WorkDroneLC", 0, -1)

fnCutsceneFace("WorkGolemLC", 0, 1)
fnCutsceneBlocker()

--Cycle.
fnCutsceneWait(125)
fnCutsceneBlocker()

--[Script Repeats]