--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Volume changers.
if(sObjectName == "ManuLoud") then
    AL_SetProperty("Mandated Music Intensity", 80.0)
    
elseif(sObjectName == "ManuQuiet") then
    AL_SetProperty("Mandated Music Intensity", 60.0)
    
--Comedy!
elseif(sObjectName == "SprintScene") then

    --Force fade to black.
	AL_SetProperty("Music", "Null")
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Collision removal.
    AL_SetProperty("Set Collision", 28, 15, 0, 0)
    AL_SetProperty("Set Collision", 30, 15, 0, 0)
    
    --Spawn characters.
    fnStandardNPCByPosition("Katarina")
    fnStandardNPCByPosition("DeliveryGolem")
    
    --Spawn robots.
    for x = 0, 2, 1 do
        for y = 0, 8, 1 do
            local iRoll = LM_GetRandomNumber(1, 100)
            TA_Create("Golem"..x..y)
                TA_SetProperty("Position", 31+x, 8+y)
                TA_SetProperty("Facing", gci_Face_West)
                TA_SetProperty("Clipping Flag", false)
                if(iRoll < 40) then
                    fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
                elseif(iRoll < 80) then
                    fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
                else
                    fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", false)
                end
            DL_PopActiveObject()
        end
    end
    
    --Reposition everyone.
    AL_SetProperty("Open Door", "DoorM")
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (29.25 * gciSizePerTile), (25.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneTeleport("Katarina", 29.25, 25.50)
    fnCutsceneFace("Katarina", -1, 0)
    fnCutsceneTeleport("DeliveryGolem", 29.25, 26.50)
    fnCutsceneFace("DeliveryGolem", 0, -1)
    fnCutsceneTeleport("Christine", 29.25, 9.50)
    fnCutsceneTeleport("55", 29.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Katarina", 1, 0)
    fnCutsceneFace("DeliveryGolem", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Katarina", 0, -1)
    fnCutsceneFace("DeliveryGolem", -1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Katarina", 0, 1)
    fnCutsceneFace("DeliveryGolem", 0, -1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Katarina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Pack") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Where is everyone?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Did they all go on break simultaneously?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] ...[SOFTBLOCK] That's actually a reasonable explanation.[SOFTBLOCK] Perhaps the machinery overheats and requires a cooldown period, so breaks are timed to coincide with that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] No, that wouldn't be the case because such machinery would have an obvious efficiency improvement and we'd have made it by now.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] The machinery doesn't look out of date or poorly maintained.[SOFTBLOCK] There must be another explanation.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] For why this place looks abandoned?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Oh, someone is coming.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 29.25, 16.50, 2.50)
    fnCutsceneMove("Christine", 28.25, 17.50, 2.50)
    fnCutsceneMove("Christine", 28.25, 26.50, 2.50)
    fnCutsceneMove("55", 29.25, 16.50, 2.50)
    fnCutsceneMove("55", 28.25, 17.50, 2.50)
    fnCutsceneMove("55", 28.25, 25.50, 2.50)
    fnCutsceneFace("Katarina", 0, -1)
    fnCutsceneFace("DeliveryGolem", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] RUUUUUUUUNNN!!!![SOFTBLOCK]") ]])
    fnCutsceneFace("Katarina", -1, 0)
    fnCutsceneFace("DeliveryGolem", -1, 0)
    fnCutsceneMove("Christine", 13.25, 26.50, 2.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneMove("55", 28.25, 26.50, 2.50)
    fnCutsceneMove("55", 13.25, 26.50, 2.50)
    fnCutsceneFace("55", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Katarina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Pack") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] Now what was that all about?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] In my sector, we have a saying.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] 'If you see a command unit running from something, follow them.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[E|Neutral] What are they running from?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Pack] Oh probably that huge crowd of golems.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Golems appear!
    for x = 0, 2, 1 do
        for y = 0, 8, 1 do
            fnCutsceneTeleport("Golem"..x..y, 28.25+x, 9.50+y)
            fnCutsceneMove(    "Golem"..x..y, 28.25+x, 22.50+y-8, 1.50)
        end
    end
    fnCutsceneFace("Katarina", 0, -1)
    fnCutsceneFace("DeliveryGolem", 0, -1)
    fnCutsceneBlocker()
    
    --RUN!
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Katarina:[VOICE|Katarina] R-[SOFTBLOCK]RUUUNNNN!!![SOFTBLOCK]") ]])
    fnCutsceneMove("Katarina", 29.25, 26.50, 3.50)
    fnCutsceneMove("Katarina", 11.25, 26.50, 3.50)
    fnCutsceneMove("DeliveryGolem", 11.25, 26.50, 3.50)
    for y = 0, 8, 1 do
        fnCutsceneMove("Golem0"..y, 28.25, 26.50, 1.50)
        fnCutsceneMove("Golem1"..y, 29.25, 27.50, 1.50)
        fnCutsceneMove("Golem2"..y, 30.25, 28.50, 1.50)
    end
    for x = 0, 2, 1 do
        for y = 0, 8, 1 do
            fnCutsceneMove("Golem"..x..y, 11.25, 26.50+x, 1.50)
        end
    end
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 325, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneWait(385)
    fnCutsceneBlocker()
    
    --Transition to next scene.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusManufactoryE", "FORCEPOS:4.0x15.0x0") ]])
    fnCutsceneBlocker()

end
