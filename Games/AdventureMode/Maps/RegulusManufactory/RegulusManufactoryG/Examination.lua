--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--[Examinables]
if(sObjectName == "Machines") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Spare processing machines.[SOFTBLOCK] Unlike the rest of this stuff, these are probably for the sector itself and not for shipping out.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Tubes") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Organic containment tubes, also used to study fluid dynamics.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Equipment") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Fabricator benches and replacement parts boxes.[SOFTBLOCK] Presumably these will be shipped out at some point.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyPop") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Fizzy Pop! machines made in this sector, ready to deliver unbridled joy followed by sugar withdrawal to Regulus City!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminals") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A wall of unused terminals ready to be shipped out.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Lights") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Surface lights.[SOFTBLOCK] There's no power supply in them.)") ]])
    fnCutsceneBlocker()
    
--[Other]

--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end