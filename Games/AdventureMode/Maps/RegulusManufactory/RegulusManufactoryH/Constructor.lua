--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusManufactoryH"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "RegulusContainment")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusManufactoryH")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.

    --[Dislocation]
    --Modifies the position of the crates.
    AL_SetProperty("Add Dislocation", "CrateDislocation", "Crate", 0, 0, 30, 5, 39 * gciSizePerTile, 19 * gciSizePerTile)
    
    --Crate is in default position.
    local iManuCrateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuCrateState", "N")
    if(iManuCrateState == 0.0) then
        AL_SetProperty("Modify Dislocation", "CrateDislocation", (39 * gciSizePerTile) + 48, 19 * gciSizePerTile)
        AL_SetProperty("Set Collision", 51, 22, 0, 0)
        AL_SetProperty("Set Collision", 52, 22, 0, 0)
        AL_SetProperty("Set Collision", 43, 22, 0, 1)
        AL_SetProperty("Set Collision", 44, 22, 0, 1)
    else
        AL_SetProperty("Set Layer Disabled", "CrateHi", true)
        AL_SetProperty("Set Layer Disabled", "CrateLo", true)
        AL_SetProperty("Set Collision", 51, 22, 0, 1)
        AL_SetProperty("Set Collision", 52, 22, 0, 1)
        AL_SetProperty("Set Collision", 43, 22, 0, 0)
        AL_SetProperty("Set Collision", 44, 22, 0, 0)
    end
    
    --[Collapse]
    --If the player hasn't completed the Manufactory subquest, remove the collapsed ceiling.
    local iManuFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")
    if(iManuFinale == 0.0) then
        AL_SetProperty("Set Layer Disabled", "Floor1Collapse", true)
        AL_SetProperty("Set Layer Disabled", "Walls0Collapse", true)
        AL_SetProperty("Set Layer Disabled", "WallsHi0Collapse", true)
        AL_SetProperty("Set Collision", 57,  8, 0, 0)
        AL_SetProperty("Set Collision", 58,  8, 0, 0)
        AL_SetProperty("Set Collision", 59,  8, 0, 0)
        AL_SetProperty("Set Collision", 60,  8, 0, 0)
        AL_SetProperty("Set Collision", 61,  8, 0, 0)
        AL_SetProperty("Set Collision", 57,  9, 0, 0)
        AL_SetProperty("Set Collision", 58,  9, 0, 0)
        AL_SetProperty("Set Collision", 59,  9, 0, 0)
        AL_SetProperty("Set Collision", 60,  9, 0, 0)
        AL_SetProperty("Set Collision", 61,  9, 0, 0)
        AL_SetProperty("Set Collision", 11, 12, 0, 0)
        AL_SetProperty("Set Collision", 12, 12, 0, 0)
        AL_SetProperty("Set Collision", 13, 12, 0, 0)
        AL_SetProperty("Set Collision", 14, 12, 0, 0)
        AL_SetProperty("Set Collision", 15, 12, 0, 0)
    end

    --[Remove NPCs]
    --Remove entities after the scenario is over.
    if(iManuFinale == 1.0) then
        AL_RemoveObject("Enemy", "EnemyAA")
        AL_RemoveObject("Enemy", "EnemyBA")
    end
end
