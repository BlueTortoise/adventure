--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusManufactoryJ"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "DatacoreAmbient")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusManufactoryJ")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    for x = 0, 8, 1 do
        for y = 0, 2, 1 do
            local iRoll = LM_GetRandomNumber(1, 100)
            TA_Create("Golem"..x..y)
                TA_SetProperty("Position", 61+x, 14+y)
                TA_SetProperty("Facing", gci_Face_East)
                TA_SetProperty("Clipping Flag", false)
                if(iRoll < 40) then
                    fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
                elseif(iRoll < 80) then
                    fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
                else
                    fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", false)
                end
            DL_PopActiveObject()
        end
    end
end
