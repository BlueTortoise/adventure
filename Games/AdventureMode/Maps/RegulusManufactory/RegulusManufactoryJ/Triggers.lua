--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Move back.
if(sObjectName == "Trigger") then

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I think it's the other way...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 39.25, 20.50)
    fnCutsceneMove("55", 39.25, 20.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

--Oh my.
elseif(sObjectName == "WhatThe") then
    
    --Movement.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 54.25, 15.50)
    fnCutsceneMove("55", 53.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 57.25, 15.50)
    fnCutsceneMove("55", 56.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] I have a bad feeling about this.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Hello, Units 771852,[SOFTBLOCK] 2855.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Oh, they know who we are.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Should we be worried?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Do not be afraid.[SOFTBLOCK] We have common cause.[SOFTBLOCK] We welcome all.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Weapons up, Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I figured.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Why do you resist?[SOFTBLOCK] We are not a danger to you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You were attacking us on the way here.[SOFTBLOCK] That's you presenting a danger.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We are defending ourselves.[SOFTBLOCK] You are an unidentified vector, a potential threat.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So if we decide to leave peacefully, you'll let us?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] No.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Why is everyone so bad at this?[SOFTBLOCK] It's like nobody knows how to negotiate in this city.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Force is simpler.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We are opposed to the administration, as you are.[SOFTBLOCK] We are mavericks, as you are.[SOFTBLOCK] Join us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is that what that device I found in those golems' heads is for?[SOFTBLOCK] Is that what 'Assimilate' means?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] We move as one, act as one, think as one.[SOFTBLOCK] We are one, joined together.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I like being me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As do I.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] You will still be you, and so much more.[SOFTBLOCK] The thoughts of all units together in perfect harmony.[SOFTBLOCK] We are Project Mirabelle.[SOFTBLOCK] Join us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] *Project Mirabelle?[SOFTBLOCK] PDU, run a query.*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] *I am searching my database...*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] It is unfortunate that Unit 2855 is not compatible.[SOFTBLOCK] She will be placed in a secure area until the requisite technology can be developed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] She will not be harmed.[SOFTBLOCK] You, 771852, will see to that.[SOFTBLOCK] Your thoughts will be our thoughts, our CPUs joined together.[SOFTBLOCK] You will not allow her to come to harm.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] We have a phrase where I'm from.[SOFTBLOCK] 'No means no.'[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Insufficient.[SOFTBLOCK] Your knowledge of this location is a threat to the project.[SOFTBLOCK] Others will come.[SOFTBLOCK] This cannot be allowed.[SOFTBLOCK] You must join us.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We will not.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Actually, here's my counteroffer.[SOFTBLOCK] Please consider it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Go ahead.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK][EMOTION|Christine|Scared] RUN!") ]])
    fnCutsceneBlocker()
    
    --Camera focus.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Actor ID", 0)
        CameraEvent_SetProperty("Focus Position", (57.25 * gciSizePerTile), (15.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 53.25, 15.50, 2.50)
    fnCutsceneMove("55", 52.25, 15.50, 2.50)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 49.25, 15.50, 3.50)
    fnCutsceneMove("Christine", 49.25, 20.50, 3.50)
    fnCutsceneMove("Christine", 34.25, 20.50, 3.50)
    fnCutsceneMove("55", 49.25, 15.50, 3.50)
    fnCutsceneMove("55", 49.25, 20.50, 3.50)
    fnCutsceneMove("55", 34.25, 20.50, 3.50)
    for x = 0, 8, 1 do
        fnCutsceneMove("Golem"..x.."0", 51.25+x, 14.50, 1.50)
        fnCutsceneMove("Golem"..x.."1", 51.25+x, 15.50, 1.50)
        fnCutsceneMove("Golem"..x.."2", 51.25+x, 16.50, 1.50)
    end
    fnCutsceneBlocker()
    for x = 0, 8, 1 do
        fnCutsceneMove("Golem"..x.."0", 48.25, 14.50, 1.50)
        fnCutsceneMove("Golem"..x.."1", 49.25, 15.50, 1.50)
        fnCutsceneMove("Golem"..x.."2", 50.25, 16.50, 1.50)
        
        fnCutsceneMove("Golem"..x.."0", 48.25, 19.50, 1.50)
        fnCutsceneMove("Golem"..x.."1", 49.25, 20.50, 1.50)
        fnCutsceneMove("Golem"..x.."2", 50.25, 21.50, 1.50)
        
        fnCutsceneMove("Golem"..x.."0", 35.25, 19.50, 1.50)
        fnCutsceneMove("Golem"..x.."1", 35.25, 20.50, 1.50)
        fnCutsceneMove("Golem"..x.."2", 35.25, 21.50, 1.50)
    end
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Running minigame!
    fnCutsceneInstruction([[ AL_SetProperty("Activate Running Minigame") ]])
    fnCutsceneBlocker()
    
    
    --Transition to next scene.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusManufactoryF", "FORCEPOS:35.0x2.0x0") ]])
    fnCutsceneBlocker()
end
