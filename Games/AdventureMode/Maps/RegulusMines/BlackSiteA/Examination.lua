--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "SecretDoor") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("BlackSiteB", "FORCEPOS:27.0x30.0x0")
	
--[Examinables]
elseif(sObjectName == "Junk") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Nothing worth keeping on this shelf.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "VentHatch") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A vent.[SOFTBLOCK] I suppose this gives away the detention facility, since there's nothing on the schematics behind this wall...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Elevator") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This leads up to Regulus City.[SOFTBLOCK] Despite this being a supposedly abandoned site, the elevator is maintained and has been used recently...)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end