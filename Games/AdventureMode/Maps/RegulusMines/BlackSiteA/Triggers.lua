--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Triggers]
--55 leaves the party.
if(sObjectName == "55Leaves") then
	
	--Variables.
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	if(iCompletedBlackSite == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My motion tracker indicates the Steam Droids are already inside the facility.[SOFTBLOCK] This is where we part ways.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Right.[SOFTBLOCK] Good luck, 55...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Move 55 away.
		fnCutsceneMove("55", 18.25, 13.50)
		fnCutsceneMove("55", 25.25, 13.50)
		fnCutsceneBlocker()
		fnCutsceneFace("55", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneTeleport("55", -100.25, -100.50)
		fnCutsceneBlocker()
		
		--Remove 55 from the party. This includes the combat party.
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "55")
		local iSlot = AC_GetProperty("Character Party Slot", "55")
		if(iSlot > -1) then
			AC_SetProperty("Set Party", iSlot, "Null")
		end
	end
	
--JX-101 joins the party.
elseif(sObjectName == "JX101Joins") then
	--Variables.
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	if(iCompletedBlackSite == 1.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 2.0)
		
		--Add JX-101 to the party.
		fnAddPartyMember("JX-101")
		
		--Give JX-101 her equipment.
		LM_ExecuteScript(gsItemListing, "MkV Pulse Pistol")
		LM_ExecuteScript(gsItemListing, "JX-101's Cuirass")
		AC_PushPartyMember("JX-101")
			ACE_SetProperty("Equip", "Weapon", "MkV Pulse Pistol")
			ACE_SetProperty("Equip", "Armor", "JX-101's Cuirass")
		DL_PopActiveObject()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] What is going on?[SOFTBLOCK] Why aren't there alarms going off?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Movement.
		fnCutsceneMove("Christine", 23.25, 5.50)
		fnCutsceneMove("Christine", 21.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("JX101", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Reporting for duty, ma'am.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Ah, good, right on time.[SOFTBLOCK] How are you legs?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A little tired.[SOFTBLOCK] Why do you ask?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I gave you the shortest and most direct route.[SOFTBLOCK] You humans tend to get tired after physical activity.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] There are some perks to being a Steam Droid despite everything that has happened...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The oxygen levels in the mines are a little low.[SOFTBLOCK] I get tired more easily here.[SOFTBLOCK] I think I'm getting used to it, though.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, I'm ready for anything.[SOFTBLOCK] But -[SOFTBLOCK] is this the detention site?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] No, this is its cover.[SOFTBLOCK] This used to be a tram servicing station, but the golems abandoned it when the mines were depleted.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] They took out everything of use to them.[SOFTBLOCK] We scavenged what they didn't take, years ago.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] To think I've walked past this place a hundred times without knowing the crimes they were committing under our noses...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("JX101", 20.25, 4.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Strike") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "FloorFake", true) ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("JX101", 20.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneFace("JX101", 1, 0)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Once you know where to look, their entrances aren't that hard to find.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Christine...[SOFTBLOCK] Something about this site is off.[SOFTBLOCK] Very off.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Normally we would expect a punitive force to have moved in by now, but none of their alarm systems are firing.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Hmm, I wonder if 55 is disabling their alarms?[SOFTBLOCK] But she didn't mention it...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Don't look a gift horse in the mouth.[SOFTBLOCK] That's a saying the Yanks are fond of.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I assume it means we are not to question good fortune.[SOFTBLOCK] Fine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You and I are the spearhead.[SOFTBLOCK] The tight quarters of these sites prohibits large troop movements.[SOFTBLOCK] Our objective is to clear the area.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Locate high-priority targets and liquidate them.[SOFTBLOCK] Liberate any prisoners.[SOFTBLOCK] Acquire high-value material.[SOFTBLOCK] Are we clear?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] As crystal.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Good.[SOFTBLOCK] Let's go.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move JX-101 onto Christine and fold the party.
		fnCutsceneMove("Christine", 20.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end

--Ending sequence.
elseif(sObjectName == "EndingSequence") then

	--Only fires at 9.0, which is the ending code.
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	if(iCompletedBlackSite ~= 9.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 10.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N", 2.0)
	
	--Spawn 55.
	fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
	
	--Movement.
	fnCutsceneMove("Christine", 23.25, 8.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneMove("JX101", 24.25, 8.50)
	fnCutsceneFace("JX101", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("DroidBSAA", 1, 0)
	fnCutsceneFace("DroidBSAB", -1, 0)
	fnCutsceneFace("DroidBSAC", -1, 0)
	fnCutsceneFace("DroidBSAD", 1, 0)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[VOICE|JX-101] Troops![SOFTBLOCK] Gather round and listen up!") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("DroidBSAA", 22.25, 8.50)
	fnCutsceneMove("DroidBSAB", 25.25, 8.50)
	fnCutsceneMove("DroidBSAC", 24.25, 9.50)
	fnCutsceneFace("DroidBSAC", 0, -1)
	fnCutsceneMove("DroidBSAD", 14.25, 9.50)
	fnCutsceneMove("DroidBSAD", 23.25, 9.50)
	fnCutsceneFace("DroidBSAD", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Mission accomplished, we're pulling out.[SOFTBLOCK] We've got several prisoners in tow, so we'll move in groups of two.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] ...[SOFTBLOCK] Still no alarms or patrols from Regulus City?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] No ma'am.[SOFTBLOCK] A repair golem came by earlier, but left without even seeing any of us.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Well, we won't be looking a gift yank in the mouth,[SOFTBLOCK] right Christine?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Close enough![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Right, I need a volunteer to act as the rear guard.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, I'll do it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Christine...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Get the prisoners out of here.[SOFTBLOCK] If we had to carry one, my fleshy arms would just get tired.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I'll not argue with the practical point, but don't take a risk.[SOFTBLOCK] And if the alarm does sound, you get right out of here.[SOFTBLOCK] Understood?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Completely.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Christine has volunteered to cover the rear.[SOFTBLOCK] Fists, move out!") ]])
	fnCutsceneBlocker()
	
	--Fade to black.
	fnCutsceneWait(90)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Teleport everyone offscreen.
	fnCutsceneTeleport("DroidBSAA", -100.0, -100.0)
	fnCutsceneTeleport("DroidBSAB", -100.0, -100.0)
	fnCutsceneTeleport("DroidBSAC", -100.0, -100.0)
	fnCutsceneTeleport("DroidBSAD", -100.0, -100.0)
	fnCutsceneTeleport("JX101", -100.0, -100.0)
	fnCutsceneTeleport("Christine", 14.25, 19.50)
	fnCutsceneTeleport("55", 25.25, 13.50)
	fnCutsceneFace("Christine", 1, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(90)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|2855] Hold position, I am on my way.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(55)
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(90)
	fnCutsceneBlocker()
	
	--Move 55.
	fnCutsceneMove("55", 19.25, 13.50)
	fnCutsceneMove("55", 16.25, 16.50)
	fnCutsceneMove("55", 16.25, 18.50)
	fnCutsceneMove("55", 15.25, 19.50)
	fnCutsceneFace("55", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, report mission status.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Successful.[SOFTBLOCK] We found some useful blueprints.[SOFTBLOCK] I took a photo of them on my PDU without JX-101 noticing.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Excellent.[SOFTBLOCK] Forward them to Unit 499323.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Already done.[SOFTBLOCK] She said she'd look them over as soon as she can be sure she won't be seen.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And do you believe you can synthesize a power core from them?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's going to be a kitbash, but I think so.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A kitbash?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You wouldn't understand unless you've made custom figurines before.[SOFTBLOCK] It may not be pretty, but it'll work.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll need to head back to Sprocket City first.[SOFTBLOCK] SX-399 can probably tell me what some of these symbols are supposed to mean.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And -[SOFTBLOCK] great job on keeping those alarms at bay, 55![SOFTBLOCK] We took no casualties![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The alarms had already been disabled before I entered the premises.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Excuse me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The alarms had already been disabled before I entered the premises.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Sigh*[SOFTBLOCK] No, 55, what does that mean?[SOFTBLOCK] Why?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not know.[SOFTBLOCK] The alarm system had been rerouted to a disconnected network address.[SOFTBLOCK] It was deliberate, even a complete incompetent would have noticed the error.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Dash it, all we ever get is more questions.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Let us return to SX-399.[SOFTBLOCK] I hope she is -[SOFTBLOCK] pleased.[SOFTBLOCK] To hear the news.") ]])
	fnCutsceneBlocker()
	
	--Remove JX-101 from the party.
	AL_SetProperty("Unfollow Actor Name", "JX101")
	local iSlot = AC_GetProperty("Character Party Slot", "JX-101")
	if(iSlot > -1) then
		AC_SetProperty("Set Party", iSlot, "Null")
	end
	
	--Add 55 to the party.
	fnAddPartyMember("55")
	
	--Move 55 onto Christine and fold the party. Remove JX-101.
	fnCutsceneMove("55", 14.25, 19.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
		
--Can't leave the area until the mission is completed.
elseif(sObjectName == "CantLeaveYet") then
	
	--Variables.
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	if(iCompletedBlackSite == 0.0 or iCompletedBlackSite == 10.0) then return end
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (I can't leave until the mission is complete!)") ]])
	fnCutsceneBlocker()
	
	--Get Christine's position.
	EM_PushEntity("Christine")
		local iX, iY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Move, keep the Y position.
	fnCutsceneMove("Christine", iX / 16.0, 24.50)
	if(iCompletedBlackSite >= 2.0) then
		fnCutsceneMove("JX101", iX / 16.0, 24.50)
	end
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
--Walk back to MinesD.
elseif(sObjectName == "WalkToMines") then
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Walk to the Sprocket City entrance?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

--[Post Decision]
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
	
	--Transition.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("TelluriumMinesD", "FORCEPOS:50.0x5.0x0") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
	
	--Get Christine's position.
	EM_PushEntity("Christine")
		local iX, iY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Move, keep the Y position.
	fnCutsceneMove("Christine", 3.25, iY / 16.0)
	fnCutsceneMove("55", 3.25, iY / 16.0)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

end
