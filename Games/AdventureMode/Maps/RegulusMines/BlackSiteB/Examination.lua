--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "NorthExit") then
	
	--Variables.
	local iBlackSiteKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlackSiteKeycard", "N")
	
	--Can't access it yet.
	if(iBlackSiteKeycard == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: [SOUND|World|AutoDoorFail]Access denied.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] *55, can you hear me?[SOFTBLOCK] We're at a locked door.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|2855] *I can't hack the door open from here, it's not on the network.[SOFTBLOCK] But, there is a keycode you can transmit to override it.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|2855] *Check the terminals on the west side of the site.[SOFTBLOCK] They may be guarded.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] *Affirmative.*") ]])
		fnCutsceneBlocker()
	
	--Level transition.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("BlackSiteC", "FORCEPOS:12.5x24.0x0")
	end
	
elseif(sObjectName == "SouthExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("BlackSiteA", "FORCEPOS:20.0x4.0x0")
	
--[Examinables]
elseif(sObjectName == "WreckedTerminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A very old Steam Droid computer, not receiving power.[SOFTBLOCK] It's doubtful it'd work even if it was powered.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "OneWayMirror") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A one way mirror.[SOFTBLOCK] Unlike a camera, such a mirror would not record the goings-on of the room...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A video displaying sheep, dogs, and raijus playing together.[SOFTBLOCK] Was this being used for recreation?)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
	
	--Variables.
	local iBlackSiteKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlackSiteKeycard", "N")
	
	--First time.
	if(iBlackSiteKeycard == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iBlackSiteKeycard", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Hmmm, yep...[SOFTBLOCK] There's an override code in the hard drive.[SOFTBLOCK] [SOUND|World|Keycard]I downloaded it to my PDU.)") ]])
		fnCutsceneBlocker()
	
	--Repeats.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (There's nothing else on the terminal except shipping manifests...)") ]])
		fnCutsceneBlocker()
	end
	
elseif(sObjectName == "TerminalC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (The terminal is used to monitor Steam Droid recharging.[SOFTBLOCK] The pressure rates are far too high...[SOFTBLOCK] the prisoner must have been in extreme pain...)") ]])
	fnCutsceneBlocker()
		
elseif(sObjectName == "TerminalD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (The terminal is used to monitor Steam Droid recharging.[SOFTBLOCK] There's barely any pressure allocated, so the prisoner would constantly feel weak and disoriented...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Interrogation logs.[SOFTBLOCK] The Steam Droid prisoners gave all kinds of information, some false, some true.[SOFTBLOCK] The interrogator concluded that pain was producing unreliable testimony, but was ordered to keep inflicting it...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PartsShelf") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Steam Droid replacement parts.[SOFTBLOCK] These are relatively new, and must have been fabricated recently.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "ElevatorShaft") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (An empty elevator shaft.[SOFTBLOCK] It goes down, not up.[SOFTBLOCK] I think I can see garbage at the bottom of it...[SOFTBLOCK] No...[SOFTBLOCK] Those are retired robots down there!)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "Vats") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Storage vats.[SOFTBLOCK] Long disused, it probably doesn't contain anything.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "VatsLeaking") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Storage vats.[SOFTBLOCK] It burst and spilled lubricant everywhere.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Oilmaker") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (An oilmaker.[SOFTBLOCK] Unflavoured, cold oil is sitting in the container.)") ]])
	fnCutsceneBlocker()

--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end