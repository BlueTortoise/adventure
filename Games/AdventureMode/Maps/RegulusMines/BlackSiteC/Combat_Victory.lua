--[Combat Victory]
--The party won!

--Music change.
AudioManager_PlayMusic("EquinoxTheme")

--[Movement]
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()
		
--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Creature:[VOICE|Golem] Don't feel bad.[SOFTBLOCK] I don't blame you.[SOFTBLOCK] You did the right thing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ..?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Creature:[VOICE|Golem] Now.[SOFTBLOCK] Leave.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
		
--Prisoner moves back.
fnCutsceneMoveFace("Monstrosity", 24.25, 4.50, 0, 1, 0.20)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
		
--Open the door.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneInstruction([[ AL_SetProperty("Close Door", "MonstrosityDoor") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()

--Variables.
local iIsJX101Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsJX101Following", "N")
local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")

--If JX-101 is present:
if(iIsJX101Following == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] We didn't even hurt it.[SOFTBLOCK] It's like we were just annoying it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess we should leave it alone...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Another of the administration's crimes.[SOFTBLOCK] Unforgivable...") ]])
	fnCutsceneBlocker()

--55 is present:
elseif(iIs55Following == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The prisoner does not seem interested in rescue.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Nothing for us in there, then.[SOFTBLOCK] Let's leave it be.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Shame.[SOFTBLOCK] It was a Lord Unit, once.[SOFTBLOCK] Now it is just worthless flesh.") ]])
	fnCutsceneBlocker()
end