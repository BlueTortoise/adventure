--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "SouthExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("BlackSiteB", "FORCEPOS:22.5x16.0x0")
	
elseif(sObjectName == "Ladder") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("BlackSiteD", "FORCEPOS:20.0x8.0x0")
	
--[Examinables]
elseif(sObjectName == "SecurityDoor") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (We don't need to go into the security station.[SOFTBLOCK] Let's leave them to argue.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "RetiredGolem") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I'd ask the PDU for an autopsy, but there's barely anything left of her.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (But it looks like the corrosive material came from...[SOFTBLOCK] inside the golem?") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "DoorLockedSW") then

	--Variables.
	local iSawPartsScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPartsScene", "N")
	
	--Not open.
	if(iSawPartsScene == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Access denied.") ]])
		fnCutsceneBlocker()
	
	--Open.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_SetProperty("Open Door", "DoorLockedSW")
	end
	
elseif(sObjectName == "DoorLockedNE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Access denied.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Monstrosity") then
	
	--Variables.
	local iKilledMonstrosity= VM_GetVar("Root/Variables/Chapter5/Scenes/iKilledMonstrosity", "N")
	
	--First time.
	if(iKilledMonstrosity == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iKilledMonstrosity", "N", 1.0)
		
		--Open the door.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "MonstrosityDoor") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Prisoner moves up.
		fnCutsceneMove("Monstrosity", 24.25, 5.50, 0.20)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --It transforms.
        fnCutsceneSetFrame("Monstrosity", "Mutant")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
        fnCutsceneSetFrame("Monstrosity", "Null")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
        fnCutsceneSetFrame("Monstrosity", "Mutant")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
        fnCutsceneSetFrame("Monstrosity", "Null")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
        fnCutsceneSetFrame("Monstrosity", "Mutant")
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] What is that thing!?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Creature:[VOICE|Golem] Circle, circle, circle.[SOFTBLOCK] Inside outside we are neither.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] K-[SOFTBLOCK]kill it![SOFTBLOCK] Right now!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Battle.
		fnCutsceneInstruction([[ AC_SetProperty("Next Music Override", "RottenTheme", 0.0000) ]])
		fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
		fnCutsceneInstruction([[ AC_SetProperty("Unlosable", true) ]])
		fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Maps/RegulusMines/BlackSiteC/Combat_Victory.lua") ]])
		fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter5/TechMonstrosity.lua", 0) ]])
		fnCutsceneBlocker()

	--Repeats.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I would...[SOFTBLOCK] rather not go in there again...)") ]])
		fnCutsceneBlocker()
	
	end

--[Other]
--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end