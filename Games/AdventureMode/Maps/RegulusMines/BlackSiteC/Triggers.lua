--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Sample object.
if(sObjectName == "BackUp") then

	--Variables.
	local iSawPartsScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPartsScene", "N")
	if(iSawPartsScene == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPartsScene", "N", 1.0)

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[VOICE|JX-101] *Christine, wait.[SOFTBLOCK] That's a security checkpoint.*") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 23.25, 22.50, 1.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneMove("JX101",     22.25, 22.50, 1.50)
		fnCutsceneFace("JX101",     0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Drat, I don't think there's a way around.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Maybe I can hack this door open...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You're a network security expert too?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, but this PDU I found had a lot of hacking programs on it.[SOFTBLOCK] Let's see what it can do.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *55?*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|2855] *Underway.[SOFTBLOCK] Hold your position.*") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move the camera.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Max Move Speed", 2.0)
			CameraEvent_SetProperty("Focus Position", (27.25 * gciSizePerTile), (19.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "GolemLord", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: Hrmph, Subject D-15 is permanently offline.[SOFTBLOCK] Pity.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave: I'm sorry, Lord Unit.[SOFTBLOCK] It seems her core sprung a leak.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: And the replacement parts?[SOFTBLOCK] Why did you not repair the subject?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave: Err, cores?[SOFTBLOCK] I don't think those were in the schematics.[SOFTBLOCK] Can you replace those?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: Tch, possibly.[SOFTBLOCK] We'll need to check the ruins again, and more thoroughly this time, and find a steam core schematic.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave: [SOFTBLOCK]Uh, Lord Golem, are you seeing this odd network traffic?[SOFTBLOCK] The doors - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: Don't change the subject![SOFTBLOCK] I have not yet decided your punishment for failing to repair a test subject![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave: But why are we bothering to repair the Steam Droids?[SOFTBLOCK] They'll just break again...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: [SOFTBLOCK][SOUND|World|Thump][SOFTBLOCK]It is not your place to question orders![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: But since you persist in asking...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: Prisoners do not grow like Empherean Fungus.[SOFTBLOCK] If we run out of Steam Droids to experiment on, well, we'd have to use other units.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: Disobedient units.[SOFTBLOCK] Would you like to share Unit 688231's fate?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave: No, Lord Golem![SOFTBLOCK] Please![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: Then you will watch your tongue...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: Why bother explaining this to you?[SOFTBLOCK] You'll forget it all when you are reprogrammed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: Honestly, I can't wait to wash my hands of this dreadful business.[SOFTBLOCK] The Cause of Science demands so much of us.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Slave: Yes, Lord Golem.[SOFTBLOCK] It demands so much.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lord: That is the first intelligent thing you've said all day.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move the camera.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Max Move Speed", 2.0)
			CameraEvent_SetProperty("Focus Actor Name", "Christine")
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I'd love to scrap both of them...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Even the Slave Unit?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Pathetic collaborator, show some damn spine.[SOFTBLOCK] If they weren't all such cowards, these atrocities would not come to pass.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] The golems are all responsible.[SOFTBLOCK] Lord or Slave, their hands are stained with blood and lubricant.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Grrrr...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[VOICE|2855] *I've bypassed the security circuit.[SOFTBLOCK] The door should be open.*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The door is open.[SOFTBLOCK] Let's get going.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutsceneMove("JX101", 23.25, 22.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	--Otherwise:
	else
	
		--Get Christine's position.
		EM_PushEntity("Christine")
			local iX, iY = TA_GetProperty("Position")
		DL_PopActiveObject()
	
		--Variables.
		local iIsJX101Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsJX101Following", "N")
		local iIs55Following    = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Better not let the security units see us...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Move, keep the Y position.
		fnCutsceneMove("Christine", 23.25, iY / gciSizePerTile)
		if(iIsJX101Following == 1.0) then
			fnCutsceneMove("JX101", 23.25, iY / gciSizePerTile)
		end
		if(iIs55Following == 1.0) then
			fnCutsceneMove("55", 23.25, iY / gciSizePerTile)
		end
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	end
end
