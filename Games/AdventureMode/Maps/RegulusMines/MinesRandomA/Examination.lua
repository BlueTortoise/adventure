--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Subscript]
--Run this script. If the variable gbCaughtScript is thrown to true, stop there.
gbCaughtScript = false
LM_ExecuteScript(gsRandomExaminationHandler, sObjectName)
if(gbCaughtScript == true) then
    gbCaughtScript = nil
    return
end

--Clear.
gbCaughtScript = nil

--[Examinables]
if(sObjectName == "Smelters") then

    --Variables.
    local iMineASmelters = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineASmelters", "N")
    if(iMineASmelters == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineASmelters", "N", 1.0)
        
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm, are these electrical smelters?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|PDU] PDU, give me a local area scan.[SOFTBLOCK] What ore is this?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Scanning...[SOFTBLOCK] Done![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Ore is a rare mineral known as abrissite.[SOFTBLOCK] It is a particularly good source of Aluminum, Cobalt, Tantalum, and Yttrium.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But these are primitive smelters, and -[SOFTBLOCK] shovels?[SOFTBLOCK] Were they hand-operating these?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Abrissite occurs in small nodules, no more than a few meters across.[SOFTBLOCK] It is extremely highly concentrated but in small areas.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This mining facility was decommissioned.[SOFTBLOCK] What we see around us is probably lower-grade ores or slag.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] So they just left it all down here?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Carrying materials to the surface costs a significant amount of energy and time.[SOFTBLOCK] Anything not worth the cost was abandoned.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] [SOUND|World|TakeItem]Maybe I'll just take some of this leftover abrissite...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] May as well look around.[SOFTBLOCK] Maybe they left something else we can use.") ]])
        fnCutsceneBlocker()
    
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Electrical smelters.[SOFTBLOCK] The heating coils were stripped out, but the heavy casing is still here.)") ]])
        fnCutsceneBlocker()
    
    end
    
elseif(sObjectName == "Pod") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Defragmentation pods used by the miners.[SOFTBLOCK] The electrical parts were stripped out.)") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "Fabricator") then
    
    --Variables.
    local iMineAFabricators = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineAFabricators", "N")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineAFabricators", "N", 1.0)
    
    if(iMineAFabricators == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A fabrication bench.[SOFTBLOCK] The built-in toolbox was stripped.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|TakeItem](Some broken tools were left behind.[SOFTBLOCK] One robot's trash is another's treasure!)") ]])
        LM_ExecuteScript(gsItemListing, "Bent Tools")
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A fabrication bench.[SOFTBLOCK] The built-in toolbox was stripped.)") ]])
    end
    
elseif(sObjectName == "Crates") then
    
    --Variables.
    local iMineACrates = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineACrates", "N")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineACrates", "N", 1.0)
    
    if(iMineACrates == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Storage crates.[SOFTBLOCK] Mostly contains damaged parts not worth carrying.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|TakeItem](I'm sure we could find a use for some of this...)") ]])
        LM_ExecuteScript(gsItemListing, "Assorted Parts")
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Storage crates.[SOFTBLOCK] Mostly contains damaged parts not worth carrying.)") ]])
    end
    
elseif(sObjectName == "Defragment") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks like the Lord Golem's defragmentation chamber and attached terminal.[SOFTBLOCK] All the useful electrical parts were stripped out.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminal") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A terminal left behind. The processor, power supply -[SOFTBLOCK] even the cooling fans were stripped out.[SOFTBLOCK] All that's left is the casing.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FakeExit") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (This door isn't receiving power, and the deadbolts have been thrown.[SOFTBLOCK] We're not getting it open without a blasting charge.)") ]])
    fnCutsceneBlocker()

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end