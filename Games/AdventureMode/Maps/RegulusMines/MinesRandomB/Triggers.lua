--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

    --Variables.
    local iMineBTalkedToDroids = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBTalkedToDroids", "N")
    if(iMineBTalkedToDroids == 1.0) then return end
    
    --Other variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iHasSteamDroidForm = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBTalkedToDroids", "N", 1.0)

    --Movement.
    fnCutsceneMove("Christine", 15.25, 15.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("55", 14.25, 15.50)
    fnCutsceneFace("55", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Droids turn to face you.
    fnCutsceneFace("DroidLeader", -1, 0)
    fnCutsceneFace("DroidA", -1, 0)
    fnCutsceneFace("DroidB", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    
    
    if(sChristineForm == "SteamDroid" or iHasSteamDroidForm == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] What the heck -[SOFTBLOCK] oh![SOFTBLOCK] Christine![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] You know my name?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Your reputation preceeds you.[SOFTBLOCK] Fist of the Future, for life![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] And you must be 55, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Phew![SOFTBLOCK] I was worried for a second.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It is uncommon to encounter allies in the mines.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Name's IN-12, and this is my squad.[SOFTBLOCK] You're pretty well known down here.[SOFTBLOCK] Care to lend a hand?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Just take a look over that chasm.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Human") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Oh goody, more creeps.[SOFTBLOCK] Will this day never end?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] And it's a human and one of those doll girls?[SOFTBLOCK] Good.[SOFTBLOCK] I love it![SOFTBLOCK] Throw more at me![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is this a bad time?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Okay, wasn't expecting that.[SOFTBLOCK] Identify yourselves.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My name's - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Shut up, X.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I'm Y, and this is X.[SOFTBLOCK] You don't need to know our real names.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[SOFTBLOCK] maybe I should get 55 a copy to read...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess that will have to do.[SOFTBLOCK] We're here to help.[SOFTBLOCK] Are you in trouble?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, you could say that.[SOFTBLOCK] Since you haven't opened fire yet, I assume you're on the level?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Quite.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] And your Command Unit friend?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] A maverick.[SOFTBLOCK] Enemy of Regulus City.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's an expression, 'Y'.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How can we help you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Name's IN-12, and this is my squad.[SOFTBLOCK] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Golem") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Oh goody, more creeps.[SOFTBLOCK] Will this day never end?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Though one of them is a Command Unit.[SOFTBLOCK] Don't usually see them amongs the crowds.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Wait![SOFTBLOCK] We're friends![SOFTBLOCK] Not enemies![SOFTBLOCK] Don't shoot![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Well well well, they can talk coherently.[SOFTBLOCK] Interesting.[SOFTBLOCK] Identify yourselves.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My name's - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Shut up, X.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I'm Y, and this is X.[SOFTBLOCK] You don't need to know our real names.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[SOFTBLOCK] maybe I should get 55 a copy to read...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess that will have to do.[SOFTBLOCK] We're here to help.[SOFTBLOCK] Are you in trouble?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, you could say that.[SOFTBLOCK] I take it you're not with Regulus City?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We've gone maverick.[SOFTBLOCK] Fight the power, sister.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's an expression, 'Y'.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How can we help you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Name's IN-12, and this is my squad.[SOFTBLOCK] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Darkmatter") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Now this is a new one.[SOFTBLOCK] Don't usually see Darkmatters down here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hello![SOFTBLOCK] Pleased to meet you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Talking Darkmatters, no less.[SOFTBLOCK] What the heck is going on in the mines lately?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] You, Darkmatter.[SOFTBLOCK] Identify yourself.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My name's - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Shut up, X.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I'm Y, and this is X.[SOFTBLOCK] You don't need to know our real names.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[SOFTBLOCK] maybe I should get 55 a copy to read...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess that will have to do.[SOFTBLOCK] We're here to help.[SOFTBLOCK] Are you in trouble?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, you could say that.[SOFTBLOCK] Is your Command Unit friend on the level?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She's a maverick.[SOFTBLOCK] She's on your side.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's an expression, 'Y'.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How can we help you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Name's IN-12, and this is my squad.[SOFTBLOCK] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Eldritch") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Oh lovely, one of those 'things'.[SOFTBLOCK] Maybe we can toss it off the edge when it rushes us.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] No, no![SOFTBLOCK] We're here to help![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Uh, ignore my appearance.[SOFTBLOCK] I'm not like them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Oh yeah?[SOFTBLOCK] Prove it.[SOFTBLOCK] Identify yourself.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My name's - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Shut up, X.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I'm Y, and this is X.[SOFTBLOCK] You don't need to know our real names.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[SOFTBLOCK] maybe I should get 55 a copy to read...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess that will have to do.[SOFTBLOCK] We're here to help.[SOFTBLOCK] Are you in trouble?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Why should I tell you anything?[SOFTBLOCK] Command Units and -[SOFTBLOCK] whatever you are -[SOFTBLOCK] normally attack us on sight.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She's a maverick, and I'm -[SOFTBLOCK] awake.[SOFTBLOCK] We're on your side.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's an expression, 'Y'.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How can we help you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Name's IN-12, and this is my squad.[SOFTBLOCK] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Electrosprite") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Uhh, one of those electricty girls I've been hearing about...[SOFTBLOCK] Are the rumours true?[SOFTBLOCK] Do you suck power out of innocent robots?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] No, no![SOFTBLOCK] We're here to help![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] And you can call us Electrosprites if you want.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Whatever.[SOFTBLOCK] What's your name?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Call me - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Shut up, X.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I'm Y, and this is X.[SOFTBLOCK] You don't need to know our real names.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[SOFTBLOCK] maybe I should get 55 a copy to read...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess that will have to do.[SOFTBLOCK] We're here to help.[SOFTBLOCK] Are you in trouble?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Why should I tell you anything?[SOFTBLOCK] Command Units shoot Steam Droids on sight.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She's a maverick.[SOFTBLOCK] We're on your side.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's an expression, 'Y'.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How can we help you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Name's IN-12, and this is my squad.[SOFTBLOCK] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "LatexDrone") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Command Unit behind us![SOFTBLOCK] We're being flanked![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Wait![SOFTBLOCK] Don't shoot![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] ...[SOFTBLOCK] The bondage drone talks first?[SOFTBLOCK] What the hey?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] You, bondage drone.[SOFTBLOCK] Identify yourself.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Call me - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Shut up, X.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I'm Y, and this is X.[SOFTBLOCK] You don't need to know our real names.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[SOFTBLOCK] maybe I should get 55 a copy to read...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I guess that will have to do.[SOFTBLOCK] We're here to help.[SOFTBLOCK] Are you in trouble?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Why should I tell you anything?[SOFTBLOCK] You're with Regulus City aren't you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No, we're mavericks! We're on your side![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's an expression, 'Y'.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How can we help you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Name's IN-12, and this is my squad.[SOFTBLOCK] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    end
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Camera moves.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (16.25 * gciSizePerTile), (33.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Look at all those wonderful crates.[SOFTBLOCK] And in those wonderful crates are doubly-wonderful supplies we're here to retrieve.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Problem is there's a bunch of whack jobs between us and those crates.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] The golems over there -[SOFTBLOCK] are they okay?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Beats me.[SOFTBLOCK] They gibber to themselves about something or other.[SOFTBLOCK] They rush you if you try to get close.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Yelling stuff about how 'big it is' and 'we need to hide!' and whatnot.[SOFTBLOCK] They've clearly lost it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] You have guns.[SOFTBLOCK] Shoot a path to the supplies.[SOFTBLOCK] You do not need our help.[BLOCK][CLEAR]") ]])
    if(sChristineForm == "SteamDroid" or iHasSteamDroidForm == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Would if I could, but can't so I won't.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why not?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] We spent too many mags on the way here, and now we're almost out. We don't have enough to clear a path.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Heck, with what we have we'd have a rough time just getting back to the nearest settlement.[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] You, 'Y', would fit in very well with my crew.[SOFTBLOCK] Physical appearance notwithstanding.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] And you are correct.[SOFTBLOCK] We do have guns.[SOFTBLOCK] But we can't shoot a path.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why not?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] See, there's this thing that guns need.[SOFTBLOCK] It's called 'ammunition'.[SOFTBLOCK] Guns don't work without it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No need to be sarcastic.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Actually, some lasrifle designs do not need ammunition.[SOFTBLOCK] It is quite possible to be unfamiliar with the term.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Well, I [SOFTBLOCK]*was*[SOFTBLOCK] being sarcastic, but 'Y' is right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Anyway, the ammunition we need?[SOFTBLOCK] It's in those crates.[SOFTBLOCK] We spent too much on our way here.[BLOCK][CLEAR]") ]])
    end
    
    --Common.
    fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] If we could get to those crates we could load up and head back to headquarters.[SOFTBLOCK] But we can't, so we're stuck here staring at them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay![SOFTBLOCK] So, we'll go get the ammo from the crates for you![BLOCK][CLEAR]") ]])
    
    if(sChristineForm == "SteamDroid" or iHasSteamDroidForm == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] I expected no less![SOFTBLOCK] You're every bit the hero JX-101 said you were.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Listen, if you can get the ammo for us, I'm sure she'll have something for you back at headquarters.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We're on it.[SOFTBLOCK] Sit tight.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Not like we were doing much otherwise.") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Huh.[SOFTBLOCK] Well, if you've survived in the mines this far then you've got to be tough.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] If you die, don't expect us to retrieve your corpses.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Very well.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We'll be back before you know it.[SOFTBLOCK] You just sit tight.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "IN-12:[E|Neutral] Not like we were doing much otherwise.[SOFTBLOCK] Get this done and I might just have a reward for you.") ]])
    end
    fnCutsceneBlocker()

    --Droids change facing.
    fnCutsceneFace("DroidLeader", 1, 0)
    fnCutsceneFace("DroidA", 1, 0)
    fnCutsceneFace("DroidB", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneMove("55", 15.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

end
