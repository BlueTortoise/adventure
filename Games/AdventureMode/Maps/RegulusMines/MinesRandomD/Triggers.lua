--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then
    
    --Variables.
    local iMineDMet = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDMet", "N")
    if(iMineDMet == 1.0) then return end

    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDMet", "N", 1.0)
    
    --Events.
    fnCutsceneMove("Christine", 36.25, 21.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("55", 37.25, 21.50)
    fnCutsceneFace("55", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneFace("PP-81", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Please, please, form a nice line.[SOFTBLOCK] One at a time, thank you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] I'm afraid I am not doing autographs at the moment, due to -[SOFTBLOCK] extenuating circumstances.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are circumstances ever [SOFTBLOCK]*not*[SOFTBLOCK] extenuating?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] No, I think that is all circumstances ever do.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And who are you, exactly?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Of course you don't recognize me.[SOFTBLOCK] This is what I get for shying away from the public eye.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PP-81:[E|Neutral] I am PP-81, the world-famous artist.[SOFTBLOCK] Surely you've heard of me now?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (No.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, of course![SOFTBLOCK] You did...[SOFTBLOCK] you know...[SOFTBLOCK] and...[SOFTBLOCK] the thing...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PP-81:[E|Neutral] I did, actually.[SOFTBLOCK] It's nice to meet a fan who knows how to properly show appreciation.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PP-81:[E|Neutral] Unlike the other fans who simply cannot take a hint.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] There's nobody else here...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Wait do you mean the monsters?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PP-81:[E|Neutral] Hardly a proper way to refer to them.[SOFTBLOCK] Rude, yes, but monstrous?[SOFTBLOCK] That's a stretch.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Them trying to gnaw off your arms is a stretch?)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, is there a reason we are dealing with this unit?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I think she needs our help.[SOFTBLOCK] Do you, PP-81?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PP-81:[E|Neutral] Kind of you to offer, but no.[SOFTBLOCK] Be on your way.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PP-81:[E|Neutral] Well I suppose there is one thing you could help me with.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PP-81:[E|Neutral] I was attempting a new mode of rendering.[SOFTBLOCK] I call it Temporal Cubism -[SOFTBLOCK] an attempt to render a scene from all four sides of a cube.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (But a cube has six sides...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PP-81:[E|Neutral] However, some over-eager fans of mine have made a real mess of my studio.[SOFTBLOCK] Such is the inevitable hunt for souveniers, I suppose.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "PP-81:[E|Neutral] They always tire themselves out eventually, but if you could recover my paints from my studio I'd be most grateful.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sure.[SOFTBLOCK] I think we can manage that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] *You think she has a reward for us?*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *Call it a hunch.*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We'll be right back with your art supplies.[SOFTBLOCK] Don't worry.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Fold the party up.
    fnCutsceneMove("55", 36.25, 21.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

end
