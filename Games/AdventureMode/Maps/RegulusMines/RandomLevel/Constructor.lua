--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RandomLevel"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "RegulusTense")
	
	--Construct the level based on the path.
	AL_SetProperty("Name", sLevelName)
	AL_SetProperty("Examine Script", fnResolvePath() .. "Examination.lua")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Map Setup
	fnResolveMapLocation("TelluriumMinesB")
	
	--[Entrance]
	local iEntranceX, iEntranceY = AdlevGenerator_GetProperty("Entrance Position")
	iEntranceY = iEntranceY - 1
	AL_CreateObject("Examinable", "Entrance", iEntranceX * gciSizePerTile, iEntranceY * gciSizePerTile, gciSizePerTile, gciSizePerTile)
	
	--[Exit]
	local iExitX, iExitY = AdlevGenerator_GetProperty("Exit Position")
	iExitY = iExitY + 1
	AL_CreateObject("Examinable", "Exit", iExitX * gciSizePerTile, iExitY * gciSizePerTile, gciSizePerTile, gciSizePerTile)
	
	--[Difficulty Script]
	--This script does the work of selecting enemy spawns and treasure generation.
	LM_ExecuteScript(fnResolvePath() .. "DifficultyHandler.lua")
	
	--Spawn the enemies we generated.
	fnStandardEnemyPulse()
    
    --Activate lights.
    AL_SetProperty("Activate Lights")
    AL_SetProperty("Set Ambient Light", 0.25, 0.25, 0.25, 1.0)
    local iMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N")
    --[[
    if(iMinesFloor >= 30 and iMinesFloor < 40) then
        AL_SetProperty("Set Ambient Light", 0.10, 0.40, 0.10, 1.0)
    end]]

    --[Statistics]
    --Store lowest floor.
    local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N")
    local iLowestMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowestMinesFloor", "N")
    if(iCurrentMinesFloor > iLowestMinesFloor) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iLowestMinesFloor", "N", iCurrentMinesFloor)
    end
    
    --Increment floors generated.
    local iTotalFloorsGenerated = VM_GetVar("Root/Variables/Chapter5/Scenes/iTotalFloorsGenerated", "N")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iTotalFloorsGenerated", "N", iTotalFloorsGenerated + 1)
    
    --Store the last seed.
    local iLastSeed = AdlevGenerator_GetProperty("Last Seed")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iLastMinesSeed", "N", iLastSeed)

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.

end
