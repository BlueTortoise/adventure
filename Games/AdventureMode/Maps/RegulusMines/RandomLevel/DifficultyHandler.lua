--[Difficulty Handler]
--This is the subscript for the random level generator that handles level difficulty. It also handles
-- spawning of enemies and treasure.

--[Variables]
--We need to know the entry point because enemies within 6 tiles of it don't spawn. This is just a QoL feature.
local iEntranceX, iEntranceY = AdlevGenerator_GetProperty("Entrance Position")

--We also need to know what floor we're on. This determines enemy density and treasure quality.
-- Note that this describes the maximum number of patrols, not enemies. Enemies who are following one
-- another count as the same enemy for our purposes.
local iMaxEnemies = 0
local iMaxTreasures = 0
local iEnemyDoubleThreshold = 0
local iEnemyTripleThreshold = 0
local iMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N")

--[Enemy Density]
if(iMinesFloor < 5) then
	iMaxEnemies = 3
	iMaxTreasures = 2
    iEnemyDoubleThreshold = 90
    iEnemyTripleThreshold = 101
elseif(iMinesFloor < 10) then
	iMaxEnemies = 3
	iMaxTreasures = 2
    iEnemyDoubleThreshold = 70
    iEnemyTripleThreshold = 95
elseif(iMinesFloor < 15) then
	iMaxEnemies = 4
	iMaxTreasures = 3
    iEnemyDoubleThreshold = 70
    iEnemyTripleThreshold = 90
elseif(iMinesFloor < 20) then
	iMaxEnemies = 5
	iMaxTreasures = 3
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 25) then
	iMaxEnemies = 6
	iMaxTreasures = 4
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 30) then
	--iMaxEnemies = 7
	iMaxTreasures = 4
    --iEnemyDoubleThreshold = 30
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 35) then
	--iMaxEnemies = 7
	iMaxTreasures = 5
    --iEnemyDoubleThreshold = 27
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 40) then
	--iMaxEnemies = 8
	iMaxTreasures = 5
    --iEnemyDoubleThreshold = 27
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 45) then
	--iMaxEnemies = 9
	iMaxTreasures = 6
    --iEnemyDoubleThreshold = 27
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
else
	--iMaxEnemies = 1000
	iMaxTreasures = 1000
    --iEnemyDoubleThreshold = 30
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80

end

--[Enemy Paths]
--Get how many paths there are. Store that data in a table for the enemies to read.
local iaPathData = {}
local iPathsTotal = AdlevGenerator_GetProperty("Enemy Paths Total")
for i = 0, iPathsTotal - 1, 1 do
	
	--Store.
	iaPathData[i] = {}
	iaPathData[i].iPathLength = AdlevGenerator_GetProperty("Enemy Path Length", i)
	iaPathData[i].iPathData = {}
	
	--Iterate across the path.
	for p = 0, iaPathData[i].iPathLength - 1, 1 do
		
		--Generate a path name.
		local sName = string.format("N%02i%02i", i, p)
		
		--Get position and place the node.
		local iPathX, iPathY = AdlevGenerator_GetProperty("Enemy Path Position", i, p)
		AL_CreateObject("Path Node", sName, iPathX * gciSizePerTile, iPathY * gciSizePerTile, gciSizePerTile, gciSizePerTile)
		
		--Store it.
		iaPathData[i].iPathData[p] = {iPathX, iPathY}
	end
end

--[Enemy Spawns]
--Roll a theme for the enemies.
local sTheme = "Robots"
local iRoll = LM_GetRandomNumber(0, 99)
if(iRoll < 30) then
    
--Eldritch critters.
elseif(iRoll < 60) then
    sTheme = "Eldritch"

--Maverick intelligent robots.
elseif(iRoll < 70) then
    sTheme = "Mavericks"

--Mixed. Mixes all previous types.
else
    sTheme = "Mixed"
end

--Get how many spawn locations there are.
local iSpawnsTotal = AdlevGenerator_GetProperty("Enemy Spawns Total")
for i = 0, iSpawnsTotal - 1, 1 do
	
	--If we pass the spawn cap, stop.
	if(i >= iMaxEnemies) then break end
	
	--Get the starting position.
	local iSpawnX, iSpawnY = AdlevGenerator_GetProperty("Enemy Spawn Position", i)
	
	--Check distance to player start. If it's really close to the player start, flip it and spawn it
	-- at the far end of the patrol path.
	local fXDistSqr = (iEntranceX - iSpawnX) * (iEntranceX - iSpawnX)
	local fYDistSqr = (iEntranceY - iSpawnY) * (iEntranceY - iSpawnY)
	local fDistance = math.sqrt(fXDistSqr + fYDistSqr)
	
	--Normal case:
    local sPatrolPath = ""
	if(fDistance >= 13.0) then
	
		--Iterate across the path. Build the patrol string.
		for p = 0, iaPathData[i].iPathLength - 1, 1 do
			local sNextNode = string.format("N%02i%02i", i, p)
			sPatrolPath = sPatrolPath .. sNextNode .. "|"
		end
		
		--Build it backwards so the enemy paths back.
		for p = iaPathData[i].iPathLength - 1, 0, -1 do
			local sNextNode = string.format("N%02i%02i", i, p)
			sPatrolPath = sPatrolPath .. sNextNode .. "|"
		end
	
	
	--Too close. Run the patrol path backwards and use the last point as the spawn.
	else
		
		--Modify the spawn position.
		iSpawnX = iaPathData[i].iPathData[iaPathData[i].iPathLength - 1][1]
		iSpawnY = iaPathData[i].iPathData[iaPathData[i].iPathLength - 1][2]
		
		--Start at the end of the path.
		for p = iaPathData[i].iPathLength - 1, 0, -1 do
			local sNextNode = string.format("N%02i%02i", i, p)
			sPatrolPath = sPatrolPath .. sNextNode .. "|"
		end
		
		--Now run it forwards again.
		for p = 0, iaPathData[i].iPathLength - 1, 1 do
			local sNextNode = string.format("N%02i%02i", i, p)
			sPatrolPath = sPatrolPath .. sNextNode .. "|"
		end
	
    end

    --[Determine Creation by Theme]
    local sParty = ""
    local sAppearance = ""
    local iSubRoll = LM_GetRandomNumber(0, 99)
    if(sTheme == "Robots") then
        if(iSubRoll < 60) then
            sParty = "ZMines_Scraprat"
            sAppearance = "Scraprat"
        else
            sParty = "ZMines_WreckedBot"
            sAppearance = "SecurityBotBroke"
        end
    elseif(sTheme == "Eldritch") then
        if(iSubRoll < 30) then
            sParty = "ZMines_HorribleLittleMan"
            sAppearance = "Horrible"
        elseif(iSubRoll < 80) then
            sParty = "ZMines_Skullcrawler"
            sAppearance = "SkullCrawler"
        else
            sParty = "ZMines_VoidRift"
            sAppearance = "VoidRift"
        end
    elseif(sTheme == "Mavericks") then
        if(iSubRoll < 10) then
            sParty = "ZMines_Lord"
            sAppearance = "GolemLordA"
        elseif(iSubRoll < 40) then
            sParty = "ZMines_Golem"
            sAppearance = "GolemSlave"
        else
            sParty = "ZMines_LatexDrone"
            sAppearance = "LatexDrone"
        end
    
    elseif(sTheme == "Mixed") then
        if(iSubRoll < 30) then
            sParty = "ZMines_Scraprat"
            sAppearance = "Scraprat"
        elseif(iSubRoll < 40) then
            sParty = "ZMines_WreckedBot"
            sAppearance = "SecurityBotBroke"
        elseif(iSubRoll < 50) then
            sParty = "ZMines_HorribleLittleMan"
            sAppearance = "Horrible"
        elseif(iSubRoll < 60) then
            sParty = "ZMines_Skullcrawler"
            sAppearance = "SkullCrawler"
        elseif(iSubRoll < 70) then
            sParty = "ZMines_VoidRift"
            sAppearance = "VoidRift"
        elseif(iSubRoll < 80) then
            sParty = "ZMines_Lord"
            sAppearance = "GolemLordA"
        elseif(iSubRoll < 90) then
            sParty = "ZMines_Golem"
            sAppearance = "GolemSlave"
        else
            sParty = "ZMines_LatexDrone"
            sAppearance = "LatexDrone"
        end
    
    end

    --Creation call.
    AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "A", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, sParty, sAppearance, "Defeat_BackToSave", 0, sPatrolPath, "Null")
    
    --[Additional Enemies]
    --Some enemies may spawn with followers. Follower count increases with difficulty.
    local iFollowerRoll = LM_GetRandomNumber(0, 99)
    
    --Scraprats always spawn with 2 followers, and can spawn with an additional follower.
    if(sAppearance == "Scraprat") then
        AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "B", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, sParty, sAppearance, "Defeat_BackToSave", 0, "Null", "ScriptEnemy" .. i .. "A")
        AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "C", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, sParty, sAppearance, "Defeat_BackToSave", 0, "Null", "ScriptEnemy" .. i .. "B")
        if(iFollowerRoll >= 75) then
            AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "D", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, sParty, sAppearance, "Defeat_BackToSave", 0, "Null", "ScriptEnemy" .. i .. "C")
        end
    
    --Wrecked Security Bots have a 1/3rd chance to have a follower.
    elseif(sAppearance == "SecurityBotBroke") then
        if(iFollowerRoll >= 66) then
            AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "B", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, sParty, sAppearance, "Defeat_BackToSave", 0, "Null", "ScriptEnemy" .. i .. "A")
        end
    
    --HLMs have 1-2 VoidRift followers.
    elseif(sAppearance == "Horrible") then
        AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "B", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, "ZMines_VoidRift", "VoidRift", "Defeat_BackToSave", 0, "Null", "ScriptEnemy" .. i .. "A")
        if(iFollowerRoll >= 50) then
            AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "C", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, "ZMines_VoidRift", "VoidRift", "Defeat_BackToSave", 0, "Null", "ScriptEnemy" .. i .. "B")
        end
    
    --Skullcrawlers can appear in groups of up to 3.
    elseif(sAppearance == "SkullCrawler") then
        if(iFollowerRoll >= 30) then
            AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "B", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, "ZMines_Skullcrawler", "SkullCrawler", "Defeat_BackToSave", 0, "Null", "ScriptEnemy" .. i .. "A")
            if(iFollowerRoll >= 70) then
                AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "C", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, "ZMines_Skullcrawler", "SkullCrawler", "Defeat_BackToSave", 0, "Null", "ScriptEnemy" .. i .. "B")
            end
        end
    end
end

--[Treasure]
--LOOOOOT! This is "bonus objects" which can include treasure chests, NPCs, and special events.
local iTreasuresMax = AdlevGenerator_GetProperty("Treasures Total")
for i = 0, iTreasuresMax - 1, 1 do
	
	--If we pass the spawn cap, stop.
	if(i >= iMaxTreasures) then break end
	
	--Get the location of the treasure.
	local iSpawnX, iSpawnY = AdlevGenerator_GetProperty("Treasure Position", i)
	
    --Roll based on the floor.
    local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N") + LM_GetRandomNumber(-5, 5)
    local iItemRoll = LM_GetRandomNumber(0, 99) + iCurrentMinesFloor
    local sItem = "Bent Tools"
    
    --Random gem!
    if(iItemRoll >= 95) then
        local iSecondaryRoll = LM_GetRandomNumber(1, 6)
        if(iSecondaryRoll == 1) then sItem = "Yemite Gem" end
        if(iSecondaryRoll == 2) then sItem = "Rubose Gem" end
        if(iSecondaryRoll == 3) then sItem = "Glintsteel Gem" end
        if(iSecondaryRoll == 4) then sItem = "Ardrite Gem" end
        if(iSecondaryRoll == 5) then sItem = "Blurleen Gem" end
        if(iSecondaryRoll == 6) then sItem = "Qederite Gem" end
    
    --Adamantite!
    elseif(iItemRoll >= 90) then
        local iSecondaryRoll = LM_GetRandomNumber(0, 99)
        if(iSecondaryRoll < 50) then 
            sItem = "Adamantite Powder x1"
        elseif(iSecondaryRoll < 90) then 
            sItem = "Adamantite Flakes x1"
        else
            sItem = "Adamantite Shard x1"
        end
    
    --Equipment!
    elseif(iItemRoll >= 87) then
        local iSecondaryRoll = LM_GetRandomNumber(0, 12)
        if(iSecondaryRoll ==  0) then sItem = "Kinetic Capacitor" end
        if(iSecondaryRoll ==  1) then sItem = "Viewfinder Module" end
        if(iSecondaryRoll ==  2) then sItem = "Sure-Grip Gloves" end
        if(iSecondaryRoll ==  3) then sItem = "Wide-Spectrum Scanner" end
        if(iSecondaryRoll ==  4) then sItem = "Pulse Radiation Dampener" end
        if(iSecondaryRoll ==  5) then sItem = "Nanite Injection" end
        if(iSecondaryRoll ==  6) then sItem = "Explication Spike" end
        if(iSecondaryRoll ==  7) then sItem = "Regeneration Mist" end
        if(iSecondaryRoll ==  8) then sItem = "Dispersion Cloak" end
        if(iSecondaryRoll ==  9) then sItem = "Adaptive Cloth Vest" end
        if(iSecondaryRoll == 10) then sItem = "Ceramic Weave Vest" end
        if(iSecondaryRoll == 11) then sItem = "Neutronium Jacket" end
        if(iSecondaryRoll == 12) then sItem = "Distribution Frame" end
    
    --Credits Chip!
    elseif(iItemRoll >= 80) then
        sItem = "Credits Chip"
    
    --All other cases, it's a repeatable quest item.
    else
        local iSecondaryRoll = LM_GetRandomNumber(0, 2)
        if(iSecondaryRoll == 0) then sItem = "Bent Tools" end
        if(iSecondaryRoll == 1) then sItem = "Assorted Parts" end
        if(iSecondaryRoll == 2) then sItem = "Recycleable Junk" end
    end
    
	--Now spawn it with some crap.
	AL_SetProperty("Add Chest", "Chest" .. i, iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, true, sItem)
	
end
