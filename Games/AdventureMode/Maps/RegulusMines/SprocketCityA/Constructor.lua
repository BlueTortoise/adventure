--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "SprocketCityA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "SprocketCity")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("SprocketCityA")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Standard NPCs.
	fnStandardNPCByPosition("DroidAA")
	fnStandardNPCByPosition("DroidAB")
	fnStandardNPCByPosition("DroidAC")
	fnStandardNPCByPosition("DroidAD")
	fnStandardNPCByPosition("DroidAE")
	fnStandardNPCByPosition("DroidAF")
	fnStandardNPCByPosition("DroidAG")
	fnStandardNPCByPosition("DroidAH")
	fnStandardNPCByPosition("DroidAI")
	fnStandardNPCByPosition("DroidAJ")
	fnStandardNPCByPosition("DroidAK")
	fnStandardNPCByPosition("DroidAL")
	fnStandardNPCByPosition("Gardener")
	fnStandardNPCByPosition("Vendor")
	fnStandardNPCByPosition("TT-233")
	fnStandardNPCByPosition("UE117")
	fnStandardNPCByPosition("SH505")
	
	--Spawn SX-399 if she hasn't moved out yet:
	local iSteamDroid250RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
	if(iSteamDroid250RepState < 2.0) then
		fnSpecialCharacter("SX399", "SteamDroid", 70, 11, gci_Face_North, true, gsRoot .. "CharacterDialogue/SprocketCity/SX399.lua")
	elseif(iSteamDroid250RepState == 2.0) then
		fnSpecialCharacter("SX399", "SteamDroid", 52, 22, gci_Face_South, true, gsRoot .. "CharacterDialogue/SprocketCity/SX399.lua")
	end
	
	--JX-101.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSteamDroid500RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N")
	if(iSteamDroid500RepState ~= 1.0 and iSXUpgradeQuest ~= 2.0 and iSXUpgradeQuest ~= 3.0) then
		fnSpecialCharacter("JX101", "SteamDroid", 52, 23, gci_Face_West,  true, gsRoot .. "CharacterDialogue/SprocketCity/JX101.lua")
    elseif(iSXUpgradeQuest == 2.0) then
        fnAddPartyMember("JX-101")
	end
	
	--Prisoners and the psychologist.
	if(iSteamDroid500RepState == 2.0) then
		fnStandardNPCByPosition("FormerPrisonerA")
		fnStandardNPCByPosition("FormerPrisonerB")
		fnStandardNPCByPosition("FormerPrisonerC")
		fnStandardNPCByPosition("FormerPrisonerD")
		fnStandardNPCByPosition("Psychologist")
	end
end
