--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToMinesE") then
	
	--Variables.
	local iSprung55       = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	if(iSprung55 == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I should probably go get 55 out of jail before I leave...)") ]])
		fnCutsceneBlocker()
		return
	elseif(iSXUpgradeQuest == 2.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I can't leave with JX-101 following me like this, I'll need to distract her...)") ]])
		fnCutsceneBlocker()
		return
	end
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("TelluriumMinesE", "FORCEPOS:31.0x5.0x0")

--[Examinables]
elseif(sObjectName == "PaintingA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A painting of a city.[SOFTBLOCK] It's titled 'Swan Song of the People'.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PaintingB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A painting of a landscape made out of brass.[SOFTBLOCK] It's titled 'Faraway Promise'.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PaintingC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A painting of a shoreline.[SOFTBLOCK] It's titled 'Unpunished'.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "PaintingD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A painting of a shadow looking into the distance.[SOFTBLOCK] It's titled 'Isolation'.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PaintingE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A painting of a landscape made out of brass.[SOFTBLOCK] It's titled 'Faraway Promise'.[SOFTBLOCK] A tag on the name plate indicates this is the original and many copies have been made.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Boxes") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Boxes full of various parts.[SOFTBLOCK] Gears, tools, pipes...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "BookA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A book of notes about various locations in the mines.[SOFTBLOCK] I suppose Steam Droids don't have portable computers, and have to record everything with pen and paper.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A non-fiction book about a girl who uses a space ship to fly to various planets and have sex with the locals.[SOFTBLOCK] It's titled 'Star Sex in the Year 7000'.[SOFTBLOCK] It's the opposite of subtle.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "PartsShelfA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Parts in bad condition.[SOFTBLOCK] It's hard to believe anyone would be desperate enough to install these...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PartsShelfB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (There's some labels on the shelf.[SOFTBLOCK] It used to be full of parts, but now there's only a few...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PartsShelfC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Scanners and hand tools, used for diagnostics on power cores.[SOFTBLOCK] There's even an old blueprint of a Steam Droid core here, but it's so faded as to be illegible.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "LeakingVat") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (This vat is clearly broken.[SOFTBLOCK] I hope they didn't spill too much lubricant before they drained it.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Crate") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A fairly good crate.[SOFTBLOCK] Not the greatest crate, but one my great uncle Nate would be proud of.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BarrelA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (It's full of water.[SOFTBLOCK] Presumably it will be used for steam at some point.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "ComputerA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (There's a play-by-mail version of a game that looks like Chess here.[SOFTBLOCK] Whoever owns this terminal is not winning.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerB") then

	--Variables.
	local iSXUpgradeQuest    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketComputers = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketComputers", "N")
	local iSprocketPhotos    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketPhotos", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0) then
		
		--Broken.
		if(iSprocketComputers == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (This computer is broken.)") ]])
			fnCutsceneBlocker()
		
		--Fixed.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (This computer is showing cute photos of Darkmatters playing with cats.[SOFTBLOCK] Too adorable!)") ]])
			fnCutsceneBlocker()
		end
	
	--Let's renormalize some pressures!
	else
	
		if(iSprocketComputers == 0.0) then
			LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Computers")
		elseif(iSprocketPhotos == 0.0) then
			LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Photos")
		else
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (This computer is showing cute photos of Darkmatters playing with cats.[SOFTBLOCK] Too adorable!)") ]])
			fnCutsceneBlocker()
		end
	end
		
	
elseif(sObjectName == "ComputerC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (This computer is hacked into the Regulus City network, and highlights security threats in the mines.[SOFTBLOCK] There's...[SOFTBLOCK] a lot of them...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] ('Fist of Tomorrow Newsletter'.[SOFTBLOCK] Reports on recent skirmishes and encouraging words.[SOFTBLOCK] There's a section dedicated to signing up new Steam Droids at the top.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (There's a play-by-mail game that looks like Chess active on this terminal.[SOFTBLOCK] This player is winning, big time.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerF") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Oh, this terminal has all sorts of photos of Darkmatters playing and doing cute things![SOFTBLOCK] Adorable!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfA" or sObjectName == "BookshelfB" or sObjectName == "BookshelfC" or sObjectName == "BookshelfD") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketLibrary  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketLibrary", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0 or iSprocketLibrary == 1.0) then
	
		if(sObjectName == "BookshelfA") then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] ('Adventures of Bidoof the Camel'.[SOFTBLOCK] In this novel, Bidoof journeys across a desert and spits on people.[SOFTBLOCK] Riveting!)") ]])
			fnCutsceneBlocker()
			
		elseif(sObjectName == "BookshelfB") then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] ('Making Bombs for Beginners'.[SOFTBLOCK] Hey, if you're new to blowing things up, you gotta start somewhere.)") ]])
			fnCutsceneBlocker()
			
		elseif(sObjectName == "BookshelfC") then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] ('Structural Geology:: Principles and Practices'.[SOFTBLOCK] Reminds me of the time I walked in on one of the Geology professors berating at a student, because I didn't understand half of what he said.)") ]])
			fnCutsceneBlocker()
			
		elseif(sObjectName == "BookshelfD") then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] ('Sex as a Tool'...[SOFTBLOCK] Oh, it's literally about someone transforming into a voltometer and having sex with a Steam Droid.[SOFTBLOCK] Okay...)") ]])
			fnCutsceneBlocker()
		end
		
	--Let's sort some books!
	else
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Library")
	end
	
elseif(sObjectName == "BookshelfE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (Books on strategy, tactics, and organization.[SOFTBLOCK] Not the sort of thing SX-399 seems she'd read...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Chair") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (This chair is well worn.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ArtStudioSign") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Public art studio.[SOFTBLOCK] All are welcome.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "LibrarySign") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Public library.[SOFTBLOCK] Please return books after four days, thank you!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FistOfTheFutureSign") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Fist of the Future Headquarters.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Schematics") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Gear and pipe network blueprints.)") ]])
	fnCutsceneBlocker()

--[Special]
elseif(sObjectName == "Boilers") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketBoilers = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketBoilers", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0 or iSprocketBoilers == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Storage vats and boilers.[SOFTBLOCK] Some contain water for vaporization, some contain lubricant.[SOFTBLOCK] Better not mix those up!)") ]])
		fnCutsceneBlocker()
	
	--Let's renormalize some pressures!
	else
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Boilers")
	end

--[Special]
--Chessboard. Can be played during the SX-399 quest.
elseif(sObjectName == "Chessboard") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketChess  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketChess", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0 or iSprocketChess == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A chessboard, though on Pandemonium it's called Penkak.[SOFTBLOCK] It's almost identical to Chess except Rooks and Bishops can only move five spaces maximum.)") ]])
		fnCutsceneBlocker()
	
	--Let's play some Penkak!
	else
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Penkak")
	end
	
--[Special]
--Easel. Drawing side-activity during the SX-399 quest.
elseif(sObjectName == "Easel") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketDrawing  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketDrawing", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0 or iSprocketDrawing == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (A painting easel, used by the artists her to draw or paint.)") ]])
		fnCutsceneBlocker()
	
	--Let's play some Penkak!
	else
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Drawing")
	end

--[Special]
elseif(sObjectName == "DoorSX399") then

	--Variables.
	local iSteamDroid250RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
	
	--No quest yet:
	if(iSteamDroid250RepState < 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (It's locked.[SOFTBLOCK] I can hear someone pouting on the other side.)") ]])
		fnCutsceneBlocker()
	
	--Quest taken, execute:
	elseif(iSteamDroid250RepState == 1.0) then
	
		--State.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N", 2.0)
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (This must be SX-399's room...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Sound.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|Knock") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Turn and move.
		fnCutsceneFace("SX399", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I am not speaking to you, mother![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Excuse me?[SOFTBLOCK] Are you SX-399?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] You're not -[SOFTBLOCK] oh my!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("SX399", 70.25, 13.50, 2.00)
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorSX399") ]])
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|DoorOpen") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] My goodness, are you a human?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Why yes I am![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Keen![SOFTBLOCK] I can't remember the last time I saw a real human![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Please, come in!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("SX399", 70.25, 12.50)
		fnCutsceneMove("SX399", 69.25, 12.50)
		fnCutsceneFace("SX399", 1, 0)
		fnCutsceneMove("Christine", 70.25, 12.50)
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, you're SX-399?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I am![SOFTBLOCK] Wow![SOFTBLOCK] Are you from Regulus City?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] No, wait.[SOFTBLOCK] Are you from Pandemonium?[SOFTBLOCK] Maybe?[SOFTBLOCK] How did you get here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm from England, actually.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I have no idea where that is, but I want to hear all about it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, it's not that interesting.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Is it more interesting than being cooped up in this room?[SOFTBLOCK] Because if it is, no detail is unimportant.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, where did you get that skirt?[SOFTBLOCK] I love the stripe![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This?[SOFTBLOCK] Well...[SOFTBLOCK] I literally manifested it with sheer willpower.[SOFTBLOCK] I think.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Are you a mage?[SOFTBLOCK] Tubular![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Listen, SX-399...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] And what sort of accent is that?[SOFTBLOCK] Does everyone in Angland talk like that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You might be surprised how many accents we have.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But, SX-399, your mother sent me to speak to you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Tch.[SOFTBLOCK] That figures.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I don't need someone to check up on me.[SOFTBLOCK] You can go right back to her and tell her - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Tell that stuffy old lady what, exactly?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] ...?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Pfft.[SOFTBLOCK] I'm not that old, dear.[SOFTBLOCK] Your mom?[SOFTBLOCK] An antique.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, don't let her hear you say that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] What's she going to do?[SOFTBLOCK] Blow steam at me?[SOFTBLOCK] I'm sooo scared.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Aww, you're just trying to get in my good graces, aren't you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So what if I am?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Now I'm no detective, but I look around Sprocket City and I don't see a lot of Steam Droids your age.[SOFTBLOCK] Do you have any friends?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] ...[SOFTBLOCK] You don't know much about Steam Droids, do you?[SOFTBLOCK] Then again, I suppose you're a human.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What do you mean?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] We all became Steam Droids decades ago.[SOFTBLOCK] In fact, I'm probably older than you are, by a lot.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You don't look it.[SOFTBLOCK] You look half my age, dear.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Flattery will get you everywhere![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Oh, I just realized I didn't get your name.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Christine.[SOFTBLOCK] Pleased to meet you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So, Christine, maybe you didn't know, but -[SOFTBLOCK] I have a condition.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I won't bore you with the details, but no, I don't have a lot of friends.[SOFTBLOCK] I can't.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why not?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] My power core doesn't work right, and I have to spend most of my time recharging it.[SOFTBLOCK] I sleep for all but seven days each year.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I tried to get to know some Steam Droids, but then I go to sleep again.[SOFTBLOCK] When I wake up, they're like different people.[SOFTBLOCK] Or, they've moved to another settlement.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] The only ones who are always here are...[SOFTBLOCK] are...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Who?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Just mother...[SOFTBLOCK] I suppose all the others are gone now.[SOFTBLOCK] It's been ten years, hasn't it?[SOFTBLOCK] But it feels like yesterday...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I'm sorry...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Because of my condition, mother detests me leaving.[SOFTBLOCK] She wants to spend all her time with me.[SOFTBLOCK] I have to sneak out when I want to do something fun.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, from her perspective, it's a special occasion.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] True, but I have other things I want to do.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Sprocket City gets more dilapidated every time I wake.[SOFTBLOCK] Yet, all the other Steam Droids look at me like I'm a burden.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] That's not how they see you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] For a newcomer, you're frighteningly certain of that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Then again, why am I bothering with you?[SOFTBLOCK] When I go to sleep, you'll be gone when I wake up.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, if you want to help around the city, why don't you just tell your mother that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] As if she would listen.[SOFTBLOCK] She just scolds me.[SOFTBLOCK] I'm sick of it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You need to take a different angle.[SOFTBLOCK] Show her that you want to be responsible, not selfish.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ask her to give you a job.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] A job?[SOFTBLOCK] She would never - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Have you tried?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] No...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Be polite, but firm.[SOFTBLOCK] Make it obvious that a compromise will be easier for both of you.[SOFTBLOCK] She's reasonable, she'll listen.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You wouldn't leap over a mountain in one jump.[SOFTBLOCK] You need to climb it slowly and carefully.[SOFTBLOCK] Ask for a job where you'll be safe, and when she's okay with that job, work your way up.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] And you think that will work?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You've nothing to lose and your future to gain![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Plus this is kind of sort of how I managed to get a car when I was seventeen.[SOFTBLOCK] I promised to use it to run errands for my mother, and before you knew it I was driving to classes and back without the chauffer.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I don't know what half of what you said means, but I may as well try.[SOFTBLOCK] Now or never, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Go ahead, I'll be right behind you.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		fnCutsceneMove("SX399", 69.25, 13.50)
		fnCutsceneFace("Christine", -1, 1)
		fnCutsceneBlocker()
		fnCutsceneMove("SX399", 70.25, 13.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneMove("SX399", 70.25, 15.50)
		fnCutsceneMove("SX399", 59.25, 15.50)
		fnCutsceneMove("SX399", 59.25, 16.50)
		fnCutsceneMove("SX399", 56.25, 16.50)
		fnCutsceneMove("SX399", 56.25, 22.50)
		fnCutsceneTeleport("JX101", 52.25, 23.50)
		fnCutsceneTeleport("SX399", 52.25, 22.50)
		fnCutsceneBlocker()
		fnCutsceneFace("SX399", 0, 1)
		fnCutsceneBlocker()
	
	--All other cases, open immediately.
	else
		AL_SetProperty("Open Door", "DoorSX399")
		AudioManager_PlaySound("World|OpenDoor")
	end

--[SX-399's Pod]
elseif(sObjectName == "SXPod") then

	--Variables.
	local iSXUpgradeQuest  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketGardening = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketGardening", "N")
	local iSprocketComputers = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketComputers", "N")
	local iSprocketBoilers   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketBoilers", "N")
	local iSprocketLibrary   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketLibrary", "N")
	local iSprocketChess     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketChess", "N")
	local iSprocketPhotos    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketPhotos", "N")
	local iSprocketHats      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketHats", "N")
	local iSprocketDrawing   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketDrawing", "N")

	--Sum.
	local iSum = iSprocketBoilers + iSprocketChess + iSprocketComputers + iSprocketDrawing + iSprocketGardening + iSprocketHats + iSprocketLibrary + iSprocketPhotos
	
	--No upgrade quest.
	if(iSXUpgradeQuest < 2.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (SX-399's recharging pod.[SOFTBLOCK] It has several special parts and extra gauges.)") ]])
		fnCutsceneBlocker()
	
	--Upgrade quest, but less than 3 activities done.
	elseif(iSXUpgradeQuest == 2.0 and iSum < 3) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (I haven't bought enough time yet.[SOFTBLOCK] I should go find some activities around town...)") ]])
		fnCutsceneBlocker()
		
	--Upgrade quest, ready to finish.
	elseif(iSXUpgradeQuest == 2.0 and iSum >= 3) then
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Finale")
	
	--Future cases.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Christine] (SX-399's recharging pod.[SOFTBLOCK] Seems she won't be needing all the extra add-ons now!)") ]])
		fnCutsceneBlocker()
		
	end
	


--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end