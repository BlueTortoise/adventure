--[Special Scripts]
--These are used during the JX-101 distraction subquest. The argument provided is the name of the activity.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Activity name.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Chess/Penkak]
--A game of memorization for pros and strategy for amatuers.
if(sObjectName == "Penkak") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketChess", "N", 1.0)

	--Spawn some NPCs.
	TA_Create("ObserverA")
		TA_SetProperty("Position", -10, -10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", false)
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	TA_Create("ObserverB")
		TA_SetProperty("Position", -10, -10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", false)
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	TA_Create("ObserverC")
		TA_SetProperty("Position", -10, -10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", false)
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Ah, Penkak.[SOFTBLOCK] The ancient game of strategy.[SOFTBLOCK] Do you think tonight's the night you finally take your mother down?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Hoo, maybe it is!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I've got some new strategies in mind.[SOFTBLOCK] It'll be like you're playing against a completely different person.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Is that so?[SOFTBLOCK] Well, show me what you're made of!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Blackout.
	fnCutsceneWait(25)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 36.25, 27.50)
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneTeleport("JX101", 38.25, 27.50)
	fnCutsceneFace("JX101", -1, 0)
	fnCutsceneTeleport("ObserverA", 46.25, 33.50)
	fnCutsceneFace("ObserverA", 1, 0)
	fnCutsceneTeleport("ObserverB", 40.25, 19.50)
	fnCutsceneTeleport("ObserverC", 41.25, 19.50)
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	
	--Move.
	fnCutsceneMove("ObserverB", 40.25, 27.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("ObserverB", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Look everyone![SOFTBLOCK] SX-399 and JX-101 are playing Penkak again![SOFTBLOCK] And SX-399 is winning!") ]])
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("ObserverB", 40.25, 28.50, 1.70)
	fnCutsceneMove("ObserverB", 36.25, 28.50, 1.70)
	fnCutsceneFace("ObserverB", 0, -1)
	fnCutsceneMove("ObserverC", 41.25, 28.50, 1.70)
	fnCutsceneMove("ObserverC", 38.25, 28.50, 1.70)
	fnCutsceneFace("ObserverC", 0, -1)
	fnCutsceneMove("ObserverA", 45.25, 33.50, 1.20)
	fnCutsceneMove("ObserverA", 45.25, 29.50, 1.20)
	fnCutsceneMove("ObserverA", 44.25, 29.50, 1.20)
	fnCutsceneMove("ObserverA", 44.25, 28.50, 1.20)
	fnCutsceneMove("ObserverA", 39.25, 28.50, 1.20)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
	fnCutsceneMove("ObserverA", 38.25, 28.50, 0.30)
	fnCutsceneMoveFace("ObserverC", 37.25, 28.50, 0, -1, 0.30)
	fnCutsceneBlocker()
	fnCutsceneFace("ObserverA", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Oh, they say you're winning, sweetie.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *tap*[SOFTBLOCK] I think they're right![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Shame that you've got only one queen to give away...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Oh, I see.[SOFTBLOCK] And what if I do, this?[SOFTBLOCK] *tap*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] !!![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Ah ha![SOFTBLOCK] Check![SOFTBLOCK] Let's see you get out of this one!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *tap*[SOFTBLOCK] Gotta sacrifice a pawn sometimes...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] No, no.[SOFTBLOCK] The lives of your pawns are not to be thrown away.[SOFTBLOCK] You must buy something even greater if you sacrifice them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Think in terms of what you gain for their loss.[SOFTBLOCK] *tap*[SOFTBLOCK] Position, pieces, victory, but never nothing.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Argh![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Checkmate.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Fiddlesticks![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Oh, don't pout, sweetie.[SOFTBLOCK] You did well, but this game is more about memorization than it truly is about strategy.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Your strategy this time was quite similar to one I played against...[SOFTBLOCK] a while ago...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] I'm not pouting...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] That was fun, though![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You can be very competitive when you want to be.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You take after me, even more so this time than usual![SOFTBLOCK] And -[SOFTBLOCK] well, I'm sure you'll get me next time...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneFace("JX101", 0, 1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[VOICE|JX-101] Show's over, you louts.[SOFTBLOCK] Back to work!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Blackout.
	fnCutsceneWait(45)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 39.25, 27.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX101", 39.25, 27.50)
	fnCutsceneFace("JX101", 0, 1)
	fnCutsceneTeleport("ObserverA", -100.0, -100.0)
	fnCutsceneTeleport("ObserverB", -100.0, -100.0)
	fnCutsceneTeleport("ObserverC", -100.0, -100.0)
	fnCutsceneBlocker()
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
--[Drawing]
--Artistry!
elseif(sObjectName == "Drawing") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketDrawing", "N", 1.0)

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You always gravitate back to the art studio.[SOFTBLOCK] Did you want to practice your drawing, sweetie?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Err...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Now now, I may have said some harsh things before, but that was just to motivate you.[SOFTBLOCK] You're getting better.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Criticism can be tough but you have to endure it to improve.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, let me show you what I've learned!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Your technique has...[SOFTBLOCK] improved![SOFTBLOCK] A great deal![SOFTBLOCK] Have you been practicing without me knowing?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (!!!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All I do is focus very hard on my hand.[SOFTBLOCK] To keep it from shaking.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Ah, very good.[SOFTBLOCK] I know your condition can make it hard to keep a steady hand, but you're barely shaking at all.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] So, who did you draw today?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This is...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] This resembles a golem...[SOFTBLOCK] but with green hair and a blue dress?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's not supposed to be a golem, mother![SOFTBLOCK] Oh bother...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Oh my, I'm sorry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Sheesh, what is wrong with me?[SOFTBLOCK] I was so focused on my technique that I forgot what I was drawing!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Well, it's a lovely piece.[SOFTBLOCK] I'll be hanging it in the war room tomorrow.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I'm proud of your work no matter what it is, sweetie.[SOFTBLOCK] So long as you try your best, you're a success in my eyes.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 10.25, 10.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX101", 10.25, 10.50)
	fnCutsceneFace("JX101", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
--[Library]
--Sorting books with mom.
elseif(sObjectName == "Library") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketLibrary", "N", 1.0)

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] The library...[SOFTBLOCK] Sheesh, the books are all out of order.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Oh, sweetie, do you remember Bidoof the Camel?[SOFTBLOCK] Those were always your favourite books when you were little.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, yes, of course![SOFTBLOCK] Bidoof the Camel![SOFTBLOCK] Love him![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Someone really should sort these books properly...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Why not us?[SOFTBLOCK] Come on, let's show our support.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You really want to spend a few hours sorting books?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm taking responsibility for my city, mother.[SOFTBLOCK] It's the right thing to do.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Hmm, here's one of the 'Magical Settlement' novellas.[SOFTBLOCK] These take me back.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What are those?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Oh sweetie, these were written a long time ago.[SOFTBLOCK] Nobody is sure who the real author is.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] They're about a boy who becomes a magical hero fighting for justice, but every time he makes a mistake, he becomes more like a girl.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] It's quite silly, but they have a special place in my power core.[SOFTBLOCK] They were the first thing I decided to read all on my own, without anyone telling me to.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I suppose, in a way, I kind of took on that same role, didn't I?[SOFTBLOCK] But the reality isn't as glamorous or funny...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A single mom, fighting for justice while raising a daughter?[SOFTBLOCK] Damn the glamour, you're a hero in this 'bots book.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 20.25, 18.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX101", 20.25, 18.50)
	fnCutsceneFace("JX101", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
--[Boilers]
--Pressure!
elseif(sObjectName == "Boilers") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketBoilers", "N", 1.0)

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] *sigh*[SOFTBLOCK] Somebody has been shirking pressure duty.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Is it something we can fix ourselves?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Sweetie, balancing the pressure in the steam pipes isn't a lot of fun.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You said you'd let me choose the activity, so I'm choosing this one.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Besides, this is how I'm going to contribute.[SOFTBLOCK] Sprocket City needs every pair of hands![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You're really taking that to heart, aren't you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (If it buys time, we'll scrub the floors too...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just show me what to do and we'll be done in no time.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Okay, let some out of there...[SOFTBLOCK] and stoke that boiler, too.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Got it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You know, we make a good team.[SOFTBLOCK] Maybe someday I could take you on a mission with me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] S-[SOFTBLOCK]strictly as an observer![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Of course mother, hee hee!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 57.25, 25.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneTeleport("JX101", 57.25, 25.50)
	fnCutsceneFace("JX101", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
--[Gardening]
--Plants!
elseif(sObjectName == "Gardening") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketGardening", "N", 1.0)

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Oh, hello there.[SOFTBLOCK] Is this a special occasion, JX-101?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] We're spending some time together, mother and daughter.[SOFTBLOCK] And you know how SX-399 loves tending to the plants.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I do?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I mean, of course I do!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] SX-399, your methods are getting rather unorthodox.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How so?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] One does not normally tend to plants with a screwdriver.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, they're robots just like us.[SOFTBLOCK] We should use the same techniques, shouldn't we?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Yes, but you can just let their brass roots soak up the lubricant.[SOFTBLOCK] You don't need to open the panels and pour it in.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm just being...[SOFTBLOCK] efficient!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 41.25, 25.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX101", 41.25, 25.50)
	fnCutsceneFace("JX101", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
--[Computers]
--Fix that piece of junk up!
elseif(sObjectName == "Computers") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketComputers", "N", 1.0)

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Great, I see the backup terminal is broken.[SOFTBLOCK] Again.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, I'm sure we can fix it if we put our heads together.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And then we can look at pictures on it![SOFTBLOCK] Grab a screwdriver!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Pass me that bolt-wrench, will you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Here you go.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] *clank*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Got it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey, it booted up![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I bet I can distract JX-101 with pictures and videos on this thing...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 61.75, 19.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneTeleport("JX101", 61.25, 19.50)
	fnCutsceneFace("JX101", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
--[Photos]
--OH MY GOD THEY'RE SO CUTE!
elseif(sObjectName == "Photos") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketPhotos", "N", 1.0)

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Mother, look![SOFTBLOCK] The terminal has photos of Darkmatters playing with kittens!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] SX-399, where do you think these photos came from?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not sure.[SOFTBLOCK] It looks like a grassy, forested habitat.[SOFTBLOCK] Maybe Pandemonium?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Or someone illicitly downloaded them from Regulus City's network.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, maybe.[SOFTBLOCK] I suppose that'd mean the golems took them, then?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] ...[SOFTBLOCK] Those killers would just as soon recycle the kittens.[SOFTBLOCK] It must have been taken on Pandemonium by a Steam Droid mage.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That is...[SOFTBLOCK] quite a theory, mother.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 61.75, 19.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneTeleport("JX101", 61.25, 19.50)
	fnCutsceneFace("JX101", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
--[Hats]
--TF2 JOKE!
elseif(sObjectName == "Hats") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketHats", "N", 1.0)

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] JX-101, looking to do some shopping?[SOFTBLOCK] I got some kit from Gearsville...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] This isn't an official visit.[SOFTBLOCK] What do you have in the more...[SOFTBLOCK] fashionable...[SOFTBLOCK] pursuits?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh my gosh, mother![SOFTBLOCK] Cute hats![SOFTBLOCK] Ribbons![SOFTBLOCK] Gloves![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Which I have always enjoyed, and if this is the first time you are finding that out, it is not suspicious.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Of course, sweetie.[SOFTBLOCK] Let's try a few things on.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|HatA] What do you think of this one?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't think the color works.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Hmm...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|HatB] This one, then?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's so you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Feh, but it's out of my price range.[SOFTBLOCK] Still, next time...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|HatC] All right, one more.[SOFTBLOCK] For the road.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh mother, you look great with or without the hat.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Flattery will get you everywhere, sweetie.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 15.25, 11.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX101", 15.25, 11.50)
	fnCutsceneFace("JX101", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
--[Finale]
--The exciting conclusion.
elseif(sObjectName == "Finale") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 3.0)

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Well, I suppose this is it.[SOFTBLOCK] In you go.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I had a lot of fun today, mother.[SOFTBLOCK] I'm glad we spent this time together.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] As am I, my sweet cherry.[SOFTBLOCK] I -[SOFTBLOCK] I - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What is it?[SOFTBLOCK] Do you have something important to tell me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] [SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK] No.[SOFTBLOCK] Just -[SOFTBLOCK] goodnight.[SOFTBLOCK] I'll see you in a year.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Goodnight.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneMove("JX101", 70.25, 12.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Stop music.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[VOICE|JX-101] Wait.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("JX101", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Your charge pump.[SOFTBLOCK] Where is it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, I...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] The special pump on the back of your neck![SOFTBLOCK] Why isn't it there?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You -[SOFTBLOCK] you are not SX-399![SOFTBLOCK] Are you?[SOFTBLOCK] I thought something was off, but I brushed the thought away![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You are not my daughter![SOFTBLOCK] Who are you?[SOFTBLOCK] What have you done with her?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Wait, no![SOFTBLOCK] I can explain![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Enough of your lies!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(15)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Some time earlier...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	
	--Map transition.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityC", "FORCEPOS:19.0x13.0x0") ]])
end