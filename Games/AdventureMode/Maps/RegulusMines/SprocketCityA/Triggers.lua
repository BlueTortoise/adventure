--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Start the mines cutscene.
if(sObjectName == "IntroInBed") then

	--Variables.
	local iSawWakeUpIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawWakeUpIntro", "N")
	if(iSawWakeUpIntro == 1.0) then return end
	
    --Get Christine's form at the start of this sequence.
    local sOriginalForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
	--Spawn NPCs.
	fnCutsceneTeleport("TT-233", 60.25, 11.50)
	fnCutsceneTeleport("JX101", 60.25, 12.50)
	fnCutsceneFace("JX101", -1, 0)
	fnCutsceneFace("TT-233", -1, 0)
	
	--Remove 55.
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AC_SetProperty("Set Party", 1, "Null")
	AL_SetProperty("Unfollow Actor Name", "55")

	--If the 55 entity happens to be on the field, move her off.
	if(EM_Exists("55") == true) then
		EM_PushEntity("55")
			TA_SetProperty("Position", -10, -10)
		DL_PopActiveObject()
	end

	--Flag to indicate she is not following Christine.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWakeUpIntro", "N", 1.0)
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Unghh...[SOFTBLOCK] it hurts...[SOFTBLOCK] Sophie...[SOFTBLOCK] please fix me...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(185)
	fnCutsceneBlocker()
    
    --Topics
    WD_SetProperty("Unlock Topic", "SteamDroids", 1)
	
	--Quickly shift Christine to Golem for the purposes of this dream sequence.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
	fnCutsceneWait(1)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Honey, I've returned with the shopping![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] The shopping?[SOFTBLOCK] Hee hee![SOFTBLOCK] Is that what you call it where you're from?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] And I see we've upgraded to honey now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] I can call you anything you like, as long as it's sweet, like you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I've always liked 'dearest'.[SOFTBLOCK] Would you like that?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Of course I would, dearest![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Oh, I seem to have damaged myself.[SOFTBLOCK] Drat.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Let me take a look at it...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Nothing the repair nanites can't fix.[SOFTBLOCK] You'll be fine in no time.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] Sophie, I love you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Aww, I love you too, dearest![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *Kiss*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Ack![SOFTBLOCK] Don't kiss me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But I - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Wake up, you stupid organic![SOFTBLOCK] Wake up!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Shift to human.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "CostumeHandlers/Christine/Human_Nude.lua") ]])
	fnCutsceneWait(1)
	fnCutsceneBlocker()
	
	--Christine switches to special frames.
	fnCutsceneSetFrame("Christine", "BedSleep")
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "TT-233", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] How is she doing, doc?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[E|Neutral] Well, the nanites must be repairing her faster than expected, because she just tried to kiss me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] By way of thanks?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[E|Neutral] She's still sleeping.[SOFTBLOCK] Maybe organics kiss in their sleep.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] When you were an organic, did you kiss anyone in your sleep?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[E|Neutral] And just how would I know that, JX?[SOFTBLOCK] I would be *asleep*.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Frame change.
	fnCutsceneSetFrame("Christine", "BedWake")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Ungh...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Frame change.
	fnCutsceneSetFrame("Christine", "BedSleep")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "BedWake")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "BedSleep")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "BedWakeR")
	fnCutsceneWait(65)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "TT-233", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] Mmm, was I dreaming?[SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Uh oh, I'm surrounded by Steam Droids and I can't move...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[E|Neutral] You hurt yourself pretty bad in that fall, so relax.[SOFTBLOCK] The nanites need more time to repair your legs and spine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The fall?[SOFTBLOCK] Oh, yes, the fall.[SOFTBLOCK] I remember the fall.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I don't seem to remember the landing, though.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Just what do you remember?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] That is correct.[SOFTBLOCK] I do not recall your face, have we met?[BLOCK][CLEAR]") ]])
    
    --Was a human to begin with:
    if(sOriginalForm == "Human") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I seem to have survived the fall even with my weak human legs...[SOFTBLOCK] so that's something.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I bet my golem legs would have been just fine...[EMOTION|Christine|Sad] Wait, 55!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (55 probably got really damaged...[SOFTBLOCK] I hope she made it out as well as I did...[SOFTBLOCK] Better play dumb until I can find her.)[BLOCK][CLEAR]") ]])
    
    --Golem:
    elseif(sOriginalForm == "Golem") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I'm human again...[SOFTBLOCK] I must have transformed after the fall...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (But 55 -[SOFTBLOCK] Command Unit legs aren't as sturdy as Golem legs![SOFTBLOCK] I hope she's okay...)[BLOCK][CLEAR]") ]])
    
    --Latex Drone:
    elseif(sOriginalForm == "LatexDrone") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I'm human again...[SOFTBLOCK] I must have transformed after the fall...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I didn't liquify on impact.[SOFTBLOCK] Maybe that latex bondage suit kept me together enough to transform?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Oh no, 55![SOFTBLOCK] I bet they got her![SOFTBLOCK] I hope she's in a better shape than I am...)[BLOCK][CLEAR]") ]])
    
    --Darkmatter:
    elseif(sOriginalForm == "Darkmatter") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I'm human again...[SOFTBLOCK] I must have transformed after the fall...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Drat, I should have spent more time mastering my starlight body.[SOFTBLOCK] I'm sure I could have dissociated and went through the rock...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Oh no, 55![SOFTBLOCK] She can't dissociate![SOFTBLOCK] Her hardmatter legs would crack like an egg on that rock!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (Hurry up you stupid, squishy legs![SOFTBLOCK] Heal![SOFTBLOCK] I need to help 55!)[BLOCK][CLEAR]") ]])
    
    --Eldritch Dreamer:
    elseif(sOriginalForm == "Eldritch") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I'm human again...[SOFTBLOCK] I must have transformed after the fall...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I was knocked out, but -[SOFTBLOCK] I guess I transformed before I entered any sort of REM sleep.[SOFTBLOCK] Phew.[SOFTBLOCK] 55 would be so cross - )[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Oh no, 55![SOFTBLOCK] They must have got her too![SOFTBLOCK] Better play dumb!)[BLOCK][CLEAR]") ]])
    
    --Electrosprite! Shocking!
    elseif(sOriginalForm == "Electrosprite") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I'm human again...[SOFTBLOCK] I must have transformed after the fall...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (How does electricity get hurt from falling really far?[SOFTBLOCK] I bet 55 would have a theory - )[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] (Oh no, 55![SOFTBLOCK] They must have got her too![SOFTBLOCK] Better play dumb until I can get to her!)[BLOCK][CLEAR]") ]])
    end
    
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Erm, I'm not sure.[SOFTBLOCK] My memory is fuzzy.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My name's 77 -[SOFTBLOCK] uh, Christine.[SOFTBLOCK] Christine Dormer.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(65)
	fnCutsceneBlocker()
	
	--Frames.
	fnCutsceneSetFrame("Christine", "BedSleep")
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Very sorry, but you two were being terribly loud.[SOFTBLOCK] It's bad form to walk into someone's room when they're sleeping.") ]])
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "BedFullR")
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "TT-233", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Wait, am I in a hospital?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[E|Neutral] Of sorts.[SOFTBLOCK] We don't have many organic visitors, so I converted the visitor's quarters into a hospice.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Listen, Christine. We need your help.[SOFTBLOCK] Whatever you can tell us would be greatly appreciated.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My help?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Yes.[SOFTBLOCK] You may not remember it, but you triggered one of our traps.[SOFTBLOCK] We set them up around the entrances to the settlement.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I assume that Command Unit that we found was chasing you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Uhhh...[SOFTBLOCK] Yes.[SOFTBLOCK] She was chasing me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Do you know anything about her?[SOFTBLOCK] Her designation, perhaps?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sorry, but I don't know anything about her.[SOFTBLOCK] I -[SOFTBLOCK] I was kidnapped, I think, and I woke up in an airlock.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I escaped, but those robot girls have been chasing me ever since.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (I mean that's partially true, so hopefully 55 won't contradict my story later if they interrogate her...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Well, the thing that was chasing you is called a Command Unit.[SOFTBLOCK] They're high ranking in Regulus City.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Is she all right?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] She's in system standby.[SOFTBLOCK] We can't seem to reactivate her, but she's alive.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Why do you ask?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] I don't want to hurt anyone.[SOFTBLOCK] I'm sorry for the trouble I've caused.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[E|Neutral] You'll need to give the nanites some time to finish repairing you.[SOFTBLOCK] After that...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You're welcome to stay with us, if you like.[SOFTBLOCK] I'm sorry, but it is unlikely we will be able to return you home under the present circumstances.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Better act dumb...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I suppose I don't have much of a choice.[SOFTBLOCK] I think I can walk again, almost...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Err, do I need to be naked for the nanites to work?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "TT-233:[E|Neutral] Ah, of course.[SOFTBLOCK] I'll get your clothes.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (This may be a lewd game, but it's still impolite to watch a girl change, you know.)") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "CostumeHandlers/Christine/Human_Normal.lua") ]])
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	
	--Reposition and fade in.
	fnCutsceneSetFrame("Christine", "Null")
	fnCutsceneTeleport("Christine", 60.25, 11.50)
	fnCutsceneTeleport("JX101", 52.25, 23.50)
	fnCutsceneTeleport("TT-233", 61.25, 15.50)
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Those nanites really worked a treat![SOFTBLOCK] I feel almost as good as golem-me feels!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Considering I'm not a pancake after falling six stories, this is about as good as I can hope for.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (All right, I should probably confer with 55 before I do anything else...)") ]])
	fnCutsceneBlocker()

--Go back and talk to JX-101.
elseif(sObjectName == "TalkToJX") then

	--Variables.
	local iMetJX101 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N")
	if(iMetJX101 == 1.0) then return end
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I should go speak to JX-101 before I head out...)") ]])
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 52.25, 18.50)
	fnCutsceneBlocker()
	
--55 headlock scene.
elseif(sObjectName == "55Headlock") then

	--Variables.
	local iSawHeadlockScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawHeadlockScene", "N")
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	if(iSawHeadlockScene == 1.0 or iSprung55 == 0.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawHeadlockScene", "N", 1.0)
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Set positions and poses.
	fnSpecialCharacter("55", "Doll", 24, 7, gci_Face_South, false, nil)
	fnCutsceneTeleport("Christine", 24.25, 8.50)
	fnCutsceneTeleport("DroidAD", 26.25, 7.50)
	fnCutsceneTeleport("DroidAI", 21.25, 9.50)
	fnCutsceneTeleport("DroidAF", 28.25, 9.50)
	fnCutsceneTeleport("JX101", 24.25, 18.50)
	fnCutsceneTeleport("TT-233", 23.25, 18.50)
	fnCutsceneFace("DroidAD", -1, 0)
	fnCutsceneFace("DroidAI", 1, 0)
	fnCutsceneFace("DroidAF", -1, 0)
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	
	--55 and Christine move a bit.
	fnCutsceneMove("55", 24.25, 8.50, 0.30)
	fnCutsceneMove("Christine", 24.25, 9.50, 0.30)
	fnCutsceneBlocker()
	fnCutsceneMove("JX101", 24.25, 11.50, 2.50)
	fnCutsceneMove("TT-233", 23.25, 11.50, 2.50)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Stop right there![SOFTBLOCK] Let the human go![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Order denied.[SOFTBLOCK] Clear my path, or the human dies.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You're surrounded, and we will open fire if the human is not released in 3,[SOFTBLOCK][SOFTBLOCK] 2,[SOFTBLOCK][SOFTBLOCK] - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] In the time it takes you to pull the trigger, I will have computed the projectile trajectory and repositioned the human to intercept it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] If you kill the girl, you won't leave here in one piece![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Your firearm is a Mk. IV Pulse Pistol.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] ...[SOFTBLOCK] Correct.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You scavenged it from a waste dump.[SOFTBLOCK] The weapon has been superseded by the Mk. V series due to a flaw in the accelerator arrays.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] When not regularly maintained, as yours is, the array malfunctions and diffuses radiation through the barrel.[SOFTBLOCK] The pulse round loses half of its charge.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You, and all your soldiers,[SOFTBLOCK] firing simultaneously,[SOFTBLOCK] will do little more than graze my reinforced chassis.[SOFTBLOCK] The human will be torn to shreds.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It will not be I who kills her, but you.[SOFTBLOCK] Now...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] Stand.[SOFTBLOCK][SOFTBLOCK] Aside.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] .[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK] Do what she says.[SOFTBLOCK] Weapons down.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move.
	fnCutsceneMove("JX101", 26.25, 11.50, 1.00)
	fnCutsceneMove("TT-233", 25.25, 11.50, 1.00)
	fnCutsceneFace("JX101", -1, 0)
	fnCutsceneFace("TT-233", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneMove("55", 24.25, 13.50, 0.30)
	fnCutsceneMove("Christine", 24.25, 14.50, 0.30)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Attack me, and I will execute the human.[SOFTBLOCK] Attempt to follow me, and I will execute the human.[SOFTBLOCK] Is that clear?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] ...[SOFTBLOCK] As crystal...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneMove("55", 24.25, 16.50, 0.30)
	fnCutsceneMove("Christine", 24.25, 17.50, 0.30)
	fnCutsceneWait(180)
	fnCutsceneBlocker()
	
	--Move to the next map.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("TelluriumMinesE", "FORCEPOS:31.0x14.0x0") ]])
	fnCutsceneBlocker()

--After the cutscene where 55 drags Christine out.
elseif(sObjectName == "After55") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSawAfter55Scene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawAfter55Scene", "N")
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	
	--SX-399 Upgrade quest.
	if(iSXUpgradeQuest == 1.5) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 2.0)
		
		--Add JX-101 to the follower list.
		fnAddPartyMember("JX-101")
		
		--Lock fadeout.
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneBlocker()
		
		--Teleport JX-101 over.
		fnCutsceneTeleport("JX101", 19.25, 18.50)
		fnCutsceneFace("Christine", 0, 1)
		
		--Fade in slowly.
		fnCutsceneWait(45)
		fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Okay, just gotta play it cool.[SOFTBLOCK] Act like SX-399, try to avoid too much contact with JX-101, but make sure I'm seen so nobody wonders where I am.[SOFTBLOCK] Easy.)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 21.25, 37.50)
		fnCutsceneMove("Christine", 24.25, 34.50)
		fnCutsceneMove("Christine", 24.25, 32.50)
		fnCutsceneMove("Christine", 20.25, 29.50)
		fnCutsceneMove("Christine", 19.25, 27.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[VOICE|JX-101] Oh, there you are, sweetie!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneMove("JX101", 19.25, 26.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Thank the gears![SOFTBLOCK] I was just about to put together a search team![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Great, that plan lasted all of five seconds.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh mother, you worry too much.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] With good reason.[SOFTBLOCK] If you had seen the things we saw on the last mission...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sure I can imagine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Well, sweetie, this is your last night before you go in for recharging.[SOFTBLOCK] No staying up late, all right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Say, I was thinking we could...[SOFTBLOCK] spend it together?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, mother, I was planning to - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I know, I know.[SOFTBLOCK] I'm willing to compromise.[SOFTBLOCK] You pick the activity.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Isn't that what you asked me for earlier?[SOFTBLOCK] Well, here it is![SOFTBLOCK] Isn't that great?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well, as long as you let me pick...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Drat![SOFTBLOCK] I can't break character!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll lead.[SOFTBLOCK] See if you can keep up, mother![SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I'll need to find some activities to keep JX-101 distracted.[SOFTBLOCK] I think I'll need to do at least 3 things...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Examining things around town may provide me with some ideas...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("JX101", 19.25, 27.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
		--Remove JX-101's dialogue script and collision flag.
		EM_PushEntity("JX101")
			TA_SetProperty("Clipping Flag", false)
			TA_SetProperty("Activation Script", "Null")
		DL_PopActiveObject()
		
		--Only Christine is in the party.
		AC_SetProperty("Set Party", 1, "Null")
		AC_SetProperty("Set Party", 2, "Null")
		AC_SetProperty("Set Party", 3, "Null")
	
		return
	end

	--Rescue droids.
	if(iSawAfter55Scene == 1.0 or iSprung55 == 0.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAfter55Scene", "N", 1.0)
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Character positioning.
	fnCutsceneTeleport("JX101", 20.25, 29.50)
	fnCutsceneFace("JX101", 0, -1)
	TA_Create("RescueDroidA")
		TA_SetProperty("Position", 19, 27)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/SprocketCity/RescueDroidA.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	TA_Create("RescueDroidB")
		TA_SetProperty("Position", 20, 27)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/SprocketCity/RescueDroidB.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	TA_Create("RescueDroidC")
		TA_SetProperty("Position", 21, 27)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/SprocketCity/RescueDroidC.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	
	--Reposition Christine.
	fnCutsceneTeleport("Christine", 20.25, 37.50)
	fnCutsceneBlocker()
	
	--Camera focus on JX-101..
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 5.0)
		CameraEvent_SetProperty("Focus Actor Name", "JX101")
	DL_PopActiveObject()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade back in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 24.25, 33.50)
	fnCutsceneMove("Christine", 24.25, 32.50)
	fnCutsceneFace("JX101", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] All right, listen up.[SOFTBLOCK] The objective is the Command Unit, and we need to intercept her before she reaches the surface.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] She's got a hostage, and will kill her if we're seen to be tailing them.[SOFTBLOCK] Stay out of sight.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: Ma'am?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Do you have a question?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: Is the hostage that human that AJ-99 found?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Yes.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: And does she wear a lot of violet clothes?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Yes.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid: And is she standing right behind you?") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("JX101", 0, 1)
	
	--Movement.
	fnCutsceneMove("Christine", 21.25, 29.50)
	fnCutsceneFace("Christine", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneFace("JX101", 1, 0)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 1.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Am I interrupting my own rescue briefing?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You're -[SOFTBLOCK] how did you escape?[SOFTBLOCK] What happened to the Command Unit?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I convinced her to let me go.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] But how?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Well, a creature with more teeth than face showed up, and came after her.[SOFTBLOCK] It made a very cohesive argument, and I ran off as it attacked her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] She...[SOFTBLOCK] did not survive the encounter...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You are certain of this?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The thing crushed her, and then dragged her off...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Hm...[SOFTBLOCK] A fortunate turn of events.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] For everyone except her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Waste not your pity on the Command Units.[SOFTBLOCK] They are ruthless killers with no remorse.[SOFTBLOCK] They think of no one save their precious Central Administration, not even themselves.[SOFTBLOCK] It is better she is dead.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Well then.[SOFTBLOCK] I am glad you are well.[SOFTBLOCK] I will assemble a team to ascertain the final fate of this Command Unit, but as there is no time pressure, it may be best to wait.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Please, make yourself comfortable and see TT-233 if you have any injuries.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I will, thank you.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

--Finale cutscene.
elseif(sObjectName == "SXCutsceneTrigger") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	if(iSXUpgradeQuest ~= 3.0) then return end

	--Execute.
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/SprocketFinale/Scene_B_SprocketCityA.lua")

--Go back and talk to AJ-99
elseif(sObjectName == "TalkToAJ") then

	--Variables.
	local iMetAJ99 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetAJ99", "N")
	if(iMetAJ99 == 1.0) then return end
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iMetAJ99", "N", 1.0)
	
	--Facing.
	fnCutsceneFace("DroidAJ", 1, 0)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Hey, there she is!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 13.25, 38.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "AJ-99:[E|Neutral] Hey there, human![SOFTBLOCK] I'm AJ-99![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "AJ-99:[E|Neutral] Well, you probably don't remember me, but I'm the one who fished you out of the ravine you fell into.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Oh![SOFTBLOCK] Thanks a lot![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "AJ-99:[E|Neutral] Don't mention it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "AJ-99:[E|Neutral] If you're heading out, climb the ladder and then check the wall on the east side.[SOFTBLOCK] We've got a secret passage set up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "AJ-99:[E|Neutral] And, obviously, don't tell any of the golems about it, because then the trap is worthless.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks for the tip.[SOFTBLOCK] I'd rather not repeat that particular episode.") ]])
	fnCutsceneBlocker()
end
