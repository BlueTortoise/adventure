--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Cutscene trigger.
if(sObjectName == "TalkTo55") then
	
	--Variables.
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	if(iSprung55 >= 1.0) then return end
	
	--Set the flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N", 1.0)
	
	--Move Christine up.
	fnCutsceneMove("Christine", 25.25, 5.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Oh my, 55's pretty badly damaged...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (No, wait.[SOFTBLOCK] This is just how she normally looks.[SOFTBLOCK] She never got her chassis fully repaired.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (And she shouldn't be in system standby, either.[SOFTBLOCK] There's nothing wrong with her...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] *55?[SOFTBLOCK] Can you hear me?*") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Turn 55 around.
	fnCutsceneFace("Christine", -1, 1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 1, 1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] *The coast is clear.[SOFTBLOCK] 55?[SOFTBLOCK] Please wake up...*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] *Activating...*") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneSetFrame("55", "Null")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
	fnCutsceneSetFrame("Christine", "Crouch")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Ow![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Do not resist...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 25.25, 9.50, 0.50)
	fnCutsceneMove("55", 25.25, 8.50, 0.50)
	fnCutsceneBlocker()
	fnCutsceneFace("DroidBC", 1, 0)
	fnCutsceneFace("DroidBD", -1, 0)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[VOICE|Steam Droid] Let her go![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] This human is my hostage.[SOFTBLOCK] Make one move, and I snap her neck.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] *cough*[SOFTBLOCK] Do what she says![SOFTBLOCK] Do what she says!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 25.25, 10.50, 0.50)
	fnCutsceneMove("55", 25.25, 9.50, 0.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneMoveFace("Christine", 19.25, 10.50, 0, 1, 0.50)
	fnCutsceneMoveFace("55", 19.25, 9.50, 0, 1, 0.50)
	fnCutsceneBlocker()
	
	--Move to the next map.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SprocketCityA", "FORCEPOS:24.0x6.0x0") ]])
	fnCutsceneBlocker()
	
end
