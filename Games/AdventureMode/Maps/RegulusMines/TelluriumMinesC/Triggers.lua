--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

	--Variables.
	local iSawMinesCCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesCCutscene", "N")
	if(iSawMinesCCutscene == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesCCutscene", "N", 1.0)
	
	--Movement.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	fnCutsceneMove("Christine", 24.25, 23.50)
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneMove("55", 23.25, 23.50)
	fnCutsceneFace("55", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Wait.[SOFTBLOCK] I hear something.") ]])
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneFace("55", 0, -1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Deactivate Player Light") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--SX-399 enters the scene.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "SX399")
	DL_PopActiveObject()
	fnCutsceneMove("SX399", 18.25, 19.50)
	fnCutsceneBlocker()
	fnCutsceneFace("SX399", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Young Droid:[E|Steam] Thought I heard someone for a moment there.[SOFTBLOCK] Best not tarry.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Face the wall.
	fnCutsceneFace("SX399", 0, -1)
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Young Droid:[E|Steam] Just a second -[SOFTBLOCK] got it!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	
	--Movement.
	fnCutsceneMove("SX399", 25.25, 19.50)
	fnCutsceneMove("SX399", 25.25, 15.50)
	fnCutsceneMove("SX399", 27.25, 15.50)
	fnCutsceneFace("SX399", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneTeleport("SX399", -100.25, -100.50)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Activate Player Light", 100, 100) ]])
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", -1, 0)
	fnCutsceneFace("55", 1, 0)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And there's our sabouteur.[SOFTBLOCK] Good.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is a Steam Droid.[SOFTBLOCK] Doubtless there are more nearby.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] She's wrecking our infrastructure![SOFTBLOCK] I've half a mind to go give her a good scolding![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It's a power conduit.[SOFTBLOCK] It's not a big deal.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Oh, not to you.[SOFTBLOCK] But you take out one conduit, and then the electrical load increases on the other conduits, and the chance of a blowout goes up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Not to mention the extra load increases corrosion on the contacts when you're past the mandated maximum voltage.[SOFTBLOCK] They're designed for plus ten, no more.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Calm down.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] Oh I'm very calm![SOFTBLOCK] Never been calmer![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Ugh.[SOFTBLOCK] Let's follow her.[SOFTBLOCK] If we catch her in time, maybe I can teach her the ins and outs of power transfer subsystems.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It's just sloppy![SOFTBLOCK] She could at least have bypassed the circuit before leaving it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] I am not the correct unit to complain to.[SOFTBLOCK] I imagine 499323 would be far more interested.[SOFTBLOCK] Save it for her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sophie gets me...[SOFTBLOCK][EMOTION|Christine|Offended] unlike *some* units...") ]])
	fnCutsceneBlocker()

	--55 moves onto Christine.
	fnCutsceneMove("55", 24.25, 23.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

end
