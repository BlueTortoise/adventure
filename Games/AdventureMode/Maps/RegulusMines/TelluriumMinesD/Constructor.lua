--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "TelluriumMinesD"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "RegulusTense")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("TelluriumMinesD")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
    
    --[Lights]
    AL_SetProperty("Activate Lights")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	local iSawMinesDCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutscene", "N")
	if(iSawMinesDCutscene == 0.0) then
		fnSpecialCharacter("SX399", "SteamDroid", 33, 10, gci_Face_North, false, nil)
	end
	
	--Disable this layer if 55 has opened the entrance.
	local iSawMinesDCutsceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutsceneB", "N")
	if(iSawMinesDCutsceneB == 1.0) then
		AL_SetProperty("Set Layer Disabled", "Floor2Fake", true)
	end
end
