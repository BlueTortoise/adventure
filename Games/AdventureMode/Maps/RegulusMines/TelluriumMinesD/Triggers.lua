--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Triggers]
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

	--Variables.
	local iSawMinesDCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutscene", "N")
	if(iSawMinesDCutscene == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutscene", "N", 1.0)
	
	--Movement.
	fnCutsceneMove("Christine", 17.25, 9.50)
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneMove("55", 17.25, 10.50)
	fnCutsceneFace("55", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Shh.[SOFTBLOCK] Look, there's our droid...") ]])
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Deactivate Player Light") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--SX-399 enters the scene.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "SX399")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Young Droid:[E|Steam] There, the station is operational.[SOFTBLOCK] Now let's see...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Young Droid:[E|Steam] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Young Droid:[E|Steam] Ah, good.[SOFTBLOCK] Transit manifests for the whole network.[SOFTBLOCK] Now we can hijack shipments at our leisure![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Young Droid:[E|Steam] I hope mother will be pleased...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneMove("SX399", 35.25, 10.50)
	fnCutsceneMove("SX399", 35.25, 5.50)
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Activate Player Light", 100, 100) ]])
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneTeleport("SX399", -100.25, -100.50)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Steam Droids are common bandits, it seems.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] They have never been directly implicated, but there are many reports on the security logs accusing them of thievery.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is why I have chosen to seek them out.[SOFTBLOCK] If not for their combat ability, their knowledge of secondary routes under Regulus City is invaluable.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She looked to be in pretty poor condition.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] They do not have fabrication machines, to my knowledge.[SOFTBLOCK] They likely have difficulty finding spare parts.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm.[SOFTBLOCK] Well, let's tail her.[SOFTBLOCK] She'll probably lead us right to their hideout.") ]])
	fnCutsceneBlocker()

	--55 moves onto Christine.
	fnCutsceneMove("55", 17.25, 9.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
--Start the second cutscene.
elseif(sObjectName == "ShesGone") then

	--Variables.
	local iSawMinesDCutsceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutsceneB", "N")
	if(iSawMinesDCutsceneB == 1.0) then return end
	
	--Other variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutsceneB", "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, 55?[SOFTBLOCK] Do you see what I see?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Caverns.[SOFTBLOCK] Empty track.[SOFTBLOCK] Confirm ocular receiver calibration code 67-B.[BLOCK][CLEAR]") ]])
	
	--Organic:
	if(sChristineForm == "Human" or sChristineForm == "Electrosprite" or sChristineForm == "Darkmatter" or sChristineForm == "Eldritch") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, confirming calibration code -[SOFTBLOCK] whatever you said.[SOFTBLOCK] I can't really recall them right now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But what I was pointing out was that the Steam Droid is nowhere in sight.[BLOCK][CLEAR]") ]])
	
	--Robot (or unhandled in case of debug usage)
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Confirm calibration code 67-B.[SOFTBLOCK] But what I meant was, the track is empty.[SOFTBLOCK] No Steam Droids.[BLOCK][CLEAR]") ]])
	end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A correct observation, but I do not require calibration on my ocular receivers.[SOFTBLOCK] You, however, do.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("55", 44.25, 4.50)
	fnCutsceneMove("55", 45.25, 3.50)
	fnCutsceneFace("55", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As expected.[SOFTBLOCK] There is a secret entrance here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The large cavity I had flagged is just below us.[SOFTBLOCK] This is a good location to put an entrance...[SOFTBLOCK] I just need to find the release latch...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(65)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Strike") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Floor2Fake", true) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move 55 back to Christine.
	fnCutsceneMove("55", 44.25, 4.50)
	fnCutsceneFace("55", -1, 0)
	fnCutsceneMove("Christine", 43.25, 4.50)
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Considering I am a Command Unit, they may attack us before we can recruit them to our cause.[SOFTBLOCK] Make sure your weapons are ready.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We should try not to hurt them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] Better they are destroyed during combat with our enemies than with us.[SOFTBLOCK] Disable when possible.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ah -[SOFTBLOCK] erm, technically true?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] Proceed.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move 55 onto Christine, fold the party.
	fnCutsceneMove("55", 43.25, 4.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Tell the player they can go to the black site.
elseif(sObjectName == "GoEast") then

	--Variables.
	local iTold55Go        = VM_GetVar("Root/Variables/Chapter5/Scenes/iTold55Go", "N")
	local iSawGoEastPrompt = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawGoEastPrompt", "N")
	if(iTold55Go == 0.0 or iSawGoEastPrompt == 1.0) then return end

	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawGoEastPrompt", "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (If we go east from here, we should be able to make our way to the detention site...)") ]])
	fnCutsceneBlocker()

--Finale.
elseif(sObjectName == "SX399Scene") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	if(iSXUpgradeQuest ~= 3.0) then return end

	--Execute.
	LM_ExecuteScript(gsRoot .. "Chapter5Scenes/SprocketFinale/Scene_C_TelluriumMinesD.lua")

--Go to the black site.
elseif(sObjectName == "WalkToBlackSite") then

	--Variables.
	local iSawGoEastPrompt = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawGoEastPrompt", "N")
	if(iSawGoEastPrompt == 0.0) then return end
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Walk to the detention site?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

--[Post Decision]
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
	
	--Transition.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("BlackSiteA", "FORCEPOS:2.0x30.0x0") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
	
	--Get Christine's position.
	EM_PushEntity("Christine")
		local iX, iY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Move, keep the Y position.
	fnCutsceneMove("Christine", 50.25, iY / 16.0)
	fnCutsceneMove("55", 50.25, iY / 16.0)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

end
