--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "TelluriumMinesE"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "RegulusTense")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("TelluriumMinesE")
	
	--[Spawn NPCS Here]
	--NPCs set to spawn here will be available for post-exec cutscenes.
    
    --[Lights]
    AL_SetProperty("Activate Lights")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--[Flag Reset]
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIsSecretPassageOpen", "N", 0.0)

	--[Spawn NPCs Here]
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	local iSawMinesECutsceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneA", "N")
	if(iSawMinesECutsceneA == 0.0) then
		fnSpecialCharacter("SX399", "SteamDroid", 37, 19, gci_Face_West, false, nil)
		fnSpecialCharacter("JX101", "SteamDroid", 27,  6, gci_Face_South, false, nil)		
	end
	
	--Blockers.
	TA_Create("BlockerS")
		TA_SetProperty("Position", 37, 53-24)
		TA_SetProperty("Clipping Flag", true)
	DL_PopActiveObject()
	TA_Create("BlockerN")
		TA_SetProperty("Position", 82-45, 46-24)
		TA_SetProperty("Clipping Flag", true)
	DL_PopActiveObject()
	
	--Collision Modifiers
	local iSawMinesECutsceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneB", "N")
	if(iSawMinesECutsceneB == 1.0) then
		AL_SetProperty("Set Layer Disabled", "Walls3Bridge", true)
		AL_SetProperty("Set Layer Disabled", "Floors3False", true)
		AL_SetProperty("Set Collision", 20, 20, 0, 1)
		AL_SetProperty("Set Collision", 20, 21, 0, 1)
		AL_SetProperty("Set Collision", 23, 20, 0, 1)
		AL_SetProperty("Set Collision", 23, 21, 0, 1)
	end
	
	--[SX-399]
	--She spawns here if certain cutscene flags are set.
	local iSteamDroid250RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
	if(iSteamDroid250RepState >= 3.0) then
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        if(iSXUpgradeQuest < 3.0) then
            fnSpecialCharacter("SX399", "SteamDroid", 25, 11, gci_Face_South, true, gsRoot .. "CharacterDialogue/SprocketCity/SX399.lua")
        else
            fnSpecialCharacter("SX399Lord", "SteamDroid", 25, 11, gci_Face_South, true, gsRoot .. "CharacterDialogue/SprocketCity/SX399.lua")
        end
        
	end
end
