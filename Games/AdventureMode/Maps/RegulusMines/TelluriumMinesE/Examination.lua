--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToMinesD") then
	AL_BeginTransitionTo("TelluriumMinesD", "FORCEPOS:45.0x3.0x0")
	
elseif(sObjectName == "ToSprocketA") then
	
	--Variables.
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --Ignore all special cases if the Tellurium Mines quests are completed.
    if(iSXUpgradeQuest >= 3.0) then
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|ClimbLadder") ]])
		fnCutsceneInstruction([[ AL_BeginTransitionTo("SprocketCityA", "FORCEPOS:14.0x37.0x0") ]])
        return
    end
	
	--If currently springing 55, don't allow this.
	if(iSprung55 == 2.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] It would be unwise if I were to return to Sprocket City.[SOFTBLOCK] Let's find a safe spot so I may brief you.") ]])
		fnCutsceneBlocker()
	
	else
	
		--If 55 is following, remove her here.
		if(iIs55Following == 1.0) then
		
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] I will conceal myself nearby.[SOFTBLOCK] Return to me when you are ready to head out.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(20)
			fnCutsceneBlocker()
			
			--Remove 55's sprite. She remains in the combat party for equipment reasons.
			gsFollowersTotal = 0
			gsaFollowerNames = {}
			giaFollowerIDs = {0}
			--AC_SetProperty("Set Party", 1, "Null")
			AL_SetProperty("Unfollow Actor Name", "55")

			--Flag to indicate she is not following Christine.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
		end
	
		--Christine needs to change form.
		if(sChristineForm ~= "Human") then
		
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I should transform, otherwise I'll really upset the Steam Droids...)") ]])
			fnCutsceneBlocker()
		
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
		end
	
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|ClimbLadder") ]])
		fnCutsceneInstruction([[ AL_BeginTransitionTo("SprocketCityA", "FORCEPOS:14.0x37.0x0") ]])
	end
		
elseif(sObjectName == "SneakS" or sObjectName == "SneakN") then
	
	--Variables.
	local iKnowsAboutSecretPassage = VM_GetVar("Root/Variables/Chapter5/Scenes/iKnowsAboutSecretPassage", "N")
	local iIsSecretPassageOpen     = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsSecretPassageOpen", "N")
	if(iKnowsAboutSecretPassage == 1.0 and iIsSecretPassageOpen == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIsSecretPassageOpen", "N", 1.0)
		
		--Wait a bit.
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		
		--Teleport the blocker entities out.
		fnCutsceneTeleport("BlockerS", -100.25, -100.50)
		fnCutsceneTeleport("BlockerN", -100.25, -100.50)
		
		--Open it up.
		fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "WallHi2False", true) ]])
		fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Walls3False", true) ]])
		fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Strike") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		
	end
	

--[Examinables]
elseif(sObjectName == "DropTrap") then

    --Variables.
	local iSawMinesECutsceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneB", "N")
	if(iSawMinesECutsceneB == 0.0) then return end

    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A six-story fall onto jagged rocks, while unpleasant, is still not the worst thing in the world.[SOFTBLOCK] That honour goes to terrible American remakes of British TV shows!)") ]])
    fnCutsceneBlocker()

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end