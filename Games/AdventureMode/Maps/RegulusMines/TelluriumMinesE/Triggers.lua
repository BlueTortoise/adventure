--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Start the mines cutscene.
if(sObjectName == "Overhearing") then

	--Variables.
	local iSawMinesECutsceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneA", "N")
	if(iSawMinesECutsceneA == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneA", "N", 1.0)
	
	--Movement.
	fnCutsceneMove("Christine", 62.25-45, 54.50-24)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneMove("55", 61.25-45, 54.50-24)
	fnCutsceneFace("55", 0, -1)
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Deactivate Player Light") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I think I see a droid over there.[SOFTBLOCK] Stay out of sight...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--SX-399 enters the scene.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "SX399")
	DL_PopActiveObject()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("SX399", 80.25-45, 43.50-24)
	fnCutsceneMove("SX399", 80.25-45, 41.50-24)
	fnCutsceneMove("SX399", 77.25-45, 41.50-24)
	fnCutsceneMove("SX399", 77.25-45, 37.50-24)
	fnCutsceneMove("SX399", 73.25-45, 37.50-24)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] SX-399![SOFTBLOCK] There you are![SOFTBLOCK] I've been worried sick![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Mother?[SOFTBLOCK] Is that you?[SOFTBLOCK] Don't you need me to say the passphrase?") ]])
	fnCutsceneBlocker()
	fnCutsceneFace("SX399", 0, -1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("JX101", 72.25-45, 37.50-24)
	fnCutsceneFace("JX101", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneFace("SX399", -1, 0)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Steam") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Pah, as if you care about security![SOFTBLOCK] What good is a passphrase when you sneak out without permission?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Humour me for a moment.[SOFTBLOCK] Imagine if a golem had seen you?[SOFTBLOCK] What then?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I -[SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Oh, I know what'd happen.[SOFTBLOCK] They'd either capture you and have you scrapped, or perhaps they'd follow you here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] And then they'd know where our settlement was, and send a team down to retire us all.[SOFTBLOCK] Would you prefer that?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Mother, no one saw me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] You're certain of this, I take it.[SOFTBLOCK] After all, you were checking behind you, removing all traces of your passing, and disabling security robots?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Sooner or later, you will be unlucky.[SOFTBLOCK] It only takes one mistake.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] But - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] And is this chemical powder on your hands the result of tampering?[SOFTBLOCK] Yes, you look like you've been in golem electronics again.[SOFTBLOCK] What did you steal?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I pulled out a power conduit, and fixed the network terminal near the entrance.[SOFTBLOCK] Now we can see all their shipping manifests![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Y-you're -[SOFTBLOCK] you're not proud of me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I'm not proud of your foolishness, that much is certain![SOFTBLOCK] If the golems do not have a repair unit fix a terminal, and it suddenly starts working, that will draw their suspicion![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Doubtless they will send a repair unit after the conduit you sabotaged and realize where the parts went.[SOFTBLOCK] You put us all at risk with your carelessness.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Argh![SOFTBLOCK] Is nothing good enough for you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I always do my best and try to help, but all I get is shouted at![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Do not raise your voice at me, young lady.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] We will talk about this later.[SOFTBLOCK] I have a meeting I must attend.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Grrrr...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I said, we will continue this conversation later.[SOFTBLOCK] Is that understood?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Yes, mother.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] SX-399...[SOFTBLOCK] I worried the whole time you were out.[SOFTBLOCK] Please - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] I don't even care![SOFTBLOCK] I'll be in my room!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "JX101")
	DL_PopActiveObject()
	fnCutsceneMove("SX399", 73.25-45, 35.50-24)
	fnCutsceneMove("SX399", 72.25-45, 35.50-24)
	fnCutsceneBlocker()
	fnCutsceneFace("JX101", 0, -1)
	fnCutsceneMove("SX399", 72.25-45, 30.50-24)
	fnCutsceneMove("SX399", 74.25-45, 30.50-24)
	fnCutsceneMove("SX399", 74.25-45, 29.50-24)
	fnCutsceneMove("SX399", 76.25-45, 29.50-24)
	fnCutsceneFace("SX399", 0, 1)
	fnCutsceneTeleport("SX399", -100.25, -100.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] *Sigh*...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move the camera to Christine.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Target identified::[SOFTBLOCK] JX-101.[SOFTBLOCK] Leader of the Fist of the Future faction.[SOFTBLOCK] One of the more important Steam Droids, she is an ideal candidate for recruitment.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] So we're just going to ignore that little spat they had?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The psychological profiles I found indicate that JX-101 cares for her daughter, SX-399.[SOFTBLOCK] Kidnapping her would be a good way to ensure the loyalty of her faction.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55![SOFTBLOCK] What's gotten in to you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We should be careful not to aggravate the situation further.[SOFTBLOCK] A falling out would reduce the effectiveness of blackmail.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Okay, that's wrong on a lot of levels.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] First, an argument isn't going to make a mother love her daughter any less...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] And -[SOFTBLOCK] kidnapping?[SOFTBLOCK] Honestly, 55, is that what your plan was?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Yes.[SOFTBLOCK] Yes it was.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The Steam Droids are old technology.[SOFTBLOCK] Their aim is shaky, their foot-speed is slower, and their chassis is more vulnerable to damage.[SOFTBLOCK] They are poor soldiers.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] During a direct confrontation with Regulus City security forces, they would be decimated.[SOFTBLOCK] Cultivating long-term relations is a waste of energy.[SOFTBLOCK] They are expendable.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I am not surprised, just disappointed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You disagree?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, your assessment is probably spot on.[SOFTBLOCK] They don't look to be in good condition, I'll give you that.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But people are not to be used up and thrown away.[SOFTBLOCK] You treat them like that, and you can expect them to treat you the same way.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] So long as we are the ones discarding them, I see no problem with it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] And will you discard me when I am no longer useful?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[SOFTBLOCK] No.[SOFTBLOCK] Because...[SOFTBLOCK] because you will not cease to be useful.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 'And them that take the sword shall perish by the sword.'[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not use such primitive melee weapons.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It's a metaphor, dumb bolt.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We do not have time for pointless philosophizing.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Just -[SOFTBLOCK] let me do the talking.[SOFTBLOCK] We'll discuss your intentions later.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Come on, their settlement can't be far away.[SOFTBLOCK] And keep your eyes peeled for traps.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneTeleport("JX101", -100.25, -100.50)

	--55 moves onto Christine.
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutsceneInstruction([[ AL_SetProperty("Activate Player Light", 100, 100) ]])
	fnCutsceneWait(5)
	fnCutsceneMove("55", 62.25-45, 54.50-24)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
--Trap!
elseif(sObjectName == "DashItAll") then

	--Variables.
	local iSawMinesECutsceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneB", "N")
	if(iSawMinesECutsceneB == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneB", "N", 1.0)
	
	--Movement.
	fnCutsceneMove("Christine", 67.25-45, 44.50-24)
	fnCutsceneMove("55", 66.25-45, 44.50-24)
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--SFX.
	fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Strike") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Walls3Bridge", true) ]])
	fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "Floors3False", true) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneFace("55", 0, 1)
	fnCutsceneWait(55)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Oh, dash it all...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Move them downwards.
	for y = 20.50, 31.50, 0.50 do
		fnCutsceneTeleport("Christine", 67.25-45, y)
		fnCutsceneTeleport("55", 66.25-45, y)
		fnCutsceneWait(1)
		fnCutsceneBlocker()
	end
	
	--Black the screen out.
	fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Strike_Crit") ]])
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(240)
	fnCutsceneBlocker()
	
	--Transition to the next scene.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SprocketCityA", "FORCEPOS:59.0x11.0x0") ]])
	fnCutsceneBlocker()
	
--55 escapes.
elseif(sObjectName == "Spring55") then

	--Variables.
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	if(iSprung55 ~= 1.0) then return end

	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N", 2.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowsAboutSecretPassage", "N", 1.0)

	--Spawn 55.
	fnSpecialCharacter("55", "Doll", 31, 13, gci_Face_South, false, nil)
	fnCutsceneTeleport("Christine", 31.25, 14.50)
	fnCutsceneSetFrame("Christine", "Crouch")

	--Blackout.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Movement.
	fnCutsceneMove("Christine", 31.25, 15.50, 0.50)
	fnCutsceneMove("55", 31.25, 14.50, 0.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMoveFace("Christine", 32.25, 15.50, 0, 1, 0.50)
	fnCutsceneMoveFace("55", 32.25, 14.50, 0, 1, 0.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *cough*[SOFTBLOCK] I don't think they're following us...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] It seems not, but they will likely form a search party soon.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Uncrouch.
	fnCutsceneSetFrame("Christine", "Null")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Phew![SOFTBLOCK] That really hurt, 55![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I needed the performance to be convincing.[SOFTBLOCK] You should not be permanently damaged.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ugh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I believe I heard some of the Steam Droids mention a secret passage.[SOFTBLOCK] Let's find it.[SOFTBLOCK] I will brief you when we are clear.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("55", 32.25, 15.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)

	--Lua globals.
	gsFollowersTotal = 1
	gsaFollowerNames = {"55"}
	giaFollowerIDs = {0}

	--Get 55's uniqueID. 
	EM_PushEntity("55")
		local iCharacterID = RE_GetID()
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iCharacterID}
	AL_SetProperty("Follow Actor ID", iCharacterID)

	--Place 55 in the combat lineup.
	AC_SetProperty("Set Party", 1, "55")
	
--Safe spot.
elseif(sObjectName == "GoToSafeSpot" or sObjectName == "GoToSafeSpotAlternate") then

	--Variables.
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	if(iSprung55 == 2.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N", 3.0)
		
		--Movement.
		fnCutsceneMove("Christine", 34.25, 37.50)
		fnCutsceneMove("Christine", 37.25, 37.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneMove("55", 34.25, 37.50)
		fnCutsceneMove("55", 37.25, 38.50)
		fnCutsceneFace("55", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay, this should be clear enough.[SOFTBLOCK] Now, you best have a good reason for putting me in a headlock like that![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] If it was not obvious, I was making sure you did not compromise your cover by being seen with me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The trap was unexpected.[SOFTBLOCK] I had never intended to enter the settlement with you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It was my assumption the Steam Droids would think you were an escaped human, and come to accept you.[SOFTBLOCK] Is that correct?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yes.[SOFTBLOCK] I don't think they suspect me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] They will not trust anyone from Regulus City, and will never believe that I would wipe my own memories.[SOFTBLOCK] More than likely they would have me scrapped.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] JX-101 mentioned that...[SOFTBLOCK] but I'm sure if you talk to her - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] No.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The Steam Droids do not attack golems strictly for political reasons.[SOFTBLOCK] If they knew I was a wanted fugitive, they would just as soon turn me in.[SOFTBLOCK] The administration would likely reward them with parts or technology.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] They have much to gain by betraying me, and much to lose by accepting me.[SOFTBLOCK] It is best if I am not seen there.[SOFTBLOCK] You must conduct this diplomacy on your own.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] *sigh*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Fine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, why did you play dead like that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The Steam Droids lack the technical expertise to evaluate my architecture.[SOFTBLOCK] I overheard a number of useful facts while they thought I was in standby.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] SX-399 is the daughter of JX-101, and the guards mentioned that she needs to receive special recharging of some sort.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I did not receive the specifics, but this may be a way to ingratiate her to us.[SOFTBLOCK] If you can isolate her, we can more easily kidnap her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] No![SOFTBLOCK] No kidnapping![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Look, I'll look into SX-399 and see what I can find.[SOFTBLOCK] But don't attack anyone![SOFTBLOCK] Don't make this worse![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will conceal myself here.[SOFTBLOCK] If you need to venture into the mines, I will accompany you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay, fine, good.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You know, I'm starting to think you prefer crawling around in the vents and shadows.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I am a high-value target to the majority of Regulus' population.[SOFTBLOCK] Social interactions are dangerous.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Right, so when this is all over, the first thing I'm doing is giving you a crash-course in etiquette.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I think lesson one should be 'Do not place your friends in headlocks'.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We should move out, and not dally with formalities.[SOFTBLOCK] Let's go.") ]])
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutsceneMove("55", 37.25, 38.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	--If 55 is not following, spawn her and add her to the party.
	else
	
		--Variables.
		local iIs55Following         = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
		local iSpokeWithTT233        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N")
		local iSpokeWith55AboutSX    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWith55AboutSX", "N")
		local iTold55Go              = VM_GetVar("Root/Variables/Chapter5/Scenes/iTold55Go", "N")
		local iSteamDroid500RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N")
		if(iIs55Following == 1.0) then return end

		--Spawn 55.
		fnSpecialCharacter("55", "Doll", 41, 38, gci_Face_West, false, nil)
		
		--Move 55 out of the wall.
        if(sObjectName == "GoToSafeSpot") then
            fnCutsceneMove("Christine", 34.25, 37.50)
            fnCutsceneFace("Christine", 1, 0)
            for x = 41.25, 40.25, -0.05 do
                fnCutsceneTeleport("55", x, 38.50)
            end
            fnCutsceneMove("55", 36.25, 38.50)
            fnCutsceneMove("55", 35.25, 37.50)
            fnCutsceneFace("55", -1, 0)
            fnCutsceneBlocker()
        
        --Alternate, using the campfire.
        else
            fnCutsceneFace("Christine", 1, 0)
            for x = 41.25, 40.25, -0.05 do
                fnCutsceneTeleport("55", x, 38.50)
            end
            fnCutsceneMove("55", 39.25, 38.50)
            fnCutsceneFace("55", -1, 0)
            fnCutsceneBlocker()
        end
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue, normal.
		if(iSpokeWithTT233 == 0.0 or iSpokeWith55AboutSX == 1.0) then
			
			--Need to tell 55 that the plan with the black site is a go.
			if(iTold55Go == 0.0 and iSteamDroid500RepState == 1.0) then
				
				--Flag.
				VM_SetVar("Root/Variables/Chapter5/Scenes/iTold55Go", "N", 1.0)
		
				--Dialogue.
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I have seen to it that JX-101 found a suitable location.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She just got through telling me about it.[SOFTBLOCK] Everything else is in place?[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] We will proceed to the site.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will not be able to accompany you, but I will be able to provide network support.[SOFTBLOCK] I should be able to hack open doors for you.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Contact me via your PDU.[SOFTBLOCK] I have set aside a special frequency for it.[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You know, 55, this is just like old times.[SOFTBLOCK] Me risking my neck, you opening doors and angrily giving me orders...[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am not programmed for nostalgia.[SOFTBLOCK] Let us proceed.") ]])
				fnCutsceneBlocker()
				
			--Normal.
			else
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Let us proceed.") ]])
				fnCutsceneBlocker()
			end
			
		--Special:
		else
		
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWith55AboutSX", "N", 1.0)
		
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your face betrays concern.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I suppose it's encouraging that you noticed.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Advertising your emotions is a weakness.[SOFTBLOCK] Your enemies will use any piece of information they can find to destroy you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You love that stonefaced look of yours.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It is for practical reasons.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So...[SOFTBLOCK] I just talked to TT-233.[SOFTBLOCK] She's one of the mechanics who has tried to work on SX-399.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] She said...[SOFTBLOCK] She said SX-399 doesn't have long...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] What [SOFTBLOCK](partial data retrieval error)[SOFTBLOCK] does that phrase mean?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] She's going to die, 55.[SOFTBLOCK] Her systems are fried.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] But we.[SOFTBLOCK][SOFTBLOCK] Can she.[SOFTBLOCK][SOFTBLOCK] We can repair her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] All units can be repaired.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] TT-233 said they've tried everything.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] We will not allow her to be retired.[SOFTBLOCK][EMOTION|2855|Angry] You're a repair unit, think of something![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] 55 -[SOFTBLOCK] 55 let go of me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK][EMOTION|2855|Down] I apologize.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] TT-233 thinks it might be her power distribution system.[SOFTBLOCK] But she said the only way to find out would likely offline her permanently.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I mean, if I could get her to Sector 96 we could just hook her to the backup distributor...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Then let's take her to Sector 96![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Woah, calm down there.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Even if we confirm the distribution system is the problem, that means we'd have to replace it.[SOFTBLOCK] As in, replace the wiring and pumps in her whole body.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] They don't manufacture Steam Droid parts in Regulus City, so I don't know what we'd do even if we got her there...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] If she were attached to the backup system, she would remain online indefinitely.[SOFTBLOCK] It is better than letting her expire.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're acting a little odd, 55.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am concerned about SX-399, as she is our means of acquiring JX-101's aid.[SOFTBLOCK] If she is offlined, we lose our best chance.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But taking her to Sector 96 presents problems of its own.[SOFTBLOCK] You take her, and JX-101 will have every Steam Droid on the moon after her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I am certain I can outmaneuver them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] It's still too risky...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unless...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unless what.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] What if SX-399 never left Sprocket City?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] What if someone took her place while she was away?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Constructing a crude replacement dummy would not work.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No, you lug-nut, me![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I get her to transform me into a Steam Droid, and then we disguise me to look like her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'll take her place, and you can take her to the repair bay.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] But you'll be here, therefore, unable to maintain SX-399.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Sophie's twice the repair unit I am![SOFTBLOCK] I'll send her a message from the PDU![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] This plan...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It'll work.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I was not doubting it.[SOFTBLOCK] I am merely unsure about SX-399's fate.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She'll be in the best of hands.[SOFTBLOCK] I trust Sophie, I know she'll know what to do.[SOFTBLOCK] Come on, let's go tell her.") ]])
			fnCutsceneBlocker()
		
		end
	
		--Move 55 and fold the party.
        if(sObjectName == "GoToSafeSpot") then
            fnCutsceneMove("55", 34.25, 37.50)
        else
            fnCutsceneMove("55", 38.25, 38.50)
        end
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()

		--Lua globals.
		gsFollowersTotal = 1
		gsaFollowerNames = {"55"}
		giaFollowerIDs = {0}

		--Get 55's uniqueID. 
		EM_PushEntity("55")
			local iCharacterID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs = {iCharacterID}
		AL_SetProperty("Follow Actor ID", iCharacterID)

		--Place 55 in the combat lineup.
		AC_SetProperty("Set Party", 1, "55")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
	
	end
end
