--[Warp Handler]
--If the player warps here using the campfire, this script is called to reposition them as necessary.

--Constants. This is the important part that changes between each room.
local fTargetX = (38.25 * gciSizePerTile)
local fTargetY = (38.50 * gciSizePerTile)

--If 55 was not in the party when the warp started, add her.
local bWas55PresentAtStart = fnIsCharacterPresent("55")
if(bWas55PresentAtStart == false) then
    --fnAddPartyMember("55")
end

--Execute.
local bAlreadySpawned55 = false
local iSaidWarpDialogue = VM_GetVar("Root/Variables/Global/2855/iSaidWarpDialogue", "N")
if(bWas55PresentAtStart == false) then
    
    --Spawn 55.
    fnSpecialCharacter("55", "Doll", 39, 38, gci_Face_South, false, nil)

	--Lua globals.
	gsFollowersTotal = 1
	gsaFollowerNames = {"55"}
	giaFollowerIDs = {0}

	--Get 55's uniqueID. 
	EM_PushEntity("55")
		local iCharacterID = RE_GetID()
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iCharacterID}
	AL_SetProperty("Follow Actor ID", iCharacterID)
    bAlreadySpawned55 = true
end

--Handler.
LM_ExecuteScript(gsStandardWarpHandler, fTargetX, fTargetY)

--If 55 was not present before the warp started, and we have not seen this dialogue yet, show it.
if(iSaidWarpDialogue == 0.0 and bWas55PresentAtStart == false) then
    
    --Flag.
    VM_SetVar("Root/Variables/Global/2855/iSaidWarpDialogue", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    
    --Handler.
    fnCutsceneMoveFace("55", 39.25, 38.50, 0, -1, 0.20)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Downed")
    fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] !!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] 55?[SOFTBLOCK] Are you okay?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Interesting.[SOFTBLOCK] The reports *did* indicate your runestone allowed limited-scope teleportation of an unknown mechanism.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Then why are you surprised?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Because I was attempting to climb a ladder when this happened.[SOFTBLOCK] I was not, as is usual, standing right next to the teleportation vector.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In most circumstances, the caster is in physical contact with any passengers.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Oh...[SOFTBLOCK] Yes, I suppose you weren't.[SOFTBLOCK] Sorry.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Maybe my rune's magic pulled you along because you're such a close friend?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] An unproveable supposition.[SOFTBLOCK] You are not the ideal candidate to advance the Cause of Science.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Gee, thanks, 55.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The magic on the runestone has proved convenient for our purposes.[SOFTBLOCK] I am undamaged.[SOFTBLOCK] Let us continue.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold the party.
    fnCutsceneSetFrame("55", "Null")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("55", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("55", 38.25, 38.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

--No need to play this scene, but also don't need to show 55 walking up.
elseif(iSaidWarpDialogue == 1.0 and bWas55PresentAtStart == false) then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    if(bAlreadySpawned55 == false) then
        fnSpecialCharacter("55", "Doll", 38, 38, gci_Face_South, false, nil)
    end
    fnCutsceneTeleport("55", 38.25, 38.50)

end