--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Next randomly generated level.
if(sObjectName == "LadderD") then

    --Variables.
    local iDebugBypassRandomLevels = VM_GetVar("Root/Variables/Chapter5/Scenes/iDebugBypassRandomLevels", "N")
    
	--SFX.
	AudioManager_PlaySound("World|ClimbLadder")
    
    --Bypass random level generation and go directly to the next story floor.
    if(iDebugBypassRandomLevels == 1.0) then
        AL_BeginTransitionTo("TelluriumMinesG", "FORCEPOS:15.0x4.0x0")
    
    --Run the RLG.
    else
        VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 31)
        AL_BeginTransitionTo("RANDOMLEVEL", "Null")
    end

--Back to the entrance.
elseif(sObjectName == "LadderU") then
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Climb the ladder back to the surface?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

--Elevator, select any of the visited floors in increments of 10.
elseif(sObjectName == "Elevator") then

    --Variables.
    local iFixedElevator30 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator30", "N")
    if(iFixedElevator30 == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedElevator30", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Amazingly the elevator's circuits aren't swiss cheese with all the radiation damage...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (All fixed![SOFTBLOCK] Now I can travel to Floor 30 quickly by accessing the elevator!)") ]])
        fnCutsceneBlocker()
    else
	
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Take the elevator back to the surface?)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesE\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
        
    end

--[Examinables]
--[Decisions]
--Really exit the mines.
elseif(sObjectName == "Yes") then

	--Go to Mines B.
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:19.0x24.0x0")

--Really exit the mines.
elseif(sObjectName == "YesE") then

	--Go to Mines B.
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:15.5x23.0x0")

--Nope.
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end