--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Triggers]
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

    --Variables.
    local iSawMinesGCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscene", "N")
    if(iSawMinesGCutscene == 1.0) then return end
    
    --More variables.
    local iHasSteamDroidForm = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscene", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 9.25, 15.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("55", 8.25, 15.50)
    fnCutsceneFace("55", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Contact![SOFTBLOCK] Two from the north![SOFTBLOCK] Get down![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Wait![SOFTBLOCK] We come in peace![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Hands up![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Weapons down, 55.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Negotiating is your strong point.[SOFTBLOCK] However I would like to point out that pneumatic weaponry is not a threat to my chassis.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Yes, pointing out your invulnerability is sure to inspire trust.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're friends.[SOFTBLOCK] May we come closer?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Make a fast move and I'll gun you down.[SOFTBLOCK] Nice and easy now.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 10.25, 16.50, 0.75)
    fnCutsceneMove("Christine", 10.25, 18.50, 0.75)
    fnCutsceneMove("55", 9.25, 16.50, 0.75)
    fnCutsceneMove("55", 9.25, 18.50, 0.75)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Christine has Steam Droid form. This means Sprocket City knows about her.
    if(iHasSteamDroidForm == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMinesGDroidKnowsChristine", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Identify yourself.[SOFTBLOCK] Who are you?[SOFTBLOCK] What are you doing here?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] If I said I was Christine, and that this is 55, would that mean anything to you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] ...[SOFTBLOCK] Christine?[SOFTBLOCK] The one from Sprocket City?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Who busted up the Black Site?[SOFTBLOCK] Who saved SX-399?[SOFTBLOCK] Who is practically JX-101's right-hand?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I didn't think I was that famous![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] You do right by us, it gets around.[SOFTBLOCK] Yes, we've heard of Christine.[SOFTBLOCK] And yes, we've heard of 55.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I would prefer if you hadn't.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She's a little gruff, don't mind her.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Well, we're in a bit of a tough spot.[SOFTBLOCK] Are you here to help again?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What's the problem?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] This is about as far as we can go down.[SOFTBLOCK] After this, things start to get too hairy.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] We found a couple of golems earlier.[SOFTBLOCK] They keep saying something bad is ten floors down.[SOFTBLOCK] Something we need to deal with.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I really just came down here to fix the elevator.[SOFTBLOCK] But I guess some cleanup duty could be added.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Fix the elevator?[SOFTBLOCK] You mean we can get back to Sprocket City without having to fight our way up twenty floors?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, yeah?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Now I know why Sprocket City's folk mention you every time we swing by there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] What can I say?[SOFTBLOCK] I like helping.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Talk to the golems we have over there.[SOFTBLOCK] If what they say is true, your services might be needed below.[SOFTBLOCK] Good luck.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --All other forms:
    else
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 1.0)
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Identify yourself.[SOFTBLOCK] Who are you?[SOFTBLOCK] What are you doing here?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not really sure if I should tell you who I am.[SOFTBLOCK] To protect myself, and my friend here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] You're going to need to prove I can trust you before I let you walk away from here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh that'll be easy.[SOFTBLOCK] I was just going to fix the elevator over there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Then you'll be able to use it to get around easily.[SOFTBLOCK] Just don't go to the top floor because the golems will probably shoot you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] ...[SOFTBLOCK] You're serious?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Probably not much point in stopping me.[SOFTBLOCK] It's not like you can use the broken elevator anyway.[SOFTBLOCK] Let me take a look at it for you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Don't try anything.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We won't.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    end

    --Fold the party up.
    fnCutsceneMove("55", 10.25, 18.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

--Second cutscene.
elseif(sObjectName == "StartCutscene2") then

    --Variables.
    local iSawMinesGCutscenePost = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N")
    if(iSawMinesGCutscenePost == 0.0 or iSawMinesGCutscenePost == 2.0) then return end
    
    --Elevator needs to be fixed.
    local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
    if(iFixedElevator40 ~= 1.0) then return end
    
    --More variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
    
    --Movement.
    fnCutsceneMove("Christine", 13.25, 20.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneMove("55", 13.25, 21.50)
    fnCutsceneFace("55", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("DroidGuardA", 1, 0)
    fnCutsceneFace("DroidGuardB", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Elevator's fixed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] While I will wait until I've actually checked it myself for that judgement, you didn't obviously sabotage it.[BLOCK][CLEAR]") ]])
    
    --Human Form:
    if(sChristineForm == "Human") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] I don't know if you're a runaway or something, but your Command Unit friend isn't inspiring confidence.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She's a maverick.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Yeah, sure.[SOFTBLOCK] Whatever you say.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] So listen up, human.[SOFTBLOCK] If you're really committed to helping, these golems we captured have been saying there's something ten floors down we need to deal with.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] We're not leaving this position.[SOFTBLOCK] You want to prove yourself?[SOFTBLOCK] Ladder's right over there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Talk to them, see what details you can get out of them.[SOFTBLOCK] Try not to get yourself killed down there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] And be careful.[SOFTBLOCK] You humans are probably fresh recruits for the things crawling in the mines.[SOFTBLOCK] I don't need more enemies.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Don't worry about us, we can handle ourselves.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --Golem Form:
    elseif(sChristineForm == "Golem") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] So are you two mavericks or something?[SOFTBLOCK] Foes of Regulus City?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's precisely the case![SOFTBLOCK] Though don't spread it around.[SOFTBLOCK] I don't want the other golems knowing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Oh yeah, I totally believe you.[SOFTBLOCK] Really.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] So listen up, then.[SOFTBLOCK] If you're really committed to helping, these golems we captured have been saying there's something ten floors down we need to deal with.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] We're not leaving this position.[SOFTBLOCK] You want to prove yourself?[SOFTBLOCK] Ladder's right over there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Talk to them, see what details you can get out of them.[SOFTBLOCK] Try not to get yourself killed down there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] And be careful.[SOFTBLOCK] We've seen other golems down here, and they aren't exactly lucid.[SOFTBLOCK] Whatever happens to them can happen to you.[SOFTBLOCK] I don't need more enemies.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Don't worry about us, we can handle ourselves.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --Latex Drone:
    elseif(sChristineForm == "LatexDrone") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] So are you two mavericks or something?[SOFTBLOCK] Foes of Regulus City?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's precisely the case![SOFTBLOCK] Appearances can be deceiving.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Oh yeah, I totally believe you.[SOFTBLOCK] Really.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] So listen up, then.[SOFTBLOCK] If you're really committed to helping, these golems we captured have been saying there's something ten floors down we need to deal with.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] We're not leaving this position.[SOFTBLOCK] You want to prove yourself?[SOFTBLOCK] Ladder's right over there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Talk to them, see what details you can get out of them.[SOFTBLOCK] Try not to get yourself killed down there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] And be careful.[SOFTBLOCK] We've seen other Regulus City types down here, and they aren't exactly lucid.[SOFTBLOCK] Whatever happens to them can happen to you.[SOFTBLOCK] I don't need more enemies.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Don't worry about us, we can handle ourselves.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --Other:
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Not to be too judgemental, but I've seen a lot of weird stuff lately.[SOFTBLOCK] Sorry if I jumped to conclusions about -[SOFTBLOCK] whatever you are.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Appearances can be deceiving.[SOFTBLOCK] I don't mean to intimidate you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Doesn't mean I trust you, yet.[SOFTBLOCK] But you're on the right track.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] So listen up, then.[SOFTBLOCK] If you're really committed to helping, these golems we captured have been saying there's something ten floors down we need to deal with.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] We're not leaving this position.[SOFTBLOCK] You want to prove yourself?[SOFTBLOCK] Ladder's right over there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] Talk to them, see what details you can get out of them.[SOFTBLOCK] Try not to get yourself killed down there.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Steam Droid:[E|Neutral] And be careful.[SOFTBLOCK] Things in the mines can mess with your head.[SOFTBLOCK] I've had to shoot fellow Steam Droids, I'd rather not have to shoot you if I can avoid it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Don't worry about us, we can handle ourselves.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    end

    --Fold the party up.
    fnCutsceneMove("55", 13.25, 20.50)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
end
