--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

	--Variables.
	local iCutscene50 = VM_GetVar("Root/Variables/Chapter5/Scenes/iCutscene50", "N")
	if(iCutscene50 == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCutscene50", "N", 1.0)
    
    --More Variables.
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Dialogue.
    if(iHasDarkmatterForm == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55...[SOFTBLOCK] I've been here before.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] When?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] No, not before...[SOFTBLOCK] after.[SOFTBLOCK] I've been here after...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] In the future.[SOFTBLOCK] I spent millions of years here...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I will wander in a haze.[SOFTBLOCK] Joining the flesh, unjoining the flesh.[SOFTBLOCK] Part and not part.[SOFTBLOCK] Forever.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] The stalks grow and grow to infinity and I look up and see -[SOFTBLOCK] the great billowing cloud of One.[SOFTBLOCK] Master.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, cognitive systems check.[BLOCK][CLEAR]") ]])
        if(sChristineForm == "Human") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I can't do a cognitive systems check on wetware, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[SOFTBLOCK] I'm just remembering something that hasn't happened yet.[BLOCK][CLEAR]") ]])
            
        elseif(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] All checksums are green, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[SOFTBLOCK] I'm just remembering something that hasn't happened yet.[BLOCK][CLEAR]") ]])
            
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Cognitive systems?[SOFTBLOCK] What a quaint notion.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[SOFTBLOCK] I'm just remembering something that hasn't happened yet.[BLOCK][CLEAR]") ]])
        end
        
        --Resume.
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] So you claim.[SOFTBLOCK] Focus on the present, not the future.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you have been here before as you assert, you can tell what is waiting ahead.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No, it's not like that...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This place is young.[SOFTBLOCK] So young.[SOFTBLOCK] The walls do not bleed, there are no hands.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'He' is not here, yet.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] He?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] I don't know.[SOFTBLOCK] I don't know who 'He' is, or even if 'He' is the right term.[SOFTBLOCK] But 'He' is not here yet.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But there is someone waiting for us up ahead.[SOFTBLOCK] We will have to destroy her.[SOFTBLOCK] It won't matter, but we'll do it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We should turn back now.[SOFTBLOCK] We have nothing to gain from this.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And yet, somehow, we're going to go ahead.[SOFTBLOCK] I know it.[SOFTBLOCK] I will, I have, I always have.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Then lead the way.[SOFTBLOCK] I will be monitoring you very closely.") ]])
        fnCutsceneBlocker()
    
    --Has Darkmatter form.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55...[SOFTBLOCK] I've been here before.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I recall your report about walls made of meat from the Serenity Crater incident.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, but -[SOFTBLOCK] that place was not here.[SOFTBLOCK] It was not even in this dimension, this universe.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] This is that other universe, making its place here.[SOFTBLOCK] Taking it.[SOFTBLOCK] We are not there, but we will be.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] So you have not been here before?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] No, not before...[SOFTBLOCK] after.[SOFTBLOCK] I've been here after...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] In the future.[SOFTBLOCK] I spent millions of years here...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I will wander in a haze.[SOFTBLOCK] Joining the flesh, unjoining the flesh.[SOFTBLOCK] Part and not part.[SOFTBLOCK] Forever.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] The stalks grow and grow to infinity and I look up and see -[SOFTBLOCK] the great billowing cloud of One.[SOFTBLOCK] Master.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, cognitive systems check.[BLOCK][CLEAR]") ]])
        if(sChristineForm == "Human") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I can't do a cognitive systems check on wetware, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[SOFTBLOCK] I'm just remembering something that hasn't happened yet.[BLOCK][CLEAR]") ]])
            
        elseif(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] All checksums are green, 55.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[SOFTBLOCK] I'm just remembering something that hasn't happened yet.[BLOCK][CLEAR]") ]])
            
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Cognitive systems?[SOFTBLOCK] What a quaint notion.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[SOFTBLOCK] I'm just remembering something that hasn't happened yet.[BLOCK][CLEAR]") ]])
        end
        
        --Resume.
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] So you claim.[SOFTBLOCK] Focus on the present, not the future.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you have been here before as you assert, you can tell what is waiting ahead.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No, it's not like that...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This place is young.[SOFTBLOCK] So young.[SOFTBLOCK] The walls do not bleed, there are no hands.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 'He' is not here, yet.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] He?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] I don't know.[SOFTBLOCK] I don't know who 'He' is, or even if 'He' is the right term.[SOFTBLOCK] But 'He' is not here yet.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But there is someone waiting for us up ahead.[SOFTBLOCK] We will have to destroy her.[SOFTBLOCK] It won't matter, but we'll do it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We should turn back now.[SOFTBLOCK] We have nothing to gain from this.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] And yet, somehow, we're going to go ahead.[SOFTBLOCK] I know it.[SOFTBLOCK] I will, I have, I always have.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Then lead the way.[SOFTBLOCK] I will be monitoring you very closely.") ]])
        fnCutsceneBlocker()
    
    end
end
