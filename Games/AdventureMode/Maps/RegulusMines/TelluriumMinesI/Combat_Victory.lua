--[Combat Victory]
--The party won!

--Flags.
VM_SetVar("Root/Variables/Chapter5/Scenes/iDefeatedBoss50", "N", 1.0)

--Music change.
AudioManager_PlayMusic("Null")

--Wait.
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "609144", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] It is as it was, as it was it shall be.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] I go now to meet the dead heart.[SOFTBLOCK] Farewell, Christine.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Walks over the pit.
fnCutsceneMoveFace("609144", 14.25, 8.50, 0, 1, 0.25)
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "RemoveOverlay", false) ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("Combat|Impact_Pierce") ]])
for i = 1, 5, 1 do
    fnCutsceneTeleport("609144", 14.25, 8.50 + (i * 0.40))
    fnCutsceneWait(1)
    fnCutsceneBlocker()
end
fnCutsceneTeleport("609144", -100, -100)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "RemoveOverlay", true) ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So that's it.[SOFTBLOCK] Done.[SOFTBLOCK] Nothing here for us.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She left a weapon.[SOFTBLOCK] Odd.[SOFTBLOCK] She wasn't using it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's not her weapon, it's mine...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Future memories are good for something, at least.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Narrator: [SOUND|World|TakeItem](Received Tellurine Electrospear.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But there isn't anything else we can do. Not here.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Drop a bomb down this shaft?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You can't kill it.[SOFTBLOCK] It's like killing a building.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] A bomb would kill a building.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Then killing a star.[SOFTBLOCK] It can't be done.[SOFTBLOCK] There's nothing to kill.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Then to what end did we come down here?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] None, I guess.[SOFTBLOCK] None at all.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] This already happened.[SOFTBLOCK] What will happen is already set.[SOFTBLOCK] Nothing we can do can change that.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And you are certain of this?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I know it in my bones...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] You are wrong so often that I have subcategorized it into 'normal' wrong and 'confrontationally' wrong.[SOFTBLOCK] There is therefore nothing to worry about.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Regardless, this mission is complete.[SOFTBLOCK] Unit 771852, we have other assignments.[SOFTBLOCK] Move out.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Yeah.[SOFTBLOCK] Unit 771852 following orders...") ]])
fnCutsceneBlocker()

--Gain an Electrospear.
LM_ExecuteScript(gsItemListing, "Tellurine Electrospear")

--Fold the party.
fnAutoFoldParty()
