--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

	--Variables.
	local iDefeatedBoss50 = VM_GetVar("Root/Variables/Chapter5/Scenes/iDefeatedBoss50", "N")
	if(iDefeatedBoss50 == 1.0) then return end
    
    --Movement.
    fnCutsceneMove("Christine", 13.75, 12.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("55", 14.75, 12.50)
    fnCutsceneFace("55", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "609144", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] Christine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 609144.[SOFTBLOCK] I'm here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do you know this unit from somewhere?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] Oh, she and I?[SOFTBLOCK] We go way forward.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] Nice to see you again for the first time as well, Unit 2855.[SOFTBLOCK] I suppose you're here to finish the job?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not know you.[SOFTBLOCK] I wiped my memories some time ago.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] I like that.[SOFTBLOCK] Fresh slate.[SOFTBLOCK] Clean it all off, ready to be degraded again just like it was before.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] I've made some improvements since last we met.[SOFTBLOCK] What do you think?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What are you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] I am me.[SOFTBLOCK] I made some improvements.[SOFTBLOCK] What do you think?[SOFTBLOCK] Ha ha ha![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] Our bodies ought to be as malleable as our minds, ought they not?[SOFTBLOCK] Ought not ought not![SOFTBLOCK] Ha ha ha ha ha ha ha ha![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 609144...[SOFTBLOCK] was the researcher in charge of the Cryogenics Facility.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] One of several, but yes.[SOFTBLOCK] I was in charge of the last specimen we held there.[SOFTBLOCK] The first, the last, the most, the least.[SOFTBLOCK] Such a thing, that one![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] There are many things one can learn when they embrace alternate ways of thinking.[SOFTBLOCK] Of doing, of being.[SOFTBLOCK] Ing ing ing.[SOFTBLOCK] Ha ha ha![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] So you're going to kill me.[SOFTBLOCK] I know.[SOFTBLOCK] You always have, you always will.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] We'll meet again, though.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] In the future, yes.[SOFTBLOCK] I won't know you then.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] Can we really say we know each other now?[SOFTBLOCK] Can we ever?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] To plumb the secrets of someone's mind, now that's real madness![SOFTBLOCK] All the things they keep from themselves, least of all others.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] Do you know what is below here, Christine?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] We're not interested.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] The beating heart of a dead world.[SOFTBLOCK] It's far down, very far, so far that if you fell you'd never reach the bottom.[SOFTBLOCK] But it's down there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] I can hear it beating.[SOFTBLOCK] Thump thump thump![SOFTBLOCK] Ha ha![SOFTBLOCK] It never stops![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] She showed me![SOFTBLOCK] She made me see it![SOFTBLOCK] It's going to come through and up and all of us will be devoured by it, incorporated into it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] The flesh will make us whole and new and all one big thing![SOFTBLOCK] One, big, thing![SOFTBLOCK] Gazing into infinity![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] Dead for eternity![SOFTBLOCK] The flesh will bleed and we will join it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I know.[SOFTBLOCK] I've seen it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But we still fight.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] *Sigh*[SOFTBLOCK] I have not yet finished reading my book.[SOFTBLOCK] That's because I haven't finished writing it, of course.[SOFTBLOCK] That will happen in ten billion years.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "609144:[E|Neutral] Come, Christine.[SOFTBLOCK] Finish what you have already done.[SOFTBLOCK] There is only now, no past or future.[SOFTBLOCK] Now now now![SOFTBLOCK] Do it!") ]])
    fnCutsceneBlocker()
	
	--[Battle]
	--Trigger a fight here.
	fnCutsceneInstruction([[ AC_SetProperty("Next Music Override", "RottenTheme", 0.0000) ]])
	fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
	fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Maps/RegulusMines/TelluriumMinesI/Combat_Victory.lua") ]])
    fnCutsceneInstruction([[ AC_SetProperty("Defeat Script",  gsRoot .. "Chapter5Scenes/Defeat_BackToSave/Scene_Begin.lua") ]])
	fnCutsceneInstruction([[ AC_SetProperty("Unretreatable", true) ]])
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter5/609144.lua", 0) ]])
	fnCutsceneBlocker()
end
