--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Ladder Exit.
if(sObjectName == "LadderUp") then
	AudioManager_PlaySound("World|ClimbLadder")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityObservatoryE", "FORCEPOS:12.0x55.0x0") ]])
	fnCutsceneBlocker()
	
--Ladder Exit.
elseif(sObjectName == "LadderDn") then
	AudioManager_PlaySound("World|ClimbLadder")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterB", "FORCEPOS:17.0x27.0x0") ]])
	fnCutsceneBlocker()

--[Examinables]
elseif(sObjectName == "Terminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (All survey units, assignments are cancelled until further notice.[SOFTBLOCK] Come back to the Observatory immediately, no exceptions![SOFTBLOCK] Direct order from Command Unit 300910!)") ]])
	fnCutsceneBlocker()

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end