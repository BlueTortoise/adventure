--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Ladder Exit.
if(sObjectName == "LadderUpW") then
	AudioManager_PlaySound("World|ClimbLadder")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterA", "FORCEPOS:16.0x10.0x0") ]])
	fnCutsceneBlocker()
	
--Ladder Exit.
elseif(sObjectName == "LadderUpE") then
	AudioManager_PlaySound("World|ClimbLadder")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterD", "FORCEPOS:13.0x11.0x0") ]])
	fnCutsceneBlocker()
	
--Ladder Exit.
elseif(sObjectName == "LadderDn") then
	AudioManager_PlaySound("World|ClimbLadder")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterE", "FORCEPOS:11.0x6.0x0") ]])
	fnCutsceneBlocker()

--[Examinables]
elseif(sObjectName == "Door") then

	--Variables.
    local fPlayerX, fPlayerY = fnGetEntityPosition("Christine")
	local iOpenedMidwayDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iOpenedMidwayDoor", "N")
	
	--Not open yet.
	if(iOpenedMidwayDoor == 0.0 or iOpenedMidwayDoor == 3.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iOpenedMidwayDoor", "N", 3.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ^Hmmm, I can't open this door.[SOFTBLOCK] It's not broken or unpowered, it's being held shut...^") ]])
		fnCutsceneBlocker()
    
	--First time opening. The value gets set to 1.0 when climbing down the nearby ladder, or 4.0 if they saw the darkmatter earlier.
	elseif(iOpenedMidwayDoor == 1.0 or iOpenedMidwayDoor == 4.0) then
	
		--Variables.
		AudioManager_PlaySound("World|AutoDoorOpen")
        AL_SetProperty("Open Door", "Door")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iOpenedMidwayDoor", "N", 2.0)
	   
		--Dialogue. Only appears if the player examined the door originally.
        if(iOpenedMidwayDoor == 4.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] ^Odd.[SOFTBLOCK] Whatever was holding the door shut is gone.[SOFTBLOCK] It opened easily.^") ]])
            fnCutsceneBlocker()
        end
	
	--Successive opens.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
        AL_SetProperty("Open Door", "Door")
	end


--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end