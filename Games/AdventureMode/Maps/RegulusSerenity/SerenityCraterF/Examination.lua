--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Door Exit.
if(sObjectName == "ToSerenityE") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterE", "FORCEPOS:27.0x48.0x0") ]])
	fnCutsceneBlocker()
	
--Door Exit.
elseif(sObjectName == "ToSerenityG") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterG", "FORCEPOS:15.5x4.0x0") ]])
	fnCutsceneBlocker()

--[Examinables]
elseif(sObjectName == "TerminalA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Looks like there's some research logs on the local drive...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Molecular composition of the sample provided is still unknown, pending the results of the high-EM tests.[SOFTBLOCK] The substance is neither acidic nor basic in the conventional sense, but neutral is probably the wrong word to describe it.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"While ostensibly a granitic composition, that's complete nonsense.[SOFTBLOCK] Most of the crust of Regulus is andesitic to basaltic, with only the very surface area being granitic.[SOFTBLOCK] Thus we know with a great deal of certainty where this 'statue' was carved.[SOFTBLOCK] Not on Regulus.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"The uranic dating will provide some clues, no doubt.[SOFTBLOCK] If this object was created on Pandemonium and transported here recently, possibly as a bizarre ruse, then that's at least possible.[SOFTBLOCK] If not, then I am at a complete loss.\")") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Looks like there's some research logs on the local drive...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"The biological samples I was provided are most unusual.[SOFTBLOCK] They react very negatively to sudden temperature changes, but can survive in a vacuum complete unhindered.[SOFTBLOCK] This is like nothing organic I've ever seen.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Unfortunately we won't have time to do more extensive testing at this facility, but I've forwarded everything I have to Central Administration.[SOFTBLOCK] I sincerely hope they authorize more tests and more sample collection.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"I also discovered something unusual.[SOFTBLOCK] The samples have a distinct metabolism, and are maintaining homeostatis, yet have no obvious food source.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"The 'blood' within them does not contain any sort of energy transfer compound like glucose.[SOFTBLOCK] If we can determine how they are storing energy, we could greatly improve our own power core technology.[SOFTBLOCK] This is very exciting!\")") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Looks like there's some research logs on the local drive...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"I was taken away from my research team and smuggled into this barren facility in the middle of nowhere to conduct routine zircon dating tests?[SOFTBLOCK] Is there some particular reason why I wasn't even allowed to tell my Slave Units where I was going?[SOFTBLOCK] No matter.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"The sample I was provided was a granite.[SOFTBLOCK] I was not told the origin rock, possibly because the sample is of unknown genesis.[SOFTBLOCK] The composition is unusual for Regulus and would likely be located in a very highly sodic deposition environment, probably somewhere on Pandemonium under a salt flat.  \")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"What is most curious is the age of the sample.[SOFTBLOCK] I was able to locate several zircon grains that I could establish uranium radiometric dates from.[SOFTBLOCK] The dates agree with one another, but not with our knowledge of physics.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"The current expected age of the universe has an upper bound of about 13.799 billion years.[SOFTBLOCK] It may be younger or older, based on which method is used, but that's our estimate based on the microwave background and current understanding of physics.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"This sample suggests a date over 20 billion years, which is considerably older than the age of Pandemonium, Regulus, the Belarus solar system itself.[SOFTBLOCK] I have repeated the measurements multiple times and recalibrated the equipment.[SOFTBLOCK] It is not in error.[SOFTBLOCK] This rock is older than it should be.\")") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Looks like there's some research logs on the local drive...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"A biological sample burst when it was removed from its container.[SOFTBLOCK] We quarantined the area immediately and scrubbed down every unit who was in contact with it, but it seemed to be chemically inert.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"After several hours, we sent in a cleaning team.[SOFTBLOCK] We noticed something very unusual.[SOFTBLOCK] The biological material left a black, blue, or yellow imprint on areas when it was removed.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"While a biological residue is fairly common amongst a number of Pandemonium-bound species, what was most unusual is that the imprints were always in the shape of hands, despite the splatter pattern of the biological material.\")[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"While more research is certainly required, Central Administration does not find it sufficiently interesting and has ordered us to decommission the facility immediately.[SOFTBLOCK] A shame.\")") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The hard drive from this terminal was removed completely.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalF") then
	
	--Variables.
	local iSawFirstEDGScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N")
	local iSawUndergroundSceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N")
	local iSawSouthernCoreCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N")
	
	--Has seen the underground scene.
	if(iSawUndergroundSceneA == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This log belongs to the administrator of this facility...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"WARNING::[SOFTBLOCK] Contents are classification code 7-B.[SOFTBLOCK] Command Units and specifically cleared Lord Units ONLY.[SOFTBLOCK] NO EXCEPTIONS.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Administrative Log of Unit 2819.[SOFTBLOCK] I was assigned to this facility with the cover story of a rich ore vein having been discovered.[SOFTBLOCK] Command Unit 300910 appeared to believe that story and ceased her inquiries afterwards.[SOFTBLOCK] Good.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"We've secured the area and set up basic security checkpoints.[SOFTBLOCK] The Slave Units assigned to security detail were reassigned and underwent forcible reprogramming as per standard containment protocols.[SOFTBLOCK] We'll be using Drone Units from here on to prevent unnecessary reprogramming.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"I've ordered any units who were in contact with the artifact to be scrubbed multiple times by a variety of different cleaners, and ordered follow-up examinations after their reprogramming.[SOFTBLOCK] If there are any long-term effects due to exposure, I'd prefer to know about them before exposing myself.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"As such, all samples are to be sealed and provided via Drone Unit delivery, and all units experiencing contact are to wear protective clothing well in excess of the usual requirements.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"After several days of work, we have our laboratory space set up, and can begin analysis of the spatial distubances reported.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"While contact with voidborne species such as the Rilmani and Darkmatters has been recorded on several instances, we've never been able to determine the exact method of their dimensional locomotion.[SOFTBLOCK] That ends today.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"When the artifact is bombarded with positrons from close range, spacetime itself begins to tear open.[SOFTBLOCK] This has been predicted by the work of Unit 91102 but never observed in practice.[SOFTBLOCK] The exact mechanism that the artifact uses to generate these rifts is unknown at the current time.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Unlike the predicted mechanism, the rift does not travel to the other side of a dimensional fold.[SOFTBLOCK] Instead it appears to be some other dimension entirely.[SOFTBLOCK] Perhaps the old legends of the Inferno and the Stainless Vista are not mere folklore.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Our next task will be to send remote-controlled units with videograph cameras attached through the rift to attempt to document whatever is on the other side.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Central Administration has seen fit to vastly increase our material budget, but is refusing to add more personnel.[SOFTBLOCK] Fortunately the Drone Units don't complain about all the extra work.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Our initial survey of the rift site has produced encouraging results.[SOFTBLOCK] The artifact only opens rifts to exactly one location, no matter where or how it is bombarded with positrons.[SOFTBLOCK] We sent a unit in with a radial camera and successfully retrieved her after six hours.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"The unit appeared no worse for wear, but I ordered her isolated pending observation anyway.[SOFTBLOCK] Central Administration has advised to exceed the normal maximum containment procedures.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"I am not usually prone to speculation, but Central Administration seems unusually keen to receive reports.[SOFTBLOCK] Further, they seem to know what I am going to report well in advance and frequently send me advice on how to achieve the results they desire.[SOFTBLOCK] It is...[SOFTBLOCK] off putting...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Currently, there are no reports of anything motile at the rift destination.[SOFTBLOCK] No subjects, organic or inorganic, have been located to date.[SOFTBLOCK] While the material that composes the walls, floors, and ceiling appears organic, it is definitely not alive.[SOFTBLOCK] It does, however, seem to respond to injury.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"We acquired some samples for study and I've sent them along to the laboratory staff.[SOFTBLOCK] Their excitement turned to dread after a single day of experimentation, but I've ordered them to press on.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"This is an official letter of protest addressed to Unit 2856.[SOFTBLOCK] We're making breakthrough after breakthrough here, and yet we have had our facility decommissioned and are being ordered to abandon everything where it is.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"I've requested a personal meeting with her but it's been denied.[SOFTBLOCK] As it stands, we'll have to follow orders.[SOFTBLOCK] It's a shame.[SOFTBLOCK] Perhaps this has something to do with the recent increase in Darkmatter sightings in the area.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (That was the last log entry.[SOFTBLOCK] It was dated...[SOFTBLOCK] one day after the Cryogenics incident...)") ]])
		fnCutsceneBlocker()
		
	--Hasn't seen it, begin the scene.
	else
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N", 1.0)
		
		--Spawn entities.
		TA_Create("DarkmatterA")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterB")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterC")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterD")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterE")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterF")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterG")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		
		--Movement.
		fnCutsceneMove("Christine", 24.75, 25.50)
		fnCutsceneMove("55", 25.75, 25.50)
		fnCutsceneFace("Christine", 0,  -1)
		fnCutsceneFace("55", 0, -1)
		
		--Reading.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This log belongs to the administrator of this facility...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"WARNING::[SOFTBLOCK] Contents are classification code 7-B.[SOFTBLOCK] Command Units and specifically cleared Lord Units ONLY.[SOFTBLOCK] NO EXCEPTIONS.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Administrative Log of Unit 2819.[SOFTBLOCK] I was assigned to this facility with the cover story of a rich ore vein having been discovered.[SOFTBLOCK] Command Unit 300910 appeared to believe that story and ceased her inquiries afterwards.[SOFTBLOCK] Good.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"We've secured the area and set up basic security checkpoints.[SOFTBLOCK] The Slave Units assigned to security detail were reassigned and underwent forcible reprogramming as per standard containment protocols.[SOFTBLOCK] We'll be using Drone Units from here on to prevent unnecessary reprogramming.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"I've ordered any units who were in contact with the artifact to be scrubbed multiple times by a variety of different cleaners, and ordered follow-up examinations after their reprogramming.[SOFTBLOCK] If there are any long-term effects due to exposure, I'd prefer to know about them before exposing myself.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"As such, all samples are to be sealed and provided via Drone Unit delivery, and all units experiencing contact are to wear protective clothing well in excess of the usual requirements.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"After several days of work, we have our laboratory space set up, and can begin analysis of the spatial distubances reported.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"While contact with voidborne species such as the Rilmani and Darkmatters has been recorded on several instances, we've never been able to determine the exact method of their dimensional locomotion.[SOFTBLOCK] That ends today.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"When the artifact is bombarded with positrons from close range, spacetime itself begins to tear open.[SOFTBLOCK] This has been predicted by the work of Unit 91102 but never observed in practice.[SOFTBLOCK] The exact mechanism that the artifact uses to generate these rifts is unknown at the current time.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Unlike the predicted mechanism, the rift does not travel to the other side of a dimensional fold.[SOFTBLOCK] Instead it appears to be some other dimension entirely.[SOFTBLOCK] Perhaps the old legends of the Inferno and the Stainless Vista are not mere folklore.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Our next task will be to send remote-controlled units with videograph cameras attached through the rift to attempt to document whatever is on the other side.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Central Administration has seen fit to vastly increase our material budget, but is refusing to add more personnel.[SOFTBLOCK] Fortunately the Drone Units don't complain about all the extra work.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Our initial survey of the rift site has produced encouraging results.[SOFTBLOCK] The artifact only opens rifts to exactly one location, no matter where or how it is bombarded with positrons.[SOFTBLOCK] We sent a unit in with a radial camera and successfully retrieved her after six hours.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"The unit appeared no worse for wear, but I ordered her isolated pending observation anyway.[SOFTBLOCK] Central Administration has advised to exceed the normal maximum containment procedures.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"I am not usually prone to speculation, but Central Administration seems unusually keen to receive reports.[SOFTBLOCK] Further, they seem to know what I am going to report well in advance and frequently send me advice on how to achieve the results they desire.[SOFTBLOCK] It is...[SOFTBLOCK] off putting...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"Currently, there are no reports of anything motile at the rift destination.[SOFTBLOCK] No subjects, organic or inorganic, have been located to date.[SOFTBLOCK] While the material that composes the walls, floors, and ceiling appears organic, it is definitely not alive.[SOFTBLOCK] It does, however, seem to respond to injury.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"We acquired some samples for study and I've sent them along to the laboratory staff.[SOFTBLOCK] Their excitement turned to dread after a single day of experimentation, but I've ordered them to press on.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"...\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"This is an official letter of protest addressed to Unit 2856.[SOFTBLOCK] We're making breakthrough after breakthrough here, and yet we have had our facility decommissioned and are being ordered to abandon everything where it is.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (\"I've requested a personal meeting with her but it's been denied.[SOFTBLOCK] As it stands, we'll have to follow orders.[SOFTBLOCK] It's a shame.[SOFTBLOCK] Perhaps this has something to do with the recent increase in Darkmatter sightings in the area.\")[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (That was the last log entry.[SOFTBLOCK] It was dated...[SOFTBLOCK] one day after the Cryogenics incident...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Music.
		fnCutsceneInstruction([[ AL_SetProperty("Music", "Vivify") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, did you read that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[BLOCK][CLEAR]") ]])
        
        --Player has not seen the LRT scene.
		if(iSawFirstEDGScene == 0.0 and iSawSouthernCoreCutscene == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 2856 is adjacent to your designation.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I realize that, but I do not know her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The biological samples collected here were transported to other locations.[SOFTBLOCK] Shortly afterwards, the Cryogenics Incident occurred.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Further, if you recall the log files from Cryogenics, the biological subject they received had unusual egg-like objects in its body.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It looks like some other samples were sent to the Biological Research Labs.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But what does this all mean?[SOFTBLOCK] And where's this artifact the administrator mentioned?") ]])
        
        --Player has seen the LRT scene.
		elseif(iSawFirstEDGScene > 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 2856....[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My biological sister, before conversion.[SOFTBLOCK] She was involved here, somehow...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I believe it was she who contacted us in the data core at the LRT facility.[SOFTBLOCK] Considering her facial structure and vocal synthesizer are a near-perfect match for mine.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What does this mean, though?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That, I do not know.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will note that the Cryogenics Incident occurred shortly after receiving a sample from this facility, and that other samples were sent to the Biological Research labs.[SOFTBLOCK] Look at the manifests.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The dates coincide...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We should have a look at this artifact.[SOFTBLOCK] Where is it?") ]])
        
        --Player met 2856 in the western core entry.
        else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unit 2856....[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You mentioned my 'strange behavior' in the LRT facility, did you not?[SOFTBLOCK] Do you recall?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It was right before we saw what Vivify did to the data core's break room...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I believe Unit 2856 may be my biological sister, before our conversion.[SOFTBLOCK] Unfortunately, her records are highly classified.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We would need to finish our work at the LRT facility in order to confirm, but I believe she is identical to me in terms of facial structure and vocal tones.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So one day after the Cryogenics incident occurred, your sister shut down this facility.[SOFTBLOCK] Hmmm...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] She is a Prime Command Unit, as I was, and would have the requisite authority.[SOFTBLOCK] I can only assume she recognized a serious threat.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The dates do coincide...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We should have a look at this artifact.[SOFTBLOCK] Where is it?") ]])
        
		end
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneTeleport("DarkmatterA", 16.25, 17.50)
		fnCutsceneTeleport("DarkmatterB", 17.25, 17.50)
		fnCutsceneTeleport("DarkmatterC",  6.25, 33.50)
		fnCutsceneTeleport("DarkmatterD",  6.25, 34.50)
		fnCutsceneTeleport("DarkmatterE", 13.25, 33.50)
		fnCutsceneTeleport("DarkmatterF", 13.25, 34.50)
		fnCutsceneBlocker()
		fnCutsceneMove("DarkmatterA", 16.25, 25.50)
		fnCutsceneMove("DarkmatterA", 20.25, 26.50)
		fnCutsceneFace("DarkmatterA", 1, 1)
		fnCutsceneMove("DarkmatterB", 17.25, 25.50)
		fnCutsceneMove("DarkmatterB", 19.25, 27.50)
		fnCutsceneMove("DarkmatterB", 19.25, 28.50)
		fnCutsceneFace("DarkmatterB", 1, 0)
		fnCutsceneMove("DarkmatterC", 19.25, 33.50)
		fnCutsceneFace("DarkmatterC", 1, -1)
		fnCutsceneMove("DarkmatterD", 19.25, 34.50)
		fnCutsceneFace("DarkmatterD", 1, -1)
		fnCutsceneMove("DarkmatterE", 14.25, 33.50)
		fnCutsceneMove("DarkmatterF", 14.25, 34.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneFace("55", -1, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55?[SOFTBLOCK] Did you hear that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are not alone...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--G teleports in.
		fnCutsceneTeleport("DarkmatterG", 28.25, 28.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, 0)
		fnCutsceneFace("55", 1, 0)
		fnCutsceneMove("DarkmatterG", 28.25, 27.50)
		fnCutsceneFace("DarkmatterG", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yikes![SOFTBLOCK] Where did you come from?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Back off, Darkmatter![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Will you *please* calm down?[SOFTBLOCK] She's doesn't look aggressive at all...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey, wait a second.[SOFTBLOCK] You're the Darkmatter who wanted to see my runestone, aren't you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Come here, Little Lady.[SOFTBLOCK] We're not going to hurt you.[SOFTBLOCK] C'mon.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 25.25, 26.50)
		fnCutsceneMove("55", 25.25, 27.50)
		fnCutsceneFace("Christine", 1, 0)
		fnCutsceneFace("55", 1, 0)
		fnCutsceneMove("DarkmatterG", 27.25, 27.50, 0.20)
		fnCutsceneMove("DarkmatterG", 26.25, 26.50, 0.20)
		fnCutsceneFace("DarkmatterG", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You want to see my runestone again?[SOFTBLOCK] Okay, but -[SOFTBLOCK] Eep![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter: Touch.[SOFTBLOCK] Help.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh my goodness![SOFTBLOCK] You can talk?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What's happening?[SOFTBLOCK] What'd she do?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter: No.[SOFTBLOCK] Hurt.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, did you hear that, 55?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No![SOFTBLOCK] Back off, Darkmatter![SOFTBLOCK] Get back![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter: No.[SOFTBLOCK] Hurt.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Wait![SOFTBLOCK] I think she said she's not going to hurt us![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter: Wrong.[SOFTBLOCK] Doll.[SOFTBLOCK] Hurt.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Err, or she -[SOFTBLOCK] doesn't want you to hurt her?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter: Wrong.[SOFTBLOCK] Doll.[SOFTBLOCK] Hurt.[SOFTBLOCK] Doll.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Okay okay, I think I get it.[SOFTBLOCK] She doesn't want you to hurt yourself![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter: Good.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] How do you know that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You can't hear that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Maybe I can hear it through my runestone...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter: Help.[SOFTBLOCK] Please.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Help you?[SOFTBLOCK] Do you want my help?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter: Please.[SOFTBLOCK] Follow.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And just where are you going?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She wants me to follow her.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("DarkmatterG", 26.25, 30.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneFace("55", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		fnCutsceneFace("DarkmatterG", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 26.25, 26.50)
		fnCutsceneMove("Christine", 26.25, 28.50)
		fnCutsceneBlocker()
		fnCutsceneFace("55", 1, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is pure idiocy.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If you've got a better idea, I'm open to suggestion.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It seems you are.[SOFTBLOCK] That thing is using you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'll be the judge of that.[SOFTBLOCK] She needs our help.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Wrong.[SOFTBLOCK] You.[SOFTBLOCK] Just.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Uh, just me?[SOFTBLOCK] Okay...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you coming, 55?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will be keeping my weapon trained on you, creature.[SOFTBLOCK] Don't do anything you'll regret.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fall in.
		fnCutsceneMove("Christine", 26.75, 29.50)
		fnCutsceneMove("Christine", 26.75, 30.50)
		fnCutsceneMove("Christine", 19.75, 30.50)
		fnCutsceneMove("Christine", 19.75, 34.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneMove("55", 25.25, 30.50)
		fnCutsceneMove("55", 19.75, 30.50)
		fnCutsceneMove("55", 19.75, 33.50)
		fnCutsceneFace("55", 0, 1)
		fnCutsceneMove("DarkmatterG", 19.75, 30.50)
		fnCutsceneMove("DarkmatterG", 19.75, 35.50)
		fnCutsceneFace("DarkmatterG", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, 1)
        
        --Guards move aside.
		fnCutsceneMove("DarkmatterGuardL", 18.25, 37.50)
		fnCutsceneMove("DarkmatterGuardR", 21.25, 37.50)
		fnCutsceneFace("DarkmatterGuardL", 1, 0)
		fnCutsceneFace("DarkmatterGuardR", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Open the door.
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "WideDoor") ]])
        fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Everyone moves through.
		fnCutsceneMove("DarkmatterG", 19.75, 42.50)
		fnCutsceneMove("DarkmatterG", 38.75, 42.50)
		fnCutsceneFace("DarkmatterG", 0, 1)
		fnCutsceneMove("Christine", 19.75, 42.50)
		fnCutsceneMove("Christine", 37.75, 42.50)
		fnCutsceneMove("55", 19.75, 42.50)
		fnCutsceneMove("55", 36.75, 42.50)
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		fnCutsceneFace("55", 0, 1)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I've got a very bad feeling about this...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 36.75, 46.50)
		fnCutsceneMove("55", 37.75, 46.50)
		fnCutsceneMove("DarkmatterG", 38.75, 46.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Transport to the next level.
		fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterG", "FORCEPOS:16.0x4.0x0") ]])
		fnCutsceneBlocker()
		
	end
	
elseif(sObjectName == "TerminalG") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The defragmentation monitor shows a massive increase in processor usage when this unit was defragmenting...[SOFTBLOCK] Nightmares, perhaps?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalH") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The logs indicate this unit defragmented normally until the third day she was stationed here, at which point she stopped defragmenting and injected oil into her power core to maintain functionality.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalI") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The unit stationed here was reassigned after reporting auditory hallucinations.[SOFTBLOCK] Her maintenance log came back clean...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalJ") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This unit requested transfers after two days in the facility.[SOFTBLOCK] She was reassigned when the facility was decomissioned...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TubeFull") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The storage tube has a number of small organic objects floating in it.[SOFTBLOCK] They look somewhat like eggs, but are porous.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "Chemicals") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Chemicals used for testing and identifying compounds.[SOFTBLOCK] There's a variety of acids and bases here, and various dissolved pure metals.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "EmptyTube") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A storage tube.[SOFTBLOCK] This one wasn't cleaned before being put in storage, and has traces of residue around the edges.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "SecurityStationDoor") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This door leads to the security station.[SOFTBLOCK] It's locked and probably not worth hacking open.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TableGross") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The table is covered in some sort of organic material.[SOFTBLOCK] It appears to be corroding the metal in some places, but not in others.[SOFTBLOCK] Is it choosing to corrode in some places, perhaps?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "WideDoor") then
    
    --Variables.
    local iSawUndergroundSceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N")
    if(iSawUndergroundSceneA == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The door isn't locked, but something is holding it shut...)") ]])
        fnCutsceneBlocker()
	else
        AL_SetProperty("Open Door", "WideDoor")
        AudioManager_PlaySound("World|AutoDoorOpen")
    
    end
--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end