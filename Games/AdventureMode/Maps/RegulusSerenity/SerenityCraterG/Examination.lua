--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Door Exit.
if(sObjectName == "ToSerenityF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterF", "FORCEPOS:37.5x46.0x0") ]])
	fnCutsceneBlocker()

--[Examinables]
elseif(sObjectName == "Terminal") then

    --Variables.
    local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    
    --Has not completed Serenity Crater. Option for boss battle.
    if(iCompletedSerenity == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Are you prepared, Christine?[SOFTBLOCK] Shall I activate the positron bombardment?[BLOCK]") ]])
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
        
       
    --Has completed Serenity.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The terminal indicates that whatever the statue was, it's inactive.[SOFTBLOCK] For now...)") ]])
        fnCutsceneBlocker()
       
    end
    
elseif(sObjectName == "Serenity") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An object not of this world.[SOFTBLOCK] It resembles a statue, but is anything but...)") ]])
    fnCutsceneBlocker()

--[Decisions]
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
    
    --Movement.
    fnCutsceneMove("Christine", 15.75, 16.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("55", 20.25, 15.50)
    fnCutsceneFace("55", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Positron accelerators are powered.[SOFTBLOCK] Beginning bombardment cycle.[SOFTBLOCK] Stand by...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I feel...[SOFTBLOCK] nauseous...[SOFTBLOCK] ugh...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It's inside my brain...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Records mismatch.[SOFTBLOCK] The reaction is not as expected.[SOFTBLOCK] No rift is opening.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Because...[SOFTBLOCK] Something's different.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55...[SOFTBLOCK] Modify the arrays.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] To?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 105.66 MeV.[SOFTBLOCK] Set the array to oscillate slowly between present energy and new setup over 15 seconds on my mark.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Mark.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Set.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] .[SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK] There is definitely a reaction.[SOFTBLOCK] How did you know about this?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55...[SOFTBLOCK] It's stirring...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Come here![SOFTBLOCK] Quickly![SOFTBLOCK] 55, HELP!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Screen goes green.
    fnCutsceneWait(25)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0.5, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Trigger boss battle.
    fnCutsceneInstruction([[ AC_SetProperty("Next Music Override", "RottenTheme", 0.0000) ]])
    fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
    fnCutsceneInstruction([[ AC_SetProperty("Unretreatable", true) ]])
    fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Maps/RegulusSerenity/SerenityCraterG/Combat_Victory.lua") ]])
    fnCutsceneInstruction([[ AC_SetProperty("Defeat Script",  gsRoot .. "Chapter5Scenes/Defeat_BackToSave/Scene_Begin.lua") ]])
    fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter5/Serenity.lua", 0) ]])
    fnCutsceneBlocker()
    
    
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end