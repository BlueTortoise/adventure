--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Sample object.
if(sObjectName == "CutsceneTrigger") then
	
	--Variables. Don't play this scene twice.
	local iSawUndergroundSceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneB", "N")
	if(iSawUndergroundSceneB == 1.0) then return end
	
	--Other variables.
	local iLRTBossResult = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N") 
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneB", "N", 1.0)
	
	--Save Christine's form.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	VM_SetVar("Root/Variables/Chapter5/Scenes/sStartedUndergroundForm", "S", sChristineForm)
	
	--Movement.
	fnCutsceneMove("Christine", 18.25, 5.50)
	fnCutsceneBlocker()
	fnCutsceneMove("Christine", 18.25, 7.50)
	fnCutsceneMove("Christine", 16.25, 9.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneMove("55", 18.25, 5.50)
	fnCutsceneMove("55", 18.25, 7.50)
	fnCutsceneMove("55", 17.25, 9.50)
	fnCutsceneFace("55", 0, 1)
	fnCutsceneMove("Darkmatter", 13.25, 5.50)
	fnCutsceneMove("Darkmatter", 13.25, 7.50)
	fnCutsceneMove("Darkmatter", 15.25, 9.50)
	fnCutsceneFace("Darkmatter", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
		
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Danger...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, wait.[SOFTBLOCK] Don't get any closer.[SOFTBLOCK] The Darkmatter says it's dangerous.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	if(iLRTBossResult == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That thing...[SOFTBLOCK] what is it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The artifact we've been reading about.[SOFTBLOCK] It seems to be glowing.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It...[SOFTBLOCK] it's not of this world...[SOFTBLOCK] I can feel it...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It doesn't belong here.[SOFTBLOCK] We need to get rid of it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Right.[SOFTBLOCK] Good.[BLOCK][CLEAR]") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It looks like Vivify...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I can see the resemblence, but it is not total.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It...[SOFTBLOCK] it's not of this world...[SOFTBLOCK] I can feel it...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] It doesn't belong here.[SOFTBLOCK] We need to get rid of it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Right.[SOFTBLOCK] Good.[BLOCK][CLEAR]") ]])
	end
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The golems should have left this thing buried.[SOFTBLOCK] Darkmatter, what do you need us to do?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Start.[SOFTBLOCK] Activate.[SOFTBLOCK] Do.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] What does that mean?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What did it say?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Start, activate, do.[SOFTBLOCK] Do you think it wants us to -[SOFTBLOCK] activate the positron bombardment cycle?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If the logs are correct, that will open the portal to the other dimension.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Scary...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But...[SOFTBLOCK] Isn't that exactly what we don't want to do?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Please...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I don't like this.[SOFTBLOCK] I don't like this one bit.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Prepare yourself.[SOFTBLOCK] If the creatures we encountered on the crater shelf originated here, then there may be more.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can't you just empathize with me for a second here?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I -[SOFTBLOCK] this is like Cryogenics...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are experiencing a fear response.[SOFTBLOCK] Suppress it.[SOFTBLOCK] Fear will make you make irrational choices and lead to failure.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have -[SOFTBLOCK] said that before.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're right.[SOFTBLOCK] Let's -[SOFTBLOCK] let's start the bombardment.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The terminal over there is hooked to the apparatus.") ]])
    fnCutsceneBlocker()
    
    --Fold the party.
	fnCutsceneMove("55", 16.25, 9.50)
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
    --[=[
	
	--Movement.
	fnCutsceneMove("Christine", 17.25,  9.50)
	fnCutsceneMove("Christine", 17.25, 16.50)
	fnCutsceneMove("Christine", 15.75, 16.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I just know I'm going to regret this...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Smooch*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] !") ]])
	
	--Screen goes black.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Transition.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterH", "FORCEPOS:20.0x50.0x0") ]])
	fnCutsceneBlocker()]=]

end
