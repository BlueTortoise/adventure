--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.
--Special: This script is also used during the reliving sequences.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
local bSpecial = false
if(not fnArgCheck(1)) then
    bSpecial = true
end

--Arg resolve.
local sObjectName = "Null"
if(bSpecial == false) then
    sObjectName = LM_GetScriptArgument(0)
else
    sObjectName = "Hole"
end

--[Exits]
--[Examinables]
--Handprint.
if(sObjectName == "Hand") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The handprint of some enormous creature.[SOFTBLOCK] It's oddly human-like, but how could something so big fit in here?)") ]])
	fnCutsceneBlocker()
	
--Green Egg.
elseif(sObjectName == "GreenEgg") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A strange green egg.[SOFTBLOCK] It's cold to the touch...)") ]])
	fnCutsceneBlocker()
	
--Hole.
elseif(sObjectName == "Hole") then

    --Music.
    local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")
    if(iIsRelivingScene == 1.0) then
        fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
    end

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of those eggs was here...[SOFTBLOCK] it's gone now.[SOFTBLOCK] Someone took it...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The golems...[SOFTBLOCK] should not have taken it...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I think -[SOFTBLOCK] I think this is important.[SOFTBLOCK] I need to close this hole, but with what?)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I'll just...[SOFTBLOCK] stick my hand in there...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Screen blacks out.
    if(iIsRelivingScene == 0.0) then
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    end
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] Eeeek![SOFTBLOCK] It's stuck![SOFTBLOCK] Let go![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] Get off me![SOFTBLOCK] No![SOFTBLOCK] EEEEEEEEEEEEEEEEEEEKKKKK!!!!!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Scene.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine pulled at her arm, her feet digging into the fleshy floor as she pulled, but could not free herself.[SOFTBLOCK] Something within the hole had taken hold of her and would not yield its grip.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Its strength was immense, and though it felt like the grip of a human, she knew that was impossible. [SOFTBLOCK] Something deep within her whispered to her, and she knew she was the only human to ever see this place.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "A second hand latched on to her arm, followed by a third, then a fourth,[SOFTBLOCK] and soon, she lost count.[SOFTBLOCK] They felt human.[SOFTBLOCK] They had five fingers.[SOFTBLOCK] They gripped as a human would grip, wrapping four fingers around her arm and locking the grip into place with a thumb.[SOFTBLOCK] But she could think of no human whose hold was so absolute that it yielded no movement at all..[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She screamed again, but it echoed, unheard, off the pulsating walls of skin and muscle.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "A new scream welled up within her, and erupted from the depths of her very being.[SOFTBLOCK] It flowed from her,[SOFTBLOCK] piercing into the air,[SOFTBLOCK] then faded,[SOFTBLOCK] muted into silence by the pulsating walls of flesh as though she were in the depths of a void.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She pulled again at her arm, and her mind began to clear.[SOFTBLOCK] She needed to escape, whatever the cost.[SOFTBLOCK] She drew her spear as she cast a silent glare at the advancing hands.[SOFTBLOCK] If she could not free herself, she would need to cut off her own arm.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She swallowed a growing lump that had risen in her throat and steadied herself.[SOFTBLOCK] She could only hope she would be able to staunch the bleeding and find a way back to Regulus City.[SOFTBLOCK] Surely Sophie would be able to replace the lost limb if she could return to golem form.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The hands continued their advance, now emerging from the black void of the hole.[SOFTBLOCK] They were the very same hands that appeared on the walls and floor.[SOFTBLOCK] They were ethereal, with no physical form, but she could still see and feel as the hands continued their advance up her arm.[SOFTBLOCK] She gritted her teeth and raised the spear.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She squinted, struggling to keep her eyes open to what she knew was to come.[SOFTBLOCK] The hands were advancing.[SOFTBLOCK] She would have only a single chance before they reached her shoulder.[SOFTBLOCK] She could not risk anything less than a clean and complete cut.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "And then,[SOFTBLOCK] even as she finalized her resolve,[SOFTBLOCK] the hands loosened their grip.[SOFTBLOCK] She blinked, and the hands wavered with an ephemeral trembling,[SOFTBLOCK] and faded into nothing.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her spear fell to the floor, any noise it could make fading into the walls of flesh that surrounded her.[SOFTBLOCK] She soon joined it, dropping to her knees and clutching at her arm in wonderment.[SOFTBLOCK] All along it, from the tips of her fingers to the toned muscle of her bicep and tricep, an uncountable number of hand prints had been etched into her skin.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked herself over, cautious of any possible threat that may have affected her as the hands had occupied her,[SOFTBLOCK] but could see nothing beyond the countless overlapping handprints now etched into her skin.[SOFTBLOCK] Her thoughts fell to silence as she struggled to comprehend what had just befallen her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Satisfied that no further dangers were immediately present, she looked more closely at the handprints on her arm.[SOFTBLOCK] They were identical to the hands that had gripped her, as though they had been tattooed onto her arm with a shimmering translucent ink.[SOFTBLOCK] She frowned and drew her arm nearer.[SOFTBLOCK] The handprints had not been shimmering a moment ago.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She stared at the handprints, her gaze sweeping across the length of her arm, and for a brief moment, she thought she could see something within them.[SOFTBLOCK] She drew her arm nearer still, nearly touching it to her face, and realized that, behind the handprints, she could see...[SOFTBLOCK] stars.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The stars were spinning out of some cataclysm, and as she stared into them, she realized it was the birth of a new universe.[SOFTBLOCK] The realization took hold of her mind, holding it with a grip no less absolute than the grip of the hands, but where the hands had brought a sense of dread, the grip on her mind brought a different feeling entirely.[SOFTBLOCK] Comfort.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine did not notice the world around her vanish into the empty nothing of a space outside the space of a new reality that had formed mere seconds ago.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
	fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine stared into the star-filled void that now existed within her arm, heedless to the world around her as the room and all that lay beyond it faded into the nothingness of a new reality that she herself had given birth to mere moments ago.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The space within her arm expanded to fill the empty void around her, dragging the stars with it until their light whited out all she could see.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The light of the stars shimmered as they burned through their fuel, expanded, went nova, and collapsed in upon themselves.[SOFTBLOCK] They spewed forth gasses and heavy elements into the vast spaces that had formed between them where before they had been close enough to touch.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "As she gazed about herself, watching the spectacle, one star caught her attention.[SOFTBLOCK] It was a massive star, one she knew at once was the size of an entire galaxy.[SOFTBLOCK] She watched it as it burned through its fuel in mere seconds before bursting into an enormous nebula, then collapsing back in upon itself to form a massive black hole in the center.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The nebula spun around it, and new stars began to form in the wake of its death.[SOFTBLOCK] She looked out upon these stars, and saw that they were familiar.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She recognized the positions of some stars and the shapes they made.[SOFTBLOCK] These were the stars that would one day fill the skies of Pandemonium.[SOFTBLOCK] Regulus, her home, would soon join them in a mere few billion years.[SOFTBLOCK] She let out a sigh of relief as understanding washed over her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "This was the time before time.[SOFTBLOCK] There was no sentient life.[SOFTBLOCK] There could be no sentient life.[SOFTBLOCK] The creator would first need to make a world for people like her to live on.[SOFTBLOCK] She whiled away the years waiting for her home to form in the lingering clouds of gas and dust, passing millions of years in a mere blink, watching with an eager anticipation..[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She began to notice the presence of another.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "They would appear at the edge of her vision, giggling and laughing before darting back into the void just as she tried to look at them.[SOFTBLOCK] As the millions of years drifted by, she grew curious about them, and when she saw them peek out from a near-by star cluster, she decided to follow her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She followed the creature across the void, from star to star, ever just a few moments behind.[SOFTBLOCK] Sometimes she was able to get within a few steps of the creature,[SOFTBLOCK] other times she lost sight of them entirely.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "When this would happen, she would turn back to watch the birth of her future home, only to find the creature watching her from some nearby proto-planet or young star.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "After many aeons of chase, the creature set down gently on a drifting rock and beckoned to her.[SOFTBLOCK] [SOFTBLOCK] She landed beside her, and saw that the girl was made of melted starlight.[SOFTBLOCK] Quietly the girl motioned for Christine to look behind her,[SOFTBLOCK] to look at what the rock orbited.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She turned around to find the event horizon of a singularity.[SOFTBLOCK] All around it space and time warped into a twisted vortex.[SOFTBLOCK] Christine reached out a trembling hand to touch it.[SOFTBLOCK] Even as the tips of her fingers neared the event horizon, the girl took her hand and held her back.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The girl guided her hand downward, and Christine smiled at her.[SOFTBLOCK] Her hand slipped beneath the waist of her skirt, and she began to massage herself.[SOFTBLOCK] The girl stood behind her and embraced her, pulling her tight and placing her own hand over Christine's.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She guided it.[SOFTBLOCK] She showed Christine's unpracticed hands to how raise and lower herself until a tingling began to build up in her periphery.[SOFTBLOCK] The girl smiled in recognition as Christine's eyes fluttered, and quickened the pace to kindle the sensation.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine closed her eyes and let the girl teach her virgin hands the rhythm of her own womanhood.[SOFTBLOCK] She was building, and knew she would soon peak, but she did not want to.[SOFTBLOCK] She wanted the building sensation to last for all of time, to grow without limit until it would coincide with the birth of the next universe...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "A surge of energy coursed through her body as the first wave of the orgasm struck.[SOFTBLOCK] Her eyes flew open as her chest grew tight, and she could see that they had drifted closer to the singularity, floating near its event horizon.[SOFTBLOCK] She looked down to where the girl's hands continued to guide her own, and she saw that they had changed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "They were metal, they were cold.[SOFTBLOCK] Metal lips kissed her neck as course hair brushed her cheek, and she turned her gaze just enough to see the smiling face of Sophie.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The girl of melted starlight smiled at her within her mind, even as the face of Sophie gave her a coy smile.[SOFTBLOCK] The orgasm began to well up within her at the sight of her tandem unit, and she knew that the girl was pleased that her new form could bring such joy to Christine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Even as one hand was guided by the girl, Christine let her other hand drift over her body, but as she reached beneath her shirt, she realized her body was no longer human.[SOFTBLOCK] Her skin had become ethereal, her body translucent.[SOFTBLOCK] She was melting into starlight.[SOFTBLOCK] A curiosity began to come over her as she watched her body change.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "A curiosity that ended as the girl's ministrations drew forth a new surge of growing orgasmic energy.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her climax approached, and it loomed over her.[SOFTBLOCK] She wanted to hold it in, to let it grow, but she knew she could not.[SOFTBLOCK] In a mere million years, perhaps two, she would orgasm.[SOFTBLOCK] It would have cosmic ramifications.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked into the singularity and saw the future scrawled across its surface, compressed like all other time in the quantum nothingness.[SOFTBLOCK] She saw her orgasm would create a star, and around it would form many companion planets and celestial bodies.[SOFTBLOCK] One of these she recognized to be Pandemonium and its silent companion, Regulus.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She gazed upon what would be, and reached out to it.[SOFTBLOCK] Just as the tips of her fingers reached the edge of the nothingness, the girl pushed her over her own event horizon.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The orgasm tore her body apart, scattering it across the infinite as her toes curled.[SOFTBLOCK] Her mind remained a single distinct entity, but she allowed her body to be broken into countless smaller ones as the energy that had built up within her coursed through her, filling her as quickly as she spread across the void.[SOFTBLOCK] Then,[SOFTBLOCK] she entered the singularity, one and many.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "It was quiet inside the expanse of the black hole.[SOFTBLOCK] Peaceful.[SOFTBLOCK] She began to reconstitute herself as she waited for the singularity to dissipate, only to realize that her body had reformed as the same melted starlight as the girl who brought her to this place.[SOFTBLOCK] She turned her gaze to the edge of the void and saw all that lay beyond, and stepped out from it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The energy swirled around her as she passed over the event horizon, the last lingering touch of her orgasm.[SOFTBLOCK] She emerged, having been born, conceived, and given birth all at the same singular moment.[SOFTBLOCK] As she looked out, a strange, pale women stood looking at her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Gone was the girl who she had followed.[SOFTBLOCK] In her place stood the strange, pale woman.[SOFTBLOCK] She was was dressed in black and adorned with ribbons and skulls.[SOFTBLOCK] The woman gasped as if in shock as her own vision took in Christine, but her face remained flat and uninterested.[SOFTBLOCK] Christine giggled at the thought of it and waved to her, but could not be delayed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The time was approaching, and she had someone she needed to be.[SOFTBLOCK] She reached back into a time to where her old self had first fallen into the space between space, and caught herself.[SOFTBLOCK] Christine was very glad she remembered to do that, or the loop would have lasted for eternity![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "She placed herself back at the hole in Serenity Crater's time, then pinched it shut.[SOFTBLOCK] The gap closed, and with her task complete, she grinned and merged herself with the physical body she had left behind those billions of years ago...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Switch Christine to Darkmatter.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Darkmatter.lua") ]])
	
	--Transition.
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityObservatoryA", "FORCEPOS:14.0x10.0x0") ]])
	fnCutsceneBlocker()

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end