--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Cutscene trigger.
if(sObjectName == "CutsceneTrigger") then
	
	--Don't retrigger.
	AL_SetProperty("Activate Lights")
	AL_SetProperty("Set Player Light No Drain", true)
	local iSawUndergroundSceneC = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneC", "N")
	if(iSawUndergroundSceneC == 1.0) then return end
	
	--Remove 55 from the party and field.
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AC_SetProperty("Set Party", 1, "Null")
	AL_SetProperty("Unfollow Actor Name", "55")
	if(EM_Exists("55") == true) then
		EM_PushEntity("55")
			TA_SetProperty("Position", -100, -100)
		DL_PopActiveObject()
	end
	
	--Variables.
	local sStartedUndergroundForm = VM_GetVar("Root/Variables/Chapter5/Scenes/sStartedUndergroundForm", "S")
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneC", "N", 1.0)
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	
	--Transform Christine to human.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
	
	--Christine screams.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] EEEEEEEEEEEEEEEEEEEKKKKKKKKKK!!!") ]])
	fnCutsceneBlocker()
	
	--Position Christine. Wounded pose.
	fnCutsceneTeleport("Christine", 20.25, 50.50)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "Wounded")
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 245, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(275)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Ugh...[SOFTBLOCK] I feel like I was torn apart and put back together inside-out...)[BLOCK][CLEAR]") ]])
	if(sStartedUndergroundForm ~= "Human") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Oh -[SOFTBLOCK] I'm human again![SOFTBLOCK] But a transformation has never felt like that before...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (And I can't transform here...)") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Well, all my bits seem to be where they're supposed to be...)") ]])
	end
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Crouches, trying to stand.
	fnCutsceneSetFrame("Christine", "Crouch")
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (My legs feel like spaghetti...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Stands up.
	fnCutsceneSetFrame("Christine", "Null")
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Okay, I have to -[SOFTBLOCK] close some rift thing...[SOFTBLOCK] Ugh...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Topic unlock.
    WD_SetProperty("Unlock Topic", "Darkmatters", 1)

end
