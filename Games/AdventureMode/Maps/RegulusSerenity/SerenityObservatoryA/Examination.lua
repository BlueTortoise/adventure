--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToExteriorEF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorEF", "FORCEPOS:36.5x17.0x0")

--Elevator.
elseif(sObjectName == "Elevator") then

    --Hasn't seen intro yet.
	local iSawSerenityIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N")
	if(iSawSerenityIntro == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](I should speak to the unit in charge before we go anywhere else.)") ]])
        return
    end

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Observation Deck\", " .. sDecisionScript .. ", \"ElevatorToObservation\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Domicile Block\", " .. sDecisionScript .. ", \"ElevatorToDomiciles\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Crater Access Basement\", " .. sDecisionScript .. ", \"ElevatorToBasement\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--Elevator to Observation Deck.
elseif(sObjectName == "ElevatorToObservation") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityObservatoryC", "FORCEPOS:19.5x4.0x0") ]])
	fnCutsceneBlocker()
    gi_Force_Facing = gci_Face_South

--Elevator to Domiciles.
elseif(sObjectName == "ElevatorToDomiciles") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityObservatoryB", "FORCEPOS:19.5x4.0x0") ]])
	fnCutsceneBlocker()
    gi_Force_Facing = gci_Face_South
	
--Elevator to the basement.
elseif(sObjectName == "ElevatorToBasement") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityObservatoryD", "FORCEPOS:15.5x4.0x0") ]])
	fnCutsceneBlocker()
    gi_Force_Facing = gci_Face_South
	
--Cancel Elevator.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")

--[Examinables]
elseif(sObjectName == "DrinkMachines") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Fizzy Pop![SOFTBLOCK] The best way to recharge your power core after a long day![SOFTBLOCK] It seems both machines here are the pink Lord Unit variety.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FabrBench") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A fabrication machine, probably used to construct spare parts.[SOFTBLOCK] It seems to be in excellent condition.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Supplies") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Tools, parts, and extra cable.[SOFTBLOCK] Lots of extra cable.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Notice to all Units from Command Unit 300910.[SOFTBLOCK] All survey assignments are hereby cancelled.[SOFTBLOCK] All Units are to return to the Observatory immediately unless explicitly ordered otherwise.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Leader](Seems Unit 300910 was in an argument with someone on the network...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Leader](300910:: I understand the danger to the Units, but why must the decommission order be permanent?[SOFTBLOCK] Surely it would make more sense to issue a temporary evacuation until the security situation is resolved!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Leader](10022:: That would create unnecessary complications in the database software.[SOFTBLOCK] It will be easier to decommission and recommission the facility.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Leader](300910:: Simpler in the software?[SOFTBLOCK] How is reassigning a dozen units going to be simpler than issuing an evacuation order?[SOFTBLOCK] This is nonsense, and I won't be complying until I hear directly from Central Administration.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Leader](10022:: Suit yourself, but Central is not going to be pleased with any delays.[SOFTBLOCK] I'd recommend decommissioning immediately, but you're not going to listen to me.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Oilmaker") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](An oilmaker.[SOFTBLOCK] What's this?[SOFTBLOCK] It's flavoured with a bit of vulcanized rubber and metal shavings?[SOFTBLOCK] Delicious!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "RVDScreen") then
    
    --Check 300910's position. If she's in the office, she comments.
    local fDollX, fDollY = fnGetEntityPosition("300910")
    if(fDollX < 9.00) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[VOICE|Doll] Ah, I see you noticed our widescreen RVD.[SOFTBLOCK] We fabricated it ourselves and have a videograph night every week.[SOFTBLOCK] You're welcome to attend![SOFTBLOCK] This week is 'Electric Horror from the Deep'![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Uh, an electric monster from underwater?[SOFTBLOCK] Really?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[VOICE|Doll] Nobody said we watched *good* videographs.") ]])
        fnCutsceneBlocker()
    
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A big-screen RVD.[SOFTBLOCK] The last played video was 'Boiling Creature from the Salty Swamp!!!'.[SOFTBLOCK] Yes, it has three exclamation points.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "WorkTerminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](A work terminal, hooked into the Observatory's local network.[SOFTBLOCK] It contains a list of work assignments and the designations of those set to complete them.)") ]])
	fnCutsceneBlocker()

--Entry door. Christine transforms if the quest is not completed.
elseif(sObjectName == "TFDoor") then

    --Transform Christine if necessary.
	local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    local sFirstSerenityForm = VM_GetVar("Root/Variables/Chapter5/Scenes/sFirstSerenityForm", "S")
    local iSawSerenityIntro  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N")
	if(sChristineForm ~= sFirstSerenityForm and iCompletedSerenity == 0.0 and iSawSerenityIntro == 1.0) then
		
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Better transform to maintain my cover...)") ]])
        fnCutsceneBlocker()
    
        --Flashwhite.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite", "Null")
        DL_PopActiveObject()
        fnCutsceneBlocker()

        fnCutsceneWait(75)
        fnCutsceneBlocker()
        if(sFirstSerenityForm == "Golem") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
        elseif(sFirstSerenityForm == "LatexDrone") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua") ]])
        elseif(sFirstSerenityForm == "Eldritch") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])
        elseif(sFirstSerenityForm == "Electrosprite") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Electrosprite.lua") ]])
        elseif(sFirstSerenityForm == "SteamDroid") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_SteamDroid.lua") ]])
        end
        fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
        fnCutsceneBlocker()
    end


--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end