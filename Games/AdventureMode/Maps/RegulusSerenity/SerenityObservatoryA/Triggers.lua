--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Introduction cutscene.
if(sObjectName == "CutsceneTrigger") then
	
	--Don't play this cutscene if we've already seen it.
	local iSawSerenityIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N")
	if(iSawSerenityIntro == 1.0) then return end
	
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/sFirstSerenityForm", "S", sChristineForm)
    
    --Allow 300910's topics.
	WD_SetProperty("Unlock Topic", "Sensors", 1)
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Oh, visitors?[SOFTBLOCK] I'll be right there!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 19.25, 10.50)
    fnCutsceneFace("Christine", -1, 0)
	fnCutsceneMove("55", 19.25, 11.50)
    fnCutsceneFace("55", -1, 0)
	fnCutsceneMove("300910", 12.25, 11.50)
	fnCutsceneMove("300910", 16.25, 11.50)
    fnCutsceneFace("300910", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--[Golem]
	--If Christine is a golem:
	if(sChristineForm == "Golem") then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Unit 771852![SOFTBLOCK] I'm glad you decided to assist![SOFTBLOCK] Honestly, when I saw your profile, I thought it might be a mistake, but here you are![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] And -[SOFTBLOCK] do my optical receptors deceive me, or is that Unit 2855?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] !!![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Everyone![SOFTBLOCK] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

	--[Latex Drone]
	elseif(sChristineForm == "LatexDrone") then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Are you -[SOFTBLOCK] Unit 771852?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Hello.[SOFTBLOCK] How may I help you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I -[SOFTBLOCK] Your profile said you were a Lord Unit.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Perhaps there was a misprint.[SOFTBLOCK] No matter, my faculties are intact and I can assist you to the best of my ability.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Well, that's a relief.[SOFTBLOCK] I thought we were in real trouble for a moment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] And -[SOFTBLOCK] do my optical receptors deceive me, or is that Unit 2855?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] !!![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Everyone![SOFTBLOCK] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
	--[Eldritch Dreamer]
    elseif(sChristineForm == "Eldritch") then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Are you -[SOFTBLOCK] what exactly are you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We don't really have a name...[SOFTBLOCK] Dreamers, perhaps?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] And you're Unit 771852?[SOFTBLOCK] Because your auth-chip is transmitting that signal.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] (Oh my goodness, where is the auth chip?[SOFTBLOCK] Maybe it's best if I don't ask...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's me![SOFTBLOCK] Pleased to meet you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Your profile stated you were a Lord Unit.[SOFTBLOCK] Is...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She was involved in an incident.[SOFTBLOCK] I assure you, she is perfectly capable of meeting your expectations,[EMOTION|2855|Smug] so long as they are adjusted downwards.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] An incident?[SOFTBLOCK] Well, if Unit 2855 says so, then I'll believe it![SOFTBLOCK] Greetings, 2855![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Your auth-chip isn't transmitting for some reason, but I'd recognize that face anywhere![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] *I've been identified!*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Everyone![SOFTBLOCK] Look![SOFTBLOCK] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
    
    --[Electrosprite]
    elseif(sChristineForm == "Electrosprite") then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Uhhhh...[SOFTBLOCK] Hey, has anyone seen a voidborne species like this before?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're not voidborne, Command Unit.[SOFTBLOCK] We're actually a partirhuman species that lives inside electrical neuro-webs.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Or rather, others like me are.[SOFTBLOCK] I'm a Lord Unit.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] That's what your profile said you were.[SOFTBLOCK] Unit 771852, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's me![SOFTBLOCK] How did you know?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] The door recorded your authenticator chip's signature.[SOFTBLOCK] Where -[SOFTBLOCK] where is it stored on your person?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] In fact, forget I asked, I don't want to know.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Despite Unit 771852's physical appearance,[SOFTBLOCK][EMOTION|2855|Smug] she is perfectly capable of absorbing impacts if that is what you require.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Hey, if Unit 2855 says you're all right, that's good enough for us.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Been a while since I saw you last.[SOFTBLOCK] How have you been?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] *I've been identified!*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Unit 2855 is here, everyone!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
    
    --[Steam Droid]
	elseif(sChristineForm == "SteamDroid") then
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Well that wasn't what I was expecting.[SOFTBLOCK] You're Unit 771852, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That is correct.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Huh.[SOFTBLOCK] I didn't think any un-upgraded Steam Droids were still in operation.[SOFTBLOCK] How have you stayed...[SOFTBLOCK] working?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm a more recent conversion.[SOFTBLOCK] I can also return to my Lord Unit state if needed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Hmm...[SOFTBLOCK] Beggars can't be choosers, I suppose.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Unit 2855 -[SOFTBLOCK] an unexpected surprise![SOFTBLOCK] Could you perhaps vouch for her?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] !!![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] It's nice to be working with you again![SOFTBLOCK] Everyone![SOFTBLOCK] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
	end

	--Movement.
	fnCutsceneMove("300910", 18.25, 11.50)
	fnCutsceneFace("300910", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--55 knocks 300910 back!
	fnCutsceneFace("GolemAA", 1, 0)
	fnCutsceneFace("GolemAB", 1, 0)
	fnCutsceneMove("55", 18.75, 11.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
	fnCutsceneMoveFace("300910", 17.25, 11.50, 1, 0, 2.00)
	fnCutsceneBlocker()
    
    --55 draws her weapon.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Draw0")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Draw1")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Draw2")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutsceneWait(35)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] That's close enough![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Oof![SOFTBLOCK] What are you doing?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Oh my goodness![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] 55![SOFTBLOCK] We are guests here, and it is impolite to point a weapon at one's host![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] It's a trap, 771852.[SOFTBLOCK] Get your weapon out.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] W-[SOFTBLOCK]wait![SOFTBLOCK] This isn't a trap![SOFTBLOCK] Unit 2855, do you not recognize me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Unit 300910, remember?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Put the pulse diffractor away, 55.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] How do you know me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] We're good friends![SOFTBLOCK] You've aided us before.[SOFTBLOCK] Don't you remember the dissociation incident?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Unit 300910, she -[SOFTBLOCK] doesn't remember you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] She wiped her memory drives recently.[SOFTBLOCK] For security reasons.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] You did?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] She's very defensive about it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Well, if there's anything I can do to help, you just let me know.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Of course.[SOFTBLOCK] And 55 will glady put her weapon away.[SOFTBLOCK] Right 55?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] [SOFTBLOCK][SOFTBLOCK][EMOTION|55|Neutral]Fine.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
    
    --55 holsters hear weapon.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Draw1")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Draw0")
	fnCutsceneWait(40)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("55", "Null")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Ah, good.[SOFTBLOCK] Sorry for the misunderstanding.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] And 771852 - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Please call me Christine.[SOFTBLOCK] It's my secondary designation.") ]])
	fnCutsceneBlocker()
    
    --Resume with new music.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "SerenityObservatory") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Smirk") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] A friend of 2855's is a friend of ours![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Maybe you can answer a few questions, then.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What was the dissociation incident you mentioned?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] A few months ago, we received a bad batch of adhesive from Regulus City.[SOFTBLOCK] A lot of our equipment was damaged and rendered inoperable.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] When Central Administration found out, they demanded the immediate, permanent closure of the observatory.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] But you overrode their demands.[SOFTBLOCK] After all, you're a Prime Command Unit![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] It's because of you we're still operating and gathering astronomical data.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] We're in a similar bind again, and again you've arrived to help us out of it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Why would damaged equipment call for a full closure?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I'm not sure.[SOFTBLOCK] I'm not privy to the decisions of Central Administration.[SOFTBLOCK] But they were most adamant about it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] And that brings me to the current problem.[SOFTBLOCK] Please, follow me to my office.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Movement.
	fnCutsceneFace("GolemAA", 0, 1)
	fnCutsceneFace("GolemAB", 0, -1)
	fnCutsceneMove("300910",    18.25, 13.50)
	fnCutsceneBlocker()
	fnCutsceneMove("300910",    18.25, 14.50)
	fnCutsceneMove("300910",     6.25, 14.50)
	fnCutsceneFace("300910", 0, -1)
	fnCutsceneMove("Christine", 18.25, 12.50)
	fnCutsceneMove("Christine", 18.25, 14.50)
	fnCutsceneMove("Christine",  8.25, 14.50)
	fnCutsceneMove("55",        18.25, 12.50)
	fnCutsceneMove("55",        18.25, 14.50)
	fnCutsceneMove("55",         7.25, 14.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Open Door", "OfficeDoor") ]])
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("300910",    6.25, 11.50)
	fnCutsceneMove("300910",    8.25, 11.50)
	fnCutsceneMove("300910",    8.25,  9.70)
	fnCutsceneMove("300910",    7.25,  9.70)
	fnCutsceneFace("300910", 0, 1)
	fnCutsceneTeleport("300910",7.15,  9.70)
	fnCutsceneTeleport("300910",7.05,  9.70)
	fnCutsceneTeleport("300910",6.95,  9.70)
	fnCutsceneTeleport("300910",6.85,  9.70)
	fnCutsceneTeleport("300910",6.75,  9.70)
	fnCutsceneTeleport("300910",6.65,  9.70)
	fnCutsceneTeleport("300910",6.55,  9.70)
	fnCutsceneTeleport("300910",6.45,  9.70)
	fnCutsceneTeleport("300910",6.35,  9.70)
	fnCutsceneTeleport("300910",6.25,  9.70)
	fnCutsceneMove("Christine", 6.25, 14.50)
	fnCutsceneMove("Christine", 6.25, 11.50)
	fnCutsceneMove("Christine", 5.75, 11.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneMove("55",        6.25, 14.50)
	fnCutsceneMove("55",        6.25, 11.50)
	fnCutsceneMove("55",        6.75, 11.50)
	fnCutsceneFace("55", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] May I get you anything?[SOFTBLOCK] Oil, Fizzy Pop!, perhaps a synaptic spike?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Just get to the point.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55, please don't be rude.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] She hasn't changed a bit![SOFTBLOCK] Ha ha![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Oh, sorry.[SOFTBLOCK] If you have any questions about your past, I can try to help.[SOFTBLOCK] But, I should tell you about our predicament first.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I'm sure you encountered the Darkmatters on the way here.[SOFTBLOCK] They've been swarming over this area.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We had a few run-ins, yes.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Well, my official reason for calling you here is to recalibrate some sensors down in the crater.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] The Darkmatters have started assaulting my survey teams.[SOFTBLOCK] I've recalled my crews to the observatory for the time being, and I'm not sending any out.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Naturally, the split second the Darkmatters got ornery, Central Administration began using it as an excuse to shut the observatory down.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Again?[SOFTBLOCK] Suspicious...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Precisely.[SOFTBLOCK] I imagine if someone dropped a pipe wrench they'd call for us to be shut down.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Unfortunately, my staff can't deal with this on their own.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] We don't have a dedicated security detail, and -[SOFTBLOCK] well, I honestly don't want to risk any of them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What do you mean by that?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Look, these Slave Units are my -[SOFTBLOCK] they're like family to me.[SOFTBLOCK] I don't want any of them getting damaged.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I know it's selfish to ask but -[SOFTBLOCK] can you help us?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] The sensors in the crater shelf don't matter.[SOFTBLOCK] What I really need is someone to find out what has the Darkmatter's so angry, and deal with that.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I know it entails some risk, so I am prepared to provide work credits...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We'll help, of course![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Really?[SOFTBLOCK] Will we?[SOFTBLOCK] Why should we put ourselves at risk?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Because these units need our help, 55.[SOFTBLOCK] That's reason enough.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] 300910, why did I help you last time?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I'm not sure, but I assume you had your reasons.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Of course, she helped you because she wanted to do the right thing.[SOFTBLOCK] Right?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral][EMOTION|55|Neutral] She's never been particularly talkative, but she's a good friend when you need one.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So, what do you know about the Darkmatters?[SOFTBLOCK] When did they start attacking your staff?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Let's see...[SOFTBLOCK] Here's the logs.[SOFTBLOCK] The first assault report is highlighted there.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This -[SOFTBLOCK] Christine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Yes?*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] *The first assault is a week before the Cryogenics Incident.[SOFTBLOCK] There may be a connection.*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] *Hmmm...*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Have there been any other units going through the area recently?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Well, yes.[SOFTBLOCK] Have you not heard about the discoveries in the crater?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] About a year ago, a survey team discovered a rare and very valuable ore down in the crater.[SOFTBLOCK] So Central sent a mining team in.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] There's an entire facility down there.[SOFTBLOCK] With their own local power supply and everything.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Is it still in operation?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] ...[SOFTBLOCK] I don't know.[SOFTBLOCK] They don't send any communications to us, they communicate directly with Central.[SOFTBLOCK] In fact I'm not supposed to know they exist, but I have Command Unit priviliges.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well then, I know where we'll be going.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Now I *do* still need you to recalibrate those sensors.[SOFTBLOCK] I'll even give you a bonus for it.[SOFTBLOCK] You'll find them on your way into the crater.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] So, good luck.[SOFTBLOCK] And thank you for doing this.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 300910?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Yes?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] It was -[SOFTBLOCK] nice.[SOFTBLOCK] Seeing you again.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] And it's always a pleasure seeing you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] The transit elevator in the basement will take you about halfway down the crater rim.[SOFTBLOCK] You'll have to climb down the cliffs on your own from there.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Be careful near the edge, obviously.[SOFTBLOCK] And good luck, 771852.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We'll have this sorted out before you know it.[SOFTBLOCK] Come on, 55!") ]])
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 6.25, 11.50)
	fnCutsceneMove("Christine", 6.25, 14.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneMove("55", 6.25, 11.50)
	fnCutsceneMove("55", 6.25, 14.50)
	fnCutsceneMove("55", 7.25, 14.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Close Door", "OfficeDoor") ]])
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneFace("55", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Well I am just so proud of you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ..?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] You told 300910 that it was nice to see her again![SOFTBLOCK] That's a very sweet thing to say![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I forced myself to say it.[SOFTBLOCK] She will make a good ally for our purposes.[SOFTBLOCK] She dislikes the Administration and her facility is well situated to resist armed assault.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's okay to admit you did it to be nice.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I didn't.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Then.[SOFTBLOCK][EMOTION|Christine|Neutral] Lie.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Lie until it becomes true.[SOFTBLOCK] If you can't change for the better, trick yourself.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not a traditional moral lesson, and certainly not the one I expected from you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I -[SOFTBLOCK] I know what it's like having multiple lives.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Now, shall we?") ]])
	fnCutsceneBlocker()
	
	--Fold the party.
	fnCutsceneMove("Christine", 7.25, 14.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
    
    --Topics
    WD_SetProperty("Unlock Topic", "SerenityCrater", 1)

--Cutscene that triggers after the Darkmatter transformation.
elseif(sObjectName == "DarkmatterTrigger") then

	--Repeat check.
	local iIsRelivingScene      = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")
	local iSawUndergroundSceneC = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneC", "N")
	local iCompletedSerenity    = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
	if(iSawUndergroundSceneC == 0.0 or iCompletedSerenity == 1.0 and iIsRelivingScene == 0.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N", 1.0)
    
    --Add work credits.
    if(iIsRelivingScene == 0.0) then
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 300)
    end
	
	--Music.
	AL_SetProperty("Music", "Null")
	
	--Spawn 55.
	fnSpecialCharacter("55", "Doll", 15, 10, gci_Face_South, false, nil)
	
	--Spawn some other golems.
	TA_Create("ConcernedGolemA")
		TA_SetProperty("Position", 15, 7)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/RegulusCity/SerenityCrater/ConcernedGolemA.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	DL_PopActiveObject()
	TA_Create("ConcernedGolemB")
		TA_SetProperty("Position", 19, 12)
		TA_SetProperty("Facing", gci_Face_West)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/RegulusCity/SerenityCrater/ConcernedGolemB.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	DL_PopActiveObject()
	TA_Create("ConcernedGolemC")
		TA_SetProperty("Position", 15, 14)
		TA_SetProperty("Facing", gci_Face_North)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/RegulusCity/SerenityCrater/ConcernedGolemC.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	DL_PopActiveObject()
	TA_Create("ConcernedGolemD")
		TA_SetProperty("Position", 16, 14)
		TA_SetProperty("Facing", gci_Face_North)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/RegulusCity/SerenityCrater/ConcernedGolemD.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	DL_PopActiveObject()
	TA_Create("Darkmatter")
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Facing", gci_Face_North)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
	DL_PopActiveObject()

	--Black the screen out, spawn entities as needed.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Teleport characters to their positions if they didn't spawn there.
	fnCutsceneTeleport("Christine", 14.25, 10.50)
	fnCutsceneTeleport("300910", 13.25, 10.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneFace("300910", 1, 0)
	fnCutsceneFace("55", -1, 0)
	fnCutsceneSetFrame("Christine", "Wounded")
	fnCutsceneBlocker()
	
	--Fade us in.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(70)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Is she...[SOFTBLOCK] dead?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] I don't know.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Is she even still 771852?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] I don't know.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] What happened?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] I DON'T KNOW![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] I'm sorry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I did not see what happened after the green flash of light.[SOFTBLOCK] But I am certain this is her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She was clutching the runestone, and the other Darkmatters seemed most concerned about her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Please wake up, Christine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] The observatory's future wasn't worth losing yours...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(70)
	fnCutsceneBlocker()
	
	--Move Christine slightly.
	fnCutsceneMove("Christine", 14.25, 11.00, 0.20)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Christine crouches.
	fnCutsceneSetFrame("Christine", "Crouch")
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ AL_SetProperty("Music", "SerenityObservatory") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] She moved![SOFTBLOCK] Christine!?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Unghhh...[SOFTBLOCK] When am I?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] She's alive![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Within parameters, then.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, report mission results.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Mission successful...[SOFTBLOCK] Reporting for new function assignment...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] 55, give her a moment, will you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Your current function assignment is to recover.[SOFTBLOCK] Take your time.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative...[SOFTBLOCK] ungh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, is that you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes, it's me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Where's Sophie?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 499323 will be located in the repair facility of Sector 96.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So this -[SOFTBLOCK] everything is as it should be.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(65)
	fnCutsceneBlocker()
	
	--Teleport the Darkmatter in.
	local ciListX = {0, 1, 1,  1,  0, -1, -1, -1}
	local ciListY = {1, 1, 0, -1, -1, -1,  0,  1}
	fnCutsceneTeleport("Darkmatter", 14.25, 9.50)
	fnCutsceneWait(3)
	fnCutsceneFace("300910", 0, -1)
	fnCutsceneFace("55", 0, -1)
	fnCutsceneBlocker()
	for i = 1, 2, 1 do
		for p = 1, 8, 1 do
			fnCutsceneFace("Darkmatter", ciListX[p], ciListY[p])
			fnCutsceneWait(3)
			fnCutsceneBlocker()
		end
	end
	for i = 1, 2, 1 do
		for p = 1, 8, 1 do
			fnCutsceneFace("Darkmatter", ciListX[p], ciListY[p])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
		end
	end
	for i = 1, 2, 1 do
		for p = 1, 8, 1 do
			fnCutsceneFace("Darkmatter", ciListX[p], ciListY[p])
			fnCutsceneWait(12)
			fnCutsceneBlocker()
		end
	end
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "Null")
	fnCutsceneFace("Darkmatter", 0, 1)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Doll", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "DarkmatterGirl", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Everyone...[SOFTBLOCK] this Darkmatter is my friend...[SOFTBLOCK] Please don't be alarmed...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] 300910, this is the Darkmatter I told you about.[SOFTBLOCK] She was the one who led us to the artifact site.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] How can you tell?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Their stellar fold patterns are distinct.[SOFTBLOCK] This is the one I saw earlier.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Are you well, my friend?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I am -[SOFTBLOCK] what did I see?[SOFTBLOCK] What was that?[SOFTBLOCK] Was that you I chased across the arc of the sky?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] ...[SOFTBLOCK] Excuse me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She is likely speaking to it.[SOFTBLOCK] They can converse, but we cannot hear them respond.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] No, young one.[SOFTBLOCK] What you saw was perhaps a glimpse of the Creator's history.[SOFTBLOCK] Her will is woven into the fabric of reality, and you rode along with it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Of course, there are many histories.[SOFTBLOCK] The past before sentience is fuzzy, and changes as the future needs.[SOFTBLOCK] Time is a sphere, after all.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But I saw...[SOFTBLOCK] everything...[SOFTBLOCK] I *was* everything.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] That is true.[SOFTBLOCK] And I am sorry that you must surrender that to return to those you love.[SOFTBLOCK] Perhaps someday you will play in the void with us.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] But you have shut the gap.[SOFTBLOCK] The rift is sealed.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55, what did you see?[SOFTBLOCK] Did I close the rift?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I had the PDU download the recordings from the equipment.[SOFTBLOCK] The artifact seems to be inert now, but I will not even attempt to guess how or why.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Probably best if you don't.[SOFTBLOCK] I don't think I could explain it to you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We Darkmatters will attend to the rifts and creatures that remain.[SOFTBLOCK] Please tell your hardmatter friends that the ridge remains unsafe.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We will let them know when we have cleared it for them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] My friend said that the ridge isn't safe, and that they're going to work on clearing it of the...[SOFTBLOCK] things...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Can we help?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No.[SOFTBLOCK] No.[SOFTBLOCK] It's best if hardmatter is not exposed to them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Your materialistic minds are not yet prepared for their true nature.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I suppose that will have to do.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 771852...[SOFTBLOCK] What did you see?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Walls made of flesh, dead and bleeding.[SOFTBLOCK] Screams that died on my lips.[SOFTBLOCK] Hands.[SOFTBLOCK] There were hands...[SOFTBLOCK] They...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Don't strain yourself.[SOFTBLOCK] We can discuss this later.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] You mentioned that she can transform herself.[SOFTBLOCK] Is that still the case?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, yes.[SOFTBLOCK] I just need to rest and adjust myself to this new body.[SOFTBLOCK] I am -[SOFTBLOCK] melted starlight...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you prepared, Christine?[SOFTBLOCK] We still have work ahead of us.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I think so.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Would you mind if we...[SOFTBLOCK] ran some tests?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ha ha ha![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] Their understanding of physics is insufficient.[SOFTBLOCK] Their machinery will not avail them yet.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm sorry, but it will be a waste of time.[SOFTBLOCK] Regulus Science has a few centuries of physics research to go before I will be of any service.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] But...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, but I did see the universe being born.[SOFTBLOCK] I must remember to upload what I saw.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] WHAT!?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You just need to open your mind, hardmatter.[SOFTBLOCK] Nothing is something.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] That's enough toying with her, Christine.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Darkmatter:[E|Neutral] We will not forget what you have done to aid us.[SOFTBLOCK] Wherever you go, the Darkmatters will be your friends.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Thank you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Unit 771852, we are in your debt.[SOFTBLOCK] You're always welcome here.[SOFTBLOCK] And -[SOFTBLOCK] we'll keep your secret safe.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Thanks.[SOFTBLOCK] I'd rather not have to fill out the administrative reports for how I pulled this mission off.[SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh...[SOFTBLOCK] I should go see Sophie...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[SOFTBLOCK] Let's get going.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Handler for reliving.
    if(iIsRelivingScene == 1.0) then
    
        --Wait a bit.
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
        --Return to the last save point and execute the post-script..
        fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_End.lua") ]])
        fnCutsceneBlocker()
        return
    end
	
	--Re-add 55. Fold the party.
	gsFollowersTotal = 1
	gsaFollowerNames = {"55"}
	giaFollowerIDs = {0}
	EM_PushEntity("55")
		local iCharacterID = RE_GetID()
	DL_PopActiveObject()
	giaFollowerIDs = {iCharacterID}
	AL_SetProperty("Follow Actor ID", iCharacterID)
	AC_SetProperty("Set Party", 1, "55")
	
	--Move 55 and Christine onto the same tile.
	fnCutsceneMove("Christine", 14.25, 10.50)
	fnCutsceneMove("55", 14.25, 10.50)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

end
