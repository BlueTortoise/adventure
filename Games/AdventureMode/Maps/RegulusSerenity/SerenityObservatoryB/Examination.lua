--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToExteriorEF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorEF", "FORCEPOS:36.5x17.0x0")

--Elevator.
elseif(sObjectName == "Elevator") then

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"ElevatorToMainFloor\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Observation Deck\", " .. sDecisionScript .. ", \"ElevatorToObservation\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Crater Access Basement\", " .. sDecisionScript .. ", \"ElevatorToBasement\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--Elevator to Domiciles.
elseif(sObjectName == "ElevatorToMainFloor") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityObservatoryA", "FORCEPOS:20.5x4.0x0") ]])
	fnCutsceneBlocker()
    gi_Force_Facing = gci_Face_South

--Elevator to Observation Deck.
elseif(sObjectName == "ElevatorToObservation") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityObservatoryC", "FORCEPOS:19.5x4.0x0") ]])
	fnCutsceneBlocker()
    gi_Force_Facing = gci_Face_South
	
--Elevator to the basement.
elseif(sObjectName == "ElevatorToBasement") then
	WD_SetProperty("Hide")
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityObservatoryD", "FORCEPOS:15.5x4.0x0") ]])
	fnCutsceneBlocker()
    gi_Force_Facing = gci_Face_South
	
--Cancel Elevator.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")

--[Examinables]
elseif(sObjectName == "DrinkMachines") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Fizzy Pop![SOFTBLOCK] The best way to recharge your power core after a long day![SOFTBLOCK] A pink Lord Unit machine is here.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "OilmakerA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (An oilmaker with a number of custom flavours, such as rubber, screws, plastic, and of course 'Industrial Waste'.[SOFTBLOCK] The package says it's real waste imported from the factories under Regulus City, but everyone knows it's made in some chemical vat.[SOFTBLOCK] Totally fake.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "OilmakerB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (An oilmaker.[SOFTBLOCK] This one makes two varieties::[SOFTBLOCK] Black, and Really Black.[SOFTBLOCK] How much more black could it be?[SOFTBLOCK] I guess the answer is...[SOFTBLOCK] none.[SOFTBLOCK] None more black.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalOilmaker") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The defragmentation log indicates the unit here doesn't defragment very often...[SOFTBLOCK] I wonder where she gets the energy?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalTable") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This terminal was last used to display a list of commendations this unit has received from Unit 300910.[SOFTBLOCK] Way to go!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalLib") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (There's a reprimand notice on here from a Command Unit assigned to an abduction team.[SOFTBLOCK] Apparently, her staff were asked to acquire human artifacts for a personal collection...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Books") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A collection of books, authentic books, from the surface of Pandemonium![SOFTBLOCK] They're in excellent condition.[SOFTBLOCK] I guess the unit here likes reading old-style.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This novella is called 'Magical Camp', and seems to be about a man who becomes a magical girl through a series of mishaps.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (...[SOFTBLOCK] I really feel for this guy...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (...[SOFTBLOCK] Giant bee girls?[SOFTBLOCK] Really?[SOFTBLOCK] That's ridiculous!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (... Oh wow, I hope there's a way to make his butt go back to normal.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (... It ends there?[SOFTBLOCK] Aww, I wanted to see where this was going!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalRec") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Notice from Command Unit 300910::[SOFTBLOCK] Please remove videographs from the display terminal if you're done with them and store them on your own local terminals.[SOFTBLOCK] The RVD terminal is running low on drive space.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "RVDScreen") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A Remote Videograph Display.[SOFTBLOCK] It's currently set to play the episodes of All My Processors in a random order.[SOFTBLOCK] I guess it really doesn't matter which order you see them in, since *anything can happen!*)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalTableB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (The terminal has a set of repair manuals queued to be uploaded during defragmentation.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalNothing") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (There's a picture of a Slave Unit blowing a kiss at the camera.[SOFTBLOCK] At the bottom it says 'I miss you, come see me soon' in curly text, and there are hearts all over it.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (This is wildly inappropriate and I should report this to Unit 300910 immediately!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (These should be power cores, not organic hearts![SOFTBLOCK] What sort of unit doesn't know that!?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalNothingB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Administrative records, going back until the Observatory was founded.[SOFTBLOCK] They're set to be synched whenever the defragmentation pod is used.)") ]])
	fnCutsceneBlocker()

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end