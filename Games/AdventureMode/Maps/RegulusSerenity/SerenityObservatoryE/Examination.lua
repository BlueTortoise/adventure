--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Elevator.
if(sObjectName == "Elevator") then

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Take the elevator back up to the Observatory?)[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"ElevatorToObservatory\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--Elevator to the observatory's basement.
elseif(sObjectName == "ElevatorToObservatory") then
	WD_SetProperty("Hide")
    gi_Force_Facing = gci_Face_South
    
    --Transform Christine if necessary.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    local sFirstSerenityForm = VM_GetVar("Root/Variables/Chapter5/Scenes/sFirstSerenityForm", "S")
	if(sChristineForm ~= sFirstSerenityForm and iCompletedSerenity == 0.0) then
		
        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Christine](Better transform to maintain my cover...)") ]])
        fnCutsceneBlocker()
    
        --Flashwhite.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite", "Null")
        DL_PopActiveObject()
        fnCutsceneBlocker()

        fnCutsceneWait(75)
        fnCutsceneBlocker()
        if(sFirstSerenityForm == "Golem") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
        elseif(sFirstSerenityForm == "LatexDrone") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua") ]])
        elseif(sFirstSerenityForm == "Eldritch") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])
        elseif(sFirstSerenityForm == "Electrosprite") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Electrosprite.lua") ]])
        elseif(sFirstSerenityForm == "SteamDroid") then
            fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_SteamDroid.lua") ]])
        end
        fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
        fnCutsceneBlocker()
    end
    
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityObservatoryD", "FORCEPOS:4.5x12.0x0") ]])
	fnCutsceneBlocker()
	
--Cancel Elevator.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")
	
--Ladder Exit.
elseif(sObjectName == "LadderS") then
	AudioManager_PlaySound("World|ClimbLadder")
	fnCutsceneInstruction([[ AL_BeginTransitionTo("SerenityCraterA", "FORCEPOS:8.0x8.0x0") ]])
	fnCutsceneBlocker()

--[Airlocks]
elseif(sObjectName == "DoorN") then
	AL_SetProperty("Close Door", "DoorS")
	VM_SetVar("Root/Variables/Chapter5/World/iSerenityAirlockState", "N", 0.0)
	
elseif(sObjectName == "DoorS") then
    
	--If Christine is an organic:
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(sChristineForm == "Human" or sChristineForm == "Raiju") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I shouldn't try to walk into a vacuum as an organic...)") ]])
        fnCutsceneBlocker()
    
    --Otherwise, open the door.
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_SetProperty("Close Door", "DoorN")
        AL_SetProperty("Open Door", "DoorS")
        VM_SetVar("Root/Variables/Chapter5/World/iSerenityAirlockState", "N", 1.0)
    end

--[Examinables]
elseif(sObjectName == "Oilmaker") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (An oilmaker.[SOFTBLOCK] The settings are all set to maximum energy density.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Terminal") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (Notice from Command Unit 300910::[SOFTBLOCK] Please don't use the defragmentation pod for naps.[SOFTBLOCK] It is only to be used if the elevator is out and you're stranded.[SOFTBLOCK] Use the oil machine if you're low on energy.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DefragPod") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (A defragmentation pod, used by units assigned for survey work.[SOFTBLOCK] There's all sorts of dirt tracked into it.[SOFTBLOCK] Wipe your feet, units!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Intercom") then
	
	--Airlock state.
	local iSerenityAirlockState = VM_GetVar("Root/Variables/Chapter5/World/iSerenityAirlockState", "N")
	
	--Pressurized:
	if(iSerenityAirlockState == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[VOICE|Doll] Is that you, 771852?[SOFTBLOCK] If you need to discuss something, we should do it in person.") ]])
		fnCutsceneBlocker()
	
	--Unpressurized:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: (I can't use the intercom, as the airlock is unpressurized and there's no sound to hear.)") ]])
		fnCutsceneBlocker()

	end

--[Debug]
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end