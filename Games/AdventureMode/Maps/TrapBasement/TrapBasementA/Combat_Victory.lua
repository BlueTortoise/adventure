--[Combat Victory]
--The party won!

--Music change.
AudioManager_PlayMusic("TheyKnowWeAreHere")

--[Movement]
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Cultist hits the floor.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(125)
fnCutsceneBlocker()

--Prisoner walks up to Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Aquillia")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Aquillia")
	ActorEvent_SetProperty("Move To", 18.25 * gciSizePerTile, 5.50 * gciSizePerTile, 0.75)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Aquillia")
	ActorEvent_SetProperty("Move To", 18.25 * gciSizePerTile, 9.50 * gciSizePerTile, 0.75)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Aquillia")
	ActorEvent_SetProperty("Move To", 10.25 * gciSizePerTile, 9.50 * gciSizePerTile, 0.75)
DL_PopActiveObject()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Prisoner falls over again.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Aquillia")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Wait.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Surprise") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: N...[SOFTBLOCK] nice work...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Are you going to be okay?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: I need to get to a doctor...[SOFTBLOCK] Luckily, you just gave me an idea.[SOFTBLOCK] You go on ahead, I'll catch up.") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()

--[Flags]
--Make sure this flag is set.
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)

--Activate the collision flags on these entities.
EM_PushEntity("Aquillia")
	TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()
EM_PushEntity("Cultist")
	TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()