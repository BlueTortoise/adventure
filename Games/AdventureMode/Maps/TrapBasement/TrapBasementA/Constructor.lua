--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "TrapBasementA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = gci_Constructor_Start
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then
		fnStandardCharacter() 
	end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("TrapBasementA")
	
	--It's possible that the player has no map. If so, grey out the map.
	local iHasNoMap = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasNoMap", "N")
	if(iHasNoMap == 1.0) then
		AM_SetMapInfo("Null", "Null", 0, 0)
	end

	--[Special]
	--At game startup, this stuff plays to give a brief introduction to the game.
	local iShowIntroScene    = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N")
	local iSeePrisonerEscape = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iSeePrisonerEscape", "N")
	if(iShowIntroScene == 1.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 1.0)
		
		--Spawn this NPC. This prisoner gives you some basics.
		TA_Create("Aquillia")
			TA_SetProperty("Position", 17, 5)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Prisoner/Root.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/Aquillia/", false)
				
			--Special frames.
			TA_SetProperty("Wipe Special Frames")
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Aquillia|Wounded")
			TA_SetProperty("Set Special Frame", "Wounded")
		DL_PopActiveObject()
		
		--Spawn a cultist enemy.
		TA_Create("Cultist")
			TA_SetProperty("Position", 8, 10)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
				
			--Special frames.
			TA_SetProperty("Wipe Special Frames")
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
		DL_PopActiveObject()
	
	--If the player has not seen the prisoner walk past them, spawn these.
	elseif(iSeePrisonerEscape == 1.0) then
		
		--Play Music.
		AL_SetProperty("Music", "TheyKnowWeAreHere")
		
		--Prisoner is where she was before.
		TA_Create("Aquillia")
			TA_SetProperty("Position", 10, 9)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Prisoner/Root.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/Aquillia/", false)
				
			--Special frames.
			TA_SetProperty("Wipe Special Frames")
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Aquillia|Wounded")
			TA_SetProperty("Set Special Frame", "Wounded")
		DL_PopActiveObject()
		
		--Spawn a cultist enemy.
		TA_Create("Cultist")
			TA_SetProperty("Position", 8, 10)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
				
			--Special frames.
			TA_SetProperty("Wipe Special Frames")
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
			TA_SetProperty("Set Special Frame", "Wounded")
		DL_PopActiveObject()
	
	--Other case, play music.
	else
		AL_SetProperty("Music", "TheyKnowWeAreHere")
	
	end

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then
end
