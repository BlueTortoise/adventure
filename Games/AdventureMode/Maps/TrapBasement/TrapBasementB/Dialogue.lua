--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = "None"
sActorName = TA_GetProperty("Name")

--Dialogue opening.
if(sTopicName == "Hello") then
	
    --Debug Vendor.
    if(sActorName == "HumanA") then
    
        --Set facing.
        TA_SetProperty("Face Character", "PlayerEntity")

        --Setup.
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Debug Vendor\", \"" .. sBasePath .. "Shop Setup.lua\", \"" .. sBasePath .. "Shop Teardown.lua\")"

        --Run the shop.
        fnCutsceneInstruction(sString)
        fnCutsceneBlocker()
        
	end
end
