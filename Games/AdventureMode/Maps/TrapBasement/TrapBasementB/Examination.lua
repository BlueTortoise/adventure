--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examinations]
--Don't go back to the previous room.
if(sObjectName == "ToBasementC") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("TrapBasementC", "FORCEPOS:56.0x35.0x0")
	
elseif(sObjectName == "Debug") then
	fnStandardDialogue("Thought:[VOICE|Leader] (Better not go back through there, that was pretty awkward.)")
    
elseif(sObjectName == "DontGoBack") then
	fnStandardDialogue("Thought:[VOICE|Leader] (Better not go back through there, that was pretty awkward.)")

--Examining the locked door...
elseif(sObjectName == "LockedDoor") then
	
	--Variables.
	local iHasSeenCultistMeetingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenCultistMeetingScene", "N")
	
	--If the player has not seen the cultist meeting scene:
	if(iHasSeenCultistMeetingScene == 0.0) then
		fnStandardDialogue("[VOICE|Mei](This door seems to be opened remotely.)")
	
	--If the player has seen the meeting scene, the door guard opens the door.
	else
	
		--This is a cutscene. Door guard faces left.
		Cutscene_CreateEvent("Face Guard East", "Actor")
			ActorEvent_SetProperty("Subject Name", "DoorCultist")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--Guard speaks.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Door Guard:[VOICE|CultistF] Hold on, I'll get it.") ]])
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--Guard faces north.
		Cutscene_CreateEvent("Face Guard North", "Actor")
			ActorEvent_SetProperty("Subject Name", "DoorCultist")
			ActorEvent_SetProperty("Face", 0, -1)
		DL_PopActiveObject()
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--Play sound, flip the switch.
		fnCutsceneInstruction([[AudioManager_PlaySound("World|FlipSwitch")]])
		fnCutsceneInstruction([[AL_SetProperty("Switch State", "DoorSwitch", true)]])
		fnCutsceneBlocker()
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Open the door.
		fnCutsceneInstruction([[AL_SetProperty("Open Door", "DoorB")]])
		fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
		fnCutsceneBlocker()
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--Guard speaks.
		Cutscene_CreateEvent("Face Guard South", "Actor")
			ActorEvent_SetProperty("Subject Name", "DoorCultist")
			ActorEvent_SetProperty("Face", 0, 1)
		DL_PopActiveObject()
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Door Guard:[VOICE|CultistF] Good to go!") ]])
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
	end

--You can't activate this switch manually.
elseif(sObjectName == "DoorSwitch") then

--Secret passage! Most players won't find this!
elseif(sObjectName == "TrickWall") then

	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](There's something funny about the wall here.[SOFTBLOCK] Mess with it?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Search Around\", " .. sDecisionScript .. ", \"Search\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Leave It\",  " .. sDecisionScript .. ", \"Leave\") ")
	fnCutsceneBlocker()
	
--Go to the next room.
elseif(sObjectName == "Search") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Mei](There's an indentation here...[SOFTBLOCK] A secret passage opened up!)") ]])
	fnCutsceneBlocker()

	--Transfer.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iEnteredSecretPassage", "N", 1.0)
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|RemoteDoor") ]])
	fnCutsceneInstruction([[ AL_BeginTransitionTo("TrapBasementSecret", "Null") ]])
	fnCutsceneBlocker()
	
--Forget it.
elseif(sObjectName == "Leave") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end