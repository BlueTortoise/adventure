--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
if(sObjectName == "ToBasementB") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("TrapBasementB", "FORCEPOS:10.0x15.0x0")
	
elseif(sObjectName == "ToBasementD") then

	--Variables.
	local iKeyCount = AdInv_GetProperty("Item Count", "Cultist Key")
	local iDoorOpenedA = VM_GetVar("Root/Variables/Chapter1/Scenes/iDoorOpenedA", "N")
	
	--No key.
	if(iKeyCount < 1) then
		AudioManager_PlaySound("World|RemoteDoor")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Locked.[SOFTBLOCK] I'm pretty sure this is the way out, I'd better look for a key.)") ]])
		fnCutsceneBlocker()
	
	--Open, show message.
	elseif(iDoorOpenedA == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iDoorOpenedA", "N", 1.0)
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The key fit the lock!)") ]])
		fnCutsceneBlocker()
		
		--Level transition.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutsceneInstruction([[ AL_BeginTransitionTo("TrapBasementD", "FORCEPOS:4.0x11.0x0") ]])
		fnCutsceneBlocker()
	
	--Don't allow the player back into this room.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Better not risk going in there again, in case there's another meeting...)") ]])
		fnCutsceneBlocker()
	end

elseif(sObjectName == "ToBasementE") then

	--Variables.
	local iKeyCount = AdInv_GetProperty("Item Count", "Cultist Key")
	local iDoorOpenedB = VM_GetVar("Root/Variables/Chapter1/Scenes/iDoorOpenedB", "N")
	
	--No key.
	if(iKeyCount < 1) then
		AudioManager_PlaySound("World|RemoteDoor")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Locked.[SOFTBLOCK] There must be a key somewhere...)") ]])
		fnCutsceneBlocker()
	
	--Open, show message.
	elseif(iDoorOpenedB == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iDoorOpenedB", "N", 1.0)
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The key fit the lock!)") ]])
		fnCutsceneBlocker()
		
		--Level transition.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutsceneInstruction([[ AL_BeginTransitionTo("TrapBasementE", "FORCEPOS:22.0x51.0x0") ]])
		fnCutsceneBlocker()
	
	--Level transition.
	else
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutsceneInstruction([[ AL_BeginTransitionTo("TrapBasementE", "FORCEPOS:22.0x51.0x0") ]])
		fnCutsceneBlocker()
	end

--[Examinations]
elseif(sObjectName == "LockedDoor") then

	--Variables.
	local iKeyCount = AdInv_GetProperty("Item Count", "Cultist Key")
	local iDoorOpenedC = VM_GetVar("Root/Variables/Chapter1/Scenes/iDoorOpenedC", "N")
	
	--No key.
	if(iKeyCount < 1) then
		AudioManager_PlaySound("World|RemoteDoor")
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Locked.[SOFTBLOCK] There must be a key somewhere...)") ]])
		fnCutsceneBlocker()
	
	--Open, show message.
	elseif(iDoorOpenedC == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iDoorOpenedC", "N", 1.0)
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The key fit the lock!)") ]])
		fnCutsceneBlocker()
		
		--Open it.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockedDoor") ]])
		fnCutsceneBlocker()
	
	--Level transition.
	else
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutsceneInstruction([[ AL_SetProperty("Open Door", "LockedDoor") ]])
		fnCutsceneBlocker()
	end

elseif(sObjectName == "Bed") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (An unkempt bed.)") ]])
	fnCutsceneBlocker()
		
elseif(sObjectName == "Vinegar") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The barrel is full of vinegar.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FoodBoxes") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (The crate is full of cured meats, jerky, and vegetables.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "JuiceBarrels") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Barrels full of juice, milk, water, and other drinks.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FoodShelf") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Some unappetizing looking food.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Junk") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Junk.[SOFTBLOCK] Nothing of note.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteInitiate") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Initiates' Barracks)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteVinegar") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Vinegar Storage.[SOFTBLOCK] VERY IMPORTANT.[SOFTBLOCK] VINEGAR TASTES GREAT, GUARD THIS WITH YOUR LIVES.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteFurniture") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Furniture Storage)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteStudy") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Study)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteRiver") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (If you are fishing in the underground river, throw back any smallmouth bass you catch.[SOFTBLOCK] Brother Joseph is allergic.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteAndromeda") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Sister Andromeda is hosting a dance class Tuesdays and Thursdays at 7::00pm.[SOFTBLOCK] Sign-up in person!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteAdept") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Adepts only beyond this point.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteDining") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Dining Room.[SOFTBLOCK] Please clean your table after using it.[SOFTBLOCK] Thank you!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteFood") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Food storage.[SOFTBLOCK] Sorry, no pineapples this month.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteDrinks") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Drink Storage.[SOFTBLOCK] Please drink the excess pineapple juice first, thank you.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "NoteAcolyte") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Acolyte's Barracks.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "MeetingRoom") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Meeting Room.[SOFTBLOCK] There's a list of meetings scheduled below the title.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Washroom") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Washrooms.[SOFTBLOCK] I think I'm fine as I am.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfA") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Psalms and Hymns'.[SOFTBLOCK] Exactly what the cover says it is.[SOFTBLOCK] The hymns reference hands and grasping a lot...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfB") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Tour Guide'.[SOFTBLOCK] Maybe I can find out where I am!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Nope, it's actually a story about a tour guide who falls in love with a tourist.[SOFTBLOCK] Darn.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfC") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Bidoof the Camel'.[SOFTBLOCK] A charming children's book about a camel who spits on people.[SOFTBLOCK] There are 40 pages of illustrations of a camel spitting on people in different clothes.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfD") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Thoughts on Weheism'.[SOFTBLOCK] I'm sure this is very useful to someone, but I don't even know what Weheism is.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfE") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('The Seven Habits of Highly Defective People'.[SOFTBLOCK] The first habit is 'Not counting correctly'...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (And then the book ends, and the rest of the pages are blank!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfF") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Jokes in Games:: Good or Bad?'.[SOFTBLOCK] A reasoned critique of jokes being present in games of various kinds.[SOFTBLOCK] I feel like I'm the victim of some sort of meta-humour right now.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfG") then
	
	--Variables.
	local iPlatinaBook = VM_GetVar("Root/Variables/Chapter1/Scenes/iPlatinaBook", "N")
	if(iPlatinaBook == 0.0) then
		AdInv_SetProperty("Add Platina", 200)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iPlatinaBook", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('200 Platina'.[SOFTBLOCK] It's a story about someone who finds 200 platina in a book...[SOFTBLOCK] Hey!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|World|TakeItem](There was 200 Platina lodged in the back of the book!") ]])
		fnCutsceneBlocker()
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('200 Platina'.[SOFTBLOCK] Despite finding free money earlier, the story isn't well written.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (That said, I'd still definitely give it a good review! Bribe your critics, kids!)") ]])
		fnCutsceneBlocker()
	end
	
elseif(sObjectName == "BookshelfH") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('How to Tell if you're in a Cult'.[SOFTBLOCK] I guess nobody here has read this one yet.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfI") then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] ('Two People Get Beat Up In a Bar'.[SOFTBLOCK] The heartwarming story of boy-meets-boy.[SOFTBLOCK] They get into a bar fight and fall in love.[SOFTBLOCK] I love happy endings!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "LadderUp") then

	--Variables.
	local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
	local iReplacedLadder = VM_GetVar("Root/Variables/Chapter1/Scenes/iReplacedLadder", "N")
	if(iReplacedLadder == 1.0) then return end

	--Flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iReplacedLadder", "N", 1.0)

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A spare ladder...[SOFTBLOCK] I've got an idea...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Mei", 54.75, 21.50)
	if(bIsFlorentinaInParty == true) then
		fnCutsceneMove("Florentina", 56.25, 19.50)
		fnCutsceneFace("Florentina", -1, 1)
	end
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I think this will work...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Mei", 55.25, 18.50)
	fnCutsceneFace("Mei", -1, 0)
	fnCutsceneBlocker()
	if(bIsFlorentinaInParty == true) then
		fnCutsceneFace("Florentina", -1, -1)
	end
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "LadderUp", true) ]])
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|RemoteDoor") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("Mei", 54.75, 21.50)
	fnCutsceneFace("Mei", 0, 1)
	fnCutsceneBlocker()
	if(bIsFlorentinaInParty == true) then
		fnCutsceneFace("Florentina", -1, -1)
	end
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "LadderDn", false) ]])
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|RemoteDoor") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	if(bIsFlorentinaInParty == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (There, that should work as a short-cut!)") ]])
		fnCutsceneBlocker()
	
	--Florentina dialogue.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] There, I made a short-cut![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Love the work-ethic, kid.[SOFTBLOCK] You should come do menial labour in my shop.") ]])
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutsceneMove("Florentina", 54.75, 19.50)
		fnCutsceneMove("Florentina", 54.75, 21.50)
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
	
	--Change collisions.
	AL_SetProperty("Set Collision", 54, 22, 0, 14)
	AL_SetProperty("Set Collision", 54, 23, 0, 14)
	AL_SetProperty("Set Collision", 55, 22, 0, 16)
	AL_SetProperty("Set Collision", 55, 23, 0, 16)

--[Debug]
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end