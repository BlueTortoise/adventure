--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Prisoner Escape Scene]
--Plays once near the start of the game. Mei watches her prisoner friend escape.
if(sObjectName == "SeePrisonerEscape") then

	--[Variables]
	--Don't play this scene twice.
	local iSeePrisonerEscape = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iSeePrisonerEscape", "N")
	if(iSeePrisonerEscape == 0.0) then return end
	
	--Set this flag to prevent double-playing.
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iSeePrisonerEscape", "N", 0.0)
	
	--[Spawning]
	TA_Create("Aquillia")
		TA_SetProperty("Position", 56, 35)
		TA_SetProperty("Facing", gci_Face_North)
		TA_SetProperty("Clipping Flag", false)
		fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
	DL_PopActiveObject()

	--Sound.
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
	fnCutsceneWait(65)
	fnCutsceneBlocker()

	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Aquillia] Hold up a second...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--[Movement]
	fnCutsceneMove("Aquillia", 52.25, 35.50)
	fnCutsceneMove("Aquillia", 52.25, 29.50)
	fnCutsceneFace("Aquillia", -1, 0)
	fnCutsceneMove("Mei", 51.25,29.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--[Dialogue]
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Cultist") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: How do I look?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] You stole her clothes?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: H-[SOFTBLOCK]heh.[SOFTBLOCK] Sorry there's only one set.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: I splinted my arm.[SOFTBLOCK] Here, you can use my Doctor Bag.[SOFTBLOCK] I should be able to get out of here by myself.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|TakeItem]*Received Doctor Bag*.[SOFTBLOCK] You can access it from the pause menu under 'Doctor Bag'.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: You can use that bag to heal up when you're not in a fight.[SOFTBLOCK] It regenerates charges if you defeat enemies quickly, too, so get aggressive with these jerks.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Thanks for the advice.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We should probably split up.[SOFTBLOCK] I don't want to give you away.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: I thought the same thing.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: If you don't make it...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I'm not scared of these goons![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Prisoner: All right, all right![SOFTBLOCK] Wish me luck.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Good luck!") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Set the Doctor Bag charges.
    gbAutoSetDoctorBagCurrentValues = true
    LM_ExecuteScript(gsComputeDoctorBagTotalPath)

	--[Movement]
	--Mei moves out of the way. Prisoner leaves and teleports away.
	fnCutsceneMoveFace("Mei", 51.25, 28.50, 0, 1)
	fnCutsceneMove("Aquillia", 50.25, 29.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Mei", -1, 1)
	fnCutsceneMove("Aquillia", 45.25, 29.50)
	fnCutsceneMove("Aquillia", 45.25, 32.50)
	fnCutsceneMove("Aquillia", 35.25, 32.50)
	fnCutsceneBlocker()
	fnCutsceneTeleport("Aquillia", -100.0, -100.0)
	fnCutsceneBlocker()
	
end