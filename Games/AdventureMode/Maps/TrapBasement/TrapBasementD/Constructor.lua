--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "TrapBasementD"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = gci_Constructor_Start
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "NULL")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then
		fnStandardCharacter() 
	end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("TrapBasementD")
	
	--It's possible that the player has no map. If so, grey out the map.
	local iHasNoMap = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasNoMap", "N")
	if(iHasNoMap == 1.0) then
		AM_SetMapInfo("Null", "Null", 0, 0)
	end

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Cultists around the meeting table.
	fnStandardNPC("Cultist A",  5,  7, gci_Face_East,  "CultistF", "Trap_Basement/None")
	fnStandardNPC("Cultist B",  7,  8, gci_Face_North, "CultistM", "Trap_Basement/None")
	fnStandardNPC("Cultist C", 10,  6, gci_Face_West,  "CultistF", "Trap_Basement/None")
	fnStandardNPC("Cultist D",  8,  5, gci_Face_South, "CultistM", "Trap_Basement/None")
	fnStandardNPC("Cultist E", 10,  5, gci_Face_West,  "CultistF", "Trap_Basement/None")
	fnStandardNPC("Cultist F",  9,  5, gci_Face_East,  "CultistM", "Trap_Basement/None")

end
