--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examinations]
--Snacks! Lovely!
if(sObjectName == "Snacks") then
	fnStandardDialogue("[VOICE|Mei](Candy and chocolates?[SOFTBLOCK] Seems to be a snack shelf...)")
	
elseif(sObjectName == "Booze") then
	fnStandardDialogue("[VOICE|Mei](Smells like...[SOFTBLOCK] moonshine.[SOFTBLOCK] Glad I don't drink...)")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end