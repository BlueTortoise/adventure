--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--To the Dimensional Trap.
if(sObjectName == "SecretDoor") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("TrapBasementB", "FORCEPOS:10.0x25.0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end