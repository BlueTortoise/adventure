--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "TrapDungeonA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = gci_Constructor_Start
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "TheyKnowWeAreHere")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then
		fnStandardCharacter() 
	end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("TrapDungeonA")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Variables.
	local iMeiUnlockedVineDoor         = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N")
	local iCompletedTrapDungeon        = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	local iSeenAlrauneBattleIntroScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N")

	--If Mei has already unlocked the vine door, open it here.
	if(iMeiUnlockedVineDoor == 1.0) then
		AL_RemoveObject("Door", "VineDoor")
	end
	
	--If we haven't seen the battle scene yet, spawn these NPCs.
	if(iSeenAlrauneBattleIntroScene == 0.0) then
		
		--Generic Alraune spawner.
		TA_Create("AlrauneA")
			TA_SetProperty("Position", 15, 23)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootA.lua")
		DL_PopActiveObject()
		TA_Create("AlrauneB")
			TA_SetProperty("Position", 17, 26)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootB.lua")
		DL_PopActiveObject()
		TA_Create("AlrauneC")
			TA_SetProperty("Position", 13, 24)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootC.lua")
		DL_PopActiveObject()
		TA_Create("AlrauneD")
			TA_SetProperty("Position", 13, 23)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootD.lua")
		DL_PopActiveObject()
		TA_Create("AlrauneE")
			TA_SetProperty("Position", 9, 19)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootE.lua")
		DL_PopActiveObject()
		
		--Rochea is here.
		TA_Create("Rochea")
			TA_SetProperty("Position", 17, 19)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Rochea/Root.lua")
		DL_PopActiveObject()

		--Spawn this cultist offscreen. She's used later in the scene.
		TA_Create("Cultist")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
		DL_PopActiveObject()
	end
	
	--Regardless of whether we've seen the scene or not, spawn these Alraunes. They're holding the cultists back.
	TA_Create("AlrauneF")
		TA_SetProperty("Position", 29, 20)
		TA_SetProperty("Clipping Flag", false)
		fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootF.lua")
	DL_PopActiveObject()
	TA_Create("AlrauneG")
		TA_SetProperty("Position", 31, 20)
		TA_SetProperty("Clipping Flag", false)
		fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootF.lua")
	DL_PopActiveObject()
	
	--If the dungeon is complete, spawn these Alraunes. This also spawns Rochea.
	if(iCompletedTrapDungeon == 1.0) then
		TA_Create("AlrauneA")
			TA_SetProperty("Position", 14, 23)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootA.lua")
		DL_PopActiveObject()
		TA_Create("AlrauneB")
			TA_SetProperty("Position", 17, 23)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootB.lua")
		DL_PopActiveObject()
		TA_Create("Rochea")
			TA_SetProperty("Position", 16, 24)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_Rochea/RootDungeonAfter.lua")
		DL_PopActiveObject()
		
	--If the variable is 2.0, set it to 1.0 and don't spawn the Alraunes.
	elseif(iCompletedTrapDungeon == 2.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N", 1.0)
	end
	
end
