--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examinations]
--Door covered in vines.
if(sObjectName == "VineDoor") then
	
	--Note: It's not possible to get here without Florentina being present.
	local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiUnlockedVineDoor = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N")
	
	--Always trip this flag:
	VM_SetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N", 1.0)
	
	--If Mei is not an Alraune:
	if(sMeiForm ~= "Alraune") then
		
		--Set this flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N", 2.0)
	
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hm, this door is covered in vines.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I can't get it open![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Give me a minute.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] How rude![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I didn't say anything -[SOFTBLOCK] did I?[SOFTBLOCK] I'm sorry...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Wha-[SOFTBLOCK] no, the vine.[SOFTBLOCK] He's insulting me...[SOFTBLOCK] I think.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You can -[SOFTBLOCK] oh right, talking to plants.[SOFTBLOCK] I forgot.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I've half a mind to cut you up, vine![SOFTBLOCK] What do you think of that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] I can't understand anything he's saying.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But I thought - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] His accent is really thick.[SOFTBLOCK] I don't think he can understand me, either.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] So what do we do?[SOFTBLOCK] Cut our way through?[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] This vine is here for a reason, and it looks like it's enchanted.[SOFTBLOCK] It might go badly for us if we cut it up.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Enchanted?[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] There's Alraune magic afoot.[SOFTBLOCK] And not mine, I mean.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Maybe if we had someone who could understand this nightshade...") ]])
		fnCutsceneBlocker()

	--If Mei is an Alraune, and has not inspected the door before:
	elseif(iMeiUnlockedVineDoor == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N", 1.0)
	
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm, this door is covered in vines.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I can't get it open![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Give me a minute.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] How rude![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] What?[SOFTBLOCK] He didn't say anything mean...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You can understand that thick accent?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Huh...[SOFTBLOCK] I think this vine is a nightshade, like you.[SOFTBLOCK] Maybe.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What's that, little guy?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Yeah![SOFTBLOCK] I guess we are![SOFTBLOCK] That's so cool.[SOFTBLOCK] Maybe I have some of your pollen in me, little one![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah yeah, skip the family reunion.[SOFTBLOCK] Can you get us through?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Apparently the other leaf-sisters asked him to bar this door.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] But I think he's willing to make an exception for us.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Yeah, thanks![SOFTBLOCK] Sure, sure.[SOFTBLOCK] I'll tell her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] What?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] He says you're very pretty.[SOFTBLOCK] I think he likes you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Well I do like a vine with an accent...") ]])
		fnCutsceneBlocker()

		--Remove the door.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutsceneInstruction([[ AL_RemoveObject("Door", "VineDoor") ]])
		fnCutsceneBlocker()


	--If Mei is an Alraune, and has inspected the door before:
	else
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N", 1.0)
	
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hello, little one![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Back to the gibberish-talking vine, I see.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh really?[SOFTBLOCK] *Oh really*?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You can understand him?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yep![SOFTBLOCK] Perfectly clearly![SOFTBLOCK] Apparently we're a similar species of nightshade![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] So we're, sorta,[SOFTBLOCK] like,[SOFTBLOCK] cousins?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Yeah whatever.[SOFTBLOCK] Can you get us through the door or not?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hmm, he said the other Alraunes asked him to block off this door, and gave him a magical boost.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But he'll make an exception because...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Because...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] He says you're very pretty.[SOFTBLOCK] I think he likes you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Oh, well.[SOFTBLOCK] Uh.[SOFTBLOCK] Okay.[SOFTBLOCK] Onward!") ]])
		fnCutsceneBlocker()

		--Remove the door.
		fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutsceneInstruction([[ AL_RemoveObject("Door", "VineDoor") ]])
		fnCutsceneBlocker()
	
	end

--Door that leads further into cultist territory.
elseif(sObjectName == "Unenterable") then
	fnStandardDialogue("(Looks like the Alraunes blocked this door to keep the cultists from getting through...)")

--Tunnel dug by the Alraunes.
elseif(sObjectName == "AlrauneTunnel") then
	fnStandardDialogue("(Seems this is where the Alraunes broke through.[SOFTBLOCK] Not much of interest in there.)")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end