--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "TrapDungeonB"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = gci_Constructor_Start
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "TheyKnowWeAreHere")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then
		fnStandardCharacter() 
	end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("TrapDungeonA")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Variables.
	local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	
	--Spawn everyone if the dungeon is not completed yet.
	if(iCompletedTrapDungeon == 0.0) then
	
		--Spawn some Alraunes and downed Cultists.
		TA_Create("CultistA")
			TA_SetProperty("Position", 7, 15)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
			TA_SetProperty("Set Special Frame", "Wounded")
		DL_PopActiveObject()
		TA_Create("AlrauneA")
			TA_SetProperty("Position", 8, 15)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootA.lua")
		DL_PopActiveObject()
		
		TA_Create("CultistB")
			TA_SetProperty("Position", 11, 9)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
			TA_SetProperty("Set Special Frame", "Wounded")
		DL_PopActiveObject()
		TA_Create("AlrauneB")
			TA_SetProperty("Position", 10, 9)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootB.lua")
		DL_PopActiveObject()
		TA_Create("AlrauneC")
			TA_SetProperty("Position", 5, 6)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootC.lua")
		DL_PopActiveObject()
		TA_Create("AlrauneD")
			TA_SetProperty("Position", 8, 18)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_TrapDungeon/RootD.lua")
		DL_PopActiveObject()
		
		--Rochea. She has special dialogue for this spot.
		TA_Create("Rochea")
			TA_SetProperty("Position", 9, 14)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_Rochea/RootDungeon.lua")
		DL_PopActiveObject()
	end

end
