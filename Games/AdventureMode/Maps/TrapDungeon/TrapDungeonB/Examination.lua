--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Examinations]
--Barrel.
if(sObjectName == "Barrel") then
	fnStandardDialogue("(Dried sardines.[SOFTBLOCK] Nutritious, if you like that sort of thing.)")

--Barrel G. Used for yellow barrels.
elseif(sObjectName == "BarrelG") then
	fnStandardDialogue("(Prune juice.[SOFTBLOCK] Yum![SOFTBLOCK] Sort of!)")
	
--Junk.
elseif(sObjectName == "Junk") then
	fnStandardDialogue("(Personal possessions of the cultists.[SOFTBLOCK] Combs, toothpicks, spare ritual knives...)")
	
--Bed. Cultists have them!
elseif(sObjectName == "Bed") then
	fnStandardDialogue("(The cultists have at least somewhat nice beds.[SOFTBLOCK] They smell pretty bad, though...)")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end