--[Constructor]
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "TrapDungeonC"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = gci_Constructor_Start
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

--[Standard]
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "LAYER|CultistProfile")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then
		fnStandardCharacter() 
	end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("TrapDungeonC")

--[Post-Exec]
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Variables.
	local iMetSpecialAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSpecialAcolyte", "N")

	--If the party has not met this Acolyte yet, spawn her.
	if(iMetSpecialAcolyte == 0.0) then
		TA_Create("Cultist")
			TA_SetProperty("Position", 5, 8)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/TrappedCultist/Root.lua")
		DL_PopActiveObject()
	end
	
	--This door starts in the opening position.
	AL_SetProperty("Open Door", "Door04")
	
	--If the special acolyte flag is 2.0, all the doors start in the open position.
	if(iMetSpecialAcolyte == 2.0) then
		AL_SetProperty("Open Door", "Door00")
		AL_SetProperty("Open Door", "Door01")
		AL_SetProperty("Open Door", "Door02")
		AL_SetProperty("Open Door", "Door03")
		AL_SetProperty("Open Door", "Door04")
		AL_SetProperty("Open Door", "Door05")
		AL_SetProperty("Open Door", "Door06")
		AL_SetProperty("Open Door", "Door07")
		AL_SetProperty("Open Door", "Door08")
		AL_SetProperty("Open Door", "Door09")
		AL_SetProperty("Open Door", "Door10")
	end
end
