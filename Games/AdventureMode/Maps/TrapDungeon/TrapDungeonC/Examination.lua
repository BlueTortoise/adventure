--[Examination]
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Functions]
--Function used to open and close most of the doors.
if(fnSwitchDoor == nil) then
	 function fnSwitchDoor(sSwitchName, sDoorNameA, sDoorNameB, bIsSwitchUp)
		
		--Arg check. sDoorNameB can be nil.
		if(sSwitchName == nil) then return end
		if(sDoorNameA == nil) then return end
		if(bIsSwitchUp == nil) then return end
		
		--Switch is currently down. Raise it, open the door.
		if(bIsSwitchUp == false) then
			
			--Change switch state.
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
			fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"" .. sSwitchName .. "\", true) ")
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
			
			--Open the door.
			fnCutsceneInstruction("AL_SetProperty(\"Open Door\", \"" .. sDoorNameA ..  "\")")
			if(sDoorNameB ~= nil) then
				fnCutsceneInstruction("AL_SetProperty(\"Open Door\", \"" .. sDoorNameB ..  "\")")
			end
			fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
		
		--It's up. Lower it, close the door.
		else
			
			--Change switch state.
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
			fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"" .. sSwitchName .. "\", false) ")
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
			
			--Open the door.
			fnCutsceneInstruction("AL_SetProperty(\"Close Door\", \"" .. sDoorNameA ..  "\")")
			if(sDoorNameB ~= nil) then
				fnCutsceneInstruction("AL_SetProperty(\"Close Door\", \"" .. sDoorNameB ..  "\")")
			end
			fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
		end
	end
end
--[Notes]
--The switches generally switch the gates with the same name as the switch. The puzzle is fairly simple.
-- More complex puzzles are probably in the offing, but this is Chapter 1. Go easy!
--Switches start in the down state.

--Switchs variable.
local bIsSwitchUp        = AL_GetProperty("Switch State")
local iMetSpecialAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSpecialAcolyte", "N")
	
--[Examinations]
--Switches. Most use the same setup.
if(sObjectName == "Switch00") then
	AL_SetProperty("Switch Handled Update")
	
	if(iMetSpecialAcolyte < 2.0) then
		fnSwitchDoor(sObjectName, "Door00", nil, bIsSwitchUp)
	else
		fnStandardDialogue("(The switch mechanism is broken...)")
	end

--Common.
elseif(sObjectName == "Switch01") then
	AL_SetProperty("Switch Handled Update")
	
	if(iMetSpecialAcolyte < 2.0) then
		fnSwitchDoor(sObjectName, "Door01", nil, bIsSwitchUp)
	else
		fnStandardDialogue("(The switch mechanism is broken...)")
	end

--Common.
elseif(sObjectName == "Switch0206") then
	AL_SetProperty("Switch Handled Update")
	
	if(iMetSpecialAcolyte < 2.0) then
		fnSwitchDoor(sObjectName, "Door02", "Door06", bIsSwitchUp)
	else
		fnStandardDialogue("(The switch mechanism is broken...)")
	end
	
	--Variables.
	local iMeiHatesPuzzles = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiHatesPuzzles", "N")
	
	--If this flag is not set, show a brief cutscene.
	if(iMeiHatesPuzzles == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiHatesPuzzles", "N", 1.0)
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hey wait a minute...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] This is a puzzle![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] I HATE PUZZLES!![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Calm down there, skippy.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Let's just get this over with, okay?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
	end

--Common.
elseif(sObjectName == "Switch08") then
	AL_SetProperty("Switch Handled Update")
	
	if(iMetSpecialAcolyte < 2.0) then
		fnSwitchDoor(sObjectName, "Door08", nil, bIsSwitchUp)
	else
		fnStandardDialogue("(The switch mechanism is broken...)")
	end

--Common.
elseif(sObjectName == "Switch09") then
	AL_SetProperty("Switch Handled Update")
	
	if(iMetSpecialAcolyte < 2.0) then
		fnSwitchDoor(sObjectName, "Door09", nil, bIsSwitchUp)
	else
		fnStandardDialogue("(The switch mechanism is broken...)")
	end

--Common.
elseif(sObjectName == "Switch07") then
	AL_SetProperty("Switch Handled Update")
	
	if(iMetSpecialAcolyte < 2.0) then
		fnSwitchDoor(sObjectName, "Door07", nil, bIsSwitchUp)
	else
		fnStandardDialogue("(The switch mechanism is broken...)")
	end

--Pulled in a cutscene. Player can't actually throw it manually.
elseif(sObjectName == "BreakSwitch") then
	AL_SetProperty("Switch Handled Update")
	fnStandardDialogue("(The switch mechanism is broken...)")

--FloodSwitch. This switch swaps two door states.
elseif(sObjectName == "FloodSwitch" or sObjectName == "FloodSwitchN") then

	--Common.
	AL_SetProperty("Switch Handled Update")
		
	--Normal case: Just handle pushing the switch:
	if(iMetSpecialAcolyte == 0.0) then
	
		--Switch is currently down. Raise it, open Door03 and close Door04.
		if(bIsSwitchUp == false) then
			
			--Change switch state.
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
			fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"FloodSwitch\", true) ")
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
			
			--Open the door.
			fnCutsceneInstruction("AL_SetProperty(\"Open Door\", \"Door03\")")
			fnCutsceneInstruction("AL_SetProperty(\"Close Door\", \"Door04\")")
			fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
		
		--It's up. Lower it, close the door.
		else
			
			--Change switch state.
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
			fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"FloodSwitch\", false) ")
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
			
			--Open the door.
			fnCutsceneInstruction("AL_SetProperty(\"Open Door\", \"Door04\")")
			fnCutsceneInstruction("AL_SetProperty(\"Close Door\", \"Door03\")")
			fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
		end

	--First case: A cutscene plays out.
	elseif(iMetSpecialAcolyte == 1.0) then

		--First, if the switch is currently up, activate the switch as normal.
		if(bIsSwitchUp == false) then
			
			--Change switch state.
			fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
			fnCutsceneInstruction("AL_SetProperty(\"Switch State\", \"FloodSwitch\", true) ")
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
			
			--Open the door.
			fnCutsceneInstruction("AL_SetProperty(\"Open Door\", \"Door03\")")
			fnCutsceneInstruction("AL_SetProperty(\"Close Door\", \"Door04\")")
			fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
			fnCutsceneBlocker()
			fnCutsceneWait(30)
			fnCutsceneBlocker()
		end

		--Execute the cutscene.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/TrapDungeon_SwitchScene/Scene_Begin.lua")

	--All further cases: Do nothing.
	elseif(iMetSpecialAcolyte == 2.0) then
		fnStandardDialogue("(The switch mechanism is broken...)")

	end

--Doors. All doors have the same script.
elseif(sObjectName == "Door00" or sObjectName == "Door01" or sObjectName == "Door02" or sObjectName == "Door03" or sObjectName == "Door04" or sObjectName == "Door05" or sObjectName == "Door06" or sObjectName == "Door07" or sObjectName == "Door08" or sObjectName == "Door09" or sObjectName == "Door10") then
	fnStandardDialogue("(Seems a mechanism controls this door...)")

else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end