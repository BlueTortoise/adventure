--[Triggers]
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

--[Triggers]
--Alraune cutscene.
if(sObjectName == "FlorentinaScene") then

	--If we haven't seen this scene, but the trap dungeon was completed, play the scene as the player exits.
	local iPostDungeonScene     = VM_GetVar("Root/Variables/Chapter1/Scenes/iPostDungeonScene", "N")
	local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	if(iPostDungeonScene == 0.0 and iCompletedTrapDungeon == 1.0) then
	
		--[Flag]
		VM_SetVar("Root/Variables/Chapter1/Scenes/iPostDungeonScene", "N", 1.0)
	
		--[Dialogue Sequence]
		--Dialogue setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] All right, the little ones shouldn't be able to hear us.[SOFTBLOCK] Start talking.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You put up a pretty good front back there.[SOFTBLOCK] I bet Rochea even fell for it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Well I didn't.[SOFTBLOCK] What happened back there?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Oh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Mei, I know you think I'm just in this for the money.[SOFTBLOCK] Actually, I still am.[SOFTBLOCK] I want the money.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] But that [SOFTBLOCK]*big*[SOFTBLOCK] thing?[SOFTBLOCK] That thing was wrong.[SOFTBLOCK] I've been to a lot of places. I have [SOFTBLOCK]*never*[SOFTBLOCK] seen anything like that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] But when you looked at it, it was like you...[SOFTBLOCK] respected it.[SOFTBLOCK] Like you already knew what it was.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] [SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK].[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I'm not trying to hurt you here.[SOFTBLOCK] Please.[SOFTBLOCK] Just, tell me.[SOFTBLOCK] Tell me what you know.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] The problem is that I don't know.[SOFTBLOCK] I can feel it, but I don't understand it.[SOFTBLOCK] I would need a whole new vocabulary to begin to describe it...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Try.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (Deep breaths, Mei...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I felt something surge, and my runestone seemed to react.[SOFTBLOCK] It...[SOFTBLOCK] got stronger as we got closer to that thing.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] When I saw it, I just -[SOFTBLOCK] I just knew what I was supposed to do.[SOFTBLOCK] Instantly.[SOFTBLOCK] Like I had been born for that moment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Fantastic![SOFTBLOCK] That's the best news I've heard in weeks![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] This is probably a bad time for jokes.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Tch, you're a real basket case aren't you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Mei, when you're playing cards and you don't have the best hand, do you know what you do?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Act like you do, and make whoever [SOFTBLOCK]*does*[SOFTBLOCK] have the best hand think twice about it.[SOFTBLOCK] Make 'em wonder,[SOFTBLOCK] make 'em doubt.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You put that doubt in them, you can make them fold.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And this is a card game to you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] They distill out certain parts of life.[SOFTBLOCK] Ignore them at your peril.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] The point is, I didn't feel what you did when you saw that thing.[SOFTBLOCK] Why should I?[SOFTBLOCK] I'm just some misanthrope.[SOFTBLOCK] I'm not a threat.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] But you, and that runestone, scare them.[SOFTBLOCK] So, they're trying to scare you into not using it.[SOFTBLOCK] It's a bluff.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] The harder they bluff, the more you know you've got them beat.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOFTBLOCK].[SOFTBLOCK].[SOFTBLOCK]. Wow.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Florentina, you really are the best![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] If anything, we're the most dangerous team on Pandemonium![SOFTBLOCK] They should be running from us![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] See?[SOFTBLOCK] Despair is the real enemy.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Thanks, Florentina.[SOFTBLOCK] You really are a - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Business associate.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah.[SOFTBLOCK] You're the best business associate I've ever had.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Enough yakking.[SOFTBLOCK] Let's get to finding out how to get you back to Earth.") ]])
	end
end