--[Chapter 1 Lookups]
--Sets the master list of map remaps for Chapter 1.
gsaMapLookups = {}

--[Functions]
--Variables used.
local iIndex = 1

--Adds a single lookup.
local fnAddLookup = function(psName, psMap, piX, piY)
	gsaMapLookups[iIndex] = {psName, psMap, piX, piY}
    iIndex = iIndex + 1
end

--Adds a lookup between two letters, ex: "BeehiveBasement" "A", "C", will do A, B, and C, in order.
local fnAddLookups = function(psNamePattern, psStartLetter, psEndLetter, psMap, piX, piY)
    
	--Setup.
	local sCurrentLetter = psStartLetter
	
	--Range check. If the letter is out of the normal range, fail.
	local iByteValue = string.byte(sCurrentLetter)
	if(iByteValue < string.byte("A") or iByteValue > string.byte("Z")) then return end
	
	--Iterate across the letters. Always stops on Z. Also always stops on non-capitals.
	while(true) do
		
		--Add it.
		local sLevelName = psNamePattern .. sCurrentLetter
        gsaMapLookups[iIndex] = {sLevelName, psMap, piX, piY}
        iIndex = iIndex + 1
		
		--If this was the ending letter, or the letter 'Z', stop.
		if(sCurrentLetter == psEndLetter or sCurrentLetter == "Z") then break end
		
		--Move to the next letter.
		local iByteValue = string.byte(sCurrentLetter)
		sCurrentLetter = string.char(iByteValue + 1)
	end
    
end

--[Map Coordinates]
--Maps/Beehive
fnAddLookups("BeehiveBasement", "A", "F", "TrannadarMap",  515,   93)
fnAddLookup( "BeehiveBasementScene",      "TrannadarMap",  515,   93)
fnAddLookup( "BeehiveInner",              "TrannadarMap",  515,   93)
fnAddLookup( "BeehiveOuter",              "TrannadarMap",  515,  173)

--Maps/DimensionalTrap
fnAddLookup("TrapMainFloorCentral",   "TrannadarMap", 1280, 909)
fnAddLookup("TrapMainFloorEast",      "TrannadarMap", 1280, 909)
fnAddLookup("TrapMainFloorExterior",  "TrannadarMap", 1291, 987)
fnAddLookup("TrapMainFloorExteriorN", "TrannadarMap", 1273, 809)
fnAddLookup("TrapMainFloorSouthHall", "TrannadarMap", 1280, 909)
fnAddLookup("TrapUpperFloorE",        "TrannadarMap", 1280, 909)
fnAddLookup("TrapUpperFloorMain",     "TrannadarMap", 1280, 909)
fnAddLookup("TrapUpperFloorW",        "TrannadarMap", 1280, 909)

--Maps/Evermoon
fnAddLookup("AlrauneAndBeeScene",    "TrannadarMap",  272, 1029)
fnAddLookup("AlrauneChamber",        "TrannadarMap",  981,  876)
fnAddLookup("BreannesPitStop",       "TrannadarMap", 1436,  535)
fnAddLookup("EvermoonCassandraA",    "TrannadarMap", 1304,  145)
fnAddLookup("EvermoonCassandraAMid", "TrannadarMap", 1304,  125)
fnAddLookup("EvermoonCassandraB",    "TrannadarMap", 1304,  105)
fnAddLookup("EvermoonCassandraCC",   "TrannadarMap", 1304,  105)
fnAddLookup("EvermoonCassandraCE",   "TrannadarMap", 1304,  105)
fnAddLookup("EvermoonCassandraCNE",  "TrannadarMap", 1304,  105)
fnAddLookup("EvermoonCassandraCNW",  "TrannadarMap", 1304,  105)
fnAddLookup("EvermoonE",             "TrannadarMap", 1638,  943)
fnAddLookup("EvermoonNE",            "TrannadarMap", 1412,  235)
fnAddLookup("EvermoonNW",            "TrannadarMap",  606,  606)
fnAddLookup("EvermoonS",             "TrannadarMap", 1149, 1217)
fnAddLookup("EvermoonSEA",           "TrannadarMap", 1311, 1283)
fnAddLookup("EvermoonSEB",           "TrannadarMap", 1311, 1283)
fnAddLookup("EvermoonSEC",           "TrannadarMap", 1311, 1283)
fnAddLookup("EvermoonSlimeScene",    "TrannadarMap",  986, 1097)
fnAddLookup("EvermoonSlimeVillage",  "TrannadarMap", 1583,  831)
fnAddLookup("EvermoonSW",            "TrannadarMap",  760, 1164)
fnAddLookup("EvermoonW",             "TrannadarMap",  818,  825)
fnAddLookup("PlainsC",               "TrannadarMap", 1016,  358)
fnAddLookup("PlainsNW",              "TrannadarMap",  634,  320)
fnAddLookup("SaltFlats",             "TrannadarMap", 1080, 1362)
fnAddLookup("SaltFlatsCave",         "TrannadarMap", 1080, 1362)
fnAddLookup("TrannadarTradingPost",  "TrannadarMap",  573,  872)
fnAddLookup("WerecatScene",          "TrannadarMap",  942, 1011)

--Maps/Quantir
fnAddLookup("QuantirNW",     "TrannadarMap", 1834,  306)
fnAddLookup("QuantirNWCave", "TrannadarMap", 1956,  310)

--Maps/QuantirManse
fnAddLookup("QuantirManseBasementE",      "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseBasementW",      "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseCentralE",       "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseCentralW",       "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseEntrance",       "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseNEHall",         "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseNEHallFloor2",   "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseNWHall",         "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseSecretExit",     "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseSEHall",         "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseSWYard",         "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseSWYardInterior", "TrannadarMap", 2048, 251)
fnAddLookup("QuantirManseTruth",          "TrannadarMap", 2048, 251)
fnAddLookup("SpookyExterior",             "TrannadarMap", 2036, 306)

--Maps/Trafal
fnAddLookup("TrafalNW", "TrannadarMap", 683, 1313)

--Maps/TrapBasement
fnAddLookup( "DimensionalTrapBasementScene", "TrannadarMap", 1281, 934)
fnAddLookup( "TrapBasementA",                "TrannadarMap", 1340, 934)
fnAddLookup( "TrapBasementB",                "TrannadarMap", 1281, 934)
fnAddLookup( "TrapBasementC",                "TrannadarMap", 1281, 934)
fnAddLookup( "TrapBasementD",                "TrannadarMap", 1281, 934)
fnAddLookup( "TrapBasementE",                "TrannadarMap", 1281, 934)
fnAddLookup( "TrapBasementF",                "TrannadarMap", 1281, 934)
fnAddLookup( "TrapBasementG",                "TrannadarMap", 1241, 934)
fnAddLookup( "TrapBasementH",                "TrannadarMap", 1281, 934)
fnAddLookup( "TrapBasementSecret",           "TrannadarMap", 1281, 934)

--Maps/TrapDungeon
fnAddLookups("TrapDungeon", "A", "F", "TrannadarMap", 1177, 932)
fnAddLookup( "TrapDungeonEntry",      "TrannadarMap", 1177, 932)
