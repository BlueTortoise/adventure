--[Warp Setter]
--Script that gets called when the warp menu is activated for an area. Populates advanced map info for that area.
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain6")
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain0")
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain1",   0,   10)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain2",  11,   20)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain3",  21,   30)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain5",  31,   50)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Cryogenics/CryolabMain4",  51, 1000)