--[Warp Setter]
--Script that gets called when the warp menu is activated for an area. Populates advanced map info for that area.
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox6")
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox0")
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox1",   0,   10)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox2",  11,   20)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox3",  21,   30)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox5",  31,   50)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/Equinox/Equinox4",  51, 1000)