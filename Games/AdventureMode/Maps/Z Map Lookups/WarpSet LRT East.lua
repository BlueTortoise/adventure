--[Warp Setter]
--Script that gets called when the warp menu is activated for an area. Populates advanced map info for that area.
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast6")
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast0")
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast1",   0,   10)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast2",  11,   20)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast3",  21,   30)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast5",  31,   50)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTEast4",  51, 1000)