--[Warp Setter]
--Script that gets called when the warp menu is activated for an area. Populates advanced map info for that area.
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest6")
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest0")
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest1",   0,   10)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest2",  11,   20)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest3",  21,   30)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest5",  31,   50)
AM_SetProperty("Advanced Map", "Root/Images/AdvMaps/LRT/LRTWest4",  51, 1000)