--[ ======================================== K Pop Beats ======================================== ]
--Kitsune Pop! It's sweeping the mountains! Everyone is moving to the beat!
-- This minigame is a stupid dancing minigame in chapter 2. What a waste of time.
local sBasePath = fnResolvePath()

--[Loading]
--Run the loader script. It auto-stops if the assets are already loaded.
LM_ExecuteScript(sBasePath .. "001 Loading.lua")
--[Boot]
--Set this flag to true to spot loading errors.
Debug_SetFlag("KPopDance", false)
AL_SetProperty("Activate KPop Minigame")

--[ ==================================== Dancer Registration ==================================== ]
--[Frame Speeds]
local iaIdleTPF = {6, 6, 6, 6}
local iaMoveTPF = {1, 1, 1, 6, 6, 6}

--[Dance Move Lookups]
local zaLookups = {}
zaLookups[1] = {"Idle",       4, "Root/Images/KPopSprites/%s/Idle|", iaIdleTPF}
zaLookups[2] = {"Arm Right",  6, "Root/Images/KPopSprites/%s/ArmR|", iaMoveTPF}
zaLookups[3] = {"Arm Left",   6, "Root/Images/KPopSprites/%s/ArmL|", iaMoveTPF}
zaLookups[4] = {"Leg Right",  6, "Root/Images/KPopSprites/%s/LegR|", iaMoveTPF}
zaLookups[5] = {"Leg Left",   6, "Root/Images/KPopSprites/%s/LegL|", iaMoveTPF}
zaLookups[6] = {"Head Roll",  6, "Root/Images/KPopSprites/%s/Down|", iaMoveTPF}
zaLookups[7] = {"Hip Thrust", 6, "Root/Images/KPopSprites/%s/HipT|", iaMoveTPF}

--[Izuna]
--Create.
KPop_SetProperty("Register Dancer", "Izuna")

--Register animations.
for i = 1, #zaLookups, 1 do
    
    --Resolve.
    local sCharacterName = "Izuna"
    local sMoveName = zaLookups[i][1]
    local iMoveFrames = zaLookups[i][2]
    local iaTPFArray = zaLookups[i][4]
    
    --Create the name.
    local sPath = string.format(zaLookups[i][3], sCharacterName)
    
    --Register everything.
    KPop_SetProperty("Register Dance Move", sCharacterName, sMoveName)
    KPop_SetProperty("Set Dancer Move Frames", sCharacterName, sMoveName, iMoveFrames)
    for p = 0, iMoveFrames-1, 1 do
        KPop_SetProperty("Set Dancer Move Frame", sCharacterName, sMoveName, p, iaTPFArray[p+1], sPath .. p)
    end
end

--[ ===================================== Song Registration ===================================== ]
--Direction Constants
local ciBtnNon = 0
local ciBtnTop = 1
local ciBtnRgt = 2
local ciBtnBot = 3
local ciBtnLft = 4
local ciBtnAct = 5
local ciBtnCan = 6
local ciRollLo = ciBtnTop
local ciRollHi = ciBtnAct

--System output storage.
--KPop_SetProperty("Activate System Output", sBasePath .. "System Output.lua")

--Song storage.
LM_ExecuteScript(sBasePath .. "100 Song.lua")

--Math Constants
--[=[
local cfSecondsToTicks = 60.0

--Beat Interval
local fInterval = 0.5000

--Song Properties
local fLeadBeat = 0.0900
local fFirstBeat = 8.5900
local fTotalLen = fFirstBeat + 90.000
local fCurrent = fLeadBeat

--Lead-in is all non-presses.
while(fCurrent < fFirstBeat) do
    
    local iTickPos = math.floor(fCurrent * cfSecondsToTicks)
    KPop_SetProperty("Register Action", iTickPos, ciBtnNon)
    fCurrent = fCurrent + fInterval
end

--Actual song. Randomized for now.
while(fCurrent < fTotalLen) do
    
    --Compute how many ticks this is.
    local iTickPos = math.floor(fCurrent * cfSecondsToTicks)
    
    --Direction.
    local iRoll = LM_GetRandomNumber(ciRollLo, ciRollHi)
    
    --Upload.
    KPop_SetProperty("Register Action", iTickPos, iRoll)
    
    --Next
    fCurrent = fCurrent + fInterval
end]=]

--[ ========================================= Finish Up ========================================= ]
--All dancers registered, position them.
KPop_SetProperty("Position Dancers")

--Fire it up!
KPop_SetProperty("Set Active", true)
