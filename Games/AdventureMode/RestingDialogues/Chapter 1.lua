--[ ================================= Chapter 1: Rest Dialogues ================================= ]
--Resting dialogues for chapter 1.

--[Variables]
--Whether or not Florentina is in the party.
local bIsFlorentinaPresent = false
if(gsFollowersTotal > 0) then bIsFlorentinaPresent = true end

--Script variables.
local iCombatVictories               = VM_GetVar("Root/Variables/Chapter1/Counts/iCombatVictories", "N")
local iVictoriesWhenFlorentinaJoined = VM_GetVar("Root/Variables/Chapter1/Counts/iVictoriesWhenFlorentinaJoined", "N")

--Cutscene flags.
local iUseSpecialBeeOpening       = VM_GetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N")
local iHasSeenFlorentinaOneWin    = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaOneWin", "N")
local iHasSeenFlorentinaFiveWin   = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaFiveWin", "N")
local iHasSeenFlorentinaTwentyWin = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaTwentyWin", "N")

--[ ======================================= Scene Handlers ====================================== ]
--We need to determine whether or not a resting dialogue needs to play out at all. If so, a special
-- bypass flag gets set. Alternately, a random roll of 5% causes a scene to play out.
local bBypassRoll = false

--5% chance of playing a scene.
local iRoll = LM_GetRandomNumber(1, 100)

--[Special Handlers]
--Florentina asks Mei about the bee thing. She must have been in the party and Mei did a voluntary bee TF.
if(iUseSpecialBeeOpening == 1.0) then
    bBypassRoll = true

--If the player needs to see the Florentina one-win cutscene.
elseif(iHasSeenFlorentinaOneWin == 0.0 and iCombatVictories > iVictoriesWhenFlorentinaJoined) then
    bBypassRoll = true

--If the player needs to see the Florentina five-win cutscene.
elseif(iHasSeenFlorentinaFiveWin == 0.0 and iHasSeenFlorentinaOneWin ~= 0.0 and iCombatVictories >= iHasSeenFlorentinaOneWin + 5) then
    bBypassRoll = true

--If the player needs to see the Florentina twenty-win cutscene.
elseif(iHasSeenFlorentinaTwentyWin == 0.0 and iHasSeenFlorentinaFiveWin ~= 0.0 and iHasSeenFlorentinaOneWin ~= 0.0 and iCombatVictories >= iHasSeenFlorentinaFiveWin + 15) then
    bBypassRoll = true
end

--If the bypass is present, or a scene is mandated, proceed.
if(iRoll > 5 and bBypassRoll == false) then
	return
end

--[ ====================================== Mei Alone Scenes ===================================== ]
--Scenes when Mei is alone.
if(bIsFlorentinaPresent == false) then

--[ ===================================== Florentina Scenes ===================================== ]
--Scenes for when Florentina is in the party.
else

	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

    --[Voluntary Bee TF]
	--Florentina has a few questions about the voluntary bee TF.
	if(iUseSpecialBeeOpening == 1.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 2.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hey, Mei![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Back there in the bee hive.[SOFTBLOCK] Why'd you eat the honey?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I just -[SOFTBLOCK] I don't know.[SOFTBLOCK] Something came over me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] I wanted it more than anything else, ever.[SOFTBLOCK] If you'd tried to keep me from it -[SOFTBLOCK] I'd have struck you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Feh. [SOFTBLOCK]Probably the bee buzzing got to you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, that wasn't it.[SOFTBLOCK] Once I remembered myself, that was the first thing I asked the other drones.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] They said they hadn't done anything to me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] And that's exactly what they would say, isn't it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] They...[SOFTBLOCK] we...[SOFTBLOCK] are incapable of lying to each other.[SOFTBLOCK] It's like lying to yourself.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] People lie to themselves all the time.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I still know they did not influence me.[SOFTBLOCK] I did it of my own accord.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] If I could do it again I would...[SOFTBLOCK] I was fantasizing about it until you spoke up.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Wow...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well, in the future, please listen to me when I tell you not to drink strange fluids.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No promises.[SOFTBLOCK] Besides, I'm fine now, right?") ]])
        fnCutsceneBlocker()
        return
    end

    --[Florentina: One Combat Victory]
	--The two bond over violence.
	if(iHasSeenFlorentinaOneWin == 0.0 and iCombatVictories > iVictoriesWhenFlorentinaJoined) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaOneWin", "N", iCombatVictories)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So you're saying you're not a fighter, huh?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I swear.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I guess some people are just naturally good at this.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're not bad either.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I can handle myself.[SOFTBLOCK] Just don't get cocky.") ]])
        fnCutsceneBlocker()
        return
    end
	
    --[Florentina: Five Combat Victories]
	--She's starting to dig your prowiss, your capacity for violence.
	if(iHasSeenFlorentinaFiveWin == 0.0 and iHasSeenFlorentinaOneWin ~= 0.0 and iCombatVictories >= iHasSeenFlorentinaOneWin + 5) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaFiveWin", "N", iCombatVictories)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Don't take this the wrong way, but I think I'm starting to like you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Is my chipper attitude getting through?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] More like the sharp end of your sword.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I love a girl who loves a good beatdown...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Er, thanks I guess.") ]])
        fnCutsceneBlocker()
        return
    end
	
    --[Florentina: Twenty Combat Victories]
    --Kicking ass is how friendships are made.
	if(iHasSeenFlorentinaTwentyWin == 0.0 and iHasSeenFlorentinaFiveWin ~= 0.0 and iHasSeenFlorentinaOneWin ~= 0.0 and iCombatVictories >= iHasSeenFlorentinaFiveWin + 15) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaTwentyWin", "N", iCombatVictories)
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Mei?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yes?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're all right.[SOFTBLOCK] You've got my back, and that's where it really counts.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] It's hard to find people you can trust in Pandemonium.[SOFTBLOCK] At least in a scrap, I can count on you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thanks, Florentina.[SOFTBLOCK] I feel the same way about you.") ]])
        fnCutsceneBlocker()
        return
    end
	
    --[Random Dialogue]
    --Roll.
	local iRoll = LM_GetRandomNumber(0, 5)
    
	--Generic dialogue:
	if(iRoll == 0 or true) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hey, want to swap scary stories?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I'm not scared of anything![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh really?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Not even [SPOOKY]SKELETONS[NOSPOOKY]?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] O-[SOFTBLOCK]Of course not![SOFTBLOCK] [SPOOKY]SKELETONS[NOSPOOKY] are n-n-nothing![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I'm getting chills just thinking about them.[SOFTBLOCK] Let's just get going, okay?") ]])
	end
	fnCutsceneBlocker()
end