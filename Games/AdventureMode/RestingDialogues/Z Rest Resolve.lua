--[ ======================================== Rest Resolve ======================================= ]
--Resolve what to do during a rest sequence. This can involve firing cutscenes, dialogues, or modifying variables.
-- Note that this script fires *after* the variables in the RestReset category are reset!
local sCurrentLevel = AL_GetProperty("Name")
if(sCurrentLevel == "Nowhere") then return end

--[Variables]
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

--[Chapter 1]
if(iCurrentChapter == 1.0) then
    LM_ExecuteScript(fnResolvePath() .. "Chapter 1.lua")

--[Chapter 5]
elseif(iCurrentChapter == 5.0) then
    return
end