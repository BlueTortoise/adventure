--[ ================================== Compute Doctor Bag Total ================================= ]
--Figures out the total number of doctor bag charges the player should have, with all boosts from various chapters
-- taken into account. This does NOT check cases where the bag is zeroed because the player does not have it.
--When the algorithm finishes, giTotalDoctorBagChargeSize/gfTotalDoctorBagChargeRate is populated with the result.
--Note: Despite being listed by chapter, any boost that is "missed" can be re-acquired in a New Chapter + run, or
-- purchased later in chapter 6.

--[ ======================================== Base Values ======================================== ]
--[Base Values]
--The base charge size is 100, and the base charge rate is 1.0
giTotalDoctorBagChargeSize = 100
gfTotalDoctorBagChargeRate = 1.0

--Upgrade storage.
local iSizeUpgrade = 0
local fRateUpgrade = 0.0

--[ ================================== Chapter-Specific Boosts ================================== ]
--[Chapter 1 Boosts]
--[Chapter 2 Boosts]
--[Chapter 3 Boosts]
--[Chapter 4 Boosts]
--[Chapter 5 Boosts]
--Work Credits terminal, boosts charges by 75.
local iWorkCreditsDoctorBagCharges = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagCharges", "N")
if(iWorkCreditsDoctorBagCharges == 1.0) then
    iSizeUpgrade = iSizeUpgrade + 75
end

--Work Credits terminal, boosts potency by 0.30
local iWorkCreditsDoctorBagPotency = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagPotency", "N")
if(iWorkCreditsDoctorBagPotency == 1.0) then
    fRateUpgrade = fRateUpgrade + 0.30
end

--[Chapter 6 Boosts]
--[Store]
--Set the globals.
giTotalDoctorBagChargeSize = 100 + iSizeUpgrade
gfTotalDoctorBagChargeRate = 1.0 + fRateUpgrade

--Store the size/rate upgrade globals.
VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargeSizeUpgrade", "N", iSizeUpgrade)
VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargeRateUpgrade", "N", fRateUpgrade)

--[ ==================================== Automatic Variables ==================================== ]
--[Automatic Internal Variable Synchronizing]
--If this flag is toggled on, calling this routine will automatically set the inventory variables
-- to the total charges compute above. Note that the current charges are not modified.
if(gbAutoSetDoctorBagProperties == true) then
    
    --Set the total charges.
    VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", giTotalDoctorBagChargeSize)
	AdInv_SetProperty("Doctor Bag Charges Max", giTotalDoctorBagChargeSize)

end

--[Automatic Recharge]
--If this flag is toggled on, the doctor bag will sync its current charges with its max charges.
-- The flag also toggles off after each call.
--This is usually used for rest actions.
if(gbAutoSetDoctorBagCurrentValues == true) then
    
    --Toggle off.
    gbAutoSetDoctorBagCurrentValues = false
    
    --Charges sync.
    VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", giTotalDoctorBagChargeSize)
	AdInv_SetProperty("Doctor Bag Charges", giTotalDoctorBagChargeSize)

end