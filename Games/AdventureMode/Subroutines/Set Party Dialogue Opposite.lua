--[Set Party Dialogue Opposite]
--Subscript, sets party positions based on who is in the party. The characters are placed opposite one another.
-- This is used for group dialogue.

--[Differences]
--Try to figure out the dialogue actor names of following characters, which may be different from their field names.
local saFollowerList = {}
for i = 1, gsFollowersTotal, 1 do
    saFollowerList[i] = gsaFollowerNames[i]
    
    if(saFollowerList[i] == "55") then
        saFollowerList[i] = "2855"
    elseif(saFollowerList[i] == "SX399") then
        saFollowerList[i] = "SX-399"
    end
end

--Alternately, act on the party leader.
local sUsePartyLeader = gsPartyLeaderName
if(sUsePartyLeader == "55") then
    sUsePartyLeader = "2855"
elseif(sUsePartyLeader == "SX399") then
    sUsePartyLeader = "SX-399"
end

--[Base]
--Just the lead character. Talking to yourself?
if(gsFollowersTotal == 0) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 1, \"" .. sUsePartyLeader .."\", \"Neutral\")"
    fnCutsceneInstruction(sString)

--One follower.
elseif(gsFollowersTotal == 1) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 1, \"" .. sUsePartyLeader .. "\", \"Neutral\")"
    fnCutsceneInstruction(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 4, \"" .. saFollowerList[1] .. "\", \"Neutral\")"
    fnCutsceneInstruction(sString)

--Two followers.
elseif(gsFollowersTotal == 2) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 2, \"" .. sUsePartyLeader .. "\", \"Neutral\")"
    fnCutsceneInstruction(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 0, \"" .. saFollowerList[1] .. "\", \"Neutral\")"
    fnCutsceneInstruction(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 4, \"" .. saFollowerList[2] .. "\", \"Neutral\")"
    fnCutsceneInstruction(sString)

--Three followers.
elseif(gsFollowersTotal == 3) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 2, \"" .. sUsePartyLeader .. "\", \"Neutral\")"
    fnCutsceneInstruction(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 0, \"" .. saFollowerList[1] .. "\", \"Neutral\")"
    fnCutsceneInstruction(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 3, \"" .. saFollowerList[2] .. "\", \"Neutral\")"
    fnCutsceneInstruction(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 5, \"" .. saFollowerList[3] .. "\", \"Neutral\")"
    fnCutsceneInstruction(sString)

end