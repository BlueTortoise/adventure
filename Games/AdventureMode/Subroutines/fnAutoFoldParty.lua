--[Function: fnAutoFoldParty]
--Causes the party to walk onto the leader and then calls the fold operation. Dynamically determines
-- who the followers are. Fun!
--This is typically called at the end of a cutscene. It causes all of the party members to move onto
-- the location of the party leader, wherever that happened to be at the end of the scene.

--Built from: ./ZLaunch.lua
--Example: fnAutoFoldParty()
function fnAutoFoldParty()
    
    --Obviously, if there are no followers we don't need to do anything.
    if(gsFollowersTotal < 1) then return end
    
    --Order the Autofold.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", gsPartyLeaderName)
		ActorEvent_SetProperty("Autofold Party")
	DL_PopActiveObject()
    fnCutsceneBlocker()
    
    --Now order all followers to assume this is their zero position.
    fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
end