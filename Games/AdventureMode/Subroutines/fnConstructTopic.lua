--[Function: fnConstructTopic]
--Constructs a topic and immediately registers the given NPC for that topic. Essentially combines two 
-- lines into one.
--Built from: ./ZLaunch.lua
--Example: fnConstructTopic("TopicName", "Dogs", 1, "Mei", 0)
function fnConstructTopic(sTopicInternalName, sTopicDisplayName, iTopicStartLevel, sNPCName, iNPCTopicLevel)

	--Debug: Builds a list of all unique topics currently in the game.
	if(gsaUniqueTopics ~= nil) then
		
		--Loop across the existing topics. If any match this new one, stop.
		local bFoundCopy = false
		local i = 1
		while(gsaUniqueTopics[i] ~= nil) do
			if(gsaUniqueTopics[i] == sTopicInternalName) then
				bFoundCopy = true
				break
			end
			
			i = i + 1
		end
		
		--If no copy was found, add it.
		if(bFoundCopy == false) then
			gsaUniqueTopics[i] = sTopicInternalName
		end
	end

	WD_SetProperty("Register Topic", sTopicInternalName, sTopicDisplayName, iTopicStartLevel)
	WD_SetProperty("Register Topic For", sTopicInternalName, sNPCName, iNPCTopicLevel)

end

--Shorthand Function. If the display name is not provided, it uses the internal name.
gsActiveNPCName = "None"
function fnConstructTopicStd(bIsUnlocked, sTopicInternalName, sTopicDisplayName)

    --Arg check. Internal must be provided.
    if(bIsUnlocked        == nil) then return end
    if(sTopicInternalName == nil) then return end
    
    --If the topic display name isn't provided, it's identical to the internal name.
    if(sTopicDisplayName == nil) then sTopicDisplayName = sTopicInternalName end
    
    --If the active NPC name is not set, fail here.
    if(gsActiveNPCName == "None") then return end

	--Debug: Builds a list of all unique topics currently in the game.
	if(gsaUniqueTopics ~= nil) then
		
		--Loop across the existing topics. If any match this new one, stop.
		local bFoundCopy = false
		local i = 1
		while(gsaUniqueTopics[i] ~= nil) do
			if(gsaUniqueTopics[i] == sTopicInternalName) then
				bFoundCopy = true
				break
			end
			
			i = i + 1
		end
		
		--If no copy was found, add it.
		if(bFoundCopy == false) then
			gsaUniqueTopics[i] = sTopicInternalName
		end
	end

    local iUnlockValue = -1
    if(bIsUnlocked) then iUnlockValue = 1 end

	WD_SetProperty("Register Topic", sTopicInternalName, sTopicDisplayName, iUnlockValue)
	WD_SetProperty("Register Topic For", sTopicInternalName, gsActiveNPCName, 0)

end