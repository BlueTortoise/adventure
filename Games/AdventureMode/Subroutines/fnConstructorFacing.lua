--[Function: fnConstructorFacing]
--Causes the player party to change facing if a global variable is tripped. Used in room constructors.
--Built from: ./ZLaunch.lua
--Example: fnConstructorFacing()
function fnConstructorFacing()

    --If gi_Force_Facing is -1, ignore it.
	if(gi_Force_Facing == -1) then return end
    
    --Always reset the flag.
    gi_Force_Facing = -1
    
    --Set the party leader to face south.
    fnCutsceneFace(gsPartyLeaderName, 0, 1)
    
    --All party members face south.
    for i = 1, gsFollowersTotal, 1 do
        fnCutsceneFace(gsaFollowerNames[i], 0, 1)
    end

end
