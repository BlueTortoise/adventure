--[Function: fnCountMeiTFs]
--Counts how many transformations Mei has accumulated. Used for some dialogue switches.
--Built from: ./ZLaunch.lua
--Example: local iMeiTFCount = fnCountMeiTFs()
function fnCountMeiTFs()

	local iTally = 0
	if(VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N") == 1.0) then iTally = iTally + 1 end
	if(VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")     == 1.0) then iTally = iTally + 1 end
	if(VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N")   == 1.0) then iTally = iTally + 1 end
	return iTally

end