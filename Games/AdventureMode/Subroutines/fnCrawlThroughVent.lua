--[Function: fnCrawlThroughVent]
--Crawling through a vent. The party moves in front of the vent, disappears, then moves back onscreen at the destination.
--Built from: ./ZLaunch.lua
--Example: fnCrawlThroughVent(fStartX, fStartY, fEndX, fEndY)
function fnCrawlThroughVent(fStartX, fStartY, fEndX, fEndY)

	--Arg check.
	if(fStartX == nil) then return end
	if(fStartY == nil) then return end
	if(fEndX == nil) then return end
	if(fEndY == nil) then return end
	
	--Move Party in front of the vent.
	fnCutsceneMove(gsPartyLeaderName, fStartX, fStartY)
	for i = 1, gsFollowersTotal, 1 do
		fnCutsceneMove(gsaFollowerNames[i], fStartX, fStartY)
	end
	fnCutsceneBlocker()
	
	--Face party north.
	fnCutsceneFace(gsPartyLeaderName, 0, -1)
	for i = 1, gsFollowersTotal, 1 do
		fnCutsceneFace(gsaFollowerNames[i], 0, -1)
	end
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Teleport them offscreen. Camera focuses on where they were.
	fnCutsceneTeleport(gsPartyLeaderName, -100.25, -100.50)
	for i = 1, gsFollowersTotal, 1 do
		fnCutsceneTeleport(gsaFollowerNames[i], -100.25, -100.50)
	end
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 0.5)
		CameraEvent_SetProperty("Focus Position", (fStartX * gciSizePerTile), (fStartY * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Camera scrolls over.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 0.5)
		CameraEvent_SetProperty("Focus Position", (fEndX * gciSizePerTile), (fEndY * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Teleport the party in.
	fnCutsceneTeleport(gsPartyLeaderName, fEndX, fEndY)
	for i = 1, gsFollowersTotal, 1 do
		fnCutsceneTeleport(gsaFollowerNames[i], fEndX, fEndY)
	end
	fnCutsceneBlocker()
	
	--Face everyone south.
	fnCutsceneFace(gsPartyLeaderName, 0, 1)
	for i = 1, gsFollowersTotal, 1 do
		fnCutsceneFace(gsaFollowerNames[i], 0, 1)
	end
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Camera follows party leader.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 2.0)
		CameraEvent_SetProperty("Focus Actor Name", gsPartyLeaderName)
	DL_PopActiveObject()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])

end