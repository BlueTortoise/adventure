--[Function: fnCutsceneFace]
--Creates and registers a cutscene change-facing event. This is an ActorEvent. Facing is done by "Speeds".
--Built from: ./ZLaunch.lua
--Example: fnCutsceneFace("Florentina", 0, 1)
function fnCutsceneFace(sActorName, fX, fY)

	--Arg check.
	if(sActorName == nil) then return end
	if(fX == nil) then return end
	if(fY == nil) then return end

	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", sActorName)
		ActorEvent_SetProperty("Face", fX, fY)
	DL_PopActiveObject()

end

--This version attempts to face a target. The engine determines the facing.
function fnCutsceneFaceTarget(sActorName, sActorTarget)

	--Arg check.
	if(sActorName == nil) then return end
	if(sActorTarget == nil) then return end

	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", sActorName)
		ActorEvent_SetProperty("Face", sActorTarget)
	DL_PopActiveObject()

end