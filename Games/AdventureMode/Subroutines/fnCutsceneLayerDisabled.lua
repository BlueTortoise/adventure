--[Function: fnCutsceneLayerDisabled]
--Creates and registers a cutscene event that changes visibility on a given tile layer.
--Built from: ./ZLaunch.lua
--Example: fnCutsceneLayerDisabled("Tile Layer 00", true)
function fnCutsceneLayerDisabled(sLayerName, bIsDisabled)

	--Argument check.
	if(sLayerName == nil) then return end
	if(bIsDisabled == nil) then return end
	
    --Construct the string.
    AL_SetProperty("Set Layer Disabled", "CloudsAnim00", true)
    local sInstruction = "AL_SetProperty(\"Set Layer Disabled\", \"" .. sLayerName .. "\", "
    if(bIsDisabled == true) then
        sInstruction = sInstruction .. "true)"
    else
        sInstruction = sInstruction .. "false)"
    end
    
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end