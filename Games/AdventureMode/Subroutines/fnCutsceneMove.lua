--[Function: fnCutsceneMove]
--Creates and registers a cutscene movement event. This is an ActorEvent. Position is in tile coordinates and allows decimals.
--Built from: ./ZLaunch.lua
--Example: fnCutsceneMove("Florentina", 1.25, 1.50)
--Example: fnCutsceneMove("Florentina", 1.25, 1.50, 0.50) --Has movement speed specified.
function fnCutsceneMove(sActorName, fX, fY, fMoveSpeed)

	--Arg check.
	if(sActorName == nil) then return end
	if(fX == nil) then return end
	if(fY == nil) then return end

	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", sActorName)
		
		--If the movement speed is not specified:
		if(fMoveSpeed == nil) then
			ActorEvent_SetProperty("Move To", fX * gciSizePerTile, fY * gciSizePerTile)
			
		--Move speed is specified:
		else
			ActorEvent_SetProperty("Move To", fX * gciSizePerTile, fY * gciSizePerTile, fMoveSpeed)
		end
	DL_PopActiveObject()

end