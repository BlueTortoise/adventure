--[Function: fnCutsceneSetFrame]
--Creates an event to set a special frame for the given character.
--Built from: ./ZLaunch.lua
--Example: fnCutsceneFace("Florentina", "Wounded")
function fnCutsceneSetFrame(sActorName, sSpecialFrameName)

	--Arg check.
	if(sActorName == nil) then return end
	if(sSpecialFrameName == nil) then return end

	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", sActorName)
		ActorEvent_SetProperty("Special Frame", sSpecialFrameName)
	DL_PopActiveObject()

end