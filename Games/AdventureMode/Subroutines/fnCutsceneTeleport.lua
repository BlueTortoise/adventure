--[Function: fnCutsceneTeleport]
--Creates and registers a cutscene teleport event. This is an ActorEvent. Position is in tile coordinates and allows decimals.
--Built from: ./ZLaunch.lua
--Example: fnCutsceneTeleport("Florentina", 1.25, 1.50)
function fnCutsceneTeleport(sActorName, fX, fY)

	--Arg check.
	if(sActorName == nil) then return end
	if(fX == nil) then return end
	if(fY == nil) then return end

	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", sActorName)
		ActorEvent_SetProperty("Teleport To", fX * gciSizePerTile, fY * gciSizePerTile)
	DL_PopActiveObject()

end