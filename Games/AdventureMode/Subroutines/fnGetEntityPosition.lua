--[Function: fnGetEntityPosition]
--Returns the X and Y coordinates of the named entity. Returns -100, -100 if the entity was not found.
-- The entity will have its position returned in terms of tiles, so 10.25 instead of 164.0.
--Built from: ./ZLaunch.lua
--Example: local fMeiX, fMeiY = fnGetEntityPosition("Mei")
function fnGetEntityPosition(sEntityName)

	--Arg check.
	if(sEntityName == nil) then
        return -100, -100
    end
	
    --Check if the entity exists. If not, return defaults.
    if(EM_Exists(sEntityName) == false) then
        return -100, -100
    end

    --Get entity's position.
    EM_PushEntity(sEntityName)
        local fEntityX, fEntityY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    --Convert to tile.
    fEntityX = fEntityX / gciSizePerTile
    fEntityY = fEntityY / gciSizePerTile
		
	return fEntityX, fEntityY
end