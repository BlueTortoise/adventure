--[Function: fnLoadCharacterGraphics]
--Loads character graphics from Sprites.slf given a pattern and uploads them to the datalibrary. Will
-- not double-load any images that is already in the library.
--Built from: ./ZLaunch.lua
--Example: fnLoadCharacterGraphics("Mei", "Root/Images/Sprites/Mei/", true)
function fnLoadCharacterGraphics(sCharacterName, sDLPath, bUsesEightDirections)
	
	--Argument check.
	if(sCharacterName       == nil) then return end
	if(sDLPath              == nil) then return end
	if(bUsesEightDirections == nil) then return end
	
	--Standardized loading set.
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      4,       4,      4,      4}
	if(bUsesEightDirections == true) then
		saSets =   {"South", "North", "West", "East", "NE", "NW", "SW", "SE"}
		iaFrames = {      4,       4,      4,      4,    4,    4,    4,    4}
	end
	
	--Add the DL directory if it doesn't exist yet.
	DL_AddPath(sDLPath)
	
	--Loading loop.
	local i = 1
	while(saSets[i] ~= nil) do
		for p = 1, iaFrames[i], 1 do
			
			--Setup.
			local sLoadName = sCharacterName .. "|" .. saSets[i] .. "|" .. p
			local sLoadPath = sDLPath .. saSets[i] .. "|" .. (p-1)
			
			--Entry already exists, don't load it. This is actually handled internally now.
			--if(DL_Exists(sLoadPath) == false) then
				DL_ExtractBitmap(sLoadName, sLoadPath)
			--end
		end
		i = i + 1
	end
	
	--If this belong to one of the special frame sets, we also load the running animations.
	-- Not all entities use this, only ones that join the player party. The two exceptions
	-- are Breanne and Sophie, who have 8-directions but aren't in the party.
	--The one exception is the Darkmatter Girls, who don't run or use their 8-directional cases
	-- except for plot-related scenes.
	if(bUsesEightDirections and sCharacterName ~= "DarkmatterGirl") then
		i = 1
		while(saSets[i] ~= nil) do
			for p = 1, iaFrames[i], 1 do
				
				--Setup.
				local sLoadName = sCharacterName .. "|" .. saSets[i] .. "|Run" .. p
				local sLoadPath = sDLPath .. saSets[i] .. "|Run" .. (p-1)
				
				--Entry already exists, don't load it. This is actually handled internally now.
				--if(DL_Exists(sLoadPath) == false) then
					DL_ExtractBitmap(sLoadName, sLoadPath)
				--end
			end
			i = i + 1
		end
		
	end
end

--[West and East Only]
--Loads only the west and east directions. Some NPCs only show these because they are animals.
function fnLoadCharacterGraphicsEW(sCharacterName, sDLPath)
	
	--Argument check.
	if(sCharacterName == nil) then return end
	if(sDLPath        == nil) then return end
	
	--Standardized loading set.
	local saSets =   {"West", "East"}
	local iaFrames = {     4,      4}
	
	--Add the DL directory if it doesn't exist yet.
	DL_AddPath(sDLPath)
	
	--Loading loop.
	local i = 1
	while(saSets[i] ~= nil) do
		for p = 1, iaFrames[i], 1 do
			
			--Setup.
			local sLoadName = sCharacterName .. "|" .. saSets[i] .. "|" .. p
			local sLoadPath = sDLPath .. saSets[i] .. "|" .. (p-1)
			
			--Entry already exists, don't load it.
			--if(DL_Exists(sLoadPath) == false) then
				DL_ExtractBitmap(sLoadName, sLoadPath)
			--end
		end
		i = i + 1
	end
end

--[Facing Only]
--Loads the facing directions only. Used for NPCs who never move.
--Example: fnLoadCharacterGraphics("GolemFancyA", "Root/Images/Sprites/GolemFancyA/")
function fnLoadCharacterGraphicsFacing(sCharacterName, sDLPath)
	
	--Argument check.
	if(sCharacterName       == nil) then return end
	if(sDLPath              == nil) then return end
	
	--Standardized loading set.
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      1,       1,      1,      1}
	
	--Add the DL directory if it doesn't exist yet.
	DL_AddPath(sDLPath)
	
	--Loading loop.
	local i = 1
	while(saSets[i] ~= nil) do
		for p = 1, iaFrames[i], 1 do
			
			--Setup.
			local sLoadName = sCharacterName .. "|" .. saSets[i] .. "|" .. p
			local sLoadPath = sDLPath .. saSets[i] .. "|" .. (p-1)
			
			--Entry already exists, don't load it.
			--if(DL_Exists(sLoadPath) == false) then
				DL_ExtractBitmap(sLoadName, sLoadPath)
			--end
		end
		i = i + 1
	end
end