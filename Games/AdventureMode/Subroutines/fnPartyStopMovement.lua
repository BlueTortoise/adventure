--[Function: fnPartyStopMovement]
--Standardized movement stopper, causes the player's party to stop their walk cycle. Cutscenes often lockout
-- the player's controls but don't necessarily stop the movement cycle so the player may be in a step animation.
-- This function removes that issue (if you don't want it).
--Deprecated as of Prototype 5-1.
--Built from: ./ZLaunch.lua
--Example: fnPartyStopMovement()
function fnPartyStopMovement()

	--EM_PushEntity("By ID", giPartyLeaderID)
	--	TA_SetProperty("Stop Moving")
	--DL_PopActiveObject()

end