-- |[ ================================= fnRemovePartyMember() ================================== ]|
--Removes the named party member, both from the following party and the combat lineup.
function fnRemovePartyMember(psPartyMemberName, pbRemoveFromField)
    
    -- |[Arg Check]|
    if(psPartyMemberName == nil) then return end
    if(pbRemoveFromField == nil) then pbRemoveFromField = false end
    
    -- |[Lookup Table]|
    --Remapper. Sometimes the combat and field names are not the same.
    local sCombatName = psPartyMemberName
    local sFieldName = psPartyMemberName
    local sVariableName = "Null"
    
    --55, has a variable.
    if(psPartyMemberName == "55") then
        sVariableName = "Root/Variables/Chapter5/Scenes/iIs55Following"
    
    --Sophie, is not a combat character.
    elseif(psPartyMemberName == "Sophie") then
        sCombatName = "Null"
    
    --JX-101. Has a variable.
    elseif(psPartyMemberName == "JX-101") then
        sVariableName = "Root/Variables/Chapter5/Scenes/iIsJX101Following"
    end
    
    -- |[Combat Lineup]|
    --Remove the entity from the combat lineup.
    if(AdvCombat_GetProperty("Is Member In Active Party", sCombatName)) then
        for i = 0, gciCombat_MaxActivePartySize-1, 1 do
            local sCheckName = AdvCombat_GetProperty("Name of Active Member", i)
            if(sCheckName == sCombatName) then
                AdvCombat_SetProperty("Party Slot", i, "Null")
            end
        end
    end
    
    -- |[Follower Set]|
    --Remove the entity from the follower group.
    for i = 1, gsFollowersTotal, 1 do
        if(gsaFollowerNames[i] == sFieldName) then
            
            --Trim.
            for p = i, gsFollowersTotal-1, 1 do
                gsaFollowerNames[p] = gsaFollowerNames[p+1]
                giaFollowerIDs[p]   = giaFollowerIDs[p+1]
            end
            gsaFollowerNames[gsFollowersTotal] = nil
            giaFollowerIDs[gsFollowersTotal]   = nil
            
            --Decrement.
            gsFollowersTotal = gsFollowersTotal - 1
            AL_SetProperty("Unfollow Actor Name", sFieldName)
            break
        end
    end

    -- |[Field Removal]|
    --If the entity happens to be on the field, move her off.
    if(pbRemoveFromField and EM_Exists(sFieldName)) then
        EM_PushEntity(sFieldName)
            TA_SetProperty("Position", -10, -10)
        DL_PopActiveObject()
    end
    
    -- |[Variable Case]|
	--Join/Leave variable. Some characters manage joining the party with script variables.
	if(sVariableName ~= "Null") then
		VM_SetVar(sVariableName, "N", 0.0)
	end
end
