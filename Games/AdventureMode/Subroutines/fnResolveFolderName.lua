--[Function: fnResolveFolderName]
--Figure out the name of the folder that a map is in. This is used to give every map a unique name.
--Built from: ./ZLaunch.lua
--Example: fnResolveFolderName(fnResolvePath())
function fnResolveFolderName(sCallingPath)

	--Arg check.
	if(sCallingPath == nil) then return "Null" end

	--Variable setup.
	local sEndingPath = "Null"
	local bHasFoundNonSlash = false
	
	--String properties.
	local iPos = string.len(sCallingPath)
	local iEndingLetter = iPos
	
	--Iterate across the string backwards.
	while(iPos > 1) do
		
		--Get the letter.
		local sLetter = string.sub(sCallingPath, iPos, iPos)
		
		--If the letter is a slash:
		if(sLetter == "/" or sLetter == "\\") then
			
			--We have found a non-slash before this:
			if(bHasFoundNonSlash == true) then
				return string.sub(sCallingPath, iPos+1, iEndingLetter)
			
			--We have not found a non-slash, so move the ending letter back.
			else
				iEndingLetter = iPos - 1
			end
		
		--If the letter is not a slash:
		else
			bHasFoundNonSlash = true
		end
		iPos = iPos - 1
	end

	--If we got this far, return the original path. It may not have had a valid slash.
	return sCallingPath
end