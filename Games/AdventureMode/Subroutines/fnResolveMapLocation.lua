--[Function: fnResolveMapLocation]
--Uploads the map information given the name of the current map, based on a lookup table established at startup.
--Built from: ./ZLaunch.lua
--Example: fnResolveMapLocation("TrapBasementA")
function fnResolveMapLocation(sMapName)

	--Arg check.
	if(sMapName == nil) then return end
	
	--Make sure the array exists.
	if(gsaMapLookups == nil) then return end
	
	--Scan.
	local i = 1
	while(gsaMapLookups[i] ~= nil) do

		--Name match?
		if(gsaMapLookups[i][1] == sMapName) then
            
            --Figure out the overlay. Not all maps have these. "Null" is a valid case.
            local sOverlayName = "Null"
            if(gsaMapLookups[i][2] == "TrannadarMap") then sOverlayName = "Root/Images/AdvMaps/General/TrannadarMapOverlay" end
            if(gsaMapLookups[i][2] == "RegulusMap")   then sOverlayName = "Root/Images/AdvMaps/General/RegulusMapOverlay"   end
            
			AM_SetMapInfo("Root/Images/AdvMaps/General/" .. gsaMapLookups[i][2], sOverlayName, gsaMapLookups[i][3], gsaMapLookups[i][4])
			break
		end

		--Next
		i = i + 1

    end
end