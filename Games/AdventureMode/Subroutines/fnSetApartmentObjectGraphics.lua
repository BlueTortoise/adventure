--[Function: fnSetApartmentObjectGraphics]
--Sets an NPC's graphics to look like the apartment object in the matching slot. The NPC should be atop the activity stack.
--Built from: ./ZLaunch.lua
--Example: fnSetApartmentObjectGraphics(iType)
function fnSetApartmentObjectGraphics(iType)

	--Arg check.
	if(iType == nil) then return end
				
	--Default graphics in case of error.
	for q = 1, 4, 1 do
		TA_SetProperty("Move Frame", 0, q-1, "Root/Images/Sprites/Quarters/Tube1")
		TA_SetProperty("Move Frame", 2, q-1, "Root/Images/Sprites/Quarters/Tube2")
		TA_SetProperty("Move Frame", 4, q-1, "Root/Images/Sprites/Quarters/Tube0")
		TA_SetProperty("Move Frame", 6, q-1, "Root/Images/Sprites/Quarters/Tube3")
	end
	
	if(iType == gci_Quarters_DefragPod) then
		for q = 1, 4, 1 do
			TA_SetProperty("Move Frame", 0, q-1, "Root/Images/Sprites/Quarters/Tube1")
			TA_SetProperty("Move Frame", 2, q-1, "Root/Images/Sprites/Quarters/Tube2")
			TA_SetProperty("Move Frame", 4, q-1, "Root/Images/Sprites/Quarters/Tube0")
			TA_SetProperty("Move Frame", 6, q-1, "Root/Images/Sprites/Quarters/Tube3")
		end
	elseif(iType == gci_Quarters_CounterL) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/CounterL")
			end
		end
	elseif(iType == gci_Quarters_CounterM) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/CounterM")
			end
		end
	elseif(iType == gci_Quarters_CounterR) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/CounterR")
			end
		end
	elseif(iType == gci_Quarters_OilMaker) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/Coffee")
			end
		end
	elseif(iType == gci_Quarters_TV) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/TV")
			end
		end
	elseif(iType == gci_Quarters_ChairS) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/ChairS")
			end
		end
	elseif(iType == gci_Quarters_CouchSL) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/CouchSL")
			end
		end
	elseif(iType == gci_Quarters_CouchSM) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/CouchSM")
			end
		end
	elseif(iType == gci_Quarters_CouchSR) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/CouchSR")
			end
		end
	elseif(iType == gci_Quarters_ChairN) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/ChairN")
			end
		end
	elseif(iType == gci_Quarters_CouchNL) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/CouchNL")
			end
		end
	elseif(iType == gci_Quarters_CouchNM) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/CouchNM")
			end
		end
	elseif(iType == gci_Quarters_CouchNR) then
		for q = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", q-1, p-1, "Root/Images/Sprites/Quarters/CouchNR")
			end
		end
	end

end