--[fnSpawnNPCPattern]
--Spawns NPCs using a name pattern, with A/B/C etc appended onto the end.
fnSpawnNPCPattern = function(psName, psStartLetter, psEndLetter)
	
	--Arg check.
	if(psName == nil) then return end
	if(psStartLetter == nil) then return end
	if(psEndLetter == nil) then return end
	
	--Setup.
	local sCurrentLetter = psStartLetter
	
	--Range check. If the letter is out of the normal range, fail.
	local iByteValue = string.byte(sCurrentLetter)
	if(iByteValue < string.byte("A") or iByteValue > string.byte("Z")) then return end
	
	--Iterate across the letters. Always stops on Z. Also always stops on non-capitals.
	while(true) do
		
		--Add the remap.
		local sNPCName = psName .. sCurrentLetter
        fnStandardNPCByPosition(sNPCName)
		
		--If this was the ending letter, or the letter 'Z', stop.
		if(sCurrentLetter == psEndLetter or sCurrentLetter == "Z") then break end
		
		--Move to the next letter.
		local iByteValue = string.byte(sCurrentLetter)
		sCurrentLetter = string.char(iByteValue + 1)
	end
end