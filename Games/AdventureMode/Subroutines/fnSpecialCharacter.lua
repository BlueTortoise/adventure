--[Function: fnSpecialCharacter]
--Creates a character with a special name, such as a player character or party member, or major NPC. This allows standardization of
-- special frames across the same character spawned in multiple places.
--It is legal to pass nil for the dialogue path. With no dialogue path, the character cannot be interacted with. This
-- is for party members.
--Party members with multiple forms are NOT handled. Mei, Christine, etc should be spawned elsewhere.
--Returns the ID of the created character, or 0 on error.
--Built from: ./ZLaunch.lua
--Example: fnSpecialCharacter("Florentina", "Alraune", -100, -100, gci_Face_South, false, nil)
function fnSpecialCharacter(psCharacterName, psFormName, piXPos, piYPos, piFacing, pbIsClipped, psDialoguePath)
	
	--[Arg check]
	if(psCharacterName == nil) then return 0 end
	if(psFormName      == nil) then return 0 end
	if(piXPos          == nil) then return 0 end
	if(piYPos          == nil) then return 0 end
	if(piFacing        == nil) then return 0 end
	if(pbIsClipped     == nil) then return 0 end
	
	--[Lookups]
	if(gzCharacterLookups == nil) then
		
		--Setup.
		giCharacterLookupsTotal = 0
		gzCharacterLookups = {}
		
		--Function
		local fnAddCharacter = function(sName, bIsEightDirectional, saTable)
			giCharacterLookupsTotal = giCharacterLookupsTotal + 1
			gzCharacterLookups[giCharacterLookupsTotal] = {sName, bIsEightDirectional, saTable}
		end
		
		--Adder.
		fnAddCharacter("Florentina", true,  {"Wounded", "Crouch", "Sleep"})
		fnAddCharacter("55",         true,  {"Chair0",  "Chair1", "Chair2", "Chair3", "Chair4", "Crouch", "Wounded", "Downed", "Draw0", "Draw1", "Draw2", "Stealth", "Exec0", "Exec1", "Exec2", "Exec3", "Exec4", "Exec5"})
		fnAddCharacter("Sophie",     true,  {nil})
		fnAddCharacter("SX399",      false, {"Laugh0", "Laugh1", "Wounded", "Crouch"})
		fnAddCharacter("JX101",      false, {nil})
		fnAddCharacter("Sammy",      true,  {nil})
	end
	
	--[Override Name]
	--Some names use overrides. They use different graphics but the same character name.
	local sOverrideName = psCharacterName
	if(sOverrideName == "SX399Lord") then sOverrideName = "SX399" end
	
	--[Common code]
	TA_Create(sOverrideName)
	
		--Get the ID of the created character.
		local iID = RE_GetID()
	
		TA_SetProperty("Position", piXPos, piYPos)
		TA_SetProperty("Facing", piFacing)
		TA_SetProperty("Clipping Flag", pbIsClipped)
		if(psDialoguePath ~= nil) then 
			TA_SetProperty("Activation Script", psDialoguePath)
		end
	
		--Search.
		for i = 1, giCharacterLookupsTotal, 1 do
			if(gzCharacterLookups[i][1] == sOverrideName) then
        
                    --Special
                    local bIsEightDirectional = gzCharacterLookups[i][2]
                    if(psCharacterName == "SX399Lord" or psFormName == "SteamLord") then bIsEightDirectional = true end
	
					--Graphics.
					fnSetCharacterGraphics("Root/Images/Sprites/" .. psCharacterName .. "/", bIsEightDirectional)
						
					--Special frames.
					TA_SetProperty("Wipe Special Frames")
					local p = 1
					while(gzCharacterLookups[i][3][p] ~= nil) do
						TA_SetProperty("Add Special Frame", gzCharacterLookups[i][3][p] , "Root/Images/Sprites/Special/" .. sOverrideName .. "|" .. gzCharacterLookups[i][3][p] )
						p = p + 1
					end
				break
			end
		end
		
	--[Clean]
	DL_PopActiveObject()
    
    --[Costume Handler]
    --Run the resolver.
    LM_ExecuteScript(gsCharacterAutoresolve, psCharacterName)
    
    --[Finish]
	return iID
end