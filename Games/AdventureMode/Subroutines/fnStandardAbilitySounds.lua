--[Function: fnStandardAbilitySounds]
--An AdventureCombatAction should be on the activity stack. This will give that ACA the normal attack sounds based
-- on the type provided. The type is usually based on the primary damage type of the attack.
--Built from: ./ZLaunch.lua
--Example: fnStandardAbilitySounds("SlashCross")
function fnStandardAbilitySounds(psType)

	--Arg check.
	if(psType == nil) then return end
	
	--Buff.
	if(psType == "Buff") then
		ACA_SetProperty("Impact SFX",   "Combat|Impact_Buff")
		ACA_SetProperty("Critical SFX", "Combat|Impact_Buff")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--Debuff.
	elseif(psType == "Debuff") then
		ACA_SetProperty("Impact SFX",   "Combat|Impact_Debuff")
		ACA_SetProperty("Critical SFX", "Combat|Impact_Debuff")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--Electricity.
	elseif(psType == "Electricity") then
		ACA_SetProperty("Impact SFX",   "Combat|Impact_Slash")
		ACA_SetProperty("Critical SFX", "Combat|Impact_Slash_Crit")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--Gunshot.
	elseif(psType == "GunShot") then
		ACA_SetProperty("Impact SFX",   "Combat|Impact_Slash")
		ACA_SetProperty("Critical SFX", "Combat|Impact_Slash_Crit")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--Healing.
	elseif(psType == "Healing") then
		--ACA_SetProperty("Impact SFX",   "Combat|Heal")
		--ACA_SetProperty("Critical SFX", "Combat|Heal")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--LaserShot.
	elseif(psType == "LaserShot") then
		ACA_SetProperty("Impact SFX",   "Combat|Impact_Laser")
		ACA_SetProperty("Critical SFX", "Combat|Impact_Laser_Crit")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--Pierce.
	elseif(psType == "Pierce") then
		ACA_SetProperty("Impact SFX",   "Combat|Impact_Pierce")
		ACA_SetProperty("Critical SFX", "Combat|Impact_Pierce_Crit")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--SlashClaw.
	elseif(psType == "SlashClaw") then
		ACA_SetProperty("Impact SFX",   "Combat|Impact_Slash")
		ACA_SetProperty("Critical SFX", "Combat|Impact_Slash_Crit")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--SlashCross.
	elseif(psType == "SlashCross") then
		ACA_SetProperty("Impact SFX",   "Combat|Impact_CrossSlash")
		ACA_SetProperty("Critical SFX", "Combat|Impact_CrossSlash_Crit")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--Strike.
	elseif(psType == "Strike") then
		ACA_SetProperty("Impact SFX",   "Combat|Impact_Strike")
		ACA_SetProperty("Critical SFX", "Combat|Impact_Strike_Crit")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	
	--Unhandled.
	else
		ACA_SetProperty("Impact SFX",   "Combat|Impact_Slash")
		ACA_SetProperty("Critical SFX", "Combat|Impact_Slash_Crit")
		ACA_SetProperty("Miss SFX",     "Combat|AttackMiss")
		ACA_SetProperty("SelfHeal SFX", "Combat|Heal")
	end


end