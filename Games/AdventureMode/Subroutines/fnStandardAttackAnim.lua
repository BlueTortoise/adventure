--[Function: fnStandardAttackAnim]
--Standard attack routine, using a string to specify which animation to use. If no string is specified, the
-- crossing slash is used. 16 frames are always present.
--Note that if the animation listed doesn't exist, nothing appears in combat.
--Built from: ./ZLaunch.lua
--Example: fnStandardAttackAnim("Healing")
function fnStandardAttackAnim(psAnimName)
	
	--If no name is provided, default to the crossing slash attack.
	if(psAnimName == nil) then psAnimName = "SlashCross" end
	
	--Default is 24 frames.
	local iFrameCount = 24
	if(psAnimName == "Strike") then iFrameCount = 10 end
	if(psAnimName == "GunShot") then iFrameCount = 20 end
	if(psAnimName == "LaserShot") then iFrameCount = 20 end
	
	--Construct the animation.
	ACA_SetProperty("Attach Animation", iFrameCount, 1)
	for i = 0, iFrameCount - 1, 1 do
		ACA_SetProperty("Set Animation Frame", i, "Root/Images/Sprites/" .. psAnimName .. string.format("/%02i", i))
	end
end