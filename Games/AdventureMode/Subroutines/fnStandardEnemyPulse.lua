--[Function: fnStandardEnemyPulse]
--Once a level has booted, it usually spawns the enemies in it and tells them to ignore the player if they
-- are of the same form as that enemy.
--Built from: ./ZLaunch.lua
--Example: fnStandardEnemyPulse()
function fnStandardEnemyPulse()

	--Spawn the enemies.
	AL_SetProperty("Run Enemy Spawner")
    
    --Which chapter?
    local sPlayerForm = "Null"
    local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
    
    if(iCurrentChapter == 0.0 or iCurrentChapter == 1.0) then
        sPlayerForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    elseif(iCurrentChapter == 5.0) then
        sPlayerForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    else
        io.write("Chapter not found in fnStandardEnemyPulse.lua")
    end
    
    --Pulse.
    AL_PulseIgnore(sPlayerForm)

end