--[Function: fnStandardLevel]
--Standard level boot sequence. Creates a new AdventureLevel, parses the map data, and sets the examinable script to the
-- default location. Almost every level does this, and in this order, so it got its own function. It perfectly legal
-- to bypass this function call.
--Built from: ./ZLaunch.lua
--Example: fnStandardLevel(gsRoot .. "Maps/Mapname/")
function fnStandardLevel(sCallingPath)

	--Create the map and pass it to the MapManager.
	AL_Create()
	
	--Calling path must exist.
	if(sCallingPath == nil) then return end

	--Basic terrain data, automatically handles layers and depth-sorting.
	AL_ParseSLF(sCallingPath .. "MapData.slf")

	--Set the examination script to the standard path. Most examinable points are set via the .slf file object parsing.
    AL_SetProperty("Base Path", sCallingPath)
	AL_SetProperty("Trigger Script", sCallingPath .. "Triggers.lua")
	AL_SetProperty("Examine Script", sCallingPath .. "Examination.lua")
    gsFieldAbilityCheckPath = sCallingPath .. "FieldAbilities.lua"

	--Drop all events. This prevents the game from running while the loading screen is firing.
	Debug_DropEvents()

end