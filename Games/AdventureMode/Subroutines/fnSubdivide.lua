--[ ========================================= Subdivide ========================================= ]
--Given a string formatted "Dogs|Are|Great" returns each of the individual words "Dogs" "Are" "Great"
-- as a list. The delimiter can be configured to use other types if desired.
--To configure delimiters, the string "|" uses | as a delimiter, and "|:" uses both "|" and ":"
-- as delimiters. There is no particular limit to how many can be used.
function fnSubdivide(psString, psDelimiters)
    
    --Argument check.
    if(psString     == nil) then return {""} end
    if(psDelimiters == nil) then return {""} end
    
    --Create a list.
    local i = 0
    local saReturn = {}
    
    --Setup.
    local iLastStringIndex = 1
    local iLength = string.len(psString)
    local iDelimiters = string.len(psDelimiters)
    
    --Begin iteration.
    for p = 1, iLength, 1 do
    
        --Get the letter here.
        local sLetter = string.sub(psString, p, p)
    
        --Scan across the delimiters.
        for x = 1, iDelimiters, 1 do
            local sDelimiter = string.sub(psDelimiters, x, x)
            
            --Match, create a new substring and add it to the list.
            if(sDelimiter == sLetter) then
                
                i = i + 1
                saReturn[i] = string.sub(psString, iLastStringIndex, p-1)
                iLastStringIndex = p+1
                break
            end
        end
    end
    
    --If we iterated across the whole thing and did not find a delimiter, or there was not a delimiter
    -- in the last slot, we need to copy the final stretch.
    if(iLastStringIndex <= iLength) then
        i = i + 1
        saReturn[i] = string.sub(psString, iLastStringIndex, iLength)
    end
    
    --Pass back the array.
    return saReturn
end