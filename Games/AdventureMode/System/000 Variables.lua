-- |[ ======================================= Variables ======================================== ]|
--Sets up variables and constants used by the Lua state. These are chapter-independent, this file
-- always runs.
--When marked with a c, assume the value is a "constant" and reflects something from the C++ code.
-- Editing these constants will not help you, I assure you.

--[Global Script Variables]
DL_AddPath("Root/Variables/Nowhere/Scenes/")
VM_SetVar("Root/Variables/Nowhere/Scenes/iHasSeenControlsDialogue", "N", 0.0)

--[Debug]
--Variables that can only be activated for debug cutscene purposes. These should all be false during normal play.
gbBypassTrannadarCutscene = false
gbBypassIntro = false
gbDontCancelMusic = false
gbLoadDebug = false

--[Facing Variable]
--When going through certain exits, this variable can track facing changes. Mostly used by constructors.
gi_Force_Facing = -1

--[Combat Standards]
--The function fnStandardAttack() populates these variables.
giStandardDamage = 0
gbStandardImmune = false
giStandardAttackType = 0

--The function fnStandardAccuracy() populates these variables.
gbAttackHit = false
gbAttackCrit = false
gbAttackGlances = false

--The function fnStandardExecution() uses this as a timer offset.
giCombatTimerOffset = 0

--This is used when a script execution package is used during fnStandardExecution(). It is populated with
-- the iTimer value of the abilities.
giStoredTimer = 0

-- |[ ===================================== C++ Constants ====================================== ]|
--Constants derived from the C++ executable. Do not edit these.
gci_Constructor_Start = 0
gci_Constructor_PostExec = 1

--SugarFont
gci_Font_NoEffects = 0
gci_Font_Bold = 1
gci_Font_Italics = 2
gci_Font_Underline = 4
gci_Font_AutocenterX = 8
gci_Font_AutocenterY = 16
gci_Font_AutocenterXY = gci_Font_AutocenterX + gci_Font_AutocenterY
gci_Font_RightAlignX = 32
gci_Font_MirrorX = 64
gci_Font_MirrorY = 128
gci_Font_DoubleRenderX = 256
gci_Font_DoubleRenderY = 512

--[Directions]
--Facing directions
gci_Face_North = 0
gci_Face_NorthEast = 1
gci_Face_East = 2
gci_Face_SouthEast = 3
gci_Face_South = 4
gci_Face_SouthWest = 5
gci_Face_West = 6
gci_Face_NorthWest = 7

--[Constants]
--Script Execution Constants.

--Dialogue Constants.
gcbDialogue_Source_NPC = false
gcbDialogue_Source_Party = true
gcbDialogue_Source_Neither = false

--Fading Constants
gci_Fade_Under_Characters = 0
gci_Fade_Under_GUI = 1
gci_Fade_Over_GUI = 2
gci_Fade_Over_GUI_Blackout_World = 3

--AI Constants. Synced to the C++ code.
gci_ACE_Initialize = 0
gci_ACE_New_Round = 1
gci_ACE_Other_ACE_Begins_Turn = 2
gci_ACE_This_ACE_Begins_Turn = 3
gci_ACE_This_ACE_Dies_Turn = 4
gci_ACE_Combat_Ends = 5

--Flashwhite
gci_Flashwhite_Ticks_Total = 150

--Sizing Constants.
gciSizePerTile = 16

--Offsets for Tilemap Actors
gcfTADefaultXOffset = -8.0
gcfTADefaultYOffset = -16.0

--Camera movement speed in cutscenes. Note that the C++ default is 5.0, the value cannot go below 1.0.
gcfCamera_Cutscene_Speed = 5.0

--Target Additions. Modifies target properties to allow downed party members. Add these to the target flags.
gci_Target_Allow_Downed = 16
gci_Target_Allow_Downed_Only = 32

--Resistance types. Used by CombatActions.
gciFactor_Slash = 0
gciFactor_Pierce = 1
gciFactor_Strike = 2
gciFactor_Fire = 3
gciFactor_Ice = 4
gciFactor_Lightning = 5
gciFactor_Holy = 6
gciFactor_Shadow = 7
gciFactor_Bleed = 8
gciFactor_Blind = 9
gciFactor_Poison = 10
gciFactor_Corrode = 11
gciFactor_Terrify = 12

--UI Rendering Slots
gci_ACE_RenderSlot_Status_Main = 0
gci_ACE_RenderSlot_Status_Left = 1
gci_ACE_RenderSlot_Status_Right = 2
gci_ACE_RenderSlot_Combat_Ally = 3
gci_ACE_RenderSlot_Combat_Main = 4
gci_ACE_RenderSlot_Basemenu_Main = 5
gci_ACE_RenderSlot_Doctor_Main = 6
gci_ACE_RenderSlot_Victory_Main = 7
gci_ACE_RenderSlot_Equip_Main = 8
gci_ACE_RenderSlot_Equip_Lft = 9
gci_ACE_RenderSlot_Equip_Rgt = 10

--[Combat Position Codes]
--Used for Position application packages.
gciACPoscode_Direct = 0 --Sets the X position manually
gciACPoscode_Party = 1 --Incorporates target into player party
gciACPoscode_Enemy = 2 --Incorporates target into enemy party

--How many ticks are needed for entities to move
gciAC_StandardMoveTicks = 25

--[Combat Party Codes]
--Codes that indicate which party grouping an entity is in.
gciACPartyGroup_None = 0
gciACPartyGroup_Party = 1
gciACPartyGroup_Enemy = 2
gciACPartyGroup_Graveyard = 3
gciACPartyGroup_Reinforcements = 4
gciACPartyGroup_Environment = 5
gciACPartyGroup_Other = 6

--Max Active Party
gciCombat_MaxActivePartySize = 4

--Stat Groupings
gciStatGroup_Base = 0        --Enemies use only the base
gciStatGroup_Job = 1         --Bonus from being in a job
gciStatGroup_Equipment = 2   --Bonus from worn equipment
gciStatGroup_Script = 3      --Bonus from story stuff
gciStatGroup_PermaEffect = 4 --Bonus from equipped abilities
gciStatGroup_TempEffect = 5  --Bonus from temporary effects
gciStatGroup_Final = 6       --All stats summed

--Stat Indexes
gciStatIndex_HPMax = 0
gciStatIndex_MPMax = 1
gciStatIndex_MPRegen = 2
gciStatIndex_CPMax = 3
gciStatIndex_FreeActionMax = 4
gciStatIndex_FreeActionGen = 5
gciStatIndex_Attack = 6
gciStatIndex_Initiative = 7
gciStatIndex_Accuracy = 8
gciStatIndex_Evade = 9
gciStatIndex_Protection = 10
gciStatIndex_Resist_Start = 11
gciStatIndex_Resist_Slash = 11
gciStatIndex_Resist_Strike = 12
gciStatIndex_Resist_Pierce = 13
gciStatIndex_Resist_Flame = 14
gciStatIndex_Resist_Freeze = 15
gciStatIndex_Resist_Shock = 16
gciStatIndex_Resist_Crusade = 17
gciStatIndex_Resist_Obscure = 18
gciStatIndex_Resist_Bleed = 19
gciStatIndex_Resist_Poison = 20
gciStatIndex_Resist_Corrode = 21
gciStatIndex_Resist_Terrify = 22
gciStatIndex_Resist_End = 22
gciStatIndex_StunCap = 23
gciStatIndex_ThreatMultiplier = 24

--Damage Types
gciDamageType_Slashing = 0
gciDamageType_Striking = 1
gciDamageType_Piercing = 2
gciDamageType_Flaming = 3
gciDamageType_Freezing = 4
gciDamageType_Shocking = 5
gciDamageType_Crusading = 6
gciDamageType_Obscuring = 7
gciDamageType_Bleeding = 8
gciDamageType_Poisoning = 9
gciDamageType_Corroding = 10
gciDamageType_Terrifying = 11
gciDamageType_Total = 12
gciDamageOffsetToResistances = 1 --Slot 0 is used for "Protection" when checking resistances.

--Resistance Immunity Threshhold
gciNormalResist = 8
gciResistanceForImmunity = 1000

--AI Script Execution
gciAI_CombatStart = 0
gciAI_TurnStart = 1
gciAI_ActionBegin = 2
gciAI_FreeActionBegin = 3
gciAI_ActionEnd = 4
gciAI_TurnEnd = 5
gciAI_KnockedOut = 6
gciAI_CombatEnds = 7
gciAI_Application_Start = 8
gciAI_Application_End = 9

--AI Special, not a system call
gciAI_UpdateThreat = 1000

--AI Stun Handler. The 0th slot is the Stunned ability slot.
gciAI_StunAbilitySlot = 0
gciAI_AmbushAbilitySlot = 1
gciAI_AbilityRollStart = 2

--Stun Constants
gciStun_CapMaxFactor = 2
gciStun_ResistMax = 3
gciStun_ResistTurnDecrement = 3
gciStun_DecrementCapDivisor = 5
gciStun_ResistLo = 0
gciStun_ResistHi = 3
gciStun_ResistApply = {}
gciStun_ResistApply[0] = 1.00
gciStun_ResistApply[1] = 0.75
gciStun_ResistApply[2] = 0.40
gciStun_ResistApply[3] = 0.10

--Job Script Execution
gciJob_Create = 0
gciJob_SwitchTo = 1
gciJob_Level = 2
gciJob_Master = 3
gciJob_JobAssembleSkillList = 4
gciJob_BeginCombat = 5
gciJob_BeginTurn = 6
gciJob_BeginAction = 7
gciJob_BeginFreeAction = 8
gciJob_EndAction = 9
gciJob_EndTurn = 10
gciJob_EndCombat = 11
gciJob_EventQueued = 12

--Query Script Code
gciQuery_Script_Code = 1000

--[Ability Constants]
--Ability Script Execution
gciAbility_Create = 0
gciAbility_AssumeJob = 1
gciAbility_BeginCombat = 2
gciAbility_BeginTurn = 3
gciAbility_BeginAction = 4
gciAbility_BeginFreeAction = 5
gciAbility_PostAction = 6
gciAbility_PaintTargets = 7
gciAbility_PaintTargetsResponse = 8
gciAbility_Execute = 9
gciAbility_TurnEnds = 10
gciAbility_CombatEnds = 11
gciAbility_CreateSpecial = 12
gciAbility_GUIApplyEffect = 13
gciAbility_EventQueued = 14
gciAbility_BuildPredictionBox = 15
gciAbility_QueryCanRun = 16

gciAbility_SpecialStart = 1000

gciAbility_SaveblockA_X1 = 6
gciAbility_SaveblockA_X2 = 9
gciAbility_SaveblockA_Y1 = 1
gciAbility_SaveblockA_Y2 = 2
gciAbility_SaveblockB_X1 = 0
gciAbility_SaveblockB_X2 = 5
gciAbility_SaveblockB_Y1 = 2
gciAbility_SaveblockB_Y2 = 2

--Standard Codes
gciAbility_ResponseStandard_DirectAction = 0
gciAbility_ResponseStandard_Passive = 1
gciAbility_ResponseStandard_Counterattack = 2

--Ability Description Y Offsets
gcfAbilityImgOffsetY = 3.0

--Ability Timer Offset When Striking Many Targets
gciAbility_TicksOffsetPerTarget = 15

--Black-Flash Ticks
gciAbility_BlackFlashTicks = 15

--Ability Grid
gciAbilityGrid_XSize = 10
gciAbilityGrid_YSize = 3
gciAbilityGrid_Class_Abilities = 6

--[Effect Constants]
gciEffect_Create = 0
gciEffect_ApplyStats = 1
gciEffect_UnApplyStats = 2
gciEffect_BeginTurn = 3
gciEffect_BeginAction = 4
gciEffect_BeginFreeAction = 5
gciEffect_PostAction = 6
gciEffect_TurnEnds = 7
gciEffect_CombatEnds = 8

--Stat Mod Standard: Description Handlers
gciEffect_StatMod_ApplyStats = 1001
gciEffect_StatMod_UnapplyStats = 1002
gciEffect_StatMod_SetShortText = 1003
gciEffect_StatMod_SetInspectorTitle = 1004
gciEffect_StatMod_SetDescription = 1005

--ThreatVs. Standard
gciEffect_ThreatVs_SetValues = 1006

--Converts gciEffect_ApplyStats to gciEffect_StatMod_ApplyStats
gciEffect_StatMod_ConvertToSubroutine = gciEffect_StatMod_ApplyStats - gciEffect_ApplyStats

--Structure used by the StatMod script.
gzaStatModStruct = {}
gzaStatModStruct.sDisplayName = "Null"
gzaStatModStruct.iDuration    = 0 --If -1, effect never expires.
gzaStatModStruct.iBacking     = 0 --One of the gsAbility_Backing_Buff series
gzaStatModStruct.iFrame       = 0 --One of the gsAbility_Frame_Active
gzaStatModStruct.sFrontImg    = "Null"
gzaStatModStruct.sStatString  = "Null"
gzaStatModStruct.saStrings    = {}
gzaStatModStruct.sShortText   = "Null"

--[Application Constants]
gciApplication_TextTicks = 15
gciApplication_EventPaddingTicks = 5
gciApplication_FlashWhiteTicks = 15
gciApplication_FlashWhiteHold = 15

--Combat Tick Timers
gciCombatTicks_StandardActionMinimum = 10
gciCombatTicks_StandardTitleLen = 120

--[Character Constants]
gciCombatEntity_Create = 0
gciCombatEntity_BeginCombat = 1
gciCombatEntity_BeginTurn = 2
gciCombatEntity_BeginAction = 3
gciCombatEntity_BeginFreeAction = 4
gciCombatEntity_EndAction = 5
gciCombatEntity_EndTurn = 6
gciCombatEntity_EndCombat = 7
gciCombatEntity_EventQueued = 8

--[Threat Handling]
gcfThreat_DamageMe = 1.0
gcfThreat_DamageAlly = 0.25
gcfThreat_Heal = 0.25
gcfThreat_EffectMe = 0.40
gcfThreat_EffectAlly = 0.10

--[Combat Animations]
--Global array of timings.
giAnimationTimingsTotal = 0
gzaAnimationTimings = {}

--Defaults for ineligible animations/sounds.
gsDefaultAttackAnimation = "Sword Slash"
gsDefaultAttackSound = "Combat\\|Impact_Slash"
gsDefaultCriticalSound = "Combat\\|Impact_Slash_Crit"

--Offsets.
giAnimationMissOffset = 10

-- |[ ==================================== Field Abilities ===================================== ]|
--Switch Types
gciFieldAbility_Create = 0
gciFieldAbility_Run = 1
gciFieldAbility_RunWhileCooling = 2
gciFieldAbility_Cancel = 3
gciFieldAbility_TouchEnemy = 4
gciFieldAbility_ModifyActor = 5

--Combat Codes
gciFieldAbility_Response_BeginCombat = 0
gciFieldAbility_Response_BeginTurn = 1
gciFieldAbility_Response_BeginAction = 2

--Universal Codes
gbFieldAbilityHandledInput = false
gciFieldAbility_Activate_Null = 0
gciFieldAbility_Activate_Florentina_PickLock = 1

--Special Level Types
gciLevel_SpecialType_Chest = 1
gciLevel_SpecialType_FakeChest = 2
gciLevel_SpecialType_Door = 3
gciLevel_SpecialType_Exit = 4
gciLevel_SpecialType_Inflection = 5
gciLevel_SpecialType_Climbable = 6
gciLevel_SpecialType_Location = 7
gciLevel_SpecialType_Position = 8
gciLevel_SpecialType_InvisZone = 9
gciLevel_SpecialType_Actor = 10

-- |[ =================================== Stat Icon Markdown =================================== ]|
gsaStatIcons = {}
gsaStatIcons[1]  = {"[Buff]",  "Root/Images/AdventureUI/DebuffIcons/Buff"}
gsaStatIcons[2]  = {"[Debuff]","Root/Images/AdventureUI/DebuffIcons/Debuff"}
gsaStatIcons[3]  = {"[Atk]",   "Root/Images/AdventureUI/StatisticIcons/Attack"}
gsaStatIcons[4]  = {"[Prt]",   "Root/Images/AdventureUI/StatisticIcons/Protection"}
gsaStatIcons[5]  = {"[Hlt]",   "Root/Images/AdventureUI/StatisticIcons/Health"}
gsaStatIcons[6]  = {"[Acc]",   "Root/Images/AdventureUI/StatisticIcons/Accuracy"}
gsaStatIcons[7]  = {"[Evd]",   "Root/Images/AdventureUI/StatisticIcons/Evade"}
gsaStatIcons[8]  = {"[Ini]",   "Root/Images/AdventureUI/StatisticIcons/Initiative"}
gsaStatIcons[9]  = {"[Man]",   "Root/Images/AdventureUI/StatisticIcons/Mana"}
gsaStatIcons[10] = {"[Blnd]",  "Root/Images/AdventureUI/StatisticIcons/Blind"}
gsaStatIcons[11] = {"[Bld]",   "Root/Images/AdventureUI/StatisticIcons/Bleed"}
gsaStatIcons[12] = {"[Psn]",   "Root/Images/AdventureUI/StatisticIcons/Poison"}
gsaStatIcons[13] = {"[Crd]",   "Root/Images/AdventureUI/StatisticIcons/Corrode"}
gsaStatIcons[14] = {"[Shld]",  "Root/Images/AdventureUI/StatisticIcons/Shields"}
gsaStatIcons[15] = {"[Adrn]",  "Root/Images/AdventureUI/StatisticIcons/Adrenaline"}
gsaStatIcons[16] = {"[Cmbp]",  "Root/Images/AdventureUI/StatisticIcons/ComboPoint"}
gsaStatIcons[17] = {"[Stun]",  "Root/Images/AdventureUI/StatisticIcons/Stun"}
gsaStatIcons[18] = {"[Turn]",  "Root/Images/AdventureUI/DebuffIcons/Clock"}
gsaStatIcons[19] = {"[Turns]", "Root/Images/AdventureUI/DebuffIcons/Clock"}
gsaStatIcons[20] = {"[Clock]", "Root/Images/AdventureUI/DebuffIcons/Clock"}
gsaStatIcons[21] = {"[Slsh]",  "Root/Images/AdventureUI/StatisticIcons/Slash"}
gsaStatIcons[22] = {"[Prc]",   "Root/Images/AdventureUI/StatisticIcons/Pierce"}
gsaStatIcons[23] = {"[Stk]",   "Root/Images/AdventureUI/StatisticIcons/Strike"}
gsaStatIcons[24] = {"[Flm]",   "Root/Images/AdventureUI/StatisticIcons/Flaming"}
gsaStatIcons[25] = {"[Frz]",   "Root/Images/AdventureUI/StatisticIcons/Freezing"}
gsaStatIcons[26] = {"[Shk]",   "Root/Images/AdventureUI/StatisticIcons/Shocking"}
gsaStatIcons[27] = {"[Cru]",   "Root/Images/AdventureUI/StatisticIcons/Crusading"}
gsaStatIcons[28] = {"[Obs]",   "Root/Images/AdventureUI/StatisticIcons/Obscuring"}
gsaStatIcons[29] = {"[Tfy]",   "Root/Images/AdventureUI/StatisticIcons/Terrify"}
gsaStatIcons[30] = {"[UpN]",   "Root/Images/AdventureUI/StatisticIcons/NrUp"}
gsaStatIcons[31] = {"[DnN]",   "Root/Images/AdventureUI/StatisticIcons/NrDn"}
giStatIconsTotal = 31

--[Modifier Table]
--Used when applying stats in gsStandardStatPath, maps strings to the variables they affect.
gzaModTable = {}
gzaModTable[ 1] = {"HPMax", gciStatIndex_HPMax}
gzaModTable[ 2] = {"MPMax", gciStatIndex_MPMax}
gzaModTable[ 3] = {"MPReg", gciStatIndex_MPRegen}
gzaModTable[ 4] = {"CPMax", gciStatIndex_CPMax}
gzaModTable[ 5] = {"FAMax", gciStatIndex_FreeActionMax}
gzaModTable[ 6] = {"FAGen", gciStatIndex_FreeActionGen}
gzaModTable[ 7] = {"Atk", gciStatIndex_Attack}
gzaModTable[ 8] = {"Ini", gciStatIndex_Initiative}
gzaModTable[ 9] = {"Acc", gciStatIndex_Accuracy}
gzaModTable[10] = {"Evd", gciStatIndex_Evade}
gzaModTable[11] = {"Prt", gciStatIndex_Protection}
gzaModTable[12] = {"ResSls", gciStatIndex_Resist_Slash}
gzaModTable[13] = {"ResStk", gciStatIndex_Resist_Strike}
gzaModTable[14] = {"ResPrc", gciStatIndex_Resist_Pierce}
gzaModTable[15] = {"ResFlm", gciStatIndex_Resist_Flame}
gzaModTable[16] = {"ResFrz", gciStatIndex_Resist_Freeze}
gzaModTable[17] = {"ResShk", gciStatIndex_Resist_Shock}
gzaModTable[18] = {"ResCru", gciStatIndex_Resist_Crusade}
gzaModTable[19] = {"ResObs", gciStatIndex_Resist_Obscure}
gzaModTable[20] = {"ResBld", gciStatIndex_Resist_Bleed}
gzaModTable[21] = {"ResPsn", gciStatIndex_Resist_Poison}
gzaModTable[22] = {"ResCrd", gciStatIndex_Resist_Corrode}
gzaModTable[23] = {"ResTfy", gciStatIndex_Resist_Terrify}
gzaModTable[24] = {"StunCap", gciStatIndex_StunCap}
gzaModTable[25] = {"ThreatMult", gciStatIndex_ThreatMultiplier}
giModTableTotal = 25
    
-- |[ ===================================== Ability Paths ====================================== ]|
--Normal Action
gsAbility_Backing_Direct = "Root/Images/AdventureUI/Abilities/OctBackGrey"
gsAbility_Backing_DoT    = "Root/Images/AdventureUI/Abilities/OctBackRed"
gsAbility_Backing_Heal   = "Root/Images/AdventureUI/Abilities/OctBackGreen"
gsAbility_Backing_Buff   = "Root/Images/AdventureUI/Abilities/OctBackBlue"
gsAbility_Backing_Debuff = "Root/Images/AdventureUI/Abilities/OctBackPurple"

gsAbility_Frame_Active  = "Root/Images/AdventureUI/Abilities/OctFrameRed"
gsAbility_Frame_Passive = "Root/Images/AdventureUI/Abilities/OctFrameBlue"
gsAbility_Frame_Special = "Root/Images/AdventureUI/Abilities/OctFrameTeal"

--Free Action
gsAbility_Backing_Free_Direct = "Root/Images/AdventureUI/Abilities/SqrBackGrey"
gsAbility_Backing_Free_DoT    = "Root/Images/AdventureUI/Abilities/SqrBackRed"
gsAbility_Backing_Free_Heal   = "Root/Images/AdventureUI/Abilities/SqrBackGreen"
gsAbility_Backing_Free_Buff   = "Root/Images/AdventureUI/Abilities/SqrBackBlue"
gsAbility_Backing_Free_Debuff = "Root/Images/AdventureUI/Abilities/SqrBackPurple"

gsAbility_Frame_Free_Active  = "Root/Images/AdventureUI/Abilities/SqrFrameRed"
gsAbility_Frame_Free_Passive = "Root/Images/AdventureUI/Abilities/SqrFrameBlue"
gsAbility_Frame_Free_Special = "Root/Images/AdventureUI/Abilities/SqrFrameTeal"

-- |[ ==================================== Random Generator ==================================== ]|
gsRandomExaminationHandler = gsRoot .. "System/902 Random Examination.lua"

-- |[ ================================= Rune Animation Counts ================================== ]|
gciMeiRuneFramesTotal = 60

-- |[ ======================================== Mugging ========================================= ]|
--Constants modifying how much XP/JP/Cash is given from mugging. Advisories, scripts decide what the
-- final values are.
gcfMugPlatinaRate = 0.20
gcfMugExperienceRate = 0.20
gcfMugJPRate = 0.20

-- |[ ================================= Effect Type Constants ================================== ]|
--Effect Base
gciEffect_None = 0
gciEffect_Terrify = 1
gciEffect_Poison = 2
gciEffect_Bleed = 3 
gciEffect_Corrode = 4 
gciEffect_Blind = 5
gciEffect_Stun = 6

--Stat Modifiers
gciEffect_Speed = 7
gciEffect_DamagePercent = 8
gciEffect_DamageFixed = 9
gciEffect_Protection = 10
gciEffect_Accuracy = 11
gciEffect_Evade = 12
gciEffect_Initiative = 13

--Resistance Buff
gciEffect_ResBuf_Slash = 14
gciEffect_ResBuf_Pierce = 15
gciEffect_ResBuf_Strike = 16
gciEffect_ResBuf_Fire = 17
gciEffect_ResBuf_Ice = 18
gciEffect_ResBuf_Lightning = 19
gciEffect_ResBuf_Holy = 20
gciEffect_ResBuf_Shadow = 21
gciEffect_ResBuf_Bleed = 22
gciEffect_ResBuf_Blind = 23
gciEffect_ResBuf_Poison = 24
gciEffect_ResBuf_Corrode = 25
gciEffect_ResBuf_Terrify = 26

--Special
gciEffect_Threat = 27

-- |[ ================================== Equipment Constants =================================== ]|
--Damage Type Equipment Categories
gciEquipment_DamageType_Base = 0
gciEquipment_DamageType_Override = 1
gciEquipment_DamageType_Bonus = 2

-- |[Adamantite and Crafting]|
--Indices
gciCraft_Adamantite_Powder = 0
gciCraft_Adamantite_Flakes = 1
gciCraft_Adamantite_Shard = 2
gciCraft_Adamantite_Piece = 3
gciCraft_Adamantite_Chunk = 4
gciCraft_Adamantite_Ore = 5
gciCraft_Adamantite_Total = 6

--Clear the starting crafting values
for i = 0, gciCraft_Adamantite_Total-1, 1 do
	AdInv_SetProperty("Crafting Material", i, 0)
end

-- |[Catalyst Constants]|
--Codes
gciCatalyst_Health = 0
gciCatalyst_Attack = 1
gciCatalyst_Initiative = 2
gciCatalyst_Dodge = 3
gciCatalyst_Accuracy = 4
gciCatalyst_Skill = 5

--Needed per boost
gciCatalyst_Health_Needed = 5
gciCatalyst_Attack_Needed = 3
gciCatalyst_Initiative_Needed = 3
gciCatalyst_Dodge_Needed = 3
gciCatalyst_Accuracy_Needed = 3
gciCatalyst_Skill_Needed = 6

--Amount per boost
gciCatalyst_Health_Bonus = 10
gciCatalyst_Attack_Bonus = 3
gciCatalyst_Initiative_Bonus = 5
gciCatalyst_Dodge_Bonus = 3
gciCatalyst_Accuracy_Bonus = 3

-- |[Gem Colors]|
gciGemColor_Grey   =   1
gciGemColor_Red    =   2
gciGemColor_Blue   =   4
gciGemColor_Orange =   8
gciGemColor_Violet =  16
gciGemColor_Yellow =  32
gciGemColor_All    = 255

gciSubgemMax = 5
gciGemSlotsMax = 6

-- |[ ===================================== Party Variables ==================================== ]|
--[Special Variables]
--Variables that indicate the player's party or NPCs associated with it.
gsPartyLeaderName = "No Leader"
giPartyLeaderID = 0

--Level music to play when reverting dialogue.
gsLevelMusic = "Null"

-- |[ ======================================= DataLibrary ====================================== ]|
--[DataLibrary Setup]
--The VariableManager needs certain paths set up for chests. Chests are "open" if a variable exists in the following pattern:
-- "Root/Variables/Chests/[ROOMNAME]/[CHESTNAME]"
--The roomname is the name of the folder containing the room, and the chestname is set in Tiled as the Name property.
DL_AddPath("Root/Variables/Chests/")

--Variables indicating overall chapter progression
DL_AddPath("Root/Variables/Global/ChapterComplete/")
VM_SetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter2", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter3", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter4", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter5", "N", 0.0)

--Storage States
LM_ExecuteScript(gsRoot .. "Chapter 1/Cleanup/900 Variable Listing.lua")

--Paths that are independent of the current chapter.
DL_AddPath("Root/Variables/Global/")
DL_AddPath("Root/Variables/System/Startup/")
DL_AddPath("Root/Variables/System/Special/")

--Catalyst Tone. Gets sets to true when the player finds the Platinum Compass.
DL_AddPath("Root/Variables/Global/CatalystTone/")
VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 0.0)

--Sound settings.
VM_SetVar("Root/Variables/System/Special/iHasSoundSettings", "N", 0.0)
VM_SetVar("Root/Variables/System/Special/fMusicVolume", "N", AudioManager_GetProperty("Music Volume"))
VM_SetVar("Root/Variables/System/Special/fSoundVolume", "N", AudioManager_GetProperty("Sound Volume"))

--Ambient Light Boost
VM_SetVar("Root/Variables/System/Special/fAmbientLightBoost", "N", 0.0)
AL_SetProperty("Ambient Light Boost", 0.0)

--Tourist mode. Can be enabled from the options menu to make the game much, much easier.
-- Useful for players who don't want challenging combat and just want to see the TF stuff. Psychos!
VM_SetVar("Root/Variables/System/Special/iIsTouristMode", "N", 0.0)
AdvCombat_SetProperty("Tourist Mode", false)

--Combat Options
VM_SetVar("Root/Variables/System/Special/iAutoUseDoctorBag", "N", 1.0)
VM_SetVar("Root/Variables/System/Special/iMemoryCursor", "N", 1.0)
AdvCombat_SetProperty("Auto Doctor Bag", true)
AdvCombat_SetProperty("Memory Cursor", true)

--Dialogue Options
VM_SetVar("Root/Variables/System/Special/iAutoHastenDialogue", "N", 0.0)
WD_SetProperty("Set AutoHasten", false)

--Catalysts. These are tracked independently of the chapter and apply across chapters. Note that these are read-only values. They are only used for saving/loading.
-- The only place they should be modified is the designated scripts (which are chest-open handlers).
DL_AddPath("Root/Variables/Global/Catalysts/")
VM_SetVar("Root/Variables/Global/Catalysts/iHealth", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iAttack", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iInitiative", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iDodge", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iAccuracy", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iMovement", "N", 0.0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Health, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Dodge, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Movement, 0)

--Total number of Catalysts in Chapter 1:
AM_SetProperty("Total Catalysts", 33)

--Quarters Constants. These are the ID values for items in Christine's quarters.
gci_Quarters_Nothing = 0
gci_Quarters_DefragPod = 1
gci_Quarters_CounterL = 2
gci_Quarters_CounterM = 3
gci_Quarters_CounterR = 4
gci_Quarters_OilMaker = 5
gci_Quarters_TV = 6
gci_Quarters_ChairS = 7
gci_Quarters_CouchSL = 8
gci_Quarters_CouchSM = 9
gci_Quarters_CouchSR = 10
gci_Quarters_ChairN = 11
gci_Quarters_CouchNL = 12
gci_Quarters_CouchNM = 13
gci_Quarters_CouchNR = 14

--Face Table Size
gci_FaceTable_Size = 32

-- |[ ================================== Stat Profile Globals ================================== ]|
--Used for the stat profiler and for standardized job stat computations.
giaHPArray  = {}
giaAtkArray = {}
giaAccArray = {}
giaEvdArray = {}
giaIniArray = {}

-- |[ ================================ Cross-Chapter Variables ================================= ]|
--Some things persist between chapters, like catalysts and doctor bag upgrades. These are their
-- base values.

--[Doctor Bag]
--Starts at 100% charge rate, 100 charges max.
DL_AddPath("Root/Variables/CrossChapter/DoctorBag/")
VM_SetVar( "Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargeSizeUpgrade", "N", 0)
VM_SetVar( "Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargeRateUpgrade", "N", 0)
VM_SetVar( "Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", 100)
VM_SetVar( "Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", 100)

--Paths and global variables used by the doctor bag.
gsComputeDoctorBagTotalPath = gsRoot .. "Subroutines/Compute Doctor Bag Total.lua"
giTotalDoctorBagChargeSize = 100
gfTotalDoctorBagChargeRate = 1.0

--If true, the gsComputeDoctorBagTotalPath routine auto-syncs the results with the inventory.
gbAutoSetDoctorBagProperties = true 

--If true, the doctor bag current charges will equal its max charges when using the above script. It
-- then toggles itself to false. This is used for rest actions.
gbAutoSetDoctorBagCurrentValues = false

--This must be synced with the UI.
AdInv_SetProperty("Doctor Bag Charges", 100)
AdInv_SetProperty("Doctor Bag Charges Max", 100)

--When bypassing the intro, start with the Doctor Bag:
if(gbBypassIntro == true) then
	VM_SetVar("Root/Variables/System/Special/iDoctorBagCharges", "N", 100.0)
	VM_SetVar("Root/Variables/System/Special/iDoctorBagChargesMax", "N", 100.0)
	AdInv_SetProperty("Doctor Bag Charges Max", 100)
	AdInv_SetProperty("Doctor Bag Charges", 100)
end

-- |[ =================================== Sprite Variables ===================================== ]|
--For any character that appears in a table with variable forms (like the Transform campfire menu),
-- these variables track their sprite. They are key-value pairs to be searchable.
--These can be updated by transformations or costume changes.
gsCharSprites = {}
gsCharSprites[1] = {"Mei",        "Root/Images/Sprites/Mei_Human/SW|0"}
gsCharSprites[2] = {"Florentina", "Root/Images/Sprites/Florentina/SW|0"}
gsCharSprites[3] = {"Christine",  "Root/Images/Sprites/Christine_Human/SW|0"}
gsCharSprites[4] = {"55",         "Root/Images/Sprites/55/SW|0"}
gsCharSprites[5] = {"JX101",      "Root/Images/Sprites/JX101/SW|0"}
gsCharSprites[6] = {"SX399",      "Root/Images/Sprites/SX399Lord/SW|0"}
gsCharSprites[7] = {"Sophie",     "Root/Images/Sprites/Sophie/SW|0"}
giCharSpritesTotal = 7

-- |[ ==================================== Follower Remaps ===================================== ]|
--Remapping structure that maps a character name to a combat name and following variable.
gzaFollowerRemaps = {}
gzaFollowerRemaps[1] = {"Florentina", "Florentina", "Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene"}
gzaFollowerRemaps[2] = {"55",         "55",         "Root/Variables/Chapter5/Scenes/iIs55Following"}
gzaFollowerRemaps[3] = {"SX399",      "SX399",      "Root/Variables/Chapter5/Scenes/iSX399IsFollowing"}
gzaFollowerRemaps[4] = {"JX101",      "JX-101",     "Root/Variables/Chapter5/Scenes/iIsJX101Following"}
gzaFollowerRemaps[5] = {"Sophie",     "Null",       "Null"}

-- |[ ===================================== Loading Lists ====================================== ]|
--Used to help compartmentalize the loading sequence. The UI loader uses a series of lists that are
-- declared here. Whenever a new enemy or form is added, just add it to these lists. Easy!

--[Sprites]
--This is the master sprite list. It is only used when loading without chapter distinctions. When using chapter-specific load routines, these
-- lists are trimmed.

--Player party.
gcsaChristineList  = {"Human", "Darkmatter", "DreamGirl", "Electrosprite", "Golem", "GolemDress", "Latex", "Male", "SteamDroid", "Raibie", "Raiju", "RaijuClothes", "Doll"}
gcsaJeanneList     = {"Human"}
gcsaMeiList        = {"Human", "Alraune", "Bee", "Ghost", "Slime", "Werecat", "Alraune_MC", "Bee_MC", "Human_MC"}
gcsaSanyaList      = {"Human"}
gcsaLottaList      = {"Human"}
gcsaTaliaList      = {"SKIP"}
gcsaOtherPartyList = {"55", "56", "Aquillia", "Breanne", "Florentina", "Sophie", "SophieDress", "SX399Lord", "Sammy"}
gcsaOtherEightList = {"DarkmatterGirl"} --8 directional NPCs who don't join the party.

--Major NPCs, four-directions.
gcsaMajorNPCList   = {"Adina", "Blythe", "Claudia", "Nadia", "Rochea", "Septima", "SX399", "JX101", "DrMaisie", "Isabel"}
gcsaCassandraList  = {"CassandraH", "CassandraW", "CassandraG"}

--Generic NPCs and Enemies.
gcsaGenericNPCList = {"GenericF0", "GenericF1", "GenericM0", "GenericM1", "CultistF", "CultistM", "MercF", "MercM", "GenericBiolabsFBlue", "GenericBiolabsFRed", "GenericBiolabsFGreen", "GenericBiolabsFYellow", 
                      "GenericBiolabsMBlue", "GenericBiolabsMRed", "GenericBiolabsMGreen", "GenericBiolabsMYellow"}
gcsaChapter1List   = {"Alraune", "BeeGirl", "BeeGirlZ", "BestFriend", "MaidGhost", "MirrorImage", "Slime", "SlimeB", "SlimeG", "Rilmani", "SkullCrawler", "Werecat"}
gcsaChapter3List   = {"Imp"}
gcsaChapter5List   = {"609144", "20", "Doll", "DollInfluenced", "Electrosprite", "GolemLordA", "GolemLordB", "GolemLordC", "GolemLordD", "GolemSlave", "GolemSlaveP", "GolemSlaveR", "EldritchDream", "Horrible",
	                  "Hoodie", "InnGeisha", "BandageImp", "Electrosprite", "LatexDrone", "Maisie", "SteamDroid", "Scraprat", "SecurityBot", "SecurityBotBroke", "Raibie", "Raiju", "Vivify", "VivifyBlack", "VoidRift",
                      "LatexBlonde", "LatexBrown", "LatexRed"}
gcsaOtherNPCList   = {"Tram"}

--Special Frame Lists.
gcsaCWSpecialList     = {"2855", "SX399", "Christine_DarkM", "Christine_DreamGirl", "Christine_Doll", "Christine_Golem", "Christine_Human", "Christine_Latex", "Christine_Male", "Christine_Raibie", "Christine_Raiju", "Christine_RaijuClothes", "Christine_Doll", "Florentina", "MeiAlraune", "MeiBee", "Mei", "MeiGhost", "MeiSlime", "MeiWerecat", "Alraune"}
gcsaCrouchSpecialList = {"MeiBeeMC", "MeiMC"}
gcsaWoundedSpeciaList = {"Aquillia", "CassandraH", "CassandraW", "CultistF", "Doll", "GenericF1", "GolemLord", "GolemSlave", "Werecat"}

--Half-Frame Lists. Used for some NPCs with two-directional movement, like sheep and chickens.
gcsaHalfFrameList = {"Sheep", "Chicken"}
