--[Graphics]
--Loads all graphics for general-purposes Adventure Mode. Chapter-and-character-specific graphics are loaded elsewhere.

--[Load Handling]
--Prevent double loads.
if(gbHasLoadedAssets ~= nil) then 
    AdvCombat_SetProperty("Construct")
    LM_ExecuteScript(gsRoot .. "Combat/Animations/ZRouting.lua")
    return
end
gbHasLoadedAssets = true

--Reset counters.
fnIssueLoadReset("AdventureMode")

--[Sub-Calls]
--Subscripts handle each individual .slf file.
local sBasePath = fnResolvePath()
io.write("Loading sprites:\n")
LM_ExecuteScript(sBasePath .. "100 Graphics Sprites.lua")
io.write("Loading portraits:\n")
LM_ExecuteScript(sBasePath .. "101 Graphics Portraits.lua")
io.write("Loading UI:\n")
LM_ExecuteScript(sBasePath .. "102 Graphics UI.lua")
io.write("Loading scenes:\n")
LM_ExecuteScript(sBasePath .. "103 Graphics Scenes.lua")
io.write("Loading controls UI:\n")
LM_ExecuteScript(sBasePath .. "104 Control Manager UI.lua")
SLF_Close()
io.write("Finished loading.\n")

--[Construction]
--Some classes need to be told when images are done loading. Do that here.
WD_SetProperty("Construct")
AdvCombat_SetProperty("Construct")

--Create all combat animations.
LM_ExecuteScript(gsRoot .. "Combat/Animations/ZRouting.lua")

--[Timer Marking]
--Finish.
fnCompleteLoadSequence()