--[Time Variables]
--These are used to dictate the flow of time. Only used in a few spots.

--[Variables]
--Setup.
DL_AddPath("Root/Variables/Global/Time/")

--Time of day. This uses the 24 hour clock. 12 is Noon, 15 would be 3 o'clock, etc.
VM_SetVar("Root/Variables/Global/Time/iTimeOfDay", "N", 9)

--[Time of Day Colors]
--Current time trackers. Note that these are for the mixing version, NOT the overlay (they'd be 0's instead of 1's for the overlay).
gcf_CurrentR = 1.00
gcf_CurrentG = 1.00
gcf_CurrentB = 1.00
gcf_CurrentA = 1.00

--Morning
gcf_Morning_R = 1.00
gcf_Morning_G = 0.80
gcf_Morning_B = 0.90
gcf_Morning_A = 1.00

--Noon.
gcf_Noon_R = 1.00
gcf_Noon_G = 1.00
gcf_Noon_B = 1.00
gcf_Noon_A = 1.00

--Evening.
gcf_Evening_R = 1.00
gcf_Evening_G = 0.75
gcf_Evening_B = 0.75
gcf_Evening_A = 1.00

--Night.
gcf_Night_R = 0.45
gcf_Night_G = 0.42
gcf_Night_B = 0.70
gcf_Night_A = 1.00

--[Function: fnModifyTimeOfDay]
--Changes the time of day colors (NOT VARIABLES). Time of day goes from 9 (Morning) to 12 (Noon) to 15 (Afternoon) to 18 (Evening).
-- The events counter counts from 1 to 4, and is the rough percentage between the hours.
-- Passing (9, 4) would result in a color equal to 12 since it's 100% towards noon from morning.
-- Time caps in the evening, so (15, 4) is night-time colors.
--Passing invalid values does nothing. Only 9, 12, 15, and 18 are accepted.
--Passing the third option changes this to being a percentage-directly function. If it is nil, the cap is 4.
giTmeOfDayModifyTicks = 45
function fnModifyTimeOfDay(iBaseTime, iEvents, iEventsMax)
	
	--Arg check.
	if(iBaseTime == nil) then return end
	if(iEvents   == nil) then return end
	if(iEventsMax == nil) then iEventsMax = 4 end
	
	--Percentage completion.
	local fPercentPrev = (iEvents-1) / iEventsMax
	local fPercentComplete = iEvents / iEventsMax
	
	--Clamp: fPercentPrev can't go lower than zero.
	if(fPercentPrev < 0.0) then fPercentPrev = 0.0 end
	
	--Clamp: Events can go up to iEventsMax. If exceeding this, then no fading is performed.
	if(iEvents >= iEventsMax+1) then
		fPercentPrev = 1.0
		fPercentComplete = 1.0
	end
	
	--Base time: 9'o'clock, goes to noon.
	if(iBaseTime == 9.0) then
		local fRS = gcf_Morning_R + ((gcf_Noon_R - gcf_Morning_R) * fPercentPrev)
		local fGS = gcf_Morning_G + ((gcf_Noon_G - gcf_Morning_G) * fPercentPrev)
		local fBS = gcf_Morning_B + ((gcf_Noon_B - gcf_Morning_B) * fPercentPrev)
		local fAS = gcf_Morning_A + ((gcf_Noon_A - gcf_Morning_A) * fPercentPrev)
		local fRE = gcf_Morning_R + ((gcf_Noon_R - gcf_Morning_R) * fPercentComplete)
		local fGE = gcf_Morning_G + ((gcf_Noon_G - gcf_Morning_G) * fPercentComplete)
		local fBE = gcf_Morning_B + ((gcf_Noon_B - gcf_Morning_B) * fPercentComplete)
		local fAE = gcf_Morning_A + ((gcf_Noon_A - gcf_Morning_A) * fPercentComplete)
		AL_SetProperty("Activate Fade", giTmeOfDayModifyTicks, gci_Fade_Under_GUI, true, fRS, fGS, fBS, fAS, fRE, fGE, fBE, fAE, true)
		
		--Save the values.
		gcf_CurrentR = fRE
		gcf_CurrentG = fGE
		gcf_CurrentB = fBE
		gcf_CurrentA = fAE
	
	--Base time: 12'o'clock, goes to 3pm. This is always clear.
	elseif(iBaseTime == 12.0) then
		AL_SetProperty("Activate Fade", giTmeOfDayModifyTicks, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 0)
		
		--Save the values.
		gcf_CurrentR = 1.00
		gcf_CurrentG = 1.00
		gcf_CurrentB = 1.00
		gcf_CurrentA = 1.00
	
	--Base time: 3'o'clock, goes to late afternoon.
	elseif(iBaseTime == 15.0) then
		local fRS = gcf_Noon_R + ((gcf_Evening_R - gcf_Noon_R) * fPercentPrev)
		local fGS = gcf_Noon_G + ((gcf_Evening_G - gcf_Noon_G) * fPercentPrev)
		local fBS = gcf_Noon_B + ((gcf_Evening_B - gcf_Noon_B) * fPercentPrev)
		local fAS = gcf_Noon_A + ((gcf_Evening_A - gcf_Noon_A) * fPercentPrev)
		local fRE = gcf_Noon_R + ((gcf_Evening_R - gcf_Noon_R) * fPercentComplete)
		local fGE = gcf_Noon_G + ((gcf_Evening_G - gcf_Noon_G) * fPercentComplete)
		local fBE = gcf_Noon_B + ((gcf_Evening_B - gcf_Noon_B) * fPercentComplete)
		local fAE = gcf_Noon_A + ((gcf_Evening_A - gcf_Noon_A) * fPercentComplete)
		AL_SetProperty("Activate Fade", giTmeOfDayModifyTicks, gci_Fade_Under_GUI, true, fRS, fGS, fBS, fAS, fRE, fGE, fBE, fAE, true)
		
		--Save the values.
		gcf_CurrentR = fRE
		gcf_CurrentG = fGE
		gcf_CurrentB = fBE
		gcf_CurrentA = fAE
	
	--Base time: 6'o'clock, goes to evening.
	elseif(iBaseTime == 18.0) then
		local fRS = gcf_Evening_R + ((gcf_Night_R - gcf_Evening_R) * fPercentPrev)
		local fGS = gcf_Evening_G + ((gcf_Night_G - gcf_Evening_G) * fPercentPrev)
		local fBS = gcf_Evening_B + ((gcf_Night_B - gcf_Evening_B) * fPercentPrev)
		local fAS = gcf_Evening_A + ((gcf_Night_A - gcf_Evening_A) * fPercentPrev)
		local fRE = gcf_Evening_R + ((gcf_Night_R - gcf_Evening_R) * fPercentComplete)
		local fGE = gcf_Evening_G + ((gcf_Night_G - gcf_Evening_G) * fPercentComplete)
		local fBE = gcf_Evening_B + ((gcf_Night_B - gcf_Evening_B) * fPercentComplete)
		local fAE = gcf_Evening_A + ((gcf_Night_A - gcf_Evening_A) * fPercentComplete)
		AL_SetProperty("Activate Fade", giTmeOfDayModifyTicks, gci_Fade_Under_GUI, true, fRS, fGS, fBS, fAS, fRE, fGE, fBE, fAE, true)
		
		--Save the values.
		gcf_CurrentR = fRE
		gcf_CurrentG = fGE
		gcf_CurrentB = fBE
		gcf_CurrentA = fAE
	end
end

--[fnCassandraTime]
--Variables set during the Cassandra Werecat event. This is called from multiple files so it's centralized here.
-- As the party fights more werecats, the time of day advances. Once night falls, it's too late for Cassandra.
--This function does not advance the time, it just sets the overlay.
function fnSetCassandraTime()
	
	--Constants.
	local cfFirstSet = 2
	local cfSecondSet = 2
	local cfThirdSet = 2
	
	--Get how many encounters we have done. Use a +1 since we're not advancing time in this function.
	local iCassandraEncounters = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N")
	iCassandraEncounters = iCassandraEncounters + 1
	
	--The first set of encounters roll to afternoon from noon.
	if(iCassandraEncounters < cfFirstSet) then
		fnModifyTimeOfDay(12, iCassandraEncounters, cfFirstSet)
	
	--The second set of encounters roll to evening from afternoon.
	elseif(iCassandraEncounters < (cfSecondSet + cfFirstSet)) then
		fnModifyTimeOfDay(15, iCassandraEncounters-cfFirstSet, cfSecondSet)
	
	--The next encounters roll from evening to night.
	else
		fnModifyTimeOfDay(18, iCassandraEncounters-(cfFirstSet + cfSecondSet), cfThirdSet)
	end
	
end

