--[Graphics: Sprites]
--Sprite graphics info.
SLF_Open(gsDatafilesPath .. "Sprites.slf")

--[Common]
--Modify the distance filters to keep everything pixellated. This is only for sprites.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
ALB_SetTextureProperty("Special Sprite Padding", true)
--Bitmap_ActivateAtlasing()

--[=================================== Sprite Loader Functions ===================================]
--Loads sprites by name. Uses lists built in 000 Variables.lua, to better compartmentalize loading.
-- All characters within the same list are expected to share the same eight-directions case.
-- Thus, NPCs and Enemies need their own lists, separated from the party.
fnExtractSpriteList = function(saNameList, bIsEightDirections, sNamePrefix)
	
	--Arg check.
	if(saNameList == nil) then return end
	if(bIsEightDirections == nil) then bIsEightDirections = false end
	--sNamePrefix can be nil. It is used for characters with multiple forms.
	
	--Loader.
	local i = 1
	while(saNameList[i] ~= nil) do
		
		--The word SKIP indicates we do not rip this. It's a placeholder for characters who will eventually
		-- fill their form list in.
		if(saNameList[i] == "SKIP") then
		
		--No prefix.
		elseif(sNamePrefix == nil) then
			fnLoadCharacterGraphics(saNameList[i], "Root/Images/Sprites/" .. saNameList[i] .. "/", bIsEightDirections)
			
		--Prefix.
		else
			fnLoadCharacterGraphics(sNamePrefix .. saNameList[i], "Root/Images/Sprites/" .. sNamePrefix .. saNameList[i] .. "/", bIsEightDirections)
		end
		
		--Next.
		i = i + 1
	end
end

--Variation that uses East/West loader only.
fnExtractSpriteListEW = function(saNameList)
	
	--Arg check.
	if(saNameList == nil) then return end
	
	--Loader.
	local i = 1
	while(saNameList[i] ~= nil) do
		
		--The word SKIP indicates we do not rip this. It's a placeholder for characters who will eventually
		-- fill their form list in.
		if(saNameList[i] == "SKIP") then
		
		--Normal.
		else
			fnLoadCharacterGraphicsEW(saNameList[i], "Root/Images/Sprites/" .. saNameList[i] .. "/")
		end
		
		--Next.
		i = i + 1
	end
end

--Loads sprites in the special frame listing. For example, most player characters have  "Name_Human|Crouch" special frames.
-- This function extracts all with the same pattern from a list.
--Presently only two special frames are handled. The function can be modified to have more.
fnExtractSpecialFrames = function(saNameList, sSpecialFrameA, sSpecialFrameB)
	
	--Arg check.
	if(saNameList == nil) then return end
	if(sSpecialFrameA == nil) then return end
	--sSpecialFrameB can be nil, in which case only one special frame is loaded.
	
	--Loader.
	local i = 1
	while(saNameList[i] ~= nil) do
        
        --Ignore SKIP.
		if(saNameList[i] == "SKIP") then
		
        else
        
            --Always load frame A.
            DL_ExtractBitmap("Spcl|" .. saNameList[i] .. "|" .. sSpecialFrameA,  "Root/Images/Sprites/Special/" .. saNameList[i] .. "|" .. sSpecialFrameA)
            
            --Load frame B if provided.
            if(sSpecialFrameB ~= nil) then
                DL_ExtractBitmap("Spcl|" .. saNameList[i] .. "|" .. sSpecialFrameB,  "Root/Images/Sprites/Special/" .. saNameList[i] .. "|" .. sSpecialFrameB)
            end
		end
        
		--Next
		i = i + 1
	end
end

--[=========================================== Nowhere ===========================================]
--The six bearers always load their human sprites. Talia is missing since she's not created yet.
fnExtractSpriteList({"Human"}, true, "Mei_")
fnExtractSpriteList({"Human"}, true, "Sanya_")
fnExtractSpriteList({"Human"}, true, "Jeanne_")
fnExtractSpriteList({"Human"}, true, "Lotta_")
fnExtractSpriteList({"Human"}, true, "Christine_")

--Maram and Septima also load.
fnExtractSpriteList({"Rilmani", "Septima"}, false)

--[==================================== Misc Character Sprites ===================================]
--[Shadows]
DL_AddPath("Root/Images/Sprites/Shadows/")
DL_ExtractBitmap("Shadow|Allchars|Neutral", gsStandardShadow)

--[Mugging]
DL_AddPath("Root/Images/Sprites/Mugging/")
DL_ExtractBitmap("MugBarEmpty", "Root/Images/Sprites/Mugging/BarEmpty")
DL_ExtractBitmap("MugBarFull",  "Root/Images/Sprites/Mugging/BarFull")
DL_ExtractBitmap("MugStun0",    "Root/Images/Sprites/Mugging/Stun0")
DL_ExtractBitmap("MugStun1",    "Root/Images/Sprites/Mugging/Stun1")
DL_ExtractBitmap("MugStun2",    "Root/Images/Sprites/Mugging/Stun2")

--[Goat]
--Crucial to the game. Baa.
DL_AddPath("Root/Images/Sprites/Goat/")
DL_ExtractBitmap("Goat|0", "Root/Images/Sprites/Goat/0")
DL_ExtractBitmap("Goat|1", "Root/Images/Sprites/Goat/1")
DL_ExtractBitmap("Goat|2", "Root/Images/Sprites/Goat/2")

--[Bag Refill]
DL_AddPath("Root/Images/Sprites/BagRefill/")
DL_ExtractBitmap("BagRefill|0", "Root/Images/Sprites/BagRefill/0")
DL_ExtractBitmap("BagRefill|1", "Root/Images/Sprites/BagRefill/1")
DL_ExtractBitmap("BagRefill|2", "Root/Images/Sprites/BagRefill/2")
DL_ExtractBitmap("BagRefill|1", "Root/Images/Sprites/BagRefill/3")

--[Catalyst]
--Used for chest-opening cases.
DL_AddPath("Root/Images/Sprites/Catalyst/")

--Health.
DL_ExtractBitmap("Catalyst|Heart",          "Root/Images/Sprites/Catalyst/Heart")
DL_ExtractBitmap("Catalyst|HeartFirework0", "Root/Images/Sprites/Catalyst/HeartFirework0")
DL_ExtractBitmap("Catalyst|HeartFirework1", "Root/Images/Sprites/Catalyst/HeartFirework1")
DL_ExtractBitmap("Catalyst|HeartFirework2", "Root/Images/Sprites/Catalyst/HeartFirework2")
DL_ExtractBitmap("Catalyst|HeartFirework3", "Root/Images/Sprites/Catalyst/HeartFirework3")

--Attack Power.
DL_ExtractBitmap("Catalyst|Sword",          "Root/Images/Sprites/Catalyst/Sword")
DL_ExtractBitmap("Catalyst|SwordFirework0", "Root/Images/Sprites/Catalyst/SwordFirework0")
DL_ExtractBitmap("Catalyst|SwordFirework1", "Root/Images/Sprites/Catalyst/SwordFirework1")
DL_ExtractBitmap("Catalyst|SwordFirework2", "Root/Images/Sprites/Catalyst/SwordFirework2")
DL_ExtractBitmap("Catalyst|SwordFirework3", "Root/Images/Sprites/Catalyst/SwordFirework3")

--Accuracy.
DL_ExtractBitmap("Catalyst|Target",          "Root/Images/Sprites/Catalyst/Target")
DL_ExtractBitmap("Catalyst|TargetFirework0", "Root/Images/Sprites/Catalyst/TargetFirework0")
DL_ExtractBitmap("Catalyst|TargetFirework1", "Root/Images/Sprites/Catalyst/TargetFirework1")
DL_ExtractBitmap("Catalyst|TargetFirework2", "Root/Images/Sprites/Catalyst/TargetFirework2")
DL_ExtractBitmap("Catalyst|TargetFirework3", "Root/Images/Sprites/Catalyst/TargetFirework3")

--Initiative.
DL_ExtractBitmap("Catalyst|Boot",          "Root/Images/Sprites/Catalyst/Boot")
DL_ExtractBitmap("Catalyst|BootFirework0", "Root/Images/Sprites/Catalyst/BootFirework0")
DL_ExtractBitmap("Catalyst|BootFirework1", "Root/Images/Sprites/Catalyst/BootFirework1")
DL_ExtractBitmap("Catalyst|BootFirework2", "Root/Images/Sprites/Catalyst/BootFirework2")
DL_ExtractBitmap("Catalyst|BootFirework3", "Root/Images/Sprites/Catalyst/BootFirework3")

--Evasion.
DL_ExtractBitmap("Catalyst|Dodge",          "Root/Images/Sprites/Catalyst/Dodge")
DL_ExtractBitmap("Catalyst|DodgeFirework0", "Root/Images/Sprites/Catalyst/DodgeFirework0")
DL_ExtractBitmap("Catalyst|DodgeFirework1", "Root/Images/Sprites/Catalyst/DodgeFirework1")
DL_ExtractBitmap("Catalyst|DodgeFirework2", "Root/Images/Sprites/Catalyst/DodgeFirework2")
DL_ExtractBitmap("Catalyst|DodgeFirework3", "Root/Images/Sprites/Catalyst/DodgeFirework3")

--Evasion.
DL_ExtractBitmap("Catalyst|Skill",          "Root/Images/Sprites/Catalyst/Skill")
DL_ExtractBitmap("Catalyst|SkillFirework0", "Root/Images/Sprites/Catalyst/SkillFirework0")
DL_ExtractBitmap("Catalyst|SkillFirework1", "Root/Images/Sprites/Catalyst/SkillFirework1")
DL_ExtractBitmap("Catalyst|SkillFirework2", "Root/Images/Sprites/Catalyst/SkillFirework2")
DL_ExtractBitmap("Catalyst|SkillFirework3", "Root/Images/Sprites/Catalyst/SkillFirework3")

--Clear this flag.
ALB_SetTextureProperty("Special Sprite Padding", false)

--[======================================== World Objects ========================================]
--[Objects]
--Setup
DL_AddPath("Root/Images/Sprites/Objects/")

--Doors
DL_ExtractBitmap("Obj|DoorNS",          "Root/Images/Sprites/Objects/DoorNS")
DL_ExtractBitmap("Obj|DoorEW",          "Root/Images/Sprites/Objects/DoorEW")
DL_ExtractBitmap("Obj|DoorNSDungeonC",  "Root/Images/Sprites/Objects/DoorNSDungeonC")
DL_ExtractBitmap("Obj|DoorNSDungeonO",  "Root/Images/Sprites/Objects/DoorNSDungeonO")
DL_ExtractBitmap("Obj|DoorNSSilverC",   "Root/Images/Sprites/Objects/DoorNSSilverC")
DL_ExtractBitmap("Obj|DoorNSSilverO",   "Root/Images/Sprites/Objects/DoorNSSilverO")
DL_ExtractBitmap("Obj|DoorNSSpooky",    "Root/Images/Sprites/Objects/DoorNSSpooky")
DL_ExtractBitmap("Obj|DoorNSDungeonCV", "Root/Images/Sprites/Objects/DoorNSDungeonCV")
DL_ExtractBitmap("Obj|DoorNSDungeonOV", "Root/Images/Sprites/Objects/DoorNSDungeonOV")
DL_ExtractBitmap("Obj|DoorNSRegulusC",  "Root/Images/Sprites/Objects/DoorNSRegulusC")
DL_ExtractBitmap("Obj|DoorNSRegulusO",  "Root/Images/Sprites/Objects/DoorNSRegulusO")
DL_ExtractBitmap("Obj|DoorNSRegulusWC", "Root/Images/Sprites/Objects/DoorNSRegulusWC")
DL_ExtractBitmap("Obj|DoorNSRegulusWO", "Root/Images/Sprites/Objects/DoorNSRegulusWO")
DL_ExtractBitmap("Obj|DoorEWRegulusC",  "Root/Images/Sprites/Objects/DoorEWRegulusC")
DL_ExtractBitmap("Obj|DoorEWRegulusFO", "Root/Images/Sprites/Objects/DoorEWRegulusFO")
DL_ExtractBitmap("Obj|DoorEWRegulusFC", "Root/Images/Sprites/Objects/DoorEWRegulusFC")

--Other objects.
DL_ExtractBitmap("Obj|PDU",     "Root/Images/Sprites/Objects/PDU")
DL_ExtractBitmap("Obj|Lantern", "Root/Images/Sprites/Objects/Lantern")

--Save point
DL_ExtractBitmap("Obj|FireUnlit", "Root/Images/Sprites/Objects/FireUnlit")
DL_ExtractBitmap("Obj|FireLit0",  "Root/Images/Sprites/Objects/FireLit0")
DL_ExtractBitmap("Obj|FireLit1",  "Root/Images/Sprites/Objects/FireLit1")
DL_ExtractBitmap("Obj|CoilUnlit", "Root/Images/Sprites/Objects/CoilUnlit")
DL_ExtractBitmap("Obj|CoilLit0",  "Root/Images/Sprites/Objects/CoilLit0")
DL_ExtractBitmap("Obj|CoilLit1",  "Root/Images/Sprites/Objects/CoilLit1")
DL_ExtractBitmap("Obj|BenchWood", "Root/Images/Sprites/Objects/BenchWood")
DL_ExtractBitmap("Obj|BenchMetal","Root/Images/Sprites/Objects/BenchMetal")

--Stairs
DL_ExtractBitmap("Obj|StairUR", "Root/Images/Sprites/Objects/StairUR")
DL_ExtractBitmap("Obj|StairUL", "Root/Images/Sprites/Objects/StairUL")
DL_ExtractBitmap("Obj|StairDR", "Root/Images/Sprites/Objects/StairDR")
DL_ExtractBitmap("Obj|StairDL", "Root/Images/Sprites/Objects/StairDL")

--Chests loaded with goodies
DL_ExtractBitmap("Obj|ChestC",   "Root/Images/Sprites/Objects/ChestC")
DL_ExtractBitmap("Obj|ChestO",   "Root/Images/Sprites/Objects/ChestO")
DL_ExtractBitmap("Obj|ChestBC",  "Root/Images/Sprites/Objects/ChestBC")
DL_ExtractBitmap("Obj|ChestBO",  "Root/Images/Sprites/Objects/ChestBO")
DL_ExtractBitmap("Obj|ChestFC",  "Root/Images/Sprites/Objects/ChestFC")
DL_ExtractBitmap("Obj|ChestFO",  "Root/Images/Sprites/Objects/ChestFO")
DL_ExtractBitmap("Obj|ChestFBC", "Root/Images/Sprites/Objects/ChestFBC")
DL_ExtractBitmap("Obj|ChestFBO", "Root/Images/Sprites/Objects/ChestFBO")

--Switches (loaded with goodies?)
DL_ExtractBitmap("Obj|SwitchUp", "Root/Images/Sprites/Objects/SwitchUp")
DL_ExtractBitmap("Obj|SwitchDn", "Root/Images/Sprites/Objects/SwitchDn")

--Ladders, Ropes, Etc
DL_ExtractBitmap("Obj|LadderTop",  "Root/Images/Sprites/Objects/LadderTop")
DL_ExtractBitmap("Obj|LadderMid",  "Root/Images/Sprites/Objects/LadderMid")
DL_ExtractBitmap("Obj|LadderBot",  "Root/Images/Sprites/Objects/LadderBot")
DL_ExtractBitmap("Obj|RopeTop",    "Root/Images/Sprites/Objects/RopeTop")
DL_ExtractBitmap("Obj|RopeMid",    "Root/Images/Sprites/Objects/RopeMid")
DL_ExtractBitmap("Obj|RopeBot",    "Root/Images/Sprites/Objects/RopeBot")
DL_ExtractBitmap("Obj|RopeAnchor", "Root/Images/Sprites/Objects/RopeAnchor")

--[Enemy Extras]
DL_AddPath("Root/Images/Sprites/Spotted/")
DL_ExtractBitmap("Spotted|Exclamation", "Root/Images/Sprites/Spotted/Exclamation")
DL_ExtractBitmap("Spotted|Question",    "Root/Images/Sprites/Spotted/Question")

--[Christine's Quarters]
--These are objects that can be placed in Christine's quarters in Chapter 5. They are NPC-format.
DL_AddPath("Root/Images/Sprites/Quarters/")
DL_ExtractBitmap("AptObj|Tube0",    "Root/Images/Sprites/Quarters/Tube0")
DL_ExtractBitmap("AptObj|Tube1",    "Root/Images/Sprites/Quarters/Tube1")
DL_ExtractBitmap("AptObj|Tube2",    "Root/Images/Sprites/Quarters/Tube2")
DL_ExtractBitmap("AptObj|Tube3",    "Root/Images/Sprites/Quarters/Tube3")
DL_ExtractBitmap("AptObj|TubeLD",   "Root/Images/Sprites/Quarters/TubeLD")
DL_ExtractBitmap("AptObj|Tube55",   "Root/Images/Sprites/Quarters/Tube55")
DL_ExtractBitmap("AptObj|CounterL", "Root/Images/Sprites/Quarters/CounterL")
DL_ExtractBitmap("AptObj|CounterM", "Root/Images/Sprites/Quarters/CounterM")
DL_ExtractBitmap("AptObj|CounterR", "Root/Images/Sprites/Quarters/CounterR")
DL_ExtractBitmap("AptObj|Coffee",   "Root/Images/Sprites/Quarters/Coffee")
DL_ExtractBitmap("AptObj|TV",       "Root/Images/Sprites/Quarters/TV")
DL_ExtractBitmap("AptObj|ChairS",   "Root/Images/Sprites/Quarters/ChairS")
DL_ExtractBitmap("AptObj|CouchSL",  "Root/Images/Sprites/Quarters/CouchSL")
DL_ExtractBitmap("AptObj|CouchSM",  "Root/Images/Sprites/Quarters/CouchSM")
DL_ExtractBitmap("AptObj|CouchSR",  "Root/Images/Sprites/Quarters/CouchSR")
DL_ExtractBitmap("AptObj|ChairN",   "Root/Images/Sprites/Quarters/ChairN")
DL_ExtractBitmap("AptObj|CouchNL",  "Root/Images/Sprites/Quarters/CouchNL")
DL_ExtractBitmap("AptObj|CouchNM",  "Root/Images/Sprites/Quarters/CouchNM")
DL_ExtractBitmap("AptObj|CouchNR",  "Root/Images/Sprites/Quarters/CouchNR")

--[======================================= Random Level Textures ========================================]
--These are stored in Sprites.slf
DL_AddPath("Root/Images/Sprites/LevelTextures/")
DL_ExtractBitmap("Level|RegulusCave",       "Root/Images/Sprites/LevelTextures/RegulusCave")
DL_ExtractBitmap("Level|RegulusCaveFungus", "Root/Images/Sprites/LevelTextures/RegulusCaveFungus")

--[=================================== Animations and UI Pieces ==================================]
--[Death]
--Enemies use the poof frames when they disappear from the game world.
DL_AddPath("Root/Images/Sprites/EnemyPoof/")
DL_ExtractBitmap("Obj|Poof0", "Root/Images/Sprites/EnemyPoof/0")
DL_ExtractBitmap("Obj|Poof1", "Root/Images/Sprites/EnemyPoof/1")
DL_ExtractBitmap("Obj|Poof2", "Root/Images/Sprites/EnemyPoof/2")
DL_ExtractBitmap("Obj|Poof3", "Root/Images/Sprites/EnemyPoof/3")

--[Combat Animations]
--These are done after atlasing is disabled since they are large, transparent, and tend to slow down
-- the atlas loader.
SLF_Open(gsDatafilesPath .. "CombatAnimations.slf")

--Loader Function
local fnLoadAnimationEntry = function(psInfileName, psLibraryName, piCount)
    
    --Add a path.
	DL_AddPath("Root/Images/CombatAnimations/" .. psLibraryName .. "/")
    
    --Crossload the images.
	for p = 0, piCount-1, 1 do
		local sName = string.format("ComFX|%s%02i", psInfileName, p)
		local sIndex = string.format("%02i", p)
		DL_ExtractBitmap(sName, "Root/Images/CombatAnimations/" .. psLibraryName .. "/" .. sIndex)
	end
end

--Load Entries
fnLoadAnimationEntry("Bleed",       "Bleed",       19)
fnLoadAnimationEntry("Blind",       "Blind",       10)
fnLoadAnimationEntry("Buff",        "Buff",        24)
fnLoadAnimationEntry("Corrode",     "Corrode",     41)
fnLoadAnimationEntry("Debuff",      "Debuff",      24)
fnLoadAnimationEntry("Electricity", "Electricity", 24)
fnLoadAnimationEntry("Fear",        "Fear",        30)
fnLoadAnimationEntry("Fire",        "Fire",        40)
fnLoadAnimationEntry("GunShot",     "GunShot",     20)
fnLoadAnimationEntry("Haste",       "Haste",       50)
fnLoadAnimationEntry("Healing",     "Healing",     24)
fnLoadAnimationEntry("Ice",         "Ice",         40)
fnLoadAnimationEntry("LaserShot",   "LaserShot",   20)
fnLoadAnimationEntry("Light",       "Light",       30)
fnLoadAnimationEntry("Pierce",      "Pierce",      24)
fnLoadAnimationEntry("Poison",      "Poison",      50)
fnLoadAnimationEntry("ShadowA",     "ShadowA",     40)
fnLoadAnimationEntry("ShadowB",     "ShadowB",     50)
fnLoadAnimationEntry("ShadowC",     "ShadowC",     38)
fnLoadAnimationEntry("Slash1",      "SlashClaw",   24)
fnLoadAnimationEntry("Slash2",      "SlashCross",  24)
fnLoadAnimationEntry("Slow",        "Slow",        70)
fnLoadAnimationEntry("Strike",      "Strike",      10)

--[Clean]
--Bitmap_DeactivateAtlasing()
ALB_SetTextureProperty("Restore Defaults")
SLF_Close()
