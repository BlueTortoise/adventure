--[Graphics: Portraits]
--Portraits, mostly used for combat and dialogue. Also appear in TF scenes sometimes.

--[ ===================================== Loading Subroutine ==================================== ]
--Used for the automated list sections.
fnExtractList = function(sPrefix, sSuffix, sDLPath, saList)
	
	--Arg check.
	if(sPrefix == nil) then return end
	if(sSuffix == nil) then return end
	if(sDLPath == nil) then return end
	if(saList  == nil) then return end
	
	--Run across the list and extract everything.
	local i = 1
	while(saList[i] ~= nil) do
		
		--If the name is "SKIP", ignore it. This means it will be filled in later.
		if(saList[i] == "SKIP") then
		
		--Extract the image.
		elseif(sCharacterPrefix == nil) then
			DL_ExtractBitmap(sPrefix .. saList[i] .. sSuffix, sDLPath .. saList[i])
		end
		
		--Next.
		i = i + 1
	end
end

--[ ============================================== Dialogue Portraits =============================================== ]
--[Nowhere Portraits]
--All six bearers, as well as Maram and Septima, always load. Only the human variants are used.
SLF_Open(gsaDatafilePaths.sMeiPath)
DL_AddPath("Root/Images/Portraits/MeiDialogue/")
DL_AddPath("Root/Images/Portraits/Combat/")
DL_ExtractBitmap("Por|Mei|Neutral", "Root/Images/Portraits/MeiDialogue/Neutral")
DL_ExtractBitmap("Party|Mei_Human", "Root/Images/Portraits/Combat/Mei_Human")

SLF_Open(gsaDatafilePaths.sChristinePath)
DL_AddPath("Root/Images/Portraits/ChristineDialogue/")
DL_ExtractBitmap("Por|Christine|Neutral",  "Root/Images/Portraits/ChristineDialogue/Neutral")
DL_ExtractBitmap("Party|Christine_Human", "Root/Images/Portraits/Combat/Christine_Human")

--Rilmani.
SLF_Open(gsaDatafilePaths.sSeptimaPath)
DL_AddPath("Root/Images/Portraits/Septima/")
DL_ExtractBitmap("Por|Septima|Neutral",   "Root/Images/Portraits/Septima/Neutral")
DL_ExtractBitmap("Por|Septima|NeutralUp", "Root/Images/Portraits/Septima/NeutralUp")

SLF_Open(gsaDatafilePaths.sMaramPath)
DL_AddPath("Root/Images/Portraits/Maram/")
DL_ExtractBitmap("Por|Maram|Neutral", "Root/Images/Portraits/Maram/Neutral")

--Combat portraits for nowhere characters.
DL_ExtractBitmap("Party|Mei_Human",       "Root/Images/Portraits/Combat/Mei_Human")
DL_ExtractBitmap("Party|Christine_Human", "Root/Images/Portraits/Combat/Christine_Human")

--[Cassandra's Portraits]
--Cassandra appears in multiple chapters, and always loads.
SLF_Open(gsaDatafilePaths.sCassandraPath)
DL_AddPath("Root/Images/Portraits/CassandraDialogue/")
DL_ExtractBitmap("Por|Cassandra|Neutral", "Root/Images/Portraits/CassandraDialogue/Human")
DL_ExtractBitmap("Por|Cassandra|Werecat", "Root/Images/Portraits/CassandraDialogue/Werecat")
DL_ExtractBitmap("Por|Cassandra|Golem",   "Root/Images/Portraits/CassandraDialogue/Golem")

-- |[ ================================== List-Load Portraits =================================== ]|
--[Dialogue Portraits]
--Portraits used when an enemy type speaks to the party.
local saChapter1DiaEnemyList = {"Alraune", "Bee", "CultistF", "CultistM", "MaidGhost", "Slime", "Werecat", "Zombee", "SlimeG", "SlimeB", "CultistFRubber"}
local saChapter5DiaEnemyList = {"Doll", "Electrosprite", "LatexDrone", "Raiju", "DarkmatterGirl", "SteamDroid", "Dreamer", "Vivify", "DollInfluenced", "201890"}
local saChapter5FancyGolemList   = {"GolemFancyA", "GolemFancyB", "GolemFancyC", "GolemFancyD", "GolemFancyE", "GolemFancyF"}

--Uses the auto-loader. These always load.
if(gbAdventureBootDebug) then io.write("   Enemies\n") end
DL_AddPath("Root/Images/Portraits/Enemies/")
SLF_Open(gsaDatafilePaths.sChapter1EmotePath)
fnExtractList("Por|", "|Neutral", "Root/Images/Portraits/Enemies/", saChapter1DiaEnemyList)

SLF_Open(gsaDatafilePaths.sChapter5EmotePath)
fnExtractList("Por|", "|Neutral", "Root/Images/Portraits/Enemies/", saChapter5DiaEnemyList)
fnExtractList("Por|", "|Neutral", "Root/Images/Portraits/Enemies/", saChapter5FancyGolemList)

--NPCs and the like. These are common to all chapters.
if(gbAdventureBootDebug) then io.write("   NPCs.\n") end
SLF_Open(gsaDatafilePaths.sChapter0EmotePath)
DL_AddPath("Root/Images/Portraits/NPCs/")
DL_ExtractBitmap("Por|HumanNPCF0|Neutral",  "Root/Images/Portraits/NPCs/HumanNPCF0")
DL_ExtractBitmap("Por|HumanNPCF0B|Neutral", "Root/Images/Portraits/NPCs/HumanNPCF0B") --Boobalicious version
DL_ExtractBitmap("Por|HumanNPCF1|Neutral",  "Root/Images/Portraits/NPCs/HumanNPCF1")
DL_ExtractBitmap("Por|HumanNPCF1B|Neutral", "Root/Images/Portraits/NPCs/HumanNPCF1B") --Boobalicious version
DL_ExtractBitmap("Por|MercF|Neutral",       "Root/Images/Portraits/NPCs/MercF")
DL_ExtractBitmap("Por|MercM|Neutral",       "Root/Images/Portraits/NPCs/MercM")

--[ ===================================================== Actors ==================================================== ]
--[Functions]
--Creates a dialogue actor with the listed aliases and a listed set of emotions.
-- All created actors use their own names as an alias, and always have a neutral emotion.
-- sPathPattern is formatted: "Root/Images/Portraits/MeiDialogue/" or thereabouts. Include the final slash.
fnCreateDialogueActor = function(sActorName, sVoiceSample, sPathPattern, bUsesBigPortraits, saAliasList, saEmotionList)
    
    --Arg Check
    if(sActorName == nil) then return end
    if(sVoiceSample == nil) then return end
    if(sPathPattern == nil) then return end
    if(bUsesBigPortraits == nil) then return end
    if(saAliasList == nil) then return end
    if(saEmotionList == nil) then return end
    
    --Create.
    DialogueActor_Create(sActorName)

        --Alias listing. Always create the same alias as the character's name.
        DialogueActor_SetProperty("Add Alias", sActorName)

        --Create additional aliases if the list contains any entries.
        local i = 1
        while(saAliasList[i] ~= nil and saAliasList[i] ~= "NOALIAS") do
            DialogueActor_SetProperty("Add Alias", saAliasList[i])
            i = i + 1
        end

        --Create the neutral emotion. All entities have this. If the saEmotionList contains no entries, then
        -- the neutral emotion is the sPathPattern.
        if(saEmotionList[1] == nil or saEmotionList[1] == "NEUTRALISPATH") then
            DialogueActor_SetProperty("Add Emotion", "Neutral", sPathPattern, bUsesBigPortraits)
        
        --Otherwise, the emotion listing follows the pattern provided.
        else
        
            --The first emotion is "Neutral". This is the case even if that's the only emotion, if you want to use the path pattern.
            DialogueActor_SetProperty("Add Emotion", "Neutral", sPathPattern .. "Neutral", bUsesBigPortraits)
        
            --Now iterate across the rest of the list.
            i = 2
            while(saEmotionList[i] ~= nil) do
                DialogueActor_SetProperty("Add Emotion", saEmotionList[i], sPathPattern .. saEmotionList[i], bUsesBigPortraits)
                i = i + 1
            end
        
        end

    --Clean up.
    DL_PopActiveObject()

    --Set the voice sample.
    WD_SetProperty("Register Voice", sActorName, sVoiceSample)
    
end

--Runebearers always have a dialogue actor with human/neutral. This is for chapter selection.
fnCreateDialogueActor("Mei",       "Voice|Mei",       "Root/Images/Portraits/MeiDialogue/",       true, {"NOALIAS"}, {"Neutral"})
fnCreateDialogueActor("Christine", "Voice|Christine", "Root/Images/Portraits/ChristineDialogue/", true, {"NOALIAS"}, {"Neutral"})

--Rilmani. Same as the bearers.
fnCreateDialogueActor("Septima", "Voice|Septima", "Root/Images/Portraits/Septima/", true, {"Rilmani"}, {"Neutral", "NeutralUp"})
fnCreateDialogueActor("Maram",   "Voice|Maram",   "Root/Images/Portraits/Maram/",   true, {"Rilmani"}, {"Neutral"})

--Common actors:
fnCreateDialogueActor("HumanF0",  "Voice|GenericF03", "Root/Images/Portraits/NPCs/HumanNPCF0",  true,  {"Human"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanF0B", "Voice|GenericF03", "Root/Images/Portraits/NPCs/HumanNPCF0B", true,  {"Human"}, {"NEUTRALISPATH"}) --Boobalicious version
fnCreateDialogueActor("HumanF1",  "Voice|GenericF02", "Root/Images/Portraits/NPCs/HumanNPCF1",  true,  {"Human"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanF1B", "Voice|GenericF02", "Root/Images/Portraits/NPCs/HumanNPCF1B", true,  {"Human"}, {"NEUTRALISPATH"}) --Boobalicious version
fnCreateDialogueActor("HumanM0",  "Voice|GenericM01", "Root/Images/Portraits/NPCs/HumanNPCM0",  true,  {"Human"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanM1",  "Voice|GenericM00", "Root/Images/Portraits/NPCs/HumanNPCM1",  true,  {"Human"}, {"NEUTRALISPATH"})

--All other dialogue actors are chapter-specific.

