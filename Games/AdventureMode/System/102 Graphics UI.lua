--[Graphics: UI]
--User interface parts, such as combat and level-up. Also contains a lot of portrait pieces that are used in
-- the UI, such as the turn-order portraits.
SLF_Open(gsDatafilesPath .. "UIAdventure.slf")

--[ ===================================== Loading Subroutine ==================================== ]
--Used for the automated list sections. These are portraits, mostly.
local fnExtractList = function(sPrefix, sDLPath, saList, sCharacterPrefix)
	
	--Arg check.
	if(sPrefix == nil) then return end
	if(sDLPath == nil) then return end
	if(saList  == nil) then return end
	
	--sCharacterPrefix can be nil. It is only used for characters with multiple forms.
	
	--Run across the list and extract everything.
	local i = 1
	while(saList[i] ~= nil) do
		
		--If the name is "SKIP", ignore it. This means it will be filled in later.
		if(saList[i] == "SKIP") then
		
		--No character prefix:
		elseif(sCharacterPrefix == nil) then
			DL_ExtractBitmap(sPrefix .. saList[i], sDLPath .. saList[i])
			
		--Character prefix is present:
		else
			DL_ExtractBitmap(sPrefix .. sCharacterPrefix .. saList[i], sDLPath .. sCharacterPrefix .. saList[i])
		end
		
		--Next.
		i = i + 1
	end
end

--[ ========================================== Game UI ========================================== ]
--Stamina Bar
DL_AddPath("Root/Images/AdventureUI/Stamina/")
DL_ExtractBitmap("AdvStamina|StaminaBarOver",   "Root/Images/AdventureUI/Stamina/BarOver")
DL_ExtractBitmap("AdvStamina|StaminaBarMiddle", "Root/Images/AdventureUI/Stamina/BarMiddle")
DL_ExtractBitmap("AdvStamina|StaminaBarUnder",  "Root/Images/AdventureUI/Stamina/BarUnder")

--[ =========================================== Icons =========================================== ]
--Item Icons
DL_AddPath("Root/Images/AdventureUI/ItemIcons/")
DL_ExtractBitmap("AdvItemIco|WeaponMei",          "Root/Images/AdventureUI/ItemIcons/WeaponMei")
DL_ExtractBitmap("AdvItemIco|RunestoneMei",       "Root/Images/AdventureUI/ItemIcons/RunestoneMei")
DL_ExtractBitmap("AdvItemIco|WeaponFlorentina",   "Root/Images/AdventureUI/ItemIcons/WeaponFlorentina")
DL_ExtractBitmap("AdvItemIco|WeaponChristine",    "Root/Images/AdventureUI/ItemIcons/WeaponChristine")
DL_ExtractBitmap("AdvItemIco|RunestoneChristine", "Root/Images/AdventureUI/ItemIcons/RunestoneChristine")
DL_ExtractBitmap("AdvItemIco|Weapon55",           "Root/Images/AdventureUI/ItemIcons/Weapon55")
DL_ExtractBitmap("AdvItemIco|WeaponJX101",        "Root/Images/AdventureUI/ItemIcons/WeaponJX101")
DL_ExtractBitmap("AdvItemIco|WeaponSX399",        "Root/Images/AdventureUI/ItemIcons/WeaponSX399")
DL_ExtractBitmap("AdvItemIco|ArmorLight",         "Root/Images/AdventureUI/ItemIcons/ArmorLight")
DL_ExtractBitmap("AdvItemIco|ArmorMedium",        "Root/Images/AdventureUI/ItemIcons/ArmorMedium")
DL_ExtractBitmap("AdvItemIco|AccessoryRing",      "Root/Images/AdventureUI/ItemIcons/AccessoryRing")
DL_ExtractBitmap("AdvItemIco|AccessoryBracer",    "Root/Images/AdventureUI/ItemIcons/AccessoryBracer")
DL_ExtractBitmap("AdvItemIco|ItemPotionRed",      "Root/Images/AdventureUI/ItemIcons/ItemPotionRed")
DL_ExtractBitmap("AdvItemIco|ItemPotionBlue",     "Root/Images/AdventureUI/ItemIcons/ItemPotionBlue")
DL_ExtractBitmap("AdvItemIco|ItemPotionViolet",   "Root/Images/AdventureUI/ItemIcons/ItemPotionViolet")
DL_ExtractBitmap("AdvItemIco|ItemPotionGreen",    "Root/Images/AdventureUI/ItemIcons/ItemPotionGreen")
DL_ExtractBitmap("AdvItemIco|ItemInjector",       "Root/Images/AdventureUI/ItemIcons/ItemInjector")
DL_ExtractBitmap("AdvItemIco|ItemHacking",        "Root/Images/AdventureUI/ItemIcons/ItemHacking")
DL_ExtractBitmap("AdvItemIco|AdamantitePowder",   "Root/Images/AdventureUI/ItemIcons/AdamantitePowder")
DL_ExtractBitmap("AdvItemIco|Unequip",            "Root/Images/AdventureUI/ItemIcons/Unequip")
DL_ExtractBitmap("AdvItemIco|JunkFantasy",        "Root/Images/AdventureUI/ItemIcons/JunkFantasy")
DL_ExtractBitmap("AdvItemIco|JunkFuture",         "Root/Images/AdventureUI/ItemIcons/JunkFuture")

--Character Faces. Placed in the ItemIcons set because they are used on equipment.
DL_ExtractBitmap("AdvItemIco|CharacterFaces", "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

--Gems
DL_ExtractBitmap("AdvItemIco|GemYellow", "Root/Images/AdventureUI/ItemIcons/GemYellow")
DL_ExtractBitmap("AdvItemIco|GemPurple", "Root/Images/AdventureUI/ItemIcons/GemPurple")
DL_ExtractBitmap("AdvItemIco|GemBlue",   "Root/Images/AdventureUI/ItemIcons/GemBlue")
DL_ExtractBitmap("AdvItemIco|GemPink",   "Root/Images/AdventureUI/ItemIcons/GemPink")

--Item Backings
DL_ExtractBitmap("AdvItemIco|BackingNeutral", "Root/Images/AdventureUI/ItemIcons/BackingNeutral")

--[ ========================================= Abilities ========================================= ]
--System
DL_AddPath("Root/Images/AdventureUI/Abilities/")
DL_ExtractBitmap("AdvAbilityIco|Select", "Root/Images/AdventureUI/Abilities/Select")

--[ ========================================== Overlays ========================================= ]
--Overlays use linear filtering.
local iLinear = DM_GetEnumeration("GL_LINEAR")
ALB_SetTextureProperty("MagFilter", iLinear)
ALB_SetTextureProperty("MinFilter", iLinear)

--Overlays need to wrap.
local iRepeatEnum = DM_GetEnumeration("GL_REPEAT")
ALB_SetTextureProperty("Wrap S", iRepeatEnum)
ALB_SetTextureProperty("Wrap T", iRepeatEnum)

--Forest overlay
DL_AddPath("Root/Images/AdventureUI/MapOverlays/")
DL_ExtractBitmap("Overlay|Fog",        "Root/Images/AdventureUI/MapOverlays/Fog")
DL_ExtractBitmap("Overlay|ForestA",    "Root/Images/AdventureUI/MapOverlays/ForestA")
DL_ExtractBitmap("Overlay|ForestB",    "Root/Images/AdventureUI/MapOverlays/ForestB")
DL_ExtractBitmap("Overlay|ForestC",    "Root/Images/AdventureUI/MapOverlays/ForestC")
DL_ExtractBitmap("Overlay|Water",      "Root/Images/AdventureUI/MapOverlays/Water")
DL_ExtractBitmap("Overlay|Underwater", "Root/Images/AdventureUI/MapOverlays/Underwater")

--Return to normal.
ALB_SetTextureProperty("Restore Defaults")

--[ ======================================== CocoMint UI ======================================== ]
--[Extraction Function]
local fnExtract = function(psaNames, psPrefix, psDLPath)
	
	--Arg check.
	if(psaNames == nil) then return end
	if(psPrefix == nil) then return end
	if(psDLPath == nil) then return end
	
	--Path.
	DL_AddPath(psDLPath)
	
	--Extraction loop.
	local i = 1
	while(psaNames[i] ~= nil) do
		DL_ExtractBitmap(psPrefix .. psaNames[i], psDLPath .. psaNames[i])
		i = i + 1
	end
end

--[Lookup Tables]
local zaLookups = {}
zaLookups[ 1] = {"AdvCampfire|",  "Root/Images/AdventureUI/CampfireMenu/", {"BaseBot", "BaseMid", "BaseTop", "Button", "Comparison", "DescriptionBox", "Header", "MapPin", "MapPinSelected"}}
zaLookups[ 2] = {"AdvDialogue|",  "Root/Images/AdventureUI/Dialogue/",     {"BorderCard", "CommandList", "ExpandArrow", "NameBox", "NamelessBox", "NamePanel", "TextInput"}}
zaLookups[ 3] = {"AdvDoctor|",    "Root/Images/AdventureUI/DoctorBag/",    {"BackButton", "BarFill", "BarFrame", "BarHeader", "HealAllBtn", "MenuBack", "MenuHeaderA", "MenuHeaderB", "MenuInset", "OptionsDeactivated", "PortraitBtn", "PortraitMask"}}
zaLookups[ 4] = {"AdvEquipment|", "Root/Images/AdventureUI/Equipment/",    {"Frames", "FramesScroll", "NamePlate", "PortraitMaskA", "PortraitMaskB", "PortraitMaskC", "SocketA", "SocketB", "SocketC", "SocketScroll"}}
zaLookups[ 5] = {"AdvGemCutter|", "Root/Images/AdventureUI/GemCutter/",    {"Banner", "Base", "ButtonBack", "GemSelectBack", "GemSelectFrame", "InfoBox", "PlatinaBanner"}}
zaLookups[ 6] = {"AdvInventory|", "Root/Images/AdventureUI/Inventory/",    {"Frames", "ScrollbarBack", "ScrollbarFront"}}
zaLookups[ 7] = {"AdvOptions|",   "Root/Images/AdventureUI/Options/",      {"Bars", "ConfirmButtons", "Header", "MenuBack", "MenuInsert", "SliderButtons", "Sliders", "TextBoxes"}}
zaLookups[ 8] = {"AdvSaveMenu|",  "Root/Images/AdventureUI/SaveMenu/",     {"ArrowLft", "ArrowRgt", "Header", "NewFileButton", "GridBackingCh0"}}
zaLookups[ 9] = {"AdvStatus|",    "Root/Images/AdventureUI/Status/",       {"AbilityInspector", "BackgroundFill", "BannerTop", "BtnBack", "CharInfo", "CharOverlayMask", "CharOverlayMaskLft", "CharOverlayMaskRgt", "EXPBarFill", "EXPBarFrame", "HealthBarFill", "HealthBarFrame", "HealthBarUnder", "Inventory", "NavButtons"}}
zaLookups[10] = {"AdvVendor|",    "Root/Images/AdventureUI/Vendor/",       {"CompareCursor", "ConfirmOverlay", "FramesBuy", "FramesSell", "FramesBuyback", "FramesGemcutter", "ScrollbarBack", "ScrollbarFront"}}
zaLookups[11] = {"AdvCostume|",    "Root/Images/AdventureUI/Costume/",     {"FrameLower", "FrameLowerActive", "FrameUpper", "Header", "SelectorLower", "SelectorUpper"}}

--[22px Item Icons]
local i = 12
zaLookups[i] = {"AdvItem22Px|", "Root/Images/AdventureUI/Symbols22/", {"WepMeiA", "WepMeiB", "WepMeiC", "WepFlorentinaA", "WepFlorentinaB", "WepFlorentinaC"}}
fnAppendTables(zaLookups[i][3], {"WepChristineA", "WepChristineB", "WepChristineC", "Wep55A", "Wep55B", "Wep55C", "WepJX101", "WepSX399A", "WepSX399B", "WepSX399C"})
fnAppendTables(zaLookups[i][3], {"RuneMei", "RuneChristine", "SystemUnequip"})
fnAppendTables(zaLookups[i][3], {"ArmGenLight", "ArmGenMedium", "ArmGenHeavy"})
fnAppendTables(zaLookups[i][3], {"AccRing", "AccBracer", "AccBoots", "AccGloves", "AccRingBrnA", "AccRingBrnB", "AccRingBrnC", "AccRingBrnD"})
fnAppendTables(zaLookups[i][3], {"ItemPotionRed", "ItemMedkit", "AccRingSlvA", "AccRingSlvB", "AccRingSlvC", "AccRingSlvD"})
fnAppendTables(zaLookups[i][3], {"ItemInjector", "ItemTech", "ItemScope", "ItemSuppressor", "ItemFlashbang", "AccRingGldA", "AccRingGldB", "AccRingGldC", "AccRingGldD"})
fnAppendTables(zaLookups[i][3], {"GemSlot", "GemYellow", "GemPurple", "GemBlue", "GemPink"})
fnAppendTables(zaLookups[i][3], {"JunkA", "JunkB"})
fnAppendTables(zaLookups[i][3], {"PepperPie"})
fnAppendTables(zaLookups[i][3], {"Crowbar", "KeyBlack", "KeySilver", "KeyBronze", "Hacksaw", "BoatOars", "ComputerChip"})
fnAppendTables(zaLookups[i][3], {"BookPurple", "BookBlue", "BookRed", "BookOrange", "BookGreen"})
fnAppendTables(zaLookups[i][3], {"JarGreen", "JarYellow", "JarRed", "JarPurple", "JarBlue"})
fnAppendTables(zaLookups[i][3], {"FlowerRed", "FlowerWhite", "FlowerPurple", "FlowerBlue", "FlowerPink"})
fnAppendTables(zaLookups[i][3], {"AdmPowder", "AdmFlakes", "AdmShards", "AdmPieces", "AdmChunks", "AdmOre"})
fnAppendTables(zaLookups[i][3], {"Gem1|R",   "Gem1|V",   "Gem1|Y",   "Gem1|O",   "Gem1|G",   "Gem1|B"})
fnAppendTables(zaLookups[i][3], {"Gem2|RV",  "Gem2|VY",  "Gem2|OV",  "Gem2|GV",  "Gem2|BV",  "Gem2|RY",  "Gem2|RO",  "Gem2|GR",  "Gem2|RB",  "Gem2|OY",  "Gem2|GY",  "Gem2|BY",  "Gem2|GO",  "Gem2|BO",  "Gem2|GB"})
fnAppendTables(zaLookups[i][3], {"Gem3|RVY", "Gem3|ROV", "Gem3|GRV", "Gem3|RBV", "Gem3|OVY", "Gem3|GVY", "Gem3|BVY", "Gem3|BOV", "Gem3|GBV", "Gem3|ROY", "Gem3|GRY", "Gem3|RBY", "Gem3|GRO", "Gem3|RBO", "Gem3|GRO", "Gem3|GRB", "Gem3|GOY", "Gem3|BOY", "Gem3|GBO", "Gem3|GBY"})
fnAppendTables(zaLookups[i][3], {"Gem4|ROVY", "Gem4|GRVY", "Gem4|RBVY", "Gem4|GROV", "Gem4|RBOV", "Gem4|GRBV", "Gem4|GOVY", "Gem4|BOVY", "Gem4|GBVY", "Gem4|GBOV", "Gem4|GROY", "Gem4|RBOY", "Gem4|GRBY", "Gem4|GRBO", "Gem4|GBOY"})
fnAppendTables(zaLookups[i][3], {"Gem5|RBOVY", "Gem5|GROVY", "Gem5|GRBVY", "Gem5|GRBOV", "Gem5|GRBOY", "Gem5|GBOVY", "Gem6"})

--[Execution]
for i = 1, #zaLookups, 1 do
	fnExtract(zaLookups[i][3], zaLookups[i][1], zaLookups[i][2])
end

--Manually add these images with different filtering.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
    DL_ExtractBitmap("AdvDialogue|TextAdventureMapParts",  "Root/Images/AdventureUI/Dialogue/TextAdventureMapParts")
    DL_ExtractBitmap("AdvDialogue|TextAdventureScrollbar", "Root/Images/AdventureUI/Dialogue/TextAdventureScrollbar")
    DL_ExtractBitmap("AdvDialogue|PopupBorderCard",        "Root/Images/AdventureUI/Dialogue/PopupBorderCard")

    --Icons
    DL_ExtractBitmap("AdvCampfire|ICO|Rest",      "Root/Images/AdventureUI/CampfireMenu/ICO|Rest")
    DL_ExtractBitmap("AdvCampfire|ICO|Chat",      "Root/Images/AdventureUI/CampfireMenu/ICO|Chat")
    DL_ExtractBitmap("AdvCampfire|ICO|Save",      "Root/Images/AdventureUI/CampfireMenu/ICO|Save")
    DL_ExtractBitmap("AdvCampfire|ICO|Skills",    "Root/Images/AdventureUI/CampfireMenu/ICO|Skills")
    DL_ExtractBitmap("AdvCampfire|ICO|Transform", "Root/Images/AdventureUI/CampfireMenu/ICO|Transform")
    DL_ExtractBitmap("AdvCampfire|ICO|Warp",      "Root/Images/AdventureUI/CampfireMenu/ICO|Warp")
    DL_ExtractBitmap("AdvCampfire|ICO|Relive",    "Root/Images/AdventureUI/CampfireMenu/ICO|Relive")
    DL_ExtractBitmap("AdvCampfire|ICO|Costume",   "Root/Images/AdventureUI/CampfireMenu/ICO|Costume")
    DL_ExtractBitmap("AdvCampfire|ICO|Password",  "Root/Images/AdventureUI/CampfireMenu/ICO|Password")
    DL_ExtractBitmap("AdvCampfire|ICO|Frame",     "Root/Images/AdventureUI/CampfireMenu/ICO|Frame")

    --Map Pin
    DL_ExtractBitmap("AdvCampfire|MapPin", "Root/Images/AdventureUI/CampfireMenu/MapPin")
    
    --Map Regions
    DL_ExtractBitmap("AdvCampfire|ICO|Trannadar",  "Root/Images/AdventureUI/CampfireMenu/ICO|Trannadar")
    DL_ExtractBitmap("AdvCampfire|ICO|Regulus",    "Root/Images/AdventureUI/CampfireMenu/ICO|Regulus")
    DL_ExtractBitmap("AdvCampfire|ICO|Cryogenics", "Root/Images/AdventureUI/CampfireMenu/ICO|Cryogenics")
    DL_ExtractBitmap("AdvCampfire|ICO|Equinox",    "Root/Images/AdventureUI/CampfireMenu/ICO|Equinox")
    DL_ExtractBitmap("AdvCampfire|ICO|LRTE",       "Root/Images/AdventureUI/CampfireMenu/ICO|LRTE")
    DL_ExtractBitmap("AdvCampfire|ICO|LRTW",       "Root/Images/AdventureUI/CampfireMenu/ICO|LRTW")
    DL_ExtractBitmap("AdvCampfire|ICO|Biolabs",    "Root/Images/AdventureUI/CampfireMenu/ICO|Biolabs")
ALB_SetTextureProperty("Restore Defaults")

--96x96 Campfire Icons. No special scaling needed.
DL_ExtractBitmap("AdvCampfire|CHATICO|Florentina", "Root/Images/AdventureUI/CampfireMenu/CHATICO|Florentina")
DL_ExtractBitmap("AdvCampfire|CHATICO|Maram",      "Root/Images/AdventureUI/CampfireMenu/CHATICO|Maram")
DL_ExtractBitmap("AdvCampfire|CHATICO|55",         "Root/Images/AdventureUI/CampfireMenu/CHATICO|55")
DL_ExtractBitmap("AdvCampfire|CHATICO|SX399",      "Root/Images/AdventureUI/CampfireMenu/CHATICO|SX399")
DL_ExtractBitmap("AdvCampfire|CHATICO|Sophie",     "Root/Images/AdventureUI/CampfireMenu/CHATICO|Sophie")
DL_ExtractBitmap("AdvCampfire|CHATICO|JX101",      "Root/Images/AdventureUI/CampfireMenu/CHATICO|JX101")
DL_ExtractBitmap("AdvCampfire|CHATICO|Frame",      "Root/Images/AdventureUI/CampfireMenu/CHATICO|Frame")

--[UITextAdventure.slf]
--Baseline text adventure UI. Note that the dialogue is not loaded from this, because it shares parts with the Adventure Dialogue.
SLF_Open(gsDatafilesPath .. "UITextAdventure.slf")
DL_AddPath("Root/Images/TxtAdv/UI/")
DL_ExtractBitmap("CombatWordBorderCard", "Root/Images/TxtAdv/UI/CombatWordBorderCard")

--[Navigation]
DL_AddPath("Root/Images/TxtAdv/Navigation/")
DL_ExtractBitmap("Navigation|Look",  "Root/Images/TxtAdv/Navigation/Look")
DL_ExtractBitmap("Navigation|North", "Root/Images/TxtAdv/Navigation/North")
DL_ExtractBitmap("Navigation|Up",    "Root/Images/TxtAdv/Navigation/Up")
DL_ExtractBitmap("Navigation|West",  "Root/Images/TxtAdv/Navigation/West")
DL_ExtractBitmap("Navigation|Wait",  "Root/Images/TxtAdv/Navigation/Wait")
DL_ExtractBitmap("Navigation|East",  "Root/Images/TxtAdv/Navigation/East")
DL_ExtractBitmap("Navigation|Think", "Root/Images/TxtAdv/Navigation/Think")
DL_ExtractBitmap("Navigation|South", "Root/Images/TxtAdv/Navigation/South")
DL_ExtractBitmap("Navigation|Down",  "Root/Images/TxtAdv/Navigation/Down")

--[ElectrospriteAdventure.slf]
--These are loaded from the dedicated slf file. They are only used in Chapter 5.
SLF_Open(gsDatafilesPath .. "ElectrospriteAdventure.slf")
DL_AddPath("Root/Images/ElectrospriteAdv/Characters/")
DL_ExtractBitmap("Player", "Root/Images/ElectrospriteAdv/Characters/Player")

--Five NPCs, from A to E. All have the same sequences.
local saArray = {"A", "B", "C", "D", "E"}
for i = 1, 5, 1 do
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "Cored",  "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "Cored")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "Golem",  "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "Golem")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "Normal", "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "Normal")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "TF0",    "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "TF0")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "TF1",    "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "TF1")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "TF2",    "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "TF2")
end

--Clean
SLF_Close()

--[ ==================================== Adventure Combat UI ==================================== ]
SLF_Open(gsDatafilesPath .. "AdvCombat.slf")

--Function. This one places the prefix in front of the DLPath name.
local fnExtractAlt = function(sPrefix, sDLPath, saNames)
    
    --Add the path.
	DL_AddPath(sDLPath)
	
	--Extraction loop.
	local i = 1
	while(saNames[i] ~= nil) do
		DL_ExtractBitmap(sPrefix .. saNames[i], sDLPath .. sPrefix .. saNames[i])
		i = i + 1
	end
end

--All share the same DLPath.
local sDLPath = "Root/Images/AdventureUI/Combat/"

--Ally Bar
local sPrefix = "AllyBar|"
local saNames = {"AllyFrame", "AllyPortraitMask"}
fnExtractAlt(sPrefix, sDLPath, saNames)

--Defeat Overlay
sPrefix = "Defeat|"
saNames = {"Defeat_0", "Defeat_1", "Defeat_2", "Defeat_3", "Defeat_4", "Defeat_5"}
fnExtractAlt(sPrefix, sDLPath, saNames)

--Enemy Health Bars
sPrefix = "EnemyHealthBar|"
saNames = {"EnemyHealthBarEdge", "EnemyHealthBarFill", "EnemyHealthBarFrame", "EnemyHealthBarStun", "EnemyHealthBarStunMarker", "EnemyHealthBarUnder"}
fnExtractAlt(sPrefix, sDLPath, saNames)

--Combat Inspector
sPrefix = "Inspector|"
saNames = {"Frames", "Name Inset"}
fnExtractAlt(sPrefix, sDLPath, saNames)

--Player Interface
sPrefix = "PlayerInterface|"
saNames = {"AbilityFrameCatalyst1", "AbilityFrameCatalyst2", "AbilityFrameCatalyst3", "AbilityFrameCatalyst4", "AbilityFrameCatalyst5", "AbilityFrameCatalyst6", "AbilityFrameCustom", "AbilityFrameMain", "AbilityFrameSecond", "AbilityHighlight", "CPPip", "Descriptionwindow", "HealthBarAdrenaline", "HealthBarHPFill", "HealthBarMix", "HealthBarMPFill", "HealthBarShield", "HealthBarUnderlay", "MainHealthBarFrame", "MainNameBanner", "MainPortraitMask", "MainPortraitRing", "MainPortraitRingBack", "PredictionBox", "TargetArrowLft", "TargetArrowRgt", "TargetBox", "TitleBox"}
fnExtractAlt(sPrefix, sDLPath, saNames)

--Turn Order
sPrefix = "TurnOrder|"
saNames = {"TurnOrderBack", "TurnOrderCircle", "TurnOrderEdge"}
fnExtractAlt(sPrefix, sDLPath, saNames)

--Victory
sPrefix = "Victory|"
saNames = {"BannerBack", "BannerBig", "DoctorBack", "DoctorFill", "DoctorFrame", "DropFrame", "ExpFrameBack", "ExpFrameFill", "ExpFrameFront", "ExpFrameMask"}
fnExtractAlt(sPrefix, sDLPath, saNames)

-- |[ ==================================== Small Portraits ===================================== ]|
--[Loading Lists]
--First is the player's party portraits. This is used for the turn-order indicator in combat.
local saChristineList = {"Human", "SKIP", "SKIP", "SKIP", "SKIP", "SKIP", "Male", "Golem", "Latex", "Darkmatter", "Electrosprite", "SteamDroid", "Eldritch", "Raiju", "Doll"}
local saMeiList = {"Human", "Alraune", "Bee", "Ghost", "Slime", "Werecat", "Zombee"}
local saPartyList = {"Florentina", "FlorentinaTH", "FlorentinaMed", "55", "JX-101", "SX-399"}

--Enemies and Bosses. Only the turn-order portraits.
local saCh1EnemyList = {"Alraune", "Bee", "CultistF", "CultistM", "Ghost", "Slime", "Werecat", "Zombee", "SkullCrawler", "Arachnophelia", "Infirm"}
local saCh5EnemyList = {"Doll", "Electrosprite", "Golem", "GolemLord", "LatexDrone", "Raiju", "Scraprat", "SecurityWrecked", "DarkmatterGirl", "SteamDroid", "Dreamer", "Vivify", "56", "TechMonstrosity", "VoidRift", "Horrible", "Serenity", "609144", "Raibie", "BandageGoblin", "Hoodie", "InnGeisha"}

--Allies and Misc
local saAlliesList = {"BeeAlly"}

--[Execution]
csPrefix = "PorSml|"
csDLPath = "Root/Images/AdventureUI/TurnPortraits/"
DL_AddPath(csDLPath)

--Enemies listing.
fnExtractList(csPrefix, csDLPath, saCh1EnemyList)
fnExtractList(csPrefix, csDLPath, saCh5EnemyList)

--Player's party characters.
fnExtractList(csPrefix, csDLPath, saChristineList, "Christine_")
fnExtractList(csPrefix, csDLPath, saMeiList, "Mei_")
fnExtractList(csPrefix, csDLPath, saPartyList)

--Allies etc.
fnExtractList(csPrefix, csDLPath, saAlliesList)

-- |[ ========================================= Icons ========================================== ]|
--File
SLF_Open(gsDatafilesPath .. "UIAdvIcons.slf")

--Damage Types
DL_AddPath("Root/Images/AdventureUI/DamageTypeIcons/")
DL_ExtractBitmap("DmgTypeIco|Slashing",   "Root/Images/AdventureUI/DamageTypeIcons/Slashing")
DL_ExtractBitmap("DmgTypeIco|Striking",   "Root/Images/AdventureUI/DamageTypeIcons/Striking")
DL_ExtractBitmap("DmgTypeIco|Piercing",   "Root/Images/AdventureUI/DamageTypeIcons/Piercing")
DL_ExtractBitmap("DmgTypeIco|Flaming",    "Root/Images/AdventureUI/DamageTypeIcons/Flaming")
DL_ExtractBitmap("DmgTypeIco|Freezing",   "Root/Images/AdventureUI/DamageTypeIcons/Freezing")
DL_ExtractBitmap("DmgTypeIco|Shocking",   "Root/Images/AdventureUI/DamageTypeIcons/Shocking")
DL_ExtractBitmap("DmgTypeIco|Crusading",  "Root/Images/AdventureUI/DamageTypeIcons/Crusading")
DL_ExtractBitmap("DmgTypeIco|Obscuring",  "Root/Images/AdventureUI/DamageTypeIcons/Obscuring")
DL_ExtractBitmap("DmgTypeIco|Bleeding",   "Root/Images/AdventureUI/DamageTypeIcons/Bleeding")
DL_ExtractBitmap("DmgTypeIco|Poisoning",  "Root/Images/AdventureUI/DamageTypeIcons/Poisoning")
DL_ExtractBitmap("DmgTypeIco|Corroding",  "Root/Images/AdventureUI/DamageTypeIcons/Corroding")
DL_ExtractBitmap("DmgTypeIco|Terrifying", "Root/Images/AdventureUI/DamageTypeIcons/Terrifying")
DL_ExtractBitmap("DmgTypeIco|Protection", "Root/Images/AdventureUI/DamageTypeIcons/Protection")
DL_ExtractBitmap("DmgTypeIco|Weapon",     "Root/Images/AdventureUI/DamageTypeIcons/Weapon")

--Comparison Icons
DL_AddPath("Root/Images/AdventureUI/StatisticIcons/")
local saCompareIcoNames = {"Attack", "Protection", "Health", "Accuracy", "Evade", "Initiative", "Mana", "Blind", "Bleed", "Poison", "Corrode", "Shields", "Adrenaline", "ComboPoint", "Stun", "Turns", "Slash", "Pierce", "Strike", "Flaming", "Freezing", "Shocking", "Crusading", "Obscuring", "Terrify", "Debuff", "Underlay", "Downx3", "Downx2", "Downx1", "Neutral", "Upx1", "Upx2", "Upx3", "Rarrow"}
for i = 1, #saCompareIcoNames, 1 do
    DL_ExtractBitmap("CompareIco|" .. saCompareIcoNames[i], "Root/Images/AdventureUI/StatisticIcons/" .. saCompareIcoNames[i])
end

--Special comparison icons only used for the combat inspector. These are narrower and taller to fit with the other stats.
DL_ExtractBitmap("CompareIcoNr|Up", "Root/Images/AdventureUI/StatisticIcons/NrUp")
DL_ExtractBitmap("CompareIcoNr|Dn", "Root/Images/AdventureUI/StatisticIcons/NrDn")

--Debuff Icons
DL_AddPath("Root/Images/AdventureUI/DebuffIcons/")
DL_ExtractBitmap("StatEffectIco|Blind",    "Root/Images/AdventureUI/DebuffIcons/Blind")
DL_ExtractBitmap("StatEffectIco|Buff",     "Root/Images/AdventureUI/DebuffIcons/Buff")
DL_ExtractBitmap("StatEffectIco|Debuff",   "Root/Images/AdventureUI/DebuffIcons/Debuff")
DL_ExtractBitmap("StatEffectIco|Clock",    "Root/Images/AdventureUI/DebuffIcons/Clock")
DL_ExtractBitmap("StatEffectIco|Accuracy", "Root/Images/AdventureUI/DebuffIcons/Accuracy")
DL_ExtractBitmap("StatEffectIco|Stun",     "Root/Images/AdventureUI/DebuffIcons/Stun")

--Effect Indicators
DL_AddPath("Root/Images/AdventureUI/EffectIndicator/")
DL_ExtractBitmap("StatEffectIco|EffectHostile",  "Root/Images/AdventureUI/EffectIndicator/EffectHostile")
DL_ExtractBitmap("StatEffectIco|EffectFriendly", "Root/Images/AdventureUI/EffectIndicator/EffectFriendly")

--Strength Indicators
DL_AddPath("Root/Images/AdventureUI/StrengthIndicator/")
DL_ExtractBitmap("StatEffectIco|Str0",  "Root/Images/AdventureUI/StrengthIndicator/Str0")
DL_ExtractBitmap("StatEffectIco|Str1",  "Root/Images/AdventureUI/StrengthIndicator/Str1")
DL_ExtractBitmap("StatEffectIco|Str2",  "Root/Images/AdventureUI/StrengthIndicator/Str2")
DL_ExtractBitmap("StatEffectIco|Str3",  "Root/Images/AdventureUI/StrengthIndicator/Str3")
DL_ExtractBitmap("StatEffectIco|Str4",  "Root/Images/AdventureUI/StrengthIndicator/Str4")
DL_ExtractBitmap("StatEffectIco|Str5",  "Root/Images/AdventureUI/StrengthIndicator/Str5")
DL_ExtractBitmap("StatEffectIco|Str6",  "Root/Images/AdventureUI/StrengthIndicator/Str6")
DL_ExtractBitmap("StatEffectIco|Str7",  "Root/Images/AdventureUI/StrengthIndicator/Str7")
DL_ExtractBitmap("StatEffectIco|Str8",  "Root/Images/AdventureUI/StrengthIndicator/Str8")
DL_ExtractBitmap("StatEffectIco|Str9",  "Root/Images/AdventureUI/StrengthIndicator/Str9")

--[Ability Icons]
--Backing and Combo Point Indicators
DL_ExtractBitmap("AbilityIco|OctBackGrey",   "Root/Images/AdventureUI/Abilities/OctBackGrey")
DL_ExtractBitmap("AbilityIco|OctBackRed",    "Root/Images/AdventureUI/Abilities/OctBackRed")
DL_ExtractBitmap("AbilityIco|OctBackGreen",  "Root/Images/AdventureUI/Abilities/OctBackGreen")
DL_ExtractBitmap("AbilityIco|OctBackBlue",   "Root/Images/AdventureUI/Abilities/OctBackBlue")
DL_ExtractBitmap("AbilityIco|OctBackPurple", "Root/Images/AdventureUI/Abilities/OctBackPurple")
DL_ExtractBitmap("AbilityIco|SqrBackGrey",   "Root/Images/AdventureUI/Abilities/SqrBackGrey")
DL_ExtractBitmap("AbilityIco|SqrBackRed",    "Root/Images/AdventureUI/Abilities/SqrBackRed")
DL_ExtractBitmap("AbilityIco|SqrBackGreen",  "Root/Images/AdventureUI/Abilities/SqrBackGreen")
DL_ExtractBitmap("AbilityIco|SqrBackBlue",   "Root/Images/AdventureUI/Abilities/SqrBackBlue")
DL_ExtractBitmap("AbilityIco|SqrBackPurple", "Root/Images/AdventureUI/Abilities/SqrBackPurple")
ALB_SetTextureProperty("MagFilter", iNearest)
DL_ExtractBitmap("AbilityIco|OctFrameRed",   "Root/Images/AdventureUI/Abilities/OctFrameRed")
DL_ExtractBitmap("AbilityIco|OctFrameBlue",  "Root/Images/AdventureUI/Abilities/OctFrameBlue")
DL_ExtractBitmap("AbilityIco|OctFrameTeal",  "Root/Images/AdventureUI/Abilities/OctFrameTeal")
DL_ExtractBitmap("AbilityIco|SqrFrameRed",   "Root/Images/AdventureUI/Abilities/SqrFrameRed")
DL_ExtractBitmap("AbilityIco|SqrFrameBlue",  "Root/Images/AdventureUI/Abilities/SqrFrameBlue")
DL_ExtractBitmap("AbilityIco|SqrFrameTeal",  "Root/Images/AdventureUI/Abilities/SqrFrameTeal")
ALB_SetTextureProperty("Restore Defaults")
DL_ExtractBitmap("AbilityIco|CmbClock", "Root/Images/AdventureUI/Abilities/CmbClock")
for i = 0, 9, 1 do
    DL_ExtractBitmap("AbilityIco|Cmb"..i, "Root/Images/AdventureUI/Abilities/Cmb"..i)
end

--Items
DL_ExtractBitmap("AbilityIco|Item|SilverRunestone", "Root/Images/AdventureUI/Abilities/Item|SilverRunestone")
DL_ExtractBitmap("AbilityIco|Item|VioletRunestone", "Root/Images/AdventureUI/Abilities/Item|VioletRunestone")
DL_ExtractBitmap("AbilityIco|Item|GenericPotion",   "Root/Images/AdventureUI/Abilities/Item|GenericPotion")
DL_ExtractBitmap("AbilityIco|Item|YellowPotion",    "Root/Images/AdventureUI/Abilities/Item|YellowPotion")

--Basic Stuff
DL_ExtractBitmap("AbilityIco|Attack",    "Root/Images/AdventureUI/Abilities/Attack")
DL_ExtractBitmap("AbilityIco|Defend",    "Root/Images/AdventureUI/Abilities/Defend")
DL_ExtractBitmap("AbilityIco|Special",   "Root/Images/AdventureUI/Abilities/Special")
DL_ExtractBitmap("AbilityIco|Retreat",   "Root/Images/AdventureUI/Abilities/Retreat")
DL_ExtractBitmap("AbilityIco|Surrender", "Root/Images/AdventureUI/Abilities/Surrender")
DL_ExtractBitmap("AbilityIco|Threat",    "Root/Images/AdventureUI/Abilities/Threat")
DL_ExtractBitmap("AbilityIco|Lock",      "Root/Images/AdventureUI/Abilities/Lock")
DL_ExtractBitmap("AbilityIco|WeaponLft", "Root/Images/AdventureUI/Abilities/WeaponLft")
DL_ExtractBitmap("AbilityIco|WeaponRgt", "Root/Images/AdventureUI/Abilities/WeaponRgt")

--[Mei's Abilities]
--Lists
local saFencerList     = {"WayOfTheBlade", "PowerfulStrike", "Rend", "BladeDance", "PommelBash", "Blind", "Precognition", "Taunt", "QuickStrike", "Trip", "Concentrate", "Whirl", "FastReflexes"}
local saNightshadeList = {"WayOfTheDruid", "SporeCloud", "Regrowth", "VineLash", "PollenRend", "FanOfLeaves", "Ripple", "NaturesBlessing", "AcidBlood"}
local saProwlerList    = {"WayOfTheClaw", "Pounce", "JumpKick", "Garrote", "HuntersInstinct", "Jab", "Lunge", "Swipe", "GloryKill"}
local saHiveScoutList  = {"WayOfTheHive", "Sting", "FlapJump", "ClawKick", "Coordinate", "WaxArmor", "ScoutsHonor", "CallForHelp"}
local saMaidList       = {"WayOfTheDead", "DusterBlast", "TidyUp", "TranslucentSmile", "FirstAid", "IcyHand", "Undying"}
local saSmartySageList = {"WayOfTheSlime", "Splash", "Goopshot", "FirmUp", "JigglyChest", "Absorb", "BigDumbGrin"}
local saZombeeList     = {"WayOfTheZombee", "Rage", "Fury", "Roar", "CorruptHoney", "Crush"}
local saRubberList     = {"SpreadRubber", "Permagrin"}

--List of Lists
local saMeiListList = {}
saMeiListList[1] = saFencerList
saMeiListList[2] = saNightshadeList
saMeiListList[3] = saProwlerList
saMeiListList[4] = saHiveScoutList
saMeiListList[5] = saMaidList
saMeiListList[6] = saSmartySageList
saMeiListList[7] = saZombeeList
saMeiListList[8] = saRubberList

--Iterate, crossload.
for i = 1, #saMeiListList, 1 do
    local saSubList = saMeiListList[i]
    for p = 1, #saSubList, 1 do
        DL_ExtractBitmap("AbilityIco|Mei|" .. saSubList[p],  "Root/Images/AdventureUI/Abilities/Mei|" .. saSubList[p])
    end
end

--[Florentina's Abilities]
--Lists.
local saMerchantList = {"Botany", "CriticalStab", "DrippingBlade", "Intimidate", "DrainVitality", "Regrowth", "CruelSlice", "VineWrap"}
local saTreasureHunterList = {"FlorentinasInstinct", "FromTheShadows", "Haymaker", "LightStep", "PickPocket", "PoisonDarts", "StickyFingers", "VineWhip", "SmashAndGrab", "WhipTrip"}
local saMediatorList = {"FlorentinasWit", "VeiledThreat", "CalledShot", "CounterArgument", "Encourage", "Smug", "Insult", "TerriblePun", "PickMeUp"}

--List of lists.
local saFlorentinaListList = {}
saFlorentinaListList[1] = saMerchantList
saFlorentinaListList[2] = saTreasureHunterList
saFlorentinaListList[3] = saMediatorList

--Iterate, crossload.
for i = 1, #saFlorentinaListList, 1 do
    local saSubList = saFlorentinaListList[i]
    for p = 1, #saSubList, 1 do
        DL_ExtractBitmap("AbilityIco|Florentina|" .. saSubList[p],  "Root/Images/AdventureUI/Abilities/Florentina|" .. saSubList[p])
    end
end

--[Christine's Abilities]
DL_ExtractBitmap("AbilityIco|Christine|MaleTech",      "Root/Images/AdventureUI/Abilities/Christine|MaleTech")
DL_ExtractBitmap("AbilityIco|Christine|FemaleTech",    "Root/Images/AdventureUI/Abilities/Christine|FemaleTech")
DL_ExtractBitmap("AbilityIco|Christine|GolemTech",     "Root/Images/AdventureUI/Abilities/Christine|GolemTech")
DL_ExtractBitmap("AbilityIco|Christine|TakePoint",     "Root/Images/AdventureUI/Abilities/Christine|TakePoint")
DL_ExtractBitmap("AbilityIco|Christine|LineFormation", "Root/Images/AdventureUI/Abilities/Christine|LineFormation")
DL_ExtractBitmap("AbilityIco|Christine|Shock",         "Root/Images/AdventureUI/Abilities/Christine|Shock")
DL_ExtractBitmap("AbilityIco|Christine|Batter",        "Root/Images/AdventureUI/Abilities/Christine|Batter")
DL_ExtractBitmap("AbilityIco|Christine|Sweep",         "Root/Images/AdventureUI/Abilities/Christine|Sweep")
DL_ExtractBitmap("AbilityIco|Christine|Rally",         "Root/Images/AdventureUI/Abilities/Christine|Rally")
DL_ExtractBitmap("AbilityIco|Christine|Puncture",      "Root/Images/AdventureUI/Abilities/Christine|Puncture")
DL_ExtractBitmap("AbilityIco|Christine|OfficerCharge", "Root/Images/AdventureUI/Abilities/Christine|OfficerCharge")
DL_ExtractBitmap("AbilityIco|Christine|Encourage",     "Root/Images/AdventureUI/Abilities/Christine|Encourage")

--55's Abilities
DL_ExtractBitmap("AbilityIco|55|Assault",          "Root/Images/AdventureUI/Abilities/55|Assault")
DL_ExtractBitmap("AbilityIco|55|Support",          "Root/Images/AdventureUI/Abilities/55|Support")
DL_ExtractBitmap("AbilityIco|55|Restore",          "Root/Images/AdventureUI/Abilities/55|Restore")
DL_ExtractBitmap("AbilityIco|55|CombatRoutines",   "Root/Images/AdventureUI/Abilities/55|CombatRoutines")
DL_ExtractBitmap("AbilityIco|55|HighPowerShot",    "Root/Images/AdventureUI/Abilities/55|HighPowerShot")
DL_ExtractBitmap("AbilityIco|55|Repair",           "Root/Images/AdventureUI/Abilities/55|Repair")
DL_ExtractBitmap("AbilityIco|55|HyperRepair",      "Root/Images/AdventureUI/Abilities/55|HyperRepair")
DL_ExtractBitmap("AbilityIco|55|WideLensShot",     "Root/Images/AdventureUI/Abilities/55|WideLensShot")
DL_ExtractBitmap("AbilityIco|55|RubberSlugShot",   "Root/Images/AdventureUI/Abilities/55|RubberSlugShot")
DL_ExtractBitmap("AbilityIco|55|DisruptorBlast",   "Root/Images/AdventureUI/Abilities/55|DisruptorBlast")
DL_ExtractBitmap("AbilityIco|55|SpotlessProtocol", "Root/Images/AdventureUI/Abilities/55|SpotlessProtocol")
DL_ExtractBitmap("AbilityIco|55|ImplosionGrenade", "Root/Images/AdventureUI/Abilities/55|ImplosionGrenade")

--JX-101's Abilities
DL_ExtractBitmap("AbilityIco|JX101|Techniques",      "Root/Images/AdventureUI/Abilities/JX101|Techniques")
DL_ExtractBitmap("AbilityIco|JX101|ThunderboltSlug", "Root/Images/AdventureUI/Abilities/JX101|ThunderboltSlug")
DL_ExtractBitmap("AbilityIco|JX101|PinningFire",     "Root/Images/AdventureUI/Abilities/JX101|PinningFire")
DL_ExtractBitmap("AbilityIco|JX101|SniperShot",      "Root/Images/AdventureUI/Abilities/JX101|SniperShot")
DL_ExtractBitmap("AbilityIco|JX101|BattleCry",       "Root/Images/AdventureUI/Abilities/JX101|BattleCry")

--SX-399's Abilities
DL_ExtractBitmap("AbilityIco|SX399|MeltaBlast",      "Root/Images/AdventureUI/Abilities/SX399|MeltaBlast")

--[ =================================== Comparison Crossload ==================================== ]
--Remaps used for the inventory.
local i18PxLookupsTotal = #saCompareIcoNames
local saBaseNames = {"Atk18", "Prt18", "HP18", "Acc18", "Evd18", "Ini18", "Blnd18", "Bld18", "Psn18", "Crd18", "Mgc18", "Cmb18", "Stn18", "Slsh18", "Prc18", "Str18", "Flm18", "Frz18", "Shk18", "Cru18", "Obs18"}

AM_SetProperty("Text Image Remaps Total", i18PxLookupsTotal)
for i = 1, i18PxLookupsTotal, 1 do
    AM_SetProperty("Text Image Remaps", i-1, saBaseNames[i], "Root/Images/AdventureUI/CompareIcons/" .. saCompareIcoNames[i])
end

--[ =================================== Adventure Menu - Base =================================== ]
--File
SLF_Open(gsDatafilesPath .. "UIAdvMenuBase.slf")

--Setup
sPrefix = "AdvBaseMenu|"
sDLPath = "Root/Images/AdventureUI/BaseMenu/"
DL_AddPath("Root/Images/AdventureUI/BaseMenu/")

--List
saNames = {"ComboBar", "Footer", "Header", "HealthBarBack", "HealthBarFill", "HealthBarFrame", "HelpBacking", "InventoryBacks", "InventoryBanners", "NameBar", "PlatinaBanner", "PlatinaText", "PortraitBack", "PortraitFront", "PortraitMask", "QuitBacking"}

--Extraction Loop
for i = 1, #saNames, 1 do
    DL_ExtractBitmap(sPrefix .. saNames[i], sDLPath .. saNames[i])
end

--Icons
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
    DL_ExtractBitmap(sPrefix .. "ICO|Inventory", sDLPath .. "ICO|Inventory")
    DL_ExtractBitmap(sPrefix .. "ICO|Equipment", sDLPath .. "ICO|Equipment")
    DL_ExtractBitmap(sPrefix .. "ICO|DoctorBag", sDLPath .. "ICO|DoctorBag")
    DL_ExtractBitmap(sPrefix .. "ICO|Status",    sDLPath .. "ICO|Status")
    DL_ExtractBitmap(sPrefix .. "ICO|Skills",    sDLPath .. "ICO|Skills")
    DL_ExtractBitmap(sPrefix .. "ICO|Map",       sDLPath .. "ICO|Map")
    DL_ExtractBitmap(sPrefix .. "ICO|Options",   sDLPath .. "ICO|Options")
    DL_ExtractBitmap(sPrefix .. "ICO|Quit",      sDLPath .. "ICO|Quit")
    DL_ExtractBitmap(sPrefix .. "ICO|Frame",     sDLPath .. "ICO|Frame")
ALB_SetTextureProperty("Restore Defaults")

--[ ================================== Adventure Menu - Skills ================================== ]
--File
SLF_Open(gsDatafilesPath .. "UIAdvMenuSkills.slf")

--Load Basic
DL_AddPath("Root/Images/AdventureUI/SkillsMenu/")
DL_ExtractBitmap("AdvMenuSkill|ClassActive",     "Root/Images/AdventureUI/SkillsMenu/ClassActive")
DL_ExtractBitmap("AdvMenuSkill|ClassMaster",     "Root/Images/AdventureUI/SkillsMenu/ClassMaster")
DL_ExtractBitmap("AdvMenuSkill|Header",          "Root/Images/AdventureUI/SkillsMenu/Header")
DL_ExtractBitmap("AdvMenuSkill|PaneJobs",        "Root/Images/AdventureUI/SkillsMenu/PaneJobs")
DL_ExtractBitmap("AdvMenuSkill|PaneSkills",      "Root/Images/AdventureUI/SkillsMenu/PaneSkills")
DL_ExtractBitmap("AdvMenuSkill|ScrollbarFront",  "Root/Images/AdventureUI/SkillsMenu/ScrollbarFront")
DL_ExtractBitmap("AdvMenuSkill|ScrollbarJobs",   "Root/Images/AdventureUI/SkillsMenu/ScrollbarJobs")
DL_ExtractBitmap("AdvMenuSkill|ScrollbarSkills", "Root/Images/AdventureUI/SkillsMenu/ScrollbarSkills")
DL_ExtractBitmap("AdvMenuSkill|SkillEquipped",   "Root/Images/AdventureUI/SkillsMenu/SkillEquipped")

--Highlight has no filtering.
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
    DL_ExtractBitmap("AdvMenuSkill|Highlight", "Root/Images/AdventureUI/SkillsMenu/Highlight")
ALB_SetTextureProperty("Restore Defaults")

--[ ============================= Adventure Menu - Field Abilities ============================== ]
--File
SLF_Open(gsDatafilesPath .. "UIAdvMenuFieldAbilities.slf")

--Load Basic
DL_AddPath("Root/Images/AdventureUI/FieldMenu/")
DL_ExtractBitmap("AdvMenuFieldAbility|Header", "Root/Images/AdventureUI/FieldMenu/Header")
DL_ExtractBitmap("AdvMenuFieldAbility|Footer", "Root/Images/AdventureUI/FieldMenu/Footer")
