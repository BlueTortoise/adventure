--[Graphics: Scenes]
--Images used in TF scenes and CGs. Some images used in those scenes are also from Portraits.slf.
-- These are used at all points in the game, or at the start. Everything else is in the chapter-specific files.
SLF_Open(gsaDatafilePaths.sScnCh0Major)

--[ ======================================= Miscellaneous ======================================= ]
--Used for tutorials and the like.
DL_AddPath("Root/Images/Scenes/System/")
DL_ExtractBitmap("Controls", "Root/Images/Scenes/System/Controls")

--Credits Sequences. Only chapter 5's exists for now.
DL_ExtractBitmap("CreditsBacking5", "Root/Images/Scenes/System/CreditsBacking5")
