--[ ===================================== Control Manager UI ==================================== ]
--Contains UI instructions for the Control Manager. This is where the little keys that appear in text
-- are resolved.

--[ ========================================== Loading ========================================== ]
--Images used for the controls interfaces.
SLF_Open(gsDatafilesPath .. "UIControls.slf")
DL_AddPath("Root/Images/ControlsUI/Keyboard/")
DL_ExtractBitmap("Keyboard|Tilde", "Root/Images/ControlsUI/Keyboard/Tilde")
DL_ExtractBitmap("Keyboard|1", "Root/Images/ControlsUI/Keyboard/1")
DL_ExtractBitmap("Keyboard|2", "Root/Images/ControlsUI/Keyboard/2")
DL_ExtractBitmap("Keyboard|3", "Root/Images/ControlsUI/Keyboard/3")
DL_ExtractBitmap("Keyboard|4", "Root/Images/ControlsUI/Keyboard/4")
DL_ExtractBitmap("Keyboard|5", "Root/Images/ControlsUI/Keyboard/5")

DL_ExtractBitmap("Keyboard|Tab", "Root/Images/ControlsUI/Keyboard/Tab")
DL_ExtractBitmap("Keyboard|E",   "Root/Images/ControlsUI/Keyboard/E")
DL_ExtractBitmap("Keyboard|Q",   "Root/Images/ControlsUI/Keyboard/Q")

DL_ExtractBitmap("Keyboard|Shift", "Root/Images/ControlsUI/Keyboard/Shift")
DL_ExtractBitmap("Keyboard|Z", "Root/Images/ControlsUI/Keyboard/Z")
DL_ExtractBitmap("Keyboard|X", "Root/Images/ControlsUI/Keyboard/X")
DL_ExtractBitmap("Keyboard|C", "Root/Images/ControlsUI/Keyboard/C")
DL_ExtractBitmap("Keyboard|V", "Root/Images/ControlsUI/Keyboard/V")
DL_ExtractBitmap("Keyboard|B", "Root/Images/ControlsUI/Keyboard/B")
DL_ExtractBitmap("Keyboard|N", "Root/Images/ControlsUI/Keyboard/N")
DL_ExtractBitmap("Keyboard|M", "Root/Images/ControlsUI/Keyboard/M")
DL_ExtractBitmap("Keyboard|Comma", "Root/Images/ControlsUI/Keyboard/Comma")
DL_ExtractBitmap("Keyboard|Period", "Root/Images/ControlsUI/Keyboard/Period")
DL_ExtractBitmap("Keyboard|Slash", "Root/Images/ControlsUI/Keyboard/Slash")

DL_ExtractBitmap("Keyboard|Ctrl", "Root/Images/ControlsUI/Keyboard/Ctrl")
DL_ExtractBitmap("Keyboard|Alt", "Root/Images/ControlsUI/Keyboard/Alt")
DL_ExtractBitmap("Keyboard|Space", "Root/Images/ControlsUI/Keyboard/Space")

DL_ExtractBitmap("Keyboard|Error", "Root/Images/ControlsUI/Keyboard/Error")

DL_ExtractBitmap("Keyboard|ArrU", "Root/Images/ControlsUI/Keyboard/ArrU")
DL_ExtractBitmap("Keyboard|ArrL", "Root/Images/ControlsUI/Keyboard/ArrL")
DL_ExtractBitmap("Keyboard|ArrD", "Root/Images/ControlsUI/Keyboard/ArrD")
DL_ExtractBitmap("Keyboard|ArrR", "Root/Images/ControlsUI/Keyboard/ArrR")

DL_ExtractBitmap("Keyboard|F1", "Root/Images/ControlsUI/Keyboard/F1")
DL_ExtractBitmap("Keyboard|F2", "Root/Images/ControlsUI/Keyboard/F2")
DL_ExtractBitmap("Keyboard|F3", "Root/Images/ControlsUI/Keyboard/F3")
DL_ExtractBitmap("Keyboard|F4", "Root/Images/ControlsUI/Keyboard/F4")
DL_ExtractBitmap("Keyboard|F5", "Root/Images/ControlsUI/Keyboard/F5")

--[ ========================================= Functions ========================================= ]
--Adder function.
local iCurrentPack = 0
local fnSetImage = function(psString, psDLPath)
    if(psString == nil) then return end
    if(psDLPath == nil) then return end
    local iKeyboard, iMouse, iJoypad = ControlManager_GetProperty("Input Codes By String", psString)
    ControlManager_SetProperty("Set Control Image Pack", iCurrentPack, iKeyboard, iMouse, iJoypad, psDLPath)
    iCurrentPack = iCurrentPack + 1
end

--[ ================================= Upload to Control Manager ================================= ]
--Create control references for the Control Manager. After this point, the program can request a
-- control's image and the Control Manager will return it.
ControlManager_SetProperty("Set Control Error Image", "Root/Images/ControlsUI/Keyboard/Error")

--Total
ControlManager_SetProperty("Allocate Control Image Packs", 35)

--[ ====================================== Allegro Version ====================================== ]
if(LM_GetSystemLibrary() == "Allegro") then
    
    --Keyboard
    fnSetImage("KEY_Tilde", "Root/Images/ControlsUI/Keyboard/Tilde")
    fnSetImage("KEY_1",     "Root/Images/ControlsUI/Keyboard/1")
    fnSetImage("KEY_2",     "Root/Images/ControlsUI/Keyboard/2")
    fnSetImage("KEY_3",     "Root/Images/ControlsUI/Keyboard/3")
    fnSetImage("KEY_4",     "Root/Images/ControlsUI/Keyboard/4")
    fnSetImage("KEY_5",     "Root/Images/ControlsUI/Keyboard/5")
    
    fnSetImage("KEY_TAB", "Root/Images/ControlsUI/Keyboard/Tab")
    fnSetImage("KEY_E",   "Root/Images/ControlsUI/Keyboard/E")
    fnSetImage("KEY_Q",   "Root/Images/ControlsUI/Keyboard/Q")
    
    fnSetImage("KEY_LSHIFT",   "Root/Images/ControlsUI/Keyboard/Shift")
    fnSetImage("KEY_Z",        "Root/Images/ControlsUI/Keyboard/Z")
    fnSetImage("KEY_X",        "Root/Images/ControlsUI/Keyboard/X")
    fnSetImage("KEY_C",        "Root/Images/ControlsUI/Keyboard/C")
    fnSetImage("KEY_V",        "Root/Images/ControlsUI/Keyboard/V")
    fnSetImage("KEY_B",        "Root/Images/ControlsUI/Keyboard/B")
    fnSetImage("KEY_N",        "Root/Images/ControlsUI/Keyboard/N")
    fnSetImage("KEY_M",        "Root/Images/ControlsUI/Keyboard/M")
    fnSetImage("KEY_COMMA",    "Root/Images/ControlsUI/Keyboard/Comma")
    fnSetImage("KEY_PERIOD",   "Root/Images/ControlsUI/Keyboard/Period")
    fnSetImage("KEY_SLASH",    "Root/Images/ControlsUI/Keyboard/Slash")
    fnSetImage("KEY_RSHIFT",   "Root/Images/ControlsUI/Keyboard/Shift")
    
    fnSetImage("KEY_LCTRL",  "Root/Images/ControlsUI/Keyboard/Ctrl")
    fnSetImage("KEY_RCTRL",  "Root/Images/ControlsUI/Keyboard/Ctrl")
    fnSetImage("KEY_ALT",    "Root/Images/ControlsUI/Keyboard/Alt")
    fnSetImage("KEY_ALTGR",  "Root/Images/ControlsUI/Keyboard/Alt")
    fnSetImage("KEY_SPACE",  "Root/Images/ControlsUI/Keyboard/Space")
    
    fnSetImage("KEY_UP",    "Root/Images/ControlsUI/Keyboard/ArrU")
    fnSetImage("KEY_DOWN",  "Root/Images/ControlsUI/Keyboard/ArrD")
    fnSetImage("KEY_LEFT",  "Root/Images/ControlsUI/Keyboard/ArrL")
    fnSetImage("KEY_RIGHT", "Root/Images/ControlsUI/Keyboard/ArrR")
    
    fnSetImage("KEY_F1", "Root/Images/ControlsUI/Keyboard/F1")
    fnSetImage("KEY_F2", "Root/Images/ControlsUI/Keyboard/F2")
    fnSetImage("KEY_F3", "Root/Images/ControlsUI/Keyboard/F3")
    fnSetImage("KEY_F4", "Root/Images/ControlsUI/Keyboard/F4")
    fnSetImage("KEY_F5", "Root/Images/ControlsUI/Keyboard/F5")
    
--[ ======================================== SDL Version ======================================== ]
else

    --Keyboard
    fnSetImage("KEY_Tilde", "Root/Images/ControlsUI/Keyboard/Tilde")
    fnSetImage("KEY_1",     "Root/Images/ControlsUI/Keyboard/1")
    fnSetImage("KEY_2",     "Root/Images/ControlsUI/Keyboard/2")
    fnSetImage("KEY_3",     "Root/Images/ControlsUI/Keyboard/3")
    fnSetImage("KEY_4",     "Root/Images/ControlsUI/Keyboard/4")
    fnSetImage("KEY_5",     "Root/Images/ControlsUI/Keyboard/5")
    
    fnSetImage("KEY_Tab", "Root/Images/ControlsUI/Keyboard/Tab")
    fnSetImage("KEY_E",   "Root/Images/ControlsUI/Keyboard/E")
    fnSetImage("KEY_Q",   "Root/Images/ControlsUI/Keyboard/Q")
    
    fnSetImage("KEY_LShift",   "Root/Images/ControlsUI/Keyboard/Shift")
    fnSetImage("KEY_Z",        "Root/Images/ControlsUI/Keyboard/Z")
    fnSetImage("KEY_X",        "Root/Images/ControlsUI/Keyboard/X")
    fnSetImage("KEY_C",        "Root/Images/ControlsUI/Keyboard/C")
    fnSetImage("KEY_V",        "Root/Images/ControlsUI/Keyboard/V")
    fnSetImage("KEY_B",        "Root/Images/ControlsUI/Keyboard/B")
    fnSetImage("KEY_N",        "Root/Images/ControlsUI/Keyboard/N")
    fnSetImage("KEY_M",        "Root/Images/ControlsUI/Keyboard/M")
    fnSetImage("KEY_COMMA",    "Root/Images/ControlsUI/Keyboard/Comma")
    fnSetImage("KEY_PERIOD",   "Root/Images/ControlsUI/Keyboard/Period")
    fnSetImage("KEY_SLASH",    "Root/Images/ControlsUI/Keyboard/Slash")
    fnSetImage("KEY_RShift",   "Root/Images/ControlsUI/Keyboard/Shift")
    
    fnSetImage("KEY_LCtrl",     "Root/Images/ControlsUI/Keyboard/Ctrl")
    fnSetImage("KEY_RCtrl",     "Root/Images/ControlsUI/Keyboard/Ctrl")
    fnSetImage("KEY_Left Alt",  "Root/Images/ControlsUI/Keyboard/Alt")
    fnSetImage("KEY_Right Alt", "Root/Images/ControlsUI/Keyboard/Alt")
    fnSetImage("KEY_Space",     "Root/Images/ControlsUI/Keyboard/Space")
    
    fnSetImage("KEY_Up",    "Root/Images/ControlsUI/Keyboard/ArrU")
    fnSetImage("KEY_Down",  "Root/Images/ControlsUI/Keyboard/ArrD")
    fnSetImage("KEY_Left",  "Root/Images/ControlsUI/Keyboard/ArrL")
    fnSetImage("KEY_Right", "Root/Images/ControlsUI/Keyboard/ArrR")
    
    fnSetImage("KEY_F1", "Root/Images/ControlsUI/Keyboard/F1")
    fnSetImage("KEY_F2", "Root/Images/ControlsUI/Keyboard/F2")
    fnSetImage("KEY_F3", "Root/Images/ControlsUI/Keyboard/F3")
    fnSetImage("KEY_F4", "Root/Images/ControlsUI/Keyboard/F4")
    fnSetImage("KEY_F5", "Root/Images/ControlsUI/Keyboard/F5")
    
end