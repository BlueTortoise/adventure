--[Build Map Path Remaps]
--Builds a static listing of map path remaps. The editor uses simplified names like "TrapBasementA" to indicate where exits go,
-- and cutscenes use the same logic. Because the maps can be stored in subdirectories, we need to remap the paths to go to 
-- specific subdirectories and do this without having to search for existence, which is slow.
--As such, we build a list of redirections here. These are also useful because Chapter 6 uses different versions of the same
-- maps at certain times to cause special effects, such as the changing seasons.
--IMPORTANT: Once the remappings are built, they should not be reset or modified during level loading. This will cause a crash.
-- Only modify them during an in-map script or before level loading occurs.

--[Master List]
--All additions are added to this list. This is the one used for the C++ code.
saMasterList = {}

--[ ======================================= Adder Function ====================================== ]
--Adds a remapping to a sublist.
local fnAddRemapping = function(saList, sStartName, sRemapName)
	
	--Baseline List. Used for debug. Makes it easier to diagnose a missing level.
	local iListLen = #saList
	saList[iListLen+1] = {}
	saList[iListLen+1].sStartName = sStartName
	saList[iListLen+1].sRemapName = sRemapName
	
	--Master list.
	iListLen = #saMasterList
	saMasterList[iListLen+1] = {}
	saMasterList[iListLen+1].sStartName = sStartName
	saMasterList[iListLen+1].sRemapName = sRemapName
end

--[ ====================================== Patterned Adder ====================================== ]
--Adds a set of remappings linearly across a set of letters. Used for simple level names in a series.
local fnAddRemapsPattern = function(psList, psName, psRemap, psStartLetter, psEndLetter)
	
	--Arg check.
	if(psList == nil) then return end
	if(psName == nil) then return end
	if(psRemap == nil) then return end
	if(psStartLetter == nil) then return end
	if(psEndLetter == nil) then return end
	
	--Setup.
	local sCurrentLetter = psStartLetter
	
	--Range check. If the letter is out of the normal range, fail.
	local iByteValue = string.byte(sCurrentLetter)
	if(iByteValue < string.byte("A") or iByteValue > string.byte("Z")) then return end
	
	--Iterate across the letters. Always stops on Z. Also always stops on non-capitals.
	while(true) do
		
		--Add the remap.
		local sLevelName = psName .. sCurrentLetter
		local sRemapName = psRemap .. sCurrentLetter
		fnAddRemapping(psList, sLevelName, sRemapName)
		
		--If this was the ending letter, or the letter 'Z', stop.
		if(sCurrentLetter == psEndLetter or sCurrentLetter == "Z") then break end
		
		--Move to the next letter.
		local iByteValue = string.byte(sCurrentLetter)
		sCurrentLetter = string.char(iByteValue + 1)
	end
end

--[ ===================================== Trannadar Region ====================================== ]
--[Beehive and Dungeon]
local saBeehiveList = {}
fnAddRemapsPattern(saBeehiveList, "BeehiveBasement", "Beehive/BeehiveBasement", "A", "F")
fnAddRemapping(saBeehiveList, "BeehiveBasementScene", "Beehive/BeehiveBasementScene")
fnAddRemapping(saBeehiveList, "BeehiveInner",         "Beehive/BeehiveInner")
fnAddRemapping(saBeehiveList, "BeehiveOuter",         "Beehive/BeehiveOuter")
fnAddRemapping(saBeehiveList, "BeehiveSpecial",         "Beehive/BeehiveSpecial")

--[Trannadar and Evermoon]
local saTrannadarList = {}
fnAddRemapping(saTrannadarList, "TrannadarTradingPost",  "Evermoon/TrannadarTradingPost")
fnAddRemapping(saTrannadarList, "AlrauneAndBeeScene",    "Evermoon/AlrauneAndBeeScene")
fnAddRemapping(saTrannadarList, "AlrauneChamber",        "Evermoon/AlrauneChamber")
fnAddRemapping(saTrannadarList, "BreannesPitStop",       "Evermoon/BreannesPitStop")
fnAddRemapping(saTrannadarList, "EvermoonCassandraA",    "Evermoon/EvermoonCassandraA")
fnAddRemapping(saTrannadarList, "EvermoonCassandraAMid", "Evermoon/EvermoonCassandraAMid")
fnAddRemapping(saTrannadarList, "EvermoonCassandraB",    "Evermoon/EvermoonCassandraB")
fnAddRemapping(saTrannadarList, "EvermoonCassandraCC",   "Evermoon/EvermoonCassandraCC")
fnAddRemapping(saTrannadarList, "EvermoonCassandraCE",   "Evermoon/EvermoonCassandraCE")
fnAddRemapping(saTrannadarList, "EvermoonCassandraCNE",  "Evermoon/EvermoonCassandraCNE")
fnAddRemapping(saTrannadarList, "EvermoonCassandraCNW",  "Evermoon/EvermoonCassandraCNW")
fnAddRemapping(saTrannadarList, "EvermoonE",             "Evermoon/EvermoonE")
fnAddRemapping(saTrannadarList, "EvermoonNE",            "Evermoon/EvermoonNE")
fnAddRemapping(saTrannadarList, "EvermoonNW",            "Evermoon/EvermoonNW")
fnAddRemapping(saTrannadarList, "EvermoonS",             "Evermoon/EvermoonS")
fnAddRemapping(saTrannadarList, "EvermoonSEA",           "Evermoon/EvermoonSEA")
fnAddRemapping(saTrannadarList, "EvermoonSEB",           "Evermoon/EvermoonSEB")
fnAddRemapping(saTrannadarList, "EvermoonSEC",           "Evermoon/EvermoonSEC")
fnAddRemapping(saTrannadarList, "EvermoonSlimeScene",    "Evermoon/EvermoonSlimeScene")
fnAddRemapping(saTrannadarList, "EvermoonSlimeVillage",  "Evermoon/EvermoonSlimeVillage")
fnAddRemapping(saTrannadarList, "EvermoonSW",            "Evermoon/EvermoonSW")
fnAddRemapping(saTrannadarList, "EvermoonW",             "Evermoon/EvermoonW")
fnAddRemapping(saTrannadarList, "PlainsC",               "Evermoon/PlainsC")
fnAddRemapping(saTrannadarList, "PlainsNW",              "Evermoon/PlainsNW")
fnAddRemapping(saTrannadarList, "SaltFlats",             "Evermoon/SaltFlats")
fnAddRemapping(saTrannadarList, "SaltFlatsCave",         "Evermoon/SaltFlatsCave")
fnAddRemapping(saTrannadarList, "WerecatScene",          "Evermoon/WerecatScene")
fnAddRemapping(saTrannadarList, "Island",                "Evermoon/Island")

--[Dimensional Trap Basement]
local saTrapBasementList = {}
fnAddRemapsPattern(saTrapBasementList, "TrapBasement",                 "TrapBasement/TrapBasement", "A", "H")
fnAddRemapping    (saTrapBasementList, "TrapBasementSecret",           "TrapBasement/TrapBasementSecret")
fnAddRemapping    (saTrapBasementList, "DimensionalTrapBasementScene", "TrapBasement/DimensionalTrapBasementScene")

--[Dimensional Trap Dungeon]
local saTrapDungeonList = {}
fnAddRemapsPattern(saTrapDungeonList, "TrapDungeon",      "TrapDungeon/TrapDungeon", "A", "F")
fnAddRemapping    (saTrapDungeonList, "TrapDungeonEntry", "TrapDungeon/TrapDungeonEntry")

--[Dimensional Trap Main Floor and Upper Floor]
local saDimensionalTrapList = {}
fnAddRemapping(saTrapDungeonList, "TrapMainFloorCentral",   "DimensionalTrap/TrapMainFloorCentral")
fnAddRemapping(saTrapDungeonList, "TrapMainFloorEast",      "DimensionalTrap/TrapMainFloorEast")
fnAddRemapping(saTrapDungeonList, "TrapMainFloorExterior",  "DimensionalTrap/TrapMainFloorExterior")
fnAddRemapping(saTrapDungeonList, "TrapMainFloorExteriorN", "DimensionalTrap/TrapMainFloorExteriorN")
fnAddRemapping(saTrapDungeonList, "TrapMainFloorSouthHall", "DimensionalTrap/TrapMainFloorSouthHall")
fnAddRemapping(saTrapDungeonList, "TrapUpperFloorE",        "DimensionalTrap/TrapUpperFloorE")
fnAddRemapping(saTrapDungeonList, "TrapUpperFloorMain",     "DimensionalTrap/TrapUpperFloorMain")
fnAddRemapping(saTrapDungeonList, "TrapUpperFloorW",        "DimensionalTrap/TrapUpperFloorW")

--[ ======================================== Void Region ======================================== ]
--[Nix Nedar]
local saNixNedarList = {}
fnAddRemapping(saNixNedarList, "NixNedarHouseEast",        "NixNedar/NixNedarHouseEast")
fnAddRemapping(saNixNedarList, "NixNedarHousePath",        "NixNedar/NixNedarHousePath")
fnAddRemapping(saNixNedarList, "NixNedarHouseSouthcenter", "NixNedar/NixNedarHouseSouthcenter")
fnAddRemapping(saNixNedarList, "NixNedarHouseSouthwest",   "NixNedar/NixNedarHouseSouthwest")
fnAddRemapping(saNixNedarList, "NixNedarMain",             "NixNedar/NixNedarMain")
fnAddRemapping(saNixNedarList, "NixNedarMaramHouse",       "NixNedar/NixNedarMaramHouse")
fnAddRemapping(saNixNedarList, "NixNedarSeptimaHouse",     "NixNedar/NixNedarSeptimaHouse")
fnAddRemapping(saNixNedarList, "NixNedarSouthPath",        "NixNedar/NixNedarSouthPath")
fnAddRemapping(saNixNedarList, "NixNedarSouthPathWander",  "NixNedar/NixNedarSouthPathWander")

--[ ====================================== Quantir Region ======================================= ]
--[Quantir High Wastes]
local saQuantirList = {}
fnAddRemapping(saQuantirList, "QuantirNW",     "Quantir/QuantirNW")
fnAddRemapping(saQuantirList, "QuantirNWCave", "Quantir/QuantirNWCave")

--[Quantir Mansion]
local saQuantirManseList = {}
fnAddRemapping(saQuantirManseList, "QuantirManseBasementE",      "QuantirManse/QuantirManseBasementE")
fnAddRemapping(saQuantirManseList, "QuantirManseBasementW",      "QuantirManse/QuantirManseBasementW")
fnAddRemapping(saQuantirManseList, "QuantirManseCentralE",       "QuantirManse/QuantirManseCentralE")
fnAddRemapping(saQuantirManseList, "QuantirManseCentralW",       "QuantirManse/QuantirManseCentralW")
fnAddRemapping(saQuantirManseList, "QuantirManseEntrance",       "QuantirManse/QuantirManseEntrance")
fnAddRemapping(saQuantirManseList, "QuantirManseNEHall",         "QuantirManse/QuantirManseNEHall")
fnAddRemapping(saQuantirManseList, "QuantirManseNEHallFloor2",   "QuantirManse/QuantirManseNEHallFloor2")
fnAddRemapping(saQuantirManseList, "QuantirManseNWHall",         "QuantirManse/QuantirManseNWHall")
fnAddRemapping(saQuantirManseList, "QuantirManseSecretExit",     "QuantirManse/QuantirManseSecretExit")
fnAddRemapping(saQuantirManseList, "QuantirManseSEHall",         "QuantirManse/QuantirManseSEHall")
fnAddRemapping(saQuantirManseList, "QuantirManseSWYard",         "QuantirManse/QuantirManseSWYard")
fnAddRemapping(saQuantirManseList, "QuantirManseSWYardInterior", "QuantirManse/QuantirManseSWYardInterior")
fnAddRemapping(saQuantirManseList, "QuantirManseTruth",          "QuantirManse/QuantirManseTruth")
fnAddRemapping(saQuantirManseList, "SpookyExterior",             "QuantirManse/SpookyExterior")


--[ ====================================== Regulus Region ======================================= ]
--[Regulus Cryogenics Facility]
local saRegulusCryoList = {}
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryo",              "RegulusCryo/RegulusCryo",              "A", "G")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoLower",         "RegulusCryo/RegulusCryoLower",         "A", "D")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoContainment",   "RegulusCryo/RegulusCryoContainment",   "A", "B")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoToFabrication", "RegulusCryo/RegulusCryoToFabrication", "A", "B")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoCommand",       "RegulusCryo/RegulusCryoCommand",       "A", "C")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoSouth",         "RegulusCryo/RegulusCryoSouth",         "A", "G")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoPowerCore",     "RegulusCryo/RegulusCryoPowerCore",     "A", "E")

--[Regulus Exterior]
local saRegulusExteriorList = {}
fnAddRemapsPattern(saRegulusExteriorList, "RegulusExteriorE",        "RegulusExterior/RegulusExteriorE", "A", "F")
fnAddRemapsPattern(saRegulusExteriorList, "RegulusExteriorS",        "RegulusExterior/RegulusExteriorS", "A", "E")
fnAddRemapsPattern(saRegulusExteriorList, "RegulusExteriorW",        "RegulusExterior/RegulusExteriorW", "A", "C")
fnAddRemapping    (saRegulusExteriorList, "RegulusExteriorStationE", "RegulusExterior/RegulusExteriorStationE")
fnAddRemapping    (saRegulusExteriorList, "RegulusExteriorWAUp",     "RegulusExterior/RegulusExteriorWAUp")
fnAddRemapping    (saRegulusExteriorList, "RegulusExteriorTRNA",     "RegulusExterior/RegulusExteriorTRNA")
fnAddRemapping    (saRegulusExteriorList, "RegulusExteriorTRNB",     "RegulusExterior/RegulusExteriorTRNB")

--[Serenity Crater and Observatory]
local saRegulusSerenityList = {}
fnAddRemapsPattern(saRegulusSerenityList, "SerenityCrater",      "RegulusSerenity/SerenityCrater",      "A", "H")
fnAddRemapsPattern(saRegulusSerenityList, "SerenityObservatory", "RegulusSerenity/SerenityObservatory", "A", "E")

--[Regulus City]
local saRegulusCityList = {}
fnAddRemapsPattern(saRegulusCityList, "RegulusCity15",     "RegulusCity/RegulusCity15",    "A", "C")
fnAddRemapsPattern(saRegulusCityList, "RegulusCity119",    "RegulusCity/RegulusCity119",   "A", "B")
fnAddRemapsPattern(saRegulusCityList, "RegulusCity198",    "RegulusCity/RegulusCity198",   "A", "D")
fnAddRemapsPattern(saRegulusCityList, "RegulusCity",       "RegulusCity/RegulusCity",      "A", "G")
fnAddRemapsPattern(saRegulusCityList, "LowerRegulusCity",  "RegulusCity/LowerRegulusCity", "A", "D")
fnAddRemapping    (saRegulusCityList, "RegulusCityX",      "RegulusCity/RegulusCityX")
fnAddRemapping    (saRegulusCityList, "RegulusCityZ",      "RegulusCity/RegulusCityZ")
fnAddRemapping    (saRegulusCityList, "RegulusCityBAlt",   "RegulusCity/RegulusCityBAlt")

--Cannot be visited normally. Used for the dating montage.
fnAddRemapping(saRegulusCityList, "RegulusCityWholeCutscene", "RegulusCity/RegulusCityWholeCutscene")

--[Regulus Equinox Facility]
local saRegulusEquinoxList = {}
fnAddRemapsPattern(saRegulusEquinoxList, "RegulusEquinox", "RegulusEquinox/RegulusEquinox", "A", "H")

--[Regulus Long-Range Telemetry Facility]
local saRegulusLRTList = {}
fnAddRemapsPattern(saRegulusLRTList, "RegulusLRT",   "RegulusLRT/RegulusLRT", "A", "H")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTEA", "RegulusLRT/RegulusLRTEA")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTGX", "RegulusLRT/RegulusLRTGX")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTGZ", "RegulusLRT/RegulusLRTGZ")
fnAddRemapsPattern(saRegulusLRTList, "RegulusLRTH",  "RegulusLRT/RegulusLRTH", "A", "F")
fnAddRemapsPattern(saRegulusLRTList, "RegulusLRTI",  "RegulusLRT/RegulusLRTI", "A", "G")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTZA", "RegulusLRT/RegulusLRTZA")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTZB", "RegulusLRT/RegulusLRTZB")

--[Regulus Manufactory]
local saRegulusManufactoryList = {}
fnAddRemapsPattern(saRegulusManufactoryList, "RegulusManufactory", "RegulusManufactory/RegulusManufactory", "A", "J")

--[Regulus Mines]
local saRegulusMinesList = {}
fnAddRemapsPattern(saRegulusMinesList, "TelluriumMines", "RegulusMines/TelluriumMines", "A", "I")
fnAddRemapsPattern(saRegulusMinesList, "SprocketCity",   "RegulusMines/SprocketCity",   "A", "B")
fnAddRemapsPattern(saRegulusMinesList, "BlackSite",      "RegulusMines/BlackSite",      "A", "D")
fnAddRemapsPattern(saRegulusMinesList, "MinesRandom",    "RegulusMines/MinesRandom",    "A", "D")

--[Arcane Academy]
local saArcaneAcademyList = {}
fnAddRemapsPattern(saArcaneAcademyList, "RegulusArcane",     "RegulusArcane/RegulusArcane", "A", "H")
fnAddRemapping    (saArcaneAcademyList, "RegulusArcaneCAlt", "RegulusArcane/RegulusArcaneCAlt")

--[Biolabs]
local saBiolabsList = {}
fnAddRemapping(saBiolabsList, "RegulusBiolabsA", "RegulusBiolabs/RegulusBiolabsAlphaA")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsAlpha",       "RegulusBiolabs/RegulusBiolabsAlpha",       "A", "D")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsAmphibian",   "RegulusBiolabs/RegulusBiolabsAmphibian",   "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsBeta",        "RegulusBiolabs/RegulusBiolabsBeta",        "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsBetaMelted",  "RegulusBiolabs/RegulusBiolabsBetaMelted",  "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsDatacore",    "RegulusBiolabs/RegulusBiolabsDatacore",    "A", "H")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsDelta",       "RegulusBiolabs/RegulusBiolabsDelta",       "A", "I")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsEpsilon",     "RegulusBiolabs/RegulusBiolabsEpsilon",     "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsGamma",       "RegulusBiolabs/RegulusBiolabsGamma",       "A", "D")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsGammaWest",   "RegulusBiolabs/RegulusBiolabsGammaWest",   "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsGenetics",    "RegulusBiolabs/RegulusBiolabsGenetics",    "A", "H")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsHydroponics", "RegulusBiolabs/RegulusBiolabsHydroponics", "A", "D")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsRaijuRanch",  "RegulusBiolabs/RegulusBiolabsRaijuRanch",  "A", "C")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsTransit",     "RegulusBiolabs/RegulusBiolabsTransit",     "A", "B")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsMovie",       "RegulusBiolabs/RegulusBiolabsMovie",       "A", "G")

--[Flashback and Finale]
local saFlashbackList = {}
fnAddRemapsPattern(saFlashbackList, "RegulusFlashback", "RegulusFlashback/RegulusFlashback", "A", "L")
fnAddRemapsPattern(saFlashbackList, "RegulusFinale",    "RegulusFinale/RegulusFinale",       "A", "H")

--[ ======================================= Trafal Region ======================================= ]
--[Trafal Glacier]
local saTrafalList = {}
fnAddRemapping(saTrafalList, "TrafalNW", "Trafal/TrafalNW")

--[ ==================================== Finalize and Upload ==================================== ]
--[Finalize]
--Using the master list, upload this to the C++ code.
AL_SetProperty("Remappings Total", #saMasterList)
for i = 1, #saMasterList, 1 do
	AL_SetProperty("Remapping", i-1, saMasterList[i].sStartName, saMasterList[i].sRemapName)
end
