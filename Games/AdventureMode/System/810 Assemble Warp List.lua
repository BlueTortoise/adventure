--[Assemble Warp List]
--This script is fired when the player selects "Warp" from the save menu. It goes through the game's conditions and assembles
-- a list of locations the player may warp to.
--The list is always clear before this executes and this script must re-build it each time.
--Generally speaking, these are set by activating the campfire in each area. It is possible to have no entries on the list.
-- This script is always fired with the name of the current room as the 0th argument. It should not be possible to warp
-- to the current location!

--[Argument Check]
--Verify.
if(fnArgCheck(1) == false) then return end

--Set.
local sCurrentRoomName = LM_GetScriptArgument(0)

--If the current room's name is "Nowhere", we can't warp.
if(sCurrentRoomName == "Nowhere") then return end

--Get which chapter we're currently playing.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

--[Variable Registry]
--Variable declaration.
local iListSize = 0
local zaList = {}

--Adder function.
local fnAddEntry = function(sRegionName, sDisplayName, sRoomName, sVariablePath)
	
	--Increase list size.
	iListSize = iListSize + 1
	
	--Add entry.
	zaList[iListSize] = {}
	zaList[iListSize].sRegionName = sRegionName
	zaList[iListSize].sDisplayName = sDisplayName
	zaList[iListSize].sRoomName = sRoomName
	zaList[iListSize].sVariablePath = sVariablePath
	zaList[iListSize].sMapName = "No Map"
	zaList[iListSize].fMapX = 0.0
	zaList[iListSize].fMapY = 0.0
	
	--If this variable has not been unlocked before, unlock it now.
	if(sRoomName == sCurrentRoomName) then
		VM_SetVar(sVariablePath, "N", 1.0)
	end
    
    --Find the entry on the map lookups list.
    local iCanUseRoom = VM_GetVar(sVariablePath, "N")
    if(iCanUseRoom == 1.0) then
        for i = 1, #gsaMapLookups, 1 do
            if(gsaMapLookups[i][1] == sRoomName) then
                zaList[iListSize].sMapName = gsaMapLookups[i][2]
                zaList[iListSize].fMapX = gsaMapLookups[i][3]
                zaList[iListSize].fMapY = gsaMapLookups[i][4]
            end
        end
    end
end

--Special: Use override maps.
local fnAddEntryAdv = function(sRegionName, sDisplayName, sRoomName, sVariablePath, sCallPath, fMapX, fMapY)
	
	--Increase list size.
	iListSize = iListSize + 1
	
	--Add entry.
	zaList[iListSize] = {}
	zaList[iListSize].sRegionName = sRegionName
	zaList[iListSize].sDisplayName = sDisplayName
	zaList[iListSize].sRoomName = sRoomName
	zaList[iListSize].sVariablePath = sVariablePath
	zaList[iListSize].sMapName = sCallPath
	zaList[iListSize].fMapX = fMapX
	zaList[iListSize].fMapY = fMapY
	
	--If this variable has not been unlocked before, unlock it now.
	if(sRoomName == sCurrentRoomName) then
		VM_SetVar(sVariablePath, "N", 1.0)
	end
    
end

--[Chapter 1]
--Associate the room names with variable names. This is done in a parallel-list format to make iteration easier.
if(iCurrentChapter == 1) then

    fnAddEntry("Trannadar Province", "Dimensional Trap Basement East", "TrapBasementB",      "Root/Variables/Chapter1/Campfires/iTrapBasementB") 
    fnAddEntry("Trannadar Province", "Dimensional Trap Basement West", "TrapBasementG",      "Root/Variables/Chapter1/Campfires/iTrapBasementG")
    fnAddEntry("Trannadar Province", "Trannadar Trading Post",         "EvermoonW",          "Root/Variables/Chapter1/Campfires/iEvermoonW")
    fnAddEntry("Trannadar Province", "Evermoon Forest South",          "EvermoonS",          "Root/Variables/Chapter1/Campfires/iEvermoonS")
    fnAddEntry("Trannadar Province", "Evermoon Forest East",           "EvermoonE",          "Root/Variables/Chapter1/Campfires/iEvermoonE")
    fnAddEntry("Trannadar Province", "Evermoon Forest North",          "EvermoonCassandraA", "Root/Variables/Chapter1/Campfires/iEvermoonCassandraA")
    fnAddEntry("Trannadar Province", "Breanne's Pit Stop",             "PlainsC",            "Root/Variables/Chapter1/Campfires/iPlainsC")
    fnAddEntry("Trannadar Province", "Bee Hive Basement",              "BeehiveBasementA",   "Root/Variables/Chapter1/Campfires/iBeehiveBasementA")
    fnAddEntry("Trannadar Province", "Quantir Estate Grounds",         "SpookyExterior",     "Root/Variables/Chapter1/Campfires/iSpookyExterior")
    fnAddEntry("Trannadar Province", "Dimensional Trap Dungeon",       "TrapDungeonA",       "Root/Variables/Chapter1/Campfires/iTrapDungeonA")
    fnAddEntry("Trannadar Province", "Alicia's Cabin",                 "EvermoonSEA",        "Root/Variables/Chapter1/Campfires/iEvermoonSEA")

--[Chapter 5]
--Same deal but on Regulus.
elseif(iCurrentChapter == 5) then

    --If this flag is set, warping is disabled.
    local iNoWarpsAllowed = VM_GetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N")
    if(iNoWarpsAllowed == 1.0) then return end
    
    --Biolabs.
    local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")

    --If currently in Sprocket City and we have not completed the Sprocket City quest chain, then the
    -- only place we can warp is the outskirts.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(sCurrentRoomName == "SprocketCityA" and iCompletedSprocketCity == 0.0) then
        fnAddEntry("Regulus", "Sprocket City Outskirts",  "TelluriumMinesE",   "Root/Variables/Chapter5/Campfires/iTelluriumMinesE") 
    
    --Not-Biolabs. First part of the chapter.
    elseif(iReachedBiolabs == 0.0) then
        fnAddEntry("Regulus",      "Regulus City: Sector 96",  "RegulusCityC",         "Root/Variables/Chapter5/Campfires/iRegulusCityC") 
        fnAddEntry("Regulus",      "Sector 96 Basement",       "LowerRegulusCityB",    "Root/Variables/Chapter5/Campfires/iLowerRegulusCityB") 
        fnAddEntry("Regulus",      "Sector 99 Lower",          "RegulusManufactoryC",  "Root/Variables/Chapter5/Campfires/iRegulusManufactoryC") 
        fnAddEntryAdv("Cryogenics","Cryogenics Facility",      "RegulusCryoC",         "Root/Variables/Chapter5/Campfires/iRegulusCryoC", "CALL|Maps/Z Map Lookups/WarpSet Cryogenics.lua", 996, 1148) 
        fnAddEntry("Regulus",      "Regulus City: Sector 15",  "RegulusCity15C",       "Root/Variables/Chapter5/Campfires/iRegulusCity15C") 
        fnAddEntry("Regulus",      "Eastern Regulus Surface",  "RegulusExteriorEA",    "Root/Variables/Chapter5/Campfires/iRegulusExteriorEA") 
        fnAddEntry("Regulus",      "Southern Regulus Surface", "RegulusExteriorSB",    "Root/Variables/Chapter5/Campfires/iRegulusExteriorSB") 
        fnAddEntry("Regulus",      "Western Regulus Surface",  "RegulusExteriorWA",    "Root/Variables/Chapter5/Campfires/iRegulusExteriorWA") 
        fnAddEntryAdv("LRT East",  "Entrance",                 "RegulusLRTA",          "Root/Variables/Chapter5/Campfires/iRegulusLRTA",     "CALL|Maps/Z Map Lookups/WarpSet LRT East.lua", 987,  515) 
        fnAddEntryAdv("LRT West",  "West Overlook",            "RegulusLRTF",          "Root/Variables/Chapter5/Campfires/iRegulusLRTF",     "CALL|Maps/Z Map Lookups/WarpSet LRT West.lua", 918, 1241) 
        fnAddEntryAdv("LRT West",  "Datacore",                 "RegulusLRTID",         "Root/Variables/Chapter5/Campfires/iRegulusLRTID",    "CALL|Maps/Z Map Lookups/WarpSet LRT West.lua", 819,  406) 
        fnAddEntryAdv("Equinox",   "Equinox Labs",             "RegulusEquinoxA",      "Root/Variables/Chapter5/Campfires/iRegulusEquinoxA", "CALL|Maps/Z Map Lookups/WarpSet Equinox.lua", 1266, 608) 
        fnAddEntry("Regulus",      "Serenity Observatory",     "SerenityObservatoryE", "Root/Variables/Chapter5/Campfires/iSerenityObservatoryE")
        fnAddEntry("Regulus",      "Serenity Crater Shelf",    "SerenityCraterD",      "Root/Variables/Chapter5/Campfires/SerenityCraterD")
        fnAddEntry("Regulus",      "Serenity Research Lab",    "SerenityCraterF",      "Root/Variables/Chapter5/Campfires/iSerenityCraterF")
        
        --Sprocket City. Can only be accessed if the quest chain is completed.
        if(iCompletedSprocketCity == 1.0) then
            fnAddEntry("Regulus", "Fist of the Future HQ", "SprocketCityA", "Root/Variables/Chapter5/Campfires/iSprocketA") 
        end
        fnAddEntry("Regulus", "Tellurium Mines Entrance", "TelluriumMinesB",   "Root/Variables/Chapter5/Campfires/iTelluriumMinesB") 
        fnAddEntry("Regulus", "Sprocket City Outskirts",  "TelluriumMinesE",   "Root/Variables/Chapter5/Campfires/iTelluriumMinesE") 
        fnAddEntry("Regulus", "Mines Black Site",         "BlackSiteA",        "Root/Variables/Chapter5/Campfires/iBlackSiteA") 
    
    --Biolabs.
    else
        fnAddEntry("Biolabs", "Alpha Laboratories",        "RegulusBiolabsAlphaA",    "Root/Variables/Chapter5/Campfires/iRegulusBiolabsAlphaA") 
        fnAddEntry("Biolabs", "Gamma Checkpoint",          "RegulusBiolabsGammaA",    "Root/Variables/Chapter5/Campfires/iRegulusBiolabsGammaA") 
        fnAddEntry("Biolabs", "Raiju Ranch Entrance",      "RegulusBiolabsDeltaB",    "Root/Variables/Chapter5/Campfires/iRegulusBiolabsDeltaB") 
        fnAddEntry("Biolabs", "Aquatic Genetics Building", "RegulusBiolabsGeneticsA", "Root/Variables/Chapter5/Campfires/iRegulusBiolabsGeneticsA") 
    end

end

--[Remaps]
--Clean any outstanding remaps.
AM_SetProperty("Clear Region Images")

--Chapter 1.
AM_SetProperty("Register Warp Region Image", "Trannadar Province", "Root/Images/AdventureUI/CampfireMenu/ICO|Trannadar")

--Chapter 5.
AM_SetProperty("Register Warp Region Image", "Regulus",    "Root/Images/AdventureUI/CampfireMenu/ICO|Regulus")
AM_SetProperty("Register Warp Region Image", "Cryogenics", "Root/Images/AdventureUI/CampfireMenu/ICO|Cryogenics")
AM_SetProperty("Register Warp Region Image", "Equinox",    "Root/Images/AdventureUI/CampfireMenu/ICO|Equinox")
AM_SetProperty("Register Warp Region Image", "LRT East",   "Root/Images/AdventureUI/CampfireMenu/ICO|LRTE")
AM_SetProperty("Register Warp Region Image", "LRT West",   "Root/Images/AdventureUI/CampfireMenu/ICO|LRTW")
AM_SetProperty("Register Warp Region Image", "Biolabs",    "Root/Images/AdventureUI/CampfireMenu/ICO|Biolabs")

--[Execution]
--Go through the variables and add entries for all of them. If less than one entry is in place, the menu cannot be opened.
-- We also check to make sure the current room is not added, since we can't warp to where we are.
for i = 1, iListSize, 1 do
	
	--If this is the current room, ignore it:
	if(zaList[i].sRoomName == sCurrentRoomName) then
		
	--Otherwise, check the variable and add it.
	else
	
		--Get the variable. It has to be 1 to be usable.
		local iIsUnlocked = VM_GetVar(zaList[i].sVariablePath, "N")
		if(iIsUnlocked == 1.0) then

            --Figure out the overlay. Not all maps have these. "Null" is a valid case.
            local sOverlayName = "Null"
            if(zaList[i].sMapName == "TrannadarMap") then sOverlayName = "TrannadarMapOverlay" end
            if(zaList[i].sMapName == "RegulusMap")   then sOverlayName = "RegulusMapOverlay"   end

            --Register.
			AM_SetProperty("Register Warp Destination", zaList[i].sRegionName, zaList[i].sDisplayName, zaList[i].sRoomName, zaList[i].sMapName, sOverlayName, zaList[i].fMapX, zaList[i].fMapY)
		end
	end
	
end
