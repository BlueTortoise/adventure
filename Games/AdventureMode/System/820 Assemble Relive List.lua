--[Assemble Relive List]
--This script is fired when the save menu opens up. It will resolve the list of scenes the player can "relive",
-- which involves playing through them again until a certain point, at which they warp back to their last save.
--It is possible to have no scenes available, in which case the menu doesn't open. The menu is clear before this fires.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

--If currently in the "Nowhere" map, don't generate anything.
local sName = AL_GetProperty("Name")
if(sName == "Nowhere") then return end

--[ ======================================= Chapter 1: Mei ====================================== ]
if(iCurrentChapter == 1.0) then
    
    --Get variables.
    local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
    local iHasBeeForm     = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm",     "N")
    local iHasSlimeForm   = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm",   "N")
    local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
    local iHasGhostForm   = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm",   "N")

    --Alraune transformation:
    if(iHasAlrauneForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Alraune", gsStandardReliveBegin, "Root/Images/Sprites/Mei_Alraune/SW|0")
    end

    --Bee transformation:
    if(iHasBeeForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Bee", gsStandardReliveBegin, "Root/Images/Sprites/Mei_Bee/SW|0")
    end

    --Ghost transformation:
    if(iHasGhostForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Ghost", gsStandardReliveBegin, "Root/Images/Sprites/Mei_Ghost/SW|0")
    end

    --Slime transformation:
    if(iHasSlimeForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Slime", gsStandardReliveBegin, "Root/Images/Sprites/Mei_Slime/SW|0")
    end

    --Werecat transformation:
    if(iHasWerecatForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Werecat", gsStandardReliveBegin, "Root/Images/Sprites/Mei_Werecat/SW|0")
    end

--[ =================================== Chapter 5: Christine ==================================== ]
elseif(iCurrentChapter == 5.0) then

    --Get variables.
    local iHasDollForm          = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
    local iHasGolemForm         = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    local iHasLatexForm         = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
    local iHasDarkmatterForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local iHasEldritchForm      = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
    local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
    local iHasSteamDroidForm    = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
    local iHasRaijuForm         = VM_GetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N")
    
    --Doll:
    if(iHasDollForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Doll", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_Begin.lua", "Root/Images/Sprites/Christine_Doll/SW|0")
    end
    
    --Golem:
    if(iHasGolemForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Golem", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_Begin.lua", "Root/Images/Sprites/Christine_Golem/SW|0")
    end
    
    --Latex Drone:
    if(iHasLatexForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Latex Drone", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_Begin.lua", "Root/Images/Sprites/Christine_Latex/SW|0")
    end
    
    --Darkmatter
    if(iHasDarkmatterForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Darkmatter", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_Begin.lua", "Root/Images/Sprites/Christine_Darkmatter/SW|0")
    end

    --Eldritch
    if(iHasEldritchForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Dreamer", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_Begin.lua", "Root/Images/Sprites/Christine_DreamGirl/SW|0")
    end
    
    --Electrosprite
    if(iHasElectrospriteForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Electrosprite", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_Begin.lua", "Root/Images/Sprites/Christine_Electrosprite/SW|0")
    end
    
    --Steam Droid
    if(iHasSteamDroidForm == 1.0) then
        AM_SetProperty("Register Relive Script", "SteamDroid", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_Begin.lua", "Root/Images/Sprites/Christine_SteamDroid/SW|0")
    end
    
    --Raiju
    if(iHasRaijuForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Raiju", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_Begin.lua", "Root/Images/Sprites/Christine_RaijuClothes/SW|0")
    end
end