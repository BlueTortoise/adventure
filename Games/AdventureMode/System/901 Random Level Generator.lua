--[Random Level Generator]
--When the program needs a random level to be generated, this script is called in stride.

--[Generator Check]
--We need to store the active path in the DataLibrary. The game needs to know it when launching save files.
-- This information should have built on menu startup.
local iGameIndex = 0
for i = 1, giGamesTotal, 1 do
    if(gsaGameEntries[i].sName == "Level Generator") then
        iGameIndex = i
        break
    end
end

--Error: Didn't find an entry for it, somehow.
if(iGameIndex == 0) then return end

--If the active path is somehow "Null", it didn't get set. Fail.
if(gsaGameEntries[iGameIndex].sActivePath == "Null") then return end

--Otherwise, store the active path.
DL_AddPath("Root/Paths/System/Startup/")
VM_SetVar("Root/Paths/System/Startup/sLevelGeneratorPath", "S", gsaGameEntries[iGameIndex].sActivePath)

--Create level generator.
AdlevGenerator_Create()

--Texture settings.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)

--Clean.
SLF_Close()
ALB_SetTextureProperty("Restore Defaults")

--Which texture to use.
local iMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N")
--if(iMinesFloor <= 30 or iMinesFloor >= 40) then
    AdlevGenerator_SetProperty("Texture Path", "Root/Images/Sprites/LevelTextures/RegulusCave")
--else
--    AdlevGenerator_SetProperty("Texture Path", "Root/Images/Sprites/LevelTextures/RegulusCaveFungus")
--end

--Tell the level to use this texture mapping.
AdlevGenerator_SetProperty("Pattern TL", 0, 0)
AdlevGenerator_SetProperty("Floors Total", 4)
AdlevGenerator_SetProperty("Floor Position", 0,  0, 112)
AdlevGenerator_SetProperty("Floor Position", 1, 16, 112)
AdlevGenerator_SetProperty("Floor Position", 2,  0, 128)
AdlevGenerator_SetProperty("Floor Position", 3, 16, 128)

--Other UV Positions
AdlevGenerator_SetProperty("Blackout Pos", 6, 0)
AdlevGenerator_SetProperty("Ladder Pos", 3, 4, 4, 4)
AdlevGenerator_SetProperty("Cliff Pos", 0, 9)
AdlevGenerator_SetProperty("Floor Edge Pos", 3, 0)
AdlevGenerator_SetProperty("Treasure Pos", 3, 3)
AdlevGenerator_SetProperty("Enemy Pos", 5, 4)

--Build grid information.
LM_ExecuteScript(gsaGameEntries[iGameIndex].sActivePath)