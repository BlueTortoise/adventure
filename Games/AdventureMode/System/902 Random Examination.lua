--[Random Floor Examinables]
--These are the ladders up and down. This script handles their examination. Some other levels share their properties
-- and thus will call this script.
--The script modifies a variable, gbCaughtScript, if it handled the examination case. It expects the same arguments
-- an examination script usually expects.
--This script's path is stored in the variable gsRandomExaminationHandler.
gbCaughtScript = false

--[Arguments]
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--[Exits]
--Down to a randomly generated level.
if(sObjectName == "LadderD" or sObjectName == "Exit") then
    
    --[Baseline]
    --Flag to indicate we caught an examine case.
    gbCaughtScript = true
    
	--SFX.
	AudioManager_PlaySound("World|ClimbLadder")
    
	--Get the current mines floor.
	local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N")
	
	--Add one to the floor counter.
	iCurrentMinesFloor = iCurrentMinesFloor + 1
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", iCurrentMinesFloor)
    
    --[Roller]
    --If the random floors haven't been rolled yet, do that here.
    local iHasRolledFloors = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasRolledFloors", "N")
    if(iHasRolledFloors == 0.0) then
        LM_ExecuteScript(gsRoot .. "System/903 Random Floor Roller.lua")
    end
    
    --[Variables]
    local iMineAFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineAFloor", "N")
    local iMineBFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBFloor", "N")
    local iMineCFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineCFloor", "N")
    local iMineDFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDFloor", "N")
    
    --When the player has left Mines B after getting the ammo, throw this flag.
    local iMineBGotAmmo      = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N")
    if(iCurrentMinesFloor-1 == iMineBFloor and iMineBGotAmmo == 2.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBLeft", "N", 1.0)
    end
    
    --[Special Case Handlers]
	--Special case: Floor 10 is TelluriumMinesC:
	if(iCurrentMinesFloor == 10) then
		AL_BeginTransitionTo("TelluriumMinesC", "FORCEPOS:23.0x23.0x0")
	
	--Special case: Floor 20 is TelluriumMinesD:
	elseif(iCurrentMinesFloor == 20) then
		AL_BeginTransitionTo("TelluriumMinesD", "FORCEPOS:15.0x9.0x0")
    
	--Special case: Floor 30 is TelluriumMinesF:
	elseif(iCurrentMinesFloor == 30) then
        AL_BeginTransitionTo("TelluriumMinesF", "FORCEPOS:16.0x4.0x0")
    
	--Special case: Floor 40 is TelluriumMinesG:
	elseif(iCurrentMinesFloor == 40) then
        AL_BeginTransitionTo("TelluriumMinesG", "FORCEPOS:15.0x4.0x0")
    
	--Special case: Floor 50 is TelluriumMinesH:
	elseif(iCurrentMinesFloor == 50) then
        AL_BeginTransitionTo("TelluriumMinesH", "FORCEPOS:5.0x25.0x0")
    
    --Special cases: Check for the randomly placed floors.
    elseif(iCurrentMinesFloor == iMineAFloor) then
		AL_BeginTransitionTo("MinesRandomA", "FORCEPOS:9.0x20.0x0")
        
    elseif(iCurrentMinesFloor == iMineBFloor) then
		AL_BeginTransitionTo("MinesRandomB", "FORCEPOS:5.0x15.0x0")
        
    elseif(iCurrentMinesFloor == iMineCFloor) then
		AL_BeginTransitionTo("MinesRandomC", "FORCEPOS:9.0x16.0x0")
        
    elseif(iCurrentMinesFloor == iMineDFloor) then
		AL_BeginTransitionTo("MinesRandomD", "FORCEPOS:37.0x13.0x0")
	
	--All other cases:
	else
		AL_BeginTransitionTo("RANDOMLEVEL", "Null")
	end
	
--Up ladder.
elseif(sObjectName == "LadderU" or sObjectName == "Entrance") then
	
	--Dialogue.
    gbCaughtScript = true
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Climb the ladder back to the surface?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

--[Decisions]
--Really exit the mines.
elseif(sObjectName == "Yes") then

	--Go to Mines B.
    gbCaughtScript = true
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:19.0x24.0x0")
    
    --When the player has left Mines B after getting the ammo, throw this flag.
    local iMineBGotAmmo      = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N")
    local iMineBFloor        = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBFloor", "N")
	local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N")
    if(iCurrentMinesFloor == iMineBFloor and iMineBGotAmmo == 2.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBLeft", "N", 1.0)
    end
    
--Nope.
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
    gbCaughtScript = true
end