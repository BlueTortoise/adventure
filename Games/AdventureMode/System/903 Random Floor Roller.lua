--[Random Floor Roller]
--This file determines which floors the Randomly-Placed Pregenerated levels are on. They are the same
-- once selected in a playthrough. All floors always appear, but might appear out of order or on different
-- floors in a given playthrough.
    
--Flag so this doesn't happen again.
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasRolledFloors", "N", 1.0)

--Variable storage.
local iFloorsToGenerate = 4
local iaFloorList = {}
local iaFloorRanges = {{4, 19}, {3, 30}, {6, 35}, {14, 38}}
local iaFixedFloors = {10, 20, 30, 40, 50}
local iFixedFloorsTotal = #iaFixedFloors

--[Overall Generation Loop]
for i = 1, iFloorsToGenerate, 1 do
    
    --[Internal Generation Loop]
    local bLegalRoll = false
    while(bLegalRoll == false) do
        
        --Generate and set.
        bLegalRoll = true
        iaFloorList[i] = LM_GetRandomNumber(iaFloorRanges[i][1], iaFloorRanges[i][2])
        
        --Special, fixed floors.
        for p = 1, iFixedFloorsTotal, 1 do
            if(iaFloorList[i] == iaFixedFloors[p]) then
                bLegalRoll = false 
                p = iFixedFloorsTotal
            end
        end
        
        --Previous floors. Two floors cannot land on the same value.
        for p = 1, i-1, 1 do
            if(iaFloorList[i] == iaFloorList[p]) then
                bLegalRoll = false 
                p = iFixedFloorsTotal
            end
            
        end
    end
end

--Store everything in variables so the engine knows where to find them.
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineAFloor", "N", iaFloorList[1])
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBFloor", "N", iaFloorList[2])
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCFloor", "N", iaFloorList[3])
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDFloor", "N", iaFloorList[4])
io.write("Floors generated: " .. iaFloorList[1] .. " " .. iaFloorList[2] .. " " .. iaFloorList[3] .. " " .. iaFloorList[4] .. "\n")
