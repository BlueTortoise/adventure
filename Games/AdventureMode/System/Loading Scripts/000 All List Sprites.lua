--[Load All Sprite Lists]
--Loads all sprites from lists, regardless of chapter.
io.write("Running all list sprites.\n")

--[======================================== Loading Lists ========================================]
--Player party.
gcsaChristineList  = {"Human", "Darkmatter", "DreamGirl", "Electrosprite", "Golem", "GolemDress", "Latex", "Male", "SteamDroid", "Raibie", "Raiju", "RaijuClothes"}
gcsaJeanneList     = {"Human"}
gcsaMeiList        = {"Human", "Alraune", "Bee", "Ghost", "Slime", "Werecat", "Alraune_MC", "Bee_MC", "Human_MC"}
gcsaSanyaList      = {"Human"}
gcsaLottaList      = {"Human"}
gcsaTaliaList      = {"SKIP"}
gcsaOtherPartyList = {"55", "56", "Aquillia", "Breanne", "Florentina", "Sophie", "SophieDress", "SX399Lord", "Sammy"}
gcsaOtherEightList = {"DarkmatterGirl"} --8 directional NPCs who don't join the party.

--Major NPCs, four-directions.
gcsaMajorNPCList   = {"Adina", "Blythe", "Claudia", "Nadia", "Rochea", "Septima", "SX399", "JX101", "DrMaisie", "Isabel"}
gcsaCassandraList  = {"CassandraH", "CassandraW", "CassandraG"}

--Generic NPCs and Enemies.
gcsaGenericNPCList = {"GenericF0", "GenericF1", "GenericM0", "GenericM1", "CultistF", "CultistM", "MercF", "MercM", "GenericBiolabsFBlue", "GenericBiolabsFRed", "GenericBiolabsFGreen", "GenericBiolabsFYellow", 
                      "GenericBiolabsMBlue", "GenericBiolabsMRed", "GenericBiolabsMGreen", "GenericBiolabsMYellow"}
gcsaChapter1List   = {"Alraune", "BeeGirl", "BeeGirlZ", "BestFriend", "MaidGhost", "MirrorImage", "Slime", "SlimeB", "SlimeG", "Rilmani", "SkullCrawler", "Werecat"}
gcsaChapter3List   = {"Imp"}
gcsaChapter5List   = {"609144", "20", "Doll", "DollInfluenced", "Electrosprite", "GolemLordA", "GolemLordB", "GolemLordC", "GolemLordD", "GolemSlave", "GolemSlaveP", "GolemSlaveR", "EldritchDream", "Horrible",
	                  "Hoodie", "InnGeisha", "BandageImp", "Electrosprite", "LatexDrone", "Maisie", "SteamDroid", "Scraprat", "SecurityBot", "SecurityBotBroke", "Raibie", "Raiju", "Vivify", "VivifyBlack", "VoidRift"}
gcsaOtherNPCList   = {"Tram"}

--Special Frame Lists.
gcsaCWSpecialList     = {"2855", "SX399", "Christine_DarkM", "Christine_DreamGirl", "Christine_Golem", "Christine_Human", "Christine_Latex", "Christine_Male", "Christine_Raibie", "Christine_Raiju", "Christine_RaijuClothes", "Florentina", "MeiAlraune", "MeiBee", "Mei", "MeiGhost", "MeiSlime", "MeiWerecat", "Alraune"}
gcsaCrouchSpecialList = {"MeiBeeMC", "MeiMC"}
gcsaWoundedSpeciaList = {"Aquillia", "CassandraH", "CassandraW", "CultistF", "Doll", "GenericF1", "GolemLord", "GolemSlave", "Werecat"}

--Half-Frame Lists. Used for some NPCs with two-directional movement, like sheep and chickens.
gcsaHalfFrameList = {"Sheep", "Chicken"}

--[======================================== Load via List ========================================]
--Player's Party
fnExtractSpriteList(gcsaChristineList,  true, "Christine_")
fnExtractSpriteList(gcsaJeanneList,     true, "Jeanne_")
fnExtractSpriteList(gcsaMeiList,        true, "Mei_")
fnExtractSpriteList(gcsaSanyaList,      true, "Sanya_")
fnExtractSpriteList(gcsaLottaList,      true, "Lotta_")
fnExtractSpriteList(gcsaTaliaList,      true, "Talia_")
fnExtractSpriteList(gcsaOtherPartyList, true)
fnExtractSpriteList(gcsaOtherEightList, true)

--Major NPCs
fnExtractSpriteList(gcsaMajorNPCList, false)
fnExtractSpriteList(gcsaCassandraList, false)

--NPCs and Enemies
fnExtractSpriteList(gcsaGenericNPCList, false)
fnExtractSpriteList(gcsaChapter1List,   false)
fnExtractSpriteList(gcsaChapter3List,   false)
fnExtractSpriteList(gcsaChapter5List,   false)
fnExtractSpriteList(gcsaOtherNPCList,   false)

--NPCs who are West/East only.
fnExtractSpriteListEW(gcsaHalfFrameList)

--Crouching and Wounded:
fnExtractSpecialFrames(gcsaCWSpecialList, "Crouch", "Wounded")

--Just Crouch:
fnExtractSpecialFrames(gcsaCrouchSpecialList, "Crouch")

--Just Wounded:
fnExtractSpecialFrames(gcsaWoundedSpeciaList, "Wounded")