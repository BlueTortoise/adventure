--[Chapter 1 Loading]
--This file loads assets unique to chapter 1. Subfiles are used where necessary.

--[Common]
--Open the datafile.
SLF_Open(gsDatafilesPath .. "Sprites.slf")

--Modify the distance filters to keep everything pixellated. This is only for sprites.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
ALB_SetTextureProperty("Special Sprite Padding", true)
--Bitmap_ActivateAtlasing()

--[======================================== Loading Lists ========================================]
--Player party. Only Mei loads her forms. Doesn't do anything if the argument specifies not to.
gcsaMeiList        = {"Human", "Alraune", "Bee", "BeeQueen", "Ghost", "Slime", "Werecat", "Alraune_MC", "Bee_MC", "Human_MC"}
gcsaOtherPartyList = {"Aquillia", "Breanne", "Florentina", "FlorentinaTH", "FlorentinaMed"}
gcsaOtherEightList = {"SKIP"}

--Major NPCs, four-directions.
gcsaMajorNPCList   = {"Adina", "Blythe", "Claudia", "Nadia", "Rochea", "Septima"}
gcsaCassandraList  = {"CassandraH", "CassandraW"}

--Generic NPCs and Enemies.
gcsaGenericNPCList = {"GenericF0", "GenericF1", "GenericM0", "GenericM1", "CultistF", "CultistM", "MercF", "MercM"}
gcsaChapter1List   = {"Alraune", "BeeBuddy", "BeeGirl", "BeeGirlZ", "BestFriend", "Jelli", "MaidGhost", "MirrorImage", "Slime", "SlimeB", "SlimeG", "Rilmani", "SkullCrawler", "Werecat", "WerecatThief"}

--Special Frame Lists. The Wounded list is needed for an enemy to correctly render as "downed" after combat.
gcsaCWSpecialList     = {"Florentina", "FlorentinaTH", "FlorentinaMed", "MeiAlraune", "MeiBee", "MeiBeeQueen", "Mei", "MeiGhost", "MeiSlime", "MeiWerecat", "Alraune"}
gcsaCrouchSpecialList = {"MeiBeeMC", "MeiMC"}
gcsaWoundedSpeciaList = {"Aquillia", "BeeGirl", "BeeGirlZ", "BestFriend", "CassandraH", "CassandraW", "CultistF", "CultistM", "GenericF1", "Jelli", "MaidGhost", "MirrorImage", "SkullCrawler", "Slime", "SlimeB", "SlimeG", "Werecat", "WerecatThief"}

--Half-Frame Lists. Used for some NPCs with two-directional movement, like sheep and chickens.
gcsaHalfFrameList = {"Sheep", "Chicken"}

--[======================================== Load via List ========================================]
--Player's Party.
fnExtractSpriteList(gcsaMeiList,        true, "Mei_")
fnExtractSpriteList(gcsaOtherPartyList, true)
fnExtractSpriteList(gcsaOtherEightList, true)

--Major NPCs.
fnExtractSpriteList(gcsaMajorNPCList, false)
fnExtractSpriteList(gcsaCassandraList, false)

--Generic NPCs and Enemies.
fnExtractSpriteList(gcsaGenericNPCList, false)
fnExtractSpriteList(gcsaChapter1List,   false)

--Special Frame Lists.
DL_AddPath("Root/Images/Sprites/Special/")
fnExtractSpecialFrames(gcsaCWSpecialList, "Crouch", "Wounded")
fnExtractSpecialFrames(gcsaCrouchSpecialList, "Crouch")
fnExtractSpecialFrames(gcsaWoundedSpeciaList, "Wounded")

--Half-Frame Lists.
fnExtractSpriteListEW(gcsaHalfFrameList)

--[Non-Standard Special Frames]
--Florentina.
DL_ExtractBitmap("Spcl|Florentina|Sleep", "Root/Images/Sprites/Special/Florentina|Sleep")

--Nadia.
DL_ExtractBitmap("Spcl|Nadia|Sleep", "Root/Images/Sprites/Special/Nadia|Sleep")

--[==================================== Misc Character Sprites ===================================]
--[Crowbar Chan]
--Second last!? But I'm way cuter than Jeanne!
DL_AddPath("Root/Images/Sprites/CrowbarChan/")
DL_ExtractBitmap("CrowbarChan|0", "Root/Images/Sprites/CrowbarChan/0")
DL_ExtractBitmap("CrowbarChan|1", "Root/Images/Sprites/CrowbarChan/1")
DL_ExtractBitmap("CrowbarChan|2", "Root/Images/Sprites/CrowbarChan/2")

--[Farm Icons]
--Used in the Salt Flats sequence.
DL_AddPath("Root/Images/Sprites/FarmIcons/")
DL_ExtractBitmap("FarmIcon|Water0", "Root/Images/Sprites/FarmIcons/Water0")
DL_ExtractBitmap("FarmIcon|Water1", "Root/Images/Sprites/FarmIcons/Water1")
DL_ExtractBitmap("FarmIcon|Ferti0", "Root/Images/Sprites/FarmIcons/Ferti0")
DL_ExtractBitmap("FarmIcon|Ferti1", "Root/Images/Sprites/FarmIcons/Ferti1")
DL_ExtractBitmap("FarmIcon|Grass0", "Root/Images/Sprites/FarmIcons/Grass0")
DL_ExtractBitmap("FarmIcon|Grass1", "Root/Images/Sprites/FarmIcons/Grass1")
DL_ExtractBitmap("FarmIcon|Polln0", "Root/Images/Sprites/FarmIcons/Polln0")
DL_ExtractBitmap("FarmIcon|Polln1", "Root/Images/Sprites/FarmIcons/Polln1")

--[Other]
--Hacksaw. Used in the Claudia sequence.
DL_AddPath("Root/Images/Sprites/Hacksaw/")
DL_ExtractBitmap("Hacksaw", "Root/Images/Sprites/Hacksaw/Hacksaw")

--[Clean Up]
--Clear this flag.
--Bitmap_DeactivateAtlasing()
ALB_SetTextureProperty("Restore Defaults")
ALB_SetTextureProperty("Special Sprite Padding", false)
SLF_Close()
