--[ ==================================== Chapter 1 Portraits ==================================== ]
--Loads all portraits for chapter 1.

--[ ============================================ Mei ============================================ ]
--[Emotes]
--Note: Mei's Human Neutral is already loaded.
SLF_Open(gsaDatafilePaths.sMeiPath)
DL_AddPath("Root/Images/Portraits/MeiDialogue/")
local saPattern = {"Neutral", "Happy", "Blush", "Sad", "Surprise", "Offended", "Cry", "Laugh", "Angry", "Smirk"}
local saForms = {"", "Alraune", "Bee", "BeeQueen", "Ghost", "Slime", "Werecat"}
for i = 1, #saForms, 1 do
    for p = 1, #saPattern, 1 do
        if(saForms[i] ~= "") then
            DL_ExtractBitmap("Por|Mei" .. saForms[i].. "|" .. saPattern[p], "Root/Images/Portraits/MeiDialogue/" .. saForms[i] .. saPattern[p])
        else
            DL_ExtractBitmap("Por|Mei|" .. saPattern[p], "Root/Images/Portraits/MeiDialogue/" .. saPattern[p])
        end
    end
end

--Mei's special frames, not available in all forms.
DL_ExtractBitmap("Por|Mei|MC",        "Root/Images/Portraits/MeiDialogue/MC")
DL_ExtractBitmap("Por|MeiAlraune|MC", "Root/Images/Portraits/MeiDialogue/AlrauneMC")
DL_ExtractBitmap("Por|MeiBee|MC",     "Root/Images/Portraits/MeiDialogue/BeeMC")
DL_ExtractBitmap("Por|Mei|Rubber",    "Root/Images/Portraits/MeiDialogue/Rubber")

--[Combat]
--Main
DL_AddPath("Root/Images/Portraits/Combat/")
DL_ExtractBitmap("Party|Mei_Alraune",             "Root/Images/Portraits/Combat/Mei_Alraune")
DL_ExtractBitmap("Party|Mei_AlrauneMC",           "Root/Images/Portraits/Combat/Mei_AlrauneMC")
DL_ExtractBitmap("Party|Mei_Bee",                 "Root/Images/Portraits/Combat/Mei_Bee")
DL_ExtractBitmap("Party|Mei_BeeQueen",            "Root/Images/Portraits/Combat/Mei_BeeQueen")
DL_ExtractBitmap("Party|Mei_Human",               "Root/Images/Portraits/Combat/Mei_Human")
DL_ExtractBitmap("Party|Mei_HumanMC",             "Root/Images/Portraits/Combat/Mei_HumanMC")
DL_ExtractBitmap("Party|Mei_Ghost",               "Root/Images/Portraits/Combat/Mei_Ghost")
DL_ExtractBitmap("Party|Mei_Rubber",              "Root/Images/Portraits/Combat/Mei_Rubber")
DL_ExtractBitmap("Party|Mei_Slime",               "Root/Images/Portraits/Combat/Mei_Slime")
DL_ExtractBitmap("Party|Mei_Werecat",             "Root/Images/Portraits/Combat/Mei_Werecat")
DL_ExtractBitmap("Party|Mei_Zombee",              "Root/Images/Portraits/Combat/Mei_Zombee")

--Countermasks
DL_ExtractBitmap("Party|Mei_Alraune_Countermask", "Root/Images/Portraits/Combat/Mei_Alraune_Countermask")
DL_ExtractBitmap("Party|Mei_Human_Countermask",   "Root/Images/Portraits/Combat/Mei_Human_Countermask")

--[ ===================================== Other Characters ====================================== ]
--[Florentina's Portraits]
--Emote
SLF_Open(gsaDatafilePaths.sFlorentinaPath)
DL_AddPath("Root/Images/Portraits/FlorentinaMerchantDialogue/")
DL_ExtractBitmap("Por|FlorentinaMerchant|Neutral",  "Root/Images/Portraits/FlorentinaMerchantDialogue/Neutral")
DL_ExtractBitmap("Por|FlorentinaMerchant|Happy",    "Root/Images/Portraits/FlorentinaMerchantDialogue/Happy")
DL_ExtractBitmap("Por|FlorentinaMerchant|Blush",    "Root/Images/Portraits/FlorentinaMerchantDialogue/Blush")
DL_ExtractBitmap("Por|FlorentinaMerchant|Facepalm", "Root/Images/Portraits/FlorentinaMerchantDialogue/Facepalm")
DL_ExtractBitmap("Por|FlorentinaMerchant|Surprise", "Root/Images/Portraits/FlorentinaMerchantDialogue/Surprise")
DL_ExtractBitmap("Por|FlorentinaMerchant|Offended", "Root/Images/Portraits/FlorentinaMerchantDialogue/Offended")
DL_ExtractBitmap("Por|FlorentinaMerchant|Confused", "Root/Images/Portraits/FlorentinaMerchantDialogue/Confused")

DL_AddPath("Root/Images/Portraits/FlorentinaMediatorDialogue/")
DL_ExtractBitmap("Por|FlorentinaMediator|Neutral",  "Root/Images/Portraits/FlorentinaMediatorDialogue/Neutral")
DL_ExtractBitmap("Por|FlorentinaMediator|Happy",    "Root/Images/Portraits/FlorentinaMediatorDialogue/Happy")
DL_ExtractBitmap("Por|FlorentinaMediator|Blush",    "Root/Images/Portraits/FlorentinaMediatorDialogue/Blush")
DL_ExtractBitmap("Por|FlorentinaMediator|Facepalm", "Root/Images/Portraits/FlorentinaMediatorDialogue/Facepalm")
DL_ExtractBitmap("Por|FlorentinaMediator|Surprise", "Root/Images/Portraits/FlorentinaMediatorDialogue/Surprise")
DL_ExtractBitmap("Por|FlorentinaMediator|Offended", "Root/Images/Portraits/FlorentinaMediatorDialogue/Offended")
DL_ExtractBitmap("Por|FlorentinaMediator|Confused", "Root/Images/Portraits/FlorentinaMediatorDialogue/Confused")

DL_AddPath("Root/Images/Portraits/FlorentinaTreasureHunterDialogue/")
DL_ExtractBitmap("Por|FlorentinaTreasureHunter|Neutral",  "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Neutral")
DL_ExtractBitmap("Por|FlorentinaTreasureHunter|Happy",    "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Happy")
DL_ExtractBitmap("Por|FlorentinaTreasureHunter|Blush",    "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Blush")
DL_ExtractBitmap("Por|FlorentinaTreasureHunter|Facepalm", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Facepalm")
DL_ExtractBitmap("Por|FlorentinaTreasureHunter|Surprise", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Surprise")
DL_ExtractBitmap("Por|FlorentinaTreasureHunter|Offended", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Offended")
DL_ExtractBitmap("Por|FlorentinaTreasureHunter|Confused", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Confused")

--Combat
DL_ExtractBitmap("Party|Florentina_Merchant",       "Root/Images/Portraits/Combat/Florentina_Merchant")
DL_ExtractBitmap("Party|Florentina_Mediator",       "Root/Images/Portraits/Combat/Florentina_Mediator")
DL_ExtractBitmap("Party|Florentina_TreasureHunter", "Root/Images/Portraits/Combat/Florentina_TreasureHunter")
DL_ExtractBitmap("Party|FlorentinaCountermask",     "Root/Images/Portraits/Combat/FlorentinaCountermask")

--[Aquillia's Portraits]
SLF_Open(gsaDatafilePaths.sAquilliaPath)
DL_AddPath("Root/Images/Portraits/AquilliaDialogue/")
DL_ExtractBitmap("Por|Aquillia|Neutral", "Root/Images/Portraits/AquilliaDialogue/HumanNeutral")

--[Crowbar Chan]
SLF_Open(gsaDatafilePaths.sChapter0EmotePath)
DL_AddPath("Root/Images/Portraits/CrowbarChan/")
DL_ExtractBitmap("Por|CrowbarChan|Neutral",  "Root/Images/Portraits/CrowbarChan/Neutral")
    
--[Minor Characters]
SLF_Open(gsaDatafilePaths.sChapter1EmotePath)
DL_AddPath("Root/Images/Portraits/Adina/")
DL_ExtractBitmap("Por|Adina|Neutral", "Root/Images/Portraits/Adina/Neutral")
DL_AddPath("Root/Images/Portraits/Blythe/")
DL_ExtractBitmap("Por|Blythe|Neutral", "Root/Images/Portraits/Blythe/Neutral")
DL_AddPath("Root/Images/Portraits/Breanne/")
DL_ExtractBitmap("Por|Breanne|Neutral", "Root/Images/Portraits/Breanne/Neutral")
DL_AddPath("Root/Images/Portraits/Nadia/")
DL_ExtractBitmap("Por|Nadia|Neutral", "Root/Images/Portraits/Nadia/Neutral")
DL_AddPath("Root/Images/Portraits/Claudia/")
DL_ExtractBitmap("Por|Claudia|Neutral", "Root/Images/Portraits/Claudia/Neutral")
DL_AddPath("Root/Images/Portraits/Rochea/")
DL_ExtractBitmap("Por|Rochea|Neutral", "Root/Images/Portraits/Rochea/Neutral")

--[Bosses]
--None.

--[ =================================== Enemy Combat Portraits ================================== ]
--Chapter 0 Normal
SLF_Open(gsaDatafilePaths.sChapter0CombatPath)
DL_ExtractBitmap("Enemy|SkullCrawler",    "Root/Images/Portraits/Combat/SkullCrawler")
DL_ExtractBitmap("Enemy|MirrorImage",     "Root/Images/Portraits/Combat/MirrorImage")

--Chapter 1 Normal
SLF_Open(gsaDatafilePaths.sChapter1CombatPath)
DL_ExtractBitmap("Enemy|Alraune",         "Root/Images/Portraits/Combat/Alraune")
DL_ExtractBitmap("Enemy|BeeGirl",         "Root/Images/Portraits/Combat/BeeGirl")
DL_ExtractBitmap("Enemy|CultistF",        "Root/Images/Portraits/Combat/CultistF")
DL_ExtractBitmap("Enemy|CultistM",        "Root/Images/Portraits/Combat/CultistM")
DL_ExtractBitmap("Enemy|MaidGhost",       "Root/Images/Portraits/Combat/MaidGhost")
DL_ExtractBitmap("Enemy|Slime",           "Root/Images/Portraits/Combat/Slime")
DL_ExtractBitmap("Enemy|Werecat",         "Root/Images/Portraits/Combat/Werecat")
DL_ExtractBitmap("Enemy|Zombee",          "Root/Images/Portraits/Combat/Zombee")

--Chapter 1 Rubber

--Chapter 1 Boss
DL_ExtractBitmap("Enemy|Arachnophelia",   "Root/Images/Portraits/Combat/Arachnophelia")
DL_ExtractBitmap("Enemy|Infirm",          "Root/Images/Portraits/Combat/Infirm")

--[Clean]
SLF_Close()
