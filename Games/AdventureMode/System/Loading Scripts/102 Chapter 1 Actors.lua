--[Dialogue Actors: Chapter 1]
--All dialogue actors from chapter 1.

--[ =================================== Party/Important Actors ================================== ]
--[Setup]
--Alias listings.
local saNoAliases        = {"NOALIAS"}
local saMeiAliases       = {"Drone", "Natalie", "Thrall"}

--Emotion listings.
local saNoEmotions         = {"Neutral"}
local saMeiEmotions        = {"Neutral", "Smirk", "Happy", "Blush", "Sad", "Surprise", "Offended", "Cry", "Laugh", "Angry", "MC"}
local saFlorentinaEmotions = {"Neutral", "Happy", "Blush", "Facepalm", "Surprise", "Offended", "Confused"}
local saSophieEmotions     = {"Neutral", "Happy", "Blush", "Smirk", "Sad", "Surprised", "Offended"}
local saJX101Emotions      = {"Neutral", "HatA", "HatB", "HatC"}
local saSammyEmotions      = {"Neutral"}

--[Chapter 1]
fnCreateDialogueActor("Mei",         "Voice|Mei",        "Root/Images/Portraits/MeiDialogue/",                true, saMeiAliases, saMeiEmotions)
fnCreateDialogueActor("Florentina",  "Voice|Florentina", "Root/Images/Portraits/FlorentinaMerchantDialogue/", true, saNoAliases,  saFlorentinaEmotions)
fnCreateDialogueActor("Breanne",     "Voice|Breanne",    "Root/Images/Portraits/Breanne/",                    true, saNoAliases,  saNoEmotions)
fnCreateDialogueActor("CrowbarChan", "Voice|Septima",    "Root/Images/Portraits/CrowbarChan/",                true, saNoAliases, saNoEmotions)

--Aquillia has a brief cameo.
DialogueActor_Create("Aquillia")
	DialogueActor_SetProperty("Add Alias", "Prisoner")
	DialogueActor_SetProperty("Add Alias", "Aquillia")
	DialogueActor_SetProperty("Add Alias", "Cultist")
	DialogueActor_SetProperty("Add Alias", "Lady")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/AquilliaDialogue/HumanNeutral", true)
	DialogueActor_SetProperty("Add Emotion", "Cultist", "Root/Images/Portraits/Enemies/CultistF", false)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Aquillia", "Voice|Aquillia")

--[ ======================================== Major Actors ======================================= ]
--Refers to actors who are important enough to usually warrant their own sprite, but not a full emotion set.
-- May also refer to actors who play a big role but only in one section of the game.

--Cassandra is created specially, since her Neutral emotion is her human form. She gets transformed a lot.
DialogueActor_Create("Cassandra")
	DialogueActor_SetProperty("Add Alias", "Lady")
	DialogueActor_SetProperty("Add Alias", "Cassandra")
	DialogueActor_SetProperty("Add Alias", "Fang")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/CassandraDialogue/Human",   true)
	DialogueActor_SetProperty("Add Emotion", "Werecat", "Root/Images/Portraits/CassandraDialogue/Werecat", true)
	DialogueActor_SetProperty("Add Emotion", "Golem",   "Root/Images/Portraits/CassandraDialogue/Golem",   true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Cassandra", "Voice|GenericF13")

--[Chapter 1]
fnCreateDialogueActor("Rochea",  "Voice|Alraune", "Root/Images/Portraits/Rochea/",  true, {"Alraune"}, saNoEmotions)
fnCreateDialogueActor("Adina",   "Voice|Alraune", "Root/Images/Portraits/Adina/",   true, {"Alraune"}, saNoEmotions)
fnCreateDialogueActor("Blythe",  "Voice|Blythe",  "Root/Images/Portraits/Blythe/",  true, {"Man"},     saNoEmotions)
fnCreateDialogueActor("Claudia", "Voice|Nadia",   "Root/Images/Portraits/Claudia/", true, saNoAliases, saNoEmotions)
fnCreateDialogueActor("Nadia",   "Voice|Nadia",   "Root/Images/Portraits/Nadia/",   true, {"Alraune"}, saNoEmotions)

--[ ======================================== Minor Actors ======================================= ]
--NPCs or Enemy actors, usually play small roles many times. Some have unique dialogue sprites because their enemy field
-- variants are too big to fit on the dialogue screen.

--[Chapter 1]
--"Named" versions, these are characters but they use generic portraits.
fnCreateDialogueActor("Hypatia",  "Voice|GenericF02", "Root/Images/Portraits/NPCs/HumanNPCF1", true,  saNoAliases, {"NEUTRALISPATH"})
fnCreateDialogueActor("Karina",   "Voice|GenericF03", "Root/Images/Portraits/NPCs/HumanNPCF0", true,  {"Lady"},    {"NEUTRALISPATH"})
fnCreateDialogueActor("Alicia",   "Voice|GenericF04", "Root/Images/Portraits/NPCs/HumanNPCF0", true,  {"Lady"},    {"NEUTRALISPATH"})
fnCreateDialogueActor("Ginny",    "Voice|Werecat",    "Root/Images/Portraits/Enemies/Werecat", false, {"Werecat"}, {"NEUTRALISPATH"})

--"Unnamed" versions. Not characters, just stand-ins for a monster/enemy type.
fnCreateDialogueActor("CultistF", "Voice|GenericF00", "Root/Images/Portraits/Enemies/CultistF",  true,  {"CultistF", "Cultist", "Female Cultist"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("CultistM", "Voice|GenericM00", "Root/Images/Portraits/Enemies/CultistM",  true,  {"CultistM", "Cultist", "Male Cultist"},   {"NEUTRALISPATH"})
fnCreateDialogueActor("Alraune",  "Voice|Alraune",    "Root/Images/Portraits/Enemies/Alraune",   true,  {"Alraune", "Dafoda", "Reika"},            {"NEUTRALISPATH"})
fnCreateDialogueActor("Alraune2", "Voice|Alraune",    "Root/Images/Portraits/Enemies/Alraune",   true,  saNoAliases,                               {"NEUTRALISPATH"})
fnCreateDialogueActor("MercF",    "Voice|GenericF11", "Root/Images/Portraits/NPCs/MercF",        true,  {"Guard"},                                 {"NEUTRALISPATH"})
fnCreateDialogueActor("MercM",    "Voice|GenericM01", "Root/Images/Portraits/NPCs/MercM",        true,  {"Guard", "Suitor"},                       {"NEUTRALISPATH"})
fnCreateDialogueActor("Bee",      "Voice|Bee",        "Root/Images/Portraits/Enemies/Bee",       false, {"Drone", "Joanie"},                       {"NEUTRALISPATH"})
fnCreateDialogueActor("Ghost",    "Voice|Ghost",      "Root/Images/Portraits/Enemies/MaidGhost", false, {"Laura", "Lydie"},                        {"NEUTRALISPATH"})
fnCreateDialogueActor("Werecat",  "Voice|Werecat",    "Root/Images/Portraits/Enemies/Werecat",   false, saNoAliases,                               {"NEUTRALISPATH"})
fnCreateDialogueActor("Zombee",   "Voice|Bee",        "Root/Images/Portraits/Enemies/Zombee",    false, saNoAliases,                               {"NEUTRALISPATH"})

--Rubberified NPCs
fnCreateDialogueActor("RubberCultistF", "Voice|GenericF00", "Root/Images/Portraits/Enemies/CultistFRubber", true,  {"Cultist", "Rubber Cultist"}, {"NEUTRALISPATH"})

--Slimes exist in three color variants.
DialogueActor_Create("Slime")
	DialogueActor_SetProperty("Add Alias", "Slime")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Enemies/Slime",  true)
	DialogueActor_SetProperty("Add Emotion", "Green",   "Root/Images/Portraits/Enemies/SlimeG", true)
	DialogueActor_SetProperty("Add Emotion", "Blue",    "Root/Images/Portraits/Enemies/SlimeB", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Slime", "Voice|GenericF00")

