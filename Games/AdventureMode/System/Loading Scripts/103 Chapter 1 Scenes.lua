--[Chapter 1 Scenes]
--Scenes for chapter 1. This includes TF scenes and runestone flashes.

--[Slime Art Gallery]
--Examination images for the art gallery.
SLF_Open(gsaDatafilePaths.sScnCh1Major)
DL_AddPath("Root/Images/Scenes/ArtGallery/")
DL_ExtractBitmap("SlimeGallery|Goodiva",     "Root/Images/Scenes/ArtGallery/Goodiva")
DL_ExtractBitmap("SlimeGallery|SlimaLisa",   "Root/Images/Scenes/ArtGallery/SlimaLisa")
DL_ExtractBitmap("SlimeGallery|Washlimeton", "Root/Images/Scenes/ArtGallery/Washlimeton")

--[Cassandra's Transformations]
--She shows up a lot. Suspicious, isn't it?
SLF_Open(gsaDatafilePaths.sScnCassandraTF)
DL_AddPath("Root/Images/Scenes/Cassandra/")

--Human
DL_ExtractBitmap("Cassandra|Neutral",    "Root/Images/Scenes/Cassandra/Neutral")

--Werecat
DL_ExtractBitmap("Cassandra|WerecatTF0", "Root/Images/Scenes/Cassandra/WerecatTF0")
DL_ExtractBitmap("Cassandra|WerecatTF1", "Root/Images/Scenes/Cassandra/WerecatTF1")
DL_ExtractBitmap("Cassandra|Werecat",    "Root/Images/Scenes/Cassandra/Werecat")

--[ =================================== Mei's Transformations =================================== ]
--Chapter 1's heroine.
SLF_Open(gsaDatafilePaths.sScnMeiTF)
DL_AddPath("Root/Images/Scenes/Mei/")

--Alraune
DL_ExtractBitmap("Mei|AlrauneTF0", "Root/Images/Scenes/Mei/AlrauneTF0")
DL_ExtractBitmap("Mei|AlrauneTF1", "Root/Images/Scenes/Mei/AlrauneTF1")

--Bee
DL_ExtractBitmap("Mei|BeeTF0",     "Root/Images/Scenes/Mei/BeeTF0")
DL_ExtractBitmap("Mei|BeeTF1",     "Root/Images/Scenes/Mei/BeeTF1")
DL_ExtractBitmap("Mei|BeeTF2",     "Root/Images/Scenes/Mei/BeeTF2")

--Ghost
DL_ExtractBitmap("Mei|GhostTF0",   "Root/Images/Scenes/Mei/GhostTF0")
DL_ExtractBitmap("Mei|GhostTF1",   "Root/Images/Scenes/Mei/GhostTF1")

--Slime
DL_ExtractBitmap("Mei|SlimeTF0",   "Root/Images/Scenes/Mei/SlimeTF0")
DL_ExtractBitmap("Mei|SlimeTF1",   "Root/Images/Scenes/Mei/SlimeTF1")

--Werecat
DL_ExtractBitmap("Mei|WerecatTF0", "Root/Images/Scenes/Mei/WerecatTF0")
DL_ExtractBitmap("Mei|WerecatTF1", "Root/Images/Scenes/Mei/WerecatTF1")

--Mei's Rune overlay. 60 frames.
SLF_Open(gsaDatafilePaths.sScnMeiRune)
for i = 0, 59, 1 do
	local sName = string.format("Mei|Rune%02i", i)
	local sPath = string.format("Root/Images/Scenes/Mei/RuneAnim%02i", i)
	DL_ExtractBitmap(sName, sPath)
end

--Still image of Mei's rune is the last one in the animation sequence.
DL_ExtractBitmap("Mei|Rune59", "Root/Images/Scenes/Mei/Rune")

--[Clean]
SLF_Close()
