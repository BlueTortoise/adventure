--[Chapter 1 Overlays]
--Setup.
local bUseLowResMode = OM_GetOption("LowResAdventureMode")
if(bUseLowResMode == false) then
    SLF_Open(gsDatafilesPath .. "OverheadMaps.slf")
else
    SLF_Open(gsDatafilesPath .. "OverheadMapsLoDef.slf")
end

--Map of the chapter:
DL_AddPath("Root/Images/AdvMaps/General/")
DL_ExtractBitmap("TrannadarMap",        "Root/Images/AdvMaps/General/TrannadarMap")
DL_ExtractBitmap("TrannadarMapOverlay", "Root/Images/AdvMaps/General/TrannadarMapOverlay")

--Background for Nix Nedar.
DL_AddPath("Root/Images/AdvMaps/Backgrounds/")
DL_ExtractBitmap("NixNedarBacking", "Root/Images/AdvMaps/Backgrounds/NixNedar")