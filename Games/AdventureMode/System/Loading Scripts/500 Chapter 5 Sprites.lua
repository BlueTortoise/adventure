--[Chapter 5 Loading]
--This file loads assets unique to chapter 5. Subfiles are used where necessary.

--[Common]
--Open the datafile.
SLF_Open(gsDatafilesPath .. "Sprites.slf")

--Modify the distance filters to keep everything pixellated. This is only for sprites.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
ALB_SetTextureProperty("Special Sprite Padding", true)
--Bitmap_ActivateAtlasing()

--[======================================== Loading Lists ========================================]
--Player party.
local gcsaChristineList  = {"Human", "Darkmatter", "DreamGirl", "Electrosprite", "Golem", "GolemDress", "Latex", "Male", "SteamDroid", "Raibie", "Raiju", "RaijuClothes", "Doll"}
local gcsaOtherPartyList = {"55", "55Sundress", "56", "Sophie", "SophieDress", "SophieLeather", "SX399Lord", "Sammy"}
local gcsaOtherEightList = {"DarkmatterGirl"} --8 directional NPCs who don't join the party.

--Major NPCs, four-directions.
local gcsaMajorNPCList   = {"SX399", "JX101", "DrMaisie", "Isabel", "Katarina"}
local gcsaCassandraList  = {"CassandraH", "CassandraG"}

--Generic NPCs and Enemies.
local gcsaGenericNPCList = {"GenericF0", "GenericF1", "GenericM0", "GenericM1", "GenericBiolabsFBlue", "GenericBiolabsFRed", "GenericBiolabsFGreen", "GenericBiolabsFYellow", "GenericBiolabsMBlue", "GenericBiolabsMRed",
    "GenericBiolabsMGreen", "GenericBiolabsMYellow", "LatexBlonde", "LatexBrown", "LatexRed"}
local gcsaChapter5List   = {"Alraune", "609144", "20", "Doll", "DollGrn", "DollPrp", "DollInfluenced", "Electrosprite", "GolemLordA", "GolemLordB", "GolemLordC", "GolemLordD", "GolemSlave", "GolemSlaveP", "GolemSlaveR",
    "EldritchDream", "Horrible", "Hoodie", "InnGeisha", "BandageImp", "Electrosprite", "LatexDrone", "Maisie", "SkullCrawler", "SteamDroid", "Scraprat", "SecurityBot", "SecurityBotBroke", "Raibie", "Raiju", "Vivify",
    "VivifyBlack", "VoidRift"}
local gcsaOtherNPCList   = {"Tram"}

--Special Frame Lists.
local gcsaCWSpecialList     = {"2855", "SX399", "Christine_DarkM", "Christine_DreamGirl", "Christine_Golem", "Christine_Human", "Christine_Latex", "Christine_Male", "Christine_Raibie", "Christine_Raiju",
    "Christine_RaijuClothes", "Alraune"}
local gcsaCrouchSpecialList = {"SKIP"}
local gcsaWoundedSpeciaList = {"CassandraH", "Doll", "GenericF1", "GolemLord", "GolemSlave", "LatexDrone"}

--Half-Frame Lists. Used for some NPCs with two-directional movement, like sheep and chickens.
local gcsaHalfFrameList = {"Sheep", "Chicken"}

--[======================================== Load via List ========================================]
--Player's Party
fnExtractSpriteList(gcsaChristineList,  true, "Christine_")
fnExtractSpriteList(gcsaOtherPartyList, true)
fnExtractSpriteList(gcsaOtherEightList, true)

--Major NPCs
fnExtractSpriteList(gcsaMajorNPCList, false)
fnExtractSpriteList(gcsaCassandraList, false)

--Generic NPCs and Enemies.
fnExtractSpriteList(gcsaGenericNPCList, false)
fnExtractSpriteList(gcsaChapter5List,   false)
fnExtractSpriteList(gcsaOtherNPCList,   false)

--Special Frames with common names
DL_AddPath("Root/Images/Sprites/Special/")
fnExtractSpecialFrames(gcsaCWSpecialList, "Crouch", "Wounded")
fnExtractSpecialFrames(gcsaCrouchSpecialList, "Crouch")
fnExtractSpecialFrames(gcsaWoundedSpeciaList, "Wounded")

--Half-Frame Lists.
fnExtractSpriteListEW(gcsaHalfFrameList)

--[==================================== Misc Character Sprites ===================================]
--[Crowbar Chan]
--*Nyu intensifies*
DL_AddPath("Root/Images/Sprites/CrowbarChan/")
DL_ExtractBitmap("CrowbarChan|0", "Root/Images/Sprites/CrowbarChan/0")
DL_ExtractBitmap("CrowbarChan|1", "Root/Images/Sprites/CrowbarChan/1")
DL_ExtractBitmap("CrowbarChan|2", "Root/Images/Sprites/CrowbarChan/2")

--[Gala Lord Golems]
--These NPCs only have facing frames, no walking frames.
local sCurrentLetter = "A"
while(true) do
    
    --Add the remap.
    local sNPCName = "GolemFancy" .. sCurrentLetter
    fnLoadCharacterGraphicsFacing(sNPCName, "Root/Images/Sprites/" .. sNPCName .. "/")
    
    --If this was the ending letter, or the letter 'Z', stop.
    if(sCurrentLetter == "X" or sCurrentLetter == "Z") then break end
    
    --Move to the next letter.
    local iByteValue = string.byte(sCurrentLetter)
    sCurrentLetter = string.char(iByteValue + 1)
end

--[Non-Standard Special Frames]
--Unit 2855
DL_ExtractBitmap("Spcl|2855|Chair0", "Root/Images/Sprites/Special/55|Chair0")
DL_ExtractBitmap("Spcl|2855|Chair1", "Root/Images/Sprites/Special/55|Chair1")
DL_ExtractBitmap("Spcl|2855|Chair2", "Root/Images/Sprites/Special/55|Chair2")
DL_ExtractBitmap("Spcl|2855|Chair3", "Root/Images/Sprites/Special/55|Chair3")
DL_ExtractBitmap("Spcl|2855|Chair4", "Root/Images/Sprites/Special/55|Chair4")
DL_ExtractBitmap("Spcl|2855|Crouch", "Root/Images/Sprites/Special/55|Crouch")
DL_ExtractBitmap("Spcl|2855|Wounded","Root/Images/Sprites/Special/55|Wounded")
DL_ExtractBitmap("Spcl|2855|Downed", "Root/Images/Sprites/Special/55|Downed")
DL_ExtractBitmap("Spcl|2855|Stealth","Root/Images/Sprites/Special/55|Stealth")
DL_ExtractBitmap("Spcl|2855|Draw0",  "Root/Images/Sprites/Special/55|Draw0")
DL_ExtractBitmap("Spcl|2855|Draw1",  "Root/Images/Sprites/Special/55|Draw1")
DL_ExtractBitmap("Spcl|2855|Draw2",  "Root/Images/Sprites/Special/55|Draw2")
DL_ExtractBitmap("Spcl|2855|Exec0",  "Root/Images/Sprites/Special/55|Exec0")
DL_ExtractBitmap("Spcl|2855|Exec1",  "Root/Images/Sprites/Special/55|Exec1")
DL_ExtractBitmap("Spcl|2855|Exec2",  "Root/Images/Sprites/Special/55|Exec2")
DL_ExtractBitmap("Spcl|2855|Exec3",  "Root/Images/Sprites/Special/55|Exec3")
DL_ExtractBitmap("Spcl|2855|Exec4",  "Root/Images/Sprites/Special/55|Exec4")
DL_ExtractBitmap("Spcl|2855|Exec5",  "Root/Images/Sprites/Special/55|Exec5")
DL_ExtractBitmap("Spcl|2855|Heavy0", "Root/Images/Sprites/Special/55|Heavy0")
DL_ExtractBitmap("Spcl|2855|Heavy1", "Root/Images/Sprites/Special/55|Heavy1")
DL_ExtractBitmap("Spcl|2855|Heavy2", "Root/Images/Sprites/Special/55|Heavy2")
DL_ExtractBitmap("Spcl|2855|Heavy3", "Root/Images/Sprites/Special/55|Heavy3")

--Christine.
DL_ExtractBitmap("Spcl|Christine_Golem|Laugh0",   "Root/Images/Sprites/Special/Christine|Laugh0")
DL_ExtractBitmap("Spcl|Christine_Golem|Laugh1",   "Root/Images/Sprites/Special/Christine|Laugh1")
DL_ExtractBitmap("Spcl|Christine_Golem|Sad",      "Root/Images/Sprites/Special/Christine|Sad")
DL_ExtractBitmap("Spcl|Christine_Human|BedSleep", "Root/Images/Sprites/Special/Christine_Human|BedSleep")
DL_ExtractBitmap("Spcl|Christine_Human|BedWake",  "Root/Images/Sprites/Special/Christine_Human|BedWake")
DL_ExtractBitmap("Spcl|Christine_Human|BedFull",  "Root/Images/Sprites/Special/Christine_Human|BedFull")
DL_ExtractBitmap("Spcl|Christine_Human|BedWakeR", "Root/Images/Sprites/Special/Christine_Human|BedWakeR")
DL_ExtractBitmap("Spcl|Christine_Human|BedFullR", "Root/Images/Sprites/Special/Christine_Human|BedFullR")
DL_ExtractBitmap("Spcl|Christine_Doll|FlatbackC", "Root/Images/Sprites/Special/Christine_Doll|FlatbackC")
DL_ExtractBitmap("Spcl|Christine_Doll|FlatbackO", "Root/Images/Sprites/Special/Christine_Doll|FlatbackO")

--Sophie.
DL_ExtractBitmap("Spcl|Sophie|Laugh0", "Root/Images/Sprites/Special/Sophie|Laugh0")
DL_ExtractBitmap("Spcl|Sophie|Laugh1", "Root/Images/Sprites/Special/Sophie|Laugh1")
DL_ExtractBitmap("Spcl|Sophie|Cry0",   "Root/Images/Sprites/Special/Sophie|Cry0")
DL_ExtractBitmap("Spcl|Sophie|Cry1",   "Root/Images/Sprites/Special/Sophie|Cry1")

--SX-399.
DL_ExtractBitmap("Spcl|SX399|Laugh0", "Root/Images/Sprites/Special/SX399|Laugh0")
DL_ExtractBitmap("Spcl|SX399|Laugh1", "Root/Images/Sprites/Special/SX399|Laugh1")

--Scraprat-splosion!
for i = 0, 11, 1 do
	local sName = "Explode"
	if(i < 10) then sName = sName .. "0" end
	sName = sName .. i
	DL_ExtractBitmap("Spcl|Scraprat|" .. sName, "Root/Images/Sprites/Special/Scraprat|" .. sName)
end

--Christine and Sophie
for i = 0, 9, 1 do
	DL_ExtractBitmap("Spcl|ChristineSophie|HoldHands" .. i, "Root/Images/Sprites/Special/ChristineSophie|HoldHands" .. i)
end
for i = 0, 7, 1 do
	DL_ExtractBitmap("Spcl|ChristineSophie|Kiss" .. i, "Root/Images/Sprites/Special/ChristineSophie|Kiss" .. i)
end

--Mutant Golem
DL_ExtractBitmap("Spcl|GolemSlave|Mutant", "Root/Images/Sprites/Special/GolemSlave|Mutant")

--Rebel Golems
DL_ExtractBitmap("Spcl|GolemSlaveR|Wounded", "Root/Images/Sprites/Special/GolemSlaveR|Wounded")
DL_ExtractBitmap("Spcl|GolemSlaveR|Sitting", "Root/Images/Sprites/Special/GolemSlaveR|Sitting")

--Latex Drone's Arm Falls Off
DL_ExtractBitmap("Spcl|LatexDrone|Arm0", "Root/Images/Sprites/Special/LatexDrone|Arm0")
DL_ExtractBitmap("Spcl|LatexDrone|Arm1", "Root/Images/Sprites/Special/LatexDrone|Arm1")
DL_ExtractBitmap("Spcl|LatexDrone|Arm2", "Root/Images/Sprites/Special/LatexDrone|Arm2")
DL_ExtractBitmap("Spcl|LatexDrone|Arm3", "Root/Images/Sprites/Special/LatexDrone|Arm3")
DL_ExtractBitmap("Spcl|LatexDrone|Arm4", "Root/Images/Sprites/Special/LatexDrone|Arm4")

--Vivify
DL_ExtractBitmap("VivifyFreeze0",  "Root/Images/Sprites/Special/Vivify|Freeze0")
DL_ExtractBitmap("VivifyFreeze1",  "Root/Images/Sprites/Special/Vivify|Freeze1")
DL_ExtractBitmap("VivifyFreeze2",  "Root/Images/Sprites/Special/Vivify|Freeze2")
DL_ExtractBitmap("VivifyFreeze3",  "Root/Images/Sprites/Special/Vivify|Freeze3")
DL_ExtractBitmap("VivifyFreeze4",  "Root/Images/Sprites/Special/Vivify|Freeze4")

--[Sammy Dances]
--The power of dance is unlimited.
for i = 0, 7, 1 do
    DL_ExtractBitmap("SammyDanceA|" .. i, "Root/Images/Sprites/Special/SammyDanceA|" .. i)
    DL_ExtractBitmap("SammyDanceB|" .. i, "Root/Images/Sprites/Special/SammyDanceB|" .. i)
    DL_ExtractBitmap("SammyDanceC|" .. i, "Root/Images/Sprites/Special/SammyDanceC|" .. i)
    DL_ExtractBitmap("SammyDanceD|" .. i, "Root/Images/Sprites/Special/SammyDanceD|" .. i)
    DL_ExtractBitmap("SammyDanceE|" .. i, "Root/Images/Sprites/Special/SammyDanceE|" .. i)
    DL_ExtractBitmap("SammyDanceF|" .. i, "Root/Images/Sprites/Special/SammyDanceF|" .. i)
    DL_ExtractBitmap("SammyDanceG|" .. i, "Root/Images/Sprites/Special/SammyDanceG|" .. i)
    DL_ExtractBitmap("SammyDanceH|" .. i, "Root/Images/Sprites/Special/SammyDanceH|" .. i)
end

--Sammy's swimming frames.
DL_ExtractBitmap("SammySwimS", "Root/Images/Sprites/Special/SammySwimS")
DL_ExtractBitmap("SammySwimW", "Root/Images/Sprites/Special/SammySwimW")

--[==================================== Misc Character Sprites ===================================]
--[Bullet Impacts]
DL_AddPath("Root/Images/Sprites/Impacts/")
DL_ExtractBitmap("Impact|Bullet0", "Root/Images/Sprites/Impacts/Bullet0")
DL_ExtractBitmap("Impact|Bullet1", "Root/Images/Sprites/Impacts/Bullet1")
DL_ExtractBitmap("Impact|Bullet2", "Root/Images/Sprites/Impacts/Bullet2")
DL_ExtractBitmap("Impact|Bullet3", "Root/Images/Sprites/Impacts/Bullet3")

--[Rocks Fall]
DL_AddPath("Root/Images/Sprites/RockFalls/")
DL_ExtractBitmap("Obj|FallingRockRegA", "Root/Images/Sprites/RockFalls/RockRegA")
DL_ExtractBitmap("Obj|FallingRockRegB", "Root/Images/Sprites/RockFalls/RockRegB")
DL_ExtractBitmap("Obj|FallingRockRegC", "Root/Images/Sprites/RockFalls/RockRegC")

--[Static Burst]
DL_AddPath("Root/Images/Sprites/StaticBurst/")
for i = 0, 12, 1 do
    DL_ExtractBitmap("Spcl|StaticAnim" .. i, "Root/Images/Sprites/StaticBurst/" .. i)
end

--Clear this flag.
--Bitmap_DeactivateAtlasing()
ALB_SetTextureProperty("Special Sprite Padding", false)

--[==================================== Running Away Minigame ====================================]
DL_AddPath("Root/Images/Sprites/RunningMinigame/")
DL_ExtractBitmap("Run|All",       "Root/Images/Sprites/RunningMinigame/All")
DL_ExtractBitmap("Run|Tutorial0", "Root/Images/Sprites/RunningMinigame/Tutorial0")
DL_ExtractBitmap("Run|Tutorial1", "Root/Images/Sprites/RunningMinigame/Tutorial1")
DL_ExtractBitmap("Run|Tutorial2", "Root/Images/Sprites/RunningMinigame/Tutorial2")
DL_ExtractBitmap("Run|Tutorial3", "Root/Images/Sprites/RunningMinigame/Tutorial3")
DL_ExtractBitmap("Run|Tutorial4", "Root/Images/Sprites/RunningMinigame/Tutorial4")
DL_ExtractBitmap("Run|Tutorial5", "Root/Images/Sprites/RunningMinigame/Tutorial5")

ALB_SetTextureProperty("Restore Defaults")

SLF_Close()
