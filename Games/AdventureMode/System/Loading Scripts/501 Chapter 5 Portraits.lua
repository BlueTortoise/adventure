--[ ==================================== Chapter 5 Portraits ==================================== ]
--Loads all portraits for chapter 5.
Debug_PushPrint(gbLoadDebug, "Beginning Portrait loader.\n")

--[ ========================================= Christine ========================================= ]
--Note: Christine's Human Neutral is already loaded.
Debug_Print("Loading Christine's portraits.\n")
SLF_Open(gsaDatafilePaths.sChristinePath)

--Form Dialogue
DL_AddPath("Root/Images/Portraits/ChristineDialogue/")
DL_AddPath("Root/Images/Portraits/Combat/")
local saPattern = {"Neutral", "Happy", "Blush", "Smirk", "Sad", "Scared", "Offended", "Cry", "Laugh", "Angry", "PDU"}
local saForms = {"", "HumanNude", "Darkmatter", "Golem", "Latex", "SteamDroid", "GolemGala", "Raiju", "RaijuClothes", "Eldritch", "Electrosprite", "Doll", "DollSweater", "DollNude"}

for i = 1, #saForms, 1 do
    for p = 1, #saPattern, 1 do
        if(saForms[i] ~= "") then
            DL_ExtractBitmap("Por|Christine" .. saForms[i].. "|" .. saPattern[p], "Root/Images/Portraits/ChristineDialogue/" .. saForms[i] .. "|" .. saPattern[p])
        else
            DL_ExtractBitmap("Por|Christine|" .. saPattern[p], "Root/Images/Portraits/ChristineDialogue/" .. saPattern[p])
        end
    end
end

--Special:
DL_ExtractBitmap("Por|ChristineGolem|Serious", "Root/Images/Portraits/ChristineDialogue/Golem|Serious")
DL_ExtractBitmap("Por|ChristineDoll|Serious",  "Root/Images/Portraits/ChristineDialogue/Doll|Serious")

--Male form.
DL_ExtractBitmap("Por|Chris|Neutral",  "Root/Images/Portraits/ChristineDialogue/Male|Neutral")

--Combat Portraits
DL_ExtractBitmap("Party|Christine_Darkmatter",    "Root/Images/Portraits/Combat/Christine_Darkmatter")
DL_ExtractBitmap("Party|Christine_Doll",          "Root/Images/Portraits/Combat/Christine_Doll")
DL_ExtractBitmap("Party|Christine_Eldritch",      "Root/Images/Portraits/Combat/Christine_Eldritch")
DL_ExtractBitmap("Party|Christine_Electrosprite", "Root/Images/Portraits/Combat/Christine_Electrosprite")
DL_ExtractBitmap("Party|Christine_Golem",         "Root/Images/Portraits/Combat/Christine_Golem")
DL_ExtractBitmap("Party|Christine_GolemDress",    "Root/Images/Portraits/Combat/Christine_Golem_Dress")
DL_ExtractBitmap("Party|Christine_Golem_Forward", "Root/Images/Portraits/Combat/Christine_Golem_Forward")
DL_ExtractBitmap("Party|Christine_Human",         "Root/Images/Portraits/Combat/Christine_Human")
DL_ExtractBitmap("Party|Christine_Latex",         "Root/Images/Portraits/Combat/Christine_Latex")
DL_ExtractBitmap("Party|Christine_Male",          "Root/Images/Portraits/Combat/Christine_Male")
DL_ExtractBitmap("Party|Christine_Raibie",        "Root/Images/Portraits/Combat/Christine_Raibie")
DL_ExtractBitmap("Party|Christine_Raiju",         "Root/Images/Portraits/Combat/Christine_Raiju")
DL_ExtractBitmap("Party|Christine_RaijuClothed",  "Root/Images/Portraits/Combat/Christine_RaijuClothed")
DL_ExtractBitmap("Party|Christine_SteamDroid",    "Root/Images/Portraits/Combat/Christine_SteamDroid")

--[ ============================================ PDU ============================================ ]
--PDU. Counts as a character.
Debug_Print("Loading PDU's portraits.\n")
SLF_Open(gsaDatafilePaths.sPDUPath)

--Normal Clothes
DL_AddPath("Root/Images/Portraits/PDUDialogue/")
DL_ExtractBitmap("Por|PDURobot|Neutral",  "Root/Images/Portraits/PDUDialogue/Neutral")
DL_ExtractBitmap("Por|PDURobot|Happy",    "Root/Images/Portraits/PDUDialogue/Happy")
DL_ExtractBitmap("Por|PDURobot|Alarmed",  "Root/Images/Portraits/PDUDialogue/Alarmed")
DL_ExtractBitmap("Por|PDURobot|Question", "Root/Images/Portraits/PDUDialogue/Question")
DL_ExtractBitmap("Por|PDURobot|Quiet",    "Root/Images/Portraits/PDUDialogue/Quiet")

--[ ============================================= 55 ============================================ ]
--55. A doll!
Debug_Print("Loading 55's portraits.\n")
SLF_Open(gsaDatafilePaths.s55Path)

--Normal Clothes
DL_AddPath("Root/Images/Portraits/2855Dialogue/")
DL_ExtractBitmap("Por|55|Neutral",  "Root/Images/Portraits/2855Dialogue/Neutral")
DL_ExtractBitmap("Por|55|Down",     "Root/Images/Portraits/2855Dialogue/Down")
DL_ExtractBitmap("Por|55|Blush",    "Root/Images/Portraits/2855Dialogue/Blush")
DL_ExtractBitmap("Por|55|Smirk",    "Root/Images/Portraits/2855Dialogue/Smirk")
DL_ExtractBitmap("Por|55|Upset",    "Root/Images/Portraits/2855Dialogue/Upset")
DL_ExtractBitmap("Por|55|Smug",     "Root/Images/Portraits/2855Dialogue/Smug")
DL_ExtractBitmap("Por|55|Offended", "Root/Images/Portraits/2855Dialogue/Offended")
DL_ExtractBitmap("Por|55|Angry",    "Root/Images/Portraits/2855Dialogue/Angry")
DL_ExtractBitmap("Por|55|Broken",   "Root/Images/Portraits/2855Dialogue/Broken")
DL_ExtractBitmap("Por|55|Cry",      "Root/Images/Portraits/2855Dialogue/Cry")

--Normal Clothes, Reversed Eyes
DL_AddPath("Root/Images/Portraits/2855DialogueRev/")
DL_ExtractBitmap("Por|55Rev|Neutral",  "Root/Images/Portraits/2855DialogueRev/Neutral")
DL_ExtractBitmap("Por|55Rev|Down",     "Root/Images/Portraits/2855DialogueRev/Down")
DL_ExtractBitmap("Por|55Rev|Blush",    "Root/Images/Portraits/2855DialogueRev/Blush")
DL_ExtractBitmap("Por|55Rev|Smirk",    "Root/Images/Portraits/2855DialogueRev/Smirk")
DL_ExtractBitmap("Por|55Rev|Upset",    "Root/Images/Portraits/2855DialogueRev/Upset")
DL_ExtractBitmap("Por|55Rev|Smug",     "Root/Images/Portraits/2855DialogueRev/Smug")
DL_ExtractBitmap("Por|55Rev|Offended", "Root/Images/Portraits/2855DialogueRev/Offended")
DL_ExtractBitmap("Por|55Rev|Angry",    "Root/Images/Portraits/2855DialogueRev/Angry")
DL_ExtractBitmap("Por|55Rev|Broken",   "Root/Images/Portraits/2855DialogueRev/Broken")
DL_ExtractBitmap("Por|55Rev|Cry",      "Root/Images/Portraits/2855DialogueRev/Cry")

--Combat
DL_ExtractBitmap("Party|55", "Root/Images/Portraits/Combat/55")

--55's Sundress Variant
DL_AddPath("Root/Images/Portraits/2855SundressDialogue/")
DL_ExtractBitmap("Por|55Sundress|Neutral",  "Root/Images/Portraits/2855SundressDialogue/Neutral")
DL_ExtractBitmap("Por|55Sundress|Down",     "Root/Images/Portraits/2855SundressDialogue/Down")
DL_ExtractBitmap("Por|55Sundress|Blush",    "Root/Images/Portraits/2855SundressDialogue/Blush")
DL_ExtractBitmap("Por|55Sundress|Smirk",    "Root/Images/Portraits/2855SundressDialogue/Smirk")
DL_ExtractBitmap("Por|55Sundress|Upset",    "Root/Images/Portraits/2855SundressDialogue/Upset")
DL_ExtractBitmap("Por|55Sundress|Smug",     "Root/Images/Portraits/2855SundressDialogue/Smug")
DL_ExtractBitmap("Por|55Sundress|Offended", "Root/Images/Portraits/2855SundressDialogue/Offended")
DL_ExtractBitmap("Por|55Sundress|Angry",    "Root/Images/Portraits/2855SundressDialogue/Angry")
DL_ExtractBitmap("Por|55Sundress|Broken",   "Root/Images/Portraits/2855SundressDialogue/Broken")
DL_ExtractBitmap("Por|55Sundress|Cry",      "Root/Images/Portraits/2855SundressDialogue/Cry")

--55's Sundress Variant, Reversed
DL_AddPath("Root/Images/Portraits/2855SundressDialogueRev/")
DL_ExtractBitmap("Por|55SundressRev|Neutral",  "Root/Images/Portraits/2855SundressDialogueRev/Neutral")
DL_ExtractBitmap("Por|55SundressRev|Down",     "Root/Images/Portraits/2855SundressDialogueRev/Down")
DL_ExtractBitmap("Por|55SundressRev|Blush",    "Root/Images/Portraits/2855SundressDialogueRev/Blush")
DL_ExtractBitmap("Por|55SundressRev|Smirk",    "Root/Images/Portraits/2855SundressDialogueRev/Smirk")
DL_ExtractBitmap("Por|55SundressRev|Upset",    "Root/Images/Portraits/2855SundressDialogueRev/Upset")
DL_ExtractBitmap("Por|55SundressRev|Smug",     "Root/Images/Portraits/2855SundressDialogueRev/Smug")
DL_ExtractBitmap("Por|55SundressRev|Offended", "Root/Images/Portraits/2855SundressDialogueRev/Offended")
DL_ExtractBitmap("Por|55SundressRev|Angry",    "Root/Images/Portraits/2855SundressDialogueRev/Angry")
DL_ExtractBitmap("Por|55SundressRev|Broken",   "Root/Images/Portraits/2855SundressDialogueRev/Broken")
DL_ExtractBitmap("Por|55SundressRev|Cry",      "Root/Images/Portraits/2855SundressDialogueRev/Cry")

--[ ============================================= 56 ============================================ ]
--55's sister. Yells a lot. Has a small emote set.
Debug_Print("Loading 56's portraits.\n")
SLF_Open(gsaDatafilePaths.s56Path)

--Normal Clothes
DL_AddPath("Root/Images/Portraits/2856Dialogue/")
DL_ExtractBitmap("Por|56|Neutral",   "Root/Images/Portraits/2856Dialogue/Neutral")
DL_ExtractBitmap("Por|56|Punchable", "Root/Images/Portraits/2856Dialogue/Punchable")
DL_ExtractBitmap("Por|56|Angry",     "Root/Images/Portraits/2856Dialogue/Angry")
DL_ExtractBitmap("Por|56|Yelling",   "Root/Images/Portraits/2856Dialogue/Yelling")

--Nude Variant
DL_AddPath("Root/Images/Portraits/2856NudeDialogue/")
DL_ExtractBitmap("Por|56Nude|Neutral",   "Root/Images/Portraits/2856NudeDialogue/Neutral")
DL_ExtractBitmap("Por|56Nude|Punchable", "Root/Images/Portraits/2856NudeDialogue/Punchable")
DL_ExtractBitmap("Por|56Nude|Angry",     "Root/Images/Portraits/2856NudeDialogue/Angry")
DL_ExtractBitmap("Por|56Nude|Yelling",   "Root/Images/Portraits/2856NudeDialogue/Yelling")

--[ =========================================== SX-399 ========================================== ]
--The one and only Steam Lord.
Debug_Print("Loading SX-399's portraits.\n")
SLF_Open(gsaDatafilePaths.sSX399Path)

--Normal
DL_AddPath("Root/Images/Portraits/SX399Dialogue/")
DL_ExtractBitmap("Por|SX399|Neutral",  "Root/Images/Portraits/SX399Dialogue/Neutral")
DL_ExtractBitmap("Por|SX399|Happy",    "Root/Images/Portraits/SX399Dialogue/Happy")
DL_ExtractBitmap("Por|SX399|Blush",    "Root/Images/Portraits/SX399Dialogue/Blush")
DL_ExtractBitmap("Por|SX399|Smirk",    "Root/Images/Portraits/SX399Dialogue/Smirk")
DL_ExtractBitmap("Por|SX399|Sad",      "Root/Images/Portraits/SX399Dialogue/Sad")
DL_ExtractBitmap("Por|SX399|Flirt",    "Root/Images/Portraits/SX399Dialogue/Flirt")
DL_ExtractBitmap("Por|SX399|Angry",    "Root/Images/Portraits/SX399Dialogue/Angry")
DL_ExtractBitmap("Por|SX399|Flirt2",   "Root/Images/Portraits/SX399Dialogue/Flirt2")
DL_ExtractBitmap("Por|SX399|Steam",    "Root/Images/Portraits/SX399Dialogue/Steam")

--Combat.
DL_ExtractBitmap("Party|SX399", "Root/Images/Portraits/Combat/SX-399")

--[ =========================================== Sophie ========================================== ]
--[Sophie's Portraits]
Debug_Print("Loading Sophie's portraits.\n")
SLF_Open(gsaDatafilePaths.sSophiePath)

--Normal
DL_AddPath("Root/Images/Portraits/SophieDialogue/")
DL_ExtractBitmap("Por|Sophie|Neutral",   "Root/Images/Portraits/SophieDialogue/Neutral")
DL_ExtractBitmap("Por|Sophie|Happy",     "Root/Images/Portraits/SophieDialogue/Happy")
DL_ExtractBitmap("Por|Sophie|Blush",     "Root/Images/Portraits/SophieDialogue/Blush")
DL_ExtractBitmap("Por|Sophie|Smirk",     "Root/Images/Portraits/SophieDialogue/Smirk")
DL_ExtractBitmap("Por|Sophie|Sad",       "Root/Images/Portraits/SophieDialogue/Sad")
DL_ExtractBitmap("Por|Sophie|Surprise",  "Root/Images/Portraits/SophieDialogue/Surprised")
DL_ExtractBitmap("Por|Sophie|Offended",  "Root/Images/Portraits/SophieDialogue/Offended")

--Gala variant.
DL_AddPath("Root/Images/Portraits/SophieDialogueGala/")
DL_ExtractBitmap("Por|SophieGala|Neutral",   "Root/Images/Portraits/SophieDialogueGala/Neutral")
DL_ExtractBitmap("Por|SophieGala|Happy",     "Root/Images/Portraits/SophieDialogueGala/Happy")
DL_ExtractBitmap("Por|SophieGala|Blush",     "Root/Images/Portraits/SophieDialogueGala/Blush")
DL_ExtractBitmap("Por|SophieGala|Smirk",     "Root/Images/Portraits/SophieDialogueGala/Smirk")
DL_ExtractBitmap("Por|SophieGala|Sad",       "Root/Images/Portraits/SophieDialogueGala/Sad")
DL_ExtractBitmap("Por|SophieGala|Surprise",  "Root/Images/Portraits/SophieDialogueGala/Surprised")
DL_ExtractBitmap("Por|SophieGala|Offended",  "Root/Images/Portraits/SophieDialogueGala/Offended")

--Domina variant.
DL_AddPath("Root/Images/Portraits/SophieDialogueDomina/")
DL_ExtractBitmap("Por|SophieWhip|Neutral",   "Root/Images/Portraits/SophieDialogueDomina/Neutral")
DL_ExtractBitmap("Por|SophieWhip|Happy",     "Root/Images/Portraits/SophieDialogueDomina/Happy")
DL_ExtractBitmap("Por|SophieWhip|Blush",     "Root/Images/Portraits/SophieDialogueDomina/Blush")
DL_ExtractBitmap("Por|SophieWhip|Smirk",     "Root/Images/Portraits/SophieDialogueDomina/Smirk")

--[ =========================================== JX-101 ========================================== ]
--While not a full party member, JX-101 is a guest party member and has a few emotes.
Debug_Print("Loading JX-101's portraits.\n")
SLF_Open(gsaDatafilePaths.sJX101Path)

--Normal.
DL_AddPath("Root/Images/Portraits/JX-101/")
DL_ExtractBitmap("Por|JX101|Neutral", "Root/Images/Portraits/JX-101/Neutral")
DL_ExtractBitmap("Por|JX101|HatA", "Root/Images/Portraits/JX-101/HatA")
DL_ExtractBitmap("Por|JX101|HatB", "Root/Images/Portraits/JX-101/HatB")
DL_ExtractBitmap("Por|JX101|HatC", "Root/Images/Portraits/JX-101/HatC")

--Combat.
DL_ExtractBitmap("Party|JX101", "Root/Images/Portraits/Combat/JX-101")

--[ =========================================== Sammy =========================================== ]
--Sammy Davis IS Agent Almond IN Some Moths Do! She also appears in other chapters.
Debug_Print("Loading Sammy's portraits.\n")
SLF_Open(gsaDatafilePaths.sSammyPath)

--Normal/Almond
DL_AddPath("Root/Images/Portraits/Sammy/")
DL_ExtractBitmap("Por|Sammy|Neutral", "Root/Images/Portraits/Sammy/Neutral")

--[ ===================================== Minor Characters ====================================== ]
Debug_Print("Loading minor character portraits.\n")
SLF_Open(gsaDatafilePaths.sChapter5EmotePath)

--Load
DL_AddPath("Root/Images/Portraits/Golem/")
DL_ExtractBitmap("Por|Golem|Neutral", "Root/Images/Portraits/Golem/Neutral")
DL_ExtractBitmap("Por|GolemPack|Neutral", "Root/Images/Portraits/Golem/Pack")
DL_AddPath("Root/Images/Portraits/GolemLord/")
DL_ExtractBitmap("Por|GolemLord|Neutral", "Root/Images/Portraits/GolemLord/Neutral")
DL_AddPath("Root/Images/Portraits/GolemLordB/")
DL_ExtractBitmap("Por|GolemLordB|Neutral", "Root/Images/Portraits/GolemLordB/Neutral")
DL_AddPath("Root/Images/Portraits/GolemLordC/")
DL_ExtractBitmap("Por|GolemLordC|Neutral", "Root/Images/Portraits/GolemLordC/Neutral")
DL_AddPath("Root/Images/Portraits/Narissa/")
DL_ExtractBitmap("Por|Raiju|Neutral", "Root/Images/Portraits/Narissa/Neutral")
DL_AddPath("Root/Images/Portraits/Maisie/")
DL_ExtractBitmap("Por|Maisie|Neutral", "Root/Images/Portraits/Maisie/Neutral")
DL_AddPath("Root/Images/Portraits/Isabel/")
DL_ExtractBitmap("Por|Isabel|Neutral", "Root/Images/Portraits/Isabel/Neutral")
DL_AddPath("Root/Images/Portraits/Katarina/")
DL_ExtractBitmap("Por|Katarina|Neutral", "Root/Images/Portraits/Katarina/Neutral")

--Golem Rebels
DL_ExtractBitmap("Por|GolemRebelHead|Neutral",  "Root/Images/Portraits/NPCs/GolemRebelHead")
DL_ExtractBitmap("Por|GolemRebelScarf|Neutral", "Root/Images/Portraits/NPCs/GolemRebelScarf")

--Special: Shadowed Doll as Administrator
DL_ExtractBitmap("Por|DollAsAdmin|Neutral",  "Root/Images/Portraits/NPCs/DollAsAdmin")

--Special: Layered Portraits
DL_AddPath("Root/Images/Portraits/Layered/")
DL_ExtractBitmap("Por|Admin|Base",   "Root/Images/Portraits/Layered/Admin|Base")
DL_ExtractBitmap("Por|Admin|Layer1", "Root/Images/Portraits/Layered/Admin|Layer1")
DL_ExtractBitmap("Por|Admin|Layer2", "Root/Images/Portraits/Layered/Admin|Layer2")

--[Bosses]
DL_AddPath("Root/Images/Portraits/609144/")
DL_AddPath("Root/Images/Portraits/Vivify/")
DL_ExtractBitmap("Por|609144|Neutral", "Root/Images/Portraits/609144/Neutral")
DL_ExtractBitmap("Por|Vivify|Neutral", "Root/Images/Portraits/Vivify/Neutral")

--[Crowbar Chan]
SLF_Open(gsaDatafilePaths.sChapter0EmotePath)
DL_AddPath("Root/Images/Portraits/CrowbarChan/")
DL_ExtractBitmap("Por|CrowbarChan|Neutral",  "Root/Images/Portraits/CrowbarChan/Neutral")

--[ ====================================== Combat Portraits ===================================== ]
--[Chapter 0 Enemies]
--File
SLF_Open(gsaDatafilePaths.sChapter0CombatPath)

--Normal
DL_ExtractBitmap("Enemy|BandageGoblin",   "Root/Images/Portraits/Combat/BandageGoblin")
DL_ExtractBitmap("Enemy|BestFriend",      "Root/Images/Portraits/Combat/BestFriend")
DL_ExtractBitmap("Enemy|Hoodie",          "Root/Images/Portraits/Combat/Hoodie")
DL_ExtractBitmap("Enemy|Horrible",        "Root/Images/Portraits/Combat/Horrible")
DL_ExtractBitmap("Enemy|InnGeisha",       "Root/Images/Portraits/Combat/InnGeisha")
DL_ExtractBitmap("Enemy|MirrorImage",     "Root/Images/Portraits/Combat/MirrorImage")
DL_ExtractBitmap("Enemy|SkullCrawler",    "Root/Images/Portraits/Combat/SkullCrawler")
DL_ExtractBitmap("Enemy|VoidRift",        "Root/Images/Portraits/Combat/VoidRift")

--[Chapter 5 Enemies]
--File
SLF_Open(gsaDatafilePaths.sChapter5CombatPath)

--Normal
DL_ExtractBitmap("Enemy|DarkmatterGirl",  "Root/Images/Portraits/Combat/DarkmatterGirl")
DL_ExtractBitmap("Enemy|Doll",            "Root/Images/Portraits/Combat/Doll")
DL_ExtractBitmap("Enemy|Dreamer",         "Root/Images/Portraits/Combat/Dreamer")
DL_ExtractBitmap("Enemy|Electrosprite",   "Root/Images/Portraits/Combat/Electrosprite")
DL_ExtractBitmap("Enemy|GolemLord",       "Root/Images/Portraits/Combat/GolemLord")
DL_ExtractBitmap("Enemy|GolemSlave",      "Root/Images/Portraits/Combat/GolemSlave")
DL_ExtractBitmap("Enemy|LatexDrone",      "Root/Images/Portraits/Combat/LatexDrone")
DL_ExtractBitmap("Enemy|Raiju",           "Root/Images/Portraits/Combat/Raiju")
DL_ExtractBitmap("Enemy|Raibie",          "Root/Images/Portraits/Combat/Raibie")
DL_ExtractBitmap("Enemy|Scraprat",        "Root/Images/Portraits/Combat/Scraprat")
DL_ExtractBitmap("Enemy|WreckedBot",      "Root/Images/Portraits/Combat/WreckedBot")

--Bosses
DL_ExtractBitmap("Enemy|609144",          "Root/Images/Portraits/Combat/609144")
DL_ExtractBitmap("Enemy|Vivify",          "Root/Images/Portraits/Combat/Vivify")
DL_ExtractBitmap("Enemy|Serenity",        "Root/Images/Portraits/Combat/Serenity")
DL_ExtractBitmap("Enemy|TechMonstrosity", "Root/Images/Portraits/Combat/TechMonstrosity")

--[ ========================================== Clean Up ========================================= ]
--[Clean]
SLF_Close()
Debug_PopPrint("Completed Portrait Loader.\n")
