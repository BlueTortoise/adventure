--[Dialogue Actors: Chapter 5]
--All dialogue actors from chapter 5.

--[Functions]
--Creates a dialogue actor with the listed aliases and a listed set of emotions.
-- All created actors use their own names as an alias, and always have a neutral emotion.
-- sPathPattern is formatted: "Root/Images/Portraits/MeiDialogue/" or thereabouts. Include the final slash.
local fnCreateDialogueActor = function(sActorName, sVoiceSample, sPathPattern, bIsNotWide, saAliasList, saEmotionList)
    
    --Arg Check
    if(sActorName == nil) then return end
    if(sVoiceSample == nil) then return end
    if(sPathPattern == nil) then return end
    if(bIsNotWide == nil) then return end
    if(saAliasList == nil) then return end
    if(saEmotionList == nil) then return end
    
    --Create.
    DialogueActor_Create(sActorName)

        --Alias listing. Always create the same alias as the character's name.
        DialogueActor_SetProperty("Add Alias", sActorName)

        --Create additional aliases if the list contains any entries.
        local i = 1
        while(saAliasList[i] ~= nil and saAliasList[i] ~= "NOALIAS") do
            DialogueActor_SetProperty("Add Alias", saAliasList[i])
            i = i + 1
        end

        --Create the neutral emotion. All entities have this. If the saEmotionList contains no entries, then
        -- the neutral emotion is the sPathPattern.
        if(saEmotionList[1] == nil or saEmotionList[1] == "NEUTRALISPATH") then
            DialogueActor_SetProperty("Add Emotion", "Neutral", sPathPattern, bIsNotWide)
        
        --Otherwise, the emotion listing follows the pattern provided.
        else
        
            --The first emotion is "Neutral". This is the case even if that's the only emotion, if you want to use the path pattern.
            DialogueActor_SetProperty("Add Emotion", "Neutral", sPathPattern .. "Neutral", bIsNotWide)
        
            --Now iterate across the rest of the list.
            i = 2
            while(saEmotionList[i] ~= nil) do
                DialogueActor_SetProperty("Add Emotion", saEmotionList[i], sPathPattern .. saEmotionList[i], bIsNotWide)
                i = i + 1
            end
        
        end

    --Clean up.
    DL_PopActiveObject()

    --Set the voice sample.
    WD_SetProperty("Register Voice", sActorName, sVoiceSample)
    
end

--[ =================================== Party/Important Actors ================================== ]
--[Setup]
--Alias listings.
local saNoAliases        = {"NOALIAS"}
local saChristineAliases = {"Chris", "771852", "772890"}
local saPDUAliases       = {"NOALIAS"}
local sa2855Aliases      = {"Girl", "55", "Tiffany"}
local saSX399Aliases     = {"Young Droid"}
local saSophieAliases    = {"499323"}
local saJX101Aliases     = {"Battle Droid"}
local saMaramAliases     = {"Rilmani"}
local saSammyAliases     = {"Almond"}

--Emotion listings.
local saNoEmotions         = {"Neutral"}
local saChristineEmotions  = {"Neutral", "Smirk", "Happy", "Blush", "Sad", "Scared", "Offended", "Cry", "Laugh", "Angry", "PDU"}
local saPDUEmotions        = {"Neutral", "Happy", "Alarmed", "Question", "Quiet"}
local sa2855Emotions       = {"Neutral", "Down",  "Blush", "Smirk", "Upset", "Smug", "Offended", "Angry", "Broken", "Cry"}
local saSX399Emotions      = {"Neutral", "Happy", "Blush", "Smirk", "Sad", "Flirt", "Angry", "Flirt2", "Steam"}
local saSophieEmotions     = {"Neutral", "Happy", "Blush", "Smirk", "Sad", "Surprised", "Offended"}
local saJX101Emotions      = {"Neutral", "HatA", "HatB", "HatC"}
local saSammyEmotions      = {"Neutral"}

--[Chapter 5]
--Mainline
fnCreateDialogueActor("Christine",   "Voice|Christine", "Root/Images/Portraits/ChristineDialogue/", true, saChristineAliases, saChristineEmotions)
fnCreateDialogueActor("PDU",         "Voice|Narrator",  "Root/Images/Portraits/PDUDialogue/",       true, saPDUAliases,       saPDUEmotions)
fnCreateDialogueActor("2855",        "Voice|55",        "Root/Images/Portraits/2855Dialogue/",      true, sa2855Aliases,      sa2855Emotions)
fnCreateDialogueActor("SX-399",      "Voice|SX399",     "Root/Images/Portraits/SX399Dialogue/",     true, saSX399Aliases,     saSX399Emotions)
fnCreateDialogueActor("Sophie",      "Voice|Sophie",    "Root/Images/Portraits/SophieDialogue/",    true, saSophieAliases,    saSophieEmotions)
fnCreateDialogueActor("JX-101",      "Voice|JX101",     "Root/Images/Portraits/JX-101/",            true, saJX101Aliases,     saJX101Emotions)
fnCreateDialogueActor("609144",      "Voice|GenericN02","Root/Images/Portraits/609144/",            true, saNoAliases,        saNoEmotions)
fnCreateDialogueActor("CrowbarChan", "Voice|Septima",   "Root/Images/Portraits/CrowbarChan/",       true, saNoAliases, saNoEmotions)

--Right-Facing Case for 55
DialogueActor_Push("2855")
    DialogueActor_SetProperty("Add Emotion Rev", "Neutral",  "Root/Images/Portraits/2855DialogueRev/Neutral",  true)
    DialogueActor_SetProperty("Add Emotion Rev", "Down",     "Root/Images/Portraits/2855DialogueRev/Down",     true)
    DialogueActor_SetProperty("Add Emotion Rev", "Blush",    "Root/Images/Portraits/2855DialogueRev/Blush",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Smirk",    "Root/Images/Portraits/2855DialogueRev/Smirk",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Upset",    "Root/Images/Portraits/2855DialogueRev/Upset",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Smug",     "Root/Images/Portraits/2855DialogueRev/Smug",     true)
    DialogueActor_SetProperty("Add Emotion Rev", "Offended", "Root/Images/Portraits/2855DialogueRev/Offended", true)
    DialogueActor_SetProperty("Add Emotion Rev", "Angry",    "Root/Images/Portraits/2855DialogueRev/Angry",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Broken",   "Root/Images/Portraits/2855DialogueRev/Broken",   true)
    DialogueActor_SetProperty("Add Emotion Rev", "Cry",      "Root/Images/Portraits/2855DialogueRev/Cry",      true)
DL_PopActiveObject()

--Alternate costumes
fnCreateDialogueActor("2855Sundress", "Voice|55", "Root/Images/Portraits/2855SundressDialogue/",  true, sa2855Aliases,      sa2855Emotions)

--Right-Facing Case for 55's Sundress variant
DialogueActor_Push("2855Sundress")
    DialogueActor_SetProperty("Add Emotion Rev", "Neutral",  "Root/Images/Portraits/2855SundressDialogueRev/Neutral",  true)
    DialogueActor_SetProperty("Add Emotion Rev", "Down",     "Root/Images/Portraits/2855SundressDialogueRev/Down",     true)
    DialogueActor_SetProperty("Add Emotion Rev", "Blush",    "Root/Images/Portraits/2855SundressDialogueRev/Blush",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Smirk",    "Root/Images/Portraits/2855SundressDialogueRev/Smirk",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Upset",    "Root/Images/Portraits/2855SundressDialogueRev/Upset",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Smug",     "Root/Images/Portraits/2855SundressDialogueRev/Smug",     true)
    DialogueActor_SetProperty("Add Emotion Rev", "Offended", "Root/Images/Portraits/2855SundressDialogueRev/Offended", true)
    DialogueActor_SetProperty("Add Emotion Rev", "Angry",    "Root/Images/Portraits/2855SundressDialogueRev/Angry",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Broken",   "Root/Images/Portraits/2855SundressDialogueRev/Broken",   true)
    DialogueActor_SetProperty("Add Emotion Rev", "Cry",      "Root/Images/Portraits/2855SundressDialogueRev/Cry",      true)
DL_PopActiveObject()

--ChrisMaleVoice. Used to manually override the voice of Christine when in male form.
-- This never actually appears, it's just used for the voice.
DialogueActor_Create("ChrisMaleVoice")
	DialogueActor_SetProperty("Add Alias", "None")
DL_PopActiveObject()
WD_SetProperty("Register Voice", "ChrisMaleVoice", "Voice|Chris")

--2856. 2855's identical twin sister. Specially created (for now) because she uses non-standard sprites. Will get emotion sets later.
DialogueActor_Create("2856")
	DialogueActor_SetProperty("Add Alias", "2856")
	DialogueActor_SetProperty("Add Alias", "56")
	DialogueActor_SetProperty("Add Alias", "2855")
	DialogueActor_SetProperty("Add Alias", "55")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/2856Dialogue/Neutral", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "2856", "Voice|55")

--[ ======================================== Major Actors ======================================= ]
--Refers to actors who are important enough to usually warrant their own sprite, but not a full emotion set.
-- May also refer to actors who play a big role but only in one section of the game.

--Cassandra is created specially, since her Neutral emotion is her human form. She gets transformed a lot.
DialogueActor_Create("Cassandra")
	DialogueActor_SetProperty("Add Alias", "Lady")
	DialogueActor_SetProperty("Add Alias", "Cassandra")
	DialogueActor_SetProperty("Add Alias", "Fang")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/CassandraDialogue/Human",   true)
	DialogueActor_SetProperty("Add Emotion", "Werecat", "Root/Images/Portraits/CassandraDialogue/Werecat", true)
	DialogueActor_SetProperty("Add Emotion", "Golem",   "Root/Images/Portraits/CassandraDialogue/Golem",   true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Cassandra", "Voice|GenericF13")

--[Chapter 5]
fnCreateDialogueActor("TT-233",  "Voice|GenericF09", "Root/Images/Portraits/Enemies/SteamDroid",  true, {"Doctor Droid"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("Sammy",   "Voice|GenericF15", "Root/Images/Portraits/Sammy/",              true, saSammyAliases,     saSammyEmotions)

--[ ======================================== Minor Actors ======================================= ]
--NPCs or Enemy actors, usually play small roles many times. Some have unique dialogue sprites because their enemy field
-- variants are too big to fit on the dialogue screen.

--"Named" NPCs.
fnCreateDialogueActor("Coconut",  "Voice|GenericF03", "Root/Images/Portraits/NPCs/HumanNPCF0", true,  {"Coconut"}, {"NEUTRALISPATH"})

--"Unnamed" versions. Not characters, just stand-ins for a monster/enemy type.
fnCreateDialogueActor("Alraune",  "Voice|Alraune",    "Root/Images/Portraits/Enemies/Alraune", true, {"Alraune", "Dafoda", "Reika"},            {"NEUTRALISPATH"})

--[Chapter 5]
fnCreateDialogueActor("GolemLord",      "Voice|GenericF09", "Root/Images/Portraits/GolemLord/Neutral",      true,  {"Golem", "Golem Lord", "Lord", "692339", "649728", "Jackie", "Trucy"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemLordB",     "Voice|GenericF10", "Root/Images/Portraits/GolemLordB/Neutral",     true,  {"Golem", "Golem Lord", "Lord", "Primrose", "Priscilla"},         {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemLordC",     "Voice|GenericF09", "Root/Images/Portraits/GolemLordC/Neutral",     true,  {"Golem", "Golem Lord", "Lord"},                     {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyA",    "Voice|GenericF10", "Root/Images/Portraits/Enemies/GolemFancyA",    true,  {"Golem", "Golem Lord", "Lord", "Yellow"},           {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyB",    "Voice|GenericF11", "Root/Images/Portraits/Enemies/GolemFancyB",    true,  {"Golem", "Golem Lord", "Lord", "Black"},            {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyC",    "Voice|GenericF12", "Root/Images/Portraits/Enemies/GolemFancyC",    true,  {"Golem", "Golem Lord", "Lord", "Blue"},             {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyD",    "Voice|GenericF13", "Root/Images/Portraits/Enemies/GolemFancyD",    true,  {"Golem", "Golem Lord", "Lord", "Teal"},             {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyE",    "Voice|GenericF14", "Root/Images/Portraits/Enemies/GolemFancyE",    true,  {"Golem", "Golem Lord", "Lord", "White"},            {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyF",    "Voice|GenericF15", "Root/Images/Portraits/Enemies/GolemFancyF",    true,  {"Golem", "Golem Lord", "Lord", "Pink"},             {"NEUTRALISPATH"})
fnCreateDialogueActor("Isabel",         "Voice|Isabel",     "Root/Images/Portraits/Isabel/Neutral",         true,  {"Isabel"},                                          {"NEUTRALISPATH"})
fnCreateDialogueActor("Katarina",       "Voice|GenericF09", "Root/Images/Portraits/Katarina/Neutral",       true,  {"Lord", "Katarina"},                                {"NEUTRALISPATH"})
fnCreateDialogueActor("Amanda",         "Voice|GenericF16", "Root/Images/Portraits/NPCs/HumanNPCF1",        true,  {"Human", "745110"},                                 {"NEUTRALISPATH"})
fnCreateDialogueActor("Doll",           "Voice|GenericF17", "Root/Images/Portraits/Enemies/Doll",           true,  {"300910", "Command Unit", "1007", "Briget", "Doll", "11042"},{"NEUTRALISPATH"})
fnCreateDialogueActor("Kernel",         "Voice|GenericF08", "Root/Images/Portraits/Enemies/Doll",           true,  {"Doll", "Kernel"},                                  {"NEUTRALISPATH"})
fnCreateDialogueActor("DollInfluenced", "Voice|GenericF17", "Root/Images/Portraits/Enemies/DollInfluenced", true,  {"300910", "Command Unit"},                          {"NEUTRALISPATH"})
fnCreateDialogueActor("LatexDrone",     "Voice|GenericF08", "Root/Images/Portraits/Enemies/LatexDrone",     false, {"Drone", "Warden", "Mirabelle"},                    {"NEUTRALISPATH"})
fnCreateDialogueActor("LatexDroneB",    "Voice|GenericF08", "Root/Images/Portraits/Enemies/LatexDrone",     false, {"DroneB"},                                          {"NEUTRALISPATH"})
fnCreateDialogueActor("DarkmatterGirl", "Voice|Darkmatter", "Root/Images/Portraits/Enemies/DarkmatterGirl", false, {"Darkmatter", "Darkmatter Girl", "Zohwellb"},       {"NEUTRALISPATH"})
fnCreateDialogueActor("Steam Droid",    "Voice|GenericF14", "Root/Images/Portraits/Enemies/SteamDroid",     true,  {"AJ-99", "RI-15", "NC-88", "VN-19", "IN-12", "PP-81", "Gunner"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("Electrosprite",  "Voice|GenericF16", "Root/Images/Portraits/Enemies/Electrosprite",  false, {"Sprite", "Psue"},                                  {"NEUTRALISPATH"})
fnCreateDialogueActor("201890",         "Voice|Ghost",      "Root/Images/Portraits/Enemies/201890",         true,  {"201890", "Golem", "20"},                           {"NEUTRALISPATH"})
fnCreateDialogueActor("Maisie",         "Voice|Maisie",     "Root/Images/Portraits/Maisie/Neutral",         true,  {"Maisie", "Doctor"},                                {"NEUTRALISPATH"})
fnCreateDialogueActor("Narissa",        "Voice|GenericF14", "Root/Images/Portraits/Narissa/Neutral",        true,  {"Raiju", "Narissa"},                                {"NEUTRALISPATH"})
fnCreateDialogueActor("Vivify",         "Voice|Darkmatter", "Root/Images/Portraits/Vivify/Neutral",         false, {"Vivify"},                                          {"NEUTRALISPATH"})

--Golems. All of these have Pack/No Pack varieties.
DialogueActor_Create("Golem")
	DialogueActor_SetProperty("Add Alias", "Golem")
	DialogueActor_SetProperty("Add Alias", "Golem A")
	DialogueActor_SetProperty("Add Alias", "Chip")
	DialogueActor_SetProperty("Add Alias", "Slave")
	DialogueActor_SetProperty("Add Alias", "278101")
	DialogueActor_SetProperty("Add Alias", "565102")
	DialogueActor_SetProperty("Add Alias", "Mina")
	DialogueActor_SetProperty("Add Alias", "Linda")
	DialogueActor_SetProperty("Add Alias", "Theresa")
	DialogueActor_SetProperty("Add Alias", "Teal")
	DialogueActor_SetProperty("Add Alias", "Vicks")
	DialogueActor_SetProperty("Add Alias", "Lefty")
	DialogueActor_SetProperty("Add Alias", "Ravital")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Golem/Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Pack",    "Root/Images/Portraits/Golem/Pack",    true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Golem", "Voice|GenericF06")
DialogueActor_Create("GolemB")
	DialogueActor_SetProperty("Add Alias", "Golem B")
	DialogueActor_SetProperty("Add Alias", "Gold")
	DialogueActor_SetProperty("Add Alias", "Wedge")
	DialogueActor_SetProperty("Add Alias", "Righty")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Golem/Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Pack",    "Root/Images/Portraits/Golem/Pack",    true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "GolemB", "Voice|GenericF07")
DialogueActor_Create("GolemC")
	DialogueActor_SetProperty("Add Alias", "Golem C")
	DialogueActor_SetProperty("Add Alias", "Red")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Golem/Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Pack",    "Root/Images/Portraits/Golem/Pack",    true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "GolemC", "Voice|GenericF08")

--Rebels!
DialogueActor_Create("RebelGolem")
	DialogueActor_SetProperty("Add Alias", "Golem")
	DialogueActor_SetProperty("Add Alias", "Rebel")
	DialogueActor_SetProperty("Add Alias", "Jo")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/NPCs/GolemRebelScarf", true)
	DialogueActor_SetProperty("Add Emotion", "Head",    "Root/Images/Portraits/NPCs/GolemRebelHead",  true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "RebelGolem", "Voice|GenericF06")

--Administrator.
DialogueActor_Create("Administrator")
	DialogueActor_SetProperty("Add Alias", "Administrator")
	DialogueActor_SetProperty("Add Emotion", "Base",   "Root/Images/Portraits/Layered/Admin|Base",   true)
	DialogueActor_SetProperty("Add Emotion", "Layer1", "Root/Images/Portraits/Layered/Admin|Layer1", true)
	DialogueActor_SetProperty("Add Emotion", "Layer2", "Root/Images/Portraits/Layered/Admin|Layer2", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Administrator", "Voice|Administrator")

--Administrator as a doll in shadow. Used once.
DialogueActor_Create("AdministratorAsDoll")
	DialogueActor_SetProperty("Add Alias", "Kernel")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/NPCs/DollAsAdmin", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "AdministratorAsDoll", "Voice|Administrator")

--Fake Chris
DialogueActor_Create("FakeChris")
	DialogueActor_SetProperty("Add Alias", "Guy")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "FakeChris", "Voice|Chris")
