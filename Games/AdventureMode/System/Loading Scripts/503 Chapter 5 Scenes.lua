--[Chapter 5 Scenes]
--Scenes for chapter 5. This includes TF scenes and runestone flashes.

--[Cassandra's Transformations]
--She shows up a lot. Suspicious, isn't it?
SLF_Open(gsaDatafilePaths.sScnCassandraTF)
DL_AddPath("Root/Images/Scenes/Cassandra/")

--Neutral
DL_ExtractBitmap("Cassandra|Neutral",    "Root/Images/Scenes/Cassandra/Neutral")

--Golem
DL_ExtractBitmap("Cassandra|GolemTF0",   "Root/Images/Scenes/Cassandra/GolemTF0")
DL_ExtractBitmap("Cassandra|GolemTF1",   "Root/Images/Scenes/Cassandra/GolemTF1")
DL_ExtractBitmap("Cassandra|GolemTF2",   "Root/Images/Scenes/Cassandra/GolemTF2")
DL_ExtractBitmap("Cassandra|GolemTF3",   "Root/Images/Scenes/Cassandra/GolemTF3")

--[ ================================ Christine's Transformations ================================ ]
--Chris to Christine to Golem. Six parts.
SLF_Open(gsaDatafilePaths.sScnChristineTF)
DL_AddPath("Root/Images/Scenes/Christine/")
DL_ExtractBitmap("Christine|GolemTF0", "Root/Images/Scenes/Christine/GolemTF0")
DL_ExtractBitmap("Christine|GolemTF1", "Root/Images/Scenes/Christine/GolemTF1")
DL_ExtractBitmap("Christine|GolemTF2", "Root/Images/Scenes/Christine/GolemTF2")
DL_ExtractBitmap("Christine|GolemTF3", "Root/Images/Scenes/Christine/GolemTF3")
DL_ExtractBitmap("Christine|GolemTF4", "Root/Images/Scenes/Christine/GolemTF4")
DL_ExtractBitmap("Christine|GolemTF5", "Root/Images/Scenes/Christine/GolemTF5")
DL_ExtractBitmap("Christine|Male",     "Root/Images/Scenes/Christine/MaleNeutral")
DL_ExtractBitmap("Christine|Female",   "Root/Images/Scenes/Christine/FemaleNeutral")

--Christine to Doll TF.
DL_ExtractBitmap("Christine|DollTF0", "Root/Images/Scenes/Christine/DollTF0")
DL_ExtractBitmap("Christine|DollTF1", "Root/Images/Scenes/Christine/DollTF1")
DL_ExtractBitmap("Christine|DollTF2", "Root/Images/Scenes/Christine/DollTF2")
DL_ExtractBitmap("Christine|DollTF3", "Root/Images/Scenes/Christine/DollTF3")
DL_ExtractBitmap("Christine|DollTF4", "Root/Images/Scenes/Christine/DollTF4")
DL_ExtractBitmap("Christine|DollTF5", "Root/Images/Scenes/Christine/DollTF5")

--Latex Drone TF.
DL_ExtractBitmap("Christine|LatexTF0", "Root/Images/Scenes/Christine/LatexTF0")
DL_ExtractBitmap("Christine|LatexTF1", "Root/Images/Scenes/Christine/LatexTF1")
DL_ExtractBitmap("Christine|LatexTF2", "Root/Images/Scenes/Christine/LatexTF2")

--Raiju TF.
DL_ExtractBitmap("Christine|RaijuTF0", "Root/Images/Scenes/Christine/RaijuTF0")
DL_ExtractBitmap("Christine|RaijuTF1", "Root/Images/Scenes/Christine/RaijuTF1")

--Steam Droid TF.
DL_ExtractBitmap("Christine|SteamTF0", "Root/Images/Scenes/Christine/SteamTF0")
DL_ExtractBitmap("Christine|SteamTF1", "Root/Images/Scenes/Christine/SteamTF1")
DL_ExtractBitmap("Christine|SteamTF2", "Root/Images/Scenes/Christine/SteamTF2")
DL_ExtractBitmap("Christine|SteamTF3", "Root/Images/Scenes/Christine/SteamTF3")

--Christine to Eldritch Dreamer
DL_ExtractBitmap("Christine|DreamerTF0", "Root/Images/Scenes/Christine/DreamerTF0")
DL_ExtractBitmap("Christine|DreamerTF1", "Root/Images/Scenes/Christine/DreamerTF1")
DL_ExtractBitmap("Christine|DreamerTF2", "Root/Images/Scenes/Christine/DreamerTF2")
DL_ExtractBitmap("Christine|DreamerTF3", "Root/Images/Scenes/Christine/DreamerTF3")

--[ ==================================== Gala Dress Sequence ==================================== ]
SLF_Open(gsaDatafilePaths.sScnGalaDress)
DL_AddPath("Root/Images/Scenes/GalaDress/")
DL_ExtractBitmap("SophieDress|Seq0", "Root/Images/Scenes/GalaDress/Seq0")
DL_ExtractBitmap("SophieDress|Seq1", "Root/Images/Scenes/GalaDress/Seq1")
DL_ExtractBitmap("SophieDress|Seq2", "Root/Images/Scenes/GalaDress/Seq2")

--[ ================================== Chapter 5 Major Scenes =================================== ]
--Other major scenes that don't fit into another category.
SLF_Open(gsaDatafilePaths.sScnCh5Major)

--Window reflection scene.
DL_AddPath("Root/Images/Scenes/WindowReflection/")
DL_ExtractBitmap("Window|Alone",    "Root/Images/Scenes/WindowReflection/Alone")
DL_ExtractBitmap("Window|Together", "Root/Images/Scenes/WindowReflection/Together")

--Sophie in a leather outfit.
DL_AddPath("Root/Images/Scenes/SophieLeather/")
DL_ExtractBitmap("Major|SophieLeather", "Root/Images/Scenes/SophieLeather/SophieLeather")

--Finale.
DL_AddPath("Root/Images/Scenes/Ch5Finale/")
DL_ExtractBitmap("Major|Chapter5Finale", "Root/Images/Scenes/Ch5Finale/Explosion")

--[ ========================================= Clean Up ========================================== ]
SLF_Close()
