--[Chapter 5 Overlays]
--Setup.
local bUseLowResMode = OM_GetOption("LowResAdventureMode")
if(bUseLowResMode == false) then
    SLF_Open(gsDatafilesPath .. "OverheadMaps.slf")
else
    SLF_Open(gsDatafilesPath .. "OverheadMapsLoDef.slf")
end

--Regulus and Biolabs have unique maps.
DL_AddPath("Root/Images/AdvMaps/General/")
DL_ExtractBitmap("BiolabsMap",        "Root/Images/AdvMaps/General/BiolabsMap")
DL_ExtractBitmap("RegulusMap",        "Root/Images/AdvMaps/General/RegulusMap")
DL_ExtractBitmap("RegulusMapOverlay", "Root/Images/AdvMaps/General/RegulusMapOverlay")

--Cryolab Maps
DL_AddPath("Root/Images/AdvMaps/Cryogenics/")
DL_AddPath("Root/Images/AdvMaps/Equinox/")
DL_AddPath("Root/Images/AdvMaps/LRT/")
for i = 0, 6, 1 do
    DL_ExtractBitmap("CryolabMain"  .. i, "Root/Images/AdvMaps/Cryogenics/CryolabMain"  .. i)
    DL_ExtractBitmap("CryolabLower" .. i, "Root/Images/AdvMaps/Cryogenics/CryolabLower" .. i)
    DL_ExtractBitmap("Equinox"      .. i, "Root/Images/AdvMaps/Equinox/Equinox"         .. i)
    DL_ExtractBitmap("LRTEast"      .. i, "Root/Images/AdvMaps/LRT/LRTEast"             .. i)
    DL_ExtractBitmap("LRTWest"      .. i, "Root/Images/AdvMaps/LRT/LRTWest"             .. i)
end
    
--Background for Christine's mind.
DL_AddPath("Root/Images/AdvMaps/Backgrounds/")
DL_ExtractBitmap("NixNedarBacking", "Root/Images/AdvMaps/Backgrounds/NixNedar")

--CRT Corruption Sequence.
DL_AddPath("Root/Images/Overlays/CRTNoise/")
for i = 0, 9, 1 do
    DL_ExtractBitmap("Overlay|CRT" .. i, "Root/Images/Overlays/CRTNoise/CRT" .. i)
end

--Clean.
SLF_Close()