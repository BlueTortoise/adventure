--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] Christine, do you remember the circumstances under which we met?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Yes.[SOFTBLOCK] Happy memories...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] S-[SOFTBLOCK]sort of.[SOFTBLOCK] I remember you whacking me and then waking up...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] And then my conversion...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I was referring to the time I spoke to you on the intercom.[SOFTBLOCK] I consider that to be our first meeting.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] Oh yeah, almost died...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] You did save my life, didn't you?[SOFTBLOCK] You repressurized the airlock, and I would have died otherwise.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] I had no need for a corpse.[SOFTBLOCK] It was purely pragmatic.[SOFTBLOCK] Without your aid, I would have been unable to repair myself.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I was instead referring to your jittery, uncertain manner.[SOFTBLOCK] The thermal scopes suggested excessively high stress levels.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Oh, yes, how unreasonable.[SOFTBLOCK] Nothing stressful at all about wandering around dark halls, surrounded by bodies, and berserk robots trying to kill you.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] However could I feel such a way, with my frail little meat body?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] But you no longer express such fears.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I got reprogrammed.[SOFTBLOCK] Obviously petty organic things like fear were overwritten, right?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I retain my robotic memories and skills even when in organic form, though I can't access them as cleanly.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Your hypothesis is unlikely.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Well,[SOFTBLOCK] you're unflappable.[SOFTBLOCK] Maybe I'm learning from you?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Also unlikely, but serviceable.[SOFTBLOCK] Very well.[BLOCK][CLEAR]")