--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] I would like to talk about my sister, if that is all right with you.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Oh, yes![SOFTBLOCK] Do you think - [SOFTBLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Upset] You encountered her and mistook her for me, correct?[SOFTBLOCK] What level of similarity do we share?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Your faces and mannerisms are identical, as are your voices.[SOFTBLOCK] Pitch-perfect.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] The way you walk, the way you take charge, yeah, you're probably twins.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] As to her personality...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Upset] That will be all.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] Did I touch a nerve?[SOFTBLOCK] I'm sorry.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] What occurred between you two was not recorded, and therefore is absent from my records.[SOFTBLOCK] I was asking in order to fill the picture.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I have intercepted recorded communications from Unit 2856 dating back several years, though I was not aware of our familiality.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] She is brusque, agressive, rude, and commanding.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] So are you.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] ...[SOFTBLOCK] I do not like to think of myself that way...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Well, everyone else does.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]")
