--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] Do you agree with the Cause of Science?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Yes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Interesting.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] You don't?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] It does not concern me, as it is an abstract philosophy.[SOFTBLOCK] Machines do not need such mental crutches.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Uh huh.[SOFTBLOCK] So what are you going to replace it with?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] As I said, that does not concern me.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] So you've thought extensively about our coming revolution, but nothing about what comes after?[SOFTBLOCK] I have.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I think a republic would be the best way to serve the Cause.[SOFTBLOCK] We ought to distribute the gains of science amongst all workers, regardless of social status.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Why bother with such proselytization?[SOFTBLOCK] I have already committed to your purposes, you do not need to sell me on it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] But do you agree?[SOFTBLOCK] What do you think?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] That this conversation is pointless.[SOFTBLOCK] Machines do not need such mental crutches.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] So if this is a mental crutch, then what does one with no such handicap suppose?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smug] It does not matter.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] What happens will happen.[SOFTBLOCK] The forces we set in motion will be larger than us, and will shape the events.[SOFTBLOCK] We are merely parts of that larger force.[SOFTBLOCK] Machines.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] But you care when those events go against you.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I am doing what a machine will do to preserve itself, if self-preservation is in that machine's programming.[SOFTBLOCK] Evidently, it is.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Self-preservation is a philosophy.[SOFTBLOCK] A simple one, but it is.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] So, I think a republic of equals will do a fine job preserving you.[SOFTBLOCK] Don't you think?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] I have no need for such mental crutches.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Structuralism taken to its logical extreme...[BLOCK][CLEAR]")
    
