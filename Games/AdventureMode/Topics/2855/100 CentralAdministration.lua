--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] In your day to day life, what sorts of contact have you had with Central Administration?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[SOFTBLOCK] Not much.[SOFTBLOCK] I periodically get mails from them giving me orders, that's about it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Hey, you don't think they don't exist, do you?[SOFTBLOCK] Like, they're a collective hallucination?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] They exist.[SOFTBLOCK] I am a former member of their ranks.[SOFTBLOCK] Plus, I have accessed their unprotected communications and some video logs.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Laugh] It's called a joke, 55.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] Very well.[SOFTBLOCK] Why did the Lord Golem cross the road?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Uhh...[SOFTBLOCK] I don't know?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] To discuss a pending revolution with a maverick Command Unit.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] You failure to laugh indicates that you are also not interested in comedy.[SOFTBLOCK] Shall we proceed?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] (That, or the joke wasn't funny, 55...)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Judging from the number of units Sophie and I have done repairs on, I'd guess that the administrative units compose less than one percent of the population of Regulus City.[SOFTBLOCK] They probably have their own repair facilities at that rate.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Quite likely on both counts.[SOFTBLOCK] Because of the high number of Command Units in their facilities, they likely do maintenance work in special workshops.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] However, none of these workshops are on the official maps of Regulus City, nor are any conversion facilities or administrative centers.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] What about the Administration Building in Sector 7?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] A front, I believe.[SOFTBLOCK] Encrypted network traffic does route to that building, but only in a slightly higher density than others.[SOFTBLOCK] The building itself is not central administration.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Hmm, so we don't actually know where it is...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Indeed.[SOFTBLOCK] Solving that mystery will be one of our preparatory objectives.[BLOCK][CLEAR]")
