--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] I noted a contradiction in your positions.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Hm?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] I have little interest in your pedantic philosophy.[SOFTBLOCK] I am a logical machine.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] But I do have an interest in the contradiction.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Oh, of course.[SOFTBLOCK] I believe you.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Whether or not you believe me has no bearing.[SOFTBLOCK] It is true.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Now, the problem is your insistence that I not speak of partirhumans as if this is a superior state to the human one.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Yet I do recall you idolizing the transformation of humans into golems.[SOFTBLOCK] Robotizing the world.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] How can you desire this while insisting its implementation be suppressed?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Yes, 55, I do imagine that.[SOFTBLOCK] It makes me feel good to imagine the disparate humans of the world becoming robots.[SOFTBLOCK] That excites me.[SOFTBLOCK] Maybe a little bit more than it ought to...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] I wonder if Sophie thinks the same thing...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Focus.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Right, yes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] But...[SOFTBLOCK] People don't understand what they are excluding themselves from.[SOFTBLOCK] They would prefer to remain ignorant in a simple world where everything makes sense than be brought into a world where they truly understand what is happening, but are powerless to stop it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] That is naivete.[SOFTBLOCK] Innocence.[SOFTBLOCK] Part of growing up is losing that innocence.[SOFTBLOCK] I suppose that transformation into something better is a part of growing up.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Interesting.[SOFTBLOCK] Your justification is not one I have seen elsewhere.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] But it makes me feel ashamed to have thought of it...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] For reference, the administration's official justification is that the humans of Pandemonium are useless as they are, and can be made useful by conversion.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Internally, of course, they merely seek to augment the labour force.[SOFTBLOCK] The justification is secondary to the true goal.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I am, of course, not implying that your sexual fetishization is the true goal and the maturation of a species merely a justification.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] ...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smug] I merely wanted to know what you thought.[SOFTBLOCK] I am satisfied.[BLOCK][CLEAR]")















