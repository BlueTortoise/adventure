--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] Have you reconsidered your stance?[SOFTBLOCK] I would like to examine your Darkmatter form.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I thought you weren't interested in science and the like.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I am not.[SOFTBLOCK] Darkmatters, however, are very resistant to damage and can phase through solid material if they so wish.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] These are notable advantages in combat.[SOFTBLOCK] We should harness them.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Well, sorry, but you can't.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Even I don't understand how I work.[SOFTBLOCK] It's like there is another form of matter, not the usual hadrons and leptons.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I think the most accurate way to put it, is if we took space and folded it so much that it became something solid.[SOFTBLOCK] It's still a vacuum, but a hard vacuum.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] And intelligent.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Happy] Why, thank you for the compliment, 55![SOFTBLOCK] You're always a treat to talk to.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] That is not what I meant.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Take the compliment and run, I always say![BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Upset] You have never said that before.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] (It's like talking to a wet towel...)[BLOCK][CLEAR]")

