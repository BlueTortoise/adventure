--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] I was present for neither the discovery nor recruitment of these 'electrosprites', as you call them.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Are you certain they can be trusted?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Positive.[SOFTBLOCK] They share a mind, temporarily, with their conduit until they can take full form.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] They'll understand the positions of the golems they merge with, and I know they'll want to help them.[SOFTBLOCK] We're in the same boat, as it were.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Hm.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Let me guess, you don't trust them?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] You are naive.[SOFTBLOCK] These are an entirely alien, unknown life form.[SOFTBLOCK] We have no reason to believe they are on our side.[SOFTBLOCK] Anything and everything could be a misrepresentation, intentional or otherwise.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Fine, them.[SOFTBLOCK] Sic them on the administration.[SOFTBLOCK] Like attack dogs.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] That I may well do, when the time comes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] *sigh*[BLOCK][CLEAR]")

