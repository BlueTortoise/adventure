--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] Christine, what is faith?[SOFTBLOCK] Why do you have it in me?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Uh, is your dictionary software malfunctioning?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I know its definition, context, pronunciation, etymology.[SOFTBLOCK] But I do not know what it is.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] You remember being a human.[SOFTBLOCK] Humans have faith.[SOFTBLOCK] Don't they?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I think so.[SOFTBLOCK] Some pretend like they don't, but they do.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Faith is where you believe something even if it's not certain.[SOFTBLOCK] It's a part of ordinary life, we use it to get by.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] It's not just believing in some higher power, if you do.[SOFTBLOCK] Other people are very complex, and if you don't have faith in them, you can never understand or get along with them.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Another word for faith might be trust.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] It is listed as synonym.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Most uses of faith are in relation to something which does not reciprocate it.[SOFTBLOCK] A God is the one you might think of first, but a government, an organization, a leader.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] A leader does not need to know of each of her followers, but the followers still have faith in her.[SOFTBLOCK] Faith that she will do what they want her to.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] And if I betray that trust?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Then I will still believe in you.[SOFTBLOCK] If you walk the wrong path, you are one step away from walking the right one.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Hmm.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] But that doesn't mean my faith will let you escape consequences.[SOFTBLOCK] If someone were to betray my trust, and I had to...[SOFTBLOCK] retire them...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Then in their final moments in this world I would ask them to apologize.[SOFTBLOCK] It would still matter.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] And if they did not?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Then...[SOFTBLOCK] my faith in them was misplaced.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I made a mistake.[SOFTBLOCK] But there are many who are worthy of faith.[SOFTBLOCK] I'll give them a chance to prove they are worthy of it, if they need one.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] If you don't have faith, then you're on a cold, lonely walk between these dark poles.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] ...[BLOCK][CLEAR]")
