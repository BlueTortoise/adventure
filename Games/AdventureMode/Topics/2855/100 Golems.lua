--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] What is it like being a golem?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Hm?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I am not one of your models.[SOFTBLOCK] I am familiar with the technical differences, but - [SOFTBLOCK][CLEAR]")
if(sChristineForm == "Golem") then
    WD_SetProperty("Append", "Christine:[E|Happy] It's the best thing![SOFTBLOCK] I love every second of it![BLOCK][CLEAR]")
else
    WD_SetProperty("Append", "Christine:[E|Happy] It's the best thing![SOFTBLOCK] I'll be changing back first chance I get![BLOCK][CLEAR]")
end
WD_SetProperty("Append", "55:[E|Neutral] Interesting.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Doesn't everyone feel this way?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] More or less.[SOFTBLOCK] The testimonials of recently converted humans share your sentiment.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I feel so much more aware, more energetic, stronger, smarter...[SOFTBLOCK] My legs don't hurt after walking or standing for a long time, I'm not as bothered by cold or heat...[SOFTBLOCK] and I'm...[SOFTBLOCK][E|Blush] a woman...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Indeed.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] So why is it interesting if everyone feels this way?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Because, in my analysis of the other partirhuman species, I see the sentiment largely echoed elsewhere.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] There are Alraunes and Raijus in the habitation domes.[SOFTBLOCK] Humans assigned to those forms report a euphoria after their transformation, similar to the one you and other Units report.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] It makes me think that, perhaps, there is something fundamental about the non-human nature of existence.[SOFTBLOCK] Are humans a fallen state, and non-humans an elevated one?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Don't talk like that.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] They're different, but not inferior.[SOFTBLOCK] Don't talk like you know the correct way to live one's life.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I was not advancing that proposition.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Don't.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Very well.[SOFTBLOCK] The vehemence behind your position is noted.[BLOCK][CLEAR]")

--Topics
WD_SetProperty("Unlock Topic", "Contradictions", 1)
