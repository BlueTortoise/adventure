--[Goodbye]
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
WD_SetProperty("Append", "55: Let us resume our mission, Unit 771852.")

--Restore the music.
AL_SetProperty("Music", glLevelMusic)