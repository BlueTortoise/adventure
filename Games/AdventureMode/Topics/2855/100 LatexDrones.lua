--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iHasLatexForm = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

--Has the form:
if(iHasLatexForm == 1.0) then
    WD_SetProperty("Append", "55:[E|Neutral] As a Drone Unit, what are your physical weaknesses?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Come again?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Drone Units are common in the security services, as they rarely ask difficult questions.[SOFTBLOCK] Optimizing our combat routines against them is a worthwhile endeavour.[BLOCK][CLEAR]")
    if(sChristineForm == "LatexDrone") then
        WD_SetProperty("Append", "Christine:[E|Neutral] Well, when you put it that way...[BLOCK][CLEAR]")
        WD_SetProperty("Append", "Christine:[E|Blush] There's a weird undertone in my base programming about being punctured.[SOFTBLOCK] The latex synth-skin is meant to remain intact at all times.[BLOCK][CLEAR]")
    else
        WD_SetProperty("Append", "Christine:[E|Blush] Going from memory -[SOFTBLOCK] There's a weird undertone in my base programming about being punctured.[SOFTBLOCK] The latex synth-skin is meant to remain intact at all times.[BLOCK][CLEAR]")
    end
    WD_SetProperty("Append", "Christine:[E|Blush] Violations of the chassis are to be repaired immediately, no exceptions.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Blush] Good units are held tight, bound up, thick and firm, squeezing, good units...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Blush] I'M A GOOD UNIT.[SOFTBLOCK] GOOD UNITS ARE HELD TIGHT, BOUND UP, THICK AND FIRM - [SOFTBLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Blush] W-[SOFTBLOCK]woah![SOFTBLOCK] Got a little carried away there![SOFTBLOCK] J-[SOFTBLOCK]just ignore what I said![BLOCK][CLEAR]")
    WD_SetProperty("Append", "55: Hm.[SOFTBLOCK] I am not privy to the conversion coding methods, they are not stored on the network.[SOFTBLOCK] Perhaps I could hack the conversion tube directly...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Offended] Don't touch her![BLOCK][CLEAR]")
    WD_SetProperty("Append", "55: Her?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Sad] I -[SOFTBLOCK] don't know where that came from, honestly.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Sad] But please don't violate the conversion tube.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Are you saying this because of the latent programming?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Offended] I'm saying it because you shouldn't hack the conversion tube, so don't.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Offended] Move on.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Affirmative.[SOFTBLOCK] Changing topic.[BLOCK][CLEAR]")

--Does not have the form:
else
    WD_SetProperty("Append", "55:[E|Neutral] Christine, please provide your detailed assessment of the Drone Units we have encountered.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Why are you asking me?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] I have noticed your ocular receivers following their movements very closely.[SOFTBLOCK] Particularly those of their legs.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] I -[SOFTBLOCK] I like their shoes.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] They look pretty and I wonder what they'd look like on me.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Their appearance should not be a consideration.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] I like your boots, too, but they don't look like they'd fit me.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Upset] Please do not make this a discussion of footwear.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Why not?[SOFTBLOCK] Are you saying you don't like shoes?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] I like shoes![SOFTBLOCK] Okay, they're kind of bolted to your legs as a Lord Golem, but I'm sure we can replace them with a wrench or a tire-iron.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] ... [SOFTBLOCK]or a welder...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Angry] Please stop...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Do all the Command Units get boots like yours, or do you just get them from the same place?[SOFTBLOCK] Do you think it'd be suspicious if I requisitioned some for me?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Offended] Christine, refocus your attentions on the present.[SOFTBLOCK] The Drone Units.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Yes?[SOFTBLOCK] What about them?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] I like their masks, too.[SOFTBLOCK] I've had to work on them in the maintenance bay, you know.[SOFTBLOCK] That's not really a mask, their optical receptors are behind those lenses.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Happy] But they look cute![SOFTBLOCK] I'm sure you could find some way to style them...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, recalibrate topics matrix and reinitialize conversation.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Okay, fine, we'll talk about something else.[BLOCK][CLEAR]")
end

