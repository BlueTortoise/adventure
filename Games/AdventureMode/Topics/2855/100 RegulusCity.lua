--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] What is ordinary life like in Regulus City?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Are you asking me for some specific reason?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I do not interact socially with its inhabitants.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Oh, I see.[SOFTBLOCK] I suppose you don't.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Well, I get out of my defragmentation pod, visit with anyone who's in my elevator, head in to the maintenance bay...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Are the units there unnerved by your presence?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] They were at first, but I think I've grown on them.[SOFTBLOCK] Apparently I'm the only Lord Golem who smiles.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Unit 445823 always waves at me as I pass the work terminals, and I like to visit with Unit 702399 when she's watching the Raijus.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] You apparently did not understand my query.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Is the sentiment of the Slave Units at the breaking point?[SOFTBLOCK] How ready are they for a revolt?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] N-[SOFTBLOCK]not ready at all![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] You don't understand.[SOFTBLOCK] Everyday life is something we tolerate.[SOFTBLOCK] We're very tough, we can endure.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] We.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Yes, we![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] It's the petty leadership, the uncaring administration, and the little cruelties it inflicts on a daily basis.[SOFTBLOCK] Removing someone's breaks, taking away their personal items, ordering extra pointless work...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] We put up with it because the threat of violence is big and scary.[SOFTBLOCK] We have something, but in war we might lose everything.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Upset] That is not encouraging.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Unfortunately, we will have to thrust war upon them.[SOFTBLOCK] When it is apparent and presented, I know they'll make the right choice.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] The limits of a people's endurance will be the limits of their oppression.[SOFTBLOCK] We golems are a very durable lot.[SOFTBLOCK] You figure out the rest.[BLOCK][CLEAR]")
