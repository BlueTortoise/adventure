--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] Tell me, Christine. Why are you a class traitor?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] That's the second time you've used that term.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] Hm, you are correct.[SOFTBLOCK] Perhaps I have an affinity for it in my harmonics.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Maybe it's an official term among Command Units.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] That is quite possible.[SOFTBLOCK] I will review the communications I have intercepted between them.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Considering your memory wipe, does that mean it's a basic part of your programming?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Hmm...[SOFTBLOCK] Disturbing.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Now I must pause to wonder if my programming is affecting my behavior in other, unidentified ways.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Maybe that's why I'm a class traitor.[SOFTBLOCK] I wasn't programmed normally.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Honestly, I don't think the conversion chamber was quite sure what to do with me.[SOFTBLOCK] It really made a mess of my memories.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Elaborate.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I remember going to school and graduating with an education degree and an English degree.[SOFTBLOCK] As a Lord Golem.[SOFTBLOCK] [E|Laugh]For a language that doesn't exist on Regulus.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] True, but that would not cause you to become a class traitor.[SOFTBLOCK] Such paradoxes are frequent in reprogramming cases.[SOFTBLOCK] Golems usually ignore them.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] ...[SOFTBLOCK] I guess that might mean it's just how I am.[SOFTBLOCK] Sooner or later I was going to betray the Lord Golems.[SOFTBLOCK] It's part of my personality.[SOFTBLOCK] Sophie seems to think so.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I like to think that, no matter what happens, I'd stand up for what's right.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] That is...[SOFTBLOCK] discouraging...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] How so?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] I wonder if how I am now is how I was before.[SOFTBLOCK] If so, and I am not like you, will I revert to that behavior eventually?[SOFTBLOCK] How much of my behavior is due to my cognition?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] I don't know, that's a big question.[SOFTBLOCK][E|Smirk] But I do know you'll be fine. I have faith in you.[BLOCK][CLEAR]")
    
--Topics
WD_SetProperty("Unlock Topic", "Faith", 1)
