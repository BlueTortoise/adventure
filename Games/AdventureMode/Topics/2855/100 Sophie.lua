--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iSXMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] How is your relationship with Sophie progressing?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Progressing?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] The standard pattern involves a period of infatuation, establishment of norms, mutualization of living spaces, and eventually social integration.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smug] It's very well documented.[SOFTBLOCK] Deviations are generally in the negative direction and result in the end of the relationship.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] You don't understand us at all, do you?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] If my assessment was incorrect, please demonstrate the flaws.[SOFTBLOCK] I cannot improve my model if I do not know how it is erroneous.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] It's not that you got anything wrong, it's just that -[SOFTBLOCK] you missed the magic of it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] The electrical signals in the brains of a golem, while encoded at various harmonics and thus difficult to parse, are by no means magical.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Not what I meant...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smug] Then demonstrate the flaws of the model.[BLOCK][CLEAR]")

if(iSXMet55 == 0.0) then
    WD_SetProperty("Append", "Christine:[E|Neutral] I don't think I can.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] You'd have to be in love to understand.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] I am not.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Then why did you ask?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Your.[SOFTBLOCK] Your.[SOFTBLOCK] Emotional well being depends on the presence of Unit 499323.[SOFTBLOCK] I wish to keep you stable.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] And that stutter there wasn't you trying to find an excuse to ask how I was doing?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Offended] A data retrieval error.[SOFTBLOCK] They happen.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Suuuure.[BLOCK][CLEAR]")
else
    WD_SetProperty("Append", "Christine:[E|Neutral] I don't think I can.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] You'd have to be in love to understand.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] I am not.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] And what about SX-399?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Offended] What about her?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] *PDU, begin monitoring 55's coginitive cycle activity.*[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] She is -[SOFTBLOCK] an interesting conversationalist.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] What about her looks?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Smirk] Red.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] What sorts of jokes does she like to tell?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Smirk] 45pct knock-knock jokes,[SOFTBLOCK] 25pct puns,[SOFTBLOCK] 15pct extended stories with punch-lines - [SOFTBLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Uh huh.[SOFTBLOCK] Okay.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] PDU?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "PDU:[E|Neutral] Unit 2855's cognitive cycles increased by 1300pct above normal operations when discussing SX-399.[SOFTBLOCK] The largest increase was during the categorization of her jokes.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Offended] That -[SOFTBLOCK] that should be obvious.[SOFTBLOCK] Recalling, sorting, and storing memories requires a great deal of processor cycles![BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Thank you, PDU, that will be all.[BLOCK][CLEAR]")
end
