--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855")

WD_SetProperty("Append", "55:[E|Neutral] Please provide your technical assessment of the capabilities of Steam Droids.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Happy] I love their color choices.[SOFTBLOCK] You'd think the green and bronze wouldn't work, but they do![BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Upset] Technical.[SOFTBLOCK] Assessment.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] You are a repair unit, act like one.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Oh, of course.[SOFTBLOCK] Silly me.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Their bodies and equipment are in poor shape.[SOFTBLOCK] I estimate their performance rates are at thirty percent or lower than optimal, even in ideal conditions.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Poor labourers, poor soldiers, and probably inadequate for research purposes unless one is researching patchwork solutions with non-standard materials.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Technical advisory is a major increase in repair personnel and increased allocation to materials budgeting.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] But we both know that's not going to happen.[SOFTBLOCK] Why do you ask?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] You are effectively confirming my evaluation that the Steam Droids would serve as expendable shock troops.[SOFTBLOCK] Why did you resist it when I made it?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] My technical assessment is the assessment of the machine, not the person within it.[SOFTBLOCK] Technically I'd expect they'd have fallen apart years ago, but their indomitable spirit or a lot of creativity are keeping them running.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] So, 55, would you prefer to have a courageous and clever ally?[SOFTBLOCK] Or one whose equipment is up to spec but untested?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smug] The latter, of course.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] of course you would.[BLOCK][CLEAR]")

