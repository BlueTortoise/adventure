--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, I need to ask you about something personal.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Your sister?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] How did you know?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] The list of things that are 'personal' with Unit 2855 is pretty short, and a small number cause her not to smirk.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] That is good.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] You appear to understand how to read my emotions very well.[SOFTBLOCK] Can we use this knowledge against my identical twin?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] No, unfortunately.[SOFTBLOCK] Believe me, I want to.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Despite your being twins, you actually don't have as much in common as you might think.[SOFTBLOCK] She has several lifetimes of experience being 2856, you have little of being 2855.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Because I have known you for almost your entire life, I have seen how you have changed with it.[SOFTBLOCK] I know how you will progress, because I have known people like you.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] You did not mention any of Earth's robotic war criminals in the past.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Laugh] Ha ha ha![SOFTBLOCK] Not what I meant![BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] (She laughed at that...)[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] Thank you for laughing at my joke.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] No, 55, what I mean is the little girls I used to teach.[SOFTBLOCK] Everyone is different, but people are usually products of their environments, and there are certain paths they tend to go down.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I've seen yours, and I've seen hers.[SOFTBLOCK] She is...[SOFTBLOCK] a control freak.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Always in charge, always gets her way, abuses those who do anything she doesn't approve of.[SOFTBLOCK] Has to have control over the situation.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] So your obstinacy when speaking with her?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] You can really set off a control freak by just not listening to them.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] It's probably not productive, but at the bare minimum it may anger her and cause her to make mistakes we can exploit.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] So when dealing with her, you are being obtuse [SOFTBLOCK]*deliberately*.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I will take that into consideration for our future conversations.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Angry] Unit 2855, what are you implying?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smug] Nothing at all.[SOFTBLOCK] Let us discuss another topic.[BLOCK][CLEAR]")
