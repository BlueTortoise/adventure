--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] Christine, I would like you to transform into an alraune at a later date.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I need to be transformed into one first, 55.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] That is what I meant.[SOFTBLOCK] I apologize if I was unclear.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Having access to new forms can be useful for tactical reasons.[SOFTBLOCK] You should acquire as many as you can.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I agree![SOFTBLOCK] I'd be like a plant repair unit![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] My job in Maintenance and Repair would be so much easier if I could just ask a broken radio-transcoder which part was burnt out.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Upset] Tactical reasons, Christine.[SOFTBLOCK] Alraunes can regenerate and speak to plants for intelligence information on enemy movements.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] True, but I don't think they'd transform me if we asked.[SOFTBLOCK] They seem to be rather choosy about new alraunes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Maybe they wouldn't appreciate having their form be considered as a tactical decision rather than a philosophical one.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] You mean they would only transform those who agree with them?[SOFTBLOCK] That is uncommon behavior among partirhumans.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] You say that, but is it really?[SOFTBLOCK] Reports indicate a great deal of happiness among those who are transformed.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Maybe it is causal the other way?[SOFTBLOCK] Maybe the partirhuman magic seeks out those who agree with it already, and transforms them?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] An interesting hypothesis.[SOFTBLOCK] I admit I am not familiar with the research literature, so I cannot provide an argument for or against.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Nevertheless, we'll have to worry about the here and now.[SOFTBLOCK] If the chance comes up, I'd like to be a plant, but let's not force the issue.[BLOCK][CLEAR]")