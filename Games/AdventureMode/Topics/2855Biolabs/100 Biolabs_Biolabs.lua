--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] Christine, what is your opinion on the biolabs themselves?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Reminds me of jolly old England.[SOFTBLOCK] If there were fewer monsters trying to kill us, it'd be like a September day in Whitley Park.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Does this remind you of Earth?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Green fields, lots of trees, they somehow have a blue sky in the biolabs despite it being a dome...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] It is part of the simulation of natural sunlight.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Laugh] Well then that makes it nothing like home![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] ...[SOFTBLOCK] The joke being how much it rains in England.[SOFTBLOCK] It's an island so we get a lot of rainy days.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] Oh...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] What is it?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] I just said 'home', didn't I?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] Sector 96 is my home, that's where I live.[SOFTBLOCK] That's where I want to be, fixing things and being with Sophie.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] But I guess Earth is still my home, too.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Does it upset you to have multiple homes?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] When you put it that way, no.[SOFTBLOCK] I suppose two is better than one, right?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] But will I ever be able to visit?[SOFTBLOCK] Will I have anything to visit about?[SOFTBLOCK] I'm very different now, would they accept me?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I have no answers to these questions.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Sorry, 55.[SOFTBLOCK] You're probably not the best person to ask.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] In a way, the biolabs are like England, so I like them, but mostly they aren't because of many little differences.[SOFTBLOCK] That answers your question.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] It does, thank you.[BLOCK][CLEAR]")