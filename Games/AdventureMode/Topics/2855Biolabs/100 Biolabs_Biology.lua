--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] I would like your opinion on biology and the study of organics.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] The robot in me says it's a waste of time.[SOFTBLOCK] We've already transcended organic material![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] But I know the natural world has so many wonders in store for us that it'd be arrogant to assume we know the best way to do things just because we are machines.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Interesting.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Not that I want to study it myself.[SOFTBLOCK] I was rubbish at biology.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I just think all topics are equally valid.[SOFTBLOCK] There is no practical argument for knowledge of one type over another.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Should not our resources be focused on topics that will provide engineering advantages to our society?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Laugh] Ha ha![SOFTBLOCK] Of course not, my dear![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] You know what it means to focus on a topic?[SOFTBLOCK] It just means the researchers will find ways to make their grant proposals line up with whatever you claim to be focusing on.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Whatever it is the researcher is passionate about researching is what they will research.[SOFTBLOCK] Science is, in many ways, an art with specific rules.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] But our society has specific problems it must research answers to.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Then there is no need to guide your scientists![SOFTBLOCK] That's what they'll want to research.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Scientists love to solve problems, it's why they got into the field in the first place.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Your perspective is that of a military commander, your motivation is simple, direct, and efficient.[SOFTBLOCK] You believe everyone else is motivated in the same way.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Well, not everyone is.[SOFTBLOCK] Some units find and solve problems because that's what they like to do.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]")