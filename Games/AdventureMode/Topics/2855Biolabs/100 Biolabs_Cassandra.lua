--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

--Cassandra became a golem:
if(iResolvedCassandraQuest == 1.0 or iResolvedCassandraQuest == 3.0) then
    WD_SetProperty("Append", "Christine:[E|Smirk] 55, I've been meaning to ask.[SOFTBLOCK] Have you heard from Cassandra at all?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Unit 771853, Logistics, Sector 15?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] If you want to use her primary designation that's fine with me.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Cassandra has provided undercover support for us.[SOFTBLOCK] Sector 15 had many materials we could use that were not correctly documented.[SOFTBLOCK] As part of her duties, she disposed of them by giving them to us.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] While she is sympathetic, however, she does not wish to participate directly.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Oh?[SOFTBLOCK] Why not?[SOFTBLOCK] She seems like she's pretty tough.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] She is, but she declined nonetheless.[SOFTBLOCK] She has still provided us with useful equipment and recruiting information, so I consider her a useful asset for the revolution.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Splendid![SOFTBLOCK] I hope she comes around![BLOCK][CLEAR]")
    
--Cassandra is a human:
elseif(iResolvedCassandraQuest == 2.0 or iResolvedCassandraQuest == 4.0) then
    WD_SetProperty("Append", "Christine:[E|Smirk] 55, I've been meaning to ask.[SOFTBLOCK] Have you heard from Cassandra at all?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] She made contact with me a few days ago.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] She had taken up residence with the steam droids of Steamston, and wanted to speak with me personally.[SOFTBLOCK] However, I was unable to do so, so we communicated via mail.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] She is 'fine', as you would say.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Great![SOFTBLOCK] We've been so busy, I didn't have time to check up on her.[SOFTBLOCK] Did she fit in with the steam droids well?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Her activities have generated some goodwill with the residents of Steamston.[SOFTBLOCK] She is apparently good with a stubgun, and assists during their scavenging operations.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] She was not interested in joining our movement, however.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] It'd be pretty dangerous for an organic...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] I am sure you will have time to reconnect with her in the future.[BLOCK][CLEAR]")
end
