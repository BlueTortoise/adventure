--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iHasDarkmatterForm = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDarkmatterForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

--Christine has darkmatter form:
if(iHasDarkmatterForm == 1.0) then
    WD_SetProperty("Append", "55:[E|Neutral] 771852, diplomatic assessment of the darkmatters.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[SOFTBLOCK] The darkmatters, I believe, are mistaken about me and the future of Regulus.[SOFTBLOCK] They've seen what I've seen, and they are wrong.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] I've been one, I [SOFTBLOCK]*am*[SOFTBLOCK] one.[SOFTBLOCK] I know how they view the universe and time.[SOFTBLOCK] It is not as unflinching as they think, because the interface point of reality was mere moments ago.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Please elaborate on this interface point.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Time flows in fixed, circular patterns.[SOFTBLOCK] It doesn't change, but it does repeat.[SOFTBLOCK] The darkmatters are aware of this, as well as Vivify, I think.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] They are aware of it but cannot change it, so they enjoy the ride.[SOFTBLOCK] But they only exist in one particular instance, not all simultaneously.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] But something odd happened recently, and time is now malleable.[SOFTBLOCK] I think my runestone is related to that.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] How?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Sad] That...[SOFTBLOCK] I don't know.[SOFTBLOCK] I don't know why anything is different this time, but I do know it is.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Sad] If I can communicate that to them, convince them, maybe I can at least get them to stop attacking us.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] If they do get in our way, don't hurt them anymore than is necessary.[SOFTBLOCK] I know we can find a solution.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] And why do you believe this?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Because nobody on Regulus seems to have any idea how to negotiate, so you're all really bad at it.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] The Administration simply takes what they want from the workers.[SOFTBLOCK] The darkmatters understand one another and are generous.[SOFTBLOCK] Nobody knows how to not get what they want.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] If we just are firm but understanding, we can win them over.[SOFTBLOCK] I know it.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Very well.[BLOCK][CLEAR]")

--Christine does not have darkmatter form:
else
    WD_SetProperty("Append", "55:[E|Neutral] 771852, diplomatic assessment of the darkmatters.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[SOFTBLOCK] The darkmatters, I believe, are mistaken about me and the future of Regulus.[SOFTBLOCK] They've seen what I've seen, and they are wrong.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] I do not have the understanding you do, so I will defer to your judgement.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] However, the darkmatters are dangerous enemies and could strike at any moment.[SOFTBLOCK] We must at least return them to neutrality.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] I think if we can stop Vivify, we can prove to them that they were wrong.[SOFTBLOCK] If we show them her future isn't set...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Sad] Wow, this is getting a little complicated...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] If they do get in our way, don't hurt them anymore than is necessary.[SOFTBLOCK] I know we can find a solution.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] And why do you believe this?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Because nobody on Regulus seems to have any idea how to negotiate, so you're all really bad at it.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] The Administration simply takes what they want from the workers.[SOFTBLOCK] The darkmatters understand one another and are generous.[SOFTBLOCK] Nobody knows how to not get what they want.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] If we just are firm but understanding, we can win them over.[SOFTBLOCK] I know it.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Very well.[BLOCK][CLEAR]")
end