--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] 771852, you have stated that you had video games on Earth.[SOFTBLOCK] How do they compare to the games on Regulus?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] The production values are actually higher in Earth games even if the technology is nowhere near as advanced.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] But the heart just isn't in them as much.[SOFTBLOCK] All of the best video games on Earth were made by people who really cared and had the talent and resources to bring their vision to life.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Independent developers made many of the best games before I left, but most people who played games only played the trash triple-A games because they were popular.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Are these your opinions, or those of someone else?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Obviously, these are my opinions.[SOFTBLOCK] Who elses' could they be?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Anyway, here on Regulus, all of the video games are passion projects made by a unit with some spare time and a dream.[SOFTBLOCK] Our technology is just good enough to make that possible.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] We may not have the budget to get a famous unit in the game, or have big rendered cutscenes, but you care about what happens because they took the time to make you care.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] So I guess I would say that games on Regulus are similar to independent games on Earth if you vastly increased their budgets.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] It's not that I hold it against the people of Earth for playing games devoid of artistic integrity.[SOFTBLOCK] Do what you like to do, not everything has to be a masterpiece.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Just expand your horizons a bit, you will enjoy the things that are out there that aren't brown and grey military shooters.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]")