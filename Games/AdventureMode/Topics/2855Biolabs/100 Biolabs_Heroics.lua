--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] Christine, I would like to criticize your tactics for a moment.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] You don't need my permission to criticize me, 55.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I do.[SOFTBLOCK] In the past, my criticisms have upset others.[SOFTBLOCK] I have elected to preface them with a permission request.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] Well that's an improvement, but why do your criticisms upset people?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] You have a tendency towards dangerous heroics.[SOFTBLOCK] You may compromise others with your actions.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Question answered.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Why do you behave this way when you know it is dangerous, and you know the statistics on heroic actions?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] No doubt you are aware they tend to result in the retirement of the unit enacting them.[SOFTBLOCK] I have sent you the data multiple times.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Don't worry, I read it.[SOFTBLOCK] My PDU notifies me every time.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] 55, you need to understand that I do what I think is right, and what I think is right is shouldering burdens on myself rather than others.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] I guess I get it from my father.[SOFTBLOCK] He was a horrible man, but he never once asked something of his friends he wouldn't do himself.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] This has triggered a negative emotional response.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] I'm a lot more like him than I thought.[SOFTBLOCK] I always tried consciously to take after my mother, but I can't escape that he is half of me.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] He possessed neutral virtues.[SOFTBLOCK] He was brave, but if his cause is evil, his bravery is virtuous but not good.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I'm different because of my cause, not my bravery.[SOFTBLOCK] I guess I owe him that much, too.[SOFTBLOCK] Would I be who I am if I didn't reject his ideology?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Your flair for theatrics is genetic, then?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, and you know what?[SOFTBLOCK] I think your calm, rational demeanor is genetic, too.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Your sister has the rational part at any rate.[SOFTBLOCK] Calm, less-so.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] The idea of neutral virtues is illuminating.[BLOCK][CLEAR]")