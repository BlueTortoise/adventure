--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "Christine:[E|Smirk] Considering we've been through the Arcane University now, 55, are you thinking of maybe taking a few classes on magic?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] What are you referring to?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Laugh] Oh cheer up, 55![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] You're a believer in the capability of science.[SOFTBLOCK] Militarily, what about magic?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I use a bit of it in my attacks, I think, but I'm just an amateur.[SOFTBLOCK] You, however, don't use it at all.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I do not have any magical talent that I am aware of.[SOFTBLOCK] I could learn, but magic cannot be programmed the way combat routines can.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] My understanding is that magic is a personal relationship, not a material one.[SOFTBLOCK] Everyone who interacts with magic does so according to their own terms.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] So you have researched it?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] For strategic reasons, yes.[SOFTBLOCK] Some of our mavericks will likely have undeveloped magical abilities that have been suppressed by a life of hard labour.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Units who show exemplary arcane capability are usually reassigned to research in the university.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Can we use it to help win the civil war?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Unlikely.[SOFTBLOCK] Magic requires years of training as well as natural aptitude.[SOFTBLOCK] It'd take five years to teach a golem to hurl a fireball, or fifteen seconds to teach them how to aim a pulse rifle.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Not to mention the fact that combat subroutines can teach a golem to reload and maintain the rifle in nanoseconds.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Oh, too bad.[SOFTBLOCK] But if we find someone who has amazing magical talent?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] By all means, it may be easier for some to help with their magic rather than their weapons.[SOFTBLOCK] At the same time, someone who shows aptitude for repairs should be a repair unit.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Magic is just another skill.[SOFTBLOCK] It has acquired its legendary reputation because of its greatest practitioners, not its average ones.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Okay, we'll keep an eye out then![BLOCK][CLEAR]")