--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] 771852, you have knowledge of such matters.[SOFTBLOCK] Do you know where the creatures in the biolabs are coming from?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] If you're asking about their true origins, I really don't know.[SOFTBLOCK] They were not here, and now they are.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Interesting, but I meant their infiltration vector.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] They're followers of 20, that unit who organized the gala.[SOFTBLOCK] I expect they entered through the university basement and spread out through the domes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] That means their retreat vector would be limited to that, assuming the organic growth has not compromised the biolabs yet.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] It hasn't.[SOFTBLOCK] Yet.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] That is good news for us.[SOFTBLOCK] Final question.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Can we use them against the administration?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] No.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Please do not become emotional.[SOFTBLOCK] I am asking this question for due diligence purposes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Sorry, it's just -[SOFTBLOCK] that's something that the Administration would do.[SOFTBLOCK] After all, that's one of their reasons for researching Project Vivify.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I am aware of that.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Good.[SOFTBLOCK] We have to be better than them.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] They will not hesitate to weaponize them against the rebels if they are able to.[SOFTBLOCK] Are you prepared to give up the rebellion if we cannot win without their help?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] I think we can win without resorting to such crimes, but...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] 55, you're the tactician.[SOFTBLOCK] I give you permission to do it on the condition that you prove it is tactically vital to do so.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] And that you, and I, are prepared to be punished for those crimes should we win.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Freedom trumps moral concerns?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Yes.[SOFTBLOCK] We want to keep our hands clean because that's the right thing to do, but the rightest thing to do is bring freedom to the people of Regulus.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] If we lose when we could have won, then that's the larger moral failure.[SOFTBLOCK] But this fact does not excuse us.[SOFTBLOCK] We must face justice for what we will do.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Some people act like self-defense is justified because it's a necessary use of violence.[SOFTBLOCK] What it actually is, is a tragedy where there are no good choices.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I see.[SOFTBLOCK] It is not enough to win most efficiently, we must win the best moral victory possible.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] How can we weigh the losses we will take versus the justifications?[SOFTBLOCK] When does someone's life become worth less than moral purity?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] That one is a bit easier.[SOFTBLOCK] It is never just the choice between those two things.[SOFTBLOCK] If you commit war crimes to win, you normalize war crimes.[SOFTBLOCK] In the future, you will do more.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] If you lionize victory, make war seem like a good thing, there will be more wars.[SOFTBLOCK] We are responsible for that future, too.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] This is extremely complicated.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] We'll get through it together, 55.[BLOCK][CLEAR]")