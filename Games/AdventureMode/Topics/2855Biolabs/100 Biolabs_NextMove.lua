--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm           = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iSpokeTo2856Biolabs      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N")
local iSawMusicComments        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMusicComments", "N")
local iSawRaijuIntro           = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
local iGotBreachingTools       = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotBreachingTools", "N")
local iBreachedDoor            = VM_GetVar("Root/Variables/Chapter5/Scenes/iBreachedDoor", "N")
local iMetVivify               = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetVivify", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] We should reasses our current objective.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative, Unit 2855.[BLOCK][CLEAR]")

--Player needs to make it to the Raiju Ranch.
if(iSawRaijuIntro == 0.0) then
    
    --Hasn't seen the music comments:
    if(iSawMusicComments == 0.0) then
        
        --Didn't speak to 2856 yet:
        if(iSpokeTo2856Biolabs == 0.0) then
            WD_SetProperty("Append", "Christine:[E|Neutral] Considering our temporary alliance, it may be best to coordinate with Unit 2856.[SOFTBLOCK] She's over there at the hydrologic uplink terminal.[BLOCK][CLEAR]")
            WD_SetProperty("Append", "Christine:[E|Laugh] Or we could not, and that'd really set her off![BLOCK][CLEAR]")
            WD_SetProperty("Append", "55:[E|Neutral] Please focus on the mission assessment, Unit 771852.[BLOCK][CLEAR]")
            WD_SetProperty("Append", "Christine:[E|Neutral] Affirmative.[SOFTBLOCK] Assessment is to speak with Unit 2856.[BLOCK][CLEAR]")
            WD_SetProperty("Append", "55:[E|Neutral] Mission assessment acceptable.[SOFTBLOCK] Thank you, Unit 771852.[BLOCK][CLEAR]")
        
        
        --Spoke to her:
        else
            WD_SetProperty("Append", "Christine:[E|Neutral] Our current objective is the Raiju Ranch, to link up with what little security presence remains and figure out how we're going to stop Vivify.[BLOCK][CLEAR]")
            WD_SetProperty("Append", "55:[E|Neutral] Secondary objectives.[BLOCK][CLEAR]")
            WD_SetProperty("Append", "Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[BLOCK][CLEAR]")
            WD_SetProperty("Append", "55:[E|Neutral] Mission assessment acceptable.[SOFTBLOCK] Thank you, Unit 771852.[BLOCK][CLEAR]")
        end
    
    --Knows they need to get to the Raiju Ranch:
    else
        WD_SetProperty("Append", "Christine:[E|Neutral] Our current objective is the Raiju Ranch, to link up with what little security presence remains and figure out how we're going to stop Vivify.[BLOCK][CLEAR]")
        WD_SetProperty("Append", "55:[E|Neutral] Secondary objectives.[BLOCK][CLEAR]")
        WD_SetProperty("Append", "Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[BLOCK][CLEAR]")
        WD_SetProperty("Append", "55:[E|Neutral] Mission assessment acceptable.[SOFTBLOCK] Thank you, Unit 771852.[BLOCK][CLEAR]")
    end

--Player has reached the Raiju Ranch, needs the breaching tools:
elseif(iGotBreachingTools == 0.0) then
    WD_SetProperty("Append", "Christine:[E|Neutral] In order to access the Epsilon labs, we're going to need some tools to cut through the military-grade fortifications there.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] These tools are stored in a shed on the eastern side of the Raiju Ranch.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Secondary objectives.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Mission assessment acceptable.[SOFTBLOCK] Thank you, Unit 771852.[BLOCK][CLEAR]")

--Player has the breaching tools:
elseif(iBreachedDoor == 0.0) then
    WD_SetProperty("Append", "Christine:[E|Neutral] In order to access the Epsilon labs, we'll need to move to the Beta-Gamma security checkpoint and check the Epsilon access in the northeastern area.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] I can then cut the walls out and we can access the labs.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Secondary objectives.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Mission assessment acceptable.[SOFTBLOCK] Thank you, Unit 771852.[BLOCK][CLEAR]")

--Player cut the door out:
elseif(iMetVivify == 0.0) then
    WD_SetProperty("Append", "Christine:[E|Neutral] Our objective is to confront Project Vivify and neutralize her as a threat to Regulus City.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Sad] And possibly everything else in existence...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Do you have a plan for neutralizing her?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Sad] No.[SOFTBLOCK] It might be a fight, it might not.[SOFTBLOCK] I simply can't be sure...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Very well.[SOFTBLOCK] Secondary objectives.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Mission assessment acceptable.[SOFTBLOCK] Thank you, Unit 771852.[BLOCK][CLEAR]")

--Player has confronted Vivify:
else
    WD_SetProperty("Append", "Christine:[E|Neutral] With Project Vivify neutralized, we should return to the Raiju Ranch and, if possible, neutralize Unit 2856 before she has a chance to call for backup.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] With her out of the way, our revolution will have a much better chance of success.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] Thank for the assessment.[SOFTBLOCK] Move out.[BLOCK][CLEAR]")
end