--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iHasSteamDroidForm = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] Christine, I would like to discuss 'All My Processors'.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Oh that drama show that's always on the RVDs?[SOFTBLOCK] What of it?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Happy] What's your favourite episode?[SOFTBLOCK] Mine's series 451, episode 12, when Daring hangs upside down to trick Drakon and then asks Drakon where name is, and then asks again and she just says 'Drakon' because it was all a part of a plot to trick Jobel into helping Patricia![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Happy] And then Daring buys an entire city by turning all the furniture in the inn into gold![BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I do not have a favourite episode.[SOFTBLOCK] I often listen to the show while I am researching or attempting to access restricted areas.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Oh?[SOFTBLOCK] Why?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] The harmonics of the show interface well with my processor.[SOFTBLOCK] I do not know why.[SOFTBLOCK] That is, in fact, an area of active research.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] So what did you want to talk about it for?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I recently discovered the show is written by a primitive artificial intelligence under development at the Arcane University.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] The staff writers take the AI's work and translate it into scripts for the actors.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Happy] Wow![SOFTBLOCK] Maybe someday soon, AIs will be as smart as partirhumans![BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] It would require either many advances in synthetic intelligence, or else enormous amounts of redundancies and an incredible amount of processing power.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] It is theoretically possible, but Regulus City does not yet have the infrastructure for it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] But, assuming that were the case.[SOFTBLOCK] I...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] How does one distinguish an AI from a partirhuman, like us?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] What if I, for example, had never been a human, but instead was an AI placed inside a command unit's body?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Our processors can't be generated synthetically, 55.[SOFTBLOCK] They are created from the human brain during the transformation process.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] True, but there is no physical difference.[SOFTBLOCK] In the future, the creation of an intelligence like ours will be possible.[SOFTBLOCK] How will we know when the AI becomes a person?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] While that's probably a really tough philosophical question, I do have an answer.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Art.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Art?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] When the machine is capable of producing works of art, it is now a person.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] But 'All My Processors' is a work of art, isn't it?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] No, not from the perspective of the AI.[SOFTBLOCK] To the AI there is no distinction between anything.[SOFTBLOCK] It creates what it creates.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] It is us, the observer, who are creating the work of art because we interperet it as such.[SOFTBLOCK] So, the AI must be capable of perceiving art, struggling with it, and learning from it.[SOFTBLOCK] Then, creating it.[SOFTBLOCK] Then it will be a person.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I have little appreciation of art, Christine.[BLOCK][CLEAR]")
if(iHasSteamDroidForm == 1.0) then
    WD_SetProperty("Append", "Christine:[E|Smirk] SX-399.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Upset] What about her?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Talk about her a bit.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Neutral] Hey ocular receptors are...[SOFTBLOCK] round.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Blush] Her hair is red and soft.[SOFTBLOCK] Her lips are...[SOFTBLOCK] soft...[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Laugh] You appreciate art, 55.[SOFTBLOCK] You're not all-the-way robot![SOFTBLOCK] Ha ha![BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Blush] (Yes, SX-399 is truly a work of art...)[BLOCK][CLEAR]")
else
    WD_SetProperty("Append", "Christine:[E|Smirk] Then maybe I'll take you on a tour of the city when all this is over.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] The architecutre of Regulus City is a work of art, isn't it?[SOFTBLOCK] Tiny towers thrusting defiantly at infinity, promising it that we will not be limited by the void of space.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Our little home of light in a sea of black.[SOFTBLOCK] That's art, isn't it?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "55:[E|Smirk] I suppose it is.[SOFTBLOCK] Thank you, Christine.[BLOCK][CLEAR]")
end
