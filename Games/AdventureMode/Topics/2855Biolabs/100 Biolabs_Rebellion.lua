--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "Christine:[E|Neutral] Before you ask something else, 55, I wanted to know how you feel about the revolution.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] It started as a necessary means of self-defense.[SOFTBLOCK] I was merely protecting myself against an existential threat, and enlisted others in that defense.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Over time, it became apparent that my self-preservation required the destruction of that threat.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] That's how you felt.[SOFTBLOCK] How do you feel now?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Down] I...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I have decided that the things I did as the Head of Security were results of the conditioning I had been exposed to, and that it was wrong.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Both my actions, and the conditioning I was subject to, that is.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Therefore, the revolution I have helped foment is the morally correct way to atone for the actions I committed, by preventing the source of those actions from committing them again through another medium.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I am prepared to face retirement for this belief.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Further, this belief is no longer selfish.[SOFTBLOCK] Even were I in your position, and innocent, I would still follow through with this.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] You've never had to put it into words, have you?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smirk] Having something to believe in and knowing what it is is quite liberating.[BLOCK][CLEAR]")
