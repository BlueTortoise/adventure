--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "2855Biolabs")

WD_SetProperty("Append", "55:[E|Neutral] Unit 771852, do you believe there are other runestones like yours?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I know for certain there are five others like it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Interesting.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] You seemed to know about them when you ambushed me under sector 96.[SOFTBLOCK] Did you know of the others?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Yes, Regulus City has collected data suggesting there are at least two more.[SOFTBLOCK] These are based on reports from the surface and memories from abducted humans that were converted.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] The reports did not seem to understand the true gravity of what the runestones may mean, considering your interactions with Vivify.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] You're right.[SOFTBLOCK] I wonder if those other five are going through what we're going through.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Do you know any specifics about the two bearers?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] One report was about a set of rumours concerning an individual named 'Mei'.[SOFTBLOCK] She appeared one day and disappeared shortly after in the Trannadar region of north-central Arulente.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] She seemed to possess the transformative and teleportation powers you do.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] And the other?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] No name.[SOFTBLOCK] A blue-haired magician sighted by an undercover cell in the major port city of Jeffespeir.[SOFTBLOCK] They intended to convert her, actually.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Happy] Did they succeed?[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] I could not access the report.[SOFTBLOCK] It had been scrubbed from the network, most likely by my sister.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Damn...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Smug] You seemed interested in her conversion status.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] Heh...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] It'd be nice to meet another unit from Earth, maybe swap stories, reminisce on where we came from.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] And plot our eventual return to bring mechanical perfection to the people of Earth...[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] My assessment was correct.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] How are you certain there are five others, though?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Same way I knew I could transform myself, somehow.[SOFTBLOCK] It's just something I know, like I always knew it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "55:[E|Neutral] Interesting.[SOFTBLOCK] If we are successful here, we may wish to find these other five bearers and coordinate with them.[SOFTBLOCK] There may be other threats you are uniquely suited for like Project Vivify.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Agreed![BLOCK][CLEAR]")
