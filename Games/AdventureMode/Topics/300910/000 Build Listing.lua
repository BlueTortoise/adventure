--[Build Listing]
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "300910"

--Topic listing.
fnConstructTopic("Name",            "Name",              1, sNPCName, 0)
fnConstructTopic("BreedingProgram", "Breeding Program", -1, sNPCName, 0)
fnConstructTopic("Raijus",          "Raijus",           -1, sNPCName, 0)
fnConstructTopic("2855",            "2855",              1, sNPCName, 0)
fnConstructTopic("Sensors",         "Sensors",           1, sNPCName, 0)