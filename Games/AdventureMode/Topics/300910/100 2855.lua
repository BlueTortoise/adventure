--[Unit 2855]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Christine:[E|Neutral] So you knew 55 before she wiped her memory, right?[SOFTBLOCK] What was she like?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Unit 771852, it's not polite to speak about someone when they're present.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "55:[E|Neutral] As far as I'm concerned, who I was then is gone.[SOFTBLOCK] I do not remember who I was.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] I understand...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] I'll say she hasn't changed too much.[SOFTBLOCK] Brusque, not talkative, and very focused.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] But these aren't bad things.[SOFTBLOCK] When she wants to help you, she will get things done and in a hurry.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, that's the 55 I know.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "55:[E|Neutral] It is - [SOFTBLOCK]comforting, I would venture.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] A memory wipe won't change who you are.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "55:[E|Neutral] You sound certain, but you should not be.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Uh, you've said that before.[SOFTBLOCK] To me, in a different context, but you've said exactly that before.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Oh ho ho![BLOCK][CLEAR]")
	WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "55:[E|Neutral] Unit 300910, were you aware I am currently wanted for questioning by Central Administration?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Yes, I was.[SOFTBLOCK] It was on my briefing a while back.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "55:[E|Neutral] But you did not act on that information.[SOFTBLOCK] Doubtless they would reward you for turning me in.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Never![BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Besides, I just assumed it was petty politicking.[SOFTBLOCK] Command Units at Central are always jockeying for position.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Glad I'm not part of that mess, and I don't want to be.[SOFTBLOCK] If you're wanted for something, I'm sure you'll work it out eventually.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "55:[E|Neutral] Does it not concern you?[SOFTBLOCK] Do you know why I am wanted for questioning?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] No, and I'm not going to ask.[SOFTBLOCK] In fact, you were never here, and I'll make sure the other units don't report you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] I hope you get things sorted out, but I know you'll do it on your own terms.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] That's as good an answer as we're ever going to hear, 55.[BLOCK][CLEAR]")

end