--[Breeding Program]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local iKnowsAboutBreedingProgram = VM_GetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N")
	
	--Flags.
	VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N", 1.0)
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Christine:[E|Neutral] What can you tell me about the breeding program?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Ahhhh, those were the days.[SOFTBLOCK] I really should go back and see how things are going over there next time I'm in for review.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Oh, and I should send a message to Unit 300917![SOFTBLOCK] She was my best friend, and I haven't seen her in months.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Did you have secondary designations in the program?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Yes, but we had to write our primary designations on everything in the education curriculum.[SOFTBLOCK] We only used secondary designations when we were by ourselves.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Unit 278282 was very strict about that, and would give you shearing duty if she overheard you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] What did you do while you were there?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Well, you wake up,[SOFTBLOCK] attend morning fitness,[SOFTBLOCK] education classes in the afternoon,[SOFTBLOCK] afternoon fitness,[SOFTBLOCK] and then you can do whatever you want after that.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] I would usually go play with the Raijus, or hang out with the younger units.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Younger units?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Uh, yes.[SOFTBLOCK] Is that odd?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] You mean, younger children?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] That's what I said,[SOFTBLOCK] isn't it?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] We used to call each other kids where I'm from.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Like baby goats?[SOFTBLOCK] I think I'll stick to units if it's all the same to you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Anyway, every fifth day was free of education classes, and we'd sometimes go visit Regulus City if we could convince our overseers to give us day passes.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] I remember scaring the daylights out of Unit 199230 once![SOFTBLOCK] She screamed[SOFTBLOCK] 'Rogue human! Rogue human! Security, help!'[SOFTBLOCK] when I showed up in the cafeteria.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Even when I showed her my badge and told her I was Unit 300910, she didn't calm down until security took me away.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I haven't seen any human children in Sector 96.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Oh, you'd probably only see them in Sector 3.[SOFTBLOCK] That's the only place we could get day passes to.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] You're bringing back a lot of memories, 771852.[SOFTBLOCK] I really miss the old habitat.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] But I have responsibilities to attend to.[SOFTBLOCK] Still, I really want to go play with the Raijus now...[BLOCK][CLEAR]")

	--Topic unlocks.
	WD_SetProperty("Unlock Topic", "Raijus", 1)

end