--[Name]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local iKnowsAboutBreedingProgram = VM_GetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N")
	
	--Flags.
	VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N", 1.0)
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Christine:[E|Neutral] Can you tell me a bit more about yourself?[SOFTBLOCK] Why do you have such a high designation as a Command Unit?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] You know, I never really thought about it that way.[SOFTBLOCK] Most of the other Command Units have pretty low designations.[SOFTBLOCK] Guess that means they were converted well before me.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] I grew up in the breeding program.[SOFTBLOCK] Regulus Citizen, born and raised![BLOCK][CLEAR]")
	if(iKnowsAboutBreedingProgram == 0.0) then
		WD_SetProperty("Append", "Christine:[E|Neutral] Breeding program?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] ...[SOFTBLOCK] 771852?[SOFTBLOCK] You must be a fairly new unit...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] The Biological Research Labs have an enormous artificial habitat.[SOFTBLOCK] They keep specimens there, and we breed humans for conversion.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] I've heard humans actually evolved on Pandemonium, but I've never been there myself.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Christine:[E|Neutral] Does that bother you?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] When I was human, I was fed, watered, educated, and always had a nice bed to defr-[SOFTBLOCK] sleep in.[SOFTBLOCK] The Slave Units were always very kind to me, and I got to play with the Raijus every day after fitness routines.[SOFTBLOCK] Could you ask for anything more?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Christine:[E|Neutral] So were you picked to become a Command Unit, then?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] I suppose so.[SOFTBLOCK] When I turned 17, I had to take an aptitude test and some other things.[SOFTBLOCK] I don't know how the other girls did, but I guess they liked my answers.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] There was a very strange test afterwards...[SOFTBLOCK] I'd rather not talk about it...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] And then I was converted, and here I am.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Christine:[E|Neutral] Huh.[SOFTBLOCK] I had sort of assumed they didn't make new Command Units.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] I'd guess I was a replacement, or maybe we needed to expand.[SOFTBLOCK] After all, I do oversee this observatory, which was under construction when I was converted.[SOFTBLOCK] We'll need new Command Units sooner or later.[BLOCK][CLEAR]")
	else
		WD_SetProperty("Append", "Christine:[E|Neutral] I'm a little jealous![SOFTBLOCK] You're a true Regulan![BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] Are you from Pandemonium?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Christine:[E|Neutral] No, but -[SOFTBLOCK] it's -[SOFTBLOCK] it's not important.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Christine:[E|Neutral] So why did you become a Command Unit and not a Lord Unit?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] When I turned 17, I had to take an aptitude test and some other things.[SOFTBLOCK] I don't know how the other girls did, but I guess they liked my answers.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] There was a very strange test afterwards...[SOFTBLOCK] I'd rather not talk about it...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] And then I was converted, and here I am.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Christine:[E|Neutral] Huh.[SOFTBLOCK] I had sort of assumed they didn't make new Command Units.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "300910:[E|Neutral] I'd guess I was a replacement, or maybe we needed to expand.[SOFTBLOCK] After all, I do oversee this observatory, which was under construction when I was converted.[SOFTBLOCK] We'll need new Command Units sooner or later.[BLOCK][CLEAR]")
	end
	WD_SetProperty("Append", "300910:[E|Neutral] Now it's funny you ask, because I've been asked why I'm different from the other Command Units.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "55:[E|Neutral] I was about to point that out.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Maybe it's because I'm from the breeding program?[SOFTBLOCK] Where are you from, 2855?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Ah, right.[SOFTBLOCK] Memories.[SOFTBLOCK] Sorry.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Was there anything else I could help you with?[BLOCK][CLEAR]")
	
	--Topic unlocks.
	WD_SetProperty("Unlock Topic", "BreedingProgram", 1)
end