--[Raijus]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Flags.
	VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N", 1.0)
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Christine:[E|Neutral] How do you generate power here without any Raijus?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] We have an underground cable that goes to the LRT Facility.[SOFTBLOCK] They've got a non-baryonic conversion reactor over there.[SOFTBLOCK] Raijus are really only used in Regulus City.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Plus if we had any Raijus here, I'd never get any work done.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Because?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Obviously I'd be snuggling them all the time![SOFTBLOCK] They're so fuzzy and huggable![BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] If you're ever in the biolabs, you should definitely go and play with them.[SOFTBLOCK] Units from the breeding program with exceptionally affectionate personalities are selected to become Raijus.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "300910:[E|Neutral] Oh, but be careful not to zap yourself.[SOFTBLOCK] They tend to zap when they get excited.[SOFTBLOCK] Ha ha![BLOCK][CLEAR]")
	
end