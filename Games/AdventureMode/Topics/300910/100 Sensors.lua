--[Topic Script]
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iRecalibratesDone = VM_GetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--No recalibrations yet:
if(iRecalibratesDone < 1.0) then
    WD_SetProperty("Append", "300910:[E|Neutral] So, to reiterate::[SOFTBLOCK] The sensors we have set up in the crater shelf need to be recalibrated.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "300910:[E|Neutral] I can't do it remotely, it has to be done by hand.[SOFTBLOCK] Can you help us with that?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Shouldn't be a problem.[SOFTBLOCK] What do I need to do?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "300910:[E|Neutral] There's a waystation partway down the crater shelf.[SOFTBLOCK] All the sensors connect with it via a heavy-duty cable.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "300910:[E|Neutral] An intercom in the waystation should allow us to keep in contact.[SOFTBLOCK] Head down there and call me when you do.[SOFTBLOCK] The calibration codes are in that waystation.[BLOCK][CLEAR]")

--Started but not completed.
elseif(iRecalibratesDone < 6.0) then
    WD_SetProperty("Append", "300910:[E|Neutral] I see you've got some of our sensors recalibrated.[SOFTBLOCK] Is everything all right?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] We got a little side-tracked.[SOFTBLOCK] There's six sensors, right?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "300910:[E|Neutral] Correct.[SOFTBLOCK] Thank you for this, Christine.[SOFTBLOCK] You're serving the Cause of Science very directly here.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] We're always happy to help.[SOFTBLOCK] You can count on us.[BLOCK][CLEAR]")

--Completed. Turning the quest in.
elseif(iRecalibratesDone == 6.0) then

    --Variables.
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N", 1000)
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 200)

    --Dialogue.
    WD_SetProperty("Append", "300910:[E|Neutral] Well, I figured you'd have called me on the intercom, but this is nice too.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] I prefer the personal touch.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] All your sensors are reset and ready to go.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "300910:[E|Neutral] Quite right![SOFTBLOCK] We're already collecting data again, and nobody had to get hurt![BLOCK][CLEAR]")
    WD_SetProperty("Append", "300910:[E|Neutral] Well done, Christine.[SOFTBLOCK] I've sent 200 Work Credits to your account.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Smirk] Happy to help![SOFTBLOCK] Anything else we can do?[BLOCK][CLEAR]")
    
    --If Christine does not have Darkmatter form, there's more quests to do:
    if(iHasDarkmatterForm == 0.0) then
        WD_SetProperty("Append", "300910:[E|Neutral] I presume you have not fully investigated the facility at the base of the crater.[BLOCK][CLEAR]")
        WD_SetProperty("Append", "300910:[E|Neutral] I have my suspicions that it is related in some way to the recent events on the shelf, but I do not have the resources to find out.[BLOCK][CLEAR]")
        WD_SetProperty("Append", "300910:[E|Neutral] The entrance point is to the west of the waystation you found the sensors at.[SOFTBLOCK] There should be a ladder down to the staging area.[BLOCK][CLEAR]")
        WD_SetProperty("Append", "Christine:[E|Neutral] We'll be heading there next, then.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]")
    
    --No that's enough.
    else
        WD_SetProperty("Append", "300910:[E|Neutral] You've already helped the observatory far in excess of what I could have expected.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]")
        WD_SetProperty("Append", "300910:[E|Neutral] We owe you a great debt.[SOFTBLOCK] All we can do is try to prepare for what comes next.[BLOCK][CLEAR]")
    end

--If it's at 1000
else
    WD_SetProperty("Append", "300910:[E|Neutral] Hmm, the volumetric scanners went out of phase all at once, and there was a ripple felt further out.[SOFTBLOCK] It was like an earthquake.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "Christine:[E|Neutral] Aren't earthquakes rather common around here?[BLOCK][CLEAR]")
    WD_SetProperty("Append", "300910:[E|Neutral] They are, but the energy travels through rock, not three-dimensional space.[BLOCK][CLEAR]")
    WD_SetProperty("Append", "300910:[E|Neutral] Yet if these numbers are correct, the phase disruption was volumetric.[SOFTBLOCK] As if space itself was shaken, and not just the rock...[BLOCK][CLEAR]")

end