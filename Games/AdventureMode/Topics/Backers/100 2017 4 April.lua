--[Alraunes]
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	WD_SetProperty("Append", "Mei:[E|Smirk] For April, it's RepeatedMeme, Kasumi Takanawa, and Tim Merritt.[SOFTBLOCK] You're making this possible![BLOCK][CLEAR]")
end