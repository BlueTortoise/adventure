--[Alraunes]
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	WD_SetProperty("Append", "Christine:[E|Neutral] Productivity awards were given to #901922 'Austin Durbin', #917114 'MarioneTTe', #900102 'RepeatedMeme', and #912772 'Forefox' for December.[SOFTBLOCK] Serve the Cause of Science well![BLOCK][CLEAR]")
end