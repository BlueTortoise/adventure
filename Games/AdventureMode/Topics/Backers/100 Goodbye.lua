--[Goodbye]
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
WD_SetProperty("Append", "Mei:[E|Happy] Thanks for your support, everyone![BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] Wow, I really wrecked the fourth wall there...")