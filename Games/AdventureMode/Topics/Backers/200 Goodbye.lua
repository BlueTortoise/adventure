--[Goodbye]
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
WD_SetProperty("Append", "Christine:[E|Neutral] All the units here are so productive! They really deserve commendations.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Regulus City wouldn't exist if it weren't for all its supporters!")