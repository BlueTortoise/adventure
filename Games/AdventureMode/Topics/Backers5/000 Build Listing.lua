--[Build Listing]
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Backers5"

--Chapter 5 backers.
fnConstructTopic("2017 B November",  "November 2017",  1, sNPCName, 0)
fnConstructTopic("2017 C December",  "December 2017",  1, sNPCName, 0)
fnConstructTopic("2018 0 January",   "January 2018",   1, sNPCName, 0)
fnConstructTopic("2018 1 February",  "February 2018",  1, sNPCName, 0)
fnConstructTopic("2018 2 March",     "March 2018",     1, sNPCName, 0)
fnConstructTopic("2018 3 April",     "April 2018",     1, sNPCName, 0)
fnConstructTopic("2018 4 May",       "May 2018",       1, sNPCName, 0)
fnConstructTopic("2018 5 June",      "June 2018",      1, sNPCName, 0)
fnConstructTopic("2018 6 July",      "July 2018",      1, sNPCName, 0)
fnConstructTopic("2018 7 August",    "August 2018",    1, sNPCName, 0)
fnConstructTopic("2018 8 September", "September 2018", 1, sNPCName, 0)
fnConstructTopic("2018 9 October",   "October 2018",   1, sNPCName, 0)
fnConstructTopic("2018 A November",  "November 2018",  1, sNPCName, 0)
fnConstructTopic("2018 B December",  "December 2018",  1, sNPCName, 0)
