--[Alraunes]
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers5")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	WD_SetProperty("Append", "Christine:[E|Smirk] Looks like the units with distinguised service listing for April was the same as the previous two months. Units #901922 'Austin Durbin', #912792 'Christian Gross', #917114 'MarioneTTe', and #900102 'RepeatedMeme'.[SOFTBLOCK] You ladies are on a roll![BLOCK][CLEAR]")
end