--[Alraunes]
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers5")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	WD_SetProperty("Append", "Christine:[E|Smirk] The only commendation for September was the outstanding achievement award continued from August, Unit #901157 'Klaysee'.[SOFTBLOCK] Her productivity was so high that her name was etched onto a research satellite that will leave the solar system in 117 years time, to be found by extraterrestrials long after the sun has gone cold.[BLOCK][CLEAR]")
end