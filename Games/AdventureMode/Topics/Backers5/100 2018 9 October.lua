--[Alraunes]
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers5")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	WD_SetProperty("Append", "Christine:[E|Smirk] Let's see... October... Familiar set of names...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Smirk] #912792 'Christian Gross', #917114 'MarioneTTe', #900102 'RepeatedMeme', Unit #903293 'Diana Nonya', Unit #999000 'Marek'...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Smirk] Units #901922 'Austin Durbin', and of course the exemplary award for Unit #901157 'Klaysee' who I am starting to develop feelings for...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Strictly platonically, of course...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] And then newcomer Unit #966403 'Gaming Chocobro', who worked very hard to get on the list.[SOFTBLOCK] Well done![BLOCK][CLEAR]")
end