--[Alraunes]
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers5")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	WD_SetProperty("Append", "Christine:[E|Smirk] In December, we had #901922 'Austin Durbin', #917114 'MarioneTTe', #900102 'RepeatedMeme'...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Scroll down a bit...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Yadda yadda yadda...[SOFTBLOCK] 'Proud of contribution'...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Smirk] Unit 966403 'Gaming Chocobro' and Unit 993012 'Abrissgurke'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Laugh] Unit #901157 'Klaysee' has achieved such success that she's getting repurposed![SOFTBLOCK] We are proud of our new Lord Unit sister![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Offended] Wait - [SOFTBLOCK]Unit 901157 was flagged as a Drone Unit before that?[SOFTBLOCK] What the hay?[BLOCK][CLEAR]")
end