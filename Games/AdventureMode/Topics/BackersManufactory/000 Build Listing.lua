--[Build Listing]
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "BackersManufactory"

--Chapter 5 backers.
fnConstructTopic("2019 A November", "November 2019", 1, sNPCName, 0)
fnConstructTopic("2019 B December", "December 2019", 1, sNPCName, 0)
fnConstructTopic("2020 C January",  "January 2020",  1, sNPCName, 0)
fnConstructTopic("2020 D February", "February 2020", 1, sNPCName, 0)
