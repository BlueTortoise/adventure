--[Backers]
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersManufactory")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	WD_SetProperty("Append", "Christine:[E|Neutral] Let's see which of my dream sequences 55 was looking over...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File MEM-5, 'Abrissgurke'.[SOFTBLOCK] Three slime girls, Lew, Goop-Sinpai, and Aaaac assemble around a table and prepare to eat an organic food substance.[SOFTBLOCK] Memory files label it 'Pizza'.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Lew halts the other slimes and produces a pineapple from nowhere, then places it on the pizza.[SOFTBLOCK] She eats a slice of pizza with pineapple on it, including the rind and leaves, in one bite.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'A group of pineappleas appear from the sky and surround Lew, who has a series of hallucinatory dances.[SOFTBLOCK] A human named James Upton appears and is squished by a pineapple.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Moments later, we return to the table.[SOFTBLOCK] Sinpai and Aaaac are horrified to see Lew has become a pineapple girl.[SOFTBLOCK] James Upton is nearby, also a pineapple girl.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The pineapple girls place more pineapples on the pizza.[SOFTBLOCK] Horrified, Sinpai and Aaaac eat it, complimenting its flavor.[SOFTBLOCK] More pineapples appear from the sky.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] The music that played in the dream sequence was most enjoyable.[SOFTBLOCK] Pineapple girls are not a partirhuman species.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Laugh] (Live just a little, 55...)[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File JR-12 'Repeated Meme'.[SOFTBLOCK] Two golems are showering, cleaning biological residue off their chassis.[SOFTBLOCK] They are Pandavenger and Kumquat.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The drain clogs and one of the golems reaches into the pipe to clear it.[SOFTBLOCK] Organic material begins bubbling out of the pipe.[SOFTBLOCK] The golems flee.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The material bursts through the floor and grows across the walls and floors.[SOFTBLOCK] An emergency response crew headed by Austin Durbin attempts to set fire to the material, but it regenerates as fast as it burns.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The sector is evacuated and bombarded by long-range artillery.[SOFTBLOCK] The organic material is not contained.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Possible future memory, though Christine herself is not present.[SOFTBLOCK] Organic material may be related to project Vivify.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Sad] I don't remember having this dream...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File Q19-P, 'Taedas'.[SOFTBLOCK] A lord golem and two slaves are sitting around a table.[SOFTBLOCK] The lord is named Klaysee, the others are Dyamonde and Sudoku.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Klaysee begins playing a game with a knife, stabbing it between her fingers on the table.[SOFTBLOCK] Dyamonde and Sudoku begin to clap, with each clap matching a stab of the knife.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The tempo slowly increases as the claps speed up until Klaysee's motions with the knife blur together.[SOFTBLOCK] Eventually, light itself begins to be pulled in.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The dream goes dark, and it is revealed this was taking place inside of a pot in a kitchen.[SOFTBLOCK] Klaysee steps out of the pot and places spaghetti noodles in it.[SOFTBLOCK] Dyamonde and Sudoku do not mind the boiling water as golems can withstand the temperature.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Klaysee serves the spaghetti with Dyamonde and Sudoku on it.[SOFTBLOCK] The golems eat it from within.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Unlikely future memory unless the spaghetti is a metaphor.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Smirk] Well 55, in dreams, everything is a metaphor...[BLOCK][CLEAR]")
end