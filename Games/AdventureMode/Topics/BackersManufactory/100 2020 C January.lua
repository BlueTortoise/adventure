--[Backers]
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersManufactory")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Variables.
    local iSXMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
    
    --Dialogue.
	WD_SetProperty("Append", "Christine:[E|Neutral] Let's see which of my dream sequences 55 was looking over...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File R7-D 'Abrissgurke'.[SOFTBLOCK] Long series of images of a location in Cornwall, a tourist destination.[SOFTBLOCK] Perception records disgust looking at crane game.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Crane game filled entirely with bread loaves of varying descriptions, all pristine.[SOFTBLOCK] Wine and cigarettes can be found in a nearby vending machine.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Ask Christine what 'Cornwall' is and verify accuracy.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Sad] Well, if it's Cornwall, it's probably tacky, boorish wine.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Smirk] I went there on vacation once, so that's got to be the genesis.[SOFTBLOCK] Maybe I ate something bad?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File P22-Q 'Sudoku'.[SOFTBLOCK] Perception is playing a card game.[SOFTBLOCK] There are two other players::[SOFTBLOCK] Memory identifies them as 'Kumquat' and 'Dyamonde'.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'These are animated characters from a TV show about giant fighting monsters.[SOFTBLOCK] Kumquat's hands are too big to hold the cards.[SOFTBLOCK] Dyamonde cheats openly but never bets anything.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Midway through the dream, the floor falls out and the group parachutes on top of a whale who then joins the card game.[SOFTBLOCK] The player busts out and is mocked by the other players.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Verify fictional characters with Christine.[SOFTBLOCK] Appearances are inconsistent with recorded Earth fauna.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Blush] No, 55, that's what they actually look like.[SOFTBLOCK] It's a TV show I watched when I was a kid...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File INF 'Lew'.[SOFTBLOCK] A greenish steam droid unit is hiding under a tree in a biological forest environment covered with snow.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'She leaps up and fires a rifle wildly into the air for several minutes.[SOFTBLOCK] File ends.'[BLOCK][CLEAR]")
    if(iSXMet55 == 1.0) then
        WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Steam droid resembles SX-399, possible future memory.[SOFTBLOCK] Consult Christine for more.'[BLOCK][CLEAR]")
        WD_SetProperty("Append", "Christine:[E|Blush] I had that dream before we met SX-399.[SOFTBLOCK] I wonder if it's a future memory?[BLOCK][CLEAR]")
    else
        WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Steam droid design is highly unusual, possible future memory.[SOFTBLOCK] Consult Christine for more information.'[BLOCK][CLEAR]")
        WD_SetProperty("Append", "Christine:[E|Sad] I've got nothing on that one.[SOFTBLOCK] Maybe we meet them soon?[BLOCK][CLEAR]")
    end
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File RJQ-4 'Durbin'.[SOFTBLOCK] Perception is being arrested and interrogated by Administration forces.[SOFTBLOCK] Fellow units 'Morbo', 'Taedas', and 'Klaysee' are likewise being arrested.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Unit 2855 appears and shocked a security unit, followed by chaos.[SOFTBLOCK] Morbo is hit by stray fire, Klaysee drags her to safety.[SOFTBLOCK] Shooting and shouting.[SOFTBLOCK] Memory goes dark.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Dream has several inconsistencies and is likely not a memory of the future.[SOFTBLOCK] Design of pulse weapons is incorrect, perception's body appears human and not a golem.[SOFTBLOCK] Try to identify the units in question and confirm their identities.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File BR-12 'Pandavenger'.[SOFTBLOCK] Perception sits at the throne of a superior.[SOFTBLOCK] The entire environment is frozen, including the perception.[SOFTBLOCK] A command is heard to follow the superior 'Repeated Meme' on a walk.[SOFTBLOCK] Perception stands up and reveals her body is completely frozen.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The perception eventually stands before a frozen individual 'Goop Sinpai', and remains there for eternity, unmoving.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] This seems to be a dream about a work of fiction.[SOFTBLOCK] Confirm with Christine.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Blush] Yep, absolutely.[SOFTBLOCK] That's a story I read once...[SOFTBLOCK] I really liked it...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File PRO-FI 'James Upton'.[SOFTBLOCK] Perception is watching a group of idol singers 'Aaaac', and five backup dancers.[SOFTBLOCK] They are doing the Crunch.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The Crunch is a series of contortions that eventually result in a damaged spinal column, but the singer has no internal bone structure.[SOFTBLOCK] They remold themselves.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The backup dancers are not so lucky, and crack their spines.[SOFTBLOCK] They die on the stage and the crowd cheers.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Probably not a real event and likely a dream mixing multiple events.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Sad] I don't know about that, 55.[SOFTBLOCK] After what I've heard about K-Pop...[BLOCK][CLEAR]")
    
end