--[Backers]
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersManufactory")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	WD_SetProperty("Append", "Christine:[E|Neutral] Seems 55 already went through my dreams and left some notes...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File 00rE, 'Sinpai'.[SOFTBLOCK] Two humans in military garb are standing facing one another.[SOFTBLOCK] They are discussing soap.[SOFTBLOCK] Memory files identify them as Kumquat and Klaysee.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Kumquat produces a series of metal ingots from her hat, ranging from iron to brass to tellurium.[SOFTBLOCK] Klaysee laughs and takes them, grinding them to powder in her hands.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'She changes the ingots into a guitar.[SOFTBLOCK] A werecat named Pandavenger appears and places her hand on the guitar.[SOFTBLOCK] She says 'Solo Battle'.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'All characters in the dream bump their heads together, transporting them to a small island somewhere on Earth.[SOFTBLOCK] A passing boat, the H.M.S. Morbo, has cheering humans on it.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Kumquat and Klaysee begin playing guitars at one another.[SOFTBLOCK] The songs that play include instruments other than guitar.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The boat crashes into the island.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Unlikely future memory.[SOFTBLOCK] Otherwise, standard dream.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Offended] Standard?[SOFTBLOCK] How can a dream be standard?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File 2772-B, 'Sudoku'.[SOFTBLOCK] A human in a white moth costume, called the 'Peppered Moth', is beating a criminal named Aaaac.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'They are standing in Abrissgurke First National Bank, a location where humans store their valuables.[SOFTBLOCK] Aaaac is beaten unconscious.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Peppered Moth removes her mask to reveal she is Austin Durbin, billionaire philanthropist.[SOFTBLOCK] She calls a press conference inside the bank.[SOFTBLOCK] Aaaac is stomped on by a journalist looking for a photo.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The journalist is named Taedas.[SOFTBLOCK] After snapping the photo, she is standing on the moon, where everyone except Peppered Moth has vanished.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Peppered Moth shoots a grappling hook gun into the sun.[SOFTBLOCK] It catches and she pulls the sun towards here.[SOFTBLOCK] The moon is swallowed in plasmatic nuclear fire.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] This is an example of the superhero genre.[SOFTBLOCK] Similar files appear periodically in Christine's memory.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Blush] I'll just leave out the part where Peppered Moth is my original character...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'File FIREFIRE, 'Lew'.[SOFTBLOCK] Christine is visible in third person, sitting in a doctor's office.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Dr. Dyamonde appears with a needle larger than her body.[SOFTBLOCK] Christine recoils.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Dyamonde attempts to puncture Christine, but the needle continually misses until Dyamonde accidentally sticks herself.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Christine's head is abruptly replaced with a sparrow's.[SOFTBLOCK] The sparrow begins making tweeting sounds.[SOFTBLOCK] Another patient, Repeated Meme, enters the office and steals Christine.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'Dyamonde withdraws the needle.[SOFTBLOCK] Mushrooms have grown on the floor.[SOFTBLOCK] A mushroom girl appears from the floor, named James Upton.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] 'The mushroom girl arrests Dr. Dyamonde for practicing medicine without a licence and for selling illegal bananas.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] '2855's Notes::[SOFTBLOCK] Fear of medical personnel appears in other dream sequences.[SOFTBLOCK] Not likely a future memory.'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Sad] ...[SOFTBLOCK] I guess I really don't like doctors, do I?[BLOCK][CLEAR]")

end