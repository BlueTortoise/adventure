--[Build Listing]
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "BackersRaiju"

--Chapter 5 backers.
fnConstructTopic("2019 A January",  "January 2019",  1, sNPCName, 0)
fnConstructTopic("2019 B February", "February 2019", 1, sNPCName, 0)
fnConstructTopic("2019 C March",    "March 2019",    1, sNPCName, 0)
fnConstructTopic("2019 D April",    "April 2019",    1, sNPCName, 0)
fnConstructTopic("2019 E May",      "May 2019",      1, sNPCName, 0)
fnConstructTopic("2019 F June",     "June 2019",     1, sNPCName, 0)
