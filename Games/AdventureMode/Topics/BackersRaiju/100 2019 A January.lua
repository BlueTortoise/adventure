--[Backers]
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRaiju")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	WD_SetProperty("Append", "Christine:[E|Smirk] In January, the Raiju Ranch was cleaned up by #901922 'Austin Durbin', #917114 'MarioneTTe', #900102 'RepeatedMeme'...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] After being dirtied in a party by a bunch of Lord Golems...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit 966403 'Gaming Chocobro' and Unit 993012 'Abrissgurke' are listed as primary offenders...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Apparently it was a party for Unit #901157 'Klaysee', who recently got repurposed.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] They offered to clean up after themselves, but apparently were so inebriated from the festivities that they could barely stand.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Let this be a lesson for you::[SOFTBLOCK] Always drink Suddetrish Synthetic Wine in moderation or your motivators will need to be cleaned.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Blush] But what a party...[BLOCK][CLEAR]")
end