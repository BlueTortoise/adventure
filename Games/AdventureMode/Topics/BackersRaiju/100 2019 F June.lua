--[Backers]
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRaiju")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	WD_SetProperty("Append", "Christine:[E|Neutral] As of June...[SOFTBLOCK] Doctor Maisie had to fix up a certain unit due to a sialolith forming in her salivary glands followed by a nasty infection...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] In solidarity, Unit #901157 'Klaysee' decided to hold a fundraiser.[SOFTBLOCK] The theme was 'Dance Until you Drop'.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #902399 'Kumquat' did the 'Electropop Boogie Swing' for fifteen minutes until being forcibly removed from the stage because the electrical discharges were shocking nearby viewers.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #907723 'Dyamonde' did 'The Magpie', which is a dance involving taking things out of other units' pockets.[SOFTBLOCK] She executed it so flawlessly that nobody saw her for weeks afterwards to congratulate her.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #894551 'James Upton' didn't know how to dance but gave twenty work credits to everyone else who did.[SOFTBLOCK] What a hero![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #882301 'Sudoku' performed a profound version of 'Sit There and State Angrily', rumoured to be one of the most difficult dances to perform.[SOFTBLOCK] The audience was moved.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #870563 'Goop Sinpai' did the walk.[SOFTBLOCK] She did the wall all night.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #993012 'Abrissgurke' formed a pyramid with several other Lord Golems.[SOFTBLOCK] It wasn't technically dancing but lots of work credits were still raised.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #901922 'Austin Durbin' spun around for twenty minutes, performing an incredible show of breakdancing, before it was determined that her leg motivators had malfunctioned and she couldn't stop herself.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #911102 'Taedas' fell on her face and got really hurt.[SOFTBLOCK] However, this was the elaborate dance 'Ow ow oops oh no' and was in fact carefully rehearsed.[SOFTBLOCK] The audience burst into applause.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #874536 'Namapo' posed magnificently, causing onlookers to swoon.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Unit #900102 'RepeatedMeme' spent most of the contest cleaning the floor of the mangled remains of those who tried to dance outside their competencies.[SOFTBLOCK] She was the real hero.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Smirk] After the fundraiser, Klaysee gave all of the funds to the clinic here in the Raiju Ranch. Hopefully some renovations will give them more space soon![BLOCK][CLEAR]")
end