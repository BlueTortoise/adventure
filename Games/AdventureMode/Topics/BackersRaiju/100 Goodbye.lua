--[Goodbye]
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
WD_SetProperty("Append", "Christine:[E|Neutral] They sure to get up to some wild stuff here in the Biolabs.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I bet if someone enjoying reading these to whoever *documented and recorded these* we'll see more of them in the future!")