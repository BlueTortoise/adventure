--[Backers]
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRepressed")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	WD_SetProperty("Append", "Christine:[E|Neutral] Let's see...[SOFTBLOCK] Well, the memory is mostly intact, but the names are all over the place...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I was visiting the department store...[SOFTBLOCK] I don't recall its name.[SOFTBLOCK] It's in downtown London...[SOFTBLOCK] Lew's?[SOFTBLOCK] Lew's Clothes and Such?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Mother had picked out some shoes and we were on our way out when we heard a commotion.[SOFTBLOCK] A lady my mother was friends with, Sinpai, with was having an argument with a customer.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] The customer was screaming that birdseed was two pounds more expensive here than in the store three blocks over, Upton's.[SOFTBLOCK] Complaining about price in downtown London, well that's a first.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] The lady kept ranting about the birdseed and Sinpai turned to my mother and mouthed 'Help me'.[SOFTBLOCK] The customer didn't even notice.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Mother shrugged to say 'What do you want me to do?'[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] We stood there in solidarity for another five minutes until eventually the customer gave up and left.[SOFTBLOCK] The store manager, Mr. Durbin, came up to console Ms. Sinpai afterwards.[SOFTBLOCK] She looked like she was about to cry.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] As we left, the rain had let up.[SOFTBLOCK] It's London, so this literally never happens.[SOFTBLOCK] The two famous street performers, Sudoku and Abrissgurke, were outside doing their juggling act.[SOFTBLOCK] We stopped to watch them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] And then we heard the screaming.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] The entire crowd turned to look at the same lady we had seen before, now screaming her head off at a traffic officer who was giving her a ticket.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Officer RepeatedMeme explained, slowly and patiently over the screaming, that you can't, in fact, double park.[SOFTBLOCK] Anywhere.[SOFTBLOCK] At any time.[SOFTBLOCK] Because it's illegal.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] After about ten minutes of furtive yelling, two more police, Officer Klaysee and Officer Kumquat, showed up.[SOFTBLOCK] They also tried to patiently explain the simple laws of parking.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Because yelling very loudly had not worked, the lady decided to yell even louder.[SOFTBLOCK] At that point, another friend of my mother's, Taedas, came up behind the lady and socked her.[SOFTBLOCK] She dropped like a brick.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Taedas was later arraigned on charges of assault, but strangely, Judge Dyamonde dismissed the case when literally no witnesses could be found.[SOFTBLOCK] Somehow, all three officers and a dozen bystanders had somehow been looking the other way at the exact moment of the assault.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I think I repressed the memory due to the sheer volume of the shouting, but damned if that wasn't a happy ending to the story.[BLOCK][CLEAR]")
    
end