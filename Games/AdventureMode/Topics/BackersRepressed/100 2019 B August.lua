--[Backers]
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRepressed")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	WD_SetProperty("Append", "Christine:[E|Neutral] A repressed memory of me in a role-playing session on the internet...[SOFTBLOCK] I would have been a teenager at the time...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I was playing as Mina 'Klaysee' Jones, with my two friends Bill 'Kumquat' Allen and Marcy 'Sudoku-head' Bright.[SOFTBLOCK] We were in the woods exploring one day when a storm hit and we were forced to find shelter.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] We found a strange mansion that had never been there, and soon found we just couldn't leave.[SOFTBLOCK] All paths looped back to the manor.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Inside, we found a strange lady.[SOFTBLOCK] She told us her name was 'aaaac', and of course we didn't believe her.[SOFTBLOCK] She didn't seem to be all there.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Naturally we started exploring, looking for loot and trying to solve the mystery of the manor.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] They got my character first.[SOFTBLOCK] There were strange creatures in the halls, I failed a few rolls and got split off from the others.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I found a room with enormous stained-glass windows.[SOFTBLOCK] There were portraits made of the stained glass.[SOFTBLOCK] 'Taedas', 'Sinpai', 'Dyamonde'...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] A monster grabbed me and I failed my struggle roll.[SOFTBLOCK] The monster held me in front of an empty stained glass window with no portrait, until my character began to empty out.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] She turned clear as glass, and a portrait of her appeared in the stained glass.[SOFTBLOCK] The DM informed me that I was to find my former friends and capture them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] The other monsters then painted me to look like I had as a human.[SOFTBLOCK] I found my friends poring over a book written by Abrissgurke, a philosopher from the 12th century.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I played along.[SOFTBLOCK] The book held important information about the manor.[SOFTBLOCK] It told the story of Winston 'Upton' Smith, and how he escaped the manor.[SOFTBLOCK] They read it and figured they had a chance.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] The book said we needed to find another, special book that held a spell.[SOFTBLOCK] I said I knew where it was.[SOFTBLOCK] I led them into an ambush, and in the confusion, I took down Kumquat and dragged him away.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] The other monsters transformed him into a gothic doll girl, given the same mission as me.[SOFTBLOCK] At this point, the DM, RepeatedMeme, told me to drop my disguise.[SOFTBLOCK] I resumed my glass form and we began hunting Sudoku-head.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] We broke up for a bit to get some snacks and use the bathroom, and had a couple more players join.[SOFTBLOCK] 'Lew' managed to go all of six turns before getting turned into a living statue girl, and 'Dyamonde' accidentally fell into a pit that turned him into a girl made of clay.[SOFTBLOCK] So Sudoku-head was pretty screwed at this point, with the entire server working against her.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Kumquat caught up to her first but lost a few rolls and got defeated.[SOFTBLOCK] Sudoku had found the magic tome, which was the diary of Austin Durbin, the creator of the manor.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] If she could reach 'aaaac' before the rest of us, she'd win the game.[SOFTBLOCK] All of us monstergirls had to stop her.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Lew, being a living statue, basically tried to guard the attic where 'aaaac' was waiting.[SOFTBLOCK] Dyamonde disguised herself as a health potion nearby, and I concealed myself in the darkness.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Sudoku walked right into the trap but used a magic spell to freeze Dyamonde.[SOFTBLOCK] I got behind her and passed a grab roll with a natural 20.[SOFTBLOCK] I sleeper'd her until she passed out.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] When she woke up, she was a pretty geisha doll.[SOFTBLOCK] The monsters won.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I had a nightmare a few days later about the transformation into a stained-glass girl, which is probably why I repressed the memory.[SOFTBLOCK] I had come down with a flu and fever dreams are just awful.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Who would have thought I would wind up in Pandemonium where this sort of thing isn't just roleplaying?[BLOCK][CLEAR]")
end