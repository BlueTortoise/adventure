--[Backers]
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRepressed")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	WD_SetProperty("Append", "Christine:[E|Neutral] Sheesh, this one is of a crime wave I experienced when I was a teenager.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Going to a private school with rich girls means nobody steals something because they need it, but to send a message.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] It was the end of the school day.[SOFTBLOCK] My school had a bussing system set up that ran between the dorms and the school, for all the foreign students.[SOFTBLOCK] Normally I took a private car, but that day I had to take the bus.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I was talking with my friend Sinpai and we had some other friends on the second floor.[SOFTBLOCK] The bus would be by in a few minutes and they hadn't come downstairs yet.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I left my bag with Sinpai to see what was taking them so long.[SOFTBLOCK] I went upstairs and found them hanging out at the balcony.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Durbin, Upton, and Aaaac were just goofing off.[SOFTBLOCK] I chided them for a bit.[SOFTBLOCK] The bus was late.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Sinpai comes up and we start chatting.[SOFTBLOCK] After a few minutes I ask 'why aren't you watching the bags' and she just shrugged.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] So I go back downstairs.[SOFTBLOCK] My bag is gone.[SOFTBLOCK] Just mine, nobody elses.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] My Gamequipment 3D 'MJS' was in that bag, along with a bunch of...[SOFTBLOCK] fetishy art...[SOFTBLOCK] from my favourite artists, like Sudoku and Abrissgurke.[SOFTBLOCK] I was into some weird stuff.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Like handholding and legitimate affection.[SOFTBLOCK] I was such a kinky kid.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Years later, I had a boyfriend.[SOFTBLOCK] Kumquat Edelburg, god he was so buff...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] He gave me a birthday present of a new Gamequipment 3D.[SOFTBLOCK] I had never replaced it, but there were so many games I had wanted to play, like Squid Squad Mobile and Rip'N'Tear 2016.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] A week later, we broke up.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I found out that he thought I didn't like the gift despite the fact that I played it constantly.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Then my new Gamequipment 3D 'MJS' got stolen, and we all knew he had done it.[SOFTBLOCK] He tried to pretend it was Klaysee, the girl who hangs out at a cafe we would go to sometimes.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] I had a few dates with her, actually.[SOFTBLOCK] I had a phase where I was into quiet girls.[SOFTBLOCK] I wonder what happened to her?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] And then I had a Gamequipment Lite which got stolen three weeks later while I was at a gaming club.[SOFTBLOCK] I went to the bathroom and came back and it was gone.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] That one I did report to the police.[SOFTBLOCK] Officer Steven, I remember he was the one who took my statement.[SOFTBLOCK] He was so buff.[SOFTBLOCK] I was a 16-year-old girl. If I had been older...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Okay I definitely have a thing for muscled guys.[SOFTBLOCK] That settles that.[SOFTBLOCK] Girls are better, but I will accept a built guy.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] RepeatedMeme had just gotten me a copy of Murder Fist, and I had already put 90 hours into it, so all that progress was lost when it was stolen.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] From then on, no mobile consoles for me.[SOFTBLOCK] I would just have Lew, Dyamonde, and Taedas over and we'd play Brawl Brothers.[SOFTBLOCK] I guess that's how I got so good at it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Christine:[E|Neutral] Maybe I repressed the memory because, well, when people steal, they send a message.[SOFTBLOCK] I had a lot of enemies at that school...[BLOCK][CLEAR]")

end