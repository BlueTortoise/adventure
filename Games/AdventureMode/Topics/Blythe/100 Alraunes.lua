--[Alraunes]
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a human...
	if(sMeiForm ~= "Alraune") then
		WD_SetProperty("Append", "Mei: Are Alraunes very common around here?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Quite. You'll find them in most forested areas.[SOFTBLOCK] I've heard there are a different breed found in jungles, but I've never seen them myself.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: They claim to be non-violent, but that rule has plenty of exceptions, it seems.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: You're fine with them working at the trading post?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: You're referring to Nadia and Florentina?[SOFTBLOCK] Anyone who follows the rules is welcome here.[SOFTBLOCK] We do business with the werecats, too, when they can behave themselves.[BLOCK][CLEAR]")
	
	--If Mei is an Alraune
	else
		WD_SetProperty("Append", "Mei: You don't seem to be uncomfortable around Alraunes.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: There are plenty of monsters on Pandemonium.[SOFTBLOCK] Alraunes are among the more agreeable of them.[SOFTBLOCK] Sometimes.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: Sometimes?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Hatred knows many forms.[SOFTBLOCK] Humans aren't any better or worse, but Alraunes seem to think they're above it.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: If all humans became Alraunes...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Yeah, sure.[SOFTBLOCK] And if all Alraunes just died, then humans wouldn't fight them anymore.[SOFTBLOCK] Nothing is more peaceful than a graveyard.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: A fair point, and well made.[BLOCK][CLEAR]")
	
		--Topic unlocks.
		WD_SetProperty("Unlock Topic", "Pandemonium", 1)
	end
end