--[Bees]
--Asking this NPC about Bee Girls in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-bee...
	if(sMeiForm ~= "Bee") then
		WD_SetProperty("Append", "Mei: Any tips if I run into a bee girl out in the forest?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: They're agile and have a weak poison in their stingers.[SOFTBLOCK] They're not very tough but they tend to attack in groups.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: If you can, isolate one and take it out before its friends arrive to help.[BLOCK][CLEAR]")
		
	--If Mei is a bee...
	else
		WD_SetProperty("Append", "Mei: My hive has a lot of respect for you, you know.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: They know of me?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: Possibly.[SOFTBLOCK] All humans look the same, sometimes.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: You're very disciplined, which is something we value.[SOFTBLOCK] If you ever wanted a change of career...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I'll have to be turning down that offer.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: We expected that, but my sisters will still try to convince you.[BLOCK][CLEAR]")
	
	end
end