--[Cultists]
--Asking this NPC about the spooky cultists in the mansion.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Variables.
local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	if(iCompletedTrapDungeon == 0.0) then
		WD_SetProperty("Append", "Mei:[E|Neutral] Do you know anything about the cultists in the mansion near here?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: A scout said he saw movement in that old heap, but we never followed up on it.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: It's a very old building and there's a lot of myths about it.[SOFTBLOCK] I don't know if its some abandoned estate or if it really is some ancient ruin from the first age.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Still, cultists very rarely are peaceful.[SOFTBLOCK] Do you know how many there were?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] Lots.[SOFTBLOCK] Dozens.[SOFTBLOCK] Probably more, I wasn't exactly counting.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: This does not bode well.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Thank you for the report.[SOFTBLOCK] I'll make sure my patrols are briefed.[BLOCK][CLEAR]")
	
	--Completed trap dungeon.
	else
		WD_SetProperty("Append", "Mei:[E|Neutral] Blythe...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Yes Miss Mei, I've heard about the situation at the mansion.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] How?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Nadia has been relaying reports.[SOFTBLOCK] The Alraunes are not interested in our help, but apparently the plants are.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Happy] Seems they have their heads on straight.[SOFTBLOCK] Metaphorically.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] So you know what happened?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I know enough.[SOFTBLOCK] If you had requested support, we'd have at least dispatched a scouting team.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I'm not certain the cultists are a major threat, at least not until I can verify what I'd been hearing.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I've tried to talk Rochea into an alliance...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: It is appreciated, though I doubt it will bear fruit.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: She may not be willing to admit it, but she's still just as proud as any human is.[SOFTBLOCK] She won't accept help until she's backed into a corner.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Happy] Which is fine with me.[SOFTBLOCK] Let them fight it out.[SOFTBLOCK] The mercs here can clean up the mess.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] You don't fully grasp the nature of what's going on here...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, I don't.[SOFTBLOCK] But you do, don't you?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: In any case, there is potential for an alliance.[SOFTBLOCK] Yet, if the threat is of the nature you're suggesting, we will need to prepare to go it alone.[SOFTBLOCK] I will see to the preparations.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Thank you for your assisstance, Mei.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Sad] Yeah...[BLOCK][CLEAR]")
	
	end
end