--[Dungeon]
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
	
	--Dialogue.
	WD_SetProperty("Append", "Mei:[E|Sad] Blythe, the cultists summoned...[SOFTBLOCK] something...[SOFTBLOCK] in the basement under the Dimensional Trap.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] It was big and extremely dangerous.[SOFTBLOCK] If they summon more, the Trading Post could be in big trouble.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: An animal of some sort?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] No, that is not how I'd put it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] It was an atrocity.[SOFTBLOCK] It had metal grafted onto its skin and its innards sloshed out of its mouth...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: Most distressing...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] They can be wounded, I'll say that much.[SOFTBLOCK] Killed, I have no idea...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] You should form an alliance with the Alraunes![SOFTBLOCK] They've no love for the cultists, either![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: I'm not sure they'd take the offer.[SOFTBLOCK] What few dealings we've had in the past usually ended with threats.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] You might not have that choice this time.[SOFTBLOCK]  Please, try.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: Their solution is always the same one.[SOFTBLOCK] Every disagreement can be solved by them turning everyone here into Alraunes.[BLOCK][CLEAR]")
	if(iHasAlrauneForm == 0.0) then
		WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, they're kind of awful at negotiating.[BLOCK][CLEAR]")
	else
		WD_SetProperty("Append", "Mei:[E|Blush] Well, there's a certain logic to that...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] Shut up,[SOFTBLOCK] Mei.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] Jeez...[BLOCK][CLEAR]")
	end
	WD_SetProperty("Append", "Blythe: Still, if this threat is as dangerous as you say it is, they'll be willing to talk at the very least.[SOFTBLOCK] I'll put together an envoy.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] See?[SOFTBLOCK] This is why he gets the big bucks.[BLOCK][CLEAR]")
end