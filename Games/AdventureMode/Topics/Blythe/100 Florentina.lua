--[Florentina]
--Asking this NPC about Florentina.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei: Has Florentina been here very long?[BLOCK][CLEAR]")
	
	--If Florentina is not present...
	if(bIsFlorentinaInParty == false) then
		WD_SetProperty("Append", "Blythe: She's been at the post for a few years now.[SOFTBLOCK] I get a lot of complaints about her.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: Oh no.[SOFTBLOCK] What did she do?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I believe she counts cards.[SOFTBLOCK] Very good at it, too.[SOFTBLOCK] Don't play a game of chance with her.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: I'll keep that in mind.[BLOCK][CLEAR]")
		
	--If Florentina is present...
	else
		WD_SetProperty("Append", "Blythe: She's right there, why don't you ask her yourself?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina: Uh, I think I've forgotten.[SOFTBLOCK] I'm not as meticulous as you are, Darius.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: This is why we have paperwork, Florentina.[SOFTBLOCK] Writing can remember things longer than minds.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina: Conveniently, writing also allows nosy to people to remember things they have no business remembering.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: Sorry if I touched a nerve.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina: I like to be unmoored, is all.[SOFTBLOCK] I've been here at least five summers, if I recall correctly.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Fortunately I have your records here.[SOFTBLOCK] You are correct, you came here five years ago.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina: See?[SOFTBLOCK] I was right![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: You're both right.[SOFTBLOCK] Heh.[BLOCK][CLEAR]")
	end
end