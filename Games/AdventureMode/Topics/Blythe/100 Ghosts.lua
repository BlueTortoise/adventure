--[Ghosts]
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-ghost:
	if(sMeiForm ~= "Ghost") then
		WD_SetProperty("Append", "Mei:[E|Neutral] Were you aware of the ghosts in the Quantir manor?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: No.[SOFTBLOCK] It's only recently that I've even heard the place is open.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] It wasn't open?[SOFTBLOCK] Nobody has tried to enter it?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Oh, we get our fair share of treasure hunters here.[SOFTBLOCK] They've tried, but they failed.[SOFTBLOCK] The windows wouldn't break, and tunneling in didn't work either.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Of course, that just caused more treasure hunters to come.[SOFTBLOCK] They failed, too.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I suppose the emergence of the ghosts is a response to the building being infiltrated.[SOFTBLOCK] I recall a religious convent that came through here being very interested in the building.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Hmmm...[BLOCK][CLEAR]")
	
	--If Mei is an Alraune
	else
		WD_SetProperty("Append", "Mei:[E|Neutral] Were you aware of the ghosts in the Quantir manor?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: No, but I assume you are.[SOFTBLOCK] Perhaps you could shed some light on the situation?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] It's quite complicated.[SOFTBLOCK] The other ghosts seem to think there's a sickness in Quantir, in the present-tense.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Ah, the plague.[SOFTBLOCK] I remember reading about it some time back.[SOFTBLOCK] It was over two centuries ago.[SOFTBLOCK] The exact date was lost, as the entire population of Quantir succumbed.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Sad] We -[SOFTBLOCK] I mean, they -[SOFTBLOCK] seemed to think I was sick.[SOFTBLOCK] Until I got...[SOFTBLOCK] chained...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Perhaps the presence of humans in the manor made the ghosts wake up?[SOFTBLOCK] That'd be my guess.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Still, this is a security issue.[SOFTBLOCK] I'll have to have notices put up.[SOFTBLOCK] Should I have the main entrance barred?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] You can try, but I can't say whether or not the other maids will remove them.[SOFTBLOCK] It might be a good idea to have Nadia do it, as they seem to not notice Alraunes.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: That's very helpful.[SOFTBLOCK] I wouldn't want to put anyone at risk.[SOFTBLOCK] Thank you, Mei.[BLOCK][CLEAR]")
	
	end
end