--[Name]
--Asking this NPC about their job. A lot of NPCs have this.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common code.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei: What is it that you do here, Captain Blythe?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: Please, you can call me Darius if you like.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: I make sure the guards here do their jobs properly and deal with the disputes that come up whenever Florentina has to haggle with someone.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: I used to do logistics, but I've let the merchants take over for that.[SOFTBLOCK] It's their coin, after all.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: Do the merchants pay you?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: They pay a fee, but most of our funding comes from the shipping companies that go through here.[SOFTBLOCK] They need safe roadways to make money, after all.[BLOCK][CLEAR]")
end