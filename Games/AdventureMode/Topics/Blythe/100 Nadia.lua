--[Nadia]
--Asking this NPC about Nadia.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Dialogue.
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei: Nadia seems...[SOFTBLOCK] different...[SOFTBLOCK] from the other guards.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: I take it your comment is more substantive than her skin's tone.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: The jokes...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: Yes, that is a common topic.[SOFTBLOCK] She's unprofessional, brash, and often rude.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: It's not her fault, though.[SOFTBLOCK] She's learning.[SOFTBLOCK] I think she forgot how to act around humans after she became an Alraune.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: That said, her foraging skills are unparalelled.[SOFTBLOCK] She's quite an asset here at the trading post.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: Mind you, her usefulness is not why she's here.[SOFTBLOCK] She's been through...[SOFTBLOCK] a lot.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: How do you mean?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: Ask her about me.[SOFTBLOCK] Ask her how we met.[SOFTBLOCK] If she can bear it, she'll tell you.[BLOCK][CLEAR]")
end