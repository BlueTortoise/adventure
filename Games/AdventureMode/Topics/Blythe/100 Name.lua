--[Name]
--Asking this NPC about their name. A lot of NPCs have this. Sometimes it's asking them what their name is, sometimes
-- it's asking them what their name means.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Topic unlocks.
	WD_SetProperty("Unlock Topic", "Pandemonium", 1)
	
	--Dialogue.
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei: Blythe.[SOFTBLOCK] Where is that name from?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: I'm from Rondheim, if that's what you're asking.[SOFTBLOCK] It's not a common name, there or anywhere.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: I have no idea where that is.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: I should have expected that, seeing as you might well not be from Pandemonium.[SOFTBLOCK] Still, Blythe is not a name that will command any respect outside of the trading post.[BLOCK][CLEAR]")
end