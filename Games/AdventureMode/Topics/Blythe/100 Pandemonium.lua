--[Pandemonium]
--Asking this NPC about Pandemonium.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Dialogue.
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei: What exactly is Pandemonium?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: It's where you're standing.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: So not Earth, then.[SOFTBLOCK] Am I on another planet?[SOFTBLOCK] In another galaxy?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Blythe: Hm, do you recognize any of the stars?[SOFTBLOCK] Perhaps you're just a long way from Earth.[SOFTBLOCK] Maybe across the ocean?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: No, no.[SOFTBLOCK] On Earth, we have electricity, airplanes, cars, satellites...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: Indoor plumbing...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: Pandemonium must be another planet.[SOFTBLOCK] It has to be.[SOFTBLOCK] And that means there must be a way back to Earth...[BLOCK][CLEAR]")
	
	--Variables.
	local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
	
	--If Florentina is not present...
	if(bIsFlorentinaInParty == false) then
		WD_SetProperty("Append", "Blythe: As I said, speak to Florentina.[SOFTBLOCK] If there is a way, she will know it, or know who knows it.[BLOCK][CLEAR]")
	else
		WD_SetProperty("Append", "Florentina: Relax.[SOFTBLOCK] I know some people who know some people.[SOFTBLOCK] We'll get you back home, you big baby.[BLOCK][CLEAR]")
	end
end