--[Slimes]
--Asking this NPC about slimes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--If Mei is not a slime...
	if(sMeiForm ~= "Slime") then
		WD_SetProperty("Append", "Mei: What should I do to...[SOFTBLOCK] ward off the advances of a slime?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Slimes aren't known for their speed.[SOFTBLOCK] Or their intelligence.[SOFTBLOCK] Or much of anything, really.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: They'll make no effort to dodge you, so take your time and pick your strikes.[SOFTBLOCK] You should be fine if you don't get overwhelmed.[BLOCK][CLEAR]")
	
	--If Mei is a slime...
	else
		WD_SetProperty("Append", "Mei: Can you tell me anything about the slimes in this area?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I would assume you would know better than I.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei: I haven't been a slime very long.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Well, slimes aren't fast.[SOFTBLOCK] They're not intelligent, either.[SOFTBLOCK] Present company excluded, of course.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I believe they're highly resistant to poisons, so that might be something to keep in mind.[BLOCK][CLEAR]")
	
	end
end