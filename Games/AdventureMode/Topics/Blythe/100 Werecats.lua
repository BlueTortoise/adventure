--[Werecats]
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-werecat:
	if(sMeiForm ~= "Werecat") then
		WD_SetProperty("Append", "Mei:[E|Neutral] Do many werecats come to the outpost?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: Few.[SOFTBLOCK] They tend not to have much patience for us.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I certainly don't have much patience for them.[SOFTBLOCK] They're needlessly aggressive.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] But some do come by?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: There are a few werecats who are scavengers.[SOFTBLOCK] Anything they find that they think they can sell, they bring here.[SOFTBLOCK] Florentina usually handles them.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: They also buy medicine sometimes.[SOFTBLOCK] Mostly for wounds that I'd guess were inflicted by other werecats.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] They're pretty dangerous in a fight.[SOFTBLOCK] Got any advice?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: They're extremely fast.[SOFTBLOCK] Try using an attack that's faster, even if it's not as powerful.[SOFTBLOCK] Better to land a weak hit than miss a strong one.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: There are some books around that may help you.[SOFTBLOCK] The researcher's cabin just north of the trading post had a copy of the Fencer's Friend, I think.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Thanks.[SOFTBLOCK] I'll go ask around.[BLOCK][CLEAR]")
	
	--If Mei is a werecat:
	else
		WD_SetProperty("Append", "Mei:[E|Neutral] Do many werecats come to the outpost?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: You're the first one we've had in a few weeks.[SOFTBLOCK] They don't have much need for us.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: They do tend to behave themselves when they're here.[SOFTBLOCK] We don't tolerate violence, and I make it very clear what we do to offenders.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] Hunters, not fools, they are.[SOFTBLOCK] Though I will admit that attacking one another for sport is something we do.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] It's a good way to train the weak kinfangs to be strong, you see.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: We're not kinfangs.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] Heh.[SOFTBLOCK] You would make a good night stalker, Darius.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I've had a few of your kin think the same thing.[SOFTBLOCK] I rebuffed them with the edge of my spear.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] Then that'd make you all the better.[SOFTBLOCK] Surely you see that?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Blythe: I appreciate the compliments, but I'm not interested.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] A shame...[BLOCK][CLEAR]")
	
	end
end