--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasBeeForm     = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
local iCanSpawnJoanie = VM_GetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--[Special]
--If Joanie has not been unlocked and Mei is a bee, special dialogue plays.
if(iCanSpawnJoanie == 0.0 and sMeiForm == "Bee") then
	WD_SetProperty("Append", "Mei:[E|Neutral] Do you have a history with the bees?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Everyone in these parts does.[SOFTBLOCK] You're about as likely to get mauled by a bear as dragged off by a bee.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Hmmm?[SOFTBLOCK] Oh?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] Oh really!?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: That smile is kinda unnervin', Mei.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] You have a friend who's a bee, don't you?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: ...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Had.[SOFTBLOCK] Past tense.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] Well she's telling me she's your friend.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: J-[SOFTBLOCK]Joanie?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] Oh, that was her name?[SOFTBLOCK] We generally don't remember our names, since we don't really need them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] ...[SOFTBLOCK] Yep![SOFTBLOCK] Okay.[SOFTBLOCK] I'll let her know.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: M-[SOFTBLOCK]Mei, I'm about to cry - [BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] Whatever for?[SOFTBLOCK] I let her know you'd like her to visit.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: *sniff*[SOFTBLOCK] Really?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] Of course.[SOFTBLOCK] She's come by a few times but we're very busy, as you might imagine.[SOFTBLOCK] I guess you must have missed her.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Oh, Mei![SOFTBLOCK] I can't believe it![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Thank you so much![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Laugh] Don't mention it![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] But, if you don't want to become a bee yourself...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Better stay near the Pit Stop...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] The hive thinks you'd be a good drone.[SOFTBLOCK] I happen to agree.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Thanks so much, Mei.[SOFTBLOCK] *sniff*[SOFTBLOCK] This means a lot...[BLOCK][CLEAR]")

	VM_SetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N", 1.0)

--[Normal]
else

	--Standard.
	WD_SetProperty("Append", "Mei:[E|Neutral] Do you have a history with the bees?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Everyone in these parts does.[SOFTBLOCK] You're about as likely to get mauled by a bear as dragged off by a bee.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Ain't sure which is worse.[BLOCK][CLEAR]")

	--If Mei is a bee:
	if(sMeiForm == "Bee") then
		WD_SetProperty("Append", "Mei:[E|Neutral] We're very sorry if we've caused you any trouble.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne: Ya don't have to be apologizin'.[SOFTBLOCK] Bees gotta survive too.[SOFTBLOCK] You can't get mad at a beaver for building a dam.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne: Nature's real cruel like that...[BLOCK][CLEAR]")

	--If Mei is not a bee, but does have the bee transformation:
	elseif(iHasBeeForm == 1.0) then
		WD_SetProperty("Append", "Mei:[E|Neutral] It's not so bad.[SOFTBLOCK] It's scary at first, but then...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne: Please don't.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne: I know you're trying to help, but there are some things that ought to be let lie.[BLOCK][CLEAR]")

	--Normal case:
	else
		WD_SetProperty("Append", "Mei:[E|Neutral] Neither sounds very appealing.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne: These'r the risks we take living out in the sticks.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne: I know it ain't the bees' fault, they're just doing what comes natural.[SOFTBLOCK] Gotta forgive and forget, else the pain will gnaw at you.[BLOCK][CLEAR]")
	end
end
