--[Breanne Sign]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
WD_SetProperty("Append", "Mei: What exactly does taff mean?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: You saw my sign, eh?[SOFTBLOCK] I wrote that when I was feelin' particularly off.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Taff is not a very nice word.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: I could guess from the context.[SOFTBLOCK] Do you have a problem with your parents?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: I love 'em to bits, really.[SOFTBLOCK] They just need to stop intruding and let me live my life.[BLOCK][CLEAR]")

--Topic unlock.
WD_SetProperty("Unlock Topic", "Parents", 1)