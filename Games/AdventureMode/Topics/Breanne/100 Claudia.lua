--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
if(iSavedClaudia == 0.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] Could you tell me anything about Sister Claudia?[SOFTBLOCK] Anything that might help find her?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] Cultist, totally.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Come again?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] Well that wasn't very polite of me.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] I was abducted by cultists![SOFTBLOCK] Now I'm trying to track one down?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] Claudia?[SOFTBLOCK] No way![SOFTBLOCK] She wouldn't hurt a fly.[SOFTBLOCK] She's the purest, goodest person in the world.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] It's so nauseating![BLOCK][CLEAR]")

	--If Florentina is present, this aside happens.
	if(bIsFlorentinaPresent) then
		WD_SetProperty("Append", "Florentina:[E|Happy] Now you know how I feel all the time.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne:[E|Neutral] Oh shush, you.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne:[E|Neutral] As I was saying, Claudia is all peace and love, all the time.[BLOCK][CLEAR]")
	else
		WD_SetProperty("Append", "Breanne:[E|Neutral] Claudia is all peace and love, all the time.[BLOCK][CLEAR]")
	end

	--Resume.
	WD_SetProperty("Append", "Breanne:[E|Neutral] She said she was on a holy mission to study the monster girls in Trannadar.[SOFTBLOCK] If she's on a holy mission, you better believe she's going to complete it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Is she a dangerous fanatic?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] If you find her, she'll shower you with pamphlets.[SOFTBLOCK] Saying no doesn't work.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] They make good kindling...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Hm.[SOFTBLOCK] Does her group have a name?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] Uh, crud.[SOFTBLOCK] I think it was...[SOFTBLOCK] Heavenly Doves?[SOFTBLOCK] Does that ring a bell?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] No, but I wasn't expecting it to.[SOFTBLOCK] I just wanted to know in case I find something she left behind.[SOFTBLOCK] Heavenly Doves, eh?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] Sorry I couldn't be more help.[BLOCK][CLEAR]")

	--If Florentina is present, this aside happens.
	if(bIsFlorentinaPresent) then
		WD_SetProperty("Append", "Florentina:[E|Neutral] Her gear has pictures of birds on it.[SOFTBLOCK] That's a pretty big tell.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] If we find a journal with a dove on it, that's hers.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne:[E|Neutral] Florentina, don't you dare rob that poor Claudia![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Happy] I was just thinking that if she had lost it, we might recognize it.[SOFTBLOCK] But now you've got me thinking...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Breanne:[E|Neutral] Don't![BLOCK][CLEAR]")
	end

--Claudia is rescued.
else
	WD_SetProperty("Append", "Breanne:[E|Neutral] Oh, Mei![SOFTBLOCK] Claudia came by again![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] She said she has you to thank for that![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well I just helped her out of a bind, is all.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] You're making the world a better place, hun![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne:[E|Neutral] She was on her way over to Outland if you want to catch up with her.[BLOCK][CLEAR]")

end