--[Florentina]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--If Florentina is not around:
if(bIsFlorentinaPresent == false) then
	WD_SetProperty("Append", "Mei: Who's Florentina?[SOFTBLOCK] What's she like?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: She's an Alraune who runs the supply shop over at the tradin' post.[SOFTBLOCK] Brusque sort.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Don't let it go foolin' you.[SOFTBLOCK] She's got her reasons for bein' the way she is, but nobody's cruel like that all the time.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: I think somethin' bad happened to her and she ain't lookin' for companionship.[SOFTBLOCK] All of this is rumours, mind.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: Have you had a lot of business dealings with her?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Nothing consistent.[SOFTBLOCK] If I get an excess of something, she can usually sell it off.[SOFTBLOCK] If she needs something...[SOFTBLOCK] disappeared...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: You're not doing anything illegal are you?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Law ain't the same here as it is down east, stranger![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: I'm not from down east, but I get what you mean.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: There's a real divide between what's moral and what's legal.[SOFTBLOCK] I try to stay on the side of the former, damn the latter.[BLOCK][CLEAR]")

--If Florentina is around:
else
	WD_SetProperty("Append", "Mei: So, Florentina, are you and Breanne friends?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina: No.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: I had a feeling you'd say that.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Do you believe her?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: Not really. People who don't like each other don't come up with clever insults for one another.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina: Astute observation, you cheese-eyed gibberer.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei: Thank you, sock-sniffing maple-butt.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: Aww, you two get along so well.[SOFTBLOCK] I'm a little jealous.[BLOCK][CLEAR]")

end