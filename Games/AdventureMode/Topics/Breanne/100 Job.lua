--[Job]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Flags.
VM_SetVar("Root/Variables/Chapter1/Breanne/iTalkedJob", "N", 1.0)

--Standard.
WD_SetProperty("Append", "Mei: Can you tell me a bit more about your daily routine?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Aren't you just a sweetie?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: I am?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: I get a lot of visitors, but they ain't asking me what I do.[SOFTBLOCK] You're either real nice or real inquisitive.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Can't it be both?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Sure can![BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Welp, this here Pit Stop was made by these two hands.[SOFTBLOCK] I hired a couple labourers from the trading post a few years back, but I did all the planning and a lot of the work myself.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Chopped the trees, built the fences, that sort of thing.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: The shed looks newer.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: I built that last winter.[SOFTBLOCK] Unfortunately it gets real dusty, but the extra storage space is nice.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: I also do the cooking, sewing, cleaning...[SOFTBLOCK] If the lakeside is quiet, I go fishing sometimes too.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Oh, and I read.[SOFTBLOCK] Travellers leave books they're done with, so I've built up a bit of a library.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: But you're the only one here?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Nope![SOFTBLOCK] My friend Mei is here right now.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Aww...[BLOCK][CLEAR]")