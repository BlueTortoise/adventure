--[Nadia]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
WD_SetProperty("Append", "Mei: So, Nadia.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: She's an Alraune from the trading post.[SOFTBLOCK] She tells jokes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Lots and lots of jokes.[SOFTBLOCK] Bad jokes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: You don't like them?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Of course I like them![SOFTBLOCK] Puns are better the worse they are.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: It's just like snoo.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: ...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Snoo?[SOFTBLOCK] What's snoo?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Not much.[SOFTBLOCK] What's snoo with you?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: ...[SOFTBLOCK] What does an anyuerism feel like...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Haha![SOFTBLOCK] Nadia told me that one![SOFTBLOCK] It's *awful* isn't it?[BLOCK][CLEAR]")