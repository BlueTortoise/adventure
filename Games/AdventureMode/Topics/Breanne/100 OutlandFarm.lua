--[Outland Farm]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
WD_SetProperty("Append", "Mei: Outland Farm?[SOFTBLOCK] A farm in a place swarming with monsters?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: You've come at a time when monster activity is a lot higher than usual.[SOFTBLOCK] This is breeding season.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Outland just hires a few more guards for a few months.[SOFTBLOCK] Things calm down a lot once the rains set in.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: So it's not always like this?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Not always, no.[SOFTBLOCK] If you *really* want to find a wild Alraune, you can.[SOFTBLOCK] Right now, they're awful agitated.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Dunno why, and not concerned, neither.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: If you're looking for some spare platina, I bet Outland is hiring.[SOFTBLOCK] You and that sword look decent in a scrap.[BLOCK][CLEAR]")

--Mei is using the Rusty Katana.
if(AdInv_GetProperty("Is Item Equipped", "Mei", "Rusty Katana") == true or AdInv_GetProperty("Is Item Equipped", "Mei", "Rusty Katana (+1)") or AdInv_GetProperty("Is Item Equipped", "Mei", "Rusty Katana (+2)")) then
	WD_SetProperty("Append", "Mei: This rusty thing?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Breanne: It's the user, not the weapon. But, you should consider getting a better one.[SOFTBLOCK] I think the equipment shop at the trading post has swords for sale.[BLOCK][CLEAR]")

--Steel Katana.
else
	WD_SetProperty("Append", "Mei: This thing?[SOFTBLOCK] I guess it looks pretty nifty.[BLOCK][CLEAR]")
end

--Resume.
WD_SetProperty("Append", "Breanne: You hold it naturally.[SOFTBLOCK] Most folks need to be taught to wield one properly, but you seem like you were born with it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: I watch some anime, I guess.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Ani-what?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Uh, it's like moving pictures.[SOFTBLOCK] We have them on Earth.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: It ain't something you learn by watching.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: You hold yourself like a trained warrior.[SOFTBLOCK] Trust me, I know a lot of them.[SOFTBLOCK] You've got all the makings.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Well, thanks for that.[SOFTBLOCK] I'm not looking for a job, but who knows?[BLOCK][CLEAR]")