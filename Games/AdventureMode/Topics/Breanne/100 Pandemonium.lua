--[Pandemonium]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
WD_SetProperty("Append", "Mei: I'm from Earth.[SOFTBLOCK] This is Pandemonium?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Yep![SOFTBLOCK] Are you sure your word for Pandemonium isn't just Earth?[SOFTBLOCK] Languages are like that.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: How many planets are there in your solar system?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Planets?[SOFTBLOCK] Well, there's Regulus, which is the moon.[SOFTBLOCK] There's Cenatroid, Meracoid, Ossiline, and Tenebree.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Did I get them all?[SOFTBLOCK] I don't have a lot of books on astronomy.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: You see, on Earth we have seven other planets.[SOFTBLOCK] Eight if you count Pluto, but only people desperate to look smart do that.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Huh.[SOFTBLOCK] Can't argue with that.[SOFTBLOCK] I guess you really are far from home.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: But, hey, home is where your heart is.[SOFTBLOCK] If you can't find a way back to Earth, you're welcome to stay here.[SOFTBLOCK] Could always use another set of hands.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: I'm not giving up just yet, but thank you for the offer.[BLOCK][CLEAR]")