--[Parents]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Set this flag.
VM_SetVar("Root/Variables/Chapter1/Breanne/iMeiKnowsAboutSuitors", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Breanne/iTalkedParents", "N", 1.0)

--Standard.
WD_SetProperty("Append", "Mei: Your parents are overbearing, huh?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: I don't know if I should be talkin' about that...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: I can relate.[SOFTBLOCK] My parents are always breathing down my neck.[SOFTBLOCK] Or they were, anyway.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Yeah![SOFTBLOCK] I'm a grown woman, I can make my own decisions![BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: And I don't need someone harping at me to get a promotion.[SOFTBLOCK] I wait tables for a living, I don't want to do that the rest of my life![BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Exactly![BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: I guess you don't live in the same town as yours.[SOFTBLOCK] Do they send you letters?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: They send me suitors.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Suitors? I guess that explains the sign.[SOFTBLOCK] They want you to get married?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Mom and Dad want me to be happy, really.[SOFTBLOCK] That also means they want their little girl to come home and have grandkids.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: So, they send up anyone they think I would like.[SOFTBLOCK] Their ideas of what I like are way off.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Heheh. Mine sent me job openings clipped from a newspaper.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: It's about what they want, but they always cloaked in the language of what they want for you.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Can't they just leave us alone?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: It's love.[SOFTBLOCK] I don't think they're selfish, maybe they can't separate what they want for us from what they want for themselves.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: When you raise someone, you think of yourselves as one person.[SOFTBLOCK] They just can't let go.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: ...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: That's proper smart, Mei.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Doesn't change that I don't want people showing up here asking for my hand.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: But you'll put up with it, right?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Don't got the option.[SOFTBLOCK] At least it makes more sense now.[BLOCK][CLEAR]")