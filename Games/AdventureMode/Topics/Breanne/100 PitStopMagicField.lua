--[Pit Stop Magic Field]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
WD_SetProperty("Append", "Mei: How does the magic field here work?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: If I knew, I'd be the greatest mage in Pandemonium.[SOFTBLOCK] I haven't the foggiest.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: It's been here for a very, very long time.[SOFTBLOCK] Possibly as long as humans have existed, maybe longer.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Had a lady come through, said she was a mage.[SOFTBLOCK] She thought it was fey magic.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Is it natural?[SOFTBLOCK] Did someone set it up?[SOFTBLOCK] I'm afraid I don't know how magic works, we don't have it on Earth.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Huh, you know, I never thought it might have been deliberate.[SOFTBLOCK] I always figured it was just the way it was.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Why would someone go to the lengths of making a magic field that scares off people who want to fight?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Why not?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: If you can make a magic field like that, you can probably blow up a building with a thought.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Maybe they wanted to make a safe place to rest?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Hey![SOFTBLOCK] This place has been a Pit Stop for thousands of years![BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: I'm continuing a grand tradition![BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: You don't have to go that far.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Shh, it's fun.[SOFTBLOCK] Let me have my fun.[BLOCK][CLEAR]")

--Topic unlock.
WD_SetProperty("Unlock Topic", "Pandemonium", 1)