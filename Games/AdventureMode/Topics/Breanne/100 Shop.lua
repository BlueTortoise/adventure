--[Let's Go Shopping!]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
WD_SetProperty("Append", "Breanne: What can I get for ya?")

--Setup.
local sBasePath = gsRoot .. "CharacterDialogue/Breanne/"
local sString = "AM_SetShopProperty(\"Show\", \"Breanne's Pit Stop\", \"" .. sBasePath .. "Shop Setup.lua\", \"Null\")"

--Run the shop.
fnCutsceneInstruction(sString)
fnCutsceneBlocker()