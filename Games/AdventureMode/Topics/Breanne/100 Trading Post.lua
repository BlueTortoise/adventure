--[Trading Post]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
WD_SetProperty("Append", "Mei: Do you go to the trading post often?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: It's been a while, since they usually deliver to me and not t'other way 'round.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: I do make visits sometimes.[SOFTBLOCK] It's pretty much the biggest settlement in Trannadar.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei: Trannadar?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: That's the province we're in, hun.[SOFTBLOCK] Quantir is northeast of here.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: Province is a bit of a misnomer, though.[SOFTBLOCK] More like unadministered wild territory.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Breanne: There's probably some noble someplace who claims title to Trannadar or somesuch, but that don't amount to a hill of beans out here.[BLOCK][CLEAR]")