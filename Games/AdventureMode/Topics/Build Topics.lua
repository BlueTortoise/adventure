--[Build Topics]
--At AdventureMode startup, builds the topic listing.
local sBasePath = fnResolvePath()
local saList = {"2855", "2855Biolabs", "300910", "Backers", "Backers5", "BackersRaiju", "BackersRepressed", "BackersManufactory", "Blythe", "Breanne", "ChristineTerminal", "CombatBooks", "EvermoonNWBookshelf", "Florentina", "Karina", "Nadia", "Rochea", "Septima", "Sophie", "SX399", "PipeNightmare", "ZGeneral"}

--Debug: If this is not nil, a list of all unique topics will be printed.
--gsaUniqueTopics = {}

--Now set as necessary.
local i = 1
while(saList[i] ~= nil) do
	LM_ExecuteScript(sBasePath .. saList[i] .. "/000 Build Listing.lua")
	i = i + 1
end

--Once the topic listing is done, get a list of all unique topics. Write them to the console.
if(gsaUniqueTopics ~= nil) then
	local i = 1
	while(gsaUniqueTopics[i] ~= nil) do
		io.write("Topic " .. i - 1 .. ": " .. gsaUniqueTopics[i] .. "\n")
		i = i + 1
	end
end
	