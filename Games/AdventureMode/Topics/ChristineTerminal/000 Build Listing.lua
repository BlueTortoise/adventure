--[Build Listing]
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "ChristineTerminal"

--Topic listing.
fnConstructTopic("Arcane Academy", "Arcane Academy", 1, sNPCName, 0)
fnConstructTopic("Breeding Program", "Breeding Program", 1, sNPCName, 0)
fnConstructTopic("Cause of Science", "Cause of Science", 1, sNPCName, 0)
fnConstructTopic("Central Administration", "Central Administration", 1, sNPCName, 0)
fnConstructTopic("Dolls", "Dolls", 1, sNPCName, 0)
fnConstructTopic("Golems", "Golems", 1, sNPCName, 0)
fnConstructTopic("Regulus City", "Regulus City", 1, sNPCName, 0)
fnConstructTopic("Steam Droids", "Steam Droids", 1, sNPCName, 0)
