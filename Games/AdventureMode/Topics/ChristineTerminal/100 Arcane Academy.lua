--[The Arcane Academy]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
WD_SetProperty("Append", "Thought: (The Regulus Arcane Academy has its origins shrouded in mystery, with much of its history lost to time.[SOFTBLOCK] Legend has it that a master of teleportation, frustrated by the feudal lords of Pandemonium, sought a place to study uninterrupted.[SOFTBLOCK] The moon was a sensible choice.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (From historical documents uncovered by archealogical research, we can say with confidence that the Academy was founded on the principles of higher education without boundaries.[SOFTBLOCK] The only entrance exam was finding it and getting to it on one's own.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (After many years, sorcerers began to move their families and support staff to the campus.[SOFTBLOCK] Magic alone was not sufficient to provide for their daily needs, and rather than risk creating a complex supply chain, the first habitation domes were created.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Many of the habitation domes created then are in the same locations as the Biological Research Labs today, though obviously the buildings have all been remodelled.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (The Academy prided itself on the Cause of Science::[SOFTBLOCK] a clear-headed, politics-free pursuit of the truth.[SOFTBLOCK] It is the same Cause we serve today.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Research into previously forbidden subjects, such as Partirhuman anatomy and Alchemical Transmutation, flourished at the Academy.[SOFTBLOCK] It is from this research that the Steam Droids and, eventually, modern Golems would emerge.)[BLOCK][CLEAR]")
