--[They're Called Command Units, Damn It]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
WD_SetProperty("Append", "Thought: (Considered to be the pinnacle of synthetic partirhuman technology, the Command Unit serves as the leadership caste of Regulus City.[SOFTBLOCK] They, and a select few Lord Units, are responsible for the city's security and progress.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (The design itself is secret, largely out of security concerns.[SOFTBLOCK] Command Units are stronger, smarter, faster, and in every way superior to the Golem series, but the expense of their conversion prohibits mass production.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Command Units are frequently nicknamed 'Dolls' out of a misunderstanding of their anatomy.[SOFTBLOCK] Their jointed frames are similar to the ball joints of conventional toy dolls of Pandemonium tradition.[SOFTBLOCK] These joints are actually superconducting selective-adhesion modules, but the superficial resemblence gave them the nickname.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Among the Command Units are a small cadre called the Prime Command Units.[SOFTBLOCK] These Command Units are authorized to make decisions in absence of Central Administration's input, and serve as extensions of Regulus City's will.[SOFTBLOCK] Their word is absolute.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (If you are in the presence of a Command Unit, show her the utmost respect and deference.[SOFTBLOCK] Their jobs are extremely difficult and necessary for the success of Regulus City, and thus you must do all within your power to accomodate them.)[BLOCK][CLEAR]")

--Topics
WD_SetProperty("Unlock Topic", "Central Administration", 1)