--[Anatomy of Golems]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
WD_SetProperty("Append", "Thought: (The Golem series of partirhuman consists of several subsets, including Slave Unit, Lord Unit, and Drone Unit.[SOFTBLOCK] While varying greatly in appearance and capability, they are in fact very similar internally.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Based on now-lost schematics for Steam Droids, the Golem improves upon them in every way.[SOFTBLOCK] Advances in computers, semiconductor miniaturization, and materials science have produced living machines that can operate in any environment with minimal support for long periods.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Lowest amongst the Golems is the Drone Unit.[SOFTBLOCK] Clad in a synthetic skin similar to rubber, these units are meant for labour in dangerous conditions.[SOFTBLOCK] They are also favoured for security units, as their chassis is easily repaired after receiving damage.[SOFTBLOCK] However, they tend to be very unintelligent.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Next is the Slave Unit.[SOFTBLOCK] These are the bulk of Regulus City's workers, constituting some 95 percent of the current working population.[SOFTBLOCK] They perform tasks ranging from manufacture, repair, survey, security, and cleaning to research and development.[SOFTBLOCK] Despite their name, Slave Units are independent, intelligent, and capable machines.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Atop the hierarchy is the Lord Unit.[SOFTBLOCK] These Units are primarily used for administrative duties and research, making sure Regulus City continues functioning optimally.[SOFTBLOCK] Due to the difficulties of their work, only individuals of exceptional intelligence and leadership are selected to become Lord Units.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (The Golem models are highly modular and can be modified to become another model with part replacement.[SOFTBLOCK] It is unlikely that another, more advanced form of partirhuman will be necessary considering the modularity of the form.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Constant scientific advances have improved modern Golems far beyond their first iterations.[SOFTBLOCK] To take one example, Golem power cores are capable of uninterrupted function for 144 hours without recharging::[SOFTBLOCK] A major improvement over the original 48 hours!)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Despite claims otherwise, the Command Unit is not of the same series of designs as Golems, and their parts are not compatible.[SOFTBLOCK] Please see the Command Unit page for more information.)[BLOCK][CLEAR]")

--Topics
WD_SetProperty("Unlock Topic", "Dolls", 1)