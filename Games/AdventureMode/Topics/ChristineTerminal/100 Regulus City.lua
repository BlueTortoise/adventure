--[History of Regulus City]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
WD_SetProperty("Append", "Thought: (Regulus City is the height of technological accomplishment and a beacon of progress for the struggling people of the Belarus star system.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Officially founded in Common Year 3315, the origins of Regulus City can be traced back centuries, to the first brick laid in the construction of Regulus Arcane Academy.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Founded by a reclusive sorcerer who considered him or herself to be above the petty politicking of Pandemonium, the Arcane Academy promised to be a place of higher learning outside the reach of monarchs and madmen.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (It is from this simple, humble origin that the Cause of Science was founded, to which we commit ourselves every day.)[BLOCK][CLEAR]")

--Topics
WD_SetProperty("Unlock Topic", "Arcane Academy", 1)
WD_SetProperty("Unlock Topic", "Cause of Science", 1)