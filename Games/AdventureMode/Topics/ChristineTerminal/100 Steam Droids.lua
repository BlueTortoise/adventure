--[Anatomy of Steam Droids]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
WD_SetProperty("Append", "Thought: (The first synthetically generated partirhuman species, Mechanicum Gynoidium (often called the Steam Droid, somewhat erroneously) was designed and built to overcome the obvious environmental hostility present on Regulus.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Many of the original documents that were involved in the production of the first Steam Droid have been lost due to simple decay or poor record keeping.[SOFTBLOCK] What is known is that the design was meant to circumvent common issues of mortality::[SOFTBLOCK] Disease, aging, physical frailty.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (The Steam Droid has a primitive power system, very advanced for its time, but totally obsoleted by modern power cores.[SOFTBLOCK] The Droid stores steam in high-pressure magically sealed tanks, and releases it to perform work.[SOFTBLOCK] Recharging a Steam Droid consists of refilling its steam supplies.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Naturally, corrosion and leaks are serious problems with the design.[SOFTBLOCK] Despite its many flaws, the prospect of eternal life was so alluring to the mages of Regulus that the entire Arcane Academy soon converted themselves to Steam Droids.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Further improvements to the design eventually led to the creation of the modern Golem design and its many variants.[SOFTBLOCK] The Command Unit 'Doll' design is a separate project and unrelated to the Steam Droid production series.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (The Steam Droid design is currently obsoleted and no further conversions are undertaken.[SOFTBLOCK] Older Steam Droids have largely been upgraded to the Golem model when possible.[SOFTBLOCK] Unfortunately, those who physically incompatible with the upgrade were left behind.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (Unable to adjust to life in Regulus City, the remaining Steam Droids rejected the Cause of Science and left.[SOFTBLOCK] Considering their state of decay, it is unlikely any have survived.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "Thought: (If you sight a Steam Droid unit, it is ideal to report it to security for capture and study.[SOFTBLOCK] Functioning Steam Droids may have novel engineering improvements to their systems which could serve the Cause of Science.[SOFTBLOCK] Thank you for your cooperation.)[BLOCK][CLEAR]")

--Topics
WD_SetProperty("Unlock Topic", "Golems", 1)
WD_SetProperty("Unlock Topic", "Dolls", 1)