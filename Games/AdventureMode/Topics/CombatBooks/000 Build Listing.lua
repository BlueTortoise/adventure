--[Build Listing]
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "CombatBooks"

--Topic listing.
fnConstructTopic("BleedPoison",    "Bleeding and Poisons",  1, sNPCName, 0)
fnConstructTopic("Blinding",       "Blinding and Accuracy", 1, sNPCName, 0)
fnConstructTopic("ComboPoints",    "Combo Points",          1, sNPCName, 0)
fnConstructTopic("Experience",     "Experience",            1, sNPCName, 0)
fnConstructTopic("Reinforcements", "Reinforcements",        1, sNPCName, 0)
fnConstructTopic("Skillbooks",     "Skill Books",           1, sNPCName, 0)
fnConstructTopic("Stunning",       "How to Stun",           1, sNPCName, 0)
fnConstructTopic("Healing",        "Healing Yourself",      1, sNPCName, 0)
