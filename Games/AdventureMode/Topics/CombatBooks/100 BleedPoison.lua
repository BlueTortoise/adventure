--[BleedPoison]
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

WD_SetProperty("Append", "[VOICE|Mei](Bleeding and poisoning attacks will deal damage over time. These attacks ignore the enemy's protection, but some enemies are resistant to bleed or poison.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](If an enemy seems like it'd be resistant to poison or bleeding, such as by being made of rock, it probably is. Enemies that inflict poison are usually resistant to it.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Damage from these attacks occurs at the beginning of the character's turn. If you can get the DoTs high enough, the enemy may keel over without you needing to attack again.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Some abilities may make an enemy more vulnerable to bleeding or poisoning, in which case they will take extra damage! Work with your teammates to max out the pain!)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Finally, some abilities are called \"Chasers\". These abilities deal extra damage to enemies suffering from bleeding or poisoning, so pick your targets carefully.)[BLOCK][CLEAR]")
	