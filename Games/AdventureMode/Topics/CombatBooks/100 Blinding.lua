--[Blinding]
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

WD_SetProperty("Append", "[VOICE|Mei](Blinding attacks will reduce their target's accuracy. Blinds stack together, and can make an enemy unable to land any hits.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Enemies also lose 5 percent accuracy for each point of speed you have over them, and vice versa. Fast enemies may ignore blinding attacks altogether!)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Also, some entities are resistant to blinds, such as Bee Girls - since they can sense through their feelers. The blind will still take effect but be reduced in impact.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](If you get blinded, you may want to try a high-accuracy attack like Quick Strike if you are unable to remove the blinding effect.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Finally, excess accuracy increases the likelihood of a Powerful Blow landing. For every point your accuracy is over 100 percent, you get a chance to land a Powerful Blow.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](A Powerful Blow deals 25 percent more damage and inflicts some stun damage on the enemy. Using abilities to slow them down can increase your Powerful Blow rate!)[BLOCK][CLEAR]")
	