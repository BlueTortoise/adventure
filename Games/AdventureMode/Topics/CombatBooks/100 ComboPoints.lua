--[Combo Points]
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

WD_SetProperty("Append", "[VOICE|Mei](As you deal damage and perform attacks, you will build up combo points (CP). These can be seen as flashing bars beneath your portrait in combat.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Special attacks require combo points to use, but will deal extra damage or may have particularly nasty effects.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Using a special attack consumes all available CP, so don't save them! CP is also lost when combat is over, so use them when you need them!)[BLOCK][CLEAR]")
	