--[Experience]
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

WD_SetProperty("Append", "[VOICE|Mei](Experience is awarded after victory in combat. Running away gives no experience.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Characters need more experience to level up as their level increases. Find tougher enemies if you're taking a long time to level!)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Your health, attack power, dodge rate, speed, and accuracy all increase when you level up.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Further, you will gain access to new abilities or bonuses if you can find a skillbook that matches your level. Search for them, it's worth it!)[BLOCK][CLEAR]")
	