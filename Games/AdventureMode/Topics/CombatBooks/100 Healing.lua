--[Stunning]
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

WD_SetProperty("Append", "[VOICE|Mei](After combat is over, you can heal yourself using your Doctor Bag. Each point of HP healed requires 1 charge from the bag.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](The bag recovers charges when you rest, but it also recovers charges when you defeat enemies in battle.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Defeating an enemy in battle on the first turn regenerates 15 percent of the bag's charges, the second turn 10, third 7, and fourth 3.5.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Being aggressive in battle means your Doctor Bag can be recharged and keep you healthy. No need to dally!)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Be on the lookout for red cross objects in the world. Touching one will instantly refill your Doctor Bag to full.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Finally, you can set your Doctor Bag to automatically heal the party after battle on the options menu.)[BLOCK][CLEAR]")
	