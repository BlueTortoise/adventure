--[Reinforcements]
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

WD_SetProperty("Append", "[VOICE|Mei](When engaging enemies, they will chase you a certain distance if they spot you. If another enemy is nearby, they will chase you, and attempt to join the battle.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](If extremely close, they may join the battle immediately. Otherwise, a number will appear above their heads, indicating how many turns they are from joining.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Having another enemy join the battle can make it far more difficult, but it can also make it easier since you can build up buffs or combo-points to unleash.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](When possible, try to pick enemies off one at a time. Isolate them and lead them away, then take them out!)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Some enemies may move in formation or patrol around, though, which can make isolating them difficult. In these cases, avoid them or be ready for a big battle!)[BLOCK][CLEAR]")
	