--[Stunning]
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

WD_SetProperty("Append", "[VOICE|Mei](Pandemonium is an extremely dangerous place for a person who cannot protect themselves. Certain bloody-minded individuals even publish periodicals with tips on fighting.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Each fighting style has its own set of books. The higher the volume number, the more complicated the manuevers are.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](These books can teach you new abilities, but if your level is too low they may be too hard to understand. If you find one you can't figure out, remember where it is, and return later.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](You may well need these abilities to survive, so read whatever books you can! Knowledge is power, literally through literacy!)[BLOCK][CLEAR]")
	