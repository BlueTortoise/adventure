--[Stunning]
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

WD_SetProperty("Append", "[VOICE|Mei](Some attacks, such as Pommel Bash, deal stun damage in addition to normal damage. Stun damage is represented by yellow waving lines.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](When an enemy has taken enough stun damage, they will be stunned for one turn and completely unable to attack or dodge.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](After being stunned, the enemy will become resistant to stuns for 3 turns. If stunned again they will increase in resistance even further.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Stunning a powerful enemy may be a good idea if you cannot blind it or reduce its attack power. It won't last forever, though, so strike quickly!)[BLOCK][CLEAR]")
	