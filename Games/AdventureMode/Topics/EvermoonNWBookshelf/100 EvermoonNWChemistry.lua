--[Chemical Interest]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "EvermoonNWBookshelf", "Leave")

--Standard.
WD_SetProperty("Append", "[VOICE|Mei](The cover page is a bunch of hearts and beakers swirling together...)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](It appears to be a story about a girl mixing a love potion and making out with a whole bunch of people.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Then, everyone starts betraying one another and everyone gets pregnant.[SOFTBLOCK] Even the men.[SOFTBLOCK] *Especially* the men.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](This book might be a bit too dramatic to keep reading...)[BLOCK][CLEAR]")
