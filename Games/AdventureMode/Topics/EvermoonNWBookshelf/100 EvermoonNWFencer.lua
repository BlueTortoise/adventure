--[Fencing Book]
--Calls "The Fencer's Friend Vol. 2" script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "EvermoonNWBookshelf", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)
LM_ExecuteScript(gsMeiSkillbook, 0)