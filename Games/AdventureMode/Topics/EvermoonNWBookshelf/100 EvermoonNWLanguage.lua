--[Foreign Languages]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "EvermoonNWBookshelf", "Leave")

--Standard.
WD_SetProperty("Append", "[VOICE|Mei](Maybe this is a book about translating foreign languages?)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](No...[SOFTBLOCK] it's a book about two girls who speak different languages meeting and teaching each other.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Then they have sex a lot, but it takes twenty chapters to get that far.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](At first I thought this was a story, but then it just became smut...)[BLOCK][CLEAR]")
