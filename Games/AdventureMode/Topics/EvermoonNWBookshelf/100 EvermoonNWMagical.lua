--[Magical Settlement]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "EvermoonNWBookshelf", "Leave")

--Standard.
WD_SetProperty("Append", "[VOICE|Mei](Seems to be part of a long series about a bunch of magical girls.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](The main character was a man at the start of the series, but has slowly been turned into a magical girl due to a series of mishaps.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](In this one, he/she gets into a drinking contest with the camp leader.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](...)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Wow!)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](...)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](That was close!)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](...)[BLOCK][CLEAR]")
WD_SetProperty("Append", "[VOICE|Mei](Is that all for this novella?[SOFTBLOCK] I want to see what happens next![SOFTBLOCK] Argh!)[BLOCK][CLEAR]")
