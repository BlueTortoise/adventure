--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is in Alraune form, or has access to it.
if(sMeiForm == "Alraune" or iHasAlrauneForm == 1.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] Florentina, I don't mean to offend, but...[SOFTBLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] A sure indication you're about to offend me.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] You don't act like the other Alraunes.[SOFTBLOCK] Why?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] It's complicated.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] But you hear the little ones whisper.[SOFTBLOCK] Don't you care about them?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] As much as I care about anyone else, really.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] The other Alraunes?[SOFTBLOCK] When they wiped their memories, I guess all that was left was peace and love.[SOFTBLOCK] That's called naivete.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] Yeah, I don't think you'd change at all if you partook of the fungus.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Feh, who knows?[SOFTBLOCK] I'm not keen to find out.[SOFTBLOCK] Neither are you, it seems.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] I haven't ruled it out.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] I thought you wanted to go back to Earth?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I do, but...[SOFTBLOCK] I don't know if I would actually stay.[SOFTBLOCK] I would want to tell me family not to worry about me...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] How sweet.[SOFTBLOCK] So the job at hand hasn't changed.[BLOCK][CLEAR]")

--Normal case.
else
	WD_SetProperty("Append", "Mei:[E|Neutral] You seem a lot different than the other Alraunes.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Offended] The so-called \"wild\" ones?[SOFTBLOCK] Yeah.[SOFTBLOCK] What tipped you off?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] No need to be sarcastic.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] Mei, when Alraunes join a new leaf-sister, they make them soak up this stuff they call the \"Cleansing Fungus\".[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] It makes you forget everything.[SOFTBLOCK] I mean, everything.[SOFTBLOCK] Your name, your home, your family, all of it -[SOFTBLOCK] gone.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] But it's voluntary, right?[SOFTBLOCK] You didn't go for it?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] Yeah, and they'll treat you like an outsider until you do.[SOFTBLOCK] For most Alraunes, it's not much of a choice.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Me?[SOFTBLOCK] I had other things I needed to do.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Oh?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] ...[SOFTBLOCK] I don't want to talk about it right now.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Can we just find you a way home?[BLOCK][CLEAR]")
end