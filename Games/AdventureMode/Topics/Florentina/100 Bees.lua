--[Bees]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm    = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is in bee form.
if(sMeiForm == "Bee") then
	WD_SetProperty("Append", "Mei:[E|Blush] Florentina, you are looking...[SOFTBLOCK] delicious today...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Surprise] Mei, if you're doing what I think you're doing...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Paying a compliment?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Subtlety is not your strong suit.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] I can smell your nectar - [SOFTBLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Blush] Too forward, kid.[SOFTBLOCK] I do find it flattering, but not right now.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] I'll just enjoy your company...[BLOCK][CLEAR]")

--Mei is not in bee form, but has access to bee form.
elseif(iHasBeeForm == 1.0) then
	WD_SetProperty("Append", "Mei:[E|Blush] So, Florentina.[SOFTBLOCK] Have you ever been...[SOFTBLOCK] intimate...[SOFTBLOCK] with a bee?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Why don't you ask your little hive friends?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I can't without my antennae.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] You're hitting on me, and you're really bad at it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] I wasn't -[SOFTBLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] You absolutely were.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] I can't help it...[SOFTBLOCK] I just want to drink your delicious nectar...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] I'm flattered.[SOFTBLOCK] Really.[SOFTBLOCK] But I'm not in the mood, okay?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Okay, sure.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] (Just, gotta make sure she doesn't catch me drooling...)[BLOCK][CLEAR]")

--Normal case.
else
	WD_SetProperty("Append", "Mei:[E|Neutral] Where I'm from, we don't have bees this big.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Count your blessings, then.[SOFTBLOCK] The only places on Pandemonium that bees *aren't* are...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] ...[SOFTBLOCK] glaciers, I guess.[SOFTBLOCK] That's all I can think of.[SOFTBLOCK] I've heard that they can survive in deserts, badlands, jungles...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] I'm detecting a slight displeasure in your voice.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] In general, I've got nothing against them.[SOFTBLOCK] They just tend to pester us Alraunes when we're busy.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] How so?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Blush] Do you know what bees do to plants?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well they -[SOFTBLOCK][SOFTBLOCK][E|Surprise] Oh![SOFTBLOCK][E|Neutral] I get you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Blush] They can be a lot of fun, but there's a lot of bees and they're always in the mood.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Laugh] Reminds me of the guys back home![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Some things are universal.[SOFTBLOCK] If you're cute, someone's going to be bothering you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Hear hear![BLOCK][CLEAR]")
end