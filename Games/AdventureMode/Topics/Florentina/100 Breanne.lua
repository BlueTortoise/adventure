--[Claudia]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Variables.
local iMetMeiWithFlorentina = VM_GetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N")

--If Mei has seen Florentina and Breanne meet.
if(iMetMeiWithFlorentina == 1.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] Do you and Breanne get along well?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] You saw it.[SOFTBLOCK] Was it not clear?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] Brusque.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] Then why are you asking?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] One does not study a lake by merely staring at its surface.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused]...[SOFTBLOCK] Come again?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I'm just trying to learn about a person who I have to fight alongside.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] I had you pegged for a sucker, Mei.[SOFTBLOCK] Keep it up and you might yet prove me wrong.[BLOCK][CLEAR]")

--If Mei has not met Breanne with Florentina present.
else
	WD_SetProperty("Append", "Mei:[E|Neutral] Do you and Breanne get along well?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] I've had dealings with her in the past. She worries too much about mannerisms and not enough about results.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] The two aren't incompatible...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] She seems to think we're friends, if that's what you wanted to know.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Are you?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] No.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] Naturally.[BLOCK][CLEAR]")
end
