--[Claudia]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
if(iSavedClaudia == 0.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] Do you remember anything about Claudia that might help find her?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] I remember that I didn't like her, that's for sure.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Laugh] You don't seem to like anyone.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Excellent choice of words.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] It was mostly Hypatia who did the bulk of the dealings.[SOFTBLOCK] I ran into her when she tried to break up a card game I was running.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Card game?[SOFTBLOCK] Like, gambling?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Ever heard of Six-Step Shuffle?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] No, never.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] You bet on who's got the best hand of cards, and there's several rounds of betting.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] The catch is that you can't bet on yourself.[SOFTBLOCK] So if you've got the best hand, you have to find a way to ruin it, or improve someone else's.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] It's all about bluffing, guessing motives, and lying.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] Apparently, her religion thinks that me taking money from dumb people is immoral.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] She got really riled up, and Blythe had to get involved.[SOFTBLOCK] That's why I let Hypatia do the dealing.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I see.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] She's a real goodie-goodie.[SOFTBLOCK] Kind of like you, except she was blonde and shorter.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] I'll consider that a compliment.[BLOCK][CLEAR]")

--Claudia has already been rescued:
else
	WD_SetProperty("Append", "Mei:[E|Neutral] Claudia is kind of a sacchrine person, isn't she?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Absolutely.[SOFTBLOCK] She was a lot of fun to mock.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] Florentina![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] What?[SOFTBLOCK] She would torture my words until they were compliments.[SOFTBLOCK] It was genuinely endearing to watch.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] So you made fun of her, but failed.[SOFTBLOCK] She was too nice to even insult.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] I didn't say that...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] Does that irk you?[SOFTBLOCK] That you couldn't get to somebody?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] In my youngers days, it probably would have.[SOFTBLOCK] Now?[SOFTBLOCK] Meh.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] I'm kind of surprised nobody has taken advantage of her charitable attitude yet.[SOFTBLOCK] Then again I can say the same about you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] You're not doing people a favour by being a jerk to them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] You're also not doing them a favour by being nice.[SOFTBLOCK] Think about it.[BLOCK][CLEAR]")

end