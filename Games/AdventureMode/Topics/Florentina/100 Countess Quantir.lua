--[Countess Quantir]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

WD_SetProperty("Append", "Mei:[E|Neutral] Do you know when Countess Quantir was alive?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] I'm not much of a historian, but I can safely say it was well before my time.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Quantir has always been an abandoned wasteland.[SOFTBLOCK] I guess she was responsible for that.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] We're looking at centuries, at least.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Sad] She suffered in that room for centuries...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Confused] Well deserved, too.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Sad] Yeah...[BLOCK][CLEAR]")