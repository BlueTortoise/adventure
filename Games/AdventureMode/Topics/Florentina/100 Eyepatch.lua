--[Werecats]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iReadAlraunes = VM_GetVar("Root/Variables/Chapter1/Scenes/iReadAlraunes", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei does not know Alraunes can regenerate:
if(iReadAlraunes == 0.0) then
	WD_SetProperty("Append", "Mei:[E|Smirk] So how'd you lose an eye, anyway?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Now let's see...[SOFTBLOCK] which story are you most likely to believe?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Knife fight is obvious.[SOFTBLOCK] Failed assassination is also possible, you'd go for that.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] You're not going to tell me, are you?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] I have my reasons for my privacy, Mei.[SOFTBLOCK] I don't like you prying.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] I just want to know you better...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Buck up, kid.[SOFTBLOCK] I'm not trying to sour you, I just don't think it's something you need to know.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Do you know how old I am?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well, I don't mean to be impolite, but...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Sixty-two.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] ..![SOFTBLOCK] Really?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] See, that one you're willing to believe.[SOFTBLOCK] Alraunes don't age like humans, we age like trees.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] We could spend days talking about all the awesome stuff I've done.[SOFTBLOCK] But, we have a job to do.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Laugh] So are you really in your sixties?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] It's like talking to a brick wall...[BLOCK][CLEAR]")

--Mei knows Florentina can regrow her eye:
else
	WD_SetProperty("Append", "Mei:[E|Smirk] So how'd you lose an eye, anyway?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Now let's see...[SOFTBLOCK] which story are you most likely to believe?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Knife fight is obvious.[SOFTBLOCK] Failed assassination is also possible, you'd go for that.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] You're not going to tell me, are you?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] I have my reasons for my privacy, Mei. I don't like you prying.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] I just want to know you better...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Buck up, kid.[SOFTBLOCK] I'm not trying to sour you, I just don't think it's something you need to know.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Do you know how old I am?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well, I don't mean to be impolite, but...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Sixty-two.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] ..![SOFTBLOCK] Really?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] See, that one you're willing to believe.[SOFTBLOCK] Alraunes don't age like humans, we age like trees.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] We could spend days talking about all the awesome stuff I've done.[SOFTBLOCK] But, we have a job to do.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] Hey, wait a second![SOFTBLOCK] That Heavenly Doves log said that Alraunes regrow lost body parts![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Well yes, we do.[SOFTBLOCK] It takes a while, though.[SOFTBLOCK] Trees take years to regrow branches, after all.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] So why don't you just regrow your eye?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Mei, when you met me, which eye was my patch over?[SOFTBLOCK] Left, or right?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well, uhh....[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] How do you know I didn't switch it when you weren't looking?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] More importantly, if you were a bounty hunter who's never seen me before, and you were told I had an eyepatch...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] And then you took it off...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Now you're getting it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Laugh] So -[SOFTBLOCK] are you actually missing an eye?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] It's like talking to a brick wall...[BLOCK][CLEAR]")

end