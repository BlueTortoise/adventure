--[Ghosts]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm      = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasGhostForm = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is in ghost form.
if(sMeiForm == "Ghost" or iHasGhostForm == 1.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] Florentina -[SOFTBLOCK] have you ever seen a ghost before?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] In your usually unsubtle way, you're implying that Earth doesn't have these.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Oh -[SOFTBLOCK] no![SOFTBLOCK] We absolutely have ghosts![SOFTBLOCK] I've seen Youku videos that show them, absolutely![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I was just wondering if this was your first time.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Not really, no.[SOFTBLOCK] There are parts of Pandemonium just lousy with the undead.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] If you're looking for an exercise in futility, go spend some time trying to kill them.[SOFTBLOCK] You'll get really tired and probably depressed.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] This kind of suffering is widespread?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] It's no different than being an Alraune, really.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] But Quantir.[SOFTBLOCK] Different.[SOFTBLOCK] That's all I can say.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]")

--Normal case.
else
	WD_SetProperty("Append", "Mei:[E|Neutral] Florentina -[SOFTBLOCK] have you ever seen a ghost before?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] In your usually unsubtle way, you're implying that Earth doesn't have these.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Oh -[SOFTBLOCK] no![SOFTBLOCK] We absolutely have ghosts![SOFTBLOCK] I've seen Youku videos that show them, absolutely![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I was just wondering if this was your first time.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Not really, no.[SOFTBLOCK] There are parts of Pandemonium just lousy with the undead.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] If you're looking for an exercise in futility, go spend some time trying to kill them.[SOFTBLOCK] You'll get really tired and probably depressed.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Surely there is something we can do?[SOFTBLOCK] We can't just let them suffer for eternity...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Actually, the Draugr aren't really suffering.[SOFTBLOCK] Insufferable, sure, but suffering?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Now -[SOFTBLOCK] Quantir.[SOFTBLOCK] Different.[SOFTBLOCK] That's all I can say.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I see...[BLOCK][CLEAR]")

end