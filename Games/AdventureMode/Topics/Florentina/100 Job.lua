--[Job]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
WD_SetProperty("Append", "Mei:[E|Neutral] Do you own the general store, or just manage it?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Confused] ...[SOFTBLOCK] what?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] Well, I mean, it's your store.[SOFTBLOCK] Is it owned by someone else?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Confused] You -[SOFTBLOCK] huh?[SOFTBLOCK] You can't own something without managing it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Surprise] That's not how we do it on Earth.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] It is here.[SOFTBLOCK] In Trannadar, if you're not using a property then you had best have a good reason for your absence.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] Hypatia and I are co-owners.[SOFTBLOCK] I've got two years seniority, so that means I make most of the decisions.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] I had heard some Japanese companies are like that, but...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] Listen, Mei.[SOFTBLOCK] People go missing fairly often here.[SOFTBLOCK] If you're gone and don't come back, the rest of us can't afford to wait.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] It hurts a lot when someone you care about...[SOFTBLOCK][E|Confused] is...[SOFTBLOCK] gone...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] Florentina?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Don't interrupt me.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] Anyway, ownership of property passes to whoever is making good use of it.[SOFTBLOCK] If nobody contests a claim for three months, it passes to the claimant.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] I saw some abandoned cabins in the forest.[SOFTBLOCK] Are those up for grabs?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] Sure, if nobody else is using them.[SOFTBLOCK] Blythe keeps a claims log in his office.[SOFTBLOCK] You looking to settle down?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Blush] Not at all.[SOFTBLOCK][E|Neutral] It's just so different from back home.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] I'm starting to think this Earth place is somehow worse than Pandemonium.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] Huh...[BLOCK][CLEAR]")