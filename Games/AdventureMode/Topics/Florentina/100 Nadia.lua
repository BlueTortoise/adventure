--[Nadia]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
WD_SetProperty("Append", "Mei:[E|Neutral] What do you think of Nadia?[SOFTBLOCK] Are you two friends?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] I wondered how long it'd be before you brought up that nutjob.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Laugh] Nutjob?[SOFTBLOCK] Well -[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Confused] If you make a pun I'm going to whack you.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Offended] Fine, jeez.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Nadia's okay.[SOFTBLOCK] Irritating, but useful.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] And would I qualify as irritating but useful?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Yes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Happy] So that means you and Nadia are good friends.[SOFTBLOCK] Case closed.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] It's all relative...[BLOCK][CLEAR]")