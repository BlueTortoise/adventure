--[Name]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Variables.
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")

--Standard. Mei hasn't seen the Alraune scene and doesn't know they change their names.
if(iHasAlrauneForm == 0.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] Could you tell me a little more about yourself?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] No.[SOFTBLOCK] I don't like to waste time, and you don't need to know anything.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] This is a rest stop.[SOFTBLOCK] Relax a little.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Offended] Tch.[SOFTBLOCK][E|Confused] What do you want to know?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I don't know...[SOFTBLOCK] Where is the name Florentina from?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Me.[SOFTBLOCK] I came up with it.[SOFTBLOCK] Sounded flowery.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well that's nice...[BLOCK][CLEAR]")

--Mei has Alraune form, so she knows they change their names.
else
	WD_SetProperty("Append", "Mei:[E|Neutral] Could you tell me a little more about yourself?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] No.[SOFTBLOCK] I don't like to waste time, and you don't need to know anything.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] This is a rest stop.[SOFTBLOCK] Relax a little.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Offended] Tch.[SOFTBLOCK][E|Confused] What do you want to know?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Was Florentina your name before you became an Alraune?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] No.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Ah.[SOFTBLOCK] I see.[SOFTBLOCK] I was under the impression you didn't breathe the cleansing fungus.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] I didn't.[SOFTBLOCK] I'm not the nature-loving type.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Then why change your name?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] My business, not yours.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] Okay then...[BLOCK][CLEAR]")
end