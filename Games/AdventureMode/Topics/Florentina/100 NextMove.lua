--[Our Next Move]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iMetClaudia                  = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
local iExaminedMirror              = VM_GetVar("Root/Variables/Chapter1/Scenes/iExaminedMirror", "N")
local iMeiKnowsRilmani             = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
local iHasBreanneMetMei            = VM_GetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N")
local iHasFoundOutlandAcolyte      = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
local iBreanneMetMeiWithFlorentina = VM_GetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--If you examined the mirror, this dialogue pops up:
if(iExaminedMirror == 1.0) then
	WD_SetProperty("Append", "Mei:[E|Sad] Florentina...[SOFTBLOCK] I'm gonna miss you...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Heh.[SOFTBLOCK] We're not done yet.[SOFTBLOCK] Let's go bust some heads.[SOFTBLOCK] You know, that can be your going-away present![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Heh.[SOFTBLOCK] Okay![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] But, whenever we're done, sooner or later, you're going to have to go through that mirror.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Yeah.[SOFTBLOCK] But, I'll enjoy our time together.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Likewise.[BLOCK][CLEAR]")
	
--If Mei knows Rilmani:
elseif(iMeiKnowsRilmani == 1.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] Okay, so this runestone has a Rilmani symbol on it.[SOFTBLOCK] Any ideas?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Well, legend has it that the big mansion east of the Trading Post has something to do with the Rilmani.[SOFTBLOCK] It's called the Dimensional Trap, after all.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] So we should check the stuff in there?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, look for anything that might have a Rilmani symbol on it.[SOFTBLOCK] That's probably our lead.[BLOCK][CLEAR]")

--If Mei met Claudia:
elseif(iMetClaudia == 1.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] Claudia mentioned that her journal could help us, and that it's in the Quantir estate.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] She didn't give you particular directions?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] She said it was a few weeks ago.[SOFTBLOCK] Someone might have moved it.[SOFTBLOCK] We'll just have to go in there and look for it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] And if we happen to find anything valuable, I'm sure nobody will mind if we take it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] I knew you'd be excited![BLOCK][CLEAR]")


--Has met Karina but not talked to Breanne:
elseif(iHasFoundOutlandAcolyte == 1.0 and iBreanneMetMeiWithFlorentina == 0.0) then

	WD_SetProperty("Append", "Mei:[E|Neutral] So, where should we go next to find Claudia?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] That Karina character said she was up in Quantir.[SOFTBLOCK] It's not far from here.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] If memory serves, the old Quantir Estate should be northeast of the trading post.[SOFTBLOCK] Run down, hasn't been used in a long time.[SOFTBLOCK] We should start there.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] What happened to it?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Dunno.[SOFTBLOCK] The caravans sometimes go by it, but they're not dumb enough to go in it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Is it dangerous?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] In Quantir, it's a dangerous business going out your front door.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Like Trannadar, it's an 'unincorporated territory'.[SOFTBLOCK] That means, no aristocrats, no royals, no laws.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well, danger is relative.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] We might also stop in at Breanne's place if you're feeling lucky.[SOFTBLOCK] She might have a clue, since Claudia probably went by her place.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] No time like the present. Let's go see what we can find![BLOCK][CLEAR]")

--Has spoken with Breanne but not Karina:
elseif(iHasFoundOutlandAcolyte == 0.0 and iBreanneMetMeiWithFlorentina == 1.0) then

	WD_SetProperty("Append", "Mei:[E|Neutral] So, where should we go next to find Claudia?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Definitely we should check out Outland Farm.[SOFTBLOCK] Breanne said one of Claudia's followers was over there.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] It's just north of the trading post, across the river.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] All right![BLOCK][CLEAR]")

--Has spoken with Breanne and Karina:
elseif(iHasFoundOutlandAcolyte == 1.0 and iBreanneMetMeiWithFlorentina == 1.0) then

	WD_SetProperty("Append", "Mei:[E|Neutral] So, where should we go next to find Claudia?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] That Karina character said she was up in Quantir.[SOFTBLOCK] It's not far from here.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] If memory serves, the old Quantir Estate should be northeast of the trading post.[SOFTBLOCK] Run down, hasn't been used in a long time.[SOFTBLOCK] We should start there.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] What happened to it?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Dunno.[SOFTBLOCK] The caravans sometimes go by it, but they're not dumb enough to go in it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Is it dangerous?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] In Quantir, it's a dangerous business going out your front door.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Like Trannadar, it's an 'unincorporated territory'.[SOFTBLOCK] That means, no aristocrats, no royals, no laws.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] Danger is relative.[SOFTBLOCK] I'm sure we can handle it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] That's the right attitude.[BLOCK][CLEAR]")
	
--Has not met Karina and did not talk to Breanne about it:
else

	WD_SetProperty("Append", "Mei:[E|Neutral] So, where should we go next to find Claudia?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Two possibilities, actually.[SOFTBLOCK] Last I heard, Claudia and her little troupe were up north, at the Outland Farm.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] I don't know if they're still there, but maybe we can find someone who saw them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] And the other possibility?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] There's an excessively nosy lady named Breanne who lives on the north end of the lake.[SOFTBLOCK] Has a place she calls the Pit Stop.[BLOCK][CLEAR]")
	
	--If Mei hasn't met Breanne:
	if(iHasBreanneMetMei == 0.0) then
		WD_SetProperty("Append", "Florentina:[E|Happy] Dumb name, but there it is.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] I don't think that's a dumb name...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] A lot of visitors to this area stop there since she has spare beds.[SOFTBLOCK] Might be a good place to look.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] All right![SOFTBLOCK] Let's go see what we can find![BLOCK][CLEAR]")
	
	--If Mei has met Breanne:
	else
		WD_SetProperty("Append", "Mei:[E|Happy] Breanne![SOFTBLOCK] She suggested I come visit you![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] Huh.[SOFTBLOCK] Well, couldn't hurt to go see what she knows.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] All right![SOFTBLOCK] Let's go see what we can find![BLOCK][CLEAR]")
	end
end