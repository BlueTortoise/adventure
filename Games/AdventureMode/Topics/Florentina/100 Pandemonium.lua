--[Pandemonium]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
WD_SetProperty("Append", "Florentina:[E|Neutral] You know, I had figured you would be asking me non-stop about Pandemonium.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Offended] I'm trying to stay focused.[SOFTBLOCK] Every time I see something new we could probably spend ten minutes discussing it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Probably.[SOFTBLOCK] I was trying to say you're less annoying on this count than some others.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Angry] Oh, okay![SOFTBLOCK] So, tell me about freakin' Pandemonium![BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Teasing you is a lot of fun.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Offended] Mrgrgr...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Ha![BLOCK][CLEAR]")