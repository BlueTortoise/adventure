--[Florentina's Past]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Topic.
local sTopic = LM_GetScriptArgument(0)

--Opener:
if(sTopic == "Hello") then

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] So, would you say you know me well enough to talk about your past, yet?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's a really long story.[SOFTBLOCK] Do you really want to hear it?[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Hear it\", " .. sDecisionScript .. ", \"HearIt\") ")
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Maybe not\",  " .. sDecisionScript .. ", \"Nope\") ")
	fnCutsceneBlocker()

--Hear the story:
elseif(sTopic == "HearIt") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I really do, Florentina.[SOFTBLOCK] Please.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Are you ever going to just give up?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Oh wait, no.[SOFTBLOCK] No you're not.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Look, if you really don't want to talk about it, I'll leave it.[SOFTBLOCK] But you don't look angry whenever I ask.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It means dredging up old stuff that I'm supposed to have moved on from.[SOFTBLOCK] I thought I had, but then you showed up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I guess I didn't really move on from it...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You can trust me.[SOFTBLOCK] I think I've demonstrated that.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] And if not -[SOFTBLOCK] I do intend to go back to Earth.[SOFTBLOCK] No way to keep your secret safer![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's not really a secret.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Okay, I may as well tell you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I'm from a city-state far to the southeast of here.[SOFTBLOCK] It's called Sturnheim, if that means anything to you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nope...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Heh.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I was going to school to be a lawyer.[SOFTBLOCK] Down there, it's a six-year study, but you're pretty much guaranteed to get a good job.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I don't like taking orders from anyone, and lawyers are generally in charge of legal offices.[SOFTBLOCK] It's a lot more tiered than in Trannadar.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] My -[SOFTBLOCK] my love...[SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Florentina?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] My fiance and I -[SOFTBLOCK] oh how I miss her...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Ahem.[SOFTBLOCK] Sorry.[SOFTBLOCK] I -[SOFTBLOCK] get a little tear-eyed about her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Oh...[SOFTBLOCK] did she pass away?[SOFTBLOCK] I'm sorry...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] No...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] We were going to move into a new house I had commissioned with my first year's salary.[SOFTBLOCK] She had always wanted a big house with her own garden.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] She loved growing vegetables in the public gardens and selling them.[SOFTBLOCK] That's how she helped put me through school, you see.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] We had a big house, good jobs, lots of money, respect...") ]])
	fnCutsceneBlocker()
	
	--Disable music.
	fnCutsceneInstruction([[ AudioManager_PlayMusic("Null") ]])
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] ...[SOFTBLOCK] I did everything right, damn it![SOFTBLOCK] I worked hard![SOFTBLOCK] I made sacrifices for my beloved Cathelina![SOFTBLOCK] She sacrificed for me![SOFTBLOCK] Isn't that what love is?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] It was all going so well...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm sorry to bring this up...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I got sick, Mei.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I got sick and nobody knew what the cause was.[SOFTBLOCK] One night I just woke up and had spit blood all over the bedsheets.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I would feel weak one minute and then strong as a horse the next.[SOFTBLOCK] Then, I'd get a fever that would last a day and be gone by midnight.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] We went to every doctor we could find.[SOFTBLOCK] We got a different diagnosis from each one, and none of their treatments worked.[SOFTBLOCK] Quacks, all of them![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I had done everything right, and this is what I got for it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] Cathelina -[SOFTBLOCK] she was so strong.[SOFTBLOCK] She kept finding new doctors and wizards who could help.[SOFTBLOCK] She never broke her calm facade.[SOFTBLOCK] She just kept going like this was a problem and we could solve it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] But I was going to die, Mei.[SOFTBLOCK] I was going to die and leave her all alone in that big house.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] There was one treatment I didn't rule out.[SOFTBLOCK] I didn't tell my beloved about it, of course.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] The Alraunes...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] She had cried herself to sleep, so I let myself out one night when I was feeling strong.[SOFTBLOCK] I went into the forest and I called to them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] They took me to their joining pool.[SOFTBLOCK] At first I walked, but they had to carry me after I had a spell of weakness.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] They put me in, and...[SOFTBLOCK] here I am.[SOFTBLOCK] Cured.[SOFTBLOCK] Healthy.[SOFTBLOCK] A plant.[SOFTBLOCK] Forever.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I didn't give a crap about the cleansing fungus, so I told them to stuff it and went to see Cathelina.[SOFTBLOCK] She met me at the door.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] She -[SOFTBLOCK] spit on me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] She said I was a grotesque monster, a mockery of the one she loved.[SOFTBLOCK] She said I should have died rather than become this.[SOFTBLOCK] She called me a coward.[SOFTBLOCK] She said I was worthless.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] And, I had spent our money searching for a cure.[SOFTBLOCK] She was poor.[SOFTBLOCK] All we had was the house, and she would have to sell it to cover my debts.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] So I left.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Oh, Florentina...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Well, now you know.[SOFTBLOCK] Now you know why I am what I am.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I'm really sorry...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's ancient history, Mei.[SOFTBLOCK] It's been forty years since then.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But you still have debt collectors coming after you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Heh.[SOFTBLOCK] More bounty hunters than anyone else.[SOFTBLOCK] I assume Cathelina sold the house and put the money into a bounty on my head.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Maybe she convinced herself that I killed her beloved and tried to take his place?[SOFTBLOCK] I don't know what could be going through her head.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I don't blame her.[SOFTBLOCK] I did this to her.[SOFTBLOCK] Maybe it really would have been better if I had had the courage to just die quickly.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] What do you think?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not going to judge you.[SOFTBLOCK] Either of you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Fair enough.") ]])
	fnCutsceneBlocker()
	
	--Disable music.
	fnCutsceneInstruction([[ AudioManager_PlayMusic("FlorentinasTheme") ]])
	fnCutsceneInstruction([[ WD_SetProperty("FastShow") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Florentina") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] It actually feels nice to talk about it with someone who understands.[SOFTBLOCK] I feel a little better about it now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Have you ever considered going back?[SOFTBLOCK] Finding her?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] ...[SOFTBLOCK] Every day.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] But she's an old woman by now, Mei.[SOFTBLOCK] She's probably remarried, had kids, grandkids...[SOFTBLOCK] moved on with her life.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] If I really love her, I'm not going to re-insert myself into that.[SOFTBLOCK] I'll let her be happy.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well.[SOFTBLOCK] I guess I get it now.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Nothing to do but keep going.[SOFTBLOCK] Until we don't anymore.[BLOCK][CLEAR]") ]])

--Hear the story:
elseif(sTopic == "Nope") then

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Florentina") ]])
	
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If it's really long, it might be best to do it later.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Fine with me...[BLOCK][CLEAR]") ]])

end
