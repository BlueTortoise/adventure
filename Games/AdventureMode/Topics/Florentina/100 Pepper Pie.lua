--[Pepper Pie]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Already completed the pie job:
if(iTakenPieJob == 2.0) then
	WD_SetProperty("Append", "Mei:[E|Laugh] Mmmm![SOFTBLOCK] You weren't kidding that this pie is good![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Ooooohhhhh yeah.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] It regenerates slowly, so it can only be used once per battle.[SOFTBLOCK] But, what a treat![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well then don't eat it all now, silly![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Just a nibble...[BLOCK][CLEAR]")

--On the pie job:
else

	--Variables.
	local iMossCount      = AdInv_GetProperty("Item Count", "Gaardian Cave Moss")
	local iPaperCount     = AdInv_GetProperty("Item Count", "Booped Paper")
	local iNectarCount    = AdInv_GetProperty("Item Count", "Decayed Bee Nectar")
	local iFlowerCount    = AdInv_GetProperty("Item Count", "Hypnotic Flower Petals")
	local iSalamiCount    = AdInv_GetProperty("Item Count", "Translucent Quantirian Salami")
	local iKokayaneeCount = AdInv_GetProperty("Item Count", "Kokayanee")
	
	--Special:
	local iHasMetAdina           = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N")
	local iMeiLovesAdina         = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
	local iAdinaExtendedMistress = VM_GetVar("Root/Variables/Chapter1/Scenes/iAdinaExtendedMistress", "N")

	--Common:
	WD_SetProperty("Append", "Mei:[E|Neutral] Okay, what's on the pie list?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Let's see here...[BLOCK][CLEAR]")
	
	--Special case:
	if(iMossCount > 0 and iPaperCount > 0 and iNectarCount > 0 and iFlowerCount > 0 and iSalamiCount > 0 and iKokayaneeCount > 0) then
		WD_SetProperty("Append", "Florentina:[E|Neutral] Actually, we have everything we need.[SOFTBLOCK] Let's go get us a pie![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Cool![SOFTBLOCK] I can't wait![SOFTBLOCK] I call first dibs on the taste-test![BLOCK][CLEAR]")
	
	--Otherwise:
	else

		--Moss:
		if(iMossCount < 1) then
			WD_SetProperty("Append", "Florentina:[E|Neutral] Gaardian Cave Moss.[SOFTBLOCK] I know this stuff -[SOFTBLOCK] nasty, but vital to the recipe.[SOFTBLOCK] Grows in damp, stony underground areas.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Neutral] Look for green mossy buildups.[SOFTBLOCK] I'll be able to tell which ones have the right stuff.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Neutral] I've got an idea of where to search...[BLOCK][CLEAR]")
		end
		
		--Paper:
		if(iPaperCount < 1) then
			WD_SetProperty("Append", "Florentina:[E|Neutral] Canine Boop Paper.[SOFTBLOCK] Uhhh...[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Surprise] Boop paper?[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Neutral] Paper that has been booped against a dog's nose.[SOFTBLOCK] Any breed of dog will do.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Smirk] Why does it need to be booped?[SOFTBLOCK] What would that do?[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Confused] Are you a mage?[SOFTBLOCK] Do you know how alchemical reactions work?[SOFTBLOCK] No?[SOFTBLOCK] Didn't think so![BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Neutral] Okay, okay.[SOFTBLOCK] Next?[BLOCK][CLEAR]")
		end
		
		--Nectar:
		if(iNectarCount < 1) then
			WD_SetProperty("Append", "Florentina:[E|Neutral] Decayed Bee Nectar.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Neutral] Well that one sounds easy.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Confused] No, it has to be decayed.[SOFTBLOCK] It won't be in a beehive, it'll be someplace else.[SOFTBLOCK] It decays if the bees don't process it in time.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Neutral] It'll be someplace near a lot of flowers where the bees over-harvested and had to discard some.[BLOCK][CLEAR]")
		end
		
		--Flower:
		if(iFlowerCount < 1) then
			WD_SetProperty("Append", "Florentina:[E|Neutral] Hypnotic Flower Petals.[SOFTBLOCK] There's a couple candidate species, but none of them are native to Trannadar.[SOFTBLOCK] This one might be tough.[BLOCK][CLEAR]")
			
			--Mei has met Adina and smelled the hypnotic flower.
			if(iHasMetAdina >= 10.0) then
				
				--Mei not in love with Adina:
				if(iMeiLovesAdina == 0.0) then
					WD_SetProperty("Append", "Mei:[E|Smirk] Oh, I think I have an idea.[SOFTBLOCK] Let's check the salt flats down south.[BLOCK][CLEAR]")
					WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, sure.[SOFTBLOCK] Might be something there.[BLOCK][CLEAR]")
				
				--Mei is in love with Adina:
				else
					WD_SetProperty("Append", "Mei:[E|Blush] Oh this one is easy![SOFTBLOCK] Mistress Adina grows those on the salt flats![BLOCK][CLEAR]")
					
					if(iAdinaExtendedMistress == 0.0) then
						WD_SetProperty("Append", "Florentina:[E|Surprise] ...[SOFTBLOCK] Mistress?[BLOCK][CLEAR]")
						WD_SetProperty("Append", "Florentina:[E|Neutral] Nevermind.[SOFTBLOCK] Let's go find the flower.[BLOCK][CLEAR]")
					else
						WD_SetProperty("Append", "Florentina:[E|Surprise] Mistress?[BLOCK][CLEAR]")
						WD_SetProperty("Append", "Florentina:[E|Neutral] Oh yeah, you guys are freaks.[SOFTBLOCK] Whatever.[SOFTBLOCK] Let's go find that flower.[BLOCK][CLEAR]")
					end
				end
			
			--Mei did not smell the flower:
			else
				WD_SetProperty("Append", "Mei:[E|Neutral] I have no idea.[SOFTBLOCK] Guess we'll have to do some searching.[BLOCK][CLEAR]")
			end
		end
		
		--Salami:
		if(iSalamiCount < 1) then
			WD_SetProperty("Append", "Florentina:[E|Neutral] Quantirian Salami.[SOFTBLOCK] That might be a bit tough.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Neutral] Why would that be?[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Neutral] Quantir's total population is less than a hundred.[SOFTBLOCK] I don't know if any of them even know how to make Quantirian Salami.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Happy] The stuff is so salted that it'd take centuries to decay, though.[SOFTBLOCK] We might be able to find some leftover if we look.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Smirk] All right![SOFTBLOCK] Next?[BLOCK][CLEAR]")
		end
		
		--Kokaynee
		if(iKokayaneeCount < 1) then
			WD_SetProperty("Append", "Florentina:[E|Happy] Kokayanee.[SOFTBLOCK] Hmmm...[SOFTBLOCK] that one hits a bit close to home.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Neutral] Huh?[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Neutral] Blythe is a real stickler about the stuff.[SOFTBLOCK] He keeps the mercs clean.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Neutral] It's a drug?[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Happy] One of the knock-you-on-your-sor drugs, actually.[SOFTBLOCK] Doesn't work on Alraunes, so I don't care for it.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Florentina:[E|Neutral] Someone will probably have a stash, but I bet they won't want to part with it.[BLOCK][CLEAR]")
			WD_SetProperty("Append", "Mei:[E|Smirk] So we'll have to ask around, and definitely don't ask any of the mercs.[SOFTBLOCK] Got it.[SOFTBLOCK] What's next?[BLOCK][CLEAR]")
		end
		
		--Final case.
		WD_SetProperty("Append", "Florentina:[E|Neutral] That's it.[SOFTBLOCK] Shall we get back to it, then?[BLOCK][CLEAR]")
	end
end