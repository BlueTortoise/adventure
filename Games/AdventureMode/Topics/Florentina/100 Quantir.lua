--[Quantir]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

WD_SetProperty("Append", "Mei:[E|Smirk] So what exactly is Quantir?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] It's usually called the Quantir High Wastes.[SOFTBLOCK] It's an arid province east of Evermoon Forest.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Something about the mountains prevents rainclouds from reaching it, or something.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Smirk] Are there many people there?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Not a lot of humans that I know about.[SOFTBLOCK] There's a few small settlements, but it's bad land for farming.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Nobody administers the land, really.[SOFTBLOCK] None of the city states care about it since it's not good for much.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] There apparently was a gold rush a few decades back that I missed out on.[SOFTBLOCK] Lots of abandoned buildings in former mining communities.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] Gold rushes...[SOFTBLOCK] we had those on Earth, too.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] When people hear they can get rich, they'll come from a continent away for even the smallest chance -[SOFTBLOCK] and damn the consequences.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] It's a good place to go if you need to get lost.[SOFTBLOCK] If you ever get a price on your head, there's all kinds of caves to hide out in.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Smirk] Spoken from experience?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Surprise] You'd impugn my honor?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] Yeah okay, I maaaay have spent a few weeks hiding out a few years back...[BLOCK][CLEAR]")