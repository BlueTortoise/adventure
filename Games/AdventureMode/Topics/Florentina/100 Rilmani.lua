--[Rilmani]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Dialogue.
WD_SetProperty("Append", "Mei:[E|Smirk] So, Rilmani.[SOFTBLOCK] Do you know anything about them that might help?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Nope, sorry.[SOFTBLOCK] I didn't even think they existed.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Smirk] The book said they're a local legend.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, there's legends about them almost everywhere you go.[SOFTBLOCK] Usually someone knew someone who knew someone who saw one.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] ...[SOFTBLOCK] Alcohol is usually involved...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Smirk] What do the legends say?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Well, they're four meters tall, can walk through walls, have short necks and legs, and they love to eat disobedient children.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, that's not useful...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] Sometimes I get people who are looking for Rilmani artifacts.[SOFTBLOCK] Usually they're procurement types.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] What does that mean?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] If you're an alchemist and you need something exotic, or a mage who wants a catalyst, you can't exactly find them for sale in a store.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] So they'll hire a procurement agent to find what they need.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Smirk] So they're like treasure hunters?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Blush] Oooh, you're making me excited.[SOFTBLOCK] Treasure is such a sexy word...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] Anyway, I've never met one who actually found a Rilmani artifact, much less a Rilmani.[SOFTBLOCK] Either they're rare, or they clean up after themselves.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Happy] You've also never met someone from Earth![BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] And you have a runestone which is probably one such artifact.[SOFTBLOCK] Suddenly I like our chances.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] The legends say they're found in that mansion near the lake.[SOFTBLOCK] It's called the Dimensional Trap sometimes.[SOFTBLOCK] We should look there.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Smirk] All right![BLOCK][CLEAR]")
