--[Rochea]
--Dialogue script. The player must have met Rochea for it to even be available, so Mei implicitly has Alraune form.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iKnowsMeiHasAlraune       = VM_GetVar("Root/Variables/Chapter1/Florentina/iKnowsMeiHasAlraune", "N")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
local iFlorentinaSpecialAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSpecialAlraune", "N")

--If Florentina doesn't know Mei is an Alraune, which is possible if Mei becomes an Alraune, switches back, then meets Florentina.
if(iKnowsMeiHasAlraune == 0.0) then
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do you know who Rochea is?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] How do you know that name?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well, you see...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm an Alraune too.[BLOCK][CLEAR]") ]])
	
	--Set variable.
	VM_SetVar("Root/Variables/Chapter1/Florentina/iKnowsMeiHasAlraune", "N", 1.0)
	
	--If Florentina knows about the runestone...
	if(iFlorentinaKnowsAboutRune == 1.0) then

		--Flag to go back to the topics listing when this is over.
		WD_SetProperty("Activate Topics After Dialogue", "Florentina")
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You are?[SOFTBLOCK] No, let me guess.[SOFTBLOCK] The runestone?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Yep.[SOFTBLOCK] I don't think you've seen me that way, yet.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Feh.[SOFTBLOCK] I'm not in a position to doubt any claim you make, am I?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Were you perchance royalty back on Earth?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm a waitress, actually.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] But if you're an Alraune, and you just arrived in Pandemonium...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Rochea joined you, didn't she.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That was her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] ... she's beautiful, isn't she?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] And so gentle...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I'm not on good terms with her.[SOFTBLOCK] I said some things I shouldn't have.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] We probably wouldn't be on good terms anyway, but I don't think she deserved it.[SOFTBLOCK] Sometimes you're just angry at the whole world.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Would an apology help?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I'll think about it, Mei.[SOFTBLOCK] I'll think about it.[BLOCK][CLEAR]") ]])
		fnCutsceneBlocker()
	
	--If not, a demonstration is in order.
	else
	
		--Flags.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSpecialAlraune", "N", 1.0)
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You are?[SOFTBLOCK] Because you don't look like one.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Huh?[SOFTBLOCK] Really?[BLOCK] Freakin' really?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] The little ones are backing you up here.[SOFTBLOCK] What gives?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I could show you...") ]])
		fnCutsceneBlocker()
		
		--This script will take over once the dialogue closes.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/100 Transform/Transform_MeiToAlraune/Scene_Begin.lua")
	end

--Otherwise, she knows.
else
	--Flag to go back to the topics listing when this is over.
	WD_SetProperty("Activate Topics After Dialogue", "Florentina")
	
	--Normal dialogue:
	if(iFlorentinaSpecialAlraune == 0.0) then
		WD_SetProperty("Append", "Florentina:[E|Neutral] Mei, when you were joined...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Yes?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Confused] Was it Rochea?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] That was her.[SOFTBLOCK] She's the one who joined me.[SOFTBLOCK] She's so beautiful, and gentle.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Did she join you too?[SOFTBLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] No, I was joined a long way from here.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Confused] I said some things to her last time we crossed paths.[SOFTBLOCK] Not all of them good.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] No surprise there.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Offended] Funny.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] Seeing you...[SOFTBLOCK] I was thinking maybe I should go and apologize to her.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I'm a little shocked to hear this coming from you.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] People aren't of a piece, Mei.[SOFTBLOCK] I don't like her, but I don't think she deserved what I did either.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] She's still there.[SOFTBLOCK] You could go see her.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] If we go that way...[SOFTBLOCK] I don't know.[SOFTBLOCK] I'll figure it out.[BLOCK][CLEAR]")
	
	--Special dialogue:
	else
		WD_SetProperty("Append", "Florentina:[E|Happy] Hey, Mei?[SOFTBLOCK] Thanks.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] For what?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Confused] It's been a long time since I really thought about life outside of business.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Confused] I try to keep myself busy, and people at the trading post are always coming and going.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Confused] But, I should probably at least try to make nice with Rochea and her covenant.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] Maybe not right away.[SOFTBLOCK] I need to think of what I'm going to say.[SOFTBLOCK] But I should say it, you know?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I understand you.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Happy] Of course, if you tell anyone I've been acting all sappy, I will deny it up and down.[SOFTBLOCK] You hear me?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Laugh] Oh, of course.[BLOCK][CLEAR]")
	
	end
end