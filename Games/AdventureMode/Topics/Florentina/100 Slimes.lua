--[Slimes]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm  = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is not slime form.
if(sMeiForm ~= "Slime") then
	WD_SetProperty("Append", "Mei:[E|Neutral] Florentina, what do you know about slimes?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] I know enough not to fool with them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Slimes are minor predators and carrion feeders.[SOFTBLOCK] They eat whatever they can catch.[SOFTBLOCK] That includes you, if you're dense.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] They'd attack one of us?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] No, more like slowly slide up to us and nudge us.[SOFTBLOCK] We're too big, and it'd take days to digest us.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] You'd have to be pretty badly wounded for a slime to try to take you on.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] But the slimes here - [SOFTBLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Breeding season.[SOFTBLOCK] They take on human form when they're looking to reproduce.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] They're pretty dumb, so they'll attack bees, Alraunes, werecats, whatever.[SOFTBLOCK] But, they're looking for stray humans.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Oh...[BLOCK][CLEAR]")

--Mei is not in slime form.
else
	WD_SetProperty("Append", "Mei:[E|Neutral] Hey, Florentina...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Huh?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] I keep catching you staring at my chest.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Blush] ...[SOFTBLOCK] and?[SOFTBLOCK] You're not exactly hiding it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] Are you attracted to women, perchance?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Blush] Yeah.[SOFTBLOCK] All monstergirls are.[SOFTBLOCK] It's just how it is.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] ...[SOFTBLOCK] oh, you don't have monstergirls on Earth, do you?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Nope.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Well, monstergirls are girls for a reason.[SOFTBLOCK] No men.[SOFTBLOCK] We're all attracted to the female form.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Not that I don't appreciate a good man, but I know what I like.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] Should I make them bigger?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Blush] They're the right size.[SOFTBLOCK] Keep them right there...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Surprise] Hey![SOFTBLOCK] Don't manipulate my libido![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Hee hee...[BLOCK][CLEAR]")
end