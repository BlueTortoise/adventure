--[Trading Post]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
WD_SetProperty("Append", "Mei:[E|Neutral] Do you know how long the trading post has been there?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Neutral] This particular instance?[SOFTBLOCK] I think Blythe mentioned he took over for someone else eight years ago, so at least that long.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] But beyond that?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] Mei, that spot has probably been a settlement for a long time.[SOFTBLOCK] It's in a good spot where rivers and paths intersect.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Happy] If that kind of spot gets abandoned, someone will get it in their head sooner or later to re-establish it.[SOFTBLOCK] Why do you ask?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Happy] Comparing it to home.[SOFTBLOCK] People have been living in the Hong Kong area for some thirty-five thousand years.[SOFTBLOCK] It's a very old city.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Surprise] Thirty-five...[SOFTBLOCK] thousand?[SOFTBLOCK] Is the world even that old?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] Earth is billions of years old.[SOFTBLOCK] I would figure Pandemonium is too.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Florentina:[E|Confused] Yeah, you're making that up.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Mei:[E|Neutral] (I guess they haven't figured out archaeology here yet...)[BLOCK][CLEAR]")