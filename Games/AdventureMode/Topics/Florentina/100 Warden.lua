--[Warden]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iHasGhostForm            = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
local iCompletedQuantirMansion = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Has not completed the mansion events:
if(iCompletedQuantirMansion == 0.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] The warden...[SOFTBLOCK] do we even know what her name was?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] I don't think so.[SOFTBLOCK] Did any of those journal pages mention it?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I guess not.[BLOCK][CLEAR]")
	
	if(iHasGhostForm == 1.0) then
		WD_SetProperty("Append", "Mei:[E|Neutral] Seems Natalie doesn't know either.[SOFTBLOCK] There were a lot of maids doing burials, so I'm not sure.[BLOCK][CLEAR]")
	end
	WD_SetProperty("Append", "Florentina:[E|Confused] Considering everything we know, reasoning with her isn't going to help.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] So how are we going to defeat her?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] Okay, so my memory is a little hazy, what with the hard hit to the head and all...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] But it seemed that she was going for you first, for some reason.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] If you were ready for it, you could probably try to block the hit.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Would that be enough?[SOFTBLOCK] It was like getting hit by a train...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] There are other ways to soften a hit in battle.[SOFTBLOCK] I'll see what I can do to help.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] It might also be a good idea to look for combat books.[SOFTBLOCK] There are a bunch of publishers who put them out on a monthly basis.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] No matter how many scraps you've been in, you can always learn something new.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] *sigh*...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] Is violence really the only way forward?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] This isn't a fight, Mei.[SOFTBLOCK] This is a mercy-kill.[SOFTBLOCK] They're just going to suffer more if we dither.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] Yeah...[BLOCK][CLEAR]")

--Normal case.
else
	WD_SetProperty("Append", "Mei:[E|Neutral] The warden...[SOFTBLOCK] we didn't even know her name...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Good riddance to bad rubbish.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] But Florentina - [SOFTBLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] Mei, that thing wasn't human.[SOFTBLOCK] It was pure anger.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] No matter what her intentions may have been, she caused a lot of people to suffer.[SOFTBLOCK] She's just as bad as Quantir was.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Confused] The right thing to do in those cases is to wash your hands and move on.[SOFTBLOCK] There's no happy ending here.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, I guess you're right.[SOFTBLOCK] We'll just have to make sure they're not forgotten.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Will you ask Blythe to record what happened here?[SOFTBLOCK] Tell the historians?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] If it'll get you to shut up, sure.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] So what will happen to the ghosts now?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Dunno.[SOFTBLOCK] Maybe they'll fade away, maybe they'll remember who they were.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] If any of the tireless dead need a job cleaning, Hypatia's pretty lousy at it.[SOFTBLOCK] I could hire one.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] R-[SOFTBLOCK]really?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Think about it -[SOFTBLOCK] no sick leave![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] Only you could put a positive spin on undeath.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] There's a time to mourn, and a time to move on.[SOFTBLOCK] Shall we get back to finding a way home for you?[BLOCK][CLEAR]")

end