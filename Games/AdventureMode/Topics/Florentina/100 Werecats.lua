--[Werecats]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is in werecat form.
if(sMeiForm == "Werecat" or iHasWerecatForm == 1.0) then
	WD_SetProperty("Append", "Mei:[E|Neutral] Do you have many dealings with the kinfangs of this region?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] You mean werecats, right?[SOFTBLOCK] Yeah, sort of.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] They've got everything they want in the forest.[SOFTBLOCK] They come to buy medicine sometimes, or to put a hit out.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] A -[SOFTBLOCK] hit?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Slang for assassination.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] I know what it means![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Why would a kinfang do that?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Search me.[SOFTBLOCK] I don't ask too many questions.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] Lazy![SOFTBLOCK] If you want someone dead, do it yourself![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Blush] You're my best friend right at this second.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] I just don't get it.[SOFTBLOCK] There's got to be a reason...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Some of the werecats prefer scavenging to hunting.[SOFTBLOCK] Maybe they're not good at violence but want another cat dead?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Hmmm...[BLOCK][CLEAR]")

--Normal case.
else
	WD_SetProperty("Append", "Mei:[E|Neutral] Do you have many dealings with the werecats of this region?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Sort of.[SOFTBLOCK] They've got everything they want in the forest.[SOFTBLOCK] They come to buy medicine sometimes, or to put a hit out.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] A -[SOFTBLOCK] hit?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Neutral] Slang for assassination.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] I know what it means![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] Do the werecats -[SOFTBLOCK] do they kill each other?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Obviously not.[SOFTBLOCK] They hire mercenaries to do it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] Maybe there's some rule against killing another werecat?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Gee, can you think of any other kind of society where killing another citizen is illegal?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] Okay, jeez, lay off![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Florentina:[E|Happy] Ha ha![SOFTBLOCK] Oh you're fun to tease![BLOCK][CLEAR]")

end