--[Alraunes]
--Karina asks you about Alraunes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Bee form.
	if(sMeiForm == "Bee") then
		WD_SetProperty("Append", "Karina: So, how do bees get along with the Alraunes?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] I'd say Florentina and I get along famously.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] I think the buzzing is melting her brain.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] You see?[SOFTBLOCK] Best friends.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: But seriously...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] They mostly leave us alone, and we leave them alone.[SOFTBLOCK] They are good sources of nectar when they want to be, but they are often obstinate and not worth the risk.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: Wait, the Alraunes themselves?[SOFTBLOCK] You can derive nectar from them?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] Yes, we just need to su- [BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Surprise] That's enough![SOFTBLOCK] Next question![BLOCK][CLEAR]")
	
	--Any other form:
	else
		WD_SetProperty("Append", "Karina: So, how do bees get along with the Alraunes?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] I'd say Florentina and I get along famously.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] You're not a bee right now, numbnuts.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Sad] Do you have to rub it in?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: But seriously...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] They mostly leave us alone, and we leave them alone.[SOFTBLOCK] They are good sources of nectar when they want to be, but they are often obstinate and not worth the risk.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: Wait, the Alraunes themselves?[SOFTBLOCK] You can derive nectar from them?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] Yes, we just need to su- [BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Surprise] That's enough![SOFTBLOCK] Next question![BLOCK][CLEAR]")
	end
end