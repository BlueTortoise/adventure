--[Claudia]
--Dialogue.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	WD_SetProperty("Append", "Karina: Mei![SOFTBLOCK] Sister Claudia says that she has you to thank for saving her![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Claudia: Indeed.[SOFTBLOCK] Thank you, Mei.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] It was nothing.[SOFTBLOCK] Really.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Claudia: I do apologize, though.[SOFTBLOCK] I did mention a possible reward, but I'm afraid Karina is all that is left of my convent.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Claudia: I'm sorry that I have nothing to offer you.[SOFTBLOCK] Except, possibly, membership.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Uhhh, I think I'll pass...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Karina: You'll always be an honorary Dove![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Claudia: Indeed.[SOFTBLOCK] Should you encounter another of our order, merely mention the Doves and they will accomodate you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well, that's not so bad.[SOFTBLOCK] I'll keep it in mind.[BLOCK][CLEAR]")
end