--[Hive Mine]
--Karina asks you about the hive mind.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Bee form.
	if(sMeiForm == "Bee") then
		WD_SetProperty("Append", "Karina: So, how does the hive mind work?[SOFTBLOCK] Can you describe it?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I'm afraid you wouldn't understand it without being able to experience it.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] It truly is incomparable to human experience.[SOFTBLOCK] Not even the internet comes close, and that place is an echo chamber.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: What's an internet?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Nevermind...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: All right, then.[SOFTBLOCK] How do you interact with the other bees?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] They...[SOFTBLOCK] we can hear one another's ideas through our antennae here, and we send our ideas to one another when we have them.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Laugh] They're always telling me what they're seeing and hearing, and what they're thinking.[SOFTBLOCK] It's like they're my own thoughts.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: Could you stop your thoughts from going out?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] N-[SOFTBLOCK]never![SOFTBLOCK] Why would I want to?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Sad] Imagine you love someone so deeply that you want to share every moment of your life with them.[SOFTBLOCK] That is how I am with my drone sisters.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: But you could, in theory?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Cry] ...[SOFTBLOCK] Only if the hive would be better served by my absence.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Sad] Even thinking this has caused us to shudder.[SOFTBLOCK] I am sorry, sisters.[BLOCK][CLEAR]")
	
	--Any other form:
	else
		WD_SetProperty("Append", "Karina: So, how does the hive mind work?[SOFTBLOCK] Can you describe it?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I'm afraid you wouldn't understand it without being able to experience it.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] It truly is incomparable to human experience.[SOFTBLOCK] Not even the internet comes close, and that place is an echo chamber.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: What's an internet?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Nevermind...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: All right, then.[SOFTBLOCK] How do you interact with the other bees?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] If I had my antennae, I'd be hearing their thoughts through them.[SOFTBLOCK] We transmit what we're thinking to one another.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: Does that mean someone could intercept them somehow?[SOFTBLOCK] Oh, or possibly listen in![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Sorry, but you don't speak our language.[SOFTBLOCK] Every hive has its own thought language.[SOFTBLOCK] It's quite complicated, actually.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: But would you?[SOFTBLOCK] Can you speak the language?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] Not speak, think.[SOFTBLOCK] When you're counting, you count in numbers, right?[SOFTBLOCK] Those numbers are ones you learned from your language.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] The way we think is organized by our language.[SOFTBLOCK] As a bee, the way I think is organized by the hive mind.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: Amazing![SOFTBLOCK] But what happens if you're cut off from the hive mind?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Sad] Well, I'd find my way back as soon as possible![SOFTBLOCK] It'd be...[SOFTBLOCK] truly frightening to be so alone...[BLOCK][CLEAR]")
	end
end