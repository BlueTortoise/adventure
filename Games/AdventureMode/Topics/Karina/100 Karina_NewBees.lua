--[New Bees]
--Karina asks you about how bees make new bees.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	WD_SetProperty("Append", "Karina: Can you tell me a bit more about the process of making a new bee?[SOFTBLOCK] I can't get close to the hive to observe it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] There is a very fast way to find out...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Karina: You simply telling me is faster, isn't it?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Suit yourself.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] We bees can turn nectar into honey with our special enzymes, much like the diminuitive bee.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] The honey is both a long-term food source, and when specially prepared, acts as a transformation agent.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Karina: I see![SOFTBLOCK] So you need honey both for food and reproduction?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Correct.[SOFTBLOCK] It takes approximately the body weight of a human in honey to fully transform one.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] We are limited in our recruitment by our honey income.[SOFTBLOCK] Once we need all our honey for food, we regrettably cannot convert more bees.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] We are thus somewhat choosy in our recruitment.[SOFTBLOCK] My hive is young, so we are always on the lookout.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Karina: Very interesting![SOFTBLOCK] That answers quite a few questions about the spread of bee hives.[BLOCK][CLEAR]")
end