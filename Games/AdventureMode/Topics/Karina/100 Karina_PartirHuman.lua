--[Alraunes]
--Karina asks you about Alraunes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	WD_SetProperty("Append", "Mei:[E|Neutral] I was wondering.[SOFTBLOCK] What is a partirhuman?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Karina: Oh, that's the technical term for what is commonly called a monster girl.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Karina: Humans are very similar to animals like dogs and birds and apes, in that we can reproduce sexually.[SOFTBLOCK] In fact, the majority of macrospecies do that.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Karina: Partirhumans cannot do that, they instead must turn humans into their own kind.[SOFTBLOCK] Incidentally, all partirhumans are female, and male humans become female during the process.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Karina: They all have a different method for the conversion.[SOFTBLOCK] Nobody is quite sure how they evolved like that, but that's what my research is for![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] (Perhaps my runestone has something to do with this...)[BLOCK][CLEAR]")
end