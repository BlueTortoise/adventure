--[Rewards]
--Karina asks you about what she could reward the hive with.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Bee form:
	if(sMeiForm == "Bee") then
		WD_SetProperty("Append", "Karina: What can I get you to act as a reward for answering my questions?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] One moment...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Yes.[SOFTBLOCK] Yes.[SOFTBLOCK] Of course.[SOFTBLOCK] No.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I see.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] We want for three things, human.[SOFTBLOCK] Nectar, recruits, and security.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Can you provide us any of those?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: Perhaps I could broker an agreement.[SOFTBLOCK] You could gather nectar from the plants on the farm here, at certain times, and the mercs wouldn't stop you.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: Of course, we'd have to clear out for a bit.[SOFTBLOCK] I don't trust the hive not to grab someone...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Yes.[SOFTBLOCK] It is less than ideal.[SOFTBLOCK] It is better than the current standoff.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] We are at consensus.[SOFTBLOCK] Your deal is tentatively accepted.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: I'll see if I can talk the Lieutenant into it![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Yes.[SOFTBLOCK] I understand.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] It seems the hive will choose another representative.[SOFTBLOCK] My mission is of higher priority.[SOFTBLOCK] We will leave written instructions for you.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Karina: Can't any of the other bees speak for the hive?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I'm afraid not.[SOFTBLOCK] They are incapable of speech in the conventional sense, as they lack the internal conception of individuality.[SOFTBLOCK] If any of them were to speak, they would all speak in unison.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I can speak, because I am...[SOFTBLOCK] different...[BLOCK][CLEAR]")
	
	--Any other form.
	else
		WD_SetProperty("Append", "Karina: What can I get you to act as a reward for answering my questions?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Well, I'd need to consult with the hive.[SOFTBLOCK] But, I can't do that without my feelers.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] So, erm, I'll go get them and get back to you.[BLOCK][CLEAR]")
	end
end