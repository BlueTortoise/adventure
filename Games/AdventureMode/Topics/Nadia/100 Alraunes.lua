--[Alraunes]
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a human...
	if(sMeiForm ~= "Alraune") then
		WD_SetProperty("Append", "Mei:[E|Neutral] So, you're an Alraune?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Yep![SOFTBLOCK] I have been as long as I can remember![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] And you're a guard here?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Oh yeah, Cap'n Blythe is just the nicest human you'll ever meet.[SOFTBLOCK] He goes to great lengths to hide it, but he's a softy on the inside.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Most humans aren't very understanding, but here at Trannadar, we have a saying::[SOFTBLOCK] \"If you don't have money, go away\".[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] That isn't a nice thing to say.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Nice, no, but it sure isn't prejudiced.[SOFTBLOCK] Well, not if you have money.[BLOCK][CLEAR]")
	
	--If Mei is an Alraune
	else
		WD_SetProperty("Append", "Mei:[E|Neutral] Have you been a leaf-sister for very long?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Hm, a few years now.[SOFTBLOCK] I don't remember what came before.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, they gave me the same offer.[SOFTBLOCK] I turned it down.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: To each her own.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Sad] Aren't you curious?[SOFTBLOCK] Don't you wonder who you used to be?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: There's really two ways it could have gone.[SOFTBLOCK] Either I was something I'd rather leave behind, or I led a boring life and it's no great loss.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I take it you've thought about it a lot.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Sometimes, when I'm on my own and the little ones don't feel like talking, I get to thinking.[SOFTBLOCK] Then, they start talking again.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] They're real chatterboxes aren't they?[BLOCK][CLEAR]")
	end
end