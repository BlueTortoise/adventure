--[Bees]
--Asking this NPC about Bee Girls in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-bee...
	if(sMeiForm ~= "Bee") then
		WD_SetProperty("Append", "Mei:[E|Neutral] Any tips if I run into a bee girl out in the forest?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: The bees?[SOFTBLOCK] Yeah, their stings are minorly poisonous.[SOFTBLOCK] It's nothing serious, but be careful.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Anything else?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Hm, I think there was someone researching bees at the farm up north.[SOFTBLOCK] If you're curious you might want to ask around up there.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] Thanks![BLOCK][CLEAR]")
		
	--If Mei is a bee...
	else
		WD_SetProperty("Append", "Mei:[E|Neutral] My sisters sure do talk about you a lot.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Really?[SOFTBLOCK] Cool![SOFTBLOCK] What do they say?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] They think you're really nosy.[SOFTBLOCK] You like to bother them when they're busy.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: H-[SOFTBLOCK]hey![SOFTBLOCK] I'm just doing my job![SOFTBLOCK] I have to keep the bees out of the area around the trading post![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Yeah.[SOFTBLOCK] Sure.[SOFTBLOCK] Okay.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: ...?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Oh, sorry.[SOFTBLOCK] My sisters said they didn't know.[SOFTBLOCK] We won't look for nectar near the post.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Well isn't that sweet as honey of them.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Yes, sure.[SOFTBLOCK] I'll tell her.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: What did they say?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] Oh, nothing.[SOFTBLOCK] Nothing about your jokes, certainly.[BLOCK][CLEAR]")
	
	end
end