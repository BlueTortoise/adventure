--[Blythe]
--Asking this NPC about Blythe.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei:[E|Neutral] Mr. Blythe is your boss?[SOFTBLOCK] How'd you two meet?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Mmm, it's kind of a sad story.[SOFTBLOCK] Are you sure you want to hear it?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Not if it upsets you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: No, it's okay.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I wasn't joined around here.[SOFTBLOCK] My spawning pool was a long way north of here, near Soswitch.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Let me tell you, Soswitch isn't a very nice place.[SOFTBLOCK] The humans are all greedy and paranoid and angry and...[SOFTBLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] Are you okay?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: ...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: They hunted us down.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I don't know if it's something we did, but one day a huge crowd of them started combing through the forest and murdering any monstergirl they found.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: They found my spawning pool.[SOFTBLOCK] My leaf-sisters tried to stop them, but they cut them down and burned the place to the ground.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: ...[SOFTBLOCK] I ran.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] I'm sorry...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I ran for a long time, days, weeks with little rest.[SOFTBLOCK] They were like a wave crashing against the beach, just going further and further across the land and washing away everything they touched.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Eventually they petered out and went back home, but I was so far away from my spawning pool and I didn't know how to get back.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I was so angry...[SOFTBLOCK] I wanted to just kill all the humans, wipe them out like they wiped out my leaf-sisters![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: But then I met the Cap'n and he was so different.[SOFTBLOCK] He didn't want to kill anyone, and he wasn't like the humans up north.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: It took me a while, but I learned that not all humans are the same.[SOFTBLOCK] Maybe if I could teach the humans that not all monstergirls are the same, my leaf-sisters won't have died for nothing.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Sorry to bring it up, Nadia.[SOFTBLOCK] You've been through a lot.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: It still hurts, but it gave me a reason to keep going.[SOFTBLOCK] You can't let your past hold you back forever.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Besides, the sun still shines and juice still tastes sweet.[SOFTBLOCK] There's always something to look forward to.[BLOCK][CLEAR]")
	
end