--[Breanne]
--Asking this NPC about Breanne.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei:[E|Neutral] Could you tell me a bit about Breanne?[SOFTBLOCK] What's she like?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Well, I don't want to say anything I'm not certain of.[SOFTBLOCK] I think she's in trouble from down east.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: There's always human men hanging around her place.[SOFTBLOCK] She doesn't like them much, and refuses to talk about them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Human men?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Sorry, force of habit.[SOFTBLOCK] All men are humans.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] That's not what I meant.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Good pun![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] Huh?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: That's not what you men-t![SOFTBLOCK] Ha ha![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Grugh...[BLOCK][CLEAR]")
	
end