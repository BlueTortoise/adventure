--[Cultists]
--Asking this NPC about the spooky cultists in the mansion.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] You're a guard, right?[SOFTBLOCK] Did you know about the cultists in the big mansion near here?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: That rotting old dump has cultists in it?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Yes.[SOFTBLOCK] You didn't know?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I steer clear of that place.[SOFTBLOCK] Gives me the willies just looking at it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: You better tell Cap'n Blythe about those cultists, they might have something to do with the strange happenings lately.[BLOCK][CLEAR]")
end