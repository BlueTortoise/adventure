--[Florentina]
--Asking this NPC about Florentina.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei:[E|Neutral] Can you tell me anything interesting about Florentina?[BLOCK][CLEAR]")
	
	--If Florentina is not present...
	if(bIsFlorentinaInParty == false) then
		WD_SetProperty("Append", "Nadia: Like, gossip?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: She's a bit of tree bark on the outside, but she's a darn good merchant and she's always going to help if you're in trouble.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: The other Alraunes don't like her much, but I never asked her why.[SOFTBLOCK] She might be shy about it.[BLOCK][CLEAR]")
		
	--If Florentina is present...
	else
		WD_SetProperty("Append", "Nadia: I'm not gonna say anything while she's standing right there![SOFTBLOCK] How ya doin', Florry?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Confused] Please don't call me that.[SOFTBLOCK] That is a terrible nickname.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Sure thing, Florry![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Facepalm] ...[SOFTBLOCK] This is what I have to deal with.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Laugh] Ha ha![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: She's bitter,[SOFTBLOCK] like a Vernonia leaf,[SOFTBLOCK] but you can count on her.[SOFTBLOCK] Just watch your coinpurse.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Confused] Hey![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Neutral] Oh, who am I kidding, she's right.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] About the coinpurse thing?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Florentina:[E|Happy] A little from column A, a little from column B...[BLOCK][CLEAR]")
	end
end