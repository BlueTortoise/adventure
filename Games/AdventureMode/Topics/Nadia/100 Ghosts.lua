--[Ghosts]
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-ghost...
	if(sMeiForm ~= "Ghost") then
		WD_SetProperty("Append", "Mei:[E|Neutral] Nadia, do you know anything about the mansion way north of here?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Oh, geez, no![SOFTBLOCK] That place creeps me out![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: I keep telling everyone it's haunted![SOFTBLOCK] Nobody believes me![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] I've been inside it.[SOFTBLOCK] It's haunted.[SOFTBLOCK] Ghosts of long-dead maids stalk the halls...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Maids?[SOFTBLOCK] G-[SOFTBLOCK]g-[SOFTBLOCK]g-[SOFTBLOCK]ghost maids?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Oh, I'm sorry if I scared you...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: That is the cutest thing ever![SOFTBLOCK] Oh, now I want to meet one![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: But then I'd get scared solid...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] Heh...[BLOCK][CLEAR]")
	
	--If Mei is a ghost:
	else
		WD_SetProperty("Append", "Mei:[E|Neutral] Nadia, do you know anything about the ghosts in the mansion north of here?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Ohmygosh your outfit is soooooooo cute, Mei![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Do you think you have a ghostly maid getup that'd fit me?[SOFTBLOCK] I know I'm a little small...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Uh, I don't think that's how it works...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Yeah.[SOFTBLOCK] I wouldn't want to spook myself, but I just love all the little frills and buttons and the big bow -[SOFTBLOCK] so cute![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] Thanks?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: What's the term for scary-cute?[SOFTBLOCK] S'cut?[SOFTBLOCK] Terror-chic?[SOFTBLOCK] Boo-tiful?[SOFTBLOCK] Spookute?[SOFTBLOCK] Frightsionable?[SOFTBLOCK] Adhorrorible?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Why do I bring this upon myself?[BLOCK][CLEAR]")
	end
end