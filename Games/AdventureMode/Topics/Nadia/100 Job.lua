--[Name]
--Asking this NPC about their job. A lot of NPCs have this.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common code.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei:[E|Neutral] What is it that you do here, Nadia?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Pretty much whatever the Cap'n says.[SOFTBLOCK] Most of the forest critters aren't interested in us Alraunes, so I can go out there without getting jumped.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Cap'n taught me how to fight, too.[SOFTBLOCK] Just in case.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I must inform you that these hands are deadly weapons.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I'm mostly in charge of foraging up food and ingredients.[SOFTBLOCK] The little ones just love getting their berries picked.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] But you don't need the job here, right?[SOFTBLOCK] Couldn't you just go live in the forest like the other Alraunes?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I guess so, but then I wouldn't get to hang around with all my friends.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Like you![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] Aww...[BLOCK][CLEAR]")
end