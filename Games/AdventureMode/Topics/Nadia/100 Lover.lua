--[Lover]
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local iMeiLovesAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei:[E|Blush] Nadia...[SOFTBLOCK] about the other night...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Oh.[SOFTBLOCK] That.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] I was -[SOFTBLOCK] I was under the influence of the werecat curse, you see, and - [SOFTBLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Don't try to walk this back.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I loved it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] Oh.[SOFTBLOCK] Well, that's good.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] I would never - [SOFTBLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I know.[SOFTBLOCK] That you would even try to apologize is proof enough.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: It seems you got lucky with me -[SOFTBLOCK] in more ways than one![SOFTBLOCK] Ha ha![BLOCK][CLEAR]")
	
	--Mei does not love Adina (or hasn't met her):
	if(iMeiLovesAdina == 0.0) then
		WD_SetProperty("Append", "Mei:[E|Blush] So...[SOFTBLOCK] are you available?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Right this second, no.[SOFTBLOCK] But, if you want to be an item...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: I have to think about it.[SOFTBLOCK] I've never been in a real relationship before.[SOFTBLOCK] I don't know if I'm ready.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] That's okay.[SOFTBLOCK] I don't know if I'll be able to stay on Pandemonium for long.[SOFTBLOCK] But, if you're willing, I'd have a reason to come back.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Or, I could come visit you on Earth![SOFTBLOCK] Do they have Alraunes there?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Ah, no.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Cool! I'd be a celebrity![SOFTBLOCK] Maybe I could go on stage with my jokes?[SOFTBLOCK] Or do you think they're too -[SOFTBLOCK] corny?[SOFTBLOCK] Ah ha ha![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] (I don't think Earth could handle Nadia...)[BLOCK][CLEAR]")
	
	--Mei fell in love with Adina.
	else
		WD_SetProperty("Append", "Mei:[E|Blush] Nadia, I didn't mean to lead you on.[SOFTBLOCK] My heart already belongs to another.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Oh, really?[SOFTBLOCK] Aww, that's too bad.[SOFTBLOCK] I really liked you![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] Not that we can't have fun, but I would need Mistress Adina's permission first.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Adina?[SOFTBLOCK] The Alraune who is farming the salt flats?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Good for you, Mei![SOFTBLOCK] She's really nice![SOFTBLOCK] I visit on my patrols and she always has some mulch cookies for me![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] Oh, Nadia, you should let her enthrall you.[SOFTBLOCK] We could be mindless slaves together...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Wha?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Nothing.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: That sounds kinda nice...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: But I have a job and junk.[SOFTBLOCK] I can't let Cap'n Blythe down...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] There is so much work to do on the salt flats.[SOFTBLOCK] If you change your mind, I have no doubt she'd love to have you.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: You're really sweet in an odd kind of way, Mei.[SOFTBLOCK] I like that![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] I'm just a humble thrall.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: And we can thrall-ways count on you to cheer us up![SOFTBLOCK] Ha Ha![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] (When she's enslaved, she won't tell any more jokes.[SOFTBLOCK] Or will she..?)[BLOCK][CLEAR]")
	
	end
end