--[Name]
--Asking this NPC about their name. A lot of NPCs have this. Sometimes it's asking them what their name is, sometimes
-- it's asking them what their name means.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Dialogue.
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] So, Nadia...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Hi![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Was that your name before you became an Alraune?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I...[SOFTBLOCK] I don't think so.[SOFTBLOCK] I think one of my leaf-sisters said it suited me, so I kept it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: It's a good name![SOFTBLOCK] Not as cool as Mei, but still pretty cool.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Smirk] Hey, there's nothing cool about my name.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: You [SOFTBLOCK]\"Mei\"[SOFTBLOCK] not think so, but I do![SOFTBLOCK] Ha ha![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] (I guess I walked into that one...)[BLOCK][CLEAR]")
end