--[Rochea]
--Asking Nadia about Rochea.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] Do you know of Rochea?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Oh.[SOFTBLOCK] Her.[SOFTBLOCK] I know her.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: She doesn't like me very much.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Why not?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Well, most Alraunes don't see much use for humans besides joining new sisters.[SOFTBLOCK] Not me![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Humans are great![SOFTBLOCK] Well, some of them are.[SOFTBLOCK] The Cap'n is really nice, and so are the other mercs here.[SOFTBLOCK] They're not all bad![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] So they see you as a traitor?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: More like they think I'm misguided.[SOFTBLOCK] They're really bitter about it, too.[SOFTBLOCK] The little ones don't mind, I don't know why the Alraunes have to be such jerks about it![BLOCK][CLEAR]")
end