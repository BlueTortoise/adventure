--[Settlements]
--Asking this NPC about nearby settlements.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Topic unlocks.
	WD_SetProperty("Unlock Topic", "Breanne", 1)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei:[E|Neutral] You mentioned there were other settlements near here.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Oh yeah, but they're not as big as the trading post.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: There's Breanne's place north-east of here.[SOFTBLOCK] She's real nice, but keeps to herself.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Then there's the Outland Farm across the hills up north.[SOFTBLOCK] Kind of a dump, but they're good to their crops.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I visit them all the time on my rounds.[SOFTBLOCK] You shouldn't have any trouble finding them.[BLOCK][CLEAR]")
	
end