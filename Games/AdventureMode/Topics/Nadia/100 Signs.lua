--[Signs]
--Asking this NPC about the signs throughout Evermoon.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei:[E|Neutral] Did you make all of those signs yourself?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Yep![SOFTBLOCK] Cap'n made it one of my first jobs once I started working here.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Most of the critters in the forest leave us Alraunes alone.[SOFTBLOCK] So long as we don't poke in their business, anyway.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: That means I get a lot of the jobs outside of the trading post, like putting up signs.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Did he approve of the jokes?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: There were no jokes on those signs.[SOFTBLOCK] Not one.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Uhhh...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Hah, gotcha![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: I don't think he knows, or cares.[SOFTBLOCK] He's got bigger problems.[BLOCK][CLEAR]")
end