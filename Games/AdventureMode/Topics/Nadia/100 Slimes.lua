--[Slimes]
--Asking this NPC about slimes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--If Mei is not a slime...
	if(sMeiForm ~= "Slime") then
		WD_SetProperty("Append", "Mei:[E|Neutral] So if I run into a slime in the forest, what should I do?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Well I'll tell you what I'd do.[SOFTBLOCK] Leg it![SOFTBLOCK] Slimes aren't very fast.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] What if running isn't an option?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: That I'm less sure about.[SOFTBLOCK] I think some types of slime are poisonous.[SOFTBLOCK] They're not fast, so a big heavy strike would probably work well.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: You could ask the Cap'n if you want to know more, I'm sure he's got into a few scraps himself.[BLOCK][CLEAR]")
	
	--If Mei is a slime...
	else
		WD_SetProperty("Append", "Mei:[E|Neutral] Do you have any advice about slimes?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Always pay them compliments.[SOFTBLOCK] Your membrane is very shiny, by the way.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Thanks![SOFTBLOCK] I think...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Don't mention it![SOFTBLOCK] I think you're goo-reat![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Sheesh.[BLOCK][CLEAR]")
	
	end
end