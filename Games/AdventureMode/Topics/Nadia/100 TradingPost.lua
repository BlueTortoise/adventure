--[Trading Post]
--Asking this NPC about Trannadar Trading Post.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Topic unlocks.
	WD_SetProperty("Unlock Topic", "Settlements", 1)
	
	--Dialogue.
	WD_SetProperty("Append", "Mei:[E|Neutral] Can you tell me a little bit about this place?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: You're standing in front of Trannadar Trading Post.[SOFTBLOCK] That's the human name for it, anyway.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: It's the biggest human settlement for a month's walk.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Why here?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: Well we're on one of the main roads leading around the mountains to the south, so we get a lot of travellers.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Nadia: The merchants here give them meals and lodging for free, 'cause the shipping companies cover the bills.[SOFTBLOCK] There's always new people visiting, of all sorts![BLOCK][CLEAR]")
	
end