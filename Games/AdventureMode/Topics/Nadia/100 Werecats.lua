--[Werecats]
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
	
	--If Mei is a non-werecat...
	if(sMeiForm ~= "Werecat") then
		WD_SetProperty("Append", "Mei:[E|Neutral] Nadia, do you have any advice about the werecats?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: They're a bunch of big pussies.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] ..![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Ha ha![SOFTBLOCK] It's true, though.[SOFTBLOCK] I bet you could hit them with some catnip and stone them out of their minds.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I meant for fighting them...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Oh...[SOFTBLOCK] Well, quick strikes might help.[SOFTBLOCK] They're really fast, so you'd have to beat them at their own game.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: I bet the Cap'n can help you there.[SOFTBLOCK] He's tussled with a few.[BLOCK][CLEAR]")
	
	--If Mei is a werecat but didn't sex Nadia:
	elseif(iMeiHasDoneNadia == 0.0) then
		WD_SetProperty("Append", "Mei:[E|Neutral] Nadia, do you have any advice about fighting werecats?[SOFTBLOCK] It -[SOFTBLOCK] didn't go so well last I tried it.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Well, you cats are really quick in short bursts.[SOFTBLOCK] It might be a good idea to try to beat them at their own game.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] They're going to be so much more experienced on the hunt than I am...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: That's why you need to use your noodle![SOFTBLOCK] Pick the right attacks and you'll win for sure![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Since you use a sword, you should look for Fencer's Friend books.[SOFTBLOCK] I think there's one in the researcher's cabin north of here![BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Smirk] Oh, okay.[SOFTBLOCK] Thanks, Nadia![BLOCK][CLEAR]")
	
	--Mei is a werecat and made love to Nadia:
	else
	
		WD_SetProperty("Append", "Mei:[E|Neutral] Nadia, do you have any advice about -[SOFTBLOCK][EMOTION|Mei|Blush] oooohhhpuurrrr....[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Just scratch them behind the ear and they're yours...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] Uuunnnhhhh...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] I'd tell you to stop but it's sooooo gooodpuurrr...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Yeah, just like that.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] You're so good with your fingers...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: So are you...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Can we be professional for a second here?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Purr-[SOFTBLOCK]fessional?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] These claws are real...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: Aww.[SOFTBLOCK] Okay.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: I'd say you need to use quick strikes to hit quick targets.[SOFTBLOCK] That's my advice.[SOFTBLOCK] There are books with tips all over the place.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Nadia: I saw someone up at that crazy researcher's cabin earlier today.[SOFTBLOCK] I think they have a copy of Fencer's Friend you might want to look at.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Laugh] Purr...[SOFTBLOCK] thanks, Nadia![BLOCK][CLEAR]")
	end
end