--[Topic Script]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare")

WD_SetProperty("Append", "Christine:[E|Neutral] All right, T-junctions.[SOFTBLOCK] How do they work again?[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: So, the priorities are N, E, S, W, in that order.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: Pipes are flowing to...[SOFTBLOCK] where we're standing, basically.[SOFTBLOCK] Just above us there, you see?[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: When *Blue* or *Yellow* pipes reach a T-Junction, it flows in from two directions.[SOFTBLOCK] For example, East and West merging to the North.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: When this happens, East is before West in the priority list of N E S W.[SOFTBLOCK] Therefore, the result is the pressure from East minus the pressure from West.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Okay.[SOFTBLOCK] I think I get that.[BLOCK][CLEAR]")