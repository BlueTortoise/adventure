--[Topic Script]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare")

WD_SetProperty("Append", "Christine:[E|Neutral] Can you quickly tell me how the pipes work again?[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: Sure thing.[SOFTBLOCK] I have the manual downloaded to my memory buffers...[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: In *Red* pipes, fluid pressure is *Added* together as it flows, including at T-junctions.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: In *Blue* or *Yellow* pipes, fluid pressure is *Added* as it flows, but at a T-junction, *Subtracted*.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: In the *Violet* pipes, it's *Added* as it flows, and *Multiplied* at a T-Junction.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Great, thanks for reminding me.[BLOCK][CLEAR]")