--[Topic Script]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare")

WD_SetProperty("Append", "Christine:[E|Neutral] What are my pressure goals for each colour of pipe?[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The target for the Red pipes is 27psi.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The target for the Blue pipes is 16psi.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The target for the Violet pipes is 39psi.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The target for the Yellow pipes is 25psi.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: You can check the pressures in all the pipes at the terminals in this area.[SOFTBLOCK] You can adjust the pipes at locations where there's a red valve.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Thanks, good to know.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The valves are configured to have specific pressures they can work at.[SOFTBLOCK] So not every combination is possible.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: And...[SOFTBLOCK] some of the pressures are negative.[SOFTBLOCK] So pay attention to that.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Well, that'd make...[SOFTBLOCK] wait.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Why are they set for specific pressures?[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: I didn't build this mess, I just maintain it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] *Sigh*[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: You can vocalize that sentiment again...[BLOCK][CLEAR]")