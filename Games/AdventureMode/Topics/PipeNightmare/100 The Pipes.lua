--[Topic Script]
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare")

WD_SetProperty("Append", "Christine:[E|Neutral] So let's hear the basics.[SOFTBLOCK] How do the pipes work?[SOFTBLOCK] Why is it so hard to fix them?[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: Okay, so.[SOFTBLOCK] Pipes.[SOFTBLOCK] Simple metal or plastic things, gets compressed fluids from one side to the other.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: But because the engineers who built Regulus City just couldn't help themselves, they made the system needlessly complex.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: In *Red* pipes, fluid pressure is *Added* together as it flows.[SOFTBLOCK] That's pretty easy.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: In *Blue* or *Yellow* pipes, fluid pressure is *Added* as it flows, but at a T-junction, *Subtracted*.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] How?[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: Oh goodness I don't know.[SOFTBLOCK] I've downloaded the schematics and technical papers but it's too dense for me.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: My poor robot brain...[SOFTBLOCK] I wonder what sort of chance an organic would even have?[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: And finally, in the *Violet* pipes, it's *Added* as it flows, and *Multiplied* at a T-Junction.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] But, okay, physical impossibilities aside...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] In the subtraction cases, how do the pipes determine which side gets subtracted from which?[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The priority is N, E, S, W, in that order.[SOFTBLOCK] The pipes are always flowing towards the stations here, at the north end.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: So if a T-junction meets two pipes, from South and East, and merges going North, the formula is East Minus South -[SOFTBLOCK] since East is higher priority.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] (This is total nonsense. Maybe I should ask her again and write it down somewhere.)[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: If you want to adjust the pressure in a pipe, look for the red valves.[SOFTBLOCK] They're configured to allow certain combinations.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: I've rigged up the terminals in this area so they can quickly report the steam pressures.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The target for the Red pipes is 27psi.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The target for the Blue pipes is 16psi.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The target for the Violet pipes is 39psi.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: The target for the Yellow pipes is 25psi.[BLOCK][CLEAR]")
WD_SetProperty("Append", "565102: If you think you've got it all figured out, let me know.[SOFTBLOCK] I have some work credits for you, and some other stuff you might like.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] This will test my skills, but a real repair unit never gives up![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] I'll have it fixed before you know it![BLOCK][CLEAR]")

WD_SetProperty("Unlock Topic", "Properties", 1)
WD_SetProperty("Unlock Topic", "Priorities", 1)
WD_SetProperty("Unlock Topic", "Targets", 1)
WD_SetProperty("Unlock Topic", "Solution", 1)