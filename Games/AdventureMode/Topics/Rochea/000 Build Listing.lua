--[Build Listing]
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Rochea"

--Topic listing.
fnConstructTopic("Alraunes",        "Alraunes",         -1, sNPCName, 0)
fnConstructTopic("Bees",            "Bees",             -1, sNPCName, 0)
fnConstructTopic("CleansingFungus", "Cleansing Fungus", -1, sNPCName, 0)
fnConstructTopic("Cultists",        "Cultists",         -1, sNPCName, 0)
fnConstructTopic("Florentina",      "Florentina",       -1, sNPCName, 0)
fnConstructTopic("Rochea",          "Rochea",           -1, sNPCName, 0)
fnConstructTopic("Nadia",           "Nadia",            -1, sNPCName, 0)
fnConstructTopic("Name",            "Name",             -1, sNPCName, 0)
fnConstructTopic("Slimes",          "Slimes",           -1, sNPCName, 0)