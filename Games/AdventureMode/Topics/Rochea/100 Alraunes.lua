--[Alraunes]
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables:
	local iMeiVolunteeredToAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N")
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei: What does it mean to be an Alraune?[SOFTBLOCK] What is our purpose?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: A good question![SOFTBLOCK] When you were a human, were you certain of your purpose?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] Ah, well, I suppose not.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: I'm afraid I can give no answers on such deep subjects.[SOFTBLOCK] I can tell you that our way is one of peace and harmony.[BLOCK][CLEAR]")
	
	if(iMeiVolunteeredToAlraune == 0.0) then
		WD_SetProperty("Append", "Mei:[E|Neutral] Is that so?[SOFTBLOCK] I seem to recall you attacking me.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] Not that it's a problem.[SOFTBLOCK] I hold no grudges.[SOFTBLOCK] You were in the right.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: Unfortunately, there are many humans who view us and nature herself with utter contempt.[SOFTBLOCK] Reasoning with them failed, so we resort to violence now.[SOFTBLOCK] I apologize that it came to that.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I understand.[SOFTBLOCK] If all humans became leaf-sisters, there would be no need for violence.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: Then you understand our philosophy with clarity.[SOFTBLOCK] Good.[BLOCK][CLEAR]")
	else
		WD_SetProperty("Append", "Mei:[E|Sad] If only the humans agreed...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: It is a shame that so many humans view us with contempt.[SOFTBLOCK] Reasoning with them has failed, so we resort to violence now.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] I understand completely.[SOFTBLOCK] I would join every one of them if I could.[SOFTBLOCK] There would be no need for violence, then.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Blush] Perhaps, someday, we will reach that unity...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: Then you understand our philosophy with clarity.[SOFTBLOCK] Good.[BLOCK][CLEAR]")
	end
end