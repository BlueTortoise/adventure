--[Bees]
--Asking this NPC about Bee Girls in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei: The bees seem to pay us a great deal of attention more than the humans.[SOFTBLOCK] Is there a reason for that?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Ah, the bees.[SOFTBLOCK] We are one of the sweetest sources of nectar for them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] They want my nectar?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] ... Is that bad?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: It's part of the natural cycle.[SOFTBLOCK] They are not normally aggressive, but in lean times the bees will take nectar when they cannot find it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: We can find no more fault with them than we find fault with a storm on the horizon.[SOFTBLOCK] Nature bears no malice.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I see.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Even so, be on your guard.[BLOCK][CLEAR]")
end