--[Cleansing Fungus]
--Concerns the fungus that Alraunes use to forget their old lives.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei: Could you tell me a little more about the cleansing fungus?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Ah, yes, the fungus.[SOFTBLOCK] It is a gift that nature has given her followers.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: When you are ready, we will prepare a special chamber where the fungus will grow.[SOFTBLOCK] You will stay in it for three full days.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: The spores will absorb your hatred, your malice, the things which make up your humanity.[SOFTBLOCK] When you breathe them in, they will purify you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] My memories? Will they be gone too?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: ...[SOFTBLOCK] Yes.[SOFTBLOCK] You have left your humanity behind physically, but not mentally.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea:[EMOTION|Mei|Neutral] If you are not ready yet, we will wait until you are.[BLOCK][CLEAR]")
end