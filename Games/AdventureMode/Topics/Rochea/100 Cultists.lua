--[Rochea]
--Asking Rochea about herself.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local iCompletedTrapDungeon        = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	local iSeenAlrauneBattleIntroScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N")
	
	--Initial case:
	if(iSeenAlrauneBattleIntroScene == 0.0) then
		WD_SetProperty("Append", "Mei:[E|Neutral] What do you know of the cultists, and that mansion near here?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: The mansion has cultists within it?[SOFTBLOCK] Human cultists?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Surprise] Is that news? [SOFTBLOCK]You did not know?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: I'm afraid not.[SOFTBLOCK] I have not heard whispers from the little ones near there, I had assumed they decided to play a prank.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: Did they have any markings that might distinguish them?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] They were thugs who wore tattered cloaks and rusty chains.[SOFTBLOCK] The cloaks had hand imprints crudely scrawled on them.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: I've never heard of such markings...[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: I will consult with the other sisters and grandfathers.[SOFTBLOCK] I'm afraid I cannot help you more than that.[BLOCK][CLEAR]")
	
	--Met the Alraunes but haven't completed the dungeon yet:
	elseif(iSeenAlrauneBattleIntroScene == 1.0 and iCompletedTrapDungeon == 0.0) then
		WD_SetProperty("Append", "Mei:[E|Neutral] Has there been any update on the front?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: The daughters of the wild are holding the cult at bay.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Florentina and I are on the job, don't worry.[SOFTBLOCK] We just needed some more supplies.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: With the help of the little ones, we can hold the cult back, but not forever.[SOFTBLOCK] Please, get what you need and stop the ritual.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] You can count on us, leaf-sister Rochea![BLOCK][CLEAR]")
	
	--Met the Alraunes and completed the dungeon:
	else
		WD_SetProperty("Append", "Mei:[E|Neutral] Leaf-sister Rochea...[SOFTBLOCK] have you reconsidered?[SOFTBLOCK] Will you talk to Blythe about an alliance?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: I am sure we can defeat the cultists without resorting to aid from a human.[SOFTBLOCK] They will use the first opportunity to betray us.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK] I can't say you're not right.[SOFTBLOCK] I think you are.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Perhaps...[SOFTBLOCK] perhaps you could do the same in reverse?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: What are you proposing?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Offended] Betray the humans before they betray us.[SOFTBLOCK] Simple.[SOFTBLOCK] Use them in fighting the cult.[SOFTBLOCK] When they are weak, strike quickly.[SOFTBLOCK] Forcibly join them.[SOFTBLOCK] Then they will help us forever.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: To be honest, I had considered that.[SOFTBLOCK] But, isn't it a human thing to betray one's allies?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Happy] When there are no humans, there will be no betrayal.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: Wise.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: I will consult with the other leaf-sisters.[SOFTBLOCK] Your proposal is indeed a valid option.[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Glad to help.[SOFTBLOCK] Nature will triumph in the end.[BLOCK][CLEAR]")
	
	end
end