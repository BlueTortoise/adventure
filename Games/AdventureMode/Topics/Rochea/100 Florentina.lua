--[Florentina]
--Dialogue.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] About Florentina...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Hmm, a name that is uttered rarely here.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I mean no offense.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Of course.[SOFTBLOCK] I know her, and she knows me.[SOFTBLOCK] Her tongue is barbed and her form disfigured.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: These things reflect her soul.[SOFTBLOCK] She hides great pain beneath her stern facade.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: I suspect she has not cleansed herself, and thus she will not be one of us until she does.[SOFTBLOCK] She must let go of that pain.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] I haven't been cleansed, either, but you still tolerate me.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: True, but your soul yearns for peace.[SOFTBLOCK] I don't think hers does.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: To bind herself to ephemeral relationships is what keeps her humanity intact.[SOFTBLOCK] That is not the way of the Alraune.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: ...[SOFTBLOCK] I am being too judgemental.[SOFTBLOCK] She will find her way in time.[BLOCK][CLEAR]")
end