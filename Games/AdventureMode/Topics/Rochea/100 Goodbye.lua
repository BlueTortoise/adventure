--[Goodbye]
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)
WD_SetProperty("Append", "Rochea: The little ones will guide you, leaf-sister.")