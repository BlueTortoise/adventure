--[Nadia]
--Dialogue.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] What is your opinion of Nadia?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: She is a terrible influence on the little ones![SOFTBLOCK] Just yesterday, they told me the worst joke...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Laugh] Uh oh, here we go.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: It is too horrible to utter here, but it involved celery and following someone without their knowledge.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Other than that, I fear she is misguided.[SOFTBLOCK] She does not merely tolerate humans, but seems to trust them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Our experience shows that to be a path leading to needless destruction.[SOFTBLOCK] They will turn on her, it is their nature.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: I only hope she comes to her senses before that happens and rejoins our union.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] You really think Blythe would betray her?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: I know he will, for certain.[SOFTBLOCK] The hearts of men are dark indeed.[BLOCK][CLEAR]")
end