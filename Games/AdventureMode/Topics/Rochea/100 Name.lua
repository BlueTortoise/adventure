--[Name]
--Asking this NPC about their name. A lot of NPCs have this. Sometimes it's asking them what their name is, sometimes
-- it's asking them what their name means.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Dialogue.
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei: Your name, Rochea.[SOFTBLOCK] Was it yours before you became a leaf-sister?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: No, I do not remember my name before then.[SOFTBLOCK] It is a name I took afterwards.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Sad] When I consume the cleansing fungus...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Your own name will be as foreign to you as mine was to me.[SOFTBLOCK] We will choose a new name as befits you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Apprehension is common.[SOFTBLOCK] If it comforts you, know that all of us felt it before we consumed the fungus.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] It does help, thank you.[BLOCK][CLEAR]")
end