--[Rochea]
--Asking Rochea about herself.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] Are you the leader here, Rochea?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Mmm, leaf-sister, we have no leaders.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] My apologies.[SOFTBLOCK]  I did not mean to offend.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: It is your lingering humanity that assumes all beings fit into some hierarchy.[SOFTBLOCK] Is a tree superior to a blade of grass?[SOFTBLOCK] Shall we measure the worth of a vine by its length?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Laugh] No plant need have its worth measured![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Then you understand why your question was silly.[SOFTBLOCK] You are young yet, and we are patient.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Though, in a manner, I am what a human would call a leader.[SOFTBLOCK] I am in charge of joining new leaf-sisters, because I happen to be very skilled at it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: I do my best to make the process as painless and relaxing as possible.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] It was wonderful.[SOFTBLOCK] It's a shame I can only experience it once.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Perhaps later I can guide you through your first joining.[SOFTBLOCK] You will need to bring a human, of course.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] That will have to wait.[SOFTBLOCK] Thank you for the offer, though.[BLOCK][CLEAR]")
end