--[Slimes]
--Asking Rochea about slimes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] The slimes I've seen in the forest.[SOFTBLOCK] Are they like us?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: No, they are closer to animals than plants in most ways.[SOFTBLOCK] They hunt for wounded or unsuspecting creatures and consume them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: They are more like carrion feeders in most cases, and not much of a threat.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] What of the ones that are humanoids?[SOFTBLOCK] They are certainly dangerous.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Oh, I see you've encountered one in heat.[SOFTBLOCK] I suppose we are nearing mating season for them.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Blush] In h-h-heat?[SOFTBLOCK] Really?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Yes, slimes take on human form when they seek to reproduce.[SOFTBLOCK] They then look for humans to convert into new slimes, like them.[SOFTBLOCK] Otherwise, they rarely attack humans.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Rochea: Slimes are not particularly intelligent, and may confuse us for humans sometimes.[SOFTBLOCK] Do not worry, merely give them a few raps on the membrane and they should leave you be.[BLOCK][CLEAR]")
	
	--Dialogue changes slightly if Mei has the slime TF.
	local iHasSlimeForm = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N")
	if(iHasSlimeForm == 0) then
		WD_SetProperty("Append", "Mei:[E|Happy] I'll try to be gentle.[BLOCK][CLEAR]")
	else
		WD_SetProperty("Append", "Mei:[E|Blush] (It sought to mate with me...[SOFTBLOCK]  mmmm...)[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Rochea: Are you all right, leaf-sister?[BLOCK][CLEAR]")
		WD_SetProperty("Append", "Mei:[E|Neutral] Hm?[SOFTBLOCK] Oh, yes.[SOFTBLOCK] Just, lost in thought.[BLOCK][CLEAR]")
	end
end