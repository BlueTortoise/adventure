--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

WD_SetProperty("Append", "SX-399:[E|Neutral] Okay, this is a bit of a personal question, boss.[SOFTBLOCK] You're a bit better at this than I am.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Go ahead.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] So uh, how do I...[SOFTBLOCK] initiate?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] With 55?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Blush] Y-[SOFTBLOCK]yeah...[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Blush] I'm kind of new...[SOFTBLOCK] at this...[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Happy] I get so pumped up when I talk to her and then I try to make a move and -[SOFTBLOCK] phew![BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] But I don't know if I'm doing it wrong because she gets upset when I try to hold her hand and she never kisses me.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Oh, all right.[SOFTBLOCK] I see the problem.[SOFTBLOCK] You're actually doing things pretty much right.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Hold her hand, talk to her.[SOFTBLOCK] That's a good starting point.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Then, you look at her and state,[SOFTBLOCK] 'I would like to kiss you now.'[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] Not very hot, Christine.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] She literally doesn't know what to do, so it feels awkward for her.[SOFTBLOCK] After doing this a few times, she'll figure out what follows from what.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Once you've got her kissing you without you needing to state it, I'm going to say get her to fondle your torsal chassis.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] But does she want to?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] She probably wants to touch you all over, but is too scared.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Blush] Me too![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Work up to it, adding to your routine each time.[SOFTBLOCK] She wants to kiss you, but she wants to kiss you right.[SOFTBLOCK] Teach her how to kiss you right.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Don't be in a hurry, you'll get to the good parts soon enough.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Blush] Kissing is the good part![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] Oh it gets so much better...[BLOCK][CLEAR]")