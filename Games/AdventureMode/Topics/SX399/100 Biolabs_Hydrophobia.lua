--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

WD_SetProperty("Append", "Christine:[E|Neutral] You're a bit more experienced at being a steam droid than I am.[SOFTBLOCK] What's this about pools of water being dangerous?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] Well, falling into a pool of water isn't exactly a common occurrence on Regulus, what with there not being any oceans or lakes.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] The biolabs are where the habitation complexes used to be.[SOFTBLOCK] When we all became steam droids, they banned boating and required special equipment to do any underwater work.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] Steam droid actuators use heat differentials to do work, so if we lose temperature, we lose energy.[SOFTBLOCK] It actually makes us very efficient in a vacuum, but underwater...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I see.[SOFTBLOCK] But your steam lord body?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] In addition to using a lot more electrical power systems in the limbs to greatly increase efficiency, my chassis is up-armored with multiple layers of vac plating.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] Apparently golems do this as a matter of course, but for us steam droids?[SOFTBLOCK] I could go underwater and barely notice.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Lovely![BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] For 55, though, I don't think any amount of protection is going to help her.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I'd say so, too.[SOFTBLOCK] I'll have to make sure not to assign her to any sort of commando work on water treatment facilities.[BLOCK][CLEAR]")