--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

WD_SetProperty("Append", "SX-399:[E|Neutral] Tactical question, Christine.[SOFTBLOCK] Would you happen to know the melting point of latex?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] 180C, I believe.[SOFTBLOCK] Though keep in mind the drone units aren't actually latex, they're ceramoweave with a rubbery coating.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] Okay, so melting point of that?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I think it's around 1200C, though its structual integrity drops precipitously at 800C.[SOFTBLOCK] Why?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] No reason.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] I didn't think you'd be so casual about the violence inherent in melting a drone unit, SX-399.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Blush] Wh-[SOFTBLOCK]what?[SOFTBLOCK] You've got me all wrong, Christine![BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] I don't want to melt *myself*.[SOFTBLOCK] I want to make sure I'd be able to withstand the latent heat from my weapons.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Excuse me?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] I've been looking into part interoperability due to my upgrade.[SOFTBLOCK] Obviously making more steam lords exactly like me will be a huge help to the war effort.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] Unfortunately it seems like I'm going to be the only one exactly like me.[SOFTBLOCK] Some of the stuff Sophie did isn't going to scale up.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] But then I found out I'm compatible with drone unit parts, and I could actually become one someday if we found a way to rework my power core![BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Happy] I could be a bubbly steam latex drone![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I am understanding the words you're saying but I'm still confused.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Flirt] 55 said it'd be a great idea.[SOFTBLOCK] I asked her why and she started talking about tactics and stuff.[SOFTBLOCK] That's how you know it's personal with her.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Being rubbery is pretty great, I admit.[SOFTBLOCK] It's nice to be encased and...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] Simplified...[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] Speaking of, if you have any advice on getting her...[SOFTBLOCK] attention...[BLOCK][CLEAR]")

WD_SetProperty("Unlock Topic", "Biolabs_Attention", 1)