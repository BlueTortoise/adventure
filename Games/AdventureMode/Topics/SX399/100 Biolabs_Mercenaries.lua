--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

WD_SetProperty("Append", "SX-399:[E|Neutral] Hey, out of curiosity, how do mercenaries work?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Mercenaries are soldiers for hire.[SOFTBLOCK] They fight wars when paid to, or do other things soldiers do.[SOFTBLOCK] Why?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] I heard some of the steam droid groups mother is talking to were 'mercenaries' but I didn't ask what she meant by that.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I guess some of the steam droids want to be paid to fight for their freedom.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Time to be frank.[SOFTBLOCK] Mercenaries are scum.[SOFTBLOCK] They kill people for money.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] On Regulus, things are terrible and everyone except a select few are suffering.[SOFTBLOCK] To be able to fight to end that, and demand payment for it, is unconscionable.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Angry] But why?[SOFTBLOCK] How can they act like everything is fine in the mines?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Oh, that's easy.[SOFTBLOCK] They know we're desperate, and they know we'll pay.[SOFTBLOCK] And I've spoken to your mother -[SOFTBLOCK] we'll pay.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Angry] You'd pay them?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] The steam droids you're talking about are some of the best equipped soldiers we'll have.[SOFTBLOCK] They bring their own weapons and have decades of experience in the mines.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] The motivation for freedom does a lot, but having a gun and knowing how to duck does a lot more.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] ...[SOFTBLOCK] My own people would do that...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] If we can capture the teleportarium complex, we could even try hiring mercenaries from Pandemonium.[SOFTBLOCK] Not sure how well they'd do, but we could do it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] 55 said the people of Pandemonium have just figured out repeater rifles, and firearms are expensive.[SOFTBLOCK] We could probably buy a mercenary company for three pulse rifles and a wink.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] Can they survive in a vacuum?[SOFTBLOCK] Won't they get gunned down by the Administration's forces?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] The one advantage of mercenaries is that you don't have to care if they die.[SOFTBLOCK] They're here for money.[SOFTBLOCK] Screw 'em.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Besides, wouldn't it be so fun to have demons or spidergirls here on Regulus?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] Are demons actually real?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Happy] I hope so![SOFTBLOCK] Then I can turn into one![BLOCK][CLEAR]")
