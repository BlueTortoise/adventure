--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

WD_SetProperty("Append", "SX-399:[E|Neutral] Hey Christine, question for you.[SOFTBLOCK] You know how organic matter works, right?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] You don't?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] I haven't been organic for over a century now.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] I guess there's that...[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] So plants and grass and stuff -[SOFTBLOCK] they ignite, right?[SOFTBLOCK] I could set this whole place on fire?[SOFTBLOCK] There's enough oxygen for that?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Sad] Why are you smiling when you say that?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] Sorry, it was just the idea...[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Happy] Torch the whole place and let the creepy-crawlies figure out how fire works![SOFTBLOCK] That'll show those dunderheads![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] It'll also show all the innocent organic wildlife.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Besides, I'm pretty sure the biolabs have automatic fire suppression systems.[SOFTBLOCK] Whether or not they'll work with all the staff evacuated and the power systems broken, I don't know.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] I got carried away.[SOFTBLOCK] I'll watch my background and make sure not to toast any innocent critters.[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Smirk] On the topic -[SOFTBLOCK] look at the birds and the squirrels![SOFTBLOCK] Aren't they so keen?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] It's the one thing I miss working in sector 96.[SOFTBLOCK] Cute animals are the best.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] Maybe we'll find a way to mechanize cute animals someday?[BLOCK][CLEAR]")
WD_SetProperty("Append", "SX-399:[E|Neutral] I think their not being robots is part of the appeal.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] True enough.[BLOCK][CLEAR]")