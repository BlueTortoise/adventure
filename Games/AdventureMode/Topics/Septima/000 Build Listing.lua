--[Build Listing]
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Septima"

--Topic listing.
fnConstructTopic("Name",            "Name",            -1, sNPCName, 0)
fnConstructTopic("NewRilmani",      "New Rilmani",     -1, sNPCName, 0)
fnConstructTopic("PrimaryWords",    "Primary Words",   -1, sNPCName, 0)
fnConstructTopic("Rilmani",         "Rilmani",         -1, sNPCName, 0)
fnConstructTopic("RilmaniLanguage", "Language",        -1, sNPCName, 0)
fnConstructTopic("Runestone",       "Runestone",       -1, sNPCName, 0)
fnConstructTopic("StillPlane",      "The Still Plane", -1, sNPCName, 0)
fnConstructTopic("Voidwalker",      "Voidwalker",      -1, sNPCName, 0)