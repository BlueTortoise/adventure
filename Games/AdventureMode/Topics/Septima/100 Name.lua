--[Name]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] Could you tell me a bit more about yourself?[SOFTBLOCK] Why are you named Septima?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: I am the seventh Rilmani of the first hundred created.[SOFTBLOCK] There were six before me, and their names were in order as mine is.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] But didn't you say you were the oldest?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: I am.[SOFTBLOCK] The ones that preceded me no longer live.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Oh...[SOFTBLOCK] I'm sorry...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: They fulfilled the purpose they were created for.[SOFTBLOCK] They died at the moment they were required to die.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: But thank you for your concern, nonetheless.[BLOCK][CLEAR]")
end