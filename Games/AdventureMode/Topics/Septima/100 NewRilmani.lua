--[Language]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] Do you Rilmani create new ones the same way as the other monster girls do?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Yes, we do.[SOFTBLOCK] Sometimes we perish due to events we cannot control.[SOFTBLOCK] The fabric of reality must be maintained, so we require replacements.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: When that happens, we convert humans to our cause.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] By force?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: If necessary, yes.[SOFTBLOCK] The candidates we select usually assent once they understand the gravity of the situation.[SOFTBLOCK] Some resist.[SOFTBLOCK] We convert them anyway.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] And you don't see a problem with that?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: No.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] Ugh.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: The purpose we have is higher than any trivial mortal concern could ever be.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: When the time is right, you, too, will become Rilmani.[SOFTBLOCK] This is unavoidable.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: That time is not now.[SOFTBLOCK] Your runestone will preserve your self because your form must change for your purpose.[SOFTBLOCK] Ours is merely one of many you will need.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: But you [SOFTBLOCK]*will*[SOFTBLOCK] become Rilmani.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] No point getting upset about it now, I guess...[BLOCK][CLEAR]")
end