--[Primary Words]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] The Rilmani language guide says that there are six primary words.[SOFTBLOCK] My runestone has one of them.[SOFTBLOCK] Why?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Your runestone bears your name.[SOFTBLOCK] It is part of you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: The Rilmani word for bravery is Mei.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Really?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Yes, it is one of several synonyms for the concept.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] But -[SOFTBLOCK] how am I able to speak to you, then?[SOFTBLOCK] I thought you were speaking - [SOFTBLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Your Rilmani tongue is flawless.[SOFTBLOCK] You speak it naturally as if you had grown up immersed by it.[SOFTBLOCK] You, obviously, did not.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Surprise] I'm speaking Rilmani right now?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Indeed.[SOFTBLOCK] Your diction is excellent.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: It is necessary for your purpose that you not struggle with our language, so you do not.[SOFTBLOCK] Did you not find it odd that you could speak to the inhabitants of Pandemonium?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Even if you had been able to, certainly you'd not be able to read their words.[SOFTBLOCK] Yet you did.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Ugh, does that mean I didn't even need to find that language guide?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Most amusing.[SOFTBLOCK] If only you had known that you merely needed to translate the symbols to activate the portal.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] Could you please smile when you say something like 'most amusing'?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: To smile is most human.[SOFTBLOCK] I find that enjoyable, even if it is beyond me.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] So what are the other primary words, then?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: They are Mei, Sanya, Jeanne, Lotta, Christine, and Talia.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Those are all names from Earth...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: There are five other bearers.[SOFTBLOCK] You are the only one we are aware of, as that was the strand of your destiny.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: When the time comes, we have no doubt you and the other bearers will unite as one.[SOFTBLOCK] We will prepare you until that time comes.[BLOCK][CLEAR]")
end