--[Rilmani]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] So what exactly are the Rilmani?[SOFTBLOCK] How are you different from the other partirhumans?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: We are the unborn.[SOFTBLOCK] We were created at the moment that reality was rent in five.[SOFTBLOCK] Our duty is to maintain the fabric of the universe.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: You, like us, are four-dimensional.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Me?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: We Rilmani travel between the manifolds just as a three-dimensional being climbs a hill.[SOFTBLOCK] With effort, certainly, but no less impossible to perceive for a two-dimensional being.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: You possess this ability as well, though you cannot perceive it.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Have you not noticed that, when you are defeated and in grave danger, you awaken safely near a dimensional low point?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] The campfires...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: There are many low points in reality.[SOFTBLOCK] You can feel them, just as you can feel certain things without knowing them directly.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: You have much in common with us.[SOFTBLOCK] We are kindred spirits.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] That...[SOFTBLOCK] explains a lot, but opens up so many more questions...[BLOCK][CLEAR]")
end