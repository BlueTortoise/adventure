--[Language]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] So what's this about the Rilmani language being discovered?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Rilmani language is something of a misnomer, as we are not its originators but merely its users.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: The language has existed as long as we have, and we are imprinted with a perfect knowledge of it.[SOFTBLOCK] All newly created Rilmani learn it immediately as well.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: However, there are many words that we do not know the meaning of, yet.[SOFTBLOCK] Once we encounter a new concept, the corresponding word's meaning becomes clear.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] So how is that different from inventing a word?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: All Rilmani will know intuitively the new concept and the full extent of its meanings, even if it is their first time hearing the word.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] But what if you encounter an idea that you don't have a word for?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: That has not happened yet, and presumably it will not.[SOFTBLOCK] The language was created by the True God who likewise imprinted us with it.[SOFTBLOCK] It is the language of reality itself.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Perhaps someday there will be a concept we cannot describe.[SOFTBLOCK] If that does happen...[SOFTBLOCK] perhaps we will have to borrow a human language?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Laugh] We're happy to share![BLOCK][CLEAR]")
end