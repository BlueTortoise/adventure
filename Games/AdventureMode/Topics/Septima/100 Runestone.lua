--[Runestone]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] So what's this runestone?[SOFTBLOCK] Did you make it?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: No.[SOFTBLOCK] The runestone you bear has existed since several seconds before the first Rilmani was created.[SOFTBLOCK] It is tied to your destiny, not ours.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Unfortunately, its true nature eludes us.[SOFTBLOCK] We do not know the words that will describe it fully.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] But it has your symbol on it![BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: You are incorrect, that is not our symbol.[SOFTBLOCK] That is *your* symbol, one that has been yours longer than I have existed.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: The runestone is tied to you, indeed, it is part of you.[SOFTBLOCK] It is an extension of your self.[SOFTBLOCK] Its true nature is known to you and has always been so, but you may not be capable of understanding it yet.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: It is part of the duties of the Rilmani to guide you to that understanding, though we will not be able to share it with you.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well, it's come in handy before, so I guess I'll hang onto it a little longer...[BLOCK][CLEAR]")
end