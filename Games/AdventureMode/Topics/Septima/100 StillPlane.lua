--[The Still Plane]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] Just what is 'The Still Plane'?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: How to put this...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: It would be what you call a dimension, even though it is not, truly.[SOFTBLOCK] There is one universe, split into five regions that do not,[SOFTBLOCK] cannot,[SOFTBLOCK] touch one another.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: This was done by folding their space, such that they are effectively five dimensions of the same universe.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Uh, okay...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Perhaps it would be easier if you thought of them as separate.[SOFTBLOCK] I cannot say.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: The Still Plane is the furthest from the other four.[SOFTBLOCK] It is the one that contains Earth, Sol, The Milky Way...[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Everything you have ever perceived, including the physical laws, is local only to The Still Plane.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] So Pandemonium is its own plane?[SOFTBLOCK] Not even in the same universe?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Correct.[SOFTBLOCK] Nix Nedar is between the dimensional folds.[SOFTBLOCK] It consumes no space.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Well I guess that'll do for an answer...[BLOCK][CLEAR]")
end