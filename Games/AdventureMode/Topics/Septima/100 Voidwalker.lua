--[Voidwalker]
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	WD_SetProperty("Append", "Mei:[E|Neutral] Why do you keep referring to me as 'voidwalker'?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Because that is one of your names.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Happy] Heh.[SOFTBLOCK][EMOTION|Mei|Neutral] No, I mean, why is it one of my names?[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: You are the first human to cross the divide between Pandemonium and The Still Plane.[SOFTBLOCK] You have crossed a void even we Rilmani cannot cross.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: It seems I have not impressed upon you the magnitude of what you have accomplished.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Imagine a normal human is a creature standing on the ground, staring at the moon.[SOFTBLOCK] We Rilmani would be birds.[SOFTBLOCK] We soar easily overhead, we are above the ground, but to reach the moon would be just as impossible.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: You are one who has performed what would be impossible to anyone else.[SOFTBLOCK] You stood on the moon, with the aid of no tools.[SOFTBLOCK] You walk across the void.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Mei:[E|Neutral] Not intentionally.[BLOCK][CLEAR]")
	WD_SetProperty("Append", "Septima: Not intentionally?[SOFTBLOCK] Incorrect.[SOFTBLOCK] A better way of putting it would be::[SOFTBLOCK] Not consciously.[BLOCK][CLEAR]")
end