--[Build Listing]
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Sophie"
gsActiveNPCName = sNPCName

--Topic listing.
fnConstructTopicStd(false, "Biolabs_Biology",        "Biology")
fnConstructTopicStd(false, "Biolabs_Electrosprites", "Electrosprites")
fnConstructTopicStd(false, "Biolabs_Runestone",      "Runestone")
fnConstructTopicStd(false, "Biolabs_Tourism",        "Tourism")