--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sophie")

WD_SetProperty("Append", "Sophie:[E|Neutral] Hey Christine, want to hear a scary story?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] You can't scare me.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Neutral] You won't be saying that once I'm done.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Try me.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Smirk] Imagine working here, in the biolabs.[SOFTBLOCK] Fixing things that break down.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Smirk] Sand in the keyboards, organic gunk in the power sockets, rust from ambient water in the air...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Scared] Sophie, no![BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Happy] Moss and fungus growing inside a terminal![SOFTBLOCK] Scrubbing and rinsing to get it all out but it gets into the little corners and never comes out![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Scared] Stop![SOFTBLOCK] Stop, I give![SOFTBLOCK] It's too horrible!BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee![SOFTBLOCK] We have it easy in sector 96![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Remind me to never assume you don't mean exactly what you say...[BLOCK][CLEAR]")
