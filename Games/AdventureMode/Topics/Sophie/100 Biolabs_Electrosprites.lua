--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sophie")

WD_SetProperty("Append", "Sophie:[E|Neutral] Christine?[SOFTBLOCK] I was wondering what you can do when you're an electrosprite.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Oh, like zip through wires?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] 55 and I tried practicing it with Psue's help, but I'm not very good at it.[SOFTBLOCK] I could get a few meters before I got stuck and she had to get me out.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] I could probably figure it out but we ran out of time and had a lot of organizing to do.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Happy] Wow![SOFTBLOCK] You can use that to sneak around and spy on units, right?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] No, actually I can't hear anything when I'm in a wire.[SOFTBLOCK] Apparently you have to listen for soundwaves disrupting the electrical waves but it was just static to me.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Psue said she would love to help me practice so I can use it to sneak around and ambush the Administration goons, though![BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Happy] Cool![BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Smirk] But actually, I was meaning...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] You want me to hop inside you?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Blush] Yeah, maybe hijack my processes, make me do things...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Blush] Psue said I'm not ready for that...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] The other electrosprites tried it a few times, but you have to be very deeply in synchronicity with the unit you jump into or you'll probably just get kicked right back out.[SOFTBLOCK] That's why they usually just hop into their tandem units.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Offended] Why would you say we're not in sync, Christine?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Laugh] Your clock speed, silly![BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Happy] Oh, I get you![SOFTBLOCK] Hee hee![BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Happy] You keep practicing because I want you inside me so bad![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Laugh] Me too![BLOCK][CLEAR]")
