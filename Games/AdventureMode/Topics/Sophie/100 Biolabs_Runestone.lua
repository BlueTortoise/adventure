--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sophie")

WD_SetProperty("Append", "Christine:[E|Neutral] You're looking at my runestone, Sophie.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Neutral] I was just wondering what mine would be like if I had one.[SOFTBLOCK] Yours is purple, lets you transform and warp around...[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Blush] I think mine would be green and it would let me turn invisible so nobody can see me![BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] There are five others, and I have a feeling that all of them let their bearer transform and such.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Offended] Not sure how I know that, but we've got a war to win.[SOFTBLOCK] Figuring that out can wait.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Blush] But mine will be green, okay?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Naturally![SOFTBLOCK] It'd go lovely with your hair![BLOCK][CLEAR]")
