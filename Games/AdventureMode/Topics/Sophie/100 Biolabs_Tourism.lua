--[Topic Script]
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sophie")

WD_SetProperty("Append", "Sophie:[E|Smirk] Christine, the map you got is a tour guide, right?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Looks that way.[SOFTBLOCK] There are some doodles on here from whatever unit left it.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Maybe we could have visited if we got some spare work credits?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Neutral] After all that we've been through, are you really floating that as a possibility?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Neutral] Well, how about this.[SOFTBLOCK] We carry out a revolution, overthrow the Administration, and bring about freedom for the people of Regulus City.[BLOCK][CLEAR]")
WD_SetProperty("Append", "Christine:[E|Smirk] Once we make this place a robotic utopia, we'll save up some spare credits and [SOFTBLOCK]*then*[SOFTBLOCK] visit.[SOFTBLOCK] Sound better?[BLOCK][CLEAR]")
WD_SetProperty("Append", "Sophie:[E|Smirk] See, now you're being reasonable.[BLOCK][CLEAR]")
