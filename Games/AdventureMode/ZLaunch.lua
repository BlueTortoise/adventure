--[ =========================================== Launch ========================================== ]
--Launching script for Adventure Mode. If an argument is passed in, it will be the path of the save file to use.
gbAdventureBootDebug = false
if(gbAdventureBootDebug) then io.write("Launching Adventure Mode.\n") end
LM_StartTimer("ZLaunch")

gbIsLoadingSequence = false
gbOnlyBootSystemVars = false
gbNoOverlay = false
local sSavePath = "None"
local iArgsTotal = LM_GetNumOfArgs()
if(iArgsTotal >= 1) then
	gbIsLoadingSequence = true
	sSavePath = LM_GetScriptArgument(0)
end

--[Resolve Root Path]
gsRoot = VM_GetVar("Root/Paths/System/Startup/sAdventurePath", "S")

--[Audio Loading]
--Main audio loading.
if(gbAdventureBootDebug) then io.write(" Loading audio.\n") end
LM_ExecuteScript(gsRoot .. "Audio/ZRouting.lua")

--Selective audio loading.
gbLoadedCreditsTheme5 = false

--[ ======================================== Path Setup ========================================= ]
--Lua system strings. These strings are booted for chapter 1, other chapters override them.
if(gbAdventureBootDebug) then io.write(" Setting global strings.\n") end
gsAutoBoot = "Nowhere"
gsSaveHandler = gsRoot .. "Save Handler/000 Save Handler.lua"
gsDatafilesPath = gsRoot .. "Datafiles/"
gsMapDirectory = gsRoot .. "Maps/"
gsItemListing = gsRoot .. "Items/Item List.lua"
gsItemImageListing = gsRoot .. "Items/Item Image List.lua"
gsCatalystHandler = gsRoot .. "System/830 Catalyst Handler.lua"
gsLayeredTrackRouter = gsRoot .. "LayeredTrackProfiles/ZRouting.lua"
gsGemHandler = gsRoot .. "Items/Gem Upgrade Mapping.lua"
gsFieldAbilityListing = gsRoot .. "Field Abilities/"
gsStandardShadow = "Root/Images/Sprites/Shadows/Generic"
gsStandardGameOver = gsRoot .. "Chapter 1/Scenes/300 Standards/Defeat/Scene_Begin.lua"
gsStandardRetreat  = gsRoot .. "Chapter 1/Scenes/300 Standards/Retreat/Scene_Begin.lua"
gsStandardRevert   = gsRoot .. "Chapter 1/Scenes/300 Standards/Revert/Scene_Begin.lua"
gsStandardReliveBegin = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_Begin.lua"
gsStandardReliveEnd   = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_End.lua"
gsStandardWarpHandler = gsRoot .. "Maps/Z Standard/Standard Warp Handler.lua"
gsCharacterAutoresolve = gsRoot .. "CostumeHandlers/YCharacterAutoresolve.lua"
gsCostumeAutoresolve = gsRoot .. "CostumeHandlers/XFormAutoresolve.lua"
gsGemNameResolvePath = gsRoot .. "Items/Resolve Gem Name.lua"

--Skillbook Paths
gsMeiSkillbook        = gsRoot .. "Combat/Party/Mei/300 Skillbook Handler.lua"
gsFlorentinaSkillbook = gsRoot .. "Combat/Party/Florentina/300 Skillbook Handler.lua"
gsJeanneSkillbook     = gsRoot .. "Combat/Party/Jeanne/300 Skillbook Handler.lua"

--Combat Paths
gsStandardDoTPath  = gsRoot .. "Combat/Effects/Z Common/DamageOverTime.lua"
gsStandardStatPath = gsRoot .. "Combat/Effects/Z Common/StatMod.lua"
gsThreatEffectPath = gsRoot .. "Combat/Effects/Z Common/ThreatVs.lua"

--Minigame Paths
gsKPopPath = gsRoot .. "Minigames/KPopBeats/000 Initializer.lua"

--Field Ability, Nonstandard Path
gsFieldAbilityCheckPath = "Null"

--[Static Paths]
Save_SetSaveHandlerPath(gsSaveHandler)

--[Utility Functions]
LM_ExecuteScript(gsRoot .. "Subroutines/fnSubdivide.lua")

--[Datafile Listing]
--Build a list of all datafiles used here. These are not called immediately, they are in the 
-- loading paths.
local fnCreatePath = nil
local bUseLowResMode = OM_GetOption("LowResAdventureMode")
gsaDatafilePaths = {}

--Path adding function. If low-res is off, just returns the normal path.
if(bUseLowResMode == false) then
    fnCreatePath = function(psPath)
        return psPath
    end

--If low-res is on, returns the low-res file path for cases that have them.
else
    fnCreatePath = function(psPath)
        --Error check.
        if(psPath == nil) then return "NO PATH" end
        
        --Check if this is a portraits case:
        if(string.sub(psPath, 1, 10) == "Portraits_") then
            return "PortraitsLD_" .. string.sub(psPath, 11)
        
        --Scenes case:
        elseif(string.sub(psPath, 1, 7) == "AdvScn_") then
            return "AdvScnLD_" .. string.sub(psPath, 8)
        end
        
        --All other cases, no low-definition.
        return psPath

    end
end

--[Build List]
--Adventure Scenes.
gsaDatafilePaths.sScnCassandraTF = gsDatafilesPath .. fnCreatePath("AdvScn_CassandraTF.slf")
gsaDatafilePaths.sScnCh0Major    = gsDatafilesPath .. fnCreatePath("AdvScn_CH0Major.slf")
gsaDatafilePaths.sScnCh1Major    = gsDatafilesPath .. fnCreatePath("AdvScn_CH1Major.slf")
gsaDatafilePaths.sScnCh5Major    = gsDatafilesPath .. fnCreatePath("AdvScn_CH5Major.slf")
gsaDatafilePaths.sScnChristineTF = gsDatafilesPath .. fnCreatePath("AdvScn_ChristineTF.slf")
gsaDatafilePaths.sScnGalaDress   = gsDatafilesPath .. fnCreatePath("AdvScn_GalaDress.slf")
gsaDatafilePaths.sScnMeiRune     = gsDatafilesPath .. fnCreatePath("AdvScn_MeiRune.slf")
gsaDatafilePaths.sScnMeiTF       = gsDatafilesPath .. fnCreatePath("AdvScn_MeiTF.slf")

--Portraits.
gsaDatafilePaths.s55Path             = gsDatafilesPath .. fnCreatePath("Portraits_55.slf")
gsaDatafilePaths.s56Path             = gsDatafilesPath .. fnCreatePath("Portraits_56.slf")
gsaDatafilePaths.sAquilliaPath       = gsDatafilesPath .. fnCreatePath("Portraits_Aquillia.slf")
gsaDatafilePaths.sCassandraPath      = gsDatafilesPath .. fnCreatePath("Portraits_Cassandra.slf")
gsaDatafilePaths.sChapter0CombatPath = gsDatafilesPath .. fnCreatePath("Portraits_CH0Combat.slf")
gsaDatafilePaths.sChapter0EmotePath  = gsDatafilesPath .. fnCreatePath("Portraits_CH0Emote.slf")
gsaDatafilePaths.sChapter1CombatPath = gsDatafilesPath .. fnCreatePath("Portraits_CH1Combat.slf")
gsaDatafilePaths.sChapter1EmotePath  = gsDatafilesPath .. fnCreatePath("Portraits_CH1Emote.slf")
gsaDatafilePaths.sChapter5CombatPath = gsDatafilesPath .. fnCreatePath("Portraits_CH5Combat.slf")
gsaDatafilePaths.sChapter5EmotePath  = gsDatafilesPath .. fnCreatePath("Portraits_CH5Emote.slf")
gsaDatafilePaths.sChristinePath      = gsDatafilesPath .. fnCreatePath("Portraits_Christine.slf")
gsaDatafilePaths.sFlorentinaPath     = gsDatafilesPath .. fnCreatePath("Portraits_Florentina.slf")
gsaDatafilePaths.sJX101Path          = gsDatafilesPath .. fnCreatePath("Portraits_JX101.slf")
gsaDatafilePaths.sMaramPath          = gsDatafilesPath .. fnCreatePath("Portraits_Maram.slf")
gsaDatafilePaths.sMeiPath            = gsDatafilesPath .. fnCreatePath("Portraits_Mei.slf")
gsaDatafilePaths.sPDUPath            = gsDatafilesPath .. fnCreatePath("Portraits_PDU.slf")
gsaDatafilePaths.sSammyPath          = gsDatafilesPath .. fnCreatePath("Portraits_Sammy.slf")
gsaDatafilePaths.sSeptimaPath        = gsDatafilesPath .. fnCreatePath("Portraits_Septima.slf")
gsaDatafilePaths.sSophiePath         = gsDatafilesPath .. fnCreatePath("Portraits_Sophie.slf")
gsaDatafilePaths.sSX399Path          = gsDatafilesPath .. fnCreatePath("Portraits_SX399.slf")

--Special: Electrosprite Adventure Path
gsCallElectrospritePath = "Null"

local iElectrospriteGameIndex = 0
for i = 1, giGamesTotal, 1 do
    if(gsaGameEntries[i].sName == "Electrosprite Adventure") then
        iElectrospriteGameIndex = i
        break
    end
end
if(iElectrospriteGameIndex == 0 or iElectrospriteGameIndex == nil) then 
    gsCallElectrospritePath = "Null"
elseif(gsaGameEntries[iElectrospriteGameIndex].sActivePath == "Null") then 
    gsCallElectrospritePath = "Null"
else
    gsCallElectrospritePath = gsaGameEntries[iElectrospriteGameIndex].sActivePath
end

--Set system strings.
if(gbAdventureBootDebug) then io.write(" Setting system strings.\n") end
AL_SetProperty("Root Path", gsRoot)
AL_SetProperty("Item Path", gsItemListing)
AL_SetProperty("Item Image Path", gsItemImageListing)
AL_SetProperty("Catalyst Path", gsCatalystHandler)
AL_SetProperty("Layered Track Routing Path", gsLayeredTrackRouter)
AM_SetProperty("Party Resolve Script", gsRoot .. "CharacterDialogue/Z System Party Resolve.lua")
AM_SetProperty("Rest Resolve Script", gsRoot .. "RestingDialogues/Z Rest Resolve.lua")
AM_SetProperty("Warp Resolve Script", gsRoot .. "System/810 Assemble Warp List.lua")
AM_SetProperty("Warp Execute Script", gsRoot .. "System/811 Execute Warp.lua")
AM_SetProperty("Relive Resolve Script", gsRoot .. "System/820 Assemble Relive List.lua")
AM_SetProperty("Costume Resolve Script", gsRoot .. "CostumeHandlers/ZCostumeListBuilder.lua")
AM_SetProperty("Field Ability Resolve Script", gsRoot .. "Field Abilities/ZAssembleFieldAbilityList.lua")
TA_SetProperty("Standard Enemy Shadow", gsStandardShadow)
WD_SetProperty("Topic Directory", gsRoot .. "Topics/")
AM_SetFormProperty("Form Party Resolve Script", gsRoot .. "FormHandlers/Build Party List.lua")
AM_SetFormProperty("Form Resolve Script", gsRoot .. "FormHandlers/Form Resolver.lua")
ADebug_SetProperty("Set Profile Path", gsRoot .. "Combat/Stat Profiles/000 Build Stat Profiles.lua")
AdInv_SetProperty("Gem Name Resolve Path", gsRoot .. "Items/Resolve Gem Name.lua")

--Order the script to pre-build the lookups.
if(gbAdventureBootDebug) then io.write(" Building item image listing.\n") end
LM_ExecuteScript(gsItemImageListing, "Null")

--Function Paths
if(gbAdventureBootDebug) then io.write(" Setting function paths.\n") end
gzFunctionPaths = {}
gzFunctionPaths.sSetPartyDialogue         = gsRoot .. "Subroutines/Set Party Dialogue.lua"
gzFunctionPaths.sSetPartyDialogueOpposite = gsRoot .. "Subroutines/Set Party Dialogue Opposite.lua"

--[ ========================================== Clearing ========================================= ]
--Wipe the DataLibrary of any script variables. Create a new Script Variable set.
if(gbAdventureBootDebug) then io.write(" Wiping Data Library.\n") end
WD_SetProperty("Wipe Topic Data")
DL_Purge("Root/Variables/", false)
DL_AddPath("Root/Variables/")

--Clear the inventory of any outstanding items.
if(gbAdventureBootDebug) then io.write(" Clearing inventory.\n") end
AdInv_SetProperty("Clear")

--Wipe the level music.
if(gbAdventureBootDebug) then io.write(" Stopping music.\n") end
AL_SetProperty("Music", "Null")

--Build program lists which do not depend on functions.
if(gbAdventureBootDebug) then io.write(" Building character form listing.\n") end
LM_ExecuteScript(gsRoot .. "FormHandlers/Build Form List.lua")

--Build Variables
if(gbAdventureBootDebug) then io.write(" Setting variables.\n") end
LM_ExecuteScript(gsRoot .. "System/000 Variables.lua")
LM_ExecuteScript(gsRoot .. "System/002 Time Variables.lua")

--Set the cleaner script.
if(gbAdventureBootDebug) then io.write(" Setting cleaner script.\n") end
EM_SetCleanerScript(gsRoot .. "System/900 Cleaner.lua")

--[ ======================================== Subroutines ======================================== ]
--Execute subroutines which will be used in the rest of the construction sequence.
if(gbAdventureBootDebug) then io.write(" Building function listing.\n") end
gcfTicksToSeconds = 1.0 / 60.0
gcfSecondsToTicks = 60/0 / 1.0

--Create a list that holds all functions. This is used to reinitialize them with a keypress later.
gbIsBuildingFunctionPaths = true
gsaFunctionPaths = {}

LM_ExecuteScript(gsRoot .. "Subroutines/fnAddPartyMember.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnArgCheck.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnConstructTopic.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnConstructorFacing.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCountMeiTFs.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneBlocker.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneFace.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneInstruction.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneLayerDisabled.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneMergeParty.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneMove.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneMoveFace.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneSetFrame.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneTeleport.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnCutsceneWait.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnGetEntityPosition.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnIsCharacterPresent.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnLoadCharacterGraphics.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnPartyStopMovement.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnRemovePartyMember.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnResolveFolderName.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnResolveMapLocation.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnSetApartmentObjectGraphics.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnSetCharacterGraphics.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnStandardAbilitySounds.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnStandardAttackAnim.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnStandardCharacter.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnStandardDialogue.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnStandardEnemyPulse.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnStandardLevel.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnStandardMajorDialogue.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnStandardNPC.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnStandardNPCByPosition.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnSpecialCharacter.lua")

--Relies on another routine.
LM_ExecuteScript(gsRoot .. "Subroutines/fnSpawnNPCPattern.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/fnAutoFoldParty.lua")

--Item Subroutines. Only used in Lua, only used for Item List.lua
LM_ExecuteScript(gsRoot .. "Items/Item Subroutines.lua")

--Second set of subroutines. These depend on other subroutines.
LM_ExecuteScript(gsRoot .. "Subroutines/fnCrawlThroughVent.lua")

--[Combat Functions]
--First wave.
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnCombatDamageFormula.lua")
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnComputeCritRate.lua")
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnComputeEffectApplyRate.lua")
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnComputeHitRate.lua")
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnStandardAttack.lua")
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnStandardDamageType.lua")
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnStandardEffectApply.lua")
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnSumBaseStats.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnApplyStatBonus.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnBuildWeaponDamage.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnCheckAITags.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnCreateTagStruct.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnGetAnimationTiming.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnGetBaseStat.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnGetIconByDamageType.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnIsEntityImmuneToDamageType.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnStunHandler.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnHandleCooldown.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnHandleCP.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnHandleMP.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnSetAbilityResponseFlags.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnPlaceJobCommonSkills.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnMarkdownHandler.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnStatRegime.lua")

--Other
LM_ExecuteScript(gsRoot .. "Combat/Enemies/Common Enemy Functions.lua")

--Packages
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnStandardBeginAction.lua") 
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnConstructDefaultApplicationPackage.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnConstructAbilityPackage.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnConstructEffectPackage.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnConstructMissPackage.lua")
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnTagCalculator.lua")

--Second wave.
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnStandardAccuracy.lua") --Depends on fnComputeHitRate and fnComputeCritRate
LM_ExecuteScript(gsRoot .. "Combat/Formulas/fnComputeDamageRange.lua") --Relies on fnCombatDamageFormula
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnBuildPredictionBox.lua") --Relies on several other formulae
LM_ExecuteScript(gsRoot .. "Combat/Routines/fnStandardExecution.lua") --Relies on several other formulae

--Once done building routines, flip this off. Optionally, list all paths stored.
gbIsBuildingFunctionPaths = false
if(false) then
    io.write("Global function list:\n")
    for i = 1, #gsaFunctionPaths, 1 do
        io.write(" " .. i-1 .. ": " .. gsaFunctionPaths[i] .. "\n")
    end
end

--Build program lists which depend on functions.
if(gbAdventureBootDebug) then io.write(" Setting topics.\n") end
LM_ExecuteScript(gsRoot .. "Topics/Build Topics.lua")

--[ ========================================== Loading ========================================== ]
--[Font Loading]
if(gbAdventureBootDebug) then io.write(" Loading fonts.\n") end
LM_ExecuteScript(gsRoot .. "Fonts/000 Boot Fonts.lua")

--[Graphics Loading]
if(gbAdventureBootDebug) then io.write(" Loading graphics.\n") end
LM_ExecuteScript(gsRoot .. "System/001 Graphics.lua")

--[Map Path Remapping]
if(gbAdventureBootDebug) then io.write(" Building map path remaps.\n") end
LM_ExecuteScript(gsRoot .. "System/200 Build Map Path Remaps.lua")

--[Clear Felled Enemies]
--Order the program to wipe out dead enemies. The load handler will populate this with new data.
if(gbAdventureBootDebug) then io.write(" Clearing destroyed enemy data.\n") end
AL_SetProperty("Wipe Destroyed Enemies")

--[ ======================================= Boot Sequence ======================================= ]
--[Normal Boot Sequence]
gsAutoBoot = "Chapter 1"

--In the normal game boot sequence, load into the dimensional trap and create the party.
if(gbIsLoadingSequence == false) then
	
    --[Character Creation]
    --Create all game characters.
    if(gbAdventureBootDebug) then io.write(" Creating party members.\n") end
    --LM_ExecuteScript(gsRoot .. "Chapter1Init/000 Party.lua")
    --LM_ExecuteScript(gsRoot .. "Chapter5Init/000 Party.lua")
    
	--[Normal Boot]
	--Boot the game to "Nowhere" which allows the player to select their chapter.
	if(gsAutoBoot == "Nowhere") then
        if(gbAdventureBootDebug) then io.write(" Booting Nowhere map.\n") end
		LM_ExecuteScript(gsMapDirectory .. "/Nowhere/Constructor.lua")
	
	--[Autoboot Chapter 1]
	--Used for debug.
	elseif(gsAutoBoot == "Chapter 1") then
        if(gbAdventureBootDebug) then io.write(" Running chapter 1 handler, autoboot.\n") end

        --Settings.
        gbBypassIntro = true
        LM_ExecuteScript(gsRoot .. "Chapter 1/000 Initialize.lua")
		LM_ExecuteScript(gsMapDirectory .. "/TrapBasement/TrapBasementC/Constructor.lua", gci_Constructor_Start)
        
        --Create Mei.
        TA_Create("Mei")
            giPartyLeaderID = RO_GetID()
            TA_SetProperty("Position", 52, 29)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            TA_SetProperty("Activation Script", "Null")
        DL_PopActiveObject()
        
        --Run Mei's costume handler.
        LM_ExecuteScript(gsCostumeAutoresolve, "Mei_Human")
	
        --Mark Mei as the actor entity.
        AL_SetProperty("Player Actor ID", giPartyLeaderID)
	
	--[Autoboot Chapter 5]
	--Used for debug.
	elseif(gsAutoBoot == "Chapter 5") then
	
		--Debug.
        if(gbAdventureBootDebug) then io.write(" Running chapter 5 handler, autoboot.\n") end
		LM_ExecuteScript(gsRoot .. "Chapter1Init/000 Party.lua")
	
		gsPartyLeaderName = "Christine"
		VM_SetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N", 5.0)
		--LM_ExecuteScript(gsRoot .. "Chapter5Init/001 Variables.lua")
		--LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Build Scene List.lua")
		LM_ExecuteScript(gsRoot .. "Maps/Build Warp List.lua", "Chapter 5")
		LM_ExecuteScript(gsMapDirectory .. "/RegulusCryo/RegulusCryoA/Constructor.lua")
		gsStandardGameOver = gsRoot .. "Chapter5Scenes/Defeat_BackToSave/Scene_Begin.lua"
		gsStandardRetreat = gsRoot .. "Chapter5Scenes/Retreat/Scene_Begin.lua"
	end
	
	--Mark sound settings as booted.
	VM_SetVar("Root/Variables/System/Special/iHasSoundSettings", "N", 1.0)

--[Load Boot Sequence]
else
	
    --[Actors Store Paths]
    --Actors will likely be created before the image loading has completed. If this is the case, they will
    -- store the path of the missing images. At the end of this file, all TilemapActors will then re-resolve
    -- any missing images.
    TA_SetProperty("Store Image Paths", true)

    --[Character Creation]
    --Create all game characters.
    if(gbAdventureBootDebug) then io.write(" Executing loading sequence.\n") end
    --LM_ExecuteScript(gsRoot .. "Chapter1Init/000 Party.lua")
    --LM_ExecuteScript(gsRoot .. "Chapter5Init/000 Party.lua")
    
    --[Variable boot]
	--Standard loading sequence, these always get set. If loading from a different chapter, these will get overwritten.
    if(gbAdventureBootDebug) then io.write(" Setting standard variables.\n") end
    --LM_ExecuteScript(gsRoot .. "Chapter1Init/001 Variables.lua")
    --LM_ExecuteScript(gsRoot .. "Chapter1Init/002 Farm Variables.lua")
    --LM_ExecuteScript(gsRoot .. "Chapter5Init/001 Variables.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 1/000 Initialize.lua")
	
	--Run the load operation here.
    if(gbAdventureBootDebug) then io.write(" Running loading operation.\n") end
	Save_ExecuteAdventureLoad(sSavePath)
	
	--Execute the post-process script.
    if(gbAdventureBootDebug) then io.write(" Executing load post-process.\n") end
	LM_ExecuteScript(gsRoot .. "Load Handler/000 Load Handler.lua")
	
	--Mark sound settings as booted.
	VM_SetVar("Root/Variables/System/Special/iHasSoundSettings", "N", 1.0)

end

--[ ======================================== Other Setup ======================================== ]
--Autoresolve flag. Generally stays on for the entire program duration.
if(gbAdventureBootDebug) then io.write(" Setting misc flags.\n") end
WD_SetProperty("Autoresolve Target", true)

--Combat system abilities. Some of the abilities in combat always stay the same and are shared by many
-- entities, so they get built once at startup.
AdvCombat_SetProperty("Set Pass Turn Properties", gsRoot .. "Combat/Abilities/Z Common/Pass Turn.lua")
AdvCombat_SetProperty("Set Retreat Properties",   gsRoot .. "Combat/Abilities/Z Common/Retreat.lua")
AdvCombat_SetProperty("Set Surrender Properties", gsRoot .. "Combat/Abilities/Z Common/Surrender.lua")

--Doctor bag path.
AdvCombat_SetProperty("Set Doctor Resolve Path", gsRoot .. "Combat/Routines/Resolve Doctor Bag.lua")

--[Menu]
--Hide the main menu.
if(gbAdventureBootDebug) then io.write(" Finishing up.\n") end
MapM_PushMenuStackHead()
	FlexMenu_FlagClose()
DL_PopActiveObject()

--Drop all events. This prevents the game from running while the loading screen is firing.
Debug_DropEvents()
gbIsLoadingSequence = false
if(gbAdventureBootDebug) then io.write("Finished Launching Adventure Mode.\n") end

--Time stuff.
local fElapsedTime = LM_FinishTimer("ZLaunch")
io.write("Boot time was " .. fElapsedTime .. "\n")