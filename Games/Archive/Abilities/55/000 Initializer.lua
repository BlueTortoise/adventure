--[Initializer]
--Script to call when you want to add an ability to a character. This acts as their master list. It can route into sub-lists.
-- The character receiving the ability should be atop the activity stack.

--Argument Listing:
-- 0: sAbilityName - Name of the ability to add.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sAbilityName = LM_GetScriptArgument(0)

--[ ===================================== Main Ability List ===================================== ]
--[Attack]
--Standard attack. 55 has a higher base accuracy than most other characters.
if(sAbilityName == "Attack") then
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.\n (100% Slashing, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Attack the enemy with your equipped weapon.\n\nDeals 100%% damage as Slashing.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Attack")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Take Cover]
elseif(sAbilityName == "Take Cover") then
	ACE_CreateAction("As Action", "Take Cover")
		ACA_SetProperty("Description", "Get behind something for 1 turn, increasing Protection\n by 80 for 1 turn and Accuracy by 20 for 2 turns.")
        ACAC_SetProperty("Extended Description", "Take cover, increasing protection by 80%% for 1 turn and Accuracy by 20%% for 2 turns.\n\nGenerates no Combo Points.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 0)
		ACA_SetProperty("New Effect", gciEffect_Protection, gci_Target_Self, 80, 1)
		ACA_SetProperty("New Effect", gciEffect_Accuracy, gci_Target_Self, 20, 2)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Defend")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()

--[Special]
--Abilities that cost Combo Points.
elseif(sAbilityName == "Special") then
	ACE_CreateAction("As Sublist", "Special")
		ACAC_SetProperty("Description", "Abilities that require Combo Points to use.")
        ACAC_SetProperty("Extended Description", "Special abilities. These require Combo Points to use.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Special")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()

--[Combat Routines]
--Sublist, adds Combat Routines to 55's list.
elseif(sAbilityName == "Combat Routines") then
	ACE_CreateAction("As Sublist", "Combat Routines")
		ACAC_SetProperty("Description", "55's Combat Routines")
        ACAC_SetProperty("Extended Description", "55's advanced combat techniques.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|CombatRoutines")
        ACAC_SetProperty("UI Position", 0, 1)
	DL_PopActiveObject()

--[Assault]
--Available if not currently in Assault.
elseif(sAbilityName == "Assault Algorithm") then
	ACE_CreateAction("As Action", "Assault Algorithm")
		ACA_SetProperty("Description", "Assault routines. Enables offensive abilities.\nNo statistic penalties or bonuses.")
        ACAC_SetProperty("Extended Description", "Change's 55's Combat Algorithm to Assault. This mode has no statistical penalties or bonuses, and allows usage of her offensive abilities.\n\nCooldown 2.\nUsing this ability resets cooldowns on Combat Routines.")
		ACA_SetProperty("Targetting", gci_Target_Self)
        ACAC_SetProperty("Clear Effect", "Support Algorithm")
        ACAC_SetProperty("Clear Effect", "Restore Algorithm")
		ACA_SetProperty("Is Toggle Action", true)
		ACA_SetProperty("Cooldown", 2)
        ACAC_SetProperty("Link Cooldown With", "Support Algorithm")
        ACAC_SetProperty("Link Cooldown With", "Restore Algorithm")
        ACAC_SetProperty("Clear Cooldown With", "Combat Routines")
		ACA_SetProperty("Changes State To", "Standard")
		ACAC_SetProperty("Greyed In State", "Standard")
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|Assault")
        ACAC_SetProperty("UI Position", 1, 1)
	DL_PopActiveObject()

--[Support]
--Available if not currently in Support.
elseif(sAbilityName == "Support Algorithm") then
	ACE_CreateAction("As Action", "Support Algorithm")
		ACA_SetProperty("Description", "Support routines. -20% damage, +10 Evade,\n+5 Accuracy, enables support and some\noffense abilities.")
        ACAC_SetProperty("Extended Description", "Change's 55's Combat Algorithm to Support. Damage -20%%, Evade +10, Accuracy +5, and allows usage of her support abilities.\n\nCooldown 2.\nUsing this ability resets cooldowns on Combat Routines.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_Self, -20, -1)
		ACA_SetProperty("New Effect", gciEffect_Evade,         gci_Target_Self,  10, -1)
		ACA_SetProperty("New Effect", gciEffect_Accuracy,      gci_Target_Self,   5, -1)
        ACAC_SetProperty("Clear Effect", "Restore Algorithm")
		ACA_SetProperty("Is Toggle Action", true)
		ACA_SetProperty("Cooldown", 2)
        ACAC_SetProperty("Link Cooldown With", "Assault Algorithm")
        ACAC_SetProperty("Link Cooldown With", "Restore Algorithm")
        ACAC_SetProperty("Clear Cooldown With", "Combat Routines")
		ACA_SetProperty("Changes State To", "Support")
		ACAC_SetProperty("Greyed In State", "Support")
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|Support")
        ACAC_SetProperty("UI Position", 2, 1)
	DL_PopActiveObject()

--[Restore]
--Available if not currently in Restore.
elseif(sAbilityName == "Restore Algorithm") then
	ACE_CreateAction("As Action", "Restore Algorithm")
		ACA_SetProperty("Description", "Restoration abilites. -30% damage, +10 Evade,\n-40 Threat. Enables repair/heal abilites.\nPoor offensive options.")
        ACAC_SetProperty("Extended Description", "Change's 55's Combat Algorithm to Restore. Damage -30%%, Evade +10, Threat -40, and allows usage of her restorative abilities.\n\nCooldown 2.\nUsing this ability resets cooldowns on Combat Routines.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_Self, -30, -1)
		ACA_SetProperty("New Effect", gciEffect_Evade,         gci_Target_Self,  10, -1)
		ACA_SetProperty("New Effect", gciEffect_Threat,        gci_Target_Self, -40, -1)
        ACAC_SetProperty("Clear Effect", "Support Algorithm")
		ACA_SetProperty("Is Toggle Action", true)
		ACA_SetProperty("Cooldown", 2)
        ACAC_SetProperty("Link Cooldown With", "Assault Algorithm")
        ACAC_SetProperty("Link Cooldown With", "Support Algorithm")
        ACAC_SetProperty("Clear Cooldown With", "Combat Routines")
		ACA_SetProperty("Changes State To", "Restore")
		ACAC_SetProperty("Greyed In State", "Restore")
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|Restore")
        ACAC_SetProperty("UI Position", 3, 1)
	DL_PopActiveObject()

--[ ==================================== Special Ability List =================================== ]
--[Special/High-Power Shot]
--Advanced move requiring 4CP, deals extra damage.
elseif(sAbilityName == "High-Power Shot") then
	ACE_CreateAction("As Action on Sublist", "Special", "High-Power Shot")
		ACA_SetProperty("Description", "A powerful close-range shot that consumes\nall combo points and deals extra damage to\na single target.\nCannot be used in Restore mode.\n(170% Slashing, Combo Point Cost: 4)")
        ACAC_SetProperty("Extended Description", "A very powerful shot for 170%% damage as Slashing. Consumes all Combo Points.\n\nCannot be used in Restore Mode.\nCombo Cost: 4.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.70)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 4)
		ACAC_SetProperty("Greyed In State", "Restore")
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|HighPowerShot")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Special/Hyper-Repair]
--Advanced move requiring 4CP, heals 55.
elseif(sAbilityName == "Hyper-Repair") then
	ACE_CreateAction("As Action on Sublist", "Special", "Hyper-Repair")
		ACA_SetProperty("Description", "Immediately restores 80% of the target's HP\nand clears Blind, Bleed, and Poison.\nCannot be used in Assault or Support modes.\nConsumes all combo points.\n(Combo Point Cost: 4)")
        ACAC_SetProperty("Extended Description", "Restores 80%% of the target's HP and immediately clears all Blind, Bleed, and Poison effects.\n\nCannot be used in Assault or Support modes.\nCombo Cost: 4.")
		ACA_SetProperty("Targetting", gci_Target_Single_Ally)
		ACA_SetProperty("Target Healing Percent", 80)
		ACA_SetProperty("Cure Target Bleed", true)
		ACA_SetProperty("Cure Target Blind", true)
		ACA_SetProperty("Cure Target Poison", true)
        ACA_SetProperty("Combo Cost", 4)
		ACAC_SetProperty("Greyed In State", "Standard")
		ACAC_SetProperty("Greyed In State", "Support")
		fnStandardAttackAnim("Healing")
		fnStandardAbilitySounds("Healing")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|HyperRepair")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()
	
--[ =================================== Combat Routines List ==================================== ]
--[Combat Routines/Rubber Shot]
--Shot that causes stunning damage.
elseif(sAbilityName == "Rubber Shot") then
	ACE_CreateAction("As Action on Sublist", "Combat Routines", "Rubber Shot")
		ACA_SetProperty("Description", "A shot with a special rubber slug. Stuns.\nUsable in Assault and Support modes.\n(80% Striking, 90 Stunning)\n(Cooldown 1, Generates 1 Combo Point)\n(Strikes with +15 Accy)")
        ACAC_SetProperty("Extended Description", "Deals 90%% damage as Striking and 70 Stun Damage. Strikes with +15 Accuracy.\n\nUsable in Assault and Support Mode.\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 0.90)
		ACA_SetProperty("Speed Modifer", 3)
		ACA_SetProperty("Cooldown", 1)
		ACA_SetProperty("New Effect", gciEffect_Stun, gci_Target_Single_Hostile, 70, 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACAC_SetProperty("Greyed In State", "Restore")
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackPurple")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|RubberSlugShot")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Wide Lens Shot]
--Hits all targets for 50% damage. Assault only. Good for clearing groups.
elseif(sAbilityName == "Wide Lens Shot") then
	ACE_CreateAction("As Action on Sublist", "Combat Routines", "Wide Lens Shot")
		ACA_SetProperty("Description", "Widens the lens on the pulse diffractor to \n hit all targets for reduced damage.\n Usable in Assault mode.\n (60% Slashing, All Enemies)\n (Cooldown 1, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Deals 60%% damage as Slasing to all targets.\n\nUsable in Assault Mode.\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.60)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACAC_SetProperty("Greyed In State", "Support")
		ACAC_SetProperty("Greyed In State", "Restore")
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|WideLensShot")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()

--[Tactical Shift]
--Increases the party's attack and protection for 3 turns. Support and Restore only.
elseif(sAbilityName == "Tactical Shift") then
	ACE_CreateAction("As Action on Sublist", "Combat Routines", "Tactical Shift")
		ACA_SetProperty("Description", "Identify enemy strategies to counter them.\nBuffs party Attack by 25% and \n Protection by 10, 3 turns.\nSupport and Restore modes only.\n(Cooldown 2, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Buffs the party. Attack +25%%, Protection +10. Effect lasts 3 turns.\n\nUsable in Support or Restore Mode.\nCooldown: 2.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_All_Allies)
		ACA_SetProperty("Cooldown", 2)
		ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_All_Allies, 25, 3)
		ACA_SetProperty("New Effect", gciEffect_Protection, gci_Target_All_Allies, 10, 3)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACAC_SetProperty("Greyed In State", "Standard")
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|Encourage")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()

--[Repair]
--Restores one ally's HP. Restore only.
elseif(sAbilityName == "Repair") then
	ACE_CreateAction("As Action on Sublist", "Combat Routines", "Repair")
		ACA_SetProperty("Description", "Spot-repair damaged parts/flesh.\nHelps if the subject stops squirming.\nRestores 70 HP, Restore mode only.\n(Cooldown 2, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Immediately restores 70HP to a single friendly target.\n\nUsable in Restore Mode.\nCooldown: 2.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Ally)
        ACA_SetProperty("Target Healing", 70)
		ACA_SetProperty("Cooldown", 2)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACAC_SetProperty("Greyed In State", "Standard")
		ACAC_SetProperty("Greyed In State", "Support")
		fnStandardAttackAnim("Healing")
		fnStandardAbilitySounds("Healing")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|Repair")
        ACAC_SetProperty("UI Position", 3, 0)
	DL_PopActiveObject()

--[Spotless Protocol]
--Clears bleed, poison, and corrosion on the party.
elseif(sAbilityName == "Spotless Protocol") then
	ACE_CreateAction("As Action on Sublist", "Combat Routines", "Spotless Protocol")
		ACA_SetProperty("Description", "Keep flesh and metal clean per specifications.\nClears Bleed, Blind, and Corrosion from \nthe party. Unavailable in Assault mode.\n(Generates 1 Combo Point, No Cooldown)")
        ACAC_SetProperty("Extended Description", "Cleans Bleed, Blind, and Corrosion effects from the entire party.\n\nUsable in Support or Restore Mode.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_All_Allies)
		ACA_SetProperty("Cooldown", 0)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACA_SetProperty("Cure Target Bleed", true)
		ACA_SetProperty("Cure Target Blind", true)
		ACA_SetProperty("Cure Target Corrosion", true)
		ACAC_SetProperty("Greyed In State", "Standard")
		fnStandardAttackAnim("Healing")
		fnStandardAbilitySounds("Healing")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|SpotlessProtocol")
        ACAC_SetProperty("UI Position", 4, 0)
	DL_PopActiveObject()
	
--[Disruptor Blast]
--Shot that deals 100% damage as Terrify and 50% as Slashing. Assault only.
elseif(sAbilityName == "Disruptor Blast") then
	ACE_CreateAction("As Action on Sublist", "Combat Routines", "Disruptor Blast")
		ACA_SetProperty("Description", "An overlarge pulse blast designed to inflict\nterror. Ignores protection. Assault mode only.\n(50% Slashing, 100% Terrify, AP:100)\n(Cooldown 2, Generates 1 Combo Point)\n(Strikes with +15 Accy)")
        ACAC_SetProperty("Extended Description", "A powerful blast that deals 50%% damage as Slashing and 100%% damage as Terrifying. Ignores Protection. Strikes with +15 Accuracy.\n\nUsable in Assault Mode.\nCooldown: 2.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.50)
		ACA_SetProperty("Damage Amount", gciFactor_Terrify, 1.00)
        ACA_SetProperty("Protection Bypass", 100)
		ACA_SetProperty("Speed Modifer", 3)
		ACA_SetProperty("Cooldown", 2)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACAC_SetProperty("Greyed In State", "Support")
		ACAC_SetProperty("Greyed In State", "Restore")
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|DisruptorBlast")
        ACAC_SetProperty("UI Position", 0, 1)
	DL_PopActiveObject()

--[Implosion Grenade]
--Deals more damage the more targets it hits.
elseif(sAbilityName == "Implosion Grenade") then
	ACE_CreateAction("As Action on Sublist", "Combat Routines", "Implosion Grenade")
		ACA_SetProperty("Description", "Deals more damage the more targets it hits.\n(75% Striking + 25% per target)\n(Cooldown 2, Generates 1 Combo Point)\n(Never misses, never crits.)")
        ACAC_SetProperty("Extended Description", "Throws a specially designed grenade that pulls enemies into each other. Deals more damage the more targets it hits. 100%% accurate, never crits, 75%% striking + 25%% per target.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 0.75)
        ACA_SetProperty("Damage Bonus From Targets", 0.25)
        ACA_SetProperty("Action Never Crits", true)
        ACA_SetProperty("Action Never Misses", true)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 2)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACAC_SetProperty("Greyed In State", "Restore")
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|ImplosionGrenade")
        ACAC_SetProperty("UI Position", 1, 1)
	DL_PopActiveObject()

end