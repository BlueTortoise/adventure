--[Initializer]
--Script to call when you want to add an ability to a character. This acts as their master list. It can route into sub-lists.
-- The character receiving the ability should be atop the activity stack.

--Argument Listing:
-- 0: sAbilityName - Name of the ability to add.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sAbilityName = LM_GetScriptArgument(0)

--[ ===================================== Main Ability List ===================================== ]
--[Attack]
--Standard attack. Christine has a higher base attack power than most other characters.
if(sAbilityName == "Attack") then
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.\n (100% Slashing, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Attack the enemy with your equipped weapon.\n\nDeals 100%% damage as Slashing.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Attack")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()
	
--[Take Point]
--Learned when Christine meets 55. Causes Christine to take the lead, increasing her threat by 200 points. This means she has a 75%
-- chance to be targeted by enemies over 55.
--This ability disappears when it is used, until "Line Formation" is used.
elseif(sAbilityName == "Take Point") then
	ACE_CreateAction("As Action", "Take Point")
		ACA_SetProperty("Description", "Take the lead of the party, threatening \n the enemy.\n (+200 Threat. Permanent.)")
        ACAC_SetProperty("Extended Description", "Take the lead of the party, increasing threat by 200 (Base 100). Enemies will prioritize attacking targets by their threat.\n\nThis ability can be cancelled with 'Line Formation'.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("New Effect", gciEffect_Threat, gci_Target_Self, 200, -1)
		ACA_SetProperty("Is Toggle Action", true)
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
		ACA_SetProperty("Changes State To", "OnPoint")
		ACAC_SetProperty("Greyed In State", "OnPoint")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|TakePoint")
        ACAC_SetProperty("UI Position", 3, 0)
	DL_PopActiveObject()
	
--[Line Formation]
--Learned when Christine meets 55. Christine returns to her normal position, resetting threat gained. Also gives
-- an evade boost for 3 turns.
elseif(sAbilityName == "Line Formation") then
	ACE_CreateAction("As Action", "Line Formation")
		ACA_SetProperty("Description", " Return to line formation, resetting threat\n to normal.\n (-200 Threat. Permanent.)")
        ACAC_SetProperty("Extended Description", "Returns to line formation with the rest of the party.\n\nCancels the threat increase of 'Take Point'.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("Is Toggle Action", true)
        ACAC_SetProperty("Clear Effect", "Take Point")
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
		ACA_SetProperty("Changes State To", "Standard")
		ACAC_SetProperty("Greyed In State", "Standard")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|LineFormation")
        ACAC_SetProperty("UI Position", 4, 0)
	DL_PopActiveObject()

--[Block]
--Christine's defensive action. Increases Protection for 3 turns!
elseif(sAbilityName == "Block") then
	ACE_CreateAction("As Action", "Block")
		ACA_SetProperty("Description", " Defend yourself, increasing Protection by 45% \n for 2 turns.\n (Generates 2 Combo Points)")
        ACAC_SetProperty("Extended Description", "Increases Protection by 45%% for 2 turns.\n\nCooldown: 2 Turns\nGenerates 2 Combo Points.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 2)
		ACA_SetProperty("New Effect", gciEffect_Protection, gci_Target_Self, 45, 2)
        ACA_SetProperty("Combo Generation", 2)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Defend")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()

--[Special]
--Abilities that cost Combo Points.
elseif(sAbilityName == "Special") then
	ACE_CreateAction("As Sublist", "Special")
		ACAC_SetProperty("Description", "Abilities that require Combo Points to use.")
        ACAC_SetProperty("Extended Description", "Special abilities. These require Combo Points to use.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Special")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()
	
--[Techniques]
--Sublist, adds Techniques to Christine's list.
elseif(sAbilityName == "Techniques") then
	ACE_CreateAction("As Sublist", "Techniques")
		ACAC_SetProperty("Description", "Spear Techniques.")
        ACAC_SetProperty("Extended Description", "Advanced weapon techniques.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|MaleTech")
        ACAC_SetProperty("UI Position", 0, 1)
	DL_PopActiveObject()

--[ ==================================== Special Ability List =================================== ]
--[Special/Batter]
--Advanced move requiring 4CP, deals extra damage.
elseif(sAbilityName == "Batter") then
	ACE_CreateAction("As Action on Sublist", "Special", "Batter")
		ACA_SetProperty("Description", "A powerful strike that consumes all combo \n points and deals stun damage to a single target.\n (200% Slashing, Combo Point Cost: 4)\n (120 Stunning)")
        ACAC_SetProperty("Extended Description", "A powerful strike, dealing 200%% damage as Slashing and 120 Stun Damage.\n\nRequires 4 Combo Points.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 2.00)
		ACA_SetProperty("New Effect", gciEffect_Stun, gci_Target_Single_Hostile, 120, 1)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 4)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|Batter")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()
	
--Advanced move requiring 4CP, generates threat and protection for 4 turns. Very powerful!
elseif(sAbilityName == "Officer Charge") then
	ACE_CreateAction("As Action on Sublist", "Special", "Officer Charge")
		ACA_SetProperty("Description", "Lead your squad to glory! Tally Ho!\nGenerates threat and increases Protection.\nLasts 4 turns.\nConsumes all combo points.\n(Combo Point Cost: 4)\n (+20 Protection, +200 Threat. 4 Turns.)")
        ACAC_SetProperty("Extended Description", "Buffs Christine, increasing Protection by 20 and Threat by 200. The effect lasts 4 turns.\n\nRequires 4 Combo Points.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("New Effect", gciEffect_Protection, gci_Target_Self, 20, 4)
		ACA_SetProperty("New Effect", gciEffect_Threat, gci_Target_Self, 200, 4)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 4)
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|OfficerCharge")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()

--[ ====================================== Techniques List ====================================== ]
--[Techniques/Shock]
--Deals electricity damage. Very useful against robots.
elseif(sAbilityName == "Shock") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Shock")
		ACA_SetProperty("Description", "Shock the enemy using your Electrospear.\n (100% Electrical)\n (No cooldown, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Deals 100%% damage as Electrical. No cooldown.\n\nGenerates 1 Combo Point.\nEffective against mechanical or aquatic enemies.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Lightning, 1.00)
        ACA_SetProperty("Combo Generation", 1)
		fnStandardAttackAnim("Electricity")
		fnStandardAbilitySounds("Electricity")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|Shock")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Techniques/Puncture]
--Deals piercing damage. Some enemies are vulnerable to this type.
elseif(sAbilityName == "Puncture") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Puncture")
		ACA_SetProperty("Description", "Stab the enemy with the point of the\n Electrospear. Deals Piercing damage.\n (100% Piercing)\n (No cooldown, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Deals 100%% damage as Piercing. No cooldown.\n\nGenerates 1 Combo Point.\nEffective against soft targets.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Pierce, 1.00)
        ACA_SetProperty("Combo Generation", 1)
		fnStandardAttackAnim("Pierce")
		fnStandardAbilitySounds("Pierce")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|Puncture")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()
	
--[Techniques/Encourage]
--Spurs the team on, increasing attack and accuracy.
elseif(sAbilityName == "Encourage") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Encourage")
		ACA_SetProperty("Description", "Encourage your team, increasing attack \n and accuracy.\n (+10% Attack, +20 Accuracy, 5 turns)\n (Cooldown 3, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Buffs the party with encouraging words. Increases Attack by 10%%, Accuracy by 20.\nEffect lasts 5 turns.\n\nCooldown: 2.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_All_Allies)
		ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_All_Allies, 10, 5)
		ACA_SetProperty("New Effect", gciEffect_Accuracy, gci_Target_All_Allies, 20, 5)
        ACA_SetProperty("Combo Generation", 1)
		ACA_SetProperty("Cooldown", 3)
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|Encourage")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()
	
--[Techniques/Sweep]
--Hits all targets for 60% damage as electricity. Useful against robots, awful against latex drones.
elseif(sAbilityName == "Sweep") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Sweep")
		ACA_SetProperty("Description", "Quickly sweep your electrospear horizontally. \n Deals reduced electrical damage to \n all enemies.\n (60% Electrical, all targets)\n (Cooldown 1, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Strikes all enemies with a horizontal sweep. Deals 60%% damage as Electrical.\n\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Lightning, 0.60)
        ACA_SetProperty("Combo Generation", 1)
		ACA_SetProperty("Cooldown", 1)
		fnStandardAttackAnim("Electricity")
		fnStandardAbilitySounds("Electricity")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|Sweep")
        ACAC_SetProperty("UI Position", 3, 0)
	DL_PopActiveObject()
	
--[Techniques/Rally]
--Attacks for extra damage and recovers 5% of max HP.
elseif(sAbilityName == "Rally") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Rally")
		ACA_SetProperty("Description", "A powerful strike that raises your morale. \n Deals extra damage, restores 5% of your HP.\n (125% Slashing)\n (Cooldown 1, Generates 1 Combo Point)\n (Strikes with +15 Accy)")
        ACAC_SetProperty("Extended Description", "Strikes one enemy for 125%% damage as Slashing. Restores 5%% of your HP when used.\nStrikes at +15 Accuracy.\n\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.25)
		ACA_SetProperty("Self Healing Percent", 5)
        ACA_SetProperty("Combo Generation", 1)
		ACA_SetProperty("Cooldown", 1)
		ACA_SetProperty("Speed Modifer", 3)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Christine|Rally")
        ACAC_SetProperty("UI Position", 4, 0)
	DL_PopActiveObject()

end