--[Initializer]
--Script to call when you want to add an ability to a character. This acts as their master list. It can route into sub-lists.
-- The character receiving the ability should be atop the activity stack.

--Argument Listing:
-- 0: sAbilityName - Name of the ability to add.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sAbilityName = LM_GetScriptArgument(0)

--[ ===================================== Main Ability List ===================================== ]
--[Attack]
--Standard attack. Florentina's is nothing special.
if(sAbilityName == "Attack") then
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.\n (100% Slashing, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Attack the enemy with your equipped weapon.\n\nDeals 100%% damage as Slashing.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Attack")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Defend]
--Florentina's defensive action. Not as effective as Mei's, but Florentina has better dodge.
elseif(sAbilityName == "Defend") then
	ACE_CreateAction("As Action", "Defend")
		ACA_SetProperty("Description", "Defend yourself, increasing Protection by 45%\n for 1 turn.\n (Generates 2 Combo Points)")
        ACAC_SetProperty("Extended Description", "Increases protection by 45 for 1 turn.\n\nGenerates 2 Combo Points.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("New Effect", gciEffect_Protection, gci_Target_Self, 45, 1)
        ACA_SetProperty("Combo Generation", 2)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Defend")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()

--[Special]
--Abilities that cost Combo Points.
elseif(sAbilityName == "Special") then
	ACE_CreateAction("As Sublist", "Special")
		ACAC_SetProperty("Description", "Abilities that require Combo Points to use.")
        ACAC_SetProperty("Extended Description", "Special abilities requiring Combo Points.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Special")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()
	
--[Botany]
--Sublist, adds Botany to Florentina's sublists.
elseif(sAbilityName == "Botany") then
	ACE_CreateAction("As Sublist", "Botany")
		ACAC_SetProperty("Description", "Florentina's abilities related to her botanical\nnature.")
        ACAC_SetProperty("Extended Description", "Florentina's botanical techniques.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Florentina|Botany")
        ACAC_SetProperty("UI Position", 0, 1)
	DL_PopActiveObject()

--[ ==================================== Special Ability List =================================== ]
--[Special/Regrow]
--Advanced move requiring 4CP, recovers 75HP immediately.
elseif(sAbilityName == "Regrowth") then
	ACE_CreateAction("As Action on Sublist", "Special", "Regrowth")
		ACA_SetProperty("Description", "Channel all Combo Points to regrow lost HP.\n (Restores 75 HP, Combo Point Cost: 4)")
        ACAC_SetProperty("Extended Description", "Restores 75HP to Florentina.\n\nCombo Cost: 4.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("Self Healing", 75)
		ACA_SetProperty("Combo Generation", 0)
		ACA_SetProperty("Combo Cost", 4)
		fnStandardAttackAnim("Healing")
		fnStandardAbilitySounds("Healing")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Florentina|Regrowth")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Special/Critical Stab]
--Advanced move requiring 4CP, deals extra damage.
elseif(sAbilityName == "Critical Stab") then
	ACE_CreateAction("As Action on Sublist", "Special", "Critical Stab")
		ACA_SetProperty("Description", "Stab the enemy where it matters most.\nPiercing damage.\nConsumes all combo points.\n(210% Piercing, Combo Point Cost: 4)")
        ACAC_SetProperty("Extended Description", "Deals 210%% damage as Piercing to a single target.\n\nCombo Cost: 4.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Pierce, 2.10)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 4)
		fnStandardAttackAnim("Pierce")
		fnStandardAbilitySounds("Pierce")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Florentina|CriticalStab")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()

--[Special/Drain Vitality]
--Advanced move requiring 4CP, deals extra damage and restores HP.
elseif(sAbilityName == "Drain Vitality") then
	ACE_CreateAction("As Action on Sublist", "Special", "Drain Vitality")
		ACA_SetProperty("Description", "Use Alraune magic to absorb some health and\ndeal extra damage.\nConsumes all combo points.\n(170% Piercing, Combo Point Cost: 4)")
        ACAC_SetProperty("Extended Description", "Deals 170%% damage as Piercing to a single target and absorbs 30%% of the damage to be restored as health.\n\nCombo Cost: 4.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Pierce, 1.70)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 4)
		ACA_SetProperty("Absorption", 30)
		fnStandardAttackAnim("Pierce")
		fnStandardAbilitySounds("Pierce")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Florentina|DrainVitality")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()
	
--[ ======================================= Botany Sublist ====================================== ]
--[Botany/Dripping Blade]
--Slash that causes poison damage.
elseif(sAbilityName == "Dripping Blade") then
	ACE_CreateAction("As Action on Sublist", "Botany", "Dripping Blade")
		ACA_SetProperty("Description", "Coat your knife with poison and slash the enemy.\n (10% Slashing, 110% Poison over 3 turns)\n (Cooldown 1, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Immediately deals 10%% damage as Slashing and an additional 110%% damage as Poison over 3 turns.\n\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash,  0.10)
		ACA_SetProperty("Damage Amount", gciFactor_Poison, 1.10)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Florentina|DrippingBlade")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()
	
--[Botany/Vine Wrap]
--Reduces the target's speed.
elseif(sAbilityName == "Vine Wrap") then
	ACE_CreateAction("As Action on Sublist", "Botany", "Vine Wrap")
		ACA_SetProperty("Description", " Call vines to wrap your target's feet, reducing\n their speed.\n (110% Slashing, +40% Acc, Cooldown 1)\n (Generates 1 Combo Point)\n (Enemy: Speed -2 for 3 Turns)")
        ACAC_SetProperty("Extended Description", "Immediately deals 110%% damage as Slashing and reduces the target's Initiative by 2, Accuracy by 10, and Evade by 10 for 3 turns. Strikes with +40 Accuracy.\n\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.10)
		ACA_SetProperty("Speed Modifer", 8)
		ACA_SetProperty("Cooldown", 1)
		ACA_SetProperty("New Effect", gciEffect_Speed, gci_Target_Single_Hostile, -2, 4)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("Debuff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackPurple")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Florentina|VineWrap")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()
	
--[Botany/Intimidate]
--Reduces the target's ATP and inflicts stun.
elseif(sAbilityName == "Intimidate") then
	ACE_CreateAction("As Action on Sublist", "Botany", "Intimidate")
		ACA_SetProperty("Description", "Florentina's fierce negotiating attitude,\n unleashed!\n (40% Terrifying, +20% Acc)\n (Generates 1 Combo Point)\n (Enemy: ATK -50% for 3 Turns)\n (Cooldown: 1 Turn)")
        ACAC_SetProperty("Extended Description", "Deals 40%% damage as Terrifying and reduces the target's Attack Power by 50%% for 3 turns. Strikes with +20 Accuracy.\n\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Terrify, 0.40)
		ACA_SetProperty("Speed Modifer", 4)
		ACA_SetProperty("Cooldown", 1)
		ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_Single_Hostile, -150, 4)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("Debuff")
		fnStandardAbilitySounds("Debuff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackPurple")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Florentina|Intimidate")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()
	
--[Botany/Cruel Slash]
--Deals extra damage as poison and bleeding, but has a longer cooldown.
elseif(sAbilityName == "Cruel Slash") then
	ACE_CreateAction("As Action on Sublist", "Botany", "Cruel Slash")
		ACA_SetProperty("Description", "Deals bleeding and poison damage at once.\n Has a long cooldown.\n (30% Piercing, Cooldown 2)\n (60% Poison and 60% Bleeding over 3 turns)")
        ACAC_SetProperty("Extended Description", "Immediately deals 30%% damage as Piercing and inflicts 60%% damage as Poison and a further 60%% damage as Bleeding over 3 turns.\n\nCooldown: 2.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Pierce, 0.30)
		ACA_SetProperty("Damage Amount", gciFactor_Bleed,  0.60)
		ACA_SetProperty("Damage Amount", gciFactor_Poison, 0.60)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 2)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("Pierce")
		fnStandardAbilitySounds("Pierce")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Florentina|CruelSlice")
        ACAC_SetProperty("UI Position", 3, 0)
	DL_PopActiveObject()

end