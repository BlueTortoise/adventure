--[Initializer]
--Script to call when you want to add an ability to a character. This acts as their master list. It can route into sub-lists.
-- The character receiving the ability should be atop the activity stack.

--Argument Listing:
-- 0: sAbilityName - Name of the ability to add.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sAbilityName = LM_GetScriptArgument(0)

--[ ===================================== Main Ability List ===================================== ]
--[Attack]
--Standard attack. 55 has a higher base accuracy than most other characters.
if(sAbilityName == "Attack") then
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.\n (100% Slashing, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Attack the enemy with your equipped weapon.\n\nDeals 100%% damage as Slashing.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Attack")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Special]
--Abilities that cost Combo Points.
elseif(sAbilityName == "Special") then
	ACE_CreateAction("As Sublist", "Special")
		ACAC_SetProperty("Description", "Abilities that require Combo Points to use.")
        ACAC_SetProperty("Extended Description", "Special abilities. These require Combo Points to use.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Special")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()
	
--[Techniques]
--JX-101's special attacks.
elseif(sAbilityName == "Techniques") then
	ACE_CreateAction("As Sublist", "Techniques")
		ACAC_SetProperty("Description", "Special combat techniques JX-101 has learned\nover the years.")
        ACAC_SetProperty("Extended Description", "JX-101's advanced combat techniques.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/JX101|Techniques")
        ACAC_SetProperty("UI Position", 0, 1)
	DL_PopActiveObject()

--[ ==================================== Special Ability List =================================== ]
--[Special/Thunderbolt Slug]
--Advanced move requiring 4CP, deals extra damage.
elseif(sAbilityName == "Thunderbolt Slug") then
	ACE_CreateAction("As Action on Sublist", "Special", "Thunderbolt Slug")
		ACA_SetProperty("Description", "An overheavy slug discharged from JX-101's\npulse pistol.\nDeals electrical damage.\nConsumes all combo points.\n(60% Slashing, 120% Electrical, Combo Point Cost: 4)")
        ACAC_SetProperty("Extended Description", "A very powerful attack that deals 60%% damage as Slashing and 120%% damage as Electrical.\n\nRequires 4 Combo Points.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.60)
		ACA_SetProperty("Damage Amount", gciFactor_Lightning, 1.20)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 4)
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/JX101|ThunderboltSlug")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Special/Pinning Fire]
--Advanced move requiring 4CP, reduces target accuracy and stuns.
elseif(sAbilityName == "Pinning Fire") then
	ACE_CreateAction("As Action on Sublist", "Special", "Pinning Fire")
		ACA_SetProperty("Description", "A volley of carefully placed shots stuns enemies\nand reduces their accuracy.\n (100% Slashing, 60 Stunning, -45 Acc/3 Turns)\n (Combo Point Cost: 4)")
        ACAC_SetProperty("Extended Description", "Discharges the full magazine of JX-101's weapon to force enemies into cover. Deals 100%% damage as Slashing and 60 Stun Damage, strikes all enemies. Enemies suffer 45 Blinding for 3 turns.\n\nRequires 4 Combo Points.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("New Effect", gciEffect_Stun, gci_Target_Single_Hostile, 60, 1)
		ACA_SetProperty("New Effect", gciEffect_Blind, gci_Target_Single_Hostile, 45, 4)
        ACA_SetProperty("Combo Cost", 4)
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackPurple")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/JX101|PinningFire")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()
	
--[ ====================================== Techniques List ====================================== ]
--[Combat Routines/Battle Cry]
--Terrifying damage, buffs the party and damages one enemy.
elseif(sAbilityName == "Battle Cry") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Battle Cry")
		ACA_SetProperty("Description", "Deals Terrify damage to one enemy and decreases \ntheir Accuracy.\n (50% Terrifying, Cooldown 1)\n (-25 Accy/3 Turns, 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Deals 50%% damage as Terrifying to a single target. Blinds by 25 for 3 turns.\n\nCooldown 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Terrify, 0.50)
		ACA_SetProperty("Cooldown", 1)
		ACA_SetProperty("New Effect", gciEffect_Blind, gci_Target_Single_Hostile, 25, 4)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/JX101|BattleCry")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Sniper Shot]
--Hits a single target for double damage.
elseif(sAbilityName == "Sniper Shot") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Sniper Shot")
		ACA_SetProperty("Description", "Hit a single target for 2.5x damage, 2 turn \ncooldown. Doesn't miss or crit.\n (250% Slashing, Cooldown 2)\n (Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Inflicts 250%% damage as Slashing to a single target, but cannot strike critically or miss.\n\nCooldown 2.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 2.50)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 2)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
        ACA_SetProperty("Action Never Crits", true)
		ACA_SetProperty("Speed Modifer", 100)
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/JX101|SniperShot")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()

--[Field Repair]
--Restores HP.
elseif(sAbilityName == "Field Repair") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Field Repair")
		ACA_SetProperty("Description", "Demonstrating pure ingenuity, repairs damaged \nflesh and metal.\n (+45 HP, single target.)\n (Cooldown 1, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Immediately restores 45HP to a single friendly target.\n\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Ally)
        ACA_SetProperty("Target Healing", 45)
		ACA_SetProperty("Cooldown", 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|Repair")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()

end