--[Initializer]
--Script to call when you want to add an ability to a character. This acts as their master list. It can route into sub-lists.
-- The character receiving the ability should be atop the activity stack.

--Argument Listing:
-- 0: sAbilityName - Name of the ability to add.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sAbilityName = LM_GetScriptArgument(0)

--[ ===================================== Main Ability List ===================================== ]
--[Attack]
--Standard attack. Mei's is nothing special.
if(sAbilityName == "Attack") then
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.\n (100% Slashing, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Attack the enemy with your equipped weapon.\n\nDeals 100%% damage as Slashing.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Attack")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Parry]
--Mei's defensive action. Increases protection and builds 3 Combo Points.
elseif(sAbilityName == "Parry") then
	ACE_CreateAction("As Action", "Parry")
		ACA_SetProperty("Description", "Defend yourself, increasing Protection by 75%\n for 1 turn.\n (Generates 2 Combo Points)")
        ACAC_SetProperty("Extended Description", "Parry incoming attacks, increasing Protection by 75 for 1 turn.\n\nGenerates 2 Combo Points.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("New Effect", gciEffect_Protection, gci_Target_Self, 75, 1)
        ACA_SetProperty("Combo Generation", 2)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Defend")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()

--[Special]
--Abilities that cost Combo Points.
elseif(sAbilityName == "Special") then
	ACE_CreateAction("As Sublist", "Special")
		ACAC_SetProperty("Description", "Abilities that require Combo Points to use.")
        ACAC_SetProperty("Extended Description", "Special abilities that require Combo Points.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Special")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()
	
--[Fencing]
--Sublist, adds Fencing to Mei's list.
elseif(sAbilityName == "Fencing") then
	ACE_CreateAction("As Sublist", "Fencing")
		ACAC_SetProperty("Description", "Mei's fencing skills.")
        ACAC_SetProperty("Extended Description", "Mei's sword techniques.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Mei|Fencing")
        ACAC_SetProperty("UI Position", 0, 1)
	DL_PopActiveObject()

--[ ====================================== Special Sublist ====================================== ]
--[Special/Powerful Strike]
--Advanced move requiring 4CP, deals extra damage.
elseif(sAbilityName == "Powerful Strike") then
	ACE_CreateAction("As Action on Sublist", "Special", "Powerful Strike")
		ACA_SetProperty("Description", " A powerful strike that consumes all combo\n points and deals extra damage to a\n single target.\n(230% Slashing, Combo Point Cost: 4)")
        ACAC_SetProperty("Extended Description", "Deals 230%% damage as Slashing to a single target.\n\nCombo Cost: 4.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 2.30)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 4)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Mei|PowerfulStrike")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Special/Blade Dance]
--Advanced move requiring 6CP, deals extra damage to all targets. Powerful!
elseif(sAbilityName == "Blade Dance") then
	ACE_CreateAction("As Action on Sublist", "Special", "Blade Dance")
		ACA_SetProperty("Description", " A powerful strike that consumes all combo\n points and deals extra damage to\n all targets.\n(190% Slashing *all*, Combo Point Cost: 6)")
        ACAC_SetProperty("Extended Description", "Deals 190%% damage as Slashing to all targets.\n\nCombo Cost: 6.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.90)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 6)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb6")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Mei|BladeDance")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()
	
--[ ====================================== Fencing Sublist ====================================== ]
--[Fencing/Rend]
--Slash that causes bleeding.
elseif(sAbilityName == "Rend") then
	ACE_CreateAction("As Action on Sublist", "Fencing", "Rend")
		ACA_SetProperty("Description", "A lateral slice, causing bleeding.\n (30% Slashing, 90% Bleeding over 3 turns)\n (Cooldown 1, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Immediately deals 30%% damage as Slashing, and inflicts a further 90%% damage as Bleeding over 3 turns.\n\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.30)
		ACA_SetProperty("Damage Amount", gciFactor_Bleed, 0.90)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Mei|Rend")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()
	
--[Fencing/Throw Sand]
--Blind the target so they miss more often.
elseif(sAbilityName == "Blind") then
	ACE_CreateAction("As Action on Sublist", "Fencing", "Blind")
		ACA_SetProperty("Description", "Throw sand in the enemy's eyes,\n causing blindness.\n (30% Piercing, +30% Acc, Cooldown 1)\n (Generates 1 Combo Point)\n (Enemy: -10% Acc for 3 turns)\n (Cannot strike critically)")
        ACAC_SetProperty("Extended Description", "Deals 30%% damage as Piercing and inflicts -10%% Accuracy of Blindness on the enemy for 3 turns. Strikes at +30 Accuracy.\n\nCooldown: 1.\nGenerates 1 Combo Point.\nCannot strike critically.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Pierce, 0.30)
		ACA_SetProperty("Speed Modifer", 6)
		ACA_SetProperty("Cooldown", 1)
		ACA_SetProperty("New Effect", gciEffect_Blind, gci_Target_Single_Hostile, 10, 4)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Pierce")
		fnStandardAbilitySounds("Pierce")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackPurple")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Mei|Blind")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()
	
--[Fencing/Quick Strike]
--Fast attack that deals reduced damage but is very accurate.
elseif(sAbilityName == "Quick Strike") then
	ACE_CreateAction("As Action on Sublist", "Fencing", "Quick Strike")
		ACA_SetProperty("Description", "A fast, light strike that is easy to hit with.\n (80% Slashing, +70% Acc, No Cooldown)\n (Generates 1 Combo Point)\n (Cannot strike critically)")
        ACAC_SetProperty("Extended Description", "An attack that deals 80%% damage as Slashing and strikes with +70 Accuracy.\n\nCooldown: 0.\nGenerates 1 Combo Point.\nCannot strike critically.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.80)
		ACA_SetProperty("Speed Modifer", 14)
		ACA_SetProperty("Cooldown", 0)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Attack")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()
	
--[Fencing/Pommel Bash]
--Bash the enemy with the hilt of Mei's sword, increasing their stun counter.
elseif(sAbilityName == "Pommel Bash") then
	ACE_CreateAction("As Action on Sublist", "Fencing", "Pommel Bash")
		ACA_SetProperty("Description", "Bash with the hilt of your sword,\n causing stun damage.\n (80% Striking, +70 Stun, Cooldown 1)\n (Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "A stunning strike with the hilt of Mei's sword. Deals 80%% damage as Striking and 70 Stun Damage.\n\nCooldown: 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 0.80)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 0)
		ACA_SetProperty("New Effect", gciEffect_Stun, gci_Target_Single_Hostile, 70, 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Mei|PommelBash")
        ACAC_SetProperty("UI Position", 3, 0)
	DL_PopActiveObject()
	
--[Fencing/Taunt]
--Mock all enemies, reducing their attack power.
elseif(sAbilityName == "Taunt") then
	ACE_CreateAction("As Action on Sublist", "Fencing", "Taunt")
		ACA_SetProperty("Description", "Mock enemies, dealing minor damage and\n reducing their attack power.\n (30% Terrifying *all*, Cooldown 1)\n (Generates 1 Combo Point)\n (Enemy: -20% Atk for 3 Turns)\n (Cannot strike critically)")
        ACAC_SetProperty("Extended Description", "Debuffs all enemies, dealing 30%% damage as Terrifying and reducing enemy Attack by 20%% for 3 turns.\n\nCooldown: 1.\nGenerates 1 Combo Point.\nCannot strike critically.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Terrify, 0.30)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Cooldown", 1)
		ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_Single_Hostile, -20, 4)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Debuff")
		fnStandardAbilitySounds("Debuff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackPurple")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Mei|Taunt")
        ACAC_SetProperty("UI Position", 4, 0)
	DL_PopActiveObject()
end