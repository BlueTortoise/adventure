--[Initializer]
--Script to call when you want to add an ability to a character. This acts as their master list. It can route into sub-lists.
-- The character receiving the ability should be atop the activity stack.

--Argument Listing:
-- 0: sAbilityName - Name of the ability to add.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sAbilityName = LM_GetScriptArgument(0)

--[ ===================================== Main Ability List ===================================== ]
--[Attack]
--Standard attack.
if(sAbilityName == "Attack") then
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.\n (100% Slashing, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Attack the enemy with your equipped weapon.\n\nDeals 100%% damage as Slashing.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Attack")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()

--[Special]
--Abilities that cost Combo Points.
elseif(sAbilityName == "Special") then
	ACE_CreateAction("As Sublist", "Special")
		ACAC_SetProperty("Description", "Abilities that require Combo Points to use.")
        ACAC_SetProperty("Extended Description", "Special abilities. These require Combo Points to use.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/Special")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()
	
--[Techniques]
--SX-399's special attacks.
elseif(sAbilityName == "Techniques") then
	ACE_CreateAction("As Sublist", "Techniques")
		ACAC_SetProperty("Description", "Special combat techniques SX-399 has learned\nover the months.")
        ACAC_SetProperty("Extended Description", "SX-399's basic combat techniques.")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGrey")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/JX101|Techniques")
        ACAC_SetProperty("UI Position", 0, 1)
	DL_PopActiveObject()

--[ ==================================== Special Ability List =================================== ]
--[Special/Thunderbolt Slug]
--Advanced move requiring 4CP, deals extra damage.
elseif(sAbilityName == "Thunderbolt Slug") then
	ACE_CreateAction("As Action on Sublist", "Special", "Thunderbolt Slug")
		ACA_SetProperty("Description", "An overheavy slug discharged from SX-399's\nsidearm. Just like mom used to make.\nDeals electrical damage.\nConsumes all combo points.\n(60% Slashing, 120% Electrical, CPCost: 4)")
        ACAC_SetProperty("Extended Description", "A very powerful attack that deals 60%% damage as Slashing and 120%% damage as Electrical.\n\nRequires 4 Combo Points.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.60)
		ACA_SetProperty("Damage Amount", gciFactor_Lightning, 1.20)
        ACA_SetProperty("Combo Generation", 0)
        ACA_SetProperty("Combo Cost", 4)
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackRed")
        ACAC_SetProperty("Combo Image",   "Root/Images/AdventureUI/Abilities/Cmb4")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/JX101|ThunderboltSlug")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()
	
--[ ====================================== Techniques List ====================================== ]
--[Techniques/Shoot 'Em Good]
--Terrifying damage, buffs the party and damages one enemy.
elseif(sAbilityName == "Shoot 'em Good") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Shoot 'em Good")
		ACA_SetProperty("Description", "Shoots em. Good-like. Deals extra damage.\n (150% Slashing, Cooldown 2)\n (1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Deals 150%% damage as Slashing to a single target.\n\nCooldown 2.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.50)
		ACA_SetProperty("Cooldown", 2)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/JX101|SniperShot")
        ACAC_SetProperty("UI Position", 0, 0)
	DL_PopActiveObject()
    
--[Techniques/Melta Blast]
--Flame damage, one enemy, ignores protection.
elseif(sAbilityName == "Melta Blast") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Melta Blast")
		ACA_SetProperty("Description", "Overloads the Fission Carbine, dealing flame\ndamage.\nCompletely bypasses protection.\n (120% Flaming, Cooldown 1, AP:100)\n (1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Deals 120%% damage as Flaming to a single target.\n\nCooldown 1.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Fire, 1.20)
        ACA_SetProperty("Protection Bypass", 100)
		ACA_SetProperty("Cooldown", 1)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("LaserShot")
		fnStandardAbilitySounds("LaserShot")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackBlue")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/SX399|MeltaBlast")
        ACAC_SetProperty("UI Position", 1, 0)
	DL_PopActiveObject()

--[Patch 'em Up]
--Restores HP.
elseif(sAbilityName == "Patch 'em Up") then
	ACE_CreateAction("As Action on Sublist", "Techniques", "Patch 'em Up")
		ACA_SetProperty("Description", "Nothing that a bit of welded metal\ncan't fix, right?\n (+60 HP, single target.)\n (Cooldown 2, Generates 1 Combo Point)")
        ACAC_SetProperty("Extended Description", "Immediately restores 60HP to a single friendly target.\n\nCooldown: 2.\nGenerates 1 Combo Point.")
		ACA_SetProperty("Targetting", gci_Target_Single_Ally)
        ACA_SetProperty("Target Healing", 60)
		ACA_SetProperty("Cooldown", 2)
        ACA_SetProperty("Combo Generation", 1)
        ACA_SetProperty("Combo Cost", 0)
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
        ACAC_SetProperty("Backing Image", "Root/Images/AdventureUI/Abilities/BackGreen")
        ACAC_SetProperty("Combo Image",   "Null")
        ACAC_SetProperty("Display Image", "Root/Images/AdventureUI/Abilities/55|Repair")
        ACAC_SetProperty("UI Position", 2, 0)
	DL_PopActiveObject()

end