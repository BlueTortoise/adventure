--[Party]
--When Chapter 1 starts up, initializes the player's party. First, add Mei's starting armor. This does
-- not fire on game load, only on chapter initialization.
--[=[

--If Mei/Florentina already exist, this has already been run.
if(AC_GetProperty("Does Character Exist", "Mei") == true) then
	return
end

--[ ============================================ Mei ============================================ ]
--Variables.
local sAbilityPath = gsRoot .. "Abilities/Mei/000 Initializer.lua"

--If not during a loading sequence, add Mei's equipment to the inventory.
if(gbIsLoadingSequence == false) then
	LM_ExecuteScript(gsItemListing, "Mei's Work Uniform")
	LM_ExecuteScript(gsItemListing, "Silver Runestone")
	
	--When bypassing the intro, give Mei the Rusty Katana.
	if(gbBypassIntro == true) then
		LM_ExecuteScript(gsItemListing, "Rusty Katana")
	end
end

--Create Mei and register her. Equip her basic gear.
AC_CreatePartyMember("Mei")

	--Mei can transform.
	ACE_SetProperty("Can Transform", true)
	
	--Path Mei uses to resolve skillbook abilities.
	ACE_SetProperty("Skillbook Resolve Path", gsRoot .. "Skillbooks/Mei/Z Assemble Volume List.lua")
	
	--Equip Mei's default clothes, and the rune she starts the game with. Only used on game boot.
	if(gbIsLoadingSequence == false) then
		ACE_SetProperty("Equip", "Armor", "Mei's Work Uniform")
		ACE_SetProperty("Equip", "Item A", "Silver Runestone")
		
		--When bypassing the intro,  equip Mei's katana.
		if(gbBypassIntro == true) then
			ACE_SetProperty("Equip", "Weapon", "Rusty Katana")
		end
	end
DL_PopActiveObject()

--"Transform" Mei into a human. This sets her default stats.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

--Set Mei's HP to 100%, give her the default abilities.
AC_PushPartyMember("Mei")

	--HP.
	ACE_SetProperty("Health Percent", 1.0)

	--Pass the ability path.
	ACE_SetProperty("Ability Path", sAbilityPath)

	--Mei's standard ability set.
	LM_ExecuteScript(sAbilityPath, "Attack")
	LM_ExecuteScript(sAbilityPath, "Parry")
	LM_ExecuteScript(sAbilityPath, "Special")
	LM_ExecuteScript(sAbilityPath, "Fencing")
	
	--Special Moves.
	LM_ExecuteScript(sAbilityPath, "Powerful Strike")
	
	--Fencing Moves.
	LM_ExecuteScript(sAbilityPath, "Rend")
	LM_ExecuteScript(sAbilityPath, "Blind")
DL_PopActiveObject()

--[ ========================================= Florentina ======================================== ]
--Variables.
sAbilityPath = gsRoot .. "Abilities/Florentina/000 Initializer.lua"

--Create Florentina. She is not in the party at game start.
AC_CreatePartyMember("Florentina")

	--Florentina cannot transform and is always an Alraune.
	ACE_SetProperty("Can Transform", false)
	
	--Path Florentina uses to resolve skillbook abilities.
	ACE_SetProperty("Skillbook Resolve Path", gsRoot .. "Skillbooks/Florentina/Z Assemble Volume List.lua")
	
	--Florentina's equipment will be added if/when the player recruits her.
DL_PopActiveObject()

--"Transform" Florentina into an Alraune. This sets her default stats. She doesn't transform any more after that.
LM_ExecuteScript(gsRoot .. "FormHandlers/Florentina/Form_Alraune.lua")

--Set Florentina's HP to 100%, give her the default abilities.
AC_PushPartyMember("Florentina")

	--HP.
	ACE_SetProperty("Health Percent", 1.0)

	--Pass the ability path.
	ACE_SetProperty("Ability Path", sAbilityPath)

	--Florentina's standard ability set.
	LM_ExecuteScript(sAbilityPath, "Attack")
	LM_ExecuteScript(sAbilityPath, "Special")
	LM_ExecuteScript(sAbilityPath, "Botany")
	
	--Special Moves.
	LM_ExecuteScript(sAbilityPath, "Critical Stab")
	LM_ExecuteScript(sAbilityPath, "Regrowth")
	
	--Botany Moves.
	LM_ExecuteScript(sAbilityPath, "Dripping Blade")
	LM_ExecuteScript(sAbilityPath, "Vine Wrap")
DL_PopActiveObject()
]=]
