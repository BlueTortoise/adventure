--[Special]
--Sets the party to the expected standard for fighting the bosses of Chapter 1. This is:
-- Mei and Florentina are level 3
-- Mei has access to Rusty Katana (+2)
-- Mei gets Light Leather Vest (+1)
-- Mei gets Arm Brace
-- Mei gets Healing Tincture (+3)
-- Florentina gets Healing Tincture (+3)

--Mei and Florentina also gain the skills they would if they found the skillbooks.

--Remember to equip the items, idiot.
LM_ExecuteScript(gsItemListing, "Steel Katana")
LM_ExecuteScript(gsItemListing, "Rusty Katana")
LM_ExecuteScript(gsItemListing, "Serrated Katana")

LM_ExecuteScript(gsItemListing, "Hunting Knife")
LM_ExecuteScript(gsItemListing, "Butterfly Knife")
LM_ExecuteScript(gsItemListing, "Wildflower's Knife")
LM_ExecuteScript(gsItemListing, "Light Leather Vest")
LM_ExecuteScript(gsItemListing, "Arm Brace")
LM_ExecuteScript(gsItemListing, "Healing Tincture")
LM_ExecuteScript(gsItemListing, "Healing Tincture")
LM_ExecuteScript(gsItemListing, "Pepper Pie")

--Level up Mei.
AC_PushPartyMember("Mei")
	
	--Quick Strike.
	local bHasAbility = ACE_GetProperty("Has Ability", "Quick Strike")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Mei/000 Initializer.lua", "Quick Strike")
	end
	
	--Pommel Bash.
	bHasAbility = ACE_GetProperty("Has Ability", "Pommel Bash")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Mei/000 Initializer.lua", "Pommel Bash")
	end
	
DL_PopActiveObject()

--Level up Florentina.
AC_PushPartyMember("Florentina")
	
	--"Defend".
	bHasAbility = ACE_GetProperty("Has Ability", "Defend")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Florentina/000 Initializer.lua", "Defend")
	end
	
	--Intimidate.
	bHasAbility = ACE_GetProperty("Has Ability", "Intimidate")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Florentina/000 Initializer.lua", "Intimidate")
	end
DL_PopActiveObject()