--[Special]
--Sets the party to the max power.
-- Mei and Florentina are level 5
-- Mei has access to Steel Katana
-- Mei gets Light Leather Vest (+2)
-- Mei gets Arm Brace
-- Mei gets Alacrity Bracer
-- Mei gets Healing Tincture (+3)
-- Florentina gets Hunting Knife (+2)
-- Florentina gets Decorative Bracer
-- Florentina gets Decorative Bracer
-- Florentina gets Healing Tincture (+3)

--Mei and Florentina also gain the skills they would if they found the skillbooks.

--Remember to equip the items, idiot.
LM_ExecuteScript(gsItemListing, "Steel Katana")
LM_ExecuteScript(gsItemListing, "Light Leather Vest (+2)")
LM_ExecuteScript(gsItemListing, "Arm Brace")
LM_ExecuteScript(gsItemListing, "Alacrity Bracer")
LM_ExecuteScript(gsItemListing, "Healing Tincture (+3)")
LM_ExecuteScript(gsItemListing, "Hunting Knife (+2)")
LM_ExecuteScript(gsItemListing, "Decorative Bracer")
LM_ExecuteScript(gsItemListing, "Jade Eye Ring")
LM_ExecuteScript(gsItemListing, "Healing Tincture (+3)")

--Level up Mei.
AC_PushPartyMember("Mei")
	
	--Quick Strike.
	local bHasAbility = ACE_GetProperty("Has Ability", "Quick Strike")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Mei/000 Initializer.lua", "Quick Strike")
	end
	
	--Pommel Bash.
	bHasAbility = ACE_GetProperty("Has Ability", "Pommel Bash")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Mei/000 Initializer.lua", "Pommel Bash")
	end
	
	--Blade Dance.
	bHasAbility = ACE_GetProperty("Has Ability", "Blade Dance")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Mei/000 Initializer.lua", "Blade Dance")
	end
	
	--Taunt.
	bHasAbility = ACE_GetProperty("Has Ability", "Taunt")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Mei/000 Initializer.lua", "Taunt")
	end
	
DL_PopActiveObject()

--Level up Florentina.
AC_PushPartyMember("Florentina")
	
	--"Defend".
	bHasAbility = ACE_GetProperty("Has Ability", "Defend")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Florentina/000 Initializer.lua", "Defend")
	end
	
	--Intimidate.
	bHasAbility = ACE_GetProperty("Has Ability", "Intimidate")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Florentina/000 Initializer.lua", "Intimidate")
	end
	
	--Drain Vitality.
	bHasAbility = ACE_GetProperty("Has Ability", "Drain Vitality")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Florentina/000 Initializer.lua", "Drain Vitality")
	end
	
	--Cruel Slash.
	bHasAbility = ACE_GetProperty("Has Ability", "Cruel Slash")
	if(bHasAbility == false) then
		LM_ExecuteScript(gsRoot .. "Abilities/Florentina/000 Initializer.lua", "Cruel Slash")
	end
DL_PopActiveObject()