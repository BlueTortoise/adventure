--[Special]
--This instantly removes Florentina from the party. Used to test cutscenes, it never triggers normally.
-- This should be called before a map relocation.
gsFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {0}
AC_SetProperty("Set Party", 1, "Null")
AL_SetProperty("Unfollow Actor Name", "Florentina")

--If the Florentina entity happens to be on the field, move her off.
if(EM_Exists("Florentina") == true) then
	EM_PushEntity("Florentina")
		TA_SetProperty("Position", -10, -10)
	DL_PopActiveObject()
end