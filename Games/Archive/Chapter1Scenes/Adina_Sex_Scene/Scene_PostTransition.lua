--[Scene Post-Transition]
--After the cutscene goes to fullbright, it switches maps. This plays afterwards.
AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, false, 1, 1, 1, 1, 1, 1, 1, 0)

--Reposition Mei.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 17, 24)
	TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()

--Reposition Adina.
EM_PushEntity("Adina")
	TA_SetProperty("Position", 16, 24)
	TA_SetProperty("Facing", gci_Face_East)
DL_PopActiveObject()

--Wait a bit.
fnCutsceneInstruction([[ AL_SetProperty("Foreground Alpha", 0, 0.00, 0) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--Mei talks to herself.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Rested, my loyal thrall?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: This slave's body is prepared, mistress.[SOFTBLOCK] Command me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Adina: Splendid.[SOFTBLOCK] You must tend to the little ones.[SOFTBLOCK] Go.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Yes mistress.[SOFTBLOCK] I obey without question.") ]])
fnCutsceneBlocker()

--Reset it so Mei does not have the special grass or pollen.
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasGrass", "N", 0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasPollen", "N", 0)

--Generate some problems for the farm.
VM_SetVar("Root/Variables/Global/Time/iTimeOfDay", "N", 9.0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", 0)
fnModifyTimeOfDay(9, 0)
fnCutsceneInstruction([[ fnGenerateFarmProblems() ]])
fnCutsceneBlocker()