--[Cultist Meeting]
--Cutscene where the cultists give you a map and instructions. How nice of those dumb, dumb people.
--This should be called by the door script when it gets opened.
local bSkipMostOfScene = false

--Variables.
local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")

--Flags.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenCultistMeetingScene", "N", 1.0)

--Wait a bit for dramatic effect.
fnCutsceneWait(145)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Uh oh...)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--If Florentina is present:
if(bIsFlorentinaInParty == true) then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Uh, Mei?[SOFTBLOCK] I'm not supposed to be in this cutscene.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You go ahead and I'll meet you in the next room.[SOFTBLOCK] And then we'll act like nothing happened.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("Florentina", 11.25, 11.50)
	fnCutsceneMove("Florentina", 11.25, 4.50)
	fnCutsceneBlocker()
	fnCutsceneTeleport("Florentina", -100.25, -100.50)
	fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
	fnCutsceneBlocker()
end

--The cultists turn to look at her.
fnCutsceneFace("Cultist A", 0, 1)
fnCutsceneFace("Cultist B", 0, 1)
fnCutsceneFace("Cultist C", 0, 1)
fnCutsceneFace("Cultist D", 0, 1)
fnCutsceneFace("Cultist E", 0, 1)
fnCutsceneFace("Cultist F", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(120)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Better leg it!)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Mei", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist:[VOICE|CultistF] Wait initiate, don't go!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Mei", 4.25, 10.50)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] (Initiate..?)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Cultist A", 5.25, 10.50)
fnCutsceneFace("Cultist A", -1, 0)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Don't look so nervous, initiate.[SOFTBLOCK] We don't bite![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: But,[SOFTBLOCK] you should know better than to enter the meeting room while we're using it.[SOFTBLOCK] Didn't you see the schedule outside?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] S-[SOFTBLOCK]seriously?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I mean, uh, sorry![SOFTBLOCK] It's...[SOFTBLOCK] my...[SOFTBLOCK] first...[SOFTBLOCK] day?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Well obviously![SOFTBLOCK] You haven't even been issued robes yet![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "*The cultists chuckle*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: *Psst![SOFTBLOCK] I'm kinda new here myself, but I just got a great idea![SOFTBLOCK] Play along!*") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Cultist A", 5.25, 7.50)
fnCutsceneFace("Cultist A", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Cultists face the room center again.
fnCutsceneFace("Cultist A", 1, 0)
fnCutsceneFace("Cultist B", 0, -1)
fnCutsceneFace("Cultist C", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Cultist A", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: There's a spot right there.[SOFTBLOCK] Come on in, we're all friends here![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (Eep!)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Cultist A", 1, 0)
fnCutsceneMove("Mei", 4.25, 6.50, 0.70)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 1, 0)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneMove("Mei", 5.25, 6.50, 0.70)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "CultistF", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "CultistM", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: Right.[SOFTBLOCK] Where were we?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistM: The patrols from that trading post have been getting dangerously close.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: Oh, yeah![SOFTBLOCK] But, I believe I can solve that little problem for us.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistM: Is that so?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: Naturally, we should send someone undercover to infiltrate the trading post.[SOFTBLOCK] Send us intelligence reports,[SOFTBLOCK] spread misinformation,[SOFTBLOCK] that sort of thing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistM: And just who will do that?[SOFTBLOCK] Our regalia is not to be removed, so says the Sister Superior.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: And if we were to send an initiate, who has not even earned her chains?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistM: And where would you propose we find such a person?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: ... [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: ... [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: *Psst![SOFTBLOCK] Volunteer, you dolt!*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Uh, I can do it![SOFTBLOCK] I haven't earned my...[SOFTBLOCK] shackles?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: *Try not to be so nervous, rookie.*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Eep![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistM: A brilliant plan![SOFTBLOCK] I will see to it you are rewarded for this.[SOFTBLOCK] Initiate, this will be a great way to earn favour for you![SOFTBLOCK] You should be honored.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: I'm double-dip honoured right now.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: Indeed![SOFTBLOCK] Here, take this map.[SOFTBLOCK] The trading post is just west of here.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|Menu|SpecialItem]*Received Parchment Map!*[SOFTBLOCK]\nYou can check the map from the pause menu.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: We will dispatch another Initiate to receive your reports and relay instructions.[SOFTBLOCK] The code word will be 'Toothbrush'.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: Oh, I almost didn't catch your name, Initiate.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Uhh...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: ...[SOFTBLOCK] Tess.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: Good luck on your mission, Initiate Tess![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "CultistF: *Great![SOFTBLOCK] We're gonna get promoted for sure![SOFTBLOCK] Please, don't deprive me of this!*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: *Oh, sure, happy to help![SOFTBLOCK] Gotta go!*") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Mei", 1, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 0, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 1, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 0, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMoveFace("Mei", 4.25, 6.50, 1, 0, 0.70)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 1, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Cultist A", 0, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist:[VOICE|CultistF] Good luck out there![SOFTBLOCK] We're all counting on you!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMoveFace("Mei", 4.25, 4.50, 1, 1, 0.70)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMoveFace("Mei", 11.25, 4.50, 1, 0, 1.0)
fnCutsceneFace("Mei", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Transition to the next room. There is a short scene, but another cutscene handles it.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutsceneInstruction([[ AL_BeginTransitionTo("TrapBasementB", gsRoot .. "Chapter1Scenes/Basement_CultistMeeting/Scene_PostTransition.lua") ]])
fnCutsceneBlocker()

--Provide the player with a map.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasNoMap", "N", 0.0)
fnResolveMapLocation("TrapBasementD")
