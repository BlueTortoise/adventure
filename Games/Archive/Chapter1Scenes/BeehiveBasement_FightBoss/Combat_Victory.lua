--[Combat Victory]
--The party won!

--[Setup]
--Flag.
VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N", 1.0)

--Darken the screen.
AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1)
AudioManager_PlayMusic("Null")

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] We -[SOFTBLOCK] we won![SOFTBLOCK] Yeah![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] The outcome was never really in doubt.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] But, what about the honey? [SOFTBLOCK]The bees?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] With the source of corruption gone, I'm sure the bees can take care of the rest.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm, what if they don't know how?[SOFTBLOCK] What if they don't know how to fix this?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I just got a good idea.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I like that smile on your face.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] She's still breathing.[SOFTBLOCK] Grab her arms.[SOFTBLOCK] Try not to hurt her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] No promises.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Quick, before the rest of the bees recover![SOFTBLOCK] C'mon!") ]])
fnCutsceneBlocker()

--Wait a while.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Dialogue resumes.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Happy") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Happy") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[SOUND|World|HardHit][E|Offended][SOFTBLOCK] Hey! Don't bang her head against the wall![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh, so...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy][SOUND|World|HardHit][SOFTBLOCK]*Don't*[SOFTBLOCK] do that?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] We need her alive!") ]])
fnCutsceneBlocker()

--Wait a while.
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Transition]
--Scene resumes on the top floor of the beehive.
fnCutsceneInstruction([[ AL_BeginTransitionTo("BeehiveInner", gsRoot .. "Chapter1Scenes/BeehiveBasement_FightBoss/Victory_PostTransition.lua") ]])
fnCutsceneBlocker()