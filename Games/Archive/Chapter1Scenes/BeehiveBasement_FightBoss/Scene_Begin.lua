--[BeehiveBasement Boss]
--Mei and Florentina have battled their way into the boss chamber. Time to fight the boss!
-- This cutscene is triggered by a floor trigger, not the debug menu.

--[Variables]
--Modifies some of the script dialogue.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--[Movement]
--Move the party up to the cultist.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (12.75 * gciSizePerTile), (12.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (13.75 * gciSizePerTile), (12.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue]
--Dialogue setup.
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Hey look, Mei![SOFTBLOCK] A dead cultist![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: ![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Has nobody informed you?[SOFTBLOCK] Allow me to be the first.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You're dead.[SOFTBLOCK] C'mere.[BLOCK][CLEAR]") ]])

--If Mei is a bee:
if(sMeiForm == "Bee") then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Drone, silence this Alraune interloper![SOFTBLOCK] How did you allow her into the sanctum?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Drone?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I don't take orders from dead people.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Y-[SOFTBLOCK]you...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: You bear the rune...[SOFTBLOCK] Just like the sister superior said![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Impossible![SOFTBLOCK] The rune bearers - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Really chatty,[SOFTBLOCK] for a corpse.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Let's shut her up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Drones, assist me![SOFTBLOCK] When I bring the runebearer's head...[SOFTBLOCK] I will be rewarded!") ]])
	fnCutsceneBlocker()

--All other forms:
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Interlopers![SOFTBLOCK] How did you get in here?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Through the...[SOFTBLOCK] door?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Your security sucks.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Y-[SOFTBLOCK]you...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: You bear the rune...[SOFTBLOCK] Just like the sister superior said![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This old thing?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Impossible![SOFTBLOCK] The rune bearers - [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Really chatty,[SOFTBLOCK] for a corpse.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Let's shut her up.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Drones, assist me![SOFTBLOCK] When I bring the runebearer's head...[SOFTBLOCK] I will be rewarded!") ]])
	fnCutsceneBlocker()

end

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Boss battle!
fnCutsceneInstruction([[
	AC_SetProperty("Next Music Override", "RottenTheme", 0.0000)
	AC_SetProperty("Activate")
	AC_SetProperty("Unretreatable", true)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Zombee.lua", 0)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/CultistCorrupter.lua", 0)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Zombee.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 4)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 8)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 12)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 16)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 20)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 24)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 28)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 32)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 36)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 40)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 44)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 48)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Next Enemy Reinforces", 52)
	LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/ZombeeSpecial.lua", 0)
	AC_SetProperty("Victory Script", gsRoot .. "Chapter1Scenes/BeehiveBasement_FightBoss/Combat_Victory.lua")
	AC_SetProperty("Defeat Script", gsRoot .. "Chapter1Scenes/Defeat_Zombee/Scene_Begin.lua")
	AC_SetProperty("No Post Chat")
]])
fnCutsceneBlocker()
