--[Scene Post-Transition]
--After getting punched out, Mei has a brief scene.
AL_SetProperty("Music", "Null")
AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)

--Florentina doesn't count as being in the party, so spawn her here.
fnSpecialCharacter("Florentina", "Alraune", 13, 14, gci_Face_East, false, nil)

--Move Mei.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 14, 14)
DL_PopActiveObject()

--Switch Mei to Bee mode. She can no longer become a zombee.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua")

--[Dialogue]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|MC] (Drone is...[SOFTBLOCK] Drone is...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|MC] (Am I a mindless tool?[SOFTBLOCK] Is drone a mindless tool?)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|MC] (Is drone...[SOFTBLOCK] is Mei?)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|MC] (Mei...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Sad] (Mei...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[E|Neutral] (My -[SOFTBLOCK] name is Mei!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (I'm not a mindless drone!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (I love my bee sisters![SOFTBLOCK] I'm [SOFTBLOCK]*so*[SOFTBLOCK] sorry![SOFTBLOCK] I couldn't control myself!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] (Yes, I'm back![SOFTBLOCK] Yes![SOFTBLOCK] I love you too!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] (And -[SOFTBLOCK] we can save the other drones![SOFTBLOCK] If I can do it, so can they!)") ]])
fnCutsceneBlocker()

--[Wake Up]
--Unfade.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Wait a few ticks.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Normal music.
fnCutsceneInstruction([[ AL_SetProperty("Music", "ForestTheme") ]])

--[Dialogue]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] F-[SOFTBLOCK]Florentina?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Easy, easy.[SOFTBLOCK] Take it slow.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Am I...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're fine.[SOFTBLOCK] Your friends are holding the line below, like before.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] If anything they're doing it better, now.[SOFTBLOCK] Their organization was sloppy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] They struggle when they can't talk to the whole hive at once.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well you seem to be a lot less mindless.[SOFTBLOCK] Are all your bits still there?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I think so.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] It was...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] There there.[SOFTBLOCK] Let it out, kid.[SOFTBLOCK] Don't bottle it up.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] They took me and...[SOFTBLOCK] it was so big...[SOFTBLOCK] I couldn't even scream...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] What was?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] We don't have a chance...[SOFTBLOCK] It was bigger than the sky and it was just staring right at me.[SOFTBLOCK] I was so small...[SOFTBLOCK] I had no choice but to obey...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] There's always a choice.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] But...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Hey.[SOFTBLOCK] Look at me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Taff.[SOFTBLOCK] That.[SOFTBLOCK] Thing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I don't even know what you're babbling about, and I don't care.[SOFTBLOCK] Taff it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You don't listen to sorholes like that![SOFTBLOCK] You [SOFTBLOCK]*kill*[SOFTBLOCK] them.[SOFTBLOCK] And if you have to die first, so be it.[SOFTBLOCK] But you don't give in.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I know it was hard, but the Mei I know and fight alongside is strong.[SOFTBLOCK] And you were strong enough to come to your senses.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] If it had been me instead of you, I don't know if I'd have come back.[SOFTBLOCK] Maybe I'd have given in, too.[SOFTBLOCK] But you're here, now, strong and independent.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] That means they can lose.[SOFTBLOCK] Whatever those cultists worship [SOFTBLOCK]*can*[SOFTBLOCK] be hurt,\n[SOFTBLOCK]*can*[SOFTBLOCK] be beaten.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah.[SOFTBLOCK] Yeah![SOFTBLOCK] You're right![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The bigger they are, the harder they fall![SOFTBLOCK] That's a saying we have on Earth![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Now that's what I like to hear![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] But the job's not done yet.[SOFTBLOCK] You ready for round two?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Let me at them![SOFTBLOCK] We can do it![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're damn skippy we can![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We should regroup.[SOFTBLOCK] Check our equipment.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Don't make the same mistake as last time.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, of course.[SOFTBLOCK] We know what's ahead.[SOFTBLOCK] This one's in the bag.[SOFTBLOCK] C'mon, Mei!") ]])
fnCutsceneBlocker()

--Florentina rejoins the party.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (14.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

--[System]
--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
gsFollowersTotal = 1
gsaFollowerNames = {"Florentina"}
giaFollowerIDs = {0}

--Get Florentina's uniqueID. 
EM_PushEntity("Florentina")
	local iFlorentinaID = RE_GetID()
DL_PopActiveObject()

--Store it and tell her to follow.
giaFollowerIDs = {iFlorentinaID}
AL_SetProperty("Follow Actor ID", iFlorentinaID)

--Place Florentina in the combat lineup.
AC_SetProperty("Set Party", 1, "Florentina")