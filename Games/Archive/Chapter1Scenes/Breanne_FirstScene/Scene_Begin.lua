--[Breanne's First Scene]
--This plays regardless of Mei's form the first time she enters the Pit Stop. Some of the dialogue changes though.
-- The scene is activated by entering the trigger inside the main room of the pit stop.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetMei", "N", 1.0)

--[Variables]
--Get values.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasFoundOutlandAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
local bIsFlorentinaHere = AL_GetProperty("Is Character Following", "Florentina")

--Breanne records the first form she saw Mei in.
VM_SetVar("Root/Variables/Chapter1/Breanne/sMeiFirstForm", "S", sMeiForm)
	
--In all cases, mark the combat intro dialogues as complete.
VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")

--[Walking]
--Mei walks in a bit.
Cutscene_CreateEvent("Move Mei Inside A", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (13.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Mei Inside B", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (14.50 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina is present, she also walks in.
if(bIsFlorentinaHere) then
	Cutscene_CreateEvent("Move Florentina Inside A", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (13.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("Move Florentina Inside B", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (14.50 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneInstruction([[ fnPartyStopMovement() ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--[Talking]
--Breanne says she'll be right there.
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Breanne", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Coming![SOFTBLOCK] Just a moment!") ]])
fnCutsceneBlocker()

--[Walking]
--Walk Breanne to the door.
Cutscene_CreateEvent("Move Breanne to Door", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Move To", (24.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Breanne West", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Open the door, play a sound.
fnCutsceneInstruction([[AL_SetProperty("Open Door", "Kitchen Door")]])
fnCutsceneInstruction([[AudioManager_PlaySound("World|OpenDoor")]])
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Walk Breanne out.
Cutscene_CreateEvent("Move Breanne to Lobby A", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Breanne to Lobby B", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Breanne to Lobby B", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Move To", (21.25 * gciSizePerTile), (14.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Timing.
Cutscene_CreateEvent("Face Breanne West", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--[Talking]
--Standard parts of the dialogue.
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Breanne", "Neutral") ]])

--Mei is alone...
if(bIsFlorentinaHere == false) then
	
	--Flag: Breanne met Mei when Florentina was present.
	VM_SetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N", 0.0)
	
	--Topic unlocks. Breanne mentioned Florentina, the trading post, the magic field, the pit stop, the season, and her own name.
	WD_SetProperty("Unlock Topic", "Florentina", 1)
	WD_SetProperty("Unlock Topic", "TradingPost", 1)
	WD_SetProperty("Unlock Topic", "PitStopMagicField", 1)
	
	--Mei is an Alraune.
	if(sMeiForm == "Alraune") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well howdy, stranger.[SOFTBLOCK] What can I do for you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] My appearance doesn't upset you, human?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Name's Breanne, in case the signs didn't give it away.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: We're open to all sorts here.[SOFTBLOCK] 'Sides, nobody can set foot on the property if they've got violent intentions.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] What?[SOFTBLOCK] How?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Search me.[SOFTBLOCK] Some kinda magic.[SOFTBLOCK] I found out about it a while back, and figured this was a good spot to set up.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: If you're here, it means you're a sweet sort.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: So, what's your name?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Please call me Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: That's a nice name, but I'm guessing it's not your first one.[SOFTBLOCK] I know how Alraunes work.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Actually, it is.[SOFTBLOCK] I...[SOFTBLOCK] decided not to clear my mind.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well, what brings you here, then?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not quite sure, myself.[SOFTBLOCK] I feel the need to find out the purpose behind this runestone, and how I got here from Earth.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Something pushing you to explore, eh?[SOFTBLOCK] I know the feeling.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Have you ever seen this symbol before?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: No, sorry.[SOFTBLOCK] I'm not much of a scholar.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Magic is a little out of my league.[SOFTBLOCK] I like to work with my hands.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I see.[SOFTBLOCK] Thank you for your time.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Now hold on there a moment.[SOFTBLOCK] Just because I don't know what it is doesn't mean I don't know who might.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Just southwest of here is a trading post.[SOFTBLOCK] You look up Florentina when you're there, and I bet she'll be able to tell you what this rune is about.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Florentina?[SOFTBLOCK] Yes, hm.[SOFTBLOCK] The little ones have spoken of her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: What's a little one?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Oh, I'm sorry.[SOFTBLOCK] You would call them grass, flowers, that sort of thing.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Nifty![SOFTBLOCK] What do you call trees?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Grandfather or grandmother, if they're fully grown.[SOFTBLOCK] Otherwise, they'd be little ones.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Hey, I know I told you to talk to Florentina, but if you want to stick around and chat for a bit...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're not busy?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: There's been a lull in visitors lately.[SOFTBLOCK] My schedule is wide open!") ]])
	
	--Mei is a Slime.
	elseif(sMeiForm == "Slime") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh shoot, you managed to get the door open did you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] W-wait![SOFTBLOCK] This isn't what it looks like![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Really?[SOFTBLOCK] Because to me, it looks like a talking slime just got my door open.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] ...[SOFTBLOCK] Actually, that's pretty much right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] But I'm not like the other slimes, I promise![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Once again, you go stating the obvious.[SOFTBLOCK] Slimes normally can't talk, silly.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Well I'm not going to attack you, is what I mean.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You can't.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I - [SOFTBLOCK]can't?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Nope.[SOFTBLOCK] This place is built on some kinda magic field.[SOFTBLOCK] Repulses anyone who intends violence.[SOFTBLOCK] I don't know how it works, mind, but it works.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Do you have a name, little slime?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei.[SOFTBLOCK] Call me Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Hm, not a common name around these parts.[SOFTBLOCK] What brings you to the pit stop?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I was looking for someone who could help me find a way back to Earth.[SOFTBLOCK] But then I got turned into a slime.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: This is recent, is it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Yes.[SOFTBLOCK] I think this runestone is somehow related, I found it when I woke up here and it did something when I got...[SOFTBLOCK] viscous...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Woah, slow down there.[SOFTBLOCK] You just said a bunch of stuff.[SOFTBLOCK] What's this runestone?[SOFTBLOCK] What's Earth?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Earth is where I'm from.[SOFTBLOCK] There was a flash of white when I was walking to the train station, and I wound up here, with this runestone.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Let me have a look...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well I don't know much about magic, but this sure looks important.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You know who'd be able to help you out?[SOFTBLOCK] Florentina.[SOFTBLOCK] She runs the provisions shop at the trading post across the river.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: At the very least she'd be able to tell you what the symbol means, or tell you how to find out.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Thanks so much...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Don't mention it![SOFTBLOCK] And, if you want to stick around and chat, I'm all ears.[SOFTBLOCK] Not a lot of customers around this time.[SOFTBLOCK] I'd love to hear what it's like being a slime.") ]])
	
	--Mei is a Bee.
	elseif(sMeiForm == "Bee") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh my![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Ain't no nectar in here, honeybun.[SOFTBLOCK] Hehe.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not looking for nectar, actually.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You can speak?[SOFTBLOCK] That's a first.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] You don't mind?[SOFTBLOCK] My form doesn't upset you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You kidding?[SOFTBLOCK] I'd love to have you as a guest.[SOFTBLOCK] Stay as long as you like![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Phew.[SOFTBLOCK] I was worried you'd think I was going to attack you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm.[SOFTBLOCK] Oh![SOFTBLOCK] Very interesting, thank you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Uhh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Sorry, I was talking to the other bees.[SOFTBLOCK] I sometimes forget you humans can't hear them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: What did they say?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] They said this place was built on a magic field of some sort.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Yep, it repulses anyone or anything with violent intentions.[SOFTBLOCK] Very powerful, very old.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Do you have a name?[SOFTBLOCK] Should I call you Drone C-137 or something?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei will do just fine, but thank you for asking.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: So what can I do for you, if you're not looking for nectar?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm looking for someone who can tell me more about this runestone.[SOFTBLOCK] It seems to have magical powers, and the hive wants to know how it works.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Runestone, eh?[SOFTBLOCK] Let me take a look at it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well I don't recognize that symbol at all.[SOFTBLOCK] Magic powers you said?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Quite.[SOFTBLOCK] It seems to have a mind of its own.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Huh.[SOFTBLOCK] I can't help you, but I bet I know who can.[SOFTBLOCK] Florentina.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: There's a trading post southwest of here, across the river.[SOFTBLOCK] She runs the provisions shop there.[SOFTBLOCK] She tends to get up to all sorts of shady stuff.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I reckon she knows a mage or two who could help you out.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: But if you want to stick around and chat, I'd love to hear what you have to say.[SOFTBLOCK] Don't get a lot of bees in here...") ]])
		
	--Mei is a Ghost.
	elseif(sMeiForm == "Ghost") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh dearest me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Are you -[SOFTBLOCK] a ghost?[SOFTBLOCK] Like an actual ghost?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Sort of.[SOFTBLOCK] It's quite complicated.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Do you -[SOFTBLOCK] are you here for unfinished business, or something like that?[SOFTBLOCK] I love to help![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] This is not the reaction I was expecting.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I was assuming you'd be fleeing in terror right now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'd never run from someone who needs my help![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Well, if you could help me get back to Earth, that'd be lovely.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, well, hmm.[SOFTBLOCK] Sorry, ghosty...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] My name is Nata -[SOFTBLOCK] Mei.[SOFTBLOCK] My name is Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Sorry Ms. Mei, but I don't know where Earth is.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh.[SOFTBLOCK] Unfortunate.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Buck up, deadite![SOFTBLOCK] I'm sure my friend Florentina can help you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: She knows everyone from wizards to cartographers![SOFTBLOCK] If anyone knows how to get to Earth, it's her![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You just head southeast of here, across the river, and she'll be at the trading post.[SOFTBLOCK] I bet she'd love to meet you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That's actually some very good news.[SOFTBLOCK] Thank you very much, Ms. Breanne.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: How'd you know my name?[SOFTBLOCK] Is it ghost powers?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] It's...[SOFTBLOCK] written on the sign out front...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, oops.[SOFTBLOCK] I forgot about that.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well, Mei.[SOFTBLOCK] This is my Pit Stop.[SOFTBLOCK] If you're tired of your ghostly journey, I'd love to have you for company.[SOFTBLOCK] I can sell you supplies, too, if you need them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: ...[SOFTBLOCK] Do ghosts need supplies?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Not really, but I appreciate the offer.") ]])
	
	--Mei is a Werecat.
	elseif(sMeiForm == "Werecat") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well aren't you just the cutest thing in the world![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Purr...[SOFTBLOCK] Purr...[SOFTBLOCK][EMOTION|Mei|Surprise] Hey![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Don't scratch me like -[SOFTBLOCK] ooooohhhpurrrr....[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You're a devil with those fingernails...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You want some chicken there little lady?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] I'd love -[SOFTBLOCK][EMOTION|Mei|Offended] stop that![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I am not a housecat![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Works every time![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: So what can I do for you, miss..?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm looking for someone who can tell me about this runestone, or maybe tell me how to get home.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: On the runestone?[SOFTBLOCK] Mm, nope, I've got nothing.[SOFTBLOCK] Sorry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Where's home?[SOFTBLOCK] I thought you cats lived out in the forest.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I've not been a fang for very long, actually, and I'd quite like to return to Hong Kong.[SOFTBLOCK] Let my parents know I'm all right.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Judging from what I've seen, I'm not on Earth anymore, am I?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Earth?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] As I expected.[SOFTBLOCK] The moon here is different...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No matter, the hunt for a way home will yield all the more glorious a catch![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'm not much for magic or the like.[SOFTBLOCK] Can't really help you there.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Unfortunate.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: But I think I know who can - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Florentina?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: That's who I was about to suggest![SOFTBLOCK] How'd you know?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Nadia mentioned her.[SOFTBLOCK] We're acquainted.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I can't direct you to a more knowledgeable sort than her![SOFTBLOCK] Just head over to the trading post and tell her I said hi![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Purr...[SOFTBLOCK] thank you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Now if you want to stick around, I wasn't kidding about the chicken...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Mmm, you know exactly what to say!") ]])
	
	--Mei is a Human, also the unhandled case.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Howdy, stranger![SOFTBLOCK] Welcome to my little corner of the world.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Thank goodness, a friendly face.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You won't find a friendlier one, that's for sure.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Considering how many roving monsters and thugs there are, you're awfully laid back.[SOFTBLOCK] Are you all alone?[SOFTBLOCK] Aren't you afraid?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Me?[SOFTBLOCK] Pah, I ain't scared of a few slimes.[SOFTBLOCK] Besides, I sited this here building right on top of a magic field.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Found out about it from some survey archives.[SOFTBLOCK] Apparently, it sends packing anyone who has violent intentions.[SOFTBLOCK] Very potent![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] So you don't mind if I rest here for a while?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I called this place the Pit Stop for a reason.[SOFTBLOCK] You traders need to slow down and take it easy every now and then.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, I'm not a trader.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Then what brings you all the way out to Evermoon forest?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Well...[SOFTBLOCK] You see, I think some goons kidnapped me.[SOFTBLOCK] Do you know about the weirdos in the building across the lake?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Sorry, but I don't go out that way very much.[SOFTBLOCK] How did you escape?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I ran and fought my way out.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Since then, I've been trying to find out where I am.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'd say you're in my Pit Stop right now.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Heh, I meant how to get back home.[SOFTBLOCK] I'm a real long way, since we sure didn't have monster girls on Earth.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Earth?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Do you recognize the name?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Nope, sorry.[SOFTBLOCK] It must be real far.[SOFTBLOCK] Pandemonium is a big place.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] *sigh*[SOFTBLOCK] I was afraid you were going to say that, but I guess I'm not surprised.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do you have any idea who might know how to get to Earth?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: We get an awful lot of travellers through here, but I don't think I've ever took to discussing geography with them.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: If anyone knows, it'd be that Florentina over at the trading post.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Head southwest through the forest, across the river.[SOFTBLOCK] She runs the provisions shop.[SOFTBLOCK] Tell her I said hi![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I might just stick around here for a little bit.[SOFTBLOCK] I need to catch my breath.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Stay as long as you like![SOFTBLOCK] I love having company.[SOFTBLOCK] Plus you probably know all kinds of stuff about Earth.[SOFTBLOCK] You can tell me all about it!") ]])
	
		--Topics
		WD_SetProperty("Unlock Topic", "Pandemonium", 1)
	end
	
--Florentina is also present.
else
	
	--Flag: Breanne met Mei when Florentina was present.
	VM_SetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N", 1.0)
	
	--Variables.
	local iMetClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
	local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
	
	--Topic unlocks. Breanne mentioned Outland Farm, Quantir, and Claudia's Convent.
	WD_SetProperty("Unlock Topic", "TradingPost", 1)
	WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
	WD_SetProperty("Unlock Topic", "Claudia", 1)

	--Mei is an Alraune.
	if(sMeiForm == "Alraune") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well well, if it isn't Florentina![SOFTBLOCK] And you brought a leaf-sister![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: That's what you call each other, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Hrmpf.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Did I say something wrong?[SOFTBLOCK] Sorry...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] No, it's fine.[SOFTBLOCK] Florentina isn't like that, but normally we call each other leaf-sister.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: And you are?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Call me Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well hello, Mei.[SOFTBLOCK] And long time, no see, Florentina.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Yeah, sure.[SOFTBLOCK] We're not here for pleasantries.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Are you ever?[SOFTBLOCK] I got some of that Runsdet fungus if you're looking to buy.[BLOCK][CLEAR]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Not right now...[SOFTBLOCK][E|Neutral] Actually, we're looking for Sister Claudia.[SOFTBLOCK] Is she staying here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Claudia?[SOFTBLOCK] I remember she and her convent came through here a few weeks ago.[SOFTBLOCK] Very tidy, not a hair out of place on them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Where did they go?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: They bought a bunch of supplies and headed out northeast, but I don't know where they were going.[SOFTBLOCK] They could be anywhere by now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I was over at the Outland Farm the other day, and one of her followers was there.[SOFTBLOCK] Maybe you should ask there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thank you, you've been a great help.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Hey, if you're not too busy, you can stick around and chat for a bit.[SOFTBLOCK] I love meeting new people![SOFTBLOCK] Or plants, in your case.") ]])
			WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
		
		--Mei is looking for Rilmani information.
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Not right now...[SOFTBLOCK][E|Neutral] Actually, we're looking for anything we can find on the Rilmani.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: The whatsit now?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This runestone has one of their symbols on it.[SOFTBLOCK] Have you ever seen anything like it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mmm, no, sorry.[SOFTBLOCK] I don't know much about them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[SOFTBLOCK] You be careful if you're going to find one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Pff.[SOFTBLOCK] I bet they're not very tough.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		
		end
	
	--Mei is a Slime.
	elseif(sMeiForm == "Slime") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh my![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Florentina, there's a slime![SOFTBLOCK] Right there![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yes, I see it.[SOFTBLOCK] Pretty nifty, isn't it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Did you manage to tame one?[SOFTBLOCK] Is that your pet?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Yes.[SOFTBLOCK] Absolutely.[SOFTBLOCK] Mei, fetch![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: It's name is Mei?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Don't treat me like a dog![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You taught it to talk, too?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] This has been so worth it already.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I don't - [BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: What is going on here?[BLOCK][CLEAR]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I'm not her pet![SOFTBLOCK] We're trying to find Sister Claudia![SOFTBLOCK] She's just pranking you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: But you're a slime, right?[SOFTBLOCK] Is that a disguise?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's complicated.[SOFTBLOCK] Sorry to be rude, but have you seen Claudia?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Sheesh.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: She came through here a few weeks ago with her convent, and then left after getting some supplies.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: They didn't say where they were going, but they did go northeast through the woods.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Thank you![SOFTBLOCK] You're very kind.[SOFTBLOCK] You could learn a thing or two, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: She's been like this for years.[SOFTBLOCK] If she were going to learn manners, she'd have done it by now.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Offended] Nice to see you, too, Breanne.[SOFTBLOCK] We'll be off, then.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Why the hurry?[SOFTBLOCK] Don't you want to have a chat for a bit?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] If you're bored, you're doing a bad job of hiding it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: It's the off season.[SOFTBLOCK] You're not one to talk, you're not busy either.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I am, in fact, very busy.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: So busy that you can't go traipsing through the forest with this very polite slime?[SOFTBLOCK] Yes, you're sooo busy.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hypatia has the shop covered, and I'm insured against Hypatia-related incidents.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: All right, but the offer still stands.") ]])
		
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I'm not her pet![SOFTBLOCK] We're looking for information![SOFTBLOCK] She's just pranking you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: But you're a slime, right?[SOFTBLOCK] Is that a disguise?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's complicated.[SOFTBLOCK] I don't mean to be rude.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, don't worry about that, hun.[SOFTBLOCK] Relative to Florentina, you're a saint![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'll not argue that point.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Nor will I.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] So, spill it.[SOFTBLOCK] What do you know about Rilmani?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: The whatsit now?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This runestone has one of their symbols on it.[SOFTBLOCK] Have you ever seen anything like it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mmm, no, sorry.[SOFTBLOCK] I don't know much about them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[SOFTBLOCK] You be careful if you're going to find one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Pff.[SOFTBLOCK] I bet they're not very tough.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
		
	--Mei is a Bee.
	elseif(sMeiForm == "Bee") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: My goodness![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Florentina, if you're looking for someplace to be alone with this bee...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] What is she talking about?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Uhhhhh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Yes?[SOFTBLOCK] Oh.[SOFTBLOCK][E|Surprise] Oh![SOFTBLOCK] ...[SOFTBLOCK][E|Blush] Maybe later...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You can talk?[SOFTBLOCK] That's a first, I've never met a talking bee![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh, I'm sorry.[SOFTBLOCK] Yes, I can speak with humans.[SOFTBLOCK] I'm a bit different than the other bees.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] *Florentina, my sisters just said...*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *Shhh![SOFTBLOCK] Not in front of Breanne!*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Right, sorry about that.[SOFTBLOCK] My name is Mei, and we're here on a mission for the hive.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Wow![SOFTBLOCK] You bees are a lot more complex than I thought![SOFTBLOCK] What can I help you with?[BLOCK][CLEAR]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We're looking for Sister Claudia.[SOFTBLOCK] Florentina said she was going this way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Yep, but she went through a few weeks ago.[SOFTBLOCK] You'll have to hustle to catch up with her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Did she say where she was going?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I didn't think to ask, but she went northeast through the woods.[SOFTBLOCK] That'd be the way to go.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Thank you very much.[SOFTBLOCK] I'll let my sisters know how helpful you were.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I'm sure they'll return the favour by jumping you next time you go out.[SOFTBLOCK] Be on your guard.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] She'd make an excellent drone![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: That's a compliment, right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Of course.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I'll be careful, then.[SOFTBLOCK] Oh, speaking of hives, I think one of Claudia's followers was observing bees at the farm across the way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We are aware of her.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: We?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The hive.[SOFTBLOCK] Sometimes I forget myself.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You know, if you're not in a hurry, I'd love to hear all about it.[SOFTBLOCK] My door is always open if you want to chat.") ]])
			WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We're looking for any information on the Rilmani we can find.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: The whatsit now?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This runestone has one of their symbols on it.[SOFTBLOCK] Have you ever seen anything like it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mmm, no, sorry.[SOFTBLOCK] I don't know much about them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[SOFTBLOCK] You be careful if you're going to find one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Pff.[SOFTBLOCK] I bet they're not very tough.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
	
	--Mei is a Ghost.
	elseif(sMeiForm == "Ghost") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: My goodness![SOFTBLOCK] Florentina, it seems one of your victims has come back to haunt you![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Oh, very funny.[SOFTBLOCK] Hilarious.[SOFTBLOCK] It's so funny I forgot to laugh.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You're Miss Breanne?[SOFTBLOCK] Hello, it's a pleasure to meet you.[SOFTBLOCK] My name is Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not normally a ghost.[SOFTBLOCK] It's quite complicated.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Anyone who says please and thank you is welcome here.[SOFTBLOCK] Florentina is, too![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You are just on a roll here, aren't you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: So what can I do for you two?[BLOCK][CLEAR]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We're looking for Sister Claudia.[SOFTBLOCK] Florentina thinks she might know how to get back to Earth.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Hmmm, Claudia...[SOFTBLOCK] Oh![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: She took her convent northeast of here a few weeks back.[SOFTBLOCK] I think they were going to go look into that mansion up north.[SOFTBLOCK] You know, the one that's supposed to be...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Haunted...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We've been up that way, yes.[SOFTBLOCK] I suppose we'll have to keep searching.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Sorry I couldn't help.[SOFTBLOCK] Would you like me to fix you something to eat?[SOFTBLOCK] Oh, oops.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thank you for the offer, but I'll never be hungry again.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I really don't know how to offer hospitality to a ghost![SOFTBLOCK] I'm new at this![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It was very nice talking with you.[SOFTBLOCK] Perhaps that's enough?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: It'll have to be.[SOFTBLOCK] Stay as long as you like!") ]])
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We're looking for any information on the Rilmani we can find.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: The whatsit now?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This runestone has one of their symbols on it.[SOFTBLOCK] Have you ever seen anything like it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mmm, no, sorry.[SOFTBLOCK] I don't know much about them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[SOFTBLOCK] You be careful if you're going to find one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Pff.[SOFTBLOCK] I bet they're not very tough.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
	
	--Mei is a Werecat.
	elseif(sMeiForm == "Werecat") then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well hello, Florentina![SOFTBLOCK] And -[SOFTBLOCK] hmmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Why are you looking at me like that?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Some of my fish have gone missing recently.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I've had nothing to do with it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] This has been so worth it already.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Of course you'd be behind it.[SOFTBLOCK] Did you hire a werecat to annoy me?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Hire?[SOFTBLOCK] Perish the thought, she'd never part with a penny![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] If I got something out of it, I would.[SOFTBLOCK] It's called an investment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Speaking of investments, Mei, why don't you tell Breanne why we're here?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Always business, never pleasure.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Business [SOFTBLOCK]*is*[SOFTBLOCK] pleasure, Breanne.[BLOCK][CLEAR]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We're looking for Sister Claudia.[SOFTBLOCK] Have you seen her recently?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Claudia?[SOFTBLOCK] Oh, Claudia, right...[SOFTBLOCK] the monk?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Her convent came through here a few weeks ago.[SOFTBLOCK] I think they went northeast after that.[SOFTBLOCK] I didn't think to ask them why.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hm, the kinfang pride is to the northeast.[SOFTBLOCK] I wonder if any of them hunted Claudia?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Guess we're going that way, then.[SOFTBLOCK] Let's go.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You know, if you weren't so curt I might tell you another useful fact.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Sheesh, what is it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Please don't let Florentina's brash attitude get to you.[SOFTBLOCK] She's helping me, isn't she?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I suppose so, but there's probably something in it for her.[SOFTBLOCK] I don't believe I got your name, miss..?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Mei.[SOFTBLOCK] Pleased to meet you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Well, one of Claudia's followers was over at Outland Farm across the river, last I saw.[SOFTBLOCK] If they were going someplace, she'd know.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Thank you kindly, miss Breanne.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, some tip.[SOFTBLOCK] We were going that way anyway.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You're welcome, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: My door is always open if you want to stay a bit.[SOFTBLOCK] That goes for you too, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Sure it does.[SOFTBLOCK] Mei, shall we?") ]])
			WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We're looking for any information on the Rilmani we can find.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: The whatsit now?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This runestone has one of their symbols on it.[SOFTBLOCK] Have you ever seen anything like it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mmm, no, sorry.[SOFTBLOCK] I don't know much about them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[SOFTBLOCK] You be careful if you're going to find one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Pff.[SOFTBLOCK] I bet they're not very tough.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
		
	--Mei is a Human, also the unhandled case.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh, Florentina![SOFTBLOCK]  And you've brought a friend![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: [E|Neutral]Yes, pleasantries to you, too.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: It wouldn't kill you to say hello every now and then.[SOFTBLOCK] When they say \"Don't be a stranger\", they're talking about you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: [E|Happy]Hello, miss Breanne.[SOFTBLOCK] My name is Mei, and we're looking for someone.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: You see?[SOFTBLOCK] She's not retching on the floor.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: [E|Offended]But she is wasting time.[SOFTBLOCK] We could have already been on our way, which would have suited you just fine.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Don't be terse.[SOFTBLOCK] So, Mei was it?[SOFTBLOCK] What can I do for you?[BLOCK][CLEAR]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: [E|Neutral]We're trying to find Sister Claudia.[SOFTBLOCK] Florentina said she may have come out this way.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Claudia...[SOFTBLOCK] Claudia...[SOFTBLOCK] oh yes, the researcher.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: She came through here a few weeks back.[SOFTBLOCK] Not here now, unfortunately.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: [E|Neutral]Did she say where she was going?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I didn't think to ask.[SOFTBLOCK] Her convent weren't the most talkative types.[SOFTBLOCK] Very interested in that mansion across the lake, but said someone drove them off.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: [E|Sad]Crud.[SOFTBLOCK] That's where I came from, and she wasn't kidding.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: When they left, they were going northeast through the woods.[SOFTBLOCK] That might be a place to start.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: [E|Neutral]What's northeast of here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: The path goes by the old Quantir estate.[SOFTBLOCK] It's abandoned, but you might be able to find a campsite.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Oh![SOFTBLOCK] I think there was someone from her convent researching bees over at Outland Farm west of here.[SOFTBLOCK]  You might want to check with them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: [E|Happy]Thank you very much Breanne![SOFTBLOCK]  You've been a huge help.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: I love helping out strangers.[SOFTBLOCK]  If you're not in a hurry, I could make some tea.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: [E|Offended]That's all right.[SOFTBLOCK] No need for that.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Nice to see you, too, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: [E|Happy]Of course.[SOFTBLOCK] Mei, shall we be off?") ]])
			WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We're looking for any information on the Rilmani we can find.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: The whatsit now?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This runestone has one of their symbols on it.[SOFTBLOCK] Have you ever seen anything like it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Mmm, no, sorry.[SOFTBLOCK] I don't know much about them.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[SOFTBLOCK] You be careful if you're going to find one.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Pff.[SOFTBLOCK] I bet they're not very tough.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
	
	end

end

--Common code.
fnCutsceneBlocker()

--[Finish Up]
--If Florentina is present, path onto Mei and fold the party up.
if(bIsFlorentinaHere) then
	Cutscene_CreateEvent("Move Florentina To Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (14.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end

--Flags.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetFriendly", "N", 1.0)

--Unlock topics.
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Name", 1)

--Clear the flag on Next Move.
WD_SetProperty("Clear Topic Read", "NextMove")
