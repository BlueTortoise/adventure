--[Build Scene List]
--Builds a list of cutscenes the player can make use of through the debug menu. Cutscenes in the debug menu are
-- used at the player's own risk, they may produce unusable situations that can only be remedied with the debug menu.
-- It's an addiction, I tell you.
local saSceneList = {"A Quickremove Florentina", "A Quickadd Florentina", "A Quickset AdinaControl", "A QuicklevelCh1BossSet", "A QuicklevelCh1Max", "A Quickset SavedClaudia", "A Quickset SavedCassandra",
					 "Basement_CultistMeeting",
					 "Breanne_FirstScene",
					 "CutsceneSample", 
					 "Defeat_Alraune", "Defeat_BackToSave", "Defeat_Bee", "Defeat_BeeAsAlraune", "Defeat_Cultists", "Defeat_Ghost", "Defeat_Slime", "Defeat_Werecat", "Defeat_Zombee",
					 "PlainsNW_PostBeeScene", 
	                 "Trannadar_FirstSceneAlraune", "Trannadar_FirstSceneBee", "Trannadar_FirstSceneHuman", "Trannadar_FirstSceneSlime", 
					 "Trannadar_SecondSceneAlraune", "Trannadar_SecondSceneBee", "Trannadar_SecondSceneSlime", "Trannadar_ThirdScene", "Trannadar_Florentina", "Trannadar_NoScene",
					 "Transform_MeiToAlraune", "Transform_MeiToBee", "Transform_MeiToHuman", "Transform_MeiToSlime"}

--Now set as necessary.
local i = 1
ADebug_SetProperty("Cutscenes Total", #saSceneList)
while(saSceneList[i] ~= nil) do
	ADebug_SetProperty("Cutscene Path", i-1, saSceneList[i])
	i = i + 1
end