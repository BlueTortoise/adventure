--[Defeat By Alraune]
--Cutscene proper. Uses the dialogue's SceneHandler to get the TF sequence.
local bSkipMostOfScene = false

--[Variables]
local iMeiVolunteeredToAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N")
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")

--[Repeat Check]
--If Mei can already turn into a Alraune, this scene does not play.
local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
if(iHasAlrauneForm == 1 and iIsRelivingScene == 0.0) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

--[Map Check]
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "AlrauneChamber") then

	--During a relive, neither of these is checked:
	if(iIsRelivingScene == 0.0) then

		--Emergency revert does not run if Mei volunteered.
		if(iMeiVolunteeredToAlraune == 1.0) then

		--Run the sub-cutscene. It adds a lot of events, and will fadeout the camera automatically.
		else
			LM_ExecuteScript(gsRoot .. "Chapter1Scenes/Z Emergency Revert/Scene_Begin.lua")
		end
	end

	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"AlrauneChamber\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutsceneInstruction(sString)
	return
end

--[Remove Florentina]
--If Florentina is in the party, remove her. She rejoins shortly.
if(gsFollowersTotal > 0) then
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AC_SetProperty("Set Party", 1, "Null")
	AL_SetProperty("Unfollow Actor Name", "Florentina")
end

--If the Florentina entity happens to be on the field, move her off.
if(EM_Exists("Florentina") == true) then
	EM_PushEntity("Florentina")
		TA_SetProperty("Position", -10, -10)
	DL_PopActiveObject()
end

--[Form]
--Reshift Mei back into a Human. This keeps the scenes looking sane.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

--[Combat]
--Restore party to full HP in case the player doesn't want to revisit a save point.
AC_SetProperty("Restore Party")

--[Topics]
--Unlock these topics if they weren't already.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "CleansingFungus", 1)

--[Music]
AL_SetProperty("Music", "Null")

--[Cutscene Execution]
--Mei can now turn into an Alraune whenever she wants. Good job, Mei!
VM_SetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N", 1.0)

--Fade from black to nothing over 120 ticks. This fade is under the UI.
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Reposition Mei, make her use the wounded image.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 17, 8)
	TA_SetProperty("Set Special Frame", "Wounded")
DL_PopActiveObject()
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1.0)
DL_PopActiveObject()

--Face Rochea east.
if(EM_Exists("Rochea") == true) then
	EM_PushEntity("Rochea")
		TA_SetProperty("Facing", gci_Face_East)
	DL_PopActiveObject()
end

--Extra Alraunes
TA_Create("AlrauneA")
	TA_SetProperty("Position", 17, 6)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_Basement_A/Root.lua")
	TA_SetProperty("Activate Wander Mode")
DL_PopActiveObject()
TA_Create("AlrauneB")
	TA_SetProperty("Position", 11, 10)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_East)
	TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_Basement_B/Root.lua")
	TA_SetProperty("Activate Wander Mode")
DL_PopActiveObject()
TA_Create("AlrauneC")
	TA_SetProperty("Position", 12, 6)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_East)
	TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Alraune_Basement_C/Root.lua")
	TA_SetProperty("Activate Wander Mode")
DL_PopActiveObject()

--Scene setup.
fnPartyStopMovement()

--Wait a bit for the fade.
fnCutsceneWait(90)
fnCutsceneBlocker()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: H-[SOFTBLOCK]huh?[SOFTBLOCK] Where...") ]])
fnCutsceneBlocker()

--[Dialogue Sequence]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Shhh...[SOFTBLOCK] All is well...") ]])
fnCutsceneBlocker()

--[Fade]
--Set a black overlay.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 35, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--[TF Sequence]
--Scene part 0.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])

--Mei did not volunteer:
if(iMeiVolunteeredToAlraune == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slowly, Mei came to her senses.[SOFTBLOCK] The faint smell of pollen suggested what the plant girl had used to stun her, and she struggled to overcome its effects.[SOFTBLOCK] Her body was lethargic and she could not resist as the Alraune carried her towards a strange yellow pool.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "As her mind began to clear, she realized there were more of them, watching her from the edges of the room.[SOFTBLOCK] She was now underground, somewhere where creeping vines had grown all over the walls.[SOFTBLOCK] The air was thick with the smell of plant matter, and the pool seemed to draw fluid from the leafy matter surrounding it.") ]])

--Mei volunteered. Text is slightly different.
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Slowly, Mei came to her senses.[SOFTBLOCK] The pollen of the Alraune still lingered within her.[SOFTBLOCK] She felt relaxed and at peace.[SOFTBLOCK] A plant girl, different from the one who had brought her here, was leading her towards a yellow, glistening pool...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "As her mind began to clear, she realized there were more of them, watching her from the edges of the room.[SOFTBLOCK] She was now underground, somewhere where creeping vines had grown all over the walls.[SOFTBLOCK] The air was thick with the smell of plant matter, and the pool seemed to draw fluid from the leafy matter surrounding it.") ]])

end
fnCutsceneBlocker()

--Scene part 1.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/AlrauneTF0") ]])

--Mei did not volunteer:
if(iMeiVolunteeredToAlraune == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The plant girl lowered Mei into the pool, carefully and gently.[SOFTBLOCK] Still helpless to resist, Mei felt the warm and comforting embrace of the pool flow over her, reaching up to her neck.[SOFTBLOCK] The Alraune joined her in the pool, supporting her and keeping her head above the surface.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her skin changed first, becoming blue like the girl who had brought her here.[SOFTBLOCK] Despite the warmth of the water, Mei still shivered as her body changed.") ]])
	
--Mei volunteered. Text is slightly different.
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "The plant girl lowered Mei into the pool, carefully and gently.[SOFTBLOCK] Mei felt the warm and comforting embrace of the pool flow over her, reaching up to her neck, relaxing her further.[SOFTBLOCK] The Alraune joined her in the pool, supporting her and keeping her head above the surface.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Her skin changed first, becoming blue like the girl who had brought her here.[SOFTBLOCK] Despite the warmth of the water, Mei still shivered as her body changed.") ]])
end
fnCutsceneBlocker()

--Scene part 2.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/AlrauneTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/AlrauneTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the fluid soaked into her now porous skin, the changes flowed into her head and along her hair, turning it green.[SOFTBLOCK] Her eyes lost focus, and she blinked several times as if to clear them.[SOFTBLOCK] Soon, her eyes had reddened, allowing her to see colors that no human could, the edges of the room scintillating as she drew her eyes across them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Still supported by the Alraune who had brought her here, Mei now felt enough strength to free herself.[SOFTBLOCK] She tugged, and the plant girl allowed her to float freely.[SOFTBLOCK] Relaxed and calm, Mei realized her head was still partly human.[SOFTBLOCK] This thought irked her.[SOFTBLOCK] She slid below the surface to allow the fluid to soak into her head and complete her transformation.") ]])
fnCutsceneBlocker()

--Disable the special frame.
Cutscene_CreateEvent("Stop Crouch Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, 0)
DL_PopActiveObject()
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])

--Scene part 3.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/AlrauneTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Alraune") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rising from the pool, Mei's transformation had completed.[SOFTBLOCK] Her humanity gone, her body blue and green like the women who had brought her here.[SOFTBLOCK] She heard murmurs from the other Alraunes watching her, welcoming her to their family.[SOFTBLOCK] She smiled.") ]])
fnCutsceneBlocker()

--[Fade]
--Clear the overlay.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 35, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--[Dialogue Sequence]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] That whispering...[SOFTBLOCK] I can hear the voices of the forest![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Welcome, leaf-sister.[SOFTBLOCK] What is your name?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Mei.[SOFTBLOCK] My name is Mei, leaf-sister.[SOFTBLOCK] Thank you, thank you![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: I am Rochea.[SOFTBLOCK] I am honored to be allowed to join you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] As I am honored to be joined![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Now that your body has become pure, your mind must follow suit.[SOFTBLOCK] Will you partake of the cleansing fungus?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Cleansing fungus?[SOFTBLOCK] What is that?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Many of those we bring here wish to forget their human past, to join the forest as we have.[SOFTBLOCK] They must unlearn their hateful behaviors to live in harmony.[SOFTBLOCK] For that, we have the cleansing fungus.[SOFTBLOCK] It will purge the memories of your old life and you will be reborn as one of us.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I want to, I want to very badly.[SOFTBLOCK] Yet, I cannot.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Are you certain?[SOFTBLOCK] A life of love and sorority is within your grasp;[SOFTBLOCK] do not allow your human past to own your future.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It is not my wish, it is this runestone I found.[SOFTBLOCK] I think it brought me here, to this world, and to you.[SOFTBLOCK] For me, it has a greater design, and I must follow it.[SOFTBLOCK] I am sorry...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: ...[SOFTBLOCK] I see that you are determined.[SOFTBLOCK] It is not our way to force you to stay with us.[SOFTBLOCK] Go, leaf-sister.[SOFTBLOCK] Nature will guide you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Thank you for understanding.[SOFTBLOCK] I hope I can return when I have the answers I need.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: But before you go...[SOFTBLOCK] please take this.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] [SOUND|World|TakeItem](Received Wildflower's Katana)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] A sword?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Left here by a leaf-sister who chose a life of peace.[SOFTBLOCK] We have enchanted it, and while we hope you never need to use it...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Never in anger, leaf-sister.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rochea: Go, Mei, find your path.[SOFTBLOCK] May we meet again when the time is right.") ]])
fnCutsceneBlocker()

--[Special]
--If this is a reliving sequence, end it here.
if(iIsRelivingScene == 1.0) then
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Unset this flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 0.0)
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter1Scenes/Z Relive Handler/Scene_End.lua") ]])
	fnCutsceneBlocker()

--[Finish Up]
--Mei is not reliving this, it's real!
else

	--Give Mei a Wildflower's Katana.
	LM_ExecuteScript(gsItemListing, "Wildflower's Katana")

	--Increment Mei's KO counter.
	local iPartyKOCount = VM_GetVar("Root/Variables/Chapter1/Scenes/iPartyKOCount", "N")
	VM_SetVar("Root/Variables/Chapter1/Scenes/iPartyKOCount", "N", iPartyKOCount + 1)

	--Mark this flag if Florentina was in the party.
	local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 1.0)
	end

	--Topics.
	WD_SetProperty("Unlock Topic", "Alraunes", 1)
	WD_SetProperty("Unlock Topic", "CleansingFungus", 1)
	WD_SetProperty("Unlock Topic", "Cultists", 1)
	WD_SetProperty("Unlock Topic", "Name", 1)
	
end
