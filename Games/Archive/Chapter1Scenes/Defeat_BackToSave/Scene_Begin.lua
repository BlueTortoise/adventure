--[Defeat By Anything, Back to Save]
--Cutscene that executes when the player is beaten by something that has no special cutscene attached. Alternately,
-- it will also fire when another defeat cutscene has already been seen, and won't play again.
--This part of the script just sends the party back to the last save point.
AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)

--Drop any other events.
Cutscene_CreateEvent("DROPALLEVENTS")

--If we are currently in the last-saved room, reposition the party.
if(AL_GetProperty("Last Save") == AL_GetProperty("Name") and false) then
	AL_BeginTransitionTo("LASTSAVEINSTANT", fnResolvePath() .. "Scene_PostTransition.lua")
	
--Otherwise, transition to that room.
else
	AL_BeginTransitionTo("LASTSAVE", fnResolvePath() .. "Scene_PostTransition.lua")
end

--[Combat]
--Restore party to full HP in case the player doesn't want to use the save point. This saves time, and is a quality-of-life feature.
AC_SetProperty("Restore Party")