--[Defeat By Bee]
--Cutscene proper. Uses the dialogue's SceneHandler to get the TF sequence.
local bSkipMostOfScene = false

--[Repeat Check]
--If Mei can already turn into a bee, this scene does not play.
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
if(iHasBeeForm == 1.0 and iIsRelivingScene == 0.0) then
	
	--[Alraune Form]
	--If Mei is in Alraune form, run that cutscene instead.
	local sForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sForm == "Alraune") then
		LM_ExecuteScript(gsRoot .. "Chapter1Scenes/Defeat_BeeAsAlraune/Scene_Begin.lua")
	
	--[Normal]
	--Otherwise, run the normal game-over script.
	else
		LM_ExecuteScript(gsStandardGameOver)
	end
	return
end

--[Knockout Scene]
--If we're not on the cutscene map, knock down both Mei and Florentina. Revert Mei back to human form if she's not in that already.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "BeehiveInner") then

	--Run the sub-cutscene. It adds a lot of events. Doesn't run during a relive.
	if(iIsRelivingScene == 0.0) then
		LM_ExecuteScript(gsRoot .. "Chapter1Scenes/Z Emergency Revert/Scene_Begin.lua")
	end
	
	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"BeehiveInner\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutsceneInstruction(sString)
	return
end

--Special variables.
local iUseSpecialBeeOpening = VM_GetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N")

--[Remove Florentina]
--Do it immediately in this case.
if(iUseSpecialBeeOpening == 0.0) then
	
	--If Florentina is in the party, remove her. She rejoins shortly.
	if(gsFollowersTotal > 0) then
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AC_SetProperty("Set Party", 1, "Null")
		AL_SetProperty("Unfollow Actor Name", "Florentina")
	end

	--If the Florentina entity happens to be on the field, move her off.
	if(EM_Exists("Florentina") == true) then
		EM_PushEntity("Florentina")
			TA_SetProperty("Position", -10, -10)
		DL_PopActiveObject()
	end

--Do it enqueued if using the special instance.
else
fnCutsceneInstruction([[ 
	if(gsFollowersTotal > 0) then
		gsFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AC_SetProperty("Set Party", 1, "Null")
		AL_SetProperty("Unfollow Actor Name", "Florentina")
	end

	--If the Florentina entity happens to be on the field, move her off.
	if(EM_Exists("Florentina") == true) then
		EM_PushEntity("Florentina")
			TA_SetProperty("Position", -10, -10)
		DL_PopActiveObject()
	end
]])
end

--[Form]
--Reshift Mei back into a Human. This keeps the scenes looking sane.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

--[Combat]
--Restore party to full HP in case the player doesn't want to revisit a save point.
AC_SetProperty("Restore Party")

--[Topics]
--Unlock these topics if they weren't already.
WD_SetProperty("Unlock Topic", "Bees", 1)

--[Music]
AL_SetProperty("Music", "Null")

--[Cutscene Execution]
--Mei can now turn into a bee whenever she wants. Good job, Mei!
VM_SetVar("Root/Variables/Global/Mei/iHasBeeForm", "N", 1.0)

--Bee defeated the party.
if(iUseSpecialBeeOpening == 0.0) then
	
	--Fading.
	AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

	--Reposition Mei, make her use the wounded image.
	EM_PushEntity("Mei")
		TA_SetProperty("Position", 19, 8)
		TA_SetProperty("Set Special Frame", "Wounded")
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Mei South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 0, 1.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()

--Using the special scene, this needs to become an instruction.
else

	--Fade is an instruction.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	
	--Reposition is also an instruction.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Teleport To", 19.25 * gciSizePerTile, 8.50 * gciSizePerTile)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Mei South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 0, 1.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
end


--Scene setup.
fnPartyStopMovement()

--Wait a bit for the fade.
fnCutsceneWait(90)
fnCutsceneBlocker()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])

--Actual talking, Bee defeated party.
if(iUseSpecialBeeOpening == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (...[SOFTBLOCK] Oh, not the bees...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (H-hey![SOFTBLOCK] I'm stuck in something!)") ]])

--Talking, voluntary.
else
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Blush") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (...[SOFTBLOCK] mmm, I fell asleep?)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (And someone...[SOFTBLOCK] covered me in honey...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Mmmm...[SOFTBLOCK] It's so thick,[SOFTBLOCK] so sensual...[SOFTBLOCK] I thinking I like this dream.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] .[SOFTBLOCK].[SOFTBLOCK].[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (H-[SOFTBLOCK]hey wait![SOFTBLOCK] This isn't a dream![SOFTBLOCK] This is bad!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (What was I thinking?)") ]])
end
	
fnCutsceneWait(15)
fnCutsceneBlocker()

--Wait for fadeout.
fnCutsceneWait(15)
fnCutsceneBlocker()

--[Scene]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei slowly regained consciousness.[SOFTBLOCK] She tried to sit up, but her body refused to move.[SOFTBLOCK] Lifting her head as much as she could, she saw that she was covered in a layer of viscous honey.[SOFTBLOCK] It held her firmly to the ground.") ]])
fnCutsceneBlocker()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (It's no use, I can't move...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "(Move...[SOFTBLOCK] Pull...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Huh?[SOFTBLOCK] Who's there?)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "(Move...[SOFTBLOCK] Pull...)") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()

--Wait for fadeout.
fnCutsceneWait(15)
fnCutsceneBlocker()

--[Scene]
--Scene.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei frowned, but followed the instructions despite her previous attempts to escape.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "This time, the thick fluid seemed to flow more easily,[SOFTBLOCK] and after a brief struggle, the honey began to give way.") ]])
fnCutsceneBlocker()

--Mei stands up.
Cutscene_CreateEvent("Stop Crouch Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] (What do you know, it worked...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "(Consume...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (That voice again...)") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()

--[Scene Variables]
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasWorkedNectar", "N", 0.0)
