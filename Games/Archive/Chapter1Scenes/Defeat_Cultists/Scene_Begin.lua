--[Defeat By Cultists]
--Cutscene proper. The entire thing is in one cutscene file. The cutscene assumes the script fade is at fullblack
-- after the level transition occurs.
local bSkipMostOfScene = false

--[Repeat Check]
--If the player has already seen this... scene... then we go straight to the respawn case.
local iHasSeenCultistScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenCultistScene", "N")
if(iHasSeenCultistScene == 1) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

--[Map Check]
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "DimensionalTrapBasementScene") then
	AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
	AL_BeginTransitionTo("DimensionalTrapBasementScene", LM_GetCallStack(0))
	return
end

--[Form]
--Reshift Mei back into a Human. This keeps the scenes looking sane.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

--[Combat]
--Restore party to full HP in case the player doesn't want to revisit a save point.
AC_SetProperty("Restore Party")

--[Music]
AL_SetProperty("Music", "ABillionEyes")

--[Cutscene Execution]
--Set the repeat flag. This needs to be done *after* the map transition.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenCultistScene", "N", 1.0)

--Increment how many times Mei has been ko'd.
local iPartyKOCount = VM_GetVar("Root/Variables/Chapter1/Scenes/iPartyKOCount", "N")
VM_SetVar("Root/Variables/Chapter1/Scenes/iPartyKOCount", "N", iPartyKOCount + 1)

--Fade from black to nothing over 60 ticks. This fade is under the UI.
AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Reposition Mei.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 10, 9)
	TA_SetProperty("Set Special Frame", "Wounded")
DL_PopActiveObject()
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move Amount", 0, 1.0)
DL_PopActiveObject()

--Create a cultist off the edge of the map.
if(EM_Exists("Scene_Cultist") == false) then
	TA_Create("Scene_Cultist")
		TA_SetProperty("Position", 10, 19)
		fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
	DL_PopActiveObject()

--Already there, just reposition.
else
	EM_PushEntity("Scene_Cultist")
		TA_SetProperty("Position", 10, 19)
	DL_PopActiveObject()
end

--Scene setup.
fnPartyStopMovement()

--Wait a bit for the fade.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Mei gets up. Wait a bit before speaking.
fnCutsceneWait(60)
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()
fnCutsceneBlocker()

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: W-[SOFTBLOCK]what happened? [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: My head is ringing... ") ]])

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Cultist walks up to the gate.
Cutscene_CreateEvent("Move Cultist North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Scene_Cultist")
	ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (14.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Face Cultist North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Scene_Cultist")
	ActorEvent_SetProperty("Face", 0, -1.0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Cultist opens the gate.
fnCutsceneWait(15)
fnCutsceneInstruction([[AL_SetProperty("Open Door", "Door")]])
fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
fnCutsceneBlocker()

--Cultist walks into the room.
Cutscene_CreateEvent("Move Cultist North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Scene_Cultist")
	ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (12.25 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneInstruction([[AL_SetProperty("Close Door", "Door")]])
fnCutsceneInstruction([[AudioManager_PlaySound("World|RemoteDoor")]])
fnCutsceneBlocker()
	
--Debug.
if(bSkipMostOfScene == false) then

	--Wait a bit.
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

	--Actual talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Unbeliever![SOFTBLOCK] Wake from your stupor! [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: You... [BLOCK][CLEAR]") ]])

	--Mei gets angry. Rightfully so!
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You have got some nerve, attacking someone like that![SOFTBLOCK] I'll - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Silence![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] How dare you![SOFTBLOCK] Let me go![SOFTBLOCK] This![SOFTBLOCK] Instant!") ]])
	fnCutsceneBlocker()
			
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
end

--Cultist walks up to Mei.
fnCutsceneWait(15)
fnCutsceneBlocker()

Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

Cutscene_CreateEvent("Move Cultist North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Scene_Cultist")
	ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (10.25 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
		
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Angry") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

--Threats!
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: I will remind you I am currently holding a knife, as well as the key to your cell.[BLOCK][CLEAR]") ]])
	
--Debug.
if(bSkipMostOfScene == false) then
	
	--More talking.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Think very carefully about your next words.[BLOCK][CLEAR]") ]])

	--Mei wisely backs off.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] What do you want from me?[BLOCK][CLEAR]") ]])

	--Threats!
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Our mercy is limitless.[SOFTBLOCK] We will forgive your trespasses if - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: Trespasses?[SOFTBLOCK] But I just woke up here...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] That's right![SOFTBLOCK] You kidnapped me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Knife...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ... [BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: All will be forgiven in exchange for one thing.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ... [BLOCK]What is it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Pledge your undying loyalty to us and our lord![SOFTBLOCK] Subsume yourself before his might![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ... [BLOCK] ... [BLOCK] I presume you mean whatever god it is you worship.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] In that case, my grandfather had a saying::[SOFTBLOCK] \"Better to die standing than live kneeling.\"[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: Is this a refusal?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] It's a way of saying \"Get stuffed\".[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I'm not scared of a petty thug like you.[SOFTBLOCK] Do your worst.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: We will do far worse things to you than your mortal perception can understand.[SOFTBLOCK] Death will be the beginning...") ]])
	fnCutsceneBlocker()
end
		
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Sound effect!
fnCutsceneInstruction([[ AL_SetProperty("Music", "ThemeOfCourage") ]])
fnCutsceneWait(90)
fnCutsceneBlocker()

--Cultist looks around, confused.
Cutscene_CreateEvent("Cultist Look Around", "Actor")
	ActorEvent_SetProperty("Subject Name", "Scene_Cultist")
	ActorEvent_SetProperty("Move Amount", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("Cultist Look Around", "Actor")
	ActorEvent_SetProperty("Subject Name", "Scene_Cultist")
	ActorEvent_SetProperty("Move Amount", 1, 0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("Cultist Look Around", "Actor")
	ActorEvent_SetProperty("Subject Name", "Scene_Cultist")
	ActorEvent_SetProperty("Move Amount", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Mei thinks to herself.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: (Hm?[BLOCK] That strange stone I found is glowing...)") ]])
fnCutsceneBlocker()

--Screen flashes white.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, true, 1, 1, 1, 0, 1, 1, 1, 0.99) ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

--Foul sorcery! Warpstone! Etc.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cultist: S-[SOFTBLOCK]sorcery![SOFTBLOCK] Foul sorcery![BLOCK][CLEAR]") ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--Screen goes to full white.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Over_GUI, true, 1, 1, 1, -1.0, 1, 1, 1, 1.00) ]])
fnCutsceneWait(30)
fnCutsceneBlocker()

--Scene transit. Go to last save point.
fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter1Scenes/Defeat_Cultists/Scene_PostTransition.lua") ]])
fnCutsceneBlocker()
