--[Volunteer To Ghost]
--Cutscene proper.

--Set.
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 1.0)
	
--In all cases, mark the combat intro dialogues as complete.
VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")

--Set these variables differently. Natalie is already wearing the uniform.
VM_SetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N", 1.0)

--Spawn Lydie.
TA_Create("Lydie")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", true)
	TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Lydie/Root.lua")
	fnSetCharacterGraphics("Root/Images/Sprites/MaidGhost/", false)
DL_PopActiveObject()

--[Remove Florentina]
--Take her out of the party, both on the status screen and overworld.
if(gsFollowersTotal > 0) then
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AC_SetProperty("Set Party", 1, "Null")
	AL_SetProperty("Unfollow Actor Name", "Florentina")
end

--If the Florentina entity happens to be on the field, move her here. Also change her dialogue.
if(EM_Exists("Florentina") == true) then
	EM_PushEntity("Florentina")
		TA_SetProperty("Position", 6, 5)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Florentina/MaidScene.lua")
		TA_SetProperty("Set Special Frame", "Crouch")
	DL_PopActiveObject()
end

--[Combat]
--Restore party to full HP in case the player doesn't want to revisit a save point.
AC_SetProperty("Restore Party")

--[Topics]
--Unlock these topics if they weren't already.
--None yet!

--[Flags]
--Variables.
local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

--[Music]
AL_SetProperty("Music", "Null")
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--[Camera]
--Move Mei to this position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (13.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Switch Mei to Ghost form.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Ghost.lua") ]])
	
--Mei temporarily changes name to Natalie.
fnCutsceneInstruction([[
AC_PushPartyMember("Mei")
	ACE_SetProperty("Display Name", "Natalie")
DL_PopActiveObject() ]])

--Unfade.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(75)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Smirk] (Hmmm hmmm hmmm...[SOFTBLOCK] dust this, dust that...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] (Oh, what time is it?[SOFTBLOCK] I was daydreaming about that Mei girl...[SOFTBLOCK] I've barely even cleaned this room!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Sad] (I hope the countess doesn't catch me, or she'll really let me have it!)") ]])
fnCutsceneBlocker()

--[Movement]
--A ghost enters the room.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Lydie")
	ActorEvent_SetProperty("Teleport To", (19.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Ghost moves up to Natalie.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Lydie")
	ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Natalie![SOFTBLOCK] What have you been doing!?[SOFTBLOCK] I thought you were supposed to clean this room![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Sad] I know![SOFTBLOCK] I'm sorry![SOFTBLOCK] I was kinda daydreaming...[SOFTBLOCK] and...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: You can tell me all about it later.[SOFTBLOCK] You've got to get this room cleaned up, pronto![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: I'm almost done my chores, so if you're really quick I promise we'll hang out when you're done.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Happy] Oh! I'll be sure to hurry![SOFTBLOCK] Thanks, Lydie![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Just hurry up!") ]])
fnCutsceneBlocker()
