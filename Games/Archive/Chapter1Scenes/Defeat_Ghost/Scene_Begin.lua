--[Defeat By Ghost]
--Cutscene proper.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")

--[Repeat Check]
--If Mei has already seen this scene, it's a normal KO.
local iHasGhostForm = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
if(iHasGhostForm == 1.0 and iIsRelivingScene == 0.0) then
	
	LM_ExecuteScript(gsStandardGameOver)
	return
end

--Set.
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 1.0)

--[Knockout Scene]
--If we're not on the cutscene map, knock down both Mei and Florentina.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "QuantirManseCentralW") then
	
	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"QuantirManseCentralW\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutsceneInstruction(sString)
	return
end

--[Remove Florentina]
--Take her out of the party, both on the status screen and overworld.
if(gsFollowersTotal > 0) then
	gsFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AC_SetProperty("Set Party", 1, "Null")
	AL_SetProperty("Unfollow Actor Name", "Florentina")
end

--If the Florentina entity happens to be on the field, move her here. Also change her dialogue.
if(EM_Exists("Florentina") == true) then
	EM_PushEntity("Florentina")
		TA_SetProperty("Position", 6, 5)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/Florentina/MaidScene.lua")
		TA_SetProperty("Set Special Frame", "Crouch")
	DL_PopActiveObject()
end

--[Combat]
--Restore party to full HP in case the player doesn't want to revisit a save point.
AC_SetProperty("Restore Party")

--[Topics]
--Unlock these topics if they weren't already.
--None yet!

--[Flags]
--Variables.
local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

--[Music]
AL_SetProperty("Music", "Null")
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--[Camera]
--Move Mei to this position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (13.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Switch Mei to Human form.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua") ]])
	
--Mei temporarily changes name to Natalie.
fnCutsceneInstruction([[
AC_PushPartyMember("Mei")
	ACE_SetProperty("Display Name", "Natalie")
DL_PopActiveObject() ]])

--Unfade.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(75)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Surprise] (.....![SOFTBLOCK] Oh, no, I was sleeping on the job!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] (That was a very strange dream, though.[SOFTBLOCK] I bet Lydie will want to hear all about it!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Sad] (No no, focus![SOFTBLOCK] The countess will be so angry![SOFTBLOCK] Better work double-time before she comes for inspection!)") ]])
fnCutsceneBlocker()

--[Movement]
--A ghost enters the room.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|FlipSwitch") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Lydie")
	ActorEvent_SetProperty("Teleport To", (19.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Ghost moves up to Natalie.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Lydie")
	ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Natalie![SOFTBLOCK] What have you been doing!?[SOFTBLOCK] This room is a disaster![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Sad] I know![SOFTBLOCK] I'm sorry![SOFTBLOCK] I just -[SOFTBLOCK] I fell asleep and had the weirdest dream...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: You can tell me all about it later.[SOFTBLOCK] You're not even dressed![SOFTBLOCK] You've got to get this room cleaned up, pronto![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Surprise] Ack, I left my uniform under my bed![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Cry] If the countess sees me out of dress she'll really let me have it![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: You're such a scatterbrain![SOFTBLOCK] Did you forget that there's a spare outfit in the guest room?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: It's in the crate over there, hurry up![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Natalie:[VOICE|Mei][EMOTION|Mei|Happy] Thanks, Lydie![SOFTBLOCK] I don't know what I'd do without you![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lydie: Just hurry up!") ]])
fnCutsceneBlocker()
