--[Debug_Startup]
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
Debug_PushPrint(false, "Debug Firing Cutscene: Defeat_Werecat\n")

--Flip the flag so this scene plays even if the player has already seen it.
VM_SetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N", 0.0)

--Otherwise, run the file normally. It may or may not change the map itself.
LM_ExecuteScript(fnResolvePath() .. "Scene_Begin.lua")
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iTimeOfDay", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iIsNight", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iIsMeiWerecat", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iSpawnNadiaEvermoonW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N", 0.0)

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")