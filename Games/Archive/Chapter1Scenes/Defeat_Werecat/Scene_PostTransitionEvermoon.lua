--[Post Transition]
--Scene that plays when the party warps back to Evermoon West.

--Variables.
local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

--Check.
local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
if(iHasSeenTrannadarFirstScene == 0.0) then
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S", "Werecat")
end

--[System]
--Black the screen out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Reposition Mei and Nadia.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (15.25 * gciSizePerTile), (46.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Teleport To", (15.25 * gciSizePerTile), (45.50 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina is present:
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (15.25 * gciSizePerTile), (47.50 * gciSizePerTile))
	DL_PopActiveObject()
end

--Reposition the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 10000.0)
	CameraEvent_SetProperty("Focus Position", (14.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Fade in.
fnCutsceneWait(45)
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--[Movement]
--Move the two up north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (39.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (39.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (13.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina is present:
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (39.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (33.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (33.50 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Reposition the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Focus Actor Name", "Mei")
DL_PopActiveObject()
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
if(iHasFlorentina == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
else
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
end

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: So then I said to the bee::[SOFTBLOCK] Nectar?[SOFTBLOCK] I hardly even knew her![SOFTBLOCK] Ha ha ha![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Kill me now...)[BLOCK][CLEAR]") ]])
if(iHasFlorentina == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I think I can make it from here, Nadia.[SOFTBLOCK] Thanks a bunch.[BLOCK][CLEAR]") ]])
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I think we can make it from here, Nadia.[SOFTBLOCK] Thanks a bunch.[BLOCK][CLEAR]") ]])
end

--If Mei met Nadia in the Werecat scene:
local iMetNadiaInWerecatScene = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N")
if(iMetNadiaInWerecatScene == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: You sure?[SOFTBLOCK] It's just over here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Positive.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Paws-itive?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] (Argh!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia:[EMOTION|Mei|Neutral] Okay then![SOFTBLOCK] It was great to meet you, Mei![SOFTBLOCK] Stop by any time![SOFTBLOCK] I bet the Cap'n and Florentina and everyone will be so stoked to meet you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah, great to meet you too, Nadia!") ]])

--Mei had met Nadia before.
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Okay![SOFTBLOCK] Great seeing you again, Mei![SOFTBLOCK] Hope everything goes well on your quest![BLOCK][CLEAR]") ]])
	if(iHasFlorentina == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: And -[SOFTBLOCK] try not to get into any more fights with the forest natives.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No promises.") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: And Florry?[SOFTBLOCK] Please take care of her, okay?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You think I wasn't?[SOFTBLOCK] I'd say she turned out rather well, actually.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Thanks![SOFTBLOCK] I think!") ]])
	end
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Movement]
--If Mei had sex with Nadia:
local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
if(iMeiHasDoneNadia == 1.0) then
	
	--[Movement]
	--Nadia walks away a bit. Slowly.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (12.75 * gciSizePerTile), (33.50 * gciSizePerTile), 0.10)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Mei walks up behind her.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (12.25 * gciSizePerTile), (33.50 * gciSizePerTile), 0.10)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (12.75 * gciSizePerTile), (33.50 * gciSizePerTile), 0.70)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face",  1, 0)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	--[Dialogue]
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])

	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ..![SOFTBLOCK] Mei, take your hand - [SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] About last night...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ...[SOFTBLOCK][SOFTBLOCK] talk to me when I'm off my shift...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Mmm...") ]])
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Nadia walks off.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (0.25 * gciSizePerTile), (34.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--If Florentina is present, fold the party. Also have a bit of a dialogue.
	if(iHasFlorentina == 1.0) then
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] What was that all about?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I hope you didn't think I wouldn't notice.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Nadia and I...[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Oh, I know exactly what happened.[SOFTBLOCK] You put your hand on her butt, after all.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You know what?[SOFTBLOCK] Good for you, kid.[SOFTBLOCK] The world's a dangerous place.[SOFTBLOCK] Needs more love in it.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] But if you break her sweet, innocent little heart...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] I'll break your legs...[SOFTBLOCK] Get it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Jeez, Florentina.[SOFTBLOCK] Where'd that come from?[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I asked if you got it.[SOFTBLOCK] Did you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Absolutely.[SOFTBLOCK] One-hundred percent.[SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Good.[SOFTBLOCK] Back to the mission, then.") ]])
		fnCutsceneBlocker()
		
		--Move Florentina onto Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (12.75 * gciSizePerTile), (33.50 * gciSizePerTile))
		DL_PopActiveObject()
		
		--Instruction to fold the party.
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
	
--If not, Nadia just walks offscreen and ends the scene.
else
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (0.25 * gciSizePerTile), (34.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--If Florentina is present, fold the party.
	if(iHasFlorentina == 1.0) then
		
		--Move Florentina onto Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (33.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Instruction to fold the party.
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end

end
fnCutsceneBlocker()

--System. Put Florentina back on the follower listing. Remember that this executes immediately and the instructions catch up to it.
if(iHasFlorentina == 1.0) then
		
	--Lua globals.
	gsFollowersTotal = 1
	gsaFollowerNames = {"Florentina"}
	giaFollowerIDs = {0}

	--Get Florentina's uniqueID. 
	EM_PushEntity("Florentina")
		local iFlorentinaID = RE_GetID()
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iFlorentinaID}
	AL_SetProperty("Follow Actor ID", iFlorentinaID)

	--Place Florentina in the combat lineup.
	AC_SetProperty("Set Party", 1, "Florentina")
end
