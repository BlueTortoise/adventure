--[Defeat By Anything, Back to Save]
--Cutscene that executes when the player is beaten by something that has no special cutscene attached. Alternately,
-- it will also fire when another defeat cutscene has already been seen, and won't play again.
--This part of the script just sends the party back to the last save point.
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

--If Florentina is present, set her to downed as well.
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
end
fnCutsceneWait(60)
fnCutsceneBlocker()

--Fade.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(60)
fnCutsceneBlocker()

fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter1Scenes/Defeat_WerecatCassandra/Scene_PostTransition.lua") ]])

--[Combat]
--Restore party to full HP in case the player doesn't want to use the save point. This saves time, and is a quality-of-life feature.
AC_SetProperty("Restore Party")