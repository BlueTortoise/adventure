--[Awaken At Camp]
--Scene plays after Mei and/or Florentina wake up the next day at the campsite. What is said depends on what exactly happened.

--Variables.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
local iSavedCassandra = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N")
local iTurnedCassandra = VM_GetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N")
local iWonFightWithCassandra = VM_GetVar("Root/Variables/Chapter1/Scenes/iWonFightWithCassandra", "N")

--Blackout.
AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)

--If Cassandra was saved, spawn her.
if(iSavedCassandra == 1.0) then
	TA_Create("Cassandra")
		TA_SetProperty("Position", 21, 19)
		TA_SetProperty("Facing", gci_Face_East)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/CassandraH/", false)
		TA_SetProperty("Activation Script", gsRoot .. "/CharacterDialogue/Cassandra/Root_Human.lua")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Reposition party.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (22.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
if(bIsFlorentinaPresent == true) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (23.25 * gciSizePerTile), (19.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
end

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Dialogue]
--In the case that Mei won the fight with a turned-Cassandra:
if(iWonFightWithCassandra == 1.0) then

	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (All right, rest's over.[SOFTBLOCK] Back to the adventure.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (If only I had been faster...)") ]])
		fnCutsceneBlocker()
	
	--Florentina is present:
	else
	
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right, rest's over.[SOFTBLOCK] Back to the adventure.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] If only we had been faster...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Don't beat yourself up, kid.[SOFTBLOCK] You tried.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] That's really all that can be asked of you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] But...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You didn't have to get involved.[SOFTBLOCK] Trying and failing is better than not trying at all.[SOFTBLOCK] You did good on that count.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Thanks for trying to cheer me up, but I still don't feel good about it...") ]])
		fnCutsceneBlocker()
		
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (18.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
	
--Setup:
elseif(bIsFlorentinaPresent == false) then
	
	--Base:
	fnStandardMajorDialogue()
	
	--If Mei turned Cassandra:
	if(iTurnedCassandra == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Ahhhhhh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Better get back to my quest, then.[SOFTBLOCK] That new fang...[SOFTBLOCK] there was something truly odd about her.[SOFTBLOCK] I didn't even get her name...") ]])
	
	--If Mei let Cassandra turn her:
	elseif(iTurnedCassandra == 2.0) then
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Ahhhh...[SOFTBLOCK] Incredible...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] This is fantastic![SOFTBLOCK] I can see, hear and smell better than ever...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I suppose that girl did me a favour, drawing me into the forest so the cats could take me.[SOFTBLOCK] I'll have to find her and return the favour someday.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] But for now, I must hunt for a way home.[SOFTBLOCK] The most challenging hunt of all!") ]])
		fnCutsceneBlocker()
		
	--If Mei saved Cassandra:
	elseif(iSavedCassandra == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Are you all right, miss?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Yes, thank you.[BLOCK][CLEAR]") ]])
		
		--If Mei is a werecat:
		if(sMeiForm == "Werecat") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: But, why did you help me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] You're strong.[SOFTBLOCK] It took many cats to take you down.[SOFTBLOCK] They don't deserve you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] One of our rules is that we do not hunt with more fangs than prey.[SOFTBLOCK] It seems that they are lax in following it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Oh, I see.[SOFTBLOCK] I have a re- [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No reward is necessary, miss..?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Cassandra of Jeffespeir.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei...[SOFTBLOCK] of Hong Kong, I guess.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Hong Kong?[SOFTBLOCK] Never heard of it.[SOFTBLOCK] Is it far away?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Further than even I know...[SOFTBLOCK] I'm looking for a way home, actually.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Oh, I see.[SOFTBLOCK] I'm afraid I can't help you there.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What were you doing alone in the forest?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I -[SOFTBLOCK] I'm not sure, actually.[SOFTBLOCK] I don't really remember how I got here.[SOFTBLOCK] I must have hurt my head.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Let me see...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, I don't see any sign of injury.[SOFTBLOCK] You can't remember?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: No, not at all.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You should definitely get someplace safe until you recover your memory.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The trading post southwest of here is safe.[SOFTBLOCK] Would you like me to take you there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Oh, I'll be fine, now.[SOFTBLOCK] The cats got the jump on me last time, but I can handle myself.[SOFTBLOCK] Really.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right.[SOFTBLOCK] Tell Nadia I sent you.[SOFTBLOCK] She'll take care of you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I need to get my things before I head out.[SOFTBLOCK] Thank you, Mei.[SOFTBLOCK] Good luck finding your way home.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Same to you, Cassandra.[SOFTBLOCK] May our paths cross again!") ]])
			fnCutsceneBlocker()
		
		--If Mei is anything else:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: But, why did you help me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I always help someone in need.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Oh, I see.[SOFTBLOCK] I have a re- [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No reward is necessary, miss..?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Cassandra of Jeffespeir.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei...[SOFTBLOCK] of Hong Kong, I guess.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Hong Kong?[SOFTBLOCK] Never heard of it.[SOFTBLOCK] Is it far away?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Further than even I know...[SOFTBLOCK] I'm looking for a way home, actually.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Oh, I see.[SOFTBLOCK] I'm afraid I can't help you there.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What were you doing alone in the forest?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I -[SOFTBLOCK] I'm not sure, actually.[SOFTBLOCK] I don't really remember how I got here.[SOFTBLOCK] I must have hurt my head.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Let me see...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, I don't see any sign of injury.[SOFTBLOCK] You can't remember?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: No, not at all.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You should definitely get someplace safe until you recover your memory.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The trading post southwest of here is safe.[SOFTBLOCK] Would you like me to take you there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Oh, I'll be fine, now.[SOFTBLOCK] The cats got the jump on me last time, but I can handle myself.[SOFTBLOCK] Really.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right.[SOFTBLOCK] Tell Nadia I sent you.[SOFTBLOCK] She'll take care of you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I need to get my things before I head out.[SOFTBLOCK] Thank you, Mei.[SOFTBLOCK] Good luck finding your way home.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Same to you, Cassandra.[SOFTBLOCK] May our paths cross again!") ]])
			fnCutsceneBlocker()
		end
	end

--If Florentina is present:
else
	
	--If Mei turned Cassandra:
	if(iTurnedCassandra == 1.0) then
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Ahhhh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] And here I was thinking you were going to try to save that girl.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, I hadn't intended to, really.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I recall you saying 'If someone calls for help, you help', right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I did help.[SOFTBLOCK] I improved her.[SOFTBLOCK] She's now a hundred times the hunter she was.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Okay, you win that one on a technicality.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I was honestly expecting you to lecture me on this.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Pah.[SOFTBLOCK] Let what comes, come, and what goes, go.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I didn't get her name, though.[SOFTBLOCK] There was definitely something odd about her.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Whatever.[SOFTBLOCK] It was fun to watch -[SOFTBLOCK] I'm not into cats, but...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] So back to business, right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Naturally![SOFTBLOCK] Let's go!") ]])
		fnCutsceneBlocker()
	
	--If Mei let Cassandra turn her:
	elseif(iTurnedCassandra == 2.0) then
		
		--Setup.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Ahhhh...[SOFTBLOCK] Incredible...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well it seems you got what you wanted out of this.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] This is fantastic![SOFTBLOCK] I can see, hear and smell better than ever...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Uh huh.[SOFTBLOCK] What about that girl that we were trying to save?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] As far as I'm concerned, we did save her.[SOFTBLOCK] She'll be so much better this way.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hey, we've still got a job to do.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Of course.[SOFTBLOCK] The hunt for a way home will be the most challenging hunt of all.[SOFTBLOCK] Will you join my pride?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Yeah, whatever.[SOFTBLOCK] Let's get going.") ]])
		fnCutsceneBlocker()
	
	--If Mei saved Cassandra:
	elseif(iSavedCassandra == 1.0) then
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Are you all right, miss?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Yes, thank you.[BLOCK][CLEAR]") ]])
		
		--If Mei is a werecat:
		if(sMeiForm == "Werecat") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: But, why did you help me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] You're strong.[SOFTBLOCK] It took many cats to take you down.[SOFTBLOCK] They don't deserve you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] One of our rules is that we do not hunt with more fangs than prey.[SOFTBLOCK] It seems that they are lax in following it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] This is a cat compliment.[SOFTBLOCK] I suggest you take it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Oh, I see.[SOFTBLOCK] I have a re- [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No reward is necessary.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Belay that.[SOFTBLOCK] I'll take -[SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No reward is necessary![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Jeez![SOFTBLOCK] Okay, okay![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] *You can pass me a reward later when she's not looking.*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Doing the right thing is its own reward, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] And money is like icing on a cake of good intentions.[SOFTBLOCK] What's your point?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *sigh*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Just ignore her, miss..?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Cassandra of Jeffespeir.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei...[SOFTBLOCK] of Hong Kong, I guess.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Hong Kong?[SOFTBLOCK] Never heard of it.[SOFTBLOCK] Is it far away?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Further than even I know...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] We're currently trying to get her back home.[SOFTBLOCK] And apparently righting wrongs while we're at it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Oh, superb![SOFTBLOCK] The world needs more people like you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What were you doing alone in the forest?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I -[SOFTBLOCK] I'm not sure, actually.[SOFTBLOCK] I don't really remember how I got here.[SOFTBLOCK] I must have hurt my head.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Let me see...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, I don't see any sign of injury.[SOFTBLOCK] You can't remember?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: No, not at all.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You should definitely get someplace safe until you recover your memory.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The trading post southwest of here is safe.[SOFTBLOCK] Would you like me to take you there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Oh, I'll be fine, now.[SOFTBLOCK] The cats got the jump on me last time, but I can handle myself.[SOFTBLOCK] Really.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right.[SOFTBLOCK] Tell Nadia I sent you.[SOFTBLOCK] She'll take care of you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] As much as this goes against my better judgement...[SOFTBLOCK] If you need something, tell Hypatia I said you could have a discount.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] She gets a discount but I don't?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] I'm a sucker for pigtails, what can I say?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I need to get my things before I head out.[SOFTBLOCK] Thank you, Mei and Florentina.[SOFTBLOCK] Good luck finding your way home.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Same to you, Cassandra.[SOFTBLOCK] May our paths cross again!") ]])
			fnCutsceneBlocker()
		
		--If Mei is anything else:
		else
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: But, why did you help me?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I always help someone in need.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] She's what is known in the psychiatric community as an 'idiot'.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Oh, I see.[SOFTBLOCK] I have a re- [SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No reward is necessary.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Belay that.[SOFTBLOCK] I'll take -[SOFTBLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No reward is necessary![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Jeez![SOFTBLOCK] Okay, okay![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] *You can pass me a reward later when she's not looking.*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Doing the right thing is its own reward, Florentina.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] And money is like icing on a cake of good intentions.[SOFTBLOCK] What's your point?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *sigh*[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Just ignore her, miss..?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Cassandra of Jeffespeir.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mei...[SOFTBLOCK] of Hong Kong, I guess.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Hong Kong?[SOFTBLOCK] Never heard of it.[SOFTBLOCK] Is it far away?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Further than even I know...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] We're currently trying to get her back home.[SOFTBLOCK] And apparently righting wrongs while we're at it.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Oh, superb![SOFTBLOCK] The world needs more people like you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What were you doing alone in the forest?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I -[SOFTBLOCK] I'm not sure, actually.[SOFTBLOCK] I don't really remember how I got here.[SOFTBLOCK] I must have hurt my head.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Let me see...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] No, I don't see any sign of injury.[SOFTBLOCK] You can't remember?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: No, not at all.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You should definitely get someplace safe until you recover your memory.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] The trading post southwest of here is safe.[SOFTBLOCK] Would you like me to take you there?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: Oh, I'll be fine, now.[SOFTBLOCK] The cats got the jump on me last time, but I can handle myself.[SOFTBLOCK] Really.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right.[SOFTBLOCK] Tell Nadia I sent you.[SOFTBLOCK] She'll take care of you.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] As much as this goes against my better judgement...[SOFTBLOCK] If you need something, tell Hypatia I said you could have a discount.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] She gets a discount but I don't?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] I'm a sucker for pigtails, what can I say?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra: I need to get my things before I head out.[SOFTBLOCK] Thank you, Mei and Florentina.[SOFTBLOCK] Good luck finding your way home.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Same to you, Cassandra.[SOFTBLOCK] May our paths cross again!") ]])
			fnCutsceneBlocker()
		end
	end
	
	--Florentina walks onto Mei and folds the party.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (18.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end