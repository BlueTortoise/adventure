--[Fail to Rescue Cassandra: Without Florentina, Mei is Non-Human]
--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Variables.
VM_SetVar("Root/Variables/Chapter1/Scenes/iWonFightWithCassandra", "N", 1.0)

--Switch back to the forest music.
fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneWait(1)

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You are defeated![SOFTBLOCK] Yield![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: Gaahhh...[SOFTBLOCK] so strong...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: We apologize...[SOFTBLOCK] spare us...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I'm not going to hurt you any more unless you make me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] *sigh*[SOFTBLOCK] But I guess it's too late for you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Fang: You are strong.[SOFTBLOCK] We will respect you.[SOFTBLOCK] Leave now.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (I guess there's nothing left for me here...)") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade to black.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(60)
fnCutsceneBlocker()
	
--Change maps. Go back to the campsite.
fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
fnCutsceneBlocker()