--[Rescue Cassandra]
--Scene that plays when Cassandra can be rescued. Can occur in one of four locations.
local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local sCassandraLocation   = VM_GetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S")
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
local iCassandraEncounters = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N")

--Once this scene starts, the event is considered over.
VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)

--[Camera Refocus]
--Focus the camera on Cassandra. Coordinates depend on the map, but it's always on the same actor.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0)
	CameraEvent_SetProperty("Focus Actor Name", "Cassandra")
DL_PopActiveObject()

--The party moves into position. This is map-specific.
local bFlorentinaUseY = false
local fMeiCoordX = 0.0
local fMeiCoordY = 0.0
if(sCassandraLocation == "EvermoonCassandraCC") then
	fMeiCoordX = 14.25
	fMeiCoordY =  7.50
elseif(sCassandraLocation == "EvermoonCassandraCNW") then
	fMeiCoordX = 10.25
	fMeiCoordY = 10.50
elseif(sCassandraLocation == "EvermoonCassandraCNE") then
	fMeiCoordX = 13.25
	fMeiCoordY = 12.50
	bFlorentinaUseY = true
elseif(sCassandraLocation == "EvermoonCassandraCE") then
	fMeiCoordX = 34.25
	fMeiCoordY = 24.50
end

--Reposition if Florentina is present:
if(bIsFlorentinaPresent == true) then
	fMeiCoordX = fMeiCoordX - 0.50
end

--Send the coordinates.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (fMeiCoordX * gciSizePerTile), (fMeiCoordY * gciSizePerTile))
DL_PopActiveObject()
if(bIsFlorentinaPresent == true) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		if(bFlorentinaUseY == false) then
			ActorEvent_SetProperty("Move To", ((fMeiCoordX+1.0) * gciSizePerTile), (fMeiCoordY * gciSizePerTile))
		else
			ActorEvent_SetProperty("Move To", ((fMeiCoordX+1.0) * gciSizePerTile), ((fMeiCoordY+1.0) * gciSizePerTile))
		end
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--[Dialogue]
--Yeah, stop them!
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])

--If Mei is alone:
if(bIsFlorentinaPresent == false) then
	
	--You're in time to save her!
	if(iCassandraEncounters < 6) then
		
		--Cassandra as a human:
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
		--If Mei is a human:
		if(sMeiForm == "Human") then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 2.0)
			
			--Dialogue:
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hey, cats![SOFTBLOCK] Leave her alone![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You look strong, human.[SOFTBLOCK] You would make a good kinfang.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] This isn't a negotiation.[SOFTBLOCK] Give me the girl![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ha ha ha![SOFTBLOCK] You are outnumbered, human![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Fangs![SOFTBLOCK] Add her to our pride!") ]])
			fnCutsceneBlocker()
			
			--Battle!
			fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsHumanSolo.lua") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Defeat Script", gsRoot .. "Chapter1Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Unretreatable", true) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneBlocker()
		
		--If Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Kinfangs![SOFTBLOCK] What goes on here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] This one will be turned when the moon glides over this glade.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: The time will come soon.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Oooh, I'm not too late.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Do you seek to join the festivities?[BLOCK]") ]])

			--Decision script is a different one.
			local sDecisionScript = "\"" .. gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Decision_AnswerWerecats.lua" .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let her go!\", " .. sDecisionScript .. ", \"LetHerGoSoloCat\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I will join you.\",  " .. sDecisionScript .. ", \"JoinThemSoloCat\") ")
			fnCutsceneBlocker()

		--If Mei is anything else:
		else
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 3.0)
			
			--Dialogue:
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hey, cats![SOFTBLOCK] Leave her alone![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You look strong.[SOFTBLOCK] You would be a good challenge.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] This isn't a negotiation.[SOFTBLOCK] Give me the girl![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ha ha ha![SOFTBLOCK] You are outnumbered![SOFTBLOCK] You have great courage![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Fangs![SOFTBLOCK] Attack!") ]])
			fnCutsceneBlocker()
			
			--Battle!
			fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsWerecatSolo.lua") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Defeat Script", gsRoot .. "Chapter1Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Unretreatable", true) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneBlocker()
		end

	--Oh no, you're too late!
	else
		
		--Cassandra as a human:
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
		--Mei has werecat form:
		local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
	
		--If Mei is a human:
		if(sMeiForm == "Human" and iHasWerecatForm == 0.0) then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 7.0)
			
			--Dialogue:
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![SOFTBLOCK] No![SOFTBLOCK] I'm too late![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You look strong, human.[SOFTBLOCK] You would make a good kinfang.[SOFTBLOCK] Come to us...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I look strong?[SOFTBLOCK] Maybe I should go to them...)[BLOCK]") ]])
			
			--Give the player a choice:
			local sDecisionScript = "\"" .. gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Decision_AnswerWerecatsFail.lua" .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Way!\", " .. sDecisionScript .. ", \"Fight\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Join Them\",  " .. sDecisionScript .. ", \"Join\") ")
			fnCutsceneBlocker()
		
		--Mei is a human and has werecat form:
		elseif(sMeiForm == "Human" and iHasWerecatForm == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ..![SOFTBLOCK] I'm too late![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (The pheremones...[SOFTBLOCK] the moon...[SOFTBLOCK] so....[SOFTBLOCK] ungggghhh...[SOFTBLOCK] horrrrnnnyyy...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Why even try to stop my kinfangs?[SOFTBLOCK] I'm -[SOFTBLOCK] I'm a fang too...[SOFTBLOCK] ungghhhh)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You have come to join us?[SOFTBLOCK] Purrr....[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Let us...[SOFTBLOCK] celebrate this joyous occasion...") ]])

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()

			--Activate scenes mode.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei strode confidently into the glade, her fellow cats already well underway in the same orgy she had enjoyed when she first joined the night hunters.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "As she approached, she allowed the latent curse that her runestone suppressed to take over again.[SOFTBLOCK] Fur, claws, and fangs replaced her human features.[SOFTBLOCK] The werecats were too busy to notice or care.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The new fang was blonde and had her eyes locked on Mei's hips as she approached.[SOFTBLOCK] Unused to her new body, the fang could hardly control the lust building inside her.[SOFTBLOCK] She practically dripped in anticipation.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei knelt in front of her and began to suck at her exposed sex.[SOFTBLOCK] The fang would have fallen in a sudden surge of pleasure, but another cat appeared behind her and groped at her chest.[SOFTBLOCK] A third soon kissed her and rubbed her gorgeous blonde fur.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The fang shuddered in orgasm, but was permitted not a second to rest.[SOFTBLOCK] The cats began to congregate around her, licking and sucking one another and her.[SOFTBLOCK] Mei continued to suck at her as another cat began working on Mei.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Her head swimming in sexual overload, Mei was barely aware of her own tongue as it slid in and out of the new fang.[SOFTBLOCK] Her delirium built with her own orgasm.[SOFTBLOCK] She continued to suck and lick.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
			fnCutsceneBlocker()
	
			--Wait a bit.
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Change maps. Go back to the campsite.
			fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
			fnCutsceneBlocker()
		
		--If Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ..![SOFTBLOCK] I'm too late![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You have come to join us?[SOFTBLOCK] Purrr....[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (The pheremones...[SOFTBLOCK] the moon...[SOFTBLOCK] so....[SOFTBLOCK] ungggghhh...[SOFTBLOCK] horrrrnnnyyy...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Let us...[SOFTBLOCK] celebrate this joyous occasion...") ]])

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()

			--Activate scenes mode.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei strode confidently into the glade, her fellow cats already well underway in the same orgy she had enjoyed when she first joined the night hunters.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The new fang was blonde and had her eyes locked on Mei's hips as she approached.[SOFTBLOCK] Unused to her new body, the fang could hardly control the lust building inside her.[SOFTBLOCK] She practically dripped in anticipation.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei knelt in front of her and began to suck at her exposed sex.[SOFTBLOCK] The fang would have fallen in a sudden surge of pleasure, but another cat appeared behind her and groped at her chest.[SOFTBLOCK] A third soon kissed her and rubbed her gorgeous blonde fur.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The fang shuddered in orgasm, but was permitted not a second to rest.[SOFTBLOCK] The cats began to congregate around her, licking and sucking one another and her.[SOFTBLOCK] Mei continued to suck at her as another cat began working on Mei.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Her head swimming in sexual overload, Mei was barely aware of her own tongue as it slid in and out of the new fang.[SOFTBLOCK] Her delirium built with her own orgasm.[SOFTBLOCK] She continued to suck and lick.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
			fnCutsceneBlocker()
	
			--Wait a bit.
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Change maps. Go back to the campsite.
			fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
			fnCutsceneBlocker()

		--If Mei is anything else:
		else
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 8.0)
			
			--Dialogue:
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![SOFTBLOCK] No![SOFTBLOCK] I'm too late![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You smell of a human, even through your disguise.[SOFTBLOCK] You look strong.[SOFTBLOCK] You would make a good kinfang.[SOFTBLOCK] Come to us...[BLOCK]") ]])
			
			--Give the player a choice:
			local sDecisionScript = "\"" .. gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Decision_AnswerWerecatsFail.lua" .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Way!\", " .. sDecisionScript .. ", \"Fight\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Join Them\",  " .. sDecisionScript .. ", \"Join\") ")
			fnCutsceneBlocker()
		end

	end

--If Florentina is here to help:
else
	
	--You're in time to save her!
	if(iCassandraEncounters < 6) then
		
		--Cassandra as a human:
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
		--If Mei is a human:
		if(sMeiForm == "Human") then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 4.0)
			
			--Dialogue:
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hey, cats![SOFTBLOCK] Leave her alone![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You look strong, human.[SOFTBLOCK] You would make a good kinfang.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] This isn't a negotiation.[SOFTBLOCK] Give me the girl![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ha ha ha![SOFTBLOCK] You are outnumbered, human![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Fangs![SOFTBLOCK] Add her to our pride!BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Good![SOFTBLOCK] I was hoping you wouldn't manage to talk them out of it!") ]])
			fnCutsceneBlocker()
			
			--Battle!
			fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsHumanTeam.lua") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Defeat Script", gsRoot .. "Chapter1Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Unretreatable", true) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneBlocker()
		
		--If Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 5.0)
			
			--Dialogue.
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Kinfangs![SOFTBLOCK] What goes on here?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] This one will be turned when the moon glides over this glade.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: The time will come soon.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Oooh, I'm not too late.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Do you seek to join the festivities?[BLOCK]") ]])

			--Decision script is a different one.
			local sDecisionScript = "\"" .. gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Decision_AnswerWerecats.lua" .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let her go!\", " .. sDecisionScript .. ", \"LetHerGoTeamCat\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I will join you.\",  " .. sDecisionScript .. ", \"JoinThemTeamCat\") ")
			fnCutsceneBlocker()

		--If Mei is anything else:
		else
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 6.0)
			
			--Dialogue:
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hey, cats![SOFTBLOCK] Leave her alone![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You look strong.[SOFTBLOCK] You would be a good challenge.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] This isn't a negotiation.[SOFTBLOCK] Give me the girl![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Ha ha ha![SOFTBLOCK] You are outnumbered![SOFTBLOCK] You have great courage![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Fangs![SOFTBLOCK] Attack![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Good![SOFTBLOCK] I was worried there'd be a peaceful resolution!") ]])
			fnCutsceneBlocker()
			
			--Battle!
			fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsWerecatSolo.lua") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Defeat Script", gsRoot .. "Chapter1Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
			fnCutsceneInstruction([[ AC_SetProperty("Unretreatable", true) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter1/Werecat.lua", 0) ]])
			fnCutsceneBlocker()
		end
	
	--You're too late!
	else
		
		--Cassandra as a human:
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
		--Mei has werecat form:
		local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
	
		--If Mei is a human:
		if(sMeiForm == "Human" and iHasWerecatForm == 0.0) then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 9.0)
			
			--Dialogue:
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![SOFTBLOCK] No![SOFTBLOCK] We're too late![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You look strong, human.[SOFTBLOCK] You would make a good kinfang.[SOFTBLOCK] Come to us...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] No chance, cats.[SOFTBLOCK] Mei, weapons up![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I look strong?[SOFTBLOCK] Maybe I should go to them...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Mei?[SOFTBLOCK] Don't tell me you're actually considering it![BLOCK]") ]])
			
			--Give the player a choice:
			local sDecisionScript = "\"" .. gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Decision_AnswerWerecatsFail.lua" .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Way!\", " .. sDecisionScript .. ", \"Fight\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Join Them\",  " .. sDecisionScript .. ", \"Join\") ")
			fnCutsceneBlocker()
		
		--Mei is a human and has werecat form:
		elseif(sMeiForm == "Human" and iHasWerecatForm == 1.0) then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ..![SOFTBLOCK] We're too late![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (The pheremones...[SOFTBLOCK] the moon...[SOFTBLOCK] so....[SOFTBLOCK] ungggghhh...[SOFTBLOCK] horrrrnnnyyy...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Why even try to stop my kinfangs?[SOFTBLOCK] I'm -[SOFTBLOCK] I'm a fang too...[SOFTBLOCK] ungghhhh)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You have come to join us?[SOFTBLOCK] Purrr....[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Let us...[SOFTBLOCK] celebrate this joyous occasion...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Mei?[SOFTBLOCK] Mei![SOFTBLOCK] What are you doing?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Joining my fangs...[SOFTBLOCK] you should come too...[SOFTBLOCK] I'll lick you...[SOFTBLOCK] unngghhhh...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Oh -[SOFTBLOCK] uh, you have fun.[SOFTBLOCK] Cats aren't my thing.[SOFTBLOCK] I'll just -[SOFTBLOCK] watch.[SOFTBLOCK] Yeah.") ]])
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()

			--Activate scenes mode.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei strode confidently into the glade, her fellow cats already well underway in the same orgy she had enjoyed when she first joined the night hunters.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "As she approached, she allowed the latent curse that her runestone suppressed to take over again.[SOFTBLOCK] Fur, claws, and fangs replaced her human features.[SOFTBLOCK] The werecats were too busy to notice or care.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The new fang was blonde and had her eyes locked on Mei's hips as she approached.[SOFTBLOCK] Unused to her new body, the fang could hardly control the lust building inside her.[SOFTBLOCK] She practically dripped in anticipation.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei knelt in front of her and began to suck at her exposed sex.[SOFTBLOCK] The fang would have fallen in a sudden surge of pleasure, but another cat appeared behind her and groped at her chest.[SOFTBLOCK] A third soon kissed her and rubbed her gorgeous blonde fur.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The fang shuddered in orgasm, but was permitted not a second to rest.[SOFTBLOCK] The cats began to congregate around her, licking and sucking one another and her.[SOFTBLOCK] Mei continued to suck at her as another cat began working on Mei.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Her head swimming in sexual overload, Mei was barely aware of her own tongue as it slid in and out of the new fang.[SOFTBLOCK] Her delirium built with her own orgasm.[SOFTBLOCK] She continued to suck and lick.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
			fnCutsceneBlocker()
	
			--Wait a bit.
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Change maps. Go back to the campsite.
			fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
			fnCutsceneBlocker()
		
		--If Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
		
			--Dialogue
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ..![SOFTBLOCK] We're too late![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You have come to join us?[SOFTBLOCK] Purrr....[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (The pheremones...[SOFTBLOCK] the moon...[SOFTBLOCK] so....[SOFTBLOCK] ungggghhh...[SOFTBLOCK] horrrrnnnyyy...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Let us...[SOFTBLOCK] celebrate this joyous occasion...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Mei?[SOFTBLOCK] Mei![SOFTBLOCK] What are you doing?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Joining my fangs...[SOFTBLOCK] you should come too...[SOFTBLOCK] I'll lick you...[SOFTBLOCK] unngghhhh...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Oh -[SOFTBLOCK] uh, you have fun.[SOFTBLOCK] Cats aren't my thing.[SOFTBLOCK] I'll just -[SOFTBLOCK] watch.[SOFTBLOCK] Yeah.") ]])
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()

			--Activate scenes mode.
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei strode confidently into the glade, her fellow cats already well underway in the same orgy she had enjoyed when she first joined the night hunters.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The new fang was blonde and had her eyes locked on Mei's hips as she approached.[SOFTBLOCK] Unused to her new body, the fang could hardly control the lust building inside her.[SOFTBLOCK] She practically dripped in anticipation.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei knelt in front of her and began to suck at her exposed sex.[SOFTBLOCK] The fang would have fallen in a sudden surge of pleasure, but another cat appeared behind her and groped at her chest.[SOFTBLOCK] A third soon kissed her and rubbed her gorgeous blonde fur.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The fang shuddered in orgasm, but was permitted not a second to rest.[SOFTBLOCK] The cats began to congregate around her, licking and sucking one another and her.[SOFTBLOCK] Mei continued to suck at her as another cat began working on Mei.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Her head swimming in sexual overload, Mei was barely aware of her own tongue as it slid in and out of the new fang.[SOFTBLOCK] Her delirium built with her own orgasm.[SOFTBLOCK] She continued to suck and lick.") ]])
			fnCutsceneBlocker()
			
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Hours passed as the orgy raged.[SOFTBLOCK] The moon continued its arc across the sky.[SOFTBLOCK] Soon, it would set, and pass behind the trees.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "With the loss of their patron moon, the cats felt themselves tire.[SOFTBLOCK] Orgasm after orgasm had taken its toll.[SOFTBLOCK] The cats soon fell asleep in a great pile.[SOFTBLOCK] Mei cuddled against the new fang and slipped into oblivion.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "When she woke, most of the cats had gone.[SOFTBLOCK] The new fang was nowhere in sight.[SOFTBLOCK] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[SOFTBLOCK] She could taste the new fang there, and she tasted strong.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei left the clearing and made her way back to the camp she had visited earlier.[SOFTBLOCK] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[SOFTBLOCK] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
			fnCutsceneBlocker()
	
			--Wait a bit.
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Change maps. Go back to the campsite.
			fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
			fnCutsceneBlocker()

		--If Mei is anything else:
		else
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 10.0)
			
			--Dialogue:
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![SOFTBLOCK] No![SOFTBLOCK] We're too late![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purrr...[SOFTBLOCK] You smell of a human, even through your disguise.[SOFTBLOCK] You look strong.[SOFTBLOCK] You would make a good kinfang.[SOFTBLOCK] Come to us...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] No chance, cats.[SOFTBLOCK] Mei, weapons up![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I look strong?[SOFTBLOCK] Maybe I should go to them...)[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Mei, don't tell me you're actually considering it...[BLOCK]") ]])
			
			--Give the player a choice:
			local sDecisionScript = "\"" .. gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Decision_AnswerWerecatsFail.lua" .. "\""
			fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"No Way!\", " .. sDecisionScript .. ", \"Fight\") ")
			fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Join Them\",  " .. sDecisionScript .. ", \"Join\") ")
			fnCutsceneBlocker()
		end
	
	end
end