--[Combat Victory]
--Scene plays if Mei defeats the werecats, solo, as a werecat.
VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N", 1.0)

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Switch back to the forest music.
fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneWait(1)
	
--[Dialogue]
--Setup.
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Hah![SOFTBLOCK] Is that all you got?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Y-[SOFTBLOCK]you...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You want to be next, kitty?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: ...[SOFTBLOCK] Take her, strong one...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] All right![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Better clear out before they change their minds!)") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Fade to black.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--Setup.
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Hey, are you still awake?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: Cure -[SOFTBLOCK] the cure is in my pack...[SOFTBLOCK] Hurry...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[SOFTBLOCK] You mean this thing?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lady: *gulp*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Looks like she passed out.[SOFTBLOCK] I better get her someplace safe...)") ]])
fnCutsceneBlocker()

fnCutsceneWait(120)
fnCutsceneBlocker()
	
--Change maps. Go back to the campsite.
fnCutsceneInstruction([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter1Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
fnCutsceneBlocker()