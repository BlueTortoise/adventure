--[Florentina's Goodbye]
--This is an interstitial script, not a cutscene script. It executes in the middle of an existing dialogue.

--Variables.
local iTakenPieJob                = VM_GetVar ("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
local iSavedClaudia               = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
local iSavedBeehive               = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N")
local iMeiTriedFruit              = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiTriedFruit", "N")
local iCompletedTrapDungeon       = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
local iCompletedQuantirMansion    = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")
local iHasSeenFlorentinaTwentyWin = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaTwentyWin", "N")

--Forms:
local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
local iHasBeeForm     = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm",     "N")
local iHasSlimeForm   = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm",   "N")
local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
local iHasGhostForm   = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm",   "N")

--Special:
local iMeiLovesAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")

--Compute Florentina's general opinion of Mei:
local iFlorentinaOpinion = 0.0
if(iTakenPieJob == 2.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iSavedClaudia == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iSavedBeehive == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iMeiTriedFruit == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iCompletedTrapDungeon == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iCompletedQuantirMansion == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iHasSeenFlorentinaTwentyWin == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end

--Compute Florentina's general adventure state:
local iFlorentinaAdventure = 0.0
if(iSavedBeehive == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
if(iCompletedTrapDungeon == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
if(iCompletedQuantirMansion == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
if(iHasSeenFlorentinaTwentyWin == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end

--Florentina's general opinion of Mei is low, so they haven't known each other long:
if(iFlorentinaOpinion < 2) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Hey, Florentina?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] This is the part where you say goodbye, right?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I know we didn't spend that much time together, but...[SOFTBLOCK] I'll miss you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Heh.[SOFTBLOCK] Yeah, it was a bit of fun.[SOFTBLOCK] Now it's back to the grind of running a store.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] If you're ever in the dimensional neighbourhood and you want to go on an adventure, look me up, yeah?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You mean that?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's nice to stretch my legs.[SOFTBLOCK] Plus, it keeps me away from the usual crop of bounty hunters and general sorholes who infest the Trading Post.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well.[SOFTBLOCK] Okay![SOFTBLOCK] I'll think of you when I'm home![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Likewise.[SOFTBLOCK] Go get 'em, kid.") ]])

--Florentina's opinion of Mei is middling:
elseif(iFlorentinaOpinion < 4) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Florentina...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] I'm gonna miss you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Ha ha![SOFTBLOCK] Crying![SOFTBLOCK] I love it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I'll miss you too, kid.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] If there's a way to visit I'll try to![SOFTBLOCK] Thanks so much for all your help![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hey, uh, you're probably not going to need the platina when you're on Earth...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] ...[SOFTBLOCK][EMOTION|Mei|Laugh] Ha ha ha ha![SOFTBLOCK] Sure, take it![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] But you owe me![SOFTBLOCK] I'll come back so you can work off your debt![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Of course.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Go get em, kid.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right![SOFTBLOCK] Here I go!") ]])

--High opinion:
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Florentina...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] I'm gonna miss you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Okay, Mei?[SOFTBLOCK] I know you're going to be jumping dimensions or whatever but...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK]*Hug*[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] If you tell anyone I did that I will hunt you down.[SOFTBLOCK] Even if you tell people on Earth, I [SOFTBLOCK]*will*[SOFTBLOCK] find you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] F-[SOFTBLOCK]Florentina![SOFTBLOCK] You're the best![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You've got a home to go to, right?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I just have to let everyone know I'm all right.[SOFTBLOCK] If I can come back, I will![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Whatever for?[BLOCK][CLEAR]") ]])
	
	if(iTakenPieJob == 2.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I didn't have a lot of friends back home...[SOFTBLOCK] But I do here![SOFTBLOCK] With you![SOFTBLOCK] And Breanne![SOFTBLOCK] And Nadia, and everyone else![BLOCK][CLEAR]") ]])
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I didn't have a lot of friends back home...[SOFTBLOCK] But I do here![SOFTBLOCK] With you![BLOCK][CLEAR]") ]])
	end
		
	if(iMeiLovesAdina == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] And, I want to be with my Mistress...[SOFTBLOCK] and I want to help with the salt flats...[BLOCK][CLEAR]") ]])
	end
	if(iHasAlrauneForm == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Oh, and I want to be with my leaf-sisters, too...[BLOCK][CLEAR]") ]])
	end
	if(iHasBeeForm == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] And the drones will miss me, too.[SOFTBLOCK] I can't forget them...[BLOCK][CLEAR]") ]])
	end
	if(iHasWerecatForm == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] And I want to stalk the night with my pride...[BLOCK][CLEAR]") ]])
	end
	if(iHasGhostForm == 1.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] And...[SOFTBLOCK] we must tell the story of Natalie and all those who died...[BLOCK][CLEAR]") ]])
	end
	
	if(iFlorentinaAdventure >= 2) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Don't forget there's still a cult that we can visit violence upon.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Oh yeah.[SOFTBLOCK] Them.[SOFTBLOCK] Will you keep an eye on them while I'm away?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Just one, naturally.[SOFTBLOCK] It's in good hands.[BLOCK][CLEAR]") ]])
	end
	
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I guess it hasn't been so bad, has it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I -[SOFTBLOCK] I get the feeling I'll be back.[SOFTBLOCK] It's the strange feeling I get, like I always knew.[SOFTBLOCK] I've been getting it a lot since I've been here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I'm not really good at goodbyes, so get going before I throw you through this mirror.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] All right![SOFTBLOCK] Here I go![SOFTBLOCK] Goodbye, Florentina! [SOFTBLOCK]Bye for now!") ]])
end