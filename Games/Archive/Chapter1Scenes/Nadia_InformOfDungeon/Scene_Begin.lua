--[Nadia Informs of the Dungeon]
--Nadia will inform the party about the Trap Dungeon.

--[Variables]
--Flag this scene not to fire twice.
VM_SetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N", 1.0)

--Get values.
local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")

--[Talking]
--Dialogue.
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Oh![SOFTBLOCK] There you are![SOFTBLOCK] I've been looking everywhere for you![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] But we haven't even done anything awful yet![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Don't be a big silly, this is serious![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Mei, the Alraunes told me to give you a special message.[BLOCK][CLEAR]") ]])

--Mei does not have Alraune form:
if(iHasAlrauneForm == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] What would they want with me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Probably going to beg you to join them.[SOFTBLOCK] Typical.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Oh no, it's not like that![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Rochea said that you really ticked off a bunch of nasty cultists who had set up in the Dimensional Trap ruins.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Yeah?[SOFTBLOCK] Well that's what they get for kidnapping me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] ...[SOFTBLOCK] Who's Rochea, again?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] She's the elder of the local Alraune covenant.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: She says the cultists are up to something big and she wants your help.[SOFTBLOCK] She said you'd know them best.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Huh, so she's been spying on us, has she?[SOFTBLOCK] We're not interested.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Oh, we're interested.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Mei?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] If the Alraunes are going to take the fight to them, I can put aside our differences.[SOFTBLOCK] Where do we meet them?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: She wouldn't say.[SOFTBLOCK] But it'll be someplace near the ruins...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] If I recall correctly, there's a basement access room on the southwest edge of the building.[SOFTBLOCK] It's been a while, we might have to search.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Though I would like to point out that this is a bad idea.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] You don't want to rough up the cultists?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh, very much so.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] It's Rochea and her ilk that I don't trust...") ]])

--Mei has Alraune form:
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Is something wrong?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I bet somebody stepped on a sunflower.[SOFTBLOCK] Oohhhh noooo.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Oh come on, Florry![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Rochea said that you really ticked off a bunch of nasty cultists who had set up in the Dimensional Trap ruins.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Yeah?[SOFTBLOCK] Well that's what they get for kidnapping me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: She says they're up to something big and she wants your help.[SOFTBLOCK] She said you'd know them best.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Not interested.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Oh, we're interested.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Mei?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Those rotten cultists kidnapped me.[SOFTBLOCK] If my leaf-sisters want to take the fight to them, I'll be right there with them.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Where does Rochea need us?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: She wasn't specific.[SOFTBLOCK] She said they had broken through into their basement, but I don't know how you'd get there from the surface.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I think there's a secondary access in the southwest corner of the building.[SOFTBLOCK] We should look there.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Though I would like to point out that this is a bad idea.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] You don't want to rough up the cultists?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Oh, very much so.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] It's Rochea and her ilk that I don't trust...") ]])

end

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()