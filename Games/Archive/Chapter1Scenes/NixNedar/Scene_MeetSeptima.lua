--[Meeting Septima]
--Cutscene where Mei meets Septima and the Rilmani.

--[Topics]
--Unlock these topics if they weren't already.
--None yet!

--[Music]
AL_SetProperty("Music", "Null")

--[Movement]
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait.
fnCutsceneWait(30)
fnCutsceneBlocker()

--RilmaniG turns around.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniG")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (Uh oh, I don't actually know how to [SOFTBLOCK]*speak*[SOFTBLOCK] Rilmani![SOFTBLOCK] This just shows what the words are, not how to say them!)") ]])
fnCutsceneBlocker()

--[Movement]
--Rilmani move towards Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Mei looks left.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, -1)
DL_PopActiveObject()

--Rilmani on the right move towards Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Mei looks right.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, -1)
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Oh jeez, I hope I didn't tick them off...)") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Septima][SOFTBLOCK] And the cloud billowed forth, swallowing them in time.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Septima][SOFTBLOCK] But the darkness waned, for as the cloud grew, so too did the star.[SOFTBLOCK] Six-pointed, it shone brighter than all the others.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Septima][SOFTBLOCK] With its majesty, the third age died.[SOFTBLOCK] Nothing would be written on its grave, save the inscription::[SOFTBLOCK] 'Voidwalker'.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Movement]
--The Rilmani look north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniA")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniG")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Septima moves south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Septima")
	ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(70)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani: The bindings came loose, and certainty died.[SOFTBLOCK] What was written was erased, and what was erased was forgotten.[SOFTBLOCK] So cometh the ender, and the forerunner.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Excuse me?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani: Valor.[SOFTBLOCK] Courage.[SOFTBLOCK] Bravery.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani: ...[SOFTBLOCK] Mei.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] ..![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] You know my name?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani: We have known of you before your one-thousandth ancestor was born.[SOFTBLOCK] Your coming is the sole strand of unbreakable destiny.[SOFTBLOCK] We, the Rilmani, are at your service.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Well at least it's a warm welcome...[SOFTBLOCK] of sorts.[SOFTBLOCK] You guys don't smile much, do you?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani: ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Rilmani: The voidwalker tells a joke![SOFTBLOCK] Confidence surges![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOFTBLOCK]*The assembled Rilmani murmur assent, but aren't smiling or laughing...*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (Well they [SOFTBLOCK]*sound*[SOFTBLOCK] happy, so that's good...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] So, uh,[SOFTBLOCK] where am I?[SOFTBLOCK] Who are you?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: You are in Nix Nedar, the space in no space.[SOFTBLOCK] This is the home of the Rilmani.[SOFTBLOCK] I am Septima, oldest among the unborn.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: Doubtless you will have many questions.[SOFTBLOCK] Please, come to my home when you are ready.[SOFTBLOCK] We have much to discuss.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Septima: And, please introduce yourself to the unborn here.[SOFTBLOCK] It will do much to lift their spirits!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Septima moves north, and teleports off the map.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Septima")
	ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (7.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Septima")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()

fnCutsceneWait(30)
fnCutsceneBlocker()

Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniA")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniG")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()

fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] This just keeps getting weirder and weirder...") ]])
fnCutsceneBlocker()

--[Movement]
--Rilmani B:
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()

--Rilmani E:
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()

--Rilmani F:
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Move To", (30.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()

--Rilmani G:
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniG")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneBlocker()

--[Music]
fnCutsceneInstruction([[ AL_SetProperty("Music", "NixNedar") ]])
fnCutsceneBlocker()
