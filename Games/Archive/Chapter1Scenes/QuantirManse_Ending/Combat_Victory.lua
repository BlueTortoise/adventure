--[Combat Victory]
--The party won! Good job!

--[Setup]
--Flag.
VM_SetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N", 1.0)
AudioManager_PlayMusic("Null")

--Reset the topic case. Florentina has more to say.
WD_SetProperty("Clear Topic Read", "Warden")

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Un-redden the screen.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 240, gci_Fade_Under_GUI, false, 0.5, 0, 0, 1, 0, 0, 0, 0) ]])

--[Dialogue]
--Setup.
fnStandardMajorDialogue()

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's...[SOFTBLOCK] it's dead.[SOFTBLOCK] It's done...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Countess?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I guess she's moved on, now.[SOFTBLOCK] I hope they both found peace...") ]])
fnCutsceneBlocker()