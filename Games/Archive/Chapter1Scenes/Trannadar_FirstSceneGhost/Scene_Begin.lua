--[Trannadar Intro Scene: Ghost]
--If Mei is a ghost the first time she enters Trannadar, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end


--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
	Cutscene_CreateEvent("Move Mei North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Move Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
else
	Cutscene_CreateEvent("Move Mei West", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

--Wait a bit.
fnCutsceneInstruction([[ fnPartyStopMovement() ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
	Cutscene_CreateEvent("Face Nadia South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Face Nadia South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()

--Face them east.
else
	Cutscene_CreateEvent("Face Nadia East", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe East", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()

end
fnCutsceneWait(5)
fnCutsceneBlocker()

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Blythe", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: ...[SOFTBLOCK] look, that's not what matters.[SOFTBLOCK] What I really want to know is how you got the cow on the roof.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: I think it'd be more important to know how I got it [SOFTBLOCK]*off*[SOFTBLOCK] the roof![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Hey...[SOFTBLOCK] B-[SOFTBLOCK]b-[SOFTBLOCK]b-[SOFTBLOCK]boss?[SOFTBLOCK] Are you seeing what I'm seeing?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, hello.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: It's a g-[SOFTBLOCK]g-[SOFTBLOCK]g-[SOFTBLOCK]g-[SOFTBLOCK]ghooooost![SOFTBLOCK] EEEEEEEE!!!![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] P-[SOFTBLOCK]please don't be afraid![SOFTBLOCK] I'm not trying to scare you![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: Nadia, get a hold of yourself.[SOFTBLOCK] She seems lucid enough.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Huh?[SOFTBLOCK] I'm not scared![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Miss Ghost Lady, you have the CUTEST outfit I've ever seen![SOFTBLOCK] Oh my gosh it looks so good on you![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thank.[SOFTBLOCK] You?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: Ugh, Nadia...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: My apologies, ma'am.[SOFTBLOCK] She's new at this.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: I am Captain Darius Blythe, head of security here.[SOFTBLOCK] This is Nadia, our newest recruit.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Hi![SOFTBLOCK] Who does your hair?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: I don't believe we've ever had a guest of your particular species here, and as you seem to be in control of your faculties, I will allow you access to the post.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Ooh![SOFTBLOCK] But -[SOFTBLOCK] no violence, or we'll kick you out.[SOFTBLOCK] Right, boss?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: That is correct.[SOFTBLOCK] No matter your urges, you must be civil here.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I wasn't looking for a fight.[SOFTBLOCK] Actually, I need to find a way back to Earth.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Earth?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You've heard of it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Nope![SOFTBLOCK] But if anyone would know, it'd be Florentina, at the general goods shop![SOFTBLOCK] You should go talk to her![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, thank you, Nadia.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: No really, who does your hair?[SOFTBLOCK] How do you get it transparent like that?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *sigh*") ]])
fnCutsceneBlocker()

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
