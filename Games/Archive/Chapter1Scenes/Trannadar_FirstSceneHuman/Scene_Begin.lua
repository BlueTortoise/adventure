--[Trannadar Intro Scene: Human]
--If Mei is human the first time she enters Trannadar, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
	Cutscene_CreateEvent("Move Mei North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Move Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
else
	Cutscene_CreateEvent("Move Mei West", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

--Wait a bit.
fnCutsceneInstruction([[ fnPartyStopMovement() ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
	Cutscene_CreateEvent("Face Nadia South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Face Nadia South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe South", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()

--Face them east.
else
	Cutscene_CreateEvent("Face Nadia East", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Blythe East", "Actor")
		ActorEvent_SetProperty("Subject Name", "Blythe")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()

end
fnCutsceneWait(5)
fnCutsceneBlocker()

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Blythe", "Neutral") ]])

--Actual talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: ...[SOFTBLOCK] and the plural of anecdote is not evidence![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: What's a plural?[SOFTBLOCK] And what's an anecdote?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Oh, wait, nevermind.[SOFTBLOCK] I think I see an anecdote right now.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Er, hello?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Greetings, anecdote![SOFTBLOCK] Welcome to Trannadar Trading Post![SOFTBLOCK] I'm Nadia, and I forgot what I was supposed to say after that![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: How'd I do, Cap'n?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: Well, she didn't burst into tears, so you're improving.[BLOCK][CLEAR]") ]])

--Variables.
local iHasMetFriendly = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasMetFriendly", "N")
local iHasReadNadiaSign = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N")

--If Mei hasn't met anyone friendly yet, this happens:
if(iHasMetFriendly == 0.0) then
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetFriendly", "N", 1.0)
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] What a relief![SOFTBLOCK] You're the first thing I've seen today that hasn't attacked me![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: You're all by yourself?[SOFTBLOCK] Yeah, the forest is crawling with monsters this time of year.[SOFTBLOCK] You should always buddy up if you go exploring.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: An anecdote like you, in an outfit like that, all alone?[SOFTBLOCK] That's just asking for trouble.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: Oh for crying out loud, Nadia![SOFTBLOCK] That's not what that word means![BLOCK][CLEAR]") ]])

--Mei has met at least one other friendly and read at least one of Nadia's signs.
elseif(iHasReadNadiaSign == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] You're Nadia?[SOFTBLOCK] You're the one who made those signs?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Gee, I've never had a fan before![SOFTBLOCK] Do you want an autograph?[SOFTBLOCK] Get it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: Oh no...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Get what?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Do you want me to *sign* something?[SOFTBLOCK] Ha ha ha ha![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] That is in keeping with what I've seen so far.[SOFTBLOCK] Is she always like this?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: Yes, all the time.[BLOCK][CLEAR]") ]])

--Mei has met at least one other friendly but somehow missed Nadia's signs.
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Well it's very nice to meet you.[SOFTBLOCK] I've been having a pretty rough day.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Of course it'd be coarse out there.[SOFTBLOCK] These woods are very dangerous, especially when you're alone.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: If you're going out exploring, you should always bring a friend.[SOFTBLOCK] Anecdotes need to stick together, you know.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: That word.[SOFTBLOCK] You keep using it...[BLOCK][CLEAR]") ]])
end

--Common.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Man: *ahem*[SOFTBLOCK] I am captain Darius Blythe, head of the trading post's guard.[SOFTBLOCK] If you need anything, please come see me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] What I really need is for someone to help me find a way home.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Are you lost?[SOFTBLOCK] Where are you from?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] If I said 'Hong Kong', would that ring a bell?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: ...[SOFTBLOCK] Never heard of it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: China?[SOFTBLOCK] Asia?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Sorry.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I was afraid you'd say that.[SOFTBLOCK] I get the sinking feeling I'm not on Earth anymore...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Sinking?[SOFTBLOCK] Earth?[SOFTBLOCK] Like, quicksand?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] Oh no...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: H-hey! Don't cry![SOFTBLOCK] Was it something I said?[SOFTBLOCK] My jokes aren't that bad, are they?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] Eh, no, no, it wasn't the joke.[SOFTBLOCK][E|Offended] Okay, it wasn't just the joke.[SOFTBLOCK] I'll be fine, really.[SOFTBLOCK] There's gotta be a reasonable explanation for how I got here.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: That's the spirit![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Blythe: Hm.[SOFTBLOCK] I don't know where Earth might be, but if anyone does, it'd be Florentina at the provisions shop.[SOFTBLOCK] She'd be happy to help you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thanks.[SOFTBLOCK] I feel better already.") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()
WD_SetProperty("Unlock Topic", "TradingPost", 1)
