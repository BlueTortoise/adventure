--[Trannadar Third Scene]
--If Nadia first met Mei in a form, and then sees her in a different form, then Nadia deduces that Mei can shapeshift.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N", 1.0)

--[Variables]
local sMeiForm         = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iMetClaudia      = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
local iExaminedMirror  = VM_GetVar("Root/Variables/Chapter1/Scenes/iExaminedMirror", "N")
local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")

--[Initial Movements]
--Mei walks towards the counter.
Cutscene_CreateEvent("Move Mei To Counter", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (33.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Face Mei to the west.
Cutscene_CreateEvent("Face Mei West", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(2)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneInstruction([[ fnPartyStopMovement() ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Hypatia", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: Hi there![SOFTBLOCK] Welcome to Trannadar General Goods.[SOFTBLOCK] My name is Hypatia, may I take your order?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, I'm sorry.[SOFTBLOCK] I thought this was Florentina's shop.[SOFTBLOCK] I was told to speak to her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: I'm her assistant.[SOFTBLOCK] She's out at the moment, but I can help you.[SOFTBLOCK] What do you need?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Have you ever heard of Earth?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: Uh, we don't really stock fertilizer...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] No, the planet.[SOFTBLOCK] Earth.[SOFTBLOCK] Cradle of humanity.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: Err...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: Sorry, but I have no idea what that is.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do you think Florentina might?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: I'd ask her, but she's, uh, out.[SOFTBLOCK] Yep.[SOFTBLOCK] Not here.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Really?[SOFTBLOCK] Are you sure she's not in the back, staring at the leftmost shelf?") ]])
fnCutsceneBlocker()

--Hypatia looks around with confusion.
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Hypatia North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Hypatia East", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneWait(45)
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Hypatia North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneWait(60)
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Hypatia East", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Hypatia goes north to talk to Florentina]
--Move to the door.
Cutscene_CreateEvent("Move Hypatia To Door", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (19.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Timing.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Open the door, play a sound.
fnCutsceneInstruction([[AL_SetProperty("Open Door", "Florentina's Rear Door")]])
fnCutsceneInstruction([[AudioManager_PlaySound("World|OpenDoor")]])
fnCutsceneBlocker()

--Mei looks at Hypatia as she moves.
Cutscene_CreateEvent("Face Mei Northwest", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, -1)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Hypatia moves into the back room to look at Florentina.
Cutscene_CreateEvent("Move Hypatia To Florentina", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Face Hypatia North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Brief pause.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Florentina turns around, and presumably says something to Hypatia. No dialogue.
Cutscene_CreateEvent("Face Florentina Southwest", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", -1, 1)
DL_PopActiveObject()
fnCutsceneWait(120)
fnCutsceneBlocker()

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("FlorentinasTheme") ]])

--Brief pause, Florentina looks in Mei's direction, as does Hypatia.
Cutscene_CreateEvent("Face Florentina South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("Face Hypatia South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--They resume speaking to one another. Florentina changes facing.
Cutscene_CreateEvent("Face Florentina Southwest", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", -1, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("Face Hypatia North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneWait(120)
fnCutsceneBlocker()

--Florentina comes out to the shop floor. Takes a few moves.
Cutscene_CreateEvent("Move Florentina To Front A", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Florentina To Front B", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Mei West", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
Cutscene_CreateEvent("Move Hypatia To Front", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (19.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Florentina To Front C", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Florentina and Hypatia face Mei.
Cutscene_CreateEvent("Face Florentina East", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
Cutscene_CreateEvent("Face Hypatia East", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Hypatia", "Neutral") ]])

--Slime form.
if(sMeiForm == "Slime") then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well you don't look like a debt collector.[SOFTBLOCK] You're not slimy enough![SOFTBLOCK][EMOTION|Florentina|Happy] Hah![BLOCK][CLEAR]") ]])

--Alraune.
elseif(sMeiForm == "Alraune") then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You sure don't *look* like a debt collector.[BLOCK][CLEAR]") ]])
	
--Bee.
elseif(sMeiForm == "Bee") then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You sure don't *look* like a debt collector.[BLOCK][CLEAR]") ]])
	
--Ghost.
elseif(sMeiForm == "Ghost") then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You sure don't *look* like a debt collector.[SOFTBLOCK] You look like a maid.[SOFTBLOCK] An undead maid.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] It is not a permanent arrangement.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Maybe a bounty hunter, then?[BLOCK][CLEAR]") ]])

--Default: Human.
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You don't look like a debt collector, not with that outfit.[BLOCK][CLEAR]") ]])
end
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm no such thing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yet you know who I am and are looking for Earth, are you?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] You've heard of it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] No.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] Crud.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] But.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] That stone.[SOFTBLOCK] Where did you get it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You mean this thing?[SOFTBLOCK] Well, I...[SOFTBLOCK] I woke up in a dingy basement not far from here, and I was holding it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Interesting.[SOFTBLOCK] Very interesting.[SOFTBLOCK] Hypatia?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: Yes?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Do you recall that monk and her little fan club that came through here a few weeks ago?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: Um, I think so.[SOFTBLOCK] They bought a lot of medical cream.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] What was the name of their leader?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: ...[SOFTBLOCK]...[SOFTBLOCK] I want to say Claudia, but I don't want to sound dumb if I'm way off.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] And wasn't she snooping around for artifacts at the old mansion before she got sent packing by thugs?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: I'm going to say maybe.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Hypatia, are you thinking what I'm thinking?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: I think so, boss.[SOFTBLOCK] But if the electric charge of a particle is smaller when measured at a distance, doesn't that imply that zero-point energy isn't non-zero, and that the fine-structure constant isn't a constant?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] What?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: What?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] Hypatia...[SOFTBLOCK] You never cease to amaze...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Ahem.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I might have to step out for a bit.[SOFTBLOCK] For real, this time.[SOFTBLOCK] You're going to be in charge for a while.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Hypatia: All right![SOFTBLOCK] I won't let you down![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Just don't forget to lock up after closing.[BLOCK][CLEAR]") ]])


--Hasn't met Claudia, hasn't examined the mirror, doesn't know Rilmani:
if(iMetClaudia == 0.0 and iExaminedMirror == 0.0 and iMeiKnowsRilmani == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] What's so interesting about this stone?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Did it occur to you that the stone might be valuable when you appeared so far away that nobody even knows where you're from, clutching it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Well, no. I figured the creepy cult abducted me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I've been doing a lot of running and fighting since then, so I haven't really had time to think on it.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] The stone has a rune on it.[SOFTBLOCK] I don't know what it is, but I've definitely seen it somewhere before.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] And I know someone who will pay top platina for a chance to see it.[SOFTBLOCK] So, I'm going to help you find that someone.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] How altruistic of you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] ...[SOFTBLOCK] I'll be enjoying a nice finder's fee, of course.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] So glad I could help your bottom line.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You're getting what you want, I suspect.[SOFTBLOCK] That's a very old symbol, probably something from the second age.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Wherever Earth is, Claudia will know.[SOFTBLOCK] I'd even bet it's connected to that rune, too.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Besides, I need to find someplace to be where certain elements cannot locate me.[SOFTBLOCK] Hanging out with a drifter is as good a place as any.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I'm not a drifter![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Maybe not where you're from.[SOFTBLOCK] Welcome to Pandemonium, by the way.") ]])

--Mei has examined the exit mirror:
elseif(iExaminedMirror == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Well, I actually kind of found a mirror that acts as a portal...[SOFTBLOCK] and there's Rilmani writing on it...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] A Rilmani artifact?[SOFTBLOCK] Are you serious?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Take me to it![SOFTBLOCK] I can probably sell that for -[SOFTBLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Okay, okay![SOFTBLOCK] Thanks for you help!") ]])

--Mei knows Rilmani:
elseif(iMeiKnowsRilmani == 1.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I found a journal Claudia left, it seems to indicate that this runestone is a Rilmani artifact.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] A Rilmani artifact?[SOFTBLOCK] Are you serious?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Do you know how I can use it to get back to Earth?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] No, but I bet if we can find another artifact it will help.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] More importantly, then I can sell it and make -[SOFTBLOCK] if I give you ten percent, you'll never have to work again![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, how...[SOFTBLOCK] generous?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] The Rilmani are supposed to be a legend, but if you say that your runestone is one of theirs...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] We should go check out the big mansion near the lake.[SOFTBLOCK] It's called the Dimensional Trap.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Some old stories say it's a Rilmani building or something.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But that's where I came from...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] That's just adding more evidence.[SOFTBLOCK] Let's go!") ]])

--Has met Claudia:
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Claudia, the monk?[SOFTBLOCK] I ran into her in the dungeon![SOFTBLOCK] Sort of![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Well, that's good.[SOFTBLOCK] So you'll know where she is, then.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Actually, she said this stone had something to do with the Rilmani.[SOFTBLOCK] Do you know how to translate it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Claudia would know more about that than I would.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] She said she had a translation guide but lost it in the mansion way north of here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Seems you really get around.[SOFTBLOCK] Have you looked for it?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I couldn't find it.[SOFTBLOCK] Will you help me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Sure -[SOFTBLOCK] on the condition that I get to keep a share of the loot.[SOFTBLOCK] That mansion has been unassailable by treasure hunters for quite a while...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I just want to find a way home.[SOFTBLOCK] You can keep whatever we find.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It also helps that I be out of the shop.[SOFTBLOCK] There are some people I'd rather not deal with who would like a word with me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Wandering in the forest with a drifter is far preferable.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not a drifter![SOFTBLOCK] I'm lost![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Welcome to Pandemonium, where getting lost is half the fun.") ]])

end

fnCutsceneBlocker()

--[Movement Sequence]
--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Florentina walks around the counter.
Cutscene_CreateEvent("Face Hypatia South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("Move Florentina South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (21.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Florentina East", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (33.25 * gciSizePerTile), (21.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--They look at each other.
Cutscene_CreateEvent("Face Florentina North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--[Dialogue Sequence]
--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Hasn't met Claudia:
if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0 and iExaminedMirror == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Okay, listen up.[SOFTBLOCK] We need to head to Outland Farm, which is a ways up north.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] We're looking for Sister Claudia.[SOFTBLOCK] I don't know if she's still there, but that's where she said she was headed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Will she know how to get back to Earth?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Maybe.[SOFTBLOCK] She was doing some kind of study of monsters in the region.[SOFTBLOCK] She's been all over the world, so we could do a lot worse than talking to her.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] All right![SOFTBLOCK] Finally, some good news![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Don't get too excited.[SOFTBLOCK] I'm not making any guarantees here.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're on point.[SOFTBLOCK] Lead the way.") ]])

--Has met Claudia:
else
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Since you seem to know your way around, I trust you'll be fine leading the way.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're on point.[SOFTBLOCK] Let's go.") ]])
end

fnCutsceneBlocker()

--[Florentina Joins the Party!]
--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Florentina walks onto Mei.
Cutscene_CreateEvent("Move Florentina to Mei", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (33.25 * gciSizePerTile), (20.49 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

--[Indicator]
--Inform the player that Florentina has joined the party.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina has joined the party!") ]])
fnCutsceneBlocker()

--Music changes back.
fnCutsceneInstruction([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneWait(30)
fnCutsceneBlocker()

--Fold the party positions up.
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

--[System]
--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
gsFollowersTotal = 1
gsaFollowerNames = {"Florentina"}
giaFollowerIDs = {0}

--Get Florentina's uniqueID. 
EM_PushEntity("Florentina")
	local iFlorentinaID = RE_GetID()
DL_PopActiveObject()

--Store it and tell her to follow.
giaFollowerIDs = {iFlorentinaID}
AL_SetProperty("Follow Actor ID", iFlorentinaID)

--Place Florentina in the combat lineup.
AC_SetProperty("Set Party", 1, "Florentina")

--Florentina's equipment
if(gbHasFlorentinasEquipment == false) then
	gbHasFlorentinasEquipment = true
	LM_ExecuteScript(gsItemListing, "Hunting Knife")
	LM_ExecuteScript(gsItemListing, "Flowery Tunic")
	LM_ExecuteScript(gsItemListing, "Florentina's Pipe")
	AC_PushPartyMember("Florentina")
		ACE_SetProperty("Equip", "Weapon", "Hunting Knife")
		ACE_SetProperty("Equip", "Armor", "Flowery Tunic")
		ACE_SetProperty("Equip", "Accessory A", "Florentina's Pipe")
	DL_PopActiveObject()
end

--Get how much XP Mei has, and give the same amount to Florentina, plus or minus 25.
AC_PushPartyMember("Mei")
	local iXPTotal = ACE_GetProperty("XP Total")
DL_PopActiveObject()

--Scatter the XP. Can't go below zero.
iXPTotal = iXPTotal + LM_GetRandomNumber(-25, 25)
if(iXPTotal < 1) then iXPTotal = 0 end

--Give that XP to Florentina.
AC_PushPartyMember("Florentina")
	ACE_SetProperty("EXP", iXPTotal)
DL_PopActiveObject()

--Unlock dialogue topics.
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Claudia", 1)
WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
WD_SetProperty("Unlock Topic", "Pandemonium", 1)
WD_SetProperty("Unlock Topic", "Eyepatch", 1)
WD_SetProperty("Unlock Topic", "Quantir", 1)
