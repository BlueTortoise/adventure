--[Trannadar Second Scene: Bee]
--If Nadia first met Mei as a human, and then Mei entered as a bee...
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei walks towards Nadia. This is based on where she spawned.
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(bIsPlayerAtBottom) then
	
	--Move Mei.
	Cutscene_CreateEvent("Move Mei North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (39.50 * gciSizePerTile))
		DL_PopActiveObject()
	end

	--Common.
	fnCutsceneBlocker()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Move Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (24.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()
else
	--Move Mei.
	Cutscene_CreateEvent("Move Mei West", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (44.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	
	--Common.
	fnCutsceneBlocker()
end

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Wait a bit.
fnCutsceneInstruction([[ fnPartyStopMovement() ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Dialogue setup.
fnStandardMajorDialogue()
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])

--Talking.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Mei?[SOFTBLOCK] Mei, is that you?[SOFTBLOCK] Speak to me![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oh, hey Nadia.[SOFTBLOCK] What's up?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: ... it worked?[SOFTBLOCK] I didn't think bee girls could talk.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Well of course we can talk.[SOFTBLOCK] We just don't usually, because we talk to each other using our antennae.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Oh wow, cool![SOFTBLOCK] Still, I've never seen someone come back after getting dragged off to a bee hive.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I guess I'm different.[SOFTBLOCK] I don't want to be, but I am.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: I'll let everyone know you're on the level, but don't try anything tricky.[SOFTBLOCK] The boss is gonna be real nervous to have you around.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] We're not up to anything, we promise.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: We?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Oops, I meant I.[SOFTBLOCK] Sometimes I forget that I'm still Mei.[SOFTBLOCK] It's - complicated.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nadia: Mei-bee it's simpler than you think![SOFTBLOCK] Ha ha![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] *groan*") ]])
fnCutsceneBlocker()

--Change the music.
fnCutsceneInstruction([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Move Florentina onto Mei, fold the party.
if(iHasSeenTrannadarFlorentinaScene == 1.0) then
	
	--Bottom.
	if(bIsPlayerAtBottom) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
		DL_PopActiveObject()

    elseif(bIsPlayerAtTop) then
        Cutscene_CreateEvent("Move Mei", "Actor")
            ActorEvent_SetProperty("Subject Name", "Florentina")
            ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
	
	--Right.
	else
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
