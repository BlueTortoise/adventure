--[Relive Handler]
--Special script, used for (most) of the instances of the player choosing to relive a transformation scene.
-- The scene requires the name of the transformation to be passed to it. The C++ code will pass the DISPLAY NAME
-- of the scene in. We would therefore expect "Transform: Alraune", for example.

--[Argument Check]
--Verify.
if(fnArgCheck(1) == false) then return end

--Set.
local sSceneName = LM_GetScriptArgument(0)

--[Storage]
--Store Mei's current form.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
VM_SetVar("Root/Variables/Chapter1/Scenes/sOriginalForm", "S", sMeiForm)

--Indicate we are reliving a scene. This causes them to end earlier or at different times.
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N", 1.0)

--[Common]
--Wait a bit.
fnCutsceneWait(10)
fnCutsceneBlocker()

--Common.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Handle Florentina case:
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
if(bIsFlorentinaPresent == true) then
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
end

--[Execution]
--Alraune case:
if(sSceneName == "Alraune") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Alraune")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Ah, becoming a leaf-sister...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Such a wonderful memory...)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Chapter1Scenes/Defeat_Alraune/Debug_Startup.lua") ]])
	
--Bee case:
elseif(sSceneName == "Bee") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Bee")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Hmmm, when I first became a drone of the hive...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (The taste of the honey, the buzzing blotting out my senses...[SOFTBLOCK] the first time I got my feelers...)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Chapter1Scenes/Defeat_Bee/Debug_Startup.lua") ]])
	
--Ghost case:
elseif(sSceneName == "Ghost") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Ghost")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (Natalie...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (I feel like I've inherited more than just her memories.[SOFTBLOCK] Is she really a part of me?)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (It's okay Natalie, your memory will be honored...)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Chapter1Scenes/Defeat_Ghost/Debug_Startup.lua") ]])

--Slime case:
elseif(sSceneName == "Slime") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Slime")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (There really is nothing more cathartic than just letting go of everything and squishing together in a big heap...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Wait that sounded really stupid...[SOFTBLOCK][EMOTION|Mei|Smirk] which is okay.[SOFTBLOCK] Slimes like me don't really have brains!)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Chapter1Scenes/Defeat_Slime/Debug_Startup.lua") ]])

--Slime case:
elseif(sSceneName == "Werecat") then
	
	--We need to store this value. It can't be overwritten in our heads!
	local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
	VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadiaStore", "N", iMeiHasDoneNadia)
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Werecat")
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (The night...[SOFTBLOCK] my kinfangs...[SOFTBLOCK] the smell of their fur...[SOFTBLOCK] the thrill of the hunt...)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I'm better, stronger, faster, than I've ever been -[SOFTBLOCK] and I love it!)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Chapter1Scenes/Defeat_Werecat/Debug_Startup.lua") ]])

end
