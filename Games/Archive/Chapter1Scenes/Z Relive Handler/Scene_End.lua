--[Scene Ending]
--After reliving a scene, return to the save point and re-add Florentina to the party if she got removed and play any dialogue that occurs.
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N", 0.0)

--First, check if Florentina should be in the party but was removed. If so, re-add her.
local bIsFlorentinaHere = false
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(iHasSeenTrannadarFlorentinaScene == 1.0) then

	--Flag.
	bIsFlorentinaHere = true

	--If Florentina is currently in the party, we don't need to do anything.
	local bIsFlorentinaPresent = false
	for i = 1, gsFollowersTotal, 1 do
		if(gsaFollowerNames[i] == "Florentina") then
			bIsFlorentinaPresent = true
		end
	end
	
	--Re-add code:
	if(bIsFlorentinaPresent == false) then
	
		--Create if she doesn't exist.
		if(EM_Exists("Florentina") == false) then
			fnSpecialCharacter("Florentina", "Alraune", -10, -10, gci_Face_South, false, nil)
		end

		--Lua globals.
		gsFollowersTotal = 1
		gsaFollowerNames = {"Florentina"}
		giaFollowerIDs = {0}

		--Get Florentina's uniqueID. 
		EM_PushEntity("Florentina")
			local iFlorentinaID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs = {iFlorentinaID}
		AL_SetProperty("Follow Actor ID", iFlorentinaID)

		--Place Florentina in the combat lineup.
		AC_SetProperty("Set Party", 1, "Florentina")

		--Party Folding
		AL_SetProperty("Fold Party")
	end
end

--Return Mei to her original form:
local sOriginalForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sOriginalForm", "S")
if(sOriginalForm == "Human") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")
elseif(sOriginalForm == "Alraune") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua")
elseif(sOriginalForm == "Bee") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua")
elseif(sOriginalForm == "Ghost") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Ghost.lua")
elseif(sOriginalForm == "Slime") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua")
elseif(sOriginalForm == "Werecat") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua")
end

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
if(bIsFlorentinaHere == true) then
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
end

--Now let's have some dialogue. This is based on what scene we saw.
local sLastRelivedScene = VM_GetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S")
if(sLastRelivedScene == "Alraune") then
	
	--If Mei is alone:
	if(bIsFlorentinaHere == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Such a wonderful feeling, being joined...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (But, I have to keep going.[SOFTBLOCK] Got to find a way home!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (Then I can join my very own leaf-sisters on Earth![SOFTBLOCK] Lord knows the environment could use some tending...)") ]])
	
	--If Florentina is here:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mmmmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Were you spacing out or something?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I was just remembering what it was like to be joined.[SOFTBLOCK] Was it pleasant for you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] It did feel pretty great...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It was a very long time ago, Mei.[SOFTBLOCK] In fact, I really only clearly remember the feeling as the waters soaked into me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] As far as these things go, I guess Alraune is one of the better ones.[SOFTBLOCK] Life could certainly be a lot worse.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yeah...") ]])
	end
	
--Bee scene.
elseif(sLastRelivedScene == "Bee") then
	
	--If Mei is alone:
	if(bIsFlorentinaHere == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (It's such a tragedy that I might have to leave my drone sisters behind and go back to Earth.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (But if there's a way I can return, I know I will -[SOFTBLOCK][EMOTION|Mei|Happy] and I'll have all sorts of rare nectars for them!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] (Not to mention I could turn the humans there into new drones...[SOFTBLOCK] but I'm getting ahead of myself!)") ]])
	
	--If Florentina is here:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Mmmmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You've got a distant look about you.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I was just thinking about how I joined the hive.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] It's really a shame you can't.[SOFTBLOCK] You don't know what you're missing![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, I'd pass.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] You wouldn't be such a grumpus with all the other drones encouraging you and helping you and thinking for you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] You'd just slip away and mindlessly follow orders...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] *snap*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Wake up, kid.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Don't snap your fingers at me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Why do you want to mindlessly follow orders all of the sudden?[SOFTBLOCK] Don't you like being,[SOFTBLOCK] well,[SOFTBLOCK] *you*?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] \"She who makes a beast of herself, relieves the pain of being a woman.\"[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] ...[SOFTBLOCK] Earth quote?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] It's...[SOFTBLOCK] apt.[SOFTBLOCK] There must have been a lot of wise Earthers.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] \"It is the province of knowledge to speak, and it is the privilege of wisdom to listen.\"[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] I spent a lot of my spare time reading things -[SOFTBLOCK] and of course, watching cat videos.[SOFTBLOCK] If you hear me say something smart, I probably copied it from someone wiser than I.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] That's okay.[SOFTBLOCK] There's nothing new under the sun.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] Hmm...[SOFTBLOCK] I think I've heard that on Earth, too...") ]])
	end
	
--Ghost scene.
elseif(sLastRelivedScene == "Ghost") then
	
	--If Mei is alone:
	if(bIsFlorentinaHere == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] (Lydie, Natalie, Laura, Joel...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Cry] (They died so young...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (But I'll be live enough for all of us -[SOFTBLOCK] I mean, them!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] (Now, to find a way home!)") ]])
	
	--If Florentina is here:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] *sigh*[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] ...[SOFTBLOCK] Do you want to talk about it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] The spirit of Natalie...[SOFTBLOCK] she lives on through me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] I'm not sure if I've merged with her or maybe just got her memories...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I'm not an expert on possession.[SOFTBLOCK] Sorry.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Really, I'm not that sad about it.[SOFTBLOCK] I think she is, and that affects me.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] But if we live our lives to the fullest, she won't have to be sad anymore.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Well that's - [SOFTBLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] She also died a virgin.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] O -[SOFTBLOCK] kay?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I don't know why I felt the need to point that out, but I did.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Blush] Maybe we should go get Natalie laid?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Oh I am [SOFTBLOCK]*way*[SOFTBLOCK] ahead of you on that one...") ]])
	end
	
--Slime scene.
elseif(sLastRelivedScene == "Slime") then
	
	--If Mei is alone:
	if(bIsFlorentinaHere == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Just thinking about being so squishy...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Ack![SOFTBLOCK] Stay focused and find a way home![SOFTBLOCK] I'll be shlicking all day if I don't focus!)") ]])
	
	--If Florentina is here:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Oooohh...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Mei, I swear that if you start touching yourself right in front of me...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Ungghh...[SOFTBLOCK] Sorry...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I was just remembering when I first became a slime.[SOFTBLOCK] It was -[SOFTBLOCK] quite an experience.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Did it make your hormones go out of control?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] They are exactly where I want them to be![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I guess I've just been so horny since I ended up on Pandemonium...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] It's totally natural.[SOFTBLOCK] Just, please try to control yourself when we're out here.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I don't need you as an uncoordinated heap when we could get jumped at any minute.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (I'll just wait until later, and then...[SOFTBLOCK] I'll squish...)") ]])
	end
	
--Werecat scene.
elseif(sLastRelivedScene == "Werecat") then
	
	--Restore this variable.
	local iMeiHasDoneNadiaStore = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadiaStore", "N")
	VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N", iMeiHasDoneNadiaStore)
	
	--If Mei is alone:
	if(bIsFlorentinaHere == false) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] (Okay world, better hang on to your hat![SOFTBLOCK] You're about to meet the original furry night hunter!)") ]])
	
	--If Florentina is here:
	else
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Mmmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Thinking of something nice?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Yeah...[SOFTBLOCK] when I first felt the moon's grace...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You're kind of an odd werecat.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] I truly stand for the principles we werecats are supposed to follow.[SOFTBLOCK] Honorable hunts, peerless agility, flawless poise...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] You just know those instinctively?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] It's like sort of a contract with the moon.[SOFTBLOCK] It makes us its perfect night hunters, in exchange we uphold its values.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] And what about balls of string?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Smirk] ...[SOFTBLOCK] Don't tempt me...") ]])
	end
	
end
fnCutsceneBlocker()
	