--[Party]
--When Chapter 5 starts up, initializes the player's party. Add starting equipment.
--[=[

--If Christine/55  already exist, this has already been run.
if(AC_GetProperty("Does Character Exist", "Christine") == true) then
	return
end

--[ ========================================= Christine ========================================= ]
--Variables.
local sAbilityPath = gsRoot .. "Abilities/Christine/000 Initializer.lua"

--If not loading, put Christine's starting equipment in the inventory.
if(gbIsLoadingSequence == false) then
	LM_ExecuteScript(gsItemListing, "Schoolmaster's Suit")
	LM_ExecuteScript(gsItemListing, "Violet Runestone")
	
	--When bypassing the intro, give Chris the Tazer.
	if(gbBypassIntro == true) then
		LM_ExecuteScript(gsItemListing, "Tazer")
	end
end

--Create Christine and register her. Give her the basic equipment set. Note that while she starts the
-- chapter as Chris, that won't be lasting long. All cutscenes will refer to him as Christine until then.
AC_CreatePartyMember("Christine")

	--Christine can transform.
	ACE_SetProperty("Can Transform", true)
	
	--Path Christine uses to resolve skillbook abilities.
	ACE_SetProperty("Skillbook Resolve Path", gsRoot .. "Skillbooks/Christine/Z Assemble Volume List.lua")
	
	--Equip Christine's default items.
	if(gbIsLoadingSequence == false) then
		ACE_SetProperty("Equip", "Armor", "Schoolmaster's Suit")
		ACE_SetProperty("Equip", "Item A", "Violet Runestone")
		
		--When bypassing the intro, equip the Tazer.
		if(gbBypassIntro == true) then
			ACE_SetProperty("Equip", "Weapon", "Tazer")
		end
	end
DL_PopActiveObject()

--"Transform" Christine into a human. This sets her default stats.
LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua")

--Set Christine's HP to 100%, give her the default abilities.
AC_PushPartyMember("Christine")

	--HP.
	ACE_SetProperty("Health Percent", 1.0)

	--Pass the ability path.
	ACE_SetProperty("Ability Path", sAbilityPath)

	--Christine's standard ability set.
	LM_ExecuteScript(sAbilityPath, "Attack")
	LM_ExecuteScript(sAbilityPath, "Take Point") --Invisible until later.
	LM_ExecuteScript(sAbilityPath, "Line Formation") --Invisible until later.
	LM_ExecuteScript(sAbilityPath, "Block")
	LM_ExecuteScript(sAbilityPath, "Special")
	LM_ExecuteScript(sAbilityPath, "Techniques")
	
	--Special Moves.
	LM_ExecuteScript(sAbilityPath, "Batter")
	
	--Techniques.
	LM_ExecuteScript(sAbilityPath, "Shock")
	LM_ExecuteScript(sAbilityPath, "Puncture")
DL_PopActiveObject()

--[ ============================================ 55 ============================================= ]
--Create 55. She is not in the party at game start.
sAbilityPath = gsRoot .. "Abilities/55/000 Initializer.lua"
AC_CreatePartyMember("55")

	--55 cannot transform and is always a doll.
	ACE_SetProperty("Can Transform", false)
	
	--Path 55 uses to resolve skillbook abilities.
	ACE_SetProperty("Skillbook Resolve Path", gsRoot .. "Skillbooks/55/Z Assemble Volume List.lua")
	
	--55's equipment will be added if/when the player recruits her.
DL_PopActiveObject()

--"Transform" 55 into a doll. This sets her default stats. She doesn't transform any more after that.
LM_ExecuteScript(gsRoot .. "FormHandlers/55/Form_Doll.lua")

--Set 55's HP to 100%, give her the default abilities.
AC_PushPartyMember("55")

	--HP.
	ACE_SetProperty("Health Percent", 1.0)

	--Pass the ability path.
	ACE_SetProperty("Ability Path", sAbilityPath)

	--55's standard ability set.
	LM_ExecuteScript(sAbilityPath, "Attack")
	LM_ExecuteScript(sAbilityPath, "Special")
	LM_ExecuteScript(sAbilityPath, "Algorithms")
	LM_ExecuteScript(sAbilityPath, "Combat Routines")
	
	--Algorithms.
	LM_ExecuteScript(sAbilityPath, "Assault Algorithm")
	LM_ExecuteScript(sAbilityPath, "Support Algorithm")
	LM_ExecuteScript(sAbilityPath, "Restore Algorithm")
	
	--Special Moves.
	LM_ExecuteScript(sAbilityPath, "High-Power Shot")
	LM_ExecuteScript(sAbilityPath, "Hyper-Repair")
	
	--Combat Routines.
	LM_ExecuteScript(sAbilityPath, "Rubber Shot")
	LM_ExecuteScript(sAbilityPath, "Wide Lens Shot")
	LM_ExecuteScript(sAbilityPath, "Disruptor Blast")
	LM_ExecuteScript(sAbilityPath, "Tactical Shift")
	LM_ExecuteScript(sAbilityPath, "Spotless Protocol")
	LM_ExecuteScript(sAbilityPath, "Repair")
DL_PopActiveObject()


--[ ========================================== JX-101 =========================================== ]
--Create JX-101. She is not in the party at game start.
sAbilityPath = gsRoot .. "Abilities/JX101/000 Initializer.lua"
AC_CreatePartyMember("JX-101")

	--JX-101 cannot transform and is always a steam droid.
	ACE_SetProperty("Can Transform", false)
	
	--Path JX-101 uses to resolve skillbook abilities.
	ACE_SetProperty("Skillbook Resolve Path", gsRoot .. "Skillbooks/JX101/Z Assemble Volume List.lua")
	
	--JX-101's equipment will be added if/when the player recruits her.
DL_PopActiveObject()

--"Transform" JX-101 into a steam droid. This sets her default stats. She doesn't transform any more after that.
LM_ExecuteScript(gsRoot .. "FormHandlers/JX101/Form_SteamDroid.lua")

--Set JX-101's HP to 100%, give her the default abilities.
AC_PushPartyMember("JX-101")

	--HP.
	ACE_SetProperty("Health Percent", 1.0)

	--Pass the ability path.
	ACE_SetProperty("Ability Path", sAbilityPath)

	--JX-101's standard ability set.
	LM_ExecuteScript(sAbilityPath, "Attack")
	LM_ExecuteScript(sAbilityPath, "Special")
	LM_ExecuteScript(sAbilityPath, "Techniques")
	
	--Specials.
	LM_ExecuteScript(sAbilityPath, "Thunderbolt Slug")
	LM_ExecuteScript(sAbilityPath, "Pinning Fire")
	
	--Techniques
	LM_ExecuteScript(sAbilityPath, "Battle Cry")
	LM_ExecuteScript(sAbilityPath, "Sniper Shot")
	LM_ExecuteScript(sAbilityPath, "Field Repair")
DL_PopActiveObject()

--[ ========================================== SX-399 =========================================== ]
--Create SX-399. She joins after the halfway point.
sAbilityPath = gsRoot .. "Abilities/SX399/000 Initializer.lua"
AC_CreatePartyMember("SX-399")

	--Always a steam lord!
	ACE_SetProperty("Can Transform", false)
	
	--Path SX-399 uses for skillbooks.
	ACE_SetProperty("Skillbook Resolve Path", gsRoot .. "Skillbooks/SX399/Z Assemble Volume List.lua")
	
	--SX-399's equipment gets added when she gets recruited.
DL_PopActiveObject()

--"Transform" SX-399 into a steam droid. This sets her default stats. She doesn't transform any more after that.
LM_ExecuteScript(gsRoot .. "FormHandlers/SX399/Form_SteamDroid.lua")

--Set SX-399's HP to 100%, give her the default abilities.
AC_PushPartyMember("SX-399")

	--HP.
	ACE_SetProperty("Health Percent", 1.0)

	--Pass the ability path.
	ACE_SetProperty("Ability Path", sAbilityPath)

	--JX-101's standard ability set.
	LM_ExecuteScript(sAbilityPath, "Attack")
	LM_ExecuteScript(sAbilityPath, "Special")
	LM_ExecuteScript(sAbilityPath, "Techniques")
	
	--Specials.
	LM_ExecuteScript(sAbilityPath, "Thunderbolt Slug")
	
	--Techniques
	LM_ExecuteScript(sAbilityPath, "Shoot 'em Good")
	LM_ExecuteScript(sAbilityPath, "Melta Blast")
	LM_ExecuteScript(sAbilityPath, "Patch 'em Up")
DL_PopActiveObject()
]=]
