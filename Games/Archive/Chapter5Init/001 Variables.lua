--[ ====================================== Script Variables ===================================== ]
--Setup.
DL_AddPath("Root/Variables/Chapter5/")

--[System]
--Special scene variables.
DL_AddPath("Root/Variables/Chapter5/Scenes/")
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasNoMap", "N", 0.0)

--Default music.
AC_SetProperty("Default Music", "BattleThemeChristine", 0.000)

--Party leader's voice starts as Chris' male voice. It changes later.
WD_SetProperty("Set Leader Voice", "ChrisMaleVoice")

--[Map Setup]
LM_ExecuteScript(gsRoot .. "Maps/Z Map Lookups/Chapter 5 Lookups.lua")

--[System Vars Boot Case]
--When loading, only re-boots the system variables and leaves the scenario variables alone.
if(gbOnlyBootSystemVars == true) then return end

--[Rest Reset]
--These variables reset back to 0 whenever the player rests.
DL_AddPath("Root/Variables/Chapter5/RestReset/")
VM_SetVar("Root/Variables/Chapter5/RestReset/iTestVariable", "N", 0.0)

--[Campfire Listing]
--These store which campfires the player has accessed. The player can warp between them at their discretion.
DL_AddPath("Root/Variables/Chapter5/Campfires/")
VM_SetVar("Root/Variables/Chapter5/Campfires/iRegulusCryoC", "N", 1.0) --Always available.
VM_SetVar("Root/Variables/Chapter5/Campfires/iRegulusExteriorEA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iRegulusExteriorSB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iRegulusExteriorWA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iRegulusLRTA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iRegulusLRTF", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iRegulusLRTID", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iBlackSiteA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iSprocketA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iTelluriumMinesB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iTelluriumMinesE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iRegulusCity15C", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Campfires/iRegulusCityC", "N", 0.0)

--Default save point.
AL_SetProperty("Last Save Point", "RegulusCryoC")

--[Special Cutscene Variables]
--This indicates that a cutscene is being "relived". The player may relive cutscenes from the save menu.
VM_SetVar("Root/Variables/Chapter5/Scenes/sOriginalForm", "S", "Human")
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "N", "None")
giFollowersTotalRelive = 0
gsaFollowerNamesRelive = {}
giaFollowerIDsRelive = {}

--[ ===================================== Costume Variables ===================================== ]
--Christine, Unlocked
DL_AddPath("Root/Variables/Costumes/Christine/")
VM_SetVar("Root/Variables/Costumes/Christine/iGolemGala", "N", 0.0)
VM_SetVar("Root/Variables/Costumes/Christine/iDollSweater", "N", 0.0)
VM_SetVar("Root/Variables/Costumes/Christine/iDollNude", "N", 0.0)
VM_SetVar("Root/Variables/Costumes/Christine/iHumanNude", "N", 0.0)
VM_SetVar("Root/Variables/Costumes/Christine/iRaijuNude", "N", 1.0)

--Christine, Wearing
VM_SetVar("Root/Variables/Global/Christine/iWearingGolemDress", "N", 0.0)
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeHuman", "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeRaiju", "S", "Normal")
VM_SetVar("Root/Variables/Global/Christine/iIsRaijuClothed", "N", 0.0)

--55
DL_AddPath("Root/Variables/Costumes/55/")

--SX-399
DL_AddPath("Root/Variables/Costumes/SX399/")

--Sophie, Unlocked
DL_AddPath("Root/Variables/Costumes/Sophie/")
VM_SetVar("Root/Variables/Costumes/Sophie/iGolemGala", "N", 0.0)

--Sophie, Wearing
VM_SetVar("Root/Variables/Global/Sophie/iWearingGolemDress", "N", 0.0)
VM_SetVar("Root/Variables/Costumes/Sophie/sCostumeGolem", "S", "Normal")

--56
DL_AddPath("Root/Variables/Costumes/56/")
VM_SetVar("Root/Variables/Costumes/56/iDollNude", "N", 0.0)

--[ ==================================== Main Quest Variables =================================== ]
DL_AddPath("Root/Variables/Chapter5/Scenes/")

--Cryogenics
VM_SetVar("Root/Variables/Chapter5/Scenes/iPartyKOCount", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasSeenScrubNotice", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoBlueCard", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i55ReturnCryoScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedLowerDDoor", "N", 0.0)

--Regulus City
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWelcomeToRegulus", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAbout55", "N", 0.0)

--LRT Facility
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawLRTOpening", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawOfficeConversation", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawShortcutConversation", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBattleCutscene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPrisonEntryScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawDiaryScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToWarden", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPrisonDiscussion", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N", 0.0) --1, defeated first pass. 2 defeated second pass. 3, lost twice, moved on.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawArmoryList", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iScavengedArmoryGuns", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGotImplosionGrenades", "N", 0.0)

--LRT Facility Lights
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawLightCutscene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTHBLightSectionA", "N", 0.0)

--Shopping Center Sector 119
VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBasementScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iShowedGetOffTram", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeToGolemStoreGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetFriendGolem", "N", 0.0)

--Gala Sequence
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeGalaLord96", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iReviewedObjectives", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal0State", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal2State", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal3State", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal4State", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetContact", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGotBug", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaInstalledBug", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGotPrimeEarrings", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iReturnedPrimeEarrings", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGotBullied", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iPostBully", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBumpedIntoLatexDrone", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGotStationary", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSentComplaint", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIdentifiedSecurityLead", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iDancedWithSophie", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPostDance", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowToGetPermission", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPermission", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawExitDialogue", "N", 0.0)

--Basement after Gala
VM_SetVar("Root/Variables/Chapter5/Scenes/iStarted55Sequence", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWhereIsEveryone", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iPhysicsEntry", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawElevatorScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSetChargeA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSetChargeB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSetChargeC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iStartInvasion", "N", 0.0)

--Biolabs Introduction
VM_SetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMusicComments", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawConvertedScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowsTubeHoles", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawGammaAirlockSequence", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i2856YelledAboutWestward", "N", 0.0)

--Biolabs Datacore
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAnnouncement", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iDatacoreSawScaredGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iDarkmattersBad", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedDatacoreElevator", "N", 0.0)

--Sheep
VM_SetVar("Root/Variables/Chapter5/Scenes/iSheepCooldown", "N", 0.0)

--Biolabs Raiju Ranch
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawHydrophobe", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGammaPowerRestored", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBigRant", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawTransitEncounter", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawStargazing", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGotBreachingTools", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBreachedDoor", "N", 0.0)

--Epsilon Labs
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawTrapDialogue", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet20", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetVivify", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsFinale", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackTempWakeup", "N", 0.0)

--Biolabs World States
VM_SetVar("Root/Variables/Chapter5/Scenes/iDatacoreNeedsTeleport", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBoatShortcut", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawItsCold", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i55ComplainedBoat", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieLeftInBiolabs", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsFirstWatchMovie", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockDialogue", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsMetNarissa", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsGotMilked", "N", 0.0)

--Narissa's Shop
DL_AddPath("Root/Variables/Chapter5/NarissaVendor/")
VM_SetVar("Root/Variables/Chapter5/NarissaVendor/sShopInfo", "S", "Recoil Dampener|Tungsten Suppressor|Enchanted Ring|")

--[ ==================================== Flashback and Finale =================================== ]
--Flashback
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N", 0.0)

--Optional
VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWrongDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N", 0.0)

--Corruption
VM_SetVar("Root/Variables/Chapter5/Scenes/iStaticTimer", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineBumpCount", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCorruptionCount", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCorruptionFails", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N", 0.0)

--Surface
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPanicScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenToPostPanic", "N", 0.0)

--[ ===================================== Biolabs Sidequests ==================================== ]
--Biolabs Aquatic Genetics
VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticMeeting", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsBlankKeycard", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsMetLord", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsSawHydrophobe", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsCompleted", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsSpokeToIsabel", "N", 0.0)

--Biolabs Rescue Reika Sidequest
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianSaw55Joke", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianDecryptAttachment", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianEntranceScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianSawReika", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianRedRelease", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianPurpleRelease", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianYellowRelease", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianBlueRelease", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianPipesReleased", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianCrackedIce", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N", 0.0)

--Biolabs Amphibian Guessing Quest
--(1) Axolotl, (2) Olm, (3) Blackbelly salamander, (4) Red-bellied newt
--(10) Bufo bufo, (11) Lemur frog, (12) Spadefoot toad, (13) Moor frog, (14) Banded bullfrog
--(20) iwokramae caecilia, (21) annalatus caecilia, (22) rubber eel
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianCorrectSpecies", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianFailureCount", "N", 0.0)

--Movie Variables
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMovieIntro", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasDoorCode", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasSeenDoorScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieTalkedToReceptionist", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieOverheard", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 0.0)

--Doctor Maisie and the Raibies!
VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundNoisemaker", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundKeycode", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundTranqGun", "N", 0.0)

--Milking
VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nobody")
VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nobody")

--[ ==================================== Side Quest Variables =================================== ]
--Pipe Nightmare
VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRedAValue", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRedBValue", "N", 19.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRedCValue", "N", 7.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRedDValue", "N", 8.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBlueAValue", "N", 12.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBlueBValue", "N", 4.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBlueCValue", "N", 18.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBlueDValue", "N", 9.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iVioletAValue", "N", 13.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iVioletBValue", "N", 4.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iVioletCValue", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iYellowAValue", "N", 12.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iYellowBValue", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iYellowCValue", "N", 8.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iYellowDValue", "N", -3.0)

--Friend Golem
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetFriendGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCanSpawnBirdcage", "N", 0.0)

--Cassandra Subquest
VM_SetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenTo", "N", 0.0)

--Equinox Facility
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxOpening", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemD", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightDollBoss", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAuthenticatorCount", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxReward", "N", 0.0)

--Serenity Crater
VM_SetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/sFirstSerenityForm", "S", "Golem")
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawDarkmatterIntro", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasRecalibrationCode", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRecalibrateCutscene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateD", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateF", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iOpenedMidwayDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/sStartedUndergroundForm", "S", "Human")
VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i300910ToldAboutCredits", "N", 0.0)

--Tellurium Mines
VM_SetVar("Root/Variables/Chapter5/Scenes/iDebugBypassRandomLevels", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasMinesWorkOrder", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToMinesLord", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedPartsGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedMinesWorkOrder", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i55ExplainedHerself", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesCCutscene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutscene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutsceneB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesFCutscene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMinesGDroidKnowsChristine", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToGGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowsAboutSecretPassage", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsSecretPassageOpen", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWakeUpIntro", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedElevator10", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedElevator20", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedElevator30", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedElevator50", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator10", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator20", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator30", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator40", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator50", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCutscene50", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iDefeatedBoss50", "N", 0.0)

--Random Mines Levels
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineASmelters", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineAFabricators", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineACrates", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBTalkedToDroids", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBLeft", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCFoundMemento", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCShelves", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDMet", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDArtSupplies", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDTerminal", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDCrate", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineAReward", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBReward", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCReward", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDReward", "N", 0.0)

--Mines Statistics
VM_SetVar("Root/Variables/Chapter5/Scenes/iLowestMinesFloor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLastMinesSeed", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTotalFloorsGenerated", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasRolledFloors", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineAFloor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBFloor", "N", 2.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCFloor", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDFloor", "N", 4.0)

--Sprocket City
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedSensor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw319", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetUE117", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSH505", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetAJ99", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawHeadlockScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAfter55Scene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitHuman", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitSteamDroid", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitDarkmatter", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitElectrosprite", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitLatexDrone", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitEldritch", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/SprocketVendor/sShopInfo", "S", "Wide-Spectrum Scanner|Viewfinder Module|Explication Spike|Dispersion Cloak|")

--JX-101 and Steam Droid Quests
VM_SetVar("Root/Variables/Chapter5/Scenes/iAskedAboutHelpingDroids", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTold55Go", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawGoEastPrompt", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsJX101Following", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedJX101Equipment", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBlackSiteKeycard", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPartsScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iKilledMonstrosity", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N", 0.0)

--SX-399.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWith55AboutSX", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketGardening", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketComputers", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketBoilers", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketLibrary", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketChess", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketPhotos", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketHats", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketDrawing", "N", 0.0)

--Electrosprite Quest
VM_SetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw198Intro", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetAmanda", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSecondAmandaScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRunFinalScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRunPostScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iGotAmandaHint", "N", 0.0)

--[ ============================ Southern Cryogenics and Manufactory ============================ ]
--Southern Cryogenics
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoSawUnpoweredDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoRepoweredDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryD", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoDeployedLadder", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoEShelf", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerABoxes", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreShutOff", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCShelf", "N", 0.0)

--Manufactory
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuFinishedJob", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSector99Scene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuTalkedTo55", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuMetKatarina", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuGotBadge", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuMetForegolemAsGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuMetAdminAsGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuTold55Plan", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuPostLatexScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuShowElevatorScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuOpenedStorageDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuCrateState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTerminalHasBackers", "N", 0.0)

--[ =================================== World State Variables =================================== ]
--Lower Regulus City
VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusRedLock", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusBlueLock", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusPartsA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusPartsB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLowerRegulusPartsC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N", 0.0)

--Exterior Train Service Facility
VM_SetVar("Root/Variables/Chapter5/Scenes/iServicePartsA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iServicePartsB", "N", 0.0)

--Door warnings.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i55ExplainedRegulusBrokenAirlock", "N", 0.0)

--Tram Stuff.
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S", "Unknown")

--55 joining and leaving
VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i55ToldAboutRadio", "N", 0.0)

--Survey Lord
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordGolem", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordLatex", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordEldritch", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordElectrosprite", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordSteamDroid", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordDarkmatter", "N", 0.0)

--Western Facility Lord
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetWAFacilityLord", "N", 0.0)

--World States
DL_AddPath("Root/Variables/Chapter5/World/")
VM_SetVar("Root/Variables/Chapter5/World/iLRTFOpenedMid", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/World/iSerenityAirlockState", "N", 0.0)

--[Opening Cutscene]
VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iShowIntroScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 0.0)

--55's Intercom state.
VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 0.0)

--Bypass intro variables.
if(gbBypassIntro == true) then
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
end

--[Other Scenes]
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAttendantStationE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUp55sSkirt", "N", 0.0)

--[ ===================================== Sophie's Variables ==================================== ]
--Unit 499323 "Sophie" can become Christine's girlfriend during this chapter. She has a lot of statistics.
DL_AddPath("Root/Variables/Chapter5/Sophie/")
VM_SetVar("Root/Variables/Chapter5/Sophie/iTalkedSophieAgain", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieDress", "N", 0.0)

--Forms shown.
VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenHuman", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenDarkmatter", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenEldritch", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenElectrosprite", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenLatex", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenSteamDroid", "N", 0.0)

--Scenes
VM_SetVar("Root/Variables/Chapter5/Sophie/iLatexInspectionScene", "N", 0.0)

--Special Human Data
VM_SetVar("Root/Variables/Chapter5/Sophie/iSpecialHumanDate", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanFirstScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanSecondScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanRepeat", "N", 0.0)

--Dates.
VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWentToQuarters", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iTotalDates", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iConversationCounter", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronize", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N", 0.0)

--Sophie explaining variables.
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedMaintenance", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedRaijus", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedFabricator", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedOpenComputers", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedTetris", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedSpaceInvaders", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedBlueSphere", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedVideograph", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedTable", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedTelevision", "N", 0.0)

--Type of Date Counters.
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateWorkInMaintenance", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateWatchRaijus", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateRunFabricators", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateBlueSphere", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateTetris", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateSpaceInvaders", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateWatchMovieKungFu", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateWatchMovieRustMonster", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateWatchMovieRustMonster2", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateWatchMovieHoneyShortCircuit", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Sophie/iDateWatchMovieUplifting", "N", 0.0)

--[ ===================================== Party's Variables ===================================== ]
--System.
DL_AddPath("Root/Variables/Global/Christine/")

--Christine's form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Human")
VM_SetVar("Root/Variables/Global/Christine/iHasDollForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iHasGolemForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iHasLatexForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N", 0.0)

--Extra form data.

--Christine's bonus equipment.
VM_SetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iLightBoostA", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iLightBoostB", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagCharges", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagPotency", "N", 0.0)

--Information Christine knows.
VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutDarkmatters", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N", 0.0)
VM_SetVar("Root/Variables/Global/Christine/iSaidAutoTransformDialogue", "N", 0.0)

--Christine's skills.
VM_SetVar("Root/Variables/Global/Christine/iSkillbook00", "N", 1.0) --Shock
VM_SetVar("Root/Variables/Global/Christine/iSkillbook01", "N", 1.0) --Batter
VM_SetVar("Root/Variables/Global/Christine/iSkillbook02", "N", 1.0) --Encourage
VM_SetVar("Root/Variables/Global/Christine/iSkillbook03", "N", 0.0) --Rally
VM_SetVar("Root/Variables/Global/Christine/iSkillbook04", "N", 0.0) --Sweep
VM_SetVar("Root/Variables/Global/Christine/iSkillbook05", "N", 0.0) --Officer Charge

--[Unit 2855's Stats]
--55's skills.
VM_SetVar("Root/Variables/Global/2855/iSkillbook00", "N", 1.0) --Algorithms
VM_SetVar("Root/Variables/Global/2855/iSkillbook01", "N", 1.0) --High-Power Shot, Rubber Shot
VM_SetVar("Root/Variables/Global/2855/iSkillbook02", "N", 1.0) --Wide lens Shot, Tactical Shift, Repair
VM_SetVar("Root/Variables/Global/2855/iSkillbook03", "N", 1.0) --Spotless Protocol
VM_SetVar("Root/Variables/Global/2855/iSkillbook04", "N", 1.0) --Disruptor Blast
VM_SetVar("Root/Variables/Global/2855/iSkillbook05", "N", 1.0) --Hyper Repair
VM_SetVar("Root/Variables/Global/2855/iSkillbook06", "N", 0.0) --Take Cover

--Other
VM_SetVar("Root/Variables/Global/2855/iSaidWarpDialogue", "N", 0.0)

--[Transformation Cutscene Variables]
--[Dialogue With Character Variables]
--[World: State Variables]
DL_AddPath("Root/Variables/Chapter5/WorldState")
VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 0.0)

--[Global Variables]
--Variables.
gsPartyLeaderName = "Christine"
giPartyLeaderID = 0

--Following Characters.
gsFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {0}
