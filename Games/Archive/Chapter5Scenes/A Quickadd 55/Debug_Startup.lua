--[Special]
--Instantly adds a character to the party if they don't already exist.
local sCombatName = "55"
local sFieldName = "55"
local sFormName = "Doll"
local sVariableName = "Root/Variables/Chapter5/Scenes/iIs55Following"

--Scan. If the character is already following, do nothing.
for i = 1, gsFollowersTotal, 1 do
	if(gsaFollowerNames[i] == sFieldName) then
		return
	end
end

--Flag to indicate she is following the chapter leader.
VM_SetVar(sVariableName, "N", 1.0)

--[Field Setting]
--Create if she doesn't exist.
if(EM_Exists(sFieldName) == false) then
	fnSpecialCharacter(sFieldName, sFormName, -100, -100, gci_Face_South, false, nil)
end

--Get the characters's uniqueID. 
EM_PushEntity(sFieldName)
	local iCharacterID = RE_GetID()
DL_PopActiveObject()

--[Lua Globals]
--Create a new table with this character in it.
local tFollowerNames = {sFieldName}
local tFollowerIDs   = {iCharacterID}

--Append the tables together.
gsFollowersTotal = gsFollowersTotal + 1
fnAppendTables(gsaFollowerNames, tFollowerNames)
fnAppendTables(giaFollowerIDs,   tFollowerIDs)
AL_SetProperty("Follow Actor ID", iCharacterID)

--[Combat Setting]
--Find an empty slot in the combat lineup.
local bFoundCharacter = false
local iEmptySlot = -1
for i = 0, 3, 1 do 
	local sExistingName = AC_GetProperty("Character In Slot", i)
	if(sExistingName == sCombatName) then
		bFoundCharacter = true
		break
	elseif(sExistingName == "Null" and iEmptySlot == -1) then
		iEmptySlot = i
	end
end

--If an empty slot exists and the character is not in the party, place her in that slot.
if(bFoundCharacter == false and iEmptySlot ~= -1) then
	AC_SetProperty("Set Party", iEmptySlot, sCombatName)
end

--[Party Folding]
--Fold the party so the character doesn't misbehave.
AL_SetProperty("Fold Party")
