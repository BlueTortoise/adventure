--[Special]
--Instantly adds a character to the party if they don't already exist.
local sFieldName = "JX101"
local sCombatName = "JX-101"
local sVariableName = "Root/Variables/Chapter5/Scenes/iIsJX101Following"
fnAddPartyMember(sFieldName, sCombatName, sVariableName)

--[Party Folding]
--Fold the party so the character doesn't misbehave.
AL_SetProperty("Fold Party")
