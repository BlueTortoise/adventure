--[Special]
--Removes 55's character from the field but leaves her in the combat party.
local bIs55Following = false
for i = 1, gsFollowersTotal, 1 do
	if(gsaFollowerNames[i] == "55") then
		bIs55Following = true
		break
	end
end

--Don't do anything if 55 isn't following.
if(bIs55Following == false) then return end

--[Field Sprite]
--Move 55 off the field.
if(EM_Exists("55") == true) then
	EM_PushEntity("55")
		TA_SetProperty("Position", -10, -10)
	DL_PopActiveObject()
end

--[Lua Globals]
--Build a list of followers that aren't 55.
local giNewTotal = 0
local gsaNewList = {}
local giaNewList = {}
for i = 1, gsFollowersTotal, 1 do
	if(gsaFollowerNames[i] ~= "55") then
		giNewTotal = giNewTotal + 1
		gsaNewList[giNewTotal] = gsaFollowerNames[i]
		giaNewList[giNewTotal] = giaFollowerIDs[i]
	end
end

--Crossload.
gsFollowersTotal = giNewTotal
gsaFollowerNames = gsaNewList
giaFollowerIDs   = giaNewList