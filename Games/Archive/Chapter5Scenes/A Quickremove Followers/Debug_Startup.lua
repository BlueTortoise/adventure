--[Special]
--Remove all following party members.
gsFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {0}

--List of candidates.
local saListName   = {"55",             "JX101",             "Sophie",    "SX399"}
local saCombatName = {"55",             "JX-101",            "Null",      "SX-399"}
local saListVar    = {"iIs55Following", "iIsJX101Following", "iIsOnDate", "iSX399IsFollowing"}
local baListCombat = {true,             true,                false,       true}
 
--Iterate and Remove
local i = 1
while(saListName[i] ~= nil) do
	
	--Actor unfollows.
	AL_SetProperty("Unfollow Actor Name", saListName[i])

	--Remove the entity from the field.
	if(EM_Exists(saListName[i]) == true) then
		EM_PushEntity(saListName[i])
			TA_SetProperty("Position", -10, -10)
		DL_PopActiveObject()
	end

	--Flag to indicate she is not following Christine.
	VM_SetVar("Root/Variables/Chapter5/Scenes/" .. saListVar[i], "N", 0.0)
	
	--If flagged, remove this character from the combat party.
	if(baListCombat[i] == true) then
		local iSlot = AC_GetProperty("Character Party Slot", saCombatName[i])
		if(iSlot > -1) then
			AC_SetProperty("Set Party", iSlot, "Null")
		end
	end
	
	--Next.
	i = i + 1
end