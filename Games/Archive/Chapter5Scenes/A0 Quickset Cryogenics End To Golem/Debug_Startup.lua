--[Special]
--This script immediately sets the conditions allowing Christine to exit the Cryogenics facility.
-- This will also shapeshift her to a Golem if she's not already.
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 8.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoBlueCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)

--Shapeshift.
--LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")

--Light.
AL_SetProperty("Activate Player Light", 3600, 3600)