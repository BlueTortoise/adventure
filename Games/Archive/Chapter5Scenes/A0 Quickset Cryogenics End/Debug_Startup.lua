--[Special]
--This script immediately sets the conditions allowing Christine to exit the Cryogenics facility.
-- This will also shapeshift her to a Golem if she's not already.
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 8.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoBlueCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)

--Shapeshift.
LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")

--Set the leader voice to Christine's.
WD_SetProperty("Set Leader Voice", "Christine")

--Light.
AL_SetProperty("Activate Player Light", 3600, 3600)

--If Christine has not received the equipment upgrade, do that here.
local iGotGolemEquipment = VM_GetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N")
if(iGotGolemEquipment == 0.0) then
	
	--Flag.
	VM_SetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N", 1.0)
	
	--Add the equipment.
	LM_ExecuteScript(gsItemListing, "Carbonweave Electrospear")
	LM_ExecuteScript(gsItemListing, "Flowing Dress")

	--Equip it.
	AC_PushPartyMember("Christine")
		ACE_SetProperty("Equip", "Weapon", "Carbonweave Electrospear")
		ACE_SetProperty("Equip", "Armor", "Flowing Dress")
	DL_PopActiveObject()

	--Delete Christine's old equipment.
	AdInv_SetProperty("Remove Item", "Tazer")
	AdInv_SetProperty("Remove Item", "Schoolmaster's Suit")
	
	--Change the description of Christine's runestone, if it's not equipped.
	if(AdInv_GetProperty("Item Count", "Violet Runestone") == 1) then
		AdInv_PushItem("Violet Runestone")
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
		DL_PopActiveObject()
	
	--Runestone was equipped so get it from Christine's inventory.
	else
		AC_PushPartyMember("Christine")
			ACE_SetProperty("Equip", "Item A", "Nothing")
			ACE_SetProperty("Equip", "Item B", "Nothing")
		DL_PopActiveObject()
		
		AdInv_PushItem("Violet Runestone")
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
		DL_PopActiveObject()
		
		AC_PushPartyMember("Christine")
			ACE_SetProperty("Equip", "Item A", "Violet Runestone")
		DL_PopActiveObject()
	end
end