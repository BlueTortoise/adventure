--[Special]
--Sets the scripts such that the player has seen the western LRT cutscene.
local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")

--Cryogenics variables.
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 8.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoBlueCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)

--Other.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWelcomeToRegulus", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPersonalQuarters", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N", 2.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N", 1.0)

--LRT.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawLRTOpening", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawOfficeConversation", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N", 1.0)

--[ ========================================= Adding 55 ========================================= ]
--Add 55 to the party lineup. She doesn't leave even when she hides in Regulus City.
if(iMet55InLowerRegulus == 0.0) then
	AC_SetProperty("Set Party", 1, "55")
	
	--Give her starting equipment.
	LM_ExecuteScript(gsItemListing, "Pulse Diffractor")
	LM_ExecuteScript(gsItemListing, "Command Unit Garb")
	AC_PushPartyMember("55")
	
		--Equip it.
		ACE_SetProperty("Equip", "Weapon", "Pulse Diffractor")
		ACE_SetProperty("Equip", "Armor", "Command Unit Garb")
		
		--55 starts the game at level 4.
		ACE_SetProperty("EXP", 17 + 66 + 115 + 1)
	DL_PopActiveObject()
end

--[ ===================================== Christine Changes ===================================== ]
--[Form]
LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")

--[Abilities and Level]
--Christine gains the "Take Point" and "Line Formation" abilities. She already had them, but they were
-- always invisible. Also, she levels up to 4 if she wasn't already.
AC_PushPartyMember("Christine")

	--Gain XP difference to level 4.
	local iChristineEXP = ACE_GetProperty("XP Total")
	if(iChristineEXP < 17 + 66 + 115) then
		ACE_SetProperty("EXP", (17 + 66 + 115) - iChristineEXP + 1)
	end

	--Modify the ability so it's usable.
	ACE_PushAction("Take Point")
		ACAC_SetProperty("Visible In State", "Standard")
	DL_PopActiveObject()
DL_PopActiveObject()

--Provide Christine with these skillbook abilities to speed the game up.
local sAbilityPath = gsRoot .. "Abilities/Christine/000 Initializer.lua"
VM_SetVar("Root/Variables/Global/Christine/iSkillbook01", "N", 1.0) --Encourage
VM_SetVar("Root/Variables/Global/Christine/iSkillbook02", "N", 1.0) --Sweep
VM_SetVar("Root/Variables/Global/Christine/iSkillbook03", "N", 1.0) --Rally
AC_PushPartyMember("Christine")
	if(ACE_GetProperty("Has Ability", "Encourage") == false) then
		LM_ExecuteScript(sAbilityPath, "Encourage")
	end
	if(ACE_GetProperty("Has Ability", "Sweep") == false) then
		LM_ExecuteScript(sAbilityPath, "Sweep")
	end
	if(ACE_GetProperty("Has Ability", "Rally") == false) then
		LM_ExecuteScript(sAbilityPath, "Rally")
	end
	if(ACE_GetProperty("Has Ability", "Officer Charge") == false) then
		LM_ExecuteScript(sAbilityPath, "Officer Charge")
	end
	if(ACE_GetProperty("Has Ability", "Puncture") == false) then
		LM_ExecuteScript(sAbilityPath, "Puncture")
	end
DL_PopActiveObject()

--[Unit 2855's Skills]
--Give 55 all her chapter skills.
VM_SetVar("Root/Variables/Global/Christine/iSkillbook00", "N", 1.0) --Algorithms
VM_SetVar("Root/Variables/Global/Christine/iSkillbook01", "N", 1.0) --High-Power Shot, Rubber Shot
VM_SetVar("Root/Variables/Global/Christine/iSkillbook02", "N", 1.0) --Wide lens Shot, Tactical Shift, Repair
VM_SetVar("Root/Variables/Global/Christine/iSkillbook03", "N", 1.0) --Spotless Protocol
VM_SetVar("Root/Variables/Global/Christine/iSkillbook04", "N", 1.0) --Disruptor Blast
VM_SetVar("Root/Variables/Global/Christine/iSkillbook05", "N", 1.0) --Hyper Repair

--[Dialogue]
--Set the leader voice to Christine's.
WD_SetProperty("Set Leader Voice", "Christine")

--[Light]
--Provide the player with the light if they didn't already have it.
AL_SetProperty("Activate Player Light", 3600, 3600)

--[Equipment]
--If Christine has not received the equipment upgrade, do that here.
local iGotGolemEquipment = VM_GetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N")
if(iGotGolemEquipment == 0.0) then
	
	--Flag.
	VM_SetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N", 1.0)
	
	--Add the equipment.
	LM_ExecuteScript(gsItemListing, "Carbonweave Electrospear")
	LM_ExecuteScript(gsItemListing, "Flowing Dress")

	--Equip it.
	AC_PushPartyMember("Christine")
		ACE_SetProperty("Equip", "Weapon", "Carbonweave Electrospear")
		ACE_SetProperty("Equip", "Armor", "Flowing Dress")
	DL_PopActiveObject()

	--Delete Christine's old equipment.
	AdInv_SetProperty("Remove Item", "Tazer")
	AdInv_SetProperty("Remove Item", "Schoolmaster's Suit")
	
	--Change the description of Christine's runestone, if it's not equipped.
	if(AdInv_GetProperty("Item Count", "Violet Runestone") == 1) then
		AdInv_PushItem("Violet Runestone")
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
		DL_PopActiveObject()
	
	--Runestone was equipped so get it from Christine's inventory.
	else
		AC_PushPartyMember("Christine")
			ACE_SetProperty("Equip", "Item A", "Nothing")
			ACE_SetProperty("Equip", "Item B", "Nothing")
		DL_PopActiveObject()
		
		AdInv_PushItem("Violet Runestone")
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
		DL_PopActiveObject()
		
		AC_PushPartyMember("Christine")
			ACE_SetProperty("Equip", "Item A", "Violet Runestone")
		DL_PopActiveObject()
	end
end