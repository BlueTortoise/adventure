--[Special]
--Sets the scripts such that the player has seen the western LRT cutscene.
local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")

--Cryogenics variables.
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 8.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoBlueCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)

--Other.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWelcomeToRegulus", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPersonalQuarters", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N", 2.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N", 1.0)

--LRT.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawLRTOpening", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawOfficeConversation", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBattleCutscene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPrisonEntryScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawDiaryScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToWarden", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPrisonDiscussion", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N", 1.0)

--Biolabs.
VM_SetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieLeftInBiolabs", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N", 1.0)

--[ ========================================= Adding 55 ========================================= ]
--Add 55 to the party lineup. She doesn't leave even when she hides in Regulus City.
if(iMet55InLowerRegulus == 0.0) then
	AC_SetProperty("Set Party", 1, "55")
	
	--Give her starting equipment.
	LM_ExecuteScript(gsItemListing, "Mk VI Pulse Diffractor")
	LM_ExecuteScript(gsItemListing, "Command Unit Garb")
	LM_ExecuteScript(gsItemListing, "Adaptive Cloth Vest")
	LM_ExecuteScript(gsItemListing, "Distribution Frame")
	LM_ExecuteScript(gsItemListing, "Magrail Harmonizer Module")
	LM_ExecuteScript(gsItemListing, "Nanite Injection")
	AC_PushPartyMember("55")
	
		--Equip it.
		ACE_SetProperty("Equip", "Weapon", "Mk VI Pulse Diffractor")
		ACE_SetProperty("Equip", "Armor", "Adaptive Cloth Vest")
		ACE_SetProperty("Equip", "Accessory A", "Distribution Frame")
		ACE_SetProperty("Equip", "Accessory B", "Magrail Harmonizer Module")
		ACE_SetProperty("Equip", "Item B", "Nanite Injection")
		
		--55 starts the game at level 4.
        ACE_SetProperty("EXP", 17 + 66 + 115 + 200 + 375 + 712 + 1208  + 2055 + 1)
	DL_PopActiveObject()
    
    --55 is added to the party lineup.
    fnAddPartyMember("55", "55", "Root/Variables/Chapter5/Scenes/iIs55Following")
end

--[ ======================================= Adding SX-399 ======================================= ]
--Add SX-399 and set her flags.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)

--Get her uniqueID. 
EM_PushEntity("SX399")
    local iFollowerID = RE_GetID()
DL_PopActiveObject()

--Store it.
gsFollowersTotal = gsFollowersTotal + 1
gsaFollowerNames[gsFollowersTotal] = "SX399"
giaFollowerIDs[gsFollowersTotal] = iFollowerID

--Tell her to follow.
AL_SetProperty("Follow Actor ID", iFollowerID)

--Add her to the combat lineup.
AC_SetProperty("Set Party", 2, "SX-399")

--Equipment.
LM_ExecuteScript(gsItemListing, "Fission Carbine")
LM_ExecuteScript(gsItemListing, "Bronze Chestguard")
LM_ExecuteScript(gsItemListing, "Nanite Injection")
AC_PushPartyMember("SX-399")
    --Equip.
    ACE_SetProperty("Equip", "Weapon", "Fission Carbine")
    ACE_SetProperty("Equip", "Armor", "Bronze Chestguard")
    ACE_SetProperty("Equip", "Item A", "Nanite Injection")
    
    --SX-399 starts the game at level 7, same as 55 and Christine.
    ACE_SetProperty("EXP", 17 + 66 + 115 + 200 + 375 + 712 + 1208  + 2055 + 1)
DL_PopActiveObject()

--[ ===================================== Christine Changes ===================================== ]
--[Form]
LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")

--[Abilities and Level]
--Christine gains the "Take Point" and "Line Formation" abilities. She already had them, but they were
-- always invisible. Also, she levels up to 4 if she wasn't already.
AC_PushPartyMember("Christine")

	--Gain XP difference to level 4.
	local iChristineEXP = ACE_GetProperty("XP Total")
	if(iChristineEXP < 17 + 66 + 115) then
		ACE_SetProperty("EXP", (17 + 66 + 115 + 200 + 375 + 712 + 1208 + 2055) - iChristineEXP + 1)
	end

	--Modify the ability so it's usable.
	ACE_PushAction("Take Point")
		ACAC_SetProperty("Visible In State", "Standard")
	DL_PopActiveObject()
DL_PopActiveObject()

--Provide Christine with these skillbook abilities to speed the game up.
local sAbilityPath = gsRoot .. "Abilities/Christine/000 Initializer.lua"
VM_SetVar("Root/Variables/Global/Christine/iSkillbook01", "N", 1.0) --Encourage
VM_SetVar("Root/Variables/Global/Christine/iSkillbook02", "N", 1.0) --Sweep
VM_SetVar("Root/Variables/Global/Christine/iSkillbook03", "N", 1.0) --Rally
AC_PushPartyMember("Christine")
	if(ACE_GetProperty("Has Ability", "Encourage") == false) then
		LM_ExecuteScript(sAbilityPath, "Encourage")
	end
	if(ACE_GetProperty("Has Ability", "Sweep") == false) then
		LM_ExecuteScript(sAbilityPath, "Sweep")
	end
	if(ACE_GetProperty("Has Ability", "Rally") == false) then
		LM_ExecuteScript(sAbilityPath, "Rally")
	end
	if(ACE_GetProperty("Has Ability", "Officer Charge") == false) then
		LM_ExecuteScript(sAbilityPath, "Officer Charge")
	end
	if(ACE_GetProperty("Has Ability", "Puncture") == false) then
		LM_ExecuteScript(sAbilityPath, "Puncture")
	end
DL_PopActiveObject()

--[Unit 2855's Skills]
--Give 55 all her chapter skills.
VM_SetVar("Root/Variables/Global/Christine/iSkillbook00", "N", 1.0) --Algorithms
VM_SetVar("Root/Variables/Global/Christine/iSkillbook01", "N", 1.0) --High-Power Shot, Rubber Shot
VM_SetVar("Root/Variables/Global/Christine/iSkillbook02", "N", 1.0) --Wide lens Shot, Tactical Shift, Repair
VM_SetVar("Root/Variables/Global/Christine/iSkillbook03", "N", 1.0) --Spotless Protocol
VM_SetVar("Root/Variables/Global/Christine/iSkillbook04", "N", 1.0) --Disruptor Blast
VM_SetVar("Root/Variables/Global/Christine/iSkillbook05", "N", 1.0) --Hyper Repair

--[Dialogue]
--Set the leader voice to Christine's.
WD_SetProperty("Set Leader Voice", "Christine")

--[Light]
--Provide the player with the light if they didn't already have it.
AL_SetProperty("Activate Player Light", 3600, 3600)

--[Equipment]
--If Christine has not received the equipment upgrade, do that here.
local iGotGolemEquipment = VM_GetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N")
if(iGotGolemEquipment == 0.0) then
	
	--Flag.
	VM_SetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N", 1.0)
	
	--Add the equipment.
	LM_ExecuteScript(gsItemListing, "Silksteel Electrospear")
	LM_ExecuteScript(gsItemListing, "Flowing Dress")
	LM_ExecuteScript(gsItemListing, "Ceramic Weave Vest")
	LM_ExecuteScript(gsItemListing, "Sure-Grip Gloves")
	LM_ExecuteScript(gsItemListing, "Spear Foregrip")
	LM_ExecuteScript(gsItemListing, "Nanite Injection")

	--Equip it.
	AC_PushPartyMember("Christine")
		ACE_SetProperty("Equip", "Weapon", "Silksteel Electrospear")
		ACE_SetProperty("Equip", "Armor", "Ceramic Weave Vest")
		ACE_SetProperty("Equip", "Accessory A", "Sure-Grip Gloves")
		ACE_SetProperty("Equip", "Accessory B", "Spear Foregrip")
		ACE_SetProperty("Equip", "Item B", "Nanite Injection")
	DL_PopActiveObject()

	--Delete Christine's old equipment.
	AdInv_SetProperty("Remove Item", "Tazer")
	AdInv_SetProperty("Remove Item", "Schoolmaster's Suit")
	
	--Change the description of Christine's runestone, if it's not equipped.
	if(AdInv_GetProperty("Item Count", "Violet Runestone") == 1) then
		AdInv_PushItem("Violet Runestone")
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
		DL_PopActiveObject()
	
	--Runestone was equipped so get it from Christine's inventory.
	else
		AC_PushPartyMember("Christine")
			ACE_SetProperty("Equip", "Item A", "Nothing")
			ACE_SetProperty("Equip", "Item B", "Nothing")
		DL_PopActiveObject()
		
		AdInv_PushItem("Violet Runestone")
			AdItem_SetProperty("Description", "An heirloom of the Dormer family, given to 771852 by her recently retired aunt.\nRestores HP when used in combat, and increases attack power by 30%% for one turn. Cooldown 2.")
		DL_PopActiveObject()
		
		AC_PushPartyMember("Christine")
			ACE_SetProperty("Equip", "Item A", "Violet Runestone")
		DL_PopActiveObject()
	end
end

--[ ======================================= Adding Sophie ======================================= ]
--Sophie is on the tail of the party during this segment.
fnAddPartyMember("Sophie", "Null", "Null")

--Inform the game that Christine and Sophie are wearing their gala dresses.
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S", "GolemGala")
VM_SetVar("Root/Variables/Costumes/Sophie/sCostumeGolem", "S", "GolemGala")
VM_SetVar("Root/Variables/Global/Christine/iWearingGolemDress", "N", 1.0)
LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Golem")
LM_ExecuteScript(gsCostumeAutoresolve, "Sophie_Golem")
    
--[ ========================================= Doctor Bag ======================================== ]
--Algorithm sets to default.
gbAutoSetDoctorBagCurrentValues = true
LM_ExecuteScript(gsComputeDoctorBagTotalPath)

