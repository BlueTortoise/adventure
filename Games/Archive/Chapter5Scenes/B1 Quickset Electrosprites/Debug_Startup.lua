--[Special]
--Sets the Electrosprite Quest to complete.
VM_SetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw198Intro", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMetAmanda", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSecondAmandaScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRunFinalScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRunPostScene", "N", 1.0)

--Capstone
VM_SetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N", 1.0)