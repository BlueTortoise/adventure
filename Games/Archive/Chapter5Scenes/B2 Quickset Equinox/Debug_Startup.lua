--[Special]
--Sets the Equinox Quest to complete.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxOpening", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemC", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemD", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightDollBoss", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAuthenticatorCount", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N", 1.0)

--Capstone
VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxReward", "N", 1.0)