--[Special]
--Sets the Serenity Quest to complete.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawDarkmatterIntro", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasRecalibrationCode", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateC", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateD", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateE", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibrateF", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iOpenedMidwayDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneC", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/sStartedUndergroundForm", "S", "Human")

--Capstone
VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i300910ToldAboutCredits", "N", 1.0)