--[Build Scene List]
--Builds a list of cutscenes the player can make use of through the debug menu. Cutscenes in the debug menu are
-- used at the player's own risk, they may produce unusable situations that can only be remedied with the debug menu.
-- It's an addiction, I tell you.
AC_SetProperty("Cutscene Path", "Chapter5Scenes")
local saSceneList = 

--Character Adders
{"A Quickadd 55", "A Quickadd JX101", "A Quickremove 55 Field", "A Quickremove Followers", 

--Main Story up to LRT
"A0 Quickset Cryogenics End", "A0 Quickset Cryogenics End To Golem", "A1 Quickset Receive Function", "A2 Quickset End Of First Date", "A3 Quickset End Of Regulus City", "A4 Quickset LRT West Done", "A5 Quickset LRT Done",
"A6 Quickset Biolabs Start",

--Side Quests up to LRT
"B0 Quickset Tellurium", "B1 Quickset Electrosprites", "B2 Quickset Equinox", "B3 Quickset Serenity",

--Other
"Defeat_LatexDrone", "Transform_SteamDroidFirst", "C0 Add Rep Items", "C1 PartyTough State", "C2 Biolabs Items", "C3 Biolabs Topics"


}

--Now set as necessary.
local i = 1
ADebug_SetProperty("Cutscenes Total", #saSceneList)
while(saSceneList[i] ~= nil) do
	ADebug_SetProperty("Cutscene Path", i-1, saSceneList[i])
	i = i + 1
end