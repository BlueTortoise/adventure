--[Special]
--Party is set to level 7, gains a set of items that a level 7 party is expected to have. This is roughly where
-- we can expect the party to be after completing most of Chapter 5 before the Gala events.
LM_ExecuteScript(gsItemListing, "Silksteel Electrospear")
LM_ExecuteScript(gsItemListing, "Neutronium Jacket")
LM_ExecuteScript(gsItemListing, "Kinetic Capacitor")
LM_ExecuteScript(gsItemListing, "Sure-Grip Gloves")

LM_ExecuteScript(gsItemListing, "Mk VI Pulse Diffractor")
LM_ExecuteScript(gsItemListing, "Adaptive Cloth Vest")
LM_ExecuteScript(gsItemListing, "Pulse Radiation Dampener")
LM_ExecuteScript(gsItemListing, "Magrail Harmonizer Module")

LM_ExecuteScript(gsItemListing, "Regeneration Mist")
LM_ExecuteScript(gsItemListing, "Nanite Injection")
LM_ExecuteScript(gsItemListing, "Nanite Injection")

AC_PushPartyMember("Christine")
    ACE_SetProperty("EXP", giaExpTable[5])
    ACE_SetProperty("EXP", giaExpTable[6])
    ACE_SetProperty("EXP", giaExpTable[7])
DL_PopActiveObject()
AC_PushPartyMember("55")
    ACE_SetProperty("EXP", giaExpTable[5])
    ACE_SetProperty("EXP", giaExpTable[6])
    ACE_SetProperty("EXP", giaExpTable[7])
DL_PopActiveObject()