--[Special]
--Christine's Items
LM_ExecuteScript(gsItemListing, "Yttrium Electrospear")

--55's Items
LM_ExecuteScript(gsItemListing, "R-77 Pulse Diffractor")
LM_ExecuteScript(gsItemListing, "Hotshot Pulse Diffractor")
LM_ExecuteScript(gsItemListing, "Smoke Bomb")

--SX-399's Items
LM_ExecuteScript(gsItemListing, "Fission Rifle")
LM_ExecuteScript(gsItemListing, "Plutonite Fission Carbine")
LM_ExecuteScript(gsItemListing, "Sweet Flame Decals")
LM_ExecuteScript(gsItemListing, "Underslung Flamethrower")

--Healing Items
LM_ExecuteScript(gsItemListing, "Emergency Medkit")

--Light Armor
LM_ExecuteScript(gsItemListing, "Battle Skirt")
LM_ExecuteScript(gsItemListing, "Hyperweave Chemise")
LM_ExecuteScript(gsItemListing, "Neon Tanktop")

--Medium Armor
LM_ExecuteScript(gsItemListing, "Brass Polymer Chestguard")

--Heavy Armor
LM_ExecuteScript(gsItemListing, "Titanweave Vest")

--Accessories
LM_ExecuteScript(gsItemListing, "Integrated Gunsight")
LM_ExecuteScript(gsItemListing, "Tungsten Suppressor")
LM_ExecuteScript(gsItemListing, "Insulated Boots")
LM_ExecuteScript(gsItemListing, "Enchanted Ring")
LM_ExecuteScript(gsItemListing, "Jade Necklace")

--Ability Items
LM_ExecuteScript(gsItemListing, "Magnesium Grenade")
LM_ExecuteScript(gsItemListing, "Recoil Dampener")