--[Cassandra To Golem]
--If the player decides to transform Cassandra in the basement of sector 15, this plays.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra gasped as Christine pressed a hand against her chest.[SOFTBLOCK] The coldness of the metal seeped through her shirt and into her skin, drawing a startled gasp from Cassandra.[SOFTBLOCK] The hand lingered there as a wry smile played across Christine's lips, while Cassandra's seemed to fumble in silent confusion.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A single moment passed, the span of a single beat of the girl's heart, before Christine withdrew her hand.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "But the cold of her touch lingered on, and was joined by pinching bite into her flesh.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Cassandra looked down as realization dawned on her mind.[SOFTBLOCK] A small, metal disc filled her gaze, the gleam of its polished surface standing out against her white shirt.[SOFTBLOCK] She grasped at it as a quiet sound escaped her throat, a wordless refusal as her fingers pried at the disc without success.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The bite deepend as her fingers grasped at the disc, and soon a burrowing sensation began to snake through her flesh, reaching out to her spine, and latching onto it.[SOFTBLOCK] She could feel herself losing control of her body as the snaking tendril worked its way up her spine until consciousness itself faded.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine looked on with glee as the fear in Cassandra's face fell away, replaced by an idle smile.[SOFTBLOCK] Cassandra's body began to move under the command of the golem core, seeking out the nearest conversion tube.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/GolemTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine followed as the memories of her own transformation filled her mind, and she was at once both excited to see the process from the other side of the glass, and driven by a quiet sensation that began to stir deep within her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The girl pulled at her clothes as she walked, tearing at their seams until the scraps of her shirt and pants fell to the floor.[SOFTBLOCK] The tube opened with a quiet hiss as the pressurized air within escaped, and she stepped inside.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The tube closed around her as she stared out of the glass with an unthinking gaze.[SOFTBLOCK] She stood motionless in her underwear, her hands hanging limp at her sides and the tips of her fingers showing only the slightest hint at the force which she had pulled against the core that now controlled her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The tube pressurized, and a small hatch slid open to reveal a tube-ladened needle that descended with a mechanical precision and inserted itself into her neck.[SOFTBLOCK] Several narrow grates soon revealed themselves in the floor of the tube, and with a whirring hum, a white fluid began to gurgle up through the grates and needle, and pool about Cassandra's feet.") ]])
fnCutsceneBlocker()
		
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/GolemTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/GolemTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The fluid began to cling to her skin, dissolving the thin fabric of her underwear and cleansing her skin of dirt and grease.[SOFTBLOCK] The flowing fluid flooded into the tube, and the refraction in the liquid accentuating the exposed curves of her body until the tube had filled.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It seeped into her skin and surged throughout her body, melting away the soft tissues and organs and replacing them with facsimiles created by the white liquid.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "With her body covered and filled, the whirring hum ceased.[SOFTBLOCK] The needle withdrew from her neck and was replaced by a helmet that descended from the same hatch.[SOFTBLOCK] It conformed to her face, blocking what little remained of her deadened senses.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/GolemTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/GolemTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "From within the helmet a multitude of needles bored into her skull, and once more the white liquid surged through the tubes and filled her.[SOFTBLOCK] The soft tissue of her brain, the last lingering proof of her humanity, was swiftly replaced.[SOFTBLOCK] It formed a complex system of processing units, memory banks, and all of the circuitry that would allow her to operate.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A flash of light pulsed into what would become her new eyes, a holographic data set containing all of the necessary programming for a newly-created Golem unit to operate, and seared itself into the first tables of her memory.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Moments later, an electrical current coursed through her body.[SOFTBLOCK] The fluid began to harden against her skin, taking the necessary properties at each point along her body that would allow her to operate in even the harshest of conditions.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "In some places a carbon nanofiber glass took form, in others an intricate nanoweave of durable alloys, and in more places still the fluid became thick conduit.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "At last the helmet lifted to reveal Cassandra's changed face, and Christine looked on in wonder.[SOFTBLOCK] The girl's human mind and body had been purged, replaced with a mechanized improvement.[SOFTBLOCK] As she stood now, she was little more than a puppet.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The grates in the floor opened again, and now instead of filling the tube the fluid began to drain.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She stared ahead of her with empty eyes, and Christine watched eagerly as the next step in the process began.[SOFTBLOCK] The needle returned and inserted itself into her spine once more, but where before there had been tubes of white fluid were now thick cables of conduit.[SOFTBLOCK] With a surge of energy, it charged her power core before withdrawing one final time.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the last of the fluid drained away, her mouth, still twisted into an idle smile, spoke with her new voice.[SOFTBLOCK] \"Reprogramming complete.\"") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/GolemTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/GolemTF3") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Above her, mechanical hands appeared from where the nozzles had once been.[SOFTBLOCK] They descended bearing a flowing cyan gown, and the new golem raised her arms and allowed herself to be dressed.[SOFTBLOCK] With this final action completed, the tube slid open with a familiar hiss and Cassandra, now Unit 771853, emerged.[SOFTBLOCK] She turned to look at Christine as she spoke once more.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "\"My current function is to report to Regulus City for further assignment\" she said with a flat voice.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine, giddy with excitement, nodded and pointed Cassandra to the elevators.[SOFTBLOCK] Watching Cassandra undergo the very same process as she had excited her in ways she could scarcely explain, and her mind filled itself with visions of a multitude of humans being converted.") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()