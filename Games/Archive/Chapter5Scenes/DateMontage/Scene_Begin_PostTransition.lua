--[Scene Begin - Post Transition]
--The montage takes place on its own special map. This is fired as that map loads.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--[ ====================================== Character Setup ====================================== ]
--Spawn Sophie. She has special frames for this cutscene.
TA_Create("Sophie")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
	TA_SetProperty("Facing", gci_Face_North)
		
	--Special frames.
	TA_SetProperty("Wipe Special Frames")
	TA_SetProperty("Add Special Frame", "Laugh0",  "Root/Images/Sprites/Special/Sophie|Laugh0")
	TA_SetProperty("Add Special Frame", "Laugh1",  "Root/Images/Sprites/Special/Sophie|Laugh1")
	for i = 0, 9, 1 do
		TA_SetProperty("Add Special Frame", "HoldHands" .. i, "Root/Images/Sprites/Special/ChristineSophie|HoldHands" .. i)
	end
DL_PopActiveObject()

--Golem Lord. Used in a few scenes.
TA_Create("GolemLordA")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/GolemLordD/", false)
	TA_SetProperty("Facing", gci_Face_North)
DL_PopActiveObject()

--Golem Slave. Used in many scenes. Has a pack on.
TA_Create("GolemSlaveA")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	TA_SetProperty("Facing", gci_Face_North)
		
	--Special frames.
	TA_SetProperty("Wipe Special Frames")
	TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlave|Wounded")
DL_PopActiveObject()

--Golem slave B and C. Used in the social cutscene.
TA_Create("GolemSlaveB")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	TA_SetProperty("Facing", gci_Face_North)
DL_PopActiveObject()
TA_Create("GolemSlaveC")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/GolemSlaveP/", false)
	TA_SetProperty("Facing", gci_Face_North)
DL_PopActiveObject()

--Scraprat. Ka-boom!
TA_Create("Scraprat")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/Scraprat/", false)
	TA_SetProperty("Facing", gci_Face_North)
	TA_SetProperty("Tiny", true)
	
	--Scraprat needs to have its movement frames specified manually.
	TA_SetProperty("Move Frame", 6, 0, "Root/Images/Sprites/Scraprat/West|2")
		
	--Special frames.
	TA_SetProperty("Wipe Special Frames")
	for i = 0, 11, 1 do
		local sName = "Explode"
		if(i < 10) then sName = sName .. "0" end
		sName = sName .. i
		TA_SetProperty("Add Special Frame", sName, "Root/Images/Sprites/Special/Scraprat|" .. sName)
	end
DL_PopActiveObject()

--[ ========================================== Scene 1 ========================================== ]
--Sophie is at work, and Christine comes in. They work together.
fnCutsceneTeleport("Christine", -100.25, -100.50)
fnCutsceneFace("Christine", -1, 0)
fnCutsceneTeleport("Sophie", 24.25, 64.50)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneBlocker()

--Focus the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 10000.0)
	CameraEvent_SetProperty("Focus Position", (22.25 * gciSizePerTile), (60.50 * gciSizePerTile))
DL_PopActiveObject()

--Start the music.
fnCutsceneInstruction([[ AL_SetProperty("Music", "SophiesTheme") ]])
fnCutsceneWait(360)
fnCutsceneBlocker()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

--Christine appears, and walks in.
fnCutsceneWait(125)
fnCutsceneBlocker()
fnCutsceneTeleport("Christine", 30.25, 61.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 27.25, 61.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 1)
fnCutsceneMove("Sophie", 25.25, 64.50)
fnCutsceneMove("Sophie", 25.25, 61.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneMove("Sophie", 26.25, 61.50)
fnCutsceneBlocker()
fnCutsceneWait(165)
fnCutsceneBlocker()

--They walk to the assembly line together.
fnCutsceneMove("Christine", 23.25, 61.50)
fnCutsceneMove("Christine", 23.25, 60.50)
fnCutsceneMove("Sophie", 22.25, 61.50)
fnCutsceneMove("Sophie", 22.25, 60.00)
fnCutsceneBlocker()
fnCutsceneWait(85)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 1)
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneMove("Christine", 23.25, 60.00, 0.50)
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneWait(145)
fnCutsceneBlocker()

--Fade out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(300)
fnCutsceneBlocker()

--[ ========================================== Scene 2 ========================================== ]
--Christine and Sophie see a golem collapse. Christine defends the golem from her vengeful lord.
fnCutsceneTeleport("Christine", 117.25, 60.50)
fnCutsceneFace("Christine", -1, 0)
fnCutsceneTeleport("Sophie", 116.25, 60.50)
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneTeleport("GolemLordA", 121.25, 51.50)
fnCutsceneFace("GolemLordA", 0, 1)
fnCutsceneTeleport("GolemSlaveA", 121.25, 50.50)
fnCutsceneFace("GolemSlaveA", 0, 1)
fnCutsceneBlocker()

--Focus the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 10000.0)
	CameraEvent_SetProperty("Focus Position", (120.25 * gciSizePerTile), (60.50 * gciSizePerTile))
DL_PopActiveObject()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

--Lord and Slave walk downwards. Slave starts to slow down.
fnCutsceneMove("GolemLordA", 120.25, 59.50)
fnCutsceneMove("GolemSlaveA", 120.25, 58.50)
fnCutsceneBlocker()
fnCutsceneMove("GolemLordA", 120.25, 63.50)
fnCutsceneMove("GolemSlaveA", 120.25, 60.50, 0.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Slave falls over. Lord turns around.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "GolemSlaveA")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneFace("GolemLordA", 0, -1)
fnCutsceneWait(65)
fnCutsceneBlocker()

--Lord turns around and approaches the golem. Christine and Sophie turn to face.
fnCutsceneFace("Christine", 1, 0)
fnCutsceneMove("GolemLordA", 120.25, 61.50)
fnCutsceneWait(65)
fnCutsceneBlocker()

--Lord strikes the golem.
fnCutsceneFace("GolemLordA", 1, 0)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
fnCutsceneFace("GolemLordA", 0, -1)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneWait(35)
fnCutsceneBlocker()

--Christine intervenes.
fnCutsceneMove("Christine", 119.25, 60.50, 2.50)
fnCutsceneMove("Christine", 119.25, 61.50, 2.50)
fnCutsceneBlocker()
fnCutsceneMoveFace("Christine", 120.25, 61.50, 0, 1, 2.50)
fnCutsceneMoveFace("GolemLordA", 120.25, 62.50, 0, -1, 2.50)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Sophie checks the downed golem.
fnCutsceneMove("Sophie", 119.25, 60.50)
fnCutsceneBlocker()
fnCutsceneWait(85)
fnCutsceneBlocker()

--Christine forces the lord back a step.
fnCutsceneMoveFace("Christine", 120.25, 62.50, 0, 1, 0.30)
fnCutsceneMoveFace("GolemLordA", 120.25, 63.50, 0, -1, 0.30)
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Sophie helps the golem up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "GolemSlaveA")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneFace("GolemSlaveA", -1, 0)
fnCutsceneWait(65)
fnCutsceneBlocker()

--Christine forces the lord back one more time.
fnCutsceneMoveFace("Christine", 120.25, 63.50, 0, 1, 0.30)
fnCutsceneMoveFace("GolemLordA", 120.25, 64.50, 0, -1, 0.30)
fnCutsceneBlocker()
fnCutsceneWait(85)
fnCutsceneBlocker()

--Sophie walks up to Christine.
fnCutsceneFace("GolemSlaveA", 0, 1)
fnCutsceneMove("Sophie", 119.25, 63.50)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()

--Sophie drags Christine back a bit.
fnCutsceneMoveFace("Christine", 120.25, 62.50, 0, 1, 0.30)
fnCutsceneMoveFace("Sophie",    119.25, 62.50, 1, 0, 0.30)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(85)
fnCutsceneBlocker()

--Christine checks the slave golem.
fnCutsceneMove("Christine", 120.25, 61.50)
fnCutsceneMove("Sophie", 119.25, 60.50)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(85)
fnCutsceneBlocker()

--Lord walks up. Christine scares her off.
fnCutsceneMove("GolemLordA", 120.25, 62.50, 0.30)
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 120.25, 61.80, 2.0)
fnCutsceneMoveFace("GolemLordA", 120.25, 67.50, 0, -1, 2.50)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Fade out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(300)
fnCutsceneBlocker()
fnCutsceneTeleport("GolemLordA", -100.25, -100.50)
fnCutsceneTeleport("GolemSlaveA", -100.25, -100.50)

--[ ========================================== Scene 3 ========================================== ]
--Working on a broken terminal. Christine punches it to fix it... somehow.
fnCutsceneTeleport("Christine", 55.25, 60.50)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneTeleport("Sophie", 56.25, 60.50)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneBlocker()

--Position the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 10000.0)
	CameraEvent_SetProperty("Focus Position", (55.75 * gciSizePerTile), (59.50 * gciSizePerTile))
DL_PopActiveObject()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(185)
fnCutsceneBlocker()

--Sophie stands at the terminal for a moment.
fnCutsceneMove("Sophie", 56.25, 59.50)
fnCutsceneBlocker()
fnCutsceneWait(185)
fnCutsceneBlocker()

--Sophie gets angry.
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 55.25, 59.50)
fnCutsceneBlocker()
fnCutsceneMoveFace("Christine", 55.25, 60.50, 1, 0)
fnCutsceneMoveFace("Sophie", 56.25, 60.50, 0, -1)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneWait(65)
fnCutsceneBlocker()

--Christine backs up...
fnCutsceneMoveFace("Christine", 55.25, 62.50, 0, -1, 0.25)
fnCutsceneBlocker()
fnCutsceneMoveFace("Sophie", 57.25, 60.50, -1, 1)
fnCutsceneWait(75)
fnCutsceneBlocker()

--Slam into the machine!
fnCutsceneMove("Christine", 55.25, 59.50, 3.0)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMoveFace("Christine", 55.25, 59.75, -1, -1, 0.15)
fnCutsceneMoveFace("Christine", 55.25, 60.00, -1,  0, 0.15)
fnCutsceneMoveFace("Christine", 55.25, 60.25, -1,  1, 0.15)
fnCutsceneMoveFace("Christine", 55.25, 60.50,  0,  1, 0.15)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Sophie comforts Christine. It helps.
fnCutsceneMove("Sophie", 56.25, 60.50)
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneWait(65)
fnCutsceneBlocker()

--Computer turns on somehow.
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "CutsceneWalls", true) ]])
fnCutsceneWait(7)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "CutsceneWalls", false) ]])
fnCutsceneWait(7)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "CutsceneWalls", true) ]])
fnCutsceneWait(7)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "CutsceneWalls", false) ]])
fnCutsceneWait(7)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "CutsceneWalls", true) ]])
fnCutsceneWait(65)
fnCutsceneBlocker()

--They turn to see it.
fnCutsceneFace("Christine", 0, -1)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneWait(95)
fnCutsceneBlocker()

--They look towards the camera and laugh.
fnCutsceneFace("Christine", 0, 1)
fnCutsceneFace("Sophie", 0, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()

--Repeat this 8 times before fading out. Alternate.
for i = 1, 8, 1 do
	fnCutsceneSetFrame("Christine", "Laugh0")
	fnCutsceneSetFrame("Sophie", "Laugh1")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "Laugh1")
	fnCutsceneSetFrame("Sophie", "Laugh0")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
end

--Start fading out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--Continue laughing.
for i = 1, 3, 1 do
	fnCutsceneSetFrame("Christine", "Laugh0")
	fnCutsceneSetFrame("Sophie", "Laugh1")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "Laugh1")
	fnCutsceneSetFrame("Sophie", "Laugh0")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
end

--Wait a while.
fnCutsceneWait(180)
fnCutsceneBlocker()

--[ ========================================== Scene 4 ========================================== ]
--The other golems shun Christine, until Sophie shows them dhe wae.
fnCutsceneTeleport("Christine", 66.25, 47.50)
fnCutsceneFace("Christine", -1, 0)
fnCutsceneTeleport("Sophie", 65.25, 47.50)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneTeleport("GolemSlaveA", 65.25, 42.50)
fnCutsceneFace("GolemSlaveA", 0, -1)
fnCutsceneTeleport("GolemSlaveB", 66.25, 42.50)
fnCutsceneFace("GolemSlaveB", 0, -1)
fnCutsceneTeleport("GolemSlaveC", 66.25, 40.50)
fnCutsceneFace("GolemSlaveC", 0, 1)
fnCutsceneBlocker()

--Clear special frames.
fnCutsceneSetFrame("Christine", "Null")
fnCutsceneSetFrame("Sophie", "Null")

--Focus the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 10000.0)
	CameraEvent_SetProperty("Focus Position", (67.25 * gciSizePerTile), (43.50 * gciSizePerTile))
DL_PopActiveObject()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Christine goes in.
fnCutsceneMove("Christine", 67.25, 47.50)
fnCutsceneMove("Christine", 67.25, 46.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorC") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 67.25, 43.50)
fnCutsceneBlocker()

--Golems turn around.
fnCutsceneFace("GolemSlaveA", 0, 1)
fnCutsceneFace("GolemSlaveB", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(125)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 67.25, 43.50, 0.30)
fnCutsceneBlocker()

--They shun her.
fnCutsceneFace("GolemSlaveA", 0, -1)
fnCutsceneFace("GolemSlaveB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(85)
fnCutsceneBlocker()

--Christine slowly walks to the other table.
fnCutsceneMove("Christine", 67.25, 43.50, 0.70)
fnCutsceneMove("Christine", 71.25, 42.50, 0.70)
fnCutsceneBlocker()
fnCutsceneSetFrame("Christine", "Sad")
fnCutsceneWait(65)
fnCutsceneBlocker()

--Sophie comes in.
fnCutsceneMove("Sophie", 67.25, 47.50)
fnCutsceneMove("Sophie", 67.25, 43.50)
fnCutsceneMove("Sophie", 70.25, 43.50)
fnCutsceneMove("Sophie", 70.25, 42.50)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(60)
fnCutsceneBlocker()
fnCutsceneWait(85)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()

--Sophie talks to the other golems.
fnCutsceneMove("Sophie", 67.25, 42.50)
fnCutsceneBlocker()
fnCutsceneWait(85)
fnCutsceneFace("GolemSlaveA", 1, 0)
fnCutsceneFace("GolemSlaveB", 1, 0)
fnCutsceneFace("GolemSlaveC", 1, 0)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Christine", "Null")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 70.25, 42.50, 0.40)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneWait(65)

--Sophie moves down to show Christine.
fnCutsceneMoveFace("Sophie", 67.25, 44.50, 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 69.25, 42.50, 0.40)
fnCutsceneBlocker()
fnCutsceneMove("GolemSlaveA", 65.25, 43.50)
fnCutsceneMove("GolemSlaveA", 69.25, 43.50)
fnCutsceneMove("GolemSlaveB", 68.25, 42.50)
fnCutsceneMove("GolemSlaveC", 67.25, 40.50)
fnCutsceneMove("GolemSlaveC", 67.25, 41.50)
fnCutsceneMove("GolemSlaveC", 69.25, 41.50)
fnCutsceneBlocker()
fnCutsceneFace("GolemSlaveA", 0, -1)
fnCutsceneFace("GolemSlaveB", 1, 0)
fnCutsceneFace("GolemSlaveC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, 1)
fnCutsceneWait(180)
fnCutsceneBlocker()

--Start fading out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(300)
fnCutsceneBlocker()

--Clean.
fnCutsceneTeleport("GolemSlaveA", -100.25, -100.50)
fnCutsceneTeleport("GolemSlaveB", -100.25, -100.50)
fnCutsceneTeleport("GolemSlaveC", -100.25, -100.50)

--[ ========================================== Scene 5 ========================================== ]
--Scraprat scene. A golem brings in a scraprat, who almost blows Sophie up.
fnCutsceneTeleport("Christine", 14.25, 60.50)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneTeleport("Sophie", 18.25, 64.50)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneTeleport("GolemSlaveA", 30.25, 61.50)
fnCutsceneFace("GolemSlaveA", -1, 0)
fnCutsceneTeleport("Scraprat", 30.25, 61.60)
fnCutsceneFace("Scraprat", -1, 0)
fnCutsceneBlocker()

--Focus the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 10000.0)
	CameraEvent_SetProperty("Focus Position", (19.25 * gciSizePerTile), (60.50 * gciSizePerTile))
DL_PopActiveObject()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(125)
fnCutsceneBlocker()

--Golem walks in.
fnCutsceneMove("GolemSlaveA", 29.25, 61.50)
fnCutsceneBlocker()
fnCutsceneMove("GolemSlaveA", 19.25, 61.50)
fnCutsceneMove("Scraprat", 20.05, 61.50)
fnCutsceneBlocker()

--Sophie walks up to the golem.
fnCutsceneMove("Sophie", 19.25, 64.50)
fnCutsceneMove("Sophie", 19.25, 62.50)
fnCutsceneBlocker()
fnCutsceneFace("GolemSlaveA", 0, 1)
fnCutsceneWait(60)
fnCutsceneBlocker()

--Golem moves aside so Sophie can see the scraprat.
fnCutsceneMoveFace("GolemSlaveA", 19.25, 60.30, 0, 1)
fnCutsceneMove("Scraprat", 18.97, 61.50)
fnCutsceneFace("Scraprat", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(60)
fnCutsceneBlocker()

--Sophie walks around the scraprat.
fnCutsceneMove("Sophie", 20.25, 62.50)
fnCutsceneMove("Sophie", 20.25, 61.50)
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(60)
fnCutsceneBlocker()
fnCutsceneMove("Sophie", 20.25, 62.50)
fnCutsceneMove("Sophie", 19.25, 62.50)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(60)
fnCutsceneBlocker()
fnCutsceneMove("Sophie", 18.25, 62.50)
fnCutsceneMove("Sophie", 18.25, 61.50)
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(60)
fnCutsceneBlocker()

--SCRAPRAT PRIMED AND READY!
fnCutsceneFace("Christine", 1, 0)
fnCutsceneSetFrame("Scraprat", "Explode00")
fnCutsceneTeleport("Scraprat", 18.97, 61.50 - (23.0 / 16.0))
fnCutsceneWait(90)
fnCutsceneBlocker()

--Golem runs away. Sophie looks confused.
fnCutsceneMove("GolemSlaveA", 19.25, 52.50, 2.50)
fnCutsceneFace("Sophie", 1, -1)
fnCutsceneBlocker()
fnCutsceneWait(20)
fnCutsceneBlocker()

--Christine runs in to save the day.
fnCutsceneMove("Christine", 14.25, 61.50, 3.50)
fnCutsceneMove("Christine", 17.25, 61.50, 3.50)
fnCutsceneBlocker()
fnCutsceneMoveFace("Christine", 12.25, 61.50, 1, 0, 1.50)
fnCutsceneMoveFace("Sophie",    13.25, 61.50, 1, 0, 1.50)
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneBlocker()

--Scraprat explodes.
local ciTicksPerFrame = 6
fnCutsceneSetFrame("Scraprat", "Explode01")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode02")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode03")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode04")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode05")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode06")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode07")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode08")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode09")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode10")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()
fnCutsceneSetFrame("Scraprat", "Explode11")
fnCutsceneWait(ciTicksPerFrame)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(180)
fnCutsceneBlocker()

--Sophie and Christine walk up to the wrecked rat.
fnCutsceneMove("Christine",   17.25, 61.50, 0.70)
fnCutsceneMove("Sophie",      18.25, 61.50, 0.70)
fnCutsceneMove("GolemSlaveA", 19.25, 60.50, 0.70)
fnCutsceneBlocker()
fnCutsceneWait(60)
fnCutsceneBlocker()

--Christine and Sophie laugh. Fade out.

--They look towards the camera and laugh.
fnCutsceneFace("Christine", 0, 1)
fnCutsceneFace("Sophie", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Repeat this 8 times before fading out. Alternate.
for i = 1, 8, 1 do
	fnCutsceneSetFrame("Christine", "Laugh0")
	fnCutsceneSetFrame("Sophie", "Laugh1")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "Laugh1")
	fnCutsceneSetFrame("Sophie", "Laugh0")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
end

--Start fading out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--Continue laughing.
for i = 1, 3, 1 do
	fnCutsceneSetFrame("Christine", "Laugh0")
	fnCutsceneSetFrame("Sophie", "Laugh1")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "Laugh1")
	fnCutsceneSetFrame("Sophie", "Laugh0")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
end

--Wait a while.
fnCutsceneWait(180)
fnCutsceneBlocker()

--Clean up.
fnCutsceneTeleport("GolemSlaveA", -100.25, -100.50)
fnCutsceneTeleport("Scraprat", -100.25, -100.50)
fnCutsceneSetFrame("Christine", "Null")
fnCutsceneSetFrame("Sophie", "Null")

--[ ========================================== Scene 6 ========================================== ]
--Sophie and Christine hold hands in the theater.
fnCutsceneTeleport("Christine", 62.25, 24.50)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneTeleport("Sophie", 62.25, 24.50)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneBlocker()

--Turn the walls off.
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "CutsceneWallsHi", true) ]])
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "CutsceneWalls", true) ]])

--Focus the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 10000.0)
	CameraEvent_SetProperty("Focus Position", (58.75 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

--Christine and Sophie move to the theater entrance.
fnCutsceneMove("Christine", 62.25, 23.50)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 62.25, 20.50)
fnCutsceneMove("Sophie",    62.25, 21.50)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 61.25, 20.50)
fnCutsceneMove("Sophie",    62.25, 20.50)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 58.25, 20.50)
fnCutsceneMove("Sophie",    59.25, 20.50)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 58.25, 19.50)
fnCutsceneMove("Sophie",    59.25, 19.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()

--They go into the theater.
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Open Door", "DoorTA") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 58.25, 18.50)
fnCutsceneMove("Sophie", 58.25, 19.50)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 58.25, 16.50)
fnCutsceneMove("Sophie", 58.25, 18.50)
fnCutsceneBlocker()
fnCutsceneMove("Sophie", 58.25, 16.50)
fnCutsceneMove("Christine", 57.75, 16.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneMove("Sophie", 58.75, 16.50)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Close Door", "DoorTA") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()

--Lights shut off.
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "CutsceneWallsHi", false) ]])
fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "CutsceneWalls", false) ]])
fnCutsceneWait(65)
fnCutsceneBlocker()

--They watch the movie for a while. Move Christine out and use Sophie's special frames.
fnCutsceneSetFrame("Sophie", "HoldHands0")
fnCutsceneTeleport("Christine", -100.25, -100.50)
fnCutsceneTeleport("Sophie", 57.75, 16.50)
fnCutsceneWait(65)
fnCutsceneBlocker()

--Animation.
fnCutsceneSetFrame("Sophie", "HoldHands1")
fnCutsceneWait(125)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "HoldHands0")
fnCutsceneWait(185)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "HoldHands2")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "HoldHands3")
fnCutsceneWait(75)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "HoldHands4")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "HoldHands5")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "HoldHands6")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "HoldHands7")
fnCutsceneWait(185)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "HoldHands8")
fnCutsceneWait(75)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "HoldHands7")
fnCutsceneWait(185)
fnCutsceneBlocker()

--Fade out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(300)
fnCutsceneBlocker()

--[ ============================================ End ============================================ ]
--Go to the next cutscene in Christine's quarters.
gbDontCancelMusic = true
fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:9.0x10.0x0") ]])
fnCutsceneBlocker()
