--[Scene Post-Transition]
--After transition, play a random dialogue line.

--[Variables]
local bIs55Present = fnIsCharacterPresent("55")

--[Overlay]
--Set the overlay to fullblack. Fade in slowly.
AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Wounded]
--Move 55 one pixel up so she's behind Christine.
if(bIs55Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", 16.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", 0.0, 16.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

--[Fade In]
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Change Christine to the "Downed" frames.
Cutscene_CreateEvent("Change Christine Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
    ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

--If 55 is present, set her to downed as well.
if(bIs55Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(240)
fnCutsceneBlocker()

--[Crouch]
--Christine gets up.
Cutscene_CreateEvent("Change Christine Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--55, if present, gets up.
if(bIs55Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Normal]
--Christine stands up.
Cutscene_CreateEvent("Change Christine Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--55, if present, gets up.
if(bIs55Present) then
	
	--Facing down.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Face", 0.0, 1.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Face", -1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Face", -1.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Christine")
		ActorEvent_SetProperty("Face", 1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Face", -1.0, -1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()

end

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

--[Talking]
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Christine talks to herself.
if(bIs55Present == false) then
	
    --Common.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    local iDialogueRoll = LM_GetRandomNumber(0, 3)
    
    --If Christine is still Chris, change the name.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    if(iHasGolemForm == 0.0) then
        if(iDialogueRoll == 0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral] I've had worse...") ]])
        elseif(iDialogueRoll == 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral] Nothing I haven't dealt with in grammar school...") ]])
        elseif(iDialogueRoll == 2) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral] These punks have nothing on those kids from Sheffield...") ]])
        elseif(iDialogueRoll == 3) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris:[E|Neutral] Just another day at the office...[SOFTBLOCK] heh...[SOFTBLOCK] ow...") ]])
        end

    --Christine.
    else
        if(iDialogueRoll == 0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I've had worse...") ]])
        elseif(iDialogueRoll == 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Nothing I haven't dealt with in grammar school...") ]])
        elseif(iDialogueRoll == 2) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] These punks have nothing on those kids from Sheffield...") ]])
        elseif(iDialogueRoll == 3) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Just another day at the office...[SOFTBLOCK] heh...[SOFTBLOCK] ow...") ]])

        end
    end

--55 and Christine.
else
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 5)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I've had worse...") ]])

	elseif(iDialogueRoll == 1) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Reminds me of a good old football riot back home...") ]])

	elseif(iDialogueRoll == 2) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hey 55, did you analyze that combat data?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Affirmative.[SOFTBLOCK] I recorded your face being struck repeatedly.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] How much do I have to pay you to delete that recording?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Nobody is that rich, Christine.") ]])

	elseif(iDialogueRoll == 3) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I think I'm getting tougher...[SOFTBLOCK] right?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Your body seems to have been designed explicitly to absorb impacts.[SOFTBLOCK] Keep it up.") ]])

	elseif(iDialogueRoll == 4) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Would you like to go over a list of all the mistakes we made in that battle?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I think it can wait until after we get revenge...") ]])

	elseif(iDialogueRoll == 5) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I appreciate your runestone saving us from certain doom.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It wasn't certain...[SOFTBLOCK] I had them right where I wanted them the whole time...[SOFTBLOCK] Ow, hurts to talk, ow ow...") ]])

	end

end

--Common.
if(bIs55Present == true) then
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", 0.0, -16.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", -16.0, 0.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--If 55 is present, walk her to Christine and fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
