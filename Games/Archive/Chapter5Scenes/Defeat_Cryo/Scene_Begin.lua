--[Defeat In Cryogenics]
--Just goes back to the last save point.
AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
AL_BeginTransitionTo("LASTSAVE", fnResolvePath() .. "Scene_PostTransition.lua")

--[Combat]
--Restore party to full HP in case the player doesn't want to use the save point. This saves time, and is a quality-of-life feature.
AC_SetProperty("Restore Party")