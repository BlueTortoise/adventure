--[Scene Post-Transition]
--After transition, play a random dialogue line.

--[Variables]
local bIs55Present = fnIsCharacterPresent("55")

--[Overlay]
--Set the overlay to fullblack. Fade in slowly.
AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Wounded]
--Move 55 one pixel up so she's behind Christine.
if(bIs55Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", 16.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", 0.0, 16.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

--[Fade In]
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Change Christine to the "Downed" frames.
Cutscene_CreateEvent("Change Christine Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
    ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

--If 55 is present, set her to downed as well.
if(bIs55Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(240)
fnCutsceneBlocker()

--[Crouch]
--Christine gets up.
Cutscene_CreateEvent("Change Christine Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--55, if present, gets up.
if(bIs55Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Normal]
--Christine stands up.
Cutscene_CreateEvent("Change Christine Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--55, if present, gets up.
if(bIs55Present) then
	
	--Facing down.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Face", 0.0, 1.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Face", -1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Face", -1.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Christine")
		ActorEvent_SetProperty("Face", 1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Face", -1.0, -1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()

end

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

--[Talking]
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Christine talks to herself.
if(bIs55Present == false) then
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 2)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I hope nobody saw that.") ]])

	elseif(iDialogueRoll == 1) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Spot of trouble and nothing more.") ]])

	elseif(iDialogueRoll == 2) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] And now we know who won't be invited over for a chat...") ]])
	end

--55 and Christine.
else
	
	--Setup.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 2)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] I hope nobody saw that.") ]])

	elseif(iDialogueRoll == 1) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Spot of trouble and nothing more.") ]])

	elseif(iDialogueRoll == 2) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] And now we know who won't be invited over for a chat...") ]])
	end

end

--Common.
if(bIs55Present == true) then
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", 0.0, -16.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", -16.0, 0.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--If 55 is present, walk her to Christine and fold the party.
	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
