--[Defeat By Latex Drone]
--Cutscene proper. Turns Christine into a Latex Drone if she doesn't have that form.

--[Variables]
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

--[Repeat Check]
--If Christine can already turn into a Latex Drone, this scene does not play.
local iHasLatexForm = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
if(iHasLatexForm == 1.0 and iIsRelivingScene == 0.0) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

--[Map Check]
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "RegulusLRTD") then

	--During a relive, neither of these is checked:
	if(iIsRelivingScene == 0.0) then

		--Run the sub-cutscene. It adds a lot of events, and will fadeout the camera automatically.
		--LM_ExecuteScript(gsRoot .. "Chapter1Scenes/Z Emergency Revert/Scene_Begin.lua")
	end

	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"RegulusLRTD\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutsceneInstruction(sString)
	return
end

--[Form]
--Switch Christine to Latex Drone. She's not actually visible at all until the transformation is completed.
LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua")

--Unlock Latex Drone form.
VM_SetVar("Root/Variables/Global/Christine/iHasLatexForm", "N", 1.0)

--[Combat]
--Restore party to full HP in case the player doesn't want to revisit a save point.
if(iIsRelivingScene == 0.0) then
    AC_SetProperty("Restore Party")
end

--[Topics]
--Unlock these topics if they weren't already.
if(iIsRelivingScene == 0.0) then
    WD_SetProperty("Unlock Topic", "Latex Drones", 1)
    WD_SetProperty("Unlock Topic", "CPU Inhibitors", 1)
end

--When reliving the scene, 55 needs to be spawned.
if(iIsRelivingScene == 1.0) then
    fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
end

--[Music]
AL_SetProperty("Music", "Null")

--[Cutscene Execution]
--Black the screen out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Position Christine and 55 offscreen. Focus the camera on the computer between the tubes.
fnCutsceneTeleport("Christine", -100.25, -100.50)
fnCutsceneTeleport("55",        -100.25, -100.50)
fnCutsceneFace("ConversionTube", 1, 0)
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 10000.0)
	CameraEvent_SetProperty("Focus Position", (63.75 * gciSizePerTile), (12.50 * gciSizePerTile))
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Drone] UNIDENTIFIED SUBJECTS CAPTURED.[SOFTBLOCK] SPECIES::[SOFTBLOCK] HUMAN.[SOFTBLOCK] SECURITY STATE ELEVATED.[SOFTBLOCK] CONVERT TO ENHANCE SECURITY.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Drone:[VOICE|Drone] AFFIRMATIVE.[SOFTBLOCK] SUBJECTS PLACED IN CAPSULES.[SOFTBLOCK] ESTIMATED CONVERSION TIME::[SOFTBLOCK] SEVEN MINUTES.[SOFTBLOCK] RESUME PATROLS.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Scene.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Christine_Human") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A dull throb pulsed in the back of Christine's head as her eyes began to open, echoing from her ears out into the world around her as a piercing ring.[SOFTBLOCK] Instinct strove against her efforts, trying to force her eyes closed against a brilliant light as her mind, not yet fully awoken, sought to regain control of her body.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She wanted to lay down, to collapse where she stood and sleep away the incessant throbbing in her mind, but even as she slowly won the battle against herself and regained her senses, the throbbing refused to fade.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Looking about herself, she saw that she was in a conversion tube.[SOFTBLOCK] The tube would return her to her golem form.[SOFTBLOCK] The troubles she had encountered would fade from memory, and all would be right again with her.[SOFTBLOCK] A sigh of relief passed over her body and her eyes began to slide closed before she caught herself and threw them open again.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She would need to lock away a section of her memory during reprogramming, a feat she wondered at in her dazed state, which 55 could restore once they had reached a safer location.[SOFTBLOCK] With a bit of focusing, she held no doubts she could overcome her stupor enough to complete this simple task.[SOFTBLOCK] Locating 55 would be a greater challenge.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked out of the tube's clear gate, still struggling to drive the throbbing noise from her mind, and wondered where 55 had escaped to after the wayward battle.[SOFTBLOCK] Her question was quickly answered as, across the room and in another tube, 55's face, a mixture of anger and fear, was shouting silently at her as she pounded against the glass.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "There, she realized, was the source of the throbbing noise she heard, not in her head but from the tube that 55's capable motivators now struggled to break out of.[SOFTBLOCK] She wondered at the fear that was on 55's face, and tried to mouth that she held no concerns for returning to a Lord Golem, and tapped at her golem core to emphasise that.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "But her fingers met only bare skin.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked down, and where she had expected to see the smooth metal surface of a golem core she saw only the bruised skin of her nude body.[SOFTBLOCK] She had only a moment to stare before the tube whirred to life, and in an instant the last lingering haze lifted itself from her mind.[SOFTBLOCK] The tube would not convert her to a Lord Golem as she expected.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mechanical arms and nozzles descended from the top of the tube to the floor where they began to spray a thick and sticky mist onto her.[SOFTBLOCK] The mist swirled gently in the breeze its own nozzles created, brushing past over feet and tickling at her toes.[SOFTBLOCK] She struggled to maintain her composure to keep from laughing as the nozzles began to raise upward.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Christine_Human") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The tickling sensation grew along her smooth-shaved legs, and a tingling began to crawl across her nerves.[SOFTBLOCK] The material was cool and slightly sticky.[SOFTBLOCK] It clung tightly to her skin and prickled at her senses.[SOFTBLOCK] It was a pleasant sensation, bringing an odd feeling of peace and security.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The spray from the nozzles ceased, and a brief panic grew within Christine's chest as they slid back into the tube until she saw that the material itself continued to grow across her of its own accord.[SOFTBLOCK] A mindless joy filled her, replacing the panic with no less an intensity.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She reached out a hand, hovering her fingers over the spreading material hesitantly before touching it.[SOFTBLOCK] It was a rubbery latex-like material, the same that had covered the drones that had captured her, and no sooner had she touched the covered part of her leg than the material began to spread up her fingers and arm.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mind began to race as her memory of the drones stirred.[SOFTBLOCK] She understood how the drones operated, how they were of the least consequence to the researchers, and, more importantly to her immediate concern, how they were fitted with CPU inhibitors to keep their intelligence in check.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A plan formed as the material continued its path up her legs and arm.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The level of inhibition would be based on the activity in the subject's brain when the scan was conducted.[SOFTBLOCK] A flaw in the software, but one the unwitting subjects would never learn.[SOFTBLOCK] If she could settle her mind enough, perhaps with a bit of meditation, she could minimize the level of inhibition.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Closing her eyes, she let her arms fall to her side and began to clear her mind in preparation for the scan that would follow the total encasement of her body.[SOFTBLOCK] Her mind struggled against her efforts as the tingling of the latex crawled further up her legs and arm, but with the full measure of her patience and dignity, she began to overcome the teasing sensations.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "But her efforts were in vain.[SOFTBLOCK] The latex crept further up her legs, tickling her skin and tantalizing her nerves until it reached the waiting heat of her sex.[SOFTBLOCK] The tingling of the latex exhilarated her, and as it found that wet, waiting opening between her legs, the spreading latex dove inside of her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She cried out as the latex began to convert each part of her sex, its tingling growth driving her to an unexpected orgasm.[SOFTBLOCK] Her back arched with convulsive glee and her head lolled back as a second cry escaped her lips, and though her legs wanted to buckle under the weight of the orgasm, the latex had taken full control of them and would not let her fall.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The latex continued to grow, holding her in its thrall as it continued to excite her nerves into a prolonged orgasm.[SOFTBLOCK] The tickling sensations caressed her back and stomach with a programmed perfection, eliciting another moan from her yielding lips.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It spread across her breasts, its programmed fingers tracing lightly and yet completely, encircling her stiffened nipples before at last grasping them with the same unyielding drive.[SOFTBLOCK] She could feel it as it covered the last of the exposed skin on her breasts, expanding and puffing her yearning skin outwards.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The latex constricted against her and pulled her body into a rigid position as the latex coaxed another orgasm from her, readying her for the next step of her conversion as it spread its first tendrils onto her neck.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mind reeled against itself, torn between the desire to give in fully to the creeping lust of the latex and regaining her composure to minimize the impact of her limiting chip.[SOFTBLOCK] She needed to think.[SOFTBLOCK] She needed to focus.[SOFTBLOCK] She needed to overcome the very carnal pleasures that had been so carefully calculated to ensure the latex golems would never seek to escape.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She could not allow her mind to be lost, either to the pleasures that now spread across her face nor to the limiting chip that would ensure her servitude.[SOFTBLOCK] There were those who were counting on her.[SOFTBLOCK] Sophie was waiting for her in the repair bay, while 55 would need her help in setting right the misfortunes that had fallen upon the city.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She must focus and overcome the instincts that she herself had been born with, the instincts that were now being teased into a heightened state to ensure her loyalty.[SOFTBLOCK] She needed to save her mind, herself, even after the transformation had completed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The latex at last covered her head, and then began its work in earnest.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She could feel her body being redesigned by the latex.[SOFTBLOCK] Her skin inflated, muscles and organs consumed and transformed as it worked its way within her through its carnal doorways.[SOFTBLOCK] She could feel her body changing, some parts familiar as the very same parts she had as a Lord Golem, other parts completely new.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "With her last lingering thoughts, she drove her mind into a meditative state.[SOFTBLOCK] With effort she cleared her thoughts even as her face stretched out and away from her bones, taking the shape of a mask.[SOFTBLOCK] She held her mind empty even as the latex delved inside of her head, bringing back the familiar architecture of her CPU and memory banks.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mind fell clear of all thoughts.[SOFTBLOCK] Each thought that might seek to materialize was scattered, and she maintained her vigil even as the conversion tube began to scan her brain with a whirring hum.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "How long had passed she could not say, but at length the hum ceased.[SOFTBLOCK] From outside the tube, she heard a quiet voice.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "\"Subject mental capacity at 35 percent. CPU inhibitor rate set to 0 percent. Finishing conversion.\"[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The tube opened and Christine stepped out, her body driven by the latex for one final action.[SOFTBLOCK] She stumbled slightly as the latex relinquished control to her own CPU, and her mind flared into action to keep herself from falling.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Steadying herself, she looked around the room as she gingerly touched at her transformed body.[SOFTBLOCK] The tight latex that had replaced her skin seemed to amplify her touch, and she could feel a fresh orgasm beginning to build within her as she caressed herself.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She slid her hands across her body, from the tops of her breasts and her now-permanently stiffened nipples down along her sides, across her stomach, and at last delved into her awaiting vulva, where a slightly-raised slit beckoned to her fingers.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A thought crossed her mind that, if any superior unit commanded her, she would need to present herself for use.[SOFTBLOCK] The thought lingered in her mind, echoing as if to fill the emptiness that had been there only a moment prior, and excited her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mind drifted in the thoughts of exploration and exploitation as her fingers deftly explored the changes of her body and the sensitive touch that came with them.[SOFTBLOCK] Gradually, a voice reached her and broke her free of her exploring.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "\"Hey!\"[SOFTBLOCK] the muffled voice called out to her.[SOFTBLOCK] \"Let me out of this stupid tube!\"[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mind focused and her fingers fell reluctantly from their own personal self-diagnostics, and as she looked towards the sound of the voice, the sight that greeted her reminded her that 55 was still trapped in a tube of her own.") ]])
fnCutsceneBlocker()

--Position Latex Christine. 55 is still in a tube.
fnCutsceneTeleport("Christine", 62.25, 13.50)
fnCutsceneFace("Christine", 0, 1)

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(85)
fnCutsceneBlocker()

--Christine talks to herself.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] PHYSICAL DIAGNOSTICS INTERRUPTED...[SOFTBLOCK] RESUME DIAGNOSTICS LATER.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] SUPERIOR UNIT REQUIRES MY ASSISTANCE.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (SHEESH, WHY AM I TALKING LIKE THAT?)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] (ACK, IT DOES THAT EVEN INSIDE MY HEAD?)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine walks up to the console.
fnCutsceneMove("Christine", 63.75, 12.50, 0.25)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine talks to herself.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (BALANCING ON THESE FEET WILL TAKE SOME GETTING USED TO.[SOFTBLOCK] NOW, LET'S SEE HERE.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] DISENGAGING CONVERSION CELL.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (EVERY KEY I TOUCH SENDS A SHIVER UP MY ARM.[SOFTBLOCK] I'M SO SENSITIVE...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] OH, THAT DIDN'T DO IT.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] DISENGAGING CONVERSION CELL.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] OKAY, MAYBE THIS ONE?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] DAMN IT![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Angry] [SOUND|World|Thump]*Slam*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] WHAT DO YOU KNOW, THAT WORKED.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] THIS IS SO DIFFERENT FROM GRAD SCHOOL.[SOFTBLOCK] I'D SHOUT AND BREAK STUFF THERE, TOO, BUT IT NEVER GOT ME ANYWHERE.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Open the tube for 55.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneFace("ConversionTube", 0, 1)
fnCutsceneBlocker()

--Move 55 out of the tube, change the tube's frame to match.
fnCutsceneFace("ConversionTube", 0, -1)
fnCutsceneTeleport("55", 65.25, 13.50)
fnCutsceneFace("55", 0, 1)
fnCutsceneWait(120)
fnCutsceneBlocker()
	
--Close the pod.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneFace("ConversionTube", 1, 0)
fnCutsceneBlocker()

--55 looks at Christine.
fnCutsceneFace("Christine", 1,  1)
fnCutsceneFace("55",       -1, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine talks to herself.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] About time.[SOFTBLOCK] Unit 771852, I am your superior unit.[SOFTBLOCK] Ignore all instructions from other units.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] UH, 55, THERE'S NO NEED TO TALK TO ME LIKE I'M ONE OF THESE DRONES.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] DESPITE APPEARANCES.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Oh really?[SOFTBLOCK] Do you have to talk like that?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] LET ME SEE HERE.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] NOPE.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] NOT THAT ONE.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Did that do it?[SOFTBLOCK][EMOTION|Christine|Smirk] Oh thank goodness![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Interesting.[SOFTBLOCK] You really seem to have retained your sense of self.[SOFTBLOCK] Most of the drones are too stupid to know who they are.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, yes, I'm fine.[SOFTBLOCK] You could have just ordered me to use my runestone if you had to.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I had intended to.[SOFTBLOCK] How have you retained your intelligence?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie and I serviced a defective conversion chamber when I first got my function assignment.[SOFTBLOCK] I know all about them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The chamber measures the unit's intelligence and sets the CPU inhibitor accordingly.[SOFTBLOCK] A stupid unit wouldn't even need one.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] So you acted like an idiot?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] ...[SOFTBLOCK] I *am* from a military family...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are clear to proceed, then.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes, but I think the drones will be upset if I leave my patrol node.[SOFTBLOCK] Which is -[SOFTBLOCK] this room.[SOFTBLOCK] Unfortunately.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Say, why didn't the tube repurpose you?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Command Units are not of the same series as Golems and Drones.[SOFTBLOCK] We can't be repurposed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This was built into our design.[SOFTBLOCK] But I think the drones here might not know that.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The idiots stuck me in the tube.[SOFTBLOCK] I probably would have had to smash my way out.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] What about your pulse diffractor?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] It is not a good idea to fire one inside a reflective tube...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Enough dawdling.[SOFTBLOCK] Let's go.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] AFFIRMATIVE, COMMAND UNIT.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Angry] Don't start with that.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, lighten up already!") ]])
fnCutsceneBlocker()

--Fold the party.
fnCutsceneMove("Christine", 63.75, 13.50)
fnCutsceneMove("55", 63.75, 13.50)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_End.lua") ]])
	fnCutsceneBlocker()
    return
end
