--[Gala Assault Briefing]
--This cutscene plays once Christine begins undertaking the assault on the Gala. It has a lot of variations based
-- on which sidequests the player has completed. Partial completion is not factored in.
--Regardless the first part of the briefing always plays out the same.

--Spawn 2855.
fnSpecialCharacter("55", "Doll", 9, 6, gci_Face_North, false, nil)

--Christine spawn position.
fnCutsceneTeleport("Christine", 3.25, 17.50)

--Black the screen out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(245)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] It should be right around here...") ]])
fnCutsceneBlocker()

--Switch Christine to Golem.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "/FormHandlers/Christine/Form_Golem.lua") ]])
fnCutsceneWait(125)
fnCutsceneBlocker()

--Begin fading in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneMove("Christine", 9.25, 17.50)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 9.25, 13.50)
fnCutsceneMove("Christine", 5.25, 13.50)
fnCutsceneMove("Christine", 5.25, 9.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("55", -1, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So these are your quarters?[SOFTBLOCK] Not exactly cozy, but I suppose it has its own charm if you ignore the stench.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is one of several hideouts I make use of.[SOFTBLOCK] This location is desireable due to an abundance of raw materials.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] It's right below Sector 42.[SOFTBLOCK] The landfill sector.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As I said, an abundance of raw materials.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well as long as the smell doesn't get to you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Simply set your olfactory sensors to standby if it bothers you so.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] That's not the point 55...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Because if the smell seeps into your clothes, security units could know you're situated near Sector 42![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Impressive, Unit 771852.[SOFTBLOCK] Fortunately my clothing can expel all odor causing materials when given a slight electrical charge.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] But you are thinking laterally.[SOFTBLOCK] Good.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I'm not here for praise, I'm here for the meeting you called.[SOFTBLOCK] Where is everyone?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] You are punctual.[SOFTBLOCK] Evidently your compatriots are not.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So how have you been?[SOFTBLOCK] We've barely spoken except for business...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Suspend this line of inquiry, Unit 771852.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sheesh...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, Sophie finished my dress but she still won't let me see it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's probably too late to get one made for you, but I'm sure we could get one off the rack that'd fit you.[SOFTBLOCK] Maybe Sophie could do some last-minute alterations?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Neither am I in need of a dress, nor your aid.[SOFTBLOCK] I am perfectly capable of performing such tasks.[SOFTBLOCK] I constructed this wardrobe myself.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Did you make the skirt so short on purpose, or for lack of materials?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] I did not want the fabric to inhibit movement of my legs in combat.[SOFTBLOCK] My hardened chassis does not need further protection, even from synthweave.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Well you look great![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Why are you so focused on appearances?[SOFTBLOCK] How something looks does not matter, yet you bring it up at every opportunity.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Looking pretty is fun, isn't it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I don't actually care if anyone really thinks I do or don't, I just like putting the effort in and the feeling it gives me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Fabulousosity is its own reward![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If such frivolities improve your emotional state then I see no reason to argue against them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ...[SOFTBLOCK] Since all such arguments are therefore encapsulated within the argument against emotions themselves.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yeah yeah, we all get it.[SOFTBLOCK] You're the big bad machine girl.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] C'mon, 55.[SOFTBLOCK] Just laugh once for me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] I am capable of laughing, but will withold it until I am in the presence of someone funny.[SOFTBLOCK] I am currently not.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] That's my girl![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Attention.[SOFTBLOCK] My perimeter sensor just went off.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh good, the others must be here.[SOFTBLOCK] I'm not sure how much longer I could keep you entertained.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Variables.
local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
local iCompletedEquinox = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")

--Debug.
if(false) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationQ.lua")

--[None]
--The case where NO sidequests have been complete:
elseif(iFinished198 == 0.0 and iSXUpgradeQuest < 3.0 and iCompletedSerenity == 0.0 and iCompletedEquinox == 0.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationA.lua")

--[Singles]
--Case where Equinox has been completed:
elseif(iFinished198 == 0.0 and iSXUpgradeQuest < 3.0 and iCompletedSerenity == 0.0 and iCompletedEquinox == 1.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationB.lua")

--Case where only Tellurium Mines has been completed.
elseif(iFinished198 == 0.0 and iSXUpgradeQuest >= 3.0 and iCompletedSerenity == 0.0 and iCompletedEquinox == 0.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationC.lua")

--Case where only Serenity Crater has been completed.
elseif(iFinished198 == 0.0 and iSXUpgradeQuest < 3.0 and iCompletedSerenity == 1.0 and iCompletedEquinox == 0.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationD.lua")

--Case where only Electrosprites has been completed.
elseif(iFinished198 == 1.0 and iSXUpgradeQuest < 3.0 and iCompletedSerenity == 0.0 and iCompletedEquinox == 0.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationE.lua")

--[Twos]
--Case where Equinox + Tellurium Mines has been completed.
elseif(iFinished198 == 0.0 and iSXUpgradeQuest >= 3.0 and iCompletedSerenity == 0.0 and iCompletedEquinox == 1.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationF.lua")

--Equinox + Serenity:
elseif(iFinished198 == 0.0 and iSXUpgradeQuest < 3.0 and iCompletedSerenity == 1.0 and iCompletedEquinox == 1.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationG.lua")

--Equinox + Electrosprites:
elseif(iFinished198 == 1.0 and iSXUpgradeQuest < 3.0 and iCompletedSerenity == 0.0 and iCompletedEquinox == 1.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationH.lua")

--Tellurium Mines + Serenity:
elseif(iFinished198 == 0.0 and iSXUpgradeQuest >= 3.0 and iCompletedSerenity == 1.0 and iCompletedEquinox == 0.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationI.lua")

--Tellurium Mines + Electrosprites:
elseif(iFinished198 == 1.0 and iSXUpgradeQuest >= 3.0 and iCompletedSerenity == 0.0 and iCompletedEquinox == 0.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationJ.lua")

--Serenity + Electrosprites:
elseif(iFinished198 == 1.0 and iSXUpgradeQuest < 3.0 and iCompletedSerenity == 1.0 and iCompletedEquinox == 0.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationK.lua")

--[Threes]
--Equinox + Mines + Serenity
elseif(iFinished198 == 0.0 and iSXUpgradeQuest >= 3.0 and iCompletedSerenity == 1.0 and iCompletedEquinox == 1.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationL.lua")

--Equinox + Mines + Electrosprites
elseif(iFinished198 == 1.0 and iSXUpgradeQuest >= 3.0 and iCompletedSerenity == 0.0 and iCompletedEquinox == 1.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationM.lua")

--Equinox + Serenity + Electrosprites
elseif(iFinished198 == 1.0 and iSXUpgradeQuest < 3.0 and iCompletedSerenity == 1.0 and iCompletedEquinox == 1.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationQ.lua")

--Mines + Serenity + Electrosprites
elseif(iFinished198 == 1.0 and iSXUpgradeQuest >= 3.0 and iCompletedSerenity == 1.0 and iCompletedEquinox == 0.0) then
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationO.lua")

--[All Four]
else
    LM_ExecuteScript(gsRoot .. "Chapter5Scenes/Gala Assault Briefing/VariationP.lua")
end

--Once the interim scene is completed, transition to the next scene.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(60)
fnCutsceneBlocker()

--Transition to Christine's quarters.
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N", 1.0)

--Next scene.
fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.5x9.0x0") ]])
fnCutsceneBlocker()