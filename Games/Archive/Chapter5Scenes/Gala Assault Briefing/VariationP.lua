--[Gala Assault Variation P]
--Everyone is here!
TA_Create("300910")
    TA_SetProperty("Position", -100, -100)
    TA_SetProperty("Facing", gci_Face_South)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
DL_PopActiveObject()
TA_Create("GoldLeader")
    TA_SetProperty("Position", 16, 18)
    TA_SetProperty("Facing", gci_Face_West)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/GolemSlaveP/", false)
DL_PopActiveObject()
TA_Create("JX-101")
    TA_SetProperty("Position", 3, 18)
    TA_SetProperty("Facing", gci_Face_South)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/JX101/", false)
DL_PopActiveObject()
TA_Create("Psue")
    TA_SetProperty("Position", 16, 17)
    TA_SetProperty("Facing", gci_Face_West)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/Electrosprite/", false)
DL_PopActiveObject()

--Move Christine and 55.
fnCutsceneMove("Christine", 6.25, 7.50)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneMove("55", 9.25, 7.50)
fnCutsceneMove("55", 7.25, 7.50)
fnCutsceneFace("55", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--JX-101 and 300910 enter.
fnCutsceneTeleport("JX-101", 6.25, 6.50)
fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("JX-101", 7.25, 6.50)
fnCutsceneFace("JX-101", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneTeleport("300910", 6.25, 6.50)
fnCutsceneFace("300910", 0, 1)
fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Doll", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "JX-101", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Figures you'd be here, waiting for us.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Unit 300910![SOFTBLOCK] It's been too long![SOFTBLOCK] How are my Darkmatter friends at the observatory?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Still a lot of work to do, but we've gotten our instruments back online.[SOFTBLOCK] The Darkmatters have basically made a little home for themselves in our storage room.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Not like we could stop them.[SOFTBLOCK] They still won't let us help with the creatures on the shelf, though.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And JX-101![SOFTBLOCK] We've been so busy we haven't had time to catch up![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] But it's the good kind of busy.[SOFTBLOCK] We can get caught up after the operation.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Though the people of Sprocket City do send their regards.[SOFTBLOCK] Seems your absence has built up longing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Do you plan to stay in Sprocket City once we've liberated Regulus City?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I don't think so, despite all its charm.[SOFTBLOCK] It's still a rustbucket in the mines, and there are still many creatures down there.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Considering we face tremendous losses when the shooting starts, it may yet serve as a fallback point.[SOFTBLOCK] So we'll squeeze every last drop of usefulness out of it, like good Steam Droids do.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] But enough visiting.[SOFTBLOCK] We have a meeting to conduct, don't we?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We still have some attendees to go.[SOFTBLOCK] Though considering the proximity alarm they just set off...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("55", 0, 1)
fnCutsceneFace("Christine", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("GoldLeader", 9.25, 18.50)
fnCutsceneMove("GoldLeader", 9.25, 13.50)
fnCutsceneMove("GoldLeader", 5.25, 13.50)
fnCutsceneMove("GoldLeader", 5.25, 9.50)
fnCutsceneMove("Psue", 9.25, 18.50)
fnCutsceneMove("Psue", 9.25, 13.50)
fnCutsceneMove("Psue", 5.25, 13.50)
fnCutsceneMove("Psue", 5.25, 10.50)
fnCutsceneMove("Psue", 6.25, 10.50)
fnCutsceneMove("Psue", 6.25,  9.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "GolemB", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Electrosprite", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Psue![SOFTBLOCK] You're here too?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] Damn right, girl![SOFTBLOCK] I found this heapin-helpin of pretty metal when I was cruising through the circuits on the way here, and I just had to manifest![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] Gold Leader, reporting in...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] Psue, you're shocking me![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] Sorry, sorry.[SOFTBLOCK] Just hugging![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] ...[SOFTBLOCK] I didn't say stop...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Gold Leader, I am Blue Leader.[SOFTBLOCK] The Steam Droid here is Red Leader, the other Command Unit is Teal Leader...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And this Lord Unit is Pink Leader.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Yeah about that.[SOFTBLOCK] How come I didn't get to pick my codename?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] Because you would have selected something frivolous, such as Scintilliting Leader.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] GOODNAMEGOODNAME BAYBEE![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] At least someone gets me![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Leader codes are to be single-syllable and to have nothing obvious to do with their subject.[SOFTBLOCK] They are codes for a reason.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Fine.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] Well, it's not like the codes matter too much.[SOFTBLOCK] I already know who everybody is.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] JX-101, famous leader of Fist of the Future.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Fist of Tomorrow, actually.[SOFTBLOCK] We changed our name.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] Unit 300910, lead researcher of Serenity Crater Observatory.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I didn't realize I was famous.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] A Command Unit who does right by their Slave Units get a reputation.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] And then there's former Head of Security, Unit 2855.[SOFTBLOCK] Famous for...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] ...[SOFTBLOCK] Did I say something wrong?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Yeah.[SOFTBLOCK] 'Famous'.[SOFTBLOCK] I don't think she likes the idea.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] ...[SOFTBLOCK] I am.[SOFTBLOCK] Different.[SOFTBLOCK] Now.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] She really is.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] And Unit 771852.[SOFTBLOCK] Hero of Equinox.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Hero?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] There were no heroes there.[SOFTBLOCK] Corpses...[SOFTBLOCK] and undertakers...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] But you did rescue Unit 599239, who was trapped there.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] She said that a unit with a funny accent and a spear saved her, flanked by a scary looking Command Unit.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] But as I said, it's not a heroic thing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 599239 shall henceforth be considered a valuable asset.[SOFTBLOCK] We should locate her when we can.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Getting her to publically testify about Equinox will be a potent recruiting tool for the coming battles.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Thank you for the information, Gold Leader.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's great to see all of you again.[SOFTBLOCK] Especially you, Psue![SOFTBLOCK] How's learning to speak going?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] BETTER THAN GOOD, SUCKAZZZZZ![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] But really, I'm the only Electrosprite who wasn't so good at it.[SOFTBLOCK] Whatever you did in the wiring made my sistaz super smart![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] I've been practicing, so if you want some emoticons [SOFTBLOCK]xD[SOFTBLOCK] xD[SOFTBLOCK] you just ask![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] Heh, not necessary.[SOFTBLOCK] So, shall we get to the operation at hand?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Reposition everyone.
fnCutsceneMove("JX-101", 7.25, 7.50)
fnCutsceneMove("JX-101", 9.25, 7.50)
fnCutsceneFace("JX-101", 0, 1)
fnCutsceneMove("300910", 6.25, 7.50)
fnCutsceneMove("300910", 8.25, 7.50)
fnCutsceneFace("300910", 0, 1)
fnCutsceneMove("GoldLeader", 5.25, 7.50)
fnCutsceneMove("GoldLeader", 7.25, 7.50)
fnCutsceneFace("GoldLeader", 0, 1)
fnCutsceneMove("Psue", 9.25, 9.50)
fnCutsceneFace("Psue", 0, -1)
fnCutsceneMove("Christine", 6.25, 9.50)
fnCutsceneMove("Christine", 8.25, 9.50)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneMove("55", 6.25, 7.50)
fnCutsceneMove("55", 6.25, 9.50)
fnCutsceneMove("55", 7.25, 9.50)
fnCutsceneFace("55", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Music starts.
fnCutsceneInstruction([[ AudioManager_PlayMusic("Briefing") ]])

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "GolemB", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Electrosprite", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] All right, I will assume we have all read our briefing packages.[SOFTBLOCK] Even still, I will reiterate the background.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please present questions as they come up.[SOFTBLOCK] Have there been any major complications to arise I have not been made aware of?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Nothing on our end.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] All clear for Team Teal.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] My troops are itching to go.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We're as ready as we're going to get.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Good.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] In two days time, that is, approximately 48 hours, the annual Sunrise Gala will commence.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] At this time, a large number of high-ranking officials in the administration of Regulus City will be together in one place.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] While the event itself has left a surprisingly small trace in the network, I have managed to piece together a rough attendance list.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will assume any unit who has purchased a major piece of clothing is a likely guest, while units who had their security assignment set to 'undefined' is a likely guard.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Wait a moment, Blue Leader.[SOFTBLOCK] Have we determined exactly why the Gala does not officially seem to exist?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] The entire city knows of it, and the invitations have gone out.[SOFTBLOCK] Why isn't it on the network?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] We have not determined the reason, if there is one.[SOFTBLOCK] Our best guess is security.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] The invite I received was sealed and handmade.[SOFTBLOCK] It was delivered by a courier.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Keeping the guest list off the networks means it's harder to hack.[SOFTBLOCK] Blue Leader has only guesses, not a full list.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Even if security is the reason, something isn't adding up.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] True, and your suspicions are well-founded.[SOFTBLOCK] Unfortunately it is not something we can determine, even given the months of advance notice.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I have determined that three of the Prime Command Units will be in attendance, as well as several sub-heads of security.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hundreds of Lord Units are in attendance, with no particular pattern.[SOFTBLOCK] In previous Galas, the Lord Units invited are politcally popular, or show great promise.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] A trap for me is not impossible, considering what I've done, but the major efficiency gains in my department are grounds enough for an invitation.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I can safely say every Command Unit received an invitation.[SOFTBLOCK] I have every year.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct, though some will be attending as plainclothes security units.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Teal Leader and Pink Leader will need to be extremely cautious.[SOFTBLOCK] Mind your words and assume every conversation is being monitored.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The Gala is being held in the main ballroom and several adjacent halls of the Arcane University in Sector 0.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Pink Leader, having an invitation, will be entering the Gala from the transit tunnels.[SOFTBLOCK] Most of the guests are entering this way.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Considering a requistion form for Stalker-Pattern Pulse Rifles I managed to find, the surface access will most likely have snipers placed atop the buildings.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] While the university campus does have access tunnels, the doors on them will most likely be sealed and have security teams on standby.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I am also assuming that attendees will be searched and screened.[SOFTBLOCK] Pink Leader will be entering without any weapons.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] But all of these are guesses?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] There is nothing on the network for me to intercept other than careless messages written by units assigned to security or cleanup detail.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Still, these are basic precautions any security plan will have.[SOFTBLOCK] Reduced access points, obvious entrances guarded, and so on.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Hmmm...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Luckily, we think we've found a way in that the security teams have passed over.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The Arcane University has heavy equipment delivered to it via transit tunnels.[SOFTBLOCK] There is a receiving bay for each of the departments.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The Physics Research Block, being the oldest building at the university, had to retrofit its design for the transit tunnels.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Their receiving bay is located sixteen floors underground, through solid rock and concrete.[SOFTBLOCK] Radio waves cannot penetrate that rock.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As such, they have a single cable that carries communications up to the main building, alongside the freight elevator to bring heavy cargo up.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is a single point of failure for the security teams.[SOFTBLOCK] They will likely have guards posted in the receiving bay, but should they come under attack, they will have difficulty notifying their superiors.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will be entering via this approach.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] But you said there's a communications cable.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I'm certain it will be alarmed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Correct.[SOFTBLOCK] The schematics suggest it is, and if the alarm goes off, the freight elevator will enter lockdown.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] But we have a unit who can adjust the communications cable.[SOFTBLOCK] I have constructed a signal relay that can be hidden within her PDU's battery case.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] At two hours before sunrise, I will be leaving the main party and heading into the physics building.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] From there, I need to attach the relay to the communication cable's service box.[SOFTBLOCK] Once I've done that, Blue Leader can use the elevator.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I will be transporting a set of shaped charges.[SOFTBLOCK] We will then make our way to sub-basement beneath the ballroom, and plant the charges on the support pillars.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Finally, a single null-point charge will be placed directly beneath the ballroom, and we will exfiltrate.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] At the moment of sunrise, I will detonate the shaped charges.[SOFTBLOCK] This will cause the building to collapse in a controlled demolition.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Finally, the null-point charge will be detonated, destabilizing every atom within 200 meters.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] While a number of units will likely survive the building's collapse, the storm of neutrons will offline anyone caught in it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] And it'll take days before they can all be dug out of the rubble, even if they do survive.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] What's your path of exfiltration?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] One shaped charge will be used to blast a hole in the wall that leads to the biological research labs.[SOFTBLOCK] We can then escape through the habitation domes.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We will likely have to fight our way out, but by that time the damage will be done.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] When the charges detonate, Teal, Red, and Gold leaders will mobilize their teams.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Teal Leader's objective is the motor pool in Sector 7.[SOFTBLOCK] When the charges detonate, their security team will likely panic and move to the university to assist with the rescue operation.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I cannot be certain of this, but considering their commander is stationed in the university, that is their most likely course of action.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Teal Leader will then deal with any resistance left in Sector 7 and enter the motor pool.[SOFTBLOCK] She will steal any vehicle she can, and destroy what she cannot.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I've taken a tour of the motor pool recently, under pretense of requisitioning a rover for the observatory.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] We should be able to secure some armored cruisers and tracked transport vehicles.[SOFTBLOCK] I've got explosive charges to disable what we can't take.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I was just about to suggest that.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Blue Leader, may I send along some support units with Teal Leader?[SOFTBLOCK] We have some spare fuel in Sprocket City that we could use.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] My team could refuel vehicles that were mothballed.[SOFTBLOCK] Plus, more pilots means more vehicles stolen.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Good thinking.[SOFTBLOCK] Please discuss the matter with Teal Leader after the meeting.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Gold Leader and Red Leader will be responsible for starting riots in their assigned sectors.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] While no doubt riots will break out when the security teams fail to respond to the attack, our targets are more specific.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Red Leader's target is Sector 244.[SOFTBLOCK] The western wall can be breached to reach the armory of Sector 170, which will likely be locked down.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Are we to be using the civilians as cover, then?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I don't think that's what she meant.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I took it as 'Let the civilians take what they can carry'.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Just be sure to have your squads take the high-power equipment, and guide the rioters to deal damage to the enemy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Avoid damage to infrastructure.[SOFTBLOCK] We want to keep the parts of the city we control intact so we can make use of their facilities.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Controlling a wildfire isn't going to be easy...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Doubly so for the security forces.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Take charge of the situation.[SOFTBLOCK] Tell the Slave Units that they are all members of Fist of Tomorrow, and let your fame do the rest.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] Oh, that's not the part that concerns me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] But allowing untrained civilians access to firearms, no matter how well meaning, is courting disaster.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] But you are right, and we will certainly deal a lot of damage this way.[SOFTBLOCK] We don't have time to be precise.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Gold Leader, your objective is easier.[SOFTBLOCK] The relay stations in Sector 30 merely need to be destroyed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Start a riot and use it as cover to destroy the relays, then deal as much damage as possible.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Form a frontline of volunteers and establish a line of communications to the command post.[SOFTBLOCK] Sector 30 is well positioned to resist frontal assault.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] Don't forget me, cutiepie![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] Us Electrosprites will sabotage equipment and tap communications for you![SOFTBLOCK] Some of us even want to get directly involved![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] You need something zapped, you just say so![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] Got it.[SOFTBLOCK] You almost make it sound easy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It will be anything but.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The first days after the attack will be extemely chaotic.[SOFTBLOCK] It will take time for battle lines to be established.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We need to be prepared to exfiltrate units that are isolated and to accept refugees.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] I've been stockpiling what I can.[SOFTBLOCK] The Serenity Crater Observatory can serve as a field hospital or refuge if needed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] Plus we can hide the vehicles we steal in the ridges nearby.[SOFTBLOCK] I know a hundred places to put them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] I've been having my sapper teams pick targets.[SOFTBLOCK] We'll collapse transit tunnels to cut off reinforcements before an assault.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] We can hit most of the city with very little notice, though I assume the Administration will wise up pretty quickly.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We've selected Sector 96 to serve as our initial rendezvous and command post.[SOFTBLOCK] If you can't make it there, send couriers so we know you're okay.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sector 96 is fairly easy to fortify and has exterior surface access, but we are fully prepared to abandon the position if things get too hot.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] If worst comes to worst, we can regroup at the abandoned Cryogenics Research Facility.[SOFTBLOCK] The transit tunnels were collapsed but it's a hardpoint that could hold out for a long time.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Remember that all plans we are making now will have to be discarded when conditions in the field change.[SOFTBLOCK] Be flexible.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] And most importantly, do not mobilize your teams if the charges do not detonate.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The critical blow to the Administration's leaders must be dealt.[SOFTBLOCK] Lacking it, the security teams will be organized and disciplined.[SOFTBLOCK] We face a rout.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Other opportunities will show themselves.[SOFTBLOCK] Do not throw it all away.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] I don't know about that.[SOFTBLOCK] The Slave Units I've been talking to are almost ready for an open revolt, considering the disrepair of the city.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] We've been mass-manufacturing weapons for months now.[SOFTBLOCK] Everything is falling apart.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] If we don't move now, the public might revolt on their own.[SOFTBLOCK] They'd be crushed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Then we'll just have to not fail, won't we?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] My friends, a new day dawns on Regulus.[SOFTBLOCK] We shall greet the sun as free units.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "300910:[E|Neutral] To freedom, my machine sisters.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "JX-101:[E|Neutral] To freedom, and to victory![SOFTBLOCK] March to tomorrow, fists held high![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gold:[E|Neutral] To freedom, sisters![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Psue:[E|Neutral] Awwwwww yeah, gonna be some freakin' freedom up in here![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] To freedom![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] To freedom.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

