--[Defeat]
--Player's party is defeated.
local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
fnCutsceneWait(55)
fnCutsceneBlocker()
fnCutsceneSetFrame("55", "Downed")
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
fnCutsceneWait(55)
fnCutsceneBlocker()

--Dialogue.
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Heh...[SOFTBLOCK] Heh...[SOFTBLOCK] Gonna take more than...[SOFTBLOCK] that...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Right, 55?[SOFTBLOCK] Right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Sys...[SOFTBLOCK] tem...[SOFTBLOCK] fail...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Hun?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(55)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneFace("SX399", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] 55, not the...[SOFTBLOCK] time to fool around...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Hey, Christine, I need a breather,[SOFTBLOCK] is that okay?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] I am...[SOFTBLOCK] not beaten...[SOFTBLOCK] but you know, be a good sport...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (It is unconcerned.[SOFTBLOCK] It is not threatened.[SOFTBLOCK] It will find its other.[SOFTBLOCK] Where is other.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Bricks are liquid.[SOFTBLOCK] I'm pleased.[SOFTBLOCK] Not sideways.[SOFTBLOCK] Sideways.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

--No SX-399.
else
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Ent-enter-stand-by...[SOFTBLOCK] Dam-dam-age crtttttttt------[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(55)
    fnCutsceneBlocker()

end

--Remove characters.
for i = 0, 19, 1 do
    local sName = "GenGolem"
    if(i < 10) then sName = sName .. "0" end
    sName = sName .. i
    fnCutsceneTeleport(sName, -100, -100)
end
fnCutsceneTeleport("2856", -100, -100)
fnCutsceneTeleport("Sophie", 44.25, 22.50)
fnCutsceneFace("Sophie", 0, 1)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "Cry0")
fnCutsceneBlocker()

--Remove followers.
AL_SetProperty("Unfollow Actor Name", "55")
if(iSX399JoinsParty == 1.0) then
    AL_SetProperty("Unfollow Actor Name", "SX399")
end
AL_SetProperty("Unfollow Actor Name", "Sophie")

--Collision.
AL_SetProperty("Set Collision", 35, 4, 0, 1)

--Give SX-399 a dialogue.
if(iSX399JoinsParty == 1.0) then
    EM_PushEntity("SX399")
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Activation Script", gsRoot .. "Chapter5Scenes/Gala Scenes/SX399.lua")
    DL_PopActiveObject()
end

--Give collisions to the entourage.
EM_PushEntity("55")
    TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()
EM_PushEntity("EntourageA")
    TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()
EM_PushEntity("EntourageB")
    TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()
EM_PushEntity("EntourageC")
    TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()
