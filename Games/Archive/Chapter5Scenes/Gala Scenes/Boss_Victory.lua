--[Victory!]
--Variables.
local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")

--The party defeats Eldritch Christine.
fnCutsceneTeleport("EntourageA", -100.25, -100.50)
fnCutsceneTeleport("EntourageB", -100.25, -100.50)
fnCutsceneTeleport("EntourageC", -100.25, -100.50)
fnCutsceneTeleport("Sophie", 40.25, 16.50)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlayMusic("Null") ]])

--Christine collapses.
fnCutsceneSetFrame("Christine", "Crouch")
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
fnCutsceneSetFrame("Christine", "Wounded")
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Sophie runs up.
fnCutsceneMove("Sophie", 33.25, 16.50, 1.70)
fnCutsceneMove("Sophie", 33.25,  5.50, 1.70)
fnCutsceneMove("Sophie", 26.25,  5.50, 1.70)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] Christine![SOFTBLOCK] Christine, no, no, no!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "Cry0")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "Cry1")
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Sad") ]])
else
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Sad") ]])
end
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] You -[SOFTBLOCK] you -[SOFTBLOCK] Christine![SOFTBLOCK] Wake up![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Her organic life signs are at zero...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Unit 499323, this physical form of hers has always had zero life signs.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Didn't you hear her whispering?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I was hiding in the side rooms there, but as soon as you started fighting her, I heard her.[SOFTBLOCK] She was telling me it'd be okay.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] She told me not to worry about her and that I should come out.[SOFTBLOCK] Come join her.[SOFTBLOCK] Be with her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It was a lure.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] But it was so convincing...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Did you have to shoot her?[SOFTBLOCK] Can't you help her?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She will awaken momentarily.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] How do you know?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Individuals like her prefer drama to efficiency.[SOFTBLOCK] While her life signs are at zero, she is fully awake and conscious, but is using this as a pretext.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] She is going to stand up and tell you the power of love brought her back from the brink, or something equally trite.[BLOCK][CLEAR]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] You think all of that is trite, 55?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Yes, when she is wasting my time with it.") ]])
else
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Right, Christine?[SOFTBLOCK] ...[SOFTBLOCK] Right?") ]])
end
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(165)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Sad") ]])
else
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Sad") ]])
end
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Christine...[SOFTBLOCK] please...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Any second now...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] I said, she will awaken and is using this as a pretext...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] Unit 771852, stop testing my patience![SOFTBLOCK] Stand up, there is important work to do![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Stop it, 55.[SOFTBLOCK] It's not going to work.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Damn it, don't you dare be dead.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] As if a few pulse rounds to the temple would be enough to retire you![SOFTBLOCK] We've been through much worse![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Please don't be dead, Christine...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Music starts.
fnCutsceneInstruction([[ AudioManager_PlayMusic("SophiesTheme") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Christine", "Crouch")
fnCutsceneSetFrame("Sophie", "Null")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Christine", "Null")
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
else
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
end
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *Cough*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Thanks a ton, 55.[SOFTBLOCK] You almost ruined it![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ...[SOFTBLOCK] The outcome was never really in doubt.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Christine![SOFTBLOCK] *smooch*[SOFTBLOCK] Oh thank goodness![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Ouch, ouch, ooh.[SOFTBLOCK] I love the kisses, but remember that I have been shot.[SOFTBLOCK] A lot.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But playing dead in this body is pretty easy, so I thought I'd try to get 55 to act nice for once.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I didn't think it'd take as long as it did, but there we are.[SOFTBLOCK] Sorry about that, Sophie.[BLOCK][CLEAR]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Oh, I knew there was something soft beneath the shell.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I was merely playing along to get Christine to stop acting.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Smirk] Nobody believes you, hun.[BLOCK][CLEAR]") ]])
end
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] You gave me such a scare![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] (Ugh, these tentacles are so gross, but I can't help myself.[SOFTBLOCK] Gotta hold her tight!)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay, okay, fun's over.[SOFTBLOCK] We still have bad guys to deal with.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Yeah, but, where did they go?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] My sister has sent a message to my PDU.[SOFTBLOCK] She is waiting for us in the biolabs north of here.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Sister?[SOFTBLOCK] Wow, 55, congratulations![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It has not been a tearful reunion.[SOFTBLOCK] Christine, we should go.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] 201890 was using you as a distraction to make her escape.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] No, no she wasn't.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She has no fear of the security forces, or of you.[SOFTBLOCK] There is only one thing she fears, and that is...[BLOCK][CLEAR]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Vivify?[BLOCK][CLEAR]") ]])
else
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Project Vivify.[BLOCK][CLEAR]") ]])
end
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes.[SOFTBLOCK] She is in the biolabs right now.[SOFTBLOCK] I can hear her song.[SOFTBLOCK] I know where she is.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She called everyone to her, and she was very angry...[SOFTBLOCK] 20 was acting without permission...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She wants us to go there.[SOFTBLOCK] She wants to meet me in person again.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] What?[SOFTBLOCK] No way, let's go home![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] ...[SOFTBLOCK] It's too dangerous.[SOFTBLOCK] What if you don't come back this time?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] She threatens the entire city, Sophie.[SOFTBLOCK] We have to stop her, and I think I'm the only one who can.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 201890 did something to me, turned me into this because I was vulnerable, but I think I can overcome it now.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Once again, we are the only ones to survive a direct encounter with Vivify's influence.[SOFTBLOCK] We have to go.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] But...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Chin up, Sophie.[SOFTBLOCK] Keep that smile going bright.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] As long as you smile, even in pitch darkness, I'll see it.[SOFTBLOCK] Your smile will guide me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Gotta be strong...[SOFTBLOCK] Gotta be confident...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But if you play a prank on me like that to prove a point again -[SOFTBLOCK] no sex for a month![BLOCK][CLEAR]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Woah, woah, Sophie, think about what you're saying![BLOCK][CLEAR]") ]])
end
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] Even the administrators would never carry out such a punishment...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Okay, okay![SOFTBLOCK] No more tricks![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We'd better go, 55, before she changes her mind![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sophie...[SOFTBLOCK] Come with us.[SOFTBLOCK] We'll find a safe spot in the biolabs and hole you up there.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The only other way out is with the security forces, and I don't trust them.[SOFTBLOCK] They might take you hostage.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A good evaluation.[SOFTBLOCK] Despite the extreme danger, she will be safer with us.[BLOCK][CLEAR]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] I'll keep you safe.[SOFTBLOCK] I think I owe you a bit of a debt, right?[BLOCK][CLEAR]") ]])
end
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Oh my goodness![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But I have to stay strong, no matter the danger...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Modify party setup. Christine is back in, and Sophie is now following.
AC_SetProperty("Set Party", 0, "Christine")
AC_SetProperty("Set Party", 1, "55")
if(iSX399JoinsParty == 1.0) then
    AC_SetProperty("Set Party", 2, "SX-399")
end

--IDs.
if(iSX399JoinsParty == 1.0) then
    EM_PushEntity("55")
        local i55ID = RE_GetID()
    DL_PopActiveObject()
        EM_PushEntity("SX399")
            local iSX399ID = RE_GetID()
        DL_PopActiveObject()
    EM_PushEntity("Sophie")
        local iSophieID = RE_GetID()
    DL_PopActiveObject()

    --Store names and IDs.
    gsFollowersTotal = 3
    gsaFollowerNames[1] = "55"
    gsaFollowerNames[2] = "SX399"
    gsaFollowerNames[3] = "Sophie"
    giaFollowerIDs[0] = i55ID
    giaFollowerIDs[1] = iSX399ID
    giaFollowerIDs[2] = iSophieID

    --Tell everyone to follow.
    AL_SetProperty("Unfollow Actor Name", "55")
    AL_SetProperty("Unfollow Actor Name", "SX399")
    AL_SetProperty("Unfollow Actor Name", "Sophie")
    AL_SetProperty("Follow Actor ID", i55ID)
    AL_SetProperty("Follow Actor ID", iSX399ID)
    AL_SetProperty("Follow Actor ID", iSophieID)
else
    EM_PushEntity("55")
        local i55ID = RE_GetID()
    DL_PopActiveObject()
    EM_PushEntity("Sophie")
        local iSophieID = RE_GetID()
    DL_PopActiveObject()

    --Store names and IDs.
    gsFollowersTotal = 2
    gsaFollowerNames[1] = "55"
    gsaFollowerNames[2] = "Sophie"
    giaFollowerIDs[0] = i55ID
    giaFollowerIDs[1] = iSophieID

    --Tell everyone to follow.
    AL_SetProperty("Unfollow Actor Name", "55")
    AL_SetProperty("Unfollow Actor Name", "Sophie")
    AL_SetProperty("Follow Actor ID", i55ID)
    AL_SetProperty("Follow Actor ID", iSophieID)
end

--Scene transition.
fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusBiolabsA", "FORCEPOS:15.0x31.0x0") ]])
fnCutsceneBlocker()

