--[ ======================================= Floor Burster ======================================= ]
--In this cutscene, the party dramatically bursts from the floor and confronts 201890!

--[ =========================================== Setup =========================================== ]
--Spawn Sophie.
TA_Create("Sophie")
    TA_SetProperty("Position", 25, 22)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/SophieDress/", true)
    TA_SetProperty("Facing", gci_Face_North)
    TA_SetProperty("Wipe Special Frames")
    TA_SetProperty("Add Special Frame", "Cry0", "Root/Images/Sprites/Special/Sophie|Cry0")
    TA_SetProperty("Add Special Frame", "Cry1", "Root/Images/Sprites/Special/Sophie|Cry1")
DL_PopActiveObject()

--2856
TA_Create("2856")
    TA_SetProperty("Position", -10, -10)
    TA_SetProperty("Facing", gci_Face_North)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/56/", true)
DL_PopActiveObject()

--Spawn 20 and her entourage.
TA_Create("20")
    TA_SetProperty("Position", 35, 4)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/20/", false)
    TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()
TA_Create("EntourageA")
    TA_SetProperty("Position", 36, 4)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/EldritchDream/", false)
    TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()
TA_Create("EntourageB")
    TA_SetProperty("Position", 37, 4)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/EldritchDream/", false)
    TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()
TA_Create("EntourageC")
    TA_SetProperty("Position", 38, 4)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/EldritchDream/", false)
    TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()

--[Disable Collisions]
AL_SetProperty("Set Collision", 24, 16, 0, 0)
AL_SetProperty("Set Collision", 25, 16, 0, 0)
AL_SetProperty("Set Collision", 26, 16, 0, 0)

--[Variables]
local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")

--[ ========================================= Execution ========================================= ]
--Black the screen out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Camera focuses on Sophie.
Cutscene_CreateEvent("CameraEvent", "Camera")
    CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
    CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (22.50 * gciSizePerTile))
DL_PopActiveObject()

--Move the rest of the party off.
fnCutsceneTeleport("Christine", -100.25, -100.50)
fnCutsceneTeleport("55", -100.25, -100.50)
if(iSX399JoinsParty == 1.0) then
    fnCutsceneTeleport("SX399", -100.25, -100.50)
end
fnCutsceneTeleport("Influenced", -100.25, -100.50)
fnCutsceneBlocker()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] S-[SOFTBLOCK]Sophie? What's wrong?[SOFTBLOCK] Why are you still at the gala?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I tried to leave, but there was a unit who wouldn't let anyone through, and she looked really weird![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] The security units are all acting really strange![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2856:[VOICE|2856] Strange?[SOFTBLOCK] How strange?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Like they keep whispering to themselves and I think their eyes are bleeding lubricant or something![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] They punched a Lord Unit who tried to force her way out and I don't know what to do![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] 55![SOFTBLOCK] We need to go![SOFTBLOCK] Right now![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] Detonating charges...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Function
local fnGolemsLookAround = function()
    for i = 0, 19, 1 do
        
        --Generate name.
        local sName = "GenGolem"
        if(i < 10) then sName = sName .. "0" end
        sName = sName .. i
        
        --NPC looks a random direction.
        local iRoll = LM_GetRandomNumber(1, 4)
        if(iRoll == 1) then
            fnCutsceneFace(sName, 0, 1)
        elseif(iRoll == 2) then
            fnCutsceneFace(sName, 0, -1)
        elseif(iRoll == 3) then
            fnCutsceneFace(sName, 1, 0)
        elseif(iRoll == 4) then
            fnCutsceneFace(sName, -1, 0)
        end
    end
end

--Explosions.
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneBlocker()
fnGolemsLookAround()

fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneBlocker()
fnGolemsLookAround()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneBlocker()
fnGolemsLookAround()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 1)
fnCutsceneWait(25)
fnGolemsLookAround()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneBlocker()
fnGolemsLookAround()
fnCutsceneWait(25)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sophie!?[SOFTBLOCK] Are you still there?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] I-[SOFTBLOCK]I thought 55 was going to blow me up?[SOFTBLOCK] What's going on?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|201890] Hello, hello, distinguished ladies![SOFTBLOCK] How are we this morning?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Camera moves. Music starts.
fnCutsceneInstruction([[ AL_SetProperty("Music", "EquinoxTheme") ]])
Cutscene_CreateEvent("CameraEvent", "Camera")
    CameraEvent_SetProperty("Max Move Speed", 10.0) --Default is 5.0
    CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (6.50 * gciSizePerTile))
DL_PopActiveObject()

--All golems look towards this point.
for i = 0, 19, 1 do
    
    --Generate name.
    local sName = "GenGolem"
    if(i < 10) then sName = sName .. "0" end
    sName = sName .. i
    fnCutsceneFace(sName, 0, -1)
end

--20 and her entourage move out.
fnCutsceneMove("20", 25.25, 4.50)
fnCutsceneMove("20", 25.25, 5.50)
fnCutsceneFace("20", 0, 1)
fnCutsceneMove("EntourageA", 23.25, 4.50)
fnCutsceneFace("EntourageA", 0, 1)
fnCutsceneMove("EntourageB", 25.25, 4.50)
fnCutsceneFace("EntourageB", 0, 1)
fnCutsceneMove("EntourageC", 27.25, 4.50)
fnCutsceneFace("EntourageC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Yes, yes, oh, and what a morning it is![SOFTBLOCK] For soon, everything about my beloved city will change.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] But, oh, I'm sure you are wondering who I am, who my friends are, and where I'm going with this![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[E|Neutral] Wonder no longer![SOFTBLOCK] For I am your gracious host and the organizer of this wonderful event![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] I go by the moniker of 201890, though of course my closest friends call me 20.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] These are my friends![SOFTBLOCK] Go on, my friends, mingle![SOFTBLOCK] Mingle!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("EntourageA", 23.25, 7.50)
fnCutsceneMove("EntourageA", 20.25, 7.50)
fnCutsceneFace("EntourageA", 0, 1)
fnCutsceneMove("EntourageB", 25.25, 7.50)
fnCutsceneMove("EntourageB", 28.25, 7.50)
fnCutsceneFace("EntourageB", 0, 1)
fnCutsceneMove("EntourageC", 27.25, 7.50)
fnCutsceneMove("EntourageC", 30.25, 7.50)
fnCutsceneFace("EntourageC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Get away from me, you freak![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] R-[SOFTBLOCK]run![SOFTBLOCK] Run!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] No, I believe you will be staying exactly where you are.[SOFTBLOCK] The exits have been blocked, and your precious security units are busy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] You see, those explosions you just heard?[SOFTBLOCK] That was them, being busy with my other friends in the basement.[SOFTBLOCK] They cannot protect you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] So you'll just remain calm, and remain seated, because I really don't want to have to snap any limbs.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] We are, after all, dignified and well dressed units, are we not?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("CommandGalaB", 32.25, 5.50)
fnCutsceneFace("CommandGalaB", -1, 0)
fnCutsceneBlocker()
fnCutsceneFace("20", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Doll", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] That, [SOFTBLOCK]'friend'[SOFTBLOCK], is quite clearly enough.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Whoever you think you are, whatever you intend to do, we will not allow it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Oh, you attend my party, and then act like you're above me?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] *Your*[SOFTBLOCK] party, is it?[SOFTBLOCK] I can't wait to hear your explanation![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Honored Command Unit, I did organize this party.[SOFTBLOCK] And I did what any good unit does -[SOFTBLOCK] delegate![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] It actually wasn't too difficult, you see.[SOFTBLOCK] By merely ordering that all communication was to be off the network, and sending a few handwritten notes, well, you did the rest![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Yes, yes, I know.[SOFTBLOCK] You're welcome![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Just who are you, anyway?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] As I said, I am 201890.[SOFTBLOCK] That's my favoured name, though there are many.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I just checked my lookup table, and Unit 201890 is a Slave Unit currently assigned to welding in Sector 120.[SOFTBLOCK] You are not her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Such a focus on names, on propriety.[SOFTBLOCK] Everything in its place, yes?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] I am not the unit you refer to.[SOFTBLOCK] This name is mine.[SOFTBLOCK] I named myself.[SOFTBLOCK] A crucial first step in one's autonomy, I'd say.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Names are something someone else gives you, before they know who you are.[SOFTBLOCK] At birth or at conversion, the name we get is always going to be the wrong one.[SOFTBLOCK] So I made my own.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] I made my own body, too.[SOFTBLOCK] Work in progress, sure, but still something to be proud of.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] So, Command Unit, what do you think?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] I think there is a place on the scrap heap for that husk of a body.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] My friends thought the same thing at first.[SOFTBLOCK] Don't you want to be friends?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Die, scum!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("CommandGalaB", 25.75, 5.50, 2.0)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Heartbeat") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneTeleport("CommandGalaB", -100.25, -100.50)
fnCutsceneBlocker()
fnCutsceneWait(305)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] Where -[SOFTBLOCK] did she go?[SOFTBLOCK] What happened?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("20", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[VOICE|201890] Why, she's still here![SOFTBLOCK] Aren't you, my new friend?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|Heartbeat") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneTeleport("Influenced", 26.25, 5.50)
fnCutsceneFace("Influenced", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[VOICE|201890] Don't be shy![SOFTBLOCK] Go on, show everyone your pretty face!") ]])
fnCutsceneBlocker()
fnCutsceneWait(125)
fnCutsceneBlocker()
fnCutsceneFace("Influenced", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Influenced", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "DollInfluenced", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] My.[SOFTBLOCK] Pretty.[SOFTBLOCK] Face.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] You see?[SOFTBLOCK] Now don't you feel much better?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] So.[SOFTBLOCK] Much.[SOFTBLOCK] My mind.[SOFTBLOCK] Infinity.[SOFTBLOCK] All thoughts.[SOFTBLOCK] Simultaneous.[SOFTBLOCK] Thank.[SOFTBLOCK] Thank.[SOFTBLOCK] It opens.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] The vault opens and brains spill out![SOFTBLOCK] Eggs fester in flesh and a scream vibrates through eternity![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] We swirl around the light until we snuff it out, sucking through into the next self![SOFTBLOCK] Yes![SOFTBLOCK] Friends![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Aww, aren't you just adorable?[SOFTBLOCK] Come![SOFTBLOCK] Let's make everyone here our friends, shall we?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Command Unit:[E|Neutral] Yes![SOFTBLOCK] Yes![SOFTBLOCK] Yes!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Move back to Sophie.
Cutscene_CreateEvent("CameraEvent", "Camera")
    CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
    CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (22.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Sad] Christine, help![SOFTBLOCK] Help![SOFTBLOCK] They're going to get me![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Sophie, find a place to hide![SOFTBLOCK] We're coming![SOFTBLOCK] We'll be right there!") ]])
fnCutsceneBlocker()
fnCutsceneWait(125)
fnCutsceneBlocker()
    
--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "2856", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55.[SOFTBLOCK] Give me that leftover blasting charge.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We need to move to the west and head up the elevators to reach the main floor.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No, that will take too long.[SOFTBLOCK] Give me the charge.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] You're wasting time.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I am thinking three-dimensionally.[SOFTBLOCK] Now, stand the hell back.") ]])
fnCutsceneBlocker()
fnCutsceneWait(125)
fnCutsceneBlocker()
fnCutsceneWait(45)

--Remove the NPCs in question.
fnCutsceneTeleport("20", 25.25, 4.50)
fnCutsceneTeleport("GenGolem15", -100.25, -100.50)
fnCutsceneTeleport("CommandGalaA", -100.25, -100.50)
fnCutsceneTeleport("GenGolem01", -100.25, -100.50)
fnCutsceneTeleport("GenGolem02", -100.25, -100.50)
fnCutsceneTeleport("GenGolem03", -100.25, -100.50)
fnCutsceneTeleport("GenGolem04", -100.25, -100.50)
fnCutsceneTeleport("GenGolem05", -100.25, -100.50)
fnCutsceneTeleport("GenGolem06", -100.25, -100.50)
fnCutsceneTeleport("GenGolem07", -100.25, -100.50)
fnCutsceneTeleport("GenGolem08", -100.25, -100.50)
fnCutsceneTeleport("GenGolem09", -100.25, -100.50)
fnCutsceneTeleport("GenGolem10", -100.25, -100.50)
fnCutsceneTeleport("GenGolem11", -100.25, -100.50)
fnCutsceneTeleport("GenGolem12", -100.25, -100.50)
fnCutsceneTeleport("GenGolem13", -100.25, -100.50)
fnCutsceneTeleport("Sophie", 45.25, 22.50)
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneTeleport("EntourageA", 45.25, 25.50)
fnCutsceneFace("EntourageA", -1, 0)
fnCutsceneTeleport("EntourageB", 43.25, 25.50)
fnCutsceneTeleport("EntourageC", 48.25, 23.50)
fnCutsceneFace("EntourageC", 1, 0)
fnCutsceneBlocker()

--Fade back in.
Cutscene_CreateEvent("CameraEvent", "Camera")
    CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
    CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(125)
fnCutsceneBlocker()

--Animate the explosion here.
local iTPF = 6
fnCutsceneInstruction([[ AudioManager_PlaySound("World|BigExplosion") ]])
for i = 1, 12, 1 do
    local sString = "AL_SetProperty(\"Set Layer Disabled\", \"FloorExpl\" .. " .. i .. ",  false)"
    fnCutsceneInstruction(sString)
    if(i > 1) then
        sString = "AL_SetProperty(\"Set Layer Disabled\", \"FloorExpl\" .. " .. i-1 .. ",  true)"
        fnCutsceneInstruction(sString)
    end
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
end

--All golems look towards this point.
for i = 0, 19, 1 do
    
    --Generate name.
    local sName = "GenGolem"
    if(i < 10) then sName = sName .. "0" end
    sName = sName .. i
    
    --Get current position.
    EM_PushEntity(sName)
        local fXPos, fYPos = TA_GetProperty("Position")
        fXPos = fXPos / gciSizePerTile
        fYPos = fYPos / gciSizePerTile
    DL_PopActiveObject()
    
    --Generate 'speeds'.
    local fXSpeed = 0
    local fYSpeed = 0
    if(fXPos < 25.25) then fXSpeed = 1.0 end
    if(fXPos > 25.25) then fXSpeed = -1.0 end
    if(fYPos < 16.25) then fYSpeed = 1.0 end
    if(fYPos > 16.25) then fYSpeed = -1.0 end
    
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", sName)
		ActorEvent_SetProperty("Face", fXSpeed, fYSpeed)
	DL_PopActiveObject()
end

fnCutsceneWait(145)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneFace("55", 0, -1)
if(iSX399JoinsParty == 1.0) then
    fnCutsceneFace("SX399", 0, -1)
end
fnCutsceneFace("2856", 0, -1)
fnCutsceneTeleport("Christine", 25.25, 16.50)
fnCutsceneTeleport("55", 26.25, 16.50)
fnCutsceneTeleport("SX399", 24.25, 16.50)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 25.25, 14.50)
fnCutsceneMove("55", 26.25, 15.50)
if(iSX399JoinsParty == 1.0) then
    fnCutsceneMove("SX399", 24.25, 15.50)
end
fnCutsceneBlocker()
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0) --Default is 5.0
	CameraEvent_SetProperty("Focus Actor Name", "Christine")
DL_PopActiveObject()
fnCutsceneTeleport("2856", 25.25, 16.50)
fnCutsceneBlocker()
fnCutsceneMove("2856", 25.25, 15.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
    
--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Okay, ladies![SOFTBLOCK] Everyone out![SOFTBLOCK] Orderly fashion, and all that![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Unit 2856, have your security units get everyone out through the tunnels.[SOFTBLOCK] We'll go distract this '20' character.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "56:[E|Neutral] And become her next casualty?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] I think we can handle ourselves.[SOFTBLOCK] Worry about your assignment, not mine.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I don't see Sophie...[SOFTBLOCK] where is she?)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] (No time -[SOFTBLOCK] gotta take down 201890 and hope the security units rescue Sophie!)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Move the party.
fnCutsceneMove("Christine", 23.25, 15.50, 2.00)
fnCutsceneBlocker()
fnCutsceneMove("55", 25.25, 15.50, 2.00)
if(iSX399JoinsParty == 1.0) then
    fnCutsceneMove("SX399", 24.25, 15.50, 2.00)
end
fnCutsceneBlocker()
fnCutsceneMove("Christine", 18.25, 15.50, 2.00)
fnCutsceneMove("Christine", 18.25, 16.50, 2.00)
fnCutsceneMove("Christine", 17.25, 16.50, 2.00)
fnCutsceneMove("Christine", 17.25,  7.50, 2.00)
fnCutsceneMove("Christine", 25.25,  7.50, 2.00)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneMove("55", 18.25, 15.50, 2.00)
fnCutsceneMove("55", 18.25, 16.50, 2.00)
fnCutsceneMove("55", 17.25, 16.50, 2.00)
fnCutsceneMove("55", 17.25,  7.50, 2.00)
fnCutsceneMove("55", 26.25,  7.50, 2.00)
fnCutsceneFace("55", 0, -1)
if(iSX399JoinsParty == 1.0) then
    fnCutsceneMove("SX399", 18.25, 15.50, 2.00)
    fnCutsceneMove("SX399", 18.25, 16.50, 2.00)
    fnCutsceneMove("SX399", 17.25, 16.50, 2.00)
    fnCutsceneMove("SX399", 17.25,  7.50, 2.00)
    fnCutsceneMove("SX399", 24.25,  7.50, 2.00)
    fnCutsceneFace("SX399", 0, -1)
end
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
    
--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
if(iSX399JoinsParty == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Hey, robo-bimbo![SOFTBLOCK] Pick on someone in your own weight class![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yeah, leave the small fry and come get the big fish.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are going to destroy you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Real inventive, 55...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Ah, finally.[SOFTBLOCK] The guest of honor.[SOFTBLOCK] The one who eeeeveryone won't shut up about.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Christine, what is it you think makes you so special?[SOFTBLOCK] Work ethic?[SOFTBLOCK] Birth?[SOFTBLOCK] No?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] All I hear about you is the possibilities, but I never hear about anything you've actually done.[SOFTBLOCK] Just what you will do.") ]])
else
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "2855", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Picking on poor defenseless units?[SOFTBLOCK] For shame![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Quit tormenting the small fry, come get the big fish.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] We are -[SOFTBLOCK] going to destroy you, interloper.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (Real inventive, 55...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Ah, finally.[SOFTBLOCK] The guest of honor.[SOFTBLOCK] The one who eeeeveryone won't shut up about.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Christine, what is it you think makes you so special?[SOFTBLOCK] Work ethic?[SOFTBLOCK] Birth?[SOFTBLOCK] No?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] All I hear about you is the possibilities, but I never hear about anything you've actually done.[SOFTBLOCK] Just what you will do.") ]])
    
    
end
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneMove("Christine", 25.25, 5.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
    
--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Who's been talking about me behind my back?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Behind your back?[SOFTBLOCK] No.[SOFTBLOCK] No.[SOFTBLOCK] Certainly not.[SOFTBLOCK] We've been talking to you this whole time, but you haven't been listening.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] So self-absorbed, petty, and greedy to boot.[SOFTBLOCK] Taking things that aren't yours because you want them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] What are you babbling about?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] The spotlight![SOFTBLOCK] I was first![SOFTBLOCK] I was the favourite, but now you are![SOFTBLOCK] Because of all the hard work you haven't put in![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Without trying you've been given everything that I would kill for![SOFTBLOCK] Do you not understand?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Nope.[SOFTBLOCK] You're just ranting.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "20:[E|Neutral] Then stop listening to my words and listen to my meaning...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] H-[SOFTBLOCK]huh?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Black the screen out.
fnCutsceneWait(125)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Music", "Vivify") ]])
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The thing known as 201890 stared at Christine, and Christine stared back.[SOFTBLOCK] She tensed, ready for an attack, but an attack did not come.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It was only then that she realized that time itself seemed to stand still.[SOFTBLOCK] She looked over her shoulder.[SOFTBLOCK] The room was still.[SOFTBLOCK] Golems were held in place, drops of liquid held in midair as they were spilled.[SOFTBLOCK] Nothing moved.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "When she looked back to 201890, she realized that she was no longer there.[SOFTBLOCK] There was no longer a there, there.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She realized she was standing nowhere, in an empty blackness.[SOFTBLOCK] She could smell something, something burnt, fleshy, wrong...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She was still in the ballroom, but it had changed.[SOFTBLOCK] The floors were made of meat.[SOFTBLOCK] The walls were fleshy with tendrils poking out.[SOFTBLOCK] Blood poured from open wounds.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She could see outside, somehow.[SOFTBLOCK] The whole city, the whole moon, had become flesh.[SOFTBLOCK] Giant stalks of unknown origin towered over her.[SOFTBLOCK] Mouths gaped and bled, their teeth gnashing and chewing at the flesh they were attached to.[SOFTBLOCK] Everything was dead, somehow, but never stopped bleeding.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Worse still, this was no hallucination.[SOFTBLOCK] In the back of her mind, in the most basic and primitive place of her being, she knew that this was true.[SOFTBLOCK] This was the future.[SOFTBLOCK] It would happen.[SOFTBLOCK] It was certain.[SOFTBLOCK] It had already happened, and all things would lead to it.[SOFTBLOCK] Struggling would do nothing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked down.[SOFTBLOCK] She was human, but still metal.[SOFTBLOCK] Her hands were covered in latex, her breasts had become melted starlight.[SOFTBLOCK] All her parts were many parts, flashing and melting into one another.[SOFTBLOCK] What was she?[SOFTBLOCK] Who was she?[SOFTBLOCK] She was nobody.[SOFTBLOCK] She was nothing.[SOFTBLOCK] A pustule on some enormous, dead creature.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her form began to shimmer.[SOFTBLOCK] She was losing herself.[SOFTBLOCK] This was how she had gotten here, into the distant future.[SOFTBLOCK] She had stepped out of time, and stepped back in, now.[SOFTBLOCK] She needed to concentrate and return herself, or she would be lost.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Among the fleshy tendrils boiling and writhing across the mutated planet, there came a voice.[SOFTBLOCK] A quiet song, singing words she could not hear.[SOFTBLOCK] It called to her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It was her.[SOFTBLOCK] 'Vivify'.[SOFTBLOCK] Not her name.[SOFTBLOCK] She had no name.[SOFTBLOCK] There were no names.[SOFTBLOCK] Everything was dead.[SOFTBLOCK] She was singing a sad song, mourning.[SOFTBLOCK] She had made mistakes.[SOFTBLOCK] She was not alive, nothing was.[SOFTBLOCK] Nothing would ever be.[SOFTBLOCK] Not what she had intended.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine understood.[SOFTBLOCK] She looked at herself.[SOFTBLOCK] Her flesh had become grey, dead.[SOFTBLOCK] She was dead in this world, but she kept moving around.[SOFTBLOCK] A sense of habit, of commitment, of purpose.[SOFTBLOCK] She was dead and could not die.[SOFTBLOCK] Vivify sung to her.[SOFTBLOCK] Her song animated this dead world.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She blinked.[SOFTBLOCK] The ballroom was back.[SOFTBLOCK] She was standing on the stage.[SOFTBLOCK] Everyone was staring at her.[SOFTBLOCK] 201890 was gone, where had she gone?[SOFTBLOCK] The song was here, around her, everywhere.[SOFTBLOCK] It was her.[SOFTBLOCK] She was animated by the song.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked at her hand again.[SOFTBLOCK] Dead.[SOFTBLOCK] Grey.[SOFTBLOCK] Incubating a corpse in an egg.[SOFTBLOCK] She saw 2855's face.[SOFTBLOCK] It was twisted into a rage.[SOFTBLOCK] She at once hated it.[SOFTBLOCK] She hated that face.[SOFTBLOCK] She was dead.[SOFTBLOCK] The song told her to hate.[SOFTBLOCK] She hated everything.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The song told her to destroy her former comrades.[SOFTBLOCK] She was dead, and dead things did what the song told them to.[SOFTBLOCK] She would destroy them...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Change Christine.
local iHasEldritchForm = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
fnCutsceneInstruction([[LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])

--Reposition.
fnCutsceneTeleport("Christine", 25.25, 4.50)
fnCutsceneFace("Christine", 0, 1)
fnCutsceneTeleport("55", 25.25, 6.50)
if(iSX399JoinsParty == 1.0) then
    fnCutsceneTeleport("SX399", 26.25, 6.50)
end
fnCutsceneTeleport("20", -100, -100)
fnCutsceneTeleport("Influenced", -100, -100)
fnCutsceneTeleport("2856", -100, -100)

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 25.25, 5.50, 0.25)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Variations. Christine did not lose to Vivify in the LRT facility.
if(iHasEldritchForm == 0.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] What happened to her?[SOFTBLOCK] 55?[SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] She has fallen victim to the influence of 201890, as we were warned.[SOFTBLOCK] She is one of them, now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This is what happens to her.[SOFTBLOCK] We observed earlier that she can hear things we cannot.[SOFTBLOCK] When she was muttering to herself, she was conversing with them.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] 'Them'?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I do not know who, but I do know it is someone who exists.[SOFTBLOCK] Perhaps it is Vivify, perhaps something far greater.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] We've got to change her back![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Then help me subdue her.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    else
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, the enemy has fled.[SOFTBLOCK] Respond.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Unit 771852, that is an order.[SOFTBLOCK] Respond, immediately.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] EM scans indicate total brain death.[SOFTBLOCK] Can you hear me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You are...[SOFTBLOCK] one of them...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] The experiments I read about in the database suggest this can be corrected, but were cut short by the test subject escaping.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine...[SOFTBLOCK] Please be recoverable.[SOFTBLOCK] I am not sure what I will do without you...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end

--Christine did lose to Vivify in the LRT facility.
else

    --SX-399 is present.
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] What happened to her?[SOFTBLOCK] 55?[SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] This has happened before.[SOFTBLOCK] In the LRT facility, Christine came into contact with Project Vivify.[SOFTBLOCK] It changed her.[SOFTBLOCK] She is one of them, now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Her brain is currently in a zero-cycle state, like sleep.[SOFTBLOCK] According to the research collected before Vivify escaped, her body is dead.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] Can't she transform again?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes, but to do so she would have to want to.[SOFTBLOCK] She is currently effectively brain-dead.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Whatever is animating her can be interrupted through the use of high-energy exposure and chemical reactions.[SOFTBLOCK] But she will need to be subdued first.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Sad] But - [SOFTBLOCK]but I don't want to fight her![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Funny.[SOFTBLOCK] She does not appear to think the same thing.[SOFTBLOCK] Prepare yourself.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --No SX-399.
    else
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "2855", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] Christine, you damned fool...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Why did you transform yourself?[SOFTBLOCK] Or were you even aware of what you were doing?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You said this would not happen again, yet it has.[SOFTBLOCK] And this time I am unsure if I will be able to stop you.[SOFTBLOCK] Think of all the units you are failing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Think of...[SOFTBLOCK] anything at all.[SOFTBLOCK] Just don't let it end like this...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end
end

--Remove Christine from the party. It's now 55 and SX-399. Darkmatter also joins!
if(iSX399JoinsParty == 1.0) then
    AC_SetProperty("Set Party", 0, "55")
    AC_SetProperty("Set Party", 1, "SX-399")
    AC_SetProperty("Set Party", 2, "Null")
else
    AC_SetProperty("Set Party", 0, "55")
    AC_SetProperty("Set Party", 1, "Null")
    AC_SetProperty("Set Party", 2, "Null")
end

--Trigger the boss battle.
fnCutsceneInstruction([[ AC_SetProperty("Next Music Override", "MotherTheme", 0.0000) ]])
fnCutsceneInstruction([[ AC_SetProperty("Activate") ]])
fnCutsceneInstruction([[ AC_SetProperty("Victory Script", gsRoot .. "Chapter5Scenes/Gala Scenes/Boss_Victory.lua") ]])
fnCutsceneInstruction([[ AC_SetProperty("Defeat Script",  gsRoot .. "Chapter5Scenes/Gala Scenes/Boss_Defeat.lua") ]])
fnCutsceneInstruction([[ AC_SetProperty("Unretreatable", true) ]])
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "Enemies/Chapter5/ChristineSleeping.lua", 0) ]])
fnCutsceneBlocker()

--Re-enable collisions here.
fnCutsceneInstruction([[ AL_SetProperty("Set Collision", 24, 16, 0, 1) ]])
fnCutsceneInstruction([[ AL_SetProperty("Set Collision", 25, 16, 0, 1) ]])
fnCutsceneInstruction([[ AL_SetProperty("Set Collision", 26, 16, 0, 1) ]])