--[Root]
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

--[Arguments]
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--[Name Resolve]
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

    --Dialogue.
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] 55, she left us alone...[SOFTBLOCK] Get up...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Neutral] Come on...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Find other.[SOFTBLOCK] Find other.[SOFTBLOCK] Bring other.[SOFTBLOCK] Make other new.[SOFTBLOCK] New.[SOFTBLOCK] New.)") ]])
    fnCutsceneBlocker()
	

end