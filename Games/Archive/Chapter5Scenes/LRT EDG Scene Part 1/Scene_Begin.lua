--[LRT Eldritch Dream Girl Part 1]
--This takes place in the cafeteria of LRTG.
		
--Music stops.
AL_SetProperty("Music", "Null")

--Party moves forward.
fnCutsceneMove("Christine", 16.25, 10.50)
fnCutsceneMove("55",        17.25, 10.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] We're here.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] We -[SOFTBLOCK] are?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The schematics indicate that terminals in this area should have direct access to the data core.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] It's quiet in here, isn't it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Where are all the drones?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Hopefully, someplace far away.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--55 finds the terminal.
fnCutsceneMove("55", 17.25, 8.50)
fnCutsceneMove("55", 24.25, 8.50)
fnCutsceneFace("55", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneInstruction([[ AL_SetProperty("Open Door", "CanteenDoor") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneMove("55", 24.25, 6.50)
fnCutsceneMove("55", 20.25, 4.50)
fnCutsceneFace("55", 0, -1)
fnCutsceneMove("Christine", 16.25, 8.50)
fnCutsceneMove("Christine", 21.25, 8.50)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] ...[SOFTBLOCK] Yes, this terminal will do.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Can you access the core?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[SOFTBLOCK] I have a lot of queries I need to run.[SOFTBLOCK] Keep watch.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So...[SOFTBLOCK] I just stand here and look pretty?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I guess I'm pretty good at both of those jobs![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Be quiet.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine hears something.
fnCutsceneFace("Christine", -1, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 1)
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55...[SOFTBLOCK] do you hear that?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] No.[SOFTBLOCK] I am busy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] It sounds like someone singing...[SOFTBLOCK] what a beautiful voice...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I've never heard someone sing like that...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] ...[SOFTBLOCK] I do not have cycles to waste, and a unit may hear you.[SOFTBLOCK] Be quiet.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine walks away.
fnCutsceneMove("Christine", 18.25, 8.50, 0.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 17.25, 9.50, 0.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (It's coming from over here...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] 55?[SOFTBLOCK] Do you mind if I...?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Offended] Do not bother me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (I will take that as a yes...)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine moves over and sees the empty security checkpoint.
fnCutsceneMove("Christine", 9.75, 11.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (The security checkpoint is totally abandoned.[SOFTBLOCK] Where are all the drones?)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine looks left and has a dialogue.
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Maybe they went to that beautiful voice, too?)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Christine", 2.75, 11.50)
fnCutsceneBlocker()

--Darken the screen while Christine is moving.
fnCutsceneMove("Christine", 2.75, 5.50)
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(85)
fnCutsceneBlocker()

--Transition maps.
fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusLRTH", "FORCEPOS:26.5x28.0x0") ]])
fnCutsceneBlocker()