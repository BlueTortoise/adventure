--[LRT Eldritch Dream Girl Part 2]
--Hallways of LRTH.

--Christine's form.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Black the screen out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(85)
fnCutsceneBlocker()

--Spawn 56.
TA_Create("56")
	TA_SetProperty("Position", 0, 0)
	TA_SetProperty("Facing", gci_Face_East)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/56/", true)
DL_PopActiveObject()

--Move 55 offscreen.
fnCutsceneTeleport("55", -100.25, -100.50)
fnCutsceneBlocker()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Christine moves north.
fnCutsceneMove("Christine", 26.75, 20.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, -1)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(125)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, -1)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 26.75, 9.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, -1)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] (Hmm, it was getting closer, and now I can't hear it at all.[SOFTBLOCK] Maybe they stopped?[SOFTBLOCK] Perhaps I should call out to them...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hello?[SOFTBLOCK] Is anyone there?") ]])
fnCutsceneBlocker()

--Teleport 56 in.
fnCutsceneTeleport("56", 26.75, 23.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("56", 26.75, 10.50, 1.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Oh, there you are.[SOFTBLOCK] Did you hear the singing, too?[BLOCK][CLEAR]") ]])

--Dialogue branches based on Christine's form.
if(sChristineForm == "Human") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] There are no humans assigned to this facility, and you lack a security pass.[SOFTBLOCK] Are you, perhaps, some kind of idiot?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Do you have any idea the extreme danger you have put a superior unit in by virtue of being here?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Nice to see you, too, 55.[SOFTBLOCK] Sheesh.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sorry for wandering off like that, but you don't have to be so harsh all the time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] 55..?[SOFTBLOCK][SOFTBLOCK] Then, are you Christine?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Of course I am.[SOFTBLOCK] Is your memory on the fritz again?[SOFTBLOCK] Do you need me to take a look at it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Then you're the one who found -[SOFTBLOCK] !!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Follow me![SOFTBLOCK] Quickly!") ]])
    
elseif(sChristineForm == "Golem") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You![SOFTBLOCK] Who are you, and how did you get in here?[SOFTBLOCK] There's only supposed to be drone units assigned to this area![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] You're not the sort to try jokes, 55...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] It's me![SOFTBLOCK] Christine![SOFTBLOCK] Sheesh, is your memory on the fritz again?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] Christine...[SOFTBLOCK] But that's the same name as -[SOFTBLOCK] !![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Follow me![SOFTBLOCK] Quickly!") ]])

elseif(sChristineForm == "LatexDrone") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Drone Unit![SOFTBLOCK] I ordered this area cleared![SOFTBLOCK] Where is your squad leader?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What, you don't recognize me?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] *Ahem*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] GREETINGS, COMMAND UNIT 2855.[SOFTBLOCK] THIS UNIT APOLOGIZES FOR WANDERING OFF BUT IS WORRIED YOUR MEMORY IS FRIED.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] THIS UNIT RECOMMENDS HAVING HER EXAMINE YOUR HEAD FOR DEFECTIVE SECTORS.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] I am not amused, Drone Unit.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Wait, did you say Command Unit 2855?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] AFFIRMATIVE.[SOFTBLOCK] YOU ARE COMMAND UNIT 2855.[SOFTBLOCK] UH...[SOFTBLOCK] aren't you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Yes, of course I am.[SOFTBLOCK] We have places to be.[SOFTBLOCK] Follow me, right now!") ]])
    
elseif(sChristineForm == "Darkmatter") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You there, Darkmatter![SOFTBLOCK] No -[SOFTBLOCK] no you're not.[SOFTBLOCK] You're different.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Uh, yeah, I am.[SOFTBLOCK] I think that's pretty obvious.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I mean, the others are far less talktative, right?[SOFTBLOCK] Ha ha ha![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And once again the joke sails over 55's head.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...[SOFTBLOCK] 55?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Are you talking to yourself or something?[SOFTBLOCK] Are your memory sectors shot?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] If you think I am -[SOFTBLOCK] then that means -[SOFTBLOCK] oh no.[SOFTBLOCK] Damn it![SOFTBLOCK] DAMN IT![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Christine, on me![SOFTBLOCK] Quickly![SOFTBLOCK] We haven't much time!") ]])
    
elseif(sChristineForm == "Electrosprite") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] It can talk, and appears to be holding humanoid form.[SOFTBLOCK] But you are not the vector.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] The vector?[SOFTBLOCK] What?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Be silent.[SOFTBLOCK] I am thinking.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] No need to be so harsh, 55.[SOFTBLOCK] What's this about a vector?[SOFTBLOCK] Did you find something in the core already?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sorry about wandering off, but I thought I heard something...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Wait, did you say 55?[SOFTBLOCK] As in, Unit 2855?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Yes?[SOFTBLOCK] What?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Then you're an electrosprite, according to the vernacular -[SOFTBLOCK] and you're...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Damn it, no![SOFTBLOCK] No![SOFTBLOCK] Not right here, not right now![SOFTBLOCK] Christine, follow me![SOFTBLOCK] Quickly!") ]])
    
elseif(sChristineForm == "SteamDroid") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] A looter.[SOFTBLOCK] Perfect.[SOFTBLOCK] Exactly what I need at this juncture.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well I think the term 'scavenging' is a little more classy, but sure, looter it is.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] And I wasn't 'looting', 55.[SOFTBLOCK] I just thought I heard something.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Did you say, 55?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I did.[SOFTBLOCK] Are you all right in the processor?[SOFTBLOCK] You're acting funny.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Not an ordinary Steam Droid...[SOFTBLOCK] You'd be...[SOFTBLOCK] Christine...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Which ties in perfectly with the run of events.[SOFTBLOCK] And here I've stumbled into it.[SOFTBLOCK] Christine, follow me.[SOFTBLOCK] Now![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Ooookayyyyyy...") ]])
end

fnCutsceneBlocker()

--Running.
fnCutsceneMove("56", 26.25, 8.50, 1.50)
fnCutsceneBlocker()
fnCutsceneMove("56", 26.25, 8.50, 1.50)
fnCutsceneMove("Christine", 26.25, 9.50, 1.50)
fnCutsceneBlocker()
fnCutsceneMove("56", 25.25, 8.50, 1.50)
fnCutsceneMove("Christine", 26.25, 8.50, 1.50)
fnCutsceneBlocker()
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 1.50)
	CameraEvent_SetProperty("Focus Position", (14.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneMove("56", 14.25, 8.50, 1.50)
fnCutsceneFace("56", 0, -1)
fnCutsceneMove("Christine", 15.25, 8.50, 1.50)
fnCutsceneFace("Christine", -1, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

--Scene transition.
fnCutsceneInstruction([[ AL_BeginTransitionTo("RegulusLRTGZ", "FORCEPOS:5.0x6.0x0") ]])
fnCutsceneBlocker()