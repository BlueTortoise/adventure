--[Meeting 55 in Lower Regulus City]
--When tracking down 55 in lower Regulus City, this scene plays when examining the terminal.

--Spawn 55.
fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: Christine, this terminal has been used by Unit 2855 to access the network.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU: She will likely come this way at some point.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: So...[SOFTBLOCK] Just gotta hide someplace around here...") ]])
fnCutsceneBlocker()

--Christine walks down a bit and looks around.
fnCutsceneMove("Christine", 38.25, 5.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Hmm...[SOFTBLOCK] I have an idea...") ]])
fnCutsceneBlocker()

--Fade out.
fnCutsceneWait(105)
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Focus the camera on this position.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Position", (43.25 * gciSizePerTile), (5.50 * gciSizePerTile))
DL_PopActiveObject()

--Teleport Christine away.
fnCutsceneTeleport("Christine", -100.25, -100.50)

--Fade back in.
fnCutsceneWait(105)
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Teleport 55 in.
fnCutsceneTeleport("55", 43.25, 5.50)
fnCutsceneWait(35)
fnCutsceneBlocker()
fnCutsceneFace("55", -1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("55", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("55", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Focus the camera on this position.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Position", (38.25 * gciSizePerTile), (5.50 * gciSizePerTile))
DL_PopActiveObject()

--55 Moves to the console.
fnCutsceneMove("55", 43.25, 9.50)
fnCutsceneMove("55", 38.25, 9.50)
fnCutsceneMove("55", 38.25, 5.00)
fnCutsceneBlocker()

fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855: ...[SOFTBLOCK] This console has been locked out from the network, too.[SOFTBLOCK] What sort of glitch is this?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] It was no glitch.") ]])
fnCutsceneBlocker()

--Teleport Christine in.
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneTeleport("Christine", 36.25, 7.50)
fnCutsceneFace("55", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] I've been waiting for hours to say that![SOFTBLOCK] Ha ha ha![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] But -[SOFTBLOCK] there was nothing on my motion tracker![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] All I had to do was stay perfectly still for six hours.[SOFTBLOCK] Sucker!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine moves to the exit.
fnCutsceneMove("Christine", 38.25, 7.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Listen, 2855, I need to - [SOFTBLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Angry] Shut up and fight!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()


fnCutsceneMove("55", 38.25, 6.50, 3.00)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "*ZAP*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] Gyaaaa!!![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] That ought to do it...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
Cutscene_CreateEvent("Flash Christine White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Flashwhite", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()
fnCutsceneWait(70)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] Oof...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Fascinating.[SOFTBLOCK] The reports were correct.[SOFTBLOCK] When placed in extreme danger, the rune bearer reverts to their true form.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine stands up.
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, 1)
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Turn around.
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Did you say...[SOFTBLOCK] true form?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smug] You are durable beyond expectations.[SOFTBLOCK] I am impressed that you are able to stand after a shock like that.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] This is my true form?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] That is what the reports said, but I did not believe them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] The process could have been painless, but you decided not to take the easy way.[SOFTBLOCK] I - [SOFTBLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Unit 2855![SOFTBLOCK] Thank you so much![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] ...[SOFTBLOCK] Seriously?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "*Hug*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] I could kiss you![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "*Smooch*[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Offended] Will you stop that?[SOFTBLOCK] You'll get lip gloss on my chassis.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] I'm -[SOFTBLOCK] I'm wearing lip gloss![SOFTBLOCK] Squeeeeeeeee!![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] I am afraid we do not have the time to research how you were able to manifest cosmetics with your runestone.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] If you refuse to follow my orders, I will not hesitate to electrocute you again.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] What?[SOFTBLOCK] Oh, no.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] 2855![SOFTBLOCK] I want to help you![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] You -[SOFTBLOCK] you do?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Terribly sorry about earlier.[SOFTBLOCK] You see -[SOFTBLOCK] I didn't want to be a -[SOFTBLOCK] a - [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Human?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] [SOFTBLOCK]...[SOFTBLOCK][SOFTBLOCK][E|Neutral] A man.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] But -[SOFTBLOCK] this is my true form, isn't it?[SOFTBLOCK] I guess I've always known that...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Sophie will probably prefer Golem me.[SOFTBLOCK] But that's okay, I can always switch back![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] You've changed your mind and wish to assist me?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, yes. Of course.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] But I will not tolerate this ordering me around.[SOFTBLOCK] If we're doing this, we're to be partners.[SOFTBLOCK] Equals.[SOFTBLOCK] Understand?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] I do not have time to consult you on every decision I make.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Then you're going to make time.[SOFTBLOCK] You do this with me, or you do it alone.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] (Calculating...)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] I am computing that compromising will be more efficient than constantly electrocuting you,[E|Smug] if much less amusing.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I will share authority with you, since I have no other option.[SOFTBLOCK] Your offer is accepted.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Capital![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Now, your memory wipe...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Unfortunately the process is not reversible.[SOFTBLOCK] Worse, I have been unable to locate any information on my past whatsoever.[SOFTBLOCK] I still do not know what happened at the Cryogenics Facility.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] It seems my records have been expunged from the network.[SOFTBLOCK] Not classified, expunged.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[EMOTION|Christine|PDU] I can confirm that.[SOFTBLOCK] The only records will be stored on eyewitnesses at this point.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Sad] And going around asking about you would bring a lot of bad attention...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] The only outstanding document I found concerning my designation was a Priority One order to bring me in for questioning.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Attempting to ask any unit who may have known me would likely bring security teams down on our heads.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sophie said that Unit 2855 was a Prime Unit, former Head of Security.[SOFTBLOCK] Does that sound right?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] It -[SOFTBLOCK] clears up a great deal, actually.[SOFTBLOCK] I seem to have exemplary hacking and combat skills.[SOFTBLOCK] If I was a high-ranking security unit, it would also explain my presence at a battlefield.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] We happen to think a revolt took place.[SOFTBLOCK] You were -[SOFTBLOCK][E|Sad] possibly...[SOFTBLOCK] the leader.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] A conclusion I had also arrived at, considering the administration's expunging of my records.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Upset] No doubt this is why my authenticator chip, and those of all the other units, were removed and destroyed.[SOFTBLOCK] The administrators can track the chips very easily if they are not specially modified.[SOFTBLOCK] Easier to remove them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I cannot say what my objective was, though.[SOFTBLOCK] Was I a leader, or merely a participant?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Given how you've dealt with me, I'd say it's more likely you were leading them.[SOFTBLOCK] They were fighting for freedom, and you aided them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I see why you have decided to aid me.[SOFTBLOCK] You are a class traitor.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] Doubtless this involves Unit 499323.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You leave Sophie out of this.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] At your insistence...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] So what do we do now?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I am not satisified that 'freedom' would be the goal of any action I took.[SOFTBLOCK] There must have been some greater design.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] But your records were expunged.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Not quite.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Regulus City has a number of backup servers at the Long Range Telemetry facility.[SOFTBLOCK] They aggregate all network traffic and archive it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] They're highly restricted, but I believe that we can infiltrate the facility.[SOFTBLOCK] With your aid.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Me?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] There was a reason that I flagged you as a Maintenance and Repair unit.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] You got me that function assignment?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Yes.[SOFTBLOCK] I have also updated your permission set.[SOFTBLOCK] PDU, please list her permissions.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "PDU:[EMOTION|Christine|PDU] Unit 771852 is a registered expert at reconnaisance, infiltration, close-quarters combat, and search-and-destroy missions.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] What?![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] With these, you should be able to access nearly any restricted area without suspicion.[SOFTBLOCK] No unit will question your certification.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Scared] But I'm not a - [SOFTBLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] You did not struggle with the scraprats in the Cryogenics Facility.[SOFTBLOCK] You will do fine.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Sheesh...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Your access permissions will get us past low-security locked doors at the LRT facility, and I should be able to hack into the data core to get what we need.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I have already fabricated modified authenticator chips which can broadcast dummy signals to fool basic software.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] Predictably they will try to stop us, but I've already taken the liberty of putting packet sniffers on the LRT's network traffic.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] They will not be able to call for help.[SOFTBLOCK] At least not until we've hacked the core and expunged all records of our activities.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Whenever we need to move anonymously, you can simply transform, and they will be blind.[SOFTBLOCK] There is no way security protocols are prepared for someone like you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You're already several steps ahead of me.[SOFTBLOCK] I can see why you're a Command Unit.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] Indeed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] Now, it's best that I am not seen with you in Regulus City.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] There is a transit station at the north end of Sector 96 on the main floor.[SOFTBLOCK] We can take it to the Telemetry facility.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I will move through the ventilation system and meet you there when you wish to depart.[SOFTBLOCK] Maintain your cover as a Lord Unit.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Hmm...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'll try to get a maintenance assignment there.[SOFTBLOCK] I'll get my PDU to contact you when we're ready to go.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Smirk] Good.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] There are other exits to the facility, as well.[SOFTBLOCK] I can meet you if you wish to venture onto the surface.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] There are other areas we should investigate.[SOFTBLOCK] We may need allies and equipment.[SOFTBLOCK] I will leave that choice to you.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] And thank you for performing your function, Unit 771852.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You can call me Christine, you know.[SOFTBLOCK] Hey, what's your secondary designation?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "2855:[E|Neutral] I do not have a secondary designation.[SOFTBLOCK] Refer to me as Unit 2855.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] How about I just call you 55 for short?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smug] I don't think it will stick, but I will not waste more time arguing this point.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Okay.[SOFTBLOCK] I'm going back to 'work'.[SOFTBLOCK] Good luck, 55.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Same to you.[SOFTBLOCK] Christine.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "(For simplicity, 55 is now in your party even when not present.[SOFTBLOCK] You can change her equipment at any time.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "(Unit 2855 may change her algorithms in battle to Assault, Support, and Restore modes.[SOFTBLOCK] Each has changes in statistics and abilities.[SOFTBLOCK] You may change algorithms once per turn without ending your turn.[SOFTBLOCK] She starts each battle in Assault mode.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "(Christine has gained several leadership abilities as well.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "(The 'Take Point' ability will increase her likelihood of being targeted by enemies by increasing her threat.[SOFTBLOCK] 'Line Formation' toggles the threat off.[SOFTBLOCK] Neither consumes her turn.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "(She also now knows the 'Encourage' ability, which buffs the party, and will find more party support abilities as she progresses.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "(Oh, and because this is a prototype, Christine and 2855 now have all of their advanced abilities.[SOFTBLOCK] You will need to find these in later prototypes, but they cannot be found in this prototype.[SOFTBLOCK] Enjoy!)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--55 goes into the vents.
fnCutsceneMove("Christine", 37.25, 7.50)
fnCutsceneFace("Christine", 1, 0)
fnCutsceneBlocker()
fnCutsceneMove("55", 38.25, 9.50)
fnCutsceneMove("55", 43.25, 9.50)
fnCutsceneMove("55", 43.25, 5.50)
fnCutsceneBlocker()

--55 Vanishes.
fnCutsceneTeleport("55", -100.25, -100.50)
fnCutsceneBlocker()
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 1.0)
	CameraEvent_SetProperty("Focus Actor Name", "Christine")
DL_PopActiveObject()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Add 55 to the party lineup. She doesn't leave even when she hides in Regulus City.
AC_SetProperty("Set Party", 1, "55")
LM_ExecuteScript(gsItemListing, "Pulse Diffractor")
LM_ExecuteScript(gsItemListing, "Command Unit Garb")
AC_PushPartyMember("55")
	--Equip.
	ACE_SetProperty("Equip", "Weapon", "Pulse Diffractor")
	ACE_SetProperty("Equip", "Armor", "Command Unit Garb")
	
	--55 starts the game at level 4.
	ACE_SetProperty("EXP", 17 + 66 + 115 + 1)
DL_PopActiveObject()

--Activate Christine's "Take Point" ability, and level her to 4 if she wasn't already.
AC_PushPartyMember("Christine")

	--Gain XP difference to level 4.
	local iChristineEXP = ACE_GetProperty("XP Total")
	if(iChristineEXP < 17 + 66 + 115) then
		ACE_SetProperty("EXP", (17 + 66 + 115) - iChristineEXP + 1)
	end

	--Un-hide the ability.
	ACE_PushAction("Take Point")
		ACAC_SetProperty("Visible In State", "Standard")
	DL_PopActiveObject()
DL_PopActiveObject()

--Give Christine the "Encourage" ability since she now actually has a party to encourage.
VM_SetVar("Root/Variables/Global/Christine/iSkillbook01", "N", 1.0)
sAbilityPath = gsRoot .. "Abilities/Christine/000 Initializer.lua"
AC_PushPartyMember("Christine")
	if(ACE_GetProperty("Has Ability", "Encourage") == false) then
		LM_ExecuteScript(sAbilityPath, "Encourage")
	end
	if(ACE_GetProperty("Has Ability", "Sweep") == false) then
		LM_ExecuteScript(sAbilityPath, "Sweep")
	end
	if(ACE_GetProperty("Has Ability", "Rally") == false) then
		LM_ExecuteScript(sAbilityPath, "Rally")
	end
	if(ACE_GetProperty("Has Ability", "Officer Charge") == false) then
		LM_ExecuteScript(sAbilityPath, "Officer Charge")
	end
DL_PopActiveObject()
