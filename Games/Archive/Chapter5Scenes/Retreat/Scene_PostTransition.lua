--[Scene Post-Transition]
--After transition, the party quickly reforms.

--[Variables]
local bIs55Present = fnIsCharacterPresent("55")
local bIsSX399Present = fnIsCharacterPresent("SX399")

--[Special]
--If using the TelluriumMinesE deployment, we need to change positions.
local sLastSave = AL_GetProperty("Last Save")
if(sLastSave == "SprocketCityA") then
    fnCutsceneTeleport("Christine", 38.25, 38.50)
    fnCutsceneTeleport("55",        38.25, 38.50)
end

--[Overlay]
--Set the overlay to fullblack. Fade in slowly.
AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Wounded]
--Move 55 so she's next to the campfire.
if(bIs55Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", 16.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Move Amount", 0.0, 16.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

--SX-399 does the same thing.
if(bIsSX399Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "SX399")
		ActorEvent_SetProperty("Move Amount", -16.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "SX399")
		ActorEvent_SetProperty("Move Amount", 0.0, 16.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

--[Fade In]
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Change Christine to the "Crouch" frames.
Cutscene_CreateEvent("Change Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--If 55 is present, set her to downed as well.
if(bIs55Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end
if(bIsSX399Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "SX399")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--[Crouch]
--Christine gets up.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--55, if present, gets up.
if(bIs55Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "55")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end
if(bIsSX399Present) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "SX399")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--[Normal]
--Christine stands up.
Cutscene_CreateEvent("Change Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneBlocker()

--55, if present, gets up.
if(bIs55Present or bIsSX399Present) then
	
	--Facing down.
    if(bIs55Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "55")
            ActorEvent_SetProperty("Face", 0.0, 1.0)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "55")
            ActorEvent_SetProperty("Special Frame", "Null")
        DL_PopActiveObject()
    end
    if(bIsSX399Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "SX399")
            ActorEvent_SetProperty("Face", 0.0, 1.0)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "SX399")
            ActorEvent_SetProperty("Special Frame", "Null")
        DL_PopActiveObject()
    end
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Turn.
    if(bIs55Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "55")
            ActorEvent_SetProperty("Face", -1.0, 1.0)
        DL_PopActiveObject()
    end
    if(bIsSX399Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "SX399")
            ActorEvent_SetProperty("Face", 1.0, 1.0)
        DL_PopActiveObject()
    end
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
    if(bIs55Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "55")
            ActorEvent_SetProperty("Face", -1.0, 0.0)
        DL_PopActiveObject()
    end
    if(bIsSX399Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "SX399")
            ActorEvent_SetProperty("Face", 1.0, 0.0)
        DL_PopActiveObject()
    end
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
    if(bIs55Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "55")
            ActorEvent_SetProperty("Face", -1.0, -1.0)
        DL_PopActiveObject()
    end
    if(bIsSX399Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "SX399")
            ActorEvent_SetProperty("Face", 1.0, -1.0)
        DL_PopActiveObject()
    end
	fnCutsceneWait(5)
	fnCutsceneBlocker()

end

--Fold the party.
if(bIs55Present == true or bIsSX399Present == true) then
    if(bIs55Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "55")
            ActorEvent_SetProperty("Move Amount", 0.0, -16.0)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "55")
            ActorEvent_SetProperty("Move Amount", -16.0, 0.0)
        DL_PopActiveObject()
        fnCutsceneBlocker()
    end
    if(bIsSX399Present) then
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "SX399")
            ActorEvent_SetProperty("Move Amount", 0.0, -16.0)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "SX399")
            ActorEvent_SetProperty("Move Amount", 16.0, 0.0)
        DL_PopActiveObject()
        fnCutsceneBlocker()
    end

	fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
