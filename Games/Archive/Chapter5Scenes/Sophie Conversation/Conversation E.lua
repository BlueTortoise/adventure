--[Sophie Conversation E]
--This sequence plays during one of the dates with Sophie.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The tandem units cheerfully chatted with one another as their lower-level routines did their work.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie kept her smile, even when the conversation took turns to dour territory.[SOFTBLOCK] Nothing made her happier than being with Christine.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "They would take lulls in their work schedule to hold hands and nuzzle one another's olfactory sensors.[SOFTBLOCK] No display of affection was too minor.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "They worked together in perfect harmony...") ]])