--[Document: The Doll and the Bolts]
--Such a tragic tale.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine and Sophie linked their cables together and plugged into the computer.[SOFTBLOCK] The document loaded and began transmitting into their architectures.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "They began to read a story about a Command Unit who,[SOFTBLOCK] after a particularly productive day that she had overseen,[SOFTBLOCK] saw a pair of high-quality tungsten bolts sitting atop a high shelf.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She did not have any particular need for the bolts, but they were of excellent quality and precision, and they shone with a shimmering luster.[SOFTBLOCK] Thoughts of what she could use such perfect bolts for filled her mind.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The shelf, however, was high above the ground, and not only was there no ladder around,[SOFTBLOCK] but she could find no other units to use as a stool.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She reached for them anyway, stretching as high as her arms and legs would hold her,[SOFTBLOCK] but she could not reach them.[SOFTBLOCK] No amount of stretching and straining would allow her to reach the bolts.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She began to pout and grow angry as the bolts taunted her from beyond her reach.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "'I don't need those stupid bolts!' she shouted. 'I'll bet they're rusted, anyway!'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The Command Unit stormed off with a huff, abandoning the bolts to the shelf.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her deductions were correct, of course.[SOFTBLOCK] The surface of the bolts only appeared to shine due to many years of metal shaving dust.[SOFTBLOCK] Beneath the dust, the bolts were rusted throughout, and would have broken if used for even the least of her projects.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The moral of the story is that Command Units are wise beyond our understanding,[SOFTBLOCK] and perfect in every way without need for improvement.") ]])

fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Huh.[SOFTBLOCK] What an odd story.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] What's odd about it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Doesn't the moral of the story seem a little out of place?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] But it's true.[SOFTBLOCK] That's the moral for almost all the stories involving Command Units.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Oh, I know it's true and that Command Units are perfect, but I thought the moral would be something like, 'You can't always get what you want.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] That's pretty clever![SOFTBLOCK] But I think it would be more appropriate for a story about a golem.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I suppose you're right.") ]])
fnCutsceneBlocker()