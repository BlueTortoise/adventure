--[Routing]
--Routes the reading scripts. The argument should be the name of the document.
local sBasePath = fnResolvePath()

--[Arguments]
--Argument Listing:
-- 0: sDocumentName - Which document to read.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sDocumentName = LM_GetScriptArgument(0)

--Call scripts as necessary.
if(sDocumentName == "DollBolts") then
	LM_ExecuteScript(sBasePath .. "Reading DollBolts.lua")
elseif(sDocumentName == "That") then
	LM_ExecuteScript(sBasePath .. "Reading That.lua")
elseif(sDocumentName == "DroneMirror") then
	LM_ExecuteScript(sBasePath .. "Reading DroneMirror.lua")
end