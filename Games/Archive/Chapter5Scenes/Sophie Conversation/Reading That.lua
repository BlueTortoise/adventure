--[Document: That]
--Such a tragic tale.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine and Sophie linked their cables together and plugged into the computer.[SOFTBLOCK] The document loaded and began transmitting into their architectures.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "They began to read a story about a group of bee girls who were working to establish a new hive.[SOFTBLOCK] They were dilligent and hard-working, and always put the needs of the hive first, which allowed them to become very successful.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the young hive grew stronger, the happy workers began to notice something following them.[SOFTBLOCK] Sometimes the creature appeared to be a slime,[SOFTBLOCK] other times it looked like a cat,[SOFTBLOCK] and occasionally,[SOFTBLOCK] it even looked like a ghost.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "No matter how it looked, it would attack them whenever it saw them, leaving them too afraid to do their jobs.[SOFTBLOCK] Over time, this lack of productivity caused the thriving colony to slip into decline.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the colony grew more fearful, the creature grew stronger, and soon the bee girls realized that it fed on the colony's fear.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Determined to grow their colony and help each other achieve maximum productivity, the hive came up with a plan that would destroy the creature's hold over their colony.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The hive decided to organize an orgy, to which the creature would be invited.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The workers began the preparations at once, and the creature came, expecting to feast upon the bee girls and their fear.[SOFTBLOCK] But even as the creature entered the colony for its victorious feast, it found its powers had vanished![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It was suddenly very fearful, but as it sought to escape, the bees usherd it into the hive where they ravished it with their bodies for the glory of their hive.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The creature found itself happy and fulfilled in a way it had not known before, and as the passions of the evening slowed, it graciously accepted a gift of honey from the bee girls.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Little did the creature know that the honey was, in fact, the necessary catalyst to defeat the creature,[SOFTBLOCK] and in consuming it, the creature's body shifted to a new shape and became like the bees' own bodies.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Now changed, the new bee girl accepted the wisdom of the bee girls, and lived its life as a loyal and productive member of the hive, and helped the hive grow to become the greatest hive around.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The moral of the story is that assimilation is better than elimination.") ]])

fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] That took one hell of a turn, didn't it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Love conquers all.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Or in this case,[SOFTBLOCK] sex and[SOFTBLOCK][SOFTBLOCK] honey.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I destroy my enemy when I make her like me.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sheesh.") ]])
fnCutsceneBlocker()