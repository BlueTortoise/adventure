--[Routing]
--Routes the conversation scripts.
local sBasePath = fnResolvePath()
local iConversationCounter = VM_GetVar("Root/Variables/Chapter5/Sophie/iConversationCounter", "N")

--Call scripts as necessary.
if(iConversationCounter == 0.0) then
	LM_ExecuteScript(sBasePath .. "Conversation A.lua")
elseif(iConversationCounter == 1.0) then
	LM_ExecuteScript(sBasePath .. "Conversation B.lua")
elseif(iConversationCounter == 2.0) then
	LM_ExecuteScript(sBasePath .. "Conversation C.lua")
elseif(iConversationCounter == 3.0) then
	LM_ExecuteScript(sBasePath .. "Conversation D.lua")
else
	LM_ExecuteScript(sBasePath .. "Conversation E.lua")
end

--Increment the conversation counter.
VM_SetVar("Root/Variables/Chapter5/Sophie/iConversationCounter", "N", iConversationCounter + 1)