--[Routing]
--Routes the videograph scripts. The argument should be the name of the videograph.
local sBasePath = fnResolvePath()

--[Arguments]
--Argument Listing:
-- 0: sVideographName - Which videograph to watch.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sVideographName = LM_GetScriptArgument(0)

--Call scripts as necessary.
if(sVideographName == "VideoRoberta") then
	LM_ExecuteScript(sBasePath .. "Videograph Roberta.lua")
elseif(sVideographName == "VideoDMG") then
	LM_ExecuteScript(sBasePath .. "Videograph DarkMatterGirl.lua")
elseif(sVideographName == "VideoSlime") then
	LM_ExecuteScript(sBasePath .. "Videograph Slime.lua")
elseif(sVideographName == "VideoBomb") then
	LM_ExecuteScript(sBasePath .. "Videograph Strangelover.lua")
end