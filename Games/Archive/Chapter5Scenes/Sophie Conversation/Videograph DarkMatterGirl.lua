--[Videograph: A Talking Dark Matter Girl!?!]
--Such a horrible tale.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the lights in the room dim, the videograph begins playing.[SOFTBLOCK] A long series of shots pan over the moonscape as an apparently-drunk narrator describe the scenes as they appear.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The voice ends with an audible hiccup as a group of survey units stumble upon a Dark Matter Girl.[SOFTBLOCK] The surveyors decide to adopt her and bring her along on their mission, where she begins talking to them.[SOFTBLOCK] She's a talking Dark Matter Girl?!?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The videograph cuts away to a completely unrelated pair of units as they argue over mundane familial issues for no apparent reason.[SOFTBLOCK] One of the units wants to be a tandem unit with the other, who remains undecided.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The Dark Matter Girl suddenly enters the scene, and after a monotonous monologue over what is apparently supposed to be a montage, she helps the two units become tandem units.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As she's leaving the domestic block, the Dark Matter Girl is struck by an out-of-control tram.[SOFTBLOCK] She lives with no apparent injuries, but rushes off after being startled by an elongated, unmoving green vegetable.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The overall acting is terrible, and the videograph ends without ever returning to the original group of surveyors or attempting to explain how the Dark Matter Girl could speak.") ]])

fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I think my CPU is running slower now that we watched that.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] The Dark Matter Girl was cute![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] ...[SOFTBLOCK] Are you one of those units who looks up pictures of Dark Matter Girls on the station network?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] ...[SOFTBLOCK] Maybe.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Do you watch videographs of them doing cute things like knocking things over and chasing lights?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] ...[SOFTBLOCK]...[SOFTBLOCK]...[SOFTBLOCK][E|Happy] maybe.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] No problem with that, but that videograph was still bad.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Hee hee hee![SOFTBLOCK] We should rewatch it just to see the cute girls again!") ]])
fnCutsceneBlocker()