--[Videograph: Slime Girls Are Easy]
--Such a tragic tale. Yep.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the lights in the room dim, the videograph begins playing.[SOFTBLOCK] Out-of-tune and off-beat saxophone music begins playing as a poorly-made title card with terrible kerning comes up,[SOFTBLOCK] 'Rime Gihs Are Easy'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The scene opens up in an unnamed forest on Pandemonium.[SOFTBLOCK] A pair of hikers are bragging to each other about their latest 'conquests' when they encounter a slime girl.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "One of the pair dares the other to 'conquer' the slime girl, and is himself dared the same by his friend.[SOFTBLOCK]The pair quickly begin violating the porous body of the slime, only to find themselves being turned into slime girls, as well.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The two newly-made slime girls are soon having sex with each other as well as the original slime girl, and seem to immediately switch partners with each climax.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "All three slimes are the same color, making it difficult to see who is penetrating what and where,[SOFTBLOCK] or if any penetration is happening at all as opposed to simply merging and separating.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Overall, it's difficult to tell what's going on, and the videograph mercifully fades out after 30 minutes as the slime girls continue the rather-unerotic orgy.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Ohhhh, I'd love to couple with a slime girl...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You would?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Heh.[SOFTBLOCK] Are you not into that?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Slimes don't have to worry about things like work and fitting in.[SOFTBLOCK] They just ooze over things and have sex whenever they want.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That's a porno, not real life.[SOFTBLOCK] They probably have to worry about getting eaten.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Maybe.[SOFTBLOCK] I still wish I could couple with one.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Just once.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] I could put in a request with the abductions units...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Eek![SOFTBLOCK] Don't![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Mwahaha!") ]])