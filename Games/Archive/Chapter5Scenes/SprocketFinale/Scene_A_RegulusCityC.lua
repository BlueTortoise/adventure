--[Scene A: Regulus City]
--In this Scene, Sophie finally meets 55 and starts to upgrade SX-399. This is called by a trigger
-- script in RegulusCityC.

--[Actor Spawning]
--Special characters.
if(EM_Exists("Sophie") == false) then
	fnSpecialCharacter("Sophie", "Golem", 18, 16, gci_Face_North, false, nil)
else
	EM_PushEntity("Sophie")
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()
	fnCutsceneTeleport("Sophie", 18.25, 16.50)
end
fnSpecialCharacter("55",     "Doll",       -100, -100, gci_Face_North, false, nil)
fnSpecialCharacter("SX399",  "SteamDroid", -100, -100, gci_Face_North, false, nil)

--Generics.
TA_Create("ScanGolem")
	TA_SetProperty("Position", 19, 15)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
DL_PopActiveObject()

--[Scene]
--Black out the screen.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Focus the camera on Sophie.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Actor Name", "Sophie")
DL_PopActiveObject()

--Move Christine and JX-101 off the stage.
fnCutsceneTeleport("Christine", -100.0, -100.0)
fnCutsceneTeleport("JX101", -100.0, -100.0)
fnCutsceneBlocker()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] All right, your scan checks out.[SOFTBLOCK] You're good to go.[SOFTBLOCK] See you in a month![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] By the way, is 771852 here?[SOFTBLOCK] I haven't seen her in a while.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[VOICE|Sophie] She said she was doing important administrative work.[SOFTBLOCK] Probably trying to get me a new Crogsvield Welder.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Golem:[VOICE|Golem] A what?[SOFTBLOCK] Nevermind, see you later, 499323!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Sophie",    19.25, 16.50)
fnCutsceneMove("Sophie",    19.25, 13.50)
fnCutsceneFace("Sophie",     1, 0)
fnCutsceneMove("ScanGolem", 19.25, 13.50)
fnCutsceneMove("ScanGolem", 29.25, 13.50)
fnCutsceneBlocker()
fnCutsceneTeleport("ScanGolem", -100.25, -100.50)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[VOICE|2855] It's just up this ladder...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneBlocker()
fnCutsceneTeleport("55", 11.25, 14.50)
fnCutsceneTeleport("SX399", 11.25, 14.50)
fnCutsceneBlocker()
fnCutsceneMove("Sophie", 13.25, 13.50)
fnCutsceneMoveFace("55", 12.25, 14.50, -1, 0)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Steam") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] So this is the SX-399 I've been hearing about.[SOFTBLOCK] And you must be 55![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Upset] We don't have time for pleasantries.[SOFTBLOCK] Her core has been losing power at an alarming rate.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Surprised] Oh my![SOFTBLOCK] Quick, get her to my scanning platform and hook her to the secondary system!") ]])
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Sophie", 19.25, 13.50, 1.70) 
fnCutsceneMove("Sophie", 19.25, 16.50, 1.70) 
fnCutsceneMove("Sophie", 18.25, 16.50, 1.70) 
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneMoveFace("55",    20.25, 14.50, -1, 0, 1.50)
fnCutsceneMoveFace("SX399", 19.25, 14.50,  0, 1, 1.50)
fnCutsceneBlocker()
fnCutsceneFace("55", -1, 1)
fnCutsceneMoveFace("SX399", 19.25, 15.50,  0, 1, 0.20)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Steam") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 2, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Okay, she's hooked in to my systems...[SOFTBLOCK] but the scanning software isn't much help.[SOFTBLOCK] Guess I have to do it the old fashioned way.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Are you all right, SX-399?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] ....... Ha haaaa, oh I'm charging up![SOFTBLOCK] Wow![SOFTBLOCK] Woooow![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] This is great![SOFTBLOCK] I didn't think we were going to make it but wo[SOFTBLOCK]o[SOFTBLOCK]o[SOFTBLOCK]oahhh![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] I'm so relieved![SOFTBLOCK] We made it![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] We're not done yet, 55.[SOFTBLOCK] This...[SOFTBLOCK] is going to take some real work.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] What can I do to help?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Cover the door.[SOFTBLOCK] Close it, make sure nobody barges in.[SOFTBLOCK] I really don't want to have to explain this to anyone.") ]])
fnCutsceneBlocker()

--Resume.
fnCutsceneMove("55", 27.25, 13.50)
fnCutsceneFace("55", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Steam") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] So you're Sophie, are you?[SOFTBLOCK] Christine said you were pretty, but I had no idea![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Christine's really nice, doing all this for me.[SOFTBLOCK] I heard you guys were rebels and -[SOFTBLOCK] oops, am I saying stuff I shouldn't?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Because I support you all the way.[SOFTBLOCK] All we have to do is convince mother and I think we'll have a real chance![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] ...[SOFTBLOCK] How come you're not saying stuff?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Because this is...[SOFTBLOCK] bad...[SOFTBLOCK] very bad...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] You know what's wrong?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Everything...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] Can you fix it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] Fix it?[SOFTBLOCK] Fix it!?[SOFTBLOCK] Hun, there's nothing to fix![SOFTBLOCK] You're a pile of rust and scrap![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] *sigh*[SOFTBLOCK] I expected that.[SOFTBLOCK] So this was nice, but futile - [SOFTBLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] There's only one choice...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] One choice?[SOFTBLOCK] What's the choice?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Neutral] I'll need you to suspend cognitive operations for a minute.[SOFTBLOCK] Begin standby procedure.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] If you say so...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Black out the screen.
fnCutsceneWait(125)
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Level transition.
fnCutsceneInstruction([[ AL_BeginTransitionTo("SprocketCityA", "FORCEPOS:56.0x23.0x0") ]])
fnCutsceneBlocker()

