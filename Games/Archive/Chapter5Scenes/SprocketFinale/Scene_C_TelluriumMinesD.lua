--[Scene C: Mines]
--Brief finale, sets variables and reconstitutes the party.

--[Flags]
--Mark the quest as completed.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 4.0)

--Remove JX-101 from the party.
gsFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {0}
AL_SetProperty("Unfollow Actor Name", "JX101")
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsJX101Following", "N", 0.0)

--Add 55 to the party.
fnSpecialCharacter("55", "Doll", 35, 5, gci_Face_South, false, nil)
fnAddPartyMember("55", "55", "Root/Variables/Chapter5/Scenes/iIs55Following")

--[Actor Spawning]
--Character teleportation.
fnCutsceneTeleport("JX101", -100, -100)
fnCutsceneTeleport("Christine", 35.25, 6.50)
fnCutsceneFace("Christine", 0, 1)
fnCutsceneSetFrame("Christine", "Null")

--[Scene]
--Black out the screen.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade in.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Scene.
fnCutsceneMove("Christine", 35.25, 11.50)
fnCutsceneMove("55", 35.25, 10.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] That went well, though you really were cutting it close there.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I admit I was wrong.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Smirk] The Steam Droids, if they are upgraded to the Steam Lord variant, will make excellent soldiers.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Uh huh.[SOFTBLOCK] Yeah, that's it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] You're not going to mention SX-399?[SOFTBLOCK] Not going to mention that little kiss?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] [SOFTBLOCK][SOFTBLOCK]We will be comrades in arms soon enough.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Riiiight.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Remove that smile from your oral intake.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] And focus on the mission, right?[SOFTBLOCK] Sure, 55, sure.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fold the party.
fnCutsceneMove("55", 35.25, 11.50)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
