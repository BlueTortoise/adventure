--[Transform Christine to Eldritch]
--Used at save points when Christine transforms from something else to a Eldritch.

--[Variables]
--Store which form Christine started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Refuse during the gala.
local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
local iStarted55Sequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted55Sequence", "N")
if(iIsGalaTime >= 2.0 and iStarted55Sequence == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Better not take off my metal skin until *after* the gala...)") ]])
	fnCutsceneBlocker()
	return
end

--Refuse if Sophie is in the party during the Biolabs.
local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
local iSawRaijuIntro  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
if(iReachedBiolabs == 1.0 and iSawRaijuIntro == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Sophie really doesn't like dreamer-me.[SOFTBLOCK] Better not gross her out...)") ]])
	fnCutsceneBlocker()
	return
end


--[Execute Transformation]
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Christine White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Flashwhite")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()