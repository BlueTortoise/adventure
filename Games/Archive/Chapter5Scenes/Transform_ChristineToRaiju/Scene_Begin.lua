--[Transform Christine to Raiju]
--Used at save points when Christine transforms from something else to a Golem.

--[Variables]
--Store which form Christine started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iIsRaijuClothed = VM_GetVar("Root/Variables/Global/Christine/iIsRaijuClothed", "N")

--[Execute Transformation]
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Christine White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Flashwhite")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()