--[Transform Christine to Steam Droid]
--Used at save points when Christine transforms from something else to a Steam Droid.

--[Variables]
--Store which form Christine started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Refuse during the gala.
local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
local iStarted55Sequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted55Sequence", "N")
if(iIsGalaTime >= 2.0 and iStarted55Sequence == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](Better not put on my bronzed skin until *after* the gala...)") ]])
	fnCutsceneBlocker()
	return
end

--[Execute Transformation]
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Christine White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Christine")
	ActorEvent_SetProperty("Flashwhite")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_SteamDroid.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()