--[Setup]
--Variables.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")
    
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Switch to scene mode.
fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine frowned.[SOFTBLOCK] She wondered what she was going to do with the electrical disturbance now that she had trapped it in the broken console.[SOFTBLOCK] What was it?[SOFTBLOCK] Was it a voidborne partirhuman, perhaps?[SOFTBLOCK] Or maybe an AI?[SOFTBLOCK] She wasn't sure, but it wouldn't be causing any more trouble.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She flipped the breaker and cut the access point.[SOFTBLOCK] The disturbance began racing through the terminal's circuitry, but she had been thorough.[SOFTBLOCK] There was no escape.[SOFTBLOCK] She almost felt sorry for it, like a mouse in a trap, but there was no other way.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Absentmindedly, she brushed her arm against one of the exposed wires.[SOFTBLOCK] Her chassis was insulated enough to resist the current but the disturbance raced towards her. She yanked her arm back but the disturbance, somehow sensing her presence, coalesced where she had touch the wire.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It began to change itself, transforming from electrical current to photonic energy.[SOFTBLOCK] A bright light appeared and Christine tried to back away, but it was too late.[SOFTBLOCK] The light lanced towards her and touched her.[SOFTBLOCK] It seemed to be jumping from the wire to her body, and it flowed into her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She felt her mind begin to race.[SOFTBLOCK] Her body was beginning to convulse as electric charge damaged her internal systems.[SOFTBLOCK] Thoughts were coming and going as if she were having a fevered dream.[SOFTBLOCK] Her whole life was flashing before her as her body collapsed and went limp.[SOFTBLOCK] She felt the runestone's power wash over her, and she reverted to human form as she blacked out...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "From within her, the light again took form.[SOFTBLOCK] It appeared on her skin, hardened, and stepped out of her.[SOFTBLOCK] It had become a woman, made of electricity and metal insulators, made in the same form as Christine.[SOFTBLOCK] This new creature looked itself over and took stock of the room it stood in.[SOFTBLOCK] It was pleased.[SOFTBLOCK] But, her gracious host lay unconscious beneath her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine awoke with a start as a bolt of electricity hit her brain.[SOFTBLOCK] She was human again, and in a vacuum...[SOFTBLOCK] not again.[SOFTBLOCK] Not again![SOFTBLOCK] She began to panic as her lungs breathed nothing.[SOFTBLOCK] She needed to transform, but she was too weakened and her body would not let her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "There was someone standing over her, reaching down to her, but she began to feel her body go numb and cold.[SOFTBLOCK] It would be over soon...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She lost feeling in her legs and arms.[SOFTBLOCK] In a few moments she would fall unconscious again as the oxygen in her blood ran out.[SOFTBLOCK] This time, she would not reawaken.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The girl made of electricity was staring at her, unsure.[SOFTBLOCK] Why was she not moving?[SOFTBLOCK] It reached down and touched her exposed breasts, and placed a blue and red symbol on each nipple.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Then, in her extremities, she began to feel a tingling.[SOFTBLOCK] Pins and needles, like she slept on them and numbed them.[SOFTBLOCK] While she had been preparing herself for her end, she realized she was no longer breathing.[SOFTBLOCK] She no longer needed to.[SOFTBLOCK] Something about her had changed, but she could still not feel her body save the tingling in her hands and feet.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The tingling moved up her arms and legs, and she lifted her hand.[SOFTBLOCK] She could see it move but barely feel it, and realized her skin was glowing.[SOFTBLOCK] Gently, she touched her calf only to see a bolt of electricity arc out of her finger into her leg.[SOFTBLOCK] Normally, such a thing was impossible in a vacuum, but once the electricity arced out of her finger, her hand dissociated into pure energy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her veins no longer carried blood but pure electrical energy.[SOFTBLOCK] She saw her skin begin to change to become like the electricity girl who still stood over her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked up, and the girl looked down and smiled at her.[SOFTBLOCK] It placed its body close to hers, holding her arms and legs down.[SOFTBLOCK] It rubbed its chest against hers until the positive and negative nipple pasties touched...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "An incredible burst of power shot through Christine as her mind roared back to life.[SOFTBLOCK] She had been dimly aware of what was happening to her before, but no longer.[SOFTBLOCK] She had been changed, transformed into a being of pure energy, and by joining their breasts together, she had temporarily joined minds with the girl.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She realized that, when this girl had jumped into her body, it had read her mind and created a physical copy based on her own self image.[SOFTBLOCK] Information flowed through her head, but she could scarcely comprehend any of it.[SOFTBLOCK] Programs, algorithms, places, people, text...[SOFTBLOCK] It was too much.[SOFTBLOCK] The girl withdrew and broke the circuit connecting their bodies.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine quickly stood and began to touch herself all over.[SOFTBLOCK] Where her energetic hands went, electricity flowed with it.[SOFTBLOCK] She was a new breed.[SOFTBLOCK] This girl was the first electrosprite, and she the second.[SOFTBLOCK] Yet, she could stay this way, in this new joy, forever.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She still had a job to do.[SOFTBLOCK] Units were counting on her.[SOFTBLOCK] She turned to face the still-smiling electrical girl and explain the situation.") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_End.lua") ]])
	fnCutsceneBlocker()
    return
end

