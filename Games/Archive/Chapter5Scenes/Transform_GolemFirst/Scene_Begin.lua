--[Golem Transformation]
--Setup.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

--Kill the music.
fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])

--Switch to scene mode.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/MaleNeutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[Voice|ChrisMaleVoice]'Ugh...[SOFTBLOCK] My head.[SOFTBLOCK] What happened?[SOFTBLOCK] Did that robot girl knock me out?' [Voice|Narrator]Chris thought.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris idly considered his surroundings.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[Voice|ChrisMaleVoice]'She put me in some sort of cage.[SOFTBLOCK] A tube.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Chris groped about the tube blindly in the faint red light.[SOFTBLOCK] He could find no obvious exit,[SOFTBLOCK] and despite his method of arrival,[SOFTBLOCK] he found himself surprisingly calm.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "He thought about this for a moment before deciding that not only was it odd to be so calm,[SOFTBLOCK] it was equally odd to be so calm about being calm.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It occurred to him, then, that something was likely controlling his emotions.[SOFTBLOCK] He accepted this as a perfectly natural occurrence.[SOFTBLOCK] It seemed equally natural to him that he should be standing there, staring out into the dim red nothingness, and smile for it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "He found himself growing curious about the tube as he stood there, and more immediately, he noticed an odd weight that clung to his chest that seemed particularly not abnormal and definitely not out of place.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "He traced his fingers over it, almost caressing it.[SOFTBLOCK] It was hard, circular, and had a metallic feel to it.[SOFTBLOCK] He traced a finger along the smooth contours of the normal object and continued to smile ahead.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A whirling sound began to hum just beyond the walls of the tube, and Chris reluctantly ended his exploration of the surface of the metallic disc, dropping his hands to his side.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Above him, two metal claws descended from an unseen opening and began to tug and cut at his clothes.[SOFTBLOCK] He stood still, grateful to the claws for their assistance, and did not want to hinder them as they went about their business.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Soon all that remained were the scraps that the claws held.[SOFTBLOCK] They retracted back into the unseen space they had emerged from, and were replaced by a single arm that held a small needle attached to a long hose.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A shallow reflection of the needle filled the glass of the tube before him as it descended from the tube.[SOFTBLOCK] Though his own memories told him that he had never liked needles, Chris could find no fault in the reflection that filled his vision.[SOFTBLOCK] It was a complement to the disc that clung to his chest.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "He continued to smile as the small needle bit into the back of his neck, and watched happily as a gossamer fluid began to fill the tube attached to it.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/MaleNeutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "From below him, in the shadows that hung about his feet, a second fluid began to bubble up from an unseen source.[SOFTBLOCK] It was warm, thick, and soft, and it tickled his toes as it bubbled in.[SOFTBLOCK] A panic struck him, but vanished in a moment.[SOFTBLOCK] This was exactly as it should be, his mind told him.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "He stood still, allowing the tube to bathe him in the flood of fluid, smiling at the dim red light, not even noticing that the fluid began to cling to his skin even as it filled him.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "He could feel his body beginning to change, and at once he welcomed it.[SOFTBLOCK] It bonded with his skin, altered his bones, repurposed his organs.[SOFTBLOCK] The fluid flowed over him, melding with his body until flesh and fluid were one.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "His body was becoming slighter, more delicate, and the first hints of light curves began to accent these changes.[SOFTBLOCK] His chest began to swell, and on either side of the metallic disc two noticeable bumps began to form. ") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A small itch began to accompany the fluid as it filled between his legs, and though he at first wished to relieve the irritation, the thought of disrupting the tube's work by moving displeased him.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "He ignored it, focusing on the warm fluid that rose around him, and soon he found that the itch faded.[SOFTBLOCK] He felt lighter and, in one perfectly normal sense, a bit more open.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Deep within him, an unexpected sense of joy began to take form even as his mind sought to take charge of the new emotion.[SOFTBLOCK] His smile deepened and his eyes softened.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The sense of openess filled him, delving into his body and permeating him.[SOFTBLOCK] It began to fill him alongside the shimmering liquid, working alongside it to tear down what was and bring about what would be.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The joy grew as his body was filled, and a sudden whirring hum surged from the metal disc on his chest as his thoughts mingled between the sense of joy and a driving need for calm.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "His smile broadened.[SOFTBLOCK] He was calm.[SOFTBLOCK] It was not the expected calm.[SOFTBLOCK] The metal disc hummed quietly as his thoughts settled.[SOFTBLOCK] He found himself happy and calm, and his mind, after a moment of hesitation, accepted this.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The whirring hum of the metal disc slowed, steadied for a moment, and then faded until only the sound of the bubbling fluid accompanied the usual hum of the tube and the quiet hiss of fluid as it flowed through the hose and into his neck.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Soon the pooling fluid reached his mouth, and he instinctively held his breath.[SOFTBLOCK] Somewhere, from deep within his mind, a thought came to him.[SOFTBLOCK] 'Breathe,' it said, and he turned his eyes towards the fluid in confusion.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "'I'll drown,' he thought, but the thought came to him again.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "'Breathe.'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "He obeyed, and the fluid poured into his lungs as it passed over his nose.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "He stood there,[SOFTBLOCK] calm,[SOFTBLOCK] happy,[SOFTBLOCK] thinking of how oddly normal it was that he could continue to breathe the fluid as easily as he breathed the air.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Soon the flow of fluid slowed, then ceased, and the needle in the back of his neck pulled itself free.[SOFTBLOCK] The arm rose into the void it had descended from, and Chris continued to stand with his arms at his side.[SOFTBLOCK] It was what he knew he should do. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A new whirring sound joined the retreating arm, and he watched the reflection of a helmet as it slid out of the darkness and covered his head.[SOFTBLOCK] His vision went dark, and he could feel the material as it conformed to his face.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The sensation of countless needles pricking into his head and face swept over him as the helmet began pinching into his maleable flesh and skull and into his brain. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Time passed, and his thoughts began to stray as he could no longer occupy himself with watching the events in the reflection.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF3") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A quiet alarm rang in the background as she opened her eyes and roused herself from slumber.[SOFTBLOCK] The alarm had not been enough to wake her, and the family chauffeur was spending the day in attendance with her mother.[SOFTBLOCK] She was late.[BLOCK][CLEAR]") ]]) 
fnCutsceneInstruction([[ WD_SetProperty("Append", "She threw herself together and rushed from her home, waving franticly at the school bus as it passed by her family's estate. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It slowed and stopped, and its doors swung open to let her on.[SOFTBLOCK] A sigh of relief passed her lips as she took an empty seat.[SOFTBLOCK] Several of the girls spoke quietly among themselves as they eyed her with suspicion.[SOFTBLOCK] This was a private bus, to be sure, but still beneath her statue.[SOFTBLOCK] She did not normally take the bus.[SOFTBLOCK] Nobody of her rank ever did. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She could feel their leering stares and could hear the occasional whispered word.[SOFTBLOCK] Accusations of abuse of wealth and what she could 'get away with' drifted to her ears above the hum of the engine.[SOFTBLOCK] She lowered herself in the seat as if to hide. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her uniform was dishevelled and dirty.[SOFTBLOCK] She had occupied herself with her studies in her room the night before, and scolded the servants when they came to check up on her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "They stopped coming around that evening, leaving her to her own devices, and her uniform to its floor-cast fate. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "This only served to deeper her shame, and she saved a note to her memory banks to ensure the uniform would always be cleaned and pressed. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF3") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mother's words echoed in her mind.[SOFTBLOCK] 'You'll be sixteen this year, Christine,' the voice said.[SOFTBLOCK] 'You need to learn how to take responsibility.' [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She struggled to believe the very words she had heard.[SOFTBLOCK] Her mother wanted her to take a summer job! [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mother, however, was correct, and Christine knew this. [SOFTBLOCK]She was spoiled and had been flaunting it.[SOFTBLOCK] But why should she have to work?[SOFTBLOCK] That is, after all, what the servants were for. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She had made the mistake of saying this out loud to her parents, and their umbrage had burned deeply as they glared at her.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF4") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her mother had used her connections to set Christine up with a manufacturing job alongside the Slave Units.[SOFTBLOCK] She would be tasked with fabricating replacement parts for a number of facilities.[SOFTBLOCK] 'It will build character,' her mother said.[SOFTBLOCK] Christine had her doubts. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She arrived on her first day on time despite her best efforts.[SOFTBLOCK] Her new co-workers stood nearby, nude with their metal skin exposed.[SOFTBLOCK] Their eyelids drooped heavily and their backs hunched as they watched her warily.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She greeted their wary gaze with one of authority.[SOFTBLOCK] She would not be cowed by manual labor. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She received her programming and access and began to work.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Though she had at first believed she would be in charge of watching over the Slave Units, she found herself working alongside them.[SOFTBLOCK] She machined refined metal ingots and slabs into pieces that would be used to assemble other parts that would ultimately be used in a final product. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She stood at the fabricator for hours, neither speaking to the other units nor allowing herself to slip into the hunched stances they took.[SOFTBLOCK] She was bored, but proud.[SOFTBLOCK] She did her job well. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Maintenance was always the worst.[SOFTBLOCK] She had to stand, unmoving, as the Monitor Units scanned her body, inside and out, for any potential imperfections.[SOFTBLOCK] She was a superior unit.[SOFTBLOCK] Unlike the units she worked alongside, she did not have imperfections.[SOFTBLOCK] She did not need the constant repairs they did. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "However, she could not deny that they were far more disciplined than she.[SOFTBLOCK] They stood there, as straight as their hunched backs would permit, their chests exposed, arms hanging at their sides.[SOFTBLOCK] Blank stares gazed forward as they awaited their turn to be scanned by the maintenance golem. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Their form was perfect.[SOFTBLOCK] They did not move.[SOFTBLOCK] She did her best to emulate the behavior.[SOFTBLOCK] She failed, and was quickly reprimanded by the Lord Golem in charge. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She did not like the Lord Golem. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine looked on in wonder.[SOFTBLOCK] How a human had escaped and gotten past security should could not guess.[SOFTBLOCK] The Lord Golem had sent her to obtain a replacement part to repair her fabricator when unexpected movement caught her attention. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A human was hiding in the storage room, struggling to conceal itself in a space too small to fit it.[SOFTBLOCK] It cowered as she looked on, and bared its teeth as she began to approach it. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The proper handling of a human was programmed into Unit 771852's memory banks, just as it was in the memories of all Units.[SOFTBLOCK] She approached slowly, hands open and up, smiling without showing her teeth, and kept herself as non-threatening as she could. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The human closed its mouth, watching her with a similar curiosity as she watched it, and seemed to relax a little.[SOFTBLOCK] She smiled at it, humming quietly to herself as she approached, and reached out an upturned hand. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The human looked at it and then back to her, unsure of her intentions but apparently no longer believing her to be a threat.[SOFTBLOCK] She smiled, tilted her head in a way she had seen humans do on the old Videographs, and discharged a low voltage from her hand. [SOFTBLOCK]The human's body seized for a moment before going limp. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She took one of the Golem Cores from the storage containers and placed it onto the human's chest.[SOFTBLOCK] Its tendrils burrowed into the human's flesh and took control of it.[SOFTBLOCK] It stood, and an idle smile came onto its face.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The human marched by Unit 771852, exiting the room.[SOFTBLOCK] The Core would direct it to the nearest Conversion Center, where it would convert the human to a new Unit, ending the problem of the escaped human subject. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Unit 771852, designated Christine, felt pride swell up within her at performing her function.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The summer was coming to an end, and the days were growing shorter.[SOFTBLOCK] Her time in the fabrication facility would soon end along with it. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She would miss her co-workers, but she knew she had to return to fulfilling her primary functions as defined by the central authority.[SOFTBLOCK] These functions, she knew, she would be able to perform much more efficiently thanks to what she had learned from the units she had worked alongside. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "They were near to her, almost as close as family.[SOFTBLOCK] Though their time together was ending, she would not let herself forget them. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "They all gathered together on her last day, crowding about each other and speaking excitedly.[SOFTBLOCK] All of the units, the very same ones who had first regarded her with such wary gazes and which she had regarded as so low beneath her, all felt sorrow at her departure. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She suggested they take a group photo, something to remember each other by, and every unit happily agreed. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Unit 771852 stood in the center as her friends stood beside her.[SOFTBLOCK] Their arms hung at their sides, their hunched backs as straight as possible, and each said their favorite catchphrase as the camera flashed.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF4") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "'REPROGRAMMING COMPLETE'.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The prickling on her head ceased, and the helmet lifted from 771852's head.[SOFTBLOCK] The fluid began to drain rapidly, with small eddies tickling her smooth booted feet where before the bubbling fluid had danced about them.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/GolemTF5") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the last of the fluid drained away, the glass of the tube slid up, and her ocular units looked out into the darkness.[SOFTBLOCK] Where once there had been only a dim green light was now a plainly visible room to her new vision. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Unit 771852 held her pose until the programming indicated it was safe to proceed.[SOFTBLOCK] The power core relinquished its initial control and passed its orders to her override CPU, which began secondary boot procedures.[SOFTBLOCK] She waited patiently as the rest of her mind came online.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lacking a function assignment, her programming loaded a temporary assignment into her buffers.[SOFTBLOCK] She was to proceed to Regulus City, Sector 96, and obtain a maintenance scan and function assginment.[SOFTBLOCK] Priority Zero.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Unit 771852 acknowledged her instructions and stepped away from the tube.") ]])
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_End.lua") ]])
	fnCutsceneBlocker()
    return
end