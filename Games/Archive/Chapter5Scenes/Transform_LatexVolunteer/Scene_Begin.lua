--[Volunteering to become a Latex Drone]
--Optional part of the manufactory subquest.

--[Variables]
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

--[Repeat Check]
--If Christine can already turn into a Latex Drone, this scene does not play.
local iHasLatexForm = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
if(iHasLatexForm == 1.0 and iIsRelivingScene == 0.0) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

--[Form]
--Switch Christine to Latex Drone. She's not actually visible at all until the transformation is completed.
--LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua")

--Unlock Latex Drone form.
VM_SetVar("Root/Variables/Global/Christine/iHasLatexForm", "N", 1.0)

--[Topics]
--Unlock these topics if they weren't already.
if(iIsRelivingScene == 0.0) then
    WD_SetProperty("Unlock Topic", "Latex Drones", 1)
    WD_SetProperty("Unlock Topic", "CPU Inhibitors", 1)
end

--[Music]
AL_SetProperty("Music", "Null")

--[Cutscene Execution]
--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Scene.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As Christine walked up to the tube, she shivered.[SOFTBLOCK] The still basement air was unusually cold against her nude human skin.[SOFTBLOCK] With the click of a button, Sophie opened the door to the tube, allowing Christine to walk in.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As Christine stepped in, her mind started to race.[SOFTBLOCK] What if something goes wrong?[SOFTBLOCK] The tube was sent in to be repaired, and Sophie took care of it almost immediately.[SOFTBLOCK] But...[SOFTBLOCK] what if...?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "No.[SOFTBLOCK] Sophie was an amazingly talented repair bot.[SOFTBLOCK] There's no way she could have messed up something as routine as this.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Pulling herself out of her internal debate, Christine focused her attention back on Sophie.[SOFTBLOCK] Her fears were assuaged when she saw a beaming, silvery smile plastered all over Sophie's face.[SOFTBLOCK] Christine gave her a thumbs up, and Sophie responded by closing the tube's door.") ]])
fnCutsceneBlocker()

--Scene.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Almost immediately, the temperature seemed to drop.[SOFTBLOCK] There was a faint whirr as spindly arms began to descend from their resting spot, each one tipped with a small nozzle.[SOFTBLOCK] They quickly reached her feet and began to spray a thick, inky substance onto her smooth skin.[SOFTBLOCK] The mist swirled in the breeze, chilling her feet even more.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It was cool, like water, but it was also firm and comforting, like a warm sock.[SOFTBLOCK] Christine had never felt anything like it before, and as it continued making its way up her bare legs, she realized...[SOFTBLOCK] she wanted more.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The seemingly chilly temperature was soon forgotten as thoughts of the substance covering her grew more and more appealing. As she reached down to touch it, she glimpsed her one-machine audience.[SOFTBLOCK] Sophie was almost as entranced as Christine was.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The substance was silky smooth, and squeaked when she ran her fingers across it.[SOFTBLOCK] Just as it moved across her legs and now her thighs, the latex-like material started making its way up her fingers and forearms.[SOFTBLOCK] As it covered more and more of her skin, she found it harder and harder to concentrate.[SOFTBLOCK] Anywhere the latex touched began to radiate a faint heat, one that grew warmer the more the substance grew.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She could feel her nether lips start to drip with arousal, the latex growing closer and closer until -[SOFTBLOCK] in a swift, cathartic moment -[SOFTBLOCK] it dove into her waiting sex.[SOFTBLOCK] The sudden movement sent a bloom of heat radiating through her body, the sensation causing a nigh instant orgasm -[SOFTBLOCK] one that seemed to last an eon.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As if spurred on by this, the latex grew faster and faster -[SOFTBLOCK] not just over her chest and torso, but also deep inside of her, looping back into that wondrous orgasm.[SOFTBLOCK] She could feel every crevice of her vagina being covered by that wonderful, warm latex material.[SOFTBLOCK] Christine could barely stand the intense onslaught of her body's rapid encasement.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Just as it seemed to start to fade away, the latex reached her now erect nipples, flowing over and teasing them like a skilled lover.[SOFTBLOCK] This was enough to bring her to another mind-blanking orgasm, one that felt even more intense than the first.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the latex reached her neck, Christine vaguely remembered something important about how drones are made.[SOFTBLOCK] Something about...[SOFTBLOCK] a chip...[SOFTBLOCK] that did...[SOFTBLOCK] something?[SOFTBLOCK] It was too hard now to try to remember.[SOFTBLOCK] The material was now rushing over the last uncovered bits of her head, her entire body was on fire from the material now covering her completely.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Everything she knew and loved about Regulus society began to be more and more appealing, as well as the desire to...[SOFTBLOCK] to...[SOFTBLOCK] serve.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Unbeknownst to Christine, the conversion tube had entered its second phase of the transformation.[SOFTBLOCK] Her organs were being replaced with infallible machine parts -[SOFTBLOCK] some universal to all the golem series, some completely new and unique to the drones.[SOFTBLOCK] Her brain was transforming back into the computer she knew and loved it as.[SOFTBLOCK] But most importantly, her mind was being scanned for its activity.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As it grew harder and harder for Christine to have an organic thought, her newly formed thought inhibitor chip grew more and more powerful;[SOFTBLOCK] each thought she had was either being suppressed or converted into a new way to serve.[SOFTBLOCK] With her thoughts growing dim, her other senses were heightened.[SOFTBLOCK] She could feel her feet pulling upwards into a hoof-heeled position, customary for all drones.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her skin became tightly bound by latex ropes, accentuating her every curve;[SOFTBLOCK] her face growing away from her metal skeleton, becoming an expressive mask;[SOFTBLOCK] and her now auburn hair spilling down her back, growing all the way down to her ass.") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A blissful eternity after the tube began its work, it retracted its arms back to the metal ceiling and the door clicked open.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine was now a fully-fledged latex drone, her mind dominated by the need to serve.[SOFTBLOCK] As she stepped out of the pod, she noticed on some deep, muted level how much more acute everything felt, the cool basement air now tangible on every inch of her onyx surface.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Now, she could even feel the faint motion of the air as it blew slowly out of a nearby ventilation grate.[SOFTBLOCK] Her simplified mind soon lost interest in this, and she reverted back to her main assignment.[SOFTBLOCK] LOCATE SUPERIOR UNIT, SERVE SUPERIOR UNIT.[SOFTBLOCK] She looked around and noticed Unit 499323 standing in front of her, looking winded.[SOFTBLOCK] Happy to have found someone to serve, Christine stepped forward.") ]])
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_End.lua") ]])
	fnCutsceneBlocker()
    return
end

--Otherwise, the cutscene resumes from Sophie's dialogue handler.