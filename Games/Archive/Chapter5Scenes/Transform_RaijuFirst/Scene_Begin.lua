--[Setup]
--Variables.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")
    
--Wait a bit.
if(iIsRelivingScene == 0.0) then
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
end

--Scene part 1.
fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine frowned as her organic stomach fluttered at the waiting.[SOFTBLOCK] Her eyes tracked Sophie as the her girlfriend busied herself with the PDU's she had found scattered around the ranch.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She pulled her eyes from Sophie's beaming face as she looked around at the PDU's, six in total, that were all pointed at the bed.[SOFTBLOCK] A bed in a small Raiju house.[SOFTBLOCK] A house for the Raijus' particular brand of...[SOFTBLOCK] Power generation.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Each PDU had been placed to record the bed from a different angle, and Christine wondered if Sophie could have found more angles if she had found more PDUs.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She crossed and uncrossed her legs as her body fidgeted in nervousness, then noticed she sat directly facing one of the PDUs.[SOFTBLOCK] A wry smile crossed her face, and she glanced at her distracted lover.[SOFTBLOCK] Keeping her eyes focused on Sophie, she spread her legs and pulled the hem of her skirt up, then pressed a finger deep against her panties.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A moment passed, perhaps a minute as she rubbed her panties while watching Sophie work, and she closed her legs.[SOFTBLOCK] Her face heated with a deep flush of embarrassment.[SOFTBLOCK] She didn't know if the devices were recording yet, but if they were, she hoped Sophie would enjoy the brief pre-show.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The flush had barely begun to fade before her nervousness returned and she began to question herself.[SOFTBLOCK] She was doing this to help people.[SOFTBLOCK] To help the ones who could not help themselves.[SOFTBLOCK] So why was she nervous?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine began to fidget again as Sophie finished the last PDU, this one hung from an angle that promised a clear view from the foot of the bed.[SOFTBLOCK] Sophie spun toward the nearby console, her smile undiminished, and let out a small gasp.[SOFTBLOCK] A brief smile was all Christine needed to know the devices were indeed already recording.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "After a moment of tweaking the devices at the console, Sophie bolted to the door with an excited shout that Christine thought sounded more like a giggle.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Silence descended on the room as the door fell shut.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[SOFTBLOCK] The door burst open, and Christine barely had a chance to turn before a Raiju pounced on her, pushing her to the bed and pinning her.[SOFTBLOCK] A cold metal tag dragged across her chest, teasing her breasts, and Christine glanced down to see the name 'Milky' written on it.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "At least, Christine thought it was her name.[SOFTBLOCK] Judging by the size of the breasts that hung in her view, it may well have been a description.[SOFTBLOCK] Christine shook her head and looked back up into the Raiju's grinning face.") ]])
fnCutsceneBlocker()

--Scene part 2.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/RaijuTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "'Milky' wasted no time before she placed her first kiss on Christine's lips.[SOFTBLOCK] A light tingle coursed through them, and Christine wondered if this was how all Raijus said 'hello' to each other.[SOFTBLOCK] A second tingling kiss on her lips and she began to appreciate the apparent disinterest in speech.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine wondered if Milky knew her job was to transform the human beneath her, or if Milky even cared.[SOFTBLOCK] As the electric kisses moved toward her neck, Christine began to wonder if she cared herself.[SOFTBLOCK] It soon became clear her Raiju friend had other goals for her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine's hands moved towards Milky's body, hesitant, but drawn by a carnal force as old as history itself.[SOFTBLOCK] Milky didn't seem to mind the hesitation, nor the light touch that followed as it slid from her bared breasts down to the curve of her hips. [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine tried to force herself to relax as Milky's kisses moved down her neck.[SOFTBLOCK] She knew she had no reason to feel bad.[SOFTBLOCK] Sophie had encouraged this, and was likely already enjoying the show, albeit from a safe distance.[SOFTBLOCK] She needed to enjoy this, as well, if only so Sophie could.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Gently, Christine put a hand on Milky's head and pushed her down.[SOFTBLOCK] Milky's lips lead a trail of tingling kisses as she let herself be pushed from Christine's neck to her breasts, nipping lightly as they passed a nipple, before sliding down and sucking at the skin just below her belly button.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Milky glanced up at her and met Christine's eyes with a wink before she moved further down, her tongue delving into Christine's soft folds.[SOFTBLOCK] The nervousness and fear drained from her as the electric current slid from Milky's tongue and into the depths of Christine's body, teasing her nerves in ways she had never felt before.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She knew she could no longer stop, even if she had wanted to.[SOFTBLOCK] Her fingers tensed and gripped Milky's hair as the Raiju's tongue poked, prodded, and massaged.[SOFTBLOCK] It was all Christine could do to hold on.[SOFTBLOCK] Her eyes slid closed as her body surged with the blissful current.") ]])
fnCutsceneBlocker()

--Scene part 3.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/RaijuTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/RaijuTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her body had already began to change when she opened her eyes again.[SOFTBLOCK] Electricity sparked across her body, but the sparks did not hurt her.[SOFTBLOCK] Electrical arcs jumped between nearby limbs, but she felt little more than a whispered tickle.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Tufts of fur began to sprout.[SOFTBLOCK] Her nails lengthened and hardened.[SOFTBLOCK] Her hair...[SOFTBLOCK] She could feel her hair thickening and standing in a permanent state of static arousal.[SOFTBLOCK] She could not stop her mind from wondering just how she would keep it managed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Milky's tongue grew still, then slid from the warm confines of Christine's body, and she withdrew as if she sensed Christine's biggest change was coming.[SOFTBLOCK] Christine groped for her head, desperate to push her face and tongue back into her body.[SOFTBLOCK] But when she glanced up at her partner, she realized why she had stopped.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Milky gave her another wink, then opened her mouth.[SOFTBLOCK] Thick bolts of electricity, of lightning, danced between her fangs.[SOFTBLOCK] Christine's eyes grew wide, and Milky pressed her face and tongue between Christine's thighs once more.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine's body seized and her back arched as the current surged through her.[SOFTBLOCK] Her fingers gripped Milky's hair with abandon, and the Raiju seemed to purr in response.[SOFTBLOCK] Her muscles shuddered as the orgasm swept through, chasing after the powerful current as her muscles began to be remade.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "All around her, the world surged to a blinding white.[SOFTBLOCK] A brief fear gripped Christine for a moment as she wondered if she had been blinded, but the fear passed as the light faded in an afterglow of sparks that matched the afterglow of her orgasm.[SOFTBLOCK] The entire room had filled with sparks.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The shudder of her muscles slowed, and Christine pushed herself up.[SOFTBLOCK] Milky already stood by the door, a carnal grin stretched drunkenly across her face.[SOFTBLOCK] Christine stood and looked at her hands.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her muscles now had electrical glands in them, and every movement generated a current.[SOFTBLOCK] With time and practice, she knew, she would be able to control the discharge.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A weapon of war.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A tool of civilization.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[SOFTBLOCK] An instrument of pleasure.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The door burst open and Milky stepped aside, allowing Sophie to run in.[SOFTBLOCK] Before Christine could even reach for her, Sophie had thrown her arms around her and drawn a few sparks from her lips with a crushing kiss.") ]])
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_End.lua") ]])
	fnCutsceneBlocker()
    return
end

--Execute form changer.
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeRaiju", "S", "Normal")
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua") ]])

--Scene positioning.
fnCutsceneTeleport("RaijuA",    42.25, 34.50)
fnCutsceneFace(    "RaijuA",     0,     1)
fnCutsceneTeleport("Christine", 42.25, 35.50)
fnCutsceneFace(    "Christine",  1,     0)
fnCutsceneTeleport("Sophie",    43.25, 35.50)
fnCutsceneFace(    "Sophie",    -1,     0)
fnCutsceneBlocker()

--Return to the ranch.
fnCutsceneWait(25)
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Raiju:[VOICE|HumanF0] Phew![SOFTBLOCK] You're pretty good for a first-timer![SOFTBLOCK] We should go again once you've charged up!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutsceneInstruction([[ AL_SetProperty("Music", "BreannesTheme") ]])

--Raiju walks off.
fnCutsceneMove("RaijuA", 45.25, 34.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneMove("RaijuA", 51.25, 34.50)
fnCutsceneMove("RaijuA", 51.25, 28.50)
fnCutsceneBlocker()
fnCutsceneTeleport("RaijuA", 36.25, 19.50)
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Eeeeee![SOFTBLOCK] Look at me Sophie![SOFTBLOCK] I'm - [SOFTBLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] Just get them all out of your system.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] The electricity puns.[SOFTBLOCK] Just get them alllllll out.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Happy] Do you find my appearance shocking?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] [EMOTION|Sophie|Neutral]I'm excited to see you![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] [EMOTION|Sophie|Offended]My services are for sale -[SOFTBLOCK] ask me what I charge![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Laugh] Got to go, better bolt![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] I'm hideous, Sophie.[SOFTBLOCK] But luckily -[SOFTBLOCK] opposites attract![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Okay, you are redeemed.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] And I got some great recordings until you blew out the white balancers![SOFTBLOCK] Hee hee![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Offended] Yeah, sorry about that.[SOFTBLOCK] Luckily you can just recalibrate the receivers.[SOFTBLOCK] No permanent damage.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Smirk] I've already set them up to do just that.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] So what precisely are you going to do with the videograph?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Delete it and never look at it ever again![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Cry] Yes, Sophie, I absolutely believe you.[SOFTBLOCK] That is something you would do.[SOFTBLOCK] Not an obvious lie, nope.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Blush] Hee hee![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Well, back to work, then.[SOFTBLOCK] I hope 55 doesn't find my appearance too sh - [BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Offended] Don't say it![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Surprising.[SOFTBLOCK] Obviously.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Sophie:[E|Happy] Hee hee![SOFTBLOCK] Let's go!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Unlock dialogue about changing clothes.
--WD_SetProperty("Unlock Topic", "Clothes", 1)

--Autofold.
fnAutoFoldParty()
fnCutsceneBlocker()
