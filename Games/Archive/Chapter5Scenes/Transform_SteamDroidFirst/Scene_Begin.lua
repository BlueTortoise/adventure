--[Steam Droid Transformation]
--Setup.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")
    
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Black the screen out.
if(gbDontGoToNextPart ~= nil and iIsRelivingScene == 0.0) then
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
end

--Switch to scene mode.
fnCutsceneInstruction([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Christine_Human") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] A power core...[SOFTBLOCK] what do you even make a power core out of?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399 has led her to a long-disused workshop on a higher floor of the mines.[SOFTBLOCK] She had asked her friends to wait outside, and SX-399 has smiled and told her she would be ready to answer any questions that came up.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She wandered around the workshop to take stock of all the materials available there.[SOFTBLOCK] Scrap was in abundance.[SOFTBLOCK] Gauges, connectors, pipes...[SOFTBLOCK] as she collected the pieces she would need, she came upon one particular item that held rapt her attention.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A rusty old rifle, of Pandemonium make, and an affixed bayonet.[SOFTBLOCK] They were not unlike the type her forefathers had once used.[SOFTBLOCK] Rusted from years of neglect, the wood began to crumble beneath her fingers as she picked it up.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She stared at it for a moment, frowned, and threw it back onto the pile.[SOFTBLOCK] The power core was for her, not for her ancestors.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Each step was more complicated than the last, though the provided instructions were sufficient to avoid major complications.[SOFTBLOCK] But with each step, her thoughts would wander back to the rifle.[SOFTBLOCK] How might it fill the needs of -[SOFTBLOCK] no, it would not do.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine stood before the nearly-finished power core.[SOFTBLOCK] The messy lab had revealed an organized chaos, formed over the years as the same process had repeated itself a hundred times before.[SOFTBLOCK] Despite this, she found herself again before the rusted rifle.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The bayonet and the barrel were both made of iron.[SOFTBLOCK] Not a choice material when dealing with steam, but there were ways to use them that would prevent a meeting between the two.[SOFTBLOCK] Christine frowned.[SOFTBLOCK] This *was* the final piece.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Taking the rifle into her hands, she broke away the rotted wood and discarded the trigger, bolt casing, and metal stock frame.[SOFTBLOCK] She only wanted the bayonet and the barrel.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She began to work again on her core, hammering on the iron from the rifle until it was a flattened mass that she wrapped around the seam in the two halves.[SOFTBLOCK] Welding it to the two sections would seal them together with a stronger bond than brass and bronze, and should never encounter the steam that the core would pump through to her future systems.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Finished, she held the core aloft, inspecting and admiring it in the same moment.[SOFTBLOCK] It was crude-looking, but she had no doubts that all Steam Droid cores would be similarly crude.[SOFTBLOCK] They were never meant to be perfect, they were meant to be an extension of the Droid who made them.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She tweaked the tuning dial and activated the core.[SOFTBLOCK] It began to hum with a throbbing pulse as it churned to life.[SOFTBLOCK] Its first task was to scan itself, to ensure the materials it was made with would be capable of withstanding regular wear and the steam it would pump throughout its host body.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It paused at the iron it encountered.[SOFTBLOCK] An unusual choice, and one that it could not find similarities for in its database, but not one that would be detrimental to its overall design, if maintained correctly.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Finding no concerns nor any further anomalies, it beeped out a confirmation of satisfaction.[SOFTBLOCK] It was pleased with the caliber of its construction.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine smirked with her own satisfaction.[SOFTBLOCK] She closed her eyes and took a deep breath.[SOFTBLOCK] When she had been transformed into a golem, she had not been given a choice.[SOFTBLOCK] Suddenly being presented with one gave her pause.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Was this the only way?[SOFTBLOCK] Was this what she wanted?[SOFTBLOCK] Anxiety gave way to certainty.[SOFTBLOCK] More lives than her own were depending on her.[SOFTBLOCK] She had to do this.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Exhaling slowly, pushing the air from her chest and feeling her breasts and shoulders gently fall, she raised the core to her chest.[SOFTBLOCK] She opened her eyes and looked down at it.[SOFTBLOCK] She was ready.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Christine_Human") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A long, low tone began to emanate from the core.[SOFTBLOCK] Perhaps it was meant to give the volunteer one final chance to withdraw from the process, or notify the supervising engineer.[SOFTBLOCK] Whatever its purpose, the tone continued for several long seconds before at last growing quiet.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Small claw-like tentacles emerged from the core, altered by the internal systems she had been provided with to ensure a safe transformation, and sealed the core to the center of her bare chest.[SOFTBLOCK] A thin hose emerged from beneath it and bore painlessly into her chest, and a viscous fluid, as dark as the richest inks she had ever seen, began to fill her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She continued to clutch the core, not of its own design but of her own volition.[SOFTBLOCK] She blinked as she felt the fluid filling her, and released the core.[SOFTBLOCK] She was still in control, unlike what the golem core had done to her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Her skin began to darken until it was indistinguishable from the inky fluid, and inside of her body, she could feel her organs beginning to shift.[SOFTBLOCK] While it felt unusual, she was more appreciative of the improvements the golem core had sported.[SOFTBLOCK] She had felt nothing internal during that change.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The black fluid was composed of primitive single-use nanomachines, invented by alechmical processes well in advance of their time.[SOFTBLOCK] The design was as primitive to the citizens of Regulus as it was advanced to her, a happy marriage of accident and fortune.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lungs became compressed tanks, her diaphragm a pump, arteries and veins became tubes to carry steam while muscle and bone became specialized actuators.[SOFTBLOCK] Ultimately would come her heart, which was already being replaced as the core burrowed itself into her chest.[SOFTBLOCK] It was a feeling even stranger than any she had yet felt, but one not altogether unpleasant.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF0") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Now the fluid reached to her brain.[SOFTBLOCK] At once she was subjected to vivid imagery.[SOFTBLOCK] She saw all of Regulus as though she were standing on Pandemonium, and she felt a twinge of sadness pass over her.[SOFTBLOCK] To be so far from the home she had grown to love, with all of its joys and faults, weighed upon her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As soon as has the vision faded, she saw herself standing in the maintenance bay in Regulus City, watching like a ghost as she worked alongside Sophie.[SOFTBLOCK] Both were smiling, chatting with each other, though she could not hear the words.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "A moment later, a coy smile spread across Sophie's face, and she bumped her shoulder into the Christine beside her.[SOFTBLOCK] The two looked at each other and giggled for a brief moment together before returning to work.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The scene shifted, and she was now walking alongside 55 in a dark hallway.[SOFTBLOCK] Where she was she could not say, but she could see a frustrated look on 55's face as the Christine she watched seemed to be telling a long joke.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She finished and, for the briefest of moments, unseen to the Christine that walked alongside her, the barest hint of a smile spread across 55's lips before vanishing almost immediately.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "No sooner had the scene ended than another began, and another after that, and another after still, and she soon lost count of all that she had seen.[SOFTBLOCK] Sophie, 55, SX-399, any many more besides.[SOFTBLOCK] All were units she cared for, to varying degrees, and all were units she wanted to help.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "As the last vision faded, she found herself still standing in the workshop.[SOFTBLOCK] She glanced down to find that the core had fully merged with her, and that the transformation of her body was complete.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "There was only one final task necessary to emerge as a Steam Droid, and a whirring of machinery above her indicated the final step was ready to begin.[SOFTBLOCK] A hose descended from the ceiling of the chamber.[SOFTBLOCK] It was thick, but remarkably flexible, and would stay so until it began to fill her with the life-giving steam.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF1") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She took the hose in her hands and guided the nozzle to her mouth.[SOFTBLOCK] The crude Steam Droid process required many manual interactions, unlike the heavily automated Golem tube.[SOFTBLOCK] To become a Steam Droid was a choice, at every step.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The cool brass tip of the nozzle slid between her lips, and she gently pulled the sheath back before sliding the nozzle deeper into her mouth.[SOFTBLOCK] Her tongue flicked about it involuntarily, allowing her saliva, or the oils that now functioned as her saliva, to lubricate its passage.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She stopped before reaching her throat, and after a moment, a quiet tone could be heard from somewhere within the lab, and the hose stiffened and throbbed between her hands as the first hiss of of life-giving steam began to flow through it, erupting from the nozzle and flowing into her.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The pumps and compressors that had once been her lungs and diaphragm began to function, pulling the warm, wet steam down her throat and causing her newly-formed brass and alloy clothing to swell at the joints as it filled the steam bladders in her chest.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She looked at her reflection in the glass and patted her lightly-swollen breasts with a smile.[SOFTBLOCK] Finally they were more than just decoration.[SOFTBLOCK] Her power core let out its own hum of satisfaction in turn.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF2") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF3") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The steam bladders quickly reached their capacity, and the hose began to retract into the ceiling.[SOFTBLOCK] Her hand guided the once-quivering hose along as she withdrew it from her mouth, and as the nozzle slipped from her lips, a small puff of steam condensed on its tip and dribbled onto her lower lip.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "She smiled and flicked her tongue along her lip, catching the errant drop and whisking it away into her mouth and down her throat.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The sheath on the nozzle slipped back into place quietly as the hose disappeared into the recesses of the chamber, and Christine looked about for the first time as a Steam Droid.[SOFTBLOCK] Her eyes fell once more onto the rotting wood of the old rifle, and her smile faded.[SOFTBLOCK] It was a part of her, but it had no bearing on her.[SOFTBLOCK] It held no sway.[SOFTBLOCK] She kicked it aside and left.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Outside, a giddy SX-399 and a flustered-looking 2855 awaited her.") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutsceneInstruction([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter5Scenes/Z Relive Handler/Scene_End.lua") ]])
	fnCutsceneBlocker()
    return
end

--Switch Christine to Steam Droid.
fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "/FormHandlers/Christine/Form_SteamDroid.lua") ]])
fnCutsceneBlocker()

--[Dialogue]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Steam") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] You look great![SOFTBLOCK] How was it?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] ...[SOFTBLOCK] Steamy.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Our path is planned and everything is in place.[SOFTBLOCK] I must apply the disguise, and we will move out.") ]])
fnCutsceneBlocker()
fnCutsceneWait(325)
fnCutsceneBlocker()

--[Dialogue]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You know, 55, since I recovered my memory...[SOFTBLOCK] I was hoping this would happen.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Stop squirming.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] You're...[SOFTBLOCK] you're my best friend, 55.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] ...?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Well you're doing my make-up.[SOFTBLOCK] Isn't that what friends do?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] I...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Sorry, I'm distracting you.[SOFTBLOCK] Carry on.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] You'll be done in a few minutes...") ]])
fnCutsceneBlocker()
fnCutsceneWait(325)
fnCutsceneBlocker()

--[Post-Scene]
--Switch Christine to Steam Droid.
if(gbDontGoToNextPart == nil) then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 1.5)
    fnCutsceneInstruction([[ LM_ExecuteScript(gsRoot .. "/FormHandlers/Christine/Form_SX399.lua") ]])
    fnCutsceneBlocker()
end

--[Dialogue]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "2855", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Steam") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] How do I look?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] It's like looking into a mirror...[SOFTBLOCK] ungh...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Are you all right?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "SX-399:[E|Steam] My core...[SOFTBLOCK] I should already have started my recharge cycle...[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Right, no time to waste.[SOFTBLOCK] 55, get going.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral] Come, we will move faster if I carry you...") ]])
fnCutsceneBlocker()

--Next segment.
if(gbDontGoToNextPart == nil) then
    fnCutsceneInstruction([[ AL_BeginTransitionTo("SprocketCityA", "FORCEPOS:14.0x37.0x0") ]])
else
    gbDontGoToNextPart = nil
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
end