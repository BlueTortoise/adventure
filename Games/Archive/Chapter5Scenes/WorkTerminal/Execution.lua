--[Work Terminal]
--Work terminals can be accessed from several places on the station, so their scripts are rerouted here.

--Variables.
local iTalkedToSophie         = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
local iReceivedFunction       = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
local iMet55InLowerRegulus    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
local iSawSpecialAnnouncement = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N")
local iMet55InBasement        = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N")

--Hasn't talked to Sophie yet:
if(iTalkedToSophie == 0.0) then
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Login credentials unknown.[SOFTBLOCK] Please report to maintenance for access permissions.") ]])
	fnCutsceneBlocker()

--Has talked to Sophie, hasn't received a function yet.
elseif(iTalkedToSophie == 1.0 and iReceivedFunction == 0.0) then
	VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N", 1.0)
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Login credentials accepted.[SOFTBLOCK] Unit 771852 has no function currently assigned.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: A special note has been placed on your designation.[SOFTBLOCK] Your function is to assume command over Maintenance and Repair facilities in this sector.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Maintenance and Repair?[SOFTBLOCK] I'll be in charge of Sophie's work![SOFTBLOCK] That's wonderful!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Roaming permissions established.[SOFTBLOCK] Please consult the security units if you require access to a high-security area for your duties.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (I now have access to most of the facility.[SOFTBLOCK] Sophie will be so happy to hear that we'll be working together![SOFTBLOCK] I should tell her right away!)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Assignments appear on work terminals such as this one from time to time.[SOFTBLOCK] Work terminals will have green screens when an assignment is available, and red when none is available.[SOFTBLOCK] If you would like to complete assignments for additional work credits, log on and perform the assignment.[SOFTBLOCK] You will be credited for you work when it is done.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Work credits may be spent at the fabricators for access to special equipment, or used to procure items for civilian consumption.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Carry out your function with dignity and poise, Lord Unit.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", false) ]])
	fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", false) ]])

--Hasn't met up with 55 yet, no special assignments ever appear.
elseif(iMet55InLowerRegulus == 0.0) then

	--If the special assignment to meet 55 in the basement of maintenance is on:
	if(iSawSpecialAnnouncement == 1.0 and iMet55InBasement == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 2.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Login accepted for Unit 771852.[SOFTBLOCK] You are currently assigned to meet a unit in the basement of Maintenance and Repair.[SOFTBLOCK] No further details are available.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] No further details?[SOFTBLOCK] Who put the assignment in?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: No metadata present.[SOFTBLOCK] Confirm function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()
		fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", false) ]])
		fnCutsceneInstruction([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", false) ]])

	--Repeat text.
	elseif(iSawSpecialAnnouncement == 2.0 and iMet55InBasement == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Login credentials accepted.[SOFTBLOCK] Unit 771852 is assigned to report to the maintenance room basement for further instruction.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Login accepted for Unit 771852.[SOFTBLOCK] No tasks exist at this time to be completed.") ]])
		fnCutsceneBlocker()
	end

--Special assignment checker.
elseif(iTalkedToSophie == 1.0 and iReceivedFunction == 1.0 and iMet55InLowerRegulus == 1.0) then

    --[Variables]
	--General:
	local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")

	--Serenity Crater:
	local iSerenityWorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N")
	local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
	
	--Cassandra Quest:
	local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
	local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
	
	--Tellurium Mines
	local iTelluriumMinesQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasMinesWorkOrder", "N")
	local iCompletedMinesWorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedMinesWorkOrder", "N")
    
    --Electrosprites.
    local i198WorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N")
    local iSaw198Intro  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw198Intro", "N")
    
    --Manufactory.
    local iManuTookJob     = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N")
    local iManuFinishedJob = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinishedJob", "N")
    
    --Implosion Grenade Ability
    local iGotImplosionGrenades = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotImplosionGrenades", "N")
    
    --Biolabs flag.
    local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
	
    --[Text]
	--Basic text.
	local sString = "WD_SetProperty(\"Append\", \"Console: Login accepted for Unit 771852.[SOFTBLOCK] Current work credits: " .. iWorkCreditsTotal .. ".[SOFTBLOCK] Please select an option.[BLOCK]\")"
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction(sString)
	local sDecisionScript = "\"" .. fnResolvePath() .. "Response.lua" .. "\""
    local sPurchaseScript = "\"" .. fnResolvePath() .. "Purchase.lua" .. "\""
	fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
	
    --[Options Builder]
	--Basic options that are always present:
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Turn in Credits Chips\", " .. sPurchaseScript .. ", \"CreditsChips\") ")
    fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Purchase Items\", " .. sPurchaseScript .. ", \"Purchase Items\") ")
    
    --Serenity Crater: Does not need to be turned in, disabled when the biolabs are reached.
	if(iCompletedSerenity == 0.0 and iReachedBiolabs == 0.0) then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Serenity Crater\", " .. sDecisionScript .. ", \"Serenity Crater\") ")
	end
    
    --Cassandra quest. Can be turned in at the biolabs, but not taken.
	if(iResolvedCassandraQuest < 3.0) then
        if(iReachedBiolabs == 0.0 or (iReachedBiolabs == 1.0 and (iResolvedCassandraQuest == 1.0 or iResolvedCassandraQuest == 2.0))) then
            fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sector 15\", " .. sDecisionScript .. ", \"Sector 15\") ")
        end
	end
    
    --Tellurium Mines. Cannot be taken in the biolabs.
	if(iCompletedMinesWorkOrder == 0.0 and iReachedBiolabs == 0.0) then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Tellurium Mines\", " .. sDecisionScript .. ", \"Tellurium Mines\") ")
	end
    
    --Electrosprites. Cannot be taken in the biolabs.
    if(i198WorkOrder == 0.0 and iSaw198Intro == 0.0 and iReachedBiolabs == 0.0) then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Sector 198\", " .. sDecisionScript .. ", \"Sector 198\") ")
    end
    
    --Manufactory. Cannot be taken in the biolabs.
    if(iManuTookJob == 0.0 and iManuFinishedJob == 0.0 and iReachedBiolabs == 0.0) then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Efficiency Request\", " .. sDecisionScript .. ", \"Efficiency\") ")
    end
    
    --Implosion grenade. Can be unlocked if the player reached the biolabs without it. Will be removed in the Big Balance Patch.
    if(iGotImplosionGrenades == 1.0 and iReachedBiolabs == 1.0) then
		fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Unlock Implosion Grenade\", " .. sDecisionScript .. ", \"Implosion\") ")
    end
    
	fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"Cancel\") ")
	fnCutsceneBlocker()
end