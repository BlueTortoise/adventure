--[Response]
--Fired after the user selects an option on the work terminal.
local sTopic = LM_GetScriptArgument(0)

--[Common]
WD_SetProperty("Hide")

--[Serenity Crater]
if(sTopic == "Serenity Crater") then
	
	--Variables.
	local iSerenityWorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N")
	local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
	
	--Dialogue.
	if(iSerenityWorkOrder == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Your special skillset has been requested at the Serenity Crater Observatory.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Command Unit 300910 has personally requested your presence at the observatory due to repair requirements in a hostile environment.[SOFTBLOCK] Please check your weaponry for defects before departing.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Additional details are to be provided upon arrival at the observatory.[SOFTBLOCK] It may be reached by taking the tram to the eastern junction, then travelling south to the crater.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Unit 300910?[SOFTBLOCK] I'm not familiar with the designation, but I guess she saw the skills 55 said I have and figured I'd be a good pick.)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()

		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N", 1.0)
	
	--Reminder.
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Work Order:: Serenity Crater Observatory Assignment.[SOFTBLOCK] Your skills are required at Serenity Crater Observatory by special request of Command Unit 300910.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Take the tram to the eastern junction, then travel east and south to Serenity Crater Observatory.[SOFTBLOCK] Unit 300910 will brief you upon arrival.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Confirm function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()
	
	end

--[Sector 15: Cassandra]
elseif(sTopic == "Sector 15") then

	--Variables
	local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
	local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
	
	--Briefing:
	if(iTookCassandraQuest == 0.0 and iResolvedCassandraQuest == 0.0) then
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Your special skillset has been requested in Sector 15.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: An unidentified individual has been consuming organic rations without clearance, and accessing the network illicitly.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The individual may or may not be a unit assigned to Sector 15.[SOFTBLOCK] Caution is recommended.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Your function is to identify and, if possible, apprehend the individual responsible.[SOFTBLOCK] Report to a work terminal when the function is completed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Hmmm...[SOFTBLOCK] I wonder if a human got out and is on the loose?)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()

		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N", 1.0)
	
	--Reminder:
	elseif(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 0.0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Work Order:: Sector 15 Assignment.[SOFTBLOCK] You are currently assigned to locate and apprehend an individual causing trouble in the lower parts of Sector 15.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Take the tram to Sector 15 and use the elevators there to access the lower floors.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Confirm function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()
	
	--Cassandra quest ended with Cassandra being converted.
	elseif(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 1.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N", 3.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: You are currently assigned to locate and apprehend an individual causing trouble in the lower parts of Sector 15.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 reports function assignment is completed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] A rogue human was located in the lower floors of Sector 15.[SOFTBLOCK] The rogue human was captured and implanted with a golem core.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Unit 771853 will serve the Cause of Science.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Affirmative Unit 771852.[SOFTBLOCK] 100 Work credits have been added to your account.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()
        
        --Provide Work Credits
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 100)

	--Cassandra quest resolved with Cassandra escaping.
	elseif(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 2.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N", 4.0)
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: You are currently assigned to locate and apprehend an individual causing trouble in the lower parts of Sector 15.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 reports function assignment is completed.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] A rogue security robot was stealing items at random and accessing network terminals without authorization.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] The security unit was retired and sent for scrap.[SOFTBLOCK] It will not be causing any more trouble.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (Yeah, they'll believe that...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Affirmative Unit 771852.[SOFTBLOCK] 100 Work credits have been added to your account.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()
        
        --Provide Work Credits
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 100)
	end

--[Tellurium Mines]
elseif(sTopic == "Tellurium Mines") then
	
	--Variables.
	local iTelluriumMinesQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasMinesWorkOrder", "N")
	local iCompletedMinesWorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedMinesWorkOrder", "N")
	
	--Briefing:
	if(iTelluriumMinesQuest == 0.0) then
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: A Lord Unit has placed a special request for your skillset in Mining Sector 73.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Recent security reports indicate numerous incursions in the mineshafts and unfillable repair orders.[SOFTBLOCK] Your task is to repair the crucial access infrastructure and deal with any security issues that arise.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Further briefing will be provided upon arrival at the Tellurium Mines.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Confirm function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()

		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasMinesWorkOrder", "N", 1.0)
		
	--Reminder:
	else
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Work Order:: Sector 73 Assignment.[SOFTBLOCK] The Tellurium Mines in Sector 73 has requested your assistance.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Recent security reports indicate numerous incursions in the mineshafts and unfillable repair orders.[SOFTBLOCK] Your task is to repair the crucial access infrastructure and deal with any security issues that arise.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Take the tram to Sector 73 and report to the Lord Unit there for further instructions.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Confirm function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()
	
	end

--[Electrosprite in Sector 198]
elseif(sTopic == "Sector 198") then

    --Variables.
    local i198WorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N")
    local iFinished198  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
	
	--Briefing:
	if(i198WorkOrder == 0.0) then
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Your repair skills have been requested in Sector 198.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: A note has been placed on the work order 'Repair oxygen leak.[SOFTBLOCK] Please see Unit 745110 for special instructions.'[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Uh, why can't Sector 198's local repair units handle it?[SOFTBLOCK] Fixing an oxygen leak should be that hard.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Your unit designation was requested specifically.[SOFTBLOCK] No reason was provided other than the special instruction request.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Hmm...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Oh, isn't Sector 198 where the Abductions department trains its teams?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Correct.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] I'm on the way![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Confirm function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()

		--Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N", 1.0)
		
	--Reminder:
	else
		
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Work Order:: Sector 198 Assignment.[SOFTBLOCK] The Abductions training department has requested your special skills.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Please contact Unit 745110 for special details.[SOFTBLOCK] She is current assigned to Sector 198.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Take the tram to Sector 198 and report to Unit 745110.[SOFTBLOCK] Confirm function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()
	
    end

--[Efficiency Expert]
elseif(sTopic == "Efficiency") then

    --Variables.
    local iManuTookJob = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N")
    
    --Briefing:
    if(iManuTookJob == 0.0) then
        
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Your social skills have been requested by Efficiency Expert Unit 600484 in Manufacturing Sector 99.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Please contact Unit 600484 for further details.[SOFTBLOCK] The tram to Sector 99 can be taken at the tram station as usual.[SOFTBLOCK] Confirm function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[SOFTBLOCK] Logging you off.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (...[SOFTBLOCK] Social skills?[SOFTBLOCK] That's a new one.)") ]])
		fnCutsceneBlocker()
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N", 1.0)
        
    --Reminder:
    else
        
		--Dialogue.
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Your social skills have been requested by Efficiency Expert Unit 600484 in Manufacturing Sector 99.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: Please contact Unit 600484 for further details.[SOFTBLOCK] The tram to Sector 99 can be taken at the tram station as usual.[SOFTBLOCK] Confirm function assignment.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[SOFTBLOCK] Is there any reason why my social skills were required?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: No additional data available.[SOFTBLOCK] Please contact Unit 600484.[SOFTBLOCK] The Cause of Science will be served.[SOFTBLOCK] Logging you off.") ]])
		fnCutsceneBlocker()
    
    end

--[Implosion Grnades]
elseif(sTopic == "Implosion") then

    VM_SetVar("Root/Variables/Chapter5/Scenes/iGotImplosionGrenades", "N", 2.0)
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Console: In a previous prototype, you acquire 55's Implosion Grenade item.[SOFTBLOCK] The skill has now been unlocked.[SOFTBLOCK] Congratulations.") ]])
    fnCutsceneBlocker()
    AC_PushPartyMember("55")
        LM_ExecuteScript(gsRoot .. "Abilities/55/000 Initializer.lua", "Implosion Grenade")
    DL_PopActiveObject()

--[Cancel]
elseif(sTopic == "Cancel") then
end

--If we're at the Raiju Ranch, never execute past this point.
local iSawRaijuIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
if(iSawRaijuIntro == 1.0) then return end

--Common code, always executes.
LM_ExecuteScript(gsRoot .. "Chapter5Scenes/WorkTerminal/QueryFunction.lua")

--A work assignment is available, so show green work terminals.
if(gbHasWorkAssignment == true) then
	AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", true)
	AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", true)
else
	AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", false)
	AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", false)
end