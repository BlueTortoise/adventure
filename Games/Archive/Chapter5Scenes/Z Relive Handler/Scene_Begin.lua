--[ ======================================= Relive Handler ====================================== ]
--Special script, used for (most) of the instances of the player choosing to relive a transformation scene.
-- The scene requires the name of the transformation to be passed to it. The C++ code will pass the DISPLAY NAME
-- of the scene in. We would therefore expect "Transform: Alraune", for example.

--[Argument Check]
--Verify.
if(fnArgCheck(1) == false) then return end

--Set.
local sSceneName = LM_GetScriptArgument(0)

--[Storage]
--Store Christine's current form.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
VM_SetVar("Root/Variables/Chapter5/Scenes/sOriginalForm", "S", sChristineForm)

--Indicate we are reliving a scene. This causes them to end earlier or at different times.
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N", 1.0)

--Save party properties.
giFollowersTotalRelive = gsFollowersTotal
gsaFollowerNamesRelive = gsaFollowerNames
giaFollowerIDsRelive = giaFollowerIDs

--[ ===================================== Common Base Code ====================================== ]
--Variables.
local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")

--Wait a bit.
fnCutsceneWait(10)
fnCutsceneBlocker()

--Common.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)

--[Execution]
--Setup.
local sInstruction = "Null"

--Doll case:
if(sSceneName == "Doll") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Doll")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Maps/RegulusFlashback/RegulusFlashbackC/BecomeDollCutscene.lua\")"
	
	--Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (My doll form...[SOFTBLOCK] My true form...[SOFTBLOCK] The form I was created in by the Administrator.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Yes, the Administrator.[SOFTBLOCK] I will not fail you.SOFTBLOCK] I will serve...)") ]])
	fnCutsceneBlocker()
	
elseif(sSceneName == "Golem") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Golem")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter5Scenes/Transform_GolemFirst/Scene_Begin.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (When I was first brought to mechanical perfection...[SOFTBLOCK] What a lovely memory...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (And my memory banks recorded it perfectly, it's like I'm really there...)") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Unit 771852 reviewing memory of golem conversion.[SOFTBLOCK] The experiences will be useful to the Cause of Science.[SOFTBLOCK] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()
    
elseif(sSceneName == "Latex Drone") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Latex Drone")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter5Scenes/Defeat_LatexDrone/Scene_Begin.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (To be wrapped up firm in squishy, tight latex and simplified into someone's tool...)") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Unit 771852 reviewing memory of drone unit conversion.[SOFTBLOCK] The experiences will be useful to the Cause of Science.[SOFTBLOCK] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()
    
elseif(sSceneName == "Darkmatter") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Darkmatter")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Maps/RegulusSerenity/SerenityCraterH/Examination.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (I became a part of the stars and witnessed the birth of the universe...)") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Unit 771852 reviewing memory of darkmatter conversion.[SOFTBLOCK] The experiences will be useful to the Cause of Science.[SOFTBLOCK] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()
    
elseif(sSceneName == "Dreamer") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Eldritch")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Maps/RegulusLRT/RegulusLRTIG/Combat_DefeatA.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Yes, my master.[SOFTBLOCK] Show me your truth...)") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Unit 771852 reviewing memory of dreamer conversion.[SOFTBLOCK] The experiences will be useful to the Cause of Science.[SOFTBLOCK] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()
    
elseif(sSceneName == "Electrosprite") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Electrosprite")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter5Scenes/Transform_ElectrospriteFirst/Scene_Begin.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Ah, Psue.[SOFTBLOCK] We shared minds and cloned bodies.[SOFTBLOCK] What a time!)") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Unit 771852 reviewing memory of electrosprite conversion.[SOFTBLOCK] The experiences will be useful to the Cause of Science.[SOFTBLOCK] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()
    
elseif(sSceneName == "SteamDroid") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "SteamDroid")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter5Scenes/Transform_SteamDroidFirst/Scene_Begin.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Ancient-era mechanical gears and steam power.[SOFTBLOCK] Somehow, it turns me on...)") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Unit 771852 reviewing memory of steam droid conversion.[SOFTBLOCK] The experiences will be useful to the Cause of Science.[SOFTBLOCK] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()
    
elseif(sSceneName == "Raiju") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Raiju")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter5Scenes/Transform_RaijuFirst/Scene_Begin.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (Well when I thought I'd be saving the Raijus, I didn't think it'd involve getting eaten out by one...)") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Unit 771852 reviewing memory of raiju conversion.[SOFTBLOCK] The experiences will be useful to the Cause of Science.[SOFTBLOCK] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()
end

--Error Check:
if(sInstruction == "Null") then return end

--[ ===================================== Common Scene Code ===================================== ]
--[Common Execution]
--Clear party.
AC_SetProperty("Set Party", 1, "Null")
AC_SetProperty("Set Party", 2, "Null")
AC_SetProperty("Set Party", 3, "Null")
AL_SetProperty("Unfollow Actor Name", "55")
AL_SetProperty("Unfollow Actor Name", "SX399")
AL_SetProperty("Unfollow Actor Name", "Sophie")

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Black the screen out.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(60)
fnCutsceneBlocker()

--Execute the scene.
fnCutsceneInstruction(sInstruction)
