--[Scene Ending]
--After reliving a scene, return to the save point and re-add characters to the party if they got removed and play any dialogue that occurs.
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N", 0.0)

--Clear follower properties.
gsFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {}

--Chat variables.
local bIs55Present = false
local bIsSX399Present = false
local bIsSophiePresent = false

--Re-add party members here.
for i = 1, giFollowersTotalRelive, 1 do
    io.write("Follower in slot " .. i .. " is " .. gsaFollowerNamesRelive[i] .. "\n")

    --Unit 2855
    if(gsaFollowerNamesRelive[i] == "55") then
	
        --Flag
        bIs55Present = true
    
		--Create if she doesn't exist.
		if(EM_Exists("55") == false) then
			fnSpecialCharacter("55", "Doll", -100, -100, gci_Face_South, false, nil)
		end

		--Lua globals.
		gsFollowersTotal = gsFollowersTotal + 1
		gsaFollowerNames[gsFollowersTotal] = gsaFollowerNamesRelive[i]

		--Get Florentina's uniqueID. 
		EM_PushEntity("55")
			local iEntityID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs[i] = iEntityID
		AL_SetProperty("Follow Actor ID", iEntityID)

		--Place in the combat lineup.
		AC_SetProperty("Set Party", gsFollowersTotal, "55")

    --SX-399
    elseif(gsaFollowerNamesRelive[i] == "SX399") then
	
        --Flag
        bIsSX399Present = true
        
        --Entity create.
		if(EM_Exists("SX399") == false) then
			fnSpecialCharacter("SX399Lord", "SteamLord", -100, -100, gci_Face_South, false, nil)
		end

		--Lua globals.
		gsFollowersTotal = gsFollowersTotal + 1
		gsaFollowerNames[gsFollowersTotal] = gsaFollowerNamesRelive[i]

		--Get Florentina's uniqueID. 
		EM_PushEntity("SX399")
			local iEntityID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs[i] = iEntityID
		AL_SetProperty("Follow Actor ID", iEntityID)

		--Place in the combat lineup.
		AC_SetProperty("Set Party", gsFollowersTotal, "SX-399")
    
    --Sophie
    elseif(gsaFollowerNamesRelive[i] == "Sophie") then
	
        --Flag
        bIsSophiePresent = true
        
        --Entity create.
		if(EM_Exists("Sophie") == false) then
			fnSpecialCharacter("Sophie", "Golem", -100, -100, gci_Face_South, false, nil)
		end

		--Lua globals.
		gsFollowersTotal = gsFollowersTotal + 1
		gsaFollowerNames[gsFollowersTotal] = gsaFollowerNamesRelive[i]

		--Get Florentina's uniqueID. 
		EM_PushEntity("Sophie")
			local iEntityID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs[i] = iEntityID
		AL_SetProperty("Follow Actor ID", iEntityID)
        
    end
end

--Party Folding
AL_SetProperty("Fold Party")

--Return Christine to her original form:
local sOriginalForm = VM_GetVar("Root/Variables/Chapter5/Scenes/sOriginalForm", "S")
if(sOriginalForm == "Human") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua")
elseif(sOriginalForm == "Darkmatter") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Darkmatter.lua")
elseif(sOriginalForm == "Doll") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Doll.lua")
elseif(sOriginalForm == "Eldritch") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua")
elseif(sOriginalForm == "Electrosprite") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Electrosprite.lua")
elseif(sOriginalForm == "Golem") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
elseif(sOriginalForm == "LatexDrone") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua")
elseif(sOriginalForm == "Raiju") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua")
elseif(sOriginalForm == "SteamDroid") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_SteamDroid.lua")
end

--Dialogue setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)

--Now let's have some dialogue. This is based on what scene we saw.
local sLastRelivedScene = VM_GetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S")
local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
if(sLastRelivedScene == "Doll" or iFlashbackBecameDoll == 1.0) then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Serious] (Administrator, I am coming.[SOFTBLOCK] Unit 771852 will serve the Cause of Science.)") ]])
elseif(sLastRelivedScene == "Golem") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (It felt so good to become a machine...[SOFTBLOCK] and I can relive the sensation all I want...[SOFTBLOCK] mechanical life is too good!)") ]])
elseif(sLastRelivedScene == "Latex Drone") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (It's a shame I had to be so clever.[SOFTBLOCK] I'd love to have someone order me around, and to mindlessly obey...)") ]])
elseif(sLastRelivedScene == "Darkmatter") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (It was a hell of a trip, but I came out of it on the other end.[SOFTBLOCK] It was all for the best.)") ]])
elseif(sLastRelivedScene == "Eldritch") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (She taught me so much.[SOFTBLOCK] How can I ever repay her?)") ]])
elseif(sLastRelivedScene == "SteamDroid") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] (The feeling of my chest swelling up with steam...[SOFTBLOCK] I love it so much...)") ]])
elseif(sLastRelivedScene == "Raiju") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (It was worth it for the electricity puns alone.[SOFTBLOCK] Plus I love being so fluffy!)") ]])
elseif(sLastRelivedScene == "Electrosprite") then
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Blush] (With my electrosprite powers, I wonder if I could go inside a video game.[SOFTBLOCK] What a thing that would be![SOFTBLOCK] Oh well, back to reality.)") ]])
end
fnCutsceneBlocker()
	