--[Chapter 5 Cleanup]
--Script that fires when chapter 5 is completed.

--Flag the chapter as completed.
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter5", "N", 1.0)

--Remove the Platinum Compass
VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 0.0)

--No party leader.
AL_SetProperty("Player Actor ID", 0)

--Wipe the party.
AC_SetProperty("Set Party", 0, "Null")
AC_SetProperty("Set Party", 1, "Null")
AC_SetProperty("Set Party", 2, "Null")
AC_SetProperty("Set Party", 3, "Null")

--Clear the inventory.
AdInv_SetProperty("Clear")

--[Reinstate Catalysts]
--Clearing the inventory zeroes off the catalyst count, but it persists between chapters. Reset them here.
local iCatalystH  = VM_GetVar("Root/Variables/Global/Catalysts/iHealth", "N")
local iCatalystAt = VM_GetVar("Root/Variables/Global/Catalysts/iAttack", "N")
local iCatalystI  = VM_GetVar("Root/Variables/Global/Catalysts/iInitiative", "N")
local iCatalystD  = VM_GetVar("Root/Variables/Global/Catalysts/iDodge", "N")
local iCatalystAc = VM_GetVar("Root/Variables/Global/Catalysts/iAccuracy", "N")
local iCatalystM  = VM_GetVar("Root/Variables/Global/Catalysts/iMovement", "N")
AdInv_SetProperty("Catalyst Count", gciCatalyst_Health,     iCatalystH)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack,     iCatalystAt)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, iCatalystI)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Dodge,      iCatalystD)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy,   iCatalystAc)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Movement,   iCatalystM)