--[Combat Begin]
--Scenes that may or may not play at the beginning of combat. Just flavour text.

--[Variables]
--Party composition.
local bIsFlorentinaPresent = false
if(gsFollowersTotal > 0) then bIsFlorentinaPresent = true end

--Mei hasn't encountered a partirhuman yet. This always plays on the first encounter of one.
local sMeiSeenPartirhuman = VM_GetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S")
if(sMeiSeenPartirhuman ~= "Nothing") then
	
	--Flag so this doesn't repeat:
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")
	
	--Alraune case:
	if(sMeiSeenPartirhuman == "Alraune") then
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Oh my![SOFTBLOCK] Who are you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Sheathe your weapon, human.[SOFTBLOCK] Do not resist.[SOFTBLOCK] You will be joined for the good of all.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] I don't know what 'joined' means, but I don't like the sound of it![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Alraune: Unsurprising.[SOFTBLOCK] To battle, then.") ]])
		fnCutsceneBlocker()
	
	--Slime case:
	elseif(sMeiSeenPartirhuman == "Slime") then
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Oh my![SOFTBLOCK] Who are you?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "(The slimy creature gives you a dumb smile and shambles towards you...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] (Wow, she's...[SOFTBLOCK] really pretty...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Wait, gotta stay focused![SOFTBLOCK] This thing could be dangerous!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Not interested in talking, huh?[SOFTBLOCK] En garde!") ]])
		fnCutsceneBlocker()
	
	--Ghost case:
	elseif(sMeiSeenPartirhuman == "Ghost") then
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] We don't need to fight...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost: You are sick.[SOFTBLOCK] Please, let me help you...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] No way![SOFTBLOCK] Back off![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Ghost: We will subdue you.[SOFTBLOCK] The sickness must be eradicated.") ]])
		fnCutsceneBlocker()
	
	--Bee case:
	elseif(sMeiSeenPartirhuman == "Bee") then
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Oh geez, a giant bee?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "(The bee gives a faint smile and advances on you...)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Okay, I've swatted bees before...") ]])
		fnCutsceneBlocker()
	
	--Bee case:
	elseif(sMeiSeenPartirhuman == "Werecat") then
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Surprise] Oh my![SOFTBLOCK] Are -[SOFTBLOCK] can you understand me?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Werecat: Purr...[SOFTBLOCK] it is time for battle, not negotiation, human![SOFTBLOCK] Fight me![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] With pleasure!") ]])
		fnCutsceneBlocker()
	end
	
	--Stop update here.
	return
end
	

--[Roller]
--There is a 10% chance of displaying any given scene. Scenes can repeat.
local iRoll = LM_GetRandomNumber(0, 100)
iRoll = 100
if(iRoll >= 10) then return end

--[Common Setup]
--Standard. Player's party is on the left, yelling at... nobody, I guess.
fnStandardMajorDialogue()

--[Mei is Alone]
--Mostly used at the start of the game.
if(bIsFlorentinaPresent == false) then
	
--[Florentina is Present]
--Later-stage stuff.
else

end

--[Common Ending]
--Cutscene blocker.
fnCutsceneBlocker()
