--[Combat Begin]
--Scenes that may or may not play at the beginning of combat. Just flavour text.

--[Variables]
--Party composition.
local bIsFlorentinaPresent = false
if(gsFollowersTotal > 0) then bIsFlorentinaPresent = true end

--Increment the victory counter.
local iCombatVictories = VM_GetVar("Root/Variables/Chapter1/Counts/iCombatVictories", "N") + 1
VM_SetVar("Root/Variables/Chapter1/Counts/iCombatVictories", "N", iCombatVictories)

--The game's first victory never plays a dialogue. This is because it's the battle vs. the cultist.
if(iCombatVictories == 1) then return end

--If Mei has not met with Florentina yet, increment the other victory counter.
local iVictoriesWhenFlorentinaJoined = VM_GetVar("Root/Variables/Chapter1/Counts/iVictoriesWhenFlorentinaJoined", "N")
if(bIsFlorentinaPresent == false) then
	iVictoriesWhenFlorentinaJoined = iVictoriesWhenFlorentinaJoined + 1
	VM_SetVar("Root/Variables/Chapter1/Counts/iVictoriesWhenFlorentinaJoined", "N", iVictoriesWhenFlorentinaJoined)
end

--[Roller]
--Roll.
local iRoll = LM_GetRandomNumber(0, 100)
	
--Variables for the upgrade dialogue.
local iMeiHasRustyKatana = AdInv_GetProperty("Is Item Equipped", "Mei", "Rusty Katana")
local iFlorentinaSaidUpgrade = VM_GetVar("Root/Variables/Chapter1/Counts/iFlorentinaSaidUpgrade", "N")
AC_PushPartyMember("Mei")
	local sMeiLevel = ACE_GetProperty("Level")
DL_PopActiveObject()

--Special: Always play this scene after the first battle Florentina is present for.
if(iCombatVictories == iVictoriesWhenFlorentinaJoined + 1 and bIsFlorentinaPresent == true) then

--Special: Florentina prompts Mei to upgrade her weapon.
elseif(iFlorentinaSaidUpgrade == 0.0 and iMeiHasRustyKatana == 1.0 and sMeiLevel >= 3 and iCombatVictories >= iVictoriesWhenFlorentinaJoined + 3 and bIsFlorentinaPresent == true) then

--Normal roller. 15% chance of displaying any scene.
elseif(iRoll >= 15) then
	return
end

--[Common Setup]
--Setup. Mei is on the left, Florentina is on the right, if present.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--[Mei is Alone]
--Mostly used at the start of the game.
if(bIsFlorentinaPresent == false) then
	
	--Roll which scene to use.
	local iRoll = LM_GetRandomNumber(0, 5)
	
	--Generic scene:
	if(iRoll == 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] No trouble at all!") ]])
	
	--Generic scene:
	elseif(iRoll == 1) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] *pant*[SOFTBLOCK] *pant*[SOFTBLOCK] They're not so tough...") ]])
	
	--Generic scene:
	elseif(iRoll == 2) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] Hey, get back up! I'm not done with you yet![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Angry] ...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Okay fine then...") ]])
	
	--Generic scene:
	elseif(iRoll == 3) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] Sheesh, I am not good at this...") ]])
	
	--Generic scene:
	elseif(iRoll == 4) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] A thin girl in a dress fighting with a sword?[SOFTBLOCK] My life has become a bad anime![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] Or maybe a really good one...") ]])
	
	--Generic scene:
	elseif(iRoll == 5) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] (Just -[SOFTBLOCK] don't think about it.[SOFTBLOCK] Gotta keep going...)") ]])
	end
	
--[Florentina is Present]
--Later-stage stuff.
else
	
	--Florentina appears on the right side.
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Roll which scene to use.
	local iRoll = LM_GetRandomNumber(0, 5)
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Special: Florentina tells Mei to upgrade her weapon.
	if(iFlorentinaSaidUpgrade == 0.0 and iMeiHasRustyKatana == 1.0 and sMeiLevel >= 3 and iCombatVictories >= iVictoriesWhenFlorentinaJoined + 3) then
		VM_SetVar("Root/Variables/Chapter1/Counts/iFlorentinaSaidUpgrade", "N", 1.0)
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] I'm really impressed, Mei.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] It's not so hard with a bit of practice...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Moreso that you manage to cut anything tougher than butter with that rusty piece of junk.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] This katana has saved my skin a few times![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Yeah, that's nice, but you should probably get it replaced.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] The equipment vendor at the trading post had a much nicer one for sale.[SOFTBLOCK] Worth a look.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] And now I'm left to assume you have some sort of deal with him.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I do, but you apparently haven't heard of a win-win before.") ]])
	
	--Special: Florentina comments on Mei's prowess.
	elseif(iCombatVictories == iVictoriesWhenFlorentinaJoined + 1) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Geez kid, you pack a punch.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] You didn't mention you're a warrior.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] But I'm not![SOFTBLOCK] This is my first time with a sword in my hands![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] If I didn't know better, I'd almost think you weren't full of it.[SOFTBLOCK] I like that!") ]])
	
	--Generic scene:
	elseif(iRoll == 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] This isn't so hard, is it?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] That's because I do most of the work![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I am...[SOFTBLOCK] supervising...") ]])
	
	--Generic scene:
	elseif(iRoll == 1) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] I had forgotten how quickly violence solves my problems.[SOFTBLOCK] We should do this more often.[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Is that the right lesson to take away from this?") ]])
	
	--Generic scene:
	elseif(iRoll == 2) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] This is getting a bit easier...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Keep up the aggression and I might actually start to like you.") ]])
	
	--Generic scene:
	elseif(iRoll == 3) then
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Neutral] Hah![SOFTBLOCK] I feel almost bad for everyone who gets in our way![BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] You do?[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] I said almost.") ]])
		
	--Form-variant scene:
	elseif(iRoll == 4) then
		
		--Human.
		if(sMeiForm == "Human") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] For a human, you're not half-bad.[SOFTBLOCK] Not half-good, either.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Laugh] That almost sounded like a nice thing to say.[SOFTBLOCK] I'll take it!") ]])
		
		--Alraune.
		elseif(sMeiForm == "Alraune") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Cripes, you let off a lot of pollen when you fight.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Which means what, exactly?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] That you like it.[SOFTBLOCK] A lot.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Sad] I -[SOFTBLOCK] I guess I do...") ]])
		
		--Bee.
		elseif(sMeiForm == "Bee") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Thanks for the help![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Well I wasn't just going to stand there.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Oh, oops.[SOFTBLOCK] I meant my hive.[SOFTBLOCK] They were calling out tips.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] But -[SOFTBLOCK] you were great, too![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Pfft.") ]])
			
		--Ghost.
		elseif(sMeiForm == "Ghost") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] I don't feel any remorse...[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Consider that an advantage.[BLOCK][CLEAR]") ]])
		
		--Slime.
		elseif(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Surprise] Gross, you got goop everywhere![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] It's wonderful, isn't it?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] You're creeping me out.") ]])
			
		--Werecat.
		elseif(sMeiForm == "Werecat") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Strong or weak, any fight keeps the claws sharp.[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Maybe I should get a cat for the shop...") ]])
		end
	
	--Generic scene:
	elseif(iRoll == 5) then
		if(sMeiForm == "Human") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Didn't even break a sweat![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Wouldn't want you to ruin your makeup, right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Offended] If I had known I was going to be transported to an alien world today, I wouldn't have worn this!") ]])
		
		--Alraune.
		elseif(sMeiForm == "Alraune") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] Didn't even break a sweat![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] ...[SOFTBLOCK] Can we sweat?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Facepalm] *sigh*") ]])
		
		--Bee.
		elseif(sMeiForm == "Bee") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Didn't even break a sweat![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Wouldn't want to ruin your makeup, right?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Happy] This isn't makeup.[SOFTBLOCK] Honey just naturally makes your skin smooth!") ]])
			
		--Ghost.
		elseif(sMeiForm == "Ghost") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Didn't even break a sweat![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Blush] Er, well, you know what I mean...") ]])
		
		--Slime.
		elseif(sMeiForm == "Slime") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Didn't even break a sweat![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Confused] Is that some kind of joke?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Heh, oops...") ]])
			
		--Werecat.
		elseif(sMeiForm == "Werecat") then
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Didn't even break a sweat![BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina:[E|Happy] Are you going to clean yourself with your tongue?[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Not while you're watching me...") ]])
		end
	end

end

--[Common Ending]
--Cutscene blocker.
fnCutsceneBlocker()
