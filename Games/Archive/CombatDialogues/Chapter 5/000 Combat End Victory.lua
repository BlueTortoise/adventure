--[Combat Begin]
--Scenes that may or may not play at the beginning of combat. Can be flavour text, can also be quest handlers.
local iTotalDefeatTags = AC_GetProperty("Total Defeat Tags")

--[Raibies Quest]
local iRaibieQuest          = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
local bIsRaibieInThisBattle = AC_GetProperty("Is Defeat Tag Present", "Raibies")
if(iRaibieQuest == 1.0 and bIsRaibieInThisBattle == true) then

    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 2.0)

    --Setup.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] All right, get some samples while she's knocked out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "55:[E|Neutral][SOUND|World|TakeItem] Hair and skin samples.[SOFTBLOCK] These should do.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Smirk] Let's get these back to the doctor post-haste!") ]])
    fnCutsceneBlocker()

end
