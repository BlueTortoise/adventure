--[Combat Dialogue Resolve]
--Script used to resolve what, if any, dialogue plays during combat. This is not the same as in-battle cutscenes, which
-- are typically used for bosses. This is the trash-talk segments that play at the beginning or end of the battle.
--Some of these are random flavor rolls, some are combat-specific. All of the relevent state variables are available, like
-- which enemy is currently being fought and where the battle is located.

--[Arguments]
--Argument Listing:
-- 0: sSituation - Will be usually "Beginning" or "Defeat" or "Victory". These are the basic conditions that combat usually uses. 
--                 Some cutscene battles use special names.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sSituation = LM_GetScriptArgument(0)

--[Execution]
--Called when battle begins.
if(sSituation == "Beginning") then
	
	--Chapter 1.
	if(AC_GetProperty("Is Character In Party", "Mei") == true) then
		LM_ExecuteScript(fnResolvePath() .. "Chapter 1/000 Combat Begin.lua")
	
	--Christine indicates this is Chapter 5.
	elseif(AC_GetProperty("Is Character In Party", "Christine") == true) then
	end

--Called when battle ends and the player wins.
elseif(sSituation == "Victory") then
	
	--Chapter 1.
	if(AC_GetProperty("Is Character In Party", "Mei") == true) then
		LM_ExecuteScript(fnResolvePath() .. "Chapter 1/000 Combat End Victory.lua")
	
	--Christine indicates this is Chapter 5.
	elseif(AC_GetProperty("Is Character In Party", "Christine") == true) then
		LM_ExecuteScript(fnResolvePath() .. "Chapter 5/000 Combat End Victory.lua")
	end

--Called when the battle ends and the player is defeated.
elseif(sSituation == "Defeat") then

end
