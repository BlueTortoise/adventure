--[Alraune]
--Medium-grade enemy, can take a few hits. Also heals herself for 1HP with every attack.
if(TA_GetProperty("Is Mugging Check") == true) then
    
    --Base.
    TA_SetProperty("Mug Platina", 16 * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", 14 * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iItemRoll >= 1 and iItemRoll <= 5) then
        TA_SetProperty("Mug Item", "Adamantite Powder x1")
    end
    return
end

--Enemy creation.
AC_CreateEnemy("Alraune")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Alraune")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Alraune")

	--Base Statistics
	ACE_SetProperty("Health Max", 95)
	ACE_SetProperty("Health", 95)
	
	--Loot
    ACE_SetProperty("EXP", 14)
    ACE_SetProperty("Platina", 16)
	ACE_SetProperty("Register Drop",  1, 5, "Adamantite Powder x1")
	
	--Equipment Variants
	ACE_SetProperty("Damage", 26)
	ACE_SetProperty("Speed", 6)
	ACE_SetProperty("Protection", 0)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.25)
	ACE_SetProperty("Resistance", gciFactor_Ice,       0.90)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 0.90)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.65)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", -2)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Alraunes", 1)

--Variable setting.
local sMeiSeenPartirhuman = VM_GetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S")
if(sMeiSeenPartirhuman == "Nothing") then
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Alraune")
end
