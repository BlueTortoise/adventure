--[Arachnophelia]
--Boss of the Quantir event seuqence.
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("Arachnophelia")

	--AI.
	ACE_SetProperty("AI Override", gsRoot .. "Enemies/Combat AI/Warden.lua")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Arachnophelia")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Arachnophelia")

	--Base Statistics
	ACE_SetProperty("Health Max", 2000)
	ACE_SetProperty("Health", 2000)
	
	--Loot
    ACE_SetProperty("EXP", 227)
    ACE_SetProperty("Platina", 0)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 1600)
	ACE_SetProperty("Speed", 0)
	ACE_SetProperty("Protection", 0)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     0.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Stun Properties
	ACE_SetProperty("Stun Threshold", 200)
    ACE_SetProperty("Stun Decrement", 200)
	
	--Starting action is an attack that will one-shot anyone who is not properly prepared for it.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Oliberate the intruder.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Damage No Scatter", true)
		ACA_SetProperty("Speed Modifer", 110)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()
WD_SetProperty("Unlock Topic", "Warden", 1)
