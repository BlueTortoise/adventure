--[Cultist Corrupter]
--Weak cultist that does an insultingly small amount of damage. Has enough health to tank several rounds of hits.
-- More Zombees are being summoned, so take her out quickly!

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--[Mug Handling]
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--[Enemy creation]
--Standard toughness.
AC_CreateEnemy("Corrupter")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/CultistF")
	--ACE_SetProperty("Card Portrait",  "Null")
	ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/CultistF")

	--Base Statistics
	ACE_SetProperty("Health Max", 773)
	ACE_SetProperty("Health", 773)
	
	--Loot
    ACE_SetProperty("EXP", 221)
	ACE_SetProperty("Platina", 267)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 3)
	ACE_SetProperty("Speed", 4)
	ACE_SetProperty("Protection", 0)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.25)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    0.90)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.25)
	ACE_SetProperty("Resistance", gciFactor_Blind,     4.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Cultists", 1)