--[Cultist Female]
--Fairly weak cultist enemies encountered at the start of the game. They don't pose much of a threat.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--[Cultist]
--Standard toughness.
if(iToughness < 1) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 31 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 9 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 20) then
            TA_SetProperty("Mug Item", "Tattered Rags")
        end
        return
    end

    --Enemy creation.
	AC_CreateEnemy("Cultist")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/CultistF")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/CultistF")

		--Base Statistics
		ACE_SetProperty("Health Max", 85)
		ACE_SetProperty("Health", 85)
		
		--Loot
		ACE_SetProperty("EXP", 9)
		ACE_SetProperty("Platina", 31)
		ACE_SetProperty("Register Drop",  1, 20, "Tattered Rags")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 20)
		ACE_SetProperty("Speed", 4)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.25)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    0.90)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.25)
		ACE_SetProperty("Resistance", gciFactor_Blind,     4.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()
	
--[Acolyte]
--Tougher version of the cultist.
elseif(iToughness == 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 62 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 22 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 20) then
            TA_SetProperty("Mug Item", "Tattered Rags")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Acolyte")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/CultistF")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/CultistF")

		--Base Statistics
		ACE_SetProperty("Health Max", 139)
		ACE_SetProperty("Health", 139)
		
		--Loot
		ACE_SetProperty("EXP", 22)
		ACE_SetProperty("Platina", 62)
		ACE_SetProperty("Register Drop",  1, 20, "Tattered Rags")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 28)
		ACE_SetProperty("Speed", 5)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.25)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    0.90)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.25)
		ACE_SetProperty("Resistance", gciFactor_Blind,     4.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()
	
--[Palm]
--Tougher version of the cultist.
elseif(iToughness == 2.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 71 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 37 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 20) then
            TA_SetProperty("Mug Item", "Tattered Rags")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Palm")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/CultistF")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/CultistF")

		--Base Statistics
		ACE_SetProperty("Health Max", 201)
		ACE_SetProperty("Health", 201)
		
		--Loot
		ACE_SetProperty("EXP", 37)
		ACE_SetProperty("Platina", 71)
		ACE_SetProperty("Register Drop",  1, 20, "Tattered Rags")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 45)
		ACE_SetProperty("Speed", 6)
		ACE_SetProperty("Protection", 10)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.25)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    0.90)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.25)
		ACE_SetProperty("Resistance", gciFactor_Blind,     2.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()
end

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Cultists", 1)