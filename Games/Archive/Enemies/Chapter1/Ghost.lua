--[Ghost]
--Durable, immune to bleed and poison. Vulnerable to shadow damage.
if(TA_GetProperty("Is Mugging Check") == true) then
    
    --Base.
    TA_SetProperty("Mug Platina", 35 * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", 36 * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iItemRoll >= 1 and iItemRoll <= 5) then
        TA_SetProperty("Mug Item", "Adamantite Powder x1")
    end
    return
end

--Enemy creation.
AC_CreateEnemy("Ghost")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Ghost")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/MaidGhost")

	--Base Statistics
	ACE_SetProperty("Health Max", 72)
	ACE_SetProperty("Health", 72)
	
	--Loot
    ACE_SetProperty("EXP", 36)
    ACE_SetProperty("Platina", 35)
	ACE_SetProperty("Register Drop",  1, 5, "Adamantite Powder x1")
	
	--Equipment Variants
	ACE_SetProperty("Damage", 51)
	ACE_SetProperty("Speed", 0)
	ACE_SetProperty("Protection", 0)
	
	--Resistances
	ACE_SetProperty("Resistance", gciFactor_Slash,     0.50)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    0.50)
	ACE_SetProperty("Resistance", gciFactor_Strike,    0.50)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 1.00)
		ACA_SetProperty("Speed Modifer", 10)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Ghosts", 1)

--Variable setting.
local sMeiSeenPartirhuman = VM_GetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S")
if(sMeiSeenPartirhuman == "Nothing") then
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Ghost")
end