--[Infirm]
--This thing is... not right.
AC_CreateEnemy("Infirm")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Infirm")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Infirm")

	--Base Statistics
	ACE_SetProperty("Health Max", 21755)
	ACE_SetProperty("Health", 1530)
	
	--Loot
    ACE_SetProperty("EXP", 210)
    ACE_SetProperty("Platina", 0)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 41)
	ACE_SetProperty("Speed", 5)
	ACE_SetProperty("Protection", 50)
	
	--Resistances
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      0.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.90)
	ACE_SetProperty("Resistance", gciFactor_Blind,     0.05)
	ACE_SetProperty("Resistance", gciFactor_Poison,    1.90)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
	
	--Stun Properties
	ACE_SetProperty("Stun Threshold", 120)
    ACE_SetProperty("Stun Decrement", 10)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 0.50)
		ACA_SetProperty("Damage Amount", gciFactor_Bleed, 0.70)
		ACA_SetProperty("Speed Modifer", 3)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()
WD_SetProperty("Unlock Topic", "Dungeon", 1)