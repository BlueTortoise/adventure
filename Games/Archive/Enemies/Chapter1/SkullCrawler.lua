--[SkullCrawler]
--It was always in the room with you, but you could never perceive it. Maybe it just hasn't noticed you yet.
if(TA_GetProperty("Is Mugging Check") == true) then
    
    --Base.
    TA_SetProperty("Mug Platina", 12 * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", 19 * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iItemRoll >= 1 and iItemRoll <= 7) then
        TA_SetProperty("Mug Item", "Emerald")
    end
    return
end

--Enemy creation.
AC_CreateEnemy("SkullCrawler")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/SkullCrawler")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/SkullCrawler")

	--Base Statistics
	ACE_SetProperty("Health Max", 122)
	ACE_SetProperty("Health", 122)
	
	--Loot
    ACE_SetProperty("EXP", 19)
    ACE_SetProperty("Platina", 12)
	ACE_SetProperty("Register Drop",  1, 7, "Emerald")
	
	--Equipment Variants
	ACE_SetProperty("Damage", 35)
	ACE_SetProperty("Speed", 12)
	ACE_SetProperty("Protection", 0)
	
	--Resistances
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      0.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     0.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()