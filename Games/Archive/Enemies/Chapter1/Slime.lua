--[Slime]
--Durable, recovers HP after each action. Annoying! Fairly slow, though, so vulnerable to blind. Also hard to bleed/poison.
if(TA_GetProperty("Is Mugging Check") == true) then
    
    --Base.
    TA_SetProperty("Mug Platina", 16 * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", 14 * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iItemRoll >= 1 and iItemRoll <= 10) then
        TA_SetProperty("Mug Item", "Ruined Armor")
    end
    if(iItemRoll >= 11 and iItemRoll <= 20) then
        TA_SetProperty("Mug Item", "Broken Spear")
    end
    return
end

--Enemy creation.
AC_CreateEnemy("Slime")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Slime")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Slime")

	--Base Statistics
	ACE_SetProperty("Health Max", 145)
	ACE_SetProperty("Health", 145)
	
	--Loot
    ACE_SetProperty("EXP", 14)
    ACE_SetProperty("Platina", 16)
	ACE_SetProperty("Register Drop",  1, 10, "Ruined Armor")
	ACE_SetProperty("Register Drop", 11, 20, "Broken Spear")
	
	--Equipment Variants
	ACE_SetProperty("Damage", 17)
	ACE_SetProperty("Speed", 3)
	ACE_SetProperty("Protection", 10)
	
	--Resistances
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    0.85)
	ACE_SetProperty("Resistance", gciFactor_Strike,    0.85)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.35)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.50)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.50)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Absorption", 20)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Slimes", 1)

--Variable setting.
local sMeiSeenPartirhuman = VM_GetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S")
if(sMeiSeenPartirhuman == "Nothing") then
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Slime")
end