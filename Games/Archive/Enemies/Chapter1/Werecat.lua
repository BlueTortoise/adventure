--[Werecat]
--Lightning fast, vulnerable to fire and ice.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--[Werecat]
--Standard toughness.
if(iToughness < 1) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 34 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 20 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 13) then
            TA_SetProperty("Mug Item", "Tattered Rags")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Werecat")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Werecat")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Werecat")

		--Base Statistics
		ACE_SetProperty("Health Max", 96)
		ACE_SetProperty("Health", 96)
		
		--Loot
		ACE_SetProperty("EXP", 20)
		ACE_SetProperty("Platina", 34)
		ACE_SetProperty("Register Drop",  1, 13, "Tattered Rags")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 32)
		ACE_SetProperty("Speed", 17)
		ACE_SetProperty("Protection", 10)
		
		--Resistances
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.35)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.25)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.45)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.50)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashClaw")
			fnStandardAbilitySounds("SlashClaw")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()
	
--[Stalker]
--Get out of here, werecat.
elseif(iToughness == 1) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 38 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 21 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 13) then
            TA_SetProperty("Mug Item", "Tattered Rags")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Stalker")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Werecat")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Werecat")

		--Base Statistics
		ACE_SetProperty("Health Max", 121)
		ACE_SetProperty("Health", 121)
		
		--Loot
		ACE_SetProperty("EXP", 21)
		ACE_SetProperty("Platina", 38)
		ACE_SetProperty("Register Drop",  1, 13, "Tattered Rags")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 42)
		ACE_SetProperty("Speed", 17)
		ACE_SetProperty("Protection", 10)
		
		--Resistances
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.35)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.25)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.45)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.50)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashClaw")
			fnStandardAbilitySounds("SlashClaw")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()
	
	--Increment the Cassandra werecat encounter number, if the event is taking place.
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
	if(iStartedCassandraEvent == 1.0) then
		
		--Set the override for the next encounter.
		AC_SetProperty("Next Music Override", "TimeSensitive", -1.0)
		
		--Increment.
		local iCassandraEncounters = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N")
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N", iCassandraEncounters + 1.0)
		
		--Set the time of day.
		fnSetCassandraTime()
	end
	
end

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Werecats", 1)

--Variable setting.
local sMeiSeenPartirhuman = VM_GetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S")
if(sMeiSeenPartirhuman == "Nothing") then
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Werecat")
end