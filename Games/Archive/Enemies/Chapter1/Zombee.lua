--[Zombee]
--30% more durable, 30% more dangerous, 30% more zom. Zombee! THE PUNS ARE TOO MUCH.
if(TA_GetProperty("Is Mugging Check") == true) then
    
    --Base.
    TA_SetProperty("Mug Platina", 52 * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", 24 * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iItemRoll >= 1 and iItemRoll <= 2) then
        TA_SetProperty("Mug Item", "Adamantite Flakes x1")
    end
    if(iItemRoll >= 11 and iItemRoll <= 15) then
        TA_SetProperty("Mug Item", "Adamantite Powder x1")
    end
    return
end

--Enemy creation.
AC_CreateEnemy("Zombee")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Zombee")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Zombee")

	--Base Statistics
	ACE_SetProperty("Health Max", 175)
	ACE_SetProperty("Health", 175)
	
	--Loot
    ACE_SetProperty("EXP", 24)
    ACE_SetProperty("Platina", 52)
	ACE_SetProperty("Register Drop", 11, 15, "Adamantite Powder x1")
	ACE_SetProperty("Register Drop",  1,  2, "Adamantite Flakes x1")
	
	--Equipment Variants
	ACE_SetProperty("Damage", 35)
	ACE_SetProperty("Speed", 5)
	ACE_SetProperty("Protection", 5)
	
	--Resistances
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.25)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.25)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      3.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    3.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     0.05)
	ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
	
	--Stun Properties
	ACE_SetProperty("Stun Threshold", 50)
    ACE_SetProperty("Stun Decrement", 5)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Damage Amount", gciFactor_Poison, 0.20)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Zombees", 1)