--[Zombee Special]
--Special zombee that gives no EXP/loot.
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("Zombee")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Zombee")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Zombee")

	--Base Statistics
	ACE_SetProperty("Health Max", 175)
	ACE_SetProperty("Health", 175)
	
	--Loot
    ACE_SetProperty("EXP", 0)
    ACE_SetProperty("Platina", 0)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 35)
	ACE_SetProperty("Speed", 5)
	ACE_SetProperty("Protection", 5)
	
	--Resistances
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.25)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.25)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      3.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    3.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     0.05)
	ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
	
	--Stun Properties
	ACE_SetProperty("Stun Threshold", 50)
    ACE_SetProperty("Stun Decrement", 5)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Damage Amount", gciFactor_Poison, 0.20)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Zombees", 1)