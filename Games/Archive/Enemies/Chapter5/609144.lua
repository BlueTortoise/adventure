--[End Boss of Tellurium Mines]
--Appears on Floor 50. Has a specific AI that changes who it attacks based on its current mode.
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("609144")

	--AI.
	ACE_SetProperty("AI Override", gsRoot .. "Enemies/Combat AI/609144.lua")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/609144")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/609144")

	--Base Statistics
	ACE_SetProperty("Health Max", 8000)
	ACE_SetProperty("Health", 8000)
    ACE_SetProperty("Stun Threshold", 90)
    ACE_SetProperty("Stun Decrement", 5)
	
	--Loot
    ACE_SetProperty("EXP", 1240)
    ACE_SetProperty("Platina", 915)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 10)
	ACE_SetProperty("Speed", 7)
	ACE_SetProperty("Initiative", -100)
	ACE_SetProperty("Evade", 20)
	ACE_SetProperty("Protection", 0)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   1.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 17.50)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		ACA_SetProperty("Action Never Misses", true)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
	DL_PopActiveObject()
	
	--Claw. Does more damage, targets Christine.
	ACE_CreateAction("As Action", "Claw")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 17.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		ACA_SetProperty("Action Never Misses", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
    
    --Flail. Only used against 55.
	ACE_CreateAction("As Action", "Flail")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 17.50)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		ACA_SetProperty("Action Never Misses", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
	
	--Multi-hit. Used in one stance.
	ACE_CreateAction("As Action", "Sweep")
		ACA_SetProperty("Description", "Hit erryone!")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 5.50)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		ACA_SetProperty("Action Never Misses", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
	
	--Multi-hit, reduces the party to 1HP. Ignore damage computation.
	ACE_CreateAction("As Action", "Devastate")
		ACA_SetProperty("Description", "Hit erryone!")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Strike,  1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		ACA_SetProperty("Action Never Misses", true)
		ACA_SetProperty("Action Devastates", true)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()
