--[Christine]
--Finale of the Gala. She falls under 201890's influence and attacks the party.
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("Christine")

	--AI.
	ACE_SetProperty("AI Override", gsRoot .. "Enemies/Combat AI/Christine.lua")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Christine_Eldritch")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Christine_Eldritch")

	--Base Statistics
	ACE_SetProperty("Health Max", 2000)
	ACE_SetProperty("Health", 2000)
	
	--Loot
    ACE_SetProperty("EXP", 1)
    ACE_SetProperty("Platina", 1)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 25)
	ACE_SetProperty("Speed", 6)
	ACE_SetProperty("Protection", 70)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.50)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.50)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   0.20)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
	DL_PopActiveObject()
	
	--Batter. Deals extra damage.
	ACE_CreateAction("As Action", "Batter")
		ACA_SetProperty("Description", "Special attack.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 2.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
	DL_PopActiveObject()
	
	--Sweep. Hits the entire party.
	ACE_CreateAction("As Action", "Sweep")
		ACA_SetProperty("Description", "Special attack.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.60)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
	DL_PopActiveObject()
	
    --Rally. Heals Christine a bit.
	ACE_CreateAction("As Action", "Rally")
		ACA_SetProperty("Description", "Special attack.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.25)
		ACA_SetProperty("Self Healing Percent", 2)
        ACA_SetProperty("Combo Generation", 1)
		ACA_SetProperty("Cooldown", 1)
		ACA_SetProperty("Speed Modifer", 3)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Project Vivify", 1)
