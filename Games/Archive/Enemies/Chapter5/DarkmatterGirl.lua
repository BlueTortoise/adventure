--[Darkmatter Girl]
--Girl made of unknown material, seems capable of moving through the void unhindered. Enigmatic.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Toughness 0. Default.
if(iToughness < 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 36 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 24 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 15) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Darkmatter")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/DarkmatterGirl")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/DarkmatterGirl")

		--Base Statistics
		ACE_SetProperty("Health Max", 241)
		ACE_SetProperty("Health", 241)
		
		--Loot
		ACE_SetProperty("EXP", 24)
		ACE_SetProperty("Platina", 36)
		ACE_SetProperty("Register Drop",  1, 15, "Adamantite Powder x1")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 48)
		ACE_SetProperty("Speed", 6)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
        ACE_SetProperty("Resistance", gciFactor_Slash,     0.90)
        ACE_SetProperty("Resistance", gciFactor_Pierce,    0.90)
        ACE_SetProperty("Resistance", gciFactor_Strike,    0.90)
        ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
        ACE_SetProperty("Resistance", gciFactor_Ice,       0.10)
        ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
        ACE_SetProperty("Resistance", gciFactor_Holy,      1.50)
        ACE_SetProperty("Resistance", gciFactor_Shadow,    0.50)
        ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
        ACE_SetProperty("Resistance", gciFactor_Blind,     2.00)
        ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
        ACE_SetProperty("Resistance", gciFactor_Terrify,   2.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashClaw")
			fnStandardAbilitySounds("SlashClaw")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end

--Unlocks this topic just by engaging.
WD_SetProperty("Unlock Topic", "Darkmatter Girls", 1)
