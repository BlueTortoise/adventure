--[Doll - Equinox H]
--Finale of Equinox.
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("8711")

	--AI.
	ACE_SetProperty("AI Override", gsRoot .. "Enemies/Combat AI/8711.lua")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Doll")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Doll")

	--Base Statistics
	ACE_SetProperty("Health Max", 605)
	ACE_SetProperty("Health", 605)
	
	--Loot
    ACE_SetProperty("EXP", 112)
    ACE_SetProperty("Platina", 240)
	ACE_SetProperty("Register Drop",  0, 100, "Adamantite Flakes x1")
	
	--Equipment Variants
	ACE_SetProperty("Damage", 22)
	ACE_SetProperty("Speed", 20)
	ACE_SetProperty("Protection", 0)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Stun properties.
	ACE_SetProperty("Stun Threshold", 50)
    ACE_SetProperty("Stun Decrement", 10)
	
	--Always hits the entire party with the basic attack.
	ACE_CreateAction("As Action", "Sonic Burst")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
	DL_PopActiveObject()
	
	--Creates a 90% protection shield for 3 turns.
	ACE_CreateAction("As Action", "Prismatic Shield")
		ACA_SetProperty("Description", "Create a potent shield for 5 turns.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("New Effect", gciEffect_Protection, gci_Target_Self, 90, 5)
		ACA_SetProperty("New Effect", gciEffect_Speed, gci_Target_Self, -13, 5)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Buff")
		fnStandardAbilitySounds("Buff")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Dolls", 1)
