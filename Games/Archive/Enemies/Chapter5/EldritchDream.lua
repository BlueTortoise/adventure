--[EldritchDream]
--Nothing unusual here.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Causes boss music. This will cause the NEXT battle to have boss music after this one. A trigger sets the first instance.
AC_SetProperty("Next Music Override", "MotherTheme", 0.0000)

--Toughness 0. Default.
if(iToughness < 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 145 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 225 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 15) then
            TA_SetProperty("Mug Item", "Adamantite Flakes x1")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Dreamer")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Dreamer")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Dreamer")

		--Base Statistics
		ACE_SetProperty("Health Max", 2100)
		ACE_SetProperty("Health", 2100)
        ACE_SetProperty("Stun Threshold", 200)
		
		--Loot
		ACE_SetProperty("EXP", 225)
		ACE_SetProperty("Platina", 145)
		ACE_SetProperty("Register Drop",  1, 15, "Adamantite Flakes x1")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 70)
		ACE_SetProperty("Speed", 7)
		ACE_SetProperty("Protection", 20)
		
		--Damage Factors
        ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
        ACE_SetProperty("Resistance", gciFactor_Pierce,    1.50)
        ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
        ACE_SetProperty("Resistance", gciFactor_Fire,      1.50)
        ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
        ACE_SetProperty("Resistance", gciFactor_Lightning, 1.50)
        ACE_SetProperty("Resistance", gciFactor_Holy,      1.50)
        ACE_SetProperty("Resistance", gciFactor_Shadow,    1.50)
        ACE_SetProperty("Resistance", gciFactor_Bleed,     0.50)
        ACE_SetProperty("Resistance", gciFactor_Blind,     0.00)
        ACE_SetProperty("Resistance", gciFactor_Poison,    0.00)
        ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashClaw")
			fnStandardAbilitySounds("SlashClaw")
		DL_PopActiveObject()
		
		--Hit the whole party.
		ACE_CreateAction("As Action", "Sweep")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.50)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashClaw")
			fnStandardAbilitySounds("SlashClaw")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end

--Unlocks this topic just by engaging.
--WD_SetProperty("Unlock Topic", "Creatures", 1)
