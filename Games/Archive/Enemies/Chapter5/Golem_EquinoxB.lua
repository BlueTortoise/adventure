--[Golem - Equinox B]
--Golem which uses piercing attacks. Also has a stunning strike.
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("Golem")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Golem")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/GolemSlave")

	--Base Statistics
	ACE_SetProperty("Health Max", 1600)
	ACE_SetProperty("Health", 1600)
	
	--Loot
    ACE_SetProperty("EXP", 65)
    ACE_SetProperty("Platina", 117)
	ACE_SetProperty("Register Drop",  0, 100, "Adamantite Flakes x1")
	
	--Equipment Variants
	ACE_SetProperty("Damage", 42)
	ACE_SetProperty("Speed", 5)
	ACE_SetProperty("Protection", 0)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Piercing attack. Effective against Latex Drones.
	ACE_CreateAction("As Action", "Clumsy Stab")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon. Piercing damage.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Pierce, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Pierce")
		fnStandardAbilitySounds("Pierce")
	DL_PopActiveObject()
	
	--Stunning attack. Golem has to cool for a turn after using it.
	ACE_CreateAction("As Action", "Knockout Punch")
		ACA_SetProperty("Description", "Deals stunning damage.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 0.70)
		ACA_SetProperty("New Effect", gciEffect_Stun, gci_Target_Single_Hostile, 130, 1)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Golems", 1)
