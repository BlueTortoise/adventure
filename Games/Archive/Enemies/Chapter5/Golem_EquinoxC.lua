--[Golem - Equinox C]
--Lord Unit. Has electrical attacks!
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("Golem")

	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/GolemLord")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/GolemLord")

	--Base Statistics
	ACE_SetProperty("Health Max", 950)
	ACE_SetProperty("Health", 950)
	
	--Loot
    ACE_SetProperty("EXP", 65)
    ACE_SetProperty("Platina", 117)
	ACE_SetProperty("Register Drop",  0, 100, "Adamantite Flakes x1")
	
	--Equipment Variants
	ACE_SetProperty("Damage", 42)
	ACE_SetProperty("Speed", 5)
	ACE_SetProperty("Protection", 0)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Electrical attack. VERY powerful against golems.
	ACE_CreateAction("As Action", "Shock")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon, deals electrical damage.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Lightning, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Electricity")
		fnStandardAbilitySounds("Electricity")
	DL_PopActiveObject()
	
	--Electrical attack. VERY powerful against golems, hits the whole party.
	ACE_CreateAction("As Action", "Discharge")
		ACA_SetProperty("Description", "Shock all enemies.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Lightning, 0.50)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Electricity")
		fnStandardAbilitySounds("Electricity")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Golems", 1)
