--[Golem - Equinox D]
--This enemy appears in the chemistry lab and uses corrosive attacks.
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("Golem")

	--AI.
	ACE_SetProperty("AI Override", gsRoot .. "Enemies/Combat AI/EquinoxGolemD.lua")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Golem")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/GolemSlave")

	--Base Statistics
	ACE_SetProperty("Health Max", 1450)
	ACE_SetProperty("Health", 1450)
	
	--Loot
    ACE_SetProperty("EXP", 65)
    ACE_SetProperty("Platina", 117)
	ACE_SetProperty("Register Drop",  0, 100, "Adamantite Flakes x1")
	
	--Equipment Variants
	ACE_SetProperty("Damage", 50)
	ACE_SetProperty("Speed", 5)
	ACE_SetProperty("Protection", 0)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Corrosive Strike, deals immediate damage as Striking and some as Corrosive.
	ACE_CreateAction("As Action", "Corrosive Strike")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon, also inflicting corrosion.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 0.70)
		ACA_SetProperty("Damage Amount", gciFactor_Corrode, 0.30)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
	DL_PopActiveObject()
	
	--Corrosive attack.
	ACE_CreateAction("As Action", "Acid Splash")
		ACA_SetProperty("Description", "Splash all enemies with corroding chemicals.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Corrode, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
	DL_PopActiveObject()
	
	--Mend. Restores 50 HP.
	ACE_CreateAction("As Action", "Mend")
		ACA_SetProperty("Description", "Restore 50 HP.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("Self Healing", 50)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Healing")
		fnStandardAbilitySounds("Healing")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Golems", 1)
