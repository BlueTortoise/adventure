--[Horrible Little Man]
--Lantern-bearing creature that sometimes uses fire damage.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Toughness 0. Default.
if(iToughness < 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 19 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 44 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 12) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        if(iItemRoll >= 95 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Qederite Gem")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Horrible Little Man")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Horrible")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Horrible")

		--Base Statistics
		ACE_SetProperty("Health Max", 160)
		ACE_SetProperty("Health", 160)
		
		--Loot
		ACE_SetProperty("EXP", 44)
		ACE_SetProperty("Platina", 19)
		ACE_SetProperty("Register Drop", 1,  12, "Adamantite Powder x1")
		ACE_SetProperty("Register Drop", 95, 100, "Qederite Gem")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 25)
		ACE_SetProperty("Speed", 6)
		ACE_SetProperty("Protection", 20)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.50)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   0.10)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("AI Priority", 70)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Fire attack!
		ACE_CreateAction("As Action", "StunAttack")
			ACA_SetProperty("Description", " Flaming attack.")
			ACA_SetProperty("AI Priority", 30)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Fire, 1.50)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

--More Initiative and a little HP.
elseif(iToughness == 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 30 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 52 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 2 and iItemRoll <= 12) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        if(iItemRoll >= 95 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Qederite Gem")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Horrible Little Man")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Horrible")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Horrible")

		--Base Statistics
		ACE_SetProperty("Health Max", 190)
		ACE_SetProperty("Health", 190)
		
		--Loot
		ACE_SetProperty("EXP", 52)
		ACE_SetProperty("Platina", 30)
		ACE_SetProperty("Register Drop",  2,  12, "Adamantite Powder x1")
		ACE_SetProperty("Register Drop", 95, 100, "Qederite Gem")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 25)
		ACE_SetProperty("Speed", 7)
		ACE_SetProperty("Protection", 20)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.50)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   0.10)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("AI Priority", 70)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Fire attack!
		ACE_CreateAction("As Action", "FireAttack")
			ACA_SetProperty("Description", " Flaming attack.")
			ACA_SetProperty("AI Priority", 30)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Fire, 1.50)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

--Biolabs variant. Extremely dangerous.
elseif(iToughness == 2.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 80 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 114 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 2 and iItemRoll <= 12) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        if(iItemRoll >= 95 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Qederite Gem")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Horrible Little Man")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Horrible")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Horrible")

		--Base Statistics
		ACE_SetProperty("Health Max", 650)
		ACE_SetProperty("Health", 650)
		
		--Loot
		ACE_SetProperty("EXP", 114)
		ACE_SetProperty("Platina", 80)
		ACE_SetProperty("Register Drop",  2,  12, "Adamantite Powder x1")
		ACE_SetProperty("Register Drop", 95, 100, "Qederite Gem")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 55)
		ACE_SetProperty("Speed", 7)
		ACE_SetProperty("Protection", 25)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.50)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   0.10)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("AI Priority", 70)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Fire attack!
		ACE_CreateAction("As Action", "FireAttack")
			ACA_SetProperty("Description", " Flaming attack.")
			ACA_SetProperty("AI Priority", 30)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Fire, 1.50)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end
