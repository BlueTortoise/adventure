--[Innsmouth Geisha]
--She's so pretty!

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Toughness 0. Default.
if(iToughness < 1.0 or true) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 92 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 45 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 15) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Geisha")

		--AI.
		ACE_SetProperty("AI Override", gsRoot .. "Enemies/Combat AI/InnGeisha.lua")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/InnGeisha")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/InnGeisha")

		--Base Statistics
		ACE_SetProperty("Health Max", 1212)
		ACE_SetProperty("Health", 1212)
		
		--Loot
		ACE_SetProperty("EXP", 45)
		ACE_SetProperty("Platina", 92)
		ACE_SetProperty("Register Drop",  1, 15, "Adamantite Powder x1")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 62)
		ACE_SetProperty("Speed", 7)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
        ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
        ACE_SetProperty("Resistance", gciFactor_Pierce,    2.00)
        ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
        ACE_SetProperty("Resistance", gciFactor_Fire,      2.00)
        ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
        ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
        ACE_SetProperty("Resistance", gciFactor_Holy,      1.50)
        ACE_SetProperty("Resistance", gciFactor_Shadow,    1.50)
        ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
        ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
        ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
        ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashClaw")
			fnStandardAbilitySounds("SlashClaw")
		DL_PopActiveObject()
        
        --Heavier hitting attack. Hits more often, too.
		ACE_CreateAction("As Action", "Pound")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Strike, 2.00)
			ACA_SetProperty("Speed Modifer", 10)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("Strike")
			fnStandardAbilitySounds("Strike")
		DL_PopActiveObject()
        
        --Also has a heal.
		ACE_CreateAction("As Action", "Restore")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_All_Allies)
            ACA_SetProperty("Target Healing Percent", 30)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("Healing")
			fnStandardAbilitySounds("Healing")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end

--Unlocks this topic just by engaging.
--WD_SetProperty("Unlock Topic", "Creatures", 1)
