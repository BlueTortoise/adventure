--[Latex Commander]
--Latex Drone that has a pack of scraprats following her around. Deals little damage but will buff the scraprats.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Toughness 0. Default.
if(iToughness < 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 10 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 27 * gcfMugExperienceRate)
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Latex Packmaster")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/LatexDrone")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/LatexDrone")

		--Base Statistics
		ACE_SetProperty("Health Max", 171)
		ACE_SetProperty("Health", 171)
		
		--Loot
		ACE_SetProperty("EXP", 27)
		ACE_SetProperty("Platina", 10)
		--ACE_SetProperty("Register Drop",  1, 5, "Adamantite Powder x1")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 22)
		ACE_SetProperty("Speed", 3)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Strike,    0.90)
		ACE_SetProperty("Resistance", gciFactor_Fire,      0.80)
		ACE_SetProperty("Resistance", gciFactor_Ice,       0.80)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 0.50)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     0.70)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    0.70)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("AI Priority", 20)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--They can also buff their party of scraprats.
		ACE_CreateAction("As Action", "BuffScraprats")
			ACA_SetProperty("Description", " Buff nearby scraprats!.\n (+10% Attack, 2 turns)")
			ACA_SetProperty("AI Priority", 80)
			ACA_SetProperty("Targetting", gci_Target_All_Allies)
			ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_All_Allies, 10, 2)
			fnStandardAttackAnim("Buff")
			fnStandardAbilitySounds("Buff")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end

--Unlocks this topic just by engaging.
WD_SetProperty("Unlock Topic", "Scraprats", 1)
WD_SetProperty("Unlock Topic", "LatexDrones", 1)
