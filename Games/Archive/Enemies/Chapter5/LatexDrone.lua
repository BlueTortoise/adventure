--[Latex Drone]
--Regular Latex Drone. Sometimes uses a stunning attack.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Toughness 0. Default.
if(iToughness < 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 18 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 24 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 8) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Latex Drone")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/LatexDrone")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/LatexDrone")

		--Base Statistics
		ACE_SetProperty("Health Max", 135)
		ACE_SetProperty("Health", 135)
		
		--Loot
		ACE_SetProperty("EXP", 24)
		ACE_SetProperty("Platina", 18)
		ACE_SetProperty("Register Drop",  1, 8, "Adamantite Powder x1")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 37)
		ACE_SetProperty("Speed", 5)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Strike,    0.90)
		ACE_SetProperty("Resistance", gciFactor_Fire,      0.80)
		ACE_SetProperty("Resistance", gciFactor_Ice,       0.80)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 0.50)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     0.70)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    0.70)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("AI Priority", 70)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Stun attack!
		ACE_CreateAction("As Action", "StunAttack")
			ACA_SetProperty("Description", " Attack that also stuns.")
			ACA_SetProperty("AI Priority", 30)
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 2.00)
			ACA_SetProperty("New Effect", gciEffect_Stun, gci_Target_Single_Hostile, 70, 1)
			fnStandardAttackAnim("SlashCross")
			fnStandardAbilitySounds("SlashCross")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end

--Unlocks this topic just by engaging.
WD_SetProperty("Unlock Topic", "Scraprats", 1)
WD_SetProperty("Unlock Topic", "LatexDrones", 1)
