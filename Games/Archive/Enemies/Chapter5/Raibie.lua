--[Raibie]
--Adorable electrical girl, turned green, frothing. Seems upset about something, maybe.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Toughness 0. Default.
if(iToughness < 1.0 or true) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 52 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 124 * gcfMugExperienceRate)
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Raibie")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
        
        --Quest Tag
        ACE_SetProperty("Add Defeat Tag", "Raibies")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Raibie")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Raibie")

		--Base Statistics
		ACE_SetProperty("Health Max", 814)
		ACE_SetProperty("Health", 814)
		
		--Loot
		ACE_SetProperty("EXP", 124)
		ACE_SetProperty("Platina", 52)
		--ACE_SetProperty("Register Drop",  1, 5, "Adamantite Powder x1")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 77)
		ACE_SetProperty("Speed", 9)
		ACE_SetProperty("Protection", 20)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.50)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 0.10)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.50)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.50)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   0.20)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Lightning, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("Electricity")
			fnStandardAbilitySounds("Electricity")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end

--Unlocks this topic just by engaging.
WD_SetProperty("Unlock Topic", "Raibies", 1)
