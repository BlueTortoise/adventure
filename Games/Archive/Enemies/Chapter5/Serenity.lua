--[Serenity Crater Boss]
--Finale of Serenity Crater. Constantly deals low damage to the whole party. Summons Void Rifts.
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("Serenity")

	--AI.
	ACE_SetProperty("AI Override", gsRoot .. "Enemies/Combat AI/Serenity.lua")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Serenity")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Serenity")

	--Base Statistics
	ACE_SetProperty("Health Max", 800)
	ACE_SetProperty("Health", 800)
	
	--Loot
    ACE_SetProperty("EXP", 300)
    ACE_SetProperty("Platina", 300)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 40)
	ACE_SetProperty("Speed", 2)
	ACE_SetProperty("Protection", 50)
    ACE_SetProperty("Stun Threshold", 1000)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    2.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       2.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.10)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.10)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   0.10)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
	
	--Attack, strikes the entire team for low damage.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
	DL_PopActiveObject()
    
    --Reconstitute. Restores 100 HP, reduces protection for 3 turns.
	ACE_CreateAction("As Action", "Reconstitute")
		ACA_SetProperty("Description", "Heal yourself and reduce protection.")
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
        ACA_SetProperty("Self Healing", 100)
        ACA_SetProperty("New Effect", gciEffect_Protection, gci_Target_Self, -50, 4)
		fnStandardAttackAnim("Healing")
		fnStandardAbilitySounds("Healing")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()
