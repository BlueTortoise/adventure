--[Tech Monstrosity]
--OH DEAR.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Toughness 0. Default.
if(iToughness < 1.0) then
    
    --Mugging.
    if(TA_GetProperty("Is Mugging Check") == true) then
        --Cannot be mugged.
        return
    end

    --Enemy creation.
	AC_CreateEnemy("Tech Monstrosity")

		--AI.
        ACE_SetProperty("AI Override", gsRoot .. "Enemies/Combat AI/Monstrosity.lua")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/TechMonstrosity")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/TechMonstrosity")

		--Base Statistics
		ACE_SetProperty("Health Max", 1000)
		ACE_SetProperty("Health", 1000)
		
		--Loot
		ACE_SetProperty("EXP", 0)
		ACE_SetProperty("Platina", 0)
		
		--Equipment Variants
		ACE_SetProperty("Damage", 1)
		ACE_SetProperty("Speed", 1)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("SlashClaw")
			fnStandardAbilitySounds("SlashClaw")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end

--Unlocks this topic just by engaging.
WD_SetProperty("Unlock Topic", "Monstrosity", 1)
