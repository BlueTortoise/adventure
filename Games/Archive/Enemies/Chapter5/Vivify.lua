--[EDG Boss LRT Facility]
--Finale of the LRT facility. Low damage but has a lot staying power and nasty corrosive attacks.
if(TA_GetProperty("Is Mugging Check") == true) then
    --Cannot be mugged.
    return
end

--Enemy creation.
AC_CreateEnemy("Vivify")

	--AI.
	ACE_SetProperty("AI Override", gsRoot .. "Enemies/Combat AI/Vivify.lua")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Vivify")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Vivify")

	--Base Statistics
	ACE_SetProperty("Health Max", 2250)
	ACE_SetProperty("Health", 2250)
	
	--Loot
    ACE_SetProperty("EXP", 200)
    ACE_SetProperty("Platina", 200)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 41)
	ACE_SetProperty("Speed", 7)
	ACE_SetProperty("Protection", 10)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.30)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 0.90)
	ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     1.50)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.50)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   0.20)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   0.25)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
	
	--Tentacle Crush. Deals 200% damage. Targets Christine first, 55 if Christine is down.
	ACE_CreateAction("As Action", "Tentacle Crush")
		ACA_SetProperty("Description", "Crush the enemy with your tentacles.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Strike, 5.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Strike")
		fnStandardAbilitySounds("Strike")
	DL_PopActiveObject()
	
	--Corrosive Spit. Deals corrosive damage over 5 turns.
	ACE_CreateAction("As Action", "Corrosive Spit")
		ACA_SetProperty("Description", "Deal corrosive daamge.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Corrode, 3.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Debuff")
		fnStandardAbilitySounds("Debuff")
	DL_PopActiveObject()
	
	--Roar. Demotivates the party and reduces their attack power and speed.
	ACE_CreateAction("As Action", "Roar")
		ACA_SetProperty("Description", "Reduce enemy damage and accuracy.")
		ACA_SetProperty("Targetting", gci_Target_All_Hostiles)
		ACA_SetProperty("Damage Amount", gciFactor_Terrify, 0.20)
		ACA_SetProperty("New Effect", gciEffect_DamagePercent, gci_Target_All_Hostiles, -15, 3)
		ACA_SetProperty("New Effect", gciEffect_Accuracy, gci_Target_All_Hostiles, -10, 3)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Debuff")
		fnStandardAbilitySounds("Debuff")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Vivify", 1)
WD_SetProperty("Unlock Topic", "2856", 1)
