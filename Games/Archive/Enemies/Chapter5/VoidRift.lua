--[Void Rift]
--Gross.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Toughness 0. Default.
if(iToughness < 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 42 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 20 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 3) then
            TA_SetProperty("Mug Item", "Adamantite Flakes x1")
        end
        if(iItemRoll >= 95 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Blurleen Gem")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Void Rift")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/VoidRift")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/VoidRift")

		--Base Statistics
		ACE_SetProperty("Health Max", 120)
		ACE_SetProperty("Health", 120)
		
		--Loot
		ACE_SetProperty("EXP", 20)
		ACE_SetProperty("Platina", 42)
		ACE_SetProperty("Register Drop",  1,  3, "Adamantite Flakes x1")
		ACE_SetProperty("Register Drop", 95, 100, "Blurleen Gem")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 44)
		ACE_SetProperty("Speed", 4)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     0.00)
		ACE_SetProperty("Resistance", gciFactor_Blind,     0.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    0.00)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Strike, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("Strike")
			fnStandardAbilitySounds("Strike")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()
    
--More HP and damage.
elseif(iToughness == 1.0) then
	AC_CreateEnemy("Void Rift")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/VoidRift")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/VoidRift")

		--Base Statistics
		ACE_SetProperty("Health Max", 280)
		ACE_SetProperty("Health", 280)
		
		--Loot
		ACE_SetProperty("EXP", 44)
		ACE_SetProperty("Platina", 64)
		ACE_SetProperty("Register Drop",  1,  10, "Adamantite Flakes x1")
		ACE_SetProperty("Register Drop", 95, 100, "Blurleen Gem")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 60)
		ACE_SetProperty("Speed", 4)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     0.00)
		ACE_SetProperty("Resistance", gciFactor_Blind,     0.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    0.00)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Strike, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("Strike")
			fnStandardAbilitySounds("Strike")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()
    
--Biolabs variant. Extremely dangerous.
elseif(iToughness == 2.0) then
	AC_CreateEnemy("Void Rift")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/VoidRift")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/VoidRift")

		--Base Statistics
		ACE_SetProperty("Health Max", 780)
		ACE_SetProperty("Health", 780)
		
		--Loot
		ACE_SetProperty("EXP", 92)
		ACE_SetProperty("Platina", 100)
		ACE_SetProperty("Register Drop",  1,  10, "Adamantite Flakes x1")
		ACE_SetProperty("Register Drop", 95, 100, "Blurleen Gem")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 120)
		ACE_SetProperty("Speed", 5)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     0.00)
		ACE_SetProperty("Resistance", gciFactor_Blind,     0.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    0.00)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Strike, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("Strike")
			fnStandardAbilitySounds("Strike")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end
