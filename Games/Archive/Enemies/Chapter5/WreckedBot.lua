--[Wrecked Security Bot]
--Security robot damaged in some conflict. Poor accuracy.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--Toughness 0. Default.
if(iToughness < 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 7 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 10 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 5) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Wrecked Bot")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/SecurityWrecked")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/WreckedBot")

		--Base Statistics
		ACE_SetProperty("Health Max", 122)
		ACE_SetProperty("Health", 122)
		
		--Loot
		ACE_SetProperty("EXP", 10)
		ACE_SetProperty("Platina", 7)
		ACE_SetProperty("Register Drop",  1, 5, "Adamantite Powder x1")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 14)
		ACE_SetProperty("Speed", 6)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     0.25)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Strike, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("Strike")
			fnStandardAbilitySounds("Strike")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

--Toughness 1. More of all stats.
elseif(iToughness == 1.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 11 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 12 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 5) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Damaged Bot")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/SecurityWrecked")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/WreckedBot")

		--Base Statistics
		ACE_SetProperty("Health Max", 122)
		ACE_SetProperty("Health", 122)
		
		--Loot
		ACE_SetProperty("EXP", 12)
		ACE_SetProperty("Platina", 11)
		ACE_SetProperty("Register Drop",  1, 5, "Adamantite Powder x1")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 24)
		ACE_SetProperty("Speed", 6)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     0.25)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("Strike")
			fnStandardAbilitySounds("Strike")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

--Toughness 2. More of all stats.
elseif(iToughness == 2.0) then

    --Mug Handling.
    if(TA_GetProperty("Is Mugging Check") == true) then
    
        --Base.
        TA_SetProperty("Mug Platina", 22 * gcfMugPlatinaRate)
        TA_SetProperty("Mug Experience", 18 * gcfMugExperienceRate)
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        if(iItemRoll >= 1 and iItemRoll <= 5) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        return
    end
    
    --Enemy creation.
	AC_CreateEnemy("Damaged Bot")

		--AI.
		ACE_SetProperty("AI Override", "DUMB AI")
		
		--Rendering
		ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/SecurityWrecked")
		--ACE_SetProperty("Card Portrait",  "Null")
		ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/WreckedBot")

		--Base Statistics
		ACE_SetProperty("Health Max", 180)
		ACE_SetProperty("Health", 180)
		
		--Loot
		ACE_SetProperty("EXP", 18)
		ACE_SetProperty("Platina", 22)
		ACE_SetProperty("Register Drop",  1, 5, "Adamantite Powder x1")
		
		--Equipment Variants
		ACE_SetProperty("Damage", 38)
		ACE_SetProperty("Speed", 7)
		ACE_SetProperty("Protection", 0)
		
		--Damage Factors
		ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Strike,    1.50)
		ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
		ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
		ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
		ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
		ACE_SetProperty("Resistance", gciFactor_Bleed,     0.25)
		ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
		ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
		ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
		
		--Starting action is just an attack.
		ACE_CreateAction("As Action", "Attack")
			ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
			ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
			ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
			ACA_SetProperty("Speed Modifer", 0)
			ACA_SetProperty("Action Never Crits", true)
			fnStandardAttackAnim("Strike")
			fnStandardAbilitySounds("Strike")
		DL_PopActiveObject()
		
		--Finish timers.
		ACE_SetProperty("Finish Timers")
		
	DL_PopActiveObject()

end

--Unlocks this topic just by engaging it.
WD_SetProperty("Unlock Topic", "Security Bots", 1)
