--[Golem - Mines Edition]
--Crazed, gibbering slave unit.

--[Random Components]
--Floor. Floor is scattered slightly, it's possible to battle an enemy that is 5 floors tougher than expected.
local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N") + LM_GetRandomNumber(-5, 5)
if(iCurrentMinesFloor < 0) then iCurrentMinesFloor = 0 end
if(iCurrentMinesFloor > 35) then iCurrentMinesFloor = 35 end

--Bonus Computation.
local iHPBonus = math.floor(iCurrentMinesFloor * 3.50)
local iXPBonus = math.floor(iCurrentMinesFloor * 1.60)
local iPlBonus = math.floor(iCurrentMinesFloor * 0.65)
local iDmBonus = math.floor(iCurrentMinesFloor * 0.45)
local iInBonus = math.floor(iCurrentMinesFloor * 0.05)

--Mug Handling.
if(TA_GetProperty("Is Mugging Check") == true) then

    --Base.
    TA_SetProperty("Mug Platina", (12 + iPlBonus) * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", (37 + iXPBonus) * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iCurrentMinesFloor < 20) then
        
        if(iItemRoll >= 1 and iItemRoll <= 5) then
            TA_SetProperty("Mug Item", "Credits Chip")
        end
    elseif(iCurrentMinesFloor < 40) then
        
        if(iItemRoll >= 1 and iItemRoll <= 7) then
            TA_SetProperty("Mug Item", "Credits Chip")
        end
    else
        
        if(iItemRoll >= 1 and iItemRoll <= 9) then
            TA_SetProperty("Mug Item", "Credits Chip")
        end
    end
    return
end

--Enemy creation.
AC_CreateEnemy("Maverick Golem")
    
    --[Loot Determination]
    if(iCurrentMinesFloor < 20) then
        ACE_SetProperty("Register Drop",  1,  5, "Credits Chip")
    elseif(iCurrentMinesFloor < 40) then
        ACE_SetProperty("Register Drop",  1,  7, "Credits Chip")
    else
        ACE_SetProperty("Register Drop",  1,  9, "Credits Chip")
    end
    
    --[Common Components]
	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")

	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Golem")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/GolemSlave")

	--Base Statistics
	ACE_SetProperty("Health Max", 210 + iHPBonus)
	ACE_SetProperty("Health", 210 + iHPBonus)
	
	--Loot
    ACE_SetProperty("EXP", 37 + iXPBonus)
    ACE_SetProperty("Platina", 12 + iPlBonus)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 22 + iDmBonus)
	ACE_SetProperty("Speed", 3 + iInBonus)
	ACE_SetProperty("Protection", 10)
	
	--Damage Factors
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.25)
	ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
	ACE_SetProperty("Resistance", gciFactor_Corrode,   3.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
        ACA_SetProperty("AI Priority", 30)
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
	DL_PopActiveObject()
	
	--Shock attack. Rarely used.
	ACE_CreateAction("As Action", "Shock")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
        ACA_SetProperty("AI Priority", 20)
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Lightning, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("Electricity")
		fnStandardAbilitySounds("Electricity")
	DL_PopActiveObject()
	
	--Ramble. Damages the golem slightly.
	ACE_CreateAction("As Action", "Maddened Rambling...")
		ACA_SetProperty("Description", "Swerve candle inside not solid.")
        ACA_SetProperty("AI Priority", 40)
		ACA_SetProperty("Targetting", gci_Target_Self)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.50)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashCross")
		fnStandardAbilitySounds("SlashCross")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()

--Unlocks this topic just by engaging her.
WD_SetProperty("Unlock Topic", "Golems", 1)
