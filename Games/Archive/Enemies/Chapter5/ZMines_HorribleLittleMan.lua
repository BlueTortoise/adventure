--[Horrible Little Man - Mines Version]
--Same as the base variety but has extra code to gain HP/Damage in the mines. Gains a corrosion attack after floor 20.

--Argument Listing:
-- 0: iToughness - Toughness rating. Always 0 for mines enemies.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--[Random Components]
--Floor. Floor is scattered slightly, it's possible to battle an enemy that is 5 floors tougher than expected.
local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N") + LM_GetRandomNumber(-5, 5)
if(iCurrentMinesFloor < 0) then iCurrentMinesFloor = 0 end
if(iCurrentMinesFloor > 35) then iCurrentMinesFloor = 35 end

--Bonus Computation.
local iHPBonus = math.floor(iCurrentMinesFloor * 3.50)
local iXPBonus = math.floor(iCurrentMinesFloor * 1.60)
local iPlBonus = math.floor(iCurrentMinesFloor * 0.65)
local iDmBonus = math.floor(iCurrentMinesFloor * 0.45)
local iInBonus = math.floor(iCurrentMinesFloor * 0.05)

--Mug Handling.
if(TA_GetProperty("Is Mugging Check") == true) then

    --Base.
    TA_SetProperty("Mug Platina", (19 + iPlBonus) * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", (44 + iXPBonus) * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iCurrentMinesFloor < 20) then
        
        if(iItemRoll >= 1 and iItemRoll <= 12) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        if(iItemRoll >= 20 and iItemRoll <= 50) then
            TA_SetProperty("Mug Item", "Bent Tools")
        end
        if(iItemRoll >= 95 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Qederite Gem")
        end
    elseif(iCurrentMinesFloor < 40) then
        
        if(iItemRoll >= 1 and iItemRoll <= 6) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        if(iItemRoll >= 7 and iItemRoll <= 14) then
            TA_SetProperty("Mug Item", "Adamantite Flakes x1")
        end
        if(iItemRoll >= 20 and iItemRoll <= 50) then
            TA_SetProperty("Mug Item", "Bent Tools")
        end
        if(iItemRoll >= 90 and iItemRoll <= 94) then
            TA_SetProperty("Mug Item", "Rubose Gem")
        end
        if(iItemRoll >= 95 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Qederite Gem")
        end
    else
        
        if(iItemRoll >= 1 and iItemRoll <= 6) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        if(iItemRoll >= 5 and iItemRoll <= 17) then
            TA_SetProperty("Mug Item", "Adamantite Flakes x1")
        end
        if(iItemRoll >= 20 and iItemRoll <= 50) then
            TA_SetProperty("Mug Item", "Bent Tools")
        end
        if(iItemRoll >= 90 and iItemRoll <= 94) then
            TA_SetProperty("Mug Item", "Rubose Gem")
        end
        if(iItemRoll >= 95 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Qederite Gem")
        end
    end
    return
end

--Enemy creation.
AC_CreateEnemy("Horrible Little Man")
    
    --[Loot Determination]
    if(iCurrentMinesFloor < 20) then
        ACE_SetProperty("Register Drop",  1,  12, "Adamantite Powder x1")
        ACE_SetProperty("Register Drop", 20,  50, "Bent Tools")
        ACE_SetProperty("Register Drop", 95, 100, "Qederite Gem")
    elseif(iCurrentMinesFloor < 40) then
        ACE_SetProperty("Register Drop",  1,   6, "Adamantite Powder x1")
        ACE_SetProperty("Register Drop",  7,  14, "Adamantite Flakes x1")
        ACE_SetProperty("Register Drop", 20,  50, "Bent Tools")
        ACE_SetProperty("Register Drop", 90,  94, "Rubose Gem")
        ACE_SetProperty("Register Drop", 95, 100, "Qederite Gem")
    else
        ACE_SetProperty("Register Drop",  1,   6, "Adamantite Powder x1")
        ACE_SetProperty("Register Drop",  5,  17, "Adamantite Flakes x1")
        ACE_SetProperty("Register Drop", 20,  50, "Bent Tools")
        ACE_SetProperty("Register Drop", 90,  94, "Rubose Gem")
        ACE_SetProperty("Register Drop", 95, 100, "Qederite Gem")
    end

    --[Enemy Components]
    --AI.
    ACE_SetProperty("AI Override", "DUMB AI")
    
    --Rendering
    ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/Horrible")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/Horrible")

    --Base Statistics
    ACE_SetProperty("Health Max", 160 + iHPBonus)
    ACE_SetProperty("Health", 160 + iHPBonus)
    
    --Loot
    ACE_SetProperty("EXP", 44 + iXPBonus)
    ACE_SetProperty("Platina", 19 + iPlBonus)
    
    --Equipment Variants
    ACE_SetProperty("Damage", 20 + iDmBonus)
    ACE_SetProperty("Speed", 6 + iInBonus)
    ACE_SetProperty("Protection", 20)
    
    --Damage Factors
    ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
    ACE_SetProperty("Resistance", gciFactor_Pierce,    1.50)
    ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
    ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
    ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
    ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
    ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
    ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
    ACE_SetProperty("Resistance", gciFactor_Bleed,     1.50)
    ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
    ACE_SetProperty("Resistance", gciFactor_Poison,    1.50)
    ACE_SetProperty("Resistance", gciFactor_Terrify,   0.10)
    
    --[Common Abilities]
    --Starting action is just an attack.
    ACE_CreateAction("As Action", "Attack")
        ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
        ACA_SetProperty("AI Priority", 70)
        ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
        ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
        ACA_SetProperty("Speed Modifer", 0)
        ACA_SetProperty("Action Never Crits", true)
        fnStandardAttackAnim("SlashCross")
        fnStandardAbilitySounds("SlashCross")
    DL_PopActiveObject()
    
    --Fire attack!
    ACE_CreateAction("As Action", "Flaming Punch")
        ACA_SetProperty("Description", " Flaming attack.")
        ACA_SetProperty("AI Priority", 30)
        ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
        ACA_SetProperty("Damage Amount", gciFactor_Fire, 1.50)
        fnStandardAttackAnim("SlashCross")
        fnStandardAbilitySounds("SlashCross")
    DL_PopActiveObject()
    
    --[Extra Abilities]
    if(iCurrentMinesFloor >= 20) then
        ACE_CreateAction("As Action", "Bile Spit")
            ACA_SetProperty("Description", " Corrosion Attack")
            ACA_SetProperty("AI Priority", 20)
            ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
            ACA_SetProperty("Damage Amount", gciFactor_Slash, 0.30)
            ACA_SetProperty("Damage Amount", gciFactor_Corrode, 0.70)
            fnStandardAttackAnim("SlashCross")
            fnStandardAbilitySounds("SlashCross")
        DL_PopActiveObject()
    end
    
    --Finish timers.
    ACE_SetProperty("Finish Timers")
    
DL_PopActiveObject()
