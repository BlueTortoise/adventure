--[Latex Drone - Mines Edition]
--Maverick Latex Drone, found in the mines.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--[Random Components]
--Floor. Floor is scattered slightly, it's possible to battle an enemy that is 5 floors tougher than expected.
local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N") + LM_GetRandomNumber(-5, 5)
if(iCurrentMinesFloor < 0) then iCurrentMinesFloor = 0 end
if(iCurrentMinesFloor > 35) then iCurrentMinesFloor = 35 end

--Bonus Computation.
local iHPBonus = math.floor(iCurrentMinesFloor * 3.00)
local iXPBonus = math.floor(iCurrentMinesFloor * 1.60)
local iPlBonus = math.floor(iCurrentMinesFloor * 0.65)
local iDmBonus = math.floor(iCurrentMinesFloor * 0.25)
local iInBonus = math.floor(iCurrentMinesFloor * 0.05)

--Mug Handling.
if(TA_GetProperty("Is Mugging Check") == true) then

    --Base.
    TA_SetProperty("Mug Platina", (18 + iPlBonus) * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", (31 + iXPBonus) * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iCurrentMinesFloor < 20) then
        
        if(iItemRoll >= 1 and iItemRoll <= 2) then
            TA_SetProperty("Mug Item", "Yemite Gem")
        end
        if(iItemRoll >= 3 and iItemRoll <= 4) then
            TA_SetProperty("Mug Item", "Rubose Gem")
        end
        if(iItemRoll >= 5 and iItemRoll <= 6) then
            TA_SetProperty("Mug Item", "Qederite Gem")
        end
    elseif(iCurrentMinesFloor < 40) then
        
        if(iItemRoll >= 1 and iItemRoll <= 3) then
            TA_SetProperty("Mug Item", "Yemite Gem")
        end
        if(iItemRoll >= 4 and iItemRoll <= 6) then
            TA_SetProperty("Mug Item", "Rubose Gem")
        end
        if(iItemRoll >= 7 and iItemRoll <= 9) then
            TA_SetProperty("Mug Item", "Qederite Gem")
        end
    else
        
        if(iItemRoll >= 1 and iItemRoll <= 4) then
            TA_SetProperty("Mug Item", "Yemite Gem")
        end
        if(iItemRoll >= 5 and iItemRoll <= 8) then
            TA_SetProperty("Mug Item", "Rubose Gem")
        end
        if(iItemRoll >= 9 and iItemRoll <= 12) then
            TA_SetProperty("Mug Item", "Qederite Gem")
        end
    end
    return
end

--Creation.
AC_CreateEnemy("Latex Drone")
    
    --Loot Determination
    if(iCurrentMinesFloor < 20) then
        ACE_SetProperty("Register Drop",  1,  2, "Yemite Gem")
        ACE_SetProperty("Register Drop",  3,  4, "Rubose Gem")
        ACE_SetProperty("Register Drop",  5,  6, "Qederite Gem")
    elseif(iCurrentMinesFloor < 40) then
        ACE_SetProperty("Register Drop",  1,  3, "Yemite Gem")
        ACE_SetProperty("Register Drop",  4,  6, "Rubose Gem")
        ACE_SetProperty("Register Drop",  7,  9, "Qederite Gem")
    else
        ACE_SetProperty("Register Drop",  1,  4, "Yemite Gem")
        ACE_SetProperty("Register Drop",  5,  8, "Rubose Gem")
        ACE_SetProperty("Register Drop",  9, 12, "Qederite Gem")
    end
    
    --[Common Components]
    --AI.
    ACE_SetProperty("AI Override", "DUMB AI")
    
    --Rendering
    ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/LatexDrone")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/LatexDrone")

    --Base Statistics
    ACE_SetProperty("Health Max", 165 + iHPBonus)
    ACE_SetProperty("Health", 165 + iHPBonus)
    
    --Loot
    ACE_SetProperty("EXP", 31 + iXPBonus)
    ACE_SetProperty("Platina", 18 + iPlBonus)
    
    --Equipment Variants
    ACE_SetProperty("Damage", 37 + iDmBonus)
    ACE_SetProperty("Speed", 5 + iInBonus)
    ACE_SetProperty("Protection", 0)
    
    --Damage Factors
    ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
    ACE_SetProperty("Resistance", gciFactor_Pierce,    1.50)
    ACE_SetProperty("Resistance", gciFactor_Strike,    0.90)
    ACE_SetProperty("Resistance", gciFactor_Fire,      0.80)
    ACE_SetProperty("Resistance", gciFactor_Ice,       0.80)
    ACE_SetProperty("Resistance", gciFactor_Lightning, 0.50)
    ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
    ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
    ACE_SetProperty("Resistance", gciFactor_Bleed,     0.70)
    ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
    ACE_SetProperty("Resistance", gciFactor_Poison,    0.70)
    ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
    
    --Starting action is just an attack.
    ACE_CreateAction("As Action", "Attack")
        ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
        ACA_SetProperty("AI Priority", 70)
        ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
        ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
        ACA_SetProperty("Speed Modifer", 0)
        ACA_SetProperty("Action Never Crits", true)
        fnStandardAttackAnim("SlashCross")
        fnStandardAbilitySounds("SlashCross")
    DL_PopActiveObject()
    
    --Stun attack!
    ACE_CreateAction("As Action", "Stun Attack")
        ACA_SetProperty("Description", " Attack that also stuns.")
        ACA_SetProperty("AI Priority", 30)
        ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
        ACA_SetProperty("Damage Amount", gciFactor_Slash, 2.00)
        ACA_SetProperty("New Effect", gciEffect_Stun, gci_Target_Single_Hostile, 70, 1)
        fnStandardAttackAnim("SlashCross")
        fnStandardAbilitySounds("SlashCross")
    DL_PopActiveObject()
    
    --Finish timers.
    ACE_SetProperty("Finish Timers")
    
DL_PopActiveObject()