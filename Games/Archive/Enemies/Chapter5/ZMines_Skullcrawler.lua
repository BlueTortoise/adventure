--[SkullCrawler - Mines Edition]
--OH HELLO LITTLE FRIEND.

--[Random Components]
--Floor. Floor is scattered slightly, it's possible to battle an enemy that is 5 floors tougher than expected.
local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N") + LM_GetRandomNumber(-5, 5)
if(iCurrentMinesFloor < 0) then iCurrentMinesFloor = 0 end
if(iCurrentMinesFloor > 35) then iCurrentMinesFloor = 35 end

--Bonus Computation.
local iHPBonus = math.floor(iCurrentMinesFloor * 3.00)
local iXPBonus = math.floor(iCurrentMinesFloor * 1.60)
local iPlBonus = math.floor(iCurrentMinesFloor * 0.65)
local iDmBonus = math.floor(iCurrentMinesFloor * 0.25)
local iInBonus = math.floor(iCurrentMinesFloor * 0.05)

--Mug Handling.
if(TA_GetProperty("Is Mugging Check") == true) then

    --Base.
    TA_SetProperty("Mug Platina", (12 + iPlBonus) * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", (19 + iXPBonus) * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iCurrentMinesFloor < 20) then
        
        if(iItemRoll >= 1 and iItemRoll <= 10) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        if(iItemRoll >= 40 and iItemRoll <= 60) then
            TA_SetProperty("Mug Item", "Assorted Parts")
        end
    elseif(iCurrentMinesFloor < 40) then
        
        if(iItemRoll >= 1 and iItemRoll <= 10) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        if(iItemRoll >= 11 and iItemRoll <= 20) then
            TA_SetProperty("Mug Item", "Adamantite Flakes x1")
        end
        if(iItemRoll >= 40 and iItemRoll <= 60) then
            TA_SetProperty("Mug Item", "Assorted Parts")
        end
    else
        
        if(iItemRoll >= 1 and iItemRoll <= 20) then
            TA_SetProperty("Mug Item", "Adamantite Powder x1")
        end
        if(iItemRoll >= 21 and iItemRoll <= 20) then
            TA_SetProperty("Mug Item", "Adamantite Flakes x1")
        end
        if(iItemRoll >= 40 and iItemRoll <= 60) then
            TA_SetProperty("Mug Item", "Assorted Parts")
        end
        if(iItemRoll >= 40 and iItemRoll <= 60) then
            TA_SetProperty("Mug Item", "Bent Tools")
        end
    end
    return
end

--Creation.
AC_CreateEnemy("SkullCrawler")
    
    --Loot Determination
    if(iCurrentMinesFloor < 20) then
        ACE_SetProperty("Register Drop",  1,  10, "Adamantite Powder x1")
        ACE_SetProperty("Register Drop", 40,  60, "Assorted Parts")
    elseif(iCurrentMinesFloor < 40) then
        ACE_SetProperty("Register Drop",  1,  10, "Adamantite Powder x1")
        ACE_SetProperty("Register Drop", 11,  20, "Adamantite Flakes x1")
        ACE_SetProperty("Register Drop", 40,  60, "Assorted Parts")
    else
        ACE_SetProperty("Register Drop",  1,  20, "Adamantite Powder x1")
        ACE_SetProperty("Register Drop", 21,  30, "Adamantite Flakes x1")
        ACE_SetProperty("Register Drop", 40,  60, "Assorted Parts")
        ACE_SetProperty("Register Drop", 40,  60, "Bent Tools")
    end

    --[Common Components]
	--AI.
	ACE_SetProperty("AI Override", "DUMB AI")
	
	--Rendering
	ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/SkullCrawler")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/SkullCrawler")

	--Base Statistics
	ACE_SetProperty("Health Max", 122 + iHPBonus)
	ACE_SetProperty("Health", 122 + iHPBonus)
	
	--Loot
    ACE_SetProperty("EXP", 19 + iXPBonus)
    ACE_SetProperty("Platina", 12 + iPlBonus)
	
	--Equipment Variants
	ACE_SetProperty("Damage", 15 + iDmBonus)
	ACE_SetProperty("Speed", 6 + iInBonus)
	ACE_SetProperty("Protection", 0)
	
	--Resistances
	ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
	ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
	ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
	ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
	ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
	ACE_SetProperty("Resistance", gciFactor_Holy,      0.00)
	ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
	ACE_SetProperty("Resistance", gciFactor_Bleed,     0.00)
	ACE_SetProperty("Resistance", gciFactor_Blind,     0.00)
	ACE_SetProperty("Resistance", gciFactor_Poison,    0.00)
	ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
	
	--Starting action is just an attack.
	ACE_CreateAction("As Action", "Attack")
		ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
		ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
		ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
		ACA_SetProperty("Speed Modifer", 0)
		ACA_SetProperty("Action Never Crits", true)
		fnStandardAttackAnim("SlashClaw")
		fnStandardAbilitySounds("SlashClaw")
	DL_PopActiveObject()
	
	--Finish timers.
	ACE_SetProperty("Finish Timers")
	
DL_PopActiveObject()