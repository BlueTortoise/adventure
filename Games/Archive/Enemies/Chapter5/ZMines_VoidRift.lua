--[Void Rift - Mines Versio]
--Gains stat bonuses as the mines increases in floor count.

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--[Random Components]
--Floor. Floor is scattered slightly, it's possible to battle an enemy that is 5 floors tougher than expected.
local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N") + LM_GetRandomNumber(-5, 5)
if(iCurrentMinesFloor < 0) then iCurrentMinesFloor = 0 end
if(iCurrentMinesFloor > 35) then iCurrentMinesFloor = 35 end

--Bonus Computation.
local iHPBonus = math.floor(iCurrentMinesFloor * 3.00)
local iXPBonus = math.floor(iCurrentMinesFloor * 1.60)
local iPlBonus = math.floor(iCurrentMinesFloor * 0.65)
local iDmBonus = math.floor(iCurrentMinesFloor * 0.25)
local iInBonus = math.floor(iCurrentMinesFloor * 0.05)

--Mug Handling.
if(TA_GetProperty("Is Mugging Check") == true) then

    --Base.
    TA_SetProperty("Mug Platina", (42 + iPlBonus) * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", (20 + iXPBonus) * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iCurrentMinesFloor < 20) then
        
        if(iItemRoll >= 1 and iItemRoll <= 3) then
            TA_SetProperty("Mug Item", "Adamantite Flakes x1")
        end
        if(iItemRoll >= 20 and iItemRoll <= 50) then
            TA_SetProperty("Mug Item", "Recycleable Junk")
        end
        if(iItemRoll >= 95 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Blurleen Gem")
        end
    elseif(iCurrentMinesFloor < 40) then
        
        if(iItemRoll >= 1 and iItemRoll <= 6) then
            TA_SetProperty("Mug Item", "Adamantite Flakes x1")
        end
        if(iItemRoll >= 20 and iItemRoll <= 50) then
            TA_SetProperty("Mug Item", "Recycleable Junk")
        end
        if(iItemRoll >= 90 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Blurleen Gem")
        end
    else
        
        if(iItemRoll >= 1 and iItemRoll <= 12) then
            TA_SetProperty("Mug Item", "Adamantite Flakes x1")
        end
        if(iItemRoll >= 20 and iItemRoll <= 70) then
            TA_SetProperty("Mug Item", "Recycleable Junk")
        end
        if(iItemRoll >= 80 and iItemRoll <= 100) then
            TA_SetProperty("Mug Item", "Blurleen Gem")
        end
    end
    return
end

--Create.
AC_CreateEnemy("Void Rift")
    
    --Loot Determination
    if(iCurrentMinesFloor < 20) then
        ACE_SetProperty("Register Drop",  1,   3, "Adamantite Flakes x1")
        ACE_SetProperty("Register Drop", 20,  50, "Recycleable Junk")
        ACE_SetProperty("Register Drop", 95, 100, "Blurleen Gem")
    elseif(iCurrentMinesFloor < 40) then
        ACE_SetProperty("Register Drop",  1,   6, "Adamantite Flakes x1")
        ACE_SetProperty("Register Drop", 20,  50, "Recycleable Junk")
        ACE_SetProperty("Register Drop", 90, 100, "Blurleen Gem")
    else
        ACE_SetProperty("Register Drop",  0,  12, "Adamantite Flakes x1")
        ACE_SetProperty("Register Drop", 20,  70, "Recycleable Junk")
        ACE_SetProperty("Register Drop", 80, 100, "Blurleen Gem")
    end

    --[Common Components]
    --AI.
    ACE_SetProperty("AI Override", "DUMB AI")
    
    --Rendering
    ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/VoidRift")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/VoidRift")

    --Base Statistics
    ACE_SetProperty("Health Max", 120 + iHPBonus)
    ACE_SetProperty("Health", 120 + iHPBonus)
    
    --Loot
    ACE_SetProperty("EXP", 20 + iXPBonus)
    ACE_SetProperty("Platina", 42 + iPlBonus)
    
    --Equipment Variants
    ACE_SetProperty("Damage", 34 + iDmBonus)
    ACE_SetProperty("Speed", 4 + iInBonus)
    ACE_SetProperty("Protection", 0)
    
    --Damage Factors
    ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
    ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
    ACE_SetProperty("Resistance", gciFactor_Strike,    1.00)
    ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
    ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
    ACE_SetProperty("Resistance", gciFactor_Lightning, 1.00)
    ACE_SetProperty("Resistance", gciFactor_Holy,      2.00)
    ACE_SetProperty("Resistance", gciFactor_Shadow,    2.00)
    ACE_SetProperty("Resistance", gciFactor_Bleed,     0.00)
    ACE_SetProperty("Resistance", gciFactor_Blind,     0.00)
    ACE_SetProperty("Resistance", gciFactor_Poison,    0.00)
    ACE_SetProperty("Resistance", gciFactor_Terrify,   0.00)
    
    --[Standard Abilities]
    --Starting action is just an attack.
    ACE_CreateAction("As Action", "Attack")
        ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
        ACA_SetProperty("AI Priority", 90)
        ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
        ACA_SetProperty("Damage Amount", gciFactor_Strike, 1.00)
        ACA_SetProperty("Speed Modifer", 0)
        ACA_SetProperty("Action Never Crits", true)
        fnStandardAttackAnim("Strike")
        fnStandardAbilitySounds("Strike")
    DL_PopActiveObject()
    
    --[Extra Abilities]
    if(iCurrentMinesFloor >= 25) then
        ACE_CreateAction("As Action", "Shimmer")
            ACA_SetProperty("Description", " Recovers HP.")
            ACA_SetProperty("AI Priority", 20)
            ACA_SetProperty("Targetting", gci_Target_Self)
            ACA_SetProperty("Self Healing", 55)
            fnStandardAttackAnim("Healing")
            fnStandardAbilitySounds("Healing")
        DL_PopActiveObject()
    end
    
    --Finish timers.
    ACE_SetProperty("Finish Timers")
    
DL_PopActiveObject()
    