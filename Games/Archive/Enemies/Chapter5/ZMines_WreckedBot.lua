--[Wrecked Security Bot - Mines Edition]
--Damn mavericks!

--Argument Listing:
-- 0: iToughness - Toughness rating. Default is 0, can go up to 3.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iToughness = tonumber(LM_GetScriptArgument(0))

--[Random Components]
--Floor. Floor is scattered slightly, it's possible to battle an enemy that is 5 floors tougher than expected.
local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N") + LM_GetRandomNumber(-5, 5)
if(iCurrentMinesFloor < 0) then iCurrentMinesFloor = 0 end
if(iCurrentMinesFloor > 35) then iCurrentMinesFloor = 35 end

--Bonus Computation.
local iHPBonus = math.floor(iCurrentMinesFloor * 3.00)
local iXPBonus = math.floor(iCurrentMinesFloor * 1.60)
local iPlBonus = math.floor(iCurrentMinesFloor * 0.65)
local iDmBonus = math.floor(iCurrentMinesFloor * 0.25)
local iInBonus = math.floor(iCurrentMinesFloor * 0.05)

--Mug Handling.
if(TA_GetProperty("Is Mugging Check") == true) then

    --Base.
    TA_SetProperty("Mug Platina", (22 + iPlBonus) * gcfMugPlatinaRate)
    TA_SetProperty("Mug Experience", (18 + iXPBonus) * gcfMugExperienceRate)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    if(iCurrentMinesFloor < 20) then
        
        if(iItemRoll >= 1 and iItemRoll <= 7) then
            TA_SetProperty("Mug Item", "Bent Tools")
        end
        if(iItemRoll >= 6 and iItemRoll <= 12) then
            TA_SetProperty("Mug Item", "Recycleable Junk")
        end
        if(iItemRoll >= 11 and iItemRoll <= 17) then
            TA_SetProperty("Mug Item", "Assorted Parts")
        end
    elseif(iCurrentMinesFloor < 40) then
        
        if(iItemRoll >= 1 and iItemRoll <= 9) then
            TA_SetProperty("Mug Item", "Bent Tools")
        end
        if(iItemRoll >= 6 and iItemRoll <= 14) then
            TA_SetProperty("Mug Item", "Recycleable Junk")
        end
        if(iItemRoll >= 11 and iItemRoll <= 19) then
            TA_SetProperty("Mug Item", "Assorted Parts")
        end
    else
        
        if(iItemRoll >= 1 and iItemRoll <= 15) then
            TA_SetProperty("Mug Item", "Bent Tools")
        end
        if(iItemRoll >= 6 and iItemRoll <= 20) then
            TA_SetProperty("Mug Item", "Recycleable Junk")
        end
        if(iItemRoll >= 11 and iItemRoll <= 25) then
            TA_SetProperty("Mug Item", "Assorted Parts")
        end
    end
    return
end

--Creation function.
AC_CreateEnemy("Damaged Bot")
    
    --Loot Determination
    if(iCurrentMinesFloor < 20) then
        ACE_SetProperty("Register Drop",  1,   7, "Bent Tools")
        ACE_SetProperty("Register Drop",  6,  12, "Recycleable Junk")
        ACE_SetProperty("Register Drop", 11,  17, "Assorted Parts")
    elseif(iCurrentMinesFloor < 40) then
        ACE_SetProperty("Register Drop",  1,   9, "Bent Tools")
        ACE_SetProperty("Register Drop",  6,  14, "Recycleable Junk")
        ACE_SetProperty("Register Drop", 11,  19, "Assorted Parts")
    else
        ACE_SetProperty("Register Drop",  1,  15, "Bent Tools")
        ACE_SetProperty("Register Drop",  6,  20, "Recycleable Junk")
        ACE_SetProperty("Register Drop", 11,  25, "Assorted Parts")
    end

    --[Common Components]
    --AI.
    ACE_SetProperty("AI Override", "DUMB AI")
    
    --Rendering
    ACE_SetProperty("Turn Portrait",  "Root/Images/AdventureUI/TurnPortraits/SecurityWrecked")
    --ACE_SetProperty("Card Portrait",  "Null")
    ACE_SetProperty("Field Portrait", "Root/Images/Portraits/Combat/WreckedBot")

    --Base Statistics
    ACE_SetProperty("Health Max", 180 + iHPBonus)
    ACE_SetProperty("Health", 180 + iHPBonus)
    
    --Loot
    ACE_SetProperty("EXP", 18 + iXPBonus)
    ACE_SetProperty("Platina", 22 + iPlBonus)
    
    --Equipment Variants
    ACE_SetProperty("Damage", 38 + iDmBonus)
    ACE_SetProperty("Speed", 6 + iInBonus)
    ACE_SetProperty("Protection", 0)
    
    --Damage Factors
    ACE_SetProperty("Resistance", gciFactor_Slash,     1.00)
    ACE_SetProperty("Resistance", gciFactor_Pierce,    1.00)
    ACE_SetProperty("Resistance", gciFactor_Strike,    1.50)
    ACE_SetProperty("Resistance", gciFactor_Fire,      1.00)
    ACE_SetProperty("Resistance", gciFactor_Ice,       1.00)
    ACE_SetProperty("Resistance", gciFactor_Lightning, 2.00)
    ACE_SetProperty("Resistance", gciFactor_Holy,      1.00)
    ACE_SetProperty("Resistance", gciFactor_Shadow,    1.00)
    ACE_SetProperty("Resistance", gciFactor_Bleed,     0.25)
    ACE_SetProperty("Resistance", gciFactor_Blind,     1.00)
    ACE_SetProperty("Resistance", gciFactor_Poison,    0.25)
    ACE_SetProperty("Resistance", gciFactor_Terrify,   1.00)
    
    --[Standard Abilities]
    --Starting action is just an attack.
    ACE_CreateAction("As Action", "Attack")
        ACA_SetProperty("Description", "Attack the enemy with your equipped weapon.")
        ACA_SetProperty("AI Priority", 50)
        ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
        ACA_SetProperty("Damage Amount", gciFactor_Slash, 1.00)
        ACA_SetProperty("Speed Modifer", 0)
        ACA_SetProperty("Action Never Crits", true)
        fnStandardAttackAnim("Strike")
        fnStandardAbilitySounds("Strike")
    DL_PopActiveObject()
    
    --[Extra Abilities]
    if(iCurrentMinesFloor >= 17) then
        ACE_CreateAction("As Action", "Lazer Shot")
            ACA_SetProperty("Description", " Kapow!")
            ACA_SetProperty("AI Priority", 50)
            ACA_SetProperty("Targetting", gci_Target_Single_Hostile)
            ACA_SetProperty("Damage Amount", gciFactor_Pierce, 1.00)
            fnStandardAttackAnim("Pierce")
            fnStandardAbilitySounds("Pierce")
        DL_PopActiveObject()
    end
    
    --Finish timers.
    ACE_SetProperty("Finish Timers")
    
DL_PopActiveObject()

