--[609144]
--Boss of the Tellurium Mines, floor 50. Has four modes which change who she is targeting. Changes vulnerabilities in each mode.
if(fnArgCheck(1) == false) then return end

--Get which execution type this is.
local iCode = LM_GetScriptArgument(0, "N")

--Constants
local ciModeElectrical = 0
local ciModeTerrify = 1
local ciModeSlashing = 2
local ciModePiercing = 3
local ciModeClobber = 4
local ciLoMode = ciModeElectrical
local ciHiMode = ciModeClobber

--Initializer. Called at combat startup. Create values here.
if(iCode == gci_ACE_Initialize) then
	ACE_SetValue("iTurnCounter", "N", 0)
	ACE_SetValue("iTurnsLeftInMode", "N", 0)
	ACE_SetValue("iCurrentMode", "N", -1)
	ACE_SetValue("iClobberTurnCount", "N", 0)
	
--Called at the start of every round when the turns are rolled. The turn list is finalized right before this call.
elseif(iCode == gci_ACE_New_Round) then

	--Increment the local round counter.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N")
	ACE_SetValue("iTurnCounter", "N", iTurnCounter + 1)
    
    --Decrement the number of turns left.
    local iTurnsLeftInMode = ACE_GetValue("iTurnsLeftInMode", "N")
    iTurnsLeftInMode = iTurnsLeftInMode - 1
    ACE_SetValue("iTurnsLeftInMode", "N", iTurnsLeftInMode)
    
    --If the turns left in the mode is less than one, change modes here.
    if(iTurnsLeftInMode < 1.0) then
        
        --Roll how many turns this will last.
        local iTurnRoll = LM_GetRandomNumber(2, 4)
        ACE_SetValue("iTurnsLeftInMode", "N", iTurnRoll)
        
        --Store the previous mode. Boss always has to switch modes.
        local iPreviousMode = ACE_GetValue("iCurrentMode", "N")
        
        --Roll a mode. Repeat until a valid mode is found.
        local iCurrentMode = -1
        local bIsValidMode = false
        while(bIsValidMode == false) do
        
            --Roll.
            bIsValidMode = true
            iCurrentMode = LM_GetRandomNumber(ciLoMode, ciHiMode)
            ACE_SetValue("iCurrentMode", "N", iCurrentMode)
        
            --If the new mode is the same as the previous mode, we fail.
            if(iCurrentMode == iPreviousMode) then bIsValidMode = false end
            
            --If the new mode is Electrical and the old is Piercing, or vice versa, fail.
            if(iCurrentMode == ciModePiercing   and iPreviousMode == ciModeElectrical) then bIsValidMode = false end
            if(iCurrentMode == ciModeElectrical and iPreviousMode == ciModePiercing)   then bIsValidMode = false end
        
            --Insert other checks here.
        end
        
        --Reset all vulnerabilities back to neutral.
        ACE_SetProperty("Resistance", gciFactor_Slash,     0.30)
        ACE_SetProperty("Resistance", gciFactor_Pierce,    0.30)
        ACE_SetProperty("Resistance", gciFactor_Strike,    0.30)
        ACE_SetProperty("Resistance", gciFactor_Fire,      0.30)
        ACE_SetProperty("Resistance", gciFactor_Ice,       0.30)
        ACE_SetProperty("Resistance", gciFactor_Lightning, 0.30)
        ACE_SetProperty("Resistance", gciFactor_Holy,      0.30)
        ACE_SetProperty("Resistance", gciFactor_Shadow,    0.30)
        ACE_SetProperty("Resistance", gciFactor_Bleed,     0.30)
        ACE_SetProperty("Resistance", gciFactor_Blind,     0.30)
        ACE_SetProperty("Resistance", gciFactor_Poison,    0.30)
        ACE_SetProperty("Resistance", gciFactor_Corrode,   0.30)
        ACE_SetProperty("Resistance", gciFactor_Terrify,   0.30)
        
        --Dialogue displays differently based on the mode.
        if(iCurrentMode == ciModeElectrical) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "609144 screams 'I'll add your parts to mine, Doll!' and exposes her wiring!") ]])
            fnCutsceneBlocker()
            ACE_SetProperty("Resistance", gciFactor_Lightning, 2.50)
        
        elseif(iCurrentMode == ciModeTerrify) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "609144 begins gibbering to herself![SOFTBLOCK] She's cowering in fear of something you can't see!") ]])
            fnCutsceneBlocker()
            ACE_SetProperty("Resistance", gciFactor_Terrify, 3.50)
        
        elseif(iCurrentMode == ciModeSlashing) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "609144 seems to be fairly lucid, and adopts a combative pose!") ]])
            fnCutsceneBlocker()
            ACE_SetProperty("Resistance", gciFactor_Slash, 1.50)
        
        elseif(iCurrentMode == ciModePiercing) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "609144 pulls her latex parts tightly around her chassis!") ]])
            fnCutsceneBlocker()
            ACE_SetProperty("Resistance", gciFactor_Pierce, 2.50)
        
        --Clobber. Handled differently, first turn is the hit and then the next two are cool-off.
        elseif(iCurrentMode == ciModeClobber) then
            ACE_SetValue("iTurnsLeftInMode", "N", 3)
            local cfVulnerability = 2.50
            ACE_SetProperty("Resistance", gciFactor_Slash,     cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Pierce,    cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Strike,    cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Fire,      cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Ice,       cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Lightning, cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Holy,      cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Shadow,    cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Bleed,     cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Blind,     cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Poison,    cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Corrode,   cfVulnerability)
            ACE_SetProperty("Resistance", gciFactor_Terrify,   cfVulnerability)
            ACE_SetValue("iClobberTurnCount", "N", 0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "609144 glares at you.[SOFTBLOCK] Her eyes grow distant...") ]])
            fnCutsceneBlocker()
        
        end
    end

--Called whenever an entity other than this one begins their turn.
elseif(iCode == gci_ACE_Other_ACE_Begins_Turn) then

--Called whenever this entity begins their turn. They should resolve an action during this pulse!
elseif(iCode == gci_ACE_This_ACE_Begins_Turn) then

	--Turn counter.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N") - 1

    --Get which mode the boss is in.
    local iCurrentMode = ACE_GetValue("iCurrentMode", "N")

    --In "Electrical" mode, she is vulnerable to Christine's electrical attacks and focus-fires 55.
    if(iCurrentMode == ciModeElectrical) then
	
        --Set the attack.
        ACE_SetProperty("Use Ability", "Flail")
        
        --Get party properties to see if 55 is still standing:
        AC_PushPartyMember("55")
            local i55HP = ACE_GetProperty("Health")
        DL_PopActiveObject()

        --If 55 is standing, target her. Otherwise, Christine.
        if(i55HP > 0) then
            ACE_SetProperty("Use Target", "55")
        else
            ACE_SetProperty("Use Target", "Christine")
        end
        
    --In "Terrify" mode, she is vulnerable to 55's Disruptor Blast and focus-fires Christine.
    elseif(iCurrentMode == ciModeTerrify) then
    
        --Roll. There is a 20% chance she will pass her turn and ramble to herself.
        local iPassRoll = LM_GetRandomNumber(0, 99)
        if(iPassRoll < 20) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "609144 seems to be rambling about something...") ]])
            fnCutsceneBlocker()
            ACE_SetProperty("Use Ability", "Pass")
            
        --Attack normally.
        else
	
            --Set the attack.
            ACE_SetProperty("Use Ability", "Claw")
            
            --Get party properties to see if Christine is still standing:
            AC_PushPartyMember("Christine")
                local iChristineHP = ACE_GetProperty("Health")
            DL_PopActiveObject()

            --If Christine is still standing, target her. Otherwise, 55.
            if(iChristineHP > 0) then
                ACE_SetProperty("Use Target", "Christine")
            else
                ACE_SetProperty("Use Target", "55")
            end
        end
    
    --In "Slashing" mode, she attacks both party members at the same time for less damage and is vulnerable to normal attacks.
    elseif(iCurrentMode == ciModeSlashing) then
        ACE_SetProperty("Use Ability", "Sweep")
    
    --In "Piercing" mode, she is vulnerable to Christine's piercing attacks and focus-fires 55.
    elseif(iCurrentMode == ciModePiercing) then
	
        --Set the attack.
        ACE_SetProperty("Use Ability", "Attack")
        
        --Get party properties to see if 55 is still standing:
        AC_PushPartyMember("55")
            local i55HP = ACE_GetProperty("Health")
        DL_PopActiveObject()

        --If 55 is standing, target her. Otherwise, Christine.
        if(i55HP > 0) then
            ACE_SetProperty("Use Target", "55")
        else
            ACE_SetProperty("Use Target", "Christine")
        end
    
    --In "Clobbering" mode, the first turn involves a devastating hit to the entire party. Then, two turns of passing.
    elseif(iCurrentMode == ciModeClobber) then
        local iClobberTurnCount = ACE_GetValue("iClobberTurnCount", "N")
        
        --First turn. Clobber!
        if(iClobberTurnCount == 0.0) then
            ACE_SetProperty("Use Ability", "Devastate")
            ACE_SetValue("iClobberTurnCount", "N", 1.0)
        
        --Second turn. Recouperate.
        elseif(iClobberTurnCount == 1.0) then 
            ACE_SetValue("iClobberTurnCount", "N", 2.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "609144 crumples to the ground...") ]])
            fnCutsceneBlocker()
            ACE_SetProperty("Use Ability", "Pass")
        
        --Third turn.
        elseif(iClobberTurnCount == 2.0) then 
            ACE_SetValue("iTurnsLeftInMode", "N", 0)
            ACE_SetValue("iClobberTurnCount", "N", 0.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "609144 will recover soon...") ]])
            fnCutsceneBlocker()
            ACE_SetProperty("Use Ability", "Pass")
        end
    end

--Called when this entity is killed.
elseif(iCode == gci_ACE_This_ACE_Dies_Turn) then

--Called right before combat ends. Experience/loot are calculated afterwards.
elseif(iCode == gci_ACE_Combat_Ends) then

--Erroneous case:
else
	Debug_ForcePrint("Error, AI script received unhandled code: " .. iCode .. "\n")

end
