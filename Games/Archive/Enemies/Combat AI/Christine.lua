--[Christine]
--Finale of the Gala. Uses some of her normal abilities, 20% chance to just pass a turn.
if(fnArgCheck(1) == false) then return end

--Get which execution type this is.
local iCode = LM_GetScriptArgument(0, "N")

--Initializer. Called at combat startup. Create values here.
if(iCode == gci_ACE_Initialize) then
	ACE_SetValue("iTurnCounter", "N", 0)
	
--Called at the start of every round when the turns are rolled. The turn list is finalized right before this call.
elseif(iCode == gci_ACE_New_Round) then

	--Increment the local round counter.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N")
	ACE_SetValue("iTurnCounter", "N", iTurnCounter + 1)

--Called whenever an entity other than this one begins their turn.
elseif(iCode == gci_ACE_Other_ACE_Begins_Turn) then

--Called whenever this entity begins their turn. They should resolve an action during this pulse!
elseif(iCode == gci_ACE_This_ACE_Begins_Turn) then

	--Turn counter.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N") - 1
	
	--20% chance, pass a turn.
    local iRoll = LM_GetRandomNumber(1, 100)
	if(iRoll <= 20) then
        
        --Common.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
        
        --Rolls what Christine mutters.
        local iTalkRoll = LM_GetRandomNumber(1, 14)
        if(iTalkRoll == 1) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: We feel no pain, we feel no pain, we feel no pain...") ]])
        elseif(iTalkRoll == 2) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:It's blind, so I'll pluck its eyes out...") ]])
        elseif(iTalkRoll == 3) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Is it okay if I take your brain..?") ]])
        elseif(iTalkRoll == 4) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: It blots out the stars...") ]])
        elseif(iTalkRoll == 5) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: I'm so happy...") ]])
        elseif(iTalkRoll == 6) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: 55 was a good friend.[SOFTBLOCK] I'm sad she died...") ]])
        elseif(iTalkRoll == 7) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Just a stump.[SOFTBLOCK] It bleeds ice...") ]])
        elseif(iTalkRoll == 8) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: I'll be that, and it will be me...") ]])
        elseif(iTalkRoll == 9) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Always?[SOFTBLOCK] Never?[SOFTBLOCK] Before the middle?") ]])
        elseif(iTalkRoll == 10) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: Awaken...[SOFTBLOCK] Awaken...") ]])
        elseif(iTalkRoll == 11) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: ...[SOFTBLOCK] But it's mine now.[SOFTBLOCK] Take it from me...[SOFTBLOCK] if you can...") ]])
        elseif(iTalkRoll == 12) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: I melted her and melted her and melted her...") ]])
        elseif(iTalkRoll == 13) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: I can feel it, crawling...[SOFTBLOCK] scratching at the back of my eyes...") ]])
        elseif(iTalkRoll == 14) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine: I want to let them come out...") ]])
        end
        
        --Common.
            fnCutsceneBlocker()
		ACE_SetProperty("Use Ability", "Pass")
	
	--Otherwise, us the normal attack rolls. Batter, Sweep, and Rally are 10% chances each.
	else
	
		--Roll.
		local iAttackRoll = LM_GetRandomNumber(0, 99)
	
		--65% chance to use the standard attack on a random target.
		if(iAttackRoll < 70) then
			ACE_SetProperty("Use Ability", "Attack")
	
		--15% chance to use Roar. Fails if she used it recently, using attack instead.
		elseif(iAttackRoll < 80) then
			ACE_SetProperty("Use Ability", "Batter")
		elseif(iAttackRoll < 90) then
			ACE_SetProperty("Use Ability", "Sweep")
		else
			ACE_SetProperty("Use Ability", "Rally")
		end
	end

--Called when this entity is killed.
elseif(iCode == gci_ACE_This_ACE_Dies_Turn) then

--Called right before combat ends. Experience/loot are calculated afterwards.
elseif(iCode == gci_ACE_Combat_Ends) then

--Erroneous case:
else
	Debug_ForcePrint("Error, AI script received unhandled code: " .. iCode .. "\n")

end
