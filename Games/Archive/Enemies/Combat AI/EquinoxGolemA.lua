--[Equinox Golem A]
--First 'Boss' of Equinox. 25% chance of cowering, and can't do this twice in a row.
if(fnArgCheck(1) == false) then return end

--Get which execution type this is.
local iCode = LM_GetScriptArgument(0, "N")

--Initializer. Called at combat startup. Create values here.
if(iCode == gci_ACE_Initialize) then
	ACE_SetValue("iCoweredLastTurn", "N", 0)
	
--Called at the start of every round when the turns are rolled. The turn list is finalized right before this call.
elseif(iCode == gci_ACE_New_Round) then

--Called whenever an entity other than this one begins their turn.
elseif(iCode == gci_ACE_Other_ACE_Begins_Turn) then

--Called whenever this entity begins their turn. They should resolve an action during this pulse!
elseif(iCode == gci_ACE_This_ACE_Begins_Turn) then

	--Variables.
	local bCowerThisTurn = false
	local iCoweredLastTurn = ACE_GetValue("iCoweredLastTurn", "N")
	
	--Zero the cower counter.
	if(iCoweredLastTurn == 1) then
		ACE_SetValue("iCoweredLastTurn", "N", 0)
	
	--Roll against cowering.
	else
	
		--Roll.
		local iRoll = LM_GetRandomNumber(0, 99)
		if(iRoll < 25) then
			bCowerThisTurn = true
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The golem seems to be cowering in fear!") ]])
			fnCutsceneBlocker()
			ACE_SetValue("iCoweredLastTurn", "N", 1)
		end
	end
	
	--Pass if cowering this turn.
	if(bCowerThisTurn) then
		ACE_SetProperty("Use Ability", "Pass")
	
	--Otherwise, roll for attacks.
	else
	
		--Roll.
		local iAttackRoll = LM_GetRandomNumber(0, 99)
	
		--75% chance to use the standard attack on a random target.
		if(iAttackRoll < 75) then
			ACE_SetProperty("Use Ability", "Attack")
	
		--Remaining is Shock.
		else
			ACE_SetProperty("Use Ability", "Shock")
		end
	end

--Called when this entity is killed.
elseif(iCode == gci_ACE_This_ACE_Dies_Turn) then

--Called right before combat ends. Experience/loot are calculated afterwards.
elseif(iCode == gci_ACE_Combat_Ends) then

--Erroneous case:
else
	Debug_ForcePrint("Error, AI script received unhandled code: " .. iCode .. "\n")

end
