--[Equinox Golem B]
--Boss of Equinox. Uses piercing attacks. Can also stun a party member, but passes a turn after doing that.
-- Needless to say, untrained slave units are not as dangerous as you might think.
if(fnArgCheck(1) == false) then return end

--Get which execution type this is.
local iCode = LM_GetScriptArgument(0, "N")

--Initializer. Called at combat startup. Create values here.
if(iCode == gci_ACE_Initialize) then
	ACE_SetValue("iStunnedLastTurn", "N", 0)
	
--Called at the start of every round when the turns are rolled. The turn list is finalized right before this call.
elseif(iCode == gci_ACE_New_Round) then

--Called whenever an entity other than this one begins their turn.
elseif(iCode == gci_ACE_Other_ACE_Begins_Turn) then

--Called whenever this entity begins their turn. They should resolve an action during this pulse!
elseif(iCode == gci_ACE_This_ACE_Begins_Turn) then

	--Variables.
	local iStunnedLastTurn = ACE_GetValue("iStunnedLastTurn", "N")
	
	--Zero the cower counter.
	if(iStunnedLastTurn == 1) then
		ACE_SetValue("iStunnedLastTurn", "N", 0)
		
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The golem over-exerted and is resting its motivators...") ]])
		fnCutsceneBlocker()
		
		--Pass.
		ACE_SetProperty("Use Ability", "Pass")
	
	--Roll attacks normally.
	else
	
		--Roll.
		local iAttackRoll = LM_GetRandomNumber(0, 99)
	
		--65% chance to use the standard attack on a random target.
		if(iAttackRoll < 65) then
			ACE_SetProperty("Use Ability", "Clumsy Stab")
	
		--Knockout punch, stuns a target.
		else
			ACE_SetProperty("Use Ability", "Knockout Punch")
		
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "The golem has thrown all its weight behind its attack!") ]])
			fnCutsceneBlocker()
			ACE_SetValue("iStunnedLastTurn", "N", 1)
		end
	end

--Called when this entity is killed.
elseif(iCode == gci_ACE_This_ACE_Dies_Turn) then

--Called right before combat ends. Experience/loot are calculated afterwards.
elseif(iCode == gci_ACE_Combat_Ends) then

--Erroneous case:
else
	Debug_ForcePrint("Error, AI script received unhandled code: " .. iCode .. "\n")

end
