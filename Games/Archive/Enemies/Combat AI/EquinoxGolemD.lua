--[Equinox Golem D]
--Boss of Equinox, uses a lot of Corrosive attacks. Stops every 4th turn to refill her chemical supply.
if(fnArgCheck(1) == false) then return end

--Get which execution type this is.
local iCode = LM_GetScriptArgument(0, "N")

--Initializer. Called at combat startup. Create values here.
if(iCode == gci_ACE_Initialize) then
	ACE_SetValue("iTurnCounter", "N", 0)
	
--Called at the start of every round when the turns are rolled. The turn list is finalized right before this call.
elseif(iCode == gci_ACE_New_Round) then

--Called whenever an entity other than this one begins their turn.
elseif(iCode == gci_ACE_Other_ACE_Begins_Turn) then

--Called whenever this entity begins their turn. They should resolve an action during this pulse!
elseif(iCode == gci_ACE_This_ACE_Begins_Turn) then

	--Variables.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N")
	
	--Increment the turn counter.
	ACE_SetValue("iTurnCounter", "N", iTurnCounter + 1)
	
	--On the 4th turn, the golem will run to refill her chemical supply.
	if(iTurnCounter == 4-1) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The golem refills its chemical supply!") ]])
		fnCutsceneBlocker()
		ACE_SetProperty("Use Ability", "Pass")
		
	--On the 5th turn, the golem restores 50 HP and resets the combat timer.
	elseif(iTurnCounter == 5-1) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The golem has sprayed a nanite solution to repair itself!") ]])
		fnCutsceneBlocker()
		ACE_SetProperty("Use Ability", "Mend")
		ACE_SetValue("iTurnCounter", "N", 0)
	
	--All other turns: Attack.
	else
		--Roll.
		local iRoll = LM_GetRandomNumber(0, 99)
		
		--75% chance of using Corrosive Strike.
		if(iRoll < 75) then
			ACE_SetProperty("Use Ability", "Corrosive Strike")
	
		--Acid Splash.
		else
			ACE_SetProperty("Use Ability", "Acid Splash")
		end
	end

--Called when this entity is killed.
elseif(iCode == gci_ACE_This_ACE_Dies_Turn) then

--Called right before combat ends. Experience/loot are calculated afterwards.
elseif(iCode == gci_ACE_Combat_Ends) then

--Erroneous case:
else
	Debug_ForcePrint("Error, AI script received unhandled code: " .. iCode .. "\n")

end
