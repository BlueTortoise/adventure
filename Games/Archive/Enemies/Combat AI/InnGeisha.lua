--[Innsmouth geisha]
--Normal enemy. Uses its normal attacks unless a party member is below 30% HP, in which case has a 50% chance to heal all enemies.
-- Cannot do this more than once every 2 turns.
if(fnArgCheck(1) == false) then return end

--Get which execution type this is.
local iCode = LM_GetScriptArgument(0, "N")

--Initializer. Called at combat startup. Create values here.
if(iCode == gci_ACE_Initialize) then
	ACE_SetValue("iHealCooldown", "N", 0)
	
--Called at the start of every round when the turns are rolled. The turn list is finalized right before this call.
elseif(iCode == gci_ACE_New_Round) then

	--Decrement the heal cooldown.
	local iHealCooldown = ACE_GetValue("iHealCooldown", "N")
	ACE_SetValue("iHealCooldown", "N", iHealCooldown - 1)

--Called whenever an entity other than this one begins their turn.
elseif(iCode == gci_ACE_Other_ACE_Begins_Turn) then

--Called whenever this entity begins their turn. They should resolve an action during this pulse!
elseif(iCode == gci_ACE_This_ACE_Begins_Turn) then

	--Turn counter.
	local iHealCooldown = ACE_GetValue("iHealCooldown", "N") - 1
	
    --Variables.
    local iHealth = ACE_GetProperty("Health")
    local iHealthMax = ACE_GetProperty("Health Max")
    local fPercent = iHealth / iHealthMax
    
    --Attack roll.
    local iRoll = LM_GetRandomNumber(1, 100)
    
	--Check HP. If it's below 30%, has a chance to heal.
	if(fPercent <= 0.30 and iHealCooldown < 1) then
        
        --Rolled a heal.
        if(iRoll <= 50) then
            ACE_SetProperty("Use Ability", "Restore")
            ACE_SetValue("iHealCooldown", "N", 2)
        
        --Rolled a normal attack.
        elseif(iRoll <= 75) then
            ACE_SetProperty("Use Ability", "Attack")
        
        --Hard attack.
        else
            ACE_SetProperty("Use Ability", "Pound")
        end
    
	--Otherwise, Attack.
    else
        
        --Rolled a normal attack.
        if(iRoll <= 66) then
            ACE_SetProperty("Use Ability", "Attack")
        
        --Hard attack.
        else
            ACE_SetProperty("Use Ability", "Pound")
        end
    
	end

--Called when this entity is killed.
elseif(iCode == gci_ACE_This_ACE_Dies_Turn) then

--Called right before combat ends. Experience/loot are calculated afterwards.
elseif(iCode == gci_ACE_Combat_Ends) then

--Erroneous case:
else
	Debug_ForcePrint("Error, AI script received unhandled code: " .. iCode .. "\n")

end
