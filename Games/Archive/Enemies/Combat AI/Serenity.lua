--[Serenity]
--Boss of Serenity Crater. Attacks the entire party for low damage, heals itself and drops protection every 5 turns.
if(fnArgCheck(1) == false) then return end

--Get which execution type this is.
local iCode = LM_GetScriptArgument(0, "N")

--Initializer. Called at combat startup. Create values here.
if(iCode == gci_ACE_Initialize) then
	ACE_SetValue("iTurnCounter", "N", 0)
	
--Called at the start of every round when the turns are rolled. The turn list is finalized right before this call.
elseif(iCode == gci_ACE_New_Round) then

	--Increment the local round counter.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N")
	ACE_SetValue("iTurnCounter", "N", iTurnCounter + 1)

--Called whenever an entity other than this one begins their turn.
elseif(iCode == gci_ACE_Other_ACE_Begins_Turn) then

--Called whenever this entity begins their turn. They should resolve an action during this pulse!
elseif(iCode == gci_ACE_This_ACE_Begins_Turn) then

	--Turn counter.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N") - 1
	
	--On turn 5, use the healing ability.
	if(iTurnCounter == 4) then
		ACE_SetProperty("Use Ability", "Reconstitute")
		ACE_SetValue("iTurnCounter", "N", 0)
	
	--Otherwise, Attack.
	else
        ACE_SetProperty("Use Ability", "Attack")
	end

--Called when this entity is killed.
elseif(iCode == gci_ACE_This_ACE_Dies_Turn) then

--Called right before combat ends. Experience/loot are calculated afterwards.
elseif(iCode == gci_ACE_Combat_Ends) then

--Erroneous case:
else
	Debug_ForcePrint("Error, AI script received unhandled code: " .. iCode .. "\n")

end
