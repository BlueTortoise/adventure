--[Vivify]
--Boss of the LRT facility. Hits Christine for 200% damage every 5th turn, otherwise uses acid strikes and normal attacks periodically.
if(fnArgCheck(1) == false) then return end

--Get which execution type this is.
local iCode = LM_GetScriptArgument(0, "N")

--Initializer. Called at combat startup. Create values here.
if(iCode == gci_ACE_Initialize) then
	ACE_SetValue("iTurnCounter", "N", 0)
	ACE_SetValue("iLastUsedRoar", "N", 0)
	
--Called at the start of every round when the turns are rolled. The turn list is finalized right before this call.
elseif(iCode == gci_ACE_New_Round) then

	--Increment the local round counter.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N")
	ACE_SetValue("iTurnCounter", "N", iTurnCounter + 1)
	
	--Warning based on the turn counter. Vivify skips her 4th turn.
	if(iTurnCounter == 4) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The creature prepares a mighty blow!") ]])
		fnCutsceneBlocker()
	end

--Called whenever an entity other than this one begins their turn.
elseif(iCode == gci_ACE_Other_ACE_Begins_Turn) then

--Called whenever this entity begins their turn. They should resolve an action during this pulse!
elseif(iCode == gci_ACE_This_ACE_Begins_Turn) then

	--Turn counter.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N") - 1
	
	--Decrement the Roar counter.
	local iLastUsedRoar = ACE_GetValue("iLastUsedRoar", "N")
	if(iLastUsedRoar > 0) then ACE_SetValue("iLastUsedRoar", "N", iLastUsedRoar - 1) end
	
	--On turn 4, always pass:
	if(iTurnCounter == 4) then
		ACE_SetProperty("Use Ability", "Pass")
	
	--On turn 5, 200% damage strike to Christine. If Christine is down, hit 55.
	elseif(iTurnCounter == 5) then
	
		--Reset the turn counter.
		ACE_SetValue("iTurnCounter", "N", 0)
	
		--Set the attack.
		ACE_SetProperty("Use Ability", "Tentacle Crush")
		
		--Get party properties to see if Christine is still standing:
		AC_PushPartyMember("Christine")
			local iChristineHP = ACE_GetProperty("Health")
		DL_PopActiveObject()

		--If Christine is still standing, target her:
		if(iChristineHP > 0) then
			ACE_SetProperty("Use Target", "Christine")
		
		--Otherwise, target 55.
		else
			ACE_SetProperty("Use Target", "55")
		end
	
	--On all other turns, we use the other attacks with a weighted roll. Vivify does not use Roar more
	-- than once every 5 turns.
	else
	
		--Roll.
		local iAttackRoll = LM_GetRandomNumber(0, 99)
	
		--65% chance to use the standard attack on a random target.
		if(iAttackRoll < 45) then
			ACE_SetProperty("Use Ability", "Attack")
	
		--15% chance to use Roar. Fails if she used it recently, using attack instead.
		elseif(iAttackRoll < 60) then
		
			--Used it recently, so fail.
			if(iLastUsedRoar-1 > 0) then
				ACE_SetProperty("Use Ability", "Attack")
				
			--Rawr!
			else
				ACE_SetProperty("Use Ability", "Roar")
				ACE_SetValue("iLastUsedRoar", "N", 5)
			end
	
		--Remaining is Corrosive Spit.
		else
			ACE_SetProperty("Use Ability", "Corrosive Spit")
		end
	end

--Called when this entity is killed.
elseif(iCode == gci_ACE_This_ACE_Dies_Turn) then

--Called right before combat ends. Experience/loot are calculated afterwards.
elseif(iCode == gci_ACE_Combat_Ends) then

--Erroneous case:
else
	Debug_ForcePrint("Error, AI script received unhandled code: " .. iCode .. "\n")

end
