--[Warden]
--Boss of the Quantir Mansion. Strikes every 3rd turn for 1000 (!) damage, always hits Mei if she is up. Otherwise,
-- hits Florentina.
if(fnArgCheck(1) == false) then return end

--Get which execution type this is.
local iCode = LM_GetScriptArgument(0, "N")

--Initializer. Called at combat startup. Create values here.
if(iCode == gci_ACE_Initialize) then
	ACE_SetValue("iTurnCounter", "N", 0)
	
--Called at the start of every round when the turns are rolled. The turn list is finalized right before this call.
elseif(iCode == gci_ACE_New_Round) then

	--Increment the local round counter.
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N")
	ACE_SetValue("iTurnCounter", "N", iTurnCounter + 1)
	
	--Warning based on the turn counter.
	if(iTurnCounter == 0) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The Warden tests its body...") ]])
		fnCutsceneBlocker()
		
	elseif(iTurnCounter == 1) then
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The Warden is gathering power...") ]])
		fnCutsceneBlocker()
	
	else
		fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "The Warden is ready to strike!") ]])
		fnCutsceneBlocker()
	
	end

--Called whenever an entity other than this one begins their turn.
elseif(iCode == gci_ACE_Other_ACE_Begins_Turn) then

--Called whenever this entity begins their turn. They should resolve an action during this pulse!
elseif(iCode == gci_ACE_This_ACE_Begins_Turn) then
	local iTurnCounter = ACE_GetValue("iTurnCounter", "N") - 1
	
	--On turn 0:
	if(iTurnCounter == 0) then
		ACE_SetProperty("Use Ability", "Pass")
		
	--On turn 1:
	elseif(iTurnCounter == 1) then
		ACE_SetProperty("Use Ability", "Pass")
	
	--On turn 2, attack!
	else
	
		--Reset the turn counter.
		ACE_SetValue("iTurnCounter", "N", 0)
	
		--Set the attack.
		ACE_SetProperty("Use Ability", "Attack")
		
		--Get party properties to see if Mei is still standing:
		AC_PushPartyMember("Mei")
			local iMeiHP = ACE_GetProperty("Health")
		DL_PopActiveObject()

		--If Mei is still standing, target her:
		if(iMeiHP > 0) then
			ACE_SetProperty("Use Target", "Mei")
		
		--Otherwise, target Florentina.
		else
			ACE_SetProperty("Use Target", "Florentina")
		end
	end

--Called when this entity is killed.
elseif(iCode == gci_ACE_This_ACE_Dies_Turn) then

--Called right before combat ends. Experience/loot are calculated afterwards.
elseif(iCode == gci_ACE_Combat_Ends) then

--Erroneous case:
else
	Debug_ForcePrint("Error, AI script received unhandled code: " .. iCode .. "\n")

end
