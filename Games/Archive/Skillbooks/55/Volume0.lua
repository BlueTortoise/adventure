--[Covert Ops Compendium: Volume 0]
--Algorithms!
local sCharacter = "55"
local sAbility = "Assault Algorithm"
local iRequiredLevel = -1
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 0.\"[SOFTBLOCK] The diagrams are very detailed, but the book isn't all that relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Algorithms are how a combat unit adapts to the moment of battle.[SOFTBLOCK] They come in three varieties::[SOFTBLOCK] Assault, Support, and Recovery.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"The Assault Algorithm is active by default in combat, and provides access to most offensive abilities but curtails most support abilities.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"The Support Algorithm reduces attack power, but provides increased survivability and access to support abilities and some recovery abilities.[SOFTBLOCK] It also retains access to the simpler offensive skills.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"The Recovery Algorithm reduces attack power and threat, but allows a unit to use the full measure of recovery and curative abilities, at the expense of very few offensive options.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Algorithms may be changed with a 1-turn cooldown, and changing algorithms resets all outstanding cooldowns.\")") ]])
			fnCutsceneBlocker()
		
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 0.\"[SOFTBLOCK] A nice refresher, but 55 already knows how to use her Algorithm abilities.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 0.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 0.\"[SOFTBLOCK] This is surprisingly informative![SOFTBLOCK] 55 learned the Assault, Support, and Repair Algorithms, which unlock various buffs and abilities!)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"55\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end