--[Covert Ops Compendium: Volume 2]
--Basic Shots.
local sCharacter = "55"
local sAbility = "Wide Lens Shot"
local iRequiredLevel = -1
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 2.\"[SOFTBLOCK] The diagrams are very detailed, but the book isn't all that relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Pulse Diffractors come with adjustable lenses to produce concentrated or diffused pulse fire on demand.[SOFTBLOCK] It is a simple matter to change the lens' focus before a shot.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Nicknamed the Wide Lens Shot, this attack does less damage than normal, but will strike all targets for slashing damage.[SOFTBLOCK] It is thus more effective when more enemies are on the field.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"For support-oriented units, Tactical Shift is an analytical ability which will identify enemy weak points and strategies.[SOFTBLOCK] The team will deal increased damage and have increased protection for 3 turns.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Lastly, the hallmark of a repair unit is their famous skill, Repair.[SOFTBLOCK] While it simply restores an ally's health, sometimes the simplest skill is the most valuable.\")") ]])
			fnCutsceneBlocker()
		
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 2.\"[SOFTBLOCK] A nice refresher, but 55 already knows how to use Wide Lens Shot, Tactical Shift, and Repair.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 2.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 2.\"[SOFTBLOCK] This is surprisingly informative![SOFTBLOCK] 55 learned the Wide Lens Shot ability, which hits all targets!)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"55\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end