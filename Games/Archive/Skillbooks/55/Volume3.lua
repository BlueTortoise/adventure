--[Covert Ops Compendium: Volume 3]
--Spotless Protocol. Never be clean, NEVER BE CLEAN.
local sCharacter = "55"
local sAbility = "Spotless Protocol"
local iRequiredLevel = -1
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 3.\"[SOFTBLOCK] The diagrams are very detailed, but the book isn't all that relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Weapon cleanliness is an important part of maintaining smooth combat operations.[SOFTBLOCK] A dirty weapon may jam or misfire, while a sullied lens cannot focus pulse fire.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"The same is true for the unit and her allies.[SOFTBLOCK] When afflicted by bleed, blind, or corrosion, taking the extra time to clean it off can preserve unit functionality.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Better yet, Spotless Protocol affects the entire party at once.[SOFTBLOCK] While it does require Support or Repair algorithms to be active, it leaves your allies able to fight in your stead.\")") ]])
			fnCutsceneBlocker()
		
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 3.\"[SOFTBLOCK] A nice refresher, but 55 already knows how to use Spotless Protocol.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 3.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 3.\"[SOFTBLOCK] This is surprisingly informative![SOFTBLOCK] 55 learned Spotless Protocol, which clears Bleed, Blind, and Corrosion.")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"55\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end