--[Covert Ops Compendium: Volume 5]
--Take Cover. Amazingly, this is one of those things 55 thinks is beneath her.
local sCharacter = "55"
local sAbility = "Take Cover"
local iRequiredLevel = -1
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 6.\"[SOFTBLOCK] The diagrams are very detailed, but the book isn't all that relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Hiding behind things is the new getting shot![SOFTBLOCK] Taking cover is back in vogue!\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Simply find a nearby rock or wall, position yourself behind it, and presto![SOFTBLOCK] You're less killable than before!\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"When lacking in adequate cover, a good unit will use durable teammates or even the ground.[SOFTBLOCK] The possibilities are endless!\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Please note that the Covert Ops Compendium does not recommend hiding behind explosive crates or fuel barrels.[SOFTBLOCK] All rights reserved.\")") ]])
			fnCutsceneBlocker()
		
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 56.\"[SOFTBLOCK] A nice refresher, but 55 already knows how to use Take Cover.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 6.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Leader](\"Covert Ops Compendium:: Volume 6.\"[SOFTBLOCK] This is surprisingly informative![SOFTBLOCK] 55 learned Take Cover, which increases protection and accuracy!")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"55\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end