--[Fencer's Friend: Volume 1]
--Teaches Batter and Take Point. Christine starts the game with these.
local sCharacter = "Christine"
local sAbility = "Batter"
local iRequiredLevel = 0
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 1.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"A powerful ability that requires 4 combo points, Batter deals extra damage and will stun most targets for one turn.[SOFTBLOCK] It may be advisible to save it and use it right before an enemy uses a powerful attack.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Every good knight should also know Take Point.[SOFTBLOCK] Enemies will feel threatened by your poise and courage, and will attack you more than your teammates.[SOFTBLOCK] Use it to draw attention from them when they are vulnerable.[SOFTBLOCK] It can be toggled off with Line Formation.\")") ]])
			fnCutsceneBlocker()
			
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 1.\"[SOFTBLOCK] A nice refresher, but Christine already knows how to use Batter.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 1.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 1.\"[SOFTBLOCK] This is surprisingly informative![SOFTBLOCK] Christine learned Batter, which stuns enemies in exchange for combo points, and Take Point, which increases her threat.)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Christine\", \"" .. LM_GetCallStack(0) .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end