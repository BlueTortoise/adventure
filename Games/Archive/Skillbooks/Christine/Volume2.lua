--[Knight's Newsletter: Volume 2]
--Teaches Encourage. Christine gets this one for free.
local sCharacter = "Christine"
local sAbility = "Encourage"
local iRequiredLevel = 1
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 2.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"You can improve the performance of your entire team by keeping their morale up. Encourage increases their accuracy and damage for 5 turns. Use it when you expect a tough fight, as its benefits will add up.\")") ]])
			fnCutsceneBlocker()
			
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 2.\"[SOFTBLOCK] A nice refresher, but Christine already knows how to use Encourage.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 2.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 2.\"[SOFTBLOCK] This is surprisingly informative![SOFTBLOCK] Christine learned Encourage, which improves the party's attack and accuracy for 5 turns!)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Christine\", \"" .. LM_GetCallStack(0) .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end