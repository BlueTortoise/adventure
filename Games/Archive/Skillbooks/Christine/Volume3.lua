--[Knight's Newsletter: Volume 3]
--Teaches Rally.
local sCharacter = "Christine"
local sAbility = "Rally"
local iRequiredLevel = 2
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 3.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"Never underestimate the effects of good morale.[SOFTBLOCK] Nothing feels better than a punitive strike, such as Rally.[SOFTBLOCK] You might even recover some health after landing one!\")") ]])
			fnCutsceneBlocker()
			
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 3.\"[SOFTBLOCK] A nice refresher, but Christine already knows how to use Rally.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 3.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 3.\"[SOFTBLOCK] This is surprisingly informative![SOFTBLOCK] Christine learned Rally, which deals extra damage and restores some HP on impact!)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Christine\", \"" .. LM_GetCallStack(0) .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end