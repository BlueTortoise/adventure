--[Knight's Newsletter: Volume 4]
--Teaches Sweep.
local sCharacter = "Christine"
local sAbility = "Rally"
local iRequiredLevel = 3
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 4.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"The length of your electrospear is for more than just good looks, it also means you can strike many enemies at once.[SOFTBLOCK] Unlike conventional weapons, the electrospear need not actually touch the enemy!\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"The Sweep ability will strike all enemies using the electrical discharge of your spear, but at reduced damage.[SOFTBLOCK] Use it to deal with large crowds of machines or aquatic creatures.\")") ]])
			fnCutsceneBlocker()
			
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 4.\"[SOFTBLOCK] A nice refresher, but Christine already knows how to use Sweep.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 4.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 4.\"[SOFTBLOCK] This is surprisingly informative![SOFTBLOCK] Christine learned Sweep, which strikes all enemies for electrical damage!)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Christine\", \"" .. LM_GetCallStack(0) .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end