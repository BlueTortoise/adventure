--[Knight's Newsletter: Volume 5]
--Teaches Officer's Charge.
local sCharacter = "Christine"
local sAbility = "Officer Charge"
local iRequiredLevel = 3
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 5.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"There are no rules on a battlefield.[SOFTBLOCK] Charging straight at an unprepared enemy is not just fair game, it is sometimes the order of the day.[SOFTBLOCK] Shatter their defenses and rally your squad.[SOFTBLOCK] Tally ho!\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Leader](\"You will increase your own threat and protection to match for 4 turns.[SOFTBLOCK] Enemies will attack you and you will be prepared for it.[SOFTBLOCK] Your allies will then have a clear opening to attack.[SOFTBLOCK] Make use of it.\")") ]])
			fnCutsceneBlocker()
			
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 5.\"[SOFTBLOCK] A nice refresher, but Christine already knows how to use Officer's Charge.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 5.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("[VOICE|Leader](\"The Knight's Newsletter:: Volume 5.\"[SOFTBLOCK] This is surprisingly informative![SOFTBLOCK] Christine learned Officer's Charge, which greatly increases threat and protection for 4 turns!)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Christine\", \"" .. LM_GetCallStack(0) .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end