--[Assemble Volume List]
--When at a campfire, a character can go back through any skillbook she has found and learn the ability or read up on it.
-- This script assembles the skillbook list.
local sBasePath = fnResolvePath()

--Listing.
local iEntries = 0
local zaList = {}

--Adder function:
local fnAddEntry = function(sAbilityName, sFullName, sVariablePath, iRequiredLevel, sExecutionPath)
	
	iEntries = iEntries + 1
	zaList[iEntries] = {}
	zaList[iEntries].sAbilityName   = sAbilityName
	zaList[iEntries].sFullName      = sFullName
	zaList[iEntries].sSkillbookPath = sVariablePath
	zaList[iEntries].iRequiredLevel = iRequiredLevel
	zaList[iEntries].sExecutionPath = sExecutionPath
	
end

--Build the ability listing.
fnAddEntry("Shock",          "Vol 0: Shock and Puncture",    "Root/Variables/Global/Christine/iSkillbook00", -1, sBasePath .. "Volume0.lua")
fnAddEntry("Batter",         "Vol 1: Batter and Take Point", "Root/Variables/Global/Christine/iSkillbook01",  0, sBasePath .. "Volume1.lua")
fnAddEntry("Encourage",      "Vol 2: Encourage",             "Root/Variables/Global/Christine/iSkillbook02",  1, sBasePath .. "Volume2.lua")
fnAddEntry("Rally",          "Vol 3: Rally",                 "Root/Variables/Global/Christine/iSkillbook03",  2, sBasePath .. "Volume3.lua")
fnAddEntry("Sweep",          "Vol 4: Sweep",                 "Root/Variables/Global/Christine/iSkillbook03",  3, sBasePath .. "Volume4.lua")
fnAddEntry("Officer Charge", "Vol 5: Officer Charge",        "Root/Variables/Global/Christine/iSkillbook03",  4, sBasePath .. "Volume5.lua")

--Go through Christine's abilities and add entries as necessary.
AC_PushPartyMember("Christine")

	--We need the level to know if Mei can learn the ability.
	local iLevel = ACE_GetProperty("Level")
	
	--Go through the list.
	for i = 1, iEntries, 1 do
		
		--Get the associated variable.
		local iHasReadBook = VM_GetVar(zaList[i].sSkillbookPath, "N")
		
		--Check if we already know the ability. If so, the menu provides useful information, and the menu has full details.
		if(ACE_GetProperty("Has Ability", zaList[i].sAbilityName)) then
			AM_SetProperty("Register Skillbook Script", zaList[i].sFullName, zaList[i].sExecutionPath)
		
		--If we don't know the ability but have read the skillbook and can learn the skill:
		elseif(iHasReadBook == 1.0 and iLevel >= zaList[i].iRequiredLevel) then
			AM_SetProperty("Register Skillbook Script", string.sub(zaList[i].sFullName, 1, 7) .. "(Read now!)", zaList[i].sExecutionPath)
		
		--If we don't know the ability but have read the skillbook, but it's too complex:
		elseif(iHasReadBook == 1.0 and iLevel < zaList[i].iRequiredLevel) then
			AM_SetProperty("Register Skillbook Script", string.sub(zaList[i].sFullName, 1, 7) .. "(???)", zaList[i].sExecutionPath)
		
		--If we haven't read the skillbook, it simply is not listed.
		else
		
		end
		
	end
	
DL_PopActiveObject()