--[Murderer's Monthly: Volume 1]
--Teaches Critical Stab. Florentina starts with this ability.
local sCharacter = "Florentina"
local sAbility = "Critical Stab"
local iRequiredLevel = 0
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Mei](\"Murderer's Monthly:: Volume 1.\"[SOFTBLOCK] Absolutely ghastly imagery, and also not really relevant...)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Florentina](\"Requiring 4 Combo Points, Critical Stab deals extra damage but is otherwise a normal attack.[SOFTBLOCK] Use it to badly damage reinforcements after taking an enemy down with normal strikes.\")") ]])
			fnCutsceneBlocker()
		
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 1.\"[SOFTBLOCK] A disturbing refresher, but Florentina already knows how to use Critical Stab.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 1.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 1.\"[SOFTBLOCK] The pictures show a lot of viscera! Florentina learned the Critical Stab ability, which deals extra damage but requires Combo Points.)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Florentina\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end