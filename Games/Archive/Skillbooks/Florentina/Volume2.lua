--[Murderer's Monthly: Volume 2]
--Teaches Defend.
local sCharacter = "Florentina"
local sAbility = "Defend"
local iRequiredLevel = 1
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Special: This book can be read from a topics listing, so if it receives an argument it behaves differently.
local bSpecial = false
if(LM_GetNumOfArgs() > 0) then bSpecial = true end

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	
	--Normal:
	if(bSpecial == false) then
		fnStandardDialogue("Thought: [VOICE|Mei](\"Murderer's Monthly:: Volume 2.\"[SOFTBLOCK] Less ghastly than the namesake might suggest, but not relevant at the moment.)")
	
	--Topics mode.
	else
		WD_SetProperty("Append", "Thought: [VOICE|Mei](\"Murderer's Monthly:: Volume 2.\"[SOFTBLOCK] Less ghastly than the namesake might suggest, but not relevant at the moment.)[BLOCK][CLEAR]")
	end

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
	
		--Normal:
		if(bSpecial == false) then
		
            --If this was called from the skillbook menu:
            if(bIsSkillbookMenu == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Florentina](\"While not as effective as Parry, Defend still generates extra Combo Points and reduces incoming damage.[SOFTBLOCK] It can be very useful when low on health, to save up for more powerful abilities.\")") ]])
                fnCutsceneBlocker()
            
            --Reading this on a bookshelf somewhere:
            else
                fnStandardDialogue("Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 2.\"[SOFTBLOCK] Despite the namesake, not very grisly this month. Also, Florentina already knows Defend.)")
            end
		
		--Topics mode.
		else
		
            --If this was called from the skillbook menu:
            if(bIsSkillbookMenu == true) then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Florentina](\"While not as effective as Parry, Defend still generates extra Combo Points and reduces incoming damage.[SOFTBLOCK] It can be very useful when low on health, to save up for more powerful abilities.\")") ]])
                fnCutsceneBlocker()
            
            --Reading this on a bookshelf somewhere:
            else
                fnStandardDialogue("Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 2.\"[SOFTBLOCK] Despite the namesake, not very grisly this month. Also, Florentina already knows Defend.)[BLOCK][CLEAR]")
            end
            
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
	
		--Normal:
		if(bSpecial == false) then
			fnStandardDialogue("Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 2.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")
		
		--Topics mode.
		else
			WD_SetProperty("Append", "Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 2.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)[BLOCK][CLEAR]")
		end

	--If they don't have the ability:
	else
	
		--Normal:
		if(bSpecial == false) then
			fnStandardDialogue("Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 2.\"[SOFTBLOCK] The pictures show how to not get counter-murdered! Florentina learned the Defend ability, which reduces damage and generates Combo Points!)")
		
		--Topics mode.
		else
			WD_SetProperty("Append", "Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 2.\"[SOFTBLOCK] The pictures show how to not get counter-murdered! Florentina learned the Defend ability, which reduces damage and generates Combo Points!)[BLOCK][CLEAR]")
		end
	
		--Activate the ability.
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Florentina\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end