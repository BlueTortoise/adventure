--[Murderer's Monthly: Volume 3]
--Teaches Intimidate.
local sCharacter = "Florentina"
local sAbility = "Intimidate"
local iRequiredLevel = 2
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Mei](\"Murderer's Monthly:: Volume 3.\"[SOFTBLOCK] Absolutely ghastly imagery, and also not really relevant...)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Florentina](\"The best strike from your enemy is the one that is hesitant and unsure.[SOFTBLOCK] By intimidating the enemy, you deal no damage but cut their attack power in half.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Florentina](\"Some enemies have specific patterns, so using this ability when they are about to land a powerful strike will greatly reduce the harm.[SOFTBLOCK] Time its use correctly!\")") ]])
			fnCutsceneBlocker()
		
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 3.\"[SOFTBLOCK] A disturbing refresher, but Florentina already knows how to use Intimidate.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 3.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardMajorDialogue()
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Florentina](\"Murderer's Monthly:: Volume 3.\"[SOFTBLOCK] There is an article on how to intimidate people, monsters, rocks...[SOFTBLOCK] It's informative enough.)[BLOCK][CLEAR][VOICE|Florentina](Florentina learned the Intimidate ability, which reduces a target's attack power!)[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Florentina: Negotiating techniques work on the battlefield?[SOFTBLOCK] I wonder if battlefield techniques work at the negotiating table...[BLOCK][CLEAR]") ]])
		fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei: That is the wrong lesson to learn, Florentina.") ]])
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Florentina\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end