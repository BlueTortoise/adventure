--[Arcanist's Advisor: Volume 0]
--Teaches Scorch. Jeanne begins the game with this.
local sCharacter = "Jeanne"
local sAbility = "Scorch"
local iRequiredLevel = -1

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Leader](\"The Arcanist's Advisor:: Volume 0.\"[SOFTBLOCK] While there are some nice pictures of pretty magic, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		fnStandardDialogue("Thought: [VOICE|Leader](\"The Arcanist's Advisor:: Volume 0.\"[SOFTBLOCK] A nice refresher, but Jeanne already knows how to use Scorch.)")

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Leader](\"The Arcanist's Advisor:: Volume 0.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Leader](\"The Arcanist's Advisor:: Volume 0.\"[SOFTBLOCK] This is surprisingly informative! Jeanne learned the Scorch ability, which deals flame damage!)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end

end