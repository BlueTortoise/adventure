--[Arcanist's Advisor: Volume 1]
--Teaches Flame Burst. Jeanne starts the game with this.
local sCharacter = "Jeanne"
local sAbility = "Flame Burst"
local iRequiredLevel = 0

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Leader](\"The Arcanist's Advisor:: Volume 1.\"[SOFTBLOCK] While there are some nice pictures of blowing things up, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		fnStandardDialogue("Thought: [VOICE|Leader](\"The Arcanist's Advisor:: Volume 1.\"[SOFTBLOCK] A nice refresher, but Jeanne already knows how to use Flame Burst.)")

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Leader](\"The Arcanist's Advisor:: Volume 1.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Leader](\"The Arcanist's Advisor:: Volume 1.\"[SOFTBLOCK] This is surprisingly informative! Jeanne learned the Flame Burst ability, which requires Combo Points but deals major fire damage to a single target.)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end

end