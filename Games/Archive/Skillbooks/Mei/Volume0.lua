--[Fencer's Friend: Volume 0]
--Teaches Rend. Mei starts the game with this, so she can't really learn it.
local sCharacter = "Mei"
local sAbility = "Rend"
local iRequiredLevel = -1
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 0.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"Rend is a technique which uses a lateral cut to open a wound on the enemy's body.[SOFTBLOCK] While the initial impact is weaker, the injury inflicted is greater overall.[SOFTBLOCK] Remember::[SOFTBLOCK] Bleed damage ignores protection!\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"Blind, however, is a technique which deals very little damage and is meant to reduce your enemy's accuracy.[SOFTBLOCK] Some enemies are particularly vulnerable.[SOFTBLOCK] If your enemy looks like they'd have trouble seeing, due to excess clothes or hair, try blinding them!\")") ]])
			fnCutsceneBlocker()
		
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 0.\"[SOFTBLOCK] A nice refresher, but Mei already knows how to use Rend.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 0.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 0.\"[SOFTBLOCK] This is surprisingly informative! Mei learned the Rend ability, which deals bleed damage over time.)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Mei\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end