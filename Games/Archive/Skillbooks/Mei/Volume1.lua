--[Fencer's Friend: Volume 1]
--Teaches Powerful Strike. Mei starts the game with this, so she can't really learn it.
local sCharacter = "Mei"
local sAbility = "Powerful Strike"
local iRequiredLevel = 0
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 1.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"Requiring 4 combo points, Powerful Strike is just a normal strike with extra weight behind it. Use it to deal extra damage to finish an enemy off quicker when facing multiple foes!\")") ]])
			fnCutsceneBlocker()
			
		--Reading this on a bookshelf somewhere:
		else
			fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 1.\"[SOFTBLOCK] A nice refresher, but Mei already knows how to use Powerful Strike.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 1.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 1.\"[SOFTBLOCK] This is surprisingly informative! Mei learned the Powerful Strike ability, which requires Combo Points but deals major damage to a single target.)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Mei\", \"" .. LM_GetCallStack(0) .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end