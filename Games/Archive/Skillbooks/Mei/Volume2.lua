--[Fencer's Friend: Volume 2]
--Teaches Quick Strike.
local sCharacter = "Mei"
local sAbility = "Quick Strike"
local iRequiredLevel = 1
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Special: This book can be read from a topics listing, so if it receives an argument it behaves differently.
local bSpecial = false
if(LM_GetNumOfArgs() > 0) then bSpecial = true end

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	
	--Normal:
	if(bSpecial == false) then
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 2.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")
	
	--Topics mode.
	else
		WD_SetProperty("Append", "Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 2.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)[BLOCK][CLEAR]")
	end

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--Normal:
		if(bSpecial == false) then
		
			--If this was called from the skillbook menu:
			if(bIsSkillbookMenu == true) then
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"While not as powerful as a normal blow, Quick Strike is much more accurate.[SOFTBLOCK] Enemies who are extremely fast may be difficult to hit, but Quick Strike will help.\")[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"If you are blinded or slowed down, using Quick Strike may aid you in still dealing damage.[SOFTBLOCK] Take note that it cannot strike critically.\")") ]])
			
			--Reading this on a bookshelf somewhere:
			else
				fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 2.\"[SOFTBLOCK] A nice refresher, but Mei already knows how to use Quick Strike.)")
			end
	
		--Topics mode.
		else
		
			--If this was called from the skillbook menu:
			if(bIsSkillbookMenu == true) then
				fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"While not as powerful as a normal blow, Quick Strike is much more accurate.[SOFTBLOCK] Enemies who are extremely fast may be difficult to hit, but Quick Strike will help.\")[BLOCK][CLEAR]") ]])
				fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"If you are blinded or slowed down, using Quick Strike may aid you in still dealing damage.[SOFTBLOCK] Take note that it cannot strike critically.\")") ]])
				fnCutsceneBlocker()
			
			--Reading this on a bookshelf somewhere:
			else
				WD_SetProperty("Major Sequence Fast", true)
				WD_SetProperty("Append", "Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 2.\"[SOFTBLOCK] A nice refresher, but Mei already knows how to use Quick Strike.)[BLOCK][CLEAR]")
			end
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		--Normal:
		if(bSpecial == false) then
			fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 2.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")
	
		--Topics mode.
		else
			WD_SetProperty("Append", "Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 2.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)[BLOCK][CLEAR]")
		end

	--If they don't have the ability:
	else
		--Normal:
		if(bSpecial == false) then
			fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 2.\"[SOFTBLOCK] This is surprisingly informative! Mei learned the Quick Strike ability, which deals reduced damage but is extremely accurate.)")
	
		--Topics mode.
		else
			WD_SetProperty("Append", "Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 2.\"[SOFTBLOCK] This is surprisingly informative! Mei learned the Quick Strike ability, which deals reduced damage but is extremely accurate.)[BLOCK][CLEAR]")
		end
		
		--Add the ability.
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Mei\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end