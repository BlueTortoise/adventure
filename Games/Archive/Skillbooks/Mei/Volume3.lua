--[Fencer's Friend: Volume 3]
--Teaches Pommel Blow
local sCharacter = "Mei"
local sAbility = "Pommel Bash"
local iRequiredLevel = 2
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 3.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"Striking an enemy with Pommel Bash may not seem as effective as using your blade, but it has its advantages.[SOFTBLOCK] It inflicts stun damage and does not incur a cooldown.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"Enemies that are not fully in control of their faculties tend to be vulnerable to stuns.[SOFTBLOCK] It may also be worth it to deal less damage in order to prevent an enemy from attacking.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"Some enemies have patterns, so stunning them at the right time may prevent a great deal of injury!\")") ]])
			fnCutsceneBlocker()
			
		--Reading this on a bookshelf somewhere:
		else
			WD_SetProperty("Major Sequence Fast", true)
			WD_SetProperty("Append", "Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 3.\"[SOFTBLOCK] A nice refresher, but Mei already knows how to use Pommel Bash.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 3.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 3.\"[SOFTBLOCK] This is surprisingly informative! Mei learned the Pommel Bash ability, which deals reduced damage but applies stun to a target.)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Mei\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end