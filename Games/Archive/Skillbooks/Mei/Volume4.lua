--[Fencer's Friend: Volume 4]
--Teaches Taunt.
local sCharacter = "Mei"
local sAbility = "Taunt"
local iRequiredLevel = 3
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 4.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"Never underestimate the effect of morale.[SOFTBLOCK] Taunting many foes to reduce their damage may end up saving you in the long run.[SOFTBLOCK] Use it if there is a very large collection of enemies at once.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"When using Taunt, you'll need to be able to follow it up with force.[SOFTBLOCK] Having allies who can attack while you Taunt is the optimal strategy.\")") ]])
			fnCutsceneBlocker()
			
		--Reading this on a bookshelf somewhere:
		else
			WD_SetProperty("Major Sequence Fast", true)
			WD_SetProperty("Append", "[VOICE|Mei](\"The Fencer's Friend:: Volume 4.\"[SOFTBLOCK] A nice refresher, but Mei already knows how to use Taunt.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 4.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 4.\"[SOFTBLOCK] This is surprisingly informative! Mei learned the Taunt ability, which reduces attack power for all enemies.)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Mei\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end