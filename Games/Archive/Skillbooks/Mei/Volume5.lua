--[Fencer's Friend: Volume 5]
--Teaches Blade Dance.
local sCharacter = "Mei"
local sAbility = "Blade Dance"
local iRequiredLevel = 4
local bIsSkillbookMenu = AM_GetProperty("Is Skillbook Menu")

--Clear the skillbook flag.
AM_SetProperty("Clear Skillbook Flag")

--Character is not present.
if(AC_GetProperty("Is Character In Party", sCharacter) == false) then
	fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 5.\"[SOFTBLOCK] While there are some nice pictures, the book doesn't seem relevant.)")

--Character is present.
else

	--Push character. Check if they know the ability.
	AC_PushPartyMember(sCharacter)
		local iLevel      = ACE_GetProperty("Level")
		local bHasAbility = ACE_GetProperty("Has Ability", sAbility)
	DL_PopActiveObject()

	--If they already have the ability:
	if(bHasAbility == true) then
		
		--If this was called from the skillbook menu:
		if(bIsSkillbookMenu == true) then
			fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"A powerful ability meant to be used after extensive training, Blade Dance requires 6 combo points and attacks all foes simultaneously.\")[BLOCK][CLEAR]") ]])
			fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Mei](\"Because it requires so much setup time and works against all foes, it may be a good idea to Parry and defend oneself until the ability is ready, then unleash it to wipe the enemy out in one strike.\")") ]])
			fnCutsceneBlocker()
			
		--Reading this on a bookshelf somewhere:
		else
			WD_SetProperty("Major Sequence Fast", true)
			WD_SetProperty("Append", "Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 5.\"[SOFTBLOCK] A nice refresher, but Mei already knows how to use Blade Dance.)")
		end

	--If they don't meet the level requirement:
	elseif(iLevel < iRequiredLevel) then
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 5.\"[SOFTBLOCK] Unfortunately, it's really hard to understand!)")

	--If they don't have the ability:
	else
		fnStandardDialogue("Thought: [VOICE|Mei](\"The Fencer's Friend:: Volume 5.\"[SOFTBLOCK] This is surprisingly informative! Mei learned the Blade Dance ability, which requires Combo Points and strikes all enemies!)")
		AC_PushPartyMember(sCharacter)
			LM_ExecuteScript(gsRoot .. "Abilities/" .. sCharacter .. "/000 Initializer.lua", sAbility)
		DL_PopActiveObject()
	end
		
	if(bIsSkillbookMenu == true) then
		
		--Resolve the script path. It should have been passed in, though in erroneous cases it may be Null.
		local sScriptPath = "Null"
		if(fnArgCheck(1) == true) then
			sScriptPath = LM_GetScriptArgument(0)
		end
		
		--Build.
		local sString = "AM_SetProperty(\"Open To Skillbook Menu\", \"Mei\", \"" .. sScriptPath .. "\")"
		fnCutsceneBlocker()
		fnCutsceneInstruction(sString)
	end

end