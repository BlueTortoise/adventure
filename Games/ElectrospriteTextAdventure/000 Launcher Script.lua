--[Launcher Script]
--This gets called in order to load the images needed for the scenario. It will only load if it gets
-- booted from the main menu, otherwise this should already be loaded in Adventure Mode.

--Flag. This allows the player to play Electrosprites any time they want.
gsDatafilesPath = fnResolvePath() .. "Datafiles/"
OM_SetOption("Unlock Electrosprites", "1.000000")
OM_WriteConfigFiles()

--Audio Loading
LM_ExecuteScript(fnResolvePath() .. "Audio/ZRouting.lua")

--Font Loading
LM_ExecuteScript(fnResolvePath() .. "Fonts/000 Boot Fonts.lua")

--[Graphics Loading]
--User interface parts, such as combat and level-up. Also contains a lot of portrait pieces that are used in
-- the UI, such as the turn-order portraits.
if(bIsBootedFromMenu == true) then
    gsPostExec = nil
end
    
--[Text Adventure UI]
SLF_Open(gsDatafilesPath .. "UITextAdventure.slf")

--[Extraction Function]
local fnExtract = function(psaNames, psPrefix, psDLPath)
    
    --Arg check.
    if(psaNames == nil) then return end
    if(psPrefix == nil) then return end
    if(psDLPath == nil) then return end
    
    --Path.
    DL_AddPath(psDLPath)
    
    --Extraction loop.
    local i = 1
    while(psaNames[i] ~= nil) do
        DL_ExtractBitmap(psPrefix .. psaNames[i], psDLPath .. psaNames[i])
        i = i + 1
    end
end

--[Lookup Tables]
local zaLookups = {}
zaLookups[ 1] = {"TxtDialogue|EA|", "Root/Images/AdventureUI/Dialogue/EA|", {"BorderCard", "TextInput", "TextInputDark", "Mask_CharacterRender", "Mask_LocalityRender", "Mask_TextRender", "Mask_WorldRender"}}
zaLookups[ 2] = {"TxtDeckEd|",      "Root/Images/AdventureUI/DeckEd/",      {"Backing", "ButtonCancel", "ButtonReset", "ButtonSave", "ButtonSmallAdd", "ButtonSmallSub", "ButtonQuestion", "HelpInlay"}}

--[Execution]
for i = 1, #zaLookups, 1 do
    fnExtract(zaLookups[i][3], zaLookups[i][1], zaLookups[i][2])
end

--Options Menu
DL_AddPath("Root/Images/DollManor/TxtOptions/")
DL_ExtractBitmap("TxtOptions|Backing",    "Root/Images/DollManor/TxtOptions/Backing")
DL_ExtractBitmap("TxtOptions|BtnBack",    "Root/Images/DollManor/TxtOptions/BtnBack")
DL_ExtractBitmap("TxtOptions|BtnCancel",  "Root/Images/DollManor/TxtOptions/BtnCancel")
DL_ExtractBitmap("TxtOptions|BtnSave",    "Root/Images/DollManor/TxtOptions/BtnSave")
DL_ExtractBitmap("TxtOptions|BtnEndings", "Root/Images/DollManor/TxtOptions/BtnEndings")
DL_ExtractBitmap("TxtOptions|Confirm",    "Root/Images/DollManor/TxtOptions/Confirm")

--Dummy. These aren't used in this mode.
DL_AddPath("Root/Images/DollManor/Combat/")
DL_ExtractDummyBitmap("Root/Images/DollManor/Combat/Bar_Heart")
DL_ExtractDummyBitmap("Root/Images/DollManor/Combat/Bar_Half")
DL_ExtractDummyBitmap("Root/Images/DollManor/Combat/Bar_Empty")
DL_AddPath("Root/Images/DollManor/Overlays/")
DL_ExtractDummyBitmap("Root/Images/DollManor/Overlays/Move")
DL_ExtractDummyBitmap("Root/Images/DollManor/Overlays/DoorH")
DL_ExtractDummyBitmap("Root/Images/DollManor/Overlays/DoorV")

--Manually add this image with different filtering.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
DL_ExtractBitmap("TxtDialogue|ST|BorderCard",             "Root/Images/AdventureUI/Dialogue/BorderCard")
DL_ExtractBitmap("TxtDialogue|ST|TextInput",              "Root/Images/AdventureUI/Dialogue/TextInput")
DL_ExtractBitmap("TxtDialogue|ST|TextAdventureMapParts",  "Root/Images/AdventureUI/Dialogue/EA|TextAdventureMapParts")
DL_ExtractBitmap("TxtDialogue|ST|TextAdventureScrollbar", "Root/Images/AdventureUI/Dialogue/EA|TextAdventureScrollbar")
DL_ExtractBitmap("TxtDialogue|ST|PopupBorderCard",        "Root/Images/AdventureUI/Dialogue/EA|PopupBorderCard")
DL_ExtractBitmap("TxtDialogue|ST|TextAdventureMapParts",  "Root/Images/AdventureUI/Dialogue/TextAdventureMapParts")
DL_ExtractBitmap("TxtDialogue|ST|TextAdventureScrollbar", "Root/Images/AdventureUI/Dialogue/TextAdventureScrollbar")
DL_ExtractBitmap("TxtDialogue|ST|PopupBorderCard",        "Root/Images/AdventureUI/Dialogue/PopupBorderCard")
ALB_SetTextureProperty("Restore Defaults")

--[Border Card UI]
DL_AddPath("Root/Images/TxtAdv/UI/")
DL_ExtractBitmap("CombatWordBorderCard", "Root/Images/TxtAdv/UI/CombatWordBorderCard")

--[Navigation]
DL_AddPath("Root/Images/TxtAdv/Navigation/")
DL_ExtractBitmap("Navigation|Look",  "Root/Images/TxtAdv/Navigation/Look")
DL_ExtractBitmap("Navigation|North", "Root/Images/TxtAdv/Navigation/North")
DL_ExtractBitmap("Navigation|Up",    "Root/Images/TxtAdv/Navigation/Up")
DL_ExtractBitmap("Navigation|West",  "Root/Images/TxtAdv/Navigation/West")
DL_ExtractBitmap("Navigation|Wait",  "Root/Images/TxtAdv/Navigation/Wait")
DL_ExtractBitmap("Navigation|East",  "Root/Images/TxtAdv/Navigation/East")
DL_ExtractBitmap("Navigation|Think", "Root/Images/TxtAdv/Navigation/Think")
DL_ExtractBitmap("Navigation|South", "Root/Images/TxtAdv/Navigation/South")
DL_ExtractBitmap("Navigation|Down",  "Root/Images/TxtAdv/Navigation/Down")
DL_ExtractBitmap("Navigation|ZoomBar",  "Root/Images/TxtAdv/Navigation/ZoomBar")
DL_ExtractBitmap("Navigation|ZoomTick", "Root/Images/TxtAdv/Navigation/ZoomTick")

--[Characters]
--These are loaded from the dedicated slf file.
SLF_Open(gsDatafilesPath .. "ElectrospriteAdventure.slf")
DL_AddPath("Root/Images/ElectrospriteAdv/Characters/")
DL_ExtractBitmap("Player", "Root/Images/ElectrospriteAdv/Characters/Player")

--Five NPCs, from A to E. All have the same sequences.
local saArray = {"A", "B", "C", "D", "E"}
for i = 1, 5, 1 do
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "Cored",  "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "Cored")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "Golem",  "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "Golem")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "Normal", "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "Normal")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "TF0",    "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "TF0")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "TF1",    "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "TF1")
    DL_ExtractBitmap("NPC" ..  saArray[i] .. "TF2",    "Root/Images/ElectrospriteAdv/Characters/NPC" ..  saArray[i] .. "TF2")
end

--Clean
SLF_Close()

--[ ===================================== Creation and Setup ==================================== ]
--Create the level. Store the previous level, if it exists, in the level for later retrieval.
TL_CreateAndSuspend()
TL_SetProperty("Construct Electrosprite")
TL_SetProperty("Set Combat Type", 1)

--Set the root path.
gzTextVar = {}
gzTextVar.sRootPath = fnResolvePath()

--Execute the rest of the construction sequence.
LM_ExecuteScript(gzTextVar.sRootPath .. "001 System Variables.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "002 Map Builder.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "003 Scenario Variables.lua")

--Build the initial command listing.
LM_ExecuteScript(gzTextVar.sRootPath .. "200 Command Builder.lua")

--Clean.
bIsBootedFromMenu = false
