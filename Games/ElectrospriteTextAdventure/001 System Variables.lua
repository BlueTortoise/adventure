--[System Variables]
--Creates all variables and constants that the rest of the scenario depends on. Everything should
-- be stored in gzTextVar to prevent conflicts with Adventure Mode.
TL_SetProperty("String Handler Path", gzTextVar.sRootPath .. "100 Input Handler Script.lua")

--Path Constants
gzTextVar.sRoomHandlers = gzTextVar.sRootPath .. "Room Handlers/"
gzTextVar.sEntityHandlers = gzTextVar.sRootPath .. "Entity Handlers/"
gzTextVar.sTurnEndScript = gzTextVar.sRootPath .. "400 Turn End.lua"
gzTextVar.sExamineHandler = gzTextVar.sRootPath .. "Item Handlers/Examinables.lua"
gzTextVar.sThinkHandler = gzTextVar.sRootPath .. "201 Think Handler.lua"

--Build Functions
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnBuildLocalityInfo.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnDialogueCutscene.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetIndexOfItemInInventory.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetRoomIndex.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetRoomPosition.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRemoveEntity.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRemoveItemFromInventory.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRemoveItemFromRoom.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnListEntities.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnMarkMinimapForPosition.lua")

--Dependent
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetIndexOfItemInRoom.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetMovePath.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnListObjects.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRebuildEntityVisibility.lua")

--Map Sizes
gzTextVar.fMapDistance = 1.0

--Activity Variables
gzTextVar.iCurrentEntity = -1

--Image Depths
gzTextVar.fImgStdDepth = -0.250000
gzTextVar.fImgStdDepthInc = -0.000001
gzTextVar.fStdYOffset = -100

--[AI State Machine Variables]
--Setting these variables changes what the AI script calls do. For example, setting gzTextVar.bIsCheckingHostility
-- to true changes the AI script calls to return hostility instead of running their AI script.
gzTextVar.bIsCheckingHostility = false
gzTextVar.bLastHostilityCheck = false
gzTextVar.bIsTriggeringFights = false