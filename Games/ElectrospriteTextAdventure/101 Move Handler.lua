--[ ===================================== Movement Handler ====================================== ]
--This script handles movement cases. It receives "Move X" where X is the direction. "North" and "N"
-- both map to the same thing.
--Sets the handler variable if it handled the input.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--[ ====================================== Command Verify ======================================= ]
--Check if this is actually a movement command.
local sFirstLetter = "x"

--If the command starts with "move":
if(string.sub(sString, 1, 4) == "move") then
    sFirstLetter = string.sub(sString, 6, 6)

--If the command starts with "go":
elseif(string.sub(sString, 1, 2) == "go") then
    sFirstLetter = string.sub(sString, 4, 4)

--If the command is a single letter long and is one of the movement directions:
elseif(sString == "n" or sString == "s" or sString == "e" or sString == "w" or sString == "u" or sString == "d") then
    sFirstLetter = string.sub(sString, 1, 1)

--If the command is a single word long and that is a movement direction:
elseif(sString == "north" or sString == "south" or sString == "east" or sString == "west" or sString == "up" or sString == "ddown") then
    sFirstLetter = string.sub(sString, 1, 1)

--Failed.
else
    return
end

--If we got this far, it's a movement command.
gbHandledInput = true

--[ ========================================= Execution ========================================= ]
--[Wound Checker]
--If the player is crippled, they cannot move.
if(gzTextVar.gzPlayer.bIsCrippled) then
    
    TL_SetProperty("Append", "You are too hurt to move.\n")
    LM_ExecuteScript(gzTextVar.sTurnEndScript)
    return
end

--[Room Locator]
--Find out what room the player is currently in. If they're not in a room, moves always fail.
local iRoomIndex = -1
for i = 1, gzTextVar.iRoomsTotal, 1 do
    if(gzTextVar.gzPlayer.sLocation == gzTextVar.zRoomList[i].sName) then
        iRoomIndex = i
        break
    end
end
if(iRoomIndex == -1) then return end

--[Connection Checker]
--See if this room has a connection in the requested direction. If it's "Null" then we don't.
local sDestination = "Null"
if(sFirstLetter == "n") then
    sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveN
elseif(sFirstLetter == "s") then
    sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveS
elseif(sFirstLetter == "e") then
    sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveE
elseif(sFirstLetter == "w") then
    sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveW
elseif(sFirstLetter == "u") then
    sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveU
elseif(sFirstLetter == "d") then
    sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveD
end

--Failure:
if(sDestination == "Null") then
    TL_SetProperty("Append", "You can't go that way.\n")
    return
end

--Unmark the previous location, and mark this one as visible.
fnUnmarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
fnMarkMinimapForPosition(sDestination)

--Success! Move the character.
gzTextVar.gzPlayer.sLocation = sDestination
local fX, fY, fZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
TL_SetProperty("Set Map Focus", -fX, -fY)

--Show the room's name, list of entities, and objects.
TL_SetProperty("Append", "Arriving at " .. sDestination .. "\n")
fnListEntities(gzTextVar.gzPlayer.sLocation, true)
fnListObjects(gzTextVar.gzPlayer.sLocation, true)

--End the turn.
LM_ExecuteScript(gzTextVar.sTurnEndScript)