--[ ========================================== Core AI ========================================== ]
--AI used by a human who has been stuck with a golem core. They will move to the conversion chamber 
-- and get converted. After that, they switch to the golem AI.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--[ ====================================== Hostility Checker ==================================== ]
--If this flag is set, the AI is checking entity hostility.
if(gzTextVar.bIsCheckingHostility == true) then
    return
end

--[ ======================================= Detection Logic ===================================== ]
--Entity ignores you.

--[ ======================================= Movement Logic ====================================== ]
--If the entity is not currently at Headquarters, it moves there.
if(gzTextVar.zEntities[i].sLocation ~= "Headquarters") then
    
    --Make the first move in the direction in question.
    gzTextVar.zEntities[i].iMoveIndex = 1
    
    --Get the current room.
    local sRoomName = gzTextVar.zEntities[i].sLocation
    local iRoomIndex = fnGetRoomIndex(sRoomName)
    
    --Get the target room.
    local iTargetIndex = fnGetRoomIndex("Headquarters")
    
    --Error checks.
    if(iRoomIndex == -1 or iTargetIndex == -1) then return end
    
    --Get the first letter of matching movement.
    local sPathInstructions = gzTextVar.zRoomList[iTargetIndex].zPathListing[iRoomIndex]
    if(sPathInstructions == "") then return end
    local sFirstLetter = string.sub(sPathInstructions, 1, 1)
    
    --Move in the requested direction:
    local sDestination = "Null"
    local sArrivesFrom = "nowhere"
    local sDepartsTo = "nowhere"
    if(sFirstLetter == "N") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveN
        sArrivesFrom = "the south"
        sDepartsTo = "to the north"
    elseif(sFirstLetter == "S") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveS
        sArrivesFrom = "the north"
        sDepartsTo = "to the south"
    elseif(sFirstLetter == "E") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveE
        sArrivesFrom = "the west"
        sDepartsTo = "to the east"
    elseif(sFirstLetter == "W") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveW
        sArrivesFrom = "the east"
        sDepartsTo = "to the west"
    elseif(sFirstLetter == "U") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveU
        sArrivesFrom = "below"
        sDepartsTo = "down"
    elseif(sFirstLetter == "D") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveD
        sArrivesFrom = "above"
        sDepartsTo = "up"
    end

    --Failure:
    if(sDestination == "Null") then
        return
    end

    --If the character was in the same location as the player:
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
        TL_SetProperty("Append", "Controlled by the golem core in " .. gzTextVar.zEntities[i].sGenderA .. " chest, " .. gzTextVar.zEntities[i].sDisplayName .. " goes " .. sDepartsTo .. ".")
        
    --If the arrival position is the same as the player's:
    elseif(sDestination == gzTextVar.gzPlayer.sLocation) then
        TL_SetProperty("Append", gzTextVar.zEntities[i].sDisplayName .. " arrives from " .. sArrivesFrom .. ".")
    end

    --Success! Move the character.
    gzTextVar.zEntities[i].sLocation = sDestination

--[Headquarters]
--When at the headquarters, the entity will step into the conversion chamber, or wait if it is occupied.
else

    --Check if the player can see this happening.
    local bPlayerSeesThis = false
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then bPlayerSeesThis = true end

    --If the conversion chamber is currently not being used by this entity, wait.
    if(gzTextVar.sConversionChamberTarget ~= gzTextVar.zEntities[i].sDisplayName) then
        
        --Can we enter it?
        if(gzTextVar.sConversionChamberTarget == "Nobody") then
            gzTextVar.sConversionChamberTarget = gzTextVar.zEntities[i].sDisplayName
            gzTextVar.iConversionState = 1
            if(bPlayerSeesThis) then
                TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
                TL_SetProperty("Append", gzTextVar.zEntities[i].sDisplayName .. " enters the conversion tube. It closes behind " .. gzTextVar.zEntities[i].sGenderA .. " with a click, and quickly fills with nanitic fluid.")
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
        
        --Wait.
    else
            if(bPlayerSeesThis) then
                TL_SetProperty("Append", "Standing perfectly still, " .. gzTextVar.zEntities[i].sDisplayName .. " waits for the conversion tube to be free.")
            end
        
        end
    
    --Chamber is being used by this entity.
    else
    
        --Stage 1: Declothed.
        if(gzTextVar.iConversionState == 1) then
            gzTextVar.iConversionState = gzTextVar.iConversionState + 1
            gzTextVar.zEntities[i].sQueryPicture = gzTextVar.zEntities[i].sTF0Picture
            if(bPlayerSeesThis) then
                TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
                TL_SetProperty("Append", "Small mechanical arms descend from the tube's ceiling and undress " .. gzTextVar.zEntities[i].sDisplayName .. ". " .. gzTextVar.zEntities[i].sGenderCapitalB .. " assists the arms when prompted by the core in " .. gzTextVar.zEntities[i].sGenderA .. " chest, but otherwise remains motionless.")
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
    
        --Stage 2: Conversion begins.
        elseif(gzTextVar.iConversionState == 2) then
            gzTextVar.iConversionState = gzTextVar.iConversionState + 1
            gzTextVar.zEntities[i].sQueryPicture = gzTextVar.zEntities[i].sTF0Picture
            if(bPlayerSeesThis) then
                TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
                TL_SetProperty("Append", "A small needle appears from above and injects itself into the unresisting " .. gzTextVar.zEntities[i].sDisplayName .. ". Nanitic fluid flows into " .. gzTextVar.zEntities[i].sGenderA .. " body as it soaks into " .. gzTextVar.zEntities[i].sGenderA .. " skin. " .. gzTextVar.zEntities[i].sGenderCapitalB .. " already looks inhuman.")
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
    
        --Stage 3: Reprogramming.
        elseif(gzTextVar.iConversionState == 3) then
            gzTextVar.iConversionState = gzTextVar.iConversionState + 1
            gzTextVar.zEntities[i].sQueryPicture = gzTextVar.zEntities[i].sTF1Picture
            if(bPlayerSeesThis) then
                TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
                TL_SetProperty("Append", "The reprogramming helmet appears from above and inserts tself over " .. gzTextVar.zEntities[i].sDisplayName .. "'s head, conforming perfectly to " .. gzTextVar.zEntities[i].sGenderA .. " face. Millions of tiny needles inject nanofluid directly into " .. gzTextVar.zEntities[i].sGenderA .. " brain, reprogramming " .. gzTextVar.zEntities[i].sGenderA .. " it and preparing " .. gzTextVar.zEntities[i].sGenderA .. " for synthetic life.")
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
    
        --Stage 4: Late Reprogramming.
        elseif(gzTextVar.iConversionState == 4) then
            gzTextVar.iConversionState = gzTextVar.iConversionState + 1
            gzTextVar.zEntities[i].sQueryPicture = gzTextVar.zEntities[i].sTF1Picture
            if(bPlayerSeesThis) then
                TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
                TL_SetProperty("Append", "The memories of " .. gzTextVar.zEntities[i].sDisplayName .. " flash before her eyes, modified by the conversion chamber to better suit her new role in life. Her familial interactions are replaced by obedience to her Lord Golem, while the books she read become technical manuals. She is nearly ready for service.")
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
    
        --Stage 5: Cooldown.
        elseif(gzTextVar.iConversionState == 5) then
            gzTextVar.iConversionState = gzTextVar.iConversionState + 1
            gzTextVar.zEntities[i].sQueryPicture = gzTextVar.zEntities[i].sTF2Picture
            if(bPlayerSeesThis) then
                TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
                TL_SetProperty("Append", "The helmet lifts to reveal the optic sensors of " .. gzTextVar.zEntities[i].sDisplayName .. ". The nanite fluid quickly drains away as she drily states 'Reprogramming complete'. She waits patiently as the injection line is replaced and begins charging her power core.")
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
    
        --Stage 6: Assignment
        elseif(gzTextVar.iConversionState == 6) then
        
            --Reset.
            gzTextVar.iConversionState = 0
            gzTextVar.sConversionChamberTarget = "Nobody"
            
            --Display.
            if(bPlayerSeesThis) then
                TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sGolemPicture, 0)
                TL_SetProperty("Append", "The drained tube opens and allows Unit " .. gzTextVar.iUnitDesignation .. " to exit. She moves to the nearby work terminal to receive her assignment. She is now at your disposal.")
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                TL_SetProperty("Append", "You have received 100 points! There are " .. (gzTextVar.iHumansLeft-1) .. " more humans to transform before this training exercise is complete.")
                TL_SetProperty("Create Blocker")
            
            --Display when the player is not present.
            else
                TL_SetProperty("Append", gzTextVar.iUnitDesignation .. " has been reborn as a golem!")
                TL_SetProperty("Append", "You have received 100 points! There are " .. (gzTextVar.iHumansLeft-1) .. " more humans to transform before this training exercise is complete.")
                TL_SetProperty("Create Blocker")
            end
        
            --Change designation variables.
            gzTextVar.zEntities[i].sDisplayName = "Unit " .. gzTextVar.iUnitDesignation
            gzTextVar.zEntities[i].sQueryName = "unit " .. gzTextVar.iUnitDesignation
            gzTextVar.zEntities[i].sQueryPicture = gzTextVar.zEntities[i].sGolemPicture
            gzTextVar.zEntities[i].sUniqueName = "unit " .. gzTextVar.iUnitDesignation
            gzTextVar.zEntities[i].bIsCrippled = false
            gzTextVar.zEntities[i].sGenderA = "her"
            gzTextVar.zEntities[i].sGenderB = "she"
            gzTextVar.zEntities[i].sGenderCapitalA = "Her"
            gzTextVar.zEntities[i].sGenderCapitalB = "She"
        
            --Next unit.
            gzTextVar.iUnitDesignation = gzTextVar.iUnitDesignation + 1
            
            --Change the AI script.
            gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Golem.lua"
            gzTextVar.zEntities[i].sAIHandler = gzTextVar.sRootPath .. "AIs/Golem.lua"
    
            --Reduce the human count.
            gzTextVar.iHumansLeft = gzTextVar.iHumansLeft - 1
            if(gzTextVar.iHumansLeft == 1) then
                gzTextVar.zEntities[gzTextVar.iSusanSlot].sLocation = "Bridge"
            end
        end
    end
end

