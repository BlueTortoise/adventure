--[ ========================================== Golem AI ========================================= ]
--AI used by converted golems. Doesn't do anything.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--[ ====================================== Hostility Checker ==================================== ]
--If this flag is set, the AI is checking entity hostility.
if(gzTextVar.bIsCheckingHostility == true) then
    return
end

--[ ======================================= Detection Logic ===================================== ]
--Entity ignores you.

--[ ======================================== Patrol Logic ======================================= ]
--Entity does not move.
