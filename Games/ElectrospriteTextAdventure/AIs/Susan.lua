--[ ========================================== Susan AI ========================================= ]
--Special AI, used by the Electrosprite character.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--[ ====================================== Hostility Checker ==================================== ]
--If this flag is set, the AI is checking entity hostility.
if(gzTextVar.bIsCheckingHostility == true) then
end

--[ ======================================= Detection Logic ===================================== ]
--If in the same room, doesn't start battles.
if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation or gzTextVar.bIsTriggeringFights) then
    return
end

--[ ======================================== Patrol Logic ======================================= ]
--[New Movement]
--If the current movement index is -1, move to the next node on the patrol list.
if(gzTextVar.zEntities[i].iMoveIndex == -1) then
    --Does not patrol

--Continuing a patrol path.
else
    --Does not patrol

end