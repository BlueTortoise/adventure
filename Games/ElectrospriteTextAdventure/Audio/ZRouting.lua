--[ ===================================== Audio Routing File ==================================== ]
--Loads music/sfx/voice samples.
local sBasePath = fnResolvePath()

--SFX Path
local sSFXPath = sBasePath .. "SFX/"
AudioManager_Register("Combat|AttackMiss",         "AsSound", "AsSample", sSFXPath .. "AttackMiss.wav")
AudioManager_Register("World|ButtonClick",         "AsSound", "AsSample", sSFXPath .. "ButtonClick.ogg")
AudioManager_Register("Combat|Heal",               "AsSound", "AsSample", sSFXPath .. "Heal.ogg")
AudioManager_Register("World|FlipSwitch",          "AsSound", "AsSample", sSFXPath .. "FlipSwitch.wav")
AudioManager_Register("Combat|Impact_Debuff",      "AsSound", "AsSample", sSFXPath .. "Impact_Debuff.ogg")
AudioManager_Register("Combat|Impact_Pierce",      "AsSound", "AsSample", sSFXPath .. "Impact_Pierce.ogg")
AudioManager_Register("Combat|Impact_Slash",       "AsSound", "AsSample", sSFXPath .. "Impact_Slash.ogg")
AudioManager_Register("Combat|Impact_Strike",      "AsSound", "AsSample", sSFXPath .. "Impact_Strike.ogg")
AudioManager_Register("Combat|Impact_Strike_Crit", "AsSound", "AsSample", sSFXPath .. "Impact_Strike_Crit.ogg")
AudioManager_Register("Combat|MonsterDie",         "AsSound", "AsSample", sSFXPath .. "MonsterDie.wav")
AudioManager_Register("World|TakeItem",            "AsSound", "AsSample", sSFXPath .. "TakeItem.wav")
