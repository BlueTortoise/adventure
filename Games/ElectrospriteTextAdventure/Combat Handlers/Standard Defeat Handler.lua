--[ ================================= Standard Defeat Handler =================================== ]
--Called when the player is defeated after combat under normal circumstances, that is, by a doll.
TL_SetProperty("Append", "After the final blow is struck, you crumple on the ground, defeated. You are far too hurt to move.")
gzTextVar.gzPlayer.bIsCrippled = true

--Player has 0 hp.
gzTextVar.gzPlayer.iHP = 0
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
