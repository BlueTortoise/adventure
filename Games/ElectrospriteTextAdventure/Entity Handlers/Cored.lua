--[ =========================================== Cored =========================================== ]
--All NPCs use this when implanted with a golem core.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)
local i = gzTextVar.iCurrentEntity

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look",   "look "   .. sEntityName)
    TL_SetProperty("Register Popup Command", "talk",   "talk "   .. sEntityName)
    return
end

--[ ========================================== Dialogue ========================================= ]
--Looking at.
if(sInstruction == "look " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "examine " .. gzTextVar.zEntities[i].sQueryName) then
    
    gbHandledInput = true
    TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
    TL_SetProperty("Append", "A typical human from Pandemonium, with a golem core attached to " .. gzTextVar.zEntities[i].sGenderA .. " chest. " .. gzTextVar.zEntities[i].sGenderCapitalB .. " has a distant look on " .. gzTextVar.zEntities[i].sGenderA .. " face as the core has taken over " .. gzTextVar.zEntities[i].sGenderA .. " muscular control. " ..  gzTextVar.zEntities[i].sGenderCapitalB .. " is obviously pleased with the state of " .. gzTextVar.zEntities[i].sGenderA .. " body.\n\n")
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Unregister Image")
    
--Attacking.
elseif(sInstruction == "attack " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    TL_SetProperty("Append", "The human has already been subdued and implanted with a core. She will be one of us soon.")
    TL_SetProperty("Create Blocker")

--Talking to.
elseif(sInstruction == "talk " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    fnDialogueCutscene(sMyName, sMyPortrait,  "'State designation and current function.'")
    fnDialogueCutscene(sTrName, sTrPortrait,  "'Seek conversion chamber. Enter conversion chamber. Be converted. Priority zero.'")
    fnDialogueCutscene(sMyName, sMyPortrait,  "You are pleased. The core has taken full control, and the human will be uplifted soon.")
    fnDialogueFinale()
    
--Transform command.
elseif(sInstruction == "transform " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    TL_SetProperty("Append", "The human has already been implanted with a golem core. She will be one of us soon.")
    TL_SetProperty("Create Blocker")
    
end