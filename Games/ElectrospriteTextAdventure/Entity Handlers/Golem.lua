--[ =========================================== Golem =========================================== ]
--All NPCs use this after transformation.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)
local i = gzTextVar.iCurrentEntity

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look",   "look "   .. sEntityName)
    TL_SetProperty("Register Popup Command", "talk",   "talk "   .. sEntityName)
    return
end

--[ ========================================== Dialogue ========================================= ]
--Looking at.
if(sInstruction == "look " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "examine " .. gzTextVar.zEntities[i].sQueryName) then
    
    gbHandledInput = true
    TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
    TL_SetProperty("Append", "A mechanical woman, uplifted from her former existence and reprogrammed to be useful to the Cause of Science. She will remain here until your expedition is complete, then return with you to Regulus to start her nice life there.\n\n")
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Unregister Image")
    
--Attacking.
elseif(sInstruction == "attack " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    TL_SetProperty("Append", "Your programming will not allow you to harm another golem.")
    TL_SetProperty("Create Blocker")

--Talking to.
elseif(sInstruction == "talk " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    fnDialogueCutscene(sMyName, sMyPortrait, "'State designation and current function.'")
    fnDialogueCutscene(sTrName, sTrPortrait, "\n" .. gzTextVar.zEntities[i].sDisplayName .. " states, 'Remain at headquarters and await completion of expedition.'")
    fnDialogueCutscene(sMyName, sMyPortrait, "\nYou can see no obvious defects in " .. gzTextVar.zEntities[i].sDisplayName .. "'s programming. However, as you turn to leave, she stops you.")
    fnDialogueCutscene(sTrName, sTrPortrait, "\n'And, thank you. I never could have imagined the perfection of synthetic life. I want to spread it to all corners of Pandemonium.'")
    fnDialogueCutscene(sMyName, sMyPortrait, "\nThis unit shows the correct programming. You have performed excellently by converting her.\n\n")
    fnDialogueFinale()
    
--Transform command.
elseif(sInstruction == "transform " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    TL_SetProperty("Append", "The subject is already a golem.")
    TL_SetProperty("Create Blocker")
    
end