--[ =========================================== Berry =========================================== ]
--Standard female human handler.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)
local i = gzTextVar.iCurrentEntity

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look",   "look "   .. sEntityName)
    TL_SetProperty("Register Popup Command", "talk",   "talk "   .. sEntityName)
    TL_SetProperty("Register Popup Command", "attack", "attack " .. sEntityName)
    if(gzTextVar.zEntities[i].bIsCrippled == true) then
        TL_SetProperty("Register Popup Command", "transform", "transform " .. sEntityName)
    end

    return
end

--[ ========================================== Dialogue ========================================= ]
--Looking at.
if(sInstruction == "look " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "examine " .. gzTextVar.zEntities[i].sQueryName) then
    
    gbHandledInput = true
    TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
    
    if(gzTextVar.zEntities[i].bIsCrippled == false) then
        TL_SetProperty("Append", "A typical human from Pandemonium. She is pretty and cognitively capable, but lacks education or philosophical wisdom. She is practically begging to be uplifted to synthetic perfection.\n\n")
        TL_SetProperty("Create Blocker")
    
    else
        TL_SetProperty("Append", "A typical human from Pandemonium. She is pretty and cognitively capable, but lacks education or philosophical wisdom. She is practically begging to be uplifted to synthetic perfection.")
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "\nShe is lying on the ground, too hurt to move. You should use the [transform] command on her.")
        TL_SetProperty("Create Blocker")
    
    end
    TL_SetProperty("Unregister Image")
    
--Attacking.
elseif(sInstruction == "attack " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
        
    --Run the target's AI with this flag set.
    if(gzTextVar.zEntities[i].bIsCrippled == false) then
        gzTextVar.bIsTriggeringFights = true
        gzTextVar.bTurnEndsWhenCombatEnds = true
        LM_ExecuteScript(gzTextVar.zEntities[i].sAIHandler, i)
        gzTextVar.bIsTriggeringFights = false
    else
        TL_SetProperty("Append", "The human is too injured to pose a threat. You should use the [transform] command on her instead.")
        TL_SetProperty("Create Blocker")
    end

--Talking to.
elseif(sInstruction == "talk " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.zEntities[i].bIsCrippled == false) then
        fnDialogueCutscene(sMyName, sMyPortrait,  "You smile and approach " .. sTrName .. ", attempting to appear as unthreatening as possible.")
        fnDialogueCutscene(sMyName, sMyPortrait,  "\n'Hello, human. Do not be afraid, I will improve you shortly.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "\n'Improve me? No way! I foolishly love my fleshy existence because I've never experienced anything else!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "\n'I'm going to obstinately resist you even if you have only my best interests in mind!'")
        fnDialogueCutscene(sMyName, sMyPortrait,  "\nYou are undeterred. This is typical amongst humans. You should use the [attack] command to subdue her.\n\n")
        fnDialogueFinale()
    
    else
        fnDialogueCutscene(sTrName, sTrPortrait,  "'I'm sorry!' the human says, laying wounded on the ground. 'You were right, I was being foolish!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "\n'Please don't hurt me, I was wrong to fight you.'")
        fnDialogueCutscene(sMyName, sMyPortrait,  "\n'Fear not, fleshling, all pain will cease soon,' you say, preparing to [transform] her.\n\n")
        fnDialogueFinale()
    end
    
--Transform command.
elseif(sInstruction == "transform " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.zEntities[i].bIsCrippled == false) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Get away from me, you monster!'")
        fnDialogueCutscene(sMyName, sMyPortrait,  "\nThe human is still far too motile. You should [attack] her to subdue her.\n\n")
        fnDialogueFinale()
    
    else
        fnDialogueCutscene(sTrName, sTrPortrait,  "You smile as you lean over the wounded form of " .. sTrName .. ". She looks up at your mechanical perfection with starry wonder.")
        fnDialogueCutscene(sTrName, gzTextVar.zEntities[i].sCoredPicture,  "\nYou implant a golem core on her chest, and you see her eyes go dim as the core begins taking over her muscular control. She is obviously happy to be relieved of the burden.")
        fnDialogueCutscene(sMyName, sMyPortrait,  "\nStanding up under the core's influence, she calmly begins moving towards your headquarters for conversion.\n\n")
        fnDialogueFinale()
        
        --Change the NPC's properties.
        gzTextVar.zEntities[i].sQueryPicture = gzTextVar.zEntities[i].sCoredPicture
        gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Cored.lua"
        gzTextVar.zEntities[i].sAIHandler = gzTextVar.sRootPath .. "AIs/CoreAI.lua"
    end
    
end