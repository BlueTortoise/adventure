--[ =========================================== Self ============================================ ]
--Handles self-examination.
TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, 0)
TL_SetProperty("Append", "You are Unit 771852. You are training to convert humans to serve the Cause of Science. You have above-average combat and cognitive skills.\n")
TL_SetProperty("Create Blocker")
TL_SetProperty("Append", "You can [attack] a human and defeat them in combat, then [transform] them to implant a golem core on their chest. They will then seek out a conversion chamber.\n\n")
TL_SetProperty("Create Blocker")
TL_SetProperty("Unregister Image")
    