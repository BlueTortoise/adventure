--[ ========================================= Boot Fonts ======================================== ]
--Boots fonts used by Electrosprite Adventure
if(gbBootedElectrospriteFonts == true) then return end
gbBootedElectrospriteFonts = true

--[Setup]
--Paths.
local sFontPath = "Data/"
local sKerningPath = "Data/Scripts/Fonts/"

--Special Flags
local ciFontNoFlags = 0
local ciFontNearest  = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge     = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS = Font_GetProperty("Constant Precache With Special S")

--[ ======================================= Font Registry ======================================= ]
--Segoe
Font_Register("Segoe 20",      sFontPath .. "segoeui.ttf",  sKerningPath .. "TextLevel_Kerning.lua", 20, ciFontNoFlags)
Font_Register("Segoe 30",      sFontPath .. "segoeui.ttf",  sKerningPath .. "TextLevel_Kerning.lua", 30, ciFontNoFlags)

--Sanchez
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 35 DFO", sFontPath .. "SanchezRegular.otf", sKerningPath .. "Sanchez30_Kerning.lua", 35, ciFontNearest + ciFontEdge + ciFontDownfade)

--Trigger 28
Font_SetProperty("Downfade", 0.99, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Trigger 28 DFO", sFontPath .. "TriggerModified.ttf", sKerningPath .. "Trigger28_Kerning.lua", 28, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Outline Width", 1)
Font_Register("Trigger 28 O", sFontPath .. "TriggerModified.ttf", sKerningPath .. "Trigger28_Kerning.lua", 28, ciFontNearest + ciFontEdge)

--[ ========================================== Aliases ========================================== ]
--Segoe 20
Font_SetProperty("Add Alias", "Segoe 20", "Text Level Medium")
Font_SetProperty("Add Alias", "Segoe 20", "Typing Combat Medium")

--Segoe 30
Font_SetProperty("Add Alias", "Segoe 30", "Text Level Large")

--Sanchez 35
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Typing Combat Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Text Lev Options Header")

--Trigger 28
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Typing Combat Command")

--Trigger 28 NDF
Font_SetProperty("Add Alias", "Trigger 28 O", "Text Lev Options Main")
