--[fnBuildLocalityInfo]
--Builds information for the locality pane based on the player's current position.
fnBuildLocalityInfo = function()
    
    --[Setup]
    --Clear the old data.
    TL_SetProperty("Clear Locality List")

    --Bases. These are always present.
    TL_SetProperty("Register Locality Entry As List", "Commands")
    TL_SetProperty("Register Locality Entry As List", "Inventory")
    TL_SetProperty("Register Locality Entry As List", "Entities")
    TL_SetProperty("Register Locality Entry As List", "Objects")
    
    --Current location.
    local sLocation = gzTextVar.gzPlayer.sLocation
    local iRoomIndex = fnGetRoomIndex(sLocation)
    
    --[Basic Commands]
    TL_SetProperty("Register Locality Entry", "look", "Commands", "INSTANT")
    TL_SetProperty("Register Locality Entry", "think", "Commands", "INSTANT")
    TL_SetProperty("Register Locality Entry", "wait", "Commands", "INSTANT")
    TL_SetProperty("Register Locality Entry", "move north", "Commands", "INSTANT")
    TL_SetProperty("Register Locality Entry", "move south", "Commands", "INSTANT")
    TL_SetProperty("Register Locality Entry", "move west", "Commands", "INSTANT")
    TL_SetProperty("Register Locality Entry", "move east", "Commands", "INSTANT")
    TL_SetProperty("Register Locality Entry", "move up", "Commands", "INSTANT")
    TL_SetProperty("Register Locality Entry", "move down", "Commands", "INSTANT")
    
    --[Entities]
    --Go through the list of entities in this room and add them.
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        if(gzTextVar.zEntities[i].sLocation == sLocation) then
            TL_SetProperty("Register Locality Entry", gzTextVar.zEntities[i].sDisplayName, "Entities", gzTextVar.zEntities[i].sCommandHandler)
        end
    end
    
    --[Objects]
    for i = 1, gzTextVar.zRoomList[iRoomIndex].iObjectsTotal, 1 do
        TL_SetProperty("Register Locality Entry", gzTextVar.zRoomList[iRoomIndex].zObjects[i].sDisplayName, "Objects", gzTextVar.zRoomList[iRoomIndex].zObjects[i].sHandlerScript)
    end
    
    --[Inventory Items]
    for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
        TL_SetProperty("Register Locality Entry", gzTextVar.gzPlayer.zaItems[i].sDisplayName, "Inventory", gzTextVar.gzPlayer.zaItems[i].sHandlerScript)
    end

    --[Finish Up]
    --Re-open opened locality categories from the previous state.
    TL_SetProperty("Resume Locality States")
    
end