--[fnDialogueCutscene]
--Standard simplified dialogue cutscene. Pass "Null" for sDisplayImg to use none. sDialogueB and sDialogueC are optional.
fnDialogueCutscene = function(sSpeakerName, sDisplayImg, sDialogueA, sDialogueB, sDialogueC)
    
    --Argument check.
    if(sDisplayImg == nil) then return end
    if(sDialogueA == nil) then return end
    
    --Clean previous scene.
    TL_SetProperty("Unregister Image")
    
    --Display image set.
    if(sDisplayImg ~= "Null") then
        TL_SetProperty("Register Image", sSpeakerName, sDisplayImg, 0)
    end
    
    --Dialogue additions.
    TL_SetProperty("Append", sDialogueA)
    if(sDialogueB ~= nil) then TL_SetProperty("Append", sDialogueB) end
    if(sDialogueC ~= nil) then TL_SetProperty("Append", sDialogueC) end
    
    --Ends the sequence.
    TL_SetProperty("Create Blocker")
end
fnDialogueFinale = function()
    TL_SetProperty("Unregister Image")
end