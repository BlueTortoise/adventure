fnGetRoomIndex = function(sRoomName)
   
    if(sRoomName == nil) then return -1 end
    
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        if(gzTextVar.zRoomList[i].sName == sRoomName) then
            return i
        end
    end
    
    return -1
end