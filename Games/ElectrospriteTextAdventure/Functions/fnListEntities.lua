--[fnListEntities]
--Lists all entities at a given location. If bSkipNoEntities is true, then "There are no entities" will not display.
fnListEntities = function(sLocation, bSkipNoEntities)
    
    --Argument check.
    if(sLocation == nil)       then return end
    if(bSkipNoEntities == nil) then return end

    --Count entities.
    local iCount = 0
    local saEntityNames = {}
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        if(gzTextVar.zEntities[i].sLocation == sLocation) then
            iCount = iCount + 1
            saEntityNames[iCount] = gzTextVar.zEntities[i].sDisplayName
        end
    end
    
    --No entities.
    if(iCount == 0) then
        if(bSkipNoEntities == false) then
            TL_SetProperty("Append", "There are no entities in the room with you.\n")
        end

    --One entity.
    elseif(iCount == 1) then
        TL_SetProperty("Append", "You can see [" .. saEntityNames[1] .. "] here.\n")
    
    --Two entities.
    elseif(iCount == 2) then
        TL_SetProperty("Append", "You can see [" .. saEntityNames[1] .. "] and [" .. saEntityNames[2] .. "] here.\n")
    
    --List.
    else
        --Setup.
        local sSentence = "You can see "
        
        --All members except the end.
        for i = 1, iCount-1, 1 do
            sSentence = sSentence .. "[" .. saEntityNames[i] .. "], "
        end
        
        --Add to the end.
        sSentence = sSentence .. "and [" .. saEntityNames[iCount] .. "] here.\n"
        
        --Append it.
        TL_SetProperty("Append", sSentence)
    
    end
end