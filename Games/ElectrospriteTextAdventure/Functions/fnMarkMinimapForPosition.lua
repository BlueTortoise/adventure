--[fnMarkMinimapForPosition]
--Marks the given location on the minimap as explored and visible, and marks all the rooms adjacent as explored and visible.
-- This is used to make the fog of war dynamically lift as the player explores.
fnMarkMinimapForPosition = function(sLocation)
    
    --Argument check.
    if(sLocation == nil) then return end
    
    --Get the index of the room.
    local iRoomIndex = fnGetRoomIndex(sLocation)
    if(iRoomIndex == -1) then return end
    
    --We need the coordinates of the room.
    local fX = gzTextVar.zRoomList[iRoomIndex].fRoomX
    local fY = gzTextVar.zRoomList[iRoomIndex].fRoomY
    local fZ = gzTextVar.zRoomList[iRoomIndex].fRoomZ
    
    --Mark this room as explored and visible.
    TL_SetProperty("Set Room Explored", fX, fY, fZ, true)
    TL_SetProperty("Set Room Visible", fX, fY, fZ, true)
    
    --Assemble the connections into an array.
    local iaArray = {}
    iaArray[1] = gzTextVar.zRoomList[iRoomIndex].iMoveN
    iaArray[2] = gzTextVar.zRoomList[iRoomIndex].iMoveE
    iaArray[3] = gzTextVar.zRoomList[iRoomIndex].iMoveS
    iaArray[4] = gzTextVar.zRoomList[iRoomIndex].iMoveW
    iaArray[5] = gzTextVar.zRoomList[iRoomIndex].iMoveU
    iaArray[6] = gzTextVar.zRoomList[iRoomIndex].iMoveD
    
    --Now use all the connections and mark them likewise. Start with north.
    for i = 1, 6, 1 do
        
        --Ignore it if it's a -1.
        if(iaArray[i] == -1) then
            
        --Otherwise, set explored/visible.
        else
            local iIndex = iaArray[i]
            fX = gzTextVar.zRoomList[iIndex].fRoomX
            fY = gzTextVar.zRoomList[iIndex].fRoomY
            fZ = gzTextVar.zRoomList[iIndex].fRoomZ
            TL_SetProperty("Set Room Explored", fX, fY, fZ, true)
            TL_SetProperty("Set Room Visible", fX, fY, fZ, true)
        end
    end
end

--Unmark. Does the same as above but unmarks the room and adjacent rooms. This is used before movement to recompute visibility.
-- It does not affect explored state, only visible.
fnUnmarkMinimapForPosition = function(sLocation)
    
    --Argument check.
    if(sLocation == nil) then return end
    
    --Get the index of the room.
    local iRoomIndex = fnGetRoomIndex(sLocation)
    if(iRoomIndex == -1) then return end
    
    --We need the coordinates of the room.
    local fX = gzTextVar.zRoomList[iRoomIndex].fRoomX
    local fY = gzTextVar.zRoomList[iRoomIndex].fRoomY
    local fZ = gzTextVar.zRoomList[iRoomIndex].fRoomZ
    
    --Mark this room as explored and visible.
    TL_SetProperty("Set Room Visible", fX, fY, fZ, false)
    
    --Assemble the connections into an array.
    local iaArray = {}
    iaArray[1] = gzTextVar.zRoomList[iRoomIndex].iMoveN
    iaArray[2] = gzTextVar.zRoomList[iRoomIndex].iMoveE
    iaArray[3] = gzTextVar.zRoomList[iRoomIndex].iMoveS
    iaArray[4] = gzTextVar.zRoomList[iRoomIndex].iMoveW
    iaArray[5] = gzTextVar.zRoomList[iRoomIndex].iMoveU
    iaArray[6] = gzTextVar.zRoomList[iRoomIndex].iMoveD
    
    --Now use all the connections and mark them likewise. Start with north.
    for i = 1, 6, 1 do
        
        --Ignore it if it's a -1.
        if(iaArray[i] == -1) then
            
        --Otherwise, set explored/visible.
        else
            local iIndex = iaArray[i]
            fX = gzTextVar.zRoomList[iIndex].fRoomX
            fY = gzTextVar.zRoomList[iIndex].fRoomY
            fZ = gzTextVar.zRoomList[iIndex].fRoomZ
            TL_SetProperty("Set Room Visible", fX, fY, fZ, false)
        end
    end
end