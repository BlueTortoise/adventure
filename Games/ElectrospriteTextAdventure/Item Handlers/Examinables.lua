--[ ======================================== Examinables ======================================== ]
--This script is used by all examinable objects. This is for objects which can only ever be examined
-- and nothing else.
--Because these items can't be picked up, they should nominally never be in the player's inventory
-- but we often don't check for that.

--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    return
end

--[ ======================================= Verification ======================================== ]
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

--[ ========================================== Handling ========================================= ]
--Run across the object listing and find the remap name.
local sRemapTo = "Null"
if(iRoomSlot == -1) then
    sRemapTo = gzTextVar.gzPlayer.zaItems[iItemSlot].sUniqueName
else
    sRemapTo = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sUniqueName
end

--No remap found.
if(sRemapTo == "Null") then
    TL_SetProperty("Append", "There is no object here like that. Types 'objects' to see a list of objects nearby.\n\n")
    return
end

--Check to see if this is "look" or "examine"
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --Description listing.
    if(sRemapTo == "conversionchamber") then
        gbHandledInput = true
        TL_SetProperty("Append", "The conversion chamber, which will uplift the primitive inhabitants of Pandemonium from their squalor. It is currently empty.\n\n")
        return
    end

--Take commands always fail.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then
    gbHandledInput = true
    TL_SetProperty("Append", "You cannot take that with you.\n\n")

end