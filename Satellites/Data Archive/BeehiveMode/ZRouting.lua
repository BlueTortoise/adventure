--[Beehive Mode]
--Images used in the beehive management game.
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/BeehiveMode.slf")
ImageLump_SetCompression(1)

--[Functions]
--Standard Sheet Rip
local fnRipSheet = function(sBaseName, sImagePath, bIsEightDir)
	
	--Arg Check
	if(sBaseName == nil) then return end
	if(sImagePath == nil) then return end
	if(bIsEightDir == nil) then return end
	
	--Constants
	local cfStartX = 0
	local cfStartY = 40
	local cfSizeX = 32
	local cfSizeY = 40
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      4,       4,      4,      4}
	if(bIsEightDir) then
		saSets =   {"South", "North", "West", "East", "NE", "NW", "SW", "SE"}
		iaFrames = {      4,       4,      4,      4,    4,    4,    4,    4}
	end
	
	--Rip
	local i = 1
	while(saSets[i] ~= nil) do
		
		--Iterate.
		for p = 1, iaFrames[i], 1 do
			
			--Name.
			local sUseName = sBaseName .. "|" .. saSets[i] .. "|" .. p
			
			--Position.
			local fRipX = cfStartX + (cfSizeX * (p-1))
			local fRipY = cfStartY + (cfSizeY * (i-1))
			
			--Rip.
			ImageLump_Rip(sUseName, sImagePath, fRipX, fRipY, cfSizeX, cfSizeY, 0)
			
		end
	
		--Next.
		i = i + 1
	end
end

--[Tiles]
--Tiles used for the world map.
ImageLump_Rip("Tiles|Tileset", sBasePath .. "LargerTilemap.png", 0, 0, -1, -1, 0)

--Entity Tiles.
ImageLump_Rip("Entities|Flower",  sBasePath .. "Entities.png", 0, 32, 20, 16, 0)

--[UI Parts]
ImageLump_Rip("UIOver|Swords",  sBasePath .. "CrossingSwords.png", 0, 0, -1, -1, 0)

--[Humans]
--Soldier Female:
fnRipSheet("Human_Merc_Female", sBasePath .. "Human_Merc_Female.png", false)

--[Drones]
--Worker:
fnRipSheet("Drone_Worker", sBasePath .. "Drone_Worker.png", false)
ImageLump_Rip("Drone_Worker|Outline",    sBasePath .. "Drone_Worker.png", 32,   0, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Shadow",     sBasePath .. "Drone_Worker.png", 64,   0, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Cultivate0", sBasePath .. "Drone_Worker.png",  0, 200, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Cultivate1", sBasePath .. "Drone_Worker.png", 32, 200, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Cultivate2", sBasePath .. "Drone_Worker.png", 64, 200, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Mine0",      sBasePath .. "Drone_Worker.png",  0, 240, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Mine1",      sBasePath .. "Drone_Worker.png", 32, 240, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Mine2",      sBasePath .. "Drone_Worker.png", 64, 240, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Gather0",    sBasePath .. "Drone_Worker.png",  0, 280, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Gather1",    sBasePath .. "Drone_Worker.png", 32, 280, 32, 40, 0)
ImageLump_Rip("Drone_Worker|Fish0",      sBasePath .. "Drone_Worker.png",  0, 320, 32, 40, 0)

--Beehive. Same format as the beegirl but only one frame.
ImageLump_Rip("Entities|Hive",    sBasePath .. "Beehive.png", 0, 0, -1, -1, 0)

--[Volunteer Transformation]
ImageLump_Rip("Volunteer|TF0", sBasePath .. "VolunteerTF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Volunteer|TF1", sBasePath .. "VolunteerTF1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Volunteer|TF2", sBasePath .. "VolunteerTF2.png", 0, 0, -1, -1, 0)


--[Finish]
SLF_Close()