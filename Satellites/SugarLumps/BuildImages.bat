SugarLumps.exe -Redirect "BuildImagesLog.txt" -Call BuildImages.lua
cd Output/
if exist "AdventureScenes.slf" (
	copy "AdventureScenes.slf" "../../../Games/AdventureMode/Datafiles/AdventureScenes.slf"
	del "AdventureScenes.slf"
	echo "Copying AdventureScenes.slf"
)
if exist "AdvCombat.slf" (
	copy "AdvCombat.slf" "../../../Games/AdventureMode/Datafiles/AdvCombat.slf"
	del "AdvCombat.slf"
	echo "Copying AdvCombat.slf"
)

::Adventure Mode UI
if exist "UIAdvIcons.slf" (
	copy "UIAdvIcons.slf" "../../../Games/AdventureMode/Datafiles/UIAdvIcons.slf"
	del "UIAdvIcons.slf"
	echo "Copying UIAdvIcons.slf"
)
if exist "UIAdvMenuBase.slf" (
	copy "UIAdvMenuBase.slf" "../../../Games/AdventureMode/Datafiles/UIAdvMenuBase.slf"
	del "UIAdvMenuBase.slf"
	echo "Copying UIAdvMenuBase.slf"
)
if exist "UIAdvMenuSkills.slf" (
	copy "UIAdvMenuSkills.slf" "../../../Games/AdventureMode/Datafiles/UIAdvMenuSkills.slf"
	del "UIAdvMenuSkills.slf"
	echo "Copying UIAdvMenuSkills.slf"
)
if exist "UIAdvMenuFieldAbilities.slf" (
	copy "UIAdvMenuFieldAbilities.slf" "../../../Games/AdventureMode/Datafiles/UIAdvMenuFieldAbilities.slf"
	del "UIAdvMenuFieldAbilities.slf"
	echo "Copying UIAdvMenuFieldAbilities.slf"
)
if exist "AdventureScenesLoDef.slf" (
	copy "AdventureScenesLoDef.slf" "../../../Games/AdventureMode/Datafiles/AdventureScenesLoDef.slf"
	del "AdventureScenesLoDef.slf"
	echo "Copying AdventureScenesLoDef.slf"
)
if exist "CombatAnimations.slf" (
	copy "CombatAnimations.slf" "../../../Games/AdventureMode/Datafiles/CombatAnimations.slf"
	del "CombatAnimations.slf"
	echo "Copying CombatAnimations.slf"
)
if exist "ElectrospriteAdventure.slf" (
	copy "ElectrospriteAdventure.slf" "../../../Games/AdventureMode/Datafiles/ElectrospriteAdventure.slf"
	copy "ElectrospriteAdventure.slf" "../../../Games/ElectrospriteTextAdventure/Datafiles/ElectrospriteAdventure.slf"
	del "ElectrospriteAdventure.slf"
	echo "Copying ElectrospriteAdventure.slf"
)
if exist "MapAnimations.slf" (
	copy "MapAnimations.slf" "../../../Games/AdventureMode/Datafiles/MapAnimations.slf"
	del "MapAnimations.slf" 
	echo "Copying MapAnimations.slf"
)
if exist "OverheadMaps.slf" (
	copy "OverheadMaps.slf" "../../../Games/AdventureMode/Datafiles/OverheadMaps.slf"
	del "OverheadMaps.slf"
	echo "Copying OverheadMaps.slf"
)
if exist "OverheadMapsLoDef.slf" (
	copy "OverheadMapsLoDef.slf" "../../../Games/AdventureMode/Datafiles/OverheadMapsLoDef.slf"
	del "OverheadMapsLoDef.slf"
	echo "Copying OverheadMapsLoDef.slf"
)

::Portrait Files
if exist "Portraits.slf" (
	copy "Portraits.slf" "../../../Games/AdventureMode/Datafiles/Portraits.slf"
	del "Portraits.slf" 
	echo "Copying Portraits.slf"
)
if exist "Portraits_Florentina.slf" (
	copy "Portraits_Florentina.slf" "../../../Games/AdventureMode/Datafiles/Portraits_Florentina.slf"
	del "Portraits_Florentina.slf" 
	echo "Copying Portraits_Florentina.slf"
)

::Low-Definition Portrait Files
if exist "PortraitsLoDef.slf" (
	copy "PortraitsLoDef.slf" "../../../Games/AdventureMode/Datafiles/PortraitsLoDef.slf"
	del "PortraitsLoDef.slf" 
	echo "Copying PortraitsLoDef.slf"
)
if exist "PortraitsLD_Florentina.slf" (
	copy "PortraitsLD_Florentina.slf" "../../../Games/AdventureMode/Datafiles/PortraitsLD_Florentina.slf"
	del "PortraitsLD_Florentina.slf" 
	echo "Copying PortraitsLD_Florentina.slf"
)


if exist "Sprites.slf" (
	copy "Sprites.slf" "../../../Data/Sprites.slf"
	copy "Sprites.slf" "../../../Games/AdventureMode/Datafiles/Sprites.slf"
	del "Sprites.slf" 
	echo "Copying Sprites.slf"
)
if exist "UIAdventure.slf" (
	copy "UIAdventure.slf" "../../../Games/AdventureMode/Datafiles/UIAdventure.slf"
	del "UIAdventure.slf" 
	echo "Copying UIAdventure.slf"
)
if exist "UIControls.slf" (
	copy "UIControls.slf" "../../../Games/AdventureMode/Datafiles/UIControls.slf"
	del "UIControls.slf" 
	echo "Copying UIControls.slf"
)
if exist "UITextAdventure.slf" (
	copy "UITextAdventure.slf" "../../../Games/AdventureMode/Datafiles/UITextAdventure.slf"
	copy "UITextAdventure.slf" "../../../Games/DollManor/Datafiles/UITextAdventure.slf"
	copy "UITextAdventure.slf" "../../../Games/ElectrospriteTextAdventure/Datafiles/UITextAdventure.slf"
	del "UITextAdventure.slf" 
	echo "Copying UITextAdventure.slf"
)