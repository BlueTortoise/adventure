--[ =================================== Chapter 0 Scene Images ================================== ]
--Images used for no particular chapter.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_CH0Major.slf")
else
	SLF_Open("Output/AdvScnLD_CH0Major.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Ripping ========================================== ]
--Controls information.
ImageLump_Rip("Controls", sBasePath .. "Controls.png", 0, 0, -1, -1, 0)

--Credits sequence(s).
ImageLump_Rip("CreditsBacking5", sBasePath .. "CreditsBacking5.png", 0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
