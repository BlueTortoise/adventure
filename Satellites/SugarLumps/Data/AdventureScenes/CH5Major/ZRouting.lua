--[ =================================== Chapter 5 Scene Images ================================== ]
--Images used for chapter 5, but not part of a TF sequence.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_CH5Major.slf")
else
	SLF_Open("Output/AdvScnLD_CH5Major.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Ripping ========================================== ]
--Finale
ImageLump_Rip("Major|Chapter5Finale", sBasePath .. "Chapter5Finale.png", 0, 0, -1, -1, gciNoTransparencies)

--Sophie with a whip!
ImageLump_Rip("Major|SophieLeather", sBasePath .. "SophieLeather.png", 0, 0, -1, -1, 0)

--Reflections in the window
ImageLump_Rip("Window|Alone",    sBasePath .. "Chapter5WindowAloneHD.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Window|Together", sBasePath .. "Chapter5WindowTogetherHD.png", 0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
