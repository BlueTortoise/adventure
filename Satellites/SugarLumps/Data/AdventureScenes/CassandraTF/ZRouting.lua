--[ ================================= Cassandra Transformations ================================= ]
--This mysterious girl keeps appearing in different chapters. What is going on?
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_CassandraTF.slf")
else
	SLF_Open("Output/AdvScnLD_CassandraTF.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Ripping ========================================== ]
--Cassandra's Default
ImageLump_Rip("Cassandra|Neutral",    sBasePath .. "Cassandra_Human.png",    0, 0, -1, -1, 0)

--Sequence to Werecat
ImageLump_Rip("Cassandra|WerecatTF0", sBasePath .. "Cassandra_Werecat0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Cassandra|WerecatTF1", sBasePath .. "Cassandra_Werecat1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Cassandra|Werecat",    sBasePath .. "Cassandra_Werecat.png",  0, 0, -1, -1, 0)

--Sequence to Golem
ImageLump_Rip("Cassandra|GolemTF0", sBasePath .. "Cassandra_Golem0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Cassandra|GolemTF1", sBasePath .. "Cassandra_Golem1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Cassandra|GolemTF2", sBasePath .. "Cassandra_Golem2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Cassandra|GolemTF3", sBasePath .. "Cassandra_Golem3.png", 0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
