--[ ==================================== Christine TF Images ==================================== ]
--Scene images for Christine's many transformations.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_ChristineTF.slf")
else
	SLF_Open("Output/AdvScnLD_ChristineTF.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Ripping ========================================== ]
--Neutral poses.
ImageLump_Rip("Christine|Male",   sBasePath .. "Christine_Male.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Christine|Female", sBasePath .. "Christine_Female.png", 0, 0, -1, -1, 0)

--Christine to Doll. Six parts.
ImageLump_Rip("Christine|DollTF0", sBasePath .. "Christine_Doll_TF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|DollTF1", sBasePath .. "Christine_Doll_TF1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|DollTF2", sBasePath .. "Christine_Doll_TF2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|DollTF3", sBasePath .. "Christine_Doll_TF3.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|DollTF4", sBasePath .. "Christine_Doll_TF4.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|DollTF5", sBasePath .. "Christine_Doll_TF5.png", 0, 0, -1, -1, 0)

--Chris to Christine to Golem. Six parts.
ImageLump_Rip("Christine|GolemTF0", sBasePath .. "Christine_Golem_TF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|GolemTF1", sBasePath .. "Christine_Golem_TF1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|GolemTF2", sBasePath .. "Christine_Golem_TF2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|GolemTF3", sBasePath .. "Christine_Golem_TF3.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|GolemTF4", sBasePath .. "Christine_Golem_TF4.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|GolemTF5", sBasePath .. "Christine_Golem_TF5.png", 0, 0, -1, -1, 0)

--Christine to Latex Drone.
ImageLump_Rip("Christine|LatexTF0", sBasePath .. "Christine_Latex_TF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|LatexTF1", sBasePath .. "Christine_Latex_TF1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|LatexTF2", sBasePath .. "Christine_Latex_TF2.png", 0, 0, -1, -1, 0)

--Christine to Raiju
ImageLump_Rip("Christine|RaijuTF0", sBasePath .. "Christine_RaijuTF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|RaijuTF1", sBasePath .. "Christine_RaijuTF1.png", 0, 0, -1, -1, 0)

--Christine to Steam Droid
ImageLump_Rip("Christine|SteamTF0", sBasePath .. "Christine_SteamDroidTF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|SteamTF1", sBasePath .. "Christine_SteamDroidTF1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|SteamTF2", sBasePath .. "Christine_SteamDroidTF2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|SteamTF3", sBasePath .. "Christine_SteamDroidTF3.png", 0, 0, -1, -1, 0)

--Christine to Eldritch Dreamer
ImageLump_Rip("Christine|DreamerTF0", sBasePath .. "Christine_EDG_TF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|DreamerTF1", sBasePath .. "Christine_EDG_TF1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|DreamerTF2", sBasePath .. "Christine_EDG_TF2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Christine|DreamerTF3", sBasePath .. "Christine_EDG_TF3.png", 0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
