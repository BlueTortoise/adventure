--[ ======================================= Mei TF Images ======================================= ]
--Scene images for Mei getting turned into various monstergirls.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_MeiTF.slf")
else
	SLF_Open("Output/AdvScnLD_MeiTF.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Ripping ========================================== ]
--Alraune Sequence
ImageLump_Rip("Mei|AlrauneTF0", sBasePath .. "Mei_Alraune_TF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|AlrauneTF1", sBasePath .. "Mei_Alraune_TF1.png", 0, 0, -1, -1, 0)

--Bee Sequence
ImageLump_Rip("Mei|BeeTF0", sBasePath .. "Mei_Bee_TF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|BeeTF1", sBasePath .. "Mei_Bee_TF1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|BeeTF2", sBasePath .. "Mei_Bee_TF2.png", 0, 0, -1, -1, 0)

--Ghost Sequence
ImageLump_Rip("Mei|GhostTF0", sBasePath .. "Mei_Ghost_TF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|GhostTF1", sBasePath .. "Mei_Ghost_TF1.png", 0, 0, -1, -1, 0)

--Slime Sequence
ImageLump_Rip("Mei|SlimeTF0", sBasePath .. "Mei_Slime_TF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|SlimeTF1", sBasePath .. "Mei_Slime_TF1.png", 0, 0, -1, -1, 0)

--Werecat Sequence
ImageLump_Rip("Mei|WerecatTF0", sBasePath .. "Mei_Werecat_TF0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|WerecatTF1", sBasePath .. "Mei_Werecat_TF1.png", 0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
