--[ ====================================== Mei Rune Images ====================================== ]
--Bravery rune. 60 frames.
local sBasePath = fnResolvePath()
ImageLump_SetBlockTrimmingFlag(true)

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_MeiRune.slf")
else
	SLF_Open("Output/AdvScnLD_MeiRune.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[Iterate]
for i = 1, 60, 1 do
	local sName = string.format("Mei|Rune%02i", i-1)
	local sPath = string.format("%srune_bravery%02i.png", sBasePath, i)
	ImageLump_Rip(sName, sPath, 0, 0, -1, -1, 0)
end

--[Clean]
ImageLump_SetBlockTrimmingFlag(false)
SLF_Close()