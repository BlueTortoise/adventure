--[ ===================================== Combat Animations ===================================== ]
--These are the animations that play over the character portraits in combat. There are variations
-- for all the major element types, as well as miscellaneous cases like healing, buffs, debuff removal,
-- and so on. They come in 4x4 grids of fixed sizes.
SLF_Open("Output/CombatAnimations.slf")

--[Execution]
--Rip the animations.
local sBasePath = fnResolvePath()

--[Image Ripping Series]
local iEntries = 0
local zaData = {}
local fnAddData = function(psFolderName, piCount, psImgPattern)
    iEntries = iEntries + 1
    zaData[iEntries] = {}
    zaData[iEntries].sFolderName = psFolderName
    zaData[iEntries].iCount      = piCount
    zaData[iEntries].sImgPattern = psImgPattern
end

--Data.
fnAddData("Bleed",       19, "bleeding")
fnAddData("Blind",       10, "blind")
fnAddData("Buff",        24, "buffa-color")
fnAddData("Corrode",     41, "corrosion")
fnAddData("Debuff",      24, "debuffa-color")
fnAddData("Electricity", 24, "electricitya-color")
fnAddData("Fear",        30, "fear_00")
fnAddData("Fire",        40, "fire")
fnAddData("GunShot",     20, "gunshot")
fnAddData("Haste",       50, "haste")
fnAddData("Healing",     24, "healinga-color")
fnAddData("Ice",         40, "ice")
fnAddData("LaserShot",   20, "lasershot")
fnAddData("Light",       30, "light")
fnAddData("Pierce",      24, "piercinga-color")
fnAddData("Poison",      50, "poison")
fnAddData("ShadowA",     40, "shadow_a")
fnAddData("ShadowB",     50, "shadow_b")
fnAddData("ShadowC",     38, "shadow_c")
fnAddData("Slash1",      24, "slash1a-color")
fnAddData("Slash2",      24, "slash2a-color")
fnAddData("Slow",        70, "slow")
fnAddData("Strike",      10, "strike2b-color")

--For each folder:
for i = 1, iEntries, 1 do
    
	for p = 1, zaData[i].iCount, 1 do
    
		local sName = string.format("ComFX|%s%02i", zaData[i].sFolderName, p-1)
		local sPath = sBasePath .. zaData[i].sFolderName .. "/" .. string.format("%s%02i", zaData[i].sImgPattern, p) .. ".png"
        --io.write("Name: " .. sName .. " " .. sPath .. "\n")
		ImageLump_Rip(sName, sPath, 0, 0, -1, -1, 0)
    end
end

--[ ========================================== Clean Up ========================================= ]
--Finish
SLF_Close()
