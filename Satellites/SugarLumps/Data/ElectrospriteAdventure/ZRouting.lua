--[Electrosprite Adventure]
--Stuff used only by the Electrosprite Text Adventure.
local sBasePath = fnResolvePath()

--Open.
SLF_Open("Output/ElectrospriteAdventure.slf")

--Player sprites.
ImageLump_Rip("Player",  sBasePath .. "Player.png",    0, 0, -1, -1, 0)

--Five NPCs, from A to E. All have the same sequences.
local saArray = {"A", "B", "C", "D", "E"}
for i = 1, 5, 1 do
    ImageLump_Rip("NPC" ..  saArray[i] .. "Cored",  sBasePath .. "NPC " ..  saArray[i] .. " Cored.png",  0, 0, -1, -1, 0)
    ImageLump_Rip("NPC" ..  saArray[i] .. "Golem",  sBasePath .. "NPC " ..  saArray[i] .. " Golem.png",  0, 0, -1, -1, 0)
    ImageLump_Rip("NPC" ..  saArray[i] .. "Normal", sBasePath .. "NPC " ..  saArray[i] .. " Normal.png", 0, 0, -1, -1, 0)
    ImageLump_Rip("NPC" ..  saArray[i] .. "TF0",    sBasePath .. "NPC " ..  saArray[i] .. " TF0.png",    0, 0, -1, -1, 0)
    ImageLump_Rip("NPC" ..  saArray[i] .. "TF1",    sBasePath .. "NPC " ..  saArray[i] .. " TF1.png",    0, 0, -1, -1, 0)
    ImageLump_Rip("NPC" ..  saArray[i] .. "TF2",    sBasePath .. "NPC " ..  saArray[i] .. " TF2.png",    0, 0, -1, -1, 0)
end

--Finish
SLF_Close()
