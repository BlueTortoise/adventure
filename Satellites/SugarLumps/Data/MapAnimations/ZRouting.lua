--[Animations]
--Temporary animations used by specific maps. They get unloaded when the map unloads.
local sBasePath = fnResolvePath()

--Setup
SLF_Open("Output/MapAnimations.slf")
ImageLump_SetCompression(1)

--[Inspection]
--Occurs between Latex Christine and Sophie in the repair bay. Each sheet has 20 frames.
local iExpected = {20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20}
local iFrameCounter = 0
for i = 1, 11, 1 do
    
    --Iteration of internal frames.
    for p = 1, iExpected[i], 1 do
        ImageLump_Rip(string.format("Inspection|%03i", iFrameCounter), sBasePath .. "Inspection/Inspection" .. (i-1) .. ".png", (p-1) * 351, 0, 351, -1, 0)
        iFrameCounter = iFrameCounter + 1
    end
end

--[Christine to Golem]
--Human Christine becomes Golem Christine.
iExpected = {20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 40}
iFrameCounter = 0 
for i = 1, 12, 1 do
    
    local sCharacter = (i - 1)
    if(i == 11) then sCharacter = "A" end
    if(i == 12) then sCharacter = "B" end
    
    --Iteration of internal frames.
    for p = 1, iExpected[i], 1 do
        ImageLump_Rip(string.format("ChristineGolem|%03i", iFrameCounter), sBasePath .. "ChristineGolem/ChristineGolem" .. sCharacter .. ".png", (p-1) * 348, 0, 348, -1, 0)
        iFrameCounter = iFrameCounter + 1
    end
end

--Finish
SLF_Close()