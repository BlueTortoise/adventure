--[Maps]
--Used in 2D mode to bring a bit of life to the proceedings. Also used in Adventure Mode.
local sBasePath = fnResolvePath()

--Lo-def mode.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/OverheadMaps.slf")
else
	SLF_Open("Output/OverheadMapsLoDef.slf")
end

--Setup
ImageLump_SetCompression(1)

--Adventure Mode.
if(gbUseLowDefinition == true) then
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end
ImageLump_Rip("TrannadarMap",    sBasePath .. "TrannadarMap.png",    0, 0, -1, -1, 0)
ImageLump_Rip("RegulusMap",      sBasePath .. "RegulusMap.png",      0, 0, -1, -1, 0)
ImageLump_Rip("NixNedarBacking", sBasePath .. "NixNedarBacking.png", 0, 0, -1, -1, 0)
ImageLump_Rip("BiolabsMap",      sBasePath .. "BiolabsMap.png",      0, 0, -1, -1, 0)

--Overlays
ImageLump_Rip("TrannadarMapOverlay", sBasePath .. "TrannadarMapOverlay.png", 0, 0, -1, -1, 0)
ImageLump_Rip("RegulusMapOverlay",   sBasePath .. "RegulusMapOverlay.png",   0, 0, -1, -1, 0)

ImageLump_SetScales(1.0, 1.0, 0, 0)

--Layered PDU Maps
for i = 0, 6, 1 do
    ImageLump_Rip("CryolabLower" .. i, sBasePath .. "CryolabLower/Map"..i..".png", 0, 0, -1, -1, 0)
    ImageLump_Rip("CryolabMain"  .. i, sBasePath .. "CryolabMain/Map"..i..".png",  0, 0, -1, -1, 0)
    ImageLump_Rip("Equinox"      .. i, sBasePath .. "Equinox/Map"..i..".png",      0, 0, -1, -1, 0)
    ImageLump_Rip("LRTEast"      .. i, sBasePath .. "LRTEast/Map"..i..".png",      0, 0, -1, -1, 0)
    ImageLump_Rip("LRTWest"      .. i, sBasePath .. "LRTWest/Map"..i..".png",      0, 0, -1, -1, 0)
end

--[CRT Noise]
for i = 0, 9, 1 do
    ImageLump_Rip("Overlay|CRT" .. i,  sBasePath .. "Noise/Noise" .. i .. ".png", 0, 0, -1, -1, 0)
end

--Finish
SLF_Close()

--[Clean]
ImageLump_SetScales(1.0, 1.0, 0, 0)