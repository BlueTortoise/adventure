--[ ======================================== 55 Portraits ======================================= ]
--Subscript for 55's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_55.slf")
else
	SLF_Open("Output/PortraitsLD_55.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for 55's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Down",     sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Blush",    sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Smirk",    sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Upset",    sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Smug",     sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Offended", sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Broken",   sPath, gciHDX3, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Cry",      sPath, gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Angry",    sPath, gciHDX2, gciHDY2, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("55", "",         sBasePath .. "55_Doll_Emote.png")
fnAutoRip("55", "Sundress", sBasePath .. "55_Sundress_Emote.png")

--[Reverse]
--55 has a distinct eye pattern that needs to be specially reversed.
fnAutoRip("55", "Rev",         sBasePath .. "55_Doll_EmoteRev.png")
fnAutoRip("55", "SundressRev", sBasePath .. "55_Sundress_EmoteRev.png")

--[Special Frames]
--None yet.

--[ ========================================== Combat =========================================== ]
--This is just a simple list of combat frames.
ImageLump_Rip("Party|55", sBasePath .. "55_Doll_Combat.png", 0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
