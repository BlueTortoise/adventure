--[ ===================================== Aquillia Portraits ==================================== ]
--Subscript for Aquillia's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Aquillia.slf")
else
	SLF_Open("Output/PortraitsLD_Aquillia.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for Aquillia's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("Aquillia", "", sBasePath .. "Aquillia_Human_Emote.png")

--[Special Frames]
--None yet.

--[ ========================================== Combat =========================================== ]
--This is just a simple list of combat frames.

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
