--[ ============================= Chapter 0 Enemy Combat Portraits ============================== ]
--Combat portraits for enemies not restricted or predominant in one single chapter.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH0Combat.slf")
else
	SLF_Open("Output/PortraitsLD_CH0Combat.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Combat =========================================== ]
--Normal Enemies
ImageLump_Rip("Enemy|BandageGoblin", sBasePath .. "Enemy_BandageGoblin.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|BestFriend",    sBasePath .. "Enemy_BestFriend.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Hoodie",        sBasePath .. "Enemy_Hoodie.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Horrible",      sBasePath .. "Enemy_Horrible.png",      0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|InnGeisha",     sBasePath .. "Enemy_InnGeisha.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|MirrorImage",   sBasePath .. "Enemy_MirrorImage.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|SkullCrawler",  sBasePath .. "Enemy_SkullCrawler.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|VoidRift",      sBasePath .. "Enemy_VoidRift.png",      0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
