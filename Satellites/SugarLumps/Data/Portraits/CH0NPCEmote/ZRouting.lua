--[ ======================================= NPC Portraits ======================================= ]
--Subscript for NPCs portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH0Emote.slf")
else
	SLF_Open("Output/PortraitsLD_CH0Emote.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--Grid NPCs
ImageLump_Rip("Por|CrowbarChan|Neutral", sBasePath .. "Chapter0_NPC_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|MercF|Neutral",       sBasePath .. "Chapter0_NPC_Emote.png", gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|MercM|Neutral",       sBasePath .. "Chapter0_NPC_Emote.png", gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|HumanNPCF0|Neutral",  sBasePath .. "Chapter0_NPC_Emote.png", gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|HumanNPCF1|Neutral",  sBasePath .. "Chapter0_NPC_Emote.png", gciHDX3, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|HumanNPCF0B|Neutral", sBasePath .. "Chapter0_NPC_Emote.png", gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|HumanNPCF1B|Neutral", sBasePath .. "Chapter0_NPC_Emote.png", gciHDX1, gciHDY2, gciWid0, gciHei0, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
