--[ ============================= Chapter 1 Enemy Combat Portraits ============================== ]
--Combat portraits for chapter 1's enemies.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH1Combat.slf")
else
	SLF_Open("Output/PortraitsLD_CH1Combat.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Combat =========================================== ]
--Normal Enemies
ImageLump_Rip("Enemy|Alraune",   sBasePath .. "Enemy_Alraune.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|BeeGirl",   sBasePath .. "Enemy_BeeGirl.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|CultistF",  sBasePath .. "Enemy_CultistF.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|CultistM",  sBasePath .. "Enemy_CultistM.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|MaidGhost", sBasePath .. "Enemy_MaidGhost.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Slime",     sBasePath .. "Enemy_Slime.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Werecat",   sBasePath .. "Enemy_Werecat.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Zombee",    sBasePath .. "Enemy_Zombee.png",    0, 0, -1, -1, 0)

--Bosses
ImageLump_Rip("Enemy|Arachnophelia", sBasePath .. "Boss_Arachnophelia.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Infirm",        sBasePath .. "Boss_Infirm.png",        0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
