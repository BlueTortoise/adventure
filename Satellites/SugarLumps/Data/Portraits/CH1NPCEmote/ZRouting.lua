--[ ======================================= NPC Portraits ======================================= ]
--Subscript for NPCs portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH1Emote.slf")
else
	SLF_Open("Output/PortraitsLD_CH1Emote.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--Grid Enemies/NPCs
ImageLump_Rip("Por|Alraune|Neutral",  sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|CultistF|Neutral", sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|CultistM|Neutral", sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Slime|Neutral",    sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|SlimeG|Neutral",   sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|SlimeB|Neutral",   sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Adina|Neutral",    sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Nadia|Neutral",    sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Rochea|Neutral",   sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Blythe|Neutral",   sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY3, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Breanne|Neutral",  sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY3, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Claudia|Neutral",  sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY3, gciWid0, gciHei0, 0)

--Wide-grid enemies
ImageLump_Rip("Por|Bee|Neutral",       sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX0, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|MaidGhost|Neutral", sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX1, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|Werecat|Neutral",   sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX2, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|Zombee|Neutral",    sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX3, gciWideY0, gciWideWid, gciWideHei, 0)

--Rubber NPC, Normal Grid
ImageLump_Rip("Por|CultistFRubber|Neutral", sBasePath .. "Chapter1_Rubber_Emote.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
