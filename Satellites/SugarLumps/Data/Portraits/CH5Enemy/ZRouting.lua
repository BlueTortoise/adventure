--[ ============================= Chapter 5 Enemy Combat Portraits ============================== ]
--Combat portraits for chapter 5's enemies.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH5Combat.slf")
else
	SLF_Open("Output/PortraitsLD_CH5Combat.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Combat =========================================== ]
--Normal Enemies
ImageLump_Rip("Enemy|DarkmatterGirl", sBasePath .. "Enemy_DarkmatterGirl.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Doll",           sBasePath .. "Enemy_Doll.png",           0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Dreamer",        sBasePath .. "Enemy_Dreamer.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Electrosprite",  sBasePath .. "Enemy_Electrosprite.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|GolemLord",      sBasePath .. "Enemy_GolemLord.png",      0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|GolemSlave",     sBasePath .. "Enemy_GolemSlave.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|LatexDrone",     sBasePath .. "Enemy_LatexDrone.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Raibie",         sBasePath .. "Enemy_Raibie.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Raiju",          sBasePath .. "Enemy_Raiju.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Scraprat",       sBasePath .. "Enemy_Scraprat.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|SteamDroid",     sBasePath .. "Enemy_SteamDroid.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|WreckedBot",     sBasePath .. "Enemy_WreckedBot.png",     0, 0, -1, -1, 0)

--Bosses
ImageLump_Rip("Enemy|609144",          sBasePath .. "Boss_609144.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Serenity",        sBasePath .. "Boss_Serenity.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|TechMonstrosity", sBasePath .. "Boss_TechMonstrosity.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Vivify",          sBasePath .. "Boss_Vivify.png",          0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
