--[ ======================================= NPC Portraits ======================================= ]
--Subscript for NPCs portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH5Emote.slf")
else
	SLF_Open("Output/PortraitsLD_CH5Emote.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--Grid NPCs
ImageLump_Rip("Por|Doll|Neutral",            sBasePath .. "Chapter5_NPC_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|DollInfluenced|Neutral",  sBasePath .. "Chapter5_NPC_Emote.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|DollAsAdmin|Neutral",     sBasePath .. "Chapter5_NPC_Emote.png", gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Raiju|Neutral",           sBasePath .. "Chapter5_NPC_Emote.png", gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|SteamDroid|Neutral",      sBasePath .. "Chapter5_NPC_Emote.png", gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemRebelHead|Neutral",  sBasePath .. "Chapter5_NPC_Emote.png", gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemRebelScarf|Neutral", sBasePath .. "Chapter5_NPC_Emote.png", gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Isabel|Neutral",          sBasePath .. "Chapter5_NPC_Emote.png", gciHDX3, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemPack|Neutral",       sBasePath .. "Chapter5_NPC_Emote.png", gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Golem|Neutral",           sBasePath .. "Chapter5_NPC_Emote.png", gciHDX1, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemLord|Neutral",       sBasePath .. "Chapter5_NPC_Emote.png", gciHDX2, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemLordB|Neutral",      sBasePath .. "Chapter5_NPC_Emote.png", gciHDX3, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|201890|Neutral",          sBasePath .. "Chapter5_NPC_Emote.png", gciHDX0, gciHDY3, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|609144|Neutral",          sBasePath .. "Chapter5_NPC_Emote.png", gciHDX1, gciHDY3, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Maisie|Neutral",          sBasePath .. "Chapter5_NPC_Emote.png", gciHDX2, gciHDY3, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemLordC|Neutral",      sBasePath .. "Chapter5_NPC_Emote.png", gciHDX3, gciHDY3, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Katarina|Neutral",        sBasePath .. "Chapter5_NPC_Emote.png", gciHDX0, gciHDY4, gciWid0, gciHei0, 0)

--Gala Golems
ImageLump_Rip("Por|GolemFancyA|Neutral",   sBasePath .. "Chapter5_NPCGala_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemFancyC|Neutral",   sBasePath .. "Chapter5_NPCGala_Emote.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemFancyB|Neutral",   sBasePath .. "Chapter5_NPCGala_Emote.png", gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemFancyD|Neutral",   sBasePath .. "Chapter5_NPCGala_Emote.png", gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemFancyF|Neutral",   sBasePath .. "Chapter5_NPCGala_Emote.png", gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|GolemFancyE|Neutral",   sBasePath .. "Chapter5_NPCGala_Emote.png", gciHDX1, gciHDY1, gciWid0, gciHei0, 0)

--Wide-grid enemies
ImageLump_Rip("Por|Electrosprite|Neutral",  sBasePath .. "Chapter5_NPC_EmoteWide.png", gciWideX0, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|LatexDrone|Neutral",     sBasePath .. "Chapter5_NPC_EmoteWide.png", gciWideX2, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|DarkmatterGirl|Neutral", sBasePath .. "Chapter5_NPC_EmoteWide.png", gciWideX3, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|Dreamer|Neutral",        sBasePath .. "Chapter5_NPC_EmoteWide.png", gciWideX0, gciWideY1, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|Vivify|Neutral",         sBasePath .. "Chapter5_NPC_EmoteWide.png", gciWideX1, gciWideY1, gciWideWid, gciWideHei, 0)

--[ ===================================== Layered Portraits ===================================== ]
--This portrait uses three layers. Each layer can be swapped out. A given emotion specifies which
-- set of three layers to use. The sizing is very specific.
local iXSize = 700
local iYSize = 576
local sRipPath = sBasePath .. "Administrator.png"
ImageLump_Rip("Por|Admin|Base",   sRipPath, iXSize * 0, iYSize * 0, iXSize, iYSize, 0)
ImageLump_Rip("Por|Admin|Layer1", sRipPath, iXSize * 1, iYSize * 0, iXSize, iYSize, 0)
ImageLump_Rip("Por|Admin|Layer2", sRipPath, iXSize * 2, iYSize * 0, iXSize, iYSize, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
