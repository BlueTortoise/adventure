--[ ==================================== Cassandra Portraits ==================================== ]
--Subscript for Cassandra's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Cassandra.slf")
else
	SLF_Open("Output/PortraitsLD_Cassandra.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for Christine's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Werecat", sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Golem",   sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("Cassandra", "", sBasePath .. "Cassandra_All_Emote.png")

--[Special Frames]
--None yet.

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
