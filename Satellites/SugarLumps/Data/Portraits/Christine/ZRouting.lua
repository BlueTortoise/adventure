--[ ==================================== Christine Portraits ==================================== ]
--Subscript for Christine's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Christine.slf")
else
	SLF_Open("Output/PortraitsLD_Christine.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for Christine's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath, bOnlyNeutralSmirk)
    
    --Switch based on emotion set.
    if(bOnlyNeutralSmirk == false) then
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Happy",   sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Blush",   sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Smirk",   sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Sad",     sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Scared",  sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Offended",sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Cry",     sPath, gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Laugh",   sPath, gciHDX1, gciHDY2, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Angry",   sPath, gciHDX2, gciHDY2, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|PDU",     sPath, gciHDX3, gciHDY2, gciWid0, gciHei0, 0)
    
    --Only neutral and smirk emotions:
    else
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Smirk",   sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
    end
end

--[General Purpose]
fnAutoRip("Christine", "Darkmatter",    sBasePath .. "Christine_Darkmatter_Emote.png",    false)
fnAutoRip("Christine", "Doll",          sBasePath .. "Christine_Doll_Emote.png",          false)
fnAutoRip("Christine", "DollSweater",   sBasePath .. "Christine_DollEnglish_Emote.png",   false)
fnAutoRip("Christine", "DollNude",      sBasePath .. "Christine_DollNude_Emote.png",      false)
fnAutoRip("Christine", "Eldritch",      sBasePath .. "Christine_Eldritch_Emote.png",      false)
fnAutoRip("Christine", "Electrosprite", sBasePath .. "Christine_Electrosprite_Emote.png", false)
fnAutoRip("Christine", "Golem",         sBasePath .. "Christine_Golem_Emote.png",         false)
fnAutoRip("Christine", "GolemGala",     sBasePath .. "Christine_GolemGala_Emote.png",     false)
fnAutoRip("Christine", "",              sBasePath .. "Christine_Human_Emote.png",         false)
fnAutoRip("Christine", "HumanNude",     sBasePath .. "Christine_HumanNude_Emote.png",     false)
fnAutoRip("Christine", "Latex",         sBasePath .. "Christine_Latex_Emote.png",         false)
fnAutoRip("Chris",     "",              sBasePath .. "Christine_Male_Emote.png",          true)
fnAutoRip("Christine", "Raiju",         sBasePath .. "Christine_Raiju_Emote.png",         false)
fnAutoRip("Christine", "RaijuClothes",  sBasePath .. "Christine_RaijuClothes_Emote.png",  false)
fnAutoRip("Christine", "SteamDroid",    sBasePath .. "Christine_Steamdroid_Emote.png",    false)

--[Special Frames]
--These are only used for certain parts of the story and only exist for two forms.
ImageLump_Rip("Por|ChristineGolem|Serious", sBasePath .. "Christine_Golem_Emote.png", gciHDX3, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|ChristineDoll|Serious",  sBasePath .. "Christine_Doll_Emote.png",  gciHDX3, gciHDY1, gciWid0, gciHei0, 0)

--[ ========================================== Combat =========================================== ]
--This is just a simple list of combat frames.
ImageLump_Rip("Party|Christine_Darkmatter",    sBasePath .. "Christine_Darkmatter_Combat.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_Doll",          sBasePath .. "Christine_Doll_Combat.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_Eldritch",      sBasePath .. "Christine_Eldritch_Combat.png",      0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_Electrosprite", sBasePath .. "Christine_Electrosprite_Combat.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_Golem",         sBasePath .. "Christine_Golem_Combat.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_GolemDress",    sBasePath .. "Christine_GolemGala_Combat.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_Golem_Forward", sBasePath .. "Christine_Golem_CombatForward.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_Human",         sBasePath .. "Christine_Human_Combat.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_Latex",         sBasePath .. "Christine_Latex_Combat.png",         0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Party|Christine_Male",          sBasePath .. "Christine_Male_Combat.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_Raibie",        sBasePath .. "Christine_Raibie_Combat.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_Raiju",         sBasePath .. "Christine_Raiju_Combat.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_RaijuClothed",  sBasePath .. "Christine_RaijuClothes_Combat.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Party|Christine_SteamDroid",    sBasePath .. "Christine_Steamdroid_Combat.png",    0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
