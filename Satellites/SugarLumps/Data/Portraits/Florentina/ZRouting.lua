--[ ==================================== Florentina Portraits =================================== ]
--Subscript for Florentina's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Florentina.slf")
else
	SLF_Open("Output/PortraitsLD_Florentina.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for Florentina's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Happy",  sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Blush",  sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Facepalm",  sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Surprise",  sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Offended",  sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Confused",  sPath, gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("Florentina", "Merchant",       sBasePath .. "Florentina_Merchant_Emote.png")
fnAutoRip("Florentina", "Mediator",       sBasePath .. "Florentina_Mediator_Emote.png")
fnAutoRip("Florentina", "TreasureHunter", sBasePath .. "Florentina_TreasureHunter_Emote.png")

--[Special Frames]
--None yet.

--[ ========================================== Combat =========================================== ]
--This is just a simple list of combat frames.
ImageLump_Rip("Party|Florentina_Merchant",       sBasePath .. "Florentina_Merchant_Combat.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Party|Florentina_Mediator",       sBasePath .. "Florentina_Mediator_Combat.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Party|Florentina_TreasureHunter", sBasePath .. "Florentina_TreasureHunter_Combat.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Party|FlorentinaCountermask",     sBasePath .. "Florentina_CombatCountermask.png",     0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
