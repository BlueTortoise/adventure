--[ ===================================== JX-101's Portraits ==================================== ]
--Subscript for Florentina's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_JX101.slf")
else
	SLF_Open("Output/PortraitsLD_JX101.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for JX-101's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|HatA",  sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|HatB",  sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|HatC",  sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("JX101", "", sBasePath .. "JX101_Steamdroid_Emote.png")

--[Special Frames]
--None yet.

--[ ========================================== Combat =========================================== ]
--This is just a simple list of combat frames.
ImageLump_Rip("Party|JX101", sBasePath .. "JX101_Steamdroid_Combat.png", 0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
