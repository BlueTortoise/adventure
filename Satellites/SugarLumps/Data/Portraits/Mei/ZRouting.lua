--[ ======================================= Mei Portraits ======================================= ]
--Subscript for Mei's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Mei.slf")
else
	SLF_Open("Output/PortraitsLD_Mei.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for Christine's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath, bOnlyNeutralSmirk)
    
    --Switch based on emotion set.
    if(bOnlyNeutralSmirk == false) then
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Smirk",    sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Happy",    sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Blush",    sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Sad",      sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Surprise", sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Offended", sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Cry",      sPath, gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Laugh",    sPath, gciHDX1, gciHDY2, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Angry",    sPath, gciHDX2, gciHDY2, gciWid0, gciHei0, 0)
    
    --Only neutral and smirk emotions:
    else
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
        ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Smirk",   sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
    end
end

--[General Purpose]
fnAutoRip("Mei", "Alraune", sBasePath .. "Mei_Alraune_Emote.png", false)
fnAutoRip("Mei", "Bee",     sBasePath .. "Mei_Bee_Emote.png",     false)
fnAutoRip("Mei", "BeeQueen",sBasePath .. "Mei_BeeQueen_Emote.png",false)
fnAutoRip("Mei", "Ghost",   sBasePath .. "Mei_Ghost_Emote.png",   false)
fnAutoRip("Mei", "",        sBasePath .. "Mei_Human_Emote.png",   false)
fnAutoRip("Mei", "Slime",   sBasePath .. "Mei_Slime_Emote.png",   false)
fnAutoRip("Mei", "Werecat", sBasePath .. "Mei_Werecat_Emote.png", false)

--[Special Frames]
--Mind-controlled Mei and Zombee Mei
ImageLump_Rip("Por|MeiAlraune|MC", sBasePath .. "Mei_Alraune_Emote.png", gciHDX3, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|MeiBee|MC",     sBasePath .. "Mei_Bee_Emote.png",     gciHDX3, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Mei|MC",        sBasePath .. "Mei_Human_Emote.png",   gciHDX3, gciHDY1, gciWid0, gciHei0, 0)

--Rubber Mei
ImageLump_Rip("Por|Mei|Rubber", sBasePath .. "Mei_Rubber_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0)

--[ ========================================== Combat =========================================== ]
--This is just a simple list of combat frames.
ImageLump_Rip("Party|Mei_Alraune",   sBasePath .. "Mei_Alraune_Combat.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_AlrauneMC", sBasePath .. "Mei_Alraune_CombatMC.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_Bee",       sBasePath .. "Mei_Bee_Combat.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_BeeQueen",  sBasePath .. "Mei_BeeQueen_Combat.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_Ghost",     sBasePath .. "Mei_Ghost_Combat.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_Human",     sBasePath .. "Mei_Human_Combat.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_HumanMC",   sBasePath .. "Mei_Human_CombatMC.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_Rubber",    sBasePath .. "Mei_Rubber_Combat.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_Slime",     sBasePath .. "Mei_Slime_Combat.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_Werecat",   sBasePath .. "Mei_Werecat_Combat.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_Zombee",    sBasePath .. "Mei_Zombee_Combat.png",    0, 0, -1, -1, 0)

--Countermasks. Used for the UI.
ImageLump_Rip("Party|Mei_Alraune_Countermask", sBasePath .. "Mei_Alraune_CombatCountermask.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Party|Mei_Human_Countermask",   sBasePath .. "Mei_Human_CombatCountermask.png",   0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
