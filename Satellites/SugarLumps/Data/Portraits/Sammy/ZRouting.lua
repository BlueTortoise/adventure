--[ =================================== Sammy Davis Portraits =================================== ]
--Sammy Davis - ACTION MOTH.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Sammy.slf")
else
	SLF_Open("Output/PortraitsLD_Sammy.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for Sammy's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Sexy",  sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Gun",  sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("Sammy", "", sBasePath .. "Sammy_Moth_Emote.png")

--[Special Frames]
--None yet.

--[ ========================================== Combat =========================================== ]
--This is just a simple list of combat frames.

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
