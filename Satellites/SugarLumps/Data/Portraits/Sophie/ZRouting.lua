--[ ===================================== Sophie Portraits ====================================== ]
--Subscript for Sophie's portraits.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Sophie.slf")
else
	SLF_Open("Output/PortraitsLD_Sophie.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for Sophie's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Happy",    sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Blush",    sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Smirk",    sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Sad",      sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Surprise", sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Offended", sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("Sophie", "",     sBasePath .. "Sophie_Golem_Emote.png")
fnAutoRip("Sophie", "Gala", sBasePath .. "Sophie_GolemGala_Emote.png")

--[Special Frames]
--BDSM Sophie. May get upgraded to a full emotes page later.
ImageLump_Rip("Por|SophieWhip|Neutral", sBasePath .. "Sophie_GolemLeather_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|SophieWhip|Happy",   sBasePath .. "Sophie_GolemLeather_Emote.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|SophieWhip|Blush",   sBasePath .. "Sophie_GolemLeather_Emote.png", gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|SophieWhip|Smirk",   sBasePath .. "Sophie_GolemLeather_Emote.png", gciHDX3, gciHDY0, gciWid0, gciHei0, 0)

--[ ========================================== Combat =========================================== ]
--No combat frames for Sophie.

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
