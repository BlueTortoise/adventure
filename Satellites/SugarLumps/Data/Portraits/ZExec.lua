--[ =================================== Portraits Compression =================================== ]
--Executes portrait compression.
Debug_PushPrint(false, "Beginning portrait compression.\n")

--Variables.
local sCurrentPath = fnResolvePath()
local baCompressionFlags = {}

--[Size Variables]
--Constants used for emote grids.
gciHDX0 = 3
gciHDX1 = 703
gciHDX2 = 1403
gciHDX3 = 2103

gciHDY0 = 3
gciHDY1 = 771
gciHDY2 = 1539
gciHDY3 = 2307
gciHDY4 = 3075

gciWid0 = 694
gciWid1 = 694
gciWid2 = 694
gciWid3 = 694

gciHei0 = 762
gciHei1 = 762
gciHei2 = 762
gciHei3 = 762

--Size constants used for "wide" emote grids. These are usually enemy portraits.
gciWideX0 = 3
gciWideX1 = 1401
gciWideX2 = 2799
gciWideX3 = 4197

gciWideY0 = 3
gciWideY1 = 827

gciWideWid = 1392
gciWideHei = 818

--[Automated Compression Function]
local fnCompressFolder = function(sFolderName, sExecFile)

	--Setup
	local bCompressedAnything = false
	local sCurrentPath = fnResolvePath()

	--Push the stack.
	TS_PushStack()

		--Get starting data.
		TS_LoadFrom(sCurrentPath .. sFolderName .. ".log")

		--Scan for new files/modifications/removals.
		local bHasDirectoryChanged = TS_ScanDirectory(sCurrentPath .. sFolderName .. "/")
		if(bHasDirectoryChanged) then
			--Compress here.
			io.write(" " .. sFolderName .. ": Files have changed - ")
			--io.write("Exec: " .. sCurrentPath .. sFolderName .. "/" .. sExecFile .. ".lua" .. "\n")
			LM_ExecuteScript(sCurrentPath .. sFolderName .. "/" .. sExecFile .. ".lua")
			io.write("done.\n")
			bCompressedAnything = true
			
			--Save a log file.
			TS_PrintTo(sCurrentPath .. sFolderName .. ".log")
		else
			io.write(" " .. sFolderName .. ": Files have not changed.\n")
		end
	
	--Clean up.
	TS_PopStack()
	return bCompressedAnything
end

--[Construction]
--Adder function.
local iTotalEntries = 0
local saEntryList = {}
local fnAddEntry = function(sName, sRoutingFile)
    if(sName == nil) then return end
    if(sRoutingFile == nil) then sRoutingFile = "ZRouting" end
    
    iTotalEntries = iTotalEntries + 1
    saEntryList[iTotalEntries] = {sName, sRoutingFile}
end

--Build an ordered list.
fnAddEntry("55")
fnAddEntry("56")
fnAddEntry("Aquillia")
fnAddEntry("Cassandra")
fnAddEntry("CH0Enemy")
fnAddEntry("CH0NPCEmote")
fnAddEntry("CH1Enemy")
fnAddEntry("CH1NPCEmote")
fnAddEntry("CH5Enemy")
fnAddEntry("CH5NPCEmote")
fnAddEntry("Christine")
fnAddEntry("Florentina")
--fnAddEntry("Jeanne")
fnAddEntry("JX101")
--fnAddEntry("Lotta")
fnAddEntry("Maram")
fnAddEntry("Mei")
fnAddEntry("PDU")
fnAddEntry("Sammy")
--fnAddEntry("Sanya")
fnAddEntry("Septima")
fnAddEntry("Sophie")
fnAddEntry("SX399")
--fnAddEntry("Talia")

--[Execution]
--Auto-exec the folders.
gbUseLowDefinition = false
ImageLump_SetAllowLineCompression(true)
for i = 1, iTotalEntries, 1 do
    baCompressionFlags[i] = fnCompressFolder(saEntryList[i][1], saEntryList[i][2])
end
ImageLump_SetAllowLineCompression(false)

--Low-definition stuff.
gbUseLowDefinition = true
ImageLump_SetAllowLineCompression(true)
for i = 1, iTotalEntries, 1 do
    if(baCompressionFlags[i] == true) then LM_ExecuteScript(sCurrentPath .. saEntryList[i][1] .. "/ZRouting.lua") end
end
ImageLump_SetAllowLineCompression(false)
gbUseLowDefinition = false

--Done
Debug_PopPrint("Finished portrait compression activities.\n")