--[Dancing]
--Used for the dancing minigame.
local sBasePath = fnResolvePath()
local sImgPath = sBasePath .. "Izuna.png"
local iWid = 48
local iHei = 48

--Sequence Registry. {Prefix, FrameCount, XStart, YStart)
local zaSequences = {}
zaSequences[1] = {"Dance|Izuna|Idle|", 4, iWid*0, iHei*0}
zaSequences[2] = {"Dance|Izuna|ArmR|", 6, iWid*0, iHei*1}
zaSequences[3] = {"Dance|Izuna|ArmL|", 6, iWid*0, iHei*2}
zaSequences[4] = {"Dance|Izuna|LegL|", 6, iWid*0, iHei*3}
zaSequences[5] = {"Dance|Izuna|LegR|", 6, iWid*0, iHei*4}
zaSequences[6] = {"Dance|Izuna|Down|", 6, iWid*0, iHei*5}
zaSequences[7] = {"Dance|Izuna|HipT|", 6, iWid*0, iHei*6}

--For each sequence, rip.
for i = 1, #zaSequences, 1 do
    local sPrefix = zaSequences[i][1]
    local iFramesTotal = zaSequences[i][2]
    local iXStart = zaSequences[i][3]
    local iYStart = zaSequences[i][4]
    for p = 1, iFramesTotal, 1 do
        ImageLump_Rip(sPrefix .. (p-1), sImgPath, iXStart + ((p-1) * iWid), iYStart, iWid, iHei, 0)
    end
end

--[Arrows]
sImgPath = sBasePath .. "Arrows.png"
iWid = 96
iHei = 96
ImageLump_Rip("Dance|ArrowTop",   sImgPath, iWid*0, iHei*0, iWid, iHei, 0)
ImageLump_Rip("Dance|ArrowRgt",   sImgPath, iWid*1, iHei*0, iWid, iHei, 0)
ImageLump_Rip("Dance|ArrowBot",   sImgPath, iWid*2, iHei*0, iWid, iHei, 0)
ImageLump_Rip("Dance|ArrowLft",   sImgPath, iWid*3, iHei*0, iWid, iHei, 0)
ImageLump_Rip("Dance|ArrowNon",   sImgPath, iWid*0, iHei*1, iWid, iHei, 0)
ImageLump_Rip("Dance|ArrowAct",   sImgPath, iWid*1, iHei*1, iWid, iHei, 0)
ImageLump_Rip("Dance|ArrowCan",   sImgPath, iWid*2, iHei*1, iWid, iHei, 0)
ImageLump_Rip("Dance|ArrowBox",   sImgPath, iWid*3, iHei*1, iWid, iHei, 0)
ImageLump_Rip("Dance|Encourage0", sImgPath, iWid*0, iHei*2, iWid, iHei, 0)
ImageLump_Rip("Dance|Encourage1", sImgPath, iWid*1, iHei*2, iWid, iHei, 0)
ImageLump_Rip("Dance|Encourage2", sImgPath, iWid*2, iHei*2, iWid, iHei, 0)
ImageLump_Rip("Dance|DiscLate",   sImgPath, iWid*0, iHei*3, iWid, iHei, 0)
ImageLump_Rip("Dance|DiscEarly",  sImgPath, iWid*1, iHei*3, iWid, iHei, 0)
ImageLump_Rip("Dance|DiscWrong",  sImgPath, iWid*2, iHei*3, iWid, iHei, 0)
