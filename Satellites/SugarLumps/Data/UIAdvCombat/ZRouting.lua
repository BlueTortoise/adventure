--[ ========================================= Combat UI ========================================= ]
--WAAAAAAAAAAAAAAAAAARRRRRRRRRRRRRR!
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/AdvCombat.slf")
ImageLump_SetCompression(1)

--[Function]
local fnRip = function(sDirPath, sPrefix, saNames)
    local i = 1
    while(saNames[i] ~= nil) do
        ImageLump_Rip(sPrefix .. saNames[i], sDirPath .. saNames[i] .. ".png", 0, 0, -1, -1, 0)
        i = i + 1
    end
end

--[ ========================================== Ally Bar ========================================= ]
local sDirPath = sBasePath .. "AllyBar/"
local sPrefix = "AllyBar|"
local saNames = {"AllyFrame", "AllyPortraitMask"}
fnRip(sDirPath, sPrefix, saNames)

--[ ====================================== Defeat Overlay ======================================= ]
sDirPath = sBasePath .. "Defeat/"
sPrefix = "Defeat|"
saNames = {"Defeat_0", "Defeat_1", "Defeat_2", "Defeat_3", "Defeat_4", "Defeat_5"}
fnRip(sDirPath, sPrefix, saNames)

--[ ===================================== Enemy Health Bar ====================================== ]
sDirPath = sBasePath .. "EnemyHealthBar/"
sPrefix = "EnemyHealthBar|"
saNames = {"EnemyHealthBarEdge", "EnemyHealthBarFill", "EnemyHealthBarFrame", "EnemyHealthBarStun", "EnemyHealthBarStunMarker", "EnemyHealthBarUnder"}
fnRip(sDirPath, sPrefix, saNames)

--[ ========================================= Inspector ========================================= ]
sDirPath = sBasePath .. "Inspector/"
sPrefix = "Inspector|"
saNames = {"Frames", "Name Inset"}
fnRip(sDirPath, sPrefix, saNames)

--[ ===================================== Player Interface ====================================== ]
sDirPath = sBasePath .. "PlayerInterface/"
sPrefix = "PlayerInterface|"
saNames = {"AbilityFrameCatalyst1", "AbilityFrameCatalyst2", "AbilityFrameCatalyst3", "AbilityFrameCatalyst4", "AbilityFrameCatalyst5", "AbilityFrameCatalyst6", "AbilityFrameCustom", "AbilityFrameMain", "AbilityFrameSecond", "AbilityHighlight", "CPPip", "Descriptionwindow", "HealthBarAdrenaline", "HealthBarHPFill", "HealthBarMix", "HealthBarMPFill", "HealthBarShield", "HealthBarUnderlay", "MainHealthBarFrame", "MainNameBanner", "MainPortraitMask", "MainPortraitRing", "MainPortraitRingBack", "PredictionBox", "TargetArrowLft", "TargetArrowRgt", "TargetBox", "TitleBox"}
fnRip(sDirPath, sPrefix, saNames)

--[ ======================================== Turn Order ========================================= ]
sDirPath = sBasePath .. "TurnOrder/"
sPrefix = "TurnOrder|"
saNames = {"TurnOrderBack", "TurnOrderCircle", "TurnOrderEdge"}
fnRip(sDirPath, sPrefix, saNames)

--[ ======================================= Turn Portraits ====================================== ]
--[Ripper Function]
--Automated, creates images named "PorSml|Alraune" and such. Uses tables.
local fnRipImages = function(sPrefix, sRipPath, saRipNames, iXValue, iYValue, iWid, iHei, sCharPrefix)
	
	--Arg check
	if(sPrefix    == nil) then return end
	if(sRipPath   == nil) then return end
	if(saRipNames == nil) then return end
	if(iXValue    == nil) then return end
	if(iYValue    == nil) then return end
	
	--sCharPrefix can be nil. It's only used for ripping the main characters.
	
	--Let 'er rip!
	local i = 0
	while(saRipNames[i+1] ~= nil) do
		
		--If the name is "SKIP", then don't rip here. This is used because slots are empty until later in some cases.
		if(saRipNames[i+1] == "SKIP") then
		else
		
			--Not using a character prefix:
			if(sCharPrefix == nil) then
				ImageLump_Rip(sPrefix .. saRipNames[i+1], sRipPath, iXValue + (iWid * i), iYValue, iWid, iHei, 0)
			
			--Character prefix. Makes names like "PorSml|Christine_Alraune"
			else
				ImageLump_Rip(sPrefix .. sCharPrefix .. saRipNames[i+1], sRipPath, iXValue + (iWid * i), iYValue, iWid, iHei, 0)
			end
			
		end
		
		--Next.
		i = i + 1
	end
end

--[Character Ripping]
--Constants
local ciWid = 80
local ciHei = 60
local sRipPath = sBasePath .. "TurnPortraits/TurnPortraitsSheet.png"

--Christine's row.
local saRipNames = {"Human", "SKIP", "SKIP", "SKIP", "SKIP", "SKIP", "Male", "Golem", "GolemDress", "Latex", "Darkmatter", "Electrosprite", "SteamDroid", "Eldritch", "Raiju", "Doll"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 0, ciWid, ciHei, "Christine_")

--Mei's row.
saRipNames = {"Human", "Alraune", "Bee", "Ghost", "Slime", "Werecat", "SKIP", "Zombee"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 2, ciWid, ciHei, "Mei_")

--Other Party Members. They don't have multiple forms.
saRipNames = {"Florentina", "55", "JX-101", "Aquillia", "SX-399"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 6, ciWid, ciHei)

--First row of classes for other party members.
saRipNames = {"FlorentinaTH"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 7, ciWid, ciHei)

--Second row of classes for other party members.
saRipNames = {"FlorentinaMed"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 8, ciWid, ciHei)

--[Enemy Ripping]
sRipPath = sBasePath .. "TurnPortraits/TurnPortraitsSheetEnemies.png"

--Chapter 1
saRipNames = {"Alraune", "Bee", "CultistF", "CultistM", "Ghost", "Slime", "Werecat", "Zombee", "SkullCrawler", "Arachnophelia", "Infirm"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 0, ciWid, ciHei)

--Chapter 5
saRipNames = {"Doll", "Electrosprite", "Golem", "GolemLord", "LatexDrone", "Raiju", "Scraprat", "SecurityWrecked", "DarkmatterGirl", "SteamDroid", "Dreamer", "Vivify", "56", "TechMonstrosity", "VoidRift", "Horrible", "Serenity", "609144", "Raibie", "BandageGoblin", "Hoodie", "InnGeisha"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 4, ciWid, ciHei)

--Allies and Other
saRipNames = {"BeeAlly"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 6, ciWid, ciHei)

--[ ========================================== Victory ========================================== ]
sDirPath = sBasePath .. "Victory/"
sPrefix = "Victory|"
saNames = {"BannerBack", "BannerBig", "DoctorBack", "DoctorFill", "DoctorFrame", "DropFrame", "ExpFrameBack", "ExpFrameFill", "ExpFrameFront", "ExpFrameMask"}
fnRip(sDirPath, sPrefix, saNames)

--[ ========================================== Clean Up ========================================= ]
--Finish
SLF_Close()