-- |[ ==================================== Adventure Icons ===================================== ]|
--Icons used in various spots in Adventure Mode. These indiciate things like damage types, debuffs,
-- characters, weapons, inventory, and more!
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/UIAdvIcons.slf")
ImageLump_SetCompression(1)

-- |[ ====================================== Damage Types ====================================== ]|
--Icons that indicate what type of damage an ability does, or resistances.
cfWid = 19
cfHei = 19
sPath = sBasePath .. "DamageTypeIcons.png"
sPrefix = "DmgTypeIco|"
ImageLump_Rip(sPrefix .. "Slashing",   sPath, (cfWid * 0.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Striking",   sPath, (cfWid * 1.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Piercing",   sPath, (cfWid * 2.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Flaming",    sPath, (cfWid * 3.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Freezing",   sPath, (cfWid * 0.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Shocking",   sPath, (cfWid * 1.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Crusading",  sPath, (cfWid * 2.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Obscuring",  sPath, (cfWid * 3.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Bleeding",   sPath, (cfWid * 0.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Poisoning",  sPath, (cfWid * 1.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Corroding",  sPath, (cfWid * 2.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Terrifying", sPath, (cfWid * 3.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Protection", sPath, (cfWid * 0.0), (cfHei * 3.0), cfWid, cfHei, 0)

--Special:
ImageLump_Rip(sPrefix .. "Weapon", sPath, 19, 57, 56, 19, 0)

-- |[ ======================================== Debuffs ========================================= ]|
--Icons for debuffs. These are not robust categories, they are whatever the debuff wants to use.
cfWid = 19
cfHei = 19
sPath = sBasePath .. "StatusEffectIcons.png"
sPrefix = "StatEffectIco|"
ImageLump_Rip(sPrefix .. "Blind",    sPath, (cfWid * 0.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Buff",     sPath, (cfWid * 1.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Debuff",   sPath, (cfWid * 2.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Clock",    sPath, (cfWid * 0.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Accuracy", sPath, (cfWid * 0.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Stun",     sPath, (cfWid * 1.0), (cfHei * 2.0), cfWid, cfHei, 0)

--Effect Indicators
cfWid = 48
cfHei = 19
ImageLump_Rip(sPrefix .. "EffectHostile",  sPath, 0, 57, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "EffectFriendly", sPath, 0, 76, cfWid, cfHei, 0)

--Strength Indicators
cfWid = 37
cfHei = 19
ImageLump_Rip(sPrefix .. "Str0",  sPath,     0, 95 + (cfHei * 0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Str1",  sPath, cfWid, 95 + (cfHei * 0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Str2",  sPath,     0, 95 + (cfHei * 1), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Str3",  sPath, cfWid, 95 + (cfHei * 1), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Str4",  sPath,     0, 95 + (cfHei * 2), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Str5",  sPath, cfWid, 95 + (cfHei * 2), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Str6",  sPath,     0, 95 + (cfHei * 3), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Str7",  sPath, cfWid, 95 + (cfHei * 3), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Str8",  sPath,     0, 95 + (cfHei * 4), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Str9",  sPath, cfWid, 95 + (cfHei * 4), cfWid, cfHei, 0)

-- |[ ==================================== Comparison Icons ==================================== ]|
--Used for displaying stats and comparing them in the inventory/equipment displays.
local s18PxPath = sBasePath .. "CompareIco.png"
local saArray = {}
saArray[1] = {"Attack", "Protection", "Health", "Accuracy", "Evade", "Initiative", "Mana"}
saArray[2] = {"Blind", "Bleed", "Poison", "Corrode", "Shields", "Adrenaline", "ComboPoint"}
saArray[3] = {"Stun", "Turns"}
saArray[4] = {"Slash", "Pierce", "Strike"}
saArray[5] = {"Flaming", "Freezing", "Shocking"}
saArray[6] = {"Crusading", "Obscuring", "Terrify", "Debuff"}
saArray[7] = {"Underlay"}
saArray[8] = {"Downx3", "Downx2", "Downx1", "Neutral", "Upx1", "Upx2", "Upx3"}
saArray[9] = {"Rarrow"}

sPrefix = "CompareIco|"
cfWid = 18
cfHei = 18
i = 1
while(saArray[i] ~= nil) do
    local p = 1
    while(saArray[i][p] ~= nil) do
        ImageLump_Rip(sPrefix .. saArray[i][p], s18PxPath, (p-1) * cfWid, (i - 1) * cfHei, cfWid, cfHei, 0)
        p = p + 1
    end
    i = i + 1
end

--Special: Used only for the combat inspector. Not as wide.
ImageLump_Rip("CompareIcoNr|Up", s18PxPath, 72, 108, 10, 18, 0)
ImageLump_Rip("CompareIcoNr|Dn", s18PxPath, 90, 108, 10, 18, 0)

-- |[ ================================= Ability Icons - General ================================ ]|
--Ability icons and parts not specific to any individual character.
cfWid = 50
cfHei = 50
sPath = sBasePath .. "AbilityIcons_System.png"
sPrefix = "AbilityIco|"

--Items
ImageLump_Rip(sPrefix .. "Item|SilverRunestone", sPath, (cfWid * 0.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Item|VioletRunestone", sPath, (cfWid * 1.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Item|GenericPotion",   sPath, (cfWid * 2.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Item|YellowPotion",    sPath, (cfWid * 3.0), (cfHei * 0.0), cfWid, cfHei, 0)

--Combo/Cooldown Indicators
ImageLump_Rip(sPrefix .. "CmbClock", sPath, (cfWid * 5.0), (cfHei * 0.0), cfWid, cfHei, 0)
for i = 0, 9, 1 do
    ImageLump_Rip(sPrefix .. "Cmb"..i, sPath, (cfWid * (6+i)), (cfHei * 0.0), cfWid, cfHei, 0)
end

--Turn Action Backing/Frames
ImageLump_Rip(sPrefix .. "OctBackGrey",   sPath, (cfWid * 0.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "OctBackRed",    sPath, (cfWid * 1.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "OctBackGreen",  sPath, (cfWid * 2.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "OctBackBlue",   sPath, (cfWid * 3.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "OctBackPurple", sPath, (cfWid * 4.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "OctFrameRed",   sPath, (cfWid * 5.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "OctFrameBlue",  sPath, (cfWid * 6.0), (cfHei * 1.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "OctFrameTeal",  sPath, (cfWid * 7.0), (cfHei * 1.0), cfWid, cfHei, 0)

--Free Action Backing/Frames
ImageLump_Rip(sPrefix .. "SqrBackGrey",   sPath, (cfWid * 0.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "SqrBackRed",    sPath, (cfWid * 1.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "SqrBackGreen",  sPath, (cfWid * 2.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "SqrBackBlue",   sPath, (cfWid * 3.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "SqrBackPurple", sPath, (cfWid * 4.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "SqrFrameRed",   sPath, (cfWid * 5.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "SqrFrameBlue",  sPath, (cfWid * 6.0), (cfHei * 2.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "SqrFrameTeal",  sPath, (cfWid * 7.0), (cfHei * 2.0), cfWid, cfHei, 0)

--Common Abilities
ImageLump_Rip(sPrefix .. "Attack",    sPath, (cfWid * 0.0), (cfHei * 3.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Defend",    sPath, (cfWid * 1.0), (cfHei * 3.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Special",   sPath, (cfWid * 2.0), (cfHei * 3.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Retreat",   sPath, (cfWid * 3.0), (cfHei * 3.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Surrender", sPath, (cfWid * 4.0), (cfHei * 3.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Threat",    sPath, (cfWid * 5.0), (cfHei * 3.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Lock",      sPath, (cfWid * 6.0), (cfHei * 3.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "WeaponLft", sPath, (cfWid * 7.0), (cfHei * 3.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "WeaponRgt", sPath, (cfWid * 8.0), (cfHei * 3.0), cfWid, cfHei, 0)

-- |[ ================================== Ability Icons - Mei =================================== ]|
--Ability icons for Mei.
cfWid = 50
cfHei = 50
local cfUseHei = 0
sPath = sBasePath .. "AbilityIcons_Mei.png"
sPrefix = "AbilityIco|"

--Fencer
cfUseHei = cfHei * 0.0
ImageLump_Rip(sPrefix .. "Mei|WayOfTheBlade",  sPath, (cfWid *  0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|PowerfulStrike", sPath, (cfWid *  1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Rend",           sPath, (cfWid *  2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|BladeDance",     sPath, (cfWid *  3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|PommelBash",     sPath, (cfWid *  4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Blind",          sPath, (cfWid *  5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Precognition",   sPath, (cfWid *  6.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Taunt",          sPath, (cfWid *  7.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|QuickStrike",    sPath, (cfWid *  8.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Trip",           sPath, (cfWid *  9.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Concentrate",    sPath, (cfWid * 10.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Whirl",          sPath, (cfWid * 11.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|FastReflexes",   sPath, (cfWid * 12.0), cfUseHei, cfWid, cfHei, 0)

--Nightshade
cfUseHei = cfHei * 1.0
ImageLump_Rip(sPrefix .. "Mei|WayOfTheDruid",   sPath, (cfWid *  0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|SporeCloud",      sPath, (cfWid *  1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Regrowth",        sPath, (cfWid *  2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|VineLash",        sPath, (cfWid *  3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|PollenRend",      sPath, (cfWid *  4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|FanOfLeaves",     sPath, (cfWid *  5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Ripple",          sPath, (cfWid *  6.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|NaturesBlessing", sPath, (cfWid *  7.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|AcidBlood",       sPath, (cfWid *  8.0), cfUseHei, cfWid, cfHei, 0)

--Prowler
cfUseHei = cfHei * 2.0
ImageLump_Rip(sPrefix .. "Mei|WayOfTheClaw",    sPath, (cfWid *  0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Pounce",          sPath, (cfWid *  1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|JumpKick",        sPath, (cfWid *  2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Garrote",         sPath, (cfWid *  3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|HuntersInstinct", sPath, (cfWid *  4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Jab",             sPath, (cfWid *  5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Lunge",           sPath, (cfWid *  6.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Swipe",           sPath, (cfWid *  7.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|GloryKill",       sPath, (cfWid *  8.0), cfUseHei, cfWid, cfHei, 0)

--Hive Scout
cfUseHei = cfHei * 3.0
ImageLump_Rip(sPrefix .. "Mei|WayOfTheHive", sPath, (cfWid *  0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Sting",        sPath, (cfWid *  1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|FlapJump",     sPath, (cfWid *  2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|ClawKick",     sPath, (cfWid *  3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Coordinate",   sPath, (cfWid *  4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|WaxArmor",     sPath, (cfWid *  5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|ScoutsHonor",  sPath, (cfWid *  6.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|CallForHelp",  sPath, (cfWid *  7.0), cfUseHei, cfWid, cfHei, 0)

--Maid
cfUseHei = cfHei * 4.0
ImageLump_Rip(sPrefix .. "Mei|WayOfTheDead",     sPath, (cfWid *  0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|DusterBlast",      sPath, (cfWid *  1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|TidyUp",           sPath, (cfWid *  2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|TranslucentSmile", sPath, (cfWid *  3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|FirstAid",         sPath, (cfWid *  4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|IcyHand",          sPath, (cfWid *  5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Undying",          sPath, (cfWid *  6.0), cfUseHei, cfWid, cfHei, 0)

--Smartysage
cfUseHei = cfHei * 5.0
ImageLump_Rip(sPrefix .. "Mei|WayOfTheSlime", sPath, (cfWid *  0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Splash",        sPath, (cfWid *  1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Goopshot",      sPath, (cfWid *  2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|FirmUp",        sPath, (cfWid *  3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|JigglyChest",   sPath, (cfWid *  4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Absorb",        sPath, (cfWid *  5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|BigDumbGrin",   sPath, (cfWid *  6.0), cfUseHei, cfWid, cfHei, 0)

--Squeaky Thrall
cfUseHei = cfHei * 0.0
ImageLump_Rip(sPrefix .. "Mei|SpreadRubber", sPath, (cfWid * 13.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Permagrin",    sPath, (cfWid * 14.0), cfUseHei, cfWid, cfHei, 0)

--Zombee
cfUseHei = cfHei * 6.0
ImageLump_Rip(sPrefix .. "Mei|WayOfTheZombee", sPath, (cfWid *  0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Rage",           sPath, (cfWid *  1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Fury",           sPath, (cfWid *  2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Roar",           sPath, (cfWid *  3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|CorruptHoney",   sPath, (cfWid *  4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Mei|Crush",          sPath, (cfWid *  5.0), cfUseHei, cfWid, cfHei, 0)

-- |[ =============================== Ability Icons - Florentina =============================== ]|
--Ability icons for Florentina.
cfWid = 50
cfHei = 50
sPath = sBasePath .. "AbilityIcons_Florentina.png"
sPrefix = "AbilityIco|"

--Merchant
cfUseHei = cfHei * 0.0
ImageLump_Rip(sPrefix .. "Florentina|Botany",        sPath, (cfWid * 0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|CriticalStab",  sPath, (cfWid * 1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|DrippingBlade", sPath, (cfWid * 2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|Intimidate",    sPath, (cfWid * 3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|DrainVitality", sPath, (cfWid * 4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|Regrowth",      sPath, (cfWid * 5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|CruelSlice",    sPath, (cfWid * 6.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|VineWrap",      sPath, (cfWid * 7.0), cfUseHei, cfWid, cfHei, 0)

--Treasure Hunter
cfUseHei = cfHei * 1.0
ImageLump_Rip(sPrefix .. "Florentina|FlorentinasInstinct", sPath, (cfWid * 0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|FromTheShadows",      sPath, (cfWid * 1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|Haymaker",            sPath, (cfWid * 2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|LightStep",           sPath, (cfWid * 3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|PickPocket",          sPath, (cfWid * 4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|PoisonDarts",         sPath, (cfWid * 5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|StickyFingers",       sPath, (cfWid * 6.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|VineWhip",            sPath, (cfWid * 7.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|SmashAndGrab",        sPath, (cfWid * 8.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|WhipTrip",            sPath, (cfWid * 9.0), cfUseHei, cfWid, cfHei, 0)

--Mediator
cfUseHei = cfHei * 2.0
ImageLump_Rip(sPrefix .. "Florentina|FlorentinasWit",  sPath, (cfWid * 0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|VeiledThreat",    sPath, (cfWid * 1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|CalledShot",      sPath, (cfWid * 2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|CounterArgument", sPath, (cfWid * 3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|Encourage",       sPath, (cfWid * 4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|Smug",            sPath, (cfWid * 5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|Insult",          sPath, (cfWid * 6.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|TerriblePun",     sPath, (cfWid * 7.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Florentina|PickMeUp",        sPath, (cfWid * 8.0), cfUseHei, cfWid, cfHei, 0)

-- |[ =============================== Ability Icons - Christine ================================ ]|
--Ability icons for Christine.
cfWid = 50
cfHei = 50
sPath = sBasePath .. "AbilityIcons_Christine.png"
sPrefix = "AbilityIco|"

--Lancer
cfUseHei = 0.0
ImageLump_Rip(sPrefix .. "Christine|MaleTech",      sPath, (cfWid * 0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|FemaleTech",    sPath, (cfWid * 1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|GolemTech",     sPath, (cfWid * 2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|TakePoint",     sPath, (cfWid * 3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|LineFormation", sPath, (cfWid * 4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|Shock",         sPath, (cfWid * 5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|Batter",        sPath, (cfWid * 6.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|Sweep",         sPath, (cfWid * 7.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|Rally",         sPath, (cfWid * 8.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|Puncture",      sPath, (cfWid * 9.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|OfficerCharge", sPath, (cfWid *10.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Christine|Encourage",     sPath, (cfWid *11.0), cfUseHei, cfWid, cfHei, 0)

-- |[ =================================== Ability Icons - 55 =================================== ]|
--Ability icons for 55.
cfWid = 50
cfHei = 50
sPath = sBasePath .. "AbilityIcons_55.png"
sPrefix = "AbilityIco|"

--Algorithms
cfUseHei = 0.0
ImageLump_Rip(sPrefix .. "55|Assault",          sPath, (cfWid * 0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|Support",          sPath, (cfWid * 1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|Restore",          sPath, (cfWid * 2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|CombatRoutines",   sPath, (cfWid * 3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|HighPowerShot",    sPath, (cfWid * 4.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|Repair",           sPath, (cfWid * 5.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|HyperRepair",      sPath, (cfWid * 6.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|WideLensShot",     sPath, (cfWid * 7.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|RubberSlugShot",   sPath, (cfWid * 8.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|DisruptorBlast",   sPath, (cfWid * 9.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|SpotlessProtocol", sPath, (cfWid *10.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "55|ImplosionGrenade", sPath, (cfWid *11.0), cfUseHei, cfWid, cfHei, 0)

-- |[ ================================= Ability Icons - JX-101 ================================= ]|
--Ability icons for JX-101.
cfWid = 50
cfHei = 50
sPath = sBasePath .. "AbilityIcons_SX399.png"
sPrefix = "AbilityIco|"

--No class
cfUseHei = 0.0
ImageLump_Rip(sPrefix .. "JX101|Techniques",      sPath, (cfWid * 0.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "JX101|ThunderboltSlug", sPath, (cfWid * 1.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "JX101|PinningFire",     sPath, (cfWid * 2.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "JX101|SniperShot",      sPath, (cfWid * 3.0), cfUseHei, cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "JX101|BattleCry",       sPath, (cfWid * 4.0), cfUseHei, cfWid, cfHei, 0)

-- |[ ================================= Ability Icons - SX-399 ================================= ]|
--Ability icons for SX-399. Borrows some from JX-101.
cfWid = 50
cfHei = 50
sPath = sBasePath .. "AbilityIcons_SX399.png"
sPrefix = "AbilityIco|"

--Warlord
cfUseHei = cfHei * 1.0
ImageLump_Rip(sPrefix .. "SX399|MeltaBlast", sPath, (cfWid * 0.0), cfUseHei, cfWid, cfHei, 0)

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()