--[ ======================================= Base Menu UI ======================================== ]
--Base menu. Used whenever the player presses cancel on the overworld.
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/UIAdvMenuBase.slf")
ImageLump_SetCompression(1)

--[ ========================================== Ripping ========================================== ]
local sPrefix = "AdvBaseMenu|"
local saNames = {"ComboBar", "Footer", "Header", "HealthBarBack", "HealthBarFill", "HealthBarFrame", "HelpBacking", "InventoryBacks", "InventoryBanners", "NameBar", "PlatinaBanner", "PortraitBack", "PortraitFront", "PortraitMask", "QuitBacking"}
local saPaths = {"ComboBar", "Footer", "Header", "HealthBarBack", "HealthBarFill", "HealthBarFrame", "HelpBacking", "InventoryBacks", "InventoryBanners", "NameBar", "PlatinaBanner", "PortraitBack", "PortraitFront", "PortraitMask", "QuitBacking"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end

--[Text Only]
ImageLump_RipData(sPrefix .. "PlatinaText", sBasePath .. "PlatinaText.png", 0, 0, -1, -1, 0)

--[Icons]
local iIconW = 32
local iIconH = 32
local sIconPath = sBasePath .. "MenuIcons.png"
ImageLump_Rip(sPrefix .. "ICO|Inventory", sIconPath, iIconW*0, iIconH*0, iIconW, iIconH, 0)
ImageLump_Rip(sPrefix .. "ICO|Equipment", sIconPath, iIconW*1, iIconH*0, iIconW, iIconH, 0)
ImageLump_Rip(sPrefix .. "ICO|DoctorBag", sIconPath, iIconW*2, iIconH*0, iIconW, iIconH, 0)
ImageLump_Rip(sPrefix .. "ICO|Status",    sIconPath, iIconW*3, iIconH*0, iIconW, iIconH, 0)
ImageLump_Rip(sPrefix .. "ICO|Skills",    sIconPath, iIconW*4, iIconH*0, iIconW, iIconH, 0)
ImageLump_Rip(sPrefix .. "ICO|Map",       sIconPath, iIconW*5, iIconH*0, iIconW, iIconH, 0)
ImageLump_Rip(sPrefix .. "ICO|Options",   sIconPath, iIconW*6, iIconH*0, iIconW, iIconH, 0)
ImageLump_Rip(sPrefix .. "ICO|Quit",      sIconPath, iIconW*7, iIconH*0, iIconW, iIconH, 0)
ImageLump_Rip(sPrefix .. "ICO|Frame",     sIconPath, iIconW*0, iIconH*1, iIconW, iIconH, 0)

--[ ========================================== Clean Up ========================================= ]
--Finish
SLF_Close()


