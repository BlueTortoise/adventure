--[ ============================= Adventure Menu - Field Abilities ============================== ]
--Field Abilities UI.
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/UIAdvMenuFieldAbilities.slf")
ImageLump_SetCompression(1)

--[ ========================================== Ripping ========================================== ]
ImageLump_Rip("AdvMenuFieldAbility|Header", sBasePath .. "Header.png", 0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuFieldAbility|Footer", sBasePath .. "Footer.png", 0, 0, -1, -1, 0)

--[ ========================================== Clean Up ========================================= ]
--Finish
SLF_Close()