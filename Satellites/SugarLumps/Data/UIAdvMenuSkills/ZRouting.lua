--[ ================================== Adventure Menu - Skills ================================== ]
--Skills UI. Borrows from the combat UI.
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/UIAdvMenuSkills.slf")
ImageLump_SetCompression(1)

--[ ========================================== Ripping ========================================== ]
ImageLump_Rip("AdvMenuSkill|ClassActive",     sBasePath .. "ClassActive.png",     0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuSkill|ClassMaster",     sBasePath .. "ClassMaster.png",     0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuSkill|Header",          sBasePath .. "Header.png",          0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuSkill|Highlight",       sBasePath .. "Highlight.png",       0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuSkill|PaneJobs",        sBasePath .. "PaneJobs.png",        0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuSkill|PaneSkills",      sBasePath .. "PaneSkills.png",      0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuSkill|ScrollbarFront",  sBasePath .. "ScrollbarFront.png",  0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuSkill|ScrollbarJobs",   sBasePath .. "ScrollbarJobs.png",   0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuSkill|ScrollbarSkills", sBasePath .. "ScrollbarSkills.png", 0, 0, -1, -1, 0)
ImageLump_Rip("AdvMenuSkill|SkillEquipped",   sBasePath .. "SkillEquipped.png",   0, 0, -1, -1, 0)

--[ ========================================== Clean Up ========================================= ]
--Finish
SLF_Close()