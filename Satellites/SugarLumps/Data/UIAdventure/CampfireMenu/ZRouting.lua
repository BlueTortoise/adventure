--[ ======================================= Campfire UI ======================================== ]
--Menu that appears at a save point/campfire/heating coil.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvCampfire|"
local saNames = {"BaseBot", "BaseMid", "BaseTop", "Button", "Comparison", "DescriptionBox", "Header", "MapPin", "MapPinSelected"}
local saPaths = {"BaseBot", "BaseMid", "BaseTop", "Button", "Comparison", "DescriptionBox", "Header", "MapPin", "MapPinSelected"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end

--Icons
local iIconSizeX = 32
local iIconSizeY = 32
local sIconPath = sBasePath .. "CampIcons.png"
ImageLump_Rip(sPrefix .. "ICO|Rest",      sIconPath, iIconSizeX * 0, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Chat",      sIconPath, iIconSizeX * 1, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Save",      sIconPath, iIconSizeX * 2, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Skills",    sIconPath, iIconSizeX * 3, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Transform", sIconPath, iIconSizeX * 4, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Warp",      sIconPath, iIconSizeX * 5, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Relive",    sIconPath, iIconSizeX * 6, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Costume",   sIconPath, iIconSizeX * 7, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Password",  sIconPath, iIconSizeX * 8, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Frame",     sIconPath, iIconSizeX * 0, iIconSizeY * 1, iIconSizeX, iIconSizeY, 0)

--Region Icons
ImageLump_Rip(sPrefix .. "ICO|Trannadar",  sIconPath, iIconSizeX * 0, iIconSizeY * 2, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Regulus",    sIconPath, iIconSizeX * 1, iIconSizeY * 2, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Cryogenics", sIconPath, iIconSizeX * 2, iIconSizeY * 2, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Equinox",    sIconPath, iIconSizeX * 3, iIconSizeY * 2, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|LRTE",       sIconPath, iIconSizeX * 4, iIconSizeY * 2, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|LRTW",       sIconPath, iIconSizeX * 5, iIconSizeY * 2, iIconSizeX, iIconSizeY, 0)
ImageLump_Rip(sPrefix .. "ICO|Biolabs",    sIconPath, iIconSizeX * 6, iIconSizeY * 2, iIconSizeX, iIconSizeY, 0)

--Chat Icons. 96x96, no scaling.
local iChatIconSizeX = 96
local iChatIconSizeY = 96
local sChatIconPath = sBasePath .. "ChatIcons.png"
ImageLump_Rip(sPrefix .. "CHATICO|Florentina", sChatIconPath, iChatIconSizeX * 0, iChatIconSizeY * 0, iChatIconSizeX, iChatIconSizeY, 0)
ImageLump_Rip(sPrefix .. "CHATICO|Maram",      sChatIconPath, iChatIconSizeX * 1, iChatIconSizeY * 0, iChatIconSizeX, iChatIconSizeY, 0)
--Chapter 2
--Chapter 3
--Chapter 4
ImageLump_Rip(sPrefix .. "CHATICO|55",     sChatIconPath, iChatIconSizeX * 0, iChatIconSizeY * 4, iChatIconSizeX, iChatIconSizeY, 0)
ImageLump_Rip(sPrefix .. "CHATICO|SX399",  sChatIconPath, iChatIconSizeX * 1, iChatIconSizeY * 4, iChatIconSizeX, iChatIconSizeY, 0)
ImageLump_Rip(sPrefix .. "CHATICO|Sophie", sChatIconPath, iChatIconSizeX * 2, iChatIconSizeY * 4, iChatIconSizeX, iChatIconSizeY, 0)
ImageLump_Rip(sPrefix .. "CHATICO|JX101",  sChatIconPath, iChatIconSizeX * 3, iChatIconSizeY * 4, iChatIconSizeX, iChatIconSizeY, 0)
--Chapter 6
--Bearers (not actually compressed yet)
ImageLump_Rip(sPrefix .. "CHATICO|Frame",  sChatIconPath, iChatIconSizeX * 0, iChatIconSizeY * 7, iChatIconSizeX, iChatIconSizeY, 0)


