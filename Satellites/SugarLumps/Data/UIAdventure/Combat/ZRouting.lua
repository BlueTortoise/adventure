--[ ========================================= Combat UI ========================================= ]
--WAAAAAAAAAAAAAAAAAARRRRRRRRRRRRRR!
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvCombat|"
local saNames = {"AllyComboBar", "AllyFrame", "AllyHealthBar", "AllyHealthBarFill", "AllyHealthBarFrame", "AllyPortraitMask", "EnemyComboBar", "EnemyHealthBarFill", "EnemyHealthBarFrame", "EnemyHealthBarUnder", "LowerActionBtn", "MainComboBar", "MainHealthBarBack", "MainHealthBarFill", "MainHealthBarFrame", "MainNameBar", "MainPortraitMask", "MainPortraitRing", "MainPortraitRingBack", "MainTopicFrames", "TurnOrderBack", "TurnOrderCircle"}
local saPaths = {"AllyComboBar", "AllyFrame", "AllyHealthBar", "AllyHealthBarFill", "AllyHealthBarFrame", "AllyPortraitMask", "EnemyComboBar", "EnemyHealthBarFill", "EnemyHealthBarFrame", "EnemyHealthBarUnder", "LowerActionBtn", "MainComboBar", "MainHealthBarBack", "MainHealthBarFill", "MainHealthBarFrame", "MainNameBar", "MainPortraitMask", "MainPortraitRing", "MainPortraitRingBack", "MainTopicFrames", "TurnOrderBack", "TurnOrderCircle"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end

--[Defeat]
ImageLump_Rip("AdvCombat|Defeat0", sBasePath .. "Defeat_0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("AdvCombat|Defeat1", sBasePath .. "Defeat_1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("AdvCombat|Defeat2", sBasePath .. "Defeat_2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("AdvCombat|Defeat3", sBasePath .. "Defeat_3.png", 0, 0, -1, -1, 0)
ImageLump_Rip("AdvCombat|Defeat4", sBasePath .. "Defeat_4.png", 0, 0, -1, -1, 0)
ImageLump_Rip("AdvCombat|Defeat5", sBasePath .. "Defeat_5.png", 0, 0, -1, -1, 0)

--[ ======================================= Turn Portraits ====================================== ]
--[Ripper Function]
--Automated, creates images named "PorSml|Alraune" and such. Uses tables.
local fnRipImages = function(sPrefix, sRipPath, saRipNames, iXValue, iYValue, iWid, iHei, sCharPrefix)
	
	--Arg check
	if(sPrefix    == nil) then return end
	if(sRipPath   == nil) then return end
	if(saRipNames == nil) then return end
	if(iXValue    == nil) then return end
	if(iYValue    == nil) then return end
	
	--sCharPrefix can be nil. It's only used for ripping the main characters.
	
	--Let 'er rip!
	local i = 0
	while(saRipNames[i+1] ~= nil) do
		
		--If the name is "SKIP", then don't rip here. This is used because slots are empty until later in some cases.
		if(saRipNames[i+1] == "SKIP") then
		else
		
			--Not using a character prefix:
			if(sCharPrefix == nil) then
				ImageLump_Rip(sPrefix .. saRipNames[i+1], sRipPath, iXValue + (iWid * i), iYValue, iWid, iHei, 0)
			
			--Character prefix. Makes names like "PorSml|Christine_Alraune"
			else
				ImageLump_Rip(sPrefix .. sCharPrefix .. saRipNames[i+1], sRipPath, iXValue + (iWid * i), iYValue, iWid, iHei, 0)
			end
			
		end
		
		--Next.
		i = i + 1
	end
end

--[Character Ripping]
----Constants
local ciWid = 80
local ciHei = 60
local sRipPath = sBasePath .. "TurnPortraitsSheet.png"

--Christine's row.
local saRipNames = {"Human", "SKIP", "SKIP", "SKIP", "SKIP", "SKIP", "Male", "Golem", "GolemDress", "Latex", "Darkmatter", "Electrosprite", "SteamDroid", "Eldritch", "Raiju", "Doll"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 0, ciWid, ciHei, "Christine_")

--Mei's row.
saRipNames = {"Human", "Alraune", "Bee", "Ghost", "Slime", "Werecat"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 2, ciWid, ciHei, "Mei_")

--Other Party Members. They don't have multiple forms.
saRipNames = {"Florentina", "55", "JX-101", "Aquillia", "SX-399"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 6, ciWid, ciHei)

--[Enemy Ripping]
sRipPath = sBasePath .. "TurnPortraitsSheetEnemies.png"
saRipNames = {"Alraune", "Bee", "CultistF", "CultistM", "Ghost", "Slime", "Werecat", "Zombee", "SkullCrawler", "Arachnophelia", "Infirm"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 0, ciWid, ciHei)

--5th Line: Chapter 5 Enemies
saRipNames = {"Doll", "Electrosprite", "Golem", "GolemLord", "LatexDrone", "Raiju", "Scraprat", "SecurityWrecked", "DarkmatterGirl", "SteamDroid", "Dreamer", "Vivify", "56", "TechMonstrosity", "VoidRift", "Horrible", "Serenity", "609144", "Raibie", "BandageGoblin", "Hoodie", "InnGeisha"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 4, ciWid, ciHei)