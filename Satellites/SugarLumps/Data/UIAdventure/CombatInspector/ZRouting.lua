--[ ==================================== Combat Inspector UI ==================================== ]
--Doot doo doo da doo INSPECTOR COMBAT.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvComInsp|"
local saNames = {"CharacterBack", "EffectInset", "Frames"}
local saPaths = {"CharacterBack", "EffectInset", "Frames"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end