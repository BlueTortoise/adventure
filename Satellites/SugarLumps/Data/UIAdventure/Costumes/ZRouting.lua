--[ ======================================== Costumes UI ======================================== ]
--An interface that allows the player to specify which costumes they want characters to wear.
--The UI is a character selector and a costume selector, using sprites to visually indicate who is who.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvCostume|"
local saNames = {"FrameLower", "FrameLowerActive", "FrameUpper", "Header", "SelectorLower", "SelectorUpper"}
local saPaths = {"FrameLower", "FrameLowerActive", "FrameUpper", "Header", "SelectorLower", "SelectorUpper"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end