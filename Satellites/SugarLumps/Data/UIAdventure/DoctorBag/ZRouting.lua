--[ ======================================== Victory UI ========================================= ]
--You killed all the baddies. Here's what you got.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvDoctor|"
local saNames = {"BackButton", "BarFill", "BarFrame", "BarHeader", "HealAllBtn", "MenuBack", "MenuHeaderA", "MenuHeaderB", "MenuInset", "OptionsDeactivated", "PortraitBtn", "PortraitMask"}
local saPaths = {"BackButton", "BarFill", "BarFrame", "BarHeader", "HealAllBtn", "MenuBack", "MenuHeaderA", "MenuHeaderB", "MenuInset", "OptionsDeactivated", "PortraitBtn", "PortraitMask"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
