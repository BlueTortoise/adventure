--[ ======================================= Equipment UI ======================================== ]
--Swap out yer kit.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvEquipment|"
local saNames = {"Frames", "FramesScroll", "NamePlate", "PortraitMaskA", "PortraitMaskB", "PortraitMaskC", "SocketA", "SocketB", "SocketC", "SocketScroll"}
local saPaths = {"Frames", "FramesScroll", "NamePlate", "PortraitMaskA", "PortraitMaskB", "PortraitMaskC", "SocketA", "SocketB", "SocketC", "SocketScroll"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
