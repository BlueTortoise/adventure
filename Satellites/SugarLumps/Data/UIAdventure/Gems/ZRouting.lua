--[ ======================================= Gemcutting UI ======================================= ]
--UI used when upgrading your gems.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvGemCutter|"
local saNames = {"Banner", "Base", "ButtonBack", "GemSelectBack", "GemSelectFrame", "InfoBox", "PlatinaBanner"}
local saPaths = {"Banner", "Base", "ButtonBack", "GemSelectBack", "GemSelectFrame", "InfoBox", "PlatinaBanner"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
