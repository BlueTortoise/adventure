-- |[ ======================================= Item Icons ======================================= ]|
--Icons used for items. Items are organized by row based on chapter.
local sBasePath = fnResolvePath()

--Variable setup.
local cfWid = 84
local cfHei = 84
local sPrefix = "AdvItemIco|"
local sPath = sBasePath .. "ItemSheet.png"

--Chapter 1
ImageLump_Rip(sPrefix .. "WeaponMei",        sPath, (cfWid * 0.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "RunestoneMei",     sPath, (cfWid * 1.0), (cfHei * 0.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "WeaponFlorentina", sPath, (cfWid * 2.0), (cfHei * 0.0), cfWid, cfHei, 0)

--Chapter 2
--Chapter 3
--Chapter 4
--Chapter 5
ImageLump_Rip(sPrefix .. "WeaponChristine",    sPath, (cfWid * 0.0), (cfHei * 4.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "RunestoneChristine", sPath, (cfWid * 1.0), (cfHei * 4.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Weapon55",           sPath, (cfWid * 2.0), (cfHei * 4.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "WeaponJX101",        sPath, (cfWid * 3.0), (cfHei * 4.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "WeaponSX399",        sPath, (cfWid * 4.0), (cfHei * 4.0), cfWid, cfHei, 0)

--Chapter 6
--Armors
ImageLump_Rip(sPrefix .. "ArmorLight",  sPath, (cfWid * 0.0), (cfHei * 6.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "ArmorMedium", sPath, (cfWid * 1.0), (cfHei * 6.0), cfWid, cfHei, 0)

--Accessories
ImageLump_Rip(sPrefix .. "AccessoryRing",   sPath, (cfWid * 0.0), (cfHei * 7.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "AccessoryBracer", sPath, (cfWid * 1.0), (cfHei * 7.0), cfWid, cfHei, 0)

--Items
ImageLump_Rip(sPrefix .. "ItemPotionRed",    sPath, (cfWid * 0.0), (cfHei * 8.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "ItemPotionBlue",   sPath, (cfWid * 1.0), (cfHei * 8.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "ItemPotionViolet", sPath, (cfWid * 2.0), (cfHei * 8.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "ItemPotionGreen",  sPath, (cfWid * 3.0), (cfHei * 8.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "ItemInjector",     sPath, (cfWid * 4.0), (cfHei * 8.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "ItemHacking",      sPath, (cfWid * 5.0), (cfHei * 8.0), cfWid, cfHei, 0)

--Adamantite
ImageLump_Rip(sPrefix .. "AdamantitePowder", sPath, (cfWid * 0.0), (cfHei * 9.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "Unequip",          sPath, (cfWid * 1.0), (cfHei * 9.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "JunkFantasy",      sPath, (cfWid * 2.0), (cfHei * 9.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "JunkFuture",       sPath, (cfWid * 3.0), (cfHei * 9.0), cfWid, cfHei, 0)

--Gems
ImageLump_Rip(sPrefix .. "GemYellow", sPath, (cfWid * 0.0), (cfHei * 10.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "GemPurple", sPath, (cfWid * 1.0), (cfHei * 10.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "GemBlue",   sPath, (cfWid * 2.0), (cfHei * 10.0), cfWid, cfHei, 0)
ImageLump_Rip(sPrefix .. "GemPink",   sPath, (cfWid * 3.0), (cfHei * 10.0), cfWid, cfHei, 0)

-- |[ ==================================== 22px Item Icons ===================================== ]|
--[22px Item Icons]
--Variables.
cfWid = 22
cfHei = 22
sPrefix = "AdvItem22Px|"
sPath = sBasePath .. "22pxIcons.png"

--Array Setup
local saArray = {}
saArray[ 1] = {"WepMeiA", "WepMeiB", "WepMeiC", "WepFlorentinaA", "WepFlorentinaB", "WepFlorentinaC"}
saArray[ 2] = {"SKIP"}
saArray[ 3] = {"SKIP"}
saArray[ 4] = {"SKIP"}
saArray[ 5] = {"WepChristineA", "WepChristineB", "WepChristineC", "Wep55A", "Wep55B", "Wep55C", "WepJX101", "WepSX399A", "WepSX399B", "WepSX399C"}
saArray[ 6] = {"SKIP"}
saArray[ 7] = {"RuneMei", "SKIP", "SKIP", "SKIP", "RuneChristine", "SKIP", "SystemUnequip"}
saArray[ 8] = {"ArmGenLight", "ArmGenMedium", "ArmGenHeavy"}
saArray[ 9] = {"AccRing", "AccBracer", "AccBoots", "AccGloves", "SKIP", "SKIP", "AccRingBrnA", "AccRingBrnB", "AccRingBrnC", "AccRingBrnD"}
saArray[10] = {"ItemPotionRed", "ItemMedkit", "SKIP", "SKIP", "SKIP", "SKIP", "AccRingSlvA", "AccRingSlvB", "AccRingSlvC", "AccRingSlvD"}
saArray[11] = {"ItemInjector", "ItemTech", "ItemScope", "ItemSuppressor", "ItemFlashbang", "SKIP", "AccRingGldA", "AccRingGldB", "AccRingGldC", "AccRingGldD"}
saArray[12] = {"GemSlot", "GemYellow", "GemPurple", "GemBlue", "GemPink"}
saArray[13] = {"JunkA", "JunkB"}
saArray[14] = {"PepperPie"}
saArray[15] = {"Crowbar", "KeyBlack", "KeySilver", "KeyBronze", "Hacksaw", "BoatOars", "ComputerChip"}
saArray[16] = {"BookPurple", "BookBlue", "BookRed", "BookOrange", "BookGreen"}
saArray[17] = {"JarGreen", "JarYellow", "JarRed", "JarPurple", "JarBlue"}
saArray[18] = {"FlowerRed", "FlowerWhite", "FlowerPurple", "FlowerBlue", "FlowerPink"}
saArray[19] = {"AdmPowder", "AdmFlakes", "AdmShards", "AdmPieces", "AdmChunks", "AdmOre"}

--[Advanced Gems]
--GRBOVY
saArray[20] = {"Gem1|R",   "Gem1|V",   "Gem1|Y",   "Gem1|O",   "Gem1|G",   "Gem1|B"}
saArray[21] = {"Gem2|RV",  "Gem2|VY",  "Gem2|OV",  "Gem2|GV",  "Gem2|BV",  "Gem2|RY",  "Gem2|RO",  "Gem2|GR",  "Gem2|RB",  "Gem2|OY",  "Gem2|GY",  "Gem2|BY",  "Gem2|GO",  "Gem2|BO",  "Gem2|GB"}
saArray[22] = {"Gem3|RVY", "Gem3|ROV", "Gem3|GRV", "Gem3|RBV", "Gem3|OVY", "Gem3|GVY", "Gem3|BVY", "Gem3|BOV", "Gem3|GBV", "Gem3|ROY", "Gem3|GRY", "Gem3|RBY", "Gem3|GRO", "Gem3|RBO", "Gem3|GRO", "Gem3|GRB", "Gem3|GOY", "Gem3|BOY", "Gem3|GBO", "Gem3|GBY"}
saArray[23] = {"Gem4|ROVY", "Gem4|GRVY", "Gem4|RBVY", "Gem4|GROV", "Gem4|RBOV", "Gem4|GRBV", "Gem4|GOVY", "Gem4|BOVY", "Gem4|GBVY", "Gem4|GBOV", "Gem4|GROY", "Gem4|RBOY", "Gem4|GRBY", "Gem4|GRBO", "Gem4|GBOY"}
saArray[24] = {"Gem5|RBOVY", "Gem5|GROVY", "Gem5|GRBVY", "Gem5|GRBOV", "Gem5|GRBOY", "Gem5|GBOVY", "Gem6"}

--[Ripping Loop]
i = 1
while(saArray[i] ~= nil) do
    local p = 1
    while(saArray[i][p] ~= nil) do
        
        --"SKIP" is ignored:
        if(saArray[i][p] == "SKIP") then
            
        --Rip.
        else
            ImageLump_Rip(sPrefix .. saArray[i][p], sPath, (p-1) * cfWid, (i - 1) * cfHei, cfWid, cfHei, 0)
        end
        p = p + 1
    end
    i = i + 1
end


-- |[ ========================================== Other ========================================= ]|
--[Item Backing]
--These borders are used in various UI spots. Different borders denote different properties.
sPrefix = "AdvItemIco|"
ImageLump_Rip(sPrefix .. "BackingNeutral", sBasePath .. "ItemBackingNeutral.png", (cfWid * 0.0), (cfHei * 8.0), cfWid, cfHei, 0)

--[Character Faces]
ImageLump_Rip(sPrefix .. "CharacterFaces", sBasePath .. "CharacterFaces.png", 0, 0, -1, -1, 0)

--[Ability Icons]
--Used during combat when the player is selecting an action. Not every icon is unique.
cfWid = 50
cfHei = 50
sPath = sBasePath .. "AbilityIcons.png"
sPrefix = "AdvAbilityIco|"

--Ability Selection
ImageLump_Rip(sPrefix .. "Select", sBasePath .. "AbilitySelect.png", 0.0, 0.0, -1, -1, 0)
