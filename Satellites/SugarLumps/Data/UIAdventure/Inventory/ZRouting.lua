--[ ======================================= Inventory UI ======================================== ]
--Examine yer kit in great detail.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvInventory|"
local saNames = {"Frames", "FramesNew", "ScrollbarBack", "ScrollbarFront"}
local saPaths = {"Frames", "FramesNew", "ScrollbarBack", "ScrollbarFront"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end