--[ ========================================= Options UI ======================================== ]
--Change them game settin's.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvOptions|"
local saNames = {"Bars", "ConfirmButtons", "Header", "MenuBack", "MenuInsert", "SliderButtons", "Sliders", "TextBoxes"}
local saPaths = {"Bars", "ConfirmButtons", "Header", "MenuBack", "MenuInsert", "SliderButtons", "Sliders", "TextBoxes"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
