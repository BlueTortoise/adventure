--[ ========================================== Save UI ========================================== ]
--Used to save the game at campfires.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvSaveMenu|"
local saNames = {"ArrowLft", "ArrowRgt", "Header", "NewFileButton", "GridBackingCh0"}
local saPaths = {"ArrowLft", "ArrowRgt", "Header", "NewFileButton", "GridBackingCh0"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end