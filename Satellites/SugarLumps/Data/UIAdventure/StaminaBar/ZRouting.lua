--[Stamina Bar]
--the small bar that appears in the top-left corner of the screen when the player is in control.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvStamina|"
local saNames = {"StaminaBarMiddle", "StaminaBarOver", "StaminaBarUnder"}
local saPaths = {"StaminaBarMiddle", "StaminaBarOver", "StaminaBarUnder"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
