--[Status]
--The status screen, exactly what you'd expect. Every image is ripped in a simple loop.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvStatus|"
local saNames = {"AbilityInspector", "BackgroundFill", "BannerTop", "BtnBack", "CharInfo", "CharOverlayMask", "CharOverlayMaskLft", "EXPBarFill", "EXPBarFrame", "HealthBarFill", "HealthBarFrame", "HealthBarUnder", "CharOverlayMaskRgt", "Inventory", "NavButtons"}
local saPaths = {"AbilityInspector", "BackgroundFill", "BannerTop", "BtnBack", "CharInfo", "CharOverlayMask", "CharOverlayMaskLft", "EXPBarFill", "EXPBarFrame", "HealthBarFill", "HealthBarFrame", "HealthBarUnder", "CharOverlayMaskRgt", "Inventory", "NavButtons"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
