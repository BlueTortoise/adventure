--[ ======================================== Victory UI ========================================= ]
--You killed all the baddies. Here's what you got.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvVictory|"
local saNames = {"Banner", "BannerHeader", "BannerBig", "DoctorBarBack", "DoctorBarFill", "DoctorBarFrame", "ExpBack", "ExpBarFill", "ExpBarFrame", "ExpHeader", "ExpLevel", "ExpNumber", "ExpPortrait", "ExpPortraitMask", "MenuBack", "MenuContent", "MenuHeaderA", "MenuHeaderB", "MenuInsert", "OkayButton"}
local saPaths = {"Banner", "BannerHeader", "BannerBig", "DoctorBarBack", "DoctorBarFill", "DoctorBarFrame", "ExpBack", "ExpBarFill", "ExpBarFrame", "ExpHeader", "ExpLevel", "ExpNumber", "ExpPortrait", "ExpPortraitMask", "MenuBack", "MenuContent", "MenuHeaderA", "MenuHeaderB", "MenuInsert", "OkayButton"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
