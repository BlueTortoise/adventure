--[World Overlays]
--Used for areas like deep forests or underwater, these overlays provide some interesting flavour
-- to the game's visual style.
local sBasePath = fnResolvePath()

--[Utility]
ImageLump_Rip("Overlay|Fog",        sBasePath .. "FogOverlay.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|ForestA",    sBasePath .. "ForestOverlayA.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|ForestB",    sBasePath .. "ForestOverlayB.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|ForestC",    sBasePath .. "ForestOverlayC.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|Water",      sBasePath .. "WaterOverlay.png",      0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|Underwater", sBasePath .. "UnderwaterOverlay.png", 0, 0, -1, -1, 0)

--[Backgrounds]
ImageLump_Rip("Underlay|Clouds",      sBasePath .. "Clouds.png",      0, 0, -1, -1, 0)
ImageLump_Rip("Underlay|UmumAsru",    sBasePath .. "UmumAsru.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Underlay|ServerBanks", sBasePath .. "ServerBanks.png", 0, 0, -1, -1, 0)