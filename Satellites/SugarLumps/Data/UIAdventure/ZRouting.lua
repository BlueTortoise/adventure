--[UI Adventure]
--UI elements used for Adventure Mode.
SLF_Open("Output/UIAdventure.slf")
ImageLump_SetCompression(1)

--Routing. Call each of the subfolders.
local sBasePath = fnResolvePath()
--LM_ExecuteScript(sBasePath .. "Basemenu/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "CampfireMenu/ZRouting.lua")
--LM_ExecuteScript(sBasePath .. "Combat/ZRouting.lua")
--LM_ExecuteScript(sBasePath .. "CombatInspector/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Costumes/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Dialogue/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "DoctorBag/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Equipment/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Gems/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Icons/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Inventory/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Options/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Save/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "StaminaBar/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Status/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Vendor/ZRouting.lua")
--LM_ExecuteScript(sBasePath .. "Victory/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "WorldOverlays/ZRouting.lua")

--Finish
SLF_Close()