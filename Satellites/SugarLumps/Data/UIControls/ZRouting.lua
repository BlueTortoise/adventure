--[ ======================================== Controls UI ======================================== ]
--Stores images that related to the keyboard/mouse/joypad.
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/UIControls.slf")
ImageLump_SetCompression(1)

--[ ========================================= Keyboard ========================================== ]
local sImgPath = sBasePath .. "Keyboard.png"
ImageLump_Rip("Keyboard|Tilde", sImgPath,   0,  40, 19, 19, 0)
ImageLump_Rip("Keyboard|1",     sImgPath,  20,  40, 19, 19, 0)
ImageLump_Rip("Keyboard|2",     sImgPath,  40,  40, 19, 19, 0)
ImageLump_Rip("Keyboard|3",     sImgPath,  60,  40, 19, 19, 0)
ImageLump_Rip("Keyboard|4",     sImgPath,  80,  40, 19, 19, 0)
ImageLump_Rip("Keyboard|5",     sImgPath, 100,  40, 19, 19, 0)

ImageLump_Rip("Keyboard|Tab",   sImgPath,   0,  60, 19, 19, 0)
ImageLump_Rip("Keyboard|E",     sImgPath,  31,  60, 19, 19, 0)
ImageLump_Rip("Keyboard|Q",     sImgPath,  52,  60, 19, 19, 0)

ImageLump_Rip("Keyboard|Shift", sImgPath,   0, 100, 44, 19, 0)
ImageLump_Rip("Keyboard|Z",     sImgPath,  45, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|X",     sImgPath,  65, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|C",     sImgPath,  85, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|V",     sImgPath, 105, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|B",     sImgPath, 125, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|N",     sImgPath, 145, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|M",     sImgPath, 165, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|Comma", sImgPath, 185, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|Period",sImgPath, 205, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|Slash", sImgPath, 225, 100, 19, 19, 0)

ImageLump_Rip("Keyboard|Ctrl",  sImgPath,   0, 120, 36, 19, 0)
ImageLump_Rip("Keyboard|Alt",   sImgPath,  37, 120, 30, 19, 0)
ImageLump_Rip("Keyboard|Space", sImgPath,  68, 120, 44, 19, 0)

ImageLump_Rip("Keyboard|Error", sImgPath,  62,   0, 28, 19, 0)
    
ImageLump_Rip("Keyboard|ArrU", sImgPath, 265,  80, 19, 19, 0)
ImageLump_Rip("Keyboard|ArrL", sImgPath, 245, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|ArrD", sImgPath, 265, 100, 19, 19, 0)
ImageLump_Rip("Keyboard|ArrR", sImgPath, 285, 100, 19, 19, 0)

ImageLump_Rip("Keyboard|F1", sImgPath,  0, 140, 19, 19, 0)
ImageLump_Rip("Keyboard|F2", sImgPath, 20, 140, 21, 19, 0)
ImageLump_Rip("Keyboard|F3", sImgPath, 42, 140, 20, 19, 0)
ImageLump_Rip("Keyboard|F4", sImgPath, 63, 140, 20, 19, 0)
ImageLump_Rip("Keyboard|F5", sImgPath, 84, 140, 20, 19, 0)

--[ ========================================== Clean Up ========================================= ]
--Finish
SLF_Close()