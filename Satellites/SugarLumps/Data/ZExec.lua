--[Image Compression Execution]
--Execs image compression for your project.
Debug_PushPrint(false, "Beginning image compression\n")

--Debug flags.
local sCurrentPath = fnResolvePath()

--Global Flags
gciNoTransparencies = 16 --SugarLumps will ignore the transparent grey/pink colours

--[Automated Compression Function]
local fnCompressFolder = function(sFolderName, sExecFile)

	--Setup
	local bCompressedAnything = false
	local sCurrentPath = fnResolvePath()

	--Push the stack.
	TS_PushStack()

		--Get starting data.
		TS_LoadFrom(sCurrentPath .. sFolderName .. ".log")

		--Scan for new files/modifications/removals.
		local bHasDirectoryChanged = TS_ScanDirectory(sCurrentPath .. sFolderName .. "/")
		if(bHasDirectoryChanged) then
			--Compress here.
			io.write(" " .. sFolderName .. ": Files have changed - ")
			--io.write("Exec: " .. sCurrentPath .. sFolderName .. "/" .. sExecFile .. ".lua" .. "\n")
			LM_ExecuteScript(sCurrentPath .. sFolderName .. "/" .. sExecFile .. ".lua")
			io.write("done.\n")
			bCompressedAnything = true
			
			--Save a log file.
			TS_PrintTo(sCurrentPath .. sFolderName .. ".log")
		else
			io.write(" " .. sFolderName .. ": Files have not changed.\n")
		end
	
	--Clean up.
	TS_PopStack()
	return bCompressedAnything
end

--[Execution]
--Setup.
local baCompressionFlags = {}

--Auto-exec the folders.
gbUseLowDefinition = false
gbUseEnhancedGraphics = false
ImageLump_SetAllowLineCompression(true)
baCompressionFlags[ 1] = fnCompressFolder("CombatAnimations", "ZRouting")
baCompressionFlags[ 2] = fnCompressFolder("ElectrospriteAdventure", "ZRouting")
baCompressionFlags[ 3] = fnCompressFolder("MapAnimations", "ZRouting")
baCompressionFlags[ 4] = fnCompressFolder("Maps", "ZRouting")
baCompressionFlags[ 5] = fnCompressFolder("Sprites", "ZRouting")
baCompressionFlags[ 6] = fnCompressFolder("UIAdventure", "ZRouting")
baCompressionFlags[ 7] = fnCompressFolder("UIAdvCombat", "ZRouting")
baCompressionFlags[ 8] = fnCompressFolder("UIAdvMenuBase", "ZRouting")
baCompressionFlags[ 9] = fnCompressFolder("UIAdvMenuFieldAbilities", "ZRouting")
baCompressionFlags[10] = fnCompressFolder("UIAdvMenuSkills", "ZRouting")
baCompressionFlags[11] = fnCompressFolder("UIControls", "ZRouting")
baCompressionFlags[12] = fnCompressFolder("UITextAdventure", "ZRouting")
baCompressionFlags[13] = fnCompressFolder("UIAdvIcon", "ZRouting")
ImageLump_SetAllowLineCompression(false)

--Low-definition stuff.
gbUseLowDefinition = true
ImageLump_SetAllowLineCompression(true)
if(baCompressionFlags[4] == true) then LM_ExecuteScript(sCurrentPath .. "Maps/ZRouting.lua") end
ImageLump_SetAllowLineCompression(false)
gbUseLowDefinition = false

--[Subfolders]
--Execute subfolders which follow the same pattern as this file.
LM_ExecuteScript(sCurrentPath .. "AdventureScenes/ZExec.lua")
LM_ExecuteScript(sCurrentPath .. "Portraits/ZExec.lua")

--[Finish Up]
--Done
Debug_PopPrint("Finished image compression activities!\n")